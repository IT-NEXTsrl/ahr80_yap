* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bis                                                        *
*              Esegue e configura zoom partit                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_225]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-02-09                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bis",oParentObject,m.pEvent)
return(i_retval)

define class tgste_bis as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_MAXDEC = 0
  w_APPO = space(10)
  w_PUNT = 0
  w_PUNT0 = 0
  w_SALDO = 0
  w_O_PAGRD = space(2)
  w_O_PAGBO = space(2)
  w_O_PAGRB = space(2)
  w_O_PAGCA = space(2)
  w_O_PAGRI = space(2)
  w_O_PAGMA = space(2)
  w_O_REGINI = ctod("  /  /  ")
  w_O_REGFIN = ctod("  /  /  ")
  w_O_BANAPP = space(10)
  w_O_BANNOS = space(10)
  w_O_DDOFIN = ctod("  /  /  ")
  w_O_DDOINI = ctod("  /  /  ")
  w_O_NDOINI = 0
  w_O_NDOFIN = 0
  w_O_ADOINI = space(10)
  w_O_ADOFIN = space(10)
  w_O_CODAGE = space(5)
  w_O_PAGNO = space(2)
  w_O_FLPART1 = space(1)
  w_O_DATPAR = ctod("  /  /  ")
  w_LOOP = 0
  w_CODVAL = space(3)
  w_DATVAL = ctod("  /  /  ")
  * --- WorkFile variables
  VALUTE_idx=0
  TMP_PART3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue e Configura lo Zoom (da GSTE_KMS)
    if this.pEvent<>"TOT"
      this.oParentObject.w_FLGSOS = IIF(this.oParentObject.w_FLSOSP $ "TN", " ", "S")
      this.oParentObject.w_FLGNSO = IIF(this.oParentObject.w_FLSOSP $ "TS", " ", "S")
      if NVL(this.pEvent,"XXX")="AFT" AND EMPTY(w_SCACLI+w_SCAFOR+w_SCACON)
        * --- Per non eseguire la query iniziale
        i_retcode = 'stop'
        return
      endif
      * --- A seconda dello stato scadenza sul quale filtrare cambio la query dello zoom
      if this.oParentObject.w_FLSALD = "T" 
        this.oParentObject.w_ZOOMSCAD.cCpQueryName = "QUERY\GSTEAKMS"
      else
        this.oParentObject.w_ZOOMSCAD.cCpQueryName = "QUERY\GSTE_KMS"
        if this.oParentObject.w_FLSALD = "R"
          this.w_O_PAGRD = this.oParentObject.w_PAGRD
          this.w_O_PAGBO = this.oParentObject.w_PAGBO
          this.w_O_PAGRB = this.oParentObject.w_PAGRB
          this.w_O_PAGCA = this.oParentObject.w_PAGCA
          this.w_O_PAGRI = this.oParentObject.w_PAGRI
          this.w_O_PAGMA = this.oParentObject.w_PAGMA
          this.w_O_REGINI = this.oParentObject.w_REGINI
          this.w_O_REGFIN = this.oParentObject.w_REGFIN
          this.w_O_BANAPP = this.oParentObject.w_BANAPP
          this.w_O_BANNOS = this.oParentObject.w_BANNOS
          this.w_O_DDOFIN = this.oParentObject.w_DDOFIN
          this.w_O_DDOINI = this.oParentObject.w_DDOINI
          this.w_O_NDOINI = this.oParentObject.w_NDOINI
          this.w_O_NDOFIN = this.oParentObject.w_NDOFIN
          this.w_O_ADOINI = this.oParentObject.w_ADOINI
          this.w_O_ADOFIN = this.oParentObject.w_ADOFIN
          this.w_O_CODAGE = this.oParentObject.w_CODAGE
          this.w_O_PAGNO = this.oParentObject.w_PAGNO
          this.w_O_FLPART1 = this.oParentObject.w_FLPART1
          this.oParentObject.w_PAGRD = "RD"
          this.oParentObject.w_PAGBO = "BO"
          this.oParentObject.w_PAGRB = "RB"
          this.oParentObject.w_PAGCA = "CA"
          this.oParentObject.w_PAGRI = "RI"
          this.oParentObject.w_PAGMA = "MA"
          this.oParentObject.w_REGINI = cp_CharToDate("  /  /    ")
          this.oParentObject.w_REGFIN = cp_CharToDate("  /  /    ")
          this.oParentObject.w_BANAPP = ""
          this.oParentObject.w_BANNOS = ""
          this.oParentObject.w_DDOFIN = cp_CharToDate("  /  /    ")
          this.oParentObject.w_DDOINI = cp_CharToDate("  /  /    ")
          this.oParentObject.w_NDOINI = 0
          this.oParentObject.w_NDOFIN = 0
          this.oParentObject.w_ADOINI = ""
          this.oParentObject.w_ADOFIN = ""
          this.oParentObject.w_CODAGE = ""
          this.oParentObject.w_PAGNO = "NO"
          this.oParentObject.w_FLPART1 = ""
        endif
      endif
      * --- Create temporary table TMP_PART3
      i_nIdx=cp_AddTableDef('TMP_PART3') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_BGQEOJYYKQ[5]
      indexes_BGQEOJYYKQ[1]='NUMPAR'
      indexes_BGQEOJYYKQ[2]='DATSCA'
      indexes_BGQEOJYYKQ[3]='TIPCON'
      indexes_BGQEOJYYKQ[4]='CODCON'
      indexes_BGQEOJYYKQ[5]='CODVAL'
      vq_exec('GSTETKMS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_BGQEOJYYKQ,.f.)
      this.TMP_PART3_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      This.oParentObject.NotifyEvent("Ricerca")
      if this.oParentObject.w_FLSALD = "R"
        * --- Determino le partite aperte
        GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.oParentObject.w_TIPCON , this.oParentObject.w_CODCON ,this.oParentObject.w_CODCON , this.oParentObject.w_VALUTA , "", "RB","BO","RD","RI","MA","CA", "GSCG_BSA","N",this.oParentObject.w_DATPAR)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        vq_exec("QUERY\GSTE_KMS_PA.VQR", this, "PARTITE")
        GSTE_BCP(this,"A","PARTITE"," ")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_PAGRD = this.w_O_PAGRD
        this.oParentObject.w_PAGBO = this.w_O_PAGBO
        this.oParentObject.w_PAGRB = this.w_O_PAGRB
        this.oParentObject.w_PAGCA = this.w_O_PAGCA
        this.oParentObject.w_PAGRI = this.w_O_PAGRI
        this.oParentObject.w_PAGMA = this.w_O_PAGMA
        this.oParentObject.w_REGINI = this.w_O_REGINI
        this.oParentObject.w_REGFIN = this.w_O_REGFIN
        this.oParentObject.w_BANAPP = this.w_O_BANAPP
        this.oParentObject.w_BANNOS = this.w_O_BANNOS
        this.oParentObject.w_DDOFIN = this.w_O_DDOFIN
        this.oParentObject.w_DDOINI = this.w_O_DDOINI
        this.oParentObject.w_NDOINI = this.w_O_NDOINI
        this.oParentObject.w_NDOFIN = this.w_O_NDOFIN
        this.oParentObject.w_ADOINI = this.w_O_ADOINI
        this.oParentObject.w_ADOFIN = this.w_O_ADOFIN
        this.oParentObject.w_CODAGE = this.w_O_CODAGE
        this.oParentObject.w_PAGNO = this.w_O_PAGNO
        this.oParentObject.w_FLPART1 = this.w_O_FLPART1
         
 Select (this.oParentObject.w_ZOOMSCAD.cCursor) 
 Zap
        * --- Applico i filtri sul risultato:
        *     - Tipo di Pagamento
        *     - Intervallo date di registrazione
        *     - Intervallo numero documento
        *     - intervallo alfa
        *     - intervallo date documento
        *     - Agente
        *     - Banca d'appoggio
        *     - Nostra banca
        Select * FROM PARTITE WHERE ; 
 ( ( TIPPAG IN (this.oParentObject.w_PAGRD , this.oParentObject.w_PAGBO , this.oParentObject.w_PAGRB , this.oParentObject.w_PAGCA , this.oParentObject.w_PAGRI , this.oParentObject.w_PAGMA ) And Not Empty(Nvl(TIPPAG,"")) ) OR ; 
 (Empty(Nvl(TIPPAG,"")) And this.oParentObject.w_PAGNO="NO") ) ; 
 And ( Empty( this.oParentObject.w_REGINI ) Or CP_TODATE(DATREG) >= this.oParentObject.w_REGINI ) And ( Empty( this.oParentObject.w_REGFIN ) Or CP_TODATE(DATREG) <= this.oParentObject.w_REGFIN ) ; 
 And ( Empty( this.oParentObject.w_NDOINI ) Or NUMDOC>= this.oParentObject.w_NDOINI ) And ( Empty( this.oParentObject.w_NDOFIN ) Or NUMDOC<= this.oParentObject.w_NDOFIN ); 
 And ( Empty( this.oParentObject.w_ADOINI ) Or ALFDOC>= this.oParentObject.w_ADOINI ) And ( Empty( this.oParentObject.w_ADOFIN ) Or ALFDOC<= this.oParentObject.w_ADOFIN ); 
 And ( Empty( this.oParentObject.w_DDOINI ) Or CP_TODATE(DATDOC) >= this.oParentObject.w_DDOINI ) And ( Empty( this.oParentObject.w_DDOFIN ) Or CP_TODATE(DATDOC) <= this.oParentObject.w_DDOFIN ) ; 
 And ( Empty( this.oParentObject.w_CODAGE ) Or Nvl(CODAGE,SPACE(5)) = this.oParentObject.w_CODAGE ) ; 
 And ( Empty( this.oParentObject.w_BANAPP ) Or Nvl(BANAPP,SPACE(10)) = this.oParentObject.w_BANAPP ) ; 
 And ( Empty( this.oParentObject.w_BANNOS ) Or Nvl(BANNOS,SPACE(15)) = this.oParentObject.w_BANNOS ) ; 
 And (Empty(this.oParentObject.w_FLPART1 ) Or Nvl(FLCRSA, "")= this.oParentObject.w_FLPART1 ) ; 
 Into Cursor PARTITE
        * --- Inserisco record elaborati nello Zoom
        do while Not Eof( "PARTITE" )
          if Not Deleted()
             
 Scatter memVar 
 Select ( this.oParentObject.w_ZOOMSCAD.cCursor) 
 Append Blank 
 Gather MemVar
          endif
          if Not Eof( "PARTITE" )
             
 Select PARTITE 
 Skip
          endif
        enddo
        Use in PARTITE
      endif
      * --- Drop temporary table TMP_PART3
      i_nIdx=cp_GetTableDefIdx('TMP_PART3')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART3')
      endif
      * --- Determino il numero massimo dei decimali per impostare il formato degli importi...
      SELECT ( this.oParentObject.w_ZOOMSCAD.cCursor )
      GO TOP
      this.w_MAXDEC = 0
      Calculate Max( Nvl( VaDecTot , 0 ) ) To this.w_MAXDEC
      if this.w_MAXDEC<>g_PERPVL
        this.w_LOOP = 1
        do while this.w_LOOP <= this.oParentObject.w_ZOOMSCAD.grd.ColumnCount
          if ALLTRIM(UPPER( this.oParentObject.w_ZoomScad.grd.Columns[ this.w_LOOP ].ControlSource )) $ "IMPDAR-IMPAVE"
            * --- This.w_LOOP per mancata interpretazione  della w_LOOP
            this.oParentObject.w_ZOOMSCAD.grd.Columns[ this.w_LOOP ].InputMask = v_pv[20*( this.w_MAXDEC +2)]
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
      endif
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT ( this.oParentObject.w_ZOOMSCAD.cCursor )
      GO TOP
      * --- Refresh della griglia
      this.oParentObject.w_ZOOMSCAD.Refresh()     
      * --- Drop temporary table TMP_PART_APE
      i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART_APE')
      endif
    else
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_FLAGTOT="S" AND this.oParentObject.w_FLSALD="R"
       
 SELECT CODVAL,SUM(IIF(NVL(SEGNO," ")="D",TOTIMP,TOTIMP*0)) AS TOTDAR,SUM(IIF(NVL(SEGNO," ")="A",TOTIMP,TOTIMP*0)) AS TOTAVE; 
 FROM ( this.oParentObject.w_ZOOMSCAD.cCursor ) GROUP BY codval ORDER BY CODVAL INTO CURSOR TMP_VALUTE
      this.oParentObject.w_TOTAVE = 0
      this.oParentObject.w_TOTDAR = 0
      this.oParentObject.w_SALDOA = 0
      this.oParentObject.w_SALDOD = 0
      if Reccount("TMP_VALUTE") >0
        if Reccount("TMP_VALUTE")=1
          this.w_CODVAL = TMP_VALUTE.CODVAL
           
 SELECT ( this.oParentObject.w_ZOOMSCAD.cCursor ) 
 GO TOP
           
 Calculate Sum( Nvl( totimp , 0 ) ) To this.oParentObject.w_TOTDAR For Nvl(SEGNO," ")="D" 
 Calculate Sum( Nvl( totimp , 0 ) ) To this.oParentObject.w_TOTAVE For Nvl(SEGNO," ")="A"
        else
           
 SELECT TMP_VALUTE 
 USE 
 SELECT ( this.oParentObject.w_ZOOMSCAD.cCursor ) 
 SELECT CODVAL,IIF(FLCRSA="A","A","N") AS ACCONTI,IIF(FLCRSA="A",CAOVAL,CAOVAL*0+1) AS CAOACC, SUM(IIF(NVL(SEGNO," ")="D",TOTIMP,TOTIMP*0)) AS TOTDAR,; 
 SUM(IIF(NVL(SEGNO," ")="A",TOTIMP,TOTIMP*0)) AS TOTAVE; 
 FROM ( this.oParentObject.w_ZOOMSCAD.cCursor ) GROUP BY codval,ACCONTI,CAOACC ORDER BY CODVAL,FLCRSA INTO CURSOR TMP_VALUTE
          this.w_CODVAL = g_PERVAL
          this.w_DATVAL = iif(Not Empty(this.oParentObject.w_DATPAR),this.oParentObject.w_DATPAR,i_DATSYS)
           
 SELECT TMP_VALUTE 
 GO TOP 
 Scan
          if TMP_VALUTE.ACCONTI="A"
            this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + iif(CODVAL<>this.oParentObject.w_VALSAL,cp_Round(VAL2MON(TOTDAR , Nvl(TMP_VALUTE.CAOACC,0), g_CAOVAL, this.w_DATVAL, g_PERVAL),g_perpvl),TOTDAR)
            this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + iif(CODVAL<>this.oParentObject.w_VALSAL,cp_Round(VAL2MON(TOTAVE , Nvl(TMP_VALUTE.CAOACC,0), g_CAOVAL, this.w_DATVAL, g_PERVAL),g_perpvl),TOTAVE)
          else
            this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + iif(CODVAL<>this.oParentObject.w_VALSAL,cp_Round(VAL2MON(TOTDAR , GETCAM(CODVAL, this.w_DATVAL, 7), g_CAOVAL, this.w_DATVAL, g_PERVAL),g_perpvl),TOTDAR)
            this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + iif(CODVAL<>this.oParentObject.w_VALSAL,cp_Round(VAL2MON(TOTAVE , GETCAM(CODVAL, this.w_DATVAL, 0), g_CAOVAL, this.w_DATVAL, g_PERVAL),g_perpvl),TOTAVE)
          endif
          EndScan
        endif
      endif
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VASIMVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VASIMVAL;
          from (i_cTable) where;
              VACODVAL = this.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_VALSAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_SALDOD = iif(this.oParentObject.w_TOTDAR-this.oParentObject.w_TOTAVE>0,this.oParentObject.w_TOTDAR-this.oParentObject.w_TOTAVE,0)
      this.oParentObject.w_SALDOA = iif(this.oParentObject.w_TOTAVE-this.oParentObject.w_TOTDAR>0,this.oParentObject.w_TOTAVE-this.oParentObject.w_TOTDAR,0)
      if USED("TMP_VALUTE")
         
 Select TMP_VALUTE 
 USE
      endif
    endif
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='*TMP_PART3'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
