* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kmp                                                        *
*              Modifica password                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-14                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kmp",oParentObject))

* --- Class definition
define class tgsut_kmp as StdForm
  Top    = 77
  Left   = 100

  * --- Standard Properties
  Width  = 430
  Height = 203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-19"
  HelpContextID=68598935
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kmp"
  cComment = "Modifica password"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AMPASSWD = space(254)
  w_OLDPSW = space(254)
  w_PWDCL = space(1)
  o_PWDCL = space(1)
  w_NEWPSW = space(254)
  o_NEWPSW = space(254)
  w_NEWPSW2 = space(254)
  w_PASSWD = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kmpPag1","gsut_kmp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOLDPSW_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMPASSWD=space(254)
      .w_OLDPSW=space(254)
      .w_PWDCL=space(1)
      .w_NEWPSW=space(254)
      .w_NEWPSW2=space(254)
      .w_PASSWD=space(254)
      .w_AMPASSWD=oParentObject.w_AMPASSWD
      .w_PASSWD=oParentObject.w_PASSWD
          .DoRTCalc(1,4,.f.)
        .w_NEWPSW2 = iif(.w_PWDCL="S", .w_NEWPSW, "")
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(iif(.w_PWDCL="S","","*"))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(iif(.w_PWDCL="S","","*"))
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
    endwith
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AMPASSWD=.w_AMPASSWD
      .oParentObject.w_PASSWD=.w_PASSWD
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_PWDCL<>.w_PWDCL.or. .o_NEWPSW<>.w_NEWPSW
            .w_NEWPSW2 = iif(.w_PWDCL="S", .w_NEWPSW, "")
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(iif(.w_PWDCL="S","","*"))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(iif(.w_PWDCL="S","","*"))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(iif(.w_PWDCL="S","","*"))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(iif(.w_PWDCL="S","","*"))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(iif(.w_PWDCL="S","","*"))
    endwith
  return

  proc Calculate_YVAUGVUWQU()
    with this
          * --- setpwd
     if CifraCnf( ALLTRIM(.w_OLDPSW),'C' )==ALLTRIM(.w_AMPASSWD) and .w_NEWPSW==.w_NEWPSW2
          .w_AMPASSWD = iif(empty(.w_NEWPSW), " ", CifraCnf( ALLTRIM(.w_NEWPSW),"C"))
          .w_PASSWD = .w_NEWPSW
     endif
    endwith
  endproc
  proc Calculate_FWIJLRSZCC()
    with this
          * --- ResetPwd
          .w_AMPASSWD = ""
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNEWPSW2_1_5.visible=!this.oPgFrm.Page1.oPag.oNEWPSW2_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
        if lower(cEvent)==lower("Update start")
          .Calculate_YVAUGVUWQU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ResetPwd")
          .Calculate_FWIJLRSZCC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOLDPSW_1_2.value==this.w_OLDPSW)
      this.oPgFrm.Page1.oPag.oOLDPSW_1_2.value=this.w_OLDPSW
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDCL_1_3.RadioValue()==this.w_PWDCL)
      this.oPgFrm.Page1.oPag.oPWDCL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPSW_1_4.value==this.w_NEWPSW)
      this.oPgFrm.Page1.oPag.oNEWPSW_1_4.value=this.w_NEWPSW
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPSW2_1_5.value==this.w_NEWPSW2)
      this.oPgFrm.Page1.oPag.oNEWPSW2_1_5.value=this.w_NEWPSW2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) == ALLTRIM(.w_AMPASSWD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLDPSW_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La password digitata non � corretta")
          case   not(empty(.w_NEWPSW) or empty(.w_NEWPSW2) or .w_NEWPSW==.w_NEWPSW2)  and not(.w_PWDCL="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNEWPSW2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le password digitate non corrispondono")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PWDCL = this.w_PWDCL
    this.o_NEWPSW = this.w_NEWPSW
    return

enddefine

* --- Define pages as container
define class tgsut_kmpPag1 as StdContainer
  Width  = 426
  height = 203
  stdWidth  = 426
  stdheight = 203
  resizeXpos=259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOLDPSW_1_2 as StdField with uid="OKEPJEIKGG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_OLDPSW", cQueryName = "OLDPSW",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "La password digitata non � corretta",;
    ToolTipText = "Vecchia password",;
    HelpContextID = 258257434,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=18, InputMask=replicate('X',254)

  func oOLDPSW_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) == ALLTRIM(.w_AMPASSWD))
    endwith
    return bRes
  endfunc

  add object oPWDCL_1_3 as StdCheck with uid="ZOZEABRGYY",rtseq=3,rtrep=.f.,left=180, top=117, caption="Mostra password", tabstop=.f.,;
    ToolTipText = "Visualizza la password in chiaro",;
    HelpContextID = 152983798,;
    cFormVar="w_PWDCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPWDCL_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPWDCL_1_3.GetRadio()
    this.Parent.oContained.w_PWDCL = this.RadioValue()
    return .t.
  endfunc

  func oPWDCL_1_3.SetRadio()
    this.Parent.oContained.w_PWDCL=trim(this.Parent.oContained.w_PWDCL)
    this.value = ;
      iif(this.Parent.oContained.w_PWDCL=='S',1,;
      0)
  endfunc

  add object oNEWPSW_1_4 as StdField with uid="PORZOOKYQF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NEWPSW", cQueryName = "NEWPSW",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nuova password",;
    HelpContextID = 258181418,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=65, InputMask=replicate('X',254)

  add object oNEWPSW2_1_5 as StdField with uid="CCBMOOPWJZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NEWPSW2", cQueryName = "NEWPSW2",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Le password digitate non corrispondono",;
    ToolTipText = "Conferma nuova password",;
    HelpContextID = 258181418,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=91, InputMask=replicate('X',254)

  func oNEWPSW2_1_5.mHide()
    with this.Parent.oContained
      return (.w_PWDCL="S")
    endwith
  endfunc

  func oNEWPSW2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_NEWPSW) or empty(.w_NEWPSW2) or .w_NEWPSW==.w_NEWPSW2)
    endwith
    return bRes
  endfunc


  add object oObj_1_9 as cp_setobjprop with uid="AUWPJKIPCT",left=0, top=224, width=120,height=21,;
    caption='NEWPSW',;
   bGlobalFont=.t.,;
    cObj="w_NEWPSW",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 258181418


  add object oObj_1_10 as cp_setobjprop with uid="ZXZGERWBKE",left=0, top=244, width=120,height=21,;
    caption='NEWPSW2',;
   bGlobalFont=.t.,;
    cObj="w_NEWPSW2",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 258181418


  add object oObj_1_11 as cp_setobjprop with uid="KWUURUPULT",left=0, top=204, width=120,height=21,;
    caption='OLDPSW',;
   bGlobalFont=.t.,;
    cObj="w_OLDPSW",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 258257434


  add object oBtn_1_12 as StdButton with uid="JSQALMJPAL",left=10, top=149, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per eliminare la password";
    , HelpContextID = 115688122;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      this.parent.oContained.NotifyEvent("ResetPwd")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_AMPASSWD))
     endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="APIWEFNJEL",left=313, top=149, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la password";
    , HelpContextID = 108890602;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not(CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) == ALLTRIM(.w_AMPASSWD) and .w_NEWPSW==.w_NEWPSW2))
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="ZJIUAZYUNY",left=368, top=149, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108890602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="FPPIBGTEZW",Visible=.t., Left=20, Top=18,;
    Alignment=1, Width=158, Height=18,;
    Caption="Vecchia password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="EMUXMWUMPQ",Visible=.t., Left=14, Top=91,;
    Alignment=1, Width=164, Height=18,;
    Caption="Conferma nuova password:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_PWDCL="S")
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="VDFPFVJAYN",Visible=.t., Left=20, Top=65,;
    Alignment=1, Width=158, Height=18,;
    Caption="Nuova password:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kmp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
