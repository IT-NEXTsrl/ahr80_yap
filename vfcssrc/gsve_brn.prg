* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_brn                                                        *
*              Controllo righe piene contratti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-30                                                      *
* Last revis.: 2012-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_brn",oParentObject,m.pPAR)
return(i_retval)

define class tgsve_brn as StdBatch
  * --- Local variables
  pPAR = space(1)
  w_MSGERR = space(100)
  w_COUNTER = 0
  w_RECPOS = 0
  w_AbsRow = 0
  w_nRelRow = 0
  w_PADRE = .NULL.
  w_RECO = 0
  w_LOBSART = ctod("  /  /  ")
  w_LCODART = space(20)
  w_Padre = .NULL.
  w_GSVE_MSO = .NULL.
  w_CONTRNUMERO = space(15)
  w_SERIAL = space(10)
  w_TIPDOC = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATREG = ctod("  /  /  ")
  w_CONTASCA = 0
  w_CONTARIG = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  CON_COSC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo righe piene Contratti da GSAC_MCO e GSVE_MCO
    * --- codice articolo su dettaglio del contratto
    this.w_PADRE = this.OparentObject
    * --- ===========================
    this.oParentObject.w_RIGHERIP = .F.
    this.oParentObject.w_NORIGHE = .F.
    do case
      case this.pPAR $ "NDG"
        if not empty(this.oParentObject.w_COCODART)
          this.w_COUNTER = 0
          do case
            case this.pPAR="N"
              this.w_PADRE.MarkPos()     
              position=recno()
              * --- CONTROLLA SE UN ARTICOLO E' STATO GIA' INSERITO.
              * --- conto le righe del cursore del dettaglio (diverse da quella attualmente modificata)
              * --- aventi articolo uguale a quello appena impostato.
              this.w_COUNTER = 0
              Count for (t_COCODART==this.oParentObject.w_COCODART and recno()<>position) And Not Deleted() To this.w_COUNTER
              * --- Mi rimetto nella riga di partenza
              this.w_PADRE.RePos()     
              if this.w_COUNTER>0
                * --- deve esistere almeno un'altra riga con quell'articolo
                this.w_MSGERR = "Articolo gi� presente nel contratto"
                ah_ErrorMsg(this.w_MSGERR,"!")
                this.oParentObject.w_COCODART = space(20)
                this.oParentObject.w_DESART = space(40)
                this.w_PADRE.SaveRow()     
                this.w_PADRE.Set("w_COCODART", SPACE(20))     
                this.w_PADRE.Set("w_DESART", SPACE(40))     
              endif
          endcase
          do case
            case this.pPAR="D"
              * --- CONTROLLA SE UN ARTICOLO E' OBSOLETO
              this.w_PADRE.MarkPos()     
              this.w_PADRE.FirstRow()     
              do while Not this.w_PADRE.Eof_Trs()
                if not empty(this.oParentObject.w_COCODART)
                  this.w_LCODART = this.w_PADRE.GET("t_COCODART")
                  this.w_LOBSART = this.w_PADRE.GET("t_OBSART")
                  = CHKDTOBS(this.w_LOBSART,this.oParentObject.w_CODATFIN,"<%1> articolo obsoleto",.T. , ALLTRIM(this.w_LCODART) )
                endif
                this.w_PADRE.NextRow()     
              enddo
              this.w_PADRE.RePos()     
          endcase
        endif
        if not empty(this.oParentObject.w_COGRUMER) and this.pPAR="G"
          this.w_COUNTER = 0
          this.w_PADRE.MarkPos()     
          position=recno()
          * --- CONTROLLA SE UN ARTICOLO E' STATO GIA' INSERITO.
          * --- conto le righe del cursore del dettaglio (diverse da quella attualmente modificata)
          * --- aventi articolo uguale a quello appena impostato.
          this.w_COUNTER = 0
          Count for (t_COGRUMER==this.oParentObject.w_COGRUMER and recno()<>position) And Not Deleted() To this.w_COUNTER
          * --- Mi rimetto nella riga di partenza
          this.w_PADRE.RePos()     
          if this.w_COUNTER>0
            * --- deve esistere almeno un'altra riga con quell'articolo
            this.w_MSGERR = "Gruppo merceologico gi� presente nel contratto"
            ah_ErrorMsg(this.w_MSGERR,"!")
            this.oParentObject.w_COGRUMER = space(5)
            this.oParentObject.w_DESGRAR = space(45)
            this.w_PADRE.SaveRow()     
            this.w_PADRE.Set("w_COGRUMER", SPACE(5))     
            this.w_PADRE.Set("w_DESGRAR", SPACE(45))     
          endif
        endif
      case this.pPAR="R"
        this.w_Padre = this.oParentObject
        if Empty(this.oParentObject.w_COFLUCOA) Or this.oParentObject.w_COFLUCOA="N"
           
 Select(this.w_Padre.cTrsName) 
 Count for Not Deleted() And (Not Empty(t_COGRUMER) Or Not Empty(t_COCODART)) To this.w_RECO
          if this.w_RECO=0
            this.oParentObject.w_NORIGHE = .T.
          endif
        endif
        if !this.oParentObject.w_NORIGHE
          this.w_GSVE_MSO = this.oParentObject.GSVE_MSO
          this.w_padre.MarkPos()     
          this.w_padre.FirstRow()     
          do while !this.w_padre.Eof_Trs()
            if this.w_padre.FullRow()
              this.w_padre.SetRow()     
              this.w_CONTRNUMERO = this.oParentObject.w_CONUMERO
              this.oParentObject.w_CONTRNUMROW = this.oParentObject.w_CPROWNUM
              this.w_GSVE_MSO.Exec_Select("_Tmp_BRN","t_coquanti,coquanti","t_coquanti<>coquanti AND (i_SRV='A' OR i_SRV='U') AND NOT DELETED()","coquanti")     
              select ("_Tmp_BRN") 
 go top 
 scan
              this.oParentObject.w_QUANTI = t_coquanti
              * --- Read from CON_COSC
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CON_COSC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2],.t.,this.CON_COSC_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "*"+;
                  " from "+i_cTable+" CON_COSC where ";
                      +"COCODICE = "+cp_ToStrODBC(this.w_CONTRNUMERO);
                      +" and CONUMROW = "+cp_ToStrODBC(this.oParentObject.w_CONTRNUMROW);
                      +" and COQUANTI = "+cp_ToStrODBC(this.oParentObject.w_QUANTI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  *;
                  from (i_cTable) where;
                      COCODICE = this.w_CONTRNUMERO;
                      and CONUMROW = this.oParentObject.w_CONTRNUMROW;
                      and COQUANTI = this.oParentObject.w_QUANTI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows>0
                this.oParentObject.w_RIGHERIP = .T.
                EXIT
              endif
              SELECT("_Tmp_BRN")
              Endscan
              USE IN SELECT("_Tmp_BRN")
            endif
            this.w_padre.NextRow()     
          enddo
          this.w_padre.RePos()     
        endif
      case this.pPAR="C"
        * --- Esegue Controllo Cancellazione Contratti
        this.w_MSGERR = " "
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MVSERIAL,MVCONTRA  from "+i_cTable+" DOC_DETT ";
              +" where MVCONTRA="+cp_ToStrODBC(this.oParentObject.w_CONUMERO)+"";
               ,"_Curs_DOC_DETT")
        else
          select MVSERIAL,MVCONTRA from (i_cTable);
           where MVCONTRA=this.oParentObject.w_CONUMERO;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          this.w_SERIAL = _Curs_DOC_DETT.MVSERIAL
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATREG"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATREG;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not EMPTY(this.w_TIPDOC)
            this.w_MSGERR = ah_Msgformat("%1 numero: %2 del %3",alltrim(this.w_TIPDOC), alltrim(str(this.w_NUMDOC,15))+iif(not empty(nvl(this.w_ALFDOC,SPACE(10))),"/"+Alltrim(this.w_ALFDOC),""), dtoc(this.w_DATREG))
          endif
          Exit
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
        if Not Empty(this.w_MSGERR)
          this.w_MSGERR = ah_Msgformat("Impossibile cancellare, contratto presente nel doc.: %1", alltrim(this.w_MSGERR) )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MSGERR
        endif
      case this.pPAR="S"
        this.w_CONTARIG = this.w_PADRE.NumRow()
        if this.w_CONTARIG<>0
          this.w_PADRE.SaveRow()     
          this.w_PADRE.Markpos()     
          this.w_PADRE.Firstrow()     
          this.w_PADRE.ChildrenchangeRow()     
          this.w_CONTASCA = this.w_PADRE.GSVE_MSO.NumRow()
          this.w_PADRE.Repos()     
          this.w_PADRE.ChildrenChangeRow()     
          if this.w_CONTASCA<>0 
            this.w_MSGERR = ah_ErrorMsg("Esistono delle righe nel dettaglio del contratto.%0Impossibile attivare/disattivare gli scaglioni ")
            this.oParentObject.o_COQUANTI = "S"
            this.oParentObject.w_COQUANTI = "S"
          endif
        endif
        if this.w_CONTARIG<>0 AND this.w_CONTASCA=0
          this.w_MSGERR = ah_ErrorMsg("Esistono delle righe nel dettaglio del contratto.%0Impossibile attivare/disattivare gli scaglioni ")
          this.oParentObject.o_COQUANTI = "N"
          this.oParentObject.w_COQUANTI = "N"
        endif
        if Not Empty(this.w_MSGERR)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MSGERR
        endif
    endcase
  endproc


  proc Init(oParentObject,pPAR)
    this.pPAR=pPAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='CON_COSC'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPAR"
endproc
