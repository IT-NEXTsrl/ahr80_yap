* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bog                                                        *
*              Apertura generazione da gestioni                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-29                                                      *
* Last revis.: 2007-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pKEYRIF,pOPENTYPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bog",oParentObject,m.pKEYRIF,m.pOPENTYPE)
return(i_retval)

define class tgsve_bog as StdBatch
  * --- Local variables
  pKEYRIF = space(15)
  pOPENTYPE = space(1)
  w_PROG = .NULL.
  w_GDSERIAL = space(10)
  w_GDPARAME = space(25)
  * --- WorkFile variables
  GENERDOC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riferimento all'anagrafica padre
    this.w_GDPARAME = This.oParentObject.Class
    do case
      case this.w_GDPARAME = "Tgsco_ago"
        this.w_GDPARAME = "Tgsco_ksf"
      case this.w_GDPARAME = "Tgsco_agd"
        this.w_GDPARAME = "Tgsco1ksf"
      case this.w_GDPARAME = "Tgsdb_ago"
        this.w_GDPARAME = "Tgsdb_ksf"
      case this.w_GDPARAME = "Tgsva_aim"
        this.w_GDPARAME = "Tgsva_bim"
    endcase
    if this.w_GDPARAME<>"Tgsva_bim"
      * --- Read from GENERDOC
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.GENERDOC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GENERDOC_idx,2],.t.,this.GENERDOC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "GDSERIAL"+;
          " from "+i_cTable+" GENERDOC where ";
              +"GDKEYRIF = "+cp_ToStrODBC(this.pKEYRIF);
              +" and GDPARAME = "+cp_ToStrODBC(this.w_GDPARAME);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          GDSERIAL;
          from (i_cTable) where;
              GDKEYRIF = this.pKEYRIF;
              and GDPARAME = this.w_GDPARAME;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GDSERIAL = NVL(cp_ToDate(_read_.GDSERIAL),cp_NullValue(_read_.GDSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_GDSERIAL = ALLTRIM(this.pKEYRIF)
    endif
    if EMPTY(NVL(this.w_GDSERIAL," "))
      ah_errormsg("Impossibile trovare chiave di riferimento. Piano di generazione eliminato")
    else
      do case
        case this.pOPENTYPE=="A"
          this.w_PROG = GSVE_AGD()
        case this.pOPENTYPE=="L"
          this.w_PROG = GSVE_KLE()
      endcase
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        this.w_PROG = .NULL.
        ah_errormsg("Impossibile aprire gestione")
        i_retcode = 'stop'
        return
      endif
      * --- carico il documento
      this.w_PROG.w_GDSERIAL = this.w_GDSERIAL
      this.w_PROG.QueryKeySet("GDSERIAL='"+ this.w_GDSERIAL +"'")     
      this.w_PROG.LoadRecWarn()     
      this.w_PROG = .NULL.
    endif
  endproc


  proc Init(oParentObject,pKEYRIF,pOPENTYPE)
    this.pKEYRIF=pKEYRIF
    this.pOPENTYPE=pOPENTYPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GENERDOC'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pKEYRIF,pOPENTYPE"
endproc
