* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kou                                                        *
*              Genera file da output utente                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-12                                                      *
* Last revis.: 2011-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kou",oParentObject))

* --- Class definition
define class tgsut_kou as StdForm
  Top    = 68
  Left   = 45

  * --- Standard Properties
  Width  = 741
  Height = 500
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-03"
  HelpContextID=166282089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  OUT_PUTS_IDX = 0
  cPrg = "gsut_kou"
  cComment = "Genera file da output utente"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_COPATHOU = space(200)
  w_OUNOMPRG1 = space(30)
  w_OUNOMPRG2 = space(30)
  w_OUROWNUM1 = 0
  w_OUROWNUM2 = 0
  w_PERSONALIZZA = space(1)
  o_PERSONALIZZA = space(1)
  w_SELEZI = space(1)
  w_PATH = space(200)
  o_PATH = space(200)
  w_MOTORE = space(3)
  o_MOTORE = space(3)
  w_APPLICAZIONE = space(3)
  w_APPLICAZIONE = space(3)
  w_APPLICAZIONE = space(3)
  w_LOCALIZZAZIONE = space(3)
  w_LPATH = .F.
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kouPag1","gsut_kou",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOUNOMPRG1_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='OUT_PUTS'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_COPATHOU=space(200)
      .w_OUNOMPRG1=space(30)
      .w_OUNOMPRG2=space(30)
      .w_OUROWNUM1=0
      .w_OUROWNUM2=0
      .w_PERSONALIZZA=space(1)
      .w_SELEZI=space(1)
      .w_PATH=space(200)
      .w_MOTORE=space(3)
      .w_APPLICAZIONE=space(3)
      .w_APPLICAZIONE=space(3)
      .w_APPLICAZIONE=space(3)
      .w_LOCALIZZAZIONE=space(3)
      .w_LPATH=.f.
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_OUNOMPRG1 = THIS.OPARENTOBJECT.w_OUNOMPRG
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_OUNOMPRG1))
          .link_1_3('Full')
        endif
        .w_OUNOMPRG2 = THIS.OPARENTOBJECT.w_OUNOMPRG
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_OUNOMPRG2))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_PERSONALIZZA = 'N'
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .w_SELEZI = "D"
        .w_PATH = .w_COPATHOU
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_MOTORE = IIF (UPPER (g_APPLICATION) ='ADHOC REVOLUTION','1','2')
        .w_APPLICAZIONE = ' '
        .w_APPLICAZIONE = ' '
        .w_APPLICAZIONE = ' '
        .w_LOCALIZZAZIONE = ' '
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        if .o_PERSONALIZZA<>.w_PERSONALIZZA
          .Calculate_DCOZFBHVKV()
        endif
        if .o_MOTORE<>.w_MOTORE
          .Calculate_RMZEZSXPCV()
        endif
        if .o_PATH<>.w_PATH
          .Calculate_OORDRXUUEC()
        endif
        if .o_PATH<>.w_PATH
          .Calculate_LMZUISUYVI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return

  proc Calculate_DCOZFBHVKV()
    with this
          * --- Se il flag output personalizzati � attivo si impostano i filtri sul numero output
          .w_OUROWNUM1 = IIF (.w_PERSONALIZZA='S' , 100,0)
          .w_OUROWNUM2 = 0
    endwith
  endproc
  proc Calculate_RMZEZSXPCV()
    with this
          * --- Al variare della linea si selezionano tutti i prodotti
          .w_APPLICAZIONE = ' '
    endwith
  endproc
  proc Calculate_OORDRXUUEC()
    with this
          * --- Aggiunge estensione al file di outpu
          .w_PATH = IIF (RIGHT(alltrim (.w_PATH) , 1)='.',alltrim(.w_PATH)+'INF', IIF (right(alltrim(.w_PATH),1)='\',alltrim(.w_PATH), IIF (not empty (.w_PATH) AND right(alltrim(.w_PATH),1)<>':'AND at ('.' , alltrim(.w_PATH),1)=0 ,alltrim(.w_PATH) + '.INF',  alltrim(.w_PATH)  )   )   )
    endwith
  endproc
  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_LPATH = chknfile(.w_path,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOUROWNUM1_1_5.enabled = this.oPgFrm.Page1.oPag.oOUROWNUM1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oOUROWNUM2_1_6.enabled = this.oPgFrm.Page1.oPag.oOUROWNUM2_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_kou
    
     IF (g_ADHOCONE<>.T. AND UPPER (cevent)='BLANK'  )
     This.oPgFrm.Height=500
     This.Height=500
     ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHOU";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHOU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_COPATHOU = NVL(_Link_.COPATHOU,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_COPATHOU = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OUNOMPRG1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OUNOMPRG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OUNOMPRG like "+cp_ToStrODBC(trim(this.w_OUNOMPRG1)+"%");

          i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OUNOMPRG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMPRG',trim(this.w_OUNOMPRG1))
          select OUNOMPRG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OUNOMPRG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OUNOMPRG1)==trim(_Link_.OUNOMPRG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OUNOMPRG1) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMPRG',cp_AbsName(oSource.parent,'oOUNOMPRG1_1_3'),i_cWhere,'',"",'GSAR_SOU.OUT_PUTS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                     +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',oSource.xKey(1))
            select OUNOMPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OUNOMPRG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                   +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(this.w_OUNOMPRG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',this.w_OUNOMPRG1)
            select OUNOMPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OUNOMPRG1 = NVL(_Link_.OUNOMPRG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_OUNOMPRG1 = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMPRG,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OUNOMPRG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OUNOMPRG2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OUNOMPRG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OUNOMPRG like "+cp_ToStrODBC(trim(this.w_OUNOMPRG2)+"%");

          i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OUNOMPRG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMPRG',trim(this.w_OUNOMPRG2))
          select OUNOMPRG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OUNOMPRG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OUNOMPRG2)==trim(_Link_.OUNOMPRG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OUNOMPRG2) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMPRG',cp_AbsName(oSource.parent,'oOUNOMPRG2_1_4'),i_cWhere,'',"",'GSAR_SOU.OUT_PUTS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                     +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',oSource.xKey(1))
            select OUNOMPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OUNOMPRG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG";
                   +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(this.w_OUNOMPRG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',this.w_OUNOMPRG2)
            select OUNOMPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OUNOMPRG2 = NVL(_Link_.OUNOMPRG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_OUNOMPRG2 = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMPRG,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OUNOMPRG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOUNOMPRG1_1_3.value==this.w_OUNOMPRG1)
      this.oPgFrm.Page1.oPag.oOUNOMPRG1_1_3.value=this.w_OUNOMPRG1
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMPRG2_1_4.value==this.w_OUNOMPRG2)
      this.oPgFrm.Page1.oPag.oOUNOMPRG2_1_4.value=this.w_OUNOMPRG2
    endif
    if not(this.oPgFrm.Page1.oPag.oOUROWNUM1_1_5.value==this.w_OUROWNUM1)
      this.oPgFrm.Page1.oPag.oOUROWNUM1_1_5.value=this.w_OUROWNUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oOUROWNUM2_1_6.value==this.w_OUROWNUM2)
      this.oPgFrm.Page1.oPag.oOUROWNUM2_1_6.value=this.w_OUROWNUM2
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSONALIZZA_1_7.RadioValue()==this.w_PERSONALIZZA)
      this.oPgFrm.Page1.oPag.oPERSONALIZZA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_10.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_11.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_11.value=this.w_PATH
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PERSONALIZZA = this.w_PERSONALIZZA
    this.o_PATH = this.w_PATH
    this.o_MOTORE = this.w_MOTORE
    return

enddefine

* --- Define pages as container
define class tgsut_kouPag1 as StdContainer
  Width  = 737
  height = 500
  stdWidth  = 737
  stdheight = 500
  resizeXpos=340
  resizeYpos=247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOUNOMPRG1_1_3 as StdField with uid="UZFSUCTORX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_OUNOMPRG1", cQueryName = "OUNOMPRG1",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Filtro sulla gestione di inizio selezione",;
    HelpContextID = 188414525,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=121, Top=8, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="OUT_PUTS", oKey_1_1="OUNOMPRG", oKey_1_2="this.w_OUNOMPRG1"

  func oOUNOMPRG1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oOUNOMPRG1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOUNOMPRG1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OUT_PUTS','*','OUNOMPRG',cp_AbsName(this.parent,'oOUNOMPRG1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSAR_SOU.OUT_PUTS_VZM',this.parent.oContained
  endproc

  add object oOUNOMPRG2_1_4 as StdField with uid="BBSYREEBWT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_OUNOMPRG2", cQueryName = "OUNOMPRG2",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Filtro sulla gestione di fine selezione",;
    HelpContextID = 188414541,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=121, Top=32, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="OUT_PUTS", oKey_1_1="OUNOMPRG", oKey_1_2="this.w_OUNOMPRG2"

  func oOUNOMPRG2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oOUNOMPRG2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOUNOMPRG2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OUT_PUTS','*','OUNOMPRG',cp_AbsName(this.parent,'oOUNOMPRG2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSAR_SOU.OUT_PUTS_VZM',this.parent.oContained
  endproc

  add object oOUROWNUM1_1_5 as StdField with uid="SMMEMEUAAE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OUROWNUM1", cQueryName = "OUROWNUM1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtro sul numero di riga dell'output",;
    HelpContextID = 165362243,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=437, Top=8, cSayPict='"999"', cGetPict='"999"'

  func oOUROWNUM1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERSONALIZZA<>'S')
    endwith
   endif
  endfunc

  add object oOUROWNUM2_1_6 as StdField with uid="TXYVRSLPUE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OUROWNUM2", cQueryName = "OUROWNUM2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtro sul numero di riga dell'output",;
    HelpContextID = 165362259,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=437, Top=32, cSayPict='"999"', cGetPict='"999"'

  func oOUROWNUM2_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERSONALIZZA<>'S')
    endwith
   endif
  endfunc

  add object oPERSONALIZZA_1_7 as StdCheck with uid="FTNBUQXXBJ",rtseq=7,rtrep=.f.,left=501, top=8, caption="Output personalizzati",;
    ToolTipText = "Filtro utilizzato per estrarre gli output derivanti da personalizzazioni (cio� tutti quegli output utente che hanno un numero d' ordine maggiore di 99)",;
    HelpContextID = 161883602,;
    cFormVar="w_PERSONALIZZA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERSONALIZZA_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPERSONALIZZA_1_7.GetRadio()
    this.Parent.oContained.w_PERSONALIZZA = this.RadioValue()
    return .t.
  endfunc

  func oPERSONALIZZA_1_7.SetRadio()
    this.Parent.oContained.w_PERSONALIZZA=trim(this.Parent.oContained.w_PERSONALIZZA)
    this.value = ;
      iif(this.Parent.oContained.w_PERSONALIZZA=='S',1,;
      0)
  endfunc


  add object oBtn_1_8 as StdButton with uid="PSTUCDPWKC",left=685, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 10640150;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .NotifyEvent ("RICALCOLA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomSel as cp_szoombox with uid="GYSKREOWDB",left=1, top=60, width=734,height=367,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OUT_PUTS",cZoomFile="GSUT_KOU",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSUT_MOU",;
    cEvent = "RICALCOLA,Init",;
    nPag=1;
    , HelpContextID = 11715558

  add object oSELEZI_1_10 as StdRadio with uid="IRSHIEQNET",rtseq=8,rtrep=.f.,left=7, top=444, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 83937062
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 83937062
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_10.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_10.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_10.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oPATH_1_11 as StdField with uid="YJTBDAQENK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file (se si specifica solo il nome file senza un path allora il file verr� salvato nella cartella exe dell installazione)",;
    HelpContextID = 161201418,;
   bGlobalFont=.t.,;
    Height=21, Width=390, Left=220, Top=460, InputMask=replicate('X',200)


  add object oBtn_1_12 as StdButton with uid="BYWCYEOHKL",left=612, top=464, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 166081066;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSUT_BOU(this.Parent.oContained,"PATH")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_18 as cp_runprogram with uid="NRQAUDFRKG",left=8, top=637, width=260,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BOU('SELEZIONE')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 11715558


  add object oBtn_1_30 as StdButton with uid="RWDSSEOVZU",left=635, top=452, width=48,height=45,;
    CpPicture="BMP\save.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare su un file gli output utenti selezionati";
    , HelpContextID = 193099286;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSUT_BOU (this.Parent.oContained,"SALVA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATH) and directory(substr(.w_path,1,rat('\',.w_path))))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="ZSONRLTWLJ",left=685, top=452, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193099286;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_13 as StdString with uid="CIEMDLLWBU",Visible=.t., Left=7, Top=12,;
    Alignment=1, Width=111, Height=18,;
    Caption="Da gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DTOSOUFINR",Visible=.t., Left=22, Top=36,;
    Alignment=1, Width=96, Height=18,;
    Caption="A gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UCBAVOLVGE",Visible=.t., Left=352, Top=12,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BVQTFLSZGZ",Visible=.t., Left=357, Top=36,;
    Alignment=1, Width=78, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SHCDKROQHP",Visible=.t., Left=143, Top=465,;
    Alignment=1, Width=75, Height=15,;
    Caption="Path+file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZFMCQFXUBT",Visible=.t., Left=12, Top=506,;
    Alignment=0, Width=533, Height=18,;
    Caption="Il file di output generato dovr� essere valido per le seguenti linee, prodotti: e localizzazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (g_adhocone<>.T.)
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="LKKYEBKUGG",Visible=.t., Left=10, Top=528,;
    Alignment=0, Width=34, Height=18,;
    Caption="Linea:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (g_adhocone<>.T.)
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="HWMFCEWULK",Visible=.t., Left=267, Top=528,;
    Alignment=0, Width=54, Height=18,;
    Caption="Prodotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (g_adhocone<>.T.)
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="YNLVYJYXPQ",Visible=.t., Left=517, Top=528,;
    Alignment=0, Width=82, Height=18,;
    Caption="Localizzazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (g_adhocone<>.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kou','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
