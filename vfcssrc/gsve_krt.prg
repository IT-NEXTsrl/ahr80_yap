* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_krt                                                        *
*              Dati testata documento                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_112]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-13                                                      *
* Last revis.: 2012-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_krt",oParentObject))

* --- Class definition
define class tgsve_krt as StdForm
  Top    = 51
  Left   = 195

  * --- Standard Properties
  Width  = 494
  Height = 402
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-02-09"
  HelpContextID=116011625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=61

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  CAU_CONT_IDX = 0
  AGENTI_IDX = 0
  CAM_AGAZ_IDX = 0
  cPrg = "gsve_krt"
  cComment = "Dati testata documento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVDATREG = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_TIPDOC = space(2)
  w_MVCLADOC = space(2)
  w_FLRIFE = space(1)
  w_MVFLVEAC = space(1)
  w_FLINTR = space(1)
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_DTOBSO = ctod('  /  /  ')
  w_TIPIVA = space(1)
  w_TIPREG = space(1)
  w_CFLPP = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_RAGSOC = space(40)
  w_INDIRI = space(35)
  w_CODCAP = space(8)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_CODFIS = space(16)
  w_MVFLVABD = space(1)
  w_MVCAUCON = space(5)
  w_DESCAC = space(35)
  w_MVCODIVE = space(5)
  w_DESAPP = space(35)
  w_MV__NOTE = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_PARCAU = space(1)
  w_FLCCAU = space(1)
  w_MVTCAMAG = space(5)
  w_DESCAU = space(35)
  w_FLAVAL = space(1)
  w_OBSCAU = ctod('  /  /  ')
  w_FLGEFA = space(1)
  w_INDIVE = 0
  w_MVCODCOM = space(15)
  w_MVFLEVAS = space(1)
  w_MVVALNAZ = space(3)
  w_COCODVAL = space(3)
  w_CAONAZ = 0
  w_MVCAOVAL = 0
  w_MVDATDOC = ctod('  /  /  ')
  w_CAUCOL = space(5)
  w_VARVAL = space(1)
  w_MVTIPDOC = space(5)
  o_MVTIPDOC = space(5)
  w_MVFLSCOM = space(1)
  w_NUMRIG = 0
  w_LIVRAGG = space(1)
  w_MVTFRAGG = space(1)
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod('  /  /  ')
  w_MVAGG_06 = ctod('  /  /  ')
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_CAMAGG01 = .NULL.
  w_CAMAGG02 = .NULL.
  w_CAMAGG03 = .NULL.
  w_CAMAGG04 = .NULL.
  w_CAMAGG05 = .NULL.
  w_CAMAGG06 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_krt
  *--- Conta le righe di detaglio che hanno MVCODICE e MVPREZZO pieni
  *--- La procedura valorizza la variabile w_NUMRIG
  Proc CountRig()
     with this
       .oParentObject.Exec_Select('TMP_KFT','Count(*) As Conta','NOT DELETED() AND ( NOT EMPTY(t_MVCODICE) OR t_MVPREZZO > 0 )','','')
       .w_NUMRIG=Nvl(TMP_KFT.Conta, 0 )
       Use In Tmp_Kft
     endwith
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_krtPag1","gsve_krt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRAGSOC_1_17
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CAMAGG01 = this.oPgFrm.Pages(1).oPag.CAMAGG01
    this.w_CAMAGG02 = this.oPgFrm.Pages(1).oPag.CAMAGG02
    this.w_CAMAGG03 = this.oPgFrm.Pages(1).oPag.CAMAGG03
    this.w_CAMAGG04 = this.oPgFrm.Pages(1).oPag.CAMAGG04
    this.w_CAMAGG05 = this.oPgFrm.Pages(1).oPag.CAMAGG05
    this.w_CAMAGG06 = this.oPgFrm.Pages(1).oPag.CAMAGG06
    DoDefault()
    proc Destroy()
      this.w_CAMAGG01 = .NULL.
      this.w_CAMAGG02 = .NULL.
      this.w_CAMAGG03 = .NULL.
      this.w_CAMAGG04 = .NULL.
      this.w_CAMAGG05 = .NULL.
      this.w_CAMAGG06 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='CAM_AGAZ'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVDATREG=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_TIPDOC=space(2)
      .w_MVCLADOC=space(2)
      .w_FLRIFE=space(1)
      .w_MVFLVEAC=space(1)
      .w_FLINTR=space(1)
      .w_BOLIVE=space(1)
      .w_PERIVE=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_TIPIVA=space(1)
      .w_TIPREG=space(1)
      .w_CFLPP=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_RAGSOC=space(40)
      .w_INDIRI=space(35)
      .w_CODCAP=space(8)
      .w_LOCALI=space(30)
      .w_PROVIN=space(2)
      .w_CODFIS=space(16)
      .w_MVFLVABD=space(1)
      .w_MVCAUCON=space(5)
      .w_DESCAC=space(35)
      .w_MVCODIVE=space(5)
      .w_DESAPP=space(35)
      .w_MV__NOTE=space(0)
      .w_OBTEST=ctod("  /  /  ")
      .w_PARCAU=space(1)
      .w_FLCCAU=space(1)
      .w_MVTCAMAG=space(5)
      .w_DESCAU=space(35)
      .w_FLAVAL=space(1)
      .w_OBSCAU=ctod("  /  /  ")
      .w_FLGEFA=space(1)
      .w_INDIVE=0
      .w_MVCODCOM=space(15)
      .w_MVFLEVAS=space(1)
      .w_MVVALNAZ=space(3)
      .w_COCODVAL=space(3)
      .w_CAONAZ=0
      .w_MVCAOVAL=0
      .w_MVDATDOC=ctod("  /  /  ")
      .w_CAUCOL=space(5)
      .w_VARVAL=space(1)
      .w_MVTIPDOC=space(5)
      .w_MVFLSCOM=space(1)
      .w_NUMRIG=0
      .w_LIVRAGG=space(1)
      .w_MVTFRAGG=space(1)
      .w_MVAGG_01=space(15)
      .w_MVAGG_02=space(15)
      .w_MVAGG_03=space(15)
      .w_MVAGG_04=space(15)
      .w_MVAGG_05=ctod("  /  /  ")
      .w_MVAGG_06=ctod("  /  /  ")
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_MVDATREG=oParentObject.w_MVDATREG
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_TIPDOC=oParentObject.w_TIPDOC
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_FLINTR=oParentObject.w_FLINTR
      .w_BOLIVE=oParentObject.w_BOLIVE
      .w_PERIVE=oParentObject.w_PERIVE
      .w_TIPIVA=oParentObject.w_TIPIVA
      .w_TIPREG=oParentObject.w_TIPREG
      .w_CFLPP=oParentObject.w_CFLPP
      .w_MVFLVABD=oParentObject.w_MVFLVABD
      .w_MVCAUCON=oParentObject.w_MVCAUCON
      .w_MVCODIVE=oParentObject.w_MVCODIVE
      .w_MV__NOTE=oParentObject.w_MV__NOTE
      .w_PARCAU=oParentObject.w_PARCAU
      .w_FLCCAU=oParentObject.w_FLCCAU
      .w_MVTCAMAG=oParentObject.w_MVTCAMAG
      .w_FLGEFA=oParentObject.w_FLGEFA
      .w_INDIVE=oParentObject.w_INDIVE
      .w_MVCODCOM=oParentObject.w_MVCODCOM
      .w_MVFLEVAS=oParentObject.w_MVFLEVAS
      .w_MVVALNAZ=oParentObject.w_MVVALNAZ
      .w_COCODVAL=oParentObject.w_COCODVAL
      .w_CAONAZ=oParentObject.w_CAONAZ
      .w_MVCAOVAL=oParentObject.w_MVCAOVAL
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_CAUCOL=oParentObject.w_CAUCOL
      .w_VARVAL=oParentObject.w_VARVAL
      .w_MVTIPDOC=oParentObject.w_MVTIPDOC
      .w_MVFLSCOM=oParentObject.w_MVFLSCOM
      .w_MVAGG_01=oParentObject.w_MVAGG_01
      .w_MVAGG_02=oParentObject.w_MVAGG_02
      .w_MVAGG_03=oParentObject.w_MVAGG_03
      .w_MVAGG_04=oParentObject.w_MVAGG_04
      .w_MVAGG_05=oParentObject.w_MVAGG_05
      .w_MVAGG_06=oParentObject.w_MVAGG_06
      .w_DACAM_01=oParentObject.w_DACAM_01
      .w_DACAM_02=oParentObject.w_DACAM_02
      .w_DACAM_03=oParentObject.w_DACAM_03
      .w_DACAM_04=oParentObject.w_DACAM_04
      .w_DACAM_05=oParentObject.w_DACAM_05
      .w_DACAM_06=oParentObject.w_DACAM_06
          .DoRTCalc(1,14,.f.)
        .w_RAGSOC = SUBSTR(.w_MV__NOTE, 1, 40)
        .w_INDIRI = SUBSTR(.w_MV__NOTE, 41, 35)
        .w_CODCAP = SUBSTR(.w_MV__NOTE, 76, 8)
        .w_LOCALI = SUBSTR(.w_MV__NOTE, 84, 30)
        .w_PROVIN = SUBSTR(.w_MV__NOTE, 114, 2)
        .w_CODFIS = SUBSTR(.w_MV__NOTE, 116, 16)
          .DoRTCalc(21,21,.f.)
        .w_MVCAUCON = .w_MVCAUCON
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MVCAUCON))
          .link_1_24('Full')
        endif
          .DoRTCalc(23,23,.f.)
        .w_MVCODIVE = .w_MVCODIVE
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MVCODIVE))
          .link_1_27('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(25,30,.f.)
        if not(empty(.w_MVTCAMAG))
          .link_1_40('Full')
        endif
      .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
    this.DoRTCalc(31,61,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_krt
    *--- Valorizzo w_NUMRIG
    this.CountRig()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVDATREG=.w_MVDATREG
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_TIPDOC=.w_TIPDOC
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_FLINTR=.w_FLINTR
      .oParentObject.w_BOLIVE=.w_BOLIVE
      .oParentObject.w_PERIVE=.w_PERIVE
      .oParentObject.w_TIPIVA=.w_TIPIVA
      .oParentObject.w_TIPREG=.w_TIPREG
      .oParentObject.w_CFLPP=.w_CFLPP
      .oParentObject.w_MVFLVABD=.w_MVFLVABD
      .oParentObject.w_MVCAUCON=.w_MVCAUCON
      .oParentObject.w_MVCODIVE=.w_MVCODIVE
      .oParentObject.w_MV__NOTE=.w_MV__NOTE
      .oParentObject.w_PARCAU=.w_PARCAU
      .oParentObject.w_FLCCAU=.w_FLCCAU
      .oParentObject.w_MVTCAMAG=.w_MVTCAMAG
      .oParentObject.w_FLGEFA=.w_FLGEFA
      .oParentObject.w_INDIVE=.w_INDIVE
      .oParentObject.w_MVCODCOM=.w_MVCODCOM
      .oParentObject.w_MVFLEVAS=.w_MVFLEVAS
      .oParentObject.w_MVVALNAZ=.w_MVVALNAZ
      .oParentObject.w_COCODVAL=.w_COCODVAL
      .oParentObject.w_CAONAZ=.w_CAONAZ
      .oParentObject.w_MVCAOVAL=.w_MVCAOVAL
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_CAUCOL=.w_CAUCOL
      .oParentObject.w_VARVAL=.w_VARVAL
      .oParentObject.w_MVTIPDOC=.w_MVTIPDOC
      .oParentObject.w_MVFLSCOM=.w_MVFLSCOM
      .oParentObject.w_MVAGG_01=.w_MVAGG_01
      .oParentObject.w_MVAGG_02=.w_MVAGG_02
      .oParentObject.w_MVAGG_03=.w_MVAGG_03
      .oParentObject.w_MVAGG_04=.w_MVAGG_04
      .oParentObject.w_MVAGG_05=.w_MVAGG_05
      .oParentObject.w_MVAGG_06=.w_MVAGG_06
      .oParentObject.w_DACAM_01=.w_DACAM_01
      .oParentObject.w_DACAM_02=.w_DACAM_02
      .oParentObject.w_DACAM_03=.w_DACAM_03
      .oParentObject.w_DACAM_04=.w_DACAM_04
      .oParentObject.w_DACAM_05=.w_DACAM_05
      .oParentObject.w_DACAM_06=.w_DACAM_06
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .DoRTCalc(1,29,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC
          .link_1_40('Full')
        endif
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,61,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRAGSOC_1_17.enabled = this.oPgFrm.Page1.oPag.oRAGSOC_1_17.mCond()
    this.oPgFrm.Page1.oPag.oINDIRI_1_18.enabled = this.oPgFrm.Page1.oPag.oINDIRI_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCODCAP_1_19.enabled = this.oPgFrm.Page1.oPag.oCODCAP_1_19.mCond()
    this.oPgFrm.Page1.oPag.oLOCALI_1_20.enabled = this.oPgFrm.Page1.oPag.oLOCALI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPROVIN_1_21.enabled = this.oPgFrm.Page1.oPag.oPROVIN_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCODFIS_1_22.enabled = this.oPgFrm.Page1.oPag.oCODFIS_1_22.mCond()
    this.oPgFrm.Page1.oPag.oMVFLVABD_1_23.enabled = this.oPgFrm.Page1.oPag.oMVFLVABD_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMVCAUCON_1_24.enabled = this.oPgFrm.Page1.oPag.oMVCAUCON_1_24.mCond()
    this.oPgFrm.Page1.oPag.oMVCODIVE_1_27.enabled = this.oPgFrm.Page1.oPag.oMVCODIVE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMVTCAMAG_1_40.enabled = this.oPgFrm.Page1.oPag.oMVTCAMAG_1_40.mCond()
    this.oPgFrm.Page1.oPag.oMVFLSCOM_1_57.enabled = this.oPgFrm.Page1.oPag.oMVFLSCOM_1_57.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG01.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG02.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG03.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG04.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG05.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG06.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCAUCON
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_MVCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_MVCAUCON))
          select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oMVCAUCON_1_24'),i_cWhere,'GSCG_ACC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MVCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MVCAUCON)
            select CCCODICE,CCDESCRI,CCTIPDOC,CCFLPPRO,CCTIPREG,CCFLRIFE,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAC = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_CFLPP = NVL(_Link_.CCFLPPRO,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_PARCAU = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCON = space(5)
      endif
      this.w_DESCAC = space(35)
      this.w_TIPDOC = space(2)
      this.w_CFLPP = space(1)
      this.w_TIPREG = space(1)
      this.w_FLRIFE = space(1)
      this.w_PARCAU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUDO(.w_MVCLADOC, .w_FLINTR, .w_MVFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile incongruente con il tipo documento")
        endif
        this.w_MVCAUCON = space(5)
        this.w_DESCAC = space(35)
        this.w_TIPDOC = space(2)
        this.w_CFLPP = space(1)
        this.w_TIPREG = space(1)
        this.w_FLRIFE = space(1)
        this.w_PARCAU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODIVE
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODIVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVE)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVCODIVE))
          select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODIVE)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODIVE) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVCODIVE_1_27'),i_cWhere,'GSAR_AIV',"",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODIVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVCODIVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVCODIVE)
            select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODIVE = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESAPP = NVL(_Link_.IVDESIVA,space(35))
      this.w_BOLIVE = NVL(_Link_.IVBOLIVA,space(1))
      this.w_PERIVE = NVL(_Link_.IVPERIVA,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODIVE = space(5)
      endif
      this.w_DESAPP = space(35)
      this.w_BOLIVE = space(1)
      this.w_PERIVE = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PERIVE=0 OR .w_TIPIVA='N') AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA esente incongruente o obsoleto")
        endif
        this.w_MVCODIVE = space(5)
        this.w_DESAPP = space(35)
        this.w_BOLIVE = space(1)
        this.w_PERIVE = 0
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODIVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCAMAG
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MVTCAMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MVTCAMAG))
          select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTCAMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMCAUCOL like "+cp_ToStrODBC(trim(this.w_MVTCAMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMCAUCOL like "+cp_ToStr(trim(this.w_MVTCAMAG)+"%");

            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVTCAMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMVTCAMAG_1_40'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVTCAMAG)
            select CMCODICE,CMCAUCOL,CMVARVAL,CMDESCRI,CMDTOBSO,CMFLAVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_VARVAL = NVL(_Link_.CMVARVAL,space(1))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_OBSCAU = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLAVAL = NVL(_Link_.CMFLAVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCAMAG = space(5)
      endif
      this.w_CAUCOL = space(5)
      this.w_VARVAL = space(1)
      this.w_DESCAU = space(35)
      this.w_OBSCAU = ctod("  /  /  ")
      this.w_FLAVAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUMA(.w_MVTCAMAG,.w_CAUCOL,.w_FLGEFA,.w_FLAVAL,.w_OBSCAU,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVTCAMAG = space(5)
        this.w_CAUCOL = space(5)
        this.w_VARVAL = space(1)
        this.w_DESCAU = space(35)
        this.w_OBSCAU = ctod("  /  /  ")
        this.w_FLAVAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_17.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_17.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRI_1_18.value==this.w_INDIRI)
      this.oPgFrm.Page1.oPag.oINDIRI_1_18.value=this.w_INDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAP_1_19.value==this.w_CODCAP)
      this.oPgFrm.Page1.oPag.oCODCAP_1_19.value=this.w_CODCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCALI_1_20.value==this.w_LOCALI)
      this.oPgFrm.Page1.oPag.oLOCALI_1_20.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVIN_1_21.value==this.w_PROVIN)
      this.oPgFrm.Page1.oPag.oPROVIN_1_21.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIS_1_22.value==this.w_CODFIS)
      this.oPgFrm.Page1.oPag.oCODFIS_1_22.value=this.w_CODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLVABD_1_23.RadioValue()==this.w_MVFLVABD)
      this.oPgFrm.Page1.oPag.oMVFLVABD_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCAUCON_1_24.value==this.w_MVCAUCON)
      this.oPgFrm.Page1.oPag.oMVCAUCON_1_24.value=this.w_MVCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAC_1_25.value==this.w_DESCAC)
      this.oPgFrm.Page1.oPag.oDESCAC_1_25.value=this.w_DESCAC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODIVE_1_27.value==this.w_MVCODIVE)
      this.oPgFrm.Page1.oPag.oMVCODIVE_1_27.value=this.w_MVCODIVE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTCAMAG_1_40.value==this.w_MVTCAMAG)
      this.oPgFrm.Page1.oPag.oMVTCAMAG_1_40.value=this.w_MVTCAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_41.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_41.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLSCOM_1_57.RadioValue()==this.w_MVFLSCOM)
      this.oPgFrm.Page1.oPag.oMVFLSCOM_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_01_1_61.value==this.w_MVAGG_01)
      this.oPgFrm.Page1.oPag.oMVAGG_01_1_61.value=this.w_MVAGG_01
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_02_1_62.value==this.w_MVAGG_02)
      this.oPgFrm.Page1.oPag.oMVAGG_02_1_62.value=this.w_MVAGG_02
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_03_1_63.value==this.w_MVAGG_03)
      this.oPgFrm.Page1.oPag.oMVAGG_03_1_63.value=this.w_MVAGG_03
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_04_1_64.value==this.w_MVAGG_04)
      this.oPgFrm.Page1.oPag.oMVAGG_04_1_64.value=this.w_MVAGG_04
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_05_1_65.value==this.w_MVAGG_05)
      this.oPgFrm.Page1.oPag.oMVAGG_05_1_65.value=this.w_MVAGG_05
    endif
    if not(this.oPgFrm.Page1.oPag.oMVAGG_06_1_66.value==this.w_MVAGG_06)
      this.oPgFrm.Page1.oPag.oMVAGG_06_1_66.value=this.w_MVAGG_06
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MVCAUCON)) or not(CHKCAUDO(.w_MVCLADOC, .w_FLINTR, .w_MVFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG)))  and (g_COGE='S' AND .w_MVCLADOC $ 'FA-NC-RF' AND oParentObject.cFunction<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCAUCON_1_24.SetFocus()
            i_bnoObbl = !empty(.w_MVCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile incongruente con il tipo documento")
          case   not((.w_PERIVE=0 OR .w_TIPIVA='N') AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST))  and (oParentObject.cFunction<>'Query')  and not(empty(.w_MVCODIVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODIVE_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA esente incongruente o obsoleto")
          case   not(CHKCAUMA(.w_MVTCAMAG,.w_CAUCOL,.w_FLGEFA,.w_FLAVAL,.w_OBSCAU,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC))  and (.w_FLCCAU = 'S')  and not(empty(.w_MVTCAMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTCAMAG_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVTIPDOC = this.w_MVTIPDOC
    return

enddefine

* --- Define pages as container
define class tgsve_krtPag1 as StdContainer
  Width  = 490
  height = 402
  stdWidth  = 490
  stdheight = 402
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_14 as StdButton with uid="FFDXXKHWVG",left=363, top=343, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 115982874;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="FZVFDIMRTG",left=414, top=343, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 108694202;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRAGSOC_1_17 as StdField with uid="LGAJLJJEPU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome e nome intestatario",;
    HelpContextID = 22905878,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=142, Top=13, InputMask=replicate('X',40)

  func oRAGSOC_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oINDIRI_1_18 as StdField with uid="YCXINFTTWO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo intestatario",;
    HelpContextID = 126050438,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=142, Top=42, InputMask=replicate('X',35)

  func oINDIRI_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oCODCAP_1_19 as StdField with uid="UQLWCSADCY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODCAP", cQueryName = "CODCAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP intestatario",;
    HelpContextID = 225272102,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=142, Top=71, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oCODCAP_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oLOCALI_1_20 as StdField with uid="WESJEHCMHC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� intestatario",;
    HelpContextID = 119230902,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=215, Top=71, InputMask=replicate('X',30)

  func oLOCALI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oPROVIN_1_21 as StdField with uid="RVTVKDHCHH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla provincia intestatario",;
    HelpContextID = 201397494,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=452, Top=71, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oPROVIN_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oCODFIS_1_22 as StdField with uid="YYWGNKEADH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODFIS", cQueryName = "CODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 15753510,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=142, Top=100, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  func oCODFIS_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oMVFLVABD_1_23 as StdCheck with uid="BJRLERBNOE",rtseq=21,rtrep=.f.,left=142, top=125, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 264669450,;
    cFormVar="w_MVFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLVABD_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLVABD_1_23.GetRadio()
    this.Parent.oContained.w_MVFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oMVFLVABD_1_23.SetRadio()
    this.Parent.oContained.w_MVFLVABD=trim(this.Parent.oContained.w_MVFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLVABD=='S',1,;
      0)
  endfunc

  func oMVFLVABD_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oMVCAUCON_1_24 as StdField with uid="WWZQVUJTOK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVCAUCON", cQueryName = "MVCAUCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile incongruente con il tipo documento",;
    ToolTipText = "Codice di contabilizzazione del documento",;
    HelpContextID = 240428780,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=187, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_MVCAUCON"

  func oMVCAUCON_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S' AND .w_MVCLADOC $ 'FA-NC-RF' AND oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oMVCAUCON_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCAUCON_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCAUCON_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oMVCAUCON_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"",'',this.parent.oContained
  endproc
  proc oMVCAUCON_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_MVCAUCON
     i_obj.ecpSave()
  endproc

  add object oDESCAC_1_25 as StdField with uid="CWBOYSDZID",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAC", cQueryName = "DESCAC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 7227190,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=187, InputMask=replicate('X',35)

  add object oMVCODIVE_1_27 as StdField with uid="HGJTHWRDKR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVCODIVE", cQueryName = "MVCODIVE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA esente incongruente o obsoleto",;
    ToolTipText = "Codice IVA applicato in caso di documento esente o non imponibile",;
    HelpContextID = 111761675,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVCODIVE"

  func oMVCODIVE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  func oMVCODIVE_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODIVE_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODIVE_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVCODIVE_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVCODIVE_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVCODIVE
     i_obj.ecpSave()
  endproc


  add object oObj_1_36 as cp_runprogram with uid="URPPZJHFIM",left=10, top=419, width=195,height=20,;
    caption='GSVE_BED(T)',;
   bGlobalFont=.t.,;
    prg='GSVE_BED("T")',;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 22244394

  add object oMVTCAMAG_1_40 as StdField with uid="GXKZJANOVX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MVTCAMAG", cQueryName = "MVTCAMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale magazzino",;
    HelpContextID = 175008013,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=153, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVTCAMAG"

  func oMVTCAMAG_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCCAU = 'S')
    endwith
   endif
  endfunc

  func oMVTCAMAG_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTCAMAG_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTCAMAG_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oMVTCAMAG_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oMVTCAMAG_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_MVTCAMAG
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_41 as StdField with uid="LXEOTEPTNR",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 40781622,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=153, InputMask=replicate('X',35)

  add object oMVFLSCOM_1_57 as StdCheck with uid="WCZPARSCRJ",rtseq=46,rtrep=.f.,left=313, top=125, caption="Calcola sconti su omaggi",;
    ToolTipText = "Se attivo: calcola sconti su omaggio",;
    HelpContextID = 241792749,;
    cFormVar="w_MVFLSCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLSCOM_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMVFLSCOM_1_57.GetRadio()
    this.Parent.oContained.w_MVFLSCOM = this.RadioValue()
    return .t.
  endfunc

  func oMVFLSCOM_1_57.SetRadio()
    this.Parent.oContained.w_MVFLSCOM=trim(this.Parent.oContained.w_MVFLSCOM)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLSCOM=='S',1,;
      0)
  endfunc

  func oMVFLSCOM_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject.cFunction = 'Load' AND .w_NUMRIG < 1)
    endwith
   endif
  endfunc

  add object oMVAGG_01_1_61 as StdField with uid="NLDMPNYWUJ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MVAGG_01", cQueryName = "MVAGG_01",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 215038199,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=208, Top=241, InputMask=replicate('X',15)

  add object oMVAGG_02_1_62 as StdField with uid="FVGOOVMXGE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MVAGG_02", cQueryName = "MVAGG_02",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 215038200,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=208, Top=266, InputMask=replicate('X',15)

  add object oMVAGG_03_1_63 as StdField with uid="QFPECASIYR",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MVAGG_03", cQueryName = "MVAGG_03",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 215038201,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=208, Top=291, InputMask=replicate('X',15)

  add object oMVAGG_04_1_64 as StdField with uid="NUIJEEJKYX",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MVAGG_04", cQueryName = "MVAGG_04",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 215038202,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=208, Top=316, InputMask=replicate('X',15)

  add object oMVAGG_05_1_65 as StdField with uid="CAKNRPLCGH",rtseq=54,rtrep=.f.,;
    cFormVar = "w_MVAGG_05", cQueryName = "MVAGG_05",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 215038203,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=208, Top=341

  add object oMVAGG_06_1_66 as StdField with uid="UGTDQKRBLY",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MVAGG_06", cQueryName = "MVAGG_06",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 215038204,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=208, Top=366


  add object CAMAGG01 as cp_calclbl with uid="JZCKXLHDHV",left=4, top=242, width=202,height=18,;
    caption='Campo aggiuntivo 1',;
   bGlobalFont=.t.,;
    caption="Campo 1",Alignment =1,;
    nPag=1;
    , HelpContextID = 248318356


  add object CAMAGG02 as cp_calclbl with uid="YBFIFIXNYF",left=4, top=268, width=202,height=18,;
    caption='Campo aggiuntivo 2',;
   bGlobalFont=.t.,;
    caption="Campo 2",Alignment =1,;
    nPag=1;
    , HelpContextID = 248318100


  add object CAMAGG03 as cp_calclbl with uid="EELMGPXNWQ",left=4, top=293, width=202,height=18,;
    caption='Campo aggiuntivo 3',;
   bGlobalFont=.t.,;
    caption="Campo 3",alignment =1,;
    nPag=1;
    , HelpContextID = 248317844


  add object CAMAGG04 as cp_calclbl with uid="WWUGRPNAOG",left=4, top=318, width=202,height=18,;
    caption='Campo aggiuntivo 4',;
   bGlobalFont=.t.,;
    caption="Campo 4",alignment =1,;
    nPag=1;
    , HelpContextID = 248317588


  add object CAMAGG05 as cp_calclbl with uid="BLEZPMMBEJ",left=4, top=343, width=202,height=18,;
    caption='Campo aggiuntivo 5',;
   bGlobalFont=.t.,;
    caption="Campo 5",alignment =1,;
    nPag=1;
    , HelpContextID = 248317332


  add object CAMAGG06 as cp_calclbl with uid="ORVYGKKIKW",left=4, top=368, width=202,height=18,;
    caption='Campo aggiuntivo 6',;
   bGlobalFont=.t.,;
    caption="Campo 6",alignment =1,;
    nPag=1;
    , HelpContextID = 248317076

  add object oStr_1_26 as StdString with uid="KOZSFZJAPK",Visible=.t., Left=14, Top=217,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice IVA esenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HZZVAIQCFS",Visible=.t., Left=14, Top=187,;
    Alignment=1, Width=126, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GURRAPEZMN",Visible=.t., Left=14, Top=13,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cognome e nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="RTBULAONQX",Visible=.t., Left=14, Top=42,;
    Alignment=1, Width=126, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CQKMYEWZWK",Visible=.t., Left=14, Top=71,;
    Alignment=1, Width=126, Height=15,;
    Caption="CAP - localit� - prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IANKLBBVBH",Visible=.t., Left=14, Top=100,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="NFFULGAHBT",Visible=.t., Left=438, Top=71,;
    Alignment=2, Width=13, Height=15,;
    Caption="--"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="TPBTTIGVHB",Visible=.t., Left=5, Top=153,;
    Alignment=1, Width=135, Height=18,;
    Caption="Causale di magazzino:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_krt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
