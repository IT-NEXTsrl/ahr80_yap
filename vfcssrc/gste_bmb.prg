* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bmb                                                        *
*              Modifica banca d'appoggio/ns banca dalla manutenzione partite/scadenze*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_25]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-11                                                      *
* Last revis.: 2012-05-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bmb",oParentObject,m.pPARAM)
return(i_retval)

define class tgste_bmb as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_PADRE = .NULL.
  w_RECO = 0
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_PTBANAPP = space(10)
  w_PTBANNOS = space(15)
  w_PTNUMCOR = space(25)
  w_TIPPAG = space(2)
  w_TEST = .f.
  w_MESS = space(200)
  w_SEGNO = space(1)
  w_BANNOSLOC = space(15)
  w_BANAPPLOC = space(10)
  w_CONCORLOC = space(25)
  w_CODCON = space(15)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica Banca d'Appoggio/Ns Banca dalla Manutenzione Partite/Scadenze
    this.w_TEST = .F.
    this.w_PADRE = this.oparentobject.oparentobject
    NC = this.w_PADRE.w_ZOOMSCAD.cCursor
    if this.pPARAM="B"
      if NOT EMPTY(this.oParentObject.w_BANAPP) OR NOT EMPTY(this.oParentObject.w_BANNOS) OR NOT EMPTY(this.oParentObject.w_CONCOR)
        * --- Try
        local bErr_038356C8
        bErr_038356C8=bTrsErr
        this.Try_038356C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(this.w_MESS,,"")
        endif
        bTrsErr=bTrsErr or bErr_038356C8
        * --- End
      else
        ah_ErrorMsg("Nessun dato inserito",,"")
      endif
    else
      this.oParentObject.w_OK = .F.
       
 Select ( NC ) 
 GO TOP 
 SCAN FOR XCHK=1
      this.w_CODCON = Nvl(CODCON,"")
      LOCATE FOR CODCON <> this.w_CODCON And XCHK = 1
      if Found()
        this.oParentObject.w_OK = .T.
        EXIT
      endif
      ENDSCAN
    endif
  endproc
  proc Try_038356C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 SELECT (NC) 
 GO TOP 
 SCAN FOR XCHK<>0
    this.w_PTSERIAL = PTSERIAL
    this.w_PTROWORD = PTROWORD
    this.w_CPROWNUM = CPROWNUM
    this.w_PTBANAPP = NVL(BANAPP, SPACE(10))
    this.w_PTBANNOS = NVL(BANNOS, SPACE(15))
    this.w_PTNUMCOR = NVL(PTNUMCOR, SPACE(25))
    this.w_TIPPAG = NVL(TIPPAG, SPACE(2))
    this.w_BANNOSLOC = IIF(EMPTY(this.oParentObject.w_BANNOS), this.w_PTBANNOS, this.oParentObject.w_BANNOS)
    if empty(this.oParentObject.w_BANAPP)
      this.w_BANAPPLOC = this.w_PTBANAPP
      this.w_CONCORLOC = this.w_PTNUMCOR
    else
      if this.oParentObject.w_BANAPP # this.w_PTBANAPP
        this.w_BANAPPLOC = this.oParentObject.w_BANAPP
        this.w_CONCORLOC = this.oParentObject.w_CONCOR
      else
        this.w_BANAPPLOC = this.w_PTBANAPP
        if ! empty(this.oParentObject.w_CONCOR)
          this.w_CONCORLOC = this.oParentObject.w_CONCOR
        else
          this.w_CONCORLOC = this.w_PTNUMCOR
        endif
      endif
    endif
    this.w_MESS = "Errore durante l'aggiornamento; operazione abbandonata"
    if NVL(this.w_TIPPAG,SPACE(2))="BO" and Empty(Nvl(this.oParentObject.w_CONCOR," ")) and NVL(this.w_SEGNO," ")="A"
      this.w_MESS = "Pagamento di tipo bonifico: inserire il conto corrente"
      * --- Raise
      i_Error=this.w_MESS
      return
    else
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTBANAPP ="+cp_NullLink(cp_ToStrODBC(this.w_BANAPPLOC),'PAR_TITE','PTBANAPP');
        +",PTBANNOS ="+cp_NullLink(cp_ToStrODBC(this.w_BANNOSLOC),'PAR_TITE','PTBANNOS');
        +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_CONCORLOC),'PAR_TITE','PTNUMCOR');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PTBANAPP = this.w_BANAPPLOC;
            ,PTBANNOS = this.w_BANNOSLOC;
            ,PTNUMCOR = this.w_CONCORLOC;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_PTSERIAL;
            and PTROWORD = this.w_PTROWORD;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_TEST = .T.
    ENDSCAN
    if NOT this.w_TEST
      ah_ErrorMsg("Selezionare almeno una scadenza",,"")
    else
      ah_ErrorMsg("Aggiornamento eseguito con successo",,"")
      THIS.OPARENTOBJECT.ECPQUIT
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Ricarico il cursore, in modo da avere i nuovi dati disponibili
    DO GSTE_BIS WITH this.w_PADRE, "BTN"
    return


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
