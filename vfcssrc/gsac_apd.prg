* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_apd                                                        *
*              Dettaglio PDA                                                   *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_129]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-28                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_apd"))

* --- Class definition
define class tgsac_apd as StdForm
  Top    = 12
  Left   = 11

  * --- Standard Properties
  Width  = 560
  Height = 369+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=160065385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=68

  * --- Constant Properties
  PDA_DETT_IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  CONTI_IDX = 0
  UNIMIS_IDX = 0
  MAGAZZIN_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  ATTIVITA_IDX = 0
  PAR_PROD_IDX = 0
  CON_TRAM_IDX = 0
  cFile = "PDA_DETT"
  cKeySelect = "PDSERIAL,PDROWNUM"
  cKeyWhere  = "PDSERIAL=this.w_PDSERIAL and PDROWNUM=this.w_PDROWNUM"
  cKeyWhereODBC = '"PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';
      +'+" and PDROWNUM="+cp_ToStrODBC(this.w_PDROWNUM)';

  cKeyWhereODBCqualified = '"PDA_DETT.PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';
      +'+" and PDA_DETT.PDROWNUM="+cp_ToStrODBC(this.w_PDROWNUM)';

  cPrg = "gsac_apd"
  cComment = "Dettaglio PDA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PPCODICE = space(2)
  w_OBTEST = ctod('  /  /  ')
  w_PDSERIAL = space(10)
  w_PDROWNUM = 0
  w_PDSTATUS = space(1)
  w_OLDSTATUS = space(1)
  w_PDCODICE = space(20)
  o_PDCODICE = space(20)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_COCONA = space(15)
  w_PDCODART = space(20)
  w_DESART = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_TIPGES = space(1)
  w_FLSERG = space(1)
  w_CODART = space(20)
  w_GIOAPP = 0
  w_QTAMIN = 0
  w_LOTRIO = 0
  w_CODCON = space(15)
  w_PDDATEVA = ctod('  /  /  ')
  w_PDDATORD = ctod('  /  /  ')
  w_PDTIPCON = space(1)
  w_PDCODCON = space(15)
  o_PDCODCON = space(15)
  w_DESFOR = space(40)
  w_PDCONTRA = space(15)
  w_PDCRIFOR = space(1)
  w_PDUMORDI = space(3)
  w_PDQTAORD = 0
  w_PDUNIMIS = space(3)
  w_PDQTAUM1 = 0
  w_PDSERRIF = space(10)
  w_PDCODMAG = space(5)
  w_DESMAG = space(30)
  w_PDCODCEN = space(15)
  w_DESCON = space(40)
  w_TIPVOC = space(1)
  w_DTOBVO = ctod('  /  /  ')
  w_PDVOCCOS = space(15)
  w_VOCDES = space(40)
  w_PDCODCOM = space(15)
  w_DTOBSO = ctod('  /  /  ')
  w_PDTIPATT = space(1)
  w_PDCODATT = space(15)
  w_PDPERASS = space(3)
  w_MOLTI3 = 0
  w_ARTOBSO = ctod('  /  /  ')
  w_MODUM2 = space(1)
  w_FLFRAZ = space(1)
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_PPCALSTA = space(5)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_CV = space(3)
  w_IVACON = space(1)
  w_FCODVAL = space(3)
  o_FCODVAL = space(3)
  w_CODVAL = space(3)
  w_CATCOM = space(3)
  w_FLSCOR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_PDCRIELA = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_PDSERIAL = this.W_PDSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PDA_DETT','gsac_apd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_apdPag1","gsac_apd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("PDA")
      .Pages(1).HelpContextID = 159780362
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDSERIAL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='CENCOST'
    this.cWorkTables[9]='VOC_COST'
    this.cWorkTables[10]='ATTIVITA'
    this.cWorkTables[11]='PAR_PROD'
    this.cWorkTables[12]='CON_TRAM'
    this.cWorkTables[13]='PDA_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(13))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PDA_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PDA_DETT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PDSERIAL = NVL(PDSERIAL,space(10))
      .w_PDROWNUM = NVL(PDROWNUM,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_47_joined
    link_1_47_joined=.f.
    local link_1_50_joined
    link_1_50_joined=.f.
    local link_1_55_joined
    link_1_55_joined=.f.
    local link_1_58_joined
    link_1_58_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PDA_DETT where PDSERIAL=KeySet.PDSERIAL
    *                            and PDROWNUM=KeySet.PDROWNUM
    *
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PDA_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PDA_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PDA_DETT '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_47_joined=this.AddJoinedLink_1_47(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_50_joined=this.AddJoinedLink_1_50(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_55_joined=this.AddJoinedLink_1_55(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_58_joined=this.AddJoinedLink_1_58(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  ,'PDROWNUM',this.w_PDROWNUM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_OLDSTATUS = space(1)
        .w_UNMIS3 = space(3)
        .w_OPERA3 = space(1)
        .w_COCONA = space(15)
        .w_DESART = space(40)
        .w_UNMIS1 = space(3)
        .w_UNMIS2 = space(3)
        .w_OPERAT = space(1)
        .w_MOLTIP = 0
        .w_TIPGES = space(1)
        .w_FLSERG = space(1)
        .w_GIOAPP = 0
        .w_QTAMIN = 0
        .w_LOTRIO = 0
        .w_CODCON = space(15)
        .w_DESFOR = space(40)
        .w_DESMAG = space(30)
        .w_DESCON = space(40)
        .w_TIPVOC = space(1)
        .w_DTOBVO = ctod("  /  /  ")
        .w_VOCDES = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_MOLTI3 = 0
        .w_ARTOBSO = ctod("  /  /  ")
        .w_MODUM2 = space(1)
        .w_FLFRAZ = space(1)
        .w_FLUSEP = space(1)
        .w_FLFRAZ1 = space(1)
        .w_PPCALSTA = space(5)
        .w_CT = space(1)
        .w_CC = space(15)
        .w_CM = space(3)
        .w_CI = ctod("  /  /  ")
        .w_CF = ctod("  /  /  ")
        .w_CV = space(3)
        .w_IVACON = space(1)
        .w_FCODVAL = space(3)
        .w_CATCOM = space(3)
        .w_FLSCOR = space(1)
        .w_OBTEST = i_DATSYS
        .w_PPCODICE = 'AA'
          .link_1_1('Load')
        .w_PDSERIAL = NVL(PDSERIAL,space(10))
        .op_PDSERIAL = .w_PDSERIAL
        .w_PDROWNUM = NVL(PDROWNUM,0)
        .w_PDSTATUS = NVL(PDSTATUS,space(1))
        .w_PDCODICE = NVL(PDCODICE,space(20))
          if link_1_9_joined
            this.w_PDCODICE = NVL(CACODICE109,NVL(this.w_PDCODICE,space(20)))
            this.w_PDCODART = NVL(CACODART109,space(20))
            this.w_COCONA = NVL(CACODCON109,space(15))
            this.w_UNMIS3 = NVL(CAUNIMIS109,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP109,0)
            this.w_OPERA3 = NVL(CAOPERAT109,space(1))
          else
          .link_1_9('Load')
          endif
        .w_PDCODART = NVL(PDCODART,space(20))
          if link_1_14_joined
            this.w_PDCODART = NVL(ARCODART114,NVL(this.w_PDCODART,space(20)))
            this.w_DESART = NVL(ARDESART114,space(40))
            this.w_PDUNIMIS = NVL(ARUNMIS1114,space(3))
            this.w_UNMIS1 = NVL(ARUNMIS1114,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2114,space(3))
            this.w_OPERAT = NVL(AROPERAT114,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP114,0)
            this.w_PDVOCCOS = NVL(ARVOCCEN114,space(15))
            this.w_TIPGES = NVL(ARTIPGES114,space(1))
            this.w_FLSERG = NVL(ARFLSERG114,space(1))
            this.w_ARTOBSO = NVL(cp_ToDate(ARDTOBSO114),ctod("  /  /  "))
            this.w_FLUSEP = NVL(ARFLUSEP114,space(1))
          else
          .link_1_14('Load')
          endif
        .w_CODART = .w_PDCODART
          .link_1_22('Load')
        .w_PDDATEVA = NVL(cp_ToDate(PDDATEVA),ctod("  /  /  "))
        .w_PDDATORD = NVL(cp_ToDate(PDDATORD),ctod("  /  /  "))
        .w_PDTIPCON = NVL(PDTIPCON,space(1))
        .w_PDCODCON = NVL(PDCODCON,space(15))
          if link_1_34_joined
            this.w_PDCODCON = NVL(ANCODICE134,NVL(this.w_PDCODCON,space(15)))
            this.w_DESFOR = NVL(ANDESCRI134,space(40))
            this.w_CATCOM = NVL(ANCATCOM134,space(3))
            this.w_FLSCOR = NVL(ANSCORPO134,space(1))
            this.w_FCODVAL = NVL(ANCODVAL134,space(3))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO134),ctod("  /  /  "))
          else
          .link_1_34('Load')
          endif
        .w_PDCONTRA = NVL(PDCONTRA,space(15))
          if link_1_36_joined
            this.w_PDCONTRA = NVL(CONUMERO136,NVL(this.w_PDCONTRA,space(15)))
            this.w_CT = NVL(COTIPCLF136,space(1))
            this.w_CC = NVL(COCODCLF136,space(15))
            this.w_CM = NVL(COCATCOM136,space(3))
            this.w_CI = NVL(cp_ToDate(CODATINI136),ctod("  /  /  "))
            this.w_CF = NVL(cp_ToDate(CODATFIN136),ctod("  /  /  "))
            this.w_CV = NVL(COCODVAL136,space(3))
            this.w_IVACON = NVL(COIVALIS136,space(1))
          else
          .link_1_36('Load')
          endif
        .w_PDCRIFOR = NVL(PDCRIFOR,space(1))
        .w_PDUMORDI = NVL(PDUMORDI,space(3))
          if link_1_38_joined
            this.w_PDUMORDI = NVL(UMCODICE138,NVL(this.w_PDUMORDI,space(3)))
            this.w_FLFRAZ1 = NVL(UMFLFRAZ138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_PDQTAORD = NVL(PDQTAORD,0)
        .w_PDUNIMIS = NVL(PDUNIMIS,space(3))
          if link_1_41_joined
            this.w_PDUNIMIS = NVL(UMCODICE141,NVL(this.w_PDUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ141,space(1))
            this.w_MODUM2 = NVL(UMMODUM2141,space(1))
          else
          .link_1_41('Load')
          endif
        .w_PDQTAUM1 = NVL(PDQTAUM1,0)
        .w_PDSERRIF = NVL(PDSERRIF,space(10))
        .w_PDCODMAG = NVL(PDCODMAG,space(5))
          if link_1_47_joined
            this.w_PDCODMAG = NVL(MGCODMAG147,NVL(this.w_PDCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG147,space(30))
          else
          .link_1_47('Load')
          endif
        .w_PDCODCEN = NVL(PDCODCEN,space(15))
          if link_1_50_joined
            this.w_PDCODCEN = NVL(CC_CONTO150,NVL(this.w_PDCODCEN,space(15)))
            this.w_DESCON = NVL(CCDESPIA150,space(40))
          else
          .link_1_50('Load')
          endif
        .w_PDVOCCOS = NVL(PDVOCCOS,space(15))
          if link_1_55_joined
            this.w_PDVOCCOS = NVL(VCCODICE155,NVL(this.w_PDVOCCOS,space(15)))
            this.w_VOCDES = NVL(VCDESCRI155,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC155,space(1))
            this.w_DTOBVO = NVL(cp_ToDate(VCDTOBSO155),ctod("  /  /  "))
          else
          .link_1_55('Load')
          endif
        .w_PDCODCOM = NVL(PDCODCOM,space(15))
          if link_1_58_joined
            this.w_PDCODCOM = NVL(CNCODCAN158,NVL(this.w_PDCODCOM,space(15)))
            this.w_DTOBSO = NVL(cp_ToDate(CNDTOBSO158),ctod("  /  /  "))
          else
          .link_1_58('Load')
          endif
        .w_PDTIPATT = NVL(PDTIPATT,space(1))
        .w_PDCODATT = NVL(PDCODATT,space(15))
          * evitabile
          *.link_1_62('Load')
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .w_PDPERASS = NVL(PDPERASS,space(3))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate()
        .w_CODVAL = iif(empty(NVL(.w_FCODVAL,'')),g_PERVAL,.w_FCODVAL)
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .w_PDCRIELA = NVL(PDCRIELA,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PDA_DETT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE = space(2)
      .w_OBTEST = ctod("  /  /  ")
      .w_PDSERIAL = space(10)
      .w_PDROWNUM = 0
      .w_PDSTATUS = space(1)
      .w_OLDSTATUS = space(1)
      .w_PDCODICE = space(20)
      .w_UNMIS3 = space(3)
      .w_OPERA3 = space(1)
      .w_COCONA = space(15)
      .w_PDCODART = space(20)
      .w_DESART = space(40)
      .w_UNMIS1 = space(3)
      .w_UNMIS2 = space(3)
      .w_OPERAT = space(1)
      .w_MOLTIP = 0
      .w_TIPGES = space(1)
      .w_FLSERG = space(1)
      .w_CODART = space(20)
      .w_GIOAPP = 0
      .w_QTAMIN = 0
      .w_LOTRIO = 0
      .w_CODCON = space(15)
      .w_PDDATEVA = ctod("  /  /  ")
      .w_PDDATORD = ctod("  /  /  ")
      .w_PDTIPCON = space(1)
      .w_PDCODCON = space(15)
      .w_DESFOR = space(40)
      .w_PDCONTRA = space(15)
      .w_PDCRIFOR = space(1)
      .w_PDUMORDI = space(3)
      .w_PDQTAORD = 0
      .w_PDUNIMIS = space(3)
      .w_PDQTAUM1 = 0
      .w_PDSERRIF = space(10)
      .w_PDCODMAG = space(5)
      .w_DESMAG = space(30)
      .w_PDCODCEN = space(15)
      .w_DESCON = space(40)
      .w_TIPVOC = space(1)
      .w_DTOBVO = ctod("  /  /  ")
      .w_PDVOCCOS = space(15)
      .w_VOCDES = space(40)
      .w_PDCODCOM = space(15)
      .w_DTOBSO = ctod("  /  /  ")
      .w_PDTIPATT = space(1)
      .w_PDCODATT = space(15)
      .w_PDPERASS = space(3)
      .w_MOLTI3 = 0
      .w_ARTOBSO = ctod("  /  /  ")
      .w_MODUM2 = space(1)
      .w_FLFRAZ = space(1)
      .w_FLUSEP = space(1)
      .w_FLFRAZ1 = space(1)
      .w_PPCALSTA = space(5)
      .w_CT = space(1)
      .w_CC = space(15)
      .w_CM = space(3)
      .w_CI = ctod("  /  /  ")
      .w_CF = ctod("  /  /  ")
      .w_CV = space(3)
      .w_IVACON = space(1)
      .w_FCODVAL = space(3)
      .w_CODVAL = space(3)
      .w_CATCOM = space(3)
      .w_FLSCOR = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_PDCRIELA = space(1)
      if .cFunction<>"Filter"
        .w_PPCODICE = 'AA'
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PPCODICE))
          .link_1_1('Full')
          endif
        .w_OBTEST = i_DATSYS
          .DoRTCalc(3,3,.f.)
        .w_PDROWNUM = 1
        .w_PDSTATUS = 'D'
        .DoRTCalc(6,7,.f.)
          if not(empty(.w_PDCODICE))
          .link_1_9('Full')
          endif
        .DoRTCalc(8,11,.f.)
          if not(empty(.w_PDCODART))
          .link_1_14('Full')
          endif
          .DoRTCalc(12,18,.f.)
        .w_CODART = .w_PDCODART
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_CODART))
          .link_1_22('Full')
          endif
          .DoRTCalc(20,25,.f.)
        .w_PDTIPCON = 'F'
        .w_PDCODCON = iif(empty(.w_COCONA), iif(empty(.w_CODCON), .w_PDCODCON, .w_CODCON), .w_COCONA)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_PDCODCON))
          .link_1_34('Full')
          endif
        .DoRTCalc(28,29,.f.)
          if not(empty(.w_PDCONTRA))
          .link_1_36('Full')
          endif
          .DoRTCalc(30,30,.f.)
        .w_PDUMORDI = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_PDUMORDI))
          .link_1_38('Full')
          endif
        .DoRTCalc(32,33,.f.)
          if not(empty(.w_PDUNIMIS))
          .link_1_41('Full')
          endif
        .DoRTCalc(34,36,.f.)
          if not(empty(.w_PDCODMAG))
          .link_1_47('Full')
          endif
        .DoRTCalc(37,38,.f.)
          if not(empty(.w_PDCODCEN))
          .link_1_50('Full')
          endif
        .DoRTCalc(39,42,.f.)
          if not(empty(.w_PDVOCCOS))
          .link_1_55('Full')
          endif
        .DoRTCalc(43,44,.f.)
          if not(empty(.w_PDCODCOM))
          .link_1_58('Full')
          endif
          .DoRTCalc(45,45,.f.)
        .w_PDTIPATT = 'A'
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_PDCODATT))
          .link_1_62('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate()
          .DoRTCalc(48,63,.f.)
        .w_CODVAL = iif(empty(NVL(.w_FCODVAL,'')),g_PERVAL,.w_FCODVAL)
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
          .DoRTCalc(65,66,.f.)
        .w_OBTEST = i_DATSYS
        .w_PDCRIELA = "N"
      endif
    endwith
    cp_BlankRecExtFlds(this,'PDA_DETT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEPDA","i_CODAZI,w_PDSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_PDSERIAL = .w_PDSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPDSERIAL_1_4.enabled = i_bVal
      .Page1.oPag.oPDROWNUM_1_5.enabled = i_bVal
      .Page1.oPag.oPDSTATUS_1_6.enabled = i_bVal
      .Page1.oPag.oPDCODICE_1_9.enabled = i_bVal
      .Page1.oPag.oPDDATEVA_1_28.enabled = i_bVal
      .Page1.oPag.oPDDATORD_1_30.enabled = i_bVal
      .Page1.oPag.oPDCODCON_1_34.enabled = i_bVal
      .Page1.oPag.oPDCONTRA_1_36.enabled = i_bVal
      .Page1.oPag.oPDUMORDI_1_38.enabled = i_bVal
      .Page1.oPag.oPDQTAORD_1_40.enabled = i_bVal
      .Page1.oPag.oPDCODMAG_1_47.enabled = i_bVal
      .Page1.oPag.oPDCODCEN_1_50.enabled = i_bVal
      .Page1.oPag.oPDVOCCOS_1_55.enabled = i_bVal
      .Page1.oPag.oPDCODCOM_1_58.enabled = i_bVal
      .Page1.oPag.oPDCODATT_1_62.enabled = i_bVal
      .Page1.oPag.oBtn_1_75.enabled = .Page1.oPag.oBtn_1_75.mCond()
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oObj_1_68.enabled = i_bVal
      .Page1.oPag.oObj_1_70.enabled = i_bVal
      .Page1.oPag.oObj_1_71.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
      .Page1.oPag.oObj_1_82.enabled = i_bVal
      .Page1.oPag.oObj_1_93.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPDSERIAL_1_4.enabled = .f.
        .Page1.oPag.oPDROWNUM_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPDSERIAL_1_4.enabled = .t.
        .Page1.oPag.oPDROWNUM_1_5.enabled = .t.
        .Page1.oPag.oPDCODICE_1_9.enabled = .t.
        .Page1.oPag.oPDDATEVA_1_28.enabled = .t.
        .Page1.oPag.oPDDATORD_1_30.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PDA_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDSERIAL,"PDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDROWNUM,"PDROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDSTATUS,"PDSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODICE,"PDCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODART,"PDCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDDATEVA,"PDDATEVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDDATORD,"PDDATORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDTIPCON,"PDTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODCON,"PDCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCONTRA,"PDCONTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCRIFOR,"PDCRIFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDUMORDI,"PDUMORDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDQTAORD,"PDQTAORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDUNIMIS,"PDUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDQTAUM1,"PDQTAUM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDSERRIF,"PDSERRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODMAG,"PDCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODCEN,"PDCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDVOCCOS,"PDVOCCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODCOM,"PDCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDTIPATT,"PDTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODATT,"PDCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDPERASS,"PDPERASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCRIELA,"PDCRIELA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    i_lTable = "PDA_DETT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PDA_DETT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PDA_DETT_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEPDA","i_CODAZI,w_PDSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PDA_DETT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PDA_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'PDA_DETT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PDSERIAL,PDROWNUM,PDSTATUS,PDCODICE,PDCODART"+;
                  ",PDDATEVA,PDDATORD,PDTIPCON,PDCODCON,PDCONTRA"+;
                  ",PDCRIFOR,PDUMORDI,PDQTAORD,PDUNIMIS,PDQTAUM1"+;
                  ",PDSERRIF,PDCODMAG,PDCODCEN,PDVOCCOS,PDCODCOM"+;
                  ",PDTIPATT,PDCODATT,PDPERASS,PDCRIELA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PDSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PDROWNUM)+;
                  ","+cp_ToStrODBC(this.w_PDSTATUS)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODART)+;
                  ","+cp_ToStrODBC(this.w_PDDATEVA)+;
                  ","+cp_ToStrODBC(this.w_PDDATORD)+;
                  ","+cp_ToStrODBC(this.w_PDTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_PDCONTRA)+;
                  ","+cp_ToStrODBC(this.w_PDCRIFOR)+;
                  ","+cp_ToStrODBCNull(this.w_PDUMORDI)+;
                  ","+cp_ToStrODBC(this.w_PDQTAORD)+;
                  ","+cp_ToStrODBCNull(this.w_PDUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_PDQTAUM1)+;
                  ","+cp_ToStrODBC(this.w_PDSERRIF)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_PDVOCCOS)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODCOM)+;
                  ","+cp_ToStrODBC(this.w_PDTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODATT)+;
                  ","+cp_ToStrODBC(this.w_PDPERASS)+;
                  ","+cp_ToStrODBC(this.w_PDCRIELA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PDA_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'PDA_DETT')
        cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_PDSERIAL,'PDROWNUM',this.w_PDROWNUM)
        INSERT INTO (i_cTable);
              (PDSERIAL,PDROWNUM,PDSTATUS,PDCODICE,PDCODART,PDDATEVA,PDDATORD,PDTIPCON,PDCODCON,PDCONTRA,PDCRIFOR,PDUMORDI,PDQTAORD,PDUNIMIS,PDQTAUM1,PDSERRIF,PDCODMAG,PDCODCEN,PDVOCCOS,PDCODCOM,PDTIPATT,PDCODATT,PDPERASS,PDCRIELA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PDSERIAL;
                  ,this.w_PDROWNUM;
                  ,this.w_PDSTATUS;
                  ,this.w_PDCODICE;
                  ,this.w_PDCODART;
                  ,this.w_PDDATEVA;
                  ,this.w_PDDATORD;
                  ,this.w_PDTIPCON;
                  ,this.w_PDCODCON;
                  ,this.w_PDCONTRA;
                  ,this.w_PDCRIFOR;
                  ,this.w_PDUMORDI;
                  ,this.w_PDQTAORD;
                  ,this.w_PDUNIMIS;
                  ,this.w_PDQTAUM1;
                  ,this.w_PDSERRIF;
                  ,this.w_PDCODMAG;
                  ,this.w_PDCODCEN;
                  ,this.w_PDVOCCOS;
                  ,this.w_PDCODCOM;
                  ,this.w_PDTIPATT;
                  ,this.w_PDCODATT;
                  ,this.w_PDPERASS;
                  ,this.w_PDCRIELA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PDA_DETT_IDX,i_nConn)
      *
      * update PDA_DETT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PDA_DETT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PDSTATUS="+cp_ToStrODBC(this.w_PDSTATUS)+;
             ",PDCODICE="+cp_ToStrODBCNull(this.w_PDCODICE)+;
             ",PDCODART="+cp_ToStrODBCNull(this.w_PDCODART)+;
             ",PDDATEVA="+cp_ToStrODBC(this.w_PDDATEVA)+;
             ",PDDATORD="+cp_ToStrODBC(this.w_PDDATORD)+;
             ",PDTIPCON="+cp_ToStrODBC(this.w_PDTIPCON)+;
             ",PDCODCON="+cp_ToStrODBCNull(this.w_PDCODCON)+;
             ",PDCONTRA="+cp_ToStrODBCNull(this.w_PDCONTRA)+;
             ",PDCRIFOR="+cp_ToStrODBC(this.w_PDCRIFOR)+;
             ",PDUMORDI="+cp_ToStrODBCNull(this.w_PDUMORDI)+;
             ",PDQTAORD="+cp_ToStrODBC(this.w_PDQTAORD)+;
             ",PDUNIMIS="+cp_ToStrODBCNull(this.w_PDUNIMIS)+;
             ",PDQTAUM1="+cp_ToStrODBC(this.w_PDQTAUM1)+;
             ",PDSERRIF="+cp_ToStrODBC(this.w_PDSERRIF)+;
             ",PDCODMAG="+cp_ToStrODBCNull(this.w_PDCODMAG)+;
             ",PDCODCEN="+cp_ToStrODBCNull(this.w_PDCODCEN)+;
             ",PDVOCCOS="+cp_ToStrODBCNull(this.w_PDVOCCOS)+;
             ",PDCODCOM="+cp_ToStrODBCNull(this.w_PDCODCOM)+;
             ",PDTIPATT="+cp_ToStrODBC(this.w_PDTIPATT)+;
             ",PDCODATT="+cp_ToStrODBCNull(this.w_PDCODATT)+;
             ",PDPERASS="+cp_ToStrODBC(this.w_PDPERASS)+;
             ",PDCRIELA="+cp_ToStrODBC(this.w_PDCRIELA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PDA_DETT')
        i_cWhere = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  ,'PDROWNUM',this.w_PDROWNUM  )
        UPDATE (i_cTable) SET;
              PDSTATUS=this.w_PDSTATUS;
             ,PDCODICE=this.w_PDCODICE;
             ,PDCODART=this.w_PDCODART;
             ,PDDATEVA=this.w_PDDATEVA;
             ,PDDATORD=this.w_PDDATORD;
             ,PDTIPCON=this.w_PDTIPCON;
             ,PDCODCON=this.w_PDCODCON;
             ,PDCONTRA=this.w_PDCONTRA;
             ,PDCRIFOR=this.w_PDCRIFOR;
             ,PDUMORDI=this.w_PDUMORDI;
             ,PDQTAORD=this.w_PDQTAORD;
             ,PDUNIMIS=this.w_PDUNIMIS;
             ,PDQTAUM1=this.w_PDQTAUM1;
             ,PDSERRIF=this.w_PDSERRIF;
             ,PDCODMAG=this.w_PDCODMAG;
             ,PDCODCEN=this.w_PDCODCEN;
             ,PDVOCCOS=this.w_PDVOCCOS;
             ,PDCODCOM=this.w_PDCODCOM;
             ,PDTIPATT=this.w_PDTIPATT;
             ,PDCODATT=this.w_PDCODATT;
             ,PDPERASS=this.w_PDPERASS;
             ,PDCRIELA=this.w_PDCRIELA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PDA_DETT_IDX,i_nConn)
      *
      * delete PDA_DETT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  ,'PDROWNUM',this.w_PDROWNUM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    if i_bUpd
      with this
            .w_PPCODICE = 'AA'
          .link_1_1('Full')
        .DoRTCalc(2,10,.t.)
        if .o_PDCODICE<>.w_PDCODICE
          .link_1_14('Full')
        endif
        .DoRTCalc(12,18,.t.)
            .w_CODART = .w_PDCODART
          .link_1_22('Full')
        .DoRTCalc(20,26,.t.)
        if .o_PDCODICE<>.w_PDCODICE
            .w_PDCODCON = iif(empty(.w_COCONA), iif(empty(.w_CODCON), .w_PDCODCON, .w_CODCON), .w_COCONA)
          .link_1_34('Full')
        endif
        .DoRTCalc(28,30,.t.)
        if .o_PDCODICE<>.w_PDCODICE.or. .o_PDCODCON<>.w_PDCODCON
            .w_PDUMORDI = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_1_38('Full')
        endif
        .DoRTCalc(32,32,.t.)
          .link_1_41('Full')
        .DoRTCalc(34,41,.t.)
        if .o_PDCODICE<>.w_PDCODICE
          .link_1_55('Full')
        endif
        .DoRTCalc(43,45,.t.)
            .w_PDTIPATT = 'A'
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate()
        .DoRTCalc(47,63,.t.)
        if .o_FCODVAL<>.w_FCODVAL
            .w_CODVAL = iif(empty(NVL(.w_FCODVAL,'')),g_PERVAL,.w_FCODVAL)
        endif
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEPDA","i_CODAZI,w_PDSERIAL")
          .op_PDSERIAL = .w_PDSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(65,68,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_82.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPDSTATUS_1_6.enabled = this.oPgFrm.Page1.oPag.oPDSTATUS_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.enabled = this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.mCond()
    this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.enabled = this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.mCond()
    this.oPgFrm.Page1.oPag.oPDCODCOM_1_58.enabled = this.oPgFrm.Page1.oPag.oPDCODCOM_1_58.mCond()
    this.oPgFrm.Page1.oPag.oPDCODATT_1_62.enabled = this.oPgFrm.Page1.oPag.oPDCODATT_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.visible=!this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_51.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.visible=!this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.mHide()
    this.oPgFrm.Page1.oPag.oVOCDES_1_56.visible=!this.oPgFrm.Page1.oPag.oVOCDES_1_56.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_75.visible=!this.oPgFrm.Page1.oPag.oBtn_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_82.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCALSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   +" and PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCALSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_PPCALSTA = NVL(_Link_.PPCALSTA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_PPCALSTA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PDCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PDCODICE))
          select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_PDCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_PDCODICE)+"%");

            select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPDCODICE_1_9'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PDCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PDCODICE)
            select CACODICE,CACODART,CACODCON,CAUNIMIS,CAMOLTIP,CAOPERAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_PDCODART = NVL(_Link_.CACODART,space(20))
      this.w_COCONA = NVL(_Link_.CACODCON,space(15))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODICE = space(20)
      endif
      this.w_PDCODART = space(20)
      this.w_COCONA = space(15)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_OPERA3 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.CACODICE as CACODICE109"+ ",link_1_9.CACODART as CACODART109"+ ",link_1_9.CACODCON as CACODCON109"+ ",link_1_9.CAUNIMIS as CAUNIMIS109"+ ",link_1_9.CAMOLTIP as CAMOLTIP109"+ ",link_1_9.CAOPERAT as CAOPERAT109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on PDA_DETT.PDCODICE=link_1_9.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODICE=link_1_9.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCODART
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARVOCCEN,ARTIPGES,ARFLSERG,ARDTOBSO,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PDCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PDCODART)
            select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARVOCCEN,ARTIPGES,ARFLSERG,ARDTOBSO,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_PDUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_PDVOCCOS = NVL(_Link_.ARVOCCEN,space(15))
      this.w_TIPGES = NVL(_Link_.ARTIPGES,space(1))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_ARTOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_PDUNIMIS = space(3)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_PDVOCCOS = space(15)
      this.w_TIPGES = space(1)
      this.w_FLSERG = space(1)
      this.w_ARTOBSO = ctod("  /  /  ")
      this.w_FLUSEP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 12 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+12<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ARCODART as ARCODART114"+ ",link_1_14.ARDESART as ARDESART114"+ ",link_1_14.ARUNMIS1 as ARUNMIS1114"+ ",link_1_14.ARUNMIS1 as ARUNMIS1114"+ ",link_1_14.ARUNMIS2 as ARUNMIS2114"+ ",link_1_14.AROPERAT as AROPERAT114"+ ",link_1_14.ARMOLTIP as ARMOLTIP114"+ ",link_1_14.ARVOCCEN as ARVOCCEN114"+ ",link_1_14.ARTIPGES as ARTIPGES114"+ ",link_1_14.ARFLSERG as ARFLSERG114"+ ",link_1_14.ARDTOBSO as ARDTOBSO114"+ ",link_1_14.ARFLUSEP as ARFLUSEP114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on PDA_DETT.PDCODART=link_1_14.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODART=link_1_14.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRGIOAPP,PRQTAMIN,PRLOTRIO,PRCODFOR";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART)
            select PRCODART,PRGIOAPP,PRQTAMIN,PRLOTRIO,PRCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.PRCODART,space(20))
      this.w_GIOAPP = NVL(_Link_.PRGIOAPP,0)
      this.w_QTAMIN = NVL(_Link_.PRQTAMIN,0)
      this.w_LOTRIO = NVL(_Link_.PRLOTRIO,0)
      this.w_CODCON = NVL(_Link_.PRCODFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_GIOAPP = 0
      this.w_QTAMIN = 0
      this.w_LOTRIO = 0
      this.w_CODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODCON
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PDTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PDTIPCON;
                     ,'ANCODICE',trim(this.w_PDCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PDTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PDTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPDCODCON_1_34'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice fornitore � obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PDTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PDCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PDTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PDTIPCON;
                       ,'ANCODICE',this.w_PDCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATCOM,ANSCORPO,ANCODVAL,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_FCODVAL = NVL(_Link_.ANCODVAL,space(3))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_CATCOM = space(3)
      this.w_FLSCOR = space(1)
      this.w_FCODVAL = space(3)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice fornitore � obsoleto")
        endif
        this.w_PDCODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_CATCOM = space(3)
        this.w_FLSCOR = space(1)
        this.w_FCODVAL = space(3)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.ANCODICE as ANCODICE134"+ ",link_1_34.ANDESCRI as ANDESCRI134"+ ",link_1_34.ANCATCOM as ANCATCOM134"+ ",link_1_34.ANSCORPO as ANSCORPO134"+ ",link_1_34.ANCODVAL as ANCODVAL134"+ ",link_1_34.ANDTOBSO as ANDTOBSO134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on PDA_DETT.PDCODCON=link_1_34.ANCODICE"+" and PDA_DETT.PDTIPCON=link_1_34.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODCON=link_1_34.ANCODICE(+)"'+'+" and PDA_DETT.PDTIPCON=link_1_34.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCONTRA
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_PDCONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CONUMERO',trim(this.w_PDCONTRA))
          select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCONTRA)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','CONUMERO',cp_AbsName(oSource.parent,'oPDCONTRA_1_36'),i_cWhere,'',"",'GSAC_APD.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CONUMERO',oSource.xKey(1))
            select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_PDCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CONUMERO',this.w_PDCONTRA)
            select CONUMERO,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCONTRA = NVL(_Link_.CONUMERO,space(15))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCONTRA = space(15)
      endif
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_PDCONTRA, 'F', .w_PDCODCON, .w_CATCOM, .w_FLSCOR,.w_CODVAL,i_DATSYS,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCONTRA = space(15)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_TRAM_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.CONUMERO as CONUMERO136"+ ",link_1_36.COTIPCLF as COTIPCLF136"+ ",link_1_36.COCODCLF as COCODCLF136"+ ",link_1_36.COCATCOM as COCATCOM136"+ ",link_1_36.CODATINI as CODATINI136"+ ",link_1_36.CODATFIN as CODATFIN136"+ ",link_1_36.COCODVAL as COCODVAL136"+ ",link_1_36.COIVALIS as COIVALIS136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on PDA_DETT.PDCONTRA=link_1_36.CONUMERO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCONTRA=link_1_36.CONUMERO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDUMORDI
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDUMORDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PDUMORDI)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PDUMORDI))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDUMORDI)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDUMORDI) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPDUMORDI_1_38'),i_cWhere,'GSAR_AUM',"",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDUMORDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PDUMORDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PDUMORDI)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDUMORDI = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDUMORDI = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_PDUMORDI), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDUMORDI = space(3)
        this.w_FLFRAZ1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDUMORDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.UMCODICE as UMCODICE138"+ ",link_1_38.UMFLFRAZ as UMFLFRAZ138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on PDA_DETT.PDUMORDI=link_1_38.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and PDA_DETT.PDUMORDI=link_1_38.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDUNIMIS
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PDUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PDUNIMIS)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.UMCODICE as UMCODICE141"+ ",link_1_41.UMFLFRAZ as UMFLFRAZ141"+ ",link_1_41.UMMODUM2 as UMMODUM2141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on PDA_DETT.PDUNIMIS=link_1_41.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and PDA_DETT.PDUNIMIS=link_1_41.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCODMAG
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_PDCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_PDCODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDCODMAG_1_47'),i_cWhere,'GSAR_AMA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_47(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_47.MGCODMAG as MGCODMAG147"+ ",link_1_47.MGDESMAG as MGDESMAG147"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_47 on PDA_DETT.PDCODMAG=link_1_47.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_47"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODMAG=link_1_47.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCODCEN
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PDCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PDCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_PDCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_PDCODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPDCODCEN_1_50'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PDCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PDCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCON = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODCEN = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_50(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_50.CC_CONTO as CC_CONTO150"+ ",link_1_50.CCDESPIA as CCDESPIA150"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_50 on PDA_DETT.PDCODCEN=link_1_50.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_50"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODCEN=link_1_50.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDVOCCOS
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDVOCCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_PDVOCCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_PDVOCCOS))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDVOCCOS)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDVOCCOS) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oPDVOCCOS_1_55'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDVOCCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_PDVOCCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_PDVOCCOS)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDVOCCOS = NVL(_Link_.VCCODICE,space(15))
      this.w_VOCDES = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_DTOBVO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDVOCCOS = space(15)
      endif
      this.w_VOCDES = space(40)
      this.w_TIPVOC = space(1)
      this.w_DTOBVO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_PDVOCCOS = space(15)
        this.w_VOCDES = space(40)
        this.w_TIPVOC = space(1)
        this.w_DTOBVO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDVOCCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_55(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_55.VCCODICE as VCCODICE155"+ ",link_1_55.VCDESCRI as VCDESCRI155"+ ",link_1_55.VCTIPVOC as VCTIPVOC155"+ ",link_1_55.VCDTOBSO as VCDTOBSO155"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_55 on PDA_DETT.PDVOCCOS=link_1_55.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_55"
          i_cKey=i_cKey+'+" and PDA_DETT.PDVOCCOS=link_1_55.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCODCOM
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PDCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PDCODCOM))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPDCODCOM_1_58'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PDCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PDCODCOM)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODCOM = space(15)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_PDCODCOM = space(15)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_58(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_58.CNCODCAN as CNCODCAN158"+ ",link_1_58.CNDTOBSO as CNDTOBSO158"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_58 on PDA_DETT.PDCODCOM=link_1_58.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_58"
          i_cKey=i_cKey+'+" and PDA_DETT.PDCODCOM=link_1_58.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCODATT
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_PDCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_PDTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_PDCODCOM;
                     ,'ATTIPATT',this.w_PDTIPATT;
                     ,'ATCODATT',trim(this.w_PDCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oPDCODATT_1_62'),i_cWhere,'GSPC_BZZ',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDCODCOM<>oSource.xKey(1);
           .or. this.w_PDTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_PDTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_PDCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_PDTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_PDCODCOM;
                       ,'ATTIPATT',this.w_PDTIPATT;
                       ,'ATCODATT',this.w_PDCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODATT = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODATT = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_PDCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_PDCODCOM))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        endif
        this.w_PDCODATT = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPDSERIAL_1_4.value==this.w_PDSERIAL)
      this.oPgFrm.Page1.oPag.oPDSERIAL_1_4.value=this.w_PDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPDROWNUM_1_5.value==this.w_PDROWNUM)
      this.oPgFrm.Page1.oPag.oPDROWNUM_1_5.value=this.w_PDROWNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oPDSTATUS_1_6.RadioValue()==this.w_PDSTATUS)
      this.oPgFrm.Page1.oPag.oPDSTATUS_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODICE_1_9.value==this.w_PDCODICE)
      this.oPgFrm.Page1.oPag.oPDCODICE_1_9.value=this.w_PDCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODART_1_14.value==this.w_PDCODART)
      this.oPgFrm.Page1.oPag.oPDCODART_1_14.value=this.w_PDCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_15.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_15.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oPDDATEVA_1_28.value==this.w_PDDATEVA)
      this.oPgFrm.Page1.oPag.oPDDATEVA_1_28.value=this.w_PDDATEVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPDDATORD_1_30.value==this.w_PDDATORD)
      this.oPgFrm.Page1.oPag.oPDDATORD_1_30.value=this.w_PDDATORD
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODCON_1_34.value==this.w_PDCODCON)
      this.oPgFrm.Page1.oPag.oPDCODCON_1_34.value=this.w_PDCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_35.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_35.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCONTRA_1_36.value==this.w_PDCONTRA)
      this.oPgFrm.Page1.oPag.oPDCONTRA_1_36.value=this.w_PDCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCRIFOR_1_37.RadioValue()==this.w_PDCRIFOR)
      this.oPgFrm.Page1.oPag.oPDCRIFOR_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDUMORDI_1_38.value==this.w_PDUMORDI)
      this.oPgFrm.Page1.oPag.oPDUMORDI_1_38.value=this.w_PDUMORDI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDQTAORD_1_40.value==this.w_PDQTAORD)
      this.oPgFrm.Page1.oPag.oPDQTAORD_1_40.value=this.w_PDQTAORD
    endif
    if not(this.oPgFrm.Page1.oPag.oPDUNIMIS_1_41.value==this.w_PDUNIMIS)
      this.oPgFrm.Page1.oPag.oPDUNIMIS_1_41.value=this.w_PDUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPDQTAUM1_1_44.value==this.w_PDQTAUM1)
      this.oPgFrm.Page1.oPag.oPDQTAUM1_1_44.value=this.w_PDQTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODMAG_1_47.value==this.w_PDCODMAG)
      this.oPgFrm.Page1.oPag.oPDCODMAG_1_47.value=this.w_PDCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_48.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_48.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.value==this.w_PDCODCEN)
      this.oPgFrm.Page1.oPag.oPDCODCEN_1_50.value=this.w_PDCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_51.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_51.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.value==this.w_PDVOCCOS)
      this.oPgFrm.Page1.oPag.oPDVOCCOS_1_55.value=this.w_PDVOCCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCDES_1_56.value==this.w_VOCDES)
      this.oPgFrm.Page1.oPag.oVOCDES_1_56.value=this.w_VOCDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODCOM_1_58.value==this.w_PDCODCOM)
      this.oPgFrm.Page1.oPag.oPDCODCOM_1_58.value=this.w_PDCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODATT_1_62.value==this.w_PDCODATT)
      this.oPgFrm.Page1.oPag.oPDCODATT_1_62.value=this.w_PDCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCRIELA_1_97.RadioValue()==this.w_PDCRIELA)
      this.oPgFrm.Page1.oPag.oPDCRIELA_1_97.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PDA_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PDSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDSERIAL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_PDSERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDROWNUM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDROWNUM_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PDROWNUM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDSTATUS))  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDSTATUS_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PDSTATUS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODICE_1_9.SetFocus()
            i_bnoObbl = !empty(.w_PDCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDDATEVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDDATEVA_1_28.SetFocus()
            i_bnoObbl = !empty(.w_PDDATEVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PDDATORD)) or not(.w_PDDATEVA>=.w_PDDATORD or empty(.w_PDDATEVA)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDDATORD_1_30.SetFocus()
            i_bnoObbl = !empty(.w_PDDATORD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_PDCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODCON_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice fornitore � obsoleto")
          case   not(CHKCONTR(.w_PDCONTRA, 'F', .w_PDCODCON, .w_CATCOM, .w_FLSCOR,.w_CODVAL,i_DATSYS,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and not(empty(.w_PDCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCONTRA_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PDUMORDI)) or not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_PDUMORDI), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDUMORDI_1_38.SetFocus()
            i_bnoObbl = !empty(.w_PDUMORDI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PDQTAORD)) or not(.w_PDQTAORD>0 and (.w_FLFRAZ1<>'S' OR .w_PDQTAORD=INT(.w_PDQTAORD))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDQTAORD_1_40.SetFocus()
            i_bnoObbl = !empty(.w_PDQTAORD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODMAG_1_47.SetFocus()
            i_bnoObbl = !empty(.w_PDCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST))  and not(g_PERCCR<>'S')  and (g_PERCCR='S')  and not(empty(.w_PDVOCCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDVOCCOS_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and (g_COMM='S' or g_PERCAN='S')  and not(empty(.w_PDCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODCOM_1_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(NOT EMPTY(.w_PDCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_PDCODCOM)))  and ((g_COMM='S' or g_PERCAN='S') AND NOT EMPTY(.w_PDCODCOM))  and not(empty(.w_PDCODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODATT_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� inesistente o incongruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsac_apd
          * Controlla Data di Obsolescenza Articolo
          if i_bRes
            if not (this.w_ARTOBSO > this.w_PDDATEVA or empty(this.w_ARTOBSO))
            if not ah_YesNo("Attenzione%0Articolo obsoleto dal %1%0Si desidera continuare?",'', DTOC(this.w_ARTOBSO))
                 i_bRes = .f.
              endif
           endif
          endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDCODICE = this.w_PDCODICE
    this.o_PDCODCON = this.w_PDCODCON
    this.o_FCODVAL = this.w_FCODVAL
    return

enddefine

* --- Define pages as container
define class tgsac_apdPag1 as StdContainer
  Width  = 556
  height = 369
  stdWidth  = 556
  stdheight = 369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPDSERIAL_1_4 as StdField with uid="NTYLPQCSTY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PDSERIAL", cQueryName = "PDSERIAL",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice seriale",;
    HelpContextID = 81793602,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=114, Top=12, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oPDROWNUM_1_5 as StdField with uid="JWGRYQEPOU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PDROWNUM", cQueryName = "PDSERIAL,PDROWNUM",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice progressivo",;
    HelpContextID = 171573827,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=218, Top=12, cSayPict='"99999999"', cGetPict='"99999999"'


  add object oPDSTATUS_1_6 as StdCombo with uid="CEHGWXTDFT",rtseq=5,rtrep=.f.,left=448,top=12,width=101,height=21;
    , ToolTipText = "Stato dell'ordine";
    , HelpContextID = 249500233;
    , cFormVar="w_PDSTATUS",RowSource=""+"Suggerita,"+"Confermata,"+"Ordinata", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPDSTATUS_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oPDSTATUS_1_6.GetRadio()
    this.Parent.oContained.w_PDSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oPDSTATUS_1_6.SetRadio()
    this.Parent.oContained.w_PDSTATUS=trim(this.Parent.oContained.w_PDSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_PDSTATUS=='S',1,;
      iif(this.Parent.oContained.w_PDSTATUS=='D',2,;
      iif(this.Parent.oContained.w_PDSTATUS=='O',3,;
      0)))
  endfunc

  func oPDSTATUS_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oPDCODICE_1_9 as StdField with uid="LVDVPAUJCQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDCODICE", cQueryName = "PDCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca dell'articolo",;
    HelpContextID = 67703355,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=114, Top=40, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_PDCODICE"

  func oPDCODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPDCODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'',this.parent.oContained
  endproc
  proc oPDCODICE_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PDCODICE
     i_obj.ecpSave()
  endproc

  add object oPDCODART_1_14 as StdField with uid="BXLZMTNZKZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PDCODART", cQueryName = "PDCODART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 201921098,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=114, Top=68, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PDCODART"

  func oPDCODART_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESART_1_15 as StdField with uid="CCMUTPHBIE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 266080822,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=279, Top=68, InputMask=replicate('X',40)

  add object oPDDATEVA_1_28 as StdField with uid="LIPBXMMXBO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PDDATEVA", cQueryName = "PDDATEVA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di prevista evasione",;
    HelpContextID = 16458295,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=114, Top=95

  add object oPDDATORD_1_30 as StdField with uid="JSVRKDDLCG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PDDATORD", cQueryName = "PDDATORD",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di emissione ordine per rispettare la data di evasione",;
    HelpContextID = 184230458,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=317, Top=94

  func oPDDATORD_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PDDATEVA>=.w_PDDATORD or empty(.w_PDDATEVA))
    endwith
    return bRes
  endfunc

  add object oPDCODCON_1_34 as StdField with uid="HEQHLDRUJC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PDCODCON", cQueryName = "PDCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice fornitore � obsoleto",;
    ToolTipText = "Codice del fornitore",;
    HelpContextID = 32959932,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=114, Top=124, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PDTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PDCODCON"

  func oPDCODCON_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODCON_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODCON_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PDTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PDTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPDCODCON_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oPDCODCON_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PDTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PDCODCON
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_35 as StdField with uid="YXONLYNVRC",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229708342,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=248, Top=124, InputMask=replicate('X',40)

  add object oPDCONTRA_1_36 as StdField with uid="IAWYEOGXLI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PDCONTRA", cQueryName = "PDCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contratto",;
    HelpContextID = 262738487,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=114, Top=155, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", oKey_1_1="CONUMERO", oKey_1_2="this.w_PDCONTRA"

  func oPDCONTRA_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCONTRA_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCONTRA_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAM','*','CONUMERO',cp_AbsName(this.parent,'oPDCONTRA_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSAC_APD.CON_TRAM_VZM',this.parent.oContained
  endproc


  add object oPDCRIFOR_1_37 as StdCombo with uid="PYJHNMNUYE",rtseq=30,rtrep=.f.,left=400,top=151,width=114,height=21, enabled=.f.;
    , ToolTipText = "Criterio di scelta del fornitore";
    , HelpContextID = 245624248;
    , cFormVar="w_PDCRIFOR",RowSource=""+"Proporzione,"+"Tempo,"+"Prezzo,"+"Affidabilit�,"+"Priorit�,"+"Ripartizione,"+"Manuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDCRIFOR_1_37.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'A',;
    iif(this.value =5,'I',;
    iif(this.value =6,'R',;
    iif(this.value =7,'M',;
    space(1)))))))))
  endfunc
  func oPDCRIFOR_1_37.GetRadio()
    this.Parent.oContained.w_PDCRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oPDCRIFOR_1_37.SetRadio()
    this.Parent.oContained.w_PDCRIFOR=trim(this.Parent.oContained.w_PDCRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_PDCRIFOR=='P',1,;
      iif(this.Parent.oContained.w_PDCRIFOR=='T',2,;
      iif(this.Parent.oContained.w_PDCRIFOR=='Z',3,;
      iif(this.Parent.oContained.w_PDCRIFOR=='A',4,;
      iif(this.Parent.oContained.w_PDCRIFOR=='I',5,;
      iif(this.Parent.oContained.w_PDCRIFOR=='R',6,;
      iif(this.Parent.oContained.w_PDCRIFOR=='M',7,;
      0)))))))
  endfunc

  add object oPDUMORDI_1_38 as StdField with uid="CKCMXUGJKA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PDUMORDI", cQueryName = "PDUMORDI",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 230175295,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=114, Top=204, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PDUMORDI"

  func oPDUMORDI_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDUMORDI_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDUMORDI_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPDUMORDI_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oPDUMORDI_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PDUMORDI
     i_obj.ecpSave()
  endproc

  add object oPDQTAORD_1_40 as StdField with uid="MWUYIXGIVX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PDQTAORD", cQueryName = "PDQTAORD",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 165605946,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=294, Top=204, cSayPict="v_pq(12)", cGetPict="v_gq(12)"

  func oPDQTAORD_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PDQTAORD>0 and (.w_FLFRAZ1<>'S' OR .w_PDQTAORD=INT(.w_PDQTAORD)))
    endwith
    return bRes
  endfunc

  add object oPDUNIMIS_1_41 as StdField with uid="PUARZBGHER",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PDUNIMIS", cQueryName = "PDUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 128372151,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=114, Top=228, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_PDUNIMIS"

  func oPDUNIMIS_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPDQTAUM1_1_44 as StdField with uid="JDAVEKTOFO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PDQTAUM1", cQueryName = "PDQTAUM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 2166233,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=294, Top=228, cSayPict="v_pq(12)", cGetPict="v_gq(12)"

  add object oPDCODMAG_1_47 as StdField with uid="FDIXSEERFS",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PDCODMAG", cQueryName = "PDCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 134812221,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDCODMAG"

  func oPDCODMAG_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODMAG_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODMAG_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDCODMAG_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"",'',this.parent.oContained
  endproc
  proc oPDCODMAG_1_47.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PDCODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_48 as StdField with uid="PYHMNILDIA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 30937654,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=259, InputMask=replicate('X',30)

  add object oPDCODCEN_1_50 as StdField with uid="SMAGWUUUCR",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PDCODCEN", cQueryName = "PDCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 235475524,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=114, Top=290, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PDCODCEN"

  func oPDCODCEN_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oPDCODCEN_1_50.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oPDCODCEN_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODCEN_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODCEN_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPDCODCEN_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oPDCODCEN_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_PDCODCEN
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_51 as StdField with uid="ETZQFDWCRD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162402870,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=248, Top=290, InputMask=replicate('X',40)

  func oDESCON_1_51.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oPDVOCCOS_1_55 as StdField with uid="ZAQEESVTFE",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PDVOCCOS", cQueryName = "PDVOCCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    ToolTipText = "Voce di costo analitica abbinata all'articolo",;
    HelpContextID = 33930679,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=114, Top=312, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_PDVOCCOS"

  func oPDVOCCOS_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oPDVOCCOS_1_55.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oPDVOCCOS_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDVOCCOS_1_55.ecpDrop(oSource)
    this.Parent.oContained.link_1_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDVOCCOS_1_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oPDVOCCOS_1_55'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oPDVOCCOS_1_55.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_PDVOCCOS
     i_obj.ecpSave()
  endproc

  add object oVOCDES_1_56 as StdField with uid="FNEYLPUUBZ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_VOCDES", cQueryName = "VOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235806038,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=248, Top=312, InputMask=replicate('X',40)

  func oVOCDES_1_56.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oPDCODCOM_1_58 as StdField with uid="EOKQCWHWCJ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_PDCODCOM", cQueryName = "PDCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice della commessa associata al centro di costo/ricavo",;
    HelpContextID = 32959933,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=114, Top=340, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PDCODCOM"

  func oPDCODCOM_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' or g_PERCAN='S')
    endwith
   endif
  endfunc

  func oPDCODCOM_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
      if .not. empty(.w_PDCODATT)
        bRes2=.link_1_62('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPDCODCOM_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODCOM_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPDCODCOM_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oPDCODCOM_1_58.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_PDCODCOM
     i_obj.ecpSave()
  endproc

  add object oPDCODATT_1_62 as StdField with uid="VVYUUEIBCE",rtseq=47,rtrep=.f.,;
    cFormVar = "w_PDCODATT", cQueryName = "PDCODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� inesistente o incongruente",;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 201921098,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=316, Top=340, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_PDCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_PDTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_PDCODATT"

  func oPDCODATT_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_COMM='S' or g_PERCAN='S') AND NOT EMPTY(.w_PDCODCOM))
    endwith
   endif
  endfunc

  func oPDCODATT_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODATT_1_62.ecpDrop(oSource)
    this.Parent.oContained.link_1_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODATT_1_62.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_PDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_PDTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_PDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_PDTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oPDCODATT_1_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"",'',this.parent.oContained
  endproc
  proc oPDCODATT_1_62.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_PDCODCOM
    i_obj.ATTIPATT=w_PDTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_PDCODATT
     i_obj.ecpSave()
  endproc


  add object oObj_1_64 as cp_runprogram with uid="SXADQUQUXP",left=573, top=186, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("upd")',;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_65 as cp_runprogram with uid="PKIHYIYKDW",left=573, top=252, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("codcon")',;
    cEvent = "w_PDCODCON Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_66 as cp_runprogram with uid="YYSGMWKQZG",left=573, top=208, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("load")',;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_67 as cp_runprogram with uid="GQNHCYPVAH",left=573, top=230, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("status")',;
    cEvent = "w_PDSTATUS Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_68 as cp_runprogram with uid="KUWCOLIQWH",left=573, top=164, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("ins")',;
    cEvent = "Insert start",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_70 as cp_runprogram with uid="CCDXZDQEGZ",left=573, top=274, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAC_BA9('DATAFINVAR')",;
    cEvent = "w_PDDATEVA Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_71 as cp_runprogram with uid="MYOEQXWOME",left=573, top=296, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("qtaord")',;
    cEvent = "w_PDQTAORD Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oObj_1_72 as cp_runprogram with uid="SPNZEMDHFZ",left=573, top=318, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("umordi")',;
    cEvent = "w_PDUMORDI Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oBtn_1_75 as StdButton with uid="FVUYPYDBTY",left=504, top=204, width=48,height=45,;
    CpPicture="bmp\doc1.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento di ordine a fornitore generato";
    , HelpContextID = 46220186;
    , tabstop=.f., Caption='\<Documento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        GSAC_BA9(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERRIF) AND .w_PDSTATUS='O' AND .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_75.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PDSERRIF) OR .w_PDSTATUS<>'O')
     endwith
    endif
  endfunc


  add object oObj_1_82 as cp_runprogram with uid="HPRWOYJRIB",left=573, top=351, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAC_BA9('DATAINIVAR')",;
    cEvent = "w_PDDATORD Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo il periodo (oltperas) e data inizio (oltdinric)";
    , HelpContextID = 17932262


  add object oObj_1_93 as cp_runprogram with uid="XVBGKDUMPA",left=573, top=373, width=178,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BA9("contra")',;
    cEvent = "w_PDCONTRA Changed",;
    nPag=1;
    , HelpContextID = 17932262


  add object oPDCRIELA_1_97 as StdCombo with uid="ZQLAEHXYJR",rtseq=68,rtrep=.f.,left=400,top=177,width=114,height=22, enabled=.f.;
    , HelpContextID = 262401481;
    , cFormVar="w_PDCRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDCRIELA_1_97.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oPDCRIELA_1_97.GetRadio()
    this.Parent.oContained.w_PDCRIELA = this.RadioValue()
    return .t.
  endfunc

  func oPDCRIELA_1_97.SetRadio()
    this.Parent.oContained.w_PDCRIELA=trim(this.Parent.oContained.w_PDCRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_PDCRIELA=='A',1,;
      iif(this.Parent.oContained.w_PDCRIELA=='M',2,;
      iif(this.Parent.oContained.w_PDCRIELA=='G',3,;
      iif(this.Parent.oContained.w_PDCRIELA=='N',4,;
      0))))
  endfunc

  add object oStr_1_3 as StdString with uid="MAPAUAYZOL",Visible=.t., Left=4, Top=13,;
    Alignment=1, Width=107, Height=15,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="OPCDFOGGAI",Visible=.t., Left=4, Top=40,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice di ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VBOWPYAMML",Visible=.t., Left=4, Top=68,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="NWJOYVFIWN",Visible=.t., Left=4, Top=95,;
    Alignment=1, Width=107, Height=15,;
    Caption="Data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="XUFDFYPUYM",Visible=.t., Left=205, Top=94,;
    Alignment=1, Width=109, Height=15,;
    Caption="Data ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="SMVVJYVGXM",Visible=.t., Left=4, Top=259,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="MSYWDCCGZO",Visible=.t., Left=4, Top=124,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="EAJARNMXZB",Visible=.t., Left=4, Top=204,;
    Alignment=1, Width=107, Height=15,;
    Caption="U.M. x ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ZVIPHZMQBK",Visible=.t., Left=4, Top=228,;
    Alignment=1, Width=107, Height=15,;
    Caption="U.M. principale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VOLEIWPECJ",Visible=.t., Left=181, Top=204,;
    Alignment=1, Width=110, Height=15,;
    Caption="Qta da ordinare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="IWBFNLQUFR",Visible=.t., Left=182, Top=228,;
    Alignment=1, Width=109, Height=15,;
    Caption="Qta 1^U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="OHIYQZEKQI",Visible=.t., Left=4, Top=290,;
    Alignment=1, Width=107, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="PVEMQUYUNU",Visible=.t., Left=4, Top=312,;
    Alignment=1, Width=107, Height=15,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="MNYUTFZFML",Visible=.t., Left=4, Top=340,;
    Alignment=1, Width=107, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="CTCXTYMFHC",Visible=.t., Left=244, Top=340,;
    Alignment=1, Width=69, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="AVBCEWSUHY",Visible=.t., Left=360, Top=13,;
    Alignment=1, Width=81, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="FWZMUUCUCU",Visible=.t., Left=4, Top=155,;
    Alignment=1, Width=107, Height=15,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="FVVTPIIKXP",Visible=.t., Left=284, Top=155,;
    Alignment=1, Width=111, Height=15,;
    Caption="Criterio di scelta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="HEFZGUMJXL",Visible=.t., Left=263, Top=179,;
    Alignment=1, Width=132, Height=18,;
    Caption="Criterio elaborazione"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_apd','PDA_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PDSERIAL=PDA_DETT.PDSERIAL";
  +" and "+i_cAliasName2+".PDROWNUM=PDA_DETT.PDROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
