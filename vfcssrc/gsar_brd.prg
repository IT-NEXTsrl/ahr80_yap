* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brd                                                        *
*              Ricalcolo totali documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_72]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-11                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Documento,pNOBFA,pNORAT,pNODET,pNOCUR,pOKRIT,pRICSPE,pRICPRO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brd",oParentObject,m.Documento,m.pNOBFA,m.pNORAT,m.pNODET,m.pNOCUR,m.pOKRIT,m.pRICSPE,m.pRICPRO)
return(i_retval)

define class tgsar_brd as StdBatch
  * --- Local variables
  w_GSVE_MDV = .NULL.
  Documento = space(10)
  pNOBFA = .f.
  pNORAT = .f.
  pNODET = .f.
  pNOCUR = .f.
  pOKRIT = .f.
  pRICSPE = .f.
  pRICPRO = .f.
  DR = space(10)
  w_MVCAUIMB = 0
  w_MVIVACAU = space(5)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODDES = space(5)
  w_GENPRO = space(1)
  w_FLSPIN = space(1)
  w_TDRIPCON = space(1)
  w_MVNUMDOC = 0
  w_MVPRD = space(2)
  w_MVFLINTE = space(1)
  w_MVCODESE = space(4)
  w_MVALFDOC = space(10)
  w_MVFLVEAC = space(1)
  w_MVTCAMAG = space(5)
  w_MVFLACCO = space(1)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTIPCON = space(1)
  w_MVTFRAGG = space(1)
  w_MVNUMREG = 0
  w_MVCODUTE = 0
  w_MVTIPDOC = space(5)
  w_MVCAUCON = space(5)
  w_DATDIV = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVCLADOC = space(2)
  w_MVSERIAL = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_MVCONTRO = space(15)
  w_MVSPEINC = 0
  w_CPROWORD = 0
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MVIVAINC = space(5)
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVPERPRO = 0
  w_MVSPEIMB = 0
  w_MVCODICE = space(41)
  w_MVQTAMOV = 0
  w_MVIMPPRO = 0
  w_MVIVAIMB = space(5)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVFLCASC = space(1)
  w_MVIVABOL = space(5)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVFLORDI = space(1)
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVFLIMPE = space(1)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVFLRISE = space(1)
  w_MVVALNAZ = space(3)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVFLELGM = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVMOLSUP = 0
  w_MVVALRIG = 0
  w_MVTCOCEN = space(15)
  w_MVPESNET = 0
  w_MVNUMCOL = 0
  w_MVIMPACC = 0
  w_MVTCOMME = space(15)
  w_MVFLTRAS = space(1)
  w_MVIMPSCO = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVSCONTI = 0
  w_MVVALMAG = 0
  w_MVACIVA1 = space(5)
  w_MVUMSUPP = space(3)
  w_MVIMPARR = 0
  w_MVIMPNAZ = 0
  w_MVACIVA2 = space(5)
  w_MVAIMPN1 = 0
  w_MVAIMPS1 = 0
  w_MVAFLOM1 = space(1)
  w_MVACIVA3 = space(5)
  w_MVAIMPN2 = 0
  w_MVAIMPS2 = 0
  w_MVAFLOM2 = space(1)
  w_MVACIVA4 = space(5)
  w_MVAIMPN3 = 0
  w_MVAIMPS3 = 0
  w_MVAFLOM3 = space(1)
  w_MVACIVA5 = space(5)
  w_MVAIMPN4 = 0
  w_MVAIMPS4 = 0
  w_MVAFLOM4 = space(1)
  w_MVACIVA6 = space(5)
  w_MVAIMPN5 = 0
  w_MVAIMPS5 = 0
  w_MVAFLOM5 = space(1)
  w_APPART = space(10)
  w_MVAIMPN6 = 0
  w_MVAIMPS6 = 0
  w_MVAFLOM6 = space(1)
  w_SPEINC = 0
  w_DECTOT = 0
  w_PERIVA = 0
  w_PERIVE = 0
  w_BOLIVE = space(1)
  w_PEIINC = 0
  w_SPEIMB = 0
  w_BOLESE = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_SPETRA = 0
  w_BOLSUP = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_SPEBOL = 0
  w_BOLCAM = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_ACCONT = 0
  w_BOLARR = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_IVAINC = space(5)
  w_BOLMIN = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_IVAIMB = space(5)
  w_TOTMERCE = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_IVATRA = space(5)
  w_TOTALE = 0
  w_CLBOLFAT = space(1)
  w_IVABOL = space(5)
  w_TOTIMPON = 0
  w_MVFLSCOR = space(1)
  w_ACQINT = space(1)
  w_CAOVAL = 0
  w_TOTIMPOS = 0
  w_MVTOTRIT = 0
  w_IMPARR = 0
  w_TOTFATTU = 0
  w_MVTOTENA = 0
  w_FLFOBO = space(1)
  w_RSNUMRAT = 0
  w_CODNAZ = space(3)
  w_MVACCPRE = 0
  w_FLINTR = space(1)
  w_RSDATRAT = ctod("  /  /  ")
  w_MVTDTEVA = ctod("  /  /  ")
  w_TIPDOC = space(2)
  w_RSIMPRAT = 0
  w_MVRIFDIC = space(10)
  w_MVRITPRE = 0
  w_TIPREG = space(1)
  w_RSMODPAG = space(10)
  w_MVFLSALD = space(1)
  w_MVCODCOM = space(15)
  w_MVIMPCOM = 0
  w_MVIMPAC2 = 0
  w_MVCODORN = space(15)
  w_XCONORN = space(15)
  w_RSFLPROV = space(1)
  w_MVFLFOSC = space(1)
  w_MVVALULT = 0
  w_AZSCOPIE = space(1)
  w_AZDTSCPI = ctod("  /  /  ")
  w_SPEACC = 0
  w_PREC = 0
  w_TIMPACC = 0
  w_PRES = 0
  w_TIMPSCO = 0
  w_CODIVA = space(3)
  w_DATCOM = ctod("  /  /  ")
  w_CAONAZ = 0
  w_SPEACC_IVA = 0
  w_ANMCALSI = space(5)
  w_ANMCALST = space(5)
  w_OLSPETRA = 0
  w_OLSPEIMB = 0
  w_Da_Ritenute = space(1)
  w_FLSPIM = space(1)
  w_FLSPTR = space(1)
  w_CHKSPEIMB = .f.
  w_CHKSPETRA = .f.
  w_impon_cpa = 0
  w_impon_gen = 0
  w_FLGMSPE = space(1)
  w_MCALSI = space(5)
  w_MCALST = space(5)
  w_MVQTACOL = 0
  w_MVQTALOR = 0
  w_MCALSI4 = space(5)
  w_MCALST4 = space(5)
  w_MCSIVT1 = space(5)
  w_MCSTVT1 = space(5)
  w_MCSIVT2 = space(5)
  w_MCSTVT2 = space(5)
  w_MCSIVT3 = space(5)
  w_MCSTVT3 = space(5)
  w_MVCODSPE = space(40)
  w_OLDSPEINC = 0
  w_OLDSPETRA = 0
  w_OLDSPEBOL = 0
  w_OLDSPEIMB = 0
  w_MSGSPE = space(100)
  w_VTMCCODI = space(5)
  w_VTMCCODT = space(5)
  w_VT2CCODI = space(5)
  w_VT2CCODT = space(5)
  w_VT3CCODI = space(5)
  w_VT3CCODT = space(5)
  w_DDMCCODI = space(5)
  w_DDMCCODT = space(5)
  w_SPMCCODI = space(5)
  w_SPMCCODT = space(5)
  w_SOLORATE = space(1)
  w_OBJDOC = .NULL.
  w_TOTDOC = 0
  w_MINRATA = 0
  w_DRPERRIT = 0
  w_DRCODTRI = space(5)
  w_VCODTRI = space(5)
  w_DRCODCAU = space(1)
  w_DRCODBUN = space(3)
  w_FLACON = space(1)
  w_DRTOTDOC = 0
  w_DRIMPOSTA = 0
  w_DRNONSOG = 0
  w_DRFODPRO = 0
  w_DRSOGGE = 0
  w_DRSOMESC = 0
  w_DRTIPFOR = space(1)
  w_DRPERPRO = 0
  w_IMPINPS = 0
  w_RITINPS = 0
  w_PERINPS = 0
  w_DRIMPONI = 0
  w_DRRITENU = 0
  w_DRDATREG = ctod("  /  /  ")
  w_DRDAVERS = 0
  w_DECTOP = 0
  w_DRESIDUO = 0
  w_DRTOTRIT = 0
  w_DRNONSO1 = 0
  w_MODIFICO = space(1)
  w_DRESIDU1 = 0
  w_DRNSOGGP = 0
  w_DRNSOGGI = 0
  w_TOTENA = 0
  w_DRRIINPS = 0
  w_DRDATCON = space(1)
  w_CODIRP = space(5)
  w_CODTR2 = space(5)
  w_CAURIT = space(1)
  w_TOTDOC = 0
  w_TOTIVA = 0
  w_IVAIND = 0
  w_AGEIMPO = 0
  w_AGERITE = 0
  w_AZIENDA = space(5)
  w_APPOINPS = 0
  w_TOTIMP = 0
  w_TOTINP = 0
  w_MESS = space(100)
  w_NONSOG = 0
  w_NONSOG1 = 0
  w_NONSOGGCA = 0
  w_TOTMRIT = 0
  w_TOTMPRE = 0
  w_TIPRITE = space(1)
  w_PERCEN = 0
  w_PADRE = .NULL.
  w_TIPRIT = space(1)
  w_TOTRIP4 = 0
  w_TIPORITE = space(1)
  w_TIPO = space(1)
  w_GESRIT = space(1)
  w_ROWRIT = 0
  w_PERIND = 0
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_PPLISRIF = space(5)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_CODAZI = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  DOC_RATE_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  VOCIIVA_idx=0
  KEY_ARTI_idx=0
  TIP_DOCU_idx=0
  TRI_BUTI_idx=0
  AGENTI_idx=0
  VDATRITE_idx=0
  CAU_CONT_idx=0
  AZIENDA_idx=0
  PAR_PROV_idx=0
  VETTORI_idx=0
  DES_DIVE_idx=0
  MODASPED_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RICALCOLA TOTALE DOCUMENTO CON LA RIPARTIZIONE DEGLI SCONTI E SPESE ACCESSORIE
    * --- Riempie il cursore GeneApp con le righe del documento e calcola i totali realizzandola ripartizione delle spese accessorie e degli sconti.
    * --- --
    * --- Lanciato da GSIM_BMM, GSIM_BAC, GSAR_BEA, GSVE_BFD, GSCO_BDP,  GSMD_BVM, GSVA_BPS
    * --- Parametri
    *     ===========
    *     Documento = seriale documento da elaborare
    * --- Se true non esegue BFA
    * --- Se true non esegue aggiornamento rate
    * --- Se true non esegue aggiornamento dettaglio
    * --- se true non chiude il cursore GENEAPP
    * --- Inizializza il vettore delle Rate Scadenze
    * --- se true esegue creazione ritenute
    * --- Se passato ricalcolo CPa e spese generali se previsto da causale
    * --- Se passato ricalcolo le provvigioni
    DIMENSION DR[1000, 9]
    FOR i = 1 TO 1000
    DR[i, 1] = cp_CharToDate("  -  -  ")
    DR[i, 2] = 0
    DR[i, 3] = "  "
    DR[i, 9] = " "
    ENDFOR
    * --- Inizializza le variabili utilizzate nei calcoli
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVCAUIMB = 0
    this.w_MVIVACAU = SPACE(5)
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVFLSCOR = " "
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_TIPDOC = "  "
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_TIPREG = " "
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVCODCON = SPACE(15)
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVFLVEAC = " "
    this.w_MVCAUCON = SPACE(5)
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTIPCON = " "
    this.w_MVTOTRIT = 0
    this.w_MVACCPRE = 0
    this.w_ACQINT = "N"
    this.w_MVTOTENA = 0
    this.w_FLFOBO = " "
    this.w_MVRITPRE = 0
    this.w_MVFLSALD = " "
    * --- Legge i dati di testata del documento
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.Documento;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
      this.w_MVIVAINC = NVL(cp_ToDate(_read_.MVIVAINC),cp_NullValue(_read_.MVIVAINC))
      this.w_MVFLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
      this.w_MVSPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
      this.w_MVIVAIMB = NVL(cp_ToDate(_read_.MVIVAIMB),cp_NullValue(_read_.MVIVAIMB))
      this.w_MVFLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
      this.w_MVSPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
      this.w_MVIVATRA = NVL(cp_ToDate(_read_.MVIVATRA),cp_NullValue(_read_.MVIVATRA))
      this.w_MVFLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
      this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
      this.w_MVIVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
      this.w_MVACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
      this.w_MVIMPARR = NVL(cp_ToDate(_read_.MVIMPARR),cp_NullValue(_read_.MVIMPARR))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_MVCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
      this.w_MVCODAGE = NVL(cp_ToDate(_read_.MVCODAGE),cp_NullValue(_read_.MVCODAGE))
      this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
      this.w_MVCODBAN = NVL(cp_ToDate(_read_.MVCODBAN),cp_NullValue(_read_.MVCODBAN))
      this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
      this.w_MVSCOCL1 = NVL(cp_ToDate(_read_.MVSCOCL1),cp_NullValue(_read_.MVSCOCL1))
      this.w_MVSCOCL2 = NVL(cp_ToDate(_read_.MVSCOCL2),cp_NullValue(_read_.MVSCOCL2))
      this.w_MVSCOPAG = NVL(cp_ToDate(_read_.MVSCOPAG),cp_NullValue(_read_.MVSCOPAG))
      this.w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
      this.w_MVTCONTR = NVL(cp_ToDate(_read_.MVTCONTR),cp_NullValue(_read_.MVTCONTR))
      this.w_MVTCOLIS = NVL(cp_ToDate(_read_.MVTCOLIS),cp_NullValue(_read_.MVTCOLIS))
      this.w_MVCAOVAL = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
      this.w_MVDATDIV = NVL(cp_ToDate(_read_.MVDATDIV),cp_NullValue(_read_.MVDATDIV))
      this.w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
      this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
      this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_MVCAUCON = NVL(cp_ToDate(_read_.MVCAUCON),cp_NullValue(_read_.MVCAUCON))
      this.w_MVTOTRIT = NVL(cp_ToDate(_read_.MVTOTRIT),cp_NullValue(_read_.MVTOTRIT))
      this.w_MVTOTENA = NVL(cp_ToDate(_read_.MVTOTENA),cp_NullValue(_read_.MVTOTENA))
      this.w_MVACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVFLSALD = NVL(cp_ToDate(_read_.MVFLSALD),cp_NullValue(_read_.MVFLSALD))
      this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_MVCODORN = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
      this.w_MVFLFOSC = NVL(cp_ToDate(_read_.MVFLFOSC),cp_NullValue(_read_.MVFLFOSC))
      this.w_MVSCONTI = NVL(cp_ToDate(_read_.MVSCONTI),cp_NullValue(_read_.MVSCONTI))
      this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_MVCAUIMB = NVL(cp_ToDate(_read_.MVCAUIMB),cp_NullValue(_read_.MVCAUIMB))
      this.w_MVIVACAU = NVL(cp_ToDate(_read_.MVIVACAU),cp_NullValue(_read_.MVIVACAU))
      this.w_MVQTALOR = NVL(cp_ToDate(_read_.MVQTALOR),cp_NullValue(_read_.MVQTALOR))
      this.w_MVQTACOL = NVL(cp_ToDate(_read_.MVQTACOL),cp_NullValue(_read_.MVQTACOL))
      this.w_MVCODVET = NVL(cp_ToDate(_read_.MVCODVET),cp_NullValue(_read_.MVCODVET))
      this.w_MVCODVE2 = NVL(cp_ToDate(_read_.MVCODVE2),cp_NullValue(_read_.MVCODVE2))
      this.w_MVCODVE3 = NVL(cp_ToDate(_read_.MVCODVE3),cp_NullValue(_read_.MVCODVE3))
      this.w_MVCODDES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
      this.w_MVCODSPE = NVL(cp_ToDate(_read_.MVCODSPE),cp_NullValue(_read_.MVCODSPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDRIPCON,TDFLSPIM,TDFLSPTR,TDMCALSI,TDMCALST,TDFLPROV,TDFLSPIN"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDRIPCON,TDFLSPIM,TDFLSPTR,TDMCALSI,TDMCALST,TDFLPROV,TDFLSPIN;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TDRIPCON = NVL(cp_ToDate(_read_.TDRIPCON),cp_NullValue(_read_.TDRIPCON))
      this.w_FLSPIM = NVL(cp_ToDate(_read_.TDFLSPIM),cp_NullValue(_read_.TDFLSPIM))
      this.w_FLSPTR = NVL(cp_ToDate(_read_.TDFLSPTR),cp_NullValue(_read_.TDFLSPTR))
      this.w_MCALSI = NVL(cp_ToDate(_read_.TDMCALSI),cp_NullValue(_read_.TDMCALSI))
      this.w_MCALST = NVL(cp_ToDate(_read_.TDMCALST),cp_NullValue(_read_.TDMCALST))
      this.w_GENPRO = NVL(cp_ToDate(_read_.TDFLPROV),cp_NullValue(_read_.TDFLPROV))
      this.w_FLSPIN = NVL(cp_ToDate(_read_.TDFLSPIN),cp_NullValue(_read_.TDFLSPIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
    * --- Crea il cursore delle righe del documento e lo rende editabile
    vq_exec("QUERY\GSAR_BRD",this,"GeneApp")
    wrcursor("GeneApp")
    * --- Calcola le valorizzazioni del documento e le scadenze
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pRICPRO
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Chiusura Cursore
    if Not this.pNOCUR
      USE IN SELECT("GeneApp")
      USE IN SELECT("tmp_bst")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili Locali
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo totali documento
    * --- Calcola totale documento e totale merce
    * --- Legge i Dati Associati alla Valuta
    this.w_DECTOT = 0
    this.w_BOLESE = 0
    this.w_BOLSUP = 0
    this.w_BOLCAM = 0
    this.w_BOLARR = 0
    this.w_BOLMIN = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM;
        from (i_cTable) where;
            VACODVAL = this.w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
      this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
      this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
      this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
      this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSCOPIE,AZDTSCPI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CodAzi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSCOPIE,AZDTSCPI;
        from (i_cTable) where;
            AZCODAZI = i_CodAzi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZSCOPIE = NVL(cp_ToDate(_read_.AZSCOPIE),cp_NullValue(_read_.AZSCOPIE))
      this.w_AZDTSCPI = NVL(cp_ToDate(_read_.AZDTSCPI),cp_NullValue(_read_.AZDTSCPI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    select GeneApp
    go top
    scan
    if EMPTY(NVL(t_MVTIPRIG," "))
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CA__TIPO"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(t_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CA__TIPO;
          from (i_cTable) where;
              CACODICE = t_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      REPLACE t_MVTIPRIG WITH this.w_MVTIPRIG
    endif
    if NVL(t_MVVALRIG,0)=0
      this.w_MVVALRIG = CAVALRIG(Nvl(t_MVPREZZO,0),Nvl(t_MVQTAMOV,0), Nvl(t_MVSCONT1,0),Nvl(t_MVSCONT2,0),Nvl(t_MVSCONT3,0),Nvl(t_MVSCONT4,0),this.w_DECTOT)
      REPLACE t_MVVALRIG WITH this.w_MVVALRIG
    endif
    this.w_TOTRIP4 = this.w_TOTRIP4 + IIF(GeneApp.t_MVFLOMAG="X" AND GeneApp.t_PRESTA="A", GeneApp.t_MVVALRIG, 0)
    this.w_TOTALE = this.w_TOTALE + GeneApp.t_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF( GeneApp.t_MVFLOMAG="X" AND ALLTRIM(NVL(t_FLSERA, "N"))<>"S", IIF(GeneApp.t_MVVALRIG>0 OR (this.w_AZSCOPIE="S" AND this.w_MVDATDOC>=this.w_AZDTSCPI) , GeneApp.t_MVVALRIG, 0), 0)
    endscan
    * --- Aggiorna le Spese Accessorie e gli Sconti Finali
    this.w_MVSCONTI = IIF(this.w_MVFLFOSC="S",this.w_MVSCONTI,Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT))
    * --- Ripartisce Spese Accessorie e Sconti
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.w_MVDATREG, this.w_MVDATDOC)
    * --- Letture delle percentuali Iva delle Spese
    if NOT EMPTY(this.w_MVIVAINC)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLINC = " "
      this.w_PEIINC = 0
    endif
    if NOT EMPTY(this.w_MVIVAIMB)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLIMB = " "
      this.w_PEIIMB = 0
    endif
    if NOT EMPTY(this.w_MVIVATRA)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVATRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_PEITRA = 0
      this.w_BOLTRA = " "
    endif
    if NOT EMPTY(this.w_MVIVABOL)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVABOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVABOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_BOLBOL = " "
    endif
    * --- Totale Spese da Ripartire
    *     Nel caso di Scorporo piede fattura e codice Iva spese presente, calcolo in w_SPEACC_IVA 
    *     la somma delle spese scorporate della loro Iva.
    this.w_SPEACC_IVA = 0
    this.w_SPEACC = 0
    if Not Empty(this.w_MVIVAINC) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", Calnet( this.w_MVSPEINC, this.w_PEIINC, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
      endif
    else
      this.w_SPEACC = IIF(this.w_MVFLRINC="S", this.w_MVSPEINC, 0)
    endif
    if Not Empty(this.w_MVIVAIMB) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", Calnet( this.w_MVSPEIMB, this.w_PEIIMB, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
    endif
    if Not Empty(this.w_MVIVATRA) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", Calnet( this.w_MVSPETRA, this.w_PEITRA, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
    endif
    if this.w_MVFLVEAC="A"
      * --- Se Acquisti, Ripartisce Spese Bolli e Arrotondamenti
      this.w_SPEACC = this.w_SPEACC+(this.w_MVSPEBOL+this.w_MVIMPARR)
    endif
    * --- Totalizzatori
    this.w_TIMPACC = 0
    this.w_TIMPSCO = 0
    * --- Legge i valori dagli archivi collegati
    this.w_GIORN1 = 0
    this.w_GIORN2 = 0
    this.w_MESE1 = 0
    this.w_MESE2 = 0
    this.w_CLBOLFAT = " "
    this.w_GIOFIS = 0
    this.w_FLINTR = " "
    this.w_CODNAZ = g_CODNAZ
    this.w_OLSPETRA = this.w_MVSPETRA
    this.w_OLSPEIMB = this.w_MVSPEIMB
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,AFFLINTR,ANGIOFIS,ANNAZION,ANCODIRP,ANCAURIT,ANTIPCLF,ANCASPRO,ANPEINPS,ANRIINPS,ANCOINPS,ANRITENU,ANCODTR2,ANFLRITE,ANMCALSI,ANMCALST"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_XCONORN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANBOLFAT,AFFLINTR,ANGIOFIS,ANNAZION,ANCODIRP,ANCAURIT,ANTIPCLF,ANCASPRO,ANPEINPS,ANRIINPS,ANCOINPS,ANRITENU,ANCODTR2,ANFLRITE,ANMCALSI,ANMCALST;
        from (i_cTable) where;
            ANTIPCON = this.w_MVTIPCON;
            and ANCODICE = this.w_XCONORN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
      this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
      this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
      this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
      this.w_CLBOLFAT = NVL(cp_ToDate(_read_.ANBOLFAT),cp_NullValue(_read_.ANBOLFAT))
      this.w_FLINTR = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
      this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
      this.w_CODNAZ = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
      this.w_CODIRP = NVL(cp_ToDate(_read_.ANCODIRP),cp_NullValue(_read_.ANCODIRP))
      this.w_CAURIT = NVL(cp_ToDate(_read_.ANCAURIT),cp_NullValue(_read_.ANCAURIT))
      this.w_DRTIPFOR = NVL(cp_ToDate(_read_.ANTIPCLF),cp_NullValue(_read_.ANTIPCLF))
      this.w_DRPERPRO = NVL(cp_ToDate(_read_.ANCASPRO),cp_NullValue(_read_.ANCASPRO))
      this.w_IMPINPS = NVL(cp_ToDate(_read_.ANPEINPS),cp_NullValue(_read_.ANPEINPS))
      this.w_RITINPS = NVL(cp_ToDate(_read_.ANRIINPS),cp_NullValue(_read_.ANRIINPS))
      this.w_PERINPS = NVL(cp_ToDate(_read_.ANCOINPS),cp_NullValue(_read_.ANCOINPS))
      this.w_TIPRITE = NVL(cp_ToDate(_read_.ANRITENU),cp_NullValue(_read_.ANRITENU))
      this.w_CODTR2 = NVL(cp_ToDate(_read_.ANCODTR2),cp_NullValue(_read_.ANCODTR2))
      this.w_TIPRIT = NVL(cp_ToDate(_read_.ANFLRITE),cp_NullValue(_read_.ANFLRITE))
      this.w_ANMCALSI = NVL(cp_ToDate(_read_.ANMCALSI),cp_NullValue(_read_.ANMCALSI))
      this.w_ANMCALST = NVL(cp_ToDate(_read_.ANMCALST),cp_NullValue(_read_.ANMCALST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Documento di Acquisto INTRA
    this.w_ACQINT = IIF(this.w_MVFLVEAC="A" AND this.w_FLINTR="S", "S", "N")
    * --- Calcoli Finali
    if Not this.pNOBFA
      this.w_SOLORATE = "N"
      if this.pRICSPE
        this.w_CHKSPEIMB = .T.
        this.w_CHKSPETRA = .T.
        this.w_Da_Ritenute = "N"
        this.w_FLGMSPE = "N"
        if Not Empty(Nvl(this.w_MVCODDES," "))
          * --- Read from DES_DIVE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DDMCCODI,DDMCCODT"+;
              " from "+i_cTable+" DES_DIVE where ";
                  +"DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                  +" and DDTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DDMCCODI,DDMCCODT;
              from (i_cTable) where;
                  DDCODICE = this.w_MVCODCON;
                  and DDTIPCON = this.w_MVTIPCON;
                  and DDCODDES = this.w_MVCODDES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
            this.w_MCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(Nvl(this.w_MVCODVET," "))
          * --- Read from VETTORI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VETTORI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2],.t.,this.VETTORI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VTMCCODI,VTMCCODT"+;
              " from "+i_cTable+" VETTORI where ";
                  +"VTCODVET = "+cp_ToStrODBC(this.w_MVCODVET);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VTMCCODI,VTMCCODT;
              from (i_cTable) where;
                  VTCODVET = this.w_MVCODVET;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCSIVT1 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
            this.w_MCSTVT1 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(Nvl(this.w_MVCODVE2," "))
          * --- Read from VETTORI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VETTORI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2],.t.,this.VETTORI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VTMCCODI,VTMCCODT"+;
              " from "+i_cTable+" VETTORI where ";
                  +"VTCODVET = "+cp_ToStrODBC(this.w_MVCODVE2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VTMCCODI,VTMCCODT;
              from (i_cTable) where;
                  VTCODVET = this.w_MVCODVE2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCSIVT2 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
            this.w_MCSTVT2 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(Nvl(this.w_MVCODVE3," "))
          * --- Read from VETTORI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VETTORI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2],.t.,this.VETTORI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VTMCCODI,VTMCCODT"+;
              " from "+i_cTable+" VETTORI where ";
                  +"VTCODVET = "+cp_ToStrODBC(this.w_MVCODVE3);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VTMCCODI,VTMCCODT;
              from (i_cTable) where;
                  VTCODVET = this.w_MVCODVE3;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCSIVT3 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
            this.w_MCSTVT3 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(Nvl(this.w_MVCODSPE," "))
          * --- Read from MODASPED
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MODASPED_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MODASPED_idx,2],.t.,this.MODASPED_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SPMCCODI,SPMCCODT"+;
              " from "+i_cTable+" MODASPED where ";
                  +"SPCODSPE = "+cp_ToStrODBC(this.w_MVCODSPE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SPMCCODI,SPMCCODT;
              from (i_cTable) where;
                  SPCODSPE = this.w_MVCODSPE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SPMCCODI = NVL(cp_ToDate(_read_.SPMCCODI),cp_NullValue(_read_.SPMCCODI))
            this.w_SPMCCODT = NVL(cp_ToDate(_read_.SPMCCODT),cp_NullValue(_read_.SPMCCODT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_OLDSPEINC = Nvl(this.w_MVSPEINC,0)
        this.w_OLDSPEBOL = this.w_MVSPEBOL
        this.w_OLDSPETRA = this.w_MVSPETRA
        this.w_OLDSPEIMB = this.w_MVSPEIMB
        this.w_MCALSI = EVL(NVL(this.w_MCSIVT1, " "), EVL(NVL(this.w_MCSIVT2, " "), EVL(NVL(this.w_MCSIVT3, " "), EVL(NVL(this.w_MCALSI4, " "), EVL(NVL(this.w_SPMCCODI, " "), EVL(NVL(this.w_ANMCALSI, " "), this.w_MCALSI ) ) ) ) ) )
        this.w_MCALST = EVL(NVL(this.w_MCSTVT1, " "), EVL(NVL(this.w_MCSTVT2, " "), EVL(NVL(this.w_MCSTVT3, " "), EVL(NVL(this.w_MCALST4, " "), EVL(NVL(this.w_SPMCCODT, " "), EVL(NVL(this.w_ANMCALST, " "), this.w_MCALST ) ) ) ) ) )
        Select t_CPROWORD,t_MVNUMCOL,t_MVTIPRIG,t_MVCODICE,t_MVVALRIG,iif(t_MVFLOMAG="X",1,0) as t_MVFLOMAG,t_PRESTA,t_MVRIFORD,t_SPEGEN,t_FLGDSPE; 
 from GENEAPP into cursor TMP_BST
        GSAR_BST(this,.T.,"TMP_BST")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if !Isalt()
          * --- Se passate valorizzate le mantengo
          if this.w_OLDSPEINC=0
            this.w_MVSPEINC = CALSPEINC( this.w_MVFLVEAC, this.w_MVCODPAG, this.w_MVCODVAL, this.w_MVTIPCON, this.w_MVCODCON, this.w_FLSPIN )
          endif
          this.w_MVSPETRA = iif(Not Empty(Nvl(this.w_OLDSPETRA,0)),this.w_OLDSPETRA,this.w_MVSPETRA)
          this.w_MVSPEBOL = iif(Not Empty(Nvl(this.w_OLDSPEBOL,0)),this.w_OLDSPEBOL,this.w_MVSPEBOL)
          this.w_MVSPEIMB = iif(Not Empty(Nvl(this.w_OLDSPEIMB,0)),this.w_OLDSPEIMB,this.w_MVSPEIMB)
        endif
      else
        if type("this.opARENTOBJECT.w_PADRE")=="O" AND lower(this.opARENTOBJECT.w_PADRE.class)=="documwriter"
          this.w_CHKSPEIMB = .T.
          this.w_CHKSPETRA = .T.
          this.w_Da_Ritenute = "N"
          this.w_FLGMSPE = "N"
          if !EMPTY(NVL(this.oParentObject.w_PADRE.w_MVCODVET, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "VTCODVET"
            ArrWhere(1,2) = this.oParentObject.w_PADRE.w_MVCODVET
            this.w_VTMCCODI = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODI" , @ArrWhere )
            this.w_VTMCCODT = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODT" , @ArrWhere )
            Release ArrWhere
          endif
          if !EMPTY(NVL(this.oParentObject.w_PADRE.w_MVCODVE2, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "VTCODVET"
            ArrWhere(1,2) = this.oParentObject.w_PADRE.w_MVCODVE2
            this.w_VT2CCODI = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODI" , @ArrWhere )
            this.w_VT2CCODT = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODT" , @ArrWhere )
            Release ArrWhere
          endif
          if !EMPTY(NVL(this.oParentObject.w_PADRE.w_MVCODVE3, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "VTCODVET"
            ArrWhere(1,2) = this.oParentObject.w_PADRE.w_MVCODVE3
            this.w_VT3CCODI = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODI" , @ArrWhere )
            this.w_VT3CCODT = this.oParentObject.w_PADRE.ReadTable( "VETTORI" , "VTMCCODT" , @ArrWhere )
            Release ArrWhere
          endif
          if !EMPTY(NVL(this.oParentObject.w_PADRE.w_MVCODDES, " "))
            DIMENSION ArrWhere(3,2)
            ArrWhere(1,1) = "DDTIPCON"
            ArrWhere(1,2) = this.w_MVTIPCON
            ArrWhere(2,1) = "DDCODICE"
            ArrWhere(2,2) = this.w_XCONORN
            ArrWhere(3,1) = "DDCODDES"
            ArrWhere(3,2) = this.oParentObject.w_PADRE.w_MVCODDES
            this.w_DDMCCODI = this.oParentObject.w_PADRE.ReadTable( "DES_DIVE" , "DDMCCODI" , @ArrWhere )
            this.w_DDMCCODT = this.oParentObject.w_PADRE.ReadTable( "DES_DIVE" , "DDMCCODT" , @ArrWhere )
            Release ArrWhere
          endif
          if !EMPTY(NVL(this.oParentObject.w_PADRE.w_MVCODSPE, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "SPCODSPE"
            ArrWhere(1,2) = this.oParentObject.w_PADRE.w_MVCODSPE
            this.w_SPMCCODI = this.oParentObject.w_PADRE.ReadTable( "MODASPED" , "SPMCCODI" , @ArrWhere )
            this.w_SPMCCODT = this.oParentObject.w_PADRE.ReadTable( "MODASPED" , "SPMCCODI" , @ArrWhere )
            Release ArrWhere
          endif
          this.w_MCALSI = EVL(NVL(this.w_VTMCCODI, " "), EVL(NVL(this.w_VT2CCODI, " "), EVL(NVL(this.w_VT3CCODI, " "), EVL(NVL(this.w_DDMCCODI, " "), EVL(NVL(this.w_SPMCCODI, " "), EVL(NVL(this.w_ANMCALSI, " "), this.w_MCALSI ) ) ) ) ) )
          this.w_MCALST = EVL(NVL(this.w_VTMCCODT, " "), EVL(NVL(this.w_VT2CCODT, " "), EVL(NVL(this.w_VT3CCODT, " "), EVL(NVL(this.w_DDMCCODT, " "), EVL(NVL(this.w_SPMCCODT, " "), EVL(NVL(this.w_ANMCALST, " "), this.w_MCALST ) ) ) ) ) )
          Select t_CPROWORD,t_MVNUMCOL,t_MVTIPRIG,t_MVCODICE,t_MVVALRIG,iif(t_MVFLOMAG="X",1,0) as t_MVFLOMAG,t_PRESTA,t_MVRIFORD,t_SPEGEN,t_FLGDSPE; 
 from GENEAPP WHERE t_MVTIPRIG<>"D" into cursor TMP_BST
          GSAR_BST(this,.F.,"TMP_BST")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Lancio il Batch per la ripartizione delle spese e degli sconti
    this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRIMB="S", (this.w_MVSPEIMB - this.w_OLSPEIMB), 0 ) + IIF(this.w_MVFLRTRA="S", (this.w_MVSPETRA - this.w_OLSPETRA), 0 )
    GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, this.w_SPEACC, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_SPEACC_IVA, , this.w_TDRIPCON )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not this.pNOBFA
      GSAR_BFA(this,"D")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.pOKRIT
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SOLORATE = "S"
        GSAR_BFA(this,"D")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Aggiornamento testata documento
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
             )
    else
      update (i_cTable) set;
          MVCODPAG = this.w_MVCODPAG;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVDATDIV = this.w_MVDATDIV;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.Documento;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDV');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVTOTRIT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTOTRIT),'DOC_MAST','MVTOTRIT');
      +",MVRITPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRITPRE),'DOC_MAST','MVRITPRE');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
             )
    else
      update (i_cTable) set;
          MVSPEBOL = this.w_MVSPEBOL;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVACCONT = this.w_MVACCONT;
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVIMPARR = this.w_MVIMPARR;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVTOTRIT = this.w_MVTOTRIT;
          ,MVRITPRE = this.w_MVRITPRE;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.Documento;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if vartype(this.oParentObject.pPadre)=="O" AND lower(this.oParentObject.pPadre.class) == "documwriter"
      * --- Aggiorno variabili oggetto DocumWriter con i calcoli eseguiti
      *     (Utile per aggiornamento rischio ed eventuali interrogazioni
      *     da parte della gestione chiamante)
      this.w_OBJDOC = this.oParentObject.pPadre
      this.w_OBJDOC.w_MVCODPAG = this.w_MVCODPAG
      this.w_OBJDOC.w_MVCODIVE = this.w_MVCODIVE
      this.w_OBJDOC.w_MVSCONTI = this.w_MVSCONTI
      this.w_OBJDOC.w_MVSPEINC = this.w_MVSPEINC
      this.w_OBJDOC.w_MVIVAINC = this.w_MVIVAINC
      this.w_OBJDOC.w_MVFLRINC = this.w_MVFLRINC
      this.w_OBJDOC.w_MVSPETRA = this.w_MVSPETRA
      this.w_OBJDOC.w_MVIVATRA = this.w_MVIVATRA
      this.w_OBJDOC.w_MVFLRTRA = this.w_MVFLRTRA
      this.w_OBJDOC.w_MVSPEIMB = this.w_MVSPEIMB
      this.w_OBJDOC.w_MVIVAIMB = this.w_MVIVAIMB
      this.w_OBJDOC.w_MVFLRIMB = this.w_MVFLRIMB
      this.w_OBJDOC.w_MVSCOCL2 = this.w_MVSCOCL2
      this.w_OBJDOC.w_MVDATDIV = this.w_MVDATDIV
      this.w_OBJDOC.w_MVCODBAN = this.w_MVCODBAN
      this.w_OBJDOC.w_MVVALNAZ = this.w_MVVALNAZ
      this.w_OBJDOC.w_MVCODVAL = this.w_MVCODVAL
      this.w_OBJDOC.w_MVCAOVAL = this.w_MVCAOVAL
      this.w_OBJDOC.w_MVSCOCL1 = this.w_MVSCOCL1
      this.w_OBJDOC.w_MVSPEBOL = this.w_MVSPEBOL
      this.w_OBJDOC.w_MVIVABOL = this.w_MVIVABOL
      this.w_OBJDOC.w_MVACCONT = this.w_MVACCONT
      this.w_OBJDOC.w_MVACIVA1 = this.w_MVACIVA1
      this.w_OBJDOC.w_MVACIVA2 = this.w_MVACIVA2
      this.w_OBJDOC.w_MVACIVA3 = this.w_MVACIVA3
      this.w_OBJDOC.w_MVACIVA4 = this.w_MVACIVA4
      this.w_OBJDOC.w_MVACIVA5 = this.w_MVACIVA5
      this.w_OBJDOC.w_MVACIVA6 = this.w_MVACIVA6
      this.w_OBJDOC.w_MVAIMPN1 = this.w_MVAIMPN1
      this.w_OBJDOC.w_MVAIMPN2 = this.w_MVAIMPN2
      this.w_OBJDOC.w_MVAIMPN3 = this.w_MVAIMPN3
      this.w_OBJDOC.w_MVAIMPN4 = this.w_MVAIMPN4
      this.w_OBJDOC.w_MVAIMPN5 = this.w_MVAIMPN5
      this.w_OBJDOC.w_MVAIMPN6 = this.w_MVAIMPN6
      this.w_OBJDOC.w_MVAIMPS1 = this.w_MVAIMPS1
      this.w_OBJDOC.w_MVAIMPS2 = this.w_MVAIMPS2
      this.w_OBJDOC.w_MVAIMPS3 = this.w_MVAIMPS3
      this.w_OBJDOC.w_MVAIMPS4 = this.w_MVAIMPS4
      this.w_OBJDOC.w_MVAIMPS5 = this.w_MVAIMPS5
      this.w_OBJDOC.w_MVAIMPS6 = this.w_MVAIMPS6
      this.w_OBJDOC.w_MVAFLOM1 = this.w_MVAFLOM1
      this.w_OBJDOC.w_MVAFLOM2 = this.w_MVAFLOM2
      this.w_OBJDOC.w_MVAFLOM3 = this.w_MVAFLOM3
      this.w_OBJDOC.w_MVAFLOM4 = this.w_MVAFLOM4
      this.w_OBJDOC.w_MVAFLOM5 = this.w_MVAFLOM5
      this.w_OBJDOC.w_MVAFLOM6 = this.w_MVAFLOM6
      this.w_OBJDOC.w_MVIMPARR = this.w_MVIMPARR
      this.w_OBJDOC.w_MVACCPRE = this.w_MVACCPRE
      this.w_OBJDOC.w_MVTOTRIT = this.w_MVTOTRIT
      this.w_OBJDOC.w_MVRITPRE = this.w_MVRITPRE
      * --- Calcolo di TOTFATTU = w_TOTIMPON+w_TOTIMPOS+w_MVSPEBOL+w_MVIMPARR
      *     w_TOTIMPON = (w_IMP1+w_IMP2+w_IMP3+w_IMP4+w_IMP5+w_IMP6)-w_MVSPEBOL
      *     w_TOTIMPOS = w_IVA1+w_IVA2+w_IVA3+w_IVA4+w_IVA5+w_IVA6
      this.w_OBJDOC.w_TOTFATTU = IIF(this.w_MVAFLOM1="X", this.w_MVAIMPN1,0) + IIF(this.w_MVAFLOM2="X", this.w_MVAIMPN2,0) + IIF(this.w_MVAFLOM3="X", this.w_MVAIMPN3,0)
      this.w_OBJDOC.w_TOTFATTU = this.w_OBJDOC.w_TOTFATTU + IIF(this.w_MVAFLOM4="X", this.w_MVAIMPN4,0) + IIF(this.w_MVAFLOM5="X", this.w_MVAIMPN5,0) + IIF(this.w_MVAFLOM6="X", this.w_MVAIMPN6,0)
      this.w_OBJDOC.w_TOTFATTU = this.w_OBJDOC.w_TOTFATTU - this.w_MVSPEBOL
      this.w_OBJDOC.w_TOTFATTU = this.w_OBJDOC.w_TOTFATTU + IIF(this.w_MVAFLOM1 $ "XI", this.w_MVAIMPS1,0) + IIF(this.w_MVAFLOM2 $ "XI", this.w_MVAIMPS2,0) + IIF(this.w_MVAFLOM3 $ "XI", this.w_MVAIMPS3,0)
      this.w_OBJDOC.w_TOTFATTU = this.w_OBJDOC.w_TOTFATTU + IIF(this.w_MVAFLOM4 $ "XI", this.w_MVAIMPS4,0) + IIF(this.w_MVAFLOM5 $ "XI", this.w_MVAIMPS5,0) + IIF(this.w_MVAFLOM6 $ "XI", this.w_MVAIMPS6,0)
      this.w_OBJDOC.w_TOTFATTU = this.w_OBJDOC.w_TOTFATTU + this.w_MVSPEBOL + this.w_MVIMPARR
      this.w_OBJDOC.w_TOTIMPOS = this.w_TOTIMPOS
    endif
    * --- Cicla sulle Righe Documento
    if Not this.pNODET
      SELECT GeneApp
      GO TOP
      SCAN FOR t_MVTIPRIG <>" " AND NOT EMPTY(t_MVCODICE)
      * --- Scrive il Dettaglio
      this.w_MVSERIAL = t_MVSERIAL
      this.w_MVIMPSCO = t_MVIMPSCO
      this.w_CPROWNUM = t_CPROWNUM
      this.w_MVFLOMAG = t_MVFLOMAG
      this.w_MVNUMRIF = t_MVNUMRIF
      this.w_MVCODIVA = t_MVCODIVA
      this.w_MVTIPRIG = t_MVTIPRIG
      this.w_MVCODICE = t_MVCODICE
      this.w_MVPERPRO = t_MVPERPRO
      this.w_MVCODART = t_MVCODART
      this.w_MVIMPPRO = t_MVIMPPRO
      this.w_MVDESART = t_MVDESART
      this.w_MVSERRIF = t_MVSERRIF
      this.w_MVDESSUP = t_MVDESSUP
      this.w_MVROWRIF = t_MVROWRIF
      this.w_MVUNIMIS = t_MVUNIMIS
      this.w_MVPESNET = t_MVPESNET
      this.w_MVCATCON = t_MVCATCON
      this.w_MVFLTRAS = t_MVFLTRAS
      this.w_MVCONTRO = t_MVCONTRO
      this.w_MVNOMENC = t_MVNOMENC
      this.w_MVCODCLA = t_MVCODCLA
      this.w_MVUMSUPP = t_MVUMSUPP
      this.w_MVCONTRA = t_MVCONTRA
      this.w_MVMOLSUP = t_MVMOLSUP
      this.w_MVCODLIS = t_MVCODLIS
      this.w_MVNUMCOL = t_MVNUMCOL
      this.w_MVQTAMOV = t_MVQTAMOV
      this.w_MVQTAUM1 = t_MVQTAUM1
      this.w_MVCONIND = t_MVCONIND
      this.w_MVPREZZO = t_MVPREZZO
      this.w_MVVALMAG = t_MVVALMAG
      this.w_MVSCONT1 = t_MVSCONT1
      this.w_MVIMPNAZ = t_MVIMPNAZ
      this.w_MVSCONT2 = t_MVSCONT2
      this.w_MVIMPACC = t_MVIMPACC
      this.w_MVSCONT3 = t_MVSCONT3
      this.w_MVSCONT4 = t_MVSCONT4
      this.w_MVCODCOM = t_MVCODCOM
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_MVIMPAC2 = t_MVIMPAC2
      this.w_MVVALRIG = IIF(t_MVVALRIG<>0,t_MVVALRIG,CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT))
      this.w_MVVALULT = t_MVVALULT
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
        +",MVCODART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
        +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
        +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
        +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
        +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
        +",MVCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
        +",MVCONIND ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
        +",MVCONTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
        +",MVCONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRO),'DOC_DETT','MVCONTRO');
        +",MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
        +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
        +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
        +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
        +",MVIMPAC2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPAC2),'DOC_DETT','MVIMPAC2');
        +",MVIMPACC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
        +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
        +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
        +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
        +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
        +",MVMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
        +",MVNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
        +",MVNUMCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
        +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
        +",MVPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
        +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
        +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
        +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
        +",MVROWRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
        +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
        +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
        +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
        +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
        +",MVSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
        +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
        +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_MVUMSUPP),'DOC_DETT','MVUMSUPP');
        +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
        +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
        +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
        +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVCATCON = this.w_MVCATCON;
            ,MVCODART = this.w_MVCODART;
            ,MVCODCLA = this.w_MVCODCLA;
            ,MVCODCOM = this.w_MVCODCOM;
            ,MVCODICE = this.w_MVCODICE;
            ,MVCODIVA = this.w_MVCODIVA;
            ,MVCODLIS = this.w_MVCODLIS;
            ,MVCONIND = this.w_MVCONIND;
            ,MVCONTRA = this.w_MVCONTRA;
            ,MVCONTRO = this.w_MVCONTRO;
            ,MVDESART = this.w_MVDESART;
            ,MVDESSUP = this.w_MVDESSUP;
            ,MVFLOMAG = this.w_MVFLOMAG;
            ,MVFLTRAS = this.w_MVFLTRAS;
            ,MVIMPAC2 = this.w_MVIMPAC2;
            ,MVIMPACC = this.w_MVIMPACC;
            ,MVIMPCOM = this.w_MVIMPCOM;
            ,MVIMPNAZ = this.w_MVIMPNAZ;
            ,MVIMPPRO = this.w_MVIMPPRO;
            ,MVIMPSCO = this.w_MVIMPSCO;
            ,MVMOLSUP = this.w_MVMOLSUP;
            ,MVNOMENC = this.w_MVNOMENC;
            ,MVNUMCOL = this.w_MVNUMCOL;
            ,MVPERPRO = this.w_MVPERPRO;
            ,MVPESNET = this.w_MVPESNET;
            ,MVPREZZO = this.w_MVPREZZO;
            ,MVQTAMOV = this.w_MVQTAMOV;
            ,MVQTAUM1 = this.w_MVQTAUM1;
            ,MVROWRIF = this.w_MVROWRIF;
            ,MVSCONT1 = this.w_MVSCONT1;
            ,MVSCONT2 = this.w_MVSCONT2;
            ,MVSCONT3 = this.w_MVSCONT3;
            ,MVSCONT4 = this.w_MVSCONT4;
            ,MVSERRIF = this.w_MVSERRIF;
            ,MVTIPRIG = this.w_MVTIPRIG;
            ,MVUMSUPP = this.w_MVUMSUPP;
            ,MVUNIMIS = this.w_MVUNIMIS;
            ,MVVALMAG = this.w_MVVALMAG;
            ,MVVALRIG = this.w_MVVALRIG;
            ,MVVALULT = this.w_MVVALULT;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT GeneApp
      ENDSCAN
    endif
    this.w_TOTDOC = 0
    this.w_MINRATA = 0
    * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
    if Not this.pNORAT
      * --- Prima di inserire le nuove rate cancella le vecchie rate e imposta il flag "scadenze confermate"
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSCAF');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.Documento);
               )
      else
        update (i_cTable) set;
            MVFLSCAF = " ";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.Documento;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RSSERIAL = "+cp_ToStrODBC(this.Documento);
               )
      else
        delete from (i_cTable) where;
              RSSERIAL = this.Documento;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      FOR L_i=1 TO 999
      if NOT EMPTY(DR[L_i, 1]) AND NOT EMPTY(DR[L_i, 2])
        this.w_RSNUMRAT = L_i
        this.w_RSDATRAT = DR[L_i, 1]
        this.w_RSIMPRAT = DR[L_i, 2]
        this.w_RSMODPAG = DR[L_i, 3]
        this.w_RSFLPROV = DR[L_i, 9]
        this.w_TOTDOC = this.w_TOTDOC + this.w_RSIMPRAT
        this.w_MINRATA = MIN(this.w_MINRATA, this.w_RSIMPRAT)
        * --- Aggiorna rate
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+",RSFLPROV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.Documento),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RSFLPROV),'DOC_RATE','RSFLPROV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.Documento,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ",'RSFLPROV',this.w_RSFLPROV)
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP,RSFLPROV &i_ccchkf. );
             values (;
               this.Documento;
               ,this.w_RSNUMRAT;
               ,this.w_RSDATRAT;
               ,this.w_RSIMPRAT;
               ,this.w_RSMODPAG;
               ," ";
               ,this.w_RSFLPROV;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      NEXT
      if this.w_MINRATA<0 and Vartype( this.oparentobject.w_RETVAL)="C"
        if this.w_TOTDOC<0
          * --- Documento non Accettato
          this.oparentobject.w_RETVAL = ah_Msgformat("Errore inserimento rate: importo totale negativo")
        else
          if Vartype( this.oparentobject.w_WARNRET)="C"
            this.oparentobject.w_WARNRET = ah_Msgformat("ATTENZIONE: rate con importo negativo")
          endif
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_03BE9B68
    bErr_03BE9B68=bTrsErr
    this.Try_03BE9B68()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      if Vartype( this.oparentobject.w_RETVAL)="C"
        this.oparentobject.w_RETVAL=ah_MsgFormat("Errore inserimento dettaglio ritenute.%0%1", MESSAGE())
      endif
    endif
    bTrsErr=bTrsErr or bErr_03BE9B68
    * --- End
  endproc
  proc Try_03BE9B68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_DRCODBUN = g_CODBUN
    this.w_DECTOP = g_PERPVL
    this.w_AZIENDA = i_codazi
    this.w_TIPORITE = IIF(this.w_MVTIPCON="C" , this.w_TIPRIT,this.w_TIPORITE)
    this.w_ROWRIT = 0
    this.w_TIPO = IIF(this.w_MVTIPCON="C" , "V","A")
    this.w_DRDATREG = this.w_MVDATREG
    * --- Se il flag Dati Confermati non � attivo, eseguo i ricalcoli, altrimenti no.
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_DRSOMESC = this.w_IVAIND + this.w_MVSPEBOL + this.w_MVIMPARR
    this.w_DRIMPOSTA = this.w_DRIMPOSTA - this.w_IVAIND
    if IsAlt()
      * --- Toglie la cassa previdenza che viene considerata nel fondo professionisti
      this.w_DRSOMESC = this.w_DRSOMESC + this.w_TOTRIP4
    endif
    * --- Inizializzo la variabile contenente il calcolo della Cassa Professionisti
    this.w_DRFODPRO = cp_ROUND(this.w_DRPERPRO/(100 + this.w_DRPERPRO) * (this.w_DRTOTDOC - this.w_DRIMPOSTA - this.w_DRNONSO1 - this.w_DRSOMESC),this.w_DECTOP)
    if IsAlt()
      * --- La cassa previdenza viene considerata nel fondo professionisti
      this.w_DRFODPRO = this.w_DRFODPRO + this.w_MVSPETRA
    endif
    this.w_DRSOGGE = this.w_DRTOTDOC - this.w_DRIMPOSTA - this.w_DRNONSO1 - this.w_DRSOMESC - this.w_DRFODPRO
    * --- Se il fornitore � soggetto sia al contributo I.R.PE.F che al contributo I.N.P.S. 
    *     devo inserire due righe differenti altrimenti una unica riga
    this.w_PERCEN = IIF(this.w_DRPERRIT<=0, this.w_PERINPS, this.w_DRPERRIT)
    this.w_DRCODTRI = this.w_CODIRP
    this.w_VCODTRI = this.w_CODIRP
    this.w_DRCODCAU = this.w_CAURIT
    this.w_FLACON = "R"
    if this.w_DRTIPFOR = "A"
      * --- Se ho riempito le somme non soggette, non devo rieffettuare il calcolo per la percentuale imponibile
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGPERIMP,AGRITIRP"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODFOR = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGPERIMP,AGRITIRP;
          from (i_cTable) where;
              AGCODFOR = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGEIMPO = NVL(cp_ToDate(_read_.AGPERIMP),cp_NullValue(_read_.AGPERIMP))
        this.w_AGERITE = NVL(cp_ToDate(_read_.AGRITIRP),cp_NullValue(_read_.AGRITIRP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DRIMPONI = cp_ROUND(this.w_AGEIMPO/100*(this.w_DRSOGGE),this.w_DECTOP)
      this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*this.w_AGERITE/100,this.w_DECTOP)
      this.w_NONSOGGCA = this.w_DRSOGGE - this.w_DRIMPONI
      this.w_DRPERRIT = this.w_AGERITE
      this.w_DRRIINPS = this.w_RITINPS
    else
       
 vq_exec("..\RITE\EXE\QUERY\GSRI_BRT", this,"PERCENTUALI") 
 SELECT PERCENTUALI
      if RECCOUNT("PERCENTUALI") > 0
        this.w_DRIMPONI = cp_ROUND(IMPONIBILE/100*this.w_DRSOGGE,this.w_DECTOP)
        this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*RITENUTA/100,this.w_DECTOP)
        this.w_NONSOGGCA = this.w_DRSOGGE - this.w_DRIMPONI
        this.w_DRPERRIT = RITENUTA
        this.w_DRRIINPS = this.w_RITINPS
      else
        * --- Codice Tributo non presente nella tabella tributi
        this.w_DRCODTRI = Space(5)
      endif
      if used ("PERCENTUALI")
        select PERCENTUALI
        use
      endif
    endif
    this.w_DRNONSOG = this.w_NONSOGGCA
    this.w_ROWRIT = this.w_ROWRIT + 1
    * --- Creo ritenute
    if Not Empty(this.w_DRCODTRI)
      this.w_MVTOTRIT = this.w_DRRITENU
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_TIPRITE="C" And Not empty(this.w_CODTR2)
      * --- Fornitore soggetto sia ad I.R.PE.F. che I.N.P.S.
      *     Devo inserire prima la riga IRPEF e poi successivamente la riga INPS
      * --- Ricalcolo sempre l'importo da versare
      this.w_ROWRIT = this.w_ROWRIT + 1
      this.w_DRCODTRI = this.w_CODTR2
      this.w_VCODTRI = this.w_CODTR2
      this.w_DRCODCAU = "A"
      this.w_FLACON = "Z"
      * --- Calcolo Imponibile INPS
      this.w_DRIMPONI = cp_ROUND((this.w_DRSOGGE) * this.w_IMPINPS/100,this.w_DECTOP)
      this.w_NONSOGGCA = this.w_DRSOGGE - this.w_DRIMPONI
      this.w_DRRITENU = cp_ROUND(this.w_DRIMPONI*(this.w_PERCEN)/100,this.w_DECTOP)
      this.w_DRPERRIT = this.w_PERINPS
      this.w_DRRIINPS = this.w_RITINPS
      this.w_DRNONSOG = this.w_NONSOGGCA
      * --- Creo ritenute
      this.w_MVRITPRE = this.w_DRRITENU
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DRTOTDOC = this.w_TOTFATTU
    this.w_DRIMPOSTA = this.w_TOTIMPOS
    this.w_IVAIND = 0
    * --- Determino l'IVA indetraibile 1a Aliquota
    if Not Empty( this.w_MVACIVA1 ) And IIF(this.w_MVAFLOM1 $ "XI", this.w_MVAIMPS1,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS1 * this.w_PERIND/100 , this.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 2a Aliquota
    if Not Empty( this.w_MVACIVA2 ) And IIF(this.w_MVAFLOM2 $ "XI", this.w_MVAIMPS2,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA2);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA2;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS2 * this.w_PERIND/100 , this.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 3a Aliquota
    if Not Empty( this.w_MVACIVA3 ) And IIF(this.w_MVAFLOM3 $ "XI", this.w_MVAIMPS3,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA3);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA3;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS3 * this.w_PERIND/100 , this.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 4a Aliquota
    if Not Empty( this.w_MVACIVA4 ) And IIF(this.w_MVAFLOM4 $ "XI", this.w_MVAIMPS4,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA4);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA4;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS4 * this.w_PERIND/100 , this.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 5a Aliquota
    if Not Empty( this.w_MVACIVA5 ) And IIF(this.w_MVAFLOM5 $ "XI", this.w_MVAIMPS5,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA5);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA5;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS5 * this.w_PERIND/100 , this.w_DECTOT )
    endif
    * --- Determino l'IVA indetraibile 6a Aliquota
    if Not Empty( this.w_MVACIVA6 ) And IIF(this.w_MVAFLOM6 $ "XI", this.w_MVAIMPS6,0)<>0
      this.w_PERIND = 0
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA6);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVACIVA6;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVAIND = this.w_IVAIND + cp_Round ( this.w_MVAIMPS6 * this.w_PERIND/100 , this.w_DECTOT )
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Insert into VDATRITE
    i_nConn=i_TableProp[this.VDATRITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VDATRITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VDATRITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",DRSOMESC"+",DRFODPRO"+",DRCODTRI"+",DRCODCAU"+",DRIMPONI"+",DRRITENU"+",DRPERRIT"+",DRNONSOG"+",DRNONSO1"+",DRCODBUN"+",CPROWNUM"+",DRRIINPS"+",DRDATCON"+",DRSPERIM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.Documento),'VDATRITE','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRSOMESC),'VDATRITE','DRSOMESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'VDATRITE','DRFODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'VDATRITE','DRCODTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'VDATRITE','DRCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRIMPONI),'VDATRITE','DRIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRITENU),'VDATRITE','DRRITENU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'VDATRITE','DRPERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRNONSOG),'VDATRITE','DRNONSOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'VDATRITE','DRNONSO1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODBUN),'VDATRITE','DRCODBUN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIT),'VDATRITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRRIINPS),'VDATRITE','DRRIINPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRDATCON),'VDATRITE','DRDATCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'VDATRITE','DRSPERIM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.Documento,'DRSOMESC',this.w_DRSOMESC,'DRFODPRO',this.w_DRFODPRO,'DRCODTRI',this.w_DRCODTRI,'DRCODCAU',this.w_DRCODCAU,'DRIMPONI',this.w_DRIMPONI,'DRRITENU',this.w_DRRITENU,'DRPERRIT',this.w_DRPERRIT,'DRNONSOG',this.w_DRNONSOG,'DRNONSO1',this.w_DRNONSO1,'DRCODBUN',this.w_DRCODBUN,'CPROWNUM',this.w_ROWRIT)
      insert into (i_cTable) (DRSERIAL,DRSOMESC,DRFODPRO,DRCODTRI,DRCODCAU,DRIMPONI,DRRITENU,DRPERRIT,DRNONSOG,DRNONSO1,DRCODBUN,CPROWNUM,DRRIINPS,DRDATCON,DRSPERIM &i_ccchkf. );
         values (;
           this.Documento;
           ,this.w_DRSOMESC;
           ,this.w_DRFODPRO;
           ,this.w_DRCODTRI;
           ,this.w_DRCODCAU;
           ,this.w_DRIMPONI;
           ,this.w_DRRITENU;
           ,this.w_DRPERRIT;
           ,this.w_DRNONSOG;
           ,this.w_DRNONSO1;
           ,this.w_DRCODBUN;
           ,this.w_ROWRIT;
           ,this.w_DRRIINPS;
           ,this.w_DRDATCON;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_PROGEN= "S"
      this.w_CODAZI = i_CODAZI
      this.w_SERPRO = this.Documento
      this.w_MASSGEN = "S"
      * --- Considero sempre come se precedentemente avesi avuto disattivo
      *     In questo modo mi aggiorna il documento generato nel modo corretto
      this.w_OLDCALPRO = "DI"
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPCALPRO,PPLISRIF"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPCALPRO,PPLISRIF;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
        this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
        this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do GSVE_BPP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Documento,pNOBFA,pNORAT,pNODET,pNOCUR,pOKRIT,pRICSPE,pRICPRO)
    this.Documento=Documento
    this.pNOBFA=pNOBFA
    this.pNORAT=pNORAT
    this.pNODET=pNODET
    this.pNOCUR=pNOCUR
    this.pOKRIT=pOKRIT
    this.pRICSPE=pRICSPE
    this.pRICPRO=pRICPRO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='TRI_BUTI'
    this.cWorkTables[10]='AGENTI'
    this.cWorkTables[11]='VDATRITE'
    this.cWorkTables[12]='CAU_CONT'
    this.cWorkTables[13]='AZIENDA'
    this.cWorkTables[14]='PAR_PROV'
    this.cWorkTables[15]='VETTORI'
    this.cWorkTables[16]='DES_DIVE'
    this.cWorkTables[17]='MODASPED'
    return(this.OpenAllTables(17))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Documento,pNOBFA,pNORAT,pNODET,pNOCUR,pOKRIT,pRICSPE,pRICPRO"
endproc
