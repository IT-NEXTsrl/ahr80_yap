* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_baf                                                        *
*              Aggiorna flag saldi in documenti                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_92]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2008-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_baf",oParentObject)
return(i_retval)

define class tgsve_baf as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Flag Saldi/Competenza (da GSAC_MDV,GSVE_MDV,GSOR_MDV)
    * --- Cambiato Flag provvisorio/Confermato, aggiorna Flag Saldi
    this.w_PADRE = This.oParentObject
    this.w_PADRE.MarkPos()     
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if Not Empty( this.oParentObject.w_MVCODICE )
        * --- Prima di eseguire la mCalc valorizzo tutte le o_ nel modo corretto
        *     altrimenti rieseguirebbe dei ricalcoli che non deve eseguire (esempio ricalcolo del prezzo)
        *     Solo o_MVFLPROV deve essere diverso dalla w_ in modo che solo i calcoli che
        *     dipendono da MVFLPROV siano effettuati
        this.w_PADRE.SaveDependsOn()     
        * --- Valorizzo a vuoto il campo o_MVFLPROV per forzare i calcoli su
        *     ogni riga dipendenti dal flag provvisorio..
        this.oParentObject.o_MVFLPROV = " "
        * --- Lancio la mCalc per svolgere i calcoli legati al flag provvisorio
        this.w_PADRE.mCalc(.t.)     
        * --- Aggiorno le o_ 
        *     Le variabili modificate dal cambio del Provvisorio potrebbero rimanere disallineate
        *     Vedi per esempio il Flag di Aggiornamento saldi
        this.w_PADRE.SaveDependsOn()     
        this.w_PADRE.SaveRow()     
      endif
      this.w_PADRE.NextRow()     
    enddo
    this.w_PADRE.RePos(.F.)     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
