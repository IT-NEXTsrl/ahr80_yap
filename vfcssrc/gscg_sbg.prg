* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sbg                                                        *
*              Stampa bilancio di generale                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-05                                                      *
* Last revis.: 2013-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sbg",oParentObject))

* --- Class definition
define class tgscg_sbg as StdForm
  Top    = 19
  Left   = 19

  * --- Standard Properties
  Width  = 689
  Height = 395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-09"
  HelpContextID=107619689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Constant Properties
  _IDX = 0
  TIR_MAST_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  VOCIRICL_IDX = 0
  cPrg = "gscg_sbg"
  cComment = "Stampa bilancio di generale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_codaz1 = space(5)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_UTENTE = 0
  w_TIPO = space(1)
  w_gestbu = space(1)
  w_ESE = space(4)
  o_ESE = space(4)
  w_ESSALDI = space(1)
  o_ESSALDI = space(1)
  w_ESSALDI = space(1)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_BILANCIO = space(15)
  w_INFRAAN = space(1)
  w_BILDES = space(0)
  w_totdat = space(1)
  o_totdat = space(1)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_monete = space(1)
  o_monete = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_CAMBGIOR = 0
  w_simval = space(5)
  w_CODBUN = space(3)
  w_SUPERBU = space(15)
  w_SBDESCRI = space(35)
  w_TUTTI = space(1)
  w_PROVVI = space(1)
  o_PROVVI = space(1)
  w_NOTE = space(1)
  o_NOTE = space(1)
  w_EXCEL = space(1)
  o_EXCEL = space(1)
  w_EXCEL = space(1)
  w_CONFRONTO = space(1)
  o_CONFRONTO = space(1)
  w_ARROT = space(1)
  o_ARROT = space(1)
  w_ARRVOCI = space(1)
  o_ARRVOCI = space(1)
  w_USO = space(1)
  w_ORIPREC = space(1)
  w_DETCON = space(1)
  o_DETCON = space(1)
  w_descri = space(30)
  w_decimi = 0
  w_BUDESCRI = space(35)
  w_CAMVAL = 0
  o_CAMVAL = 0
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_TIPVOC = space(10)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_TIPSTA = space(1)
  w_PRDECTOT = 0
  w_PRINIESE = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sbgPag1","gscg_sbg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='TIR_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='BUSIUNIT'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='SB_MAST'
    this.cWorkTables[7]='VOCIRICL'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSCG_BBG(this,1)
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sbg
    this.bUpdated=.t.
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_codaz1=space(5)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_UTENTE=0
      .w_TIPO=space(1)
      .w_gestbu=space(1)
      .w_ESE=space(4)
      .w_ESSALDI=space(1)
      .w_ESSALDI=space(1)
      .w_VALAPP=space(3)
      .w_BILANCIO=space(15)
      .w_INFRAAN=space(1)
      .w_BILDES=space(0)
      .w_totdat=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_ESPREC=space(1)
      .w_monete=space(1)
      .w_DIVISA=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_CAMBGIOR=0
      .w_simval=space(5)
      .w_CODBUN=space(3)
      .w_SUPERBU=space(15)
      .w_SBDESCRI=space(35)
      .w_TUTTI=space(1)
      .w_PROVVI=space(1)
      .w_NOTE=space(1)
      .w_EXCEL=space(1)
      .w_EXCEL=space(1)
      .w_CONFRONTO=space(1)
      .w_ARROT=space(1)
      .w_ARRVOCI=space(1)
      .w_USO=space(1)
      .w_ORIPREC=space(1)
      .w_DETCON=space(1)
      .w_descri=space(30)
      .w_decimi=0
      .w_BUDESCRI=space(35)
      .w_CAMVAL=0
      .w_EXTRAEUR=0
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_TIPVOC=space(10)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_TIPSTA=space(1)
      .w_PRDECTOT=0
      .w_PRINIESE=ctod("  /  /  ")
        .w_codaz1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_codaz1))
          .link_1_1('Full')
        endif
        .w_MASABI = ' '
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',NVL(LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1),'    '))
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_UTENTE = g_CODUTE
        .w_TIPO = 'M'
          .DoRTCalc(7,7,.f.)
        .w_ESE = g_codese
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ESE))
          .link_1_8('Full')
        endif
        .w_ESSALDI = IIF(.w_MASABI='S','M','S')
        .w_ESSALDI = IIF(.w_MASABI='S',' ','S')
        .w_VALAPP = g_PERVAL
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_VALAPP))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_BILANCIO))
          .link_1_12('Full')
        endif
        .w_INFRAAN = 'N'
          .DoRTCalc(14,14,.f.)
        .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        .w_monete = 'c'
        .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_DIVISA))
          .link_1_20('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
          .DoRTCalc(23,23,.f.)
        .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CODBUN))
          .link_1_24('Full')
        endif
        .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_25('Full')
        endif
          .DoRTCalc(26,27,.f.)
        .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
          .DoRTCalc(29,31,.f.)
        .w_CONFRONTO = ' '
        .w_ARROT = 'N'
        .w_ARRVOCI = IIF(.w_DETCON='S','M','V')
        .w_USO = 'S'
        .w_ORIPREC = IIF(.w_MASABI='S','M','S')
        .w_DETCON = IIF((.w_CONFRONTO='S' AND .w_EXCEL='S') OR (.w_ARROT$'ST' AND .w_ARRVOCI='V'),' ',.w_DETCON)
          .DoRTCalc(38,40,.f.)
        .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
          .DoRTCalc(42,44,.f.)
        .w_TIPVOC = 'M'
          .DoRTCalc(46,47,.f.)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_esepre))
          .link_1_62('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_63('Full')
        endif
          .DoRTCalc(50,50,.f.)
        .w_TIPSTA = 'T'
    endwith
    this.DoRTCalc(52,53,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,10,.t.)
        if .o_ESE<>.w_ESE
          .link_1_11('Full')
        endif
        .DoRTCalc(12,14,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        endif
        if .o_data1<>.w_data1.or. .o_totdat<>.w_totdat.or. .o_PROVVI<>.w_PROVVI
            .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        endif
        .DoRTCalc(19,19,.t.)
        if .o_monete<>.w_monete.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
          .link_1_20('Full')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_totdat<>.w_totdat.or. .o_ESPREC<>.w_ESPREC.or. .o_data1<>.w_data1.or. .o_ESSALDI<>.w_ESSALDI
            .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
          .link_1_24('Full')
        endif
        if .o_data1<>.w_data1.or. .o_ESPREC<>.w_ESPREC.or. .o_totdat<>.w_totdat.or. .o_ESSALDI<>.w_ESSALDI
            .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
          .link_1_25('Full')
        endif
        .DoRTCalc(26,27,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        endif
        .DoRTCalc(29,31,.t.)
        if .o_NOTE<>.w_NOTE
            .w_CONFRONTO = ' '
        endif
        .DoRTCalc(33,33,.t.)
        if .o_DETCON<>.w_DETCON
            .w_ARRVOCI = IIF(.w_DETCON='S','M','V')
        endif
        .DoRTCalc(35,36,.t.)
        if .o_CONFRONTO<>.w_CONFRONTO.or. .o_EXCEL<>.w_EXCEL.or. .o_ARRVOCI<>.w_ARRVOCI.or. .o_ARROT<>.w_ARROT
            .w_DETCON = IIF((.w_CONFRONTO='S' AND .w_EXCEL='S') OR (.w_ARROT$'ST' AND .w_ARRVOCI='V'),' ',.w_DETCON)
        endif
        .DoRTCalc(38,40,.t.)
        if .o_EXTRAEUR<>.w_EXTRAEUR.or. .o_DIVISA<>.w_DIVISA
            .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        endif
        .DoRTCalc(42,47,.t.)
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_62('Full')
        endif
          .link_1_63('Full')
        if .o_CAMVAL<>.w_CAMVAL
          .Calculate_XBDFWMIFUK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(50,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_XBDFWMIFUK()
    with this
          * --- Inserisci cambio giornaliero
     if not empty(.w_DIVISA) and .w_DIVISA<>g_perval and .w_CAMBGIOR<>.w_CAMVAL
          GSAR_BIC(this;
              ,.w_DIVISA;
              ,.w_CAMVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBILANCIO_1_12.enabled = this.oPgFrm.Page1.oPag.oBILANCIO_1_12.mCond()
    this.oPgFrm.Page1.oPag.ototdat_1_15.enabled = this.oPgFrm.Page1.oPag.ototdat_1_15.mCond()
    this.oPgFrm.Page1.oPag.odata1_1_16.enabled = this.oPgFrm.Page1.oPag.odata1_1_16.mCond()
    this.oPgFrm.Page1.oPag.odata2_1_17.enabled = this.oPgFrm.Page1.oPag.odata2_1_17.mCond()
    this.oPgFrm.Page1.oPag.oESPREC_1_18.enabled = this.oPgFrm.Page1.oPag.oESPREC_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDIVISA_1_20.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCODBUN_1_24.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_25.enabled = this.oPgFrm.Page1.oPag.oSUPERBU_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPROVVI_1_28.enabled = this.oPgFrm.Page1.oPag.oPROVVI_1_28.mCond()
    this.oPgFrm.Page1.oPag.oDETCON_1_37.enabled = this.oPgFrm.Page1.oPag.oDETCON_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_54.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_9.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oESSALDI_1_10.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBILANCIO_1_12.visible=!this.oPgFrm.Page1.oPag.oBILANCIO_1_12.mHide()
    this.oPgFrm.Page1.oPag.oINFRAAN_1_13.visible=!this.oPgFrm.Page1.oPag.oINFRAAN_1_13.mHide()
    this.oPgFrm.Page1.oPag.oESPREC_1_18.visible=!this.oPgFrm.Page1.oPag.oESPREC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCODBUN_1_24.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_24.mHide()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_25.visible=!this.oPgFrm.Page1.oPag.oSUPERBU_1_25.mHide()
    this.oPgFrm.Page1.oPag.oSBDESCRI_1_26.visible=!this.oPgFrm.Page1.oPag.oSBDESCRI_1_26.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_30.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_30.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_31.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCONFRONTO_1_32.visible=!this.oPgFrm.Page1.oPag.oCONFRONTO_1_32.mHide()
    this.oPgFrm.Page1.oPag.oARRVOCI_1_34.visible=!this.oPgFrm.Page1.oPag.oARRVOCI_1_34.mHide()
    this.oPgFrm.Page1.oPag.oORIPREC_1_36.visible=!this.oPgFrm.Page1.oPag.oORIPREC_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.visible=!this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codaz1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codaz1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codaz1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_codaz1)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codaz1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_codaz1 = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codaz1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        endif
        this.w_ESE = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_DECNAZ = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BILANCIO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIR_MAST_IDX,3]
    i_lTable = "TIR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2], .t., this.TIR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BILANCIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MTR',True,'TIR_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_BILANCIO)+"%");
                   +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TR__TIPO,TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TR__TIPO',this.w_TIPO;
                     ,'TRCODICE',trim(this.w_BILANCIO))
          select TR__TIPO,TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TR__TIPO,TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BILANCIO)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BILANCIO) and !this.bDontReportError
            deferred_cp_zoom('TIR_MAST','*','TR__TIPO,TRCODICE',cp_AbsName(oSource.parent,'oBILANCIO_1_12'),i_cWhere,'GSCG_MTR',"Strutture di bilancio",'GSAR0MTR.TIR_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TR__TIPO',oSource.xKey(1);
                       ,'TRCODICE',oSource.xKey(2))
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BILANCIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TR__TIPO,TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_BILANCIO);
                   +" and TR__TIPO="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TR__TIPO',this.w_TIPO;
                       ,'TRCODICE',this.w_BILANCIO)
            select TR__TIPO,TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BILANCIO = NVL(_Link_.TRCODICE,space(15))
      this.w_BILDES = NVL(_Link_.TRDESCRI,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_BILANCIO = space(15)
      endif
      this.w_BILDES = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.TR__TIPO,1)+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BILANCIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_DIVISA)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_20'),i_cWhere,'GSAR_AVL',"Divise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_simval = NVL(_Link_.VASIMVAL,space(5))
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_EXTRAEUR = 0
      this.w_simval = space(5)
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_codaz1;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_24'),i_cWhere,'GSAR_KBU',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MSB',True,'SB_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SBCODICE like "+cp_ToStrODBC(trim(this.w_SUPERBU)+"%");

          i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SBCODICE',trim(this.w_SUPERBU))
          select SBCODICE,SBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUPERBU)==trim(_Link_.SBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SUPERBU) and !this.bDontReportError
            deferred_cp_zoom('SB_MAST','*','SBCODICE',cp_AbsName(oSource.parent,'oSUPERBU_1_25'),i_cWhere,'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',oSource.xKey(1))
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_PRINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
      this.w_PRINIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_PRDECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
      this.w_PRDECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_8.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_8.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_9.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_10.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBILANCIO_1_12.value==this.w_BILANCIO)
      this.oPgFrm.Page1.oPag.oBILANCIO_1_12.value=this.w_BILANCIO
    endif
    if not(this.oPgFrm.Page1.oPag.oINFRAAN_1_13.RadioValue()==this.w_INFRAAN)
      this.oPgFrm.Page1.oPag.oINFRAAN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBILDES_1_14.value==this.w_BILDES)
      this.oPgFrm.Page1.oPag.oBILDES_1_14.value=this.w_BILDES
    endif
    if not(this.oPgFrm.Page1.oPag.ototdat_1_15.RadioValue()==this.w_totdat)
      this.oPgFrm.Page1.oPag.ototdat_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odata1_1_16.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_16.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_17.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_17.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oESPREC_1_18.RadioValue()==this.w_ESPREC)
      this.oPgFrm.Page1.oPag.oESPREC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.omonete_1_19.RadioValue()==this.w_monete)
      this.oPgFrm.Page1.oPag.omonete_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_20.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_20.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.osimval_1_23.value==this.w_simval)
      this.oPgFrm.Page1.oPag.osimval_1_23.value=this.w_simval
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_24.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_24.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSUPERBU_1_25.value==this.w_SUPERBU)
      this.oPgFrm.Page1.oPag.oSUPERBU_1_25.value=this.w_SUPERBU
    endif
    if not(this.oPgFrm.Page1.oPag.oSBDESCRI_1_26.value==this.w_SBDESCRI)
      this.oPgFrm.Page1.oPag.oSBDESCRI_1_26.value=this.w_SBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_1_27.RadioValue()==this.w_TUTTI)
      this.oPgFrm.Page1.oPag.oTUTTI_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_28.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_29.RadioValue()==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_30.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_31.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFRONTO_1_32.RadioValue()==this.w_CONFRONTO)
      this.oPgFrm.Page1.oPag.oCONFRONTO_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT_1_33.RadioValue()==this.w_ARROT)
      this.oPgFrm.Page1.oPag.oARROT_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARRVOCI_1_34.RadioValue()==this.w_ARRVOCI)
      this.oPgFrm.Page1.oPag.oARRVOCI_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUSO_1_35.RadioValue()==this.w_USO)
      this.oPgFrm.Page1.oPag.oUSO_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORIPREC_1_36.RadioValue()==this.w_ORIPREC)
      this.oPgFrm.Page1.oPag.oORIPREC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETCON_1_37.RadioValue()==this.w_DETCON)
      this.oPgFrm.Page1.oPag.oDETCON_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.value==this.w_BUDESCRI)
      this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.value=this.w_BUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_54.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_54.value=this.w_CAMVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ESE)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio storicizzato")
          case   (empty(.w_BILANCIO))  and not(g_APPLICATION="ADHOC REVOLUTION")  and (g_APPLICATION<>"ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBILANCIO_1_12.SetFocus()
            i_bnoObbl = !empty(.w_BILANCIO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo date non valido o data inferiore a quella di storicizzazione")
          case   not(empty(.w_data1) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non valido")
          case   (empty(.w_DIVISA))  and (.w_monete<>'c')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_20.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_54.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_62.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESE = this.w_ESE
    this.o_ESSALDI = this.w_ESSALDI
    this.o_VALAPP = this.w_VALAPP
    this.o_totdat = this.w_totdat
    this.o_data1 = this.w_data1
    this.o_ESPREC = this.w_ESPREC
    this.o_monete = this.w_monete
    this.o_DIVISA = this.w_DIVISA
    this.o_PROVVI = this.w_PROVVI
    this.o_NOTE = this.w_NOTE
    this.o_EXCEL = this.w_EXCEL
    this.o_CONFRONTO = this.w_CONFRONTO
    this.o_ARROT = this.w_ARROT
    this.o_ARRVOCI = this.w_ARRVOCI
    this.o_DETCON = this.w_DETCON
    this.o_CAMVAL = this.w_CAMVAL
    this.o_EXTRAEUR = this.w_EXTRAEUR
    return

enddefine

* --- Define pages as container
define class tgscg_sbgPag1 as StdContainer
  Width  = 685
  height = 395
  stdWidth  = 685
  stdheight = 395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_8 as StdField with uid="HDHRXGXWEA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 107314618,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=116, Top=6, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oESSALDI_1_9 as StdCombo with uid="LHILIIGAEZ",rtseq=9,rtrep=.f.,left=318,top=8,width=163,height=21;
    , ToolTipText = "I dati estratti per esercizio attuale verranno presi dall'origine indicata";
    , HelpContextID = 224632250;
    , cFormVar="w_ESSALDI",RowSource=""+"Saldi esercizio,"+"Movimenti di primanota,"+"Saldi bilancio UE", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oESSALDI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oESSALDI_1_9.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_9.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      iif(this.Parent.oContained.w_ESSALDI=='M',2,;
      iif(this.Parent.oContained.w_ESSALDI=='B',3,;
      0)))
  endfunc

  func oESSALDI_1_9.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oESSALDI_1_10 as StdCheck with uid="HYXMAEJYHM",rtseq=10,rtrep=.f.,left=179, top=9, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 224632250,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_10.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_10.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_10.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION" or .w_MASABI='S')
    endwith
  endfunc

  add object oBILANCIO_1_12 as StdField with uid="CGDRGNGLJU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BILANCIO", cQueryName = "BILANCIO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio selezionata",;
    HelpContextID = 239343515,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=548, Top=6, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIR_MAST", cZoomOnZoom="GSCG_MTR", oKey_1_1="TR__TIPO", oKey_1_2="this.w_TIPO", oKey_2_1="TRCODICE", oKey_2_2="this.w_BILANCIO"

  func oBILANCIO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oBILANCIO_1_12.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oBILANCIO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oBILANCIO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBILANCIO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIR_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TR__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TR__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'TIR_MAST','*','TR__TIPO,TRCODICE',cp_AbsName(this.parent,'oBILANCIO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MTR',"Strutture di bilancio",'GSAR0MTR.TIR_MAST_VZM',this.parent.oContained
  endproc
  proc oBILANCIO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MTR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TR__TIPO=w_TIPO
     i_obj.w_TRCODICE=this.parent.oContained.w_BILANCIO
     i_obj.ecpSave()
  endproc

  add object oINFRAAN_1_13 as StdCheck with uid="RRZRWNHGRB",rtseq=13,rtrep=.f.,left=116, top=30, caption="Escludi movimenti da chiusura infrannuale",;
    ToolTipText = "Se attivo, non stampa le registrazioni da chiusure infrannuali",;
    HelpContextID = 17003130,;
    cFormVar="w_INFRAAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFRAAN_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINFRAAN_1_13.GetRadio()
    this.Parent.oContained.w_INFRAAN = this.RadioValue()
    return .t.
  endfunc

  func oINFRAAN_1_13.SetRadio()
    this.Parent.oContained.w_INFRAAN=trim(this.Parent.oContained.w_INFRAAN)
    this.value = ;
      iif(this.Parent.oContained.w_INFRAAN=='S',1,;
      0)
  endfunc

  func oINFRAAN_1_13.mHide()
    with this.Parent.oContained
      return (Not islron())
    endwith
  endfunc

  add object oBILDES_1_14 as StdMemo with uid="OUTWELNEAD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_BILDES", cQueryName = "BILDES",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del bilancio",;
    HelpContextID = 19851286,;
   bGlobalFont=.t.,;
    Height=78, Width=562, Left=116, Top=48


  add object ototdat_1_15 as StdCombo with uid="BRHXJMVGFR",rtseq=15,rtrep=.f.,left=116,top=132,width=136,height=21;
    , ToolTipText = "Tipo bilancio selezionato";
    , HelpContextID = 68260150;
    , cFormVar="w_totdat",RowSource=""+"Totale esercizio,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ototdat_1_15.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func ototdat_1_15.GetRadio()
    this.Parent.oContained.w_totdat = this.RadioValue()
    return .t.
  endfunc

  func ototdat_1_15.SetRadio()
    this.Parent.oContained.w_totdat=trim(this.Parent.oContained.w_totdat)
    this.value = ;
      iif(this.Parent.oContained.w_totdat=='T',1,;
      iif(this.Parent.oContained.w_totdat=='D',2,;
      0))
  endfunc

  func ototdat_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object odata1_1_16 as StdField with uid="LRAOKJALSD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo date non valido o data inferiore a quella di storicizzazione",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 49380810,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=336, Top=132

  func odata1_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata1_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)
    endwith
    return bRes
  endfunc

  add object odata2_1_17 as StdField with uid="VDVPBDFQIF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non valido",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 48332234,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=436, Top=132

  func odata2_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata2_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc

  add object oESPREC_1_18 as StdCheck with uid="NULXFFIFIL",rtseq=18,rtrep=.f.,left=523, top=132, caption="Saldi es. precedente",;
    ToolTipText = "Considera i saldi dell'esercizio precedente",;
    HelpContextID = 20787782,;
    cFormVar="w_ESPREC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESPREC_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESPREC_1_18.GetRadio()
    this.Parent.oContained.w_ESPREC = this.RadioValue()
    return .t.
  endfunc

  func oESPREC_1_18.SetRadio()
    this.Parent.oContained.w_ESPREC=trim(this.Parent.oContained.w_ESPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ESPREC=='S',1,;
      0)
  endfunc

  func oESPREC_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTDAT='D' AND .w_DATA1=.w_DATINIESE)
    endwith
   endif
  endfunc

  func oESPREC_1_18.mHide()
    with this.Parent.oContained
      return (.w_PROVVI='S')
    endwith
  endfunc


  add object omonete_1_19 as StdCombo with uid="DFMUFDTKKC",rtseq=19,rtrep=.f.,left=116,top=161,width=136,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 105001158;
    , cFormVar="w_monete",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func omonete_1_19.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func omonete_1_19.GetRadio()
    this.Parent.oContained.w_monete = this.RadioValue()
    return .t.
  endfunc

  func omonete_1_19.SetRadio()
    this.Parent.oContained.w_monete=trim(this.Parent.oContained.w_monete)
    this.value = ;
      iif(this.Parent.oContained.w_monete=='c',1,;
      iif(this.Parent.oContained.w_monete=='a',2,;
      0))
  endfunc

  add object oDIVISA_1_20 as StdField with uid="YVASPWERFS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 1345590,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=336, Top=161, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete<>'c')
    endwith
   endif
  endfunc

  func oDIVISA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Divise",'',this.parent.oContained
  endproc
  proc oDIVISA_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_DIVISA
     i_obj.ecpSave()
  endproc

  add object osimval_1_23 as StdField with uid="ERKUGGURPJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_simval", cQueryName = "simval",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 203627302,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=386, Top=161, InputMask=replicate('X',5)

  add object oCODBUN_1_24 as StdField with uid="IMRWTDQUOB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 221015590,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=116, Top=217, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_gestbu $ 'ET') and EMPTY(.w_SUPERBU) and .w_ESSALDI<>'S' and g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oCODBUN_1_24.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODBUN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"Business Unit",'',this.parent.oContained
  endproc
  proc oCODBUN_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_codaz1
     i_obj.w_BUCODICE=this.parent.oContained.w_CODBUN
     i_obj.ecpSave()
  endproc

  add object oSUPERBU_1_25 as StdField with uid="YUWOKATWYZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SUPERBU", cQueryName = "SUPERBU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo Business Unit -attivo solo senza riporto saldi",;
    HelpContextID = 16790822,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=116, Top=247, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SB_MAST", cZoomOnZoom="GSAR_MSB", oKey_1_1="SBCODICE", oKey_1_2="this.w_SUPERBU"

  func oSUPERBU_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_gestbu $ 'ET') AND EMPTY(.w_CODBUN) and .w_ESSALDI<>'S' AND g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oSUPERBU_1_25.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oSUPERBU_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oSUPERBU_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUPERBU_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SB_MAST','*','SBCODICE',cp_AbsName(this.parent,'oSUPERBU_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this.parent.oContained
  endproc
  proc oSUPERBU_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MSB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SBCODICE=this.parent.oContained.w_SUPERBU
     i_obj.ecpSave()
  endproc

  add object oSBDESCRI_1_26 as StdField with uid="FWMLDOVXTV",rtseq=26,rtrep=.f.,;
    cFormVar = "w_SBDESCRI", cQueryName = "SBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34562671,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=252, Top=247, InputMask=replicate('X',35)

  func oSBDESCRI_1_26.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oTUTTI_1_27 as StdCheck with uid="YAJCHNOHIZ",rtseq=27,rtrep=.f.,left=116, top=272, caption="Saldi significativi",;
    ToolTipText = "Stampa solo i conti con saldo diverso da 0",;
    HelpContextID = 25201354,;
    cFormVar="w_TUTTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTUTTI_1_27.GetRadio()
    this.Parent.oContained.w_TUTTI = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_1_27.SetRadio()
    this.Parent.oContained.w_TUTTI=trim(this.Parent.oContained.w_TUTTI)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI=='S',1,;
      0)
  endfunc


  add object oPROVVI_1_28 as StdCombo with uid="WZWYZYTQAG",value=3,rtseq=28,rtrep=.f.,left=116,top=190,width=136,height=21;
    , ToolTipText = "Selezione dello stato dei movimenti da stampare";
    , HelpContextID = 139534838;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_28.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPROVVI_1_28.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_28.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oNOTE_1_29 as StdCheck with uid="BHNBPHDIZA",rtseq=29,rtrep=.f.,left=276, top=293, caption="Note aggiuntive",;
    ToolTipText = "Stampa le note associate alle voci",;
    HelpContextID = 102732074,;
    cFormVar="w_NOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOTE_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oNOTE_1_29.GetRadio()
    this.Parent.oContained.w_NOTE = this.RadioValue()
    return .t.
  endfunc

  func oNOTE_1_29.SetRadio()
    this.Parent.oContained.w_NOTE=trim(this.Parent.oContained.w_NOTE)
    this.value = ;
      iif(this.Parent.oContained.w_NOTE=='S',1,;
      0)
  endfunc

  add object oEXCEL_1_30 as StdCheck with uid="IDTTJCJMPK",rtseq=30,rtrep=.f.,left=432, top=272, caption="Stampa su calc",;
    ToolTipText = "Invia i dati su foglio calc",;
    HelpContextID = 23107770,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEXCEL_1_30.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_30.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_30.mHide()
    with this.Parent.oContained
      return (g_office='M')
    endwith
  endfunc

  add object oEXCEL_1_31 as StdCheck with uid="YEUEKHLKZQ",rtseq=31,rtrep=.f.,left=432, top=272, caption="Stampa su Excel",;
    ToolTipText = "Invia i dati su foglio Excel",;
    HelpContextID = 23107770,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEXCEL_1_31.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_31.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_31.mHide()
    with this.Parent.oContained
      return (g_office<>'M')
    endwith
  endfunc

  add object oCONFRONTO_1_32 as StdCheck with uid="JEGLMRCLLH",rtseq=32,rtrep=.f.,left=432, top=293, caption="Confronto es. precedente",;
    ToolTipText = "Confronto con es. precedente",;
    HelpContextID = 234951530,;
    cFormVar="w_CONFRONTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFRONTO_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFRONTO_1_32.GetRadio()
    this.Parent.oContained.w_CONFRONTO = this.RadioValue()
    return .t.
  endfunc

  func oCONFRONTO_1_32.SetRadio()
    this.Parent.oContained.w_CONFRONTO=trim(this.Parent.oContained.w_CONFRONTO)
    this.value = ;
      iif(this.Parent.oContained.w_CONFRONTO=='S',1,;
      0)
  endfunc

  func oCONFRONTO_1_32.mHide()
    with this.Parent.oContained
      return (!(empty(.w_NOTE)))
    endwith
  endfunc


  add object oARROT_1_33 as StdCombo with uid="GKMTMDLFLT",rtseq=33,rtrep=.f.,left=117,top=316,width=151,height=21;
    , ToolTipText = "Arrotonda importi";
    , HelpContextID = 14003962;
    , cFormVar="w_ARROT",RowSource=""+"Importi originali,"+"Importi arrotondati,"+"Importi troncati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARROT_1_33.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARROT_1_33.GetRadio()
    this.Parent.oContained.w_ARROT = this.RadioValue()
    return .t.
  endfunc

  func oARROT_1_33.SetRadio()
    this.Parent.oContained.w_ARROT=trim(this.Parent.oContained.w_ARROT)
    this.value = ;
      iif(this.Parent.oContained.w_ARROT=='N',1,;
      iif(this.Parent.oContained.w_ARROT=='S',2,;
      iif(this.Parent.oContained.w_ARROT=='T',3,;
      0)))
  endfunc


  add object oARRVOCI_1_34 as StdCombo with uid="TBIRGUDVTT",rtseq=34,rtrep=.f.,left=276,top=316,width=131,height=21;
    , ToolTipText = "Arrotonda o tronca voci di bilancio oppure dettaglio voci";
    , HelpContextID = 236891898;
    , cFormVar="w_ARRVOCI",RowSource=""+"Voci,"+"Dettaglio voci", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARRVOCI_1_34.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oARRVOCI_1_34.GetRadio()
    this.Parent.oContained.w_ARRVOCI = this.RadioValue()
    return .t.
  endfunc

  func oARRVOCI_1_34.SetRadio()
    this.Parent.oContained.w_ARRVOCI=trim(this.Parent.oContained.w_ARRVOCI)
    this.value = ;
      iif(this.Parent.oContained.w_ARRVOCI=='V',1,;
      iif(this.Parent.oContained.w_ARRVOCI=='M',2,;
      0))
  endfunc

  func oARRVOCI_1_34.mHide()
    with this.Parent.oContained
      return (.w_ARROT='N')
    endwith
  endfunc

  add object oUSO_1_35 as StdCheck with uid="SOOTCKCJAW",rtseq=35,rtrep=.f.,left=116, top=293, caption="Uso esterno",;
    ToolTipText = "Specifica se la stampa � ad uso interno o esterno",;
    HelpContextID = 107273402,;
    cFormVar="w_USO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUSO_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUSO_1_35.GetRadio()
    this.Parent.oContained.w_USO = this.RadioValue()
    return .t.
  endfunc

  func oUSO_1_35.SetRadio()
    this.Parent.oContained.w_USO=trim(this.Parent.oContained.w_USO)
    this.value = ;
      iif(this.Parent.oContained.w_USO=='S',1,;
      0)
  endfunc


  add object oORIPREC_1_36 as StdCombo with uid="AEGWSZGINK",rtseq=36,rtrep=.f.,left=432,top=316,width=163,height=21;
    , ToolTipText = "I dati estratti per esercizio precedente verranno presi dall'origine indicata";
    , HelpContextID = 67813862;
    , cFormVar="w_ORIPREC",RowSource=""+"Saldi esercizio,"+"Movimenti di primanota,"+"Saldi bilancio UE", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORIPREC_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oORIPREC_1_36.GetRadio()
    this.Parent.oContained.w_ORIPREC = this.RadioValue()
    return .t.
  endfunc

  func oORIPREC_1_36.SetRadio()
    this.Parent.oContained.w_ORIPREC=trim(this.Parent.oContained.w_ORIPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ORIPREC=='S',1,;
      iif(this.Parent.oContained.w_ORIPREC=='M',2,;
      iif(this.Parent.oContained.w_ORIPREC=='B',3,;
      0)))
  endfunc

  func oORIPREC_1_36.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S' OR .w_CONFRONTO<>'S' OR g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oDETCON_1_37 as StdCheck with uid="AIAMWDBXVF",rtseq=37,rtrep=.f.,left=276, top=272, caption="Dettaglio voci",;
    ToolTipText = "Stampa l'elenco dei componenti della voce",;
    HelpContextID = 214852662,;
    cFormVar="w_DETCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETCON_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDETCON_1_37.GetRadio()
    this.Parent.oContained.w_DETCON = this.RadioValue()
    return .t.
  endfunc

  func oDETCON_1_37.SetRadio()
    this.Parent.oContained.w_DETCON=trim(this.Parent.oContained.w_DETCON)
    this.value = ;
      iif(this.Parent.oContained.w_DETCON=='S',1,;
      0)
  endfunc

  func oDETCON_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (.w_CONFRONTO='S' AND .w_EXCEL='S')  AND (.w_ARROT='N' OR .w_ARRVOCI='M'))
    endwith
   endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="NDASJAFEBO",left=579, top=344, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 251761686;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSCG_BBG(this.Parent.oContained,1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_monete='c' or ((not empty(.w_DIVISA))and(not empty(.w_CAMVAL)))) and iif(g_APPLICATION = "ad hoc ENTERPRISE",not empty(.w_BILANCIO), .t.))
      endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="NLMNHXMBPQ",left=632, top=344, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 251761686;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBUDESCRI_1_51 as StdField with uid="YIIXBMVRTI",rtseq=40,rtrep=.f.,;
    cFormVar = "w_BUDESCRI", cQueryName = "BUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34567263,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=166, Top=217, InputMask=replicate('X',35)

  func oBUDESCRI_1_51.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oCAMVAL_1_54 as StdField with uid="KVTIQCJDFU",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 167833638,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=501, Top=161, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
    endwith
   endif
  endfunc

  add object oStr_1_40 as StdString with uid="BPUEMPCWXC",Visible=.t., Left=413, Top=132,;
    Alignment=1, Width=20, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="SSOHJFPBMQ",Visible=.t., Left=19, Top=6,;
    Alignment=1, Width=95, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="WFFEHCMRQG",Visible=.t., Left=257, Top=161,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="XOYYWBOZII",Visible=.t., Left=483, Top=9,;
    Alignment=1, Width=63, Height=15,;
    Caption="Bilancio:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="VMCBWEBFFB",Visible=.t., Left=15, Top=247,;
    Alignment=1, Width=99, Height=15,;
    Caption="Gruppo B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="ALDRSKHBRW",Visible=.t., Left=5, Top=161,;
    Alignment=1, Width=109, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="VYHUHVRQQI",Visible=.t., Left=444, Top=161,;
    Alignment=1, Width=55, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="PUKCKPISOE",Visible=.t., Left=19, Top=132,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="ADKVKJVYMA",Visible=.t., Left=253, Top=132,;
    Alignment=1, Width=78, Height=15,;
    Caption="Periodo dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="EYGEZFSUTW",Visible=.t., Left=19, Top=217,;
    Alignment=1, Width=95, Height=15,;
    Caption="B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="TSHJITFASN",Visible=.t., Left=19, Top=190,;
    Alignment=1, Width=95, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="EVCJSTUCGA",Visible=.t., Left=19, Top=317,;
    Alignment=1, Width=95, Height=15,;
    Caption="Arrotondamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="NNDEPXVVFG",Visible=.t., Left=178, Top=8,;
    Alignment=1, Width=135, Height=18,;
    Caption="Origine eser. attuale:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sbg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
