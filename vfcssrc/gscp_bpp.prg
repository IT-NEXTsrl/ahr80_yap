* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bpp                                                        *
*              Memorizza release CPZ e SSFA                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-13                                                      *
* Last revis.: 2005-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bpp",oParentObject)
return(i_retval)

define class tgscp_bpp as StdBatch
  * --- Local variables
  * --- WorkFile variables
  ZACTKEY_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna tabella ZACTKEY per memorizzazione release CPZ e SSFA
    * --- Prova inserimento poi aggiorna
    this.oParentObject.W_ZACTKEY = IIF(EMPTY(NVL(this.oParentObject.w_ZACTKEY," ")), "0000000001" , this.oParentObject.w_ZACTKEY )
    * --- Try
    local bErr_03C40F70
    bErr_03C40F70=bTrsErr
    this.Try_03C40F70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into ZACTKEY
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ZACTKEY_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZACTKEY_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ZACTKEY_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MONUMREL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MONUMREL),'ZACTKEY','MONUMREL');
        +",MORELCPZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MORELCPZ),'ZACTKEY','MORELCPZ');
            +i_ccchkf ;
        +" where ";
            +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.W_ZACTKEY);
               )
      else
        update (i_cTable) set;
            MONUMREL = this.oParentObject.w_MONUMREL;
            ,MORELCPZ = this.oParentObject.w_MORELCPZ;
            &i_ccchkf. ;
         where;
            MOCODICE = this.oParentObject.W_ZACTKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_03C40F70
    * --- End
    * --- Aggiorna variabili globali
    this.oParentObject.w_MORELCPZ = IIF( Val(STRTRAN(this.oParentObject.w_MORELCPZ,".",","))>=1.1, this.oParentObject.w_MORELCPZ, "1.1")
    this.oParentObject.w_MONUMREL = IIF( Val(STRTRAN(this.oParentObject.w_MONUMREL,".",","))>=1.1, this.oParentObject.w_MONUMREL, "1.1")
    g_ZRELSSFA = IIF(Empty(this.oParentObject.w_MONUMREL), "1.1", this.oParentObject.w_MONUMREL)
    g_ZRELCPZ = IIF(Empty(this.oParentObject.w_MORELCPZ), "1.1", this.oParentObject.w_MORELCPZ)
  endproc
  proc Try_03C40F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ZACTKEY
    i_nConn=i_TableProp[this.ZACTKEY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZACTKEY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZACTKEY_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOCODICE"+",MONUMREL"+",MORELCPZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.W_ZACTKEY),'ZACTKEY','MOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MONUMREL),'ZACTKEY','MONUMREL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MORELCPZ),'ZACTKEY','MORELCPZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.oParentObject.W_ZACTKEY,'MONUMREL',this.oParentObject.w_MONUMREL,'MORELCPZ',this.oParentObject.w_MORELCPZ)
      insert into (i_cTable) (MOCODICE,MONUMREL,MORELCPZ &i_ccchkf. );
         values (;
           this.oParentObject.W_ZACTKEY;
           ,this.oParentObject.w_MONUMREL;
           ,this.oParentObject.w_MORELCPZ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ZACTKEY'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
