* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkg                                                        *
*              Gestione utenti e gruppi struttura aziendale                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-02                                                      *
* Last revis.: 2011-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkg",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bkg as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_PADRE = .NULL.
  w_MARKPOS = 0
  w_OLDAREA = space(10)
  w_GRUATT = space(5)
  w_PERATT = space(5)
  w_AGGIORNA = .f.
  w_CPROWORD = 0
  w_DPGRUPRE = space(5)
  w_SRTIPCOL = space(1)
  * --- WorkFile variables
  STRU_RIS_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OLDAREA = SELECT()
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="CUGRU"
        SELECT(this.oParentObject.w_GRUPPI.cCursor)
        this.w_MARKPOS = RECNO()
        COUNT FOR XCHK=1 TO this.oParentObject.w_NRGRUSEL
        GO this.w_MARKPOS
        this.oParentObject.w_FLGRUPRE = IIF(this.oParentObject.w_NRGRUSEL=1, this.oParentObject.w_FLGRUPRE, .F.)
        this.w_PADRE.mCalc()     
        this.w_PADRE.mEnableControls()     
      case this.pOPER=="CUPER"
        SELECT(this.oParentObject.w_PERSONE.cCursor)
        this.w_MARKPOS = RECNO()
        COUNT FOR XCHK=1 TO this.oParentObject.w_NRPERSEL
        GO this.w_MARKPOS
        this.oParentObject.w_FLRESP = IIF(this.oParentObject.w_NRPERSEL>0, this.oParentObject.w_FLRESP, .F.)
        this.w_PADRE.mCalc()     
        this.w_PADRE.mEnableControls()     
      case this.pOPER=="SELPE" OR this.pOPER=="DESPE" OR this.pOPER=="INVPE"
        UPDATE (this.oParentObject.w_PERSONE.cCursor) SET XCHK=ICASE(this.pOPER=="SELPE", 1, this.pOPER=="DESPE", 0, IIF(XCHK=1,0,1))
        this.w_PADRE.NotifyEvent("ModZoomPersone")     
      case this.pOPER=="SELGR" OR this.pOPER=="DESGR" OR this.pOPER=="INVGR"
        UPDATE (this.oParentObject.w_GRUPPI.cCursor) SET XCHK=ICASE(this.pOPER=="SELGR", 1, this.pOPER=="DESGR", 0, IIF(XCHK=1,0,1))
        this.w_PADRE.NotifyEvent("ModZoomGruppi")     
      case this.pOPER=="AGGIO"
        SELECT(this.oParentObject.w_GRUPPI.cCursor)
        GO TOP
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_037FF710
        bErr_037FF710=bTrsErr
        this.Try_037FF710()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante l'associazione persone/gruppi%0%1", 64, .F., MESSAGE())
        endif
        bTrsErr=bTrsErr or bErr_037FF710
        * --- End
    endcase
    if !EMPTY(this.w_OLDAREA)
      SELECT(this.w_OLDAREA)
    endif
  endproc
  proc Try_037FF710()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SCAN FOR XCHK=1
    this.w_GRUATT = DPCODICE
    this.w_CPROWORD = 0
    * --- Select from STRU_RIS
    i_nConn=i_TableProp[this.STRU_RIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRU_RIS_idx,2],.t.,this.STRU_RIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" STRU_RIS ";
          +" where SRCODICE="+cp_ToStrODBC(this.w_GRUATT)+"";
           ,"_Curs_STRU_RIS")
    else
      select MAX(CPROWORD) AS CPROWORD from (i_cTable);
       where SRCODICE=this.w_GRUATT;
        into cursor _Curs_STRU_RIS
    endif
    if used('_Curs_STRU_RIS')
      select _Curs_STRU_RIS
      locate for 1=1
      do while not(eof())
      this.w_CPROWORD = NVL(_Curs_STRU_RIS.CPROWORD, 0)
        select _Curs_STRU_RIS
        continue
      enddo
      use
    endif
    SELECT(this.oParentObject.w_PERSONE.cCursor)
    GO TOP
    SCAN FOR XCHK=1
    this.w_PERATT = DPCODICE
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_SRTIPCOL = DPTIPRIS
    this.w_AGGIORNA = .F.
    * --- Try
    local bErr_03A22CC0
    bErr_03A22CC0=bTrsErr
    this.Try_03A22CC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      this.w_CPROWORD = this.w_CPROWORD - 10
      * --- Write into STRU_RIS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.STRU_RIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STRU_RIS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.STRU_RIS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SRFLRESP ="+cp_NullLink(cp_ToStrODBC(IIF(this.oParentObject.w_FLRESP And this.w_SRTIPCOL="P", "S", "N")),'STRU_RIS','SRFLRESP');
            +i_ccchkf ;
        +" where ";
            +"SRCODICE = "+cp_ToStrODBC(this.w_GRUATT);
            +" and SRRISCOL = "+cp_ToStrODBC(this.w_PERATT);
               )
      else
        update (i_cTable) set;
            SRFLRESP = IIF(this.oParentObject.w_FLRESP And this.w_SRTIPCOL="P", "S", "N");
            &i_ccchkf. ;
         where;
            SRCODICE = this.w_GRUATT;
            and SRRISCOL = this.w_PERATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_03A22CC0
    * --- End
    if this.oParentObject.w_FLGRUPRE
      this.w_AGGIORNA = this.oParentObject.w_FLGFORCE
      if !this.w_AGGIORNA
        this.w_DPGRUPRE = ""
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPGRUPRE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_PERATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPGRUPRE;
            from (i_cTable) where;
                DPCODICE = this.w_PERATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_AGGIORNA = EMPTY(NVL(this.w_DPGRUPRE, ""))
      endif
    endif
    if this.w_AGGIORNA and this.w_SRTIPCOL <>"R"
      * --- Nel caso delle risorse non devo valorizzare il gruppo
      * --- Write into DIPENDEN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIPENDEN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DPGRUPRE ="+cp_NullLink(cp_ToStrODBC(this.w_GRUATT),'DIPENDEN','DPGRUPRE');
            +i_ccchkf ;
        +" where ";
            +"DPCODICE = "+cp_ToStrODBC(this.w_PERATT);
               )
      else
        update (i_cTable) set;
            DPGRUPRE = this.w_GRUATT;
            &i_ccchkf. ;
         where;
            DPCODICE = this.w_PERATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    SELECT(this.oParentObject.w_PERSONE.cCursor)
    ENDSCAN
    SELECT(this.oParentObject.w_GRUPPI.cCursor)
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    UPDATE (this.oParentObject.w_PERSONE.cCursor) SET XCHK=0
    UPDATE (this.oParentObject.w_GRUPPI.cCursor) SET XCHK=0
    this.w_PADRE.NotifyEvent("ModZoomPersone")     
    this.w_PADRE.NotifyEvent("ModZoomGruppi")     
    ah_ErrorMsg("Associazione persone/gruppi avvenuta correttamente", 64)
    return
  proc Try_03A22CC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STRU_RIS
    i_nConn=i_TableProp[this.STRU_RIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRU_RIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STRU_RIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SRCODICE"+",CPROWORD"+",SRTIPCOL"+",SRRISCOL"+",SRFLRESP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GRUATT),'STRU_RIS','SRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'STRU_RIS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SRTIPCOL),'STRU_RIS','SRTIPCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERATT),'STRU_RIS','SRRISCOL');
      +","+cp_NullLink(cp_ToStrODBC(IIF(this.oParentObject.w_FLRESP And this.w_SRTIPCOL="P", "S", "N")),'STRU_RIS','SRFLRESP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SRCODICE',this.w_GRUATT,'CPROWORD',this.w_CPROWORD,'SRTIPCOL',this.w_SRTIPCOL,'SRRISCOL',this.w_PERATT,'SRFLRESP',IIF(this.oParentObject.w_FLRESP And this.w_SRTIPCOL="P", "S", "N"))
      insert into (i_cTable) (SRCODICE,CPROWORD,SRTIPCOL,SRRISCOL,SRFLRESP &i_ccchkf. );
         values (;
           this.w_GRUATT;
           ,this.w_CPROWORD;
           ,this.w_SRTIPCOL;
           ,this.w_PERATT;
           ,IIF(this.oParentObject.w_FLRESP And this.w_SRTIPCOL="P", "S", "N");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STRU_RIS'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_STRU_RIS')
      use in _Curs_STRU_RIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
