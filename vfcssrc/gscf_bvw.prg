* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bvw                                                        *
*              Gestione di GSCF_KVW                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-04                                                      *
* Last revis.: 2013-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bvw",oParentObject,m.pOPER)
return(i_retval)

define class tgscf_bvw as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_LOSTAMP = space(10)
  w_IDXCOLORE = .f.
  * --- WorkFile variables
  TMPSTAMP_idx=0
  TMPSTAMP2_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione di GSCF_KVW
    if not empty(this.oParentObject.w_FLDFILTER) and this.pOPER="INIT"
      * --- imposto il filtro sul mio campo
      this.oParentObject.w_zoomdett.addwhere("LOFLDNAM=" + cp_tostrodbc(alltrim(this.oParentObject.w_FLDFILTER)))     
    endif
    if NOT this.oParentObject.w_FROMGEST
      do case
        case this.pOPER="BLANK"
          * --- Create temporary table TMPSTAMP
          if !cp_ExistTableDef('TMPSTAMP')
            i_nIdx=cp_AddTableDef('TMPSTAMP',.t.) && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPSTAMP_proto';
                  )
            this.TMPSTAMP_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            if this.TMPSTAMP_idx=0
              this.TMPSTAMP_idx=cp_ascan(@i_TableProp,lower('TMPSTAMP'),i_nTables)
            endif
          endif
        case this.pOPER="INIT"
          this.oParentObject.NotifyEvent("RequeryD")     
        case this.pOPER="DONE"
          * --- Drop temporary table TMPSTAMP
          i_nIdx=cp_GetTableDefIdx('TMPSTAMP')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPSTAMP')
          endif
        case this.pOPER="REQUERY"
          * --- Create temporary table TMPSTAMP
          if !cp_ExistTableDef('TMPSTAMP')
            i_nIdx=cp_AddTableDef('TMPSTAMP',.t.) && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPSTAMP_proto';
                  )
            this.TMPSTAMP_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            if this.TMPSTAMP_idx=0
              this.TMPSTAMP_idx=cp_ascan(@i_TableProp,lower('TMPSTAMP'),i_nTables)
            endif
          endif
          * --- Delete from TMPSTAMP
          i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"1 = "+cp_ToStrODBC(1);
                   )
          else
            delete from (i_cTable) where;
                  1 = 1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Insert into TMPSTAMP
          i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCF1BVW",this.TMPSTAMP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Elimino le righe di testata che non hanno righe nel dettaglio a causa 
          *     della selezione sul tipo operazione (INS. DEL, REP)
          * --- Create temporary table TMPSTAMP2
          i_nIdx=cp_AddTableDef('TMPSTAMP2') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSCF2BVW',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPSTAMP2_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Delete from TMPSTAMP
          i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"1 = "+cp_ToStrODBC(1);
                   )
          else
            delete from (i_cTable) where;
                  1 = 1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Insert into TMPSTAMP
          i_nConn=i_TableProp[this.TMPSTAMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSTAMP2_idx,2])
            i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TMPSTAMP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPSTAMP2
          i_nIdx=cp_GetTableDefIdx('TMPSTAMP2')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPSTAMP2')
          endif
          this.oParentObject.NotifyEvent("RequeryM")     
          this.oParentObject.mCalc(.t.)     
          this.oParentObject.NotifyEvent("RequeryD")     
      endcase
    endif
    if this.pOPER="COLORE" and used(this.oParentObject.w_zoom.cCursor) and reccount(this.oParentObject.w_zoom.cCursor)>0
      this.w_LOSTAMP = "XXXXXXXXXX"
      SELECT (this.oParentObject.w_zoom.cCursor)
      SCAN
      if this.w_LOSTAMP<>LOSTAMP
        this.w_LOSTAMP = LOSTAMP
        this.w_IDXCOLORE = not this.w_IDXCOLORE
      endif
      REPLACE COLORE WITH IIF(this.w_IDXCOLORE, RGB(128,128,255), RGB(255,255,0))
      ENDSCAN
      GO TOP
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPSTAMP'
    this.cWorkTables[2]='*TMPSTAMP2'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
