* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_btm                                                        *
*              Tracciabilita commessa                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-19                                                      *
* Last revis.: 2016-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ODPSEL,Scelta,w_PDANUM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_btm",oParentObject,m.w_ODPSEL,m.Scelta,m.w_PDANUM)
return(i_retval)

define class tgsac_btm as StdBatch
  * --- Local variables
  w_ODPSEL = space(15)
  Scelta = space(10)
  w_PDANUM = 0
  GestODP = .NULL.
  TMPc = space(10)
  w_OLTCOART = space(20)
  w_OLTPROVE = space(1)
  w_OLTSTATO = space(1)
  w_DPOGGMPS = space(1)
  w_OK = .f.
  w_ODPSEL = space(10)
  * --- WorkFile variables
  ART_PROD_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gestione PDA/ODP 
    *     Da GSCO_KTC GSAC_KTC
    do case
      case len(this.w_ODPSEL)=15
        * --- Legge ODP selezionato
        * --- Per evitare che la lettura di ODL_MAST diretta dia errore se la chiamata della presente routine arriva dalle PDA uso una query. 
        *     E' chiaro che non avr� mai w_ODPSEL di 15 caratteri se arrivo dalle PDA ma mi basta la definizione in tables per avere l'errore.
        vq_exec("QUERY/GSAC_BTM.VQR",this,"_ODL_MAST_")
        * --- restituisce sempre una sola riga
        Select _ODL_MAST_ 
 Go Top
        this.w_OLTCOART = _ODL_MAST_.OLTCOART
        this.w_OLTSTATO = _ODL_MAST_.OLTSTATO
        this.w_OLTPROVE = _ODL_MAST_.OLTPROVE
        if Used("_ODL_MAST_")
           
 Select _ODL_MAST_ 
 Use
        endif
        * --- Definisce e chiama gestione
        do case
          case this.w_OLTPROVE="L"
            * --- OCL
            this.GestODP = GSCO_AOP(this.oParentObject, "Z")
          case this.w_OLTPROVE="I"
            * --- ODL
            this.GestODP = GSCO_AOP(this.oParentObject, "L")
          otherwise
            * --- ODA
            this.GestODP = GSCO_AOP(this.oParentObject, "E")
        endcase
        this.GestODP.w_OLCODODL = this.w_ODPSEL
        this.TMPc = "OLCODODL="+cp_ToStrODBC(this.w_ODPSEL)
        this.w_OK = TRUE
      case len(this.w_ODPSEL)=10 and type("this.w_PDANUM")="N" and this.w_PDANUM>0
        * --- Gestione PDA
        this.GestODP = GSAC_APD()
        this.GestODP.w_PDSERIAL = this.w_ODPSEL
        this.GestODP.w_PDROWNUM = this.w_PDANUM
        this.TMPc = "PDSERIAL="+cp_ToStrODBC(this.w_ODPSEL)+" and PDROWNUM="+cp_ToStrODBC(this.w_PDANUM)
        this.w_OK = TRUE
    endcase
    do case
      case this.w_OK AND (this.Scelta="VISUALIZZA_ODP" or this.Scelta="MODIFICA_ODP" or this.Scelta="ELIMINA_ODP")
        * --- Carica record e lascia in interrograzione ...
        this.GestODP.QueryKeySet(this.TmpC,"")     
        this.GestODP.LoadRecWarn()     
        do case
          case this.Scelta = "MODIFICA_ODP"
            * --- Modifica record
            this.GestODP.ECPEdit()     
          case this.Scelta = "ELIMINA_ODP"
            * --- Cancella record ...
            this.GestODP.ECPDelete()     
        endcase
    endcase
  endproc


  proc Init(oParentObject,w_ODPSEL,Scelta,w_PDANUM)
    this.w_ODPSEL=w_ODPSEL
    this.Scelta=Scelta
    this.w_PDANUM=w_PDANUM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ART_PROD'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='CONTI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ODPSEL,Scelta,w_PDANUM"
endproc
