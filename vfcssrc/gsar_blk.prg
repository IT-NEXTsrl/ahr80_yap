* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_blk                                                        *
*              Controllo disponibilitÓ lotti                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_40]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-22                                                      *
* Last revis.: 2012-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoTransaction,w_PADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_blk",oParentObject,m.pNoTransaction,m.w_PADRE)
return(i_retval)

define class tgsar_blk as StdBatch
  * --- Local variables
  pNoTransaction = space(1)
  w_PADRE = .NULL.
  w_SERIALD = space(10)
  w_SERIALM = space(10)
  w_CODART = space(20)
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_CODMAG = space(5)
  w_DISP = 0
  w_NROW = space(5)
  w_FLDISP = space(1)
  w_QTAESI = 0
  w_DISLOT = space(1)
  w_ANNULLA = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_DAIM = .f.
  w_OK = .f.
  w_SUQTAPER = 0
  w_SUQTRPER = 0
  w_CODMAT = space(5)
  w_MVFLCASC = space(1)
  w_FLDEL = .f.
  w_TERR = 0
  w_QTAMSG = 0
  w_LMESS = space(200)
  * --- WorkFile variables
  DOC_DETT_idx=0
  SALDIART_idx=0
  ART_ICOL_idx=0
  SALDILOT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check DisponibilitÓ articoli
    *     =================================
    *     
    *     Recupero dal transitorio i dati relativi alla gestione saldi di magazzino da stornare...
    *     Per queste operazioni considero tutte le righe modificate, quindi anche quelle
    *     cancellate...
    * --- Se passato a 'S' controlli fuori transazione (Msg con conferma per tutti gli
    *     articoli sotto diposnibilitÓ pressenti) altrimenti
    *     controlli solo per articoli con blocco disponibilita' senza conferma, messaggio 
    *     proposto solo per il primo (Transazione abbandonata)
    * --- Gestione che invoca il metodo (gestione che ha un transitorio...)
    if this.pNoTransaction <>"S"
      this.w_OK = .T.
      do case
        case Upper ( this.w_PADRE.Class )= "TGSOR_MDV" Or Upper ( this.w_PADRE.Class )= "TGSVE_MDV" or Upper ( this.w_PADRE.Class )= "TGSAC_MDV" 
          this.w_SERIALM = this.w_PADRE.w_MVSERIAL
          * --- Scorro il transitorio e riga per riga verifico la disponibilitÓ sul database...
          * --- Select from GSMDM3BCL
          do vq_exec with 'GSMDM3BCL',this,'_Curs_GSMDM3BCL','',.f.,.t.
          if used('_Curs_GSMDM3BCL')
            select _Curs_GSMDM3BCL
            locate for 1=1
            do while not(eof())
            this.w_CODART = KEYSAL
            this.w_CODMAG = CODMAG
            this.w_CODLOT = Nvl(CODLOT,Space(20))
            this.w_CODUBI = Nvl(CODUBI,Space(20))
            this.w_DISP = Nvl ( QTAESI, 0 )
            this.w_DISLOT = Nvl(ARDISLOT,Space(1))
            if this.w_DISLOT $ "SC"
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if ! this.w_OK
              Exit
            endif
              select _Curs_GSMDM3BCL
              continue
            enddo
            use
          endif
          * --- Controllo sui record cancellati
          this.w_FLDEL = this.w_PADRE.cFunction="Query"
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          * --- Controllo righe non cancellate
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos(.F.)     
          if this.w_OK
            this.w_PADRE.MarkPos()     
            this.w_PADRE.FirstRowDel()     
            * --- Secondo giro sulle eliminate
            do while Not this.w_PADRE.Eof_Trs()
              this.w_PADRE.SetRow()     
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.NextRowDel()     
            enddo
            this.w_PADRE.RePos(.F.)     
          endif
        case Upper ( this.w_PADRE.Class )= "TGSMA_MVM" 
          this.w_SERIALM = this.w_PADRE.w_MMSERIAL
          * --- Scorro il transitorio e riga per riga verifico la disponibilitÓ sul database...
          * --- Select from GSMDD3BCL
          do vq_exec with 'GSMDD3BCL',this,'_Curs_GSMDD3BCL','',.f.,.t.
          if used('_Curs_GSMDD3BCL')
            select _Curs_GSMDD3BCL
            locate for 1=1
            do while not(eof())
            this.w_CODART = KEYSAL
            this.w_CODMAG = CODMAG
            this.w_CODLOT = Nvl(CODLOT,Space(20))
            this.w_CODUBI = Nvl(CODUBI,Space(20))
            this.w_DISP = Nvl ( QTAESI, 0 )
            this.w_DISLOT = Nvl(ARDISLOT,Space(1))
            if this.w_DISLOT $ "SC"
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if ! this.w_OK
              Exit
            endif
              select _Curs_GSMDD3BCL
              continue
            enddo
            use
          endif
          * --- Controllo sui record cancellati  o modificati
          this.w_FLDEL = this.w_PADRE.cFunction="Query"
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          * --- Controllo righe non cancellate
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos(.F.)     
          if this.w_OK
            this.w_PADRE.MarkPos()     
            this.w_PADRE.FirstRowDel()     
            * --- Secondo giro sulle eliminate
            do while Not this.w_PADRE.Eof_Trs()
              this.w_PADRE.SetRow()     
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.NextRowDel()     
            enddo
            this.w_PADRE.RePos(.F.)     
          endif
        case Upper ( this.w_PADRE.Class )= "TGSPS_MVD" 
          this.w_SERIALM = this.w_PADRE.w_MDSERIAL
          * --- Scorro il transitorio e riga per riga verifico la disponibilitÓ sul database...
          * --- Select from GSMDV2BCL
          do vq_exec with 'GSMDV2BCL',this,'_Curs_GSMDV2BCL','',.f.,.t.
          if used('_Curs_GSMDV2BCL')
            select _Curs_GSMDV2BCL
            locate for 1=1
            do while not(eof())
            this.w_CODART = KEYSAL
            this.w_CODMAG = CODMAG
            this.w_CODLOT = Nvl(CODLOT,Space(20))
            this.w_CODUBI = Nvl(CODUBI,Space(20))
            this.w_DISP = Nvl ( QTAESI, 0 )
            this.w_DISLOT = Nvl(ARDISLOT,Space(1))
            if this.w_DISLOT $ "SC"
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if ! this.w_OK
              Exit
            endif
              select _Curs_GSMDV2BCL
              continue
            enddo
            use
          endif
          * --- Controllo sui record cancellati
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          * --- Test Se riga Eliminata
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            if this.w_PADRE.RowStatus()="D" OR this.w_PADRE.RowStatus()="U" 
              this.w_CODART = this.w_PADRE.w_MDCODART
              this.w_CODMAG = this.w_PADRE.w_MDCODMAG
              this.w_CODLOT = Nvl(this.w_PADRE.w_MDCODLOT,Space(20))
              this.w_CODUBI = Nvl(this.w_PADRE.w_MDCODUBI,Space(20))
              this.w_DISLOT = Nvl(this.w_PADRE.w_DISLOT,Space(1))
              this.w_MVFLCASC = this.w_PADRE.w_MDFLCASC
              * --- Se il record Ŕ stato modificato verifico anche il slado del record originario
              if this.w_PADRE.RowStatus()="U"
                this.w_CODART = this.w_PADRE.GET ("MDCODART")
                this.w_CODMAG = this.w_PADRE.GET ("MDLOTMAG")
                this.w_CODLOT = Nvl(this.w_PADRE.GET ("MDCODLOT"),Space(20))
                this.w_CODUBI = Nvl(this.w_PADRE.GET ("MDCODUBI"),Space(20))
              endif
              if NOT EMPTY (this.w_CODLOT) AND NOT EMPTY (this.w_CODMAG) AND this.w_MVFLCASC="+"
                * --- Read from SALDILOT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.SALDILOT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "SUQTAPER,SUQTRPER"+;
                    " from "+i_cTable+" SALDILOT where ";
                        +"SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                        +" and SUCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                        +" and SUCODART = "+cp_ToStrODBC(this.w_CODART);
                        +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    SUQTAPER,SUQTRPER;
                    from (i_cTable) where;
                        SUCODLOT = this.w_CODLOT;
                        and SUCODMAG = this.w_CODMAG;
                        and SUCODART = this.w_CODART;
                        and SUCODUBI = this.w_CODUBI;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
                  this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_DISP = this.w_SUQTAPER - this.w_SUQTRPER
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if ! this.w_OK
                Exit
              endif
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos(.F.)     
      endcase
      * --- Sommo il risultato in un unico trs...
      if ! Empty (this.w_MESBLOK) And this.w_DISLOT="S"
        * --- Notifico al batch chiamante l'annullo della transazione 
        *     solo se controllo DisponibilitÓ = Si 
        *     Nel casi di Si con Conferma il messaggio viene dato in questo batch
        this.oParentObject.w_MESS = this.w_MESBLOK
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipo errore
    *     0 = ok
    *     1 = Bloccante (Esistenza)
    this.w_TERR = 0
    this.w_QTAMSG = 0
    * --- Verifico se articolo Ok...
    if this.w_DISP < 0
      this.w_QTAMSG = this.w_DISP
      this.w_TERR = 1
    endif
    if this.w_TERR<>0 and this.w_DISLOT $ "SC"
      do case
        case Not Empty(this.w_CODLOT) AND Not Empty(this.w_CODUBI)
          this.w_LMESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0DisponibilitÓ lotto: %3%0Ubicazione: %4%0DisponibilitÓ contabile articolo negativa (%5)%0", ALLTR(this.w_CODART), this.w_CODMAG, ALLTRIM(this.w_CODLOT), ALLTRIM(this.w_CODUBI), TRAN(this.w_QTAMSG,"@Z"+v_PQ(12)))
        case Not Empty(this.w_CODLOT)
          this.w_LMESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0DisponibilitÓ lotto: %3%0DisponibilitÓ contabile articolo negativa (%4)%0", ALLTR(this.w_CODART), this.w_CODMAG, ALLTRIM(this.w_CODLOT), TRAN(this.w_QTAMSG,"@Z"+v_PQ(12)))
        case Not Empty(this.w_CODUBI)
          this.w_LMESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0Ubicazione: %3%0DisponibilitÓ contabile articolo negativa (%4)%0", ALLTR(this.w_CODART), this.w_CODMAG, ALLTRIM(this.w_CODUBI), TRAN(this.w_QTAMSG,"@Z"+v_PQ(12)))
        otherwise
          this.w_LMESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0DisponibilitÓ contabile articolo negativa (%3)%0", ALLTR(this.w_CODART), this.w_CODMAG, TRAN(this.w_QTAMSG,"@Z"+v_PQ(12)))
      endcase
      if this.w_DISLOT<>"S"
        this.w_LMESS = this.w_LMESS + ah_MsgFormat("Confermi ugualmente?")
        this.w_OK = ah_YesNo(this.w_LMESS)
      else
        * --- Bloccante
        this.w_MESBLOK = this.w_MESBLOK + IIF(EMPTY(this.w_MESBLOK),"", Chr(13)) + this.w_LMESS
        this.w_OK = .F.
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.RowStatus()="D" OR this.w_PADRE.RowStatus()="U" or this.w_FLDEL
      this.w_CODART = this.w_PADRE.w_MMKEYSAL
      this.w_CODMAG = this.w_PADRE.w_MMCODMAG
      this.w_CODMAT = this.w_PADRE.w_MMCODMAT
      this.w_CODLOT = Nvl(this.w_PADRE.w_MMCODLOT,Space(20))
      this.w_CODUBI = Nvl(this.w_PADRE.w_MMCODUBI,Space(20))
      this.w_DISLOT = Nvl(this.w_PADRE.w_DISLOT,Space(1))
      this.w_MVFLCASC = this.w_PADRE.w_MMFLCASC
      * --- Se il record Ŕ stato modificato verifico anche il slado del record originario
      if this.w_PADRE.RowStatus()="U"
        this.w_CODART = this.w_PADRE.GET ("MMKEYSAL")
        this.w_CODMAG = this.w_PADRE.GET ("MMCODMAG")
        this.w_CODMAT = this.w_PADRE.GET ("MMCODMAT")
        this.w_CODLOT = Nvl( this.w_PADRE.GET ("MMCODLOT"),Space(20))
        this.w_CODUBI = Nvl(this.w_PADRE.GET ("MMCODUBI"),Space(20))
      endif
      if NOT EMPTY (this.w_CODLOT) AND NOT EMPTY (this.w_CODMAG) AND this.w_MVFLCASC="+"
        * --- Read from SALDILOT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SUQTAPER,SUQTRPER"+;
            " from "+i_cTable+" SALDILOT where ";
                +"SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                +" and SUCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SUCODART = "+cp_ToStrODBC(this.w_CODART);
                +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SUQTAPER,SUQTRPER;
            from (i_cTable) where;
                SUCODLOT = this.w_CODLOT;
                and SUCODMAG = this.w_CODMAG;
                and SUCODART = this.w_CODART;
                and SUCODUBI = this.w_CODUBI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
          this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DISP = this.w_SUQTAPER - this.w_SUQTRPER
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_MVFLCASC = this.w_PADRE.w_MMF2CASC
      if NOT EMPTY (this.w_CODLOT) AND NOT EMPTY (this.w_CODMAT) AND this.w_MVFLCASC="+"
        this.w_CODMAG = this.w_PADRE.w_MMCODMAT
        this.w_CODUBI = Nvl(this.w_PADRE.w_MMCODUB2,Space(20))
        * --- Se il record Ŕ stato modificato verifico anche il slado del record originario
        if this.w_PADRE.RowStatus()="U"
          this.w_CODMAG = this.w_PADRE.GET ("MMCODMAT")
          this.w_CODUBI = Nvl(this.w_PADRE.GET ("MMCODUB2"),Space(20))
        endif
        * --- Read from SALDILOT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SUQTAPER,SUQTRPER"+;
            " from "+i_cTable+" SALDILOT where ";
                +"SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                +" and SUCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SUCODART = "+cp_ToStrODBC(this.w_CODART);
                +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SUQTAPER,SUQTRPER;
            from (i_cTable) where;
                SUCODLOT = this.w_CODLOT;
                and SUCODMAG = this.w_CODMAG;
                and SUCODART = this.w_CODART;
                and SUCODUBI = this.w_CODUBI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
          this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DISP = this.w_SUQTAPER - this.w_SUQTRPER
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if ! this.w_OK
        Exit
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.RowStatus()="D" OR this.w_PADRE.RowStatus()="U" or this.w_FLDEL
      this.w_CODART = this.w_PADRE.w_MVKEYSAL
      this.w_CODMAG = this.w_PADRE.w_MVCODMAG
      this.w_CODMAT = this.w_PADRE.w_MVCODMAT
      this.w_CODLOT = Nvl(this.w_PADRE.w_MVCODLOT,Space(20))
      this.w_CODUBI = Nvl(this.w_PADRE.w_MVCODUBI,Space(20))
      this.w_DISLOT = Nvl(this.w_PADRE.w_DISLOT,Space(1))
      this.w_MVFLCASC = this.w_PADRE.w_MVFLCASC
      * --- Se il record Ŕ stato modificato verifico anche il slado del record originario
      if this.w_PADRE.RowStatus()="U"
        this.w_CODLOT = Nvl(this.w_PADRE.GET ("MVCODLOT"),Space(20))
        this.w_CODMAG = this.w_PADRE.GET ("MVCODMAG")
        this.w_CODMAT = this.w_PADRE.GET ("MVCODMAT")
        this.w_CODUBI = Nvl(this.w_PADRE.GET ("MVCODUBI"),Space(20))
        this.w_CODART = this.w_PADRE.GET ("MVKEYSAL")
      endif
      if NOT EMPTY (this.w_CODLOT) AND NOT EMPTY (this.w_CODMAG) AND this.w_MVFLCASC="+"
        * --- Read from SALDILOT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SUQTAPER,SUQTRPER"+;
            " from "+i_cTable+" SALDILOT where ";
                +"SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                +" and SUCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SUCODART = "+cp_ToStrODBC(this.w_CODART);
                +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SUQTAPER,SUQTRPER;
            from (i_cTable) where;
                SUCODLOT = this.w_CODLOT;
                and SUCODMAG = this.w_CODMAG;
                and SUCODART = this.w_CODART;
                and SUCODUBI = this.w_CODUBI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
          this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DISP = this.w_SUQTAPER - this.w_SUQTRPER
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_MVFLCASC = this.w_PADRE.w_MVF2CASC
      if NOT EMPTY (this.w_CODLOT) AND NOT EMPTY (this.w_CODMAT) AND this.w_MVFLCASC="+"
        this.w_CODMAG = this.w_PADRE.w_MVCODMAT
        this.w_CODUBI = Nvl(this.w_PADRE.w_MVCODUB2,Space(20))
        * --- Se il record Ŕ stato modificato verifico anche il slado del record originario
        if this.w_PADRE.RowStatus()="U"
          this.w_CODMAG = this.w_PADRE.GET ("MVCODMAT")
          this.w_CODUBI = Nvl(this.w_PADRE.GET ("MVCODUB2"),Space(20))
        endif
        * --- Read from SALDILOT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SUQTAPER,SUQTRPER"+;
            " from "+i_cTable+" SALDILOT where ";
                +"SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
                +" and SUCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SUCODART = "+cp_ToStrODBC(this.w_CODART);
                +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SUQTAPER,SUQTRPER;
            from (i_cTable) where;
                SUCODLOT = this.w_CODLOT;
                and SUCODMAG = this.w_CODMAG;
                and SUCODART = this.w_CODART;
                and SUCODUBI = this.w_CODUBI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
          this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DISP = this.w_SUQTAPER - this.w_SUQTRPER
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if ! this.w_OK
        Exit
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pNoTransaction,w_PADRE)
    this.pNoTransaction=pNoTransaction
    this.w_PADRE=w_PADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='SALDILOT'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSMDM3BCL')
      use in _Curs_GSMDM3BCL
    endif
    if used('_Curs_GSMDD3BCL')
      use in _Curs_GSMDD3BCL
    endif
    if used('_Curs_GSMDV2BCL')
      use in _Curs_GSMDV2BCL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoTransaction,w_PADRE"
endproc
