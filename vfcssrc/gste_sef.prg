* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sef                                                        *
*              Stampa effetti/R.B.                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_22]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2011-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sef",oParentObject))

* --- Class definition
define class tgste_sef as StdForm
  Top    = 10
  Left   = 15

  * --- Standard Properties
  Width  = 622
  Height = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-07-08"
  HelpContextID=57291881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  DIS_TINT_IDX = 0
  cPrg = "gste_sef"
  cComment = "Stampa effetti/R.B."
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATSTA = ctod('  /  /  ')
  w_FLEURO = space(1)
  w_FLSTA = space(2)
  w_NUMERO = 0
  w_ANNO = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_DADATA = ctod('  /  /  ')
  w_NUMDISM = space(10)
  w_NUMERO1 = 0
  w_ANNO1 = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DESCRI1 = space(35)
  w_ZOOMEF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sefPag1","gste_sef",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATSTA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMEF = this.oPgFrm.Pages(1).oPag.ZOOMEF
    DoDefault()
    proc Destroy()
      this.w_ZOOMEF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIS_TINT'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BEF with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sef
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATSTA=ctod("  /  /  ")
      .w_FLEURO=space(1)
      .w_FLSTA=space(2)
      .w_NUMERO=0
      .w_ANNO=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_DADATA=ctod("  /  /  ")
      .w_NUMDISM=space(10)
      .w_NUMERO1=0
      .w_ANNO1=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DESCRI1=space(35)
        .w_DATSTA = i_datsys
          .DoRTCalc(2,2,.f.)
        .w_FLSTA = 'RB'
          .DoRTCalc(4,6,.f.)
        .w_DADATA = G_INIESE
        .w_NUMDISM = Nvl( .w_zoomef.getvar('DINUMDIS') , Space(10))
        .w_NUMERO1 = Nvl( .w_zoomef.getvar('DINUMERO') , 0 )
      .oPgFrm.Page1.oPag.ZOOMEF.Calculate(.F.)
        .w_ANNO1 = Nvl( .w_zoomef.getvar('DI__ANNO') , Space(4))
        .w_DATA1 = CP_TODATE(.w_zoomef.getvar('DIDATDIS'))
        .w_DESCRI1 = Nvl ( .w_zoomef.getvar('DIDESCRI') , Space( 35 ) )
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
            .w_NUMDISM = Nvl( .w_zoomef.getvar('DINUMDIS') , Space(10))
            .w_NUMERO1 = Nvl( .w_zoomef.getvar('DINUMERO') , 0 )
        .oPgFrm.Page1.oPag.ZOOMEF.Calculate(.F.)
            .w_ANNO1 = Nvl( .w_zoomef.getvar('DI__ANNO') , Space(4))
            .w_DATA1 = CP_TODATE(.w_zoomef.getvar('DIDATDIS'))
            .w_DESCRI1 = Nvl ( .w_zoomef.getvar('DIDESCRI') , Space( 35 ) )
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMEF.Calculate(.F.)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMEF.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_1.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_1.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEURO_1_2.RadioValue()==this.w_FLEURO)
      this.oPgFrm.Page1.oPag.oFLEURO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTA_1_3.RadioValue()==this.w_FLSTA)
      this.oPgFrm.Page1.oPag.oFLSTA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_7.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_7.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_10.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_10.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_11.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_11.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_15.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_15.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO1_1_22.value==this.w_ANNO1)
      this.oPgFrm.Page1.oPag.oANNO1_1_22.value=this.w_ANNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_23.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_23.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_25.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_25.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_sefPag1 as StdContainer
  Width  = 618
  height = 424
  stdWidth  = 618
  stdheight = 424
  resizeXpos=459
  resizeYpos=299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATSTA_1_1 as StdField with uid="AWXNZCCNJI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di presentazione degli effetti",;
    HelpContextID = 53367094,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=386, Top=6

  add object oFLEURO_1_2 as StdCheck with uid="XKNGRAJSLT",rtseq=2,rtrep=.f.,left=483, top=6, caption="Importi in Euro",;
    ToolTipText = "Se attivo: gli importi delle scadenze in valuta EMU saranno stampati convertiti in Euro",;
    HelpContextID = 17787990,;
    cFormVar="w_FLEURO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEURO_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLEURO_1_2.GetRadio()
    this.Parent.oContained.w_FLEURO = this.RadioValue()
    return .t.
  endfunc

  func oFLEURO_1_2.SetRadio()
    this.Parent.oContained.w_FLEURO=trim(this.Parent.oContained.w_FLEURO)
    this.value = ;
      iif(this.Parent.oContained.w_FLEURO=='S',1,;
      0)
  endfunc


  add object oFLSTA_1_3 as StdCombo with uid="RJIAFLXIKR",rtseq=3,rtrep=.f.,left=10,top=36,width=122,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 251704234;
    , cFormVar="w_FLSTA",RowSource=""+"Ricevute bancarie,"+"Cambiali/tratte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSTA_1_3.RadioValue()
    return(iif(this.value =1,'RB',;
    iif(this.value =2,'CA',;
    space(2))))
  endfunc
  func oFLSTA_1_3.GetRadio()
    this.Parent.oContained.w_FLSTA = this.RadioValue()
    return .t.
  endfunc

  func oFLSTA_1_3.SetRadio()
    this.Parent.oContained.w_FLSTA=trim(this.Parent.oContained.w_FLSTA)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTA=='RB',1,;
      iif(this.Parent.oContained.w_FLSTA=='CA',2,;
      0))
  endfunc


  add object oBtn_1_4 as StdButton with uid="BRKXQKPTJT",left=514, top=368, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 57263130;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        do GSTE_BEF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="MWTXFYONXQ",left=564, top=368, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 49974458;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_7 as StdField with uid="SFZVMQMZFM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 16774614,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=216, Top=36, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO_1_8 as StdField with uid="UTNWCDPZJT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento selezionato",;
    HelpContextID = 51773946,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=296, Top=36, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oDATDIS_1_10 as StdField with uid="IKSWUNTAYL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta selezionata",;
    HelpContextID = 74404150,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=386, Top=36

  add object oDADATA_1_11 as StdField with uid="MNBEIRUAQH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtra le distinte a partire da una certa data",;
    HelpContextID = 52121910,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=386, Top=66


  add object oBtn_1_12 as StdButton with uid="PPERWXJIGM",left=564, top=36, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 119630358;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO1_1_15 as StdField with uid="NTISPFXAJL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della distinta selezionata",;
    HelpContextID = 16774614,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=153, Top=368, cSayPict='"999999"', cGetPict='"999999"'


  add object ZOOMEF as cp_zoombox with uid="THAZEEGDAX",left=1, top=93, width=619,height=268,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIS_TINT",cZoomFile="GSTE_ZEF",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",bRetriveAllRows=.f.,bAdvOptions=.t.,cZoomOnZoom="",;
    cEvent = "Esegui,Init",;
    nPag=1;
    , HelpContextID = 120705766

  add object oANNO1_1_22 as StdField with uid="EGMCQYEOIU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANNO1", cQueryName = "ANNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della distinta selezionata",;
    HelpContextID = 393722,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=230, Top=368, InputMask=replicate('X',4)

  add object oDATA1_1_23 as StdField with uid="YAVBNYPEJJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della distinta selezionata",;
    HelpContextID = 1289930,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=332, Top=368

  add object oDESCRI1_1_25 as StdField with uid="ZRWJJBPKLD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della distinta selezionata",;
    HelpContextID = 184436022,;
   bGlobalFont=.t.,;
    Height=21, Width=257, Left=154, Top=391, InputMask=replicate('X',35)

  add object oStr_1_6 as StdString with uid="ULXNXAWJFJ",Visible=.t., Left=292, Top=6,;
    Alignment=1, Width=92, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DEIPEULFWO",Visible=.t., Left=287, Top=36,;
    Alignment=0, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BQCNXYCSRV",Visible=.t., Left=12, Top=368,;
    Alignment=1, Width=139, Height=15,;
    Caption="Distinta selezionata:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="GLQYBWZZEV",Visible=.t., Left=134, Top=36,;
    Alignment=1, Width=80, Height=15,;
    Caption="Distinta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WGZHZDARDN",Visible=.t., Left=350, Top=36,;
    Alignment=1, Width=34, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="NLITWFXOBO",Visible=.t., Left=6, Top=6,;
    Alignment=0, Width=203, Height=15,;
    Caption="Criteri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="QHDMTOUZFB",Visible=.t., Left=222, Top=368,;
    Alignment=0, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="YPLPWENFYT",Visible=.t., Left=280, Top=368,;
    Alignment=1, Width=50, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="SQUXNUWLEN",Visible=.t., Left=325, Top=66,;
    Alignment=1, Width=59, Height=18,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oBox_1_20 as StdBox with uid="KKPQWKYOZK",left=6, top=23, width=227,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sef','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
