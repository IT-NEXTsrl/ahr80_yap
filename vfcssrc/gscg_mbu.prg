* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mbu                                                        *
*              Manutenzione bilancio UE                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-14                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_mbu"))

* --- Class definition
define class tgscg_mbu as StdTrsForm
  Top    = 11
  Left   = 6

  * --- Standard Properties
  Width  = 617
  Height = 381+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=113911145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  BIL_MAST_IDX = 0
  BIL_DETT_IDX = 0
  ESERCIZI_IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  cFile = "BIL_MAST"
  cFileDetail = "BIL_DETT"
  cKeySelect = "SLSERIAL"
  cKeyWhere  = "SLSERIAL=this.w_SLSERIAL"
  cKeyDetail  = "SLFLMACO=this.w_SLFLMACO and SLSERIAL=this.w_SLSERIAL and SLCODCON=this.w_SLCODCON"
  cKeyWhereODBC = '"SLSERIAL="+cp_ToStrODBC(this.w_SLSERIAL)';

  cKeyDetailWhereODBC = '"SLFLMACO="+cp_ToStrODBC(this.w_SLFLMACO)';
      +'+" and SLSERIAL="+cp_ToStrODBC(this.w_SLSERIAL)';
      +'+" and SLCODCON="+cp_ToStrODBC(this.w_SLCODCON)';

  cKeyWhereODBCqualified = '"BIL_DETT.SLSERIAL="+cp_ToStrODBC(this.w_SLSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'BIL_DETT.CPROWORD '
  cPrg = "gscg_mbu"
  cComment = "Manutenzione bilancio UE"
  i_nRowNum = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SLCODICE = space(15)
  w_CODAZI = space(5)
  w_SLDESCRI = space(40)
  w_SL__NOTE = space(0)
  w_CPROWORD = 0
  w_SLFLMACO = space(1)
  w_SLCODESE = space(4)
  w_SLSERIAL = space(10)
  w_SLCODCON = space(15)
  w_SLIMPDAR = 0
  w_SLIMPAVE = 0
  w_SL_SEGNO = space(1)
  w_SL__DAVE = space(1)
  w_DESCRI = space(40)
  w_UTCC = 0
  w_UTDV = ctot('')
  w_UTCV = 0
  w_UTDC = ctot('')
  w_SLCODMAS = space(15)
  w_CODMAS = space(10)
  w_MDESCRI = space(40)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_SLSERIAL = this.W_SLSERIAL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'BIL_MAST','gscg_mbu')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mbuPag1","gscg_mbu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione bilancio UE")
      .Pages(1).HelpContextID = 29236113
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='MASTRI'
    this.cWorkTables[4]='BIL_MAST'
    this.cWorkTables[5]='BIL_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.BIL_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.BIL_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SLSERIAL = NVL(SLSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from BIL_MAST where SLSERIAL=KeySet.SLSERIAL
    *
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2],this.bLoadRecFilter,this.BIL_MAST_IDX,"gscg_mbu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('BIL_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "BIL_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"BIL_DETT.","BIL_MAST.")
      i_cTable = i_cTable+' BIL_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SLSERIAL',this.w_SLSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_SLCODICE = NVL(SLCODICE,space(15))
        .w_SLDESCRI = NVL(SLDESCRI,space(40))
        .w_SL__NOTE = NVL(SL__NOTE,space(0))
        .w_SLCODESE = NVL(SLCODESE,space(4))
          * evitabile
          *.link_1_6('Load')
        .w_SLSERIAL = NVL(SLSERIAL,space(10))
        .op_SLSERIAL = .w_SLSERIAL
        .w_UTCC = NVL(UTCC,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'BIL_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from BIL_DETT where SLFLMACO=KeySet.SLFLMACO
      *                            and SLSERIAL=KeySet.SLSERIAL
      *                            and SLCODCON=KeySet.SLCODCON
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('BIL_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "BIL_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" BIL_DETT"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'SLSERIAL',this.w_SLSERIAL  )
        select * from (i_cTable) BIL_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRI = space(40)
          .w_CODMAS = space(10)
          .w_MDESCRI = space(40)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SLFLMACO = NVL(SLFLMACO,space(1))
          .w_SLCODCON = NVL(SLCODCON,space(15))
          if link_2_3_joined
            this.w_SLCODCON = NVL(ANCODICE203,NVL(this.w_SLCODCON,space(15)))
            this.w_DESCRI = NVL(ANDESCRI203,space(40))
            this.w_CODMAS = NVL(ANCONSUP203,space(10))
          else
          .link_2_3('Load')
          endif
          .w_SLIMPDAR = NVL(SLIMPDAR,0)
          .w_SLIMPAVE = NVL(SLIMPAVE,0)
          .w_SL_SEGNO = NVL(SL_SEGNO,space(1))
          .w_SL__DAVE = NVL(SL__DAVE,space(1))
          .w_SLCODMAS = NVL(SLCODMAS,space(15))
          .link_2_12('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace SLFLMACO with .w_SLFLMACO
          replace SLCODCON with .w_SLCODCON
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_mbu
    if this.bLoaded
    with this
    .oPgFrm.Page1.oPag.oBody.SetFullFocus()
    endwith
    endif
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_SLCODICE=space(15)
      .w_CODAZI=space(5)
      .w_SLDESCRI=space(40)
      .w_SL__NOTE=space(0)
      .w_CPROWORD=10
      .w_SLFLMACO=space(1)
      .w_SLCODESE=space(4)
      .w_SLSERIAL=space(10)
      .w_SLCODCON=space(15)
      .w_SLIMPDAR=0
      .w_SLIMPAVE=0
      .w_SL_SEGNO=space(1)
      .w_SL__DAVE=space(1)
      .w_DESCRI=space(40)
      .w_UTCC=0
      .w_UTDV=ctot("")
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_SLCODMAS=space(15)
      .w_CODMAS=space(10)
      .w_MDESCRI=space(40)
      if .cFunction<>"Filter"
        .w_SLCODICE = 'BILANCOGE'
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,7,.f.)
        if not(empty(.w_SLCODESE))
         .link_1_6('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_SLCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(10,20,.f.)
        if not(empty(.w_CODMAS))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'BIL_MAST')
    this.DoRTCalc(21,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSLDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oSL__NOTE_1_4.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'BIL_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERBIL","i_CODAZI,w_SLSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_SLSERIAL = .w_SLSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODICE,"SLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDESCRI,"SLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SL__NOTE,"SL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODESE,"SLCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLSERIAL,"SLSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
    i_lTable = "BIL_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.BIL_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_SLFLMACO C(1);
      ,t_SLCODCON C(15);
      ,t_SLIMPDAR N(18,4);
      ,t_SLIMPAVE N(18,4);
      ,t_DESCRI C(40);
      ,t_CODMAS C(10);
      ,t_MDESCRI C(40);
      ,SLFLMACO C(1);
      ,SLCODCON C(15);
      ,t_SL_SEGNO C(1);
      ,t_SL__DAVE C(1);
      ,t_SLCODMAS C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mbubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSLFLMACO_2_2.controlsource=this.cTrsName+'.t_SLFLMACO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSLCODCON_2_3.controlsource=this.cTrsName+'.t_SLCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPDAR_2_4.controlsource=this.cTrsName+'.t_SLIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPAVE_2_5.controlsource=this.cTrsName+'.t_SLIMPAVE'
    this.oPgFRm.Page1.oPag.oDESCRI_2_8.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oCODMAS_2_12.controlsource=this.cTrsName+'.t_CODMAS'
    this.oPgFRm.Page1.oPag.oMDESCRI_2_13.controlsource=this.cTrsName+'.t_MDESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(90)
    this.AddVLine(219)
    this.AddVLine(409)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SERBIL","i_CODAZI,w_SLSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into BIL_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'BIL_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'BIL_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(SLCODICE,SLDESCRI,SL__NOTE,SLCODESE,SLSERIAL"+;
                  ",UTCC,UTDV,UTCV,UTDC"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_SLCODICE)+;
                    ","+cp_ToStrODBC(this.w_SLDESCRI)+;
                    ","+cp_ToStrODBC(this.w_SL__NOTE)+;
                    ","+cp_ToStrODBCNull(this.w_SLCODESE)+;
                    ","+cp_ToStrODBC(this.w_SLSERIAL)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'BIL_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'BIL_MAST')
        cp_CheckDeletedKey(i_cTable,0,'SLSERIAL',this.w_SLSERIAL)
        INSERT INTO (i_cTable);
              (SLCODICE,SLDESCRI,SL__NOTE,SLCODESE,SLSERIAL,UTCC,UTDV,UTCV,UTDC &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_SLCODICE;
                  ,this.w_SLDESCRI;
                  ,this.w_SL__NOTE;
                  ,this.w_SLCODESE;
                  ,this.w_SLSERIAL;
                  ,this.w_UTCC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
      *
      * insert into BIL_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CPROWORD,SLFLMACO,SLSERIAL,SLCODCON,SLIMPDAR"+;
                  ",SLIMPAVE,SL_SEGNO,SL__DAVE,SLCODMAS,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_SLFLMACO)+","+cp_ToStrODBC(this.w_SLSERIAL)+","+cp_ToStrODBCNull(this.w_SLCODCON)+","+cp_ToStrODBC(this.w_SLIMPDAR)+;
             ","+cp_ToStrODBC(this.w_SLIMPAVE)+","+cp_ToStrODBC(this.w_SL_SEGNO)+","+cp_ToStrODBC(this.w_SL__DAVE)+","+cp_ToStrODBC(this.w_SLCODMAS)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'SLFLMACO',this.w_SLFLMACO,'SLSERIAL',this.w_SLSERIAL,'SLCODCON',this.w_SLCODCON)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CPROWORD,this.w_SLFLMACO,this.w_SLSERIAL,this.w_SLCODCON,this.w_SLIMPDAR"+;
                ",this.w_SLIMPAVE,this.w_SL_SEGNO,this.w_SL__DAVE,this.w_SLCODMAS,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update BIL_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'BIL_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " SLCODICE="+cp_ToStrODBC(this.w_SLCODICE)+;
             ",SLDESCRI="+cp_ToStrODBC(this.w_SLDESCRI)+;
             ",SL__NOTE="+cp_ToStrODBC(this.w_SL__NOTE)+;
             ",SLCODESE="+cp_ToStrODBCNull(this.w_SLCODESE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'BIL_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'SLSERIAL',this.w_SLSERIAL  )
          UPDATE (i_cTable) SET;
              SLCODICE=this.w_SLCODICE;
             ,SLDESCRI=this.w_SLDESCRI;
             ,SL__NOTE=this.w_SL__NOTE;
             ,SLCODESE=this.w_SLCODESE;
             ,UTCC=this.w_UTCC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_SLCODCON)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from BIL_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and SLFLMACO="+cp_ToStrODBC(&i_TN.->SLFLMACO)+;
                            " and SLCODCON="+cp_ToStrODBC(&i_TN.->SLCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and SLFLMACO=&i_TN.->SLFLMACO;
                            and SLCODCON=&i_TN.->SLCODCON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace SLFLMACO with this.w_SLFLMACO
              replace SLCODCON with this.w_SLCODCON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update BIL_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SLIMPDAR="+cp_ToStrODBC(this.w_SLIMPDAR)+;
                     ",SLIMPAVE="+cp_ToStrODBC(this.w_SLIMPAVE)+;
                     ",SL_SEGNO="+cp_ToStrODBC(this.w_SL_SEGNO)+;
                     ",SL__DAVE="+cp_ToStrODBC(this.w_SL__DAVE)+;
                     ",SLCODMAS="+cp_ToStrODBC(this.w_SLCODMAS)+;
                     " ,SLFLMACO="+cp_ToStrODBC(this.w_SLFLMACO)+;
                     " ,SLCODCON="+cp_ToStrODBC(this.w_SLCODCON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and SLFLMACO="+cp_ToStrODBC(SLFLMACO)+;
                             " and SLCODCON="+cp_ToStrODBC(SLCODCON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SLIMPDAR=this.w_SLIMPDAR;
                     ,SLIMPAVE=this.w_SLIMPAVE;
                     ,SL_SEGNO=this.w_SL_SEGNO;
                     ,SL__DAVE=this.w_SL__DAVE;
                     ,SLCODMAS=this.w_SLCODMAS;
                     ,SLFLMACO=this.w_SLFLMACO;
                     ,SLCODCON=this.w_SLCODCON;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and SLFLMACO=&i_TN.->SLFLMACO;
                                      and SLCODCON=&i_TN.->SLCODCON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_SLCODCON)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.BIL_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.BIL_DETT_IDX,2])
        *
        * delete BIL_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and SLFLMACO="+cp_ToStrODBC(&i_TN.->SLFLMACO)+;
                            " and SLCODCON="+cp_ToStrODBC(&i_TN.->SLCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and SLFLMACO=&i_TN.->SLFLMACO;
                              and SLCODCON=&i_TN.->SLCODCON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
        *
        * delete BIL_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_SLCODCON)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BIL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BIL_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_1_6('Full')
        .DoRTCalc(8,19,.t.)
          .link_2_12('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SERBIL","i_CODAZI,w_SLSERIAL")
          .op_SLSERIAL = .w_SLSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SL_SEGNO with this.w_SL_SEGNO
      replace t_SL__DAVE with this.w_SL__DAVE
      replace t_SLCODMAS with this.w_SLCODMAS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSLCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSLCODCON_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SLCODESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_SLCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_SLCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SLCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLFLMACO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SLFLMACO;
                     ,'ANCODICE',trim(this.w_SLCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SLCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLFLMACO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SLFLMACO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SLCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSLCODCON_2_3'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SLFLMACO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SLFLMACO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SLCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLFLMACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SLFLMACO;
                       ,'ANCODICE',this.w_SLCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_CODMAS = NVL(_Link_.ANCONSUP,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_CODMAS = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"+ ",link_2_3.ANCONSUP as ANCONSUP203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on BIL_DETT.SLCODCON=link_2_3.ANCODICE"+" and BIL_DETT.SLFLMACO=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and BIL_DETT.SLCODCON=link_2_3.ANCODICE(+)"'+'+" and BIL_DETT.SLFLMACO=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODMAS
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CODMAS)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAS = NVL(_Link_.MCCODICE,space(10))
      this.w_MDESCRI = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAS = space(10)
      endif
      this.w_MDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSLDESCRI_1_3.value==this.w_SLDESCRI)
      this.oPgFrm.Page1.oPag.oSLDESCRI_1_3.value=this.w_SLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSL__NOTE_1_4.value==this.w_SL__NOTE)
      this.oPgFrm.Page1.oPag.oSL__NOTE_1_4.value=this.w_SL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSLCODESE_1_6.value==this.w_SLCODESE)
      this.oPgFrm.Page1.oPag.oSLCODESE_1_6.value=this.w_SLCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_2_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_2_8.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oDESCRI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAS_2_12.value==this.w_CODMAS)
      this.oPgFrm.Page1.oPag.oCODMAS_2_12.value=this.w_CODMAS
      replace t_CODMAS with this.oPgFrm.Page1.oPag.oCODMAS_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMDESCRI_2_13.value==this.w_MDESCRI)
      this.oPgFrm.Page1.oPag.oMDESCRI_2_13.value=this.w_MDESCRI
      replace t_MDESCRI with this.oPgFrm.Page1.oPag.oMDESCRI_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLFLMACO_2_2.value==this.w_SLFLMACO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLFLMACO_2_2.value=this.w_SLFLMACO
      replace t_SLFLMACO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLFLMACO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLCODCON_2_3.value==this.w_SLCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLCODCON_2_3.value=this.w_SLCODCON
      replace t_SLCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPDAR_2_4.value==this.w_SLIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPDAR_2_4.value=this.w_SLIMPDAR
      replace t_SLIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPDAR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPAVE_2_5.value==this.w_SLIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPAVE_2_5.value=this.w_SLIMPAVE
      replace t_SLIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLIMPAVE_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'BIL_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_SLFLMACO $ 'C-F-G') and (NOT EMPTY(.w_SLCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSLFLMACO_2_2
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if NOT EMPTY(.w_SLCODCON)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_SLCODCON))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_SLFLMACO=space(1)
      .w_SLCODCON=space(15)
      .w_SLIMPDAR=0
      .w_SLIMPAVE=0
      .w_SL_SEGNO=space(1)
      .w_SL__DAVE=space(1)
      .w_DESCRI=space(40)
      .w_SLCODMAS=space(15)
      .w_CODMAS=space(10)
      .w_MDESCRI=space(40)
      .DoRTCalc(1,9,.f.)
      if not(empty(.w_SLCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(10,20,.f.)
      if not(empty(.w_CODMAS))
        .link_2_12('Full')
      endif
    endwith
    this.DoRTCalc(21,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SLFLMACO = t_SLFLMACO
    this.w_SLCODCON = t_SLCODCON
    this.w_SLIMPDAR = t_SLIMPDAR
    this.w_SLIMPAVE = t_SLIMPAVE
    this.w_SL_SEGNO = t_SL_SEGNO
    this.w_SL__DAVE = t_SL__DAVE
    this.w_DESCRI = t_DESCRI
    this.w_SLCODMAS = t_SLCODMAS
    this.w_CODMAS = t_CODMAS
    this.w_MDESCRI = t_MDESCRI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SLFLMACO with this.w_SLFLMACO
    replace t_SLCODCON with this.w_SLCODCON
    replace t_SLIMPDAR with this.w_SLIMPDAR
    replace t_SLIMPAVE with this.w_SLIMPAVE
    replace t_SL_SEGNO with this.w_SL_SEGNO
    replace t_SL__DAVE with this.w_SL__DAVE
    replace t_DESCRI with this.w_DESCRI
    replace t_SLCODMAS with this.w_SLCODMAS
    replace t_CODMAS with this.w_CODMAS
    replace t_MDESCRI with this.w_MDESCRI
    if i_srv='A'
      replace SLFLMACO with this.w_SLFLMACO
      replace SLCODCON with this.w_SLCODCON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Caricamento non consentito. Movimentare i conti mediante la funzione di caricamento\modifica manutenzione bilancio UE"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_mbuPag1 as StdContainer
  Width  = 613
  height = 381
  stdWidth  = 613
  stdheight = 381
  resizeYpos=191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSLDESCRI_1_3 as StdField with uid="KFNOTZURNJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SLDESCRI", cQueryName = "SLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del tipo di riclassificazione",;
    HelpContextID = 28273775,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=101, Top=9, InputMask=replicate('X',40)

  add object oSL__NOTE_1_4 as StdMemo with uid="HREGABBRIS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SL__NOTE", cQueryName = "SL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 226172011,;
   bGlobalFont=.t.,;
    Height=52, Width=336, Left=101, Top=37

  add object oSLCODESE_1_6 as StdField with uid="TUEKAGGOHF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SLCODESE", cQueryName = "SLCODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo di riclassificazione",;
    HelpContextID = 46750827,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=56, Left=532, Top=9, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_SLCODESE"

  func oSLCODESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=95, width=607,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Rigo",Field2="SLFLMACO",Label2="Tipo",Field3="SLCODCON",Label3="Conto",Field4="SLIMPDAR",Label4="Importo dare",Field5="SLIMPAVE",Label5="Importo avere",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202179706

  add object oStr_1_5 as StdString with uid="MDBAPFEURT",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=94, Height=15,;
    Caption="Descrizione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="YGZNPAMECE",Visible=.t., Left=448, Top=9,;
    Alignment=1, Width=81, Height=18,;
    Caption="Esercizio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=115,;
    width=603+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=116,width=602+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCRI_2_8.Refresh()
      this.Parent.oCODMAS_2_12.Refresh()
      this.Parent.oMDESCRI_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oSLCODCON_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCRI_2_8 as StdTrsField with uid="KVJXSDPXRO",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 127816758,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=409, Left=101, Top=332, InputMask=replicate('X',40)

  add object oCODMAS_2_12 as StdTrsField with uid="JXRDZUKSQV",rtseq=20,rtrep=.t.,;
    cFormVar="w_CODMAS",value=space(10),enabled=.f.,;
    HelpContextID = 9924134,;
    cTotal="", bFixedPos=.t., cQueryName = "CODMAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=112, Left=101, Top=358, InputMask=replicate('X',10), cLinkFile="MASTRI", oKey_1_1="MCCODICE", oKey_1_2="this.w_CODMAS"

  func oCODMAS_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMDESCRI_2_13 as StdTrsField with uid="ASEICUZUEL",rtseq=21,rtrep=.t.,;
    cFormVar="w_MDESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 4361274,;
    cTotal="", bFixedPos=.t., cQueryName = "MDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=219, Top=358, InputMask=replicate('X',40)

  add object oStr_2_9 as StdString with uid="QLLLJCSVLM",Visible=.t., Left=5, Top=334,;
    Alignment=1, Width=94, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="KHYHQRSLOK",Visible=.t., Left=5, Top=359,;
    Alignment=1, Width=94, Height=18,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mbuBodyRow as CPBodyRowCnt
  Width=593
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="QQJFDGSIQL",rtseq=5,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo riga",;
    HelpContextID = 234508138,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oSLFLMACO_2_2 as StdTrsField with uid="RMAAVCKCWA",rtseq=6,rtrep=.t.,;
    cFormVar="w_SLFLMACO",value=space(1),isprimarykey=.t.,;
    HelpContextID = 257330293,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=55, Top=-1, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oSLFLMACO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SLFLMACO $ 'C-F-G')
      if .not. empty(.w_SLCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSLCODCON_2_3 as StdTrsField with uid="PNEGXWFJFR",rtseq=9,rtrep=.t.,;
    cFormVar="w_SLCODCON",value=space(15),isprimarykey=.t.,;
    HelpContextID = 255239052,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=123, Left=87, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SLFLMACO", oKey_2_1="ANCODICE", oKey_2_2="this.w_SLCODCON"

  func oSLCODCON_2_3.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_SLFLMACO))
    endwith
  endfunc

  func oSLCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSLCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSLCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSLCODCON_2_3.readonly and this.parent.oSLCODCON_2_3.isprimarykey)
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SLFLMACO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SLFLMACO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSLCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
   endif
  endproc
  proc oSLCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SLFLMACO
     i_obj.w_ANCODICE=this.parent.oContained.w_SLCODCON
    i_obj.ecpSave()
  endproc

  add object oSLIMPDAR_2_4 as StdTrsField with uid="GALOGJGTYM",rtseq=10,rtrep=.t.,;
    cFormVar="w_SLIMPDAR",value=0,;
    HelpContextID = 42450040,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=183, Left=217, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  add object oSLIMPAVE_2_5 as StdTrsField with uid="LHVJJGMQAP",rtseq=11,rtrep=.t.,;
    cFormVar="w_SLIMPAVE",value=0,;
    HelpContextID = 260553835,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=182, Left=406, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mbu','BIL_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SLSERIAL=BIL_MAST.SLSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
