* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kpr                                                        *
*              Riassegnamento numero protocollo                                *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_32]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-11                                                      *
* Last revis.: 2007-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kpr",oParentObject))

* --- Class definition
define class tgscg_kpr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 415
  Height = 220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-09"
  HelpContextID=149562729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kpr"
  cComment = "Riassegnamento numero protocollo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODANN = 0
  o_CODANN = 0
  w_DATINI = ctod('  /  /  ')
  w_SERFIL = space(10)
  w_SELTOT = space(1)
  o_SELTOT = space(1)
  w_INIPRO = 0
  o_INIPRO = 0
  w_SELPRO = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kprPag1","gscg_kpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODANN_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODANN=0
      .w_DATINI=ctod("  /  /  ")
      .w_SERFIL=space(10)
      .w_SELTOT=space(1)
      .w_INIPRO=0
      .w_SELPRO=0
        .w_CODANN = YEAR(i_DATSYS)
        .w_DATINI = cp_CharToDate('01-01-'+ALLTRIM(STR(.w_CODANN)))
        .w_SERFIL = SPACE(10)
          .DoRTCalc(4,5,.f.)
        .w_SELPRO = IIF(.w_INIPRO=0, 0, .w_INIPRO-1)
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(ah_MsgFormat("Questa funzione riassegna la numerazione progressiva del protocollo%0per le registrazioni contabili / documenti di acquisto"))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODANN<>.w_CODANN
            .w_DATINI = cp_CharToDate('01-01-'+ALLTRIM(STR(.w_CODANN)))
        endif
        if .o_SELTOT<>.w_SELTOT
            .w_SERFIL = SPACE(10)
        endif
        .DoRTCalc(4,5,.t.)
        if .o_INIPRO<>.w_INIPRO
            .w_SELPRO = IIF(.w_INIPRO=0, 0, .w_INIPRO-1)
        endif
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(ah_MsgFormat("Questa funzione riassegna la numerazione progressiva del protocollo%0per le registrazioni contabili / documenti di acquisto"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(ah_MsgFormat("Questa funzione riassegna la numerazione progressiva del protocollo%0per le registrazioni contabili / documenti di acquisto"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSERFIL_1_6.enabled = this.oPgFrm.Page1.oPag.oSERFIL_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODANN_1_2.value==this.w_CODANN)
      this.oPgFrm.Page1.oPag.oCODANN_1_2.value=this.w_CODANN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERFIL_1_6.value==this.w_SERFIL)
      this.oPgFrm.Page1.oPag.oSERFIL_1_6.value=this.w_SERFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oSELTOT_1_7.RadioValue()==this.w_SELTOT)
      this.oPgFrm.Page1.oPag.oSELTOT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINIPRO_1_8.value==this.w_INIPRO)
      this.oPgFrm.Page1.oPag.oINIPRO_1_8.value=this.w_INIPRO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODANN)) or not(.w_CODANN>1900 AND .w_CODANN<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODANN_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODANN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una data appropriata")
          case   ((empty(.w_DATINI)) or not(YEAR(.w_DATINI)=.w_CODANN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere dello stesso anno di selezione del protocollo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODANN = this.w_CODANN
    this.o_SELTOT = this.w_SELTOT
    this.o_INIPRO = this.w_INIPRO
    return

enddefine

* --- Define pages as container
define class tgscg_kprPag1 as StdContainer
  Width  = 411
  height = 220
  stdWidth  = 411
  stdheight = 220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODANN_1_2 as StdField with uid="DHJHOQTRQB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODANN", cQueryName = "CODANN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una data appropriata",;
    ToolTipText = "Anno di elaborazione",;
    HelpContextID = 96768474,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=109, Top=75, cSayPict='"9999"', cGetPict='"9999"'

  func oCODANN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CODANN>1900 AND .w_CODANN<2100)
    endwith
    return bRes
  endfunc

  add object oDATINI_1_4 as StdField with uid="GXVJYDAPHZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere dello stesso anno di selezione del protocollo",;
    ToolTipText = "Data di registrazione a partire dalla quale verranno rinumerate le registrazioni",;
    HelpContextID = 180068298,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=109, Top=104

  func oDATINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (YEAR(.w_DATINI)=.w_CODANN)
    endwith
    return bRes
  endfunc

  add object oSERFIL_1_6 as StdField with uid="ZRPRCOETSV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SERFIL", cQueryName = "SERFIL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie da riprotocollare",;
    HelpContextID = 135183066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=109, Top=133, InputMask=replicate('X',10)

  func oSERFIL_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELTOT=' ')
    endwith
   endif
  endfunc

  add object oSELTOT_1_7 as StdCheck with uid="SUJKEXRPHZ",rtseq=4,rtrep=.f.,left=199, top=133, caption="Tutte le serie",;
    ToolTipText = "Se attivo: non esegue alcun filtro sulla serie protocollo",;
    HelpContextID = 262216410,;
    cFormVar="w_SELTOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELTOT_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSELTOT_1_7.GetRadio()
    this.Parent.oContained.w_SELTOT = this.RadioValue()
    return .t.
  endfunc

  func oSELTOT_1_7.SetRadio()
    this.Parent.oContained.w_SELTOT=trim(this.Parent.oContained.w_SELTOT)
    this.value = ;
      iif(this.Parent.oContained.w_SELTOT=='S',1,;
      0)
  endfunc

  add object oINIPRO_1_8 as StdField with uid="WEZXNNODLI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INIPRO", cQueryName = "INIPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero protocollo a partire dal quale verranno rinumerate le registrazioni",;
    HelpContextID = 74793594,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=162, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999


  add object oBtn_1_10 as StdButton with uid="ZNREPGHWCX",left=304, top=169, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rielaborare la numerazione protocollo";
    , HelpContextID = 149533978;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        do GSCG_BPR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="XYGIHPUFHG",left=356, top=169, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 142245306;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_14 as cp_calclbl with uid="VODPRMFWML",left=10, top=24, width=390,height=43,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 28434918

  add object oStr_1_1 as StdString with uid="HUAYXSECQY",Visible=.t., Left=10, Top=7,;
    Alignment=0, Width=60, Height=18,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="CRKWAMVOFT",Visible=.t., Left=46, Top=75,;
    Alignment=1, Width=59, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EJQCNGRGBZ",Visible=.t., Left=13, Top=104,;
    Alignment=1, Width=93, Height=18,;
    Caption="Data iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EKLGEAVBIU",Visible=.t., Left=10, Top=133,;
    Alignment=1, Width=95, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OLGTWWYWWC",Visible=.t., Left=43, Top=162,;
    Alignment=1, Width=62, Height=18,;
    Caption="Protocollo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kpr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
