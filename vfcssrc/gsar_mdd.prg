* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mdd                                                        *
*              Destinazioni diverse                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_76]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mdd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mdd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mdd")
  return

* --- Class definition
define class tgsar_mdd as StdPCForm
  Width  = 731
  Height = 493
  Top    = 50
  Left   = 10
  cComment = "Destinazioni diverse"
  cPrg = "gsar_mdd"
  HelpContextID=80312169
  add object cnt as tcgsar_mdd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mdd as PCContext
  w_DDTIPCON = space(1)
  w_DDCODICE = space(15)
  w_DDCODDES = space(5)
  w_DDNOMDES = space(40)
  w_DDTIPRIF = space(2)
  w_DDPERSON = space(40)
  w_DDTELEFO = space(18)
  w_DD__NOTE = space(40)
  w_DDNUMFAX = space(18)
  w_DDINDIRI = space(35)
  w_DDDTOBSO = space(8)
  w_DD_EMAIL = space(254)
  w_DD_EMPEC = space(254)
  w_DD___CAP = space(8)
  w_DDLOCALI = space(30)
  w_DDPROVIN = space(2)
  w_DDCODNAZ = space(3)
  w_DESNAZ = space(35)
  w_DDCODVET = space(5)
  w_DESVET = space(35)
  w_DDCODPOR = space(1)
  w_DESPOR = space(30)
  w_DDCODSPE = space(3)
  w_DDCATOPE = space(2)
  w_DDCODAGE = space(5)
  w_DDTIPOPE = space(10)
  w_DESSPE = space(35)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_DDPREDEF = space(1)
  w_DESAGE = space(35)
  w_DTOBSO = space(8)
  w_FLSGNNRE = space(1)
  w_TIDESCRI = space(30)
  w_TIDTOBSO = space(8)
  w_DDMCCODI = space(5)
  w_DDMCCODT = space(5)
  w_DESMCIMB = space(35)
  w_DESMCTRA = space(35)
  w_DDRFDICH = space(15)
  w_NUMDIC = 0
  w_ANNDIC = space(4)
  w_DATDIC = space(8)
  w_SERDIC = space(3)
  w_DDTELFAX = space(18)
  w_CONTRESI = 0
  w_TOTRESI = 0
  w_SEDERESI = 0
  w_DDTIPINF = space(3)
  w_DDCODEST = space(7)
  w_DDCODCLA = space(5)
  w_DDCODPEC = space(10)
  proc Save(i_oFrom)
    this.w_DDTIPCON = i_oFrom.w_DDTIPCON
    this.w_DDCODICE = i_oFrom.w_DDCODICE
    this.w_DDCODDES = i_oFrom.w_DDCODDES
    this.w_DDNOMDES = i_oFrom.w_DDNOMDES
    this.w_DDTIPRIF = i_oFrom.w_DDTIPRIF
    this.w_DDPERSON = i_oFrom.w_DDPERSON
    this.w_DDTELEFO = i_oFrom.w_DDTELEFO
    this.w_DD__NOTE = i_oFrom.w_DD__NOTE
    this.w_DDNUMFAX = i_oFrom.w_DDNUMFAX
    this.w_DDINDIRI = i_oFrom.w_DDINDIRI
    this.w_DDDTOBSO = i_oFrom.w_DDDTOBSO
    this.w_DD_EMAIL = i_oFrom.w_DD_EMAIL
    this.w_DD_EMPEC = i_oFrom.w_DD_EMPEC
    this.w_DD___CAP = i_oFrom.w_DD___CAP
    this.w_DDLOCALI = i_oFrom.w_DDLOCALI
    this.w_DDPROVIN = i_oFrom.w_DDPROVIN
    this.w_DDCODNAZ = i_oFrom.w_DDCODNAZ
    this.w_DESNAZ = i_oFrom.w_DESNAZ
    this.w_DDCODVET = i_oFrom.w_DDCODVET
    this.w_DESVET = i_oFrom.w_DESVET
    this.w_DDCODPOR = i_oFrom.w_DDCODPOR
    this.w_DESPOR = i_oFrom.w_DESPOR
    this.w_DDCODSPE = i_oFrom.w_DDCODSPE
    this.w_DDCATOPE = i_oFrom.w_DDCATOPE
    this.w_DDCODAGE = i_oFrom.w_DDCODAGE
    this.w_DDTIPOPE = i_oFrom.w_DDTIPOPE
    this.w_DESSPE = i_oFrom.w_DESSPE
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_DDPREDEF = i_oFrom.w_DDPREDEF
    this.w_DESAGE = i_oFrom.w_DESAGE
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_FLSGNNRE = i_oFrom.w_FLSGNNRE
    this.w_TIDESCRI = i_oFrom.w_TIDESCRI
    this.w_TIDTOBSO = i_oFrom.w_TIDTOBSO
    this.w_DDMCCODI = i_oFrom.w_DDMCCODI
    this.w_DDMCCODT = i_oFrom.w_DDMCCODT
    this.w_DESMCIMB = i_oFrom.w_DESMCIMB
    this.w_DESMCTRA = i_oFrom.w_DESMCTRA
    this.w_DDRFDICH = i_oFrom.w_DDRFDICH
    this.w_NUMDIC = i_oFrom.w_NUMDIC
    this.w_ANNDIC = i_oFrom.w_ANNDIC
    this.w_DATDIC = i_oFrom.w_DATDIC
    this.w_SERDIC = i_oFrom.w_SERDIC
    this.w_DDTELFAX = i_oFrom.w_DDTELFAX
    this.w_CONTRESI = i_oFrom.w_CONTRESI
    this.w_TOTRESI = i_oFrom.w_TOTRESI
    this.w_SEDERESI = i_oFrom.w_SEDERESI
    this.w_DDTIPINF = i_oFrom.w_DDTIPINF
    this.w_DDCODEST = i_oFrom.w_DDCODEST
    this.w_DDCODCLA = i_oFrom.w_DDCODCLA
    this.w_DDCODPEC = i_oFrom.w_DDCODPEC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DDTIPCON = this.w_DDTIPCON
    i_oTo.w_DDCODICE = this.w_DDCODICE
    i_oTo.w_DDCODDES = this.w_DDCODDES
    i_oTo.w_DDNOMDES = this.w_DDNOMDES
    i_oTo.w_DDTIPRIF = this.w_DDTIPRIF
    i_oTo.w_DDPERSON = this.w_DDPERSON
    i_oTo.w_DDTELEFO = this.w_DDTELEFO
    i_oTo.w_DD__NOTE = this.w_DD__NOTE
    i_oTo.w_DDNUMFAX = this.w_DDNUMFAX
    i_oTo.w_DDINDIRI = this.w_DDINDIRI
    i_oTo.w_DDDTOBSO = this.w_DDDTOBSO
    i_oTo.w_DD_EMAIL = this.w_DD_EMAIL
    i_oTo.w_DD_EMPEC = this.w_DD_EMPEC
    i_oTo.w_DD___CAP = this.w_DD___CAP
    i_oTo.w_DDLOCALI = this.w_DDLOCALI
    i_oTo.w_DDPROVIN = this.w_DDPROVIN
    i_oTo.w_DDCODNAZ = this.w_DDCODNAZ
    i_oTo.w_DESNAZ = this.w_DESNAZ
    i_oTo.w_DDCODVET = this.w_DDCODVET
    i_oTo.w_DESVET = this.w_DESVET
    i_oTo.w_DDCODPOR = this.w_DDCODPOR
    i_oTo.w_DESPOR = this.w_DESPOR
    i_oTo.w_DDCODSPE = this.w_DDCODSPE
    i_oTo.w_DDCATOPE = this.w_DDCATOPE
    i_oTo.w_DDCODAGE = this.w_DDCODAGE
    i_oTo.w_DDTIPOPE = this.w_DDTIPOPE
    i_oTo.w_DESSPE = this.w_DESSPE
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_DDPREDEF = this.w_DDPREDEF
    i_oTo.w_DESAGE = this.w_DESAGE
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_FLSGNNRE = this.w_FLSGNNRE
    i_oTo.w_TIDESCRI = this.w_TIDESCRI
    i_oTo.w_TIDTOBSO = this.w_TIDTOBSO
    i_oTo.w_DDMCCODI = this.w_DDMCCODI
    i_oTo.w_DDMCCODT = this.w_DDMCCODT
    i_oTo.w_DESMCIMB = this.w_DESMCIMB
    i_oTo.w_DESMCTRA = this.w_DESMCTRA
    i_oTo.w_DDRFDICH = this.w_DDRFDICH
    i_oTo.w_NUMDIC = this.w_NUMDIC
    i_oTo.w_ANNDIC = this.w_ANNDIC
    i_oTo.w_DATDIC = this.w_DATDIC
    i_oTo.w_SERDIC = this.w_SERDIC
    i_oTo.w_DDTELFAX = this.w_DDTELFAX
    i_oTo.w_CONTRESI = this.w_CONTRESI
    i_oTo.w_TOTRESI = this.w_TOTRESI
    i_oTo.w_SEDERESI = this.w_SEDERESI
    i_oTo.w_DDTIPINF = this.w_DDTIPINF
    i_oTo.w_DDCODEST = this.w_DDCODEST
    i_oTo.w_DDCODCLA = this.w_DDCODCLA
    i_oTo.w_DDCODPEC = this.w_DDCODPEC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mdd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 731
  Height = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DES_DIVE_IDX = 0
  VETTORI_IDX = 0
  PORTI_IDX = 0
  MODASPED_IDX = 0
  NAZIONI_IDX = 0
  AGENTI_IDX = 0
  TIPCODIV_IDX = 0
  METCALSP_IDX = 0
  DIC_INTE_IDX = 0
  PAG_AMEN_IDX = 0
  cFile = "DES_DIVE"
  cKeySelect = "DDTIPCON,DDCODICE"
  cKeyWhere  = "DDTIPCON=this.w_DDTIPCON and DDCODICE=this.w_DDCODICE"
  cKeyDetail  = "DDTIPCON=this.w_DDTIPCON and DDCODICE=this.w_DDCODICE and DDCODDES=this.w_DDCODDES"
  cKeyWhereODBC = '"DDTIPCON="+cp_ToStrODBC(this.w_DDTIPCON)';
      +'+" and DDCODICE="+cp_ToStrODBC(this.w_DDCODICE)';

  cKeyDetailWhereODBC = '"DDTIPCON="+cp_ToStrODBC(this.w_DDTIPCON)';
      +'+" and DDCODICE="+cp_ToStrODBC(this.w_DDCODICE)';
      +'+" and DDCODDES="+cp_ToStrODBC(this.w_DDCODDES)';

  cKeyWhereODBCqualified = '"DES_DIVE.DDTIPCON="+cp_ToStrODBC(this.w_DDTIPCON)';
      +'+" and DES_DIVE.DDCODICE="+cp_ToStrODBC(this.w_DDCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mdd"
  cComment = "Destinazioni diverse"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DDTIPCON = space(1)
  w_DDCODICE = space(15)
  w_DDCODDES = space(5)
  o_DDCODDES = space(5)
  w_DDNOMDES = space(40)
  w_DDTIPRIF = space(2)
  o_DDTIPRIF = space(2)
  w_DDPERSON = space(40)
  w_DDTELEFO = space(18)
  w_DD__NOTE = space(40)
  w_DDNUMFAX = space(18)
  o_DDNUMFAX = space(18)
  w_DDINDIRI = space(35)
  w_DDDTOBSO = ctod('  /  /  ')
  o_DDDTOBSO = ctod('  /  /  ')
  w_DD_EMAIL = space(254)
  w_DD_EMPEC = space(254)
  w_DD___CAP = space(8)
  w_DDLOCALI = space(30)
  w_DDPROVIN = space(2)
  w_DDCODNAZ = space(3)
  w_DESNAZ = space(35)
  w_DDCODVET = space(5)
  w_DESVET = space(35)
  w_DDCODPOR = space(1)
  w_DESPOR = space(30)
  w_DDCODSPE = space(3)
  w_DDCATOPE = space(2)
  w_DDCODAGE = space(5)
  w_DDTIPOPE = space(10)
  w_DESSPE = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DDPREDEF = space(1)
  w_DESAGE = space(35)
  w_DTOBSO = ctod('  /  /  ')
  w_FLSGNNRE = space(1)
  w_TIDESCRI = space(30)
  w_TIDTOBSO = ctod('  /  /  ')
  w_DDMCCODI = space(5)
  w_DDMCCODT = space(5)
  w_DESMCIMB = space(35)
  w_DESMCTRA = space(35)
  w_DDRFDICH = space(15)
  w_NUMDIC = 0
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_SERDIC = space(3)
  w_DDTELFAX = space(18)
  w_CONTRESI = 0
  w_TOTRESI = 0
  o_TOTRESI = 0
  w_SEDERESI = 0
  w_DDTIPINF = space(3)
  w_DDCODEST = space(7)
  w_DDCODCLA = space(5)
  w_DDCODPEC = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mddPag1","gsar_mdd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='VETTORI'
    this.cWorkTables[2]='PORTI'
    this.cWorkTables[3]='MODASPED'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='TIPCODIV'
    this.cWorkTables[7]='METCALSP'
    this.cWorkTables[8]='DIC_INTE'
    this.cWorkTables[9]='PAG_AMEN'
    this.cWorkTables[10]='DES_DIVE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DES_DIVE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DES_DIVE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mdd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_21_joined
    link_2_21_joined=.f.
    local link_2_23_joined
    link_2_23_joined=.f.
    local link_2_34_joined
    link_2_34_joined=.f.
    local link_2_35_joined
    link_2_35_joined=.f.
    local link_2_38_joined
    link_2_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DES_DIVE where DDTIPCON=KeySet.DDTIPCON
    *                            and DDCODICE=KeySet.DDCODICE
    *                            and DDCODDES=KeySet.DDCODDES
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2],this.bLoadRecFilter,this.DES_DIVE_IDX,"gsar_mdd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DES_DIVE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DES_DIVE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DES_DIVE '
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_21_joined=this.AddJoinedLink_2_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_23_joined=this.AddJoinedLink_2_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_34_joined=this.AddJoinedLink_2_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_35_joined=this.AddJoinedLink_2_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_38_joined=this.AddJoinedLink_2_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DDTIPCON',this.w_DDTIPCON  ,'DDCODICE',this.w_DDCODICE  )
      select * from (i_cTable) DES_DIVE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTRESI = 0
        .w_DDTIPCON = NVL(DDTIPCON,space(1))
        .w_DDCODICE = NVL(DDCODICE,space(15))
        .w_FLSGNNRE = this.oParentObject.w_ANFLSGRE
        .w_CONTRESI = .w_TOTRESI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DES_DIVE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRESI = 0
      scan
        with this
          .w_DESNAZ = space(35)
          .w_DESVET = space(35)
          .w_DESPOR = space(30)
          .w_DESSPE = space(35)
        .w_OBTEST = i_datsys
          .w_DATOBSO = ctod("  /  /  ")
          .w_DESAGE = space(35)
          .w_DTOBSO = ctod("  /  /  ")
          .w_TIDESCRI = space(30)
          .w_TIDTOBSO = ctod("  /  /  ")
          .w_DESMCIMB = space(35)
          .w_DESMCTRA = space(35)
          .w_NUMDIC = 0
          .w_ANNDIC = space(4)
          .w_DATDIC = ctod("  /  /  ")
          .w_SERDIC = space(3)
          .w_DDCODDES = NVL(DDCODDES,space(5))
          .w_DDNOMDES = NVL(DDNOMDES,space(40))
          .w_DDTIPRIF = NVL(DDTIPRIF,space(2))
          .w_DDPERSON = NVL(DDPERSON,space(40))
          .w_DDTELEFO = NVL(DDTELEFO,space(18))
          .w_DD__NOTE = NVL(DD__NOTE,space(40))
          .w_DDNUMFAX = NVL(DDNUMFAX,space(18))
          .w_DDINDIRI = NVL(DDINDIRI,space(35))
          .w_DDDTOBSO = NVL(cp_ToDate(DDDTOBSO),ctod("  /  /  "))
          .w_DD_EMAIL = NVL(DD_EMAIL,space(254))
          .w_DD_EMPEC = NVL(DD_EMPEC,space(254))
          .w_DD___CAP = NVL(DD___CAP,space(8))
          .w_DDLOCALI = NVL(DDLOCALI,space(30))
          .w_DDPROVIN = NVL(DDPROVIN,space(2))
          .w_DDCODNAZ = NVL(DDCODNAZ,space(3))
          if link_2_15_joined
            this.w_DDCODNAZ = NVL(NACODNAZ215,NVL(this.w_DDCODNAZ,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ215,space(35))
          else
          .link_2_15('Load')
          endif
          .w_DDCODVET = NVL(DDCODVET,space(5))
          if link_2_17_joined
            this.w_DDCODVET = NVL(VTCODVET217,NVL(this.w_DDCODVET,space(5)))
            this.w_DESVET = NVL(VTDESVET217,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(VTDTOBSO217),ctod("  /  /  "))
          else
          .link_2_17('Load')
          endif
          .w_DDCODPOR = NVL(DDCODPOR,space(1))
          if link_2_19_joined
            this.w_DDCODPOR = NVL(POCODPOR219,NVL(this.w_DDCODPOR,space(1)))
            this.w_DESPOR = NVL(PODESPOR219,space(30))
          else
          .link_2_19('Load')
          endif
          .w_DDCODSPE = NVL(DDCODSPE,space(3))
          if link_2_21_joined
            this.w_DDCODSPE = NVL(SPCODSPE221,NVL(this.w_DDCODSPE,space(3)))
            this.w_DESSPE = NVL(SPDESSPE221,space(35))
          else
          .link_2_21('Load')
          endif
          .w_DDCATOPE = NVL(DDCATOPE,space(2))
          .w_DDCODAGE = NVL(DDCODAGE,space(5))
          if link_2_23_joined
            this.w_DDCODAGE = NVL(AGCODAGE223,NVL(this.w_DDCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE223,space(35))
            this.w_DTOBSO = NVL(cp_ToDate(AGDTOBSO223),ctod("  /  /  "))
          else
          .link_2_23('Load')
          endif
          .w_DDTIPOPE = NVL(DDTIPOPE,space(10))
          .link_2_24('Load')
          .w_DDPREDEF = NVL(DDPREDEF,space(1))
          .w_DDMCCODI = NVL(DDMCCODI,space(5))
          if link_2_34_joined
            this.w_DDMCCODI = NVL(MSCODICE234,NVL(this.w_DDMCCODI,space(5)))
            this.w_DESMCIMB = NVL(MSDESCRI234,space(35))
          else
          .link_2_34('Load')
          endif
          .w_DDMCCODT = NVL(DDMCCODT,space(5))
          if link_2_35_joined
            this.w_DDMCCODT = NVL(MSCODICE235,NVL(this.w_DDMCCODT,space(5)))
            this.w_DESMCTRA = NVL(MSDESCRI235,space(35))
          else
          .link_2_35('Load')
          endif
          .w_DDRFDICH = NVL(DDRFDICH,space(15))
          if link_2_38_joined
            this.w_DDRFDICH = NVL(DISERIAL238,NVL(this.w_DDRFDICH,space(15)))
            this.w_ANNDIC = NVL(DI__ANNO238,space(4))
            this.w_NUMDIC = NVL(DINUMDOC238,0)
            this.w_DATDIC = NVL(cp_ToDate(DIDATDIC238),ctod("  /  /  "))
            this.w_SERDIC = NVL(DISERDOC238,space(3))
          else
          .link_2_38('Load')
          endif
          .w_DDTELFAX = NVL(DDTELFAX,space(18))
        .w_SEDERESI = IIF(.w_DDTIPRIF='RE' AND empty(.w_DDDTOBSO),1,0)
          .w_DDTIPINF = NVL(DDTIPINF,space(3))
          .w_DDCODEST = NVL(DDCODEST,space(7))
          .w_DDCODCLA = NVL(DDCODCLA,space(5))
          .w_DDCODPEC = NVL(DDCODPEC,space(0))
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRESI = .w_TOTRESI+.w_SEDERESI
          replace DDCODDES with .w_DDCODDES
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLSGNNRE = this.oParentObject.w_ANFLSGRE
        .w_CONTRESI = .w_TOTRESI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_39.enabled = .oPgFrm.Page1.oPag.oBtn_2_39.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_44.enabled = .oPgFrm.Page1.oPag.oBtn_2_44.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DDTIPCON=space(1)
      .w_DDCODICE=space(15)
      .w_DDCODDES=space(5)
      .w_DDNOMDES=space(40)
      .w_DDTIPRIF=space(2)
      .w_DDPERSON=space(40)
      .w_DDTELEFO=space(18)
      .w_DD__NOTE=space(40)
      .w_DDNUMFAX=space(18)
      .w_DDINDIRI=space(35)
      .w_DDDTOBSO=ctod("  /  /  ")
      .w_DD_EMAIL=space(254)
      .w_DD_EMPEC=space(254)
      .w_DD___CAP=space(8)
      .w_DDLOCALI=space(30)
      .w_DDPROVIN=space(2)
      .w_DDCODNAZ=space(3)
      .w_DESNAZ=space(35)
      .w_DDCODVET=space(5)
      .w_DESVET=space(35)
      .w_DDCODPOR=space(1)
      .w_DESPOR=space(30)
      .w_DDCODSPE=space(3)
      .w_DDCATOPE=space(2)
      .w_DDCODAGE=space(5)
      .w_DDTIPOPE=space(10)
      .w_DESSPE=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DDPREDEF=space(1)
      .w_DESAGE=space(35)
      .w_DTOBSO=ctod("  /  /  ")
      .w_FLSGNNRE=space(1)
      .w_TIDESCRI=space(30)
      .w_TIDTOBSO=ctod("  /  /  ")
      .w_DDMCCODI=space(5)
      .w_DDMCCODT=space(5)
      .w_DESMCIMB=space(35)
      .w_DESMCTRA=space(35)
      .w_DDRFDICH=space(15)
      .w_NUMDIC=0
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_SERDIC=space(3)
      .w_DDTELFAX=space(18)
      .w_CONTRESI=0
      .w_TOTRESI=0
      .w_SEDERESI=0
      .w_DDTIPINF=space(3)
      .w_DDCODEST=space(7)
      .w_DDCODCLA=space(5)
      .w_DDCODPEC=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_DDTIPRIF = 'GE'
        .DoRTCalc(6,11,.f.)
        .w_DD_EMAIL = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMAIL, ' ')), .oParentObject .w_AN_EMAIL, .w_DD_EMAIL ), SPACE(25))
        .w_DD_EMPEC = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMPEC, ' ')), .oParentObject .w_AN_EMPEC, .w_DD_EMPEC ), SPACE(25))
        .DoRTCalc(14,17,.f.)
        if not(empty(.w_DDCODNAZ))
         .link_2_15('Full')
        endif
        .DoRTCalc(18,18,.f.)
        .w_DDCODVET = IIF(.w_DDTIPRIF<>'SB', .w_DDCODVET, SPACE(5))
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_DDCODVET))
         .link_2_17('Full')
        endif
        .DoRTCalc(20,20,.f.)
        .w_DDCODPOR = IIF(.w_DDTIPRIF<>'SB', .w_DDCODPOR, ' ')
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_DDCODPOR))
         .link_2_19('Full')
        endif
        .DoRTCalc(22,22,.f.)
        .w_DDCODSPE = IIF( .w_DDTIPRIF <> 'SB', .w_DDCODSPE, SPACE(3) )
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_DDCODSPE))
         .link_2_21('Full')
        endif
        .w_DDCATOPE = 'OP'
        .w_DDCODAGE = IIF(.w_DDTIPRIF='CO', .w_DDCODAGE,SPACE(5))
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_DDCODAGE))
         .link_2_23('Full')
        endif
        .w_DDTIPOPE = SPACE(10)
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_DDTIPOPE))
         .link_2_24('Full')
        endif
        .DoRTCalc(27,27,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(29,29,.f.)
        .w_DDPREDEF = 'N'
        .DoRTCalc(31,32,.f.)
        .w_FLSGNNRE = this.oParentObject.w_ANFLSGRE
        .DoRTCalc(34,35,.f.)
        .w_DDMCCODI = SPACE(5)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_DDMCCODI))
         .link_2_34('Full')
        endif
        .w_DDMCCODT = SPACE(5)
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_DDMCCODT))
         .link_2_35('Full')
        endif
        .DoRTCalc(38,40,.f.)
        if not(empty(.w_DDRFDICH))
         .link_2_38('Full')
        endif
        .DoRTCalc(41,44,.f.)
        .w_DDTELFAX = .w_DDNUMFAX
        .w_CONTRESI = .w_TOTRESI
        .DoRTCalc(47,47,.f.)
        .w_SEDERESI = IIF(.w_DDTIPRIF='RE' AND empty(.w_DDDTOBSO),1,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(49,49,.f.)
        .w_DDCODEST = Space(7)
        .w_DDCODCLA = Space(5)
        .w_DDCODPEC = ' '
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
      endif
    endwith
    cp_BlankRecExtFlds(this,'DES_DIVE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled = this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_44.enabled = this.oPgFrm.Page1.oPag.oBtn_2_44.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDDPERSON_2_4.enabled = i_bVal
      .Page1.oPag.oDDTELEFO_2_5.enabled = i_bVal
      .Page1.oPag.oDD__NOTE_2_6.enabled = i_bVal
      .Page1.oPag.oDDNUMFAX_2_7.enabled = i_bVal
      .Page1.oPag.oDDINDIRI_2_8.enabled = i_bVal
      .Page1.oPag.oDDDTOBSO_2_9.enabled = i_bVal
      .Page1.oPag.oDD_EMAIL_2_10.enabled = i_bVal
      .Page1.oPag.oDD_EMPEC_2_11.enabled = i_bVal
      .Page1.oPag.oDD___CAP_2_12.enabled = i_bVal
      .Page1.oPag.oDDLOCALI_2_13.enabled = i_bVal
      .Page1.oPag.oDDPROVIN_2_14.enabled = i_bVal
      .Page1.oPag.oDDCODNAZ_2_15.enabled = i_bVal
      .Page1.oPag.oDDCODVET_2_17.enabled = i_bVal
      .Page1.oPag.oDDCODPOR_2_19.enabled = i_bVal
      .Page1.oPag.oDDCODSPE_2_21.enabled = i_bVal
      .Page1.oPag.oDDCODAGE_2_23.enabled = i_bVal
      .Page1.oPag.oDDTIPOPE_2_24.enabled = i_bVal
      .Page1.oPag.oDDMCCODI_2_34.enabled = i_bVal
      .Page1.oPag.oDDMCCODT_2_35.enabled = i_bVal
      .Page1.oPag.oCONTRESI_3_1.enabled = i_bVal
      .Page1.oPag.oDDCODEST_2_50.enabled = i_bVal
      .Page1.oPag.oDDCODCLA_2_51.enabled = i_bVal
      .Page1.oPag.oDDCODPEC_2_52.enabled = i_bVal
      .Page1.oPag.oBtn_2_39.enabled = .Page1.oPag.oBtn_2_39.mCond()
      .Page1.oPag.oBtn_2_44.enabled = .Page1.oPag.oBtn_2_44.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DES_DIVE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DDTIPCON,"DDTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DDCODICE,"DDCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DDCODDES C(5);
      ,t_DDNOMDES C(40);
      ,t_DDTIPRIF N(3);
      ,t_DDPERSON C(40);
      ,t_DDTELEFO C(18);
      ,t_DD__NOTE C(40);
      ,t_DDNUMFAX C(18);
      ,t_DDINDIRI C(35);
      ,t_DDDTOBSO D(8);
      ,t_DD_EMAIL C(254);
      ,t_DD_EMPEC C(254);
      ,t_DD___CAP C(8);
      ,t_DDLOCALI C(30);
      ,t_DDPROVIN C(2);
      ,t_DDCODNAZ C(3);
      ,t_DESNAZ C(35);
      ,t_DDCODVET C(5);
      ,t_DESVET C(35);
      ,t_DDCODPOR C(1);
      ,t_DESPOR C(30);
      ,t_DDCODSPE C(3);
      ,t_DDCODAGE C(5);
      ,t_DDTIPOPE C(10);
      ,t_DESSPE C(35);
      ,t_DDPREDEF N(3);
      ,t_DESAGE C(35);
      ,t_TIDESCRI C(30);
      ,t_DDMCCODI C(5);
      ,t_DDMCCODT C(5);
      ,t_DESMCIMB C(35);
      ,t_DESMCTRA C(35);
      ,t_NUMDIC N(6);
      ,t_ANNDIC C(4);
      ,t_DATDIC D(8);
      ,t_SERDIC C(3);
      ,t_DDTIPINF N(3);
      ,t_DDCODEST C(7);
      ,t_DDCODCLA C(5);
      ,t_DDCODPEC M(10);
      ,DDCODDES C(5);
      ,t_DDCATOPE C(2);
      ,t_OBTEST D(8);
      ,t_DATOBSO D(8);
      ,t_DTOBSO D(8);
      ,t_TIDTOBSO D(8);
      ,t_DDRFDICH C(15);
      ,t_DDTELFAX C(18);
      ,t_SEDERESI N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mddbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODDES_2_1.controlsource=this.cTrsName+'.t_DDCODDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDNOMDES_2_2.controlsource=this.cTrsName+'.t_DDNOMDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.controlsource=this.cTrsName+'.t_DDTIPRIF'
    this.oPgFRm.Page1.oPag.oDDPERSON_2_4.controlsource=this.cTrsName+'.t_DDPERSON'
    this.oPgFRm.Page1.oPag.oDDTELEFO_2_5.controlsource=this.cTrsName+'.t_DDTELEFO'
    this.oPgFRm.Page1.oPag.oDD__NOTE_2_6.controlsource=this.cTrsName+'.t_DD__NOTE'
    this.oPgFRm.Page1.oPag.oDDNUMFAX_2_7.controlsource=this.cTrsName+'.t_DDNUMFAX'
    this.oPgFRm.Page1.oPag.oDDINDIRI_2_8.controlsource=this.cTrsName+'.t_DDINDIRI'
    this.oPgFRm.Page1.oPag.oDDDTOBSO_2_9.controlsource=this.cTrsName+'.t_DDDTOBSO'
    this.oPgFRm.Page1.oPag.oDD_EMAIL_2_10.controlsource=this.cTrsName+'.t_DD_EMAIL'
    this.oPgFRm.Page1.oPag.oDD_EMPEC_2_11.controlsource=this.cTrsName+'.t_DD_EMPEC'
    this.oPgFRm.Page1.oPag.oDD___CAP_2_12.controlsource=this.cTrsName+'.t_DD___CAP'
    this.oPgFRm.Page1.oPag.oDDLOCALI_2_13.controlsource=this.cTrsName+'.t_DDLOCALI'
    this.oPgFRm.Page1.oPag.oDDPROVIN_2_14.controlsource=this.cTrsName+'.t_DDPROVIN'
    this.oPgFRm.Page1.oPag.oDDCODNAZ_2_15.controlsource=this.cTrsName+'.t_DDCODNAZ'
    this.oPgFRm.Page1.oPag.oDESNAZ_2_16.controlsource=this.cTrsName+'.t_DESNAZ'
    this.oPgFRm.Page1.oPag.oDDCODVET_2_17.controlsource=this.cTrsName+'.t_DDCODVET'
    this.oPgFRm.Page1.oPag.oDESVET_2_18.controlsource=this.cTrsName+'.t_DESVET'
    this.oPgFRm.Page1.oPag.oDDCODPOR_2_19.controlsource=this.cTrsName+'.t_DDCODPOR'
    this.oPgFRm.Page1.oPag.oDESPOR_2_20.controlsource=this.cTrsName+'.t_DESPOR'
    this.oPgFRm.Page1.oPag.oDDCODSPE_2_21.controlsource=this.cTrsName+'.t_DDCODSPE'
    this.oPgFRm.Page1.oPag.oDDCODAGE_2_23.controlsource=this.cTrsName+'.t_DDCODAGE'
    this.oPgFRm.Page1.oPag.oDDTIPOPE_2_24.controlsource=this.cTrsName+'.t_DDTIPOPE'
    this.oPgFRm.Page1.oPag.oDESSPE_2_25.controlsource=this.cTrsName+'.t_DESSPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.controlsource=this.cTrsName+'.t_DDPREDEF'
    this.oPgFRm.Page1.oPag.oDESAGE_2_29.controlsource=this.cTrsName+'.t_DESAGE'
    this.oPgFRm.Page1.oPag.oTIDESCRI_2_32.controlsource=this.cTrsName+'.t_TIDESCRI'
    this.oPgFRm.Page1.oPag.oDDMCCODI_2_34.controlsource=this.cTrsName+'.t_DDMCCODI'
    this.oPgFRm.Page1.oPag.oDDMCCODT_2_35.controlsource=this.cTrsName+'.t_DDMCCODT'
    this.oPgFRm.Page1.oPag.oDESMCIMB_2_36.controlsource=this.cTrsName+'.t_DESMCIMB'
    this.oPgFRm.Page1.oPag.oDESMCTRA_2_37.controlsource=this.cTrsName+'.t_DESMCTRA'
    this.oPgFRm.Page1.oPag.oNUMDIC_2_40.controlsource=this.cTrsName+'.t_NUMDIC'
    this.oPgFRm.Page1.oPag.oANNDIC_2_41.controlsource=this.cTrsName+'.t_ANNDIC'
    this.oPgFRm.Page1.oPag.oDATDIC_2_42.controlsource=this.cTrsName+'.t_DATDIC'
    this.oPgFRm.Page1.oPag.oSERDIC_2_43.controlsource=this.cTrsName+'.t_SERDIC'
    this.oPgFRm.Page1.oPag.oDDTIPINF_2_49.controlsource=this.cTrsName+'.t_DDTIPINF'
    this.oPgFRm.Page1.oPag.oDDCODEST_2_50.controlsource=this.cTrsName+'.t_DDCODEST'
    this.oPgFRm.Page1.oPag.oDDCODCLA_2_51.controlsource=this.cTrsName+'.t_DDCODCLA'
    this.oPgFRm.Page1.oPag.oDDCODPEC_2_52.controlsource=this.cTrsName+'.t_DDCODPEC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.AddVLine(363)
    this.AddVLine(489)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODDES_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
      *
      * insert into DES_DIVE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DES_DIVE')
        i_extval=cp_InsertValODBCExtFlds(this,'DES_DIVE')
        i_cFldBody=" "+;
                  "(DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF"+;
                  ",DDPERSON,DDTELEFO,DD__NOTE,DDNUMFAX,DDINDIRI"+;
                  ",DDDTOBSO,DD_EMAIL,DD_EMPEC,DD___CAP,DDLOCALI"+;
                  ",DDPROVIN,DDCODNAZ,DDCODVET,DDCODPOR,DDCODSPE"+;
                  ",DDCATOPE,DDCODAGE,DDTIPOPE,DDPREDEF,DDMCCODI"+;
                  ",DDMCCODT,DDRFDICH,DDTELFAX,DDTIPINF,DDCODEST"+;
                  ",DDCODCLA,DDCODPEC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DDTIPCON)+","+cp_ToStrODBC(this.w_DDCODICE)+","+cp_ToStrODBC(this.w_DDCODDES)+","+cp_ToStrODBC(this.w_DDNOMDES)+","+cp_ToStrODBC(this.w_DDTIPRIF)+;
             ","+cp_ToStrODBC(this.w_DDPERSON)+","+cp_ToStrODBC(this.w_DDTELEFO)+","+cp_ToStrODBC(this.w_DD__NOTE)+","+cp_ToStrODBC(this.w_DDNUMFAX)+","+cp_ToStrODBC(this.w_DDINDIRI)+;
             ","+cp_ToStrODBC(this.w_DDDTOBSO)+","+cp_ToStrODBC(this.w_DD_EMAIL)+","+cp_ToStrODBC(this.w_DD_EMPEC)+","+cp_ToStrODBC(this.w_DD___CAP)+","+cp_ToStrODBC(this.w_DDLOCALI)+;
             ","+cp_ToStrODBC(this.w_DDPROVIN)+","+cp_ToStrODBCNull(this.w_DDCODNAZ)+","+cp_ToStrODBCNull(this.w_DDCODVET)+","+cp_ToStrODBCNull(this.w_DDCODPOR)+","+cp_ToStrODBCNull(this.w_DDCODSPE)+;
             ","+cp_ToStrODBC(this.w_DDCATOPE)+","+cp_ToStrODBCNull(this.w_DDCODAGE)+","+cp_ToStrODBCNull(this.w_DDTIPOPE)+","+cp_ToStrODBC(this.w_DDPREDEF)+","+cp_ToStrODBCNull(this.w_DDMCCODI)+;
             ","+cp_ToStrODBCNull(this.w_DDMCCODT)+","+cp_ToStrODBCNull(this.w_DDRFDICH)+","+cp_ToStrODBC(this.w_DDTELFAX)+","+cp_ToStrODBC(this.w_DDTIPINF)+","+cp_ToStrODBC(this.w_DDCODEST)+;
             ","+cp_ToStrODBC(this.w_DDCODCLA)+","+cp_ToStrODBC(this.w_DDCODPEC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DES_DIVE')
        i_extval=cp_InsertValVFPExtFlds(this,'DES_DIVE')
        cp_CheckDeletedKey(i_cTable,0,'DDTIPCON',this.w_DDTIPCON,'DDCODICE',this.w_DDCODICE,'DDCODDES',this.w_DDCODDES)
        INSERT INTO (i_cTable) (;
                   DDTIPCON;
                  ,DDCODICE;
                  ,DDCODDES;
                  ,DDNOMDES;
                  ,DDTIPRIF;
                  ,DDPERSON;
                  ,DDTELEFO;
                  ,DD__NOTE;
                  ,DDNUMFAX;
                  ,DDINDIRI;
                  ,DDDTOBSO;
                  ,DD_EMAIL;
                  ,DD_EMPEC;
                  ,DD___CAP;
                  ,DDLOCALI;
                  ,DDPROVIN;
                  ,DDCODNAZ;
                  ,DDCODVET;
                  ,DDCODPOR;
                  ,DDCODSPE;
                  ,DDCATOPE;
                  ,DDCODAGE;
                  ,DDTIPOPE;
                  ,DDPREDEF;
                  ,DDMCCODI;
                  ,DDMCCODT;
                  ,DDRFDICH;
                  ,DDTELFAX;
                  ,DDTIPINF;
                  ,DDCODEST;
                  ,DDCODCLA;
                  ,DDCODPEC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DDTIPCON;
                  ,this.w_DDCODICE;
                  ,this.w_DDCODDES;
                  ,this.w_DDNOMDES;
                  ,this.w_DDTIPRIF;
                  ,this.w_DDPERSON;
                  ,this.w_DDTELEFO;
                  ,this.w_DD__NOTE;
                  ,this.w_DDNUMFAX;
                  ,this.w_DDINDIRI;
                  ,this.w_DDDTOBSO;
                  ,this.w_DD_EMAIL;
                  ,this.w_DD_EMPEC;
                  ,this.w_DD___CAP;
                  ,this.w_DDLOCALI;
                  ,this.w_DDPROVIN;
                  ,this.w_DDCODNAZ;
                  ,this.w_DDCODVET;
                  ,this.w_DDCODPOR;
                  ,this.w_DDCODSPE;
                  ,this.w_DDCATOPE;
                  ,this.w_DDCODAGE;
                  ,this.w_DDTIPOPE;
                  ,this.w_DDPREDEF;
                  ,this.w_DDMCCODI;
                  ,this.w_DDMCCODT;
                  ,this.w_DDRFDICH;
                  ,this.w_DDTELFAX;
                  ,this.w_DDTIPINF;
                  ,this.w_DDCODEST;
                  ,this.w_DDCODCLA;
                  ,this.w_DDCODPEC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DDCODDES<>SPACE(5)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DES_DIVE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DDCODDES="+cp_ToStrODBC(&i_TN.->DDCODDES)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DES_DIVE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DDCODDES=&i_TN.->DDCODDES;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DDCODDES<>SPACE(5)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DDCODDES="+cp_ToStrODBC(&i_TN.->DDCODDES)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DDCODDES=&i_TN.->DDCODDES;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DDCODDES with this.w_DDCODDES
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DES_DIVE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DES_DIVE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DDNOMDES="+cp_ToStrODBC(this.w_DDNOMDES)+;
                     ",DDTIPRIF="+cp_ToStrODBC(this.w_DDTIPRIF)+;
                     ",DDPERSON="+cp_ToStrODBC(this.w_DDPERSON)+;
                     ",DDTELEFO="+cp_ToStrODBC(this.w_DDTELEFO)+;
                     ",DD__NOTE="+cp_ToStrODBC(this.w_DD__NOTE)+;
                     ",DDNUMFAX="+cp_ToStrODBC(this.w_DDNUMFAX)+;
                     ",DDINDIRI="+cp_ToStrODBC(this.w_DDINDIRI)+;
                     ",DDDTOBSO="+cp_ToStrODBC(this.w_DDDTOBSO)+;
                     ",DD_EMAIL="+cp_ToStrODBC(this.w_DD_EMAIL)+;
                     ",DD_EMPEC="+cp_ToStrODBC(this.w_DD_EMPEC)+;
                     ",DD___CAP="+cp_ToStrODBC(this.w_DD___CAP)+;
                     ",DDLOCALI="+cp_ToStrODBC(this.w_DDLOCALI)+;
                     ",DDPROVIN="+cp_ToStrODBC(this.w_DDPROVIN)+;
                     ",DDCODNAZ="+cp_ToStrODBCNull(this.w_DDCODNAZ)+;
                     ",DDCODVET="+cp_ToStrODBCNull(this.w_DDCODVET)+;
                     ",DDCODPOR="+cp_ToStrODBCNull(this.w_DDCODPOR)+;
                     ",DDCODSPE="+cp_ToStrODBCNull(this.w_DDCODSPE)+;
                     ",DDCATOPE="+cp_ToStrODBC(this.w_DDCATOPE)+;
                     ",DDCODAGE="+cp_ToStrODBCNull(this.w_DDCODAGE)+;
                     ",DDTIPOPE="+cp_ToStrODBCNull(this.w_DDTIPOPE)+;
                     ",DDPREDEF="+cp_ToStrODBC(this.w_DDPREDEF)+;
                     ",DDMCCODI="+cp_ToStrODBCNull(this.w_DDMCCODI)+;
                     ",DDMCCODT="+cp_ToStrODBCNull(this.w_DDMCCODT)+;
                     ",DDRFDICH="+cp_ToStrODBCNull(this.w_DDRFDICH)+;
                     ",DDTELFAX="+cp_ToStrODBC(this.w_DDTELFAX)+;
                     ",DDTIPINF="+cp_ToStrODBC(this.w_DDTIPINF)+;
                     ",DDCODEST="+cp_ToStrODBC(this.w_DDCODEST)+;
                     ",DDCODCLA="+cp_ToStrODBC(this.w_DDCODCLA)+;
                     ",DDCODPEC="+cp_ToStrODBC(this.w_DDCODPEC)+;
                     ",DDCODDES="+cp_ToStrODBC(this.w_DDCODDES)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DDCODDES="+cp_ToStrODBC(DDCODDES)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DES_DIVE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DDNOMDES=this.w_DDNOMDES;
                     ,DDTIPRIF=this.w_DDTIPRIF;
                     ,DDPERSON=this.w_DDPERSON;
                     ,DDTELEFO=this.w_DDTELEFO;
                     ,DD__NOTE=this.w_DD__NOTE;
                     ,DDNUMFAX=this.w_DDNUMFAX;
                     ,DDINDIRI=this.w_DDINDIRI;
                     ,DDDTOBSO=this.w_DDDTOBSO;
                     ,DD_EMAIL=this.w_DD_EMAIL;
                     ,DD_EMPEC=this.w_DD_EMPEC;
                     ,DD___CAP=this.w_DD___CAP;
                     ,DDLOCALI=this.w_DDLOCALI;
                     ,DDPROVIN=this.w_DDPROVIN;
                     ,DDCODNAZ=this.w_DDCODNAZ;
                     ,DDCODVET=this.w_DDCODVET;
                     ,DDCODPOR=this.w_DDCODPOR;
                     ,DDCODSPE=this.w_DDCODSPE;
                     ,DDCATOPE=this.w_DDCATOPE;
                     ,DDCODAGE=this.w_DDCODAGE;
                     ,DDTIPOPE=this.w_DDTIPOPE;
                     ,DDPREDEF=this.w_DDPREDEF;
                     ,DDMCCODI=this.w_DDMCCODI;
                     ,DDMCCODT=this.w_DDMCCODT;
                     ,DDRFDICH=this.w_DDRFDICH;
                     ,DDTELFAX=this.w_DDTELFAX;
                     ,DDTIPINF=this.w_DDTIPINF;
                     ,DDCODEST=this.w_DDCODEST;
                     ,DDCODCLA=this.w_DDCODCLA;
                     ,DDCODPEC=this.w_DDCODPEC;
                     ,DDCODDES=this.w_DDCODDES;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DDCODDES=&i_TN.->DDCODDES;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DDCODDES<>SPACE(5)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DES_DIVE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DDCODDES="+cp_ToStrODBC(&i_TN.->DDCODDES)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DDCODDES=&i_TN.->DDCODDES;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DDCODDES<>SPACE(5)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DD_EMAIL = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMAIL, ' ')), .oParentObject .w_AN_EMAIL, .w_DD_EMAIL ), SPACE(25))
        endif
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DD_EMPEC = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMPEC, ' ')), .oParentObject .w_AN_EMPEC, .w_DD_EMPEC ), SPACE(25))
        endif
        .DoRTCalc(14,18,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODVET = IIF(.w_DDTIPRIF<>'SB', .w_DDCODVET, SPACE(5))
          .link_2_17('Full')
        endif
        .DoRTCalc(20,20,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODPOR = IIF(.w_DDTIPRIF<>'SB', .w_DDCODPOR, ' ')
          .link_2_19('Full')
        endif
        .DoRTCalc(22,22,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODSPE = IIF( .w_DDTIPRIF <> 'SB', .w_DDCODSPE, SPACE(3) )
          .link_2_21('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODAGE = IIF(.w_DDTIPRIF='CO', .w_DDCODAGE,SPACE(5))
          .link_2_23('Full')
        endif
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDTIPOPE = SPACE(10)
          .link_2_24('Full')
        endif
        .DoRTCalc(27,29,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDPREDEF = 'N'
        endif
        if .o_DDCODDES<>.w_DDCODDES
          .Calculate_QPBYLYTGJU()
        endif
        .DoRTCalc(31,32,.t.)
          .w_FLSGNNRE = this.oParentObject.w_ANFLSGRE
        .DoRTCalc(34,35,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDMCCODI = SPACE(5)
          .link_2_34('Full')
        endif
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDMCCODT = SPACE(5)
          .link_2_35('Full')
        endif
        .DoRTCalc(38,39,.t.)
          .link_2_38('Full')
        .DoRTCalc(41,44,.t.)
        if .o_DDNUMFAX<>.w_DDNUMFAX
          .w_DDTELFAX = .w_DDNUMFAX
        endif
        if .o_TOTRESI<>.w_TOTRESI
          .w_CONTRESI = .w_TOTRESI
        endif
        .DoRTCalc(47,47,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF.or. .o_DDDTOBSO<>.w_DDDTOBSO
          .w_TOTRESI = .w_TOTRESI-.w_sederesi
          .w_SEDERESI = IIF(.w_DDTIPRIF='RE' AND empty(.w_DDDTOBSO),1,0)
          .w_TOTRESI = .w_TOTRESI+.w_sederesi
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(49,49,.t.)
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODEST = Space(7)
        endif
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODCLA = Space(5)
        endif
        if .o_DDTIPRIF<>.w_DDTIPRIF
          .w_DDCODPEC = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DDCATOPE with this.w_DDCATOPE
      replace t_OBTEST with this.w_OBTEST
      replace t_DATOBSO with this.w_DATOBSO
      replace t_DTOBSO with this.w_DTOBSO
      replace t_TIDTOBSO with this.w_TIDTOBSO
      replace t_DDRFDICH with this.w_DDRFDICH
      replace t_DDTELFAX with this.w_DDTELFAX
      replace t_SEDERESI with this.w_SEDERESI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
    endwith
  return
  proc Calculate_QPBYLYTGJU()
    with this
          * --- Simulo zerofill
          .w_DDCODDES = IIF( ( Left( Alltrim( .w_DDCODDES ) ,1 ) <>"0" And  Len( Alltrim( .w_DDCODDES ) )=5 ) Or Left( Alltrim( .w_DDCODDES ) ,1 ) ="0" , .w_DDCODDES , Right( Repl('0',5)+Alltrim( .w_DDCODDES ) ,5 )  )
    endwith
  endproc
  proc Calculate_DDBLIGHKTT()
    with this
          * --- Eliminazione valori dichiarazione di intento
          .w_DDRFDICH = SPACE(10)
          .w_NUMDIC = 0
          .w_SERDIC = space(3)
          .w_ANNDIC = space(4)
          .w_DATDIC = cp_CharToDate('  /  /    ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDNOMDES_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDNOMDES_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDTIPRIF_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDTIPRIF_2_3.mCond()
    this.oPgFrm.Page1.oPag.oDDPERSON_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDPERSON_2_4.mCond()
    this.oPgFrm.Page1.oPag.oDDTELEFO_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDTELEFO_2_5.mCond()
    this.oPgFrm.Page1.oPag.oDD__NOTE_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDD__NOTE_2_6.mCond()
    this.oPgFrm.Page1.oPag.oDDNUMFAX_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDNUMFAX_2_7.mCond()
    this.oPgFrm.Page1.oPag.oDDINDIRI_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDINDIRI_2_8.mCond()
    this.oPgFrm.Page1.oPag.oDDDTOBSO_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDDTOBSO_2_9.mCond()
    this.oPgFrm.Page1.oPag.oDD_EMAIL_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDD_EMAIL_2_10.mCond()
    this.oPgFrm.Page1.oPag.oDD_EMPEC_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDD_EMPEC_2_11.mCond()
    this.oPgFrm.Page1.oPag.oDD___CAP_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDD___CAP_2_12.mCond()
    this.oPgFrm.Page1.oPag.oDDLOCALI_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDLOCALI_2_13.mCond()
    this.oPgFrm.Page1.oPag.oDDPROVIN_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDPROVIN_2_14.mCond()
    this.oPgFrm.Page1.oPag.oDDCODNAZ_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODNAZ_2_15.mCond()
    this.oPgFrm.Page1.oPag.oDDCODVET_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODVET_2_17.mCond()
    this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.mCond()
    this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.mCond()
    this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.mCond()
    this.oPgFrm.Page1.oPag.oDDTIPOPE_2_24.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDTIPOPE_2_24.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDPREDEF_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDDPREDEF_2_28.mCond()
    this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.mCond()
    this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.mCond()
    this.oPgFrm.Page1.oPag.oDDCODEST_2_50.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODEST_2_50.mCond()
    this.oPgFrm.Page1.oPag.oDDCODCLA_2_51.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODCLA_2_51.mCond()
    this.oPgFrm.Page1.oPag.oDDCODPEC_2_52.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDDCODPEC_2_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled =this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_44.enabled =this.oPgFrm.Page1.oPag.oBtn_2_44.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDDCODVET_2_17.visible=!this.oPgFrm.Page1.oPag.oDDCODVET_2_17.mHide()
    this.oPgFrm.Page1.oPag.oDESVET_2_18.visible=!this.oPgFrm.Page1.oPag.oDESVET_2_18.mHide()
    this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.visible=!this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.mHide()
    this.oPgFrm.Page1.oPag.oDESPOR_2_20.visible=!this.oPgFrm.Page1.oPag.oDESPOR_2_20.mHide()
    this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.visible=!this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.mHide()
    this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.visible=!this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.mHide()
    this.oPgFrm.Page1.oPag.oDESSPE_2_25.visible=!this.oPgFrm.Page1.oPag.oDESSPE_2_25.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE_2_29.visible=!this.oPgFrm.Page1.oPag.oDESAGE_2_29.mHide()
    this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.visible=!this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.mHide()
    this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.visible=!this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.mHide()
    this.oPgFrm.Page1.oPag.oDESMCIMB_2_36.visible=!this.oPgFrm.Page1.oPag.oDESMCIMB_2_36.mHide()
    this.oPgFrm.Page1.oPag.oDESMCTRA_2_37.visible=!this.oPgFrm.Page1.oPag.oDESMCTRA_2_37.mHide()
    this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.visible=!this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Elimina")
          .Calculate_DDBLIGHKTT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_53.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DDCODNAZ
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_DDCODNAZ)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_DDCODNAZ))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDCODNAZ)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDCODNAZ) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oDDCODNAZ_2_15'),i_cWhere,'GSAR_ANZ',"Elenco nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_DDCODNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_DDCODNAZ)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODNAZ = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODNAZ = space(3)
      endif
      this.w_DESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.NACODNAZ as NACODNAZ215"+ ",link_2_15.NADESNAZ as NADESNAZ215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on DES_DIVE.DDCODNAZ=link_2_15.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and DES_DIVE.DDCODNAZ=link_2_15.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDCODVET
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_DDCODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_DDCODVET))
          select VTCODVET,VTDESVET,VTDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDCODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_DDCODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_DDCODVET)+"%");

            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DDCODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oDDCODVET_2_17'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_DDCODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_DDCODVET)
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VTDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODVET = space(5)
      endif
      this.w_DESVET = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice vettore inesistente oppure obsoleto")
        endif
        this.w_DDCODVET = space(5)
        this.w_DESVET = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VETTORI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.VTCODVET as VTCODVET217"+ ",link_2_17.VTDESVET as VTDESVET217"+ ",link_2_17.VTDTOBSO as VTDTOBSO217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on DES_DIVE.DDCODVET=link_2_17.VTCODVET"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and DES_DIVE.DDCODVET=link_2_17.VTCODVET(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDCODPOR
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APO',True,'PORTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" POCODPOR like "+cp_ToStrODBC(trim(this.w_DDCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by POCODPOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'POCODPOR',trim(this.w_DDCODPOR))
          select POCODPOR,PODESPOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by POCODPOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDCODPOR)==trim(_Link_.POCODPOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDCODPOR) and !this.bDontReportError
            deferred_cp_zoom('PORTI','*','POCODPOR',cp_AbsName(oSource.parent,'oDDCODPOR_2_19'),i_cWhere,'GSAR_APO',"Porti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                     +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',oSource.xKey(1))
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_DDCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_DDCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_DESPOR = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODPOR = space(1)
      endif
      this.w_DESPOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.POCODPOR as POCODPOR219"+ ","+cp_TransLinkFldName('link_2_19.PODESPOR')+" as PODESPOR219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on DES_DIVE.DDCODPOR=link_2_19.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and DES_DIVE.DDCODPOR=link_2_19.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDCODSPE
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_DDCODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_DDCODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDCODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDCODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oDDCODSPE_2_21'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_DDCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_DDCODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_21.SPCODSPE as SPCODSPE221"+ ","+cp_TransLinkFldName('link_2_21.SPDESSPE')+" as SPDESSPE221"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_21 on DES_DIVE.DDCODSPE=link_2_21.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_21"
          i_cKey=i_cKey+'+" and DES_DIVE.DDCODSPE=link_2_21.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDCODAGE
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_DDCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_DDCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_DDCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_DDCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DDCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oDDCODAGE_2_23'),i_cWhere,'GSAR_AGE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_DDCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_DDCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DDCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_23.AGCODAGE as AGCODAGE223"+ ",link_2_23.AGDESAGE as AGDESAGE223"+ ",link_2_23.AGDTOBSO as AGDTOBSO223"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_23 on DES_DIVE.DDCODAGE=link_2_23.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_23"
          i_cKey=i_cKey+'+" and DES_DIVE.DDCODAGE=link_2_23.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDTIPOPE
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_DDTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_DDCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_DDCATOPE;
                     ,'TICODICE',trim(this.w_DDTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oDDTIPOPE_2_24'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DDCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_DDCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_DDTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_DDCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_DDCATOPE;
                       ,'TICODICE',this.w_DDTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_TIDTOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DDTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_TIDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_DDTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_TIDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DDMCCODI
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDMCCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_DDMCCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_DDMCCODI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDMCCODI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDMCCODI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oDDMCCODI_2_34'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDMCCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_DDMCCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_DDMCCODI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDMCCODI = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCIMB = NVL(_Link_.MSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DDMCCODI = space(5)
      endif
      this.w_DESMCIMB = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDMCCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_34.MSCODICE as MSCODICE234"+ ",link_2_34.MSDESCRI as MSDESCRI234"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_34 on DES_DIVE.DDMCCODI=link_2_34.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_34"
          i_cKey=i_cKey+'+" and DES_DIVE.DDMCCODI=link_2_34.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDMCCODT
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDMCCODT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_DDMCCODT)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_DDMCCODT))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DDMCCODT)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DDMCCODT) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oDDMCCODT_2_35'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDMCCODT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_DDMCCODT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_DDMCCODT)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDMCCODT = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCTRA = NVL(_Link_.MSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DDMCCODT = space(5)
      endif
      this.w_DESMCTRA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDMCCODT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_35.MSCODICE as MSCODICE235"+ ",link_2_35.MSDESCRI as MSDESCRI235"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_35 on DES_DIVE.DDMCCODT=link_2_35.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_35"
          i_cKey=i_cKey+'+" and DES_DIVE.DDMCCODT=link_2_35.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDRFDICH
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_lTable = "DIC_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2], .t., this.DIC_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDRFDICH) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDRFDICH)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DI__ANNO,DINUMDOC,DIDATDIC,DISERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(this.w_DDRFDICH);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',this.w_DDRFDICH)
            select DISERIAL,DI__ANNO,DINUMDOC,DIDATDIC,DISERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDRFDICH = NVL(_Link_.DISERIAL,space(15))
      this.w_ANNDIC = NVL(_Link_.DI__ANNO,space(4))
      this.w_NUMDIC = NVL(_Link_.DINUMDOC,0)
      this.w_DATDIC = NVL(cp_ToDate(_Link_.DIDATDIC),ctod("  /  /  "))
      this.w_SERDIC = NVL(_Link_.DISERDOC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DDRFDICH = space(15)
      endif
      this.w_ANNDIC = space(4)
      this.w_NUMDIC = 0
      this.w_DATDIC = ctod("  /  /  ")
      this.w_SERDIC = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])+'\'+cp_ToStr(_Link_.DISERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDRFDICH Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIC_INTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_38.DISERIAL as DISERIAL238"+ ",link_2_38.DI__ANNO as DI__ANNO238"+ ",link_2_38.DINUMDOC as DINUMDOC238"+ ",link_2_38.DIDATDIC as DIDATDIC238"+ ",link_2_38.DISERDOC as DISERDOC238"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_38 on DES_DIVE.DDRFDICH=link_2_38.DISERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_38"
          i_cKey=i_cKey+'+" and DES_DIVE.DDRFDICH=link_2_38.DISERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDDPERSON_2_4.value==this.w_DDPERSON)
      this.oPgFrm.Page1.oPag.oDDPERSON_2_4.value=this.w_DDPERSON
      replace t_DDPERSON with this.oPgFrm.Page1.oPag.oDDPERSON_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDTELEFO_2_5.value==this.w_DDTELEFO)
      this.oPgFrm.Page1.oPag.oDDTELEFO_2_5.value=this.w_DDTELEFO
      replace t_DDTELEFO with this.oPgFrm.Page1.oPag.oDDTELEFO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDD__NOTE_2_6.value==this.w_DD__NOTE)
      this.oPgFrm.Page1.oPag.oDD__NOTE_2_6.value=this.w_DD__NOTE
      replace t_DD__NOTE with this.oPgFrm.Page1.oPag.oDD__NOTE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDNUMFAX_2_7.value==this.w_DDNUMFAX)
      this.oPgFrm.Page1.oPag.oDDNUMFAX_2_7.value=this.w_DDNUMFAX
      replace t_DDNUMFAX with this.oPgFrm.Page1.oPag.oDDNUMFAX_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDINDIRI_2_8.value==this.w_DDINDIRI)
      this.oPgFrm.Page1.oPag.oDDINDIRI_2_8.value=this.w_DDINDIRI
      replace t_DDINDIRI with this.oPgFrm.Page1.oPag.oDDINDIRI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDDTOBSO_2_9.value==this.w_DDDTOBSO)
      this.oPgFrm.Page1.oPag.oDDDTOBSO_2_9.value=this.w_DDDTOBSO
      replace t_DDDTOBSO with this.oPgFrm.Page1.oPag.oDDDTOBSO_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDD_EMAIL_2_10.value==this.w_DD_EMAIL)
      this.oPgFrm.Page1.oPag.oDD_EMAIL_2_10.value=this.w_DD_EMAIL
      replace t_DD_EMAIL with this.oPgFrm.Page1.oPag.oDD_EMAIL_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDD_EMPEC_2_11.value==this.w_DD_EMPEC)
      this.oPgFrm.Page1.oPag.oDD_EMPEC_2_11.value=this.w_DD_EMPEC
      replace t_DD_EMPEC with this.oPgFrm.Page1.oPag.oDD_EMPEC_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDD___CAP_2_12.value==this.w_DD___CAP)
      this.oPgFrm.Page1.oPag.oDD___CAP_2_12.value=this.w_DD___CAP
      replace t_DD___CAP with this.oPgFrm.Page1.oPag.oDD___CAP_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDLOCALI_2_13.value==this.w_DDLOCALI)
      this.oPgFrm.Page1.oPag.oDDLOCALI_2_13.value=this.w_DDLOCALI
      replace t_DDLOCALI with this.oPgFrm.Page1.oPag.oDDLOCALI_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDPROVIN_2_14.value==this.w_DDPROVIN)
      this.oPgFrm.Page1.oPag.oDDPROVIN_2_14.value=this.w_DDPROVIN
      replace t_DDPROVIN with this.oPgFrm.Page1.oPag.oDDPROVIN_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODNAZ_2_15.value==this.w_DDCODNAZ)
      this.oPgFrm.Page1.oPag.oDDCODNAZ_2_15.value=this.w_DDCODNAZ
      replace t_DDCODNAZ with this.oPgFrm.Page1.oPag.oDDCODNAZ_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_2_16.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_2_16.value=this.w_DESNAZ
      replace t_DESNAZ with this.oPgFrm.Page1.oPag.oDESNAZ_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODVET_2_17.value==this.w_DDCODVET)
      this.oPgFrm.Page1.oPag.oDDCODVET_2_17.value=this.w_DDCODVET
      replace t_DDCODVET with this.oPgFrm.Page1.oPag.oDDCODVET_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVET_2_18.value==this.w_DESVET)
      this.oPgFrm.Page1.oPag.oDESVET_2_18.value=this.w_DESVET
      replace t_DESVET with this.oPgFrm.Page1.oPag.oDESVET_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.value==this.w_DDCODPOR)
      this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.value=this.w_DDCODPOR
      replace t_DDCODPOR with this.oPgFrm.Page1.oPag.oDDCODPOR_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPOR_2_20.value==this.w_DESPOR)
      this.oPgFrm.Page1.oPag.oDESPOR_2_20.value=this.w_DESPOR
      replace t_DESPOR with this.oPgFrm.Page1.oPag.oDESPOR_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.value==this.w_DDCODSPE)
      this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.value=this.w_DDCODSPE
      replace t_DDCODSPE with this.oPgFrm.Page1.oPag.oDDCODSPE_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.value==this.w_DDCODAGE)
      this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.value=this.w_DDCODAGE
      replace t_DDCODAGE with this.oPgFrm.Page1.oPag.oDDCODAGE_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDTIPOPE_2_24.value==this.w_DDTIPOPE)
      this.oPgFrm.Page1.oPag.oDDTIPOPE_2_24.value=this.w_DDTIPOPE
      replace t_DDTIPOPE with this.oPgFrm.Page1.oPag.oDDTIPOPE_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSPE_2_25.value==this.w_DESSPE)
      this.oPgFrm.Page1.oPag.oDESSPE_2_25.value=this.w_DESSPE
      replace t_DESSPE with this.oPgFrm.Page1.oPag.oDESSPE_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_2_29.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_2_29.value=this.w_DESAGE
      replace t_DESAGE with this.oPgFrm.Page1.oPag.oDESAGE_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_2_32.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_2_32.value=this.w_TIDESCRI
      replace t_TIDESCRI with this.oPgFrm.Page1.oPag.oTIDESCRI_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.value==this.w_DDMCCODI)
      this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.value=this.w_DDMCCODI
      replace t_DDMCCODI with this.oPgFrm.Page1.oPag.oDDMCCODI_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.value==this.w_DDMCCODT)
      this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.value=this.w_DDMCCODT
      replace t_DDMCCODT with this.oPgFrm.Page1.oPag.oDDMCCODT_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCIMB_2_36.value==this.w_DESMCIMB)
      this.oPgFrm.Page1.oPag.oDESMCIMB_2_36.value=this.w_DESMCIMB
      replace t_DESMCIMB with this.oPgFrm.Page1.oPag.oDESMCIMB_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCTRA_2_37.value==this.w_DESMCTRA)
      this.oPgFrm.Page1.oPag.oDESMCTRA_2_37.value=this.w_DESMCTRA
      replace t_DESMCTRA with this.oPgFrm.Page1.oPag.oDESMCTRA_2_37.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_2_40.value==this.w_NUMDIC)
      this.oPgFrm.Page1.oPag.oNUMDIC_2_40.value=this.w_NUMDIC
      replace t_NUMDIC with this.oPgFrm.Page1.oPag.oNUMDIC_2_40.value
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIC_2_41.value==this.w_ANNDIC)
      this.oPgFrm.Page1.oPag.oANNDIC_2_41.value=this.w_ANNDIC
      replace t_ANNDIC with this.oPgFrm.Page1.oPag.oANNDIC_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_2_42.value==this.w_DATDIC)
      this.oPgFrm.Page1.oPag.oDATDIC_2_42.value=this.w_DATDIC
      replace t_DATDIC with this.oPgFrm.Page1.oPag.oDATDIC_2_42.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDIC_2_43.value==this.w_SERDIC)
      this.oPgFrm.Page1.oPag.oSERDIC_2_43.value=this.w_SERDIC
      replace t_SERDIC with this.oPgFrm.Page1.oPag.oSERDIC_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTRESI_3_1.value==this.w_CONTRESI)
      this.oPgFrm.Page1.oPag.oCONTRESI_3_1.value=this.w_CONTRESI
    endif
    if not(this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.RadioValue()==this.w_DDTIPINF)
      this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.SetRadio()
      replace t_DDTIPINF with this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODEST_2_50.value==this.w_DDCODEST)
      this.oPgFrm.Page1.oPag.oDDCODEST_2_50.value=this.w_DDCODEST
      replace t_DDCODEST with this.oPgFrm.Page1.oPag.oDDCODEST_2_50.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODCLA_2_51.value==this.w_DDCODCLA)
      this.oPgFrm.Page1.oPag.oDDCODCLA_2_51.value=this.w_DDCODCLA
      replace t_DDCODCLA with this.oPgFrm.Page1.oPag.oDDCODCLA_2_51.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDDCODPEC_2_52.value==this.w_DDCODPEC)
      this.oPgFrm.Page1.oPag.oDDCODPEC_2_52.value=this.w_DDCODPEC
      replace t_DDCODPEC with this.oPgFrm.Page1.oPag.oDDCODPEC_2_52.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODDES_2_1.value==this.w_DDCODDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODDES_2_1.value=this.w_DDCODDES
      replace t_DDCODDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODDES_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDNOMDES_2_2.value==this.w_DDNOMDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDNOMDES_2_2.value=this.w_DDNOMDES
      replace t_DDNOMDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDNOMDES_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.RadioValue()==this.w_DDTIPRIF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.SetRadio()
      replace t_DDTIPRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.RadioValue()==this.w_DDPREDEF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.SetRadio()
      replace t_DDPREDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.value
    endif
    cp_SetControlsValueExtFlds(this,'DES_DIVE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TOTRESI<=1)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCONTRESI_3_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione: sono state inserite pi� sedi di tipo residenza con data di obsolescenza non valorizzata")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(NOT(.w_FLSGNNRE='N' AND .w_DDTIPRIF='SB'AND (EMPTY(.w_DDDTOBSO) OR .w_DDDTOBSO>i_datsys)) OR .w_FLSGNNRE='S')
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Impossibile salvare sede per soggetti con flag non residenti non impostato")
        case   not(NOT(.w_FLSGNNRE='N' AND .w_DDTIPRIF='SB'AND (EMPTY(.w_DDDTOBSO) OR .w_DDDTOBSO>i_datsys)) OR .w_FLSGNNRE='S') and (Not Empty(.w_DDCODDES)) and (.w_DDCODDES<>SPACE(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impossibile impostare tipologia sede per soggetti con flag non residenti non impostato")
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES)) and not(empty(.w_DDCODVET)) and (.w_DDCODDES<>SPACE(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oDDCODVET_2_17
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice vettore inesistente oppure obsoleto")
        case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) and (.w_DDTIPRIF='CO' And Not Empty(.w_DDCODDES)) and not(empty(.w_DDCODAGE)) and (.w_DDCODDES<>SPACE(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oDDCODAGE_2_23
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST)) and (.w_DDTIPRIF='CO' and Not Empty(.w_DDCODDES)) and not(empty(.w_DDTIPOPE)) and (.w_DDCODDES<>SPACE(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oDDTIPOPE_2_24
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
      endcase
      if .w_DDCODDES<>SPACE(5)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DDCODDES = this.w_DDCODDES
    this.o_DDTIPRIF = this.w_DDTIPRIF
    this.o_DDNUMFAX = this.w_DDNUMFAX
    this.o_DDDTOBSO = this.w_DDDTOBSO
    this.o_TOTRESI = this.w_TOTRESI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DDCODDES<>SPACE(5))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=CHK_SEDI(this , t_DDCODDES)
    if !i_bRes
      cp_ErrorMsg("Impossibile cancellare le sedi movimentate ","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DDCODDES=space(5)
      .w_DDNOMDES=space(40)
      .w_DDTIPRIF=space(2)
      .w_DDPERSON=space(40)
      .w_DDTELEFO=space(18)
      .w_DD__NOTE=space(40)
      .w_DDNUMFAX=space(18)
      .w_DDINDIRI=space(35)
      .w_DDDTOBSO=ctod("  /  /  ")
      .w_DD_EMAIL=space(254)
      .w_DD_EMPEC=space(254)
      .w_DD___CAP=space(8)
      .w_DDLOCALI=space(30)
      .w_DDPROVIN=space(2)
      .w_DDCODNAZ=space(3)
      .w_DESNAZ=space(35)
      .w_DDCODVET=space(5)
      .w_DESVET=space(35)
      .w_DDCODPOR=space(1)
      .w_DESPOR=space(30)
      .w_DDCODSPE=space(3)
      .w_DDCATOPE=space(2)
      .w_DDCODAGE=space(5)
      .w_DDTIPOPE=space(10)
      .w_DESSPE=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DDPREDEF=space(1)
      .w_DESAGE=space(35)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TIDESCRI=space(30)
      .w_TIDTOBSO=ctod("  /  /  ")
      .w_DDMCCODI=space(5)
      .w_DDMCCODT=space(5)
      .w_DESMCIMB=space(35)
      .w_DESMCTRA=space(35)
      .w_DDRFDICH=space(15)
      .w_NUMDIC=0
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_SERDIC=space(3)
      .w_DDTELFAX=space(18)
      .w_SEDERESI=0
      .w_DDTIPINF=space(3)
      .w_DDCODEST=space(7)
      .w_DDCODCLA=space(5)
      .w_DDCODPEC=space(0)
      .DoRTCalc(1,4,.f.)
        .w_DDTIPRIF = 'GE'
      .DoRTCalc(6,11,.f.)
        .w_DD_EMAIL = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMAIL, ' ')), .oParentObject .w_AN_EMAIL, .w_DD_EMAIL ), SPACE(25))
        .w_DD_EMPEC = IIF(.w_DDTIPRIF<>'SB', IIF( EMPTY( NVL(.w_DD_EMPEC, ' ')), .oParentObject .w_AN_EMPEC, .w_DD_EMPEC ), SPACE(25))
      .DoRTCalc(14,17,.f.)
      if not(empty(.w_DDCODNAZ))
        .link_2_15('Full')
      endif
      .DoRTCalc(18,18,.f.)
        .w_DDCODVET = IIF(.w_DDTIPRIF<>'SB', .w_DDCODVET, SPACE(5))
      .DoRTCalc(19,19,.f.)
      if not(empty(.w_DDCODVET))
        .link_2_17('Full')
      endif
      .DoRTCalc(20,20,.f.)
        .w_DDCODPOR = IIF(.w_DDTIPRIF<>'SB', .w_DDCODPOR, ' ')
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_DDCODPOR))
        .link_2_19('Full')
      endif
      .DoRTCalc(22,22,.f.)
        .w_DDCODSPE = IIF( .w_DDTIPRIF <> 'SB', .w_DDCODSPE, SPACE(3) )
      .DoRTCalc(23,23,.f.)
      if not(empty(.w_DDCODSPE))
        .link_2_21('Full')
      endif
        .w_DDCATOPE = 'OP'
        .w_DDCODAGE = IIF(.w_DDTIPRIF='CO', .w_DDCODAGE,SPACE(5))
      .DoRTCalc(25,25,.f.)
      if not(empty(.w_DDCODAGE))
        .link_2_23('Full')
      endif
        .w_DDTIPOPE = SPACE(10)
      .DoRTCalc(26,26,.f.)
      if not(empty(.w_DDTIPOPE))
        .link_2_24('Full')
      endif
      .DoRTCalc(27,27,.f.)
        .w_OBTEST = i_datsys
      .DoRTCalc(29,29,.f.)
        .w_DDPREDEF = 'N'
      .DoRTCalc(31,35,.f.)
        .w_DDMCCODI = SPACE(5)
      .DoRTCalc(36,36,.f.)
      if not(empty(.w_DDMCCODI))
        .link_2_34('Full')
      endif
        .w_DDMCCODT = SPACE(5)
      .DoRTCalc(37,37,.f.)
      if not(empty(.w_DDMCCODT))
        .link_2_35('Full')
      endif
      .DoRTCalc(38,40,.f.)
      if not(empty(.w_DDRFDICH))
        .link_2_38('Full')
      endif
      .DoRTCalc(41,44,.f.)
        .w_DDTELFAX = .w_DDNUMFAX
      .DoRTCalc(46,47,.f.)
        .w_SEDERESI = IIF(.w_DDTIPRIF='RE' AND empty(.w_DDDTOBSO),1,0)
      .DoRTCalc(49,49,.f.)
        .w_DDCODEST = Space(7)
        .w_DDCODCLA = Space(5)
        .w_DDCODPEC = ' '
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate(IIF(.oParentObject .w_ANFLESIG='S' , AH_MsgFormat("Codice IPA:"),AH_MsgFormat("Codice destinatario:")))
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DDCODDES = t_DDCODDES
    this.w_DDNOMDES = t_DDNOMDES
    this.w_DDTIPRIF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.RadioValue(.t.)
    this.w_DDPERSON = t_DDPERSON
    this.w_DDTELEFO = t_DDTELEFO
    this.w_DD__NOTE = t_DD__NOTE
    this.w_DDNUMFAX = t_DDNUMFAX
    this.w_DDINDIRI = t_DDINDIRI
    this.w_DDDTOBSO = t_DDDTOBSO
    this.w_DD_EMAIL = t_DD_EMAIL
    this.w_DD_EMPEC = t_DD_EMPEC
    this.w_DD___CAP = t_DD___CAP
    this.w_DDLOCALI = t_DDLOCALI
    this.w_DDPROVIN = t_DDPROVIN
    this.w_DDCODNAZ = t_DDCODNAZ
    this.w_DESNAZ = t_DESNAZ
    this.w_DDCODVET = t_DDCODVET
    this.w_DESVET = t_DESVET
    this.w_DDCODPOR = t_DDCODPOR
    this.w_DESPOR = t_DESPOR
    this.w_DDCODSPE = t_DDCODSPE
    this.w_DDCATOPE = t_DDCATOPE
    this.w_DDCODAGE = t_DDCODAGE
    this.w_DDTIPOPE = t_DDTIPOPE
    this.w_DESSPE = t_DESSPE
    this.w_OBTEST = t_OBTEST
    this.w_DATOBSO = t_DATOBSO
    this.w_DDPREDEF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.RadioValue(.t.)
    this.w_DESAGE = t_DESAGE
    this.w_DTOBSO = t_DTOBSO
    this.w_TIDESCRI = t_TIDESCRI
    this.w_TIDTOBSO = t_TIDTOBSO
    this.w_DDMCCODI = t_DDMCCODI
    this.w_DDMCCODT = t_DDMCCODT
    this.w_DESMCIMB = t_DESMCIMB
    this.w_DESMCTRA = t_DESMCTRA
    this.w_DDRFDICH = t_DDRFDICH
    this.w_NUMDIC = t_NUMDIC
    this.w_ANNDIC = t_ANNDIC
    this.w_DATDIC = t_DATDIC
    this.w_SERDIC = t_SERDIC
    this.w_DDTELFAX = t_DDTELFAX
    this.w_SEDERESI = t_SEDERESI
    this.w_DDTIPINF = this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.RadioValue(.t.)
    this.w_DDCODEST = t_DDCODEST
    this.w_DDCODCLA = t_DDCODCLA
    this.w_DDCODPEC = t_DDCODPEC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DDCODDES with this.w_DDCODDES
    replace t_DDNOMDES with this.w_DDNOMDES
    replace t_DDTIPRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPRIF_2_3.ToRadio()
    replace t_DDPERSON with this.w_DDPERSON
    replace t_DDTELEFO with this.w_DDTELEFO
    replace t_DD__NOTE with this.w_DD__NOTE
    replace t_DDNUMFAX with this.w_DDNUMFAX
    replace t_DDINDIRI with this.w_DDINDIRI
    replace t_DDDTOBSO with this.w_DDDTOBSO
    replace t_DD_EMAIL with this.w_DD_EMAIL
    replace t_DD_EMPEC with this.w_DD_EMPEC
    replace t_DD___CAP with this.w_DD___CAP
    replace t_DDLOCALI with this.w_DDLOCALI
    replace t_DDPROVIN with this.w_DDPROVIN
    replace t_DDCODNAZ with this.w_DDCODNAZ
    replace t_DESNAZ with this.w_DESNAZ
    replace t_DDCODVET with this.w_DDCODVET
    replace t_DESVET with this.w_DESVET
    replace t_DDCODPOR with this.w_DDCODPOR
    replace t_DESPOR with this.w_DESPOR
    replace t_DDCODSPE with this.w_DDCODSPE
    replace t_DDCATOPE with this.w_DDCATOPE
    replace t_DDCODAGE with this.w_DDCODAGE
    replace t_DDTIPOPE with this.w_DDTIPOPE
    replace t_DESSPE with this.w_DESSPE
    replace t_OBTEST with this.w_OBTEST
    replace t_DATOBSO with this.w_DATOBSO
    replace t_DDPREDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDPREDEF_2_28.ToRadio()
    replace t_DESAGE with this.w_DESAGE
    replace t_DTOBSO with this.w_DTOBSO
    replace t_TIDESCRI with this.w_TIDESCRI
    replace t_TIDTOBSO with this.w_TIDTOBSO
    replace t_DDMCCODI with this.w_DDMCCODI
    replace t_DDMCCODT with this.w_DDMCCODT
    replace t_DESMCIMB with this.w_DESMCIMB
    replace t_DESMCTRA with this.w_DESMCTRA
    replace t_DDRFDICH with this.w_DDRFDICH
    replace t_NUMDIC with this.w_NUMDIC
    replace t_ANNDIC with this.w_ANNDIC
    replace t_DATDIC with this.w_DATDIC
    replace t_SERDIC with this.w_SERDIC
    replace t_DDTELFAX with this.w_DDTELFAX
    replace t_SEDERESI with this.w_SEDERESI
    replace t_DDTIPINF with this.oPgFrm.Page1.oPag.oDDTIPINF_2_49.ToRadio()
    replace t_DDCODEST with this.w_DDCODEST
    replace t_DDCODCLA with this.w_DDCODCLA
    replace t_DDCODPEC with this.w_DDCODPEC
    if i_srv='A'
      replace DDCODDES with this.w_DDCODDES
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRESI = .w_TOTRESI-.w_sederesi
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mddPag1 as StdContainer
  Width  = 727
  height = 493
  stdWidth  = 727
  stdheight = 493
  resizeXpos=329
  resizeYpos=67
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=4, width=518,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="DDCODDES",Label1="Codice",Field2="DDNOMDES",Label2="Rag.sociale/descrizione",Field3="DDTIPRIF",Label3="Tipo",Field4="DDPREDEF",Label4="Pred.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_1 as StdString with uid="UNLLSJZZRJ",Visible=.t., Left=8, Top=156,;
    Alignment=1, Width=86, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="AQGMSRIBNW",Visible=.t., Left=387, Top=131,;
    Alignment=1, Width=46, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BFLWJZXFPM",Visible=.t., Left=8, Top=282,;
    Alignment=1, Width=86, Height=15,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="CBBQHEXSIT",Visible=.t., Left=8, Top=307,;
    Alignment=1, Width=86, Height=15,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="FWEMOWWKLX",Visible=.t., Left=532, Top=25,;
    Alignment=1, Width=38, Height=15,;
    Caption="Tel.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="AOMFQLYXMM",Visible=.t., Left=50, Top=182,;
    Alignment=1, Width=44, Height=15,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XPVJOXMQBW",Visible=.t., Left=8, Top=129,;
    Alignment=1, Width=86, Height=15,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="XRSHPEYSWE",Visible=.t., Left=8, Top=332,;
    Alignment=1, Width=86, Height=15,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="FDHXAJRHLL",Visible=.t., Left=8, Top=257,;
    Alignment=1, Width=86, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GDCPEAGGHM",Visible=.t., Left=8, Top=231,;
    Alignment=1, Width=86, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="AZYXOAXCAD",Visible=.t., Left=401, Top=231,;
    Alignment=1, Width=36, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="QFHWXXZDVR",Visible=.t., Left=386, Top=156,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CTCOZJSZHP",Visible=.t., Left=8, Top=357,;
    Alignment=1, Width=86, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.oParentObject .w_ANTIPCON <>'C' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="FCCQFCLVVH",Visible=.t., Left=9, Top=383,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo op. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="JTGRCRHBVE",Visible=.t., Left=532, Top=50,;
    Alignment=1, Width=38, Height=15,;
    Caption="FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QSYAXDATLX",Visible=.t., Left=384, Top=306,;
    Alignment=1, Width=66, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="WISOIYCOVH",Visible=.t., Left=384, Top=331,;
    Alignment=1, Width=66, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="TVRRRFOFTK",Visible=.t., Left=382, Top=284,;
    Alignment=0, Width=149, Height=18,;
    Caption="Metodi di calcolo spese"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="PUCRTMLYAO",Visible=.t., Left=382, Top=360,;
    Alignment=0, Width=205, Height=18,;
    Caption="Riferimento dichiarazione di intento"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="THYYFUEPJR",Visible=.t., Left=442, Top=386,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MYVZGHLMUE",Visible=.t., Left=543, Top=383,;
    Alignment=1, Width=22, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ADMACVDPOE",Visible=.t., Left=489, Top=386,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DQIYIJNDWG",Visible=.t., Left=50, Top=208,;
    Alignment=1, Width=44, Height=15,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="AJRTOSVKUZ",Visible=.t., Left=531, Top=97,;
    Alignment=1, Width=75, Height=18,;
    Caption="Sede Infinity:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (g_REVI<>"S" OR .w_DDTIPRIF<>"GE" OR Empty(.w_DDTIPINF))
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="UJOAJTPJPI",Visible=.t., Left=12, Top=462,;
    Alignment=1, Width=119, Height=18,;
    Caption="PEC destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="WMHUMSFKJI",Visible=.t., Left=7, Top=413,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NLKOVPKROC",Visible=.t., Left=345, Top=438,;
    Alignment=1, Width=105, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_1_31 as StdBox with uid="ZMSJNPFXFD",left=5, top=431, width=712,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=23,;
    width=514+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=24,width=513+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDDPERSON_2_4.Refresh()
      this.Parent.oDDTELEFO_2_5.Refresh()
      this.Parent.oDD__NOTE_2_6.Refresh()
      this.Parent.oDDNUMFAX_2_7.Refresh()
      this.Parent.oDDINDIRI_2_8.Refresh()
      this.Parent.oDDDTOBSO_2_9.Refresh()
      this.Parent.oDD_EMAIL_2_10.Refresh()
      this.Parent.oDD_EMPEC_2_11.Refresh()
      this.Parent.oDD___CAP_2_12.Refresh()
      this.Parent.oDDLOCALI_2_13.Refresh()
      this.Parent.oDDPROVIN_2_14.Refresh()
      this.Parent.oDDCODNAZ_2_15.Refresh()
      this.Parent.oDESNAZ_2_16.Refresh()
      this.Parent.oDDCODVET_2_17.Refresh()
      this.Parent.oDESVET_2_18.Refresh()
      this.Parent.oDDCODPOR_2_19.Refresh()
      this.Parent.oDESPOR_2_20.Refresh()
      this.Parent.oDDCODSPE_2_21.Refresh()
      this.Parent.oDDCODAGE_2_23.Refresh()
      this.Parent.oDDTIPOPE_2_24.Refresh()
      this.Parent.oDESSPE_2_25.Refresh()
      this.Parent.oDESAGE_2_29.Refresh()
      this.Parent.oTIDESCRI_2_32.Refresh()
      this.Parent.oDDMCCODI_2_34.Refresh()
      this.Parent.oDDMCCODT_2_35.Refresh()
      this.Parent.oDESMCIMB_2_36.Refresh()
      this.Parent.oDESMCTRA_2_37.Refresh()
      this.Parent.oNUMDIC_2_40.Refresh()
      this.Parent.oANNDIC_2_41.Refresh()
      this.Parent.oDATDIC_2_42.Refresh()
      this.Parent.oSERDIC_2_43.Refresh()
      this.Parent.oDDTIPINF_2_49.Refresh()
      this.Parent.oDDCODEST_2_50.Refresh()
      this.Parent.oDDCODCLA_2_51.Refresh()
      this.Parent.oDDCODPEC_2_52.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDDPERSON_2_4 as StdTrsField with uid="STQMHMGYXN",rtseq=6,rtrep=.t.,;
    cFormVar="w_DDPERSON",value=space(40),;
    ToolTipText = "Persona di riferimento",;
    HelpContextID = 207564412,;
    cTotal="", bFixedPos=.t., cQueryName = "DDPERSON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=97, Top=129, InputMask=replicate('X',40)

  func oDDPERSON_2_4.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDDTELEFO_2_5 as StdTrsField with uid="YJVNOBFTAC",rtseq=7,rtrep=.t.,;
    cFormVar="w_DDTELEFO",value=space(18),;
    ToolTipText = "Numero della sede",;
    HelpContextID = 88150405,;
    cTotal="", bFixedPos=.t., cQueryName = "DDTELEFO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=571, Top=25, InputMask=replicate('X',18)

  proc oDDTELEFO_2_5.mDefault
    with this.Parent.oContained
      if empty(.w_DDTELEFO)
        .w_DDTELEFO = .oParentObject .w_ANTELEFO
      endif
    endwith
  endproc

  func oDDTELEFO_2_5.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDD__NOTE_2_6 as StdTrsField with uid="YBKHQPSHGT",rtseq=8,rtrep=.t.,;
    cFormVar="w_DD__NOTE",value=space(40),;
    ToolTipText = "Annotazioni aggiuntive sulla sede",;
    HelpContextID = 259768699,;
    cTotal="", bFixedPos=.t., cQueryName = "DD__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=434, Top=129, InputMask=replicate('X',40)

  func oDD__NOTE_2_6.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDDNUMFAX_2_7 as StdTrsField with uid="NZABIDSUDY",rtseq=9,rtrep=.t.,;
    cFormVar="w_DDNUMFAX",value=space(18),;
    ToolTipText = "Numero di FAX",;
    HelpContextID = 107000206,;
    cTotal="", bFixedPos=.t., cQueryName = "DDNUMFAX",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=571, Top=50, InputMask=replicate('X',18)

  proc oDDNUMFAX_2_7.mDefault
    with this.Parent.oContained
      if empty(.w_DDNUMFAX)
        .w_DDNUMFAX = .oParentObject .w_ANTELFAX
      endif
    endwith
  endproc

  func oDDNUMFAX_2_7.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDDINDIRI_2_8 as StdTrsField with uid="ANQXNQPWAS",rtseq=10,rtrep=.t.,;
    cFormVar="w_DDINDIRI",value=space(35),;
    ToolTipText = "Indirizzo della sede",;
    HelpContextID = 147415423,;
    cTotal="", bFixedPos=.t., cQueryName = "DDINDIRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=97, Top=155, InputMask=replicate('X',35)

  proc oDDINDIRI_2_8.mDefault
    with this.Parent.oContained
      if empty(.w_DDINDIRI)
        .w_DDINDIRI = .oParentObject .w_ANINDIRI
      endif
    endwith
  endproc

  func oDDINDIRI_2_8.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDDDTOBSO_2_9 as StdTrsField with uid="KTXUHLEXNV",rtseq=11,rtrep=.t.,;
    cFormVar="w_DDDTOBSO",value=ctod("  /  /  "),;
    ToolTipText = "Data di obsolescenza",;
    HelpContextID = 41881989,;
    cTotal="", bFixedPos=.t., cQueryName = "DDDTOBSO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=511, Top=155

  func oDDDTOBSO_2_9.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDD_EMAIL_2_10 as StdTrsField with uid="JYTRXWDANK",rtseq=12,rtrep=.t.,;
    cFormVar="w_DD_EMAIL",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica della sede",;
    HelpContextID = 246300286,;
    cTotal="", bFixedPos=.t., cQueryName = "DD_EMAIL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=623, Left=97, Top=180, InputMask=replicate('X',254)

  func oDD_EMAIL_2_10.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDD_EMPEC_2_11 as StdTrsField with uid="VXXRNHPVCP",rtseq=13,rtrep=.t.,;
    cFormVar="w_DD_EMPEC",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica certificata della sede",;
    HelpContextID = 5357945,;
    cTotal="", bFixedPos=.t., cQueryName = "DD_EMPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=623, Left=97, Top=205, InputMask=replicate('X',254)

  func oDD_EMPEC_2_11.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDD___CAP_2_12 as StdTrsField with uid="BPGYIXKKDF",rtseq=14,rtrep=.t.,;
    cFormVar="w_DD___CAP",value=space(8),;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 76267910,;
    cTotal="", bFixedPos=.t., cQueryName = "DD___CAP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=97, Top=231, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oDD___CAP_2_12.mDefault
    with this.Parent.oContained
      if empty(.w_DD___CAP)
        .w_DD___CAP = .oParentObject .w_AN___CAP
      endif
    endwith
  endproc

  func oDD___CAP_2_12.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  proc oDD___CAP_2_12.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DD___CAP",".w_DDLOCALI",".w_DDPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDDLOCALI_2_13 as StdTrsField with uid="QLYWFMGXJJ",rtseq=15,rtrep=.t.,;
    cFormVar="w_DDLOCALI",value=space(30),;
    ToolTipText = "Localit� della sede",;
    HelpContextID = 256208513,;
    cTotal="", bFixedPos=.t., cQueryName = "DDLOCALI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=167, Top=231, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oDDLOCALI_2_13.mDefault
    with this.Parent.oContained
      if empty(.w_DDLOCALI)
        .w_DDLOCALI = .oParentObject .w_ANLOCALI
      endif
    endwith
  endproc

  func oDDLOCALI_2_13.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  proc oDDLOCALI_2_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_DD___CAP",".w_DDLOCALI",".w_DDPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDDPROVIN_2_14 as StdTrsField with uid="TQPROIFBXB",rtseq=16,rtrep=.t.,;
    cFormVar="w_DDPROVIN",value=space(2),;
    ToolTipText = "Provincia della sede",;
    HelpContextID = 159526524,;
    cTotal="", bFixedPos=.t., cQueryName = "DDPROVIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=438, Top=231, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2), bHasZoom = .t. 

  proc oDDPROVIN_2_14.mDefault
    with this.Parent.oContained
      if empty(.w_DDPROVIN)
        .w_DDPROVIN = .oParentObject .w_ANPROVIN
      endif
    endwith
  endproc

  func oDDPROVIN_2_14.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  proc oDDPROVIN_2_14.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_DDPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDDCODNAZ_2_15 as StdTrsField with uid="OMRZWCCQMD",rtseq=17,rtrep=.t.,;
    cFormVar="w_DDCODNAZ",value=space(3),;
    ToolTipText = "Codice nazione della sede",;
    HelpContextID = 231342480,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODNAZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=97, Top=256, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_DDCODNAZ"

  proc oDDCODNAZ_2_15.mDefault
    with this.Parent.oContained
      if empty(.w_DDCODNAZ)
        .w_DDCODNAZ = .oParentObject .w_ANNAZION
      endif
    endwith
  endproc

  func oDDCODNAZ_2_15.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDCODNAZ_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDCODNAZ_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDCODNAZ_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oDDCODNAZ_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Elenco nazioni",'',this.parent.oContained
  endproc
  proc oDDCODNAZ_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_DDCODNAZ
    i_obj.ecpSave()
  endproc

  add object oDESNAZ_2_16 as StdTrsField with uid="UWRPACCXKZ",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESNAZ",value=space(35),enabled=.f.,;
    HelpContextID = 161088054,;
    cTotal="", bFixedPos=.t., cQueryName = "DESNAZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=167, Top=256, InputMask=replicate('X',35)

  add object oDDCODVET_2_17 as StdTrsField with uid="VHRTCGEOBR",rtseq=19,rtrep=.t.,;
    cFormVar="w_DDCODVET",value=space(5),;
    ToolTipText = "Codice del vettore utilizzato per quella sede",;
    HelpContextID = 97124746,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODVET",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice vettore inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=97, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_DDCODVET"

  func oDDCODVET_2_17.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDCODVET_2_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  func oDDCODVET_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDCODVET_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDCODVET_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oDDCODVET_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oDDCODVET_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_DDCODVET
    i_obj.ecpSave()
  endproc

  add object oDESVET_2_18 as StdTrsField with uid="XYYDRZDZER",rtseq=20,rtrep=.t.,;
    cFormVar="w_DESVET",value=space(35),enabled=.f.,;
    HelpContextID = 65143350,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVET",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=167, Top=281, InputMask=replicate('X',35)

  func oDESVET_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oDDCODPOR_2_19 as StdTrsField with uid="DYZRKUNYJT",rtseq=21,rtrep=.t.,;
    cFormVar="w_DDCODPOR",value=space(1),;
    ToolTipText = "Tipo porto",;
    HelpContextID = 3538552,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODPOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=97, Top=306, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="PORTI", cZoomOnZoom="GSAR_APO", oKey_1_1="POCODPOR", oKey_1_2="this.w_DDCODPOR"

  func oDDCODPOR_2_19.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDCODPOR_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  func oDDCODPOR_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDCODPOR_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDCODPOR_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PORTI','*','POCODPOR',cp_AbsName(this.parent,'oDDCODPOR_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APO',"Porti",'',this.parent.oContained
  endproc
  proc oDDCODPOR_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_POCODPOR=this.parent.oContained.w_DDCODPOR
    i_obj.ecpSave()
  endproc

  add object oDESPOR_2_20 as StdTrsField with uid="SCDDPIDYYA",rtseq=22,rtrep=.t.,;
    cFormVar="w_DESPOR",value=space(30),enabled=.f.,;
    HelpContextID = 41681462,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=167, Top=306, InputMask=replicate('X',30)

  func oDESPOR_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oDDCODSPE_2_21 as StdTrsField with uid="UPRVFKMHAC",rtseq=23,rtrep=.t.,;
    cFormVar="w_DDCODSPE",value=space(3),;
    ToolTipText = "Codice del mezzo di spedizione",;
    HelpContextID = 46793083,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODSPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=97, Top=331, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_DDCODSPE"

  func oDDCODSPE_2_21.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF<>'SB' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDCODSPE_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  func oDDCODSPE_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDCODSPE_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDCODSPE_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oDDCODSPE_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oDDCODSPE_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_DDCODSPE
    i_obj.ecpSave()
  endproc

  add object oDDCODAGE_2_23 as StdTrsField with uid="TLJUHJQECN",rtseq=25,rtrep=.t.,;
    cFormVar="w_DDCODAGE",value=space(5),;
    ToolTipText = "Codice agente che cura i rapporti con il cliente relativamente a questa sede",;
    HelpContextID = 13238651,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODAGE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=97, Top=356, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_DDCODAGE"

  func oDDCODAGE_2_23.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='CO' And Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDCODAGE_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject .w_ANTIPCON <>'C' OR IsAlt())
    endwith
    endif
  endfunc

  func oDDCODAGE_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDCODAGE_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDCODAGE_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oDDCODAGE_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'',this.parent.oContained
  endproc
  proc oDDCODAGE_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_DDCODAGE
    i_obj.ecpSave()
  endproc

  add object oDDTIPOPE_2_24 as StdTrsField with uid="ATGWNZUHAX",rtseq=26,rtrep=.t.,;
    cFormVar="w_DDTIPOPE",value=space(10),;
    HelpContextID = 8056453,;
    cTotal="", bFixedPos=.t., cQueryName = "DDTIPOPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=97, Top=381, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_DDCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_DDTIPOPE"

  func oDDTIPOPE_2_24.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='CO' and Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDTIPOPE_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDTIPOPE_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDTIPOPE_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_DDCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_DDCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oDDTIPOPE_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oDDTIPOPE_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_DDCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_DDTIPOPE
    i_obj.ecpSave()
  endproc

  add object oDESSPE_2_25 as StdTrsField with uid="DESJFHLBZU",rtseq=27,rtrep=.t.,;
    cFormVar="w_DESSPE",value=space(35),enabled=.f.,;
    HelpContextID = 175177162,;
    cTotal="", bFixedPos=.t., cQueryName = "DESSPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=167, Top=331, InputMask=replicate('X',35)

  func oDESSPE_2_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oDESAGE_2_29 as StdTrsField with uid="YJYEKSEUII",rtseq=31,rtrep=.t.,;
    cFormVar="w_DESAGE",value=space(35),enabled=.f.,;
    HelpContextID = 185793994,;
    cTotal="", bFixedPos=.t., cQueryName = "DESAGE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=167, Top=356, InputMask=replicate('X',35)

  func oDESAGE_2_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject .w_ANTIPCON <>'C' OR IsAlt())
    endwith
    endif
  endfunc

  add object oTIDESCRI_2_32 as StdTrsField with uid="TNOOFYXTGO",rtseq=34,rtrep=.t.,;
    cFormVar="w_TIDESCRI",value=space(30),enabled=.f.,;
    HelpContextID = 61871999,;
    cTotal="", bFixedPos=.t., cQueryName = "TIDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=189, Left=181, Top=381, InputMask=replicate('X',30)

  add object oDDMCCODI_2_34 as StdTrsField with uid="MKIFBREMKS",rtseq=36,rtrep=.t.,;
    cFormVar="w_DDMCCODI",value=space(5),;
    ToolTipText = "Metodo di calcolo spese imballo",;
    HelpContextID = 246325631,;
    cTotal="", bFixedPos=.t., cQueryName = "DDMCCODI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=452, Top=306, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_DDMCCODI"

  func oDDMCCODI_2_34.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='CO')
    endwith
  endfunc

  func oDDMCCODI_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  func oDDMCCODI_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDMCCODI_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDMCCODI_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oDDMCCODI_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oDDMCCODI_2_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_DDMCCODI
    i_obj.ecpSave()
  endproc

  add object oDDMCCODT_2_35 as StdTrsField with uid="KCEFKTQYSO",rtseq=37,rtrep=.t.,;
    cFormVar="w_DDMCCODT",value=space(5),;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 246325642,;
    cTotal="", bFixedPos=.t., cQueryName = "DDMCCODT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=452, Top=331, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_DDMCCODT"

  func oDDMCCODT_2_35.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='CO')
    endwith
  endfunc

  func oDDMCCODT_2_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  func oDDMCCODT_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oDDMCCODT_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDDMCCODT_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oDDMCCODT_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oDDMCCODT_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_DDMCCODT
    i_obj.ecpSave()
  endproc

  add object oDESMCIMB_2_36 as StdTrsField with uid="MRGQGHVCJH",rtseq=38,rtrep=.t.,;
    cFormVar="w_DESMCIMB",value=space(35),enabled=.f.,;
    HelpContextID = 122092936,;
    cTotal="", bFixedPos=.t., cQueryName = "DESMCIMB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=201, Left=518, Top=306, InputMask=replicate('X',35)

  func oDESMCIMB_2_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oDESMCTRA_2_37 as StdTrsField with uid="OMEUQHUMJL",rtseq=39,rtrep=.t.,;
    cFormVar="w_DESMCTRA",value=space(35),enabled=.f.,;
    HelpContextID = 62456439,;
    cTotal="", bFixedPos=.t., cQueryName = "DESMCTRA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=201, Left=518, Top=332, InputMask=replicate('X',35)

  func oDESMCTRA_2_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oBtn_2_39 as StdButton with uid="PKKSUWHQWC",width=20,height=19,;
   left=645, top=383,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare la dichiarazione di intento";
    , HelpContextID = 80111146;
  , bGlobalFont=.t.

    proc oBtn_2_39.Click()
      do GSAR_KDI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_39.mCond()
    with this.Parent.oContained
      return (upper(.cfunction)$'LOAD-EDIT' and .w_DDTIPRIF$'CO/FA' and Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oNUMDIC_2_40 as StdTrsField with uid="LZSZMXBJYG",rtseq=41,rtrep=.t.,;
    cFormVar="w_NUMDIC",value=0,enabled=.f.,;
    ToolTipText = "Numero della eventuale dichiarazione di esenzione",;
    HelpContextID = 217074986,;
    cTotal="", bFixedPos=.t., cQueryName = "NUMDIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=381, Top=382

  add object oANNDIC_2_41 as StdTrsField with uid="WDPJRHWULM",rtseq=42,rtrep=.t.,;
    cFormVar="w_ANNDIC",value=space(4),enabled=.f.,;
    ToolTipText = "Anno della eventuale dichiarazione di esenzione",;
    HelpContextID = 217072890,;
    cTotal="", bFixedPos=.t., cQueryName = "ANNDIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=497, Top=382, InputMask=replicate('X',4)

  add object oDATDIC_2_42 as StdTrsField with uid="XUSTCCGTDF",rtseq=43,rtrep=.t.,;
    cFormVar="w_DATDIC",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data della eventuale dichiarazione di esenzione",;
    HelpContextID = 217051594,;
    cTotal="", bFixedPos=.t., cQueryName = "DATDIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=566, Top=382

  add object oSERDIC_2_43 as StdTrsField with uid="NXNPEGQYTT",rtseq=44,rtrep=.t.,;
    cFormVar="w_SERDIC",value=space(3),enabled=.f.,;
    HelpContextID = 217058522,;
    cTotal="", bFixedPos=.t., cQueryName = "SERDIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=452, Top=382, InputMask=replicate('X',3)

  add object oBtn_2_44 as StdButton with uid="VJARRGRPGZ",width=48,height=45,;
   left=671, top=359,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per rimuovere il riferimento alla dichiarazione di intento";
    , HelpContextID = 3836230;
    , Tabstop=.f.,Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_44.Click()
      with this.Parent.oContained
        .notifyevent("Elimina")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_44.mCond()
    with this.Parent.oContained
      return (upper(.cfunction)$'LOAD-EDIT' and Not Empty( .w_DDRFDICH ))
    endwith
  endfunc

  add object oDDTIPINF_2_49 as StdTrsCombo with uid="SBEAAPXWXK",rtrep=.t.,;
    cFormVar="w_DDTIPINF", RowSource=""+"Legale,"+"Lavoro,"+"Ufficio,"+"Abitazione,"+"Universit�,"+"Secondaria,"+"Deposito,"+"Magazzino,"+"Amministrativa,"+"Filiale" , enabled=.f.,;
    HelpContextID = 108719748,;
    Height=26, Width=110, Left=610, Top=96,;
    cTotal="", cQueryName = "DDTIPINF",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDDTIPINF_2_49.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DDTIPINF,&i_cF..t_DDTIPINF),this.value)
    return(iif(xVal =1,'LEG',;
    iif(xVal =2,'LAV',;
    iif(xVal =3,'UFF',;
    iif(xVal =4,'ABI',;
    iif(xVal =5,'UNI',;
    iif(xVal =6,'SEC',;
    iif(xVal =7,'DEP',;
    iif(xVal =8,'MAG',;
    iif(xVal =9,'AMM',;
    iif(xVal =10,'FIL',;
    space(3))))))))))))
  endfunc
  func oDDTIPINF_2_49.GetRadio()
    this.Parent.oContained.w_DDTIPINF = this.RadioValue()
    return .t.
  endfunc

  func oDDTIPINF_2_49.ToRadio()
    this.Parent.oContained.w_DDTIPINF=trim(this.Parent.oContained.w_DDTIPINF)
    return(;
      iif(this.Parent.oContained.w_DDTIPINF=='LEG',1,;
      iif(this.Parent.oContained.w_DDTIPINF=='LAV',2,;
      iif(this.Parent.oContained.w_DDTIPINF=='UFF',3,;
      iif(this.Parent.oContained.w_DDTIPINF=='ABI',4,;
      iif(this.Parent.oContained.w_DDTIPINF=='UNI',5,;
      iif(this.Parent.oContained.w_DDTIPINF=='SEC',6,;
      iif(this.Parent.oContained.w_DDTIPINF=='DEP',7,;
      iif(this.Parent.oContained.w_DDTIPINF=='MAG',8,;
      iif(this.Parent.oContained.w_DDTIPINF=='AMM',9,;
      iif(this.Parent.oContained.w_DDTIPINF=='FIL',10,;
      0)))))))))))
  endfunc

  func oDDTIPINF_2_49.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDDTIPINF_2_49.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI<>"S" OR .w_DDTIPRIF<>"GE" OR Empty(.w_DDTIPINF))
    endwith
    endif
  endfunc

  add object oDDCODEST_2_50 as StdTrsField with uid="ULPHPOLIKR",rtseq=50,rtrep=.t.,;
    cFormVar="w_DDCODEST",value=space(7),;
    ToolTipText = "Per le fatture PA contiene il codice, di 6 caratteri, dell'ufficio destinatario della fattura, riportato nella rubrica �Indice PA�,  per le fatture privati contiene il codice, di 7 caratteri, assegnato dal Sdi ai sogg. che hanno accreditato un canale",;
    HelpContextID = 80347530,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODEST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=437, InputMask=replicate('X',7)

  func oDDCODEST_2_50.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='FA')
    endwith
  endfunc

  add object oDDCODCLA_2_51 as StdTrsField with uid="AHXUJBSGGD",rtseq=51,rtrep=.t.,;
    cFormVar="w_DDCODCLA",value=space(5),;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 221642377,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=452, Top=437, InputMask=replicate('X',5)

  func oDDCODCLA_2_51.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='FA')
    endwith
  endfunc

  add object oDDCODPEC_2_52 as StdTrsMemo with uid="GGXAHRKWXQ",rtseq=52,rtrep=.t.,;
    cFormVar="w_DDCODPEC",value=space(0),;
    ToolTipText = "Indirizzo PEC al quale inviare il documento",;
    HelpContextID = 264896889,;
    cTotal="", bFixedPos=.t., cQueryName = "DDCODPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=579, Left=134, Top=462

  func oDDCODPEC_2_52.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='FA')
    endwith
  endfunc

  add object oObj_2_53 as cp_calclbl with uid="YLASLTPADN",width=130,height=15,;
   left=1, top=438,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 97685478

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oCONTRESI_3_1 as StdField with uid="QZSPYAMDDU",rtseq=46,rtrep=.f.,;
    cFormVar="w_CONTRESI",value=0,;
    HelpContextID = 95403119,;
    cQueryName = "CONTRESI",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione: sono state inserite pi� sedi di tipo residenza con data di obsolescenza non valorizzata",;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=739, Top=288, Visible=.f.

  func oCONTRESI_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TOTRESI<=1)
    endwith
    return bRes
  endfunc
enddefine

* --- Defining Body row
define class tgsar_mddBodyRow as CPBodyRowCnt
  Width=504
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDDCODDES_2_1 as StdTrsField with uid="GISPMZVYGW",rtseq=3,rtrep=.t.,;
    cFormVar="w_DDCODDES",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 63570313,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, cSayPict=["!!!!!"], cGetPict=["!!!!!"], InputMask=replicate('X',5)

  add object oDDNOMDES_2_2 as StdTrsField with uid="GXVSJWTHJG",rtseq=4,rtrep=.t.,;
    cFormVar="w_DDNOMDES",value=space(40),;
    ToolTipText = "Ragione sociale della sede",;
    HelpContextID = 73052553,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=289, Left=60, Top=0, InputMask=replicate('X',40)

  proc oDDNOMDES_2_2.mDefault
    with this.Parent.oContained
      if empty(.w_DDNOMDES)
        .w_DDNOMDES = .oParentObject .w_ANDESCRI
      endif
    endwith
  endproc

  func oDDNOMDES_2_2.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  add object oDDTIPRIF_2_3 as StdTrsCombo with uid="ZZVESFWAZR",rtrep=.t.,;
    cFormVar="w_DDTIPRIF", RowSource=""+"Generico,"+"Consegna,"+"Pagamento,"+"Avviso,"+"Residenza,"+"Fatturazione,"+"Stab. org. sogg. N. R." , ;
    ToolTipText = "Tipologia della sede",;
    HelpContextID = 226160260,;
    Height=21, Width=122, Left=353, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Impossibile impostare tipologia sede per soggetti con flag non residenti non impostato";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDDTIPRIF_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DDTIPRIF,&i_cF..t_DDTIPRIF),this.value)
    return(iif(xVal =1,'GE',;
    iif(xVal =2,'CO',;
    iif(xVal =3,'PA',;
    iif(xVal =4,'SO',;
    iif(xVal =5,'RE',;
    iif(xVal =6,'FA',;
    iif(xVal =7,'SB',;
    space(2)))))))))
  endfunc
  func oDDTIPRIF_2_3.GetRadio()
    this.Parent.oContained.w_DDTIPRIF = this.RadioValue()
    return .t.
  endfunc

  func oDDTIPRIF_2_3.ToRadio()
    this.Parent.oContained.w_DDTIPRIF=trim(this.Parent.oContained.w_DDTIPRIF)
    return(;
      iif(this.Parent.oContained.w_DDTIPRIF=='GE',1,;
      iif(this.Parent.oContained.w_DDTIPRIF=='CO',2,;
      iif(this.Parent.oContained.w_DDTIPRIF=='PA',3,;
      iif(this.Parent.oContained.w_DDTIPRIF=='SO',4,;
      iif(this.Parent.oContained.w_DDTIPRIF=='RE',5,;
      iif(this.Parent.oContained.w_DDTIPRIF=='FA',6,;
      iif(this.Parent.oContained.w_DDTIPRIF=='SB',7,;
      0))))))))
  endfunc

  func oDDTIPRIF_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDDTIPRIF_2_3.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DDCODDES))
    endwith
  endfunc

  func oDDTIPRIF_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT(.w_FLSGNNRE='N' AND .w_DDTIPRIF='SB'AND (EMPTY(.w_DDDTOBSO) OR .w_DDDTOBSO>i_datsys)) OR .w_FLSGNNRE='S')
    endwith
    return bRes
  endfunc

  add object oDDPREDEF_2_28 as StdTrsCheck with uid="HTAZKJYHQN",rtrep=.t.,;
    cFormVar="w_DDPREDEF",  caption="",;
    ToolTipText = "Sede di consegna predefinita",;
    HelpContextID = 64868732,;
    Left=479, Top=1, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDDPREDEF_2_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DDPREDEF,&i_cF..t_DDPREDEF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDDPREDEF_2_28.GetRadio()
    this.Parent.oContained.w_DDPREDEF = this.RadioValue()
    return .t.
  endfunc

  func oDDPREDEF_2_28.ToRadio()
    this.Parent.oContained.w_DDPREDEF=trim(this.Parent.oContained.w_DDPREDEF)
    return(;
      iif(this.Parent.oContained.w_DDPREDEF=='S',1,;
      0))
  endfunc

  func oDDPREDEF_2_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDDPREDEF_2_28.mCond()
    with this.Parent.oContained
      return (.w_DDTIPRIF='CO')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDDCODDES_2_1.When()
    return(.t.)
  proc oDDCODDES_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDDCODDES_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mdd','DES_DIVE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DDTIPCON=DES_DIVE.DDTIPCON";
  +" and "+i_cAliasName2+".DDCODICE=DES_DIVE.DDCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
