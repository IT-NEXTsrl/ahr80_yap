* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aou                                                        *
*              Campi Outlook                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-09                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aou"))

* --- Class definition
define class tgsar_aou as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 462
  Height = 155+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  MAP_OUTL_IDX = 0
  cFile = "MAP_OUTL"
  cKeySelect = "MOPRPRTY,MOTIPO_P"
  cKeyWhere  = "MOPRPRTY=this.w_MOPRPRTY and MOTIPO_P=this.w_MOTIPO_P"
  cKeyWhereODBC = '"MOPRPRTY="+cp_ToStrODBC(this.w_MOPRPRTY)';
      +'+" and MOTIPO_P="+cp_ToStrODBC(this.w_MOTIPO_P)';

  cKeyWhereODBCqualified = '"MAP_OUTL.MOPRPRTY="+cp_ToStrODBC(this.w_MOPRPRTY)';
      +'+" and MAP_OUTL.MOTIPO_P="+cp_ToStrODBC(this.w_MOTIPO_P)';

  cPrg = "gsar_aou"
  cComment = "Campi Outlook"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MOPRPRTY = space(50)
  w_ChiaveFittizia = space(50)
  o_ChiaveFittizia = space(50)
  w_MOTIPO_P = space(1)
  w_MODESCRI = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAP_OUTL','gsar_aou')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aouPag1","gsar_aou",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Propriet�")
      .Pages(1).HelpContextID = 166855062
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOPRPRTY_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='MAP_OUTL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAP_OUTL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAP_OUTL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MOPRPRTY = NVL(MOPRPRTY,space(50))
      .w_MOTIPO_P = NVL(MOTIPO_P,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MAP_OUTL where MOPRPRTY=KeySet.MOPRPRTY
    *                            and MOTIPO_P=KeySet.MOTIPO_P
    *
    i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAP_OUTL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAP_OUTL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MAP_OUTL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOPRPRTY',this.w_MOPRPRTY  ,'MOTIPO_P',this.w_MOTIPO_P  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ChiaveFittizia = space(50)
        .w_MOPRPRTY = NVL(MOPRPRTY,space(50))
        .w_MOTIPO_P = NVL(MOTIPO_P,space(1))
        .w_MODESCRI = NVL(MODESCRI,space(50))
        cp_LoadRecExtFlds(this,'MAP_OUTL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MOPRPRTY = space(50)
      .w_ChiaveFittizia = space(50)
      .w_MOTIPO_P = space(1)
      .w_MODESCRI = space(50)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_MOTIPO_P = 'C'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAP_OUTL')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMOPRPRTY_1_1.enabled = i_bVal
      .Page1.oPag.oChiaveFittizia_1_2.enabled = i_bVal
      .Page1.oPag.oMODESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMOPRPRTY_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMOPRPRTY_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MAP_OUTL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRPRTY,"MOPRPRTY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPO_P,"MOTIPO_P",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESCRI,"MODESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
    i_lTable = "MAP_OUTL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAP_OUTL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SO1 with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MAP_OUTL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MAP_OUTL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAP_OUTL')
        i_extval=cp_InsertValODBCExtFlds(this,'MAP_OUTL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MOPRPRTY,MOTIPO_P,MODESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MOPRPRTY)+;
                  ","+cp_ToStrODBC(this.w_MOTIPO_P)+;
                  ","+cp_ToStrODBC(this.w_MODESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAP_OUTL')
        i_extval=cp_InsertValVFPExtFlds(this,'MAP_OUTL')
        cp_CheckDeletedKey(i_cTable,0,'MOPRPRTY',this.w_MOPRPRTY,'MOTIPO_P',this.w_MOTIPO_P)
        INSERT INTO (i_cTable);
              (MOPRPRTY,MOTIPO_P,MODESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MOPRPRTY;
                  ,this.w_MOTIPO_P;
                  ,this.w_MODESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MAP_OUTL_IDX,i_nConn)
      *
      * update MAP_OUTL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MAP_OUTL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MODESCRI="+cp_ToStrODBC(this.w_MODESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MAP_OUTL')
        i_cWhere = cp_PKFox(i_cTable  ,'MOPRPRTY',this.w_MOPRPRTY  ,'MOTIPO_P',this.w_MOTIPO_P  )
        UPDATE (i_cTable) SET;
              MODESCRI=this.w_MODESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MAP_OUTL_IDX,i_nConn)
      *
      * delete MAP_OUTL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MOPRPRTY',this.w_MOPRPRTY  ,'MOTIPO_P',this.w_MOTIPO_P  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAP_OUTL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAP_OUTL_IDX,2])
    if i_bUpd
      with this
        if .o_ChiaveFittizia<>.w_ChiaveFittizia
          .Calculate_DQQFZOLLTI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DQQFZOLLTI()
    with this
          * --- Avvalora MOPRPRTY
          .w_MOPRPRTY = .w_ChiaveFittizia
    endwith
  endproc
  proc Calculate_XKRHLAGSGQ()
    with this
          * --- UserProp
          .w_ChiaveFittizia = IIF(UPPER(LEFT(.w_ChiaveFittizia,8))='USERPROP',.w_ChiaveFittizia,"UserProperties('"+ALLTRIM(.w_ChiaveFittizia)+"').Value")
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.enabled = this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.mCond()
    this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.enabled = this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.visible=!this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.mHide()
    this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.visible=!this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("UserProp")
          .Calculate_XKRHLAGSGQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.value==this.w_MOPRPRTY)
      this.oPgFrm.Page1.oPag.oMOPRPRTY_1_1.value=this.w_MOPRPRTY
    endif
    if not(this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.value==this.w_ChiaveFittizia)
      this.oPgFrm.Page1.oPag.oChiaveFittizia_1_2.value=this.w_ChiaveFittizia
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_4.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_4.value=this.w_MODESCRI
    endif
    cp_SetControlsValueExtFlds(this,'MAP_OUTL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ChiaveFittizia = this.w_ChiaveFittizia
    return

enddefine

* --- Define pages as container
define class tgsar_aouPag1 as StdContainer
  Width  = 458
  height = 155
  stdWidth  = 458
  stdheight = 155
  resizeXpos=399
  resizeYpos=152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOPRPRTY_1_1 as StdField with uid="ZKTTZOLEPN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MOPRPRTY", cQueryName = "MOPRPRTY",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Propriet� Outlook",;
    HelpContextID = 214818079,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=89, Top=10, InputMask=replicate('X',50)

  func oMOPRPRTY_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
   endif
  endfunc

  func oMOPRPRTY_1_1.mHide()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oChiaveFittizia_1_2 as StdField with uid="HFZYKUMKVI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ChiaveFittizia", cQueryName = "ChiaveFittizia",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 95448881,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=89, Top=10, InputMask=replicate('X',50)

  func oChiaveFittizia_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oChiaveFittizia_1_2.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oMODESCRI_1_4 as StdField with uid="VUDZHPGIZC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 233839887,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=89, Top=33, InputMask=replicate('X',50)


  add object oBtn_1_8 as StdButton with uid="AJNEXBDUAC",left=404, top=106, width=48,height=45,;
    CpPicture="USER.ico", caption="", nPag=1;
    , ToolTipText = "Premere per caricare una propriet� 'User properties'";
    , HelpContextID = 36393542;
    , Caption='\<Prop. utente';
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("UserProp")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Load')
      endwith
    endif
  endfunc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Load')
     endwith
    endif
  endfunc

  add object oStr_1_5 as StdString with uid="TUAYAAVUGP",Visible=.t., Left=20, Top=33,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="SLHIOWXTJN",Visible=.t., Left=20, Top=10,;
    Alignment=1, Width=68, Height=18,;
    Caption="Propriet�:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="YPEXRNCUED",Visible=.t., Left=20, Top=64,;
    Alignment=0, Width=413, Height=18,;
    Caption="Attenzione! I campi definiti dall'utente nell'elemento dovranno essere inseriti"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="QLIFDVPTIZ",Visible=.t., Left=20, Top=82,;
    Alignment=0, Width=367, Height=18,;
    Caption="utilizzando la dicitura 'UserProperties'. Premendo il bottone a fianco"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="GNBBWGVJIW",Visible=.t., Left=20, Top=100,;
    Alignment=0, Width=363, Height=18,;
    Caption="la stringa sar� inserita correttamente."  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aou','MAP_OUTL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOPRPRTY=MAP_OUTL.MOPRPRTY";
  +" and "+i_cAliasName2+".MOTIPO_P=MAP_OUTL.MOTIPO_P";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
