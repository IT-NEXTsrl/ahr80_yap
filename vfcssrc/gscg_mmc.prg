* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mmc                                                        *
*              Dettaglio movimenti conti correnti                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_183]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-04                                                      *
* Last revis.: 2015-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mmc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mmc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mmc")
  return

* --- Class definition
define class tgscg_mmc as StdPCForm
  Width  = 747
  Height = 336
  Top    = 213
  Left   = 19
  cComment = "Dettaglio movimenti conti correnti"
  cPrg = "gscg_mmc"
  HelpContextID=197797225
  add object cnt as tcgscg_mmc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mmc as PCContext
  w_CCSERIAL = space(10)
  w_CCROWRIF = 0
  w_CCCODCAU = space(5)
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_FLCOMP = space(1)
  w_CCNUMCOR = space(15)
  w_CONCOL = space(15)
  w_TIPCAU = space(1)
  w_NUMCOR = space(15)
  w_TIPCON = space(1)
  w_BAVAL = space(3)
  w_CALFIR = space(3)
  w_CCDATVAL = space(8)
  w_CCIMPCRE = 0
  w_CCIMPDEB = 0
  w_CCCOSCOM = 0
  w_CCCOMVAL = 0
  w_CPROWORD = 0
  w_TOTDEB = 0
  w_TOTCRE = 0
  w_TOTCOM = 0
  w_DESCOR = space(35)
  w_OBTEST = space(8)
  w_CCCODVAL = space(3)
  w_DESCAU = space(35)
  w_CCOBSO = space(8)
  w_TOTALE = 0
  w_CCFLCOMM = space(1)
  w_CCNUMCOR = space(15)
  w_FLCRDE = space(1)
  w_CCCAOVAL = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CODEUR = space(3)
  w_DECEUR = 0
  w_CALCPIP = 0
  w_OLDCAU = space(5)
  w_OLDNUM = space(5)
  w_DATREG = space(8)
  w_SIMVAL = space(5)
  w_CCDESCRI = space(50)
  w_DATDOC = space(8)
  w_CCNUMREG = 0
  w_CCNUMDOC = 0
  w_CC__ANNO = space(4)
  w_CCALFDOC = space(10)
  proc Save(i_oFrom)
    this.w_CCSERIAL = i_oFrom.w_CCSERIAL
    this.w_CCROWRIF = i_oFrom.w_CCROWRIF
    this.w_CCCODCAU = i_oFrom.w_CCCODCAU
    this.w_CCFLCRED = i_oFrom.w_CCFLCRED
    this.w_CCFLDEBI = i_oFrom.w_CCFLDEBI
    this.w_FLCOMP = i_oFrom.w_FLCOMP
    this.w_CCNUMCOR = i_oFrom.w_CCNUMCOR
    this.w_CONCOL = i_oFrom.w_CONCOL
    this.w_TIPCAU = i_oFrom.w_TIPCAU
    this.w_NUMCOR = i_oFrom.w_NUMCOR
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_BAVAL = i_oFrom.w_BAVAL
    this.w_CALFIR = i_oFrom.w_CALFIR
    this.w_CCDATVAL = i_oFrom.w_CCDATVAL
    this.w_CCIMPCRE = i_oFrom.w_CCIMPCRE
    this.w_CCIMPDEB = i_oFrom.w_CCIMPDEB
    this.w_CCCOSCOM = i_oFrom.w_CCCOSCOM
    this.w_CCCOMVAL = i_oFrom.w_CCCOMVAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_TOTDEB = i_oFrom.w_TOTDEB
    this.w_TOTCRE = i_oFrom.w_TOTCRE
    this.w_TOTCOM = i_oFrom.w_TOTCOM
    this.w_DESCOR = i_oFrom.w_DESCOR
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CCCODVAL = i_oFrom.w_CCCODVAL
    this.w_DESCAU = i_oFrom.w_DESCAU
    this.w_CCOBSO = i_oFrom.w_CCOBSO
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_CCFLCOMM = i_oFrom.w_CCFLCOMM
    this.w_CCNUMCOR = i_oFrom.w_CCNUMCOR
    this.w_FLCRDE = i_oFrom.w_FLCRDE
    this.w_CCCAOVAL = i_oFrom.w_CCCAOVAL
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_CODEUR = i_oFrom.w_CODEUR
    this.w_DECEUR = i_oFrom.w_DECEUR
    this.w_CALCPIP = i_oFrom.w_CALCPIP
    this.w_OLDCAU = i_oFrom.w_OLDCAU
    this.w_OLDNUM = i_oFrom.w_OLDNUM
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_DATDOC = i_oFrom.w_DATDOC
    this.w_CCNUMREG = i_oFrom.w_CCNUMREG
    this.w_CCNUMDOC = i_oFrom.w_CCNUMDOC
    this.w_CC__ANNO = i_oFrom.w_CC__ANNO
    this.w_CCALFDOC = i_oFrom.w_CCALFDOC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CCSERIAL = this.w_CCSERIAL
    i_oTo.w_CCROWRIF = this.w_CCROWRIF
    i_oTo.w_CCCODCAU = this.w_CCCODCAU
    i_oTo.w_CCFLCRED = this.w_CCFLCRED
    i_oTo.w_CCFLDEBI = this.w_CCFLDEBI
    i_oTo.w_FLCOMP = this.w_FLCOMP
    i_oTo.w_CCNUMCOR = this.w_CCNUMCOR
    i_oTo.w_CONCOL = this.w_CONCOL
    i_oTo.w_TIPCAU = this.w_TIPCAU
    i_oTo.w_NUMCOR = this.w_NUMCOR
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_BAVAL = this.w_BAVAL
    i_oTo.w_CALFIR = this.w_CALFIR
    i_oTo.w_CCDATVAL = this.w_CCDATVAL
    i_oTo.w_CCIMPCRE = this.w_CCIMPCRE
    i_oTo.w_CCIMPDEB = this.w_CCIMPDEB
    i_oTo.w_CCCOSCOM = this.w_CCCOSCOM
    i_oTo.w_CCCOMVAL = this.w_CCCOMVAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_TOTDEB = this.w_TOTDEB
    i_oTo.w_TOTCRE = this.w_TOTCRE
    i_oTo.w_TOTCOM = this.w_TOTCOM
    i_oTo.w_DESCOR = this.w_DESCOR
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CCCODVAL = this.w_CCCODVAL
    i_oTo.w_DESCAU = this.w_DESCAU
    i_oTo.w_CCOBSO = this.w_CCOBSO
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_CCFLCOMM = this.w_CCFLCOMM
    i_oTo.w_CCNUMCOR = this.w_CCNUMCOR
    i_oTo.w_FLCRDE = this.w_FLCRDE
    i_oTo.w_CCCAOVAL = this.w_CCCAOVAL
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_CODEUR = this.w_CODEUR
    i_oTo.w_DECEUR = this.w_DECEUR
    i_oTo.w_CALCPIP = this.w_CALCPIP
    i_oTo.w_OLDCAU = this.w_OLDCAU
    i_oTo.w_OLDNUM = this.w_OLDNUM
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_DATDOC = this.w_DATDOC
    i_oTo.w_CCNUMREG = this.w_CCNUMREG
    i_oTo.w_CCNUMDOC = this.w_CCNUMDOC
    i_oTo.w_CC__ANNO = this.w_CC__ANNO
    i_oTo.w_CCALFDOC = this.w_CCALFDOC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mmc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 747
  Height = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-13"
  HelpContextID=197797225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CCM_DETT_IDX = 0
  COC_MAST_IDX = 0
  CCC_MAST_IDX = 0
  VALUTE_IDX = 0
  CCC_DETT_IDX = 0
  cFile = "CCM_DETT"
  cKeySelect = "CCSERIAL,CCROWRIF"
  cKeyWhere  = "CCSERIAL=this.w_CCSERIAL and CCROWRIF=this.w_CCROWRIF"
  cKeyDetail  = "CCSERIAL=this.w_CCSERIAL and CCROWRIF=this.w_CCROWRIF"
  cKeyWhereODBC = '"CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';

  cKeyDetailWhereODBC = '"CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CCM_DETT.CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL)';
      +'+" and CCM_DETT.CCROWRIF="+cp_ToStrODBC(this.w_CCROWRIF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCM_DETT.CPROWORD '
  cPrg = "gscg_mmc"
  cComment = "Dettaglio movimenti conti correnti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCSERIAL = space(10)
  w_CCROWRIF = 0
  w_CCCODCAU = space(5)
  o_CCCODCAU = space(5)
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_FLCOMP = space(1)
  w_CCNUMCOR = space(15)
  o_CCNUMCOR = space(15)
  w_CONCOL = space(15)
  w_TIPCAU = space(1)
  w_NUMCOR = space(15)
  o_NUMCOR = space(15)
  w_TIPCON = space(1)
  w_BAVAL = space(3)
  w_CALFIR = space(3)
  w_CCDATVAL = ctod('  /  /  ')
  w_CCIMPCRE = 0
  o_CCIMPCRE = 0
  w_CCIMPDEB = 0
  o_CCIMPDEB = 0
  w_CCCOSCOM = 0
  o_CCCOSCOM = 0
  w_CCCOMVAL = 0
  o_CCCOMVAL = 0
  w_CPROWORD = 0
  o_CPROWORD = 0
  w_TOTDEB = 0
  w_TOTCRE = 0
  w_TOTCOM = 0
  w_DESCOR = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_CCCODVAL = space(3)
  w_DESCAU = space(35)
  w_CCOBSO = ctod('  /  /  ')
  w_TOTALE = 0
  w_CCFLCOMM = space(1)
  w_CCNUMCOR = space(15)
  w_FLCRDE = space(1)
  w_CCCAOVAL = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CODEUR = space(3)
  w_DECEUR = 0
  w_CALCPIP = 0
  w_OLDCAU = space(5)
  w_OLDNUM = space(5)
  w_DATREG = ctod('  /  /  ')
  o_DATREG = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_CCDESCRI = space(50)
  w_DATDOC = ctod('  /  /  ')
  w_CCNUMREG = 0
  w_CCNUMDOC = 0
  w_CC__ANNO = space(4)
  w_CCALFDOC = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_mmc
  Procedure InitSon
  With This
  .w_CCDESCRI=IIF(Left(.Oparentobject.w_PNDESRIG+space(50),50) <> Left(.Oparentobject.w_PNDESSUP + space(50),50)And Not Empty(.Oparentobject.w_PNDESRIG),.Oparentobject.w_PNDESRIG,.Oparentobject.w_PNDESSUP)
  .w_CCNUMREG=.oParentObject.w_PNNUMRER
  .w_CC__ANNO=.oParentObject.w_PNCOMPET
  .w_CCNUMDOC=.oParentObject.w_PNNUMDOC
  .w_DATDOC=.oParentObject.w_PNDATDOC
  .w_DATREG=.oParentObject.w_PNDATREG
  .w_CCALFDOC=.oParentObject.w_PNALFDOC
  .w_CCCAOVAL=.oParentObject.w_PNCAOVAL
  .w_CCCODVAL=.oParentObject.w_PNCODVAL
  
      .mCalc(.T.)
  		.SetControlsValue()
  		* Mostro / Nascondo i Controls
  		.mHideControls()
  EndWith
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mmcPag1","gscg_mmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='CCC_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CCC_DETT'
    this.cWorkTables[5]='CCM_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCM_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCM_DETT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mmc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CCM_DETT where CCSERIAL=KeySet.CCSERIAL
    *                            and CCROWRIF=KeySet.CCROWRIF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2],this.bLoadRecFilter,this.CCM_DETT_IDX,"gscg_mmc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCM_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCM_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CCM_DETT '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCSERIAL',this.w_CCSERIAL  ,'CCROWRIF',this.w_CCROWRIF  )
      select * from (i_cTable) CCM_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTDEB = 0
        .w_TOTCRE = 0
        .w_TOTCOM = 0
        .w_DECTOT = 0
        .w_DECEUR = 0
        .w_SIMVAL = space(5)
        .w_CCSERIAL = NVL(CCSERIAL,space(10))
        .w_CCROWRIF = NVL(CCROWRIF,0)
        .w_OBTEST = .w_DATREG
        .w_CCCODVAL = .w_CCCODVAL
          .link_1_4('Load')
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .w_CCCAOVAL = GETCAM(.w_CCCODVAL, I_DATSYS)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CODEUR = g_PERVAL
          .link_1_8('Load')
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .w_OLDCAU = .w_CCCODCAU
        .w_OLDNUM = .w_CCNUMCOR
        .w_DATREG = .w_DATREG
        .w_CCDESCRI = .w_CCDESCRI
        .w_DATDOC = .w_DATDOC
        .w_CCNUMREG = .w_CCNUMREG
        .w_CCNUMDOC = .w_CCNUMDOC
        .w_CC__ANNO = .w_CC__ANNO
        .w_CCALFDOC = .w_CCALFDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CCM_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTDEB = 0
      this.w_TOTCRE = 0
      this.w_TOTCOM = 0
      scan
        with this
          .w_FLCOMP = space(1)
          .w_CONCOL = space(15)
          .w_TIPCAU = space(1)
          .w_TIPCON = space(1)
          .w_BAVAL = space(3)
          .w_CALFIR = space(3)
          .w_DESCOR = space(35)
          .w_DESCAU = space(35)
          .w_CCOBSO = ctod("  /  /  ")
          .w_FLCRDE = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CCCODCAU = NVL(CCCODCAU,space(5))
          if link_2_1_joined
            this.w_CCCODCAU = NVL(CACODICE201,NVL(this.w_CCCODCAU,space(5)))
            this.w_TIPCAU = NVL(CATIPCON201,space(1))
            this.w_DESCAU = NVL(CADESCRI201,space(35))
            this.w_FLCOMP = NVL(CAFLCOMP201,space(1))
            this.w_FLCRDE = NVL(CAFLCRDE201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_CCFLCRED = NVL(CCFLCRED,space(1))
          .w_CCFLDEBI = NVL(CCFLDEBI,space(1))
          .w_CCNUMCOR = NVL(CCNUMCOR,space(15))
          if link_2_5_joined
            this.w_CCNUMCOR = NVL(BACODBAN205,NVL(this.w_CCNUMCOR,space(15)))
            this.w_CALFIR = NVL(BACALFES205,space(3))
            this.w_DESCOR = NVL(BADESCRI205,space(35))
            this.w_TIPCON = NVL(BATIPCON205,space(1))
            this.w_BAVAL = NVL(BACODVAL205,space(3))
            this.w_CCOBSO = NVL(cp_ToDate(BADTOBSO205),ctod("  /  /  "))
            this.w_CONCOL = NVL(BACONCOL205,space(15))
          else
          .link_2_5('Load')
          endif
        .w_NUMCOR = .w_CCNUMCOR
          .link_2_8('Load')
          .w_CCDATVAL = NVL(cp_ToDate(CCDATVAL),ctod("  /  /  "))
          .w_CCIMPCRE = NVL(CCIMPCRE,0)
          .w_CCIMPDEB = NVL(CCIMPDEB,0)
          .w_CCCOSCOM = NVL(CCCOSCOM,0)
          .w_CCCOMVAL = NVL(CCCOMVAL,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CCFLCOMM = NVL(CCFLCOMM,space(1))
          .w_CCNUMCOR = NVL(CCNUMCOR,space(15))
          * evitabile
          *.link_2_25('Load')
          select (this.cTrsName)
          append blank
          replace CCFLCRED with .w_CCFLCRED
          replace CCFLDEBI with .w_CCFLDEBI
          replace CCNUMCOR with .w_CCNUMCOR
          replace CCIMPCRE with .w_CCIMPCRE
          replace CCIMPDEB with .w_CCIMPDEB
          replace CCCOMVAL with .w_CCCOMVAL
          replace CCFLCOMM with .w_CCFLCOMM
          replace CCNUMCOR with .w_CCNUMCOR
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTDEB = .w_TOTDEB+.w_CCIMPDEB
          .w_TOTCRE = .w_TOTCRE+.w_CCIMPCRE
          .w_TOTCOM = .w_TOTCOM+.w_CCCOMVAL
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = .w_DATREG
        .w_CCCODVAL = .w_CCCODVAL
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .w_CCCAOVAL = GETCAM(.w_CCCODVAL, I_DATSYS)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CODEUR = g_PERVAL
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .w_OLDCAU = .w_CCCODCAU
        .w_OLDNUM = .w_CCNUMCOR
        .w_DATREG = .w_DATREG
        .w_CCDESCRI = .w_CCDESCRI
        .w_DATDOC = .w_DATDOC
        .w_CCNUMREG = .w_CCNUMREG
        .w_CCNUMDOC = .w_CCNUMDOC
        .w_CC__ANNO = .w_CC__ANNO
        .w_CCALFDOC = .w_CCALFDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_30.enabled = .oPgFrm.Page1.oPag.oBtn_1_30.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CCSERIAL=space(10)
      .w_CCROWRIF=0
      .w_CCCODCAU=space(5)
      .w_CCFLCRED=space(1)
      .w_CCFLDEBI=space(1)
      .w_FLCOMP=space(1)
      .w_CCNUMCOR=space(15)
      .w_CONCOL=space(15)
      .w_TIPCAU=space(1)
      .w_NUMCOR=space(15)
      .w_TIPCON=space(1)
      .w_BAVAL=space(3)
      .w_CALFIR=space(3)
      .w_CCDATVAL=ctod("  /  /  ")
      .w_CCIMPCRE=0
      .w_CCIMPDEB=0
      .w_CCCOSCOM=0
      .w_CCCOMVAL=0
      .w_CPROWORD=10
      .w_TOTDEB=0
      .w_TOTCRE=0
      .w_TOTCOM=0
      .w_DESCOR=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_CCCODVAL=space(3)
      .w_DESCAU=space(35)
      .w_CCOBSO=ctod("  /  /  ")
      .w_TOTALE=0
      .w_CCFLCOMM=space(1)
      .w_CCNUMCOR=space(15)
      .w_FLCRDE=space(1)
      .w_CCCAOVAL=0
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CODEUR=space(3)
      .w_DECEUR=0
      .w_CALCPIP=0
      .w_OLDCAU=space(5)
      .w_OLDNUM=space(5)
      .w_DATREG=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_CCDESCRI=space(50)
      .w_DATDOC=ctod("  /  /  ")
      .w_CCNUMREG=0
      .w_CCNUMDOC=0
      .w_CC__ANNO=space(4)
      .w_CCALFDOC=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CCCODCAU = THIS.oParentObject .w_CAUMOV
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CCCODCAU))
         .link_2_1('Full')
        endif
        .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
        .DoRTCalc(6,6,.f.)
        .w_CCNUMCOR = THIS.oParentObject .w_CONBAN
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CCNUMCOR))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_NUMCOR = .w_CCNUMCOR
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_NUMCOR))
         .link_2_8('Full')
        endif
        .DoRTCalc(11,13,.f.)
        .w_CCDATVAL = IIF(G_BANC='S',CALCFEST(.w_DATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU, 1),cp_CharToDate('  -  -    '))
        .DoRTCalc(15,17,.f.)
        .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0,cp_ROUND(.w_CCCOSCOM*.w_CCCAOVAL, .w_DECTOT),0)
        .DoRTCalc(19,23,.f.)
        .w_OBTEST = .w_DATREG
        .w_CCCODVAL = .w_CCCODVAL
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CCCODVAL))
         .link_1_4('Full')
        endif
        .DoRTCalc(26,27,.f.)
        .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        .w_CCFLCOMM = '+'
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CCNUMCOR))
         .link_2_25('Full')
        endif
        .DoRTCalc(31,31,.f.)
        .w_CCCAOVAL = GETCAM(.w_CCCODVAL, I_DATSYS)
        .DoRTCalc(33,33,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CODEUR = g_PERVAL
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CODEUR))
         .link_1_8('Full')
        endif
        .DoRTCalc(36,36,.f.)
        .w_CALCPIP = DEFPIP(.w_DECEUR)
        .w_OLDCAU = .w_CCCODCAU
        .w_OLDNUM = .w_CCNUMCOR
        .w_DATREG = .w_DATREG
        .DoRTCalc(41,41,.f.)
        .w_CCDESCRI = .w_CCDESCRI
        .w_DATDOC = .w_DATDOC
        .w_CCNUMREG = .w_CCNUMREG
        .w_CCNUMDOC = .w_CCNUMDOC
        .w_CC__ANNO = .w_CC__ANNO
        .w_CCALFDOC = .w_CCALFDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCM_DETT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_mmc
    this.InitSon()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CCM_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gscg_mmc
    this.InitSon()
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCSERIAL,"CCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCROWRIF,"CCROWRIF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CCCODCAU C(5);
      ,t_CCNUMCOR C(15);
      ,t_CCDATVAL D(8);
      ,t_CCIMPCRE N(18,4);
      ,t_CCIMPDEB N(18,4);
      ,t_CCCOSCOM N(12,4);
      ,t_CPROWORD N(4);
      ,t_DESCOR C(35);
      ,t_DESCAU C(35);
      ,CCFLCRED C(1);
      ,CCFLDEBI C(1);
      ,CCNUMCOR C(15);
      ,CCIMPCRE N(18,4);
      ,CCIMPDEB N(18,4);
      ,CCCOMVAL N(12,4);
      ,CCFLCOMM C(1);
      ,CPROWNUM N(10);
      ,t_CCFLCRED C(1);
      ,t_CCFLDEBI C(1);
      ,t_FLCOMP C(1);
      ,t_CONCOL C(15);
      ,t_TIPCAU C(1);
      ,t_NUMCOR C(15);
      ,t_TIPCON C(1);
      ,t_BAVAL C(3);
      ,t_CALFIR C(3);
      ,t_CCCOMVAL N(12,4);
      ,t_CCOBSO D(8);
      ,t_CCFLCOMM C(1);
      ,t_FLCRDE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mmcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.controlsource=this.cTrsName+'.t_CCCODCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_5.controlsource=this.cTrsName+'.t_CCNUMCOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_12.controlsource=this.cTrsName+'.t_CCDATVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_13.controlsource=this.cTrsName+'.t_CCIMPCRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_14.controlsource=this.cTrsName+'.t_CCIMPDEB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_15.controlsource=this.cTrsName+'.t_CCCOSCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_17.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oDESCOR_2_19.controlsource=this.cTrsName+'.t_DESCOR'
    this.oPgFRm.Page1.oPag.oDESCAU_2_20.controlsource=this.cTrsName+'.t_DESCAU'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(50)
    this.AddVLine(119)
    this.AddVLine(245)
    this.AddVLine(319)
    this.AddVLine(460)
    this.AddVLine(601)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      *
      * insert into CCM_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCM_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'CCM_DETT')
        i_cFldBody=" "+;
                  "(CCSERIAL,CCROWRIF,CCCODCAU,CCFLCRED,CCFLDEBI"+;
                  ",CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CCCOSCOM"+;
                  ",CCCOMVAL,CPROWORD,CCFLCOMM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCSERIAL)+","+cp_ToStrODBC(this.w_CCROWRIF)+","+cp_ToStrODBCNull(this.w_CCCODCAU)+","+cp_ToStrODBC(this.w_CCFLCRED)+","+cp_ToStrODBC(this.w_CCFLDEBI)+;
             ","+cp_ToStrODBCNull(this.w_CCNUMCOR)+","+cp_ToStrODBC(this.w_CCDATVAL)+","+cp_ToStrODBC(this.w_CCIMPCRE)+","+cp_ToStrODBC(this.w_CCIMPDEB)+","+cp_ToStrODBC(this.w_CCCOSCOM)+;
             ","+cp_ToStrODBC(this.w_CCCOMVAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CCFLCOMM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCM_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'CCM_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CCSERIAL',this.w_CCSERIAL,'CCROWRIF',this.w_CCROWRIF)
        INSERT INTO (i_cTable) (;
                   CCSERIAL;
                  ,CCROWRIF;
                  ,CCCODCAU;
                  ,CCFLCRED;
                  ,CCFLDEBI;
                  ,CCNUMCOR;
                  ,CCDATVAL;
                  ,CCIMPCRE;
                  ,CCIMPDEB;
                  ,CCCOSCOM;
                  ,CCCOMVAL;
                  ,CPROWORD;
                  ,CCFLCOMM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CCSERIAL;
                  ,this.w_CCROWRIF;
                  ,this.w_CCCODCAU;
                  ,this.w_CCFLCRED;
                  ,this.w_CCFLDEBI;
                  ,this.w_CCNUMCOR;
                  ,this.w_CCDATVAL;
                  ,this.w_CCIMPCRE;
                  ,this.w_CCIMPDEB;
                  ,this.w_CCCOSCOM;
                  ,this.w_CCCOMVAL;
                  ,this.w_CPROWORD;
                  ,this.w_CCFLCOMM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_CCCODCAU) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CCM_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CCM_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CCCODCAU) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCM_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CCM_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CCCODCAU="+cp_ToStrODBCNull(this.w_CCCODCAU)+;
                     ",CCFLCRED="+cp_ToStrODBC(this.w_CCFLCRED)+;
                     ",CCFLDEBI="+cp_ToStrODBC(this.w_CCFLDEBI)+;
                     ",CCNUMCOR="+cp_ToStrODBCNull(this.w_CCNUMCOR)+;
                     ",CCDATVAL="+cp_ToStrODBC(this.w_CCDATVAL)+;
                     ",CCIMPCRE="+cp_ToStrODBC(this.w_CCIMPCRE)+;
                     ",CCIMPDEB="+cp_ToStrODBC(this.w_CCIMPDEB)+;
                     ",CCCOSCOM="+cp_ToStrODBC(this.w_CCCOSCOM)+;
                     ",CCCOMVAL="+cp_ToStrODBC(this.w_CCCOMVAL)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CCFLCOMM="+cp_ToStrODBC(this.w_CCFLCOMM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CCM_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CCCODCAU=this.w_CCCODCAU;
                     ,CCFLCRED=this.w_CCFLCRED;
                     ,CCFLDEBI=this.w_CCFLDEBI;
                     ,CCNUMCOR=this.w_CCNUMCOR;
                     ,CCDATVAL=this.w_CCDATVAL;
                     ,CCIMPCRE=this.w_CCIMPCRE;
                     ,CCIMPDEB=this.w_CCIMPDEB;
                     ,CCCOSCOM=this.w_CCCOSCOM;
                     ,CCCOMVAL=this.w_CCCOMVAL;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CCFLCOMM=this.w_CCFLCOMM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCRED,space(1))==this.w_CCFLCRED;
              and NVL(&i_cF..CCIMPCRE,0)==this.w_CCIMPCRE;
              and NVL(&i_cF..CCFLDEBI,space(1))==this.w_CCFLDEBI;
              and NVL(&i_cF..CCIMPDEB,0)==this.w_CCIMPDEB;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..CCFLCRED,space(1)),'BASALCRE','',NVL(&i_cF..CCIMPCRE,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..CCFLDEBI,space(1)),'BASALDEB','',NVL(&i_cF..CCIMPDEB,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..CCNUMCOR,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" BASALCRE="+i_cOp1+","           +" BASALDEB="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(NVL(&i_cF..CCNUMCOR,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..CCFLCRED,'BASALCRE',i_cF+'.CCIMPCRE',&i_cF..CCIMPCRE,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..CCFLDEBI,'BASALDEB',i_cF+'.CCIMPDEB',&i_cF..CCIMPDEB,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',&i_cF..CCNUMCOR)
      UPDATE (i_cTable) SET ;
           BASALCRE=&i_cOp1.  ,;
           BASALDEB=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCOMM,space(1))==this.w_CCFLCOMM;
              and NVL(&i_cF..CCCOMVAL,0)==this.w_CCCOMVAL;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..CCFLCOMM,space(1)),'BASALDEB','',NVL(&i_cF..CCCOMVAL,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..CCNUMCOR,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" BASALDEB="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(NVL(&i_cF..CCNUMCOR,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..CCFLCOMM,'BASALDEB',i_cF+'.CCCOMVAL',&i_cF..CCCOMVAL,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',&i_cF..CCNUMCOR)
      UPDATE (i_cTable) SET ;
           BASALDEB=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCRED,space(1))==this.w_CCFLCRED;
              and NVL(&i_cF..CCIMPCRE,0)==this.w_CCIMPCRE;
              and NVL(&i_cF..CCFLDEBI,space(1))==this.w_CCFLDEBI;
              and NVL(&i_cF..CCIMPDEB,0)==this.w_CCIMPDEB;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(this.w_CCFLCRED,'BASALCRE','this.w_CCIMPCRE',this.w_CCIMPCRE,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_CCFLDEBI,'BASALDEB','this.w_CCIMPDEB',this.w_CCIMPDEB,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_CCNUMCOR)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" BASALCRE="+i_cOp1  +",";
         +" BASALDEB="+i_cOp2  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_CCFLCRED,'BASALCRE','this.w_CCIMPCRE',this.w_CCIMPCRE,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_CCFLDEBI,'BASALDEB','this.w_CCIMPDEB',this.w_CCIMPDEB,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',this.w_CCNUMCOR)
      UPDATE (i_cTable) SET;
           BASALCRE=&i_cOp1.  ,;
           BASALDEB=&i_cOp2.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..CCFLCOMM,space(1))==this.w_CCFLCOMM;
              and NVL(&i_cF..CCCOMVAL,0)==this.w_CCCOMVAL;
              and NVL(&i_cF..CCNUMCOR,space(15))==this.w_CCNUMCOR;

      i_cOp1=cp_SetTrsOp(this.w_CCFLCOMM,'BASALDEB','this.w_CCCOMVAL',this.w_CCCOMVAL,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_CCNUMCOR)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" BASALDEB="+i_cOp1  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_CCFLCOMM,'BASALDEB','this.w_CCCOMVAL',this.w_CCCOMVAL,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'BACODBAN',this.w_CCNUMCOR)
      UPDATE (i_cTable) SET;
           BASALDEB=&i_cOp1.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CCCODCAU) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CCM_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CCCODCAU) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCM_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCM_DETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CPROWORD<>.w_CPROWORD
          .link_2_1('Full')
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_CCCODCAU<>.w_CCCODCAU
          .link_2_5('Full')
        endif
        .DoRTCalc(8,9,.t.)
        if .o_CCNUMCOR<>.w_CCNUMCOR
          .w_NUMCOR = .w_CCNUMCOR
          .link_2_8('Full')
        endif
        .DoRTCalc(11,13,.t.)
        if .o_DATREG<>.w_DATREG.or. .o_CCCODCAU<>.w_CCCODCAU.or. .o_CCNUMCOR<>.w_CCNUMCOR
          .w_CCDATVAL = IIF(G_BANC='S',CALCFEST(.w_DATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU, 1),cp_CharToDate('  -  -    '))
        endif
        .DoRTCalc(15,17,.t.)
        if .o_CCCOSCOM<>.w_CCCOSCOM.or. .o_CCIMPCRE<>.w_CCIMPCRE.or. .o_CCIMPDEB<>.w_CCIMPDEB
          .w_TOTCOM = .w_TOTCOM-.w_cccomval
          .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0,cp_ROUND(.w_CCCOSCOM*.w_CCCAOVAL, .w_DECTOT),0)
          .w_TOTCOM = .w_TOTCOM+.w_cccomval
        endif
        .DoRTCalc(19,23,.t.)
        if .o_DATREG<>.w_DATREG
          .w_OBTEST = .w_DATREG
        endif
          .w_CCCODVAL = .w_CCCODVAL
          .link_1_4('Full')
        .DoRTCalc(26,27,.t.)
        if .o_CCIMPCRE<>.w_CCIMPCRE.or. .o_CCIMPDEB<>.w_CCIMPDEB.or. .o_CCCOSCOM<>.w_CCCOSCOM
          .w_TOTALE = .w_TOTCRE - (.w_TOTDEB + .w_TOTCOM)
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .w_CCFLCOMM = '+'
        endif
        if .o_CCCODCAU<>.w_CCCODCAU
          .link_2_25('Full')
        endif
        .DoRTCalc(31,31,.t.)
          .w_CCCAOVAL = GETCAM(.w_CCCODVAL, I_DATSYS)
        .DoRTCalc(33,33,.t.)
          .w_CALCPICT = DEFPIC(.w_DECTOT)
          .w_CODEUR = g_PERVAL
          .link_1_8('Full')
        .DoRTCalc(36,36,.t.)
          .w_CALCPIP = DEFPIP(.w_DECEUR)
          .w_OLDCAU = .w_CCCODCAU
          .w_OLDNUM = .w_CCNUMCOR
          .w_DATREG = .w_DATREG
        .DoRTCalc(41,41,.t.)
          .w_CCDESCRI = .w_CCDESCRI
        if .o_DATREG<>.w_DATREG
          .w_DATDOC = .w_DATDOC
        endif
          .w_CCNUMREG = .w_CCNUMREG
          .w_CCNUMDOC = .w_CCNUMDOC
          .w_CC__ANNO = .w_CC__ANNO
          .w_CCALFDOC = .w_CCALFDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CCFLCRED with this.w_CCFLCRED
      replace t_CCFLDEBI with this.w_CCFLDEBI
      replace t_FLCOMP with this.w_FLCOMP
      replace t_CONCOL with this.w_CONCOL
      replace t_TIPCAU with this.w_TIPCAU
      replace t_NUMCOR with this.w_NUMCOR
      replace t_TIPCON with this.w_TIPCON
      replace t_BAVAL with this.w_BAVAL
      replace t_CALFIR with this.w_CALFIR
      replace t_CCCOMVAL with this.w_CCCOMVAL
      replace t_CCOBSO with this.w_CCOBSO
      replace t_CCFLCOMM with this.w_CCFLCOMM
      replace t_CCNUMCOR with this.w_CCNUMCOR
      replace t_FLCRDE with this.w_FLCRDE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPCRE_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPCRE_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPDEB_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCIMPDEB_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCOSCOM_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCOSCOM_2_15.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODCAU
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_MAST_IDX,3]
    i_lTable = "CCC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2], .t., this.CCC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MCT',True,'CCC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CCCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CCCODCAU))
          select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODCAU)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CCC_MAST','*','CACODICE',cp_AbsName(oSource.parent,'oCCCODCAU_2_1'),i_cWhere,'GSBA_MCT',"Causali movimenti C\C",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CCCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCODCAU)
            select CACODICE,CATIPCON,CADESCRI,CAFLCOMP,CAFLCRDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODCAU = NVL(_Link_.CACODICE,space(5))
      this.w_TIPCAU = NVL(_Link_.CATIPCON,space(1))
      this.w_DESCAU = NVL(_Link_.CADESCRI,space(35))
      this.w_FLCOMP = NVL(_Link_.CAFLCOMP,space(1))
      this.w_FLCRDE = NVL(_Link_.CAFLCRDE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODCAU = space(5)
      endif
      this.w_TIPCAU = space(1)
      this.w_DESCAU = space(35)
      this.w_FLCOMP = space(1)
      this.w_FLCRDE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CCC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCC_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CATIPCON as CATIPCON201"+ ",link_2_1.CADESCRI as CADESCRI201"+ ",link_2_1.CAFLCOMP as CAFLCOMP201"+ ",link_2_1.CAFLCRDE as CAFLCRDE201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CCM_DETT.CCCODCAU=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CCM_DETT.CCCODCAU=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCNUMCOR
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CCNUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CCNUMCOR))
          select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCNUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCCNUMCOR_2_5'),i_cWhere,'GSTE_ACB',"Conti correnti",'GSBA_QCM.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCNUMCOR)
            select BACODBAN,BACALFES,BADESCRI,BATIPCON,BACODVAL,BADTOBSO,BACONCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCNUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_CALFIR = NVL(_Link_.BACALFES,space(3))
      this.w_DESCOR = NVL(_Link_.BADESCRI,space(35))
      this.w_TIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_BAVAL = NVL(_Link_.BACODVAL,space(3))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_CONCOL = NVL(_Link_.BACONCOL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CCNUMCOR = space(15)
      endif
      this.w_CALFIR = space(3)
      this.w_DESCOR = space(35)
      this.w_TIPCON = space(1)
      this.w_BAVAL = space(3)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_CONCOL = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CCNUMCOR) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_CCCODCAU, .w_TIPCAU, .w_CCNUMCOR, .w_TIPCON) AND (.w_CCOBSO>.w_DATREG OR EMPTY(.w_CCOBSO))) AND(.w_BAVAL=THIS.oParentObject .w_PNCODVAL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente inesistente o incongruente o obsoleto")
        endif
        this.w_CCNUMCOR = space(15)
        this.w_CALFIR = space(3)
        this.w_DESCOR = space(35)
        this.w_TIPCON = space(1)
        this.w_BAVAL = space(3)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_CONCOL = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.BACODBAN as BACODBAN205"+ ",link_2_5.BACALFES as BACALFES205"+ ",link_2_5.BADESCRI as BADESCRI205"+ ",link_2_5.BATIPCON as BATIPCON205"+ ",link_2_5.BACODVAL as BACODVAL205"+ ",link_2_5.BADTOBSO as BADTOBSO205"+ ",link_2_5.BACONCOL as BACONCOL205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on CCM_DETT.CCNUMCOR=link_2_5.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and CCM_DETT.CCNUMCOR=link_2_5.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NUMCOR
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCC_DETT_IDX,3]
    i_lTable = "CCC_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2], .t., this.CCC_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CANUMCOR,CAIMPCOM";
                   +" from "+i_cTable+" "+i_lTable+" where CANUMCOR="+cp_ToStrODBC(this.w_NUMCOR);
                   +" and CACODICE="+cp_ToStrODBC(this.w_CCCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CCCODCAU;
                       ,'CANUMCOR',this.w_NUMCOR)
            select CACODICE,CANUMCOR,CAIMPCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOR = NVL(_Link_.CANUMCOR,space(15))
      this.w_CCCOSCOM = NVL(_Link_.CAIMPCOM,0)
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOR = space(15)
      endif
      this.w_CCCOSCOM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCC_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)+'\'+cp_ToStr(_Link_.CANUMCOR,1)
      cp_ShowWarn(i_cKey,this.CCC_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODVAL
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CCCODVAL)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CCCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CCCAOVAL = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCNUMCOR
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCNUMCOR)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCNUMCOR = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CCNUMCOR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODEUR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODEUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODEUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODEUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODEUR)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODEUR = NVL(_Link_.VACODVAL,space(3))
      this.w_DECEUR = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODEUR = space(3)
      endif
      this.w_DECEUR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODEUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCOR_2_19.value==this.w_DESCOR)
      this.oPgFrm.Page1.oPag.oDESCOR_2_19.value=this.w_DESCOR
      replace t_DESCOR with this.oPgFrm.Page1.oPag.oDESCOR_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODVAL_1_4.value==this.w_CCCODVAL)
      this.oPgFrm.Page1.oPag.oCCCODVAL_1_4.value=this.w_CCCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_2_20.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_2_20.value=this.w_DESCAU
      replace t_DESCAU with this.oPgFrm.Page1.oPag.oDESCAU_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCAOVAL_1_5.value==this.w_CCCAOVAL)
      this.oPgFrm.Page1.oPag.oCCCAOVAL_1_5.value=this.w_CCCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_13.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_13.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_19.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_19.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_21.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_21.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMREG_1_23.value==this.w_CCNUMREG)
      this.oPgFrm.Page1.oPag.oCCNUMREG_1_23.value=this.w_CCNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMDOC_1_24.value==this.w_CCNUMDOC)
      this.oPgFrm.Page1.oPag.oCCNUMDOC_1_24.value=this.w_CCNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCC__ANNO_1_27.value==this.w_CC__ANNO)
      this.oPgFrm.Page1.oPag.oCC__ANNO_1_27.value=this.w_CC__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCCALFDOC_1_29.value==this.w_CCALFDOC)
      this.oPgFrm.Page1.oPag.oCCALFDOC_1_29.value=this.w_CCALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value==this.w_CCCODCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value=this.w_CCCODCAU
      replace t_CCCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODCAU_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_5.value==this.w_CCNUMCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_5.value=this.w_CCNUMCOR
      replace t_CCNUMCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_12.value==this.w_CCDATVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_12.value=this.w_CCDATVAL
      replace t_CCDATVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDATVAL_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_13.value==this.w_CCIMPCRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_13.value=this.w_CCIMPCRE
      replace t_CCIMPCRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPCRE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_14.value==this.w_CCIMPDEB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_14.value=this.w_CCIMPDEB
      replace t_CCIMPDEB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCIMPDEB_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_15.value==this.w_CCCOSCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_15.value=this.w_CCCOSCOM
      replace t_CCCOSCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCOSCOM_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_17.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_17.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_17.value
    endif
    cp_SetControlsValueExtFlds(this,'CCM_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((EMPTY(.w_CCNUMCOR) OR .w_FLCOMP<>'N' OR CHKNUMCR(.w_CCCODCAU, .w_TIPCAU, .w_CCNUMCOR, .w_TIPCON) AND (.w_CCOBSO>.w_DATREG OR EMPTY(.w_CCOBSO))) AND(.w_BAVAL=THIS.oParentObject .w_PNCODVAL)) and not(empty(.w_CCNUMCOR)) and (NOT EMPTY(.w_CCCODCAU) AND (.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCNUMCOR_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto corrente inesistente o incongruente o obsoleto")
      endcase
      if NOT EMPTY(.w_CCCODCAU) AND (.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCCODCAU = this.w_CCCODCAU
    this.o_CCNUMCOR = this.w_CCNUMCOR
    this.o_NUMCOR = this.w_NUMCOR
    this.o_CCIMPCRE = this.w_CCIMPCRE
    this.o_CCIMPDEB = this.w_CCIMPDEB
    this.o_CCCOSCOM = this.w_CCCOSCOM
    this.o_CCCOMVAL = this.w_CCCOMVAL
    this.o_CPROWORD = this.w_CPROWORD
    this.o_DATREG = this.w_DATREG
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CCCODCAU) AND (t_CCIMPCRE<>0 OR t_CCIMPDEB<>0))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CCCODCAU=space(5)
      .w_CCFLCRED=space(1)
      .w_CCFLDEBI=space(1)
      .w_FLCOMP=space(1)
      .w_CCNUMCOR=space(15)
      .w_CONCOL=space(15)
      .w_TIPCAU=space(1)
      .w_NUMCOR=space(15)
      .w_TIPCON=space(1)
      .w_BAVAL=space(3)
      .w_CALFIR=space(3)
      .w_CCDATVAL=ctod("  /  /  ")
      .w_CCIMPCRE=0
      .w_CCIMPDEB=0
      .w_CCCOSCOM=0
      .w_CCCOMVAL=0
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_DESCOR=space(35)
      .w_DESCAU=space(35)
      .w_CCOBSO=ctod("  /  /  ")
      .w_CCFLCOMM=space(1)
      .w_CCNUMCOR=space(15)
      .w_FLCRDE=space(1)
      .DoRTCalc(1,2,.f.)
        .w_CCCODCAU = THIS.oParentObject .w_CAUMOV
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_CCCODCAU))
        .link_2_1('Full')
      endif
        .w_CCFLCRED = IIF(.w_FLCRDE='C', '+', ' ')
        .w_CCFLDEBI = IIF(.w_FLCRDE='D', '+', ' ')
      .DoRTCalc(6,6,.f.)
        .w_CCNUMCOR = THIS.oParentObject .w_CONBAN
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CCNUMCOR))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,9,.f.)
        .w_NUMCOR = .w_CCNUMCOR
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_NUMCOR))
        .link_2_8('Full')
      endif
      .DoRTCalc(11,13,.f.)
        .w_CCDATVAL = IIF(G_BANC='S',CALCFEST(.w_DATREG, .w_CALFIR, .w_CCNUMCOR, .w_CCCODCAU, .w_TIPCAU, 1),cp_CharToDate('  -  -    '))
      .DoRTCalc(15,17,.f.)
        .w_CCCOMVAL = IIF(.w_CCIMPCRE<>0 OR .w_CCIMPDEB<>0,cp_ROUND(.w_CCCOSCOM*.w_CCCAOVAL, .w_DECTOT),0)
      .DoRTCalc(19,28,.f.)
        .w_CCFLCOMM = '+'
      .DoRTCalc(30,30,.f.)
      if not(empty(.w_CCNUMCOR))
        .link_2_25('Full')
      endif
    endwith
    this.DoRTCalc(31,47,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CCCODCAU = t_CCCODCAU
    this.w_CCFLCRED = t_CCFLCRED
    this.w_CCFLDEBI = t_CCFLDEBI
    this.w_FLCOMP = t_FLCOMP
    this.w_CCNUMCOR = t_CCNUMCOR
    this.w_CONCOL = t_CONCOL
    this.w_TIPCAU = t_TIPCAU
    this.w_NUMCOR = t_NUMCOR
    this.w_TIPCON = t_TIPCON
    this.w_BAVAL = t_BAVAL
    this.w_CALFIR = t_CALFIR
    this.w_CCDATVAL = t_CCDATVAL
    this.w_CCIMPCRE = t_CCIMPCRE
    this.w_CCIMPDEB = t_CCIMPDEB
    this.w_CCCOSCOM = t_CCCOSCOM
    this.w_CCCOMVAL = t_CCCOMVAL
    this.w_CPROWORD = t_CPROWORD
    this.w_DESCOR = t_DESCOR
    this.w_DESCAU = t_DESCAU
    this.w_CCOBSO = t_CCOBSO
    this.w_CCFLCOMM = t_CCFLCOMM
    this.w_CCNUMCOR = t_CCNUMCOR
    this.w_FLCRDE = t_FLCRDE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CCCODCAU with this.w_CCCODCAU
    replace t_CCFLCRED with this.w_CCFLCRED
    replace t_CCFLDEBI with this.w_CCFLDEBI
    replace t_FLCOMP with this.w_FLCOMP
    replace t_CCNUMCOR with this.w_CCNUMCOR
    replace t_CONCOL with this.w_CONCOL
    replace t_TIPCAU with this.w_TIPCAU
    replace t_NUMCOR with this.w_NUMCOR
    replace t_TIPCON with this.w_TIPCON
    replace t_BAVAL with this.w_BAVAL
    replace t_CALFIR with this.w_CALFIR
    replace t_CCDATVAL with this.w_CCDATVAL
    replace t_CCIMPCRE with this.w_CCIMPCRE
    replace t_CCIMPDEB with this.w_CCIMPDEB
    replace t_CCCOSCOM with this.w_CCCOSCOM
    replace t_CCCOMVAL with this.w_CCCOMVAL
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DESCOR with this.w_DESCOR
    replace t_DESCAU with this.w_DESCAU
    replace t_CCOBSO with this.w_CCOBSO
    replace t_CCFLCOMM with this.w_CCFLCOMM
    replace t_CCNUMCOR with this.w_CCNUMCOR
    replace t_FLCRDE with this.w_FLCRDE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTCRE = .w_TOTCRE-.w_ccimpcre
        .w_TOTDEB = .w_TOTDEB-.w_ccimpdeb
        .w_TOTCOM = .w_TOTCOM-.w_cccomval
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mmcPag1 as StdContainer
  Width  = 743
  height = 336
  stdWidth  = 743
  stdheight = 336
  resizeXpos=162
  resizeYpos=228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODVAL_1_4 as StdField with uid="NJHOUGAGFN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CCCODVAL", cQueryName = "CCCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta movimento",;
    HelpContextID = 248074866,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=111, Top=97, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CCCODVAL"

  func oCCCODVAL_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCCCAOVAL_1_5 as StdField with uid="RMDGPGRAGF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CCCAOVAL", cQueryName = "CCCAOVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio valuta movimento",;
    HelpContextID = 258691698,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=338, Top=97

  add object oDATREG_1_13 as StdField with uid="SMOUPPFGPB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione movimento",;
    HelpContextID = 2269130,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=391, Top=12

  add object oSIMVAL_1_16 as StdField with uid="PBBZSYCMYJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo valuta movimento",;
    HelpContextID = 190777050,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=216, Top=97, InputMask=replicate('X',5)

  add object oCCDESCRI_1_19 as StdField with uid="DYJJEQGRVM",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione movimento",;
    HelpContextID = 212820591,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=111, Top=69, InputMask=replicate('X',50)

  add object oDATDOC_1_21 as StdField with uid="CXSEFQNXLU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 59809738,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=391, Top=40

  add object oCCNUMREG_1_23 as StdField with uid="QUIUSKUXKO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CCNUMREG", cQueryName = "CCNUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 190841453,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oCCNUMDOC_1_24 as StdField with uid="AGXTRGWSNW",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CCNUMDOC", cQueryName = "CCNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 44039575,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=40

  add object oCC__ANNO_1_27 as StdField with uid="OYYZRCYXAO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CC__ANNO", cQueryName = "CC__ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Serie del movimento",;
    HelpContextID = 156560779,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=188, Top=12, InputMask=replicate('X',4)

  add object oCCALFDOC_1_29 as StdField with uid="RENVWYVYGB",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CCALFDOC", cQueryName = "CCALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 52022679,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=243, Top=40, InputMask=replicate('X',10)


  add object oBtn_1_30 as StdButton with uid="XVFSKZRQBE",left=684, top=17, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per calcolo automatico movimenti di C\C da modello contabile associato";
    , HelpContextID = 126877734;
    ,  Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        do GSCG_BMC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    with this.Parent.oContained
      return ((.oParentObject.cFunction='Edit' ) OR (.oParentObject.cFunction='Load'))
    endwith
  endfunc

  func oBtn_1_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! ((.oParentObject.cFunction='Edit' ) OR (.oParentObject.cFunction='Load')))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=131, width=730,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="CCCODCAU",Label2="Causale",Field3="CCNUMCOR",Label3="Conto corrente",Field4="CCDATVAL",Label4="Data valuta",Field5="CCIMPCRE",Label5="Importo creditore",Field6="CCIMPDEB",Label6="Importo debitore",Field7="CCCOSCOM",Label7="Commissioni",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118293626

  add object oStr_1_14 as StdString with uid="PIBXYTSFHV",Visible=.t., Left=27, Top=12,;
    Alignment=1, Width=81, Height=18,;
    Caption="Numero reg.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="MOVWGJCPPY",Visible=.t., Left=353, Top=12,;
    Alignment=1, Width=36, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="YPEFVLOMIH",Visible=.t., Left=271, Top=97,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZUEDRMSYDX",Visible=.t., Left=53, Top=97,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JJSAIIWHLS",Visible=.t., Left=26, Top=69,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TBQQUGLXGU",Visible=.t., Left=353, Top=40,;
    Alignment=1, Width=36, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WDYPMCHIQC",Visible=.t., Left=26, Top=40,;
    Alignment=1, Width=82, Height=18,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JKMXNBHTLF",Visible=.t., Left=175, Top=12,;
    Alignment=2, Width=12, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PPKYLJWAEF",Visible=.t., Left=230, Top=40,;
    Alignment=2, Width=12, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=150,;
    width=726+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=151,width=725+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CCC_MAST|COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCOR_2_19.Refresh()
      this.Parent.oDESCAU_2_20.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CCC_MAST'
        oDropInto=this.oBodyCol.oRow.oCCCODCAU_2_1
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oCCNUMCOR_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCOR_2_19 as StdTrsField with uid="SHWKACJVFA",rtseq=23,rtrep=.t.,;
    cFormVar="w_DESCOR",value=space(35),enabled=.f.,;
    HelpContextID = 76655562,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=449, Top=310, InputMask=replicate('X',35)

  add object oDESCAU_2_20 as StdTrsField with uid="MYJKUGMVAK",rtseq=26,rtrep=.t.,;
    cFormVar="w_DESCAU",value=space(35),enabled=.f.,;
    HelpContextID = 41003978,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=89, Top=310, InputMask=replicate('X',35)

  add object oStr_2_22 as StdString with uid="WDAUNCDZUD",Visible=.t., Left=9, Top=311,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="YLRAHWPKPC",Visible=.t., Left=351, Top=311,;
    Alignment=1, Width=94, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oBox_2_18 as StdBox with uid="KSUKVGAQFG",left=7, top=171, width=1,height=130

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mmcBodyRow as CPBodyRowCnt
  Width=716
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCCCODCAU_2_1 as StdTrsField with uid="VNDAGABCOL",rtseq=3,rtrep=.t.,;
    cFormVar="w_CCCODCAU",value=space(5),;
    HelpContextID = 70692229,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=41, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCC_MAST", cZoomOnZoom="GSBA_MCT", oKey_1_1="CACODICE", oKey_1_2="this.w_CCCODCAU"

  func oCCCODCAU_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_NUMCOR)
        bRes2=.link_2_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCCCODCAU_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCCODCAU_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCC_MAST','*','CACODICE',cp_AbsName(this.parent,'oCCCODCAU_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MCT',"Causali movimenti C\C",'',this.parent.oContained
  endproc
  proc oCCCODCAU_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CCCODCAU
    i_obj.ecpSave()
  endproc

  add object oCCNUMCOR_2_5 as StdTrsField with uid="XAYZKSLDWN",rtseq=7,rtrep=.t.,;
    cFormVar="w_CCNUMCOR",value=space(15),;
    HelpContextID = 60816776,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente inesistente o incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=116, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CCNUMCOR"

  func oCCNUMCOR_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCNUMCOR_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCNUMCOR_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCCNUMCOR_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'GSBA_QCM.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCCNUMCOR_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CCNUMCOR
    i_obj.ecpSave()
  endproc

  add object oCCDATVAL_2_12 as StdTrsField with uid="GTFSQKUDSH",rtseq=14,rtrep=.t.,;
    cFormVar="w_CCDATVAL",value=ctod("  /  /  "),;
    HelpContextID = 263938674,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=236, Top=0

  add object oCCIMPCRE_2_13 as StdTrsField with uid="DPTQFKSANY",rtseq=15,rtrep=.t.,;
    cFormVar="w_CCIMPCRE",value=0,;
    HelpContextID = 210219627,;
    cTotal = "this.Parent.oContained.w_totcre", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=311, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  func oCCIMPCRE_2_13.mCond()
    with this.Parent.oContained
      return (.w_FLCRDE='C' AND NOT EMPTY(.w_CCNUMCOR))
    endwith
  endfunc

  add object oCCIMPDEB_2_14 as StdTrsField with uid="RHXYXEBUMZ",rtseq=16,rtrep=.t.,;
    cFormVar="w_CCIMPDEB",value=0,;
    HelpContextID = 226996840,;
    cTotal = "this.Parent.oContained.w_totdeb", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=451, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  func oCCIMPDEB_2_14.mCond()
    with this.Parent.oContained
      return (.w_FLCRDE='D' AND NOT EMPTY(.w_CCNUMCOR))
    endwith
  endfunc

  add object oCCCOSCOM_2_15 as StdTrsField with uid="CPGCQFMPYA",rtseq=17,rtrep=.t.,;
    cFormVar="w_CCCOSCOM",value=0,;
    HelpContextID = 54963597,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=119, Left=592, Top=0, cSayPict=[v_PV(32+VVP)], cGetPict=[v_GV(32+VVP)]

  func oCCCOSCOM_2_15.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CCNUMCOR) AND .w_TIPCON=' ')
    endwith
  endfunc

  add object oCPROWORD_2_17 as StdTrsField with uid="ZTHAAJMWJS",rtseq=19,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 150622058,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0
  add object oLast as LastKeyMover
  * ---
  func oCCCODCAU_2_1.When()
    return(.t.)
  proc oCCCODCAU_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCCCODCAU_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mmc','CCM_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCSERIAL=CCM_DETT.CCSERIAL";
  +" and "+i_cAliasName2+".CCROWRIF=CCM_DETT.CCROWRIF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
