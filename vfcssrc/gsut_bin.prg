* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bin                                                        *
*              Inizializza database                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_113]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-19                                                      *
* Last revis.: 2014-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPRG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bin",oParentObject,m.pPRG)
return(i_retval)

define class tgsut_bin as StdBatch
  * --- Local variables
  pPRG = space(1)
  w_strconnect = space(60)
  w_appo = space(100)
  * --- WorkFile variables
  AZIENDA_idx=0
  AZBACKUP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione database (da GSUT_KBK)
    if this.pPRG= "C"
      * --- Memorizzo il Path dell'ultimo BackUp
      this.oParentObject.w_PATBCK = ADDBS(jUSTPATH( this.oParentObject.w_PATBCK ))
      * --- Write into AZBACKUP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZBACKUP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZBACKUP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZBCKLOG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PATBCK),'AZBACKUP','AZBCKLOG');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
               )
      else
        update (i_cTable) set;
            AZBCKLOG = this.oParentObject.w_PATBCK;
            &i_ccchkf. ;
         where;
            AZCODAZI = i_codazi;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Leggo il Path dell'ultimo BackUp, valorizzo le variabili da mostrare nella
      *     maschera
      * --- Read from AZBACKUP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZBACKUP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2],.t.,this.AZBACKUP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZBCKLOG"+;
          " from "+i_cTable+" AZBACKUP where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZBCKLOG;
          from (i_cTable) where;
              AZCODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_patbck = NVL(cp_ToDate(_read_.AZBCKLOG),cp_NullValue(_read_.AZBCKLOG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo la stringa di connessione
      this.oParentObject.w_connessi = i_ServerConn[1,3]
      this.w_strconnect = sqlgetprop(i_ServerConn[1,2],"Connectstring")
      * --- Leggo il nome del Server per DB2 non lo posso recuperare dalla stringa di connessione
      if CP_DBTYPE<>"DB2"
        this.oParentObject.w_conn1 = alltrim(substr(this.w_strconnect,at("SERVER",this.w_strconnect)+7,40))
        if at(";",this.oParentObject.w_conn1)>0
          this.oParentObject.w_SERVERNAME = alltrim(substr(this.oParentObject.w_conn1,1,at(";",this.oParentObject.w_conn1)-1))
        else
          this.oParentObject.w_SERVERNAME = this.oParentObject.w_conn1
        endif
      else
        this.oParentObject.w_SERVERNAME = ""
      endif
      * --- Leggo il nome del database, a seconda dei server diversifico la ricerca...
      do case
        case CP_DBTYPE="SQLServer"
          this.oParentObject.w_conn1 = alltrim(substr(this.w_strconnect,at("DATABASE",this.w_strconnect)+9,40))
        case CP_DBTYPE="DB2"
          this.oParentObject.w_conn1 = alltrim(substr(this.w_strconnect,at("DBALIAS",this.w_strconnect)+8,40))
        case CP_DBTYPE="Oracle"
          * --- Dopo DSN nome database...
          this.oParentObject.w_conn1 = alltrim(substr(this.w_strconnect,at("DSN",this.w_strconnect)+4,40))
          * --- Cerco il ; per identificare eventuale nome del servizio...
          this.w_appo = alltrim(substr(this.w_strconnect,at("DBQ",this.w_strconnect)+4,40))
          if at(";",this.w_appo)>0
            this.oParentObject.w_service = substr(this.w_appo,1,at(";",this.w_appo)-1)
          else
            this.oParentObject.w_service = this.w_appo
          endif
        case CP_DBTYPE="PostgreSQL"
          * --- DSN=AHE_80_PGR;DATABASE=ahe80b;SERVER=localhost;PORT=5432;UID=postgres;PWD=zucchetti;CA=d;A6=;A7=100;A8=4096;B0=254;B1=4000;BI=2;C2=dd_;;CX=1c5223b;A1=7.4-2
          this.oParentObject.w_conn1 = alltrim(substr(this.w_strconnect,at("DATABASE",this.w_strconnect)+9,40))
          this.oParentObject.w_SERVERNAME = alltrim(substr(this.w_strconnect,at("SERVER",this.w_strconnect)+7,40))
          if at(";",this.oParentObject.w_SERVERNAME)>0
            this.oParentObject.w_SERVERNAME = left(this.oParentObject.w_SERVERNAME, at(";",this.oParentObject.w_SERVERNAME)-1)
          endif
          this.oParentObject.w_PORT = alltrim(substr(this.w_strconnect,at("PORT",this.w_strconnect)+5,40))
          if at(";",this.oParentObject.w_PORT)>0
            this.oParentObject.w_PORT = left(this.oParentObject.w_PORT, at(";",this.oParentObject.w_PORT)-1)
          endif
      endcase
      * --- Recupero nome da database sul quale eseguo il back Up (solo per la maschera)
      if at(";",this.oParentObject.w_conn1)>0
        this.oParentObject.w_OriDat = alltrim(substr(this.oParentObject.w_conn1,1,at(";",this.oParentObject.w_conn1)-1))
      else
        this.oParentObject.w_OriDat = this.oParentObject.w_conn1
      endif
      this.oParentObject.w_PATBCK = ADDBS(JUSTPATH(this.oParentObject.w_PATBCK))+NOME_BCK( .F. , this.oParentObject.w_ORIDAT ) 
      if CP_DBTYPE="PostgreSQL"
        this.oParentObject.w_PATBCK = FORCEEXT(this.oParentObject.w_PATBCK,".backup")
      endif
      * --- Legge la user id dalla connessione ODBC
      this.w_appo = alltrim(substr(this.w_strconnect,at("UID",this.w_strconnect)+4,40))
      if at(";",this.w_appo)>0
        this.oParentObject.w_user = substr(this.w_appo,1,at(";",this.w_appo)-1)
      else
        this.oParentObject.w_user = this.w_appo
      endif
      * --- Legge la password dalla connessione ODBC
      this.w_appo = alltrim(substr(this.w_strconnect,at("PWD",this.w_strconnect)+4,40))
      if at(";",this.w_appo)>0
        this.oParentObject.w_passwd = substr(this.w_appo,1,at(";",this.w_appo)-1)
      else
        this.oParentObject.w_passwd = this.w_appo
      endif
    endif
  endproc


  proc Init(oParentObject,pPRG)
    this.pPRG=pPRG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='AZBACKUP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPRG"
endproc
