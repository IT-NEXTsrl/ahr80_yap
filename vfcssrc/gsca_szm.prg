* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_szm                                                        *
*              Visualizza schede analitica                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_45]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2014-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_szm",oParentObject))

* --- Class definition
define class tgsca_szm as StdForm
  Top    = 1
  Left   = 6

  * --- Standard Properties
  Width  = 798
  Height = 575
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-29"
  HelpContextID=26573463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Constant Properties
  _IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  VOC_COST_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsca_szm"
  cComment = "Visualizza schede analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(3)
  w_FLGMOV = space(1)
  w_PROVE = space(1)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_FINESE = ctod('  /  /  ')
  w_FINESE2 = ctod('  /  /  ')
  w_ESE = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_PROVCONF = space(1)
  w_CODCON = space(15)
  o_CODCON = space(15)
  w_CODVOC = space(15)
  w_NOANALITICA = space(1)
  o_NOANALITICA = space(1)
  w_TIPMOV = space(10)
  w_SALDO = 0
  w_DATA = ctod('  /  /  ')
  w_NUMERO = 0
  w_ALFA = space(10)
  w_SERIALE = space(35)
  w_FLGDAVE = space(1)
  w_CAUSALE = space(40)
  w_FLANAL = space(1)
  w_DESPIA = space(40)
  w_DESPIA3 = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_IMPAVE = 0
  w_IMPDAR = 0
  w_PNROWNUM = 0
  w_CLADOC = space(2)
  w_DATREG = ctod('  /  /  ')
  w_CODCOM = space(15)
  w_DESPIA2 = space(40)
  w_INIESE = ctod('  /  /  ')
  w_PNCODCON = space(15)
  o_PNCODCON = space(15)
  w_ANDESCRI = space(40)
  w_TIPCON = space(10)
  w_TIPOCF = space(1)
  o_TIPOCF = space(1)
  w_CODCON1 = space(15)
  w_DESCRI = space(40)
  w_ZoomScad = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_szmPag1","gsca_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLGMOV_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='VOC_COST'
    this.cWorkTables[6]='CONTI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(3)
      .w_FLGMOV=space(1)
      .w_PROVE=space(1)
      .w_CODESE=space(4)
      .w_FINESE=ctod("  /  /  ")
      .w_FINESE2=ctod("  /  /  ")
      .w_ESE=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_PROVCONF=space(1)
      .w_CODCON=space(15)
      .w_CODVOC=space(15)
      .w_NOANALITICA=space(1)
      .w_TIPMOV=space(10)
      .w_SALDO=0
      .w_DATA=ctod("  /  /  ")
      .w_NUMERO=0
      .w_ALFA=space(10)
      .w_SERIALE=space(35)
      .w_FLGDAVE=space(1)
      .w_CAUSALE=space(40)
      .w_FLANAL=space(1)
      .w_DESPIA=space(40)
      .w_DESPIA3=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_IMPAVE=0
      .w_IMPDAR=0
      .w_PNROWNUM=0
      .w_CLADOC=space(2)
      .w_DATREG=ctod("  /  /  ")
      .w_CODCOM=space(15)
      .w_DESPIA2=space(40)
      .w_INIESE=ctod("  /  /  ")
      .w_PNCODCON=space(15)
      .w_ANDESCRI=space(40)
      .w_TIPCON=space(10)
      .w_TIPOCF=space(1)
      .w_CODCON1=space(15)
      .w_DESCRI=space(40)
        .w_AZIENDA = i_CODAZI
        .w_FLGMOV = 'E'
        .w_PROVE = 'T'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODESE))
          .link_1_4('Full')
        endif
        .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
          .DoRTCalc(6,7,.f.)
        .w_DATA1 = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        .w_DATA2 = IIF(EMPTY(.w_CODESE), g_FINESE, .w_FINESE)
        .w_PROVCONF = 'N'
        .w_CODCON = ''
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODCON))
          .link_1_11('Full')
        endif
        .w_CODVOC = ''
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODVOC))
          .link_1_12('Full')
        endif
        .w_NOANALITICA = 'N'
        .w_TIPMOV = .w_ZoomScad.getVar('FLDIRE')
          .DoRTCalc(15,15,.f.)
        .w_DATA = .w_ZoomScad.getVar('DATDOC')
        .w_NUMERO = .w_ZoomScad.getVar('NUMDOC')
        .w_ALFA = .w_ZoomScad.getVar('ALFDOC')
        .w_SERIALE = .w_ZoomScad.getVar('SERIAL')
          .DoRTCalc(20,20,.f.)
        .w_CAUSALE = .w_ZoomScad.getVar('DESVOC')
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
          .DoRTCalc(22,24,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(26,26,.f.)
        .w_IMPAVE = .w_ZoomScad.GetVar('IMPAVE')
        .w_IMPDAR = .w_ZoomScad.GetVar('IMPDAR')
        .w_PNROWNUM = .w_ZoomScad.GetVar('CPROWNUM')
        .w_CLADOC = .w_ZoomScad.getVar('CLADOC')
        .w_DATREG = .w_ZoomScad.GetVar('DATREG')
        .w_CODCOM = ''
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_CODCOM))
          .link_1_46('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(33,35,.f.)
        if not(empty(.w_PNCODCON))
          .link_1_52('Full')
        endif
          .DoRTCalc(36,36,.f.)
        .w_TIPCON = 'G'
        .w_TIPOCF = ' '
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_CODCON1))
          .link_1_57('Full')
        endif
    endwith
    this.DoRTCalc(40,40,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
        .DoRTCalc(6,7,.t.)
        if .o_CODESE<>.w_CODESE
            .w_DATA1 = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        endif
        if .o_CODESE<>.w_CODESE
            .w_DATA2 = IIF(EMPTY(.w_CODESE), g_FINESE, .w_FINESE)
        endif
        .DoRTCalc(10,10,.t.)
        if .o_NOANALITICA<>.w_NOANALITICA
            .w_CODCON = ''
          .link_1_11('Full')
        endif
        if .o_NOANALITICA<>.w_NOANALITICA
            .w_CODVOC = ''
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.t.)
            .w_TIPMOV = .w_ZoomScad.getVar('FLDIRE')
        .DoRTCalc(15,15,.t.)
            .w_DATA = .w_ZoomScad.getVar('DATDOC')
            .w_NUMERO = .w_ZoomScad.getVar('NUMDOC')
            .w_ALFA = .w_ZoomScad.getVar('ALFDOC')
            .w_SERIALE = .w_ZoomScad.getVar('SERIAL')
        .DoRTCalc(20,20,.t.)
            .w_CAUSALE = .w_ZoomScad.getVar('DESVOC')
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .DoRTCalc(22,26,.t.)
            .w_IMPAVE = .w_ZoomScad.GetVar('IMPAVE')
            .w_IMPDAR = .w_ZoomScad.GetVar('IMPDAR')
            .w_PNROWNUM = .w_ZoomScad.GetVar('CPROWNUM')
            .w_CLADOC = .w_ZoomScad.getVar('CLADOC')
            .w_DATREG = .w_ZoomScad.GetVar('DATREG')
        if .o_NOANALITICA<>.w_NOANALITICA
            .w_CODCOM = ''
          .link_1_46('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        if .o_TIPOCF<>.w_TIPOCF
          .Calculate_QWKRQHGBHI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,40,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
    endwith
  return

  proc Calculate_QWKRQHGBHI()
    with this
          * --- Sbianco intestatario
          .w_CODCON1 = iif(.w_TIPOCF=' ','',.w_CODCON1)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_11.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCODVOC_1_12.enabled = this.oPgFrm.Page1.oPag.oCODVOC_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCODCOM_1_46.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_46.mCond()
    this.oPgFrm.Page1.oPag.oCODCON1_1_57.enabled = this.oPgFrm.Page1.oPag.oCODCON1_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCODCOM_1_46.visible=!this.oPgFrm.Page1.oPag.oCODCOM_1_46.mHide()
    this.oPgFrm.Page1.oPag.oDESPIA2_1_47.visible=!this.oPgFrm.Page1.oPag.oDESPIA2_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_4'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CODCON))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_CODCON)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCODCON_1_11'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CODCON)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESPIA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVOC
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_CODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_CODVOC))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_CODVOC)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_CODVOC)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oCODVOC_1_12'),i_cWhere,'GSCA_AVC',"Voci di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_CODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_CODVOC)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESPIA3 = NVL(_Link_.VCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODVOC = space(15)
      endif
      this.w_DESPIA3 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce inesistente oppure obsoleto")
        endif
        this.w_CODVOC = space(15)
        this.w_DESPIA3 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_46'),i_cWhere,'GSAR_ACN',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPIA2 = NVL(_Link_.CNDESCAN,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESPIA2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        endif
        this.w_CODCOM = space(15)
        this.w_DESPIA2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCON
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PNCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPNCODCON_1_52'),i_cWhere,'GSAR_BZC',"Elenco conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PNCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_PNCODCON = space(15)
        this.w_ANDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON1
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCF;
                     ,'ANCODICE',trim(this.w_CODCON1))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCF);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON1_1_57'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCF;
                       ,'ANCODICE',this.w_CODCON1)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON1 = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CODCON1 = space(15)
        this.w_DESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLGMOV_1_2.RadioValue()==this.w_FLGMOV)
      this.oPgFrm.Page1.oPag.oFLGMOV_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVE_1_3.RadioValue()==this.w_PROVE)
      this.oPgFrm.Page1.oPag.oPROVE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_4.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_4.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_8.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_8.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_9.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_9.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVCONF_1_10.RadioValue()==this.w_PROVCONF)
      this.oPgFrm.Page1.oPag.oPROVCONF_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_11.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_11.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVOC_1_12.value==this.w_CODVOC)
      this.oPgFrm.Page1.oPag.oCODVOC_1_12.value=this.w_CODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOANALITICA_1_13.RadioValue()==this.w_NOANALITICA)
      this.oPgFrm.Page1.oPag.oNOANALITICA_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDO_1_20.value==this.w_SALDO)
      this.oPgFrm.Page1.oPag.oSALDO_1_20.value=this.w_SALDO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGDAVE_1_28.value==this.w_FLGDAVE)
      this.oPgFrm.Page1.oPag.oFLGDAVE_1_28.value=this.w_FLGDAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSALE_1_29.value==this.w_CAUSALE)
      this.oPgFrm.Page1.oPag.oCAUSALE_1_29.value=this.w_CAUSALE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_1_34.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_1_34.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA3_1_36.value==this.w_DESPIA3)
      this.oPgFrm.Page1.oPag.oDESPIA3_1_36.value=this.w_DESPIA3
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_46.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_46.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA2_1_47.value==this.w_DESPIA2)
      this.oPgFrm.Page1.oPag.oDESPIA2_1_47.value=this.w_DESPIA2
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCON_1_52.value==this.w_PNCODCON)
      this.oPgFrm.Page1.oPag.oPNCODCON_1_52.value=this.w_PNCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_53.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_53.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCF_1_56.RadioValue()==this.w_TIPOCF)
      this.oPgFrm.Page1.oPag.oTIPOCF_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON1_1_57.value==this.w_CODCON1)
      this.oPgFrm.Page1.oPag.oCODCON1_1_57.value=this.w_CODCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_58.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_58.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DATA1<=.w_DATA2 OR (EMPTY(.w_DATA2))) AND .w_DATA1>=.w_INIESE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio")
          case   not(.w_DATA1<=.w_DATA2 OR EMPTY(.w_DATA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOANALITICA<>'S')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOANALITICA<>'S')  and not(empty(.w_CODVOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVOC_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(g_PERCAN<>'S')  and (.w_NOANALITICA<>'S')  and not(empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PNCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCODCON_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_TIPOCF<>' ')  and not(empty(.w_CODCON1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON1_1_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    this.o_CODCON = this.w_CODCON
    this.o_NOANALITICA = this.w_NOANALITICA
    this.o_PNCODCON = this.w_PNCODCON
    this.o_TIPOCF = this.w_TIPOCF
    return

enddefine

* --- Define pages as container
define class tgsca_szmPag1 as StdContainer
  Width  = 794
  height = 575
  stdWidth  = 794
  stdheight = 575
  resizeXpos=463
  resizeYpos=399
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLGMOV_1_2 as StdCombo with uid="HQYOCGIOJA",rtseq=2,rtrep=.f.,left=44,top=10,width=113,height=21;
    , ToolTipText = "Tipo di movimento selezionato";
    , HelpContextID = 53003434;
    , cFormVar="w_FLGMOV",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGMOV_1_2.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oFLGMOV_1_2.GetRadio()
    this.Parent.oContained.w_FLGMOV = this.RadioValue()
    return .t.
  endfunc

  func oFLGMOV_1_2.SetRadio()
    this.Parent.oContained.w_FLGMOV=trim(this.Parent.oContained.w_FLGMOV)
    this.value = ;
      iif(this.Parent.oContained.w_FLGMOV=='E',1,;
      iif(this.Parent.oContained.w_FLGMOV=='P',2,;
      0))
  endfunc


  add object oPROVE_1_3 as StdCombo with uid="ZDIXVACFAW",rtseq=3,rtrep=.f.,left=44,top=39,width=113,height=21;
    , ToolTipText = "Natura dei movimenti selezionati";
    , HelpContextID = 104907254;
    , cFormVar="w_PROVE",RowSource=""+"Tutti,"+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVE_1_3.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'O',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oPROVE_1_3.GetRadio()
    this.Parent.oContained.w_PROVE = this.RadioValue()
    return .t.
  endfunc

  func oPROVE_1_3.SetRadio()
    this.Parent.oContained.w_PROVE=trim(this.Parent.oContained.w_PROVE)
    this.value = ;
      iif(this.Parent.oContained.w_PROVE=='T',1,;
      iif(this.Parent.oContained.w_PROVE=='O',2,;
      iif(this.Parent.oContained.w_PROVE=='E',3,;
      0)))
  endfunc

  add object oCODESE_1_4 as StdField with uid="ZMIJKNZDWM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 66122202,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=233, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATA1_1_8 as StdField with uid="XCIFZMAYLP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio",;
    ToolTipText = "Data di registrazione di inizio visualizzazione",;
    HelpContextID = 82575414,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=380, Top=11

  func oDATA1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATA1<=.w_DATA2 OR (EMPTY(.w_DATA2))) AND .w_DATA1>=.w_INIESE)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_9 as StdField with uid="PYSJNDDGFL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di registrazione di fine visualizzazione",;
    HelpContextID = 83623990,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=508, Top=11

  func oDATA2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA1<=.w_DATA2 OR EMPTY(.w_DATA1))
    endwith
    return bRes
  endfunc


  add object oPROVCONF_1_10 as StdCombo with uid="YVYYMUBPRT",rtseq=10,rtrep=.f.,left=680,top=10,width=92,height=21;
    , ToolTipText = "Stato della registrazione (confermato / provvisorio)";
    , HelpContextID = 182402500;
    , cFormVar="w_PROVCONF",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVCONF_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPROVCONF_1_10.GetRadio()
    this.Parent.oContained.w_PROVCONF = this.RadioValue()
    return .t.
  endfunc

  func oPROVCONF_1_10.SetRadio()
    this.Parent.oContained.w_PROVCONF=trim(this.Parent.oContained.w_PROVCONF)
    this.value = ;
      iif(this.Parent.oContained.w_PROVCONF=='N',1,;
      iif(this.Parent.oContained.w_PROVCONF=='S',2,;
      0))
  endfunc

  add object oCODCON_1_11 as StdField with uid="NIWNCTYEIY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Centro di costo/ricavo selezionato",;
    HelpContextID = 187888090,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=233, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CODCON"

  func oCODCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOANALITICA<>'S')
    endwith
   endif
  endfunc

  func oCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCODVOC_1_12 as StdField with uid="AOPDMNGEPW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODVOC", cQueryName = "CODVOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce inesistente oppure obsoleto",;
    ToolTipText = "Voce selezionata",;
    HelpContextID = 102756826,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=233, Top=67, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_CODVOC"

  func oCODVOC_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOANALITICA<>'S')
    endwith
   endif
  endfunc

  func oCODVOC_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVOC_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVOC_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oCODVOC_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCODVOC_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_CODVOC
     i_obj.ecpSave()
  endproc

  add object oNOANALITICA_1_13 as StdCheck with uid="UNIASEYXFF",rtseq=13,rtrep=.f.,left=233, top=179, caption="Solo registrazioni con movimenti di analitica mancanti",;
    ToolTipText = "Ricerca le registrazioni contabili con movimenti di analitica mancanti in quanto provenienti da contabilizzazione e con aggiornamento manuale dell'analitica",;
    HelpContextID = 33306298,;
    cFormVar="w_NOANALITICA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOANALITICA_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOANALITICA_1_13.GetRadio()
    this.Parent.oContained.w_NOANALITICA = this.RadioValue()
    return .t.
  endfunc

  func oNOANALITICA_1_13.SetRadio()
    this.Parent.oContained.w_NOANALITICA=trim(this.Parent.oContained.w_NOANALITICA)
    this.value = ;
      iif(this.Parent.oContained.w_NOANALITICA=='S',1,;
      0)
  endfunc


  add object oBtn_1_15 as StdButton with uid="RRATFROFZL",left=737, top=100, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64939754;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do GSCA_BVM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="PGWDUJBTRZ",left=6, top=525, width=48,height=45,;
    CpPicture="BMP\ORIGINE.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione associata alla riga selezionata";
    , HelpContextID = 110330342;
    , Caption='Or\<igine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSCA_BZP(this.Parent.oContained,.w_SERIALE, .w_TIPMOV,.w_ALFA,.w_NUMERO,.w_DATA)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="VHAYDOERBK",left=59, top=525, width=48,height=45,;
    CpPicture="BMP\Elabora.bmp", caption="", nPag=1;
    , ToolTipText = "Manutenzione righe di analitica della registrazione contabile";
    , HelpContextID = 21096752;
    , Caption='\<Manutenz.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSCA_BMX(this.Parent.oContained,.w_SERIALE,.w_PNROWNUM,.w_IMPDAR,.w_IMPAVE,.w_TIPMOV,.w_DATREG)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')) AND g_COGE='S' AND LEFT(.w_TIPMOV,5)='(P.N.')
      endwith
    endif
  endfunc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (LEFT(.w_TIPMOV,2)='(D')
     endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="TRZRAEEFMY",left=59, top=525, width=48,height=45,;
    CpPicture="BMP\Elabora.bmp", caption="", nPag=1;
    , ToolTipText = "Manutenzione righe di analitica della registrazione contabile";
    , HelpContextID = 21096752;
    , Caption='\<Manutenz.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        do GSCA_BAA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')) AND g_COGE='S' AND LEFT(.w_TIPMOV,2)='(D')
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (LEFT(.w_TIPMOV,2)<>'(D')
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="LSSBWPNWMC",left=737, top=525, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33890886;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSALDO_1_20 as StdField with uid="WHVTIRGTTY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SALDO", cQueryName = "SALDO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dei movimenti selezionati",;
    HelpContextID = 114196774,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=580, Top=525, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFLGDAVE_1_28 as StdField with uid="ZECUVNLGPX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FLGDAVE", cQueryName = "FLGDAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 68273322,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=554, Top=525, InputMask=replicate('X',1)

  add object oCAUSALE_1_29 as StdField with uid="YGIMONWFZO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CAUSALE", cQueryName = "CAUSALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235007962,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=220, Top=525, InputMask=replicate('X',40)


  add object ZoomScad as cp_zoombox with uid="DAZIYPEDPC",left=1, top=199, width=789,height=322,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CDC_MANU",cZoomFile="GSCA_SZM",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="GSZM_BCC",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 63864346

  add object oDESPIA_1_34 as StdField with uid="NMYNFXNRYL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142937034,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=379, Top=39, InputMask=replicate('X',40)

  add object oDESPIA3_1_36 as StdField with uid="IHMVOVONFZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESPIA3", cQueryName = "DESPIA3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142937034,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=379, Top=67, InputMask=replicate('X',40)

  add object oCODCOM_1_46 as StdField with uid="BXUPRPSGAW",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa inesistente oppure obsoleto",;
    ToolTipText = "Commessa selezionata",;
    HelpContextID = 204665306,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=233, Top=95, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOANALITICA<>'S')
    endwith
   endif
  endfunc

  func oCODCOM_1_46.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S')
    endwith
  endfunc

  func oCODCOM_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_46.ecpDrop(oSource)
    this.Parent.oContained.link_1_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCODCOM_1_46.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDESPIA2_1_47 as StdField with uid="LATAJQWGGF",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESPIA2", cQueryName = "DESPIA2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142937034,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=379, Top=95, InputMask=replicate('X',40)

  func oDESPIA2_1_47.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S')
    endwith
  endfunc


  add object oObj_1_51 as cp_runprogram with uid="UAIOGLNCRD",left=21, top=640, width=142,height=28,;
    caption='GSCA_BZP(..)',;
   bGlobalFont=.t.,;
    prg="GSCA_BZP(w_SERIALE, w_TIPMOV,w_ALFA,w_NUMERO,w_DATA)",;
    cEvent = "w_zoomscad selected",;
    nPag=1;
    , HelpContextID = 167187254

  add object oPNCODCON_1_52 as StdField with uid="YWWDKJFDHV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PNCODCON", cQueryName = "PNCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice conto contabile",;
    HelpContextID = 114753980,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=233, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PNCODCON"

  func oPNCODCON_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODCON_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODCON_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPNCODCON_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'',this.parent.oContained
  endproc
  proc oPNCODCON_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PNCODCON
     i_obj.ecpSave()
  endproc

  add object oANDESCRI_1_53 as StdField with uid="FYMKGKRTSB",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione conto",;
    HelpContextID = 99676849,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=379, Top=123, InputMask=replicate('X',40)


  add object oTIPOCF_1_56 as StdCombo with uid="VBIDEFVZDV",value=3,rtseq=38,rtrep=.f.,left=89,top=151,width=142,height=21;
    , ToolTipText = "Tipo cliente/fornitore";
    , HelpContextID = 65418954;
    , cFormVar="w_TIPOCF",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCF_1_56.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPOCF_1_56.GetRadio()
    this.Parent.oContained.w_TIPOCF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCF_1_56.SetRadio()
    this.Parent.oContained.w_TIPOCF=trim(this.Parent.oContained.w_TIPOCF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCF=='C',1,;
      iif(this.Parent.oContained.w_TIPOCF=='F',2,;
      iif(this.Parent.oContained.w_TIPOCF=='',3,;
      0)))
  endfunc

  func oTIPOCF_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON1)
        bRes2=.link_1_57('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON1_1_57 as StdField with uid="AVLNXWJTAL",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CODCON1", cQueryName = "CODCON1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 187888090,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=233, Top=151, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON1"

  func oCODCON1_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOCF<>' ')
    endwith
   endif
  endfunc

  func oCODCON1_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON1_1_57.ecpDrop(oSource)
    this.Parent.oContained.link_1_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON1_1_57.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON1_1_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'',this.parent.oContained
  endproc
  proc oCODCON1_1_57.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOCF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON1
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_58 as StdField with uid="HBJNFXNXZX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione intestatario",;
    HelpContextID = 134090,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=379, Top=151, InputMask=replicate('X',40)

  add object oStr_1_21 as StdString with uid="CCNIMBZTPO",Visible=.t., Left=318, Top=11,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="GFPRIGCFZT",Visible=.t., Left=462, Top=11,;
    Alignment=1, Width=45, Height=15,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GYAPTBQRTP",Visible=.t., Left=506, Top=525,;
    Alignment=1, Width=46, Height=15,;
    Caption="Saldo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="VBAIOPANQW",Visible=.t., Left=172, Top=525,;
    Alignment=1, Width=47, Height=15,;
    Caption="Voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YGSXCVXLDR",Visible=.t., Left=6, Top=10,;
    Alignment=1, Width=36, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UGOKXHVKQB",Visible=.t., Left=178, Top=39,;
    Alignment=1, Width=54, Height=15,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="LTTFBIWKYY",Visible=.t., Left=182, Top=67,;
    Alignment=1, Width=50, Height=15,;
    Caption="Voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BKSPWDUWMB",Visible=.t., Left=626, Top=10,;
    Alignment=1, Width=51, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="WURXRDOBTL",Visible=.t., Left=157, Top=95,;
    Alignment=1, Width=74, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="WMPAYGEVJZ",Visible=.t., Left=173, Top=11,;
    Alignment=1, Width=59, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="SAOAPDLIFN",Visible=.t., Left=134, Top=126,;
    Alignment=1, Width=97, Height=15,;
    Caption="Conto contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="AYGWAHWKAF",Visible=.t., Left=-12, Top=152,;
    Alignment=1, Width=98, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
