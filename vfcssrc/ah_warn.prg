* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ah_warn                                                         *
*              Mostra post-in                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-01-02                                                      *
* Last revis.: 2012-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTABELLA,pTABELLA2,pCAMPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tah_warn",oParentObject,m.pTABELLA,m.pTABELLA2,m.pCAMPO)
return(i_retval)

define class tah_warn as StdBatch
  * --- Local variables
  w_cKEY = space(250)
  pTABELLA = space(10)
  pTABELLA2 = space(10)
  pCAMPO = space(10)
  w_CODICE = space(10)
  w_TABELLA = space(10)
  w_TABELLA2 = space(10)
  w_CAMPO = space(10)
  w_cKEY = space(250)
  w_CACODART = space(20)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Es. con i valori di default cerca i post-in associati all'articolo legato al codice di ricerca
    *     I parametri in pi� servono per un possibile uso pi� generale
    *     Per cercare i post-in dell'aticolo legato al codice di ricerca in realt� sarebbe sufficiente
    *     solo la parte nel ramo CASE
    * --- Mostra i post-in di archivi collegati
    *     oParentObject : � il codice digitato dall'utente
    *     pTABELLA : � la tabella a cui appartiente il codice digitato dall'utente (default "KEY_ARTI")
    *     pTABELLA2 : � la tabella a cui sono associati i post-in (default "ART_ICOL")
    *     pCAMPO : � il campo della tabella pTABELLA a cui a cui � legato il post-in (dafault "CACODART")
    * --- Assegna i valori di default
    this.w_CODICE = oParentObject
    if EMPTY( this.pTABELLA )
      this.w_TABELLA = "KEY_ARTI"
    else
      this.w_TABELLA = this.pTABELLA
    endif
    if EMPTY( this.pTABELLA2 )
      this.w_TABELLA2 = "ART_ICOL"
    else
      this.w_TABELLA2 = this.pTABELLA2
    endif
    if EMPTY( this.pCAMPO )
      this.w_CAMPO = "CACODART"
    else
      this.w_CAMPO = this.pCAMPO
    endif
    do case
      case this.w_TABELLA = "KEY_ARTI" AND this.w_TABELLA2 = "ART_ICOL" AND this.w_CAMPO = "CACODART"
        if NOT EMPTY( this.w_CODICE )
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODART"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODART;
              from (i_cTable) where;
                  CACODICE = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY( NVL( this.w_CACODART, "" ) )
            this.w_cKEY = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+"\"+cp_ToStr(this.w_CACODART,1)
            cp_ShowWarn(this.w_cKey,this.ART_ICOL_IDX)
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTABELLA,pTABELLA2,pCAMPO)
    this.pTABELLA=pTABELLA
    this.pTABELLA2=pTABELLA2
    this.pCAMPO=pCAMPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTABELLA,pTABELLA2,pCAMPO"
endproc
