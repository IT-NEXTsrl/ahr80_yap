* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_aar                                                        *
*              Articoli                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_611]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2018-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_aar"))

* --- Class definition
define class tgsma_aar as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 728
  Height = 486+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-18"
  HelpContextID=143293289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=209

  * --- Constant Properties
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  ART_ICOL_IDX = 0
  KEY_ARTI_IDX = 0
  TIP_MATE_IDX = 0
  NOT_ARTI_IDX = 0
  GRUMERC_IDX = 0
  VOCIIVA_IDX = 0
  CATEGOMO_IDX = 0
  CACOARTI_IDX = 0
  TIPICONF_IDX = 0
  NOMENCLA_IDX = 0
  GRUPRO_IDX = 0
  CAT_SCMA_IDX = 0
  MARCHI_IDX = 0
  FAM_ARTI_IDX = 0
  CONTI_IDX = 0
  LIS_TINI_IDX = 0
  CLA_RICA_IDX = 0
  CLA_RIGD_IDX = 0
  PAR_RIOR_IDX = 0
  VOC_COST_IDX = 0
  TIP_COLL_IDX = 0
  CON_COLL_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_PROD_IDX = 0
  REP_ARTI_IDX = 0
  DISMBASE_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  CMT_MAST_IDX = 0
  TIPCODIV_IDX = 0
  CENCOST_IDX = 0
  CONTRART_IDX = 0
  NUMAUT_M_IDX = 0
  MAT_CRI_IDX = 0
  cFile = "ART_ICOL"
  cKeySelect = "ARCODART"
  cQueryFilter="ARTIPART in ('PF','SE','MP','PH','FS')"
  cKeyWhere  = "ARCODART=this.w_ARCODART"
  cKeyWhereODBC = '"ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cKeyWhereODBCqualified = '"ART_ICOL.ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cPrg = "gsma_aar"
  cComment = "Articoli"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0AAR'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_TIPOPE = space(4)
  w_ARCODART = space(20)
  o_ARCODART = space(20)
  w_ARDESART = space(40)
  w_ARTIPART = space(2)
  o_ARTIPART = space(2)
  w_ARTIPART = space(2)
  w_ARDESSUP = space(0)
  w_TABKEY = space(8)
  w_VALATT = space(20)
  w_ARUNMIS1 = space(3)
  w_OLDUNI = space(3)
  w_ARUNMIS2 = space(3)
  o_ARUNMIS2 = space(3)
  w_AROPERAT = space(1)
  w_OLDUN2 = space(3)
  w_ARMOLTIP = 0
  w_ARFLUSEP = space(1)
  w_ARPREZUM = space(1)
  w_ARFLINVE = space(1)
  w_ARSTASUP = space(1)
  w_ARASSIVA = space(1)
  w_ARCATOPE = space(2)
  w_ARCODIVA = space(5)
  w_ARTIPOPE = space(10)
  w_DESIVA = space(35)
  w_ARCATCON = space(5)
  w_DESCAT = space(35)
  w_ARCODFAM = space(5)
  w_DESFAM = space(35)
  w_ARGRUMER = space(5)
  w_DESGRU = space(35)
  w_ARCATOMO = space(5)
  w_DESOMO = space(35)
  w_ARTIPGES = space(1)
  o_ARTIPGES = space(1)
  w_ARPROPRE = space(1)
  o_ARPROPRE = space(1)
  w_ARTIPBAR = space(1)
  o_ARTIPBAR = space(1)
  w_TIPBAR = space(1)
  w_ARFLDISP = space(1)
  o_ARFLDISP = space(1)
  w_ARFLDISC = space(1)
  w_ARCODCLA = space(3)
  w_ARSALCOM = space(1)
  o_ARSALCOM = space(1)
  w_ARDTINVA = ctod('  /  /  ')
  w_ARDTOBSO = ctod('  /  /  ')
  w_ARFLCOM1 = space(1)
  o_ARFLCOM1 = space(1)
  w_ARFLCOM1 = space(1)
  w_ARFLCOM1 = space(1)
  w_CODICE = space(41)
  w_ARFLESAU = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CODESE = space(4)
  w_OMODKEY = space(10)
  w_NMODKEY = space(10)
  w_CODI = space(20)
  w_DESC = space(40)
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_ARPESNET = 0
  w_ARPESLOR = 0
  w_ARTIPCO1 = space(5)
  o_ARTIPCO1 = space(5)
  w_ARTPCONF = space(3)
  o_ARTPCONF = space(3)
  w_DESTCF = space(30)
  w_ARPZCONF = 0
  w_ARCOCOL1 = 0
  w_ARDIMLUN = 0
  w_ARDIMLAR = 0
  w_ARDIMALT = 0
  w_VOLUME = 0
  o_VOLUME = 0
  w_ARUMVOLU = space(3)
  w_ARUMDIME = space(3)
  w_ARDESVOL = space(15)
  w_ARFLCONA = space(1)
  w_ARPESNE2 = 0
  w_ARPESLO2 = 0
  w_ARTIPCO2 = space(5)
  o_ARTIPCO2 = space(5)
  w_ARTPCON2 = space(3)
  o_ARTPCON2 = space(3)
  w_DESTCF2 = space(30)
  w_ARPZCON2 = 0
  w_ARCOCOL2 = 0
  w_ARDIMLU2 = 0
  w_ARDIMLA2 = 0
  w_ARDIMAL2 = 0
  w_VOLUM2 = 0
  o_VOLUM2 = 0
  w_ARUMVOL2 = space(3)
  w_ARUMDIM2 = space(3)
  w_ARDESVO2 = space(15)
  w_ARFLCON2 = space(1)
  w_ARUTISER = space(1)
  o_ARUTISER = space(1)
  w_ARPERSER = space(1)
  w_ARNOMENC = space(8)
  w_ARMOLSUP = 0
  w_ARDATINT = space(1)
  o_ARDATINT = space(1)
  w_ARUMSUPP = space(3)
  o_ARUMSUPP = space(3)
  w_CODI = space(20)
  w_DESC = space(40)
  w_TIPPRO = space(1)
  w_ARCODMAR = space(5)
  w_DESMAR = space(35)
  w_ARGRUPRO = space(5)
  w_DESGPP = space(35)
  w_FLPRO = space(1)
  w_ARCATSCM = space(5)
  w_DESSCM = space(35)
  w_FLSCM = space(1)
  w_ARCODRIC = space(5)
  w_DESCLR = space(35)
  w_PERCLR = 0
  w_OBTEST = ctod('  /  /  ')
  w_ARARTPOS = space(1)
  o_ARARTPOS = space(1)
  w_ARCODREP = space(3)
  o_ARCODREP = space(3)
  w_DESREP = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_READPRO = space(10)
  w_GESWIP = space(1)
  w_CODI = space(20)
  w_DESC = space(40)
  w_TIPVOC = space(1)
  w_DBOBSO = ctod('  /  /  ')
  w_DBINVA = ctod('  /  /  ')
  w_LICODART = space(10)
  w_TIPRIC = space(10)
  w_ARPUBWEB = space(1)
  w_ARFLPECO = space(1)
  w_ARCODGRU = space(5)
  w_ARCODSOT = space(5)
  w_ARMINVEN = 0
  w_DESCOL = space(35)
  w_DESCOL2 = space(35)
  w_ARSTACOD = space(1)
  w_DISMAG = space(1)
  w_DESSOT = space(35)
  w_DESTIT = space(35)
  w_PERIVA = 0
  w_IVAREP = space(5)
  w_PERIVR = 0
  w_FLFRAZ = space(1)
  w_F2FRAZ = space(1)
  w_ARPUBWEB = space(1)
  w_ARPUBWEB = space(1)
  w_DESCLA = space(30)
  w_ARFLLOTT = space(1)
  o_ARFLLOTT = space(1)
  w_ARFLLMAG = space(1)
  w_ARCLALOT = space(5)
  w_ARDISLOT = space(1)
  o_ARDISLOT = space(1)
  w_ARFLESUL = space(1)
  w_ARGESMAT = space(1)
  o_ARGESMAT = space(1)
  w_ARCLAMAT = space(5)
  w_ARVOCCEN = space(15)
  w_ARVOCRIC = space(15)
  w_ARCODCEN = space(15)
  w_ARCODDIS = space(20)
  o_ARCODDIS = space(20)
  w_ARFLCOMP = space(1)
  w_ARMAGPRE = space(5)
  w_ARTIPPRE = space(1)
  w_CMTDESCRI = space(30)
  w_CLTDESCRI = space(30)
  w_DESVOC = space(40)
  w_DESRIC = space(40)
  w_DESDIS = space(40)
  w_DESPRE = space(30)
  w_TIPCLA = space(1)
  w_TIPMAT = space(1)
  w_OLDFLAG = space(1)
  w_TIDESCRI = space(30)
  w_DESCEN = space(40)
  w_DISKIT = space(10)
  w_ARKITIMB = space(1)
  o_ARKITIMB = space(1)
  w_ARFLESIM = space(1)
  w_ARFLAPCA = space(1)
  w_ODATKEY = ctod('  /  /  ')
  w_NDATKEY = ctod('  /  /  ')
  w_ARDATINT = space(1)
  w_ARCLACRI = space(5)
  w_MATCRI = space(35)
  w_UMSUPP = space(3)
  w_DESNOM = space(35)
  w_ARCHKUCA = space(1)
  w_NULLA = .F.
  w_DELIMM = .F.
  w_GESTGUID = space(14)
  w_GEST = space(10)
  o_GEST = space(10)
  w_ARFLCOMM = space(1)
  w_ARTIPPKR = space(2)
  w_MAGOBSO = ctod('  /  /  ')
  w_POPOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_RICOBSO = ctod('  /  /  ')
  w_DCEOBSO = ctod('  /  /  ')
  w_CODI = space(20)
  w_DESC = space(40)
  w_CODATT = space(10)
  w_IMMODATR = space(10)
  w_ARTIPIMP = space(1)
  w_ARMAGIMP = space(5)
  w_DESMIMP = space(30)
  w_CODI = space(20)
  w_DESC = space(40)
  w_ARFSRIFE = space(20)
  w_DESRIFFS = space(40)
  w_ARCMPCAR = space(1)
  w_ARCONCAR = space(1)
  w_ARCODTIP = space(35)
  w_ARCODCLF = space(5)
  w_ARCODVAL = space(35)
  w_ARFLGESC = space(1)
  w_OFATELE = space(75)
  w_NFATELE = space(75)

  * --- Children pointers
  GSMA_MLI = .NULL.
  GSMA_MRP = .NULL.
  GSMA_APR = .NULL.
  GSMA_MAL = .NULL.
  GSAR_MAR = .NULL.
  GSAR_MR2 = .NULL.
  GSMA_ANO = .NULL.
  GSMA_MDA = .NULL.
  GSMA_MAC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=9, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ART_ICOL','gsma_aar')
    stdPageFrame::Init()
    *set procedure to GSMA_MLI additive
    with this
      .Pages(1).addobject("oPag","tgsma_aarPag1","gsma_aar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Articolo")
      .Pages(1).HelpContextID = 48874635
      .Pages(2).addobject("oPag","tgsma_aarPag2","gsma_aar",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tecniche")
      .Pages(2).HelpContextID = 24780699
      .Pages(3).addobject("oPag","tgsma_aarPag3","gsma_aar",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Commerciali")
      .Pages(3).HelpContextID = 4246175
      .Pages(4).addobject("oPag","tgsma_aarPag4","gsma_aar",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Magazzino")
      .Pages(4).HelpContextID = 109223388
      .Pages(5).addobject("oPag","tgsma_aarPag5","gsma_aar",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Gestioni collegate")
      .Pages(5).HelpContextID = 105586576
      .Pages(6).addobject("oPag","tgsma_aarPag6","gsma_aar",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Premi")
      .Pages(6).HelpContextID = 25605130
      .Pages(7).addobject("oPag","tgsma_aarPag7","gsma_aar",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Distinte alternative")
      .Pages(7).HelpContextID = 172419524
      .Pages(8).addobject("oPag","tgsma_aarPag8","gsma_aar",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Note")
      .Pages(8).HelpContextID = 136169258
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODART_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMA_MLI
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[36]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='TIP_MATE'
    this.cWorkTables[5]='NOT_ARTI'
    this.cWorkTables[6]='GRUMERC'
    this.cWorkTables[7]='VOCIIVA'
    this.cWorkTables[8]='CATEGOMO'
    this.cWorkTables[9]='CACOARTI'
    this.cWorkTables[10]='TIPICONF'
    this.cWorkTables[11]='NOMENCLA'
    this.cWorkTables[12]='GRUPRO'
    this.cWorkTables[13]='CAT_SCMA'
    this.cWorkTables[14]='MARCHI'
    this.cWorkTables[15]='FAM_ARTI'
    this.cWorkTables[16]='CONTI'
    this.cWorkTables[17]='LIS_TINI'
    this.cWorkTables[18]='CLA_RICA'
    this.cWorkTables[19]='CLA_RIGD'
    this.cWorkTables[20]='PAR_RIOR'
    this.cWorkTables[21]='VOC_COST'
    this.cWorkTables[22]='TIP_COLL'
    this.cWorkTables[23]='CON_COLL'
    this.cWorkTables[24]='MAGAZZIN'
    this.cWorkTables[25]='PAR_PROD'
    this.cWorkTables[26]='REP_ARTI'
    this.cWorkTables[27]='DISMBASE'
    this.cWorkTables[28]='TIT_SEZI'
    this.cWorkTables[29]='SOT_SEZI'
    this.cWorkTables[30]='CMT_MAST'
    this.cWorkTables[31]='TIPCODIV'
    this.cWorkTables[32]='CENCOST'
    this.cWorkTables[33]='CONTRART'
    this.cWorkTables[34]='NUMAUT_M'
    this.cWorkTables[35]='MAT_CRI'
    this.cWorkTables[36]='ART_ICOL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(36))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ART_ICOL_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MLI = CREATEOBJECT('stdLazyChild',this,'GSMA_MLI')
    this.GSMA_MRP = CREATEOBJECT('stdLazyChild',this,'GSMA_MRP')
    this.GSMA_APR = CREATEOBJECT('stdDynamicChild',this,'GSMA_APR',this.oPgFrm.Page4.oPag.oLinkPC_4_6)
    this.GSMA_APR.createrealchild()
    this.GSMA_MAL = CREATEOBJECT('stdDynamicChild',this,'GSMA_MAL',this.oPgFrm.Page3.oPag.oLinkPC_3_34)
    this.GSMA_MAL.createrealchild()
    this.GSAR_MAR = CREATEOBJECT('stdLazyChild',this,'GSAR_MAR')
    this.GSAR_MR2 = CREATEOBJECT('stdDynamicChild',this,'GSAR_MR2',this.oPgFrm.Page6.oPag.oLinkPC_6_1)
    this.GSAR_MR2.createrealchild()
    this.GSMA_ANO = CREATEOBJECT('stdDynamicChild',this,'GSMA_ANO',this.oPgFrm.Page8.oPag.oLinkPC_8_1)
    this.GSMA_ANO.createrealchild()
    this.GSMA_MDA = CREATEOBJECT('stdDynamicChild',this,'GSMA_MDA',this.oPgFrm.Page7.oPag.oLinkPC_7_4)
    this.GSMA_MAC = CREATEOBJECT('stdLazyChild',this,'GSMA_MAC')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MLI)
      this.GSMA_MLI.DestroyChildrenChain()
      this.GSMA_MLI=.NULL.
    endif
    if !ISNULL(this.GSMA_MRP)
      this.GSMA_MRP.DestroyChildrenChain()
      this.GSMA_MRP=.NULL.
    endif
    if !ISNULL(this.GSMA_APR)
      this.GSMA_APR.DestroyChildrenChain()
      this.GSMA_APR=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_6')
    if !ISNULL(this.GSMA_MAL)
      this.GSMA_MAL.DestroyChildrenChain()
      this.GSMA_MAL=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_34')
    if !ISNULL(this.GSAR_MAR)
      this.GSAR_MAR.DestroyChildrenChain()
      this.GSAR_MAR=.NULL.
    endif
    if !ISNULL(this.GSAR_MR2)
      this.GSAR_MR2.DestroyChildrenChain()
      this.GSAR_MR2=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_1')
    if !ISNULL(this.GSMA_ANO)
      this.GSMA_ANO.DestroyChildrenChain()
      this.GSMA_ANO=.NULL.
    endif
    this.oPgFrm.Page8.oPag.RemoveObject('oLinkPC_8_1')
    if !ISNULL(this.GSMA_MDA)
      this.GSMA_MDA.DestroyChildrenChain()
      this.GSMA_MDA=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_4')
    if !ISNULL(this.GSMA_MAC)
      this.GSMA_MAC.DestroyChildrenChain()
      this.GSMA_MAC=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MRP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_APR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MAL.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MAR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MR2.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_ANO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MDA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MAC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MRP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_APR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MAL.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MAR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MR2.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_ANO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MDA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MAC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MLI.NewDocument()
    this.GSMA_MRP.NewDocument()
    this.GSMA_APR.NewDocument()
    this.GSMA_MAL.NewDocument()
    this.GSAR_MAR.NewDocument()
    this.GSAR_MR2.NewDocument()
    this.GSMA_ANO.NewDocument()
    this.GSMA_MDA.NewDocument()
    this.GSMA_MAC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MLI.SetKey(;
            .w_ARCODART,"LICODART";
            )
      this.GSMA_MRP.SetKey(;
            .w_ARCODART,"LICODART";
            )
      this.GSMA_APR.SetKey(;
            .w_ARCODART,"PRCODART";
            )
      this.GSMA_MAL.SetKey(;
            .w_ARCODART,"ARCODICE";
            )
      this.GSAR_MAR.SetKey(;
            .w_ARCODART,"MCCODART";
            )
      this.GSAR_MR2.SetKey(;
            .w_GESTGUID,"GUID";
            )
      this.GSMA_ANO.SetKey(;
            .w_ARCODART,"ARCODART";
            )
      this.GSMA_MDA.SetKey(;
            .w_ARCODART,"DICODART";
            )
      this.GSMA_MAC.SetKey(;
            .w_ARCODART,"CCCODART";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MLI.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"LICODART";
             )
      .GSMA_MRP.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"LICODART";
             )
      .GSMA_APR.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"PRCODART";
             )
      .GSMA_MAL.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"ARCODICE";
             )
      .GSAR_MAR.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"MCCODART";
             )
      .GSAR_MR2.ChangeRow(this.cRowID+'      1',1;
             ,.w_GESTGUID,"GUID";
             )
      .WriteTo_GSAR_MR2()
      .GSMA_ANO.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"ARCODART";
             )
      .GSMA_MDA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"DICODART";
             )
      .GSMA_MAC.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"CCCODART";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_APR)
        i_f=.GSMA_APR.BuildFilter()
        if !(i_f==.GSMA_APR.cQueryFilter)
          i_fnidx=.GSMA_APR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_APR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_APR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_APR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_APR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSMA_MAL)
        i_f=.GSMA_MAL.BuildFilter()
        if !(i_f==.GSMA_MAL.cQueryFilter)
          i_fnidx=.GSMA_MAL.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MAL.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MAL.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MAL.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MAL.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MR2)
        i_f=.GSAR_MR2.BuildFilter()
        if !(i_f==.GSAR_MR2.cQueryFilter)
          i_fnidx=.GSAR_MR2.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MR2.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MR2.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MR2.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MR2.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSMA_ANO)
        i_f=.GSMA_ANO.BuildFilter()
        if !(i_f==.GSMA_ANO.cQueryFilter)
          i_fnidx=.GSMA_ANO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_ANO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_ANO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_ANO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_ANO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSMA_MDA)
        i_f=.GSMA_MDA.BuildFilter()
        if !(i_f==.GSMA_MDA.cQueryFilter)
          i_fnidx=.GSMA_MDA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MDA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MDA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MDA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MDA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MR2()
  if at('gsar_mr2',lower(this.GSAR_MR2.class))<>0
    if this.GSAR_MR2.w_GEST<>this.w_GEST
      this.GSAR_MR2.w_GEST = this.w_GEST
      this.GSAR_MR2.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ARCODART = NVL(ARCODART,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_22_joined
    link_2_22_joined=.f.
    local link_2_23_joined
    link_2_23_joined=.f.
    local link_2_37_joined
    link_2_37_joined=.f.
    local link_3_4_joined
    link_3_4_joined=.f.
    local link_3_7_joined
    link_3_7_joined=.f.
    local link_3_10_joined
    link_3_10_joined=.f.
    local link_3_18_joined
    link_3_18_joined=.f.
    local link_3_26_joined
    link_3_26_joined=.f.
    local link_3_31_joined
    link_3_31_joined=.f.
    local link_3_32_joined
    link_3_32_joined=.f.
    local link_5_3_joined
    link_5_3_joined=.f.
    local link_5_7_joined
    link_5_7_joined=.f.
    local link_5_8_joined
    link_5_8_joined=.f.
    local link_5_9_joined
    link_5_9_joined=.f.
    local link_5_10_joined
    link_5_10_joined=.f.
    local link_5_13_joined
    link_5_13_joined=.f.
    local link_5_45_joined
    link_5_45_joined=.f.
    local link_5_53_joined
    link_5_53_joined=.f.
    local link_7_7_joined
    link_7_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ART_ICOL where ARCODART=KeySet.ARCODART
    *
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ART_ICOL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ART_ICOL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ART_ICOL '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_22_joined=this.AddJoinedLink_2_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_23_joined=this.AddJoinedLink_2_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_37_joined=this.AddJoinedLink_2_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_4_joined=this.AddJoinedLink_3_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_7_joined=this.AddJoinedLink_3_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_10_joined=this.AddJoinedLink_3_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_18_joined=this.AddJoinedLink_3_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_26_joined=this.AddJoinedLink_3_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_31_joined=this.AddJoinedLink_3_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_32_joined=this.AddJoinedLink_3_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_3_joined=this.AddJoinedLink_5_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_7_joined=this.AddJoinedLink_5_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_8_joined=this.AddJoinedLink_5_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_9_joined=this.AddJoinedLink_5_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_10_joined=this.AddJoinedLink_5_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_13_joined=this.AddJoinedLink_5_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_45_joined=this.AddJoinedLink_5_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_53_joined=this.AddJoinedLink_5_53(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_7_joined=this.AddJoinedLink_7_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = space(1)
        .w_TABKEY = 'ART_ICOL'
        .w_VALATT = .w_ARCODART
        .w_DESIVA = space(35)
        .w_DESCAT = space(35)
        .w_DESFAM = space(35)
        .w_DESGRU = space(35)
        .w_DESOMO = space(35)
        .w_OMODKEY = space(10)
        .w_DESTCF = space(30)
        .w_DESTCF2 = space(30)
        .w_DESMAR = space(35)
        .w_DESGPP = space(35)
        .w_FLPRO = space(1)
        .w_DESSCM = space(35)
        .w_FLSCM = space(1)
        .w_DESCLR = space(35)
        .w_PERCLR = 0
        .w_OBTEST = i_datsys
        .w_DESREP = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_READPRO = 'PF'
        .w_GESWIP = space(1)
        .w_TIPVOC = space(1)
        .w_DBOBSO = ctod("  /  /  ")
        .w_DBINVA = ctod("  /  /  ")
        .w_TIPRIC = space(10)
        .w_DESCOL = space(35)
        .w_DESCOL2 = space(35)
        .w_DISMAG = space(1)
        .w_DESSOT = space(35)
        .w_DESTIT = space(35)
        .w_PERIVA = 0
        .w_IVAREP = space(5)
        .w_PERIVR = 0
        .w_FLFRAZ = space(1)
        .w_F2FRAZ = space(1)
        .w_DESCLA = space(30)
        .w_CMTDESCRI = space(30)
        .w_CLTDESCRI = space(30)
        .w_DESVOC = space(40)
        .w_DESRIC = space(40)
        .w_DESDIS = space(40)
        .w_DESPRE = space(30)
        .w_TIPCLA = space(1)
        .w_TIPMAT = space(1)
        .w_TIDESCRI = space(30)
        .w_DESCEN = space(40)
        .w_DISKIT = space(10)
        .w_ODATKEY = ctod("  /  /  ")
        .w_MATCRI = space(35)
        .w_UMSUPP = space(3)
        .w_DESNOM = space(35)
        .w_NULLA = .f.
        .w_DELIMM = .f.
        .w_GEST = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_MAGOBSO = ctod("  /  /  ")
        .w_POPOBSO = ctod("  /  /  ")
        .w_CENOBSO = ctod("  /  /  ")
        .w_RICOBSO = ctod("  /  /  ")
        .w_DCEOBSO = ctod("  /  /  ")
        .w_CODATT = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("GRUPPO") )
        .w_IMMODATR = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_DESMIMP = space(30)
        .w_DESRIFFS = space(40)
          .link_1_1('Load')
        .w_TIPOPE = this.cFunction
        .w_ARCODART = NVL(ARCODART,space(20))
        .w_ARDESART = NVL(ARDESART,space(40))
        .w_ARTIPART = NVL(ARTIPART,space(2))
        .w_ARTIPART = NVL(ARTIPART,space(2))
        .w_ARDESSUP = NVL(ARDESSUP,space(0))
        .w_ARUNMIS1 = NVL(ARUNMIS1,space(3))
          if link_1_11_joined
            this.w_ARUNMIS1 = NVL(UMCODICE111,NVL(this.w_ARUNMIS1,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_OLDUNI = .w_ARUNMIS1
        .w_ARUNMIS2 = NVL(ARUNMIS2,space(3))
          if link_1_13_joined
            this.w_ARUNMIS2 = NVL(UMCODICE113,NVL(this.w_ARUNMIS2,space(3)))
            this.w_F2FRAZ = NVL(UMFLFRAZ113,space(1))
          else
          .link_1_13('Load')
          endif
        .w_AROPERAT = NVL(AROPERAT,space(1))
        .w_OLDUN2 = .w_ARUNMIS2
        .w_ARMOLTIP = NVL(ARMOLTIP,0)
        .w_ARFLUSEP = NVL(ARFLUSEP,space(1))
        .w_ARPREZUM = NVL(ARPREZUM,space(1))
        .w_ARFLINVE = NVL(ARFLINVE,space(1))
        .w_ARSTASUP = NVL(ARSTASUP,space(1))
        .w_ARASSIVA = NVL(ARASSIVA,space(1))
        .w_ARCATOPE = NVL(ARCATOPE,space(2))
        .w_ARCODIVA = NVL(ARCODIVA,space(5))
          if link_1_23_joined
            this.w_ARCODIVA = NVL(IVCODIVA123,NVL(this.w_ARCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA123,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO123),ctod("  /  /  "))
            this.w_PERIVA = NVL(IVPERIVA123,0)
          else
          .link_1_23('Load')
          endif
        .w_ARTIPOPE = NVL(ARTIPOPE,space(10))
          .link_1_24('Load')
        .w_ARCATCON = NVL(ARCATCON,space(5))
          if link_1_26_joined
            this.w_ARCATCON = NVL(C1CODICE126,NVL(this.w_ARCATCON,space(5)))
            this.w_DESCAT = NVL(C1DESCRI126,space(35))
          else
          .link_1_26('Load')
          endif
        .w_ARCODFAM = NVL(ARCODFAM,space(5))
          if link_1_28_joined
            this.w_ARCODFAM = NVL(FACODICE128,NVL(this.w_ARCODFAM,space(5)))
            this.w_DESFAM = NVL(FADESCRI128,space(35))
          else
          .link_1_28('Load')
          endif
        .w_ARGRUMER = NVL(ARGRUMER,space(5))
          if link_1_30_joined
            this.w_ARGRUMER = NVL(GMCODICE130,NVL(this.w_ARGRUMER,space(5)))
            this.w_DESGRU = NVL(GMDESCRI130,space(35))
          else
          .link_1_30('Load')
          endif
        .w_ARCATOMO = NVL(ARCATOMO,space(5))
          if link_1_32_joined
            this.w_ARCATOMO = NVL(OMCODICE132,NVL(this.w_ARCATOMO,space(5)))
            this.w_DESOMO = NVL(OMDESCRI132,space(35))
          else
          .link_1_32('Load')
          endif
        .w_ARTIPGES = NVL(ARTIPGES,space(1))
        .w_ARPROPRE = NVL(ARPROPRE,space(1))
        .w_ARTIPBAR = NVL(ARTIPBAR,space(1))
        .w_TIPBAR = '0'
        .w_ARFLDISP = NVL(ARFLDISP,space(1))
        .w_ARFLDISC = NVL(ARFLDISC,space(1))
        .w_ARCODCLA = NVL(ARCODCLA,space(3))
          if link_1_40_joined
            this.w_ARCODCLA = NVL(TRCODCLA140,NVL(this.w_ARCODCLA,space(3)))
            this.w_DESCLA = NVL(TRDESCLA140,space(30))
          else
          .link_1_40('Load')
          endif
        .w_ARSALCOM = NVL(ARSALCOM,space(1))
        .w_ARDTINVA = NVL(cp_ToDate(ARDTINVA),ctod("  /  /  "))
        .w_ARDTOBSO = NVL(cp_ToDate(ARDTOBSO),ctod("  /  /  "))
        .w_ARFLCOM1 = NVL(ARFLCOM1,space(1))
        .w_ARFLCOM1 = NVL(ARFLCOM1,space(1))
        .w_ARFLCOM1 = NVL(ARFLCOM1,space(1))
        .w_CODICE = .w_ARCODART
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .w_ARFLESAU = NVL(ARFLESAU,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_UM1 = .w_ARUNMIS1
        .w_UM2 = .w_ARUNMIS2
        .w_ARPESNET = NVL(ARPESNET,0)
        .w_ARPESLOR = NVL(ARPESLOR,0)
        .w_ARTIPCO1 = NVL(ARTIPCO1,space(5))
          if link_2_7_joined
            this.w_ARTIPCO1 = NVL(TCCODICE207,NVL(this.w_ARTIPCO1,space(5)))
            this.w_DESCOL = NVL(TCDESCRI207,space(35))
          else
          .link_2_7('Load')
          endif
        .w_ARTPCONF = NVL(ARTPCONF,space(3))
          if link_2_8_joined
            this.w_ARTPCONF = NVL(TCCODCON208,NVL(this.w_ARTPCONF,space(3)))
            this.w_DESTCF = NVL(TCDESCON208,space(30))
            this.w_ARCOCOL1 = NVL(TCQTACON208,0)
          else
          .link_2_8('Load')
          endif
        .w_ARPZCONF = NVL(ARPZCONF,0)
        .w_ARCOCOL1 = NVL(ARCOCOL1,0)
        .w_ARDIMLUN = NVL(ARDIMLUN,0)
        .w_ARDIMLAR = NVL(ARDIMLAR,0)
        .w_ARDIMALT = NVL(ARDIMALT,0)
        .w_VOLUME = cp_ROUND(.w_ARDIMLUN*.w_ARDIMLAR*.w_ARDIMALT,3)
        .w_ARUMVOLU = NVL(ARUMVOLU,space(3))
          * evitabile
          *.link_2_16('Load')
        .w_ARUMDIME = NVL(ARUMDIME,space(3))
          * evitabile
          *.link_2_17('Load')
        .w_ARDESVOL = NVL(ARDESVOL,space(15))
        .w_ARFLCONA = NVL(ARFLCONA,space(1))
        .w_ARPESNE2 = NVL(ARPESNE2,0)
        .w_ARPESLO2 = NVL(ARPESLO2,0)
        .w_ARTIPCO2 = NVL(ARTIPCO2,space(5))
          if link_2_22_joined
            this.w_ARTIPCO2 = NVL(TCCODICE222,NVL(this.w_ARTIPCO2,space(5)))
            this.w_DESCOL2 = NVL(TCDESCRI222,space(35))
          else
          .link_2_22('Load')
          endif
        .w_ARTPCON2 = NVL(ARTPCON2,space(3))
          if link_2_23_joined
            this.w_ARTPCON2 = NVL(TCCODCON223,NVL(this.w_ARTPCON2,space(3)))
            this.w_DESTCF2 = NVL(TCDESCON223,space(30))
            this.w_ARCOCOL2 = NVL(TCQTACON223,0)
          else
          .link_2_23('Load')
          endif
        .w_ARPZCON2 = NVL(ARPZCON2,0)
        .w_ARCOCOL2 = NVL(ARCOCOL2,0)
        .w_ARDIMLU2 = NVL(ARDIMLU2,0)
        .w_ARDIMLA2 = NVL(ARDIMLA2,0)
        .w_ARDIMAL2 = NVL(ARDIMAL2,0)
        .w_VOLUM2 = cp_ROUND(.w_ARDIMLU2*.w_ARDIMLA2*.w_ARDIMAL2,3)
        .w_ARUMVOL2 = NVL(ARUMVOL2,space(3))
          * evitabile
          *.link_2_31('Load')
        .w_ARUMDIM2 = NVL(ARUMDIM2,space(3))
          * evitabile
          *.link_2_32('Load')
        .w_ARDESVO2 = NVL(ARDESVO2,space(15))
        .w_ARFLCON2 = NVL(ARFLCON2,space(1))
        .w_ARUTISER = NVL(ARUTISER,space(1))
        .w_ARPERSER = NVL(ARPERSER,space(1))
        .w_ARNOMENC = NVL(ARNOMENC,space(8))
          if link_2_37_joined
            this.w_ARNOMENC = NVL(NMCODICE237,NVL(this.w_ARNOMENC,space(8)))
            this.w_DESNOM = NVL(NMDESCRI237,space(35))
            this.w_ARUMSUPP = NVL(NMUNISUP237,space(3))
            this.w_UMSUPP = NVL(NMUNISUP237,space(3))
          else
          .link_2_37('Load')
          endif
        .w_ARMOLSUP = NVL(ARMOLSUP,0)
        .w_ARDATINT = NVL(ARDATINT,space(1))
        .w_ARUMSUPP = NVL(ARUMSUPP,space(3))
          * evitabile
          *.link_2_40('Load')
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_TIPPRO = 'F'
        .w_ARCODMAR = NVL(ARCODMAR,space(5))
          if link_3_4_joined
            this.w_ARCODMAR = NVL(MACODICE304,NVL(this.w_ARCODMAR,space(5)))
            this.w_DESMAR = NVL(MADESCRI304,space(35))
          else
          .link_3_4('Load')
          endif
        .w_ARGRUPRO = NVL(ARGRUPRO,space(5))
          if link_3_7_joined
            this.w_ARGRUPRO = NVL(GPCODICE307,NVL(this.w_ARGRUPRO,space(5)))
            this.w_DESGPP = NVL(GPDESCRI307,space(35))
            this.w_FLPRO = NVL(GPTIPPRO307,space(1))
          else
          .link_3_7('Load')
          endif
        .w_ARCATSCM = NVL(ARCATSCM,space(5))
          if link_3_10_joined
            this.w_ARCATSCM = NVL(CSCODICE310,NVL(this.w_ARCATSCM,space(5)))
            this.w_DESSCM = NVL(CSDESCRI310,space(35))
            this.w_FLSCM = NVL(CSTIPCAT310,space(1))
          else
          .link_3_10('Load')
          endif
        .w_ARCODRIC = NVL(ARCODRIC,space(5))
          if link_3_18_joined
            this.w_ARCODRIC = NVL(CRCODICE318,NVL(this.w_ARCODRIC,space(5)))
            this.w_DESCLR = NVL(CRDESCRI318,space(35))
            this.w_PERCLR = NVL(CRPERCEN318,0)
          else
          .link_3_18('Load')
          endif
        .w_ARARTPOS = NVL(ARARTPOS,space(1))
        .w_ARCODREP = NVL(ARCODREP,space(3))
          if link_3_26_joined
            this.w_ARCODREP = NVL(RECODREP326,NVL(this.w_ARCODREP,space(3)))
            this.w_DESREP = NVL(REDESREP326,space(40))
            this.w_IVAREP = NVL(RECODIVA326,space(5))
          else
          .link_3_26('Load')
          endif
          .link_4_1('Load')
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_LICODART = .w_ARCODART
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
        .w_ARFLPECO = NVL(ARFLPECO,space(1))
        .w_ARCODGRU = NVL(ARCODGRU,space(5))
          if link_3_31_joined
            this.w_ARCODGRU = NVL(TSCODICE331,NVL(this.w_ARCODGRU,space(5)))
            this.w_DESTIT = NVL(TSDESCRI331,space(35))
          else
          .link_3_31('Load')
          endif
        .w_ARCODSOT = NVL(ARCODSOT,space(5))
          if link_3_32_joined
            this.w_ARCODSOT = NVL(SGCODSOT332,NVL(this.w_ARCODSOT,space(5)))
            this.w_DESSOT = NVL(SGDESCRI332,space(35))
          else
          .link_3_32('Load')
          endif
        .w_ARMINVEN = NVL(ARMINVEN,0)
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .w_ARSTACOD = NVL(ARSTACOD,space(1))
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
          .link_3_42('Load')
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .w_ARFLLOTT = NVL(ARFLLOTT,space(1))
        .w_ARFLLMAG = NVL(ARFLLMAG,space(1))
        .w_ARCLALOT = NVL(ARCLALOT,space(5))
          if link_5_3_joined
            this.w_ARCLALOT = NVL(CMCODICE503,NVL(this.w_ARCLALOT,space(5)))
            this.w_CLTDESCRI = NVL(CMDESCRI503,space(30))
            this.w_TIPCLA = NVL(CMTIPCLA503,space(1))
          else
          .link_5_3('Load')
          endif
        .w_ARDISLOT = NVL(ARDISLOT,space(1))
        .w_ARFLESUL = NVL(ARFLESUL,space(1))
        .w_ARGESMAT = NVL(ARGESMAT,space(1))
        .w_ARCLAMAT = NVL(ARCLAMAT,space(5))
          if link_5_7_joined
            this.w_ARCLAMAT = NVL(CMCODICE507,NVL(this.w_ARCLAMAT,space(5)))
            this.w_CMTDESCRI = NVL(CMDESCRI507,space(30))
            this.w_TIPMAT = NVL(CMTIPCLA507,space(1))
          else
          .link_5_7('Load')
          endif
        .w_ARVOCCEN = NVL(ARVOCCEN,space(15))
          if link_5_8_joined
            this.w_ARVOCCEN = NVL(VCCODICE508,NVL(this.w_ARVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI508,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC508,space(1))
            this.w_CENOBSO = NVL(cp_ToDate(VCDTOBSO508),ctod("  /  /  "))
          else
          .link_5_8('Load')
          endif
        .w_ARVOCRIC = NVL(ARVOCRIC,space(15))
          if link_5_9_joined
            this.w_ARVOCRIC = NVL(VCCODICE509,NVL(this.w_ARVOCRIC,space(15)))
            this.w_DESRIC = NVL(VCDESCRI509,space(40))
            this.w_TIPRIC = NVL(VCTIPVOC509,space(10))
            this.w_RICOBSO = NVL(cp_ToDate(VCDTOBSO509),ctod("  /  /  "))
          else
          .link_5_9('Load')
          endif
        .w_ARCODCEN = NVL(ARCODCEN,space(15))
          if link_5_10_joined
            this.w_ARCODCEN = NVL(CC_CONTO510,NVL(this.w_ARCODCEN,space(15)))
            this.w_DESCEN = NVL(CCDESPIA510,space(40))
            this.w_DCEOBSO = NVL(cp_ToDate(CCDTOBSO510),ctod("  /  /  "))
          else
          .link_5_10('Load')
          endif
        .w_ARCODDIS = NVL(ARCODDIS,space(20))
          .link_5_11('Load')
        .w_ARFLCOMP = NVL(ARFLCOMP,space(1))
        .w_ARMAGPRE = NVL(ARMAGPRE,space(5))
          if link_5_13_joined
            this.w_ARMAGPRE = NVL(MGCODMAG513,NVL(this.w_ARMAGPRE,space(5)))
            this.w_DESPRE = NVL(MGDESMAG513,space(30))
            this.w_DISMAG = NVL(MGDISMAG513,space(1))
            this.w_MAGOBSO = NVL(cp_ToDate(MGDTOBSO513),ctod("  /  /  "))
          else
          .link_5_13('Load')
          endif
        .w_ARTIPPRE = NVL(ARTIPPRE,space(1))
        .w_OLDFLAG = .w_ARFLLOTT
        .w_ARKITIMB = NVL(ARKITIMB,space(1))
        .w_ARFLESIM = NVL(ARFLESIM,space(1))
        .w_ARFLAPCA = NVL(ARFLAPCA,space(1))
        .w_NDATKEY = .w_ARDTOBSO
        .w_ARDATINT = NVL(ARDATINT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .w_ARCLACRI = NVL(ARCLACRI,space(5))
          if link_5_45_joined
            this.w_ARCLACRI = NVL(MTCODICE545,NVL(this.w_ARCLACRI,space(5)))
            this.w_MATCRI = NVL(MTDESCRI545,space(35))
          else
          .link_5_45('Load')
          endif
        .w_ARCHKUCA = NVL(ARCHKUCA,space(1))
        .w_GESTGUID = NVL(GESTGUID,space(14))
        .w_ARFLCOMM = NVL(ARFLCOMM,space(1))
        .w_ARTIPPKR = NVL(ARTIPPKR,space(2))
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_ARTIPIMP = NVL(ARTIPIMP,space(1))
        .w_ARMAGIMP = NVL(ARMAGIMP,space(5))
          if link_5_53_joined
            this.w_ARMAGIMP = NVL(MGCODMAG553,NVL(this.w_ARMAGIMP,space(5)))
            this.w_DESMIMP = NVL(MGDESMAG553,space(30))
          else
          .link_5_53('Load')
          endif
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_ARFSRIFE = NVL(ARFSRIFE,space(20))
          if link_7_7_joined
            this.w_ARFSRIFE = NVL(ARCODART707,NVL(this.w_ARFSRIFE,space(20)))
            this.w_DESRIFFS = NVL(ARDESART707,space(40))
          else
          .link_7_7('Load')
          endif
        .w_ARCMPCAR = NVL(ARCMPCAR,space(1))
        .w_ARCONCAR = NVL(ARCONCAR,space(1))
        .w_ARCODTIP = NVL(ARCODTIP,space(35))
        .w_ARCODCLF = NVL(ARCODCLF,space(5))
        .w_ARCODVAL = NVL(ARCODVAL,space(35))
        .w_ARFLGESC = NVL(ARFLGESC,space(1))
        .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        cp_LoadRecExtFlds(this,'ART_ICOL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_15.enabled = this.oPgFrm.Page3.oPag.oBtn_3_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsma_aar
    if NOT this.w_ARTIPART $ 'PF-SE-MP-PH-FS'
       this.BlankRec()
    else
       * --- Rilancio Controllo Editabilit�
       this.mEnableControls()
    endif
    
    * -- Per inizializzare il vecchio valore.
    * ---Per controllo cambiamento valori e aggiornamento chiave di ricerca
    with this
    .w_OMODKEY = .w_ARDESART+.w_ARDESSUP+DTOC(.w_ARDTINVA)
    .w_ODATKEY = .w_ARDTOBSO
    endwith
    
    
    this.GSAR_MAR.linkPcclick(.t.)      
    this.GSAR_MAR.linkPcclick()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_aar
    * --- Per caricamento rapido: eseguo i metodi usati nella BlankRec senza per�
    * --- sbiancare le variabili
    IF g_PERART='S' AND this.cFunction='Load' AND NOT EMPTY(this.w_ARCODART)
       * --- Valorizzo i dati da non riportare sul nuovo articolo
       this.w_ARCODART=iif(this.cFunction='Load' AND this.w_AUTO='S', 'AUTO',Space(20))
       this.w_ARCODDIS=Space(20)
       * --- sbianco la data obsolescenza se inferiore ad oggi, e rivalorizzo data creazione
       * --- e utente che ha creato l'articolo
       this.w_ARDTOBSO=IIf( this.w_ARDTOBSO<=i_DATSYS , cp_CharToDate('  -  -    ') , this.w_ARDTOBSO )
       this.w_UTDC=SetInfoDate(this.cCALUTD)
       this.w_UTCC=i_CODUTE
       this.w_UTCV=0
       this.w_DESDIS=''
       this.w_ARFLCOMP=' '
       this.w_UTDV={} 
       this.w_GESTGUID=TTOC(DATETIME(),1)
        
       * --- Inizio: assegnamenti per far si che i figli integrati vengano salvati
       * --- Articoli Alternativi
       this.GSMA_MAL.bLoaded=.f.
       this.GSMA_MAL.bUpdated=.t.
       this.GSMA_MAL.MarkPos()
         set delete off
         update (this.GSMA_MAL.cTrsName) set i_Srv='A' where 1=1
         set delete on
       this.GSMA_MAL.RePos()
       
          * --- Contributi Accessori
       this.GSAR_MAR.bLoaded=.f.
       if(type('this.GSAR_MAR.CNT')='O')
         this.GSAR_MAR.CNT.bUpdated=.t.
         this.GSAR_MAR.CNT.MarkPos()
         set delete off
         update (this.GSAR_MAR.CNT.cTrsName) set i_Srv='A' where 1=1
         set delete on
         this.GSAR_MAR.CNT.RePos()
       endif
    
       * --- Magazzino
       this.GSMA_APR.bLoaded=.f.
       this.GSMA_APR.bUpdated=this.GSMA_APR.bUpdated
       if this.GSMA_APR.bUpdated
         * --- Produzione Inizio
         * --- Occorre azzerare il LLC antrimenti l'MRP non lo ricalcola
         this.GSMA_APR.w_PRLOWLEV=0
         * --- Occorre azzerare i costi
         this.GSMA_APR.w_PRESECOS=space(4)
         this.GSMA_APR.w_PUUTEELA=0
         this.GSMA_APR.w_PRINVCOS=space(6)
         this.GSMA_APR.w_PRLISCOS=space(5)
         this.GSMA_APR.w_PRLOTSTD=0
         this.GSMA_APR.w_PRDATCOS=ctod('  /  /  ')
         this.GSMA_APR.w_PRLOTULT=0
         this.GSMA_APR.w_PRDATULT=ctod('  /  /  ')
         this.GSMA_APR.w_PRLOTMED=0
         this.GSMA_APR.w_PRDATMED=ctod('  /  /  ')
         this.GSMA_APR.w_PRLOTLIS=0
         this.GSMA_APR.w_PRDATLIS=ctod('  /  /  ')
         this.GSMA_APR.w_PRCSLAVO=0
         this.GSMA_APR.w_PRCULAVO=0
         this.GSMA_APR.w_PRCMLAVO=0
         this.GSMA_APR.w_PRCLLAVO=0
         this.GSMA_APR.w_PRCSLAVE=0
         this.GSMA_APR.w_PRCULAVE=0
         this.GSMA_APR.w_PRCMLAVE=0
         this.GSMA_APR.w_PRCLLAVE=0
         this.GSMA_APR.w_PRCSMATE=0
         this.GSMA_APR.w_PRCUMATE=0
         this.GSMA_APR.w_PRCMMATE=0
         this.GSMA_APR.w_PRCLMATE=0
         this.GSMA_APR.w_PRCSSPGE=0
         this.GSMA_APR.w_PRCUSPGE=0
         this.GSMA_APR.w_PRCMSPGE=0
         this.GSMA_APR.w_PRCLSPGE=0
         this.GSMA_APR.w_PRCSLAVC=0
         this.GSMA_APR.w_PRCULAVC=0
         this.GSMA_APR.w_PRCMLAVC=0
         this.GSMA_APR.w_PRCLLAVC=0
         this.GSMA_APR.w_PRCSLVCE=0
         this.GSMA_APR.w_PRCULVCE=0
         this.GSMA_APR.w_PRCMLVCE=0
         this.GSMA_APR.w_PRCLLVCE=0
         this.GSMA_APR.w_PRCSMATC=0
         this.GSMA_APR.w_PRCUMATC=0
         this.GSMA_APR.w_PRCMMATC=0
         this.GSMA_APR.w_PRCLMATC=0
         this.GSMA_APR.w_PRCSSPGC=0
         this.GSMA_APR.w_PRCUSPGC=0
         this.GSMA_APR.w_PRCMSPGC=0
         this.GSMA_APR.w_PRCLSPGC=0
         this.GSMA_APR.w_TOTCSLAVO=0
         this.GSMA_APR.w_TOTCULAVO=0
         this.GSMA_APR.w_TOTCMLAVO=0
         this.GSMA_APR.w_TOTCLLAVO=0
         this.GSMA_APR.w_TOTCSLAVE=0
         this.GSMA_APR.w_TOTCULAVE=0
         this.GSMA_APR.w_TOTCMLAVE=0
         this.GSMA_APR.w_TOTCLLAVE=0
         this.GSMA_APR.w_TOTCSMATE=0
         this.GSMA_APR.w_TOTCUMATE=0
         this.GSMA_APR.w_TOTCMMATE=0
         this.GSMA_APR.w_TOTCLMATE=0
         this.GSMA_APR.w_TOTCSSPGE=0
         this.GSMA_APR.w_TOTCUSPGE=0
         this.GSMA_APR.w_TOTCMSPGE=0
         this.GSMA_APR.w_TOTCLSPGE=0
         this.GSMA_APR.w_TOTSTAND=0
         this.GSMA_APR.w_TOTULTIM=0
         this.GSMA_APR.w_TOTMEDIO=0
         this.GSMA_APR.w_TOTLISTI=0
         this.GSMA_APR.SetControlsValue()
         * --- Produzione Fine
       endif
       this.GSMA_APR.GSMA_MPR.bLoaded=.f.
       this.GSMA_APR.GSMA_MPR.bUpdated=this.GSMA_APR.GSMA_MPR.bUpdated
       this.GSMA_APR.GSMA_MPR.MarkPos()
         set delete off
         update (this.GSMA_APR.GSMA_MPR.cTrsName) set i_Srv='A' where 1=1
         set delete on
       this.GSMA_APR.GSMA_MPR.RePos()
       * --- Attributi- premi fine anno
       if g_GPFA='S'
         this.GSAR_MR2.bLoaded=.f.
         this.GSAR_MR2.bUpdated=.t.
         this.GSAR_MR2.MarkPos()
           set delete off
           update (this.GSAR_MR2.cTrsName) set i_Srv='A' where 1=1
           set delete on
         this.GSAR_MR2.RePos()
       endif
       * --- Note
       this.GSMA_ANO.bLoaded=.f.
       this.GSMA_ANO.bUpdated=.t.
    
       * ---Non carico i Ricar. Prezzi del vecchio articolo
       this.GSMA_MRP.NewDocument()
       * --- Non carico i Listini del vecchio articolo
       this.GSMA_MLI.NewDocument()
       * --- Fine
       this.SaveDependsOn()
       *Ripeto i link per problema sicurezza sul record E0000017549
       local L_MACRO, nAttIdx
       AMEMBERS(aMethods,this,1)
       FOR nAttIdx = 1 TO ALEN(aMethods, 1)
         If aMethods(nAttIdx, 2)=="Method" AND LEFT(aMethods(nAttIdx, 1), 5)=="LINK_" AND !aMethods(nAttIdx, 1)=="LINK_1_1"
           L_MACRO = "this." + ALLTRIM(aMethods(nAttIdx, 1)) + "('Full')"
           &L_MACRO
         EndIf
       NEXT
       Release aMethods, nAttIdx, L_MACRO
       this.SetControlsValue()
       this.mHideControls()
       this.ChildrenChangeRow()
       this.NotifyEvent('Blank')
       this.mCalc(.t.)
       return
    Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_TIPOPE = space(4)
      .w_ARCODART = space(20)
      .w_ARDESART = space(40)
      .w_ARTIPART = space(2)
      .w_ARTIPART = space(2)
      .w_ARDESSUP = space(0)
      .w_TABKEY = space(8)
      .w_VALATT = space(20)
      .w_ARUNMIS1 = space(3)
      .w_OLDUNI = space(3)
      .w_ARUNMIS2 = space(3)
      .w_AROPERAT = space(1)
      .w_OLDUN2 = space(3)
      .w_ARMOLTIP = 0
      .w_ARFLUSEP = space(1)
      .w_ARPREZUM = space(1)
      .w_ARFLINVE = space(1)
      .w_ARSTASUP = space(1)
      .w_ARASSIVA = space(1)
      .w_ARCATOPE = space(2)
      .w_ARCODIVA = space(5)
      .w_ARTIPOPE = space(10)
      .w_DESIVA = space(35)
      .w_ARCATCON = space(5)
      .w_DESCAT = space(35)
      .w_ARCODFAM = space(5)
      .w_DESFAM = space(35)
      .w_ARGRUMER = space(5)
      .w_DESGRU = space(35)
      .w_ARCATOMO = space(5)
      .w_DESOMO = space(35)
      .w_ARTIPGES = space(1)
      .w_ARPROPRE = space(1)
      .w_ARTIPBAR = space(1)
      .w_TIPBAR = space(1)
      .w_ARFLDISP = space(1)
      .w_ARFLDISC = space(1)
      .w_ARCODCLA = space(3)
      .w_ARSALCOM = space(1)
      .w_ARDTINVA = ctod("  /  /  ")
      .w_ARDTOBSO = ctod("  /  /  ")
      .w_ARFLCOM1 = space(1)
      .w_ARFLCOM1 = space(1)
      .w_ARFLCOM1 = space(1)
      .w_CODICE = space(41)
      .w_ARFLESAU = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CODESE = space(4)
      .w_OMODKEY = space(10)
      .w_NMODKEY = space(10)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_UM1 = space(3)
      .w_UM2 = space(3)
      .w_ARPESNET = 0
      .w_ARPESLOR = 0
      .w_ARTIPCO1 = space(5)
      .w_ARTPCONF = space(3)
      .w_DESTCF = space(30)
      .w_ARPZCONF = 0
      .w_ARCOCOL1 = 0
      .w_ARDIMLUN = 0
      .w_ARDIMLAR = 0
      .w_ARDIMALT = 0
      .w_VOLUME = 0
      .w_ARUMVOLU = space(3)
      .w_ARUMDIME = space(3)
      .w_ARDESVOL = space(15)
      .w_ARFLCONA = space(1)
      .w_ARPESNE2 = 0
      .w_ARPESLO2 = 0
      .w_ARTIPCO2 = space(5)
      .w_ARTPCON2 = space(3)
      .w_DESTCF2 = space(30)
      .w_ARPZCON2 = 0
      .w_ARCOCOL2 = 0
      .w_ARDIMLU2 = 0
      .w_ARDIMLA2 = 0
      .w_ARDIMAL2 = 0
      .w_VOLUM2 = 0
      .w_ARUMVOL2 = space(3)
      .w_ARUMDIM2 = space(3)
      .w_ARDESVO2 = space(15)
      .w_ARFLCON2 = space(1)
      .w_ARUTISER = space(1)
      .w_ARPERSER = space(1)
      .w_ARNOMENC = space(8)
      .w_ARMOLSUP = 0
      .w_ARDATINT = space(1)
      .w_ARUMSUPP = space(3)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_TIPPRO = space(1)
      .w_ARCODMAR = space(5)
      .w_DESMAR = space(35)
      .w_ARGRUPRO = space(5)
      .w_DESGPP = space(35)
      .w_FLPRO = space(1)
      .w_ARCATSCM = space(5)
      .w_DESSCM = space(35)
      .w_FLSCM = space(1)
      .w_ARCODRIC = space(5)
      .w_DESCLR = space(35)
      .w_PERCLR = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_ARARTPOS = space(1)
      .w_ARCODREP = space(3)
      .w_DESREP = space(40)
      .w_DATOBSO = ctod("  /  /  ")
      .w_READPRO = space(10)
      .w_GESWIP = space(1)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_TIPVOC = space(1)
      .w_DBOBSO = ctod("  /  /  ")
      .w_DBINVA = ctod("  /  /  ")
      .w_LICODART = space(10)
      .w_TIPRIC = space(10)
      .w_ARPUBWEB = space(1)
      .w_ARFLPECO = space(1)
      .w_ARCODGRU = space(5)
      .w_ARCODSOT = space(5)
      .w_ARMINVEN = 0
      .w_DESCOL = space(35)
      .w_DESCOL2 = space(35)
      .w_ARSTACOD = space(1)
      .w_DISMAG = space(1)
      .w_DESSOT = space(35)
      .w_DESTIT = space(35)
      .w_PERIVA = 0
      .w_IVAREP = space(5)
      .w_PERIVR = 0
      .w_FLFRAZ = space(1)
      .w_F2FRAZ = space(1)
      .w_ARPUBWEB = space(1)
      .w_ARPUBWEB = space(1)
      .w_DESCLA = space(30)
      .w_ARFLLOTT = space(1)
      .w_ARFLLMAG = space(1)
      .w_ARCLALOT = space(5)
      .w_ARDISLOT = space(1)
      .w_ARFLESUL = space(1)
      .w_ARGESMAT = space(1)
      .w_ARCLAMAT = space(5)
      .w_ARVOCCEN = space(15)
      .w_ARVOCRIC = space(15)
      .w_ARCODCEN = space(15)
      .w_ARCODDIS = space(20)
      .w_ARFLCOMP = space(1)
      .w_ARMAGPRE = space(5)
      .w_ARTIPPRE = space(1)
      .w_CMTDESCRI = space(30)
      .w_CLTDESCRI = space(30)
      .w_DESVOC = space(40)
      .w_DESRIC = space(40)
      .w_DESDIS = space(40)
      .w_DESPRE = space(30)
      .w_TIPCLA = space(1)
      .w_TIPMAT = space(1)
      .w_OLDFLAG = space(1)
      .w_TIDESCRI = space(30)
      .w_DESCEN = space(40)
      .w_DISKIT = space(10)
      .w_ARKITIMB = space(1)
      .w_ARFLESIM = space(1)
      .w_ARFLAPCA = space(1)
      .w_ODATKEY = ctod("  /  /  ")
      .w_NDATKEY = ctod("  /  /  ")
      .w_ARDATINT = space(1)
      .w_ARCLACRI = space(5)
      .w_MATCRI = space(35)
      .w_UMSUPP = space(3)
      .w_DESNOM = space(35)
      .w_ARCHKUCA = space(1)
      .w_NULLA = .f.
      .w_DELIMM = .f.
      .w_GESTGUID = space(14)
      .w_GEST = space(10)
      .w_ARFLCOMM = space(1)
      .w_ARTIPPKR = space(2)
      .w_MAGOBSO = ctod("  /  /  ")
      .w_POPOBSO = ctod("  /  /  ")
      .w_CENOBSO = ctod("  /  /  ")
      .w_RICOBSO = ctod("  /  /  ")
      .w_DCEOBSO = ctod("  /  /  ")
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_CODATT = space(10)
      .w_IMMODATR = space(10)
      .w_ARTIPIMP = space(1)
      .w_ARMAGIMP = space(5)
      .w_DESMIMP = space(30)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_ARFSRIFE = space(20)
      .w_DESRIFFS = space(40)
      .w_ARCMPCAR = space(1)
      .w_ARCONCAR = space(1)
      .w_ARCODTIP = space(35)
      .w_ARCODCLF = space(5)
      .w_ARCODVAL = space(35)
      .w_ARFLGESC = space(1)
      .w_OFATELE = space(75)
      .w_NFATELE = space(75)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_TIPOPE = this.cFunction
        .w_ARCODART = iif(.cFunction = 'Load' AND .w_AUTO='S', 'AUTO',.w_ARCODART)
          .DoRTCalc(5,6,.f.)
        .w_ARTIPART = 'PF'
          .DoRTCalc(8,8,.f.)
        .w_TABKEY = 'ART_ICOL'
        .w_VALATT = .w_ARCODART
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_ARUNMIS1))
          .link_1_11('Full')
          endif
        .w_OLDUNI = .w_ARUNMIS1
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_ARUNMIS2))
          .link_1_13('Full')
          endif
        .w_AROPERAT = '*'
        .w_OLDUN2 = .w_ARUNMIS2
        .w_ARMOLTIP = 0
        .w_ARFLUSEP = ' '
        .w_ARPREZUM = 'N'
        .w_ARFLINVE = 'S'
          .DoRTCalc(20,20,.f.)
        .w_ARASSIVA = 'S'
        .w_ARCATOPE = 'AR'
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_ARCODIVA))
          .link_1_23('Full')
          endif
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_ARTIPOPE))
          .link_1_24('Full')
          endif
        .DoRTCalc(25,26,.f.)
          if not(empty(.w_ARCATCON))
          .link_1_26('Full')
          endif
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_ARCODFAM))
          .link_1_28('Full')
          endif
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_ARGRUMER))
          .link_1_30('Full')
          endif
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_ARCATOMO))
          .link_1_32('Full')
          endif
          .DoRTCalc(33,33,.f.)
        .w_ARTIPGES = 'F'
        .w_ARPROPRE = iif(g_PROD='S' , IIF(.w_ARTIPART='MP', 'E',  'I' ) , 'E' )
        .w_ARTIPBAR = '0'
        .w_TIPBAR = '0'
        .w_ARFLDISP = IIF((g_PERDIS='S') OR (.w_ARFLLOTT $ 'SC')  OR  .w_ARGESMAT ='S', 'S', 'N')
        .w_ARFLDISC = 'N'
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_ARCODCLA))
          .link_1_40('Full')
          endif
          .DoRTCalc(41,43,.f.)
        .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),.w_ARFLCOM1,"N")
        .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),.w_ARFLCOM1,"N")
        .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),iif(.w_ARSALCOM='S',.w_ARFLCOM1,"N"),"N")
        .w_CODICE = .w_ARCODART
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .w_ARFLESAU = 'N'
          .DoRTCalc(49,52,.f.)
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
          .DoRTCalc(54,54,.f.)
        .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_UM1 = .w_ARUNMIS1
        .w_UM2 = .w_ARUNMIS2
        .DoRTCalc(60,62,.f.)
          if not(empty(.w_ARTIPCO1))
          .link_2_7('Full')
          endif
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_ARTPCONF))
          .link_2_8('Full')
          endif
          .DoRTCalc(64,64,.f.)
        .w_ARPZCONF = 0
          .DoRTCalc(66,69,.f.)
        .w_VOLUME = cp_ROUND(.w_ARDIMLUN*.w_ARDIMLAR*.w_ARDIMALT,3)
        .DoRTCalc(71,71,.f.)
          if not(empty(.w_ARUMVOLU))
          .link_2_16('Full')
          endif
        .w_ARUMDIME = SPACE(3)
        .DoRTCalc(72,72,.f.)
          if not(empty(.w_ARUMDIME))
          .link_2_17('Full')
          endif
          .DoRTCalc(73,73,.f.)
        .w_ARFLCONA = ' '
        .DoRTCalc(75,77,.f.)
          if not(empty(.w_ARTIPCO2))
          .link_2_22('Full')
          endif
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_ARTPCON2))
          .link_2_23('Full')
          endif
          .DoRTCalc(79,79,.f.)
        .w_ARPZCON2 = 0
          .DoRTCalc(81,84,.f.)
        .w_VOLUM2 = cp_ROUND(.w_ARDIMLU2*.w_ARDIMLA2*.w_ARDIMAL2,3)
        .DoRTCalc(86,86,.f.)
          if not(empty(.w_ARUMVOL2))
          .link_2_31('Full')
          endif
        .w_ARUMDIM2 = SPACE(3)
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_ARUMDIM2))
          .link_2_32('Full')
          endif
          .DoRTCalc(88,88,.f.)
        .w_ARFLCON2 = ' '
        .w_ARUTISER = 'N'
        .w_ARPERSER = 'I'
        .w_ARNOMENC = ''
        .DoRTCalc(92,92,.f.)
          if not(empty(.w_ARNOMENC))
          .link_2_37('Full')
          endif
        .w_ARMOLSUP = 0
        .w_ARDATINT = IIF(.w_ARUTISER='N', 'F', 'N')
        .DoRTCalc(95,95,.f.)
          if not(empty(.w_ARUMSUPP))
          .link_2_40('Full')
          endif
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_TIPPRO = 'F'
        .DoRTCalc(99,99,.f.)
          if not(empty(.w_ARCODMAR))
          .link_3_4('Full')
          endif
        .DoRTCalc(100,101,.f.)
          if not(empty(.w_ARGRUPRO))
          .link_3_7('Full')
          endif
        .DoRTCalc(102,104,.f.)
          if not(empty(.w_ARCATSCM))
          .link_3_10('Full')
          endif
        .DoRTCalc(105,107,.f.)
          if not(empty(.w_ARCODRIC))
          .link_3_18('Full')
          endif
          .DoRTCalc(108,109,.f.)
        .w_OBTEST = i_datsys
        .w_ARARTPOS = ' '
        .w_ARCODREP = IIF(.w_ARARTPOS<>'S',SPACE(3),.w_ARCODREP)
        .DoRTCalc(112,112,.f.)
          if not(empty(.w_ARCODREP))
          .link_3_26('Full')
          endif
          .DoRTCalc(113,114,.f.)
        .w_READPRO = 'PF'
        .DoRTCalc(115,115,.f.)
          if not(empty(.w_READPRO))
          .link_4_1('Full')
          endif
          .DoRTCalc(116,116,.f.)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
          .DoRTCalc(119,121,.f.)
        .w_LICODART = .w_ARCODART
          .DoRTCalc(123,123,.f.)
        .w_ARPUBWEB = ' '
        .w_ARFLPECO = 'C'
        .DoRTCalc(126,126,.f.)
          if not(empty(.w_ARCODGRU))
          .link_3_31('Full')
          endif
        .DoRTCalc(127,127,.f.)
          if not(empty(.w_ARCODSOT))
          .link_3_32('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
          .DoRTCalc(128,130,.f.)
        .w_ARSTACOD = SPACE(1)
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .DoRTCalc(132,136,.f.)
          if not(empty(.w_IVAREP))
          .link_3_42('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
          .DoRTCalc(137,139,.f.)
        .w_ARPUBWEB = 'N'
        .w_ARPUBWEB = 'N'
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
          .DoRTCalc(142,142,.f.)
        .w_ARFLLOTT = 'N'
        .w_ARFLLMAG = IIF(.w_ARFLLOTT='C','S',' ')
        .w_ARCLALOT = Space(5)
        .DoRTCalc(145,145,.f.)
          if not(empty(.w_ARCLALOT))
          .link_5_3('Full')
          endif
        .w_ARDISLOT = 'S'
        .w_ARFLESUL = 'N'
          .DoRTCalc(148,148,.f.)
        .w_ARCLAMAT = Space(5)
        .DoRTCalc(149,149,.f.)
          if not(empty(.w_ARCLAMAT))
          .link_5_7('Full')
          endif
        .DoRTCalc(150,150,.f.)
          if not(empty(.w_ARVOCCEN))
          .link_5_8('Full')
          endif
        .DoRTCalc(151,151,.f.)
          if not(empty(.w_ARVOCRIC))
          .link_5_9('Full')
          endif
        .DoRTCalc(152,152,.f.)
          if not(empty(.w_ARCODCEN))
          .link_5_10('Full')
          endif
        .w_ARCODDIS = IIF(.w_ARKITIMB='N',.w_ARCODDIS, Space(20))
        .DoRTCalc(153,153,.f.)
          if not(empty(.w_ARCODDIS))
          .link_5_11('Full')
          endif
        .w_ARFLCOMP = ' '
        .DoRTCalc(155,155,.f.)
          if not(empty(.w_ARMAGPRE))
          .link_5_13('Full')
          endif
        .w_ARTIPPRE = 'N'
          .DoRTCalc(157,164,.f.)
        .w_OLDFLAG = .w_ARFLLOTT
          .DoRTCalc(166,168,.f.)
        .w_ARKITIMB = 'N'
        .w_ARFLESIM = IIF(.w_ARKITIMB<>'N' Or .w_DISKIT<>'I' Or Empty(.w_ARCODDIS),' ',.w_ARFLESIM)
          .DoRTCalc(171,172,.f.)
        .w_NDATKEY = .w_ARDTOBSO
        .w_ARDATINT = 'F'
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .DoRTCalc(175,175,.f.)
          if not(empty(.w_ARCLACRI))
          .link_5_45('Full')
          endif
          .DoRTCalc(176,178,.f.)
        .w_ARCHKUCA = 'N'
          .DoRTCalc(180,180,.f.)
        .w_DELIMM = .f.
          .DoRTCalc(182,182,.f.)
        .w_GEST = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_ARFLCOMM = iif(.w_ARTIPGES="F" and !empty(.w_ARFLCOM1),iif(.w_ARFLCOM1="C","S",.w_ARFLCOM1),"N")
        .w_ARTIPPKR = iif(.cFunction='Load', .w_ARTIPART, .w_ARTIPPKR)
          .DoRTCalc(186,190,.f.)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_CODATT = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("GRUPPO") )
        .w_IMMODATR = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_ARTIPIMP = 'C'
        .DoRTCalc(196,196,.f.)
          if not(empty(.w_ARMAGIMP))
          .link_5_53('Full')
          endif
          .DoRTCalc(197,197,.f.)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .DoRTCalc(200,200,.f.)
          if not(empty(.w_ARFSRIFE))
          .link_7_7('Full')
          endif
          .DoRTCalc(201,201,.f.)
        .w_ARCMPCAR = "N"
        .w_ARCONCAR = 'N'
          .DoRTCalc(204,207,.f.)
        .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
      endif
    endwith
    cp_BlankRecExtFlds(this,'ART_ICOL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_15.enabled = this.oPgFrm.Page3.oPag.oBtn_3_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oARCODART_1_4.enabled = i_bVal
      .Page1.oPag.oARDESART_1_5.enabled = i_bVal
      .Page1.oPag.oARTIPART_1_7.enabled = i_bVal
      .Page1.oPag.oARDESSUP_1_8.enabled = i_bVal
      .Page1.oPag.oARUNMIS1_1_11.enabled = i_bVal
      .Page1.oPag.oARUNMIS2_1_13.enabled = i_bVal
      .Page1.oPag.oAROPERAT_1_14.enabled = i_bVal
      .Page1.oPag.oARMOLTIP_1_16.enabled = i_bVal
      .Page1.oPag.oARFLUSEP_1_17.enabled = i_bVal
      .Page1.oPag.oARPREZUM_1_18.enabled = i_bVal
      .Page1.oPag.oARFLINVE_1_19.enabled = i_bVal
      .Page1.oPag.oARSTASUP_1_20.enabled = i_bVal
      .Page1.oPag.oARASSIVA_1_21.enabled = i_bVal
      .Page1.oPag.oARCODIVA_1_23.enabled = i_bVal
      .Page1.oPag.oARTIPOPE_1_24.enabled = i_bVal
      .Page1.oPag.oARCATCON_1_26.enabled = i_bVal
      .Page1.oPag.oARCODFAM_1_28.enabled = i_bVal
      .Page1.oPag.oARGRUMER_1_30.enabled = i_bVal
      .Page1.oPag.oARCATOMO_1_32.enabled = i_bVal
      .Page1.oPag.oARTIPGES_1_34.enabled = i_bVal
      .Page1.oPag.oARPROPRE_1_35.enabled = i_bVal
      .Page1.oPag.oARTIPBAR_1_36.enabled = i_bVal
      .Page1.oPag.oTIPBAR_1_37.enabled = i_bVal
      .Page1.oPag.oARFLDISP_1_38.enabled = i_bVal
      .Page1.oPag.oARFLDISC_1_39.enabled = i_bVal
      .Page1.oPag.oARCODCLA_1_40.enabled = i_bVal
      .Page1.oPag.oARSALCOM_1_41.enabled = i_bVal
      .Page1.oPag.oARDTINVA_1_42.enabled = i_bVal
      .Page1.oPag.oARDTOBSO_1_43.enabled = i_bVal
      .Page1.oPag.oARFLCOM1_1_44.enabled = i_bVal
      .Page1.oPag.oARFLCOM1_1_45.enabled = i_bVal
      .Page1.oPag.oARFLCOM1_1_46.enabled = i_bVal
      .Page2.oPag.oARPESNET_2_5.enabled = i_bVal
      .Page2.oPag.oARPESLOR_2_6.enabled = i_bVal
      .Page2.oPag.oARTIPCO1_2_7.enabled = i_bVal
      .Page2.oPag.oARTPCONF_2_8.enabled = i_bVal
      .Page2.oPag.oARPZCONF_2_10.enabled = i_bVal
      .Page2.oPag.oARCOCOL1_2_11.enabled = i_bVal
      .Page2.oPag.oARDIMLUN_2_12.enabled = i_bVal
      .Page2.oPag.oARDIMLAR_2_13.enabled = i_bVal
      .Page2.oPag.oARDIMALT_2_14.enabled = i_bVal
      .Page2.oPag.oARUMVOLU_2_16.enabled = i_bVal
      .Page2.oPag.oARUMDIME_2_17.enabled = i_bVal
      .Page2.oPag.oARDESVOL_2_18.enabled = i_bVal
      .Page2.oPag.oARFLCONA_2_19.enabled = i_bVal
      .Page2.oPag.oARPESNE2_2_20.enabled = i_bVal
      .Page2.oPag.oARPESLO2_2_21.enabled = i_bVal
      .Page2.oPag.oARTIPCO2_2_22.enabled = i_bVal
      .Page2.oPag.oARTPCON2_2_23.enabled = i_bVal
      .Page2.oPag.oARPZCON2_2_25.enabled = i_bVal
      .Page2.oPag.oARCOCOL2_2_26.enabled = i_bVal
      .Page2.oPag.oARDIMLU2_2_27.enabled = i_bVal
      .Page2.oPag.oARDIMLA2_2_28.enabled = i_bVal
      .Page2.oPag.oARDIMAL2_2_29.enabled = i_bVal
      .Page2.oPag.oARUMVOL2_2_31.enabled = i_bVal
      .Page2.oPag.oARUMDIM2_2_32.enabled = i_bVal
      .Page2.oPag.oARDESVO2_2_33.enabled = i_bVal
      .Page2.oPag.oARFLCON2_2_34.enabled = i_bVal
      .Page2.oPag.oARUTISER_2_35.enabled = i_bVal
      .Page2.oPag.oARPERSER_2_36.enabled = i_bVal
      .Page2.oPag.oARNOMENC_2_37.enabled = i_bVal
      .Page2.oPag.oARMOLSUP_2_38.enabled = i_bVal
      .Page2.oPag.oARDATINT_2_39.enabled = i_bVal
      .Page3.oPag.oARCODMAR_3_4.enabled = i_bVal
      .Page3.oPag.oARGRUPRO_3_7.enabled = i_bVal
      .Page3.oPag.oARCATSCM_3_10.enabled = i_bVal
      .Page3.oPag.oARCODRIC_3_18.enabled = i_bVal
      .Page3.oPag.oARARTPOS_3_25.enabled = i_bVal
      .Page3.oPag.oARCODREP_3_26.enabled = i_bVal
      .Page3.oPag.oARPUBWEB_3_29.enabled = i_bVal
      .Page3.oPag.oARFLPECO_3_30.enabled = i_bVal
      .Page3.oPag.oARCODGRU_3_31.enabled = i_bVal
      .Page3.oPag.oARCODSOT_3_32.enabled = i_bVal
      .Page3.oPag.oARMINVEN_3_33.enabled = i_bVal
      .Page3.oPag.oARPUBWEB_3_44.enabled = i_bVal
      .Page3.oPag.oARPUBWEB_3_45.enabled = i_bVal
      .Page5.oPag.oARFLLOTT_5_1.enabled = i_bVal
      .Page5.oPag.oARFLLMAG_5_2.enabled = i_bVal
      .Page5.oPag.oARCLALOT_5_3.enabled = i_bVal
      .Page5.oPag.oARDISLOT_5_4.enabled = i_bVal
      .Page5.oPag.oARFLESUL_5_5.enabled = i_bVal
      .Page5.oPag.oARGESMAT_5_6.enabled = i_bVal
      .Page5.oPag.oARCLAMAT_5_7.enabled = i_bVal
      .Page5.oPag.oARVOCCEN_5_8.enabled = i_bVal
      .Page5.oPag.oARVOCRIC_5_9.enabled = i_bVal
      .Page5.oPag.oARCODCEN_5_10.enabled = i_bVal
      .Page5.oPag.oARCODDIS_5_11.enabled = i_bVal
      .Page5.oPag.oARFLCOMP_5_12.enabled = i_bVal
      .Page5.oPag.oARMAGPRE_5_13.enabled = i_bVal
      .Page5.oPag.oARTIPPRE_5_14.enabled = i_bVal
      .Page5.oPag.oARKITIMB_5_42.enabled = i_bVal
      .Page5.oPag.oARFLESIM_5_44.enabled = i_bVal
      .Page3.oPag.oARFLAPCA_3_47.enabled = i_bVal
      .Page5.oPag.oARCLACRI_5_45.enabled = i_bVal
      .Page1.oPag.oARCHKUCA_1_115.enabled = i_bVal
      .Page5.oPag.oARTIPIMP_5_52.enabled = i_bVal
      .Page5.oPag.oARMAGIMP_5_53.enabled = i_bVal
      .Page5.oPag.oARCMPCAR_5_58.enabled = i_bVal
      .Page3.oPag.oARCODTIP_3_53.enabled = i_bVal
      .Page3.oPag.oARCODCLF_3_54.enabled = i_bVal
      .Page3.oPag.oARCODVAL_3_55.enabled = i_bVal
      .Page3.oPag.oARFLGESC_3_56.enabled = i_bVal
      .Page1.oPag.oBtn_1_75.enabled = .Page1.oPag.oBtn_1_75.mCond()
      .Page1.oPag.oBtn_1_77.enabled = .Page1.oPag.oBtn_1_77.mCond()
      .Page3.oPag.oBtn_3_6.enabled = .Page3.oPag.oBtn_3_6.mCond()
      .Page3.oPag.oBtn_3_15.enabled = .Page3.oPag.oBtn_3_15.mCond()
      .Page1.oPag.oBtn_1_83.enabled = .Page1.oPag.oBtn_1_83.mCond()
      .Page1.oPag.oBtn_1_84.enabled = .Page1.oPag.oBtn_1_84.mCond()
      .Page1.oPag.oBtn_1_103.enabled = .Page1.oPag.oBtn_1_103.mCond()
      .Page1.oPag.oBtn_1_104.enabled = .Page1.oPag.oBtn_1_104.mCond()
      .Page1.oPag.oBtn_1_105.enabled = .Page1.oPag.oBtn_1_105.mCond()
      .Page1.oPag.oBtn_1_106.enabled = .Page1.oPag.oBtn_1_106.mCond()
      .Page1.oPag.oObj_1_48.enabled = i_bVal
      .Page1.oPag.oObj_1_85.enabled = i_bVal
      .Page1.oPag.oObj_1_86.enabled = i_bVal
      .Page1.oPag.oObj_1_87.enabled = i_bVal
      .Page1.oPag.oObj_1_90.enabled = i_bVal
      .Page1.oPag.oObj_1_96.enabled = i_bVal
      .Page1.oPag.oObj_1_97.enabled = i_bVal
      .Page1.oPag.oObj_1_113.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oARCODART_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oARCODART_1_4.enabled = .t.
        .Page1.oPag.oARDESART_1_5.enabled = .t.
        .Page5.oPag.oARCODDIS_5_11.enabled = .t.
      endif
    endwith
    this.GSMA_MLI.SetStatus(i_cOp)
    this.GSMA_MRP.SetStatus(i_cOp)
    this.GSMA_APR.SetStatus(i_cOp)
    this.GSMA_MAL.SetStatus(i_cOp)
    this.GSAR_MAR.SetStatus(i_cOp)
    this.GSAR_MR2.SetStatus(i_cOp)
    this.GSMA_ANO.SetStatus(i_cOp)
    this.GSMA_MDA.SetStatus(i_cOp)
    this.GSMA_MAC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ART_ICOL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MLI.SetChildrenStatus(i_cOp)
  *  this.GSMA_MRP.SetChildrenStatus(i_cOp)
  *  this.GSMA_APR.SetChildrenStatus(i_cOp)
  *  this.GSMA_MAL.SetChildrenStatus(i_cOp)
  *  this.GSAR_MAR.SetChildrenStatus(i_cOp)
  *  this.GSAR_MR2.SetChildrenStatus(i_cOp)
  *  this.GSMA_ANO.SetChildrenStatus(i_cOp)
  *  this.GSMA_MDA.SetChildrenStatus(i_cOp)
  *  this.GSMA_MAC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODART,"ARCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESART,"ARDESART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPART,"ARTIPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPART,"ARTIPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESSUP,"ARDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS1,"ARUNMIS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS2,"ARUNMIS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AROPERAT,"AROPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLTIP,"ARMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLUSEP,"ARFLUSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPREZUM,"ARPREZUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLINVE,"ARFLINVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTASUP,"ARSTASUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARASSIVA,"ARASSIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATOPE,"ARCATOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODIVA,"ARCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPOPE,"ARTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATCON,"ARCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODFAM,"ARCODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARGRUMER,"ARGRUMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATOMO,"ARCATOMO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPGES,"ARTIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPROPRE,"ARPROPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPBAR,"ARTIPBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLDISP,"ARFLDISP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLDISC,"ARFLDISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCLA,"ARCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSALCOM,"ARSALCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTINVA,"ARDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTOBSO,"ARDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOM1,"ARFLCOM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOM1,"ARFLCOM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOM1,"ARFLCOM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLESAU,"ARFLESAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPESNET,"ARPESNET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPESLOR,"ARPESLOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPCO1,"ARTIPCO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTPCONF,"ARTPCONF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPZCONF,"ARPZCONF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCOCOL1,"ARCOCOL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMLUN,"ARDIMLUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMLAR,"ARDIMLAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMALT,"ARDIMALT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMVOLU,"ARUMVOLU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMDIME,"ARUMDIME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESVOL,"ARDESVOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCONA,"ARFLCONA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPESNE2,"ARPESNE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPESLO2,"ARPESLO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPCO2,"ARTIPCO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTPCON2,"ARTPCON2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPZCON2,"ARPZCON2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCOCOL2,"ARCOCOL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMLU2,"ARDIMLU2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMLA2,"ARDIMLA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDIMAL2,"ARDIMAL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMVOL2,"ARUMVOL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMDIM2,"ARUMDIM2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESVO2,"ARDESVO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCON2,"ARFLCON2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUTISER,"ARUTISER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPERSER,"ARPERSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARNOMENC,"ARNOMENC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLSUP,"ARMOLSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDATINT,"ARDATINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMSUPP,"ARUMSUPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODMAR,"ARCODMAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARGRUPRO,"ARGRUPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATSCM,"ARCATSCM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODRIC,"ARCODRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARARTPOS,"ARARTPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODREP,"ARCODREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLPECO,"ARFLPECO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODGRU,"ARCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODSOT,"ARCODSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMINVEN,"ARMINVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTACOD,"ARSTACOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLLOTT,"ARFLLOTT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLLMAG,"ARFLLMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCLALOT,"ARCLALOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDISLOT,"ARDISLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLESUL,"ARFLESUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARGESMAT,"ARGESMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCLAMAT,"ARCLAMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARVOCCEN,"ARVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARVOCRIC,"ARVOCRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCEN,"ARCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODDIS,"ARCODDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOMP,"ARFLCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMAGPRE,"ARMAGPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPPRE,"ARTIPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARKITIMB,"ARKITIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLESIM,"ARFLESIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLAPCA,"ARFLAPCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDATINT,"ARDATINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCLACRI,"ARCLACRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCHKUCA,"ARCHKUCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GESTGUID,"GESTGUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOMM,"ARFLCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPPKR,"ARTIPPKR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPIMP,"ARTIPIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMAGIMP,"ARMAGIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFSRIFE,"ARFSRIFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCMPCAR,"ARCMPCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCONCAR,"ARCONCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODTIP,"ARCODTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCLF,"ARCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODVAL,"ARCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLGESC,"ARFLGESC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsma_aar
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo
    IF NOT EMPTY(i_cWhere)
       IF AT('ARTIPART', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and ARTIPART in ('PF','SE','MP','PH','MC','MA','IM')"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    i_lTable = "ART_ICOL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SAR with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ART_ICOL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValODBCExtFlds(this,'ART_ICOL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ARCODART,ARDESART,ARTIPART,ARDESSUP,ARUNMIS1"+;
                  ",ARUNMIS2,AROPERAT,ARMOLTIP,ARFLUSEP,ARPREZUM"+;
                  ",ARFLINVE,ARSTASUP,ARASSIVA,ARCATOPE,ARCODIVA"+;
                  ",ARTIPOPE,ARCATCON,ARCODFAM,ARGRUMER,ARCATOMO"+;
                  ",ARTIPGES,ARPROPRE,ARTIPBAR,ARFLDISP,ARFLDISC"+;
                  ",ARCODCLA,ARSALCOM,ARDTINVA,ARDTOBSO,ARFLCOM1"+;
                  ",ARFLESAU,UTCC,UTCV,UTDC,UTDV"+;
                  ",ARPESNET,ARPESLOR,ARTIPCO1,ARTPCONF,ARPZCONF"+;
                  ",ARCOCOL1,ARDIMLUN,ARDIMLAR,ARDIMALT,ARUMVOLU"+;
                  ",ARUMDIME,ARDESVOL,ARFLCONA,ARPESNE2,ARPESLO2"+;
                  ",ARTIPCO2,ARTPCON2,ARPZCON2,ARCOCOL2,ARDIMLU2"+;
                  ",ARDIMLA2,ARDIMAL2,ARUMVOL2,ARUMDIM2,ARDESVO2"+;
                  ",ARFLCON2,ARUTISER,ARPERSER,ARNOMENC,ARMOLSUP"+;
                  ",ARUMSUPP,ARCODMAR,ARGRUPRO,ARCATSCM,ARCODRIC"+;
                  ",ARARTPOS,ARCODREP,ARPUBWEB,ARFLPECO,ARCODGRU"+;
                  ",ARCODSOT,ARMINVEN,ARSTACOD,ARFLLOTT,ARFLLMAG"+;
                  ",ARCLALOT,ARDISLOT,ARFLESUL,ARGESMAT,ARCLAMAT"+;
                  ",ARVOCCEN,ARVOCRIC,ARCODCEN,ARCODDIS,ARFLCOMP"+;
                  ",ARMAGPRE,ARTIPPRE,ARKITIMB,ARFLESIM,ARFLAPCA"+;
                  ",ARDATINT,ARCLACRI,ARCHKUCA,GESTGUID,ARFLCOMM"+;
                  ",ARTIPPKR,ARTIPIMP,ARMAGIMP,ARFSRIFE,ARCMPCAR"+;
                  ",ARCONCAR,ARCODTIP,ARCODCLF,ARCODVAL,ARFLGESC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ARCODART)+;
                  ","+cp_ToStrODBC(this.w_ARDESART)+;
                  ","+cp_ToStrODBC(this.w_ARTIPART)+;
                  ","+cp_ToStrODBC(this.w_ARDESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
                  ","+cp_ToStrODBC(this.w_AROPERAT)+;
                  ","+cp_ToStrODBC(this.w_ARMOLTIP)+;
                  ","+cp_ToStrODBC(this.w_ARFLUSEP)+;
                  ","+cp_ToStrODBC(this.w_ARPREZUM)+;
                  ","+cp_ToStrODBC(this.w_ARFLINVE)+;
                  ","+cp_ToStrODBC(this.w_ARSTASUP)+;
                  ","+cp_ToStrODBC(this.w_ARASSIVA)+;
                  ","+cp_ToStrODBC(this.w_ARCATOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODIVA)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODFAM)+;
                  ","+cp_ToStrODBCNull(this.w_ARGRUMER)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATOMO)+;
                  ","+cp_ToStrODBC(this.w_ARTIPGES)+;
                  ","+cp_ToStrODBC(this.w_ARPROPRE)+;
                  ","+cp_ToStrODBC(this.w_ARTIPBAR)+;
                  ","+cp_ToStrODBC(this.w_ARFLDISP)+;
                  ","+cp_ToStrODBC(this.w_ARFLDISC)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODCLA)+;
                  ","+cp_ToStrODBC(this.w_ARSALCOM)+;
                  ","+cp_ToStrODBC(this.w_ARDTINVA)+;
                  ","+cp_ToStrODBC(this.w_ARDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_ARFLCOM1)+;
                  ","+cp_ToStrODBC(this.w_ARFLESAU)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ARPESNET)+;
                  ","+cp_ToStrODBC(this.w_ARPESLOR)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPCO1)+;
                  ","+cp_ToStrODBCNull(this.w_ARTPCONF)+;
                  ","+cp_ToStrODBC(this.w_ARPZCONF)+;
                  ","+cp_ToStrODBC(this.w_ARCOCOL1)+;
                  ","+cp_ToStrODBC(this.w_ARDIMLUN)+;
                  ","+cp_ToStrODBC(this.w_ARDIMLAR)+;
                  ","+cp_ToStrODBC(this.w_ARDIMALT)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMVOLU)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMDIME)+;
                  ","+cp_ToStrODBC(this.w_ARDESVOL)+;
                  ","+cp_ToStrODBC(this.w_ARFLCONA)+;
                  ","+cp_ToStrODBC(this.w_ARPESNE2)+;
                  ","+cp_ToStrODBC(this.w_ARPESLO2)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPCO2)+;
                  ","+cp_ToStrODBCNull(this.w_ARTPCON2)+;
                  ","+cp_ToStrODBC(this.w_ARPZCON2)+;
                  ","+cp_ToStrODBC(this.w_ARCOCOL2)+;
                  ","+cp_ToStrODBC(this.w_ARDIMLU2)+;
                  ","+cp_ToStrODBC(this.w_ARDIMLA2)+;
                  ","+cp_ToStrODBC(this.w_ARDIMAL2)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMVOL2)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMDIM2)+;
                  ","+cp_ToStrODBC(this.w_ARDESVO2)+;
                  ","+cp_ToStrODBC(this.w_ARFLCON2)+;
                  ","+cp_ToStrODBC(this.w_ARUTISER)+;
                  ","+cp_ToStrODBC(this.w_ARPERSER)+;
                  ","+cp_ToStrODBCNull(this.w_ARNOMENC)+;
                  ","+cp_ToStrODBC(this.w_ARMOLSUP)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMSUPP)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODMAR)+;
                  ","+cp_ToStrODBCNull(this.w_ARGRUPRO)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATSCM)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODRIC)+;
                  ","+cp_ToStrODBC(this.w_ARARTPOS)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODREP)+;
                  ","+cp_ToStrODBC(this.w_ARPUBWEB)+;
                  ","+cp_ToStrODBC(this.w_ARFLPECO)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODGRU)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODSOT)+;
                  ","+cp_ToStrODBC(this.w_ARMINVEN)+;
                  ","+cp_ToStrODBC(this.w_ARSTACOD)+;
                  ","+cp_ToStrODBC(this.w_ARFLLOTT)+;
                  ","+cp_ToStrODBC(this.w_ARFLLMAG)+;
                  ","+cp_ToStrODBCNull(this.w_ARCLALOT)+;
                  ","+cp_ToStrODBC(this.w_ARDISLOT)+;
                  ","+cp_ToStrODBC(this.w_ARFLESUL)+;
                  ","+cp_ToStrODBC(this.w_ARGESMAT)+;
                  ","+cp_ToStrODBCNull(this.w_ARCLAMAT)+;
                  ","+cp_ToStrODBCNull(this.w_ARVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_ARVOCRIC)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODDIS)+;
                  ","+cp_ToStrODBC(this.w_ARFLCOMP)+;
                  ","+cp_ToStrODBCNull(this.w_ARMAGPRE)+;
                  ","+cp_ToStrODBC(this.w_ARTIPPRE)+;
                  ","+cp_ToStrODBC(this.w_ARKITIMB)+;
                  ","+cp_ToStrODBC(this.w_ARFLESIM)+;
                  ","+cp_ToStrODBC(this.w_ARFLAPCA)+;
                  ","+cp_ToStrODBC(this.w_ARDATINT)+;
                  ","+cp_ToStrODBCNull(this.w_ARCLACRI)+;
                  ","+cp_ToStrODBC(this.w_ARCHKUCA)+;
                  ","+cp_ToStrODBC(this.w_GESTGUID)+;
                  ","+cp_ToStrODBC(this.w_ARFLCOMM)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_ARTIPPKR)+;
                  ","+cp_ToStrODBC(this.w_ARTIPIMP)+;
                  ","+cp_ToStrODBCNull(this.w_ARMAGIMP)+;
                  ","+cp_ToStrODBCNull(this.w_ARFSRIFE)+;
                  ","+cp_ToStrODBC(this.w_ARCMPCAR)+;
                  ","+cp_ToStrODBC(this.w_ARCONCAR)+;
                  ","+cp_ToStrODBC(this.w_ARCODTIP)+;
                  ","+cp_ToStrODBC(this.w_ARCODCLF)+;
                  ","+cp_ToStrODBC(this.w_ARCODVAL)+;
                  ","+cp_ToStrODBC(this.w_ARFLGESC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValVFPExtFlds(this,'ART_ICOL')
        cp_CheckDeletedKey(i_cTable,0,'ARCODART',this.w_ARCODART)
        INSERT INTO (i_cTable);
              (ARCODART,ARDESART,ARTIPART,ARDESSUP,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLUSEP,ARPREZUM,ARFLINVE,ARSTASUP,ARASSIVA,ARCATOPE,ARCODIVA,ARTIPOPE,ARCATCON,ARCODFAM,ARGRUMER,ARCATOMO,ARTIPGES,ARPROPRE,ARTIPBAR,ARFLDISP,ARFLDISC,ARCODCLA,ARSALCOM,ARDTINVA,ARDTOBSO,ARFLCOM1,ARFLESAU,UTCC,UTCV,UTDC,UTDV,ARPESNET,ARPESLOR,ARTIPCO1,ARTPCONF,ARPZCONF,ARCOCOL1,ARDIMLUN,ARDIMLAR,ARDIMALT,ARUMVOLU,ARUMDIME,ARDESVOL,ARFLCONA,ARPESNE2,ARPESLO2,ARTIPCO2,ARTPCON2,ARPZCON2,ARCOCOL2,ARDIMLU2,ARDIMLA2,ARDIMAL2,ARUMVOL2,ARUMDIM2,ARDESVO2,ARFLCON2,ARUTISER,ARPERSER,ARNOMENC,ARMOLSUP,ARUMSUPP,ARCODMAR,ARGRUPRO,ARCATSCM,ARCODRIC,ARARTPOS,ARCODREP,ARPUBWEB,ARFLPECO,ARCODGRU,ARCODSOT,ARMINVEN,ARSTACOD,ARFLLOTT,ARFLLMAG,ARCLALOT,ARDISLOT,ARFLESUL,ARGESMAT,ARCLAMAT,ARVOCCEN,ARVOCRIC,ARCODCEN,ARCODDIS,ARFLCOMP,ARMAGPRE,ARTIPPRE,ARKITIMB,ARFLESIM,ARFLAPCA,ARDATINT,ARCLACRI,ARCHKUCA,GESTGUID,ARFLCOMM,ARTIPPKR,ARTIPIMP,ARMAGIMP,ARFSRIFE,ARCMPCAR,ARCONCAR,ARCODTIP,ARCODCLF,ARCODVAL,ARFLGESC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ARCODART;
                  ,this.w_ARDESART;
                  ,this.w_ARTIPART;
                  ,this.w_ARDESSUP;
                  ,this.w_ARUNMIS1;
                  ,this.w_ARUNMIS2;
                  ,this.w_AROPERAT;
                  ,this.w_ARMOLTIP;
                  ,this.w_ARFLUSEP;
                  ,this.w_ARPREZUM;
                  ,this.w_ARFLINVE;
                  ,this.w_ARSTASUP;
                  ,this.w_ARASSIVA;
                  ,this.w_ARCATOPE;
                  ,this.w_ARCODIVA;
                  ,this.w_ARTIPOPE;
                  ,this.w_ARCATCON;
                  ,this.w_ARCODFAM;
                  ,this.w_ARGRUMER;
                  ,this.w_ARCATOMO;
                  ,this.w_ARTIPGES;
                  ,this.w_ARPROPRE;
                  ,this.w_ARTIPBAR;
                  ,this.w_ARFLDISP;
                  ,this.w_ARFLDISC;
                  ,this.w_ARCODCLA;
                  ,this.w_ARSALCOM;
                  ,this.w_ARDTINVA;
                  ,this.w_ARDTOBSO;
                  ,this.w_ARFLCOM1;
                  ,this.w_ARFLESAU;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ARPESNET;
                  ,this.w_ARPESLOR;
                  ,this.w_ARTIPCO1;
                  ,this.w_ARTPCONF;
                  ,this.w_ARPZCONF;
                  ,this.w_ARCOCOL1;
                  ,this.w_ARDIMLUN;
                  ,this.w_ARDIMLAR;
                  ,this.w_ARDIMALT;
                  ,this.w_ARUMVOLU;
                  ,this.w_ARUMDIME;
                  ,this.w_ARDESVOL;
                  ,this.w_ARFLCONA;
                  ,this.w_ARPESNE2;
                  ,this.w_ARPESLO2;
                  ,this.w_ARTIPCO2;
                  ,this.w_ARTPCON2;
                  ,this.w_ARPZCON2;
                  ,this.w_ARCOCOL2;
                  ,this.w_ARDIMLU2;
                  ,this.w_ARDIMLA2;
                  ,this.w_ARDIMAL2;
                  ,this.w_ARUMVOL2;
                  ,this.w_ARUMDIM2;
                  ,this.w_ARDESVO2;
                  ,this.w_ARFLCON2;
                  ,this.w_ARUTISER;
                  ,this.w_ARPERSER;
                  ,this.w_ARNOMENC;
                  ,this.w_ARMOLSUP;
                  ,this.w_ARUMSUPP;
                  ,this.w_ARCODMAR;
                  ,this.w_ARGRUPRO;
                  ,this.w_ARCATSCM;
                  ,this.w_ARCODRIC;
                  ,this.w_ARARTPOS;
                  ,this.w_ARCODREP;
                  ,this.w_ARPUBWEB;
                  ,this.w_ARFLPECO;
                  ,this.w_ARCODGRU;
                  ,this.w_ARCODSOT;
                  ,this.w_ARMINVEN;
                  ,this.w_ARSTACOD;
                  ,this.w_ARFLLOTT;
                  ,this.w_ARFLLMAG;
                  ,this.w_ARCLALOT;
                  ,this.w_ARDISLOT;
                  ,this.w_ARFLESUL;
                  ,this.w_ARGESMAT;
                  ,this.w_ARCLAMAT;
                  ,this.w_ARVOCCEN;
                  ,this.w_ARVOCRIC;
                  ,this.w_ARCODCEN;
                  ,this.w_ARCODDIS;
                  ,this.w_ARFLCOMP;
                  ,this.w_ARMAGPRE;
                  ,this.w_ARTIPPRE;
                  ,this.w_ARKITIMB;
                  ,this.w_ARFLESIM;
                  ,this.w_ARFLAPCA;
                  ,this.w_ARDATINT;
                  ,this.w_ARCLACRI;
                  ,this.w_ARCHKUCA;
                  ,this.w_GESTGUID;
                  ,this.w_ARFLCOMM;
                  ,this.w_ARTIPPKR;
                  ,this.w_ARTIPIMP;
                  ,this.w_ARMAGIMP;
                  ,this.w_ARFSRIFE;
                  ,this.w_ARCMPCAR;
                  ,this.w_ARCONCAR;
                  ,this.w_ARCODTIP;
                  ,this.w_ARCODCLF;
                  ,this.w_ARCODVAL;
                  ,this.w_ARFLGESC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ART_ICOL_IDX,i_nConn)
      *
      * update ART_ICOL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ART_ICOL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ARDESART="+cp_ToStrODBC(this.w_ARDESART)+;
             ",ARTIPART="+cp_ToStrODBC(this.w_ARTIPART)+;
             ",ARDESSUP="+cp_ToStrODBC(this.w_ARDESSUP)+;
             ",ARUNMIS1="+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
             ",ARUNMIS2="+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
             ",AROPERAT="+cp_ToStrODBC(this.w_AROPERAT)+;
             ",ARMOLTIP="+cp_ToStrODBC(this.w_ARMOLTIP)+;
             ",ARFLUSEP="+cp_ToStrODBC(this.w_ARFLUSEP)+;
             ",ARPREZUM="+cp_ToStrODBC(this.w_ARPREZUM)+;
             ",ARFLINVE="+cp_ToStrODBC(this.w_ARFLINVE)+;
             ",ARSTASUP="+cp_ToStrODBC(this.w_ARSTASUP)+;
             ",ARASSIVA="+cp_ToStrODBC(this.w_ARASSIVA)+;
             ",ARCATOPE="+cp_ToStrODBC(this.w_ARCATOPE)+;
             ",ARCODIVA="+cp_ToStrODBCNull(this.w_ARCODIVA)+;
             ",ARTIPOPE="+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
             ",ARCATCON="+cp_ToStrODBCNull(this.w_ARCATCON)+;
             ",ARCODFAM="+cp_ToStrODBCNull(this.w_ARCODFAM)+;
             ",ARGRUMER="+cp_ToStrODBCNull(this.w_ARGRUMER)+;
             ",ARCATOMO="+cp_ToStrODBCNull(this.w_ARCATOMO)+;
             ",ARTIPGES="+cp_ToStrODBC(this.w_ARTIPGES)+;
             ",ARPROPRE="+cp_ToStrODBC(this.w_ARPROPRE)+;
             ",ARTIPBAR="+cp_ToStrODBC(this.w_ARTIPBAR)+;
             ",ARFLDISP="+cp_ToStrODBC(this.w_ARFLDISP)+;
             ",ARFLDISC="+cp_ToStrODBC(this.w_ARFLDISC)+;
             ",ARCODCLA="+cp_ToStrODBCNull(this.w_ARCODCLA)+;
             ",ARSALCOM="+cp_ToStrODBC(this.w_ARSALCOM)+;
             ",ARDTINVA="+cp_ToStrODBC(this.w_ARDTINVA)+;
             ",ARDTOBSO="+cp_ToStrODBC(this.w_ARDTOBSO)+;
             ",ARFLCOM1="+cp_ToStrODBC(this.w_ARFLCOM1)+;
             ",ARFLESAU="+cp_ToStrODBC(this.w_ARFLESAU)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ARPESNET="+cp_ToStrODBC(this.w_ARPESNET)+;
             ",ARPESLOR="+cp_ToStrODBC(this.w_ARPESLOR)+;
             ",ARTIPCO1="+cp_ToStrODBCNull(this.w_ARTIPCO1)+;
             ",ARTPCONF="+cp_ToStrODBCNull(this.w_ARTPCONF)+;
             ",ARPZCONF="+cp_ToStrODBC(this.w_ARPZCONF)+;
             ",ARCOCOL1="+cp_ToStrODBC(this.w_ARCOCOL1)+;
             ",ARDIMLUN="+cp_ToStrODBC(this.w_ARDIMLUN)+;
             ",ARDIMLAR="+cp_ToStrODBC(this.w_ARDIMLAR)+;
             ",ARDIMALT="+cp_ToStrODBC(this.w_ARDIMALT)+;
             ",ARUMVOLU="+cp_ToStrODBCNull(this.w_ARUMVOLU)+;
             ",ARUMDIME="+cp_ToStrODBCNull(this.w_ARUMDIME)+;
             ",ARDESVOL="+cp_ToStrODBC(this.w_ARDESVOL)+;
             ",ARFLCONA="+cp_ToStrODBC(this.w_ARFLCONA)+;
             ",ARPESNE2="+cp_ToStrODBC(this.w_ARPESNE2)+;
             ",ARPESLO2="+cp_ToStrODBC(this.w_ARPESLO2)+;
             ",ARTIPCO2="+cp_ToStrODBCNull(this.w_ARTIPCO2)+;
             ",ARTPCON2="+cp_ToStrODBCNull(this.w_ARTPCON2)+;
             ",ARPZCON2="+cp_ToStrODBC(this.w_ARPZCON2)+;
             ",ARCOCOL2="+cp_ToStrODBC(this.w_ARCOCOL2)+;
             ",ARDIMLU2="+cp_ToStrODBC(this.w_ARDIMLU2)+;
             ",ARDIMLA2="+cp_ToStrODBC(this.w_ARDIMLA2)+;
             ",ARDIMAL2="+cp_ToStrODBC(this.w_ARDIMAL2)+;
             ",ARUMVOL2="+cp_ToStrODBCNull(this.w_ARUMVOL2)+;
             ",ARUMDIM2="+cp_ToStrODBCNull(this.w_ARUMDIM2)+;
             ",ARDESVO2="+cp_ToStrODBC(this.w_ARDESVO2)+;
             ",ARFLCON2="+cp_ToStrODBC(this.w_ARFLCON2)+;
             ",ARUTISER="+cp_ToStrODBC(this.w_ARUTISER)+;
             ",ARPERSER="+cp_ToStrODBC(this.w_ARPERSER)+;
             ",ARNOMENC="+cp_ToStrODBCNull(this.w_ARNOMENC)+;
             ",ARMOLSUP="+cp_ToStrODBC(this.w_ARMOLSUP)+;
             ",ARUMSUPP="+cp_ToStrODBCNull(this.w_ARUMSUPP)+;
             ",ARCODMAR="+cp_ToStrODBCNull(this.w_ARCODMAR)+;
             ",ARGRUPRO="+cp_ToStrODBCNull(this.w_ARGRUPRO)+;
             ",ARCATSCM="+cp_ToStrODBCNull(this.w_ARCATSCM)+;
             ",ARCODRIC="+cp_ToStrODBCNull(this.w_ARCODRIC)+;
             ",ARARTPOS="+cp_ToStrODBC(this.w_ARARTPOS)+;
             ",ARCODREP="+cp_ToStrODBCNull(this.w_ARCODREP)+;
             ",ARPUBWEB="+cp_ToStrODBC(this.w_ARPUBWEB)+;
             ",ARFLPECO="+cp_ToStrODBC(this.w_ARFLPECO)+;
             ",ARCODGRU="+cp_ToStrODBCNull(this.w_ARCODGRU)+;
             ",ARCODSOT="+cp_ToStrODBCNull(this.w_ARCODSOT)+;
             ",ARMINVEN="+cp_ToStrODBC(this.w_ARMINVEN)+;
             ",ARSTACOD="+cp_ToStrODBC(this.w_ARSTACOD)+;
             ",ARFLLOTT="+cp_ToStrODBC(this.w_ARFLLOTT)+;
             ",ARFLLMAG="+cp_ToStrODBC(this.w_ARFLLMAG)+;
             ",ARCLALOT="+cp_ToStrODBCNull(this.w_ARCLALOT)+;
             ",ARDISLOT="+cp_ToStrODBC(this.w_ARDISLOT)+;
             ",ARFLESUL="+cp_ToStrODBC(this.w_ARFLESUL)+;
             ",ARGESMAT="+cp_ToStrODBC(this.w_ARGESMAT)+;
             ",ARCLAMAT="+cp_ToStrODBCNull(this.w_ARCLAMAT)+;
             ",ARVOCCEN="+cp_ToStrODBCNull(this.w_ARVOCCEN)+;
             ",ARVOCRIC="+cp_ToStrODBCNull(this.w_ARVOCRIC)+;
             ",ARCODCEN="+cp_ToStrODBCNull(this.w_ARCODCEN)+;
             ",ARCODDIS="+cp_ToStrODBCNull(this.w_ARCODDIS)+;
             ",ARFLCOMP="+cp_ToStrODBC(this.w_ARFLCOMP)+;
             ",ARMAGPRE="+cp_ToStrODBCNull(this.w_ARMAGPRE)+;
             ",ARTIPPRE="+cp_ToStrODBC(this.w_ARTIPPRE)+;
             ",ARKITIMB="+cp_ToStrODBC(this.w_ARKITIMB)+;
             ",ARFLESIM="+cp_ToStrODBC(this.w_ARFLESIM)+;
             ",ARFLAPCA="+cp_ToStrODBC(this.w_ARFLAPCA)+;
             ",ARDATINT="+cp_ToStrODBC(this.w_ARDATINT)+;
             ",ARCLACRI="+cp_ToStrODBCNull(this.w_ARCLACRI)+;
             ",ARCHKUCA="+cp_ToStrODBC(this.w_ARCHKUCA)+;
             ",GESTGUID="+cp_ToStrODBC(this.w_GESTGUID)+;
             ",ARFLCOMM="+cp_ToStrODBC(this.w_ARFLCOMM)+;
             ",ARTIPPKR="+cp_ToStrODBC(this.w_ARTIPPKR)+;
             ""
             i_nnn=i_nnn+;
             ",ARTIPIMP="+cp_ToStrODBC(this.w_ARTIPIMP)+;
             ",ARMAGIMP="+cp_ToStrODBCNull(this.w_ARMAGIMP)+;
             ",ARFSRIFE="+cp_ToStrODBCNull(this.w_ARFSRIFE)+;
             ",ARCMPCAR="+cp_ToStrODBC(this.w_ARCMPCAR)+;
             ",ARCONCAR="+cp_ToStrODBC(this.w_ARCONCAR)+;
             ",ARCODTIP="+cp_ToStrODBC(this.w_ARCODTIP)+;
             ",ARCODCLF="+cp_ToStrODBC(this.w_ARCODCLF)+;
             ",ARCODVAL="+cp_ToStrODBC(this.w_ARCODVAL)+;
             ",ARFLGESC="+cp_ToStrODBC(this.w_ARFLGESC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ART_ICOL')
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        UPDATE (i_cTable) SET;
              ARDESART=this.w_ARDESART;
             ,ARTIPART=this.w_ARTIPART;
             ,ARDESSUP=this.w_ARDESSUP;
             ,ARUNMIS1=this.w_ARUNMIS1;
             ,ARUNMIS2=this.w_ARUNMIS2;
             ,AROPERAT=this.w_AROPERAT;
             ,ARMOLTIP=this.w_ARMOLTIP;
             ,ARFLUSEP=this.w_ARFLUSEP;
             ,ARPREZUM=this.w_ARPREZUM;
             ,ARFLINVE=this.w_ARFLINVE;
             ,ARSTASUP=this.w_ARSTASUP;
             ,ARASSIVA=this.w_ARASSIVA;
             ,ARCATOPE=this.w_ARCATOPE;
             ,ARCODIVA=this.w_ARCODIVA;
             ,ARTIPOPE=this.w_ARTIPOPE;
             ,ARCATCON=this.w_ARCATCON;
             ,ARCODFAM=this.w_ARCODFAM;
             ,ARGRUMER=this.w_ARGRUMER;
             ,ARCATOMO=this.w_ARCATOMO;
             ,ARTIPGES=this.w_ARTIPGES;
             ,ARPROPRE=this.w_ARPROPRE;
             ,ARTIPBAR=this.w_ARTIPBAR;
             ,ARFLDISP=this.w_ARFLDISP;
             ,ARFLDISC=this.w_ARFLDISC;
             ,ARCODCLA=this.w_ARCODCLA;
             ,ARSALCOM=this.w_ARSALCOM;
             ,ARDTINVA=this.w_ARDTINVA;
             ,ARDTOBSO=this.w_ARDTOBSO;
             ,ARFLCOM1=this.w_ARFLCOM1;
             ,ARFLESAU=this.w_ARFLESAU;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ARPESNET=this.w_ARPESNET;
             ,ARPESLOR=this.w_ARPESLOR;
             ,ARTIPCO1=this.w_ARTIPCO1;
             ,ARTPCONF=this.w_ARTPCONF;
             ,ARPZCONF=this.w_ARPZCONF;
             ,ARCOCOL1=this.w_ARCOCOL1;
             ,ARDIMLUN=this.w_ARDIMLUN;
             ,ARDIMLAR=this.w_ARDIMLAR;
             ,ARDIMALT=this.w_ARDIMALT;
             ,ARUMVOLU=this.w_ARUMVOLU;
             ,ARUMDIME=this.w_ARUMDIME;
             ,ARDESVOL=this.w_ARDESVOL;
             ,ARFLCONA=this.w_ARFLCONA;
             ,ARPESNE2=this.w_ARPESNE2;
             ,ARPESLO2=this.w_ARPESLO2;
             ,ARTIPCO2=this.w_ARTIPCO2;
             ,ARTPCON2=this.w_ARTPCON2;
             ,ARPZCON2=this.w_ARPZCON2;
             ,ARCOCOL2=this.w_ARCOCOL2;
             ,ARDIMLU2=this.w_ARDIMLU2;
             ,ARDIMLA2=this.w_ARDIMLA2;
             ,ARDIMAL2=this.w_ARDIMAL2;
             ,ARUMVOL2=this.w_ARUMVOL2;
             ,ARUMDIM2=this.w_ARUMDIM2;
             ,ARDESVO2=this.w_ARDESVO2;
             ,ARFLCON2=this.w_ARFLCON2;
             ,ARUTISER=this.w_ARUTISER;
             ,ARPERSER=this.w_ARPERSER;
             ,ARNOMENC=this.w_ARNOMENC;
             ,ARMOLSUP=this.w_ARMOLSUP;
             ,ARUMSUPP=this.w_ARUMSUPP;
             ,ARCODMAR=this.w_ARCODMAR;
             ,ARGRUPRO=this.w_ARGRUPRO;
             ,ARCATSCM=this.w_ARCATSCM;
             ,ARCODRIC=this.w_ARCODRIC;
             ,ARARTPOS=this.w_ARARTPOS;
             ,ARCODREP=this.w_ARCODREP;
             ,ARPUBWEB=this.w_ARPUBWEB;
             ,ARFLPECO=this.w_ARFLPECO;
             ,ARCODGRU=this.w_ARCODGRU;
             ,ARCODSOT=this.w_ARCODSOT;
             ,ARMINVEN=this.w_ARMINVEN;
             ,ARSTACOD=this.w_ARSTACOD;
             ,ARFLLOTT=this.w_ARFLLOTT;
             ,ARFLLMAG=this.w_ARFLLMAG;
             ,ARCLALOT=this.w_ARCLALOT;
             ,ARDISLOT=this.w_ARDISLOT;
             ,ARFLESUL=this.w_ARFLESUL;
             ,ARGESMAT=this.w_ARGESMAT;
             ,ARCLAMAT=this.w_ARCLAMAT;
             ,ARVOCCEN=this.w_ARVOCCEN;
             ,ARVOCRIC=this.w_ARVOCRIC;
             ,ARCODCEN=this.w_ARCODCEN;
             ,ARCODDIS=this.w_ARCODDIS;
             ,ARFLCOMP=this.w_ARFLCOMP;
             ,ARMAGPRE=this.w_ARMAGPRE;
             ,ARTIPPRE=this.w_ARTIPPRE;
             ,ARKITIMB=this.w_ARKITIMB;
             ,ARFLESIM=this.w_ARFLESIM;
             ,ARFLAPCA=this.w_ARFLAPCA;
             ,ARDATINT=this.w_ARDATINT;
             ,ARCLACRI=this.w_ARCLACRI;
             ,ARCHKUCA=this.w_ARCHKUCA;
             ,GESTGUID=this.w_GESTGUID;
             ,ARFLCOMM=this.w_ARFLCOMM;
             ,ARTIPPKR=this.w_ARTIPPKR;
             ,ARTIPIMP=this.w_ARTIPIMP;
             ,ARMAGIMP=this.w_ARMAGIMP;
             ,ARFSRIFE=this.w_ARFSRIFE;
             ,ARCMPCAR=this.w_ARCMPCAR;
             ,ARCONCAR=this.w_ARCONCAR;
             ,ARCODTIP=this.w_ARCODTIP;
             ,ARCODCLF=this.w_ARCODCLF;
             ,ARCODVAL=this.w_ARCODVAL;
             ,ARFLGESC=this.w_ARFLGESC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MLI : Saving
      this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"LICODART";
             )
      this.GSMA_MLI.mReplace()
      * --- GSMA_MRP : Saving
      this.GSMA_MRP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"LICODART";
             )
      this.GSMA_MRP.mReplace()
      * --- GSMA_APR : Saving
      this.GSMA_APR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"PRCODART";
             )
      this.GSMA_APR.mReplace()
      * --- GSMA_MAL : Saving
      this.GSMA_MAL.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"ARCODICE";
             )
      this.GSMA_MAL.mReplace()
      * --- GSAR_MAR : Saving
      this.GSAR_MAR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"MCCODART";
             )
      this.GSAR_MAR.mReplace()
      * --- GSAR_MR2 : Saving
      this.GSAR_MR2.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GESTGUID,"GUID";
             )
      this.GSAR_MR2.mReplace()
      * --- GSMA_ANO : Saving
      this.GSMA_ANO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"ARCODART";
             )
      this.GSMA_ANO.mReplace()
      * --- GSMA_MDA : Saving
      this.GSMA_MDA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"DICODART";
             )
      this.GSMA_MDA.mReplace()
      * --- GSMA_MAC : Saving
      this.GSMA_MAC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"CCCODART";
             )
      this.GSMA_MAC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMA_MLI : Deleting
    this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"LICODART";
           )
    this.GSMA_MLI.mDelete()
    * --- GSMA_MRP : Deleting
    this.GSMA_MRP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"LICODART";
           )
    this.GSMA_MRP.mDelete()
    * --- GSMA_APR : Deleting
    this.GSMA_APR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"PRCODART";
           )
    this.GSMA_APR.mDelete()
    * --- GSMA_MAL : Deleting
    this.GSMA_MAL.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"ARCODICE";
           )
    this.GSMA_MAL.mDelete()
    * --- GSAR_MAR : Deleting
    this.GSAR_MAR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"MCCODART";
           )
    this.GSAR_MAR.mDelete()
    * --- GSAR_MR2 : Deleting
    this.GSAR_MR2.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GESTGUID,"GUID";
           )
    this.GSAR_MR2.mDelete()
    * --- GSMA_ANO : Deleting
    this.GSMA_ANO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"ARCODART";
           )
    this.GSMA_ANO.mDelete()
    * --- GSMA_MDA : Deleting
    this.GSMA_MDA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"DICODART";
           )
    this.GSMA_MDA.mDelete()
    * --- GSMA_MAC : Deleting
    this.GSMA_MAC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"CCCODART";
           )
    this.GSMA_MAC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ART_ICOL_IDX,i_nConn)
      *
      * delete ART_ICOL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_TIPOPE = this.cFunction
        if .o_AUTO<>.w_AUTO
            .w_ARCODART = iif(.cFunction = 'Load' AND .w_AUTO='S', 'AUTO',.w_ARCODART)
        endif
        .DoRTCalc(5,11,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OLDUNI = .w_ARUNMIS1
        endif
        .DoRTCalc(13,14,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OLDUN2 = .w_ARUNMIS2
        endif
        if .o_ARUNMIS2<>.w_ARUNMIS2
            .w_ARMOLTIP = 0
        endif
        .DoRTCalc(17,34,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARPROPRE = iif(g_PROD='S' , IIF(.w_ARTIPART='MP', 'E',  'I' ) , 'E' )
        endif
        .DoRTCalc(36,36,.t.)
        if .o_ARTIPBAR<>.w_ARTIPBAR
            .w_TIPBAR = '0'
        endif
        if .o_ARFLLOTT<>.w_ARFLLOTT.or. .o_ARGESMAT<>.w_ARGESMAT.or. .o_ARDISLOT<>.w_ARDISLOT
            .w_ARFLDISP = IIF((g_PERDIS='S') OR (.w_ARFLLOTT $ 'SC')  OR  .w_ARGESMAT ='S', 'S', 'N')
        endif
        .DoRTCalc(39,43,.t.)
        if .o_ARTIPGES<>.w_ARTIPGES.or. .o_ARTIPART<>.w_ARTIPART.or. .o_ARPROPRE<>.w_ARPROPRE.or. .o_ARSALCOM<>.w_ARSALCOM
            .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),.w_ARFLCOM1,"N")
        endif
        if .o_ARTIPGES<>.w_ARTIPGES.or. .o_ARTIPART<>.w_ARTIPART.or. .o_ARPROPRE<>.w_ARPROPRE.or. .o_ARSALCOM<>.w_ARSALCOM
            .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),.w_ARFLCOM1,"N")
        endif
        if .o_ARTIPGES<>.w_ARTIPGES.or. .o_ARTIPART<>.w_ARTIPART.or. .o_ARSALCOM<>.w_ARSALCOM.or. .o_ARPROPRE<>.w_ARPROPRE
            .w_ARFLCOM1 = iif(g_MODA="S" and .w_ARTIPGES="F" and !empty(.w_ARFLCOM1),iif(.w_ARSALCOM='S',.w_ARFLCOM1,"N"),"N")
        endif
            .w_CODICE = .w_ARCODART
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .DoRTCalc(48,52,.t.)
            .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .DoRTCalc(54,54,.t.)
            .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
            .w_UM1 = .w_ARUNMIS1
            .w_UM2 = .w_ARUNMIS2
        .DoRTCalc(60,62,.t.)
        if .o_ARTIPCO1<>.w_ARTIPCO1
          .link_2_8('Full')
        endif
        .DoRTCalc(64,64,.t.)
        if .o_ARTPCONF<>.w_ARTPCONF
            .w_ARPZCONF = 0
        endif
        .DoRTCalc(66,69,.t.)
            .w_VOLUME = cp_ROUND(.w_ARDIMLUN*.w_ARDIMLAR*.w_ARDIMALT,3)
        .DoRTCalc(71,71,.t.)
        if .o_VOLUME<>.w_VOLUME
            .w_ARUMDIME = SPACE(3)
          .link_2_17('Full')
        endif
        .DoRTCalc(73,77,.t.)
        if .o_ARTIPCO2<>.w_ARTIPCO2
          .link_2_23('Full')
        endif
        .DoRTCalc(79,79,.t.)
        if .o_ARTPCON2<>.w_ARTPCON2
            .w_ARPZCON2 = 0
        endif
        .DoRTCalc(81,84,.t.)
            .w_VOLUM2 = cp_ROUND(.w_ARDIMLU2*.w_ARDIMLA2*.w_ARDIMAL2,3)
        .DoRTCalc(86,86,.t.)
        if .o_VOLUM2<>.w_VOLUM2
            .w_ARUMDIM2 = SPACE(3)
          .link_2_32('Full')
        endif
        .DoRTCalc(88,91,.t.)
        if .o_ARUTISER<>.w_ARUTISER
            .w_ARNOMENC = ''
          .link_2_37('Full')
        endif
        if .o_ARUMSUPP<>.w_ARUMSUPP
            .w_ARMOLSUP = 0
        endif
        if .o_ARUTISER<>.w_ARUTISER
            .w_ARDATINT = IIF(.w_ARUTISER='N', 'F', 'N')
        endif
          .link_2_40('Full')
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
            .w_TIPPRO = 'F'
        .DoRTCalc(99,111,.t.)
        if .o_ARARTPOS<>.w_ARARTPOS
            .w_ARCODREP = IIF(.w_ARARTPOS<>'S',SPACE(3),.w_ARCODREP)
          .link_3_26('Full')
        endif
        .DoRTCalc(113,114,.t.)
          .link_4_1('Full')
        .DoRTCalc(116,116,.t.)
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
        .DoRTCalc(119,121,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_LICODART = .w_ARCODART
        endif
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .DoRTCalc(123,135,.t.)
        if .o_ARCODREP<>.w_ARCODREP
          .link_3_42('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .DoRTCalc(137,143,.t.)
        if .o_ARFLLOTT<>.w_ARFLLOTT
            .w_ARFLLMAG = IIF(.w_ARFLLOTT='C','S',' ')
        endif
        if .o_ARFLLOTT<>.w_ARFLLOTT
            .w_ARCLALOT = Space(5)
          .link_5_3('Full')
        endif
        if .o_ARFLLOTT<>.w_ARFLLOTT
            .w_ARDISLOT = 'S'
        endif
        .DoRTCalc(147,148,.t.)
        if .o_ARGESMAT<>.w_ARGESMAT
            .w_ARCLAMAT = Space(5)
          .link_5_7('Full')
        endif
        .DoRTCalc(150,152,.t.)
        if .o_ARKITIMB<>.w_ARKITIMB
            .w_ARCODDIS = IIF(.w_ARKITIMB='N',.w_ARCODDIS, Space(20))
          .link_5_11('Full')
        endif
        if .o_ARCODDIS<>.w_ARCODDIS
            .w_ARFLCOMP = ' '
        endif
        .DoRTCalc(155,164,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OLDFLAG = .w_ARFLLOTT
        endif
        .DoRTCalc(166,169,.t.)
        if .o_ARCODDIS<>.w_ARCODDIS.or. .o_ARKITIMB<>.w_ARKITIMB
            .w_ARFLESIM = IIF(.w_ARKITIMB<>'N' Or .w_DISKIT<>'I' Or Empty(.w_ARCODDIS),' ',.w_ARFLESIM)
        endif
        .DoRTCalc(171,172,.t.)
            .w_NDATKEY = .w_ARDTOBSO
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        if .o_ARDATINT<>.w_ARDATINT
          .Calculate_YPGSBLGPNE()
        endif
        if .o_ARDISLOT<>.w_ARDISLOT.or. .o_ARFLLOTT<>.w_ARFLLOTT.or. .o_ARGESMAT<>.w_ARGESMAT
          .Calculate_DGHGZSHNDN()
        endif
        .DoRTCalc(174,183,.t.)
        if .o_ARTIPGES<>.w_ARTIPGES.or. .o_ARTIPART<>.w_ARTIPART.or. .o_ARFLCOM1<>.w_ARFLCOM1
            .w_ARFLCOMM = iif(.w_ARTIPGES="F" and !empty(.w_ARFLCOM1),iif(.w_ARFLCOM1="C","S",.w_ARFLCOM1),"N")
        endif
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARTIPPKR = iif(.cFunction='Load', .w_ARTIPART, .w_ARTIPPKR)
        endif
        .DoRTCalc(186,190,.t.)
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
        if  .o_GEST<>.w_GEST
          .WriteTo_GSAR_MR2()
        endif
        .DoRTCalc(193,197,.t.)
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
          .link_7_7('Full')
        .DoRTCalc(201,202,.t.)
        if .o_ARCODART<>.w_ARCODART.or. .o_ARTIPART<>.w_ARTIPART
            .w_ARCONCAR = 'N'
        endif
        .DoRTCalc(204,207,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        endif
            .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_86.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
    endwith
  return

  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola Auto
          GSAR_BSS(this;
             )
    endwith
  endproc
  proc Calculate_YPGSBLGPNE()
    with this
          * --- Sbianca campi INTRA su cambio combo
          .w_ARNOMENC = IIF(.w_ARDATINT='N',SPACE(8), .w_ARNOMENC)
          .w_DESNOM = IIF(.w_ARDATINT='N',SPACE(35), .w_DESNOM)
          .w_ARUMSUPP = IIF(.w_ARDATINT='N',SPACE(3), .w_ARUMSUPP)
          .w_ARMOLSUP = IIF(.w_ARDATINT='N',0, .w_ARMOLSUP)
    endwith
  endproc
  proc Calculate_DGHGZSHNDN()
    with this
          * --- Messaggio al cambio della disponibilit�
          gsma_bpd(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_YBHCACTYYL()
    with this
          * --- Tipo articolo (PK I.revolution)
          .w_ARTIPPKR = evl(.w_ARTIPPKR, .w_ARTIPART)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oARCODART_1_4.enabled = this.oPgFrm.Page1.oPag.oARCODART_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAROPERAT_1_14.enabled = this.oPgFrm.Page1.oPag.oAROPERAT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oARMOLTIP_1_16.enabled = this.oPgFrm.Page1.oPag.oARMOLTIP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oARFLDISP_1_38.enabled = this.oPgFrm.Page1.oPag.oARFLDISP_1_38.mCond()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.enabled = this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.mCond()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.enabled = this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.mCond()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.enabled = this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.mCond()
    this.oPgFrm.Page2.oPag.oARTPCONF_2_8.enabled = this.oPgFrm.Page2.oPag.oARTPCONF_2_8.mCond()
    this.oPgFrm.Page2.oPag.oARPZCONF_2_10.enabled = this.oPgFrm.Page2.oPag.oARPZCONF_2_10.mCond()
    this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.enabled = this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.mCond()
    this.oPgFrm.Page2.oPag.oARUMDIME_2_17.enabled = this.oPgFrm.Page2.oPag.oARUMDIME_2_17.mCond()
    this.oPgFrm.Page2.oPag.oARPESNE2_2_20.enabled = this.oPgFrm.Page2.oPag.oARPESNE2_2_20.mCond()
    this.oPgFrm.Page2.oPag.oARPESLO2_2_21.enabled = this.oPgFrm.Page2.oPag.oARPESLO2_2_21.mCond()
    this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.enabled = this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.mCond()
    this.oPgFrm.Page2.oPag.oARTPCON2_2_23.enabled = this.oPgFrm.Page2.oPag.oARTPCON2_2_23.mCond()
    this.oPgFrm.Page2.oPag.oARPZCON2_2_25.enabled = this.oPgFrm.Page2.oPag.oARPZCON2_2_25.mCond()
    this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.enabled = this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.mCond()
    this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.enabled = this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.mCond()
    this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.enabled = this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.mCond()
    this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.enabled = this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.mCond()
    this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.enabled = this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.mCond()
    this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.enabled = this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.mCond()
    this.oPgFrm.Page2.oPag.oARDESVO2_2_33.enabled = this.oPgFrm.Page2.oPag.oARDESVO2_2_33.mCond()
    this.oPgFrm.Page2.oPag.oARUTISER_2_35.enabled = this.oPgFrm.Page2.oPag.oARUTISER_2_35.mCond()
    this.oPgFrm.Page2.oPag.oARNOMENC_2_37.enabled = this.oPgFrm.Page2.oPag.oARNOMENC_2_37.mCond()
    this.oPgFrm.Page2.oPag.oARMOLSUP_2_38.enabled = this.oPgFrm.Page2.oPag.oARMOLSUP_2_38.mCond()
    this.oPgFrm.Page2.oPag.oARDATINT_2_39.enabled = this.oPgFrm.Page2.oPag.oARDATINT_2_39.mCond()
    this.oPgFrm.Page3.oPag.oARARTPOS_3_25.enabled = this.oPgFrm.Page3.oPag.oARARTPOS_3_25.mCond()
    this.oPgFrm.Page3.oPag.oARCODREP_3_26.enabled = this.oPgFrm.Page3.oPag.oARCODREP_3_26.mCond()
    this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.enabled = this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.mCond()
    this.oPgFrm.Page3.oPag.oARCODGRU_3_31.enabled = this.oPgFrm.Page3.oPag.oARCODGRU_3_31.mCond()
    this.oPgFrm.Page3.oPag.oARCODSOT_3_32.enabled = this.oPgFrm.Page3.oPag.oARCODSOT_3_32.mCond()
    this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.enabled = this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.mCond()
    this.oPgFrm.Page5.oPag.oARFLLOTT_5_1.enabled = this.oPgFrm.Page5.oPag.oARFLLOTT_5_1.mCond()
    this.oPgFrm.Page5.oPag.oARFLLMAG_5_2.enabled = this.oPgFrm.Page5.oPag.oARFLLMAG_5_2.mCond()
    this.oPgFrm.Page5.oPag.oARCLALOT_5_3.enabled = this.oPgFrm.Page5.oPag.oARCLALOT_5_3.mCond()
    this.oPgFrm.Page5.oPag.oARDISLOT_5_4.enabled = this.oPgFrm.Page5.oPag.oARDISLOT_5_4.mCond()
    this.oPgFrm.Page5.oPag.oARFLESUL_5_5.enabled = this.oPgFrm.Page5.oPag.oARFLESUL_5_5.mCond()
    this.oPgFrm.Page5.oPag.oARGESMAT_5_6.enabled = this.oPgFrm.Page5.oPag.oARGESMAT_5_6.mCond()
    this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.enabled = this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.mCond()
    this.oPgFrm.Page5.oPag.oARVOCCEN_5_8.enabled = this.oPgFrm.Page5.oPag.oARVOCCEN_5_8.mCond()
    this.oPgFrm.Page5.oPag.oARVOCRIC_5_9.enabled = this.oPgFrm.Page5.oPag.oARVOCRIC_5_9.mCond()
    this.oPgFrm.Page5.oPag.oARCODDIS_5_11.enabled = this.oPgFrm.Page5.oPag.oARCODDIS_5_11.mCond()
    this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.enabled = this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.mCond()
    this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.enabled = this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.mCond()
    this.oPgFrm.Page5.oPag.oARKITIMB_5_42.enabled = this.oPgFrm.Page5.oPag.oARKITIMB_5_42.mCond()
    this.oPgFrm.Page5.oPag.oARFLESIM_5_44.enabled = this.oPgFrm.Page5.oPag.oARFLESIM_5_44.mCond()
    this.oPgFrm.Page5.oPag.oARCLACRI_5_45.enabled = this.oPgFrm.Page5.oPag.oARCLACRI_5_45.mCond()
    this.oPgFrm.Page5.oPag.oARCMPCAR_5_58.enabled = this.oPgFrm.Page5.oPag.oARCMPCAR_5_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_15.enabled = this.oPgFrm.Page3.oPag.oBtn_3_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_76.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_76.mCond()
    this.oPgFrm.Page3.oPag.oLinkPC_3_48.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page5.oPag.oARMAGIMP_5_53
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(6).enabled=not(g_GPFA<>"S")
    this.oPgFrm.Pages(7).enabled=not(g_PRFA<>"S")
    local i_show5
    i_show5=not(g_GPFA<>"S")
    this.oPgFrm.Pages(6).enabled=i_show5 and not(g_GPFA<>"S")
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Premi"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    local i_show6
    i_show6=not(g_PRFA<>"S")
    this.oPgFrm.Pages(7).enabled=i_show6 and not(g_PRFA<>"S")
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Distinte alternative"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    this.oPgFrm.Page1.oPag.oARTIPART_1_6.visible=!this.oPgFrm.Page1.oPag.oARTIPART_1_6.mHide()
    this.oPgFrm.Page1.oPag.oARTIPART_1_7.visible=!this.oPgFrm.Page1.oPag.oARTIPART_1_7.mHide()
    this.oPgFrm.Page1.oPag.oARTIPBAR_1_36.visible=!this.oPgFrm.Page1.oPag.oARTIPBAR_1_36.mHide()
    this.oPgFrm.Page1.oPag.oTIPBAR_1_37.visible=!this.oPgFrm.Page1.oPag.oTIPBAR_1_37.mHide()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.visible=!this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.mHide()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.visible=!this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.mHide()
    this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.visible=!this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page2.oPag.oUM2_2_4.visible=!this.oPgFrm.Page2.oPag.oUM2_2_4.mHide()
    this.oPgFrm.Page2.oPag.oARTPCONF_2_8.visible=!this.oPgFrm.Page2.oPag.oARTPCONF_2_8.mHide()
    this.oPgFrm.Page2.oPag.oDESTCF_2_9.visible=!this.oPgFrm.Page2.oPag.oDESTCF_2_9.mHide()
    this.oPgFrm.Page2.oPag.oARPZCONF_2_10.visible=!this.oPgFrm.Page2.oPag.oARPZCONF_2_10.mHide()
    this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.visible=!this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.mHide()
    this.oPgFrm.Page2.oPag.oARPESNE2_2_20.visible=!this.oPgFrm.Page2.oPag.oARPESNE2_2_20.mHide()
    this.oPgFrm.Page2.oPag.oARPESLO2_2_21.visible=!this.oPgFrm.Page2.oPag.oARPESLO2_2_21.mHide()
    this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.visible=!this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.mHide()
    this.oPgFrm.Page2.oPag.oARTPCON2_2_23.visible=!this.oPgFrm.Page2.oPag.oARTPCON2_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESTCF2_2_24.visible=!this.oPgFrm.Page2.oPag.oDESTCF2_2_24.mHide()
    this.oPgFrm.Page2.oPag.oARPZCON2_2_25.visible=!this.oPgFrm.Page2.oPag.oARPZCON2_2_25.mHide()
    this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.visible=!this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.mHide()
    this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.visible=!this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.mHide()
    this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.visible=!this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.mHide()
    this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.visible=!this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.mHide()
    this.oPgFrm.Page2.oPag.oVOLUM2_2_30.visible=!this.oPgFrm.Page2.oPag.oVOLUM2_2_30.mHide()
    this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.visible=!this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.mHide()
    this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.visible=!this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.mHide()
    this.oPgFrm.Page2.oPag.oARDESVO2_2_33.visible=!this.oPgFrm.Page2.oPag.oARDESVO2_2_33.mHide()
    this.oPgFrm.Page2.oPag.oARFLCON2_2_34.visible=!this.oPgFrm.Page2.oPag.oARFLCON2_2_34.mHide()
    this.oPgFrm.Page2.oPag.oARPERSER_2_36.visible=!this.oPgFrm.Page2.oPag.oARPERSER_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_56.visible=!this.oPgFrm.Page2.oPag.oStr_2_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_58.visible=!this.oPgFrm.Page2.oPag.oStr_2_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_64.visible=!this.oPgFrm.Page2.oPag.oStr_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_66.visible=!this.oPgFrm.Page2.oPag.oStr_2_66.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_68.visible=!this.oPgFrm.Page2.oPag.oStr_2_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_6.visible=!this.oPgFrm.Page3.oPag.oBtn_3_6.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_15.visible=!this.oPgFrm.Page3.oPag.oBtn_3_15.mHide()
    this.oPgFrm.Page3.oPag.oARARTPOS_3_25.visible=!this.oPgFrm.Page3.oPag.oARARTPOS_3_25.mHide()
    this.oPgFrm.Page3.oPag.oARCODREP_3_26.visible=!this.oPgFrm.Page3.oPag.oARCODREP_3_26.mHide()
    this.oPgFrm.Page3.oPag.oDESREP_3_27.visible=!this.oPgFrm.Page3.oPag.oDESREP_3_27.mHide()
    this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.visible=!this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.mHide()
    this.oPgFrm.Page3.oPag.oARCODGRU_3_31.visible=!this.oPgFrm.Page3.oPag.oARCODGRU_3_31.mHide()
    this.oPgFrm.Page3.oPag.oARCODSOT_3_32.visible=!this.oPgFrm.Page3.oPag.oARCODSOT_3_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_71.visible=!this.oPgFrm.Page2.oPag.oStr_2_71.mHide()
    this.oPgFrm.Page2.oPag.oDESCOL2_2_73.visible=!this.oPgFrm.Page2.oPag.oDESCOL2_2_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_36.visible=!this.oPgFrm.Page3.oPag.oStr_3_36.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_38.visible=!this.oPgFrm.Page3.oPag.oStr_3_38.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_39.visible=!this.oPgFrm.Page3.oPag.oStr_3_39.mHide()
    this.oPgFrm.Page3.oPag.oDESSOT_3_40.visible=!this.oPgFrm.Page3.oPag.oDESSOT_3_40.mHide()
    this.oPgFrm.Page3.oPag.oDESTIT_3_41.visible=!this.oPgFrm.Page3.oPag.oDESTIT_3_41.mHide()
    this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.visible=!this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.mHide()
    this.oPgFrm.Page3.oPag.oARPUBWEB_3_45.visible=!this.oPgFrm.Page3.oPag.oARPUBWEB_3_45.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_46.visible=!this.oPgFrm.Page3.oPag.oStr_3_46.mHide()
    this.oPgFrm.Page5.oPag.oARCLALOT_5_3.visible=!this.oPgFrm.Page5.oPag.oARCLALOT_5_3.mHide()
    this.oPgFrm.Page5.oPag.oARDISLOT_5_4.visible=!this.oPgFrm.Page5.oPag.oARDISLOT_5_4.mHide()
    this.oPgFrm.Page5.oPag.oARGESMAT_5_6.visible=!this.oPgFrm.Page5.oPag.oARGESMAT_5_6.mHide()
    this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.visible=!this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.mHide()
    this.oPgFrm.Page5.oPag.oARCODDIS_5_11.visible=!this.oPgFrm.Page5.oPag.oARCODDIS_5_11.mHide()
    this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.visible=!this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.mHide()
    this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.visible=!this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_15.visible=!this.oPgFrm.Page5.oPag.oStr_5_15.mHide()
    this.oPgFrm.Page5.oPag.oCMTDESCRI_5_16.visible=!this.oPgFrm.Page5.oPag.oCMTDESCRI_5_16.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_17.visible=!this.oPgFrm.Page5.oPag.oStr_5_17.mHide()
    this.oPgFrm.Page5.oPag.oCLTDESCRI_5_18.visible=!this.oPgFrm.Page5.oPag.oCLTDESCRI_5_18.mHide()
    this.oPgFrm.Page5.oPag.oDESDIS_5_26.visible=!this.oPgFrm.Page5.oPag.oDESDIS_5_26.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_27.visible=!this.oPgFrm.Page5.oPag.oStr_5_27.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_30.visible=!this.oPgFrm.Page5.oPag.oStr_5_30.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_32.visible=!this.oPgFrm.Page5.oPag.oStr_5_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_103.visible=!this.oPgFrm.Page1.oPag.oBtn_1_103.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_104.visible=!this.oPgFrm.Page1.oPag.oBtn_1_104.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_105.visible=!this.oPgFrm.Page1.oPag.oBtn_1_105.mHide()
    this.oPgFrm.Page5.oPag.oARKITIMB_5_42.visible=!this.oPgFrm.Page5.oPag.oARKITIMB_5_42.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_43.visible=!this.oPgFrm.Page5.oPag.oStr_5_43.mHide()
    this.oPgFrm.Page5.oPag.oARFLESIM_5_44.visible=!this.oPgFrm.Page5.oPag.oARFLESIM_5_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_106.visible=!this.oPgFrm.Page1.oPag.oBtn_1_106.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_74.visible=!this.oPgFrm.Page2.oPag.oStr_2_74.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_75.visible=!this.oPgFrm.Page2.oPag.oStr_2_75.mHide()
    this.oPgFrm.Page3.oPag.oARFLAPCA_3_47.visible=!this.oPgFrm.Page3.oPag.oARFLAPCA_3_47.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_3_48.visible=!this.oPgFrm.Page3.oPag.oLinkPC_3_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_110.visible=!this.oPgFrm.Page1.oPag.oStr_1_110.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_81.visible=!this.oPgFrm.Page2.oPag.oStr_2_81.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_82.visible=!this.oPgFrm.Page2.oPag.oStr_2_82.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_86.visible=!this.oPgFrm.Page2.oPag.oStr_2_86.mHide()
    this.oPgFrm.Page5.oPag.oLinkPC_5_57.visible=!this.oPgFrm.Page5.oPag.oLinkPC_5_57.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_86.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_90.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_97.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_113.Event(cEvent)
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YBHCACTYYL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('AR');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','AR';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUNMIS1
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS1))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS1)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS1) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS1_1_11'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS1)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS1 = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.UMCODICE as UMCODICE111"+ ",link_1_11.UMFLFRAZ as UMFLFRAZ111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on ART_ICOL.ARUNMIS1=link_1_11.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and ART_ICOL.ARUNMIS1=link_1_11.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUNMIS2
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS2))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS2_1_13'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS2)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS2 = NVL(_Link_.UMCODICE,space(3))
      this.w_F2FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS2 = space(3)
      endif
      this.w_F2FRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUNMIS2<>.w_ARUNMIS1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ARUNMIS2 = space(3)
        this.w_F2FRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.UMCODICE as UMCODICE113"+ ",link_1_13.UMFLFRAZ as UMFLFRAZ113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on ART_ICOL.ARUNMIS2=link_1_13.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and ART_ICOL.ARUNMIS2=link_1_13.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODIVA
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ARCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ARCODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_ARCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_ARCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oARCODIVA_1_23'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ARCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ARCODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_ARCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_ARCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PERIVA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.IVCODIVA as IVCODIVA123"+ ",link_1_23.IVDESIVA as IVDESIVA123"+ ",link_1_23.IVDTOBSO as IVDTOBSO123"+ ",link_1_23.IVPERIVA as IVPERIVA123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on ART_ICOL.ARCODIVA=link_1_23.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODIVA=link_1_23.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTIPOPE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_ARTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ARCATOPE;
                     ,'TICODICE',trim(this.w_ARTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oARTIPOPE_1_24'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ARTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ARCATOPE;
                       ,'TICODICE',this.w_ARTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_POPOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_POPOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_POPOBSO) OR .w_POPOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice obsoleto oppure inesistente")
        endif
        this.w_ARTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_POPOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCATCON
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_ARCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_ARCATCON))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATCON)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oARCATCON_1_26'),i_cWhere,'GSAR_AC1',"Categorie contabili articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_ARCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_ARCATCON)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATCON = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCAT = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATCON = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.C1CODICE as C1CODICE126"+ ",link_1_26.C1DESCRI as C1DESCRI126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on ART_ICOL.ARCATCON=link_1_26.C1CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATCON=link_1_26.C1CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODFAM
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_ARCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_ARCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oARCODFAM_1_28'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_ARCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_ARCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.FACODICE as FACODICE128"+ ","+cp_TransLinkFldName('link_1_28.FADESCRI')+" as FADESCRI128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on ART_ICOL.ARCODFAM=link_1_28.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODFAM=link_1_28.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARGRUMER
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_ARGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_ARGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_ARGRUMER)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_ARGRUMER))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_ARGRUMER)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oARGRUMER_1_30'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_ARGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_ARGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.GMCODICE as GMCODICE130"+ ","+cp_TransLinkFldName('link_1_30.GMDESCRI')+" as GMDESCRI130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on ART_ICOL.ARGRUMER=link_1_30.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and ART_ICOL.ARGRUMER=link_1_30.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCATOMO
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_ARCATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_ARCATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_ARCATOMO)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_ARCATOMO))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_ARCATOMO)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oARCATOMO_1_32'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_ARCATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_ARCATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_DESOMO = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATOMO = space(5)
      endif
      this.w_DESOMO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.OMCODICE as OMCODICE132"+ ","+cp_TransLinkFldName('link_1_32.OMDESCRI')+" as OMDESCRI132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on ART_ICOL.ARCATOMO=link_1_32.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATOMO=link_1_32.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODCLA
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_ARCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_ARCODCLA))
          select TRCODCLA,TRDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODCLA)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oARCODCLA_1_40'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_ARCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_ARCODCLA)
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODCLA = NVL(_Link_.TRCODCLA,space(3))
      this.w_DESCLA = NVL(_Link_.TRDESCLA,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODCLA = space(3)
      endif
      this.w_DESCLA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_RIGD_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.TRCODCLA as TRCODCLA140"+ ",link_1_40.TRDESCLA as TRDESCLA140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on ART_ICOL.ARCODCLA=link_1_40.TRCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODCLA=link_1_40.TRCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTIPCO1
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPCO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_ARTIPCO1)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_ARTIPCO1))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTIPCO1)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTIPCO1) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oARTIPCO1_2_7'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPCO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ARTIPCO1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPCO1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPCO1 = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPCO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_COLL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.TCCODICE as TCCODICE207"+ ",link_2_7.TCDESCRI as TCDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on ART_ICOL.ARTIPCO1=link_2_7.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and ART_ICOL.ARTIPCO1=link_2_7.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTPCONF
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_COLL_IDX,3]
    i_lTable = "CON_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2], .t., this.CON_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTPCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'CON_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODCON like "+cp_ToStrODBC(trim(this.w_ARTPCONF)+"%");
                   +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO1);

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE,TCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',this.w_ARTIPCO1;
                     ,'TCCODCON',trim(this.w_ARTPCONF))
          select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE,TCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTPCONF)==trim(_Link_.TCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTPCONF) and !this.bDontReportError
            deferred_cp_zoom('CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(oSource.parent,'oARTPCONF_2_8'),i_cWhere,'GSAR_MTO',"Confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTIPCO1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1);
                       ,'TCCODCON',oSource.xKey(2))
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTPCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(this.w_ARTPCONF);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ARTIPCO1;
                       ,'TCCODCON',this.w_ARTPCONF)
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTPCONF = NVL(_Link_.TCCODCON,space(3))
      this.w_DESTCF = NVL(_Link_.TCDESCON,space(30))
      this.w_ARCOCOL1 = NVL(_Link_.TCQTACON,0)
    else
      if i_cCtrl<>'Load'
        this.w_ARTPCONF = space(3)
      endif
      this.w_DESTCF = space(30)
      this.w_ARCOCOL1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODCON,1)
      cp_ShowWarn(i_cKey,this.CON_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTPCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_COLL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.TCCODCON as TCCODCON208"+ ",link_2_8.TCDESCON as TCDESCON208"+ ",link_2_8.TCQTACON as TCQTACON208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on ART_ICOL.ARTPCONF=link_2_8.TCCODCON"+" and ART_ICOL.ARTIPCO1=link_2_8.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and ART_ICOL.ARTPCONF=link_2_8.TCCODCON(+)"'+'+" and ART_ICOL.ARTIPCO1=link_2_8.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUMVOLU
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMVOLU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUMVOLU)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUMVOLU))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUMVOLU)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUMVOLU) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUMVOLU_2_16'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMVOLU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMVOLU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMVOLU)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMVOLU = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMVOLU = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMVOLU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUMDIME
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMDIME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUMDIME)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUMDIME))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUMDIME)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUMDIME) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUMDIME_2_17'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMDIME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMDIME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMDIME)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMDIME = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMDIME = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMDIME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTIPCO2
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPCO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_ARTIPCO2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_ARTIPCO2))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTIPCO2)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTIPCO2) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oARTIPCO2_2_22'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPCO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ARTIPCO2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPCO2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL2 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPCO2 = space(5)
      endif
      this.w_DESCOL2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPCO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_COLL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_22.TCCODICE as TCCODICE222"+ ",link_2_22.TCDESCRI as TCDESCRI222"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_22 on ART_ICOL.ARTIPCO2=link_2_22.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_22"
          i_cKey=i_cKey+'+" and ART_ICOL.ARTIPCO2=link_2_22.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTPCON2
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_COLL_IDX,3]
    i_lTable = "CON_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2], .t., this.CON_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTPCON2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'CON_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODCON like "+cp_ToStrODBC(trim(this.w_ARTPCON2)+"%");
                   +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO2);

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE,TCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',this.w_ARTIPCO2;
                     ,'TCCODCON',trim(this.w_ARTPCON2))
          select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE,TCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTPCON2)==trim(_Link_.TCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTPCON2) and !this.bDontReportError
            deferred_cp_zoom('CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(oSource.parent,'oARTPCON2_2_23'),i_cWhere,'GSAR_MTO',"Confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTIPCO2<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO2);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1);
                       ,'TCCODCON',oSource.xKey(2))
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTPCON2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(this.w_ARTPCON2);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_ARTIPCO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ARTIPCO2;
                       ,'TCCODCON',this.w_ARTPCON2)
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTPCON2 = NVL(_Link_.TCCODCON,space(3))
      this.w_DESTCF2 = NVL(_Link_.TCDESCON,space(30))
      this.w_ARCOCOL2 = NVL(_Link_.TCQTACON,0)
    else
      if i_cCtrl<>'Load'
        this.w_ARTPCON2 = space(3)
      endif
      this.w_DESTCF2 = space(30)
      this.w_ARCOCOL2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODCON,1)
      cp_ShowWarn(i_cKey,this.CON_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTPCON2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_COLL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_23.TCCODCON as TCCODCON223"+ ",link_2_23.TCDESCON as TCDESCON223"+ ",link_2_23.TCQTACON as TCQTACON223"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_23 on ART_ICOL.ARTPCON2=link_2_23.TCCODCON"+" and ART_ICOL.ARTIPCO2=link_2_23.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_23"
          i_cKey=i_cKey+'+" and ART_ICOL.ARTPCON2=link_2_23.TCCODCON(+)"'+'+" and ART_ICOL.ARTIPCO2=link_2_23.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUMVOL2
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMVOL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUMVOL2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUMVOL2))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUMVOL2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUMVOL2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUMVOL2_2_31'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMVOL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMVOL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMVOL2)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMVOL2 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMVOL2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMVOL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUMDIM2
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMDIM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUMDIM2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUMDIM2))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUMDIM2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUMDIM2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUMDIM2_2_32'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMDIM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMDIM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMDIM2)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMDIM2 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMDIM2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMDIM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARNOMENC
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARNOMENC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_ARNOMENC)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_ARNOMENC))
          select NMCODICE,NMDESCRI,NMUNISUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARNOMENC)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARNOMENC) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oARNOMENC_2_37'),i_cWhere,'GSAR_ANM',"",'GSMA_AZN.NOMENCLA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARNOMENC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_ARNOMENC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_ARNOMENC)
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARNOMENC = NVL(_Link_.NMCODICE,space(8))
      this.w_DESNOM = NVL(_Link_.NMDESCRI,space(35))
      this.w_ARUMSUPP = NVL(_Link_.NMUNISUP,space(3))
      this.w_UMSUPP = NVL(_Link_.NMUNISUP,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARNOMENC = space(8)
      endif
      this.w_DESNOM = space(35)
      this.w_ARUMSUPP = space(3)
      this.w_UMSUPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUTISER='N' OR LEN(ALLTRIM(.w_ARNOMENC))=6 OR LEN(ALLTRIM(.w_ARNOMENC))=5
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o incongruente")
        endif
        this.w_ARNOMENC = space(8)
        this.w_DESNOM = space(35)
        this.w_ARUMSUPP = space(3)
        this.w_UMSUPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARNOMENC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOMENCLA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_37.NMCODICE as NMCODICE237"+ ",link_2_37.NMDESCRI as NMDESCRI237"+ ",link_2_37.NMUNISUP as NMUNISUP237"+ ",link_2_37.NMUNISUP as NMUNISUP237"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_37 on ART_ICOL.ARNOMENC=link_2_37.NMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_37"
          i_cKey=i_cKey+'+" and ART_ICOL.ARNOMENC=link_2_37.NMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUMSUPP
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMSUPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMSUPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMSUPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMSUPP)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMSUPP = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMSUPP = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMSUPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODMAR
  func Link_3_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_ARCODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_ARCODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oARCODMAR_3_4'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_ARCODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_ARCODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_4.MACODICE as MACODICE304"+ ",link_3_4.MADESCRI as MADESCRI304"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_4 on ART_ICOL.ARCODMAR=link_3_4.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_4"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODMAR=link_3_4.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARGRUPRO
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARGRUPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ARGRUPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ARGRUPRO))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARGRUPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARGRUPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oARGRUPRO_3_7'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSMA_AAR.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARGRUPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ARGRUPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ARGRUPRO)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARGRUPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_DESGPP = NVL(_Link_.GPDESCRI,space(35))
      this.w_FLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARGRUPRO = space(5)
      endif
      this.w_DESGPP = space(35)
      this.w_FLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRO $ ' A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo articolo")
        endif
        this.w_ARGRUPRO = space(5)
        this.w_DESGPP = space(35)
        this.w_FLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARGRUPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_7.GPCODICE as GPCODICE307"+ ",link_3_7.GPDESCRI as GPDESCRI307"+ ",link_3_7.GPTIPPRO as GPTIPPRO307"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_7 on ART_ICOL.ARGRUPRO=link_3_7.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_7"
          i_cKey=i_cKey+'+" and ART_ICOL.ARGRUPRO=link_3_7.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCATSCM
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_ARCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_ARCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oARCATSCM_3_10'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSMA_AAR.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_ARCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_ARCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo articolo")
        endif
        this.w_ARCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_10.CSCODICE as CSCODICE310"+ ",link_3_10.CSDESCRI as CSDESCRI310"+ ",link_3_10.CSTIPCAT as CSTIPCAT310"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_10 on ART_ICOL.ARCATSCM=link_3_10.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_10"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATSCM=link_3_10.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODRIC
  func Link_3_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_ARCODRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI,CRPERCEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_ARCODRIC))
          select CRCODICE,CRDESCRI,CRPERCEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODRIC)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODRIC) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oARCODRIC_3_18'),i_cWhere,'GSAR_ACR',"Classi di ricarico dei listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI,CRPERCEN";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI,CRPERCEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI,CRPERCEN";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_ARCODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_ARCODRIC)
            select CRCODICE,CRDESCRI,CRPERCEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODRIC = NVL(_Link_.CRCODICE,space(5))
      this.w_DESCLR = NVL(_Link_.CRDESCRI,space(35))
      this.w_PERCLR = NVL(_Link_.CRPERCEN,0)
    else
      if i_cCtrl<>'Load'
        this.w_ARCODRIC = space(5)
      endif
      this.w_DESCLR = space(35)
      this.w_PERCLR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_RICA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_18.CRCODICE as CRCODICE318"+ ",link_3_18.CRDESCRI as CRDESCRI318"+ ",link_3_18.CRPERCEN as CRPERCEN318"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_18 on ART_ICOL.ARCODRIC=link_3_18.CRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_18"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODRIC=link_3_18.CRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODREP
  func Link_3_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_ARCODREP)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_ARCODREP))
          select RECODREP,REDESREP,RECODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODREP)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODREP) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oARCODREP_3_26'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_ARCODREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_ARCODREP)
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODREP = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP = NVL(_Link_.REDESREP,space(40))
      this.w_IVAREP = NVL(_Link_.RECODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODREP = space(3)
      endif
      this.w_DESREP = space(40)
      this.w_IVAREP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REP_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_26.RECODREP as RECODREP326"+ ",link_3_26.REDESREP as REDESREP326"+ ",link_3_26.RECODIVA as RECODIVA326"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_26 on ART_ICOL.ARCODREP=link_3_26.RECODREP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_26"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODREP=link_3_26.RECODREP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPRO
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPGESWIP";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPRO)
            select PPCODICE,PPGESWIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPRO = NVL(_Link_.PPCODICE,space(10))
      this.w_GESWIP = NVL(_Link_.PPGESWIP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPRO = space(10)
      endif
      this.w_GESWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODGRU
  func Link_3_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_ARCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_ARCODGRU))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TSDESCRI like "+cp_ToStrODBC(trim(this.w_ARCODGRU)+"%");

            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TSDESCRI like "+cp_ToStr(trim(this.w_ARCODGRU)+"%");

            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oARCODGRU_3_31'),i_cWhere,'GSOF_ATS',"Codici gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_ARCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_ARCODGRU)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODGRU = NVL(_Link_.TSCODICE,space(5))
      this.w_DESTIT = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODGRU = space(5)
      endif
      this.w_DESTIT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIT_SEZI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_31.TSCODICE as TSCODICE331"+ ",link_3_31.TSDESCRI as TSDESCRI331"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_31 on ART_ICOL.ARCODGRU=link_3_31.TSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_31"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODGRU=link_3_31.TSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODSOT
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_ARCODGRU;
                     ,'SGCODSOT',trim(this.w_ARCODSOT))
          select SGCODGRU,SGCODSOT,SGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" SGDESCRI like "+cp_ToStrODBC(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);

            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" SGDESCRI like "+cp_ToStr(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStr(this.w_ARCODGRU);

            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oARCODSOT_3_32'),i_cWhere,'GSOF_ASF',"Codici sottogruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARCODGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_ARCODSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_ARCODGRU;
                       ,'SGCODSOT',this.w_ARCODSOT)
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODSOT = NVL(_Link_.SGCODSOT,space(5))
      this.w_DESSOT = NVL(_Link_.SGDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODSOT = space(5)
      endif
      this.w_DESSOT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SOT_SEZI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_32.SGCODSOT as SGCODSOT332"+ ",link_3_32.SGDESCRI as SGDESCRI332"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_32 on ART_ICOL.ARCODSOT=link_3_32.SGCODSOT"+" and ART_ICOL.ARCODGRU=link_3_32.SGCODGRU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_32"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODSOT=link_3_32.SGCODSOT(+)"'+'+" and ART_ICOL.ARCODGRU=link_3_32.SGCODGRU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IVAREP
  func Link_3_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAREP)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAREP = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVR = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_IVAREP = space(5)
      endif
      this.w_PERIVR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCLALOT
  func Link_5_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCLALOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_BZC',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_ARCLALOT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_ARCLALOT))
          select CMCODICE,CMDESCRI,CMTIPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCLALOT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCLALOT) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oARCLALOT_5_3'),i_cWhere,'GSMD_BZC',"Classi lotti",'GSMDLACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCLALOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_ARCLALOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_ARCLALOT)
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCLALOT = NVL(_Link_.CMCODICE,space(5))
      this.w_CLTDESCRI = NVL(_Link_.CMDESCRI,space(30))
      this.w_TIPCLA = NVL(_Link_.CMTIPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCLALOT = space(5)
      endif
      this.w_CLTDESCRI = space(30)
      this.w_TIPCLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCLA='L'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, classe inesistente o incongruente")
        endif
        this.w_ARCLALOT = space(5)
        this.w_CLTDESCRI = space(30)
        this.w_TIPCLA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCLALOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CMT_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_3.CMCODICE as CMCODICE503"+ ",link_5_3.CMDESCRI as CMDESCRI503"+ ",link_5_3.CMTIPCLA as CMTIPCLA503"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_3 on ART_ICOL.ARCLALOT=link_5_3.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_3"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCLALOT=link_5_3.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCLAMAT
  func Link_5_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCLAMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_BZC',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_ARCLAMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_ARCLAMAT))
          select CMCODICE,CMDESCRI,CMTIPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCLAMAT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCLAMAT) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oARCLAMAT_5_7'),i_cWhere,'GSMD_BZC',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCLAMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_ARCLAMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_ARCLAMAT)
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCLAMAT = NVL(_Link_.CMCODICE,space(5))
      this.w_CMTDESCRI = NVL(_Link_.CMDESCRI,space(30))
      this.w_TIPMAT = NVL(_Link_.CMTIPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCLAMAT = space(5)
      endif
      this.w_CMTDESCRI = space(30)
      this.w_TIPMAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAT='M'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, classe insesistente o incongruente")
        endif
        this.w_ARCLAMAT = space(5)
        this.w_CMTDESCRI = space(30)
        this.w_TIPMAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCLAMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CMT_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_7.CMCODICE as CMCODICE507"+ ",link_5_7.CMDESCRI as CMDESCRI507"+ ",link_5_7.CMTIPCLA as CMTIPCLA507"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_7 on ART_ICOL.ARCLAMAT=link_5_7.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_7"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCLAMAT=link_5_7.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARVOCCEN
  func Link_5_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ARVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ARVOCCEN))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oARVOCCEN_5_8'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMACAAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ARVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ARVOCCEN)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R' AND (EMPTY(.w_CENOBSO) OR .w_CENOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di costo incongruente o obsoleto")
        endif
        this.w_ARVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_8.VCCODICE as VCCODICE508"+ ",link_5_8.VCDESCRI as VCDESCRI508"+ ",link_5_8.VCTIPVOC as VCTIPVOC508"+ ",link_5_8.VCDTOBSO as VCDTOBSO508"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_8 on ART_ICOL.ARVOCCEN=link_5_8.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_8"
          i_cKey=i_cKey+'+" and ART_ICOL.ARVOCCEN=link_5_8.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARVOCRIC
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARVOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ARVOCRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ARVOCRIC))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARVOCRIC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARVOCRIC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oARVOCRIC_5_9'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARVOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ARVOCRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ARVOCRIC)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARVOCRIC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESRIC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPRIC = NVL(_Link_.VCTIPVOC,space(10))
      this.w_RICOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARVOCRIC = space(15)
      endif
      this.w_DESRIC = space(40)
      this.w_TIPRIC = space(10)
      this.w_RICOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIC<>'C' AND (EMPTY(.w_RICOBSO) OR .w_RICOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_ARVOCRIC = space(15)
        this.w_DESRIC = space(40)
        this.w_TIPRIC = space(10)
        this.w_RICOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARVOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_9.VCCODICE as VCCODICE509"+ ",link_5_9.VCDESCRI as VCDESCRI509"+ ",link_5_9.VCTIPVOC as VCTIPVOC509"+ ",link_5_9.VCDTOBSO as VCDTOBSO509"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_9 on ART_ICOL.ARVOCRIC=link_5_9.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_9"
          i_cKey=i_cKey+'+" and ART_ICOL.ARVOCRIC=link_5_9.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODCEN
  func Link_5_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ARCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ARCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oARCODCEN_5_10'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ARCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ARCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_DCEOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_DCEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DCEOBSO) OR .w_DCEOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice centro di costo obsoleto")
        endif
        this.w_ARCODCEN = space(15)
        this.w_DESCEN = space(40)
        this.w_DCEOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_10.CC_CONTO as CC_CONTO510"+ ",link_5_10.CCDESPIA as CCDESPIA510"+ ",link_5_10.CCDTOBSO as CCDTOBSO510"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_10 on ART_ICOL.ARCODCEN=link_5_10.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_10"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODCEN=link_5_10.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODDIS
  func Link_5_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BAK',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_ARCODDIS)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_ARCODDIS))
          select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODDIS)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODDIS) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oARCODDIS_5_11'),i_cWhere,'GSAR_BAK',"Elenco distinte base - kit",'GSDS_AAR.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_ARCODDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_ARCODDIS)
            select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODDIS = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
      this.w_DBOBSO = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
      this.w_DBINVA = NVL(cp_ToDate(_Link_.DBDTINVA),ctod("  /  /  "))
      this.w_DISKIT = NVL(_Link_.DBDISKIT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODDIS = space(20)
      endif
      this.w_DESDIS = space(40)
      this.w_DBOBSO = ctod("  /  /  ")
      this.w_DBINVA = ctod("  /  /  ")
      this.w_DISKIT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DBOBSO) OR .w_DBOBSO>=i_DATSYS) AND (EMPTY(.w_DBINVA) OR .w_DBINVA<=i_DATSYS)) and ((g_VEFA = 'S' And .w_DISKIT = 'I') Or (g_DISB = 'S' And .w_DISKIT = 'D') Or ((g_VEFA = 'S' Or g_GPOS = 'S') And .w_DISKIT = 'K')) And .w_ARKITIMB='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati")
        endif
        this.w_ARCODDIS = space(20)
        this.w_DESDIS = space(40)
        this.w_DBOBSO = ctod("  /  /  ")
        this.w_DBINVA = ctod("  /  /  ")
        this.w_DISKIT = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARMAGPRE
  func Link_5_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARMAGPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ARMAGPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ARMAGPRE))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARMAGPRE)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARMAGPRE) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oARMAGPRE_5_13'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARMAGPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ARMAGPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ARMAGPRE)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARMAGPRE = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESPRE = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG = NVL(_Link_.MGDISMAG,space(1))
      this.w_MAGOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARMAGPRE = space(5)
      endif
      this.w_DESPRE = space(30)
      this.w_DISMAG = space(1)
      this.w_MAGOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_ARMAGPRE) OR .w_DISMAG='S' AND (EMPTY(.w_MAGOBSO) OR .w_MAGOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino produzione non nettificabile o obsoleto")
        endif
        this.w_ARMAGPRE = space(5)
        this.w_DESPRE = space(30)
        this.w_DISMAG = space(1)
        this.w_MAGOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARMAGPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_13.MGCODMAG as MGCODMAG513"+ ",link_5_13.MGDESMAG as MGDESMAG513"+ ",link_5_13.MGDISMAG as MGDISMAG513"+ ",link_5_13.MGDTOBSO as MGDTOBSO513"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_13 on ART_ICOL.ARMAGPRE=link_5_13.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_13"
          i_cKey=i_cKey+'+" and ART_ICOL.ARMAGPRE=link_5_13.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCLACRI
  func Link_5_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAT_CRI_IDX,3]
    i_lTable = "MAT_CRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2], .t., this.MAT_CRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCLACRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMR',True,'MAT_CRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MTCODICE like "+cp_ToStrODBC(trim(this.w_ARCLACRI)+"%");

          i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MTCODICE',trim(this.w_ARCLACRI))
          select MTCODICE,MTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCLACRI)==trim(_Link_.MTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCLACRI) and !this.bDontReportError
            deferred_cp_zoom('MAT_CRI','*','MTCODICE',cp_AbsName(oSource.parent,'oARCLACRI_5_45'),i_cWhere,'GSAR_AMR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',oSource.xKey(1))
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCLACRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MTCODICE,MTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MTCODICE="+cp_ToStrODBC(this.w_ARCLACRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MTCODICE',this.w_ARCLACRI)
            select MTCODICE,MTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCLACRI = NVL(_Link_.MTCODICE,space(5))
      this.w_MATCRI = NVL(_Link_.MTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCLACRI = space(5)
      endif
      this.w_MATCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])+'\'+cp_ToStr(_Link_.MTCODICE,1)
      cp_ShowWarn(i_cKey,this.MAT_CRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCLACRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAT_CRI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAT_CRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_45.MTCODICE as MTCODICE545"+ ",link_5_45.MTDESCRI as MTDESCRI545"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_45 on ART_ICOL.ARCLACRI=link_5_45.MTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_45"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCLACRI=link_5_45.MTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARMAGIMP
  func Link_5_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARMAGIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ARMAGIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ARMAGIMP))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARMAGIMP)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARMAGIMP) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oARMAGIMP_5_53'),i_cWhere,'GSAR_AMA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARMAGIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ARMAGIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ARMAGIMP)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARMAGIMP = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMIMP = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ARMAGIMP = space(5)
      endif
      this.w_DESMIMP = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARMAGIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_53(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_53.MGCODMAG as MGCODMAG553"+ ",link_5_53.MGDESMAG as MGDESMAG553"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_53 on ART_ICOL.ARMAGIMP=link_5_53.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_53"
          i_cKey=i_cKey+'+" and ART_ICOL.ARMAGIMP=link_5_53.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARFSRIFE
  func Link_7_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARFSRIFE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARFSRIFE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARFSRIFE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARFSRIFE)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARFSRIFE = NVL(_Link_.ARCODART,space(20))
      this.w_DESRIFFS = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARFSRIFE = space(20)
      endif
      this.w_DESRIFFS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARFSRIFE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_7.ARCODART as ARCODART707"+ ",link_7_7.ARDESART as ARDESART707"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_7 on ART_ICOL.ARFSRIFE=link_7_7.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_7"
          i_cKey=i_cKey+'+" and ART_ICOL.ARFSRIFE=link_7_7.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODART_1_4.value==this.w_ARCODART)
      this.oPgFrm.Page1.oPag.oARCODART_1_4.value=this.w_ARCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_5.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_5.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPART_1_6.RadioValue()==this.w_ARTIPART)
      this.oPgFrm.Page1.oPag.oARTIPART_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPART_1_7.RadioValue()==this.w_ARTIPART)
      this.oPgFrm.Page1.oPag.oARTIPART_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESSUP_1_8.value==this.w_ARDESSUP)
      this.oPgFrm.Page1.oPag.oARDESSUP_1_8.value=this.w_ARDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_11.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_11.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.value==this.w_ARUNMIS2)
      this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.value=this.w_ARUNMIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oAROPERAT_1_14.RadioValue()==this.w_AROPERAT)
      this.oPgFrm.Page1.oPag.oAROPERAT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARMOLTIP_1_16.value==this.w_ARMOLTIP)
      this.oPgFrm.Page1.oPag.oARMOLTIP_1_16.value=this.w_ARMOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLUSEP_1_17.RadioValue()==this.w_ARFLUSEP)
      this.oPgFrm.Page1.oPag.oARFLUSEP_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARPREZUM_1_18.RadioValue()==this.w_ARPREZUM)
      this.oPgFrm.Page1.oPag.oARPREZUM_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLINVE_1_19.RadioValue()==this.w_ARFLINVE)
      this.oPgFrm.Page1.oPag.oARFLINVE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARSTASUP_1_20.RadioValue()==this.w_ARSTASUP)
      this.oPgFrm.Page1.oPag.oARSTASUP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARASSIVA_1_21.RadioValue()==this.w_ARASSIVA)
      this.oPgFrm.Page1.oPag.oARASSIVA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODIVA_1_23.value==this.w_ARCODIVA)
      this.oPgFrm.Page1.oPag.oARCODIVA_1_23.value=this.w_ARCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPOPE_1_24.value==this.w_ARTIPOPE)
      this.oPgFrm.Page1.oPag.oARTIPOPE_1_24.value=this.w_ARTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_25.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_25.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARCATCON_1_26.value==this.w_ARCATCON)
      this.oPgFrm.Page1.oPag.oARCATCON_1_26.value=this.w_ARCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_27.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_27.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODFAM_1_28.value==this.w_ARCODFAM)
      this.oPgFrm.Page1.oPag.oARCODFAM_1_28.value=this.w_ARCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_29.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_29.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oARGRUMER_1_30.value==this.w_ARGRUMER)
      this.oPgFrm.Page1.oPag.oARGRUMER_1_30.value=this.w_ARGRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_31.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_31.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oARCATOMO_1_32.value==this.w_ARCATOMO)
      this.oPgFrm.Page1.oPag.oARCATOMO_1_32.value=this.w_ARCATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOMO_1_33.value==this.w_DESOMO)
      this.oPgFrm.Page1.oPag.oDESOMO_1_33.value=this.w_DESOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPGES_1_34.RadioValue()==this.w_ARTIPGES)
      this.oPgFrm.Page1.oPag.oARTIPGES_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARPROPRE_1_35.RadioValue()==this.w_ARPROPRE)
      this.oPgFrm.Page1.oPag.oARPROPRE_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPBAR_1_36.RadioValue()==this.w_ARTIPBAR)
      this.oPgFrm.Page1.oPag.oARTIPBAR_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPBAR_1_37.RadioValue()==this.w_TIPBAR)
      this.oPgFrm.Page1.oPag.oTIPBAR_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLDISP_1_38.RadioValue()==this.w_ARFLDISP)
      this.oPgFrm.Page1.oPag.oARFLDISP_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLDISC_1_39.RadioValue()==this.w_ARFLDISC)
      this.oPgFrm.Page1.oPag.oARFLDISC_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODCLA_1_40.value==this.w_ARCODCLA)
      this.oPgFrm.Page1.oPag.oARCODCLA_1_40.value=this.w_ARCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oARSALCOM_1_41.RadioValue()==this.w_ARSALCOM)
      this.oPgFrm.Page1.oPag.oARSALCOM_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARDTINVA_1_42.value==this.w_ARDTINVA)
      this.oPgFrm.Page1.oPag.oARDTINVA_1_42.value=this.w_ARDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARDTOBSO_1_43.value==this.w_ARDTOBSO)
      this.oPgFrm.Page1.oPag.oARDTOBSO_1_43.value=this.w_ARDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.RadioValue()==this.w_ARFLCOM1)
      this.oPgFrm.Page1.oPag.oARFLCOM1_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.RadioValue()==this.w_ARFLCOM1)
      this.oPgFrm.Page1.oPag.oARFLCOM1_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.RadioValue()==this.w_ARFLCOM1)
      this.oPgFrm.Page1.oPag.oARFLCOM1_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_1.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oUM1_2_3.value==this.w_UM1)
      this.oPgFrm.Page2.oPag.oUM1_2_3.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUM2_2_4.value==this.w_UM2)
      this.oPgFrm.Page2.oPag.oUM2_2_4.value=this.w_UM2
    endif
    if not(this.oPgFrm.Page2.oPag.oARPESNET_2_5.value==this.w_ARPESNET)
      this.oPgFrm.Page2.oPag.oARPESNET_2_5.value=this.w_ARPESNET
    endif
    if not(this.oPgFrm.Page2.oPag.oARPESLOR_2_6.value==this.w_ARPESLOR)
      this.oPgFrm.Page2.oPag.oARPESLOR_2_6.value=this.w_ARPESLOR
    endif
    if not(this.oPgFrm.Page2.oPag.oARTIPCO1_2_7.value==this.w_ARTIPCO1)
      this.oPgFrm.Page2.oPag.oARTIPCO1_2_7.value=this.w_ARTIPCO1
    endif
    if not(this.oPgFrm.Page2.oPag.oARTPCONF_2_8.value==this.w_ARTPCONF)
      this.oPgFrm.Page2.oPag.oARTPCONF_2_8.value=this.w_ARTPCONF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTCF_2_9.value==this.w_DESTCF)
      this.oPgFrm.Page2.oPag.oDESTCF_2_9.value=this.w_DESTCF
    endif
    if not(this.oPgFrm.Page2.oPag.oARPZCONF_2_10.value==this.w_ARPZCONF)
      this.oPgFrm.Page2.oPag.oARPZCONF_2_10.value=this.w_ARPZCONF
    endif
    if not(this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.value==this.w_ARCOCOL1)
      this.oPgFrm.Page2.oPag.oARCOCOL1_2_11.value=this.w_ARCOCOL1
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMLUN_2_12.value==this.w_ARDIMLUN)
      this.oPgFrm.Page2.oPag.oARDIMLUN_2_12.value=this.w_ARDIMLUN
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMLAR_2_13.value==this.w_ARDIMLAR)
      this.oPgFrm.Page2.oPag.oARDIMLAR_2_13.value=this.w_ARDIMLAR
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMALT_2_14.value==this.w_ARDIMALT)
      this.oPgFrm.Page2.oPag.oARDIMALT_2_14.value=this.w_ARDIMALT
    endif
    if not(this.oPgFrm.Page2.oPag.oVOLUME_2_15.value==this.w_VOLUME)
      this.oPgFrm.Page2.oPag.oVOLUME_2_15.value=this.w_VOLUME
    endif
    if not(this.oPgFrm.Page2.oPag.oARUMVOLU_2_16.value==this.w_ARUMVOLU)
      this.oPgFrm.Page2.oPag.oARUMVOLU_2_16.value=this.w_ARUMVOLU
    endif
    if not(this.oPgFrm.Page2.oPag.oARUMDIME_2_17.value==this.w_ARUMDIME)
      this.oPgFrm.Page2.oPag.oARUMDIME_2_17.value=this.w_ARUMDIME
    endif
    if not(this.oPgFrm.Page2.oPag.oARDESVOL_2_18.value==this.w_ARDESVOL)
      this.oPgFrm.Page2.oPag.oARDESVOL_2_18.value=this.w_ARDESVOL
    endif
    if not(this.oPgFrm.Page2.oPag.oARFLCONA_2_19.RadioValue()==this.w_ARFLCONA)
      this.oPgFrm.Page2.oPag.oARFLCONA_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARPESNE2_2_20.value==this.w_ARPESNE2)
      this.oPgFrm.Page2.oPag.oARPESNE2_2_20.value=this.w_ARPESNE2
    endif
    if not(this.oPgFrm.Page2.oPag.oARPESLO2_2_21.value==this.w_ARPESLO2)
      this.oPgFrm.Page2.oPag.oARPESLO2_2_21.value=this.w_ARPESLO2
    endif
    if not(this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.value==this.w_ARTIPCO2)
      this.oPgFrm.Page2.oPag.oARTIPCO2_2_22.value=this.w_ARTIPCO2
    endif
    if not(this.oPgFrm.Page2.oPag.oARTPCON2_2_23.value==this.w_ARTPCON2)
      this.oPgFrm.Page2.oPag.oARTPCON2_2_23.value=this.w_ARTPCON2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTCF2_2_24.value==this.w_DESTCF2)
      this.oPgFrm.Page2.oPag.oDESTCF2_2_24.value=this.w_DESTCF2
    endif
    if not(this.oPgFrm.Page2.oPag.oARPZCON2_2_25.value==this.w_ARPZCON2)
      this.oPgFrm.Page2.oPag.oARPZCON2_2_25.value=this.w_ARPZCON2
    endif
    if not(this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.value==this.w_ARCOCOL2)
      this.oPgFrm.Page2.oPag.oARCOCOL2_2_26.value=this.w_ARCOCOL2
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.value==this.w_ARDIMLU2)
      this.oPgFrm.Page2.oPag.oARDIMLU2_2_27.value=this.w_ARDIMLU2
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.value==this.w_ARDIMLA2)
      this.oPgFrm.Page2.oPag.oARDIMLA2_2_28.value=this.w_ARDIMLA2
    endif
    if not(this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.value==this.w_ARDIMAL2)
      this.oPgFrm.Page2.oPag.oARDIMAL2_2_29.value=this.w_ARDIMAL2
    endif
    if not(this.oPgFrm.Page2.oPag.oVOLUM2_2_30.value==this.w_VOLUM2)
      this.oPgFrm.Page2.oPag.oVOLUM2_2_30.value=this.w_VOLUM2
    endif
    if not(this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.value==this.w_ARUMVOL2)
      this.oPgFrm.Page2.oPag.oARUMVOL2_2_31.value=this.w_ARUMVOL2
    endif
    if not(this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.value==this.w_ARUMDIM2)
      this.oPgFrm.Page2.oPag.oARUMDIM2_2_32.value=this.w_ARUMDIM2
    endif
    if not(this.oPgFrm.Page2.oPag.oARDESVO2_2_33.value==this.w_ARDESVO2)
      this.oPgFrm.Page2.oPag.oARDESVO2_2_33.value=this.w_ARDESVO2
    endif
    if not(this.oPgFrm.Page2.oPag.oARFLCON2_2_34.RadioValue()==this.w_ARFLCON2)
      this.oPgFrm.Page2.oPag.oARFLCON2_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARUTISER_2_35.RadioValue()==this.w_ARUTISER)
      this.oPgFrm.Page2.oPag.oARUTISER_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARPERSER_2_36.RadioValue()==this.w_ARPERSER)
      this.oPgFrm.Page2.oPag.oARPERSER_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARNOMENC_2_37.value==this.w_ARNOMENC)
      this.oPgFrm.Page2.oPag.oARNOMENC_2_37.value=this.w_ARNOMENC
    endif
    if not(this.oPgFrm.Page2.oPag.oARMOLSUP_2_38.value==this.w_ARMOLSUP)
      this.oPgFrm.Page2.oPag.oARMOLSUP_2_38.value=this.w_ARMOLSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oARDATINT_2_39.RadioValue()==this.w_ARDATINT)
      this.oPgFrm.Page2.oPag.oARDATINT_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_1.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_2.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODMAR_3_4.value==this.w_ARCODMAR)
      this.oPgFrm.Page3.oPag.oARCODMAR_3_4.value=this.w_ARCODMAR
    endif
    if not(this.oPgFrm.Page3.oPag.oDESMAR_3_5.value==this.w_DESMAR)
      this.oPgFrm.Page3.oPag.oDESMAR_3_5.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page3.oPag.oARGRUPRO_3_7.value==this.w_ARGRUPRO)
      this.oPgFrm.Page3.oPag.oARGRUPRO_3_7.value=this.w_ARGRUPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oDESGPP_3_8.value==this.w_DESGPP)
      this.oPgFrm.Page3.oPag.oDESGPP_3_8.value=this.w_DESGPP
    endif
    if not(this.oPgFrm.Page3.oPag.oARCATSCM_3_10.value==this.w_ARCATSCM)
      this.oPgFrm.Page3.oPag.oARCATSCM_3_10.value=this.w_ARCATSCM
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSCM_3_11.value==this.w_DESSCM)
      this.oPgFrm.Page3.oPag.oDESSCM_3_11.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODRIC_3_18.value==this.w_ARCODRIC)
      this.oPgFrm.Page3.oPag.oARCODRIC_3_18.value=this.w_ARCODRIC
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCLR_3_19.value==this.w_DESCLR)
      this.oPgFrm.Page3.oPag.oDESCLR_3_19.value=this.w_DESCLR
    endif
    if not(this.oPgFrm.Page3.oPag.oPERCLR_3_22.value==this.w_PERCLR)
      this.oPgFrm.Page3.oPag.oPERCLR_3_22.value=this.w_PERCLR
    endif
    if not(this.oPgFrm.Page3.oPag.oARARTPOS_3_25.RadioValue()==this.w_ARARTPOS)
      this.oPgFrm.Page3.oPag.oARARTPOS_3_25.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODREP_3_26.value==this.w_ARCODREP)
      this.oPgFrm.Page3.oPag.oARCODREP_3_26.value=this.w_ARCODREP
    endif
    if not(this.oPgFrm.Page3.oPag.oDESREP_3_27.value==this.w_DESREP)
      this.oPgFrm.Page3.oPag.oDESREP_3_27.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_3.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_3.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESC_4_4.value==this.w_DESC)
      this.oPgFrm.Page4.oPag.oDESC_4_4.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page3.oPag.oARPUBWEB_3_29.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARFLPECO_3_30.RadioValue()==this.w_ARFLPECO)
      this.oPgFrm.Page3.oPag.oARFLPECO_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODGRU_3_31.value==this.w_ARCODGRU)
      this.oPgFrm.Page3.oPag.oARCODGRU_3_31.value=this.w_ARCODGRU
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODSOT_3_32.value==this.w_ARCODSOT)
      this.oPgFrm.Page3.oPag.oARCODSOT_3_32.value=this.w_ARCODSOT
    endif
    if not(this.oPgFrm.Page3.oPag.oARMINVEN_3_33.value==this.w_ARMINVEN)
      this.oPgFrm.Page3.oPag.oARMINVEN_3_33.value=this.w_ARMINVEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOL_2_72.value==this.w_DESCOL)
      this.oPgFrm.Page2.oPag.oDESCOL_2_72.value=this.w_DESCOL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOL2_2_73.value==this.w_DESCOL2)
      this.oPgFrm.Page2.oPag.oDESCOL2_2_73.value=this.w_DESCOL2
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSOT_3_40.value==this.w_DESSOT)
      this.oPgFrm.Page3.oPag.oDESSOT_3_40.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page3.oPag.oDESTIT_3_41.value==this.w_DESTIT)
      this.oPgFrm.Page3.oPag.oDESTIT_3_41.value=this.w_DESTIT
    endif
    if not(this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page3.oPag.oARPUBWEB_3_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARPUBWEB_3_45.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page3.oPag.oARPUBWEB_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_99.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_99.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page5.oPag.oARFLLOTT_5_1.RadioValue()==this.w_ARFLLOTT)
      this.oPgFrm.Page5.oPag.oARFLLOTT_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARFLLMAG_5_2.RadioValue()==this.w_ARFLLMAG)
      this.oPgFrm.Page5.oPag.oARFLLMAG_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARCLALOT_5_3.value==this.w_ARCLALOT)
      this.oPgFrm.Page5.oPag.oARCLALOT_5_3.value=this.w_ARCLALOT
    endif
    if not(this.oPgFrm.Page5.oPag.oARDISLOT_5_4.RadioValue()==this.w_ARDISLOT)
      this.oPgFrm.Page5.oPag.oARDISLOT_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARFLESUL_5_5.RadioValue()==this.w_ARFLESUL)
      this.oPgFrm.Page5.oPag.oARFLESUL_5_5.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARGESMAT_5_6.RadioValue()==this.w_ARGESMAT)
      this.oPgFrm.Page5.oPag.oARGESMAT_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.value==this.w_ARCLAMAT)
      this.oPgFrm.Page5.oPag.oARCLAMAT_5_7.value=this.w_ARCLAMAT
    endif
    if not(this.oPgFrm.Page5.oPag.oARVOCCEN_5_8.value==this.w_ARVOCCEN)
      this.oPgFrm.Page5.oPag.oARVOCCEN_5_8.value=this.w_ARVOCCEN
    endif
    if not(this.oPgFrm.Page5.oPag.oARVOCRIC_5_9.value==this.w_ARVOCRIC)
      this.oPgFrm.Page5.oPag.oARVOCRIC_5_9.value=this.w_ARVOCRIC
    endif
    if not(this.oPgFrm.Page5.oPag.oARCODCEN_5_10.value==this.w_ARCODCEN)
      this.oPgFrm.Page5.oPag.oARCODCEN_5_10.value=this.w_ARCODCEN
    endif
    if not(this.oPgFrm.Page5.oPag.oARCODDIS_5_11.value==this.w_ARCODDIS)
      this.oPgFrm.Page5.oPag.oARCODDIS_5_11.value=this.w_ARCODDIS
    endif
    if not(this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.RadioValue()==this.w_ARFLCOMP)
      this.oPgFrm.Page5.oPag.oARFLCOMP_5_12.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARMAGPRE_5_13.value==this.w_ARMAGPRE)
      this.oPgFrm.Page5.oPag.oARMAGPRE_5_13.value=this.w_ARMAGPRE
    endif
    if not(this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.RadioValue()==this.w_ARTIPPRE)
      this.oPgFrm.Page5.oPag.oARTIPPRE_5_14.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCMTDESCRI_5_16.value==this.w_CMTDESCRI)
      this.oPgFrm.Page5.oPag.oCMTDESCRI_5_16.value=this.w_CMTDESCRI
    endif
    if not(this.oPgFrm.Page5.oPag.oCLTDESCRI_5_18.value==this.w_CLTDESCRI)
      this.oPgFrm.Page5.oPag.oCLTDESCRI_5_18.value=this.w_CLTDESCRI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESVOC_5_21.value==this.w_DESVOC)
      this.oPgFrm.Page5.oPag.oDESVOC_5_21.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page5.oPag.oDESRIC_5_23.value==this.w_DESRIC)
      this.oPgFrm.Page5.oPag.oDESRIC_5_23.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page5.oPag.oDESDIS_5_26.value==this.w_DESDIS)
      this.oPgFrm.Page5.oPag.oDESDIS_5_26.value=this.w_DESDIS
    endif
    if not(this.oPgFrm.Page5.oPag.oDESPRE_5_28.value==this.w_DESPRE)
      this.oPgFrm.Page5.oPag.oDESPRE_5_28.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_101.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_101.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCEN_5_40.value==this.w_DESCEN)
      this.oPgFrm.Page5.oPag.oDESCEN_5_40.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page5.oPag.oARKITIMB_5_42.RadioValue()==this.w_ARKITIMB)
      this.oPgFrm.Page5.oPag.oARKITIMB_5_42.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARFLESIM_5_44.RadioValue()==this.w_ARFLESIM)
      this.oPgFrm.Page5.oPag.oARFLESIM_5_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARFLAPCA_3_47.RadioValue()==this.w_ARFLAPCA)
      this.oPgFrm.Page3.oPag.oARFLAPCA_3_47.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARCLACRI_5_45.value==this.w_ARCLACRI)
      this.oPgFrm.Page5.oPag.oARCLACRI_5_45.value=this.w_ARCLACRI
    endif
    if not(this.oPgFrm.Page5.oPag.oMATCRI_5_47.value==this.w_MATCRI)
      this.oPgFrm.Page5.oPag.oMATCRI_5_47.value=this.w_MATCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oUMSUPP_2_76.value==this.w_UMSUPP)
      this.oPgFrm.Page2.oPag.oUMSUPP_2_76.value=this.w_UMSUPP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_79.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_79.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oARCHKUCA_1_115.RadioValue()==this.w_ARCHKUCA)
      this.oPgFrm.Page1.oPag.oARCHKUCA_1_115.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCODI_7_1.value==this.w_CODI)
      this.oPgFrm.Page7.oPag.oCODI_7_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page7.oPag.oDESC_7_2.value==this.w_DESC)
      this.oPgFrm.Page7.oPag.oDESC_7_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page5.oPag.oARTIPIMP_5_52.RadioValue()==this.w_ARTIPIMP)
      this.oPgFrm.Page5.oPag.oARTIPIMP_5_52.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oARMAGIMP_5_53.value==this.w_ARMAGIMP)
      this.oPgFrm.Page5.oPag.oARMAGIMP_5_53.value=this.w_ARMAGIMP
    endif
    if not(this.oPgFrm.Page5.oPag.oDESMIMP_5_55.value==this.w_DESMIMP)
      this.oPgFrm.Page5.oPag.oDESMIMP_5_55.value=this.w_DESMIMP
    endif
    if not(this.oPgFrm.Page8.oPag.oCODI_8_2.value==this.w_CODI)
      this.oPgFrm.Page8.oPag.oCODI_8_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page8.oPag.oDESC_8_3.value==this.w_DESC)
      this.oPgFrm.Page8.oPag.oDESC_8_3.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page7.oPag.oARFSRIFE_7_7.value==this.w_ARFSRIFE)
      this.oPgFrm.Page7.oPag.oARFSRIFE_7_7.value=this.w_ARFSRIFE
    endif
    if not(this.oPgFrm.Page7.oPag.oDESRIFFS_7_9.value==this.w_DESRIFFS)
      this.oPgFrm.Page7.oPag.oDESRIFFS_7_9.value=this.w_DESRIFFS
    endif
    if not(this.oPgFrm.Page5.oPag.oARCMPCAR_5_58.RadioValue()==this.w_ARCMPCAR)
      this.oPgFrm.Page5.oPag.oARCMPCAR_5_58.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODTIP_3_53.value==this.w_ARCODTIP)
      this.oPgFrm.Page3.oPag.oARCODTIP_3_53.value=this.w_ARCODTIP
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODCLF_3_54.value==this.w_ARCODCLF)
      this.oPgFrm.Page3.oPag.oARCODCLF_3_54.value=this.w_ARCODCLF
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODVAL_3_55.value==this.w_ARCODVAL)
      this.oPgFrm.Page3.oPag.oARCODVAL_3_55.value=this.w_ARCODVAL
    endif
    if not(this.oPgFrm.Page3.oPag.oARFLGESC_3_56.RadioValue()==this.w_ARFLGESC)
      this.oPgFrm.Page3.oPag.oARFLGESC_3_56.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ART_ICOL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ARCODART)) or not(IIF(.cFunction='Load' AND .w_AUTO<>'S',chkcodar('A',.w_ARCODART),.T.)))  and (.cFunction='Query' OR (.w_AUTO<>'S' AND .cFunction<>'Edit'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODART_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ARCODART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARUNMIS1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS1_1_11.SetFocus()
            i_bnoObbl = !empty(.w_ARUNMIS1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARUNMIS2<>.w_ARUNMIS1)  and not(empty(.w_ARUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARMOLTIP))  and (NOT EMPTY(.w_ARUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARMOLTIP_1_16.SetFocus()
            i_bnoObbl = !empty(.w_ARMOLTIP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ARCODIVA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODIVA_1_23.SetFocus()
            i_bnoObbl = !empty(.w_ARCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(EMPTY(.w_POPOBSO) OR .w_POPOBSO>.w_OBTEST)  and not(empty(.w_ARTIPOPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTIPOPE_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice obsoleto oppure inesistente")
          case   (empty(.w_ARCATCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCATCON_1_26.SetFocus()
            i_bnoObbl = !empty(.w_ARCATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((CALCBAR(.w_CODICE, .w_TIPBAR, 0) OR  .w_ARTIPBAR<>'3'))  and not(.w_ARTIPBAR<>'3' OR .cFunction<>'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPBAR_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARTPCONF))  and not(EMPTY(.w_ARTIPCO1))  and (NOT EMPTY(.w_ARTIPCO1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTPCONF_2_8.SetFocus()
            i_bnoObbl = !empty(.w_ARTPCONF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARPZCONF))  and not(EMPTY(.w_ARTPCONF))  and (NOT EMPTY(.w_ARTPCONF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARPZCONF_2_10.SetFocus()
            i_bnoObbl = !empty(.w_ARPZCONF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARCOCOL1))  and not(EMPTY(.w_ARTPCONF))  and (NOT EMPTY(.w_ARTPCONF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARCOCOL1_2_11.SetFocus()
            i_bnoObbl = !empty(.w_ARCOCOL1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare il numero di confezioni per collo")
          case   (empty(.w_ARTPCON2))  and not(EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTIPCO2))  and (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTIPCO2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTPCON2_2_23.SetFocus()
            i_bnoObbl = !empty(.w_ARTPCON2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARPZCON2))  and not(EMPTY(.w_ARUNMIS2) OR  EMPTY(.w_ARTPCON2))  and (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTPCON2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARPZCON2_2_25.SetFocus()
            i_bnoObbl = !empty(.w_ARPZCON2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARCOCOL2))  and not(EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTPCON2))  and (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTPCON2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARCOCOL2_2_26.SetFocus()
            i_bnoObbl = !empty(.w_ARCOCOL2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare il numero di confezioni per collo")
          case   not(.w_ARUTISER='N' OR LEN(ALLTRIM(.w_ARNOMENC))=6 OR LEN(ALLTRIM(.w_ARNOMENC))=5)  and (g_INTR='S' and (.w_ARDATINT<>'N' OR .w_ARUTISER<>'N'))  and not(empty(.w_ARNOMENC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARNOMENC_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o incongruente")
          case   not(.w_FLPRO $ ' A')  and not(empty(.w_ARGRUPRO))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oARGRUPRO_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo articolo")
          case   not(.w_FLSCM $ ' A')  and not(empty(.w_ARCATSCM))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oARCATSCM_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo articolo")
          case   not(.w_FLFRAZ<>'S' Or .w_ARMINVEN =Int(.w_ARMINVEN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oARMINVEN_3_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'unit� di misura principale non � frazionabile")
          case   not(.w_TIPCLA='L')  and not(.w_ARFLLOTT='N')  and (g_PERLOT = 'S' And .w_ARFLLOTT<>'N' and .w_ARTIPART<>'FS')  and not(empty(.w_ARCLALOT))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARCLALOT_5_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, classe inesistente o incongruente")
          case   ((empty(.w_ARCLAMAT)) or not(.w_TIPMAT='M'))  and not(g_MATR<>'S')  and (.w_ARGESMAT='S' And g_MATR='S')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARCLAMAT_5_7.SetFocus()
            i_bnoObbl = !empty(.w_ARCLAMAT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, classe insesistente o incongruente")
          case   not(.w_TIPVOC<>'R' AND (EMPTY(.w_CENOBSO) OR .w_CENOBSO>.w_OBTEST))  and (g_PERCCR='S' OR g_COMM='S')  and not(empty(.w_ARVOCCEN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARVOCCEN_5_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di costo incongruente o obsoleto")
          case   not(.w_TIPRIC<>'C' AND (EMPTY(.w_RICOBSO) OR .w_RICOBSO>.w_OBTEST))  and (g_PERCCR='S' OR g_COMM='S')  and not(empty(.w_ARVOCRIC))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARVOCRIC_5_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(EMPTY(.w_DCEOBSO) OR .w_DCEOBSO>.w_OBTEST)  and not(empty(.w_ARCODCEN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARCODCEN_5_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice centro di costo obsoleto")
          case   not(((EMPTY(.w_DBOBSO) OR .w_DBOBSO>=i_DATSYS) AND (EMPTY(.w_DBINVA) OR .w_DBINVA<=i_DATSYS)) and ((g_VEFA = 'S' And .w_DISKIT = 'I') Or (g_DISB = 'S' And .w_DISKIT = 'D') Or ((g_VEFA = 'S' Or g_GPOS = 'S') And .w_DISKIT = 'K')) And .w_ARKITIMB='N')  and not(g_EACD<>'S' And g_GPOS <> 'S')  and ((g_EACD = 'S' Or g_GPOS = 'S') And .w_ARKITIMB='N')  and not(empty(.w_ARCODDIS))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARCODDIS_5_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati")
          case   not(EMPTY(.w_ARMAGPRE) OR .w_DISMAG='S' AND (EMPTY(.w_MAGOBSO) OR .w_MAGOBSO>.w_OBTEST))  and not(empty(.w_ARMAGPRE))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARMAGPRE_5_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino produzione non nettificabile o obsoleto")
          case   (empty(.w_ARMAGIMP) and (.w_ARTIPIMP='F'))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oARMAGIMP_5_53.SetFocus()
            i_bnoObbl = !empty(.w_ARMAGIMP) or !(.w_ARTIPIMP='F')
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MLI.CheckForm()
      if i_bres
        i_bres=  .GSMA_MLI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MRP.CheckForm()
      if i_bres
        i_bres=  .GSMA_MRP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_APR.CheckForm()
      if i_bres
        i_bres=  .GSMA_APR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MAL.CheckForm()
      if i_bres
        i_bres=  .GSMA_MAL.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MAR.CheckForm()
      if i_bres
        i_bres=  .GSAR_MAR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MR2.CheckForm()
      if i_bres
        i_bres=  .GSAR_MR2.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_ANO.CheckForm()
      if i_bres
        i_bres=  .GSMA_ANO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=8
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MDA.CheckForm()
      if i_bres
        i_bres=  .GSMA_MDA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MAC.CheckForm()
      if i_bres
        i_bres=  .GSMA_MAC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsma_aar
      if i_bRes and g_GPOS='S' AND .w_ARARTPOS='S'
        if empty(.w_ARCODREP)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Codice reparto inesistente")
        else
          if .w_PERIVA<>.w_PERIVR
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("Aliquota IVA reparto incongruente con aliquota iva articolo")
          endif
        endif
      endif
      if i_bRes
        i_bRes =CHKMEMO(.w_ARDESSUP)
      endif
      
      if i_bRes and g_COAC='S' AND THIS.w_ARFLAPCA = 'S'
        L_NUMEROCONTRIBUTICONAI = GSAR_BE3( THIS , "ART" )
        IF L_NUMEROCONTRIBUTICONAI > 0 
          IF THIS.w_ARPESNET = 0 OR THIS.w_ARPESLOR = 0 OR ( !EMPTY(THIS.w_ARUNMIS2)  AND (THIS.w_ARPESNE2 = 0 OR THIS.w_ARPESLO2 = 0))
            i_bRes = AH_YESNO("Sono stati inseriti contributi accessori a peso, ma il peso netto o lordo non � stato specificato.%0Si desidera confermare?")
            i_cErrorMsg = ''
          ENDIF 
        endif
      endif
      if i_bRes and g_GPFA="S" and empty(nvl(this.w_GESTGUID,"")) and this.GSAR_MR2.numrow() > 0
          this.w_GESTGUID = SUBSTR(DTOC(DATE(),1),4)+SUBSTR(SYS(2015),2)
      endif
      if i_bRes and g_GPFA="S" and this.GSAR_MR2.numrow() > 0
          L_MESSAGE = CHK_ELE_ATTR( "D", this.GSAR_MR2 )
          if not empty (L_MESSAGE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = L_MESSAGE
          endif
      ENDIF
      if i_bRes and empty(.w_ARCODART)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Inserire codice articolo")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_ARCODART = this.w_ARCODART
    this.o_ARTIPART = this.w_ARTIPART
    this.o_ARUNMIS2 = this.w_ARUNMIS2
    this.o_ARTIPGES = this.w_ARTIPGES
    this.o_ARPROPRE = this.w_ARPROPRE
    this.o_ARTIPBAR = this.w_ARTIPBAR
    this.o_ARFLDISP = this.w_ARFLDISP
    this.o_ARSALCOM = this.w_ARSALCOM
    this.o_ARFLCOM1 = this.w_ARFLCOM1
    this.o_ARTIPCO1 = this.w_ARTIPCO1
    this.o_ARTPCONF = this.w_ARTPCONF
    this.o_VOLUME = this.w_VOLUME
    this.o_ARTIPCO2 = this.w_ARTIPCO2
    this.o_ARTPCON2 = this.w_ARTPCON2
    this.o_VOLUM2 = this.w_VOLUM2
    this.o_ARUTISER = this.w_ARUTISER
    this.o_ARDATINT = this.w_ARDATINT
    this.o_ARUMSUPP = this.w_ARUMSUPP
    this.o_ARARTPOS = this.w_ARARTPOS
    this.o_ARCODREP = this.w_ARCODREP
    this.o_ARFLLOTT = this.w_ARFLLOTT
    this.o_ARDISLOT = this.w_ARDISLOT
    this.o_ARGESMAT = this.w_ARGESMAT
    this.o_ARCODDIS = this.w_ARCODDIS
    this.o_ARKITIMB = this.w_ARKITIMB
    this.o_GEST = this.w_GEST
    * --- GSMA_MLI : Depends On
    this.GSMA_MLI.SaveDependsOn()
    * --- GSMA_MRP : Depends On
    this.GSMA_MRP.SaveDependsOn()
    * --- GSMA_APR : Depends On
    this.GSMA_APR.SaveDependsOn()
    * --- GSMA_MAL : Depends On
    this.GSMA_MAL.SaveDependsOn()
    * --- GSAR_MAR : Depends On
    this.GSAR_MAR.SaveDependsOn()
    * --- GSAR_MR2 : Depends On
    this.GSAR_MR2.SaveDependsOn()
    * --- GSMA_ANO : Depends On
    this.GSMA_ANO.SaveDependsOn()
    * --- GSMA_MDA : Depends On
    this.GSMA_MDA.SaveDependsOn()
    * --- GSMA_MAC : Depends On
    this.GSMA_MAC.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsma_aarPag1 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  resizeXpos=452
  resizeYpos=103
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODART_1_4 as StdField with uid="ZKXSPWVPQE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ARCODART", cQueryName = "ARCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 49738918,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=60, Top=9, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20)

  func oARCODART_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Query' OR (.w_AUTO<>'S' AND .cFunction<>'Edit'))
    endwith
   endif
  endfunc

  func oARCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.cFunction='Load' AND .w_AUTO<>'S',chkcodar('A',.w_ARCODART),.T.))
    endwith
    return bRes
  endfunc

  add object oARDESART_1_5 as StdField with uid="YKJKPAYVZL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 34661542,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=246, Top=9, InputMask=replicate('X',40)


  add object oARTIPART_1_6 as StdCombo with uid="YLPNRNIJCD",rtseq=6,rtrep=.f.,left=79,top=38,width=158,height=21, enabled=.f.;
    , ToolTipText = "Tipo articolo";
    , HelpContextID = 37479590;
    , cFormVar="w_ARTIPART",RowSource=""+"Articolo di fase", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPART_1_6.RadioValue()
    return(iif(this.value =1,'FS',;
    space(2)))
  endfunc
  func oARTIPART_1_6.GetRadio()
    this.Parent.oContained.w_ARTIPART = this.RadioValue()
    return .t.
  endfunc

  func oARTIPART_1_6.SetRadio()
    this.Parent.oContained.w_ARTIPART=trim(this.Parent.oContained.w_ARTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPART=='FS',1,;
      0)
  endfunc

  func oARTIPART_1_6.mHide()
    with this.Parent.oContained
      return (g_PRFA<>'S' OR .w_ARTIPART<>'FS')
    endwith
  endfunc


  add object oARTIPART_1_7 as StdCombo with uid="IIQNRFOMIB",rtseq=7,rtrep=.f.,left=79,top=38,width=158,height=21;
    , ToolTipText = "Tipo articolo";
    , HelpContextID = 37479590;
    , cFormVar="w_ARTIPART",RowSource=""+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPART_1_7.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'MP',;
    iif(this.value =4,'PH',;
    space(2))))))
  endfunc
  func oARTIPART_1_7.GetRadio()
    this.Parent.oContained.w_ARTIPART = this.RadioValue()
    return .t.
  endfunc

  func oARTIPART_1_7.SetRadio()
    this.Parent.oContained.w_ARTIPART=trim(this.Parent.oContained.w_ARTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPART=='PF',1,;
      iif(this.Parent.oContained.w_ARTIPART=='SE',2,;
      iif(this.Parent.oContained.w_ARTIPART=='MP',3,;
      iif(this.Parent.oContained.w_ARTIPART=='PH',4,;
      0))))
  endfunc

  func oARTIPART_1_7.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S' Or .w_ARTIPART='FS')
    endwith
  endfunc

  add object oARDESSUP_1_8 as StdMemo with uid="CJUKAAEMKJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ARDESSUP", cQueryName = "ARDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva dell'articolo",;
    HelpContextID = 267328342,;
   bGlobalFont=.t.,;
    Height=69, Width=363, Left=246, Top=37

  add object oARUNMIS1_1_11 as StdField with uid="TOTOIMWOXX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 93924151,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=147, Top=129, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS1"

  func oARUNMIS1_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS1_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS1_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS1_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUNMIS1_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS1
     i_obj.ecpSave()
  endproc

  add object oARUNMIS2_1_13 as StdField with uid="TCEQBUQQAT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ARUNMIS2", cQueryName = "ARUNMIS2",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale seconda unit� di misura",;
    HelpContextID = 93924152,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=261, Top=129, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS2"

  func oARUNMIS2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS2_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS2_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS2_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUNMIS2_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS2
     i_obj.ecpSave()
  endproc


  add object oAROPERAT_1_14 as StdCombo with uid="UAYOKYLNCY",rtseq=14,rtrep=.f.,left=314,top=129,width=35,height=21;
    , ToolTipText = "Operatore (moltip. o divis.) tra la 2^U.M. e la 1^U.M.";
    , HelpContextID = 31798438;
    , cFormVar="w_AROPERAT",RowSource=""+"X,"+"/", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAROPERAT_1_14.RadioValue()
    return(iif(this.value =1,'*',;
    iif(this.value =2,'/',;
    space(1))))
  endfunc
  func oAROPERAT_1_14.GetRadio()
    this.Parent.oContained.w_AROPERAT = this.RadioValue()
    return .t.
  endfunc

  func oAROPERAT_1_14.SetRadio()
    this.Parent.oContained.w_AROPERAT=trim(this.Parent.oContained.w_AROPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_AROPERAT=='*',1,;
      iif(this.Parent.oContained.w_AROPERAT=='/',2,;
      0))
  endfunc

  func oAROPERAT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  add object oARMOLTIP_1_16 as StdField with uid="ORBHPHKOYU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ARMOLTIP", cQueryName = "ARMOLTIP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di rapporto tra la 1^U.M. e la 2^U.M.",;
    HelpContextID = 9022294,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=359, Top=129, cSayPict='"99999.9999"', cGetPict='"99999.9999"'

  func oARMOLTIP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc


  add object oARFLUSEP_1_17 as StdCombo with uid="WZECSIGKXE",value=3,rtseq=17,rtrep=.f.,left=458,top=129,width=136,height=21;
    , ToolTipText = "Per quantit� e prezzo: viene ricalcolato il prezzo al cambio della quantit�";
    , HelpContextID = 1456982;
    , cFormVar="w_ARFLUSEP",RowSource=""+"Per quantit�,"+"Per quantit� e prezzo,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLUSEP_1_17.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oARFLUSEP_1_17.GetRadio()
    this.Parent.oContained.w_ARFLUSEP = this.RadioValue()
    return .t.
  endfunc

  func oARFLUSEP_1_17.SetRadio()
    this.Parent.oContained.w_ARFLUSEP=trim(this.Parent.oContained.w_ARFLUSEP)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLUSEP=='Q',1,;
      iif(this.Parent.oContained.w_ARFLUSEP=='S',2,;
      iif(this.Parent.oContained.w_ARFLUSEP=='',3,;
      0)))
  endfunc


  add object oARPREZUM_1_18 as StdCombo with uid="SWVMEMHUHQ",rtseq=18,rtrep=.f.,left=458,top=180,width=136,height=21;
    , ToolTipText = "Applica prezzo valido per UM";
    , HelpContextID = 102554451;
    , cFormVar="w_ARPREZUM",RowSource=""+"Principale,"+"Filtra,"+"Converti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPREZUM_1_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARPREZUM_1_18.GetRadio()
    this.Parent.oContained.w_ARPREZUM = this.RadioValue()
    return .t.
  endfunc

  func oARPREZUM_1_18.SetRadio()
    this.Parent.oContained.w_ARPREZUM=trim(this.Parent.oContained.w_ARPREZUM)
    this.value = ;
      iif(this.Parent.oContained.w_ARPREZUM=='N',1,;
      iif(this.Parent.oContained.w_ARPREZUM=='S',2,;
      iif(this.Parent.oContained.w_ARPREZUM=='C',3,;
      0)))
  endfunc

  add object oARFLINVE_1_19 as StdCheck with uid="TPYLDPGBZZ",rtseq=19,rtrep=.f.,left=458, top=205, caption="Inventario",;
    ToolTipText = "Se attivo: l'articolo viene considerato nell'elaborazione inventario",;
    HelpContextID = 173423435,;
    cFormVar="w_ARFLINVE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARFLINVE_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLINVE_1_19.GetRadio()
    this.Parent.oContained.w_ARFLINVE = this.RadioValue()
    return .t.
  endfunc

  func oARFLINVE_1_19.SetRadio()
    this.Parent.oContained.w_ARFLINVE=trim(this.Parent.oContained.w_ARFLINVE)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLINVE=='S',1,;
      0)
  endfunc

  add object oARSTASUP_1_20 as StdCheck with uid="CACFFYBIWL",rtseq=20,rtrep=.f.,left=458, top=232, caption="Stampa des.suppl.",;
    ToolTipText = "Se attivo stampa descrizione supplementare nei documenti abilitati",;
    HelpContextID = 249498454,;
    cFormVar="w_ARSTASUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSTASUP_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSTASUP_1_20.GetRadio()
    this.Parent.oContained.w_ARSTASUP = this.RadioValue()
    return .t.
  endfunc

  func oARSTASUP_1_20.SetRadio()
    this.Parent.oContained.w_ARSTASUP=trim(this.Parent.oContained.w_ARSTASUP)
    this.value = ;
      iif(this.Parent.oContained.w_ARSTASUP=='S',1,;
      0)
  endfunc

  add object oARASSIVA_1_21 as StdCheck with uid="JHZUIBADBA",rtseq=21,rtrep=.f.,left=458, top=259, caption="No IVA per assestamento",;
    ToolTipText = "Se attivo l'articolo non partecipa alla determinazione IVA per fatture da emettere",;
    HelpContextID = 100461383,;
    cFormVar="w_ARASSIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARASSIVA_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oARASSIVA_1_21.GetRadio()
    this.Parent.oContained.w_ARASSIVA = this.RadioValue()
    return .t.
  endfunc

  func oARASSIVA_1_21.SetRadio()
    this.Parent.oContained.w_ARASSIVA=trim(this.Parent.oContained.w_ARASSIVA)
    this.value = ;
      iif(this.Parent.oContained.w_ARASSIVA=='S',1,;
      0)
  endfunc

  add object oARCODIVA_1_23 as StdField with uid="DWJOZIIOBW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ARCODIVA", cQueryName = "ARCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA dell'articolo",;
    HelpContextID = 84478791,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ARCODIVA"

  func oARCODIVA_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODIVA_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODIVA_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oARCODIVA_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oARCODIVA_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ARCODIVA
     i_obj.ecpSave()
  endproc

  add object oARTIPOPE_1_24 as StdField with uid="UUUXVOUCFN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARTIPOPE", cQueryName = "ARTIPOPE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice obsoleto oppure inesistente",;
    ToolTipText = "Tipo operazione IVA",;
    HelpContextID = 71034037,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=147, Top=180, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ARCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ARTIPOPE"

  func oARTIPOPE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTIPOPE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTIPOPE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ARCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ARCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oARTIPOPE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'',this.parent.oContained
  endproc
  proc oARTIPOPE_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ARCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_ARTIPOPE
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_25 as StdField with uid="RCUFCSWUED",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31195594,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=215, Top=156, InputMask=replicate('X',35)

  add object oARCATCON_1_26 as StdField with uid="WZPRRDKGHV",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ARCATCON", cQueryName = "ARCATCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile dell'articolo",;
    HelpContextID = 324780,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=205, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_ARCATCON"

  func oARCATCON_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATCON_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATCON_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oARCATCON_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili articoli",'',this.parent.oContained
  endproc
  proc oARCATCON_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_ARCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_27 as StdField with uid="SVFMFDUGHB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3277258,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=215, Top=205, InputMask=replicate('X',35)

  add object oARCODFAM_1_28 as StdField with uid="BCTRLBCYTH",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ARCODFAM", cQueryName = "ARCODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia di appartenenza dell'articolo",;
    HelpContextID = 234288301,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=232, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_ARCODFAM"

  func oARCODFAM_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODFAM_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODFAM_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oARCODFAM_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oARCODFAM_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_ARCODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_29 as StdField with uid="GBMSHQKSPD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 120521162,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=215, Top=232, InputMask=replicate('X',35)

  add object oARGRUMER_1_30 as StdField with uid="TEBRQJJBDM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ARGRUMER", cQueryName = "ARGRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di appartenenza",;
    HelpContextID = 169626456,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_ARGRUMER"

  func oARGRUMER_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oARGRUMER_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARGRUMER_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oARGRUMER_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oARGRUMER_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_ARGRUMER
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_31 as StdField with uid="TJUUIIQKFL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 236847562,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=215, Top=259, InputMask=replicate('X',35)

  add object oARCATOMO_1_32 as StdField with uid="LDVVTTQAUH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ARCATOMO", cQueryName = "ARCATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di appartenenza",;
    HelpContextID = 67433643,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=286, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_ARCATOMO"

  func oARCATOMO_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATOMO_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATOMO_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oARCATOMO_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oARCATOMO_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_ARCATOMO
     i_obj.ecpSave()
  endproc

  add object oDESOMO_1_33 as StdField with uid="EHLLAVSMEU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESOMO", cQueryName = "DESOMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 73793994,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=215, Top=286, InputMask=replicate('X',35)


  add object oARTIPGES_1_34 as StdCombo with uid="FGOBRMASYX",rtseq=34,rtrep=.f.,left=178,top=315,width=152,height=21;
    , ToolTipText = "Tipo gestione per approvvigionamento articolo (se a consumo non verr� considerata per la gestione fabbisogni/PDA)";
    , HelpContextID = 63183705;
    , cFormVar="w_ARTIPGES",RowSource=""+"a fabbisogno,"+"a scorta,"+"a consumo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPGES_1_34.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARTIPGES_1_34.GetRadio()
    this.Parent.oContained.w_ARTIPGES = this.RadioValue()
    return .t.
  endfunc

  func oARTIPGES_1_34.SetRadio()
    this.Parent.oContained.w_ARTIPGES=trim(this.Parent.oContained.w_ARTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPGES=='F',1,;
      iif(this.Parent.oContained.w_ARTIPGES=='S',2,;
      iif(this.Parent.oContained.w_ARTIPGES=='C',3,;
      0)))
  endfunc


  add object oARPROPRE_1_35 as StdCombo with uid="UGICAUIYFL",rtseq=35,rtrep=.f.,left=574,top=315,width=136,height=21;
    , ToolTipText = "Provenienza preferenziale dell'articolo (se interna o C/Lavoro non verr� considerata per la gestione fabbisogni/PDA)";
    , HelpContextID = 54731957;
    , cFormVar="w_ARPROPRE",RowSource=""+"Interna,"+"Esterna,"+"C/Lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPROPRE_1_35.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    space(1)))))
  endfunc
  func oARPROPRE_1_35.GetRadio()
    this.Parent.oContained.w_ARPROPRE = this.RadioValue()
    return .t.
  endfunc

  func oARPROPRE_1_35.SetRadio()
    this.Parent.oContained.w_ARPROPRE=trim(this.Parent.oContained.w_ARPROPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ARPROPRE=='I',1,;
      iif(this.Parent.oContained.w_ARPROPRE=='E',2,;
      iif(this.Parent.oContained.w_ARPROPRE=='L',3,;
      0)))
  endfunc


  add object oARTIPBAR_1_36 as StdCombo with uid="EHVEYCRAMF",rtseq=36,rtrep=.f.,left=178,top=342,width=152,height=21;
    , ToolTipText = "Eventuale tipologia di barcode associata all'articolo usato per la generazione";
    , HelpContextID = 20702376;
    , cFormVar="w_ARTIPBAR",RowSource=""+"Genera EAN 8,"+"Genera EAN 13,"+"Codice articolo barcode,"+"No/altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPBAR_1_36.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'0',;
    space(1))))))
  endfunc
  func oARTIPBAR_1_36.GetRadio()
    this.Parent.oContained.w_ARTIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oARTIPBAR_1_36.SetRadio()
    this.Parent.oContained.w_ARTIPBAR=trim(this.Parent.oContained.w_ARTIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPBAR=='1',1,;
      iif(this.Parent.oContained.w_ARTIPBAR=='2',2,;
      iif(this.Parent.oContained.w_ARTIPBAR=='3',3,;
      iif(this.Parent.oContained.w_ARTIPBAR=='0',4,;
      0))))
  endfunc

  func oARTIPBAR_1_36.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc


  add object oTIPBAR_1_37 as StdCombo with uid="POMRVROPAO",rtseq=37,rtrep=.f.,left=574,top=342,width=136,height=21;
    , HelpContextID = 36908234;
    , cFormVar="w_TIPBAR",RowSource=""+"EAN 8,"+"EAN 13,"+"ALFA 39,"+"UPC A,"+"UPC E,"+"2D5,"+"2D5 interleave,"+"Farmaceutico,"+"198,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPBAR_1_37.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'0',;
    space(1))))))))))))
  endfunc
  func oTIPBAR_1_37.GetRadio()
    this.Parent.oContained.w_TIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPBAR_1_37.SetRadio()
    this.Parent.oContained.w_TIPBAR=trim(this.Parent.oContained.w_TIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPBAR=='1',1,;
      iif(this.Parent.oContained.w_TIPBAR=='2',2,;
      iif(this.Parent.oContained.w_TIPBAR=='3',3,;
      iif(this.Parent.oContained.w_TIPBAR=='4',4,;
      iif(this.Parent.oContained.w_TIPBAR=='5',5,;
      iif(this.Parent.oContained.w_TIPBAR=='6',6,;
      iif(this.Parent.oContained.w_TIPBAR=='7',7,;
      iif(this.Parent.oContained.w_TIPBAR=='8',8,;
      iif(this.Parent.oContained.w_TIPBAR=='9',9,;
      iif(this.Parent.oContained.w_TIPBAR=='0',10,;
      0))))))))))
  endfunc

  func oTIPBAR_1_37.mHide()
    with this.Parent.oContained
      return (.w_ARTIPBAR<>'3' OR .cFunction<>'Load')
    endwith
  endfunc

  func oTIPBAR_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((CALCBAR(.w_CODICE, .w_TIPBAR, 0) OR  .w_ARTIPBAR<>'3'))
    endwith
    return bRes
  endfunc


  add object oARFLDISP_1_38 as StdCombo with uid="RJKTQIGQPU",rtseq=38,rtrep=.f.,left=178,top=369,width=152,height=21;
    , ToolTipText = "Controlla la disponibilit� dell'articolo sui documenti";
    , HelpContextID = 84294486;
    , cFormVar="w_ARFLDISP",RowSource=""+"No,"+"Si,"+"S� con conferma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLDISP_1_38.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARFLDISP_1_38.GetRadio()
    this.Parent.oContained.w_ARFLDISP = this.RadioValue()
    return .t.
  endfunc

  func oARFLDISP_1_38.SetRadio()
    this.Parent.oContained.w_ARFLDISP=trim(this.Parent.oContained.w_ARFLDISP)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLDISP=='N',1,;
      iif(this.Parent.oContained.w_ARFLDISP=='S',2,;
      iif(this.Parent.oContained.w_ARFLDISP=='C',3,;
      0)))
  endfunc

  func oARFLDISP_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_ARFLLOTT<>'S' AND .w_ARFLLOTT<>'C'  AND .w_ARGESMAT <>'S') OR .w_ARDISLOT $ 'N-C')
    endwith
   endif
  endfunc

  func oARFLDISP_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !((g_PERDIS='S') OR (.w_ARFLLOTT $ 'SC')  OR  .w_ARGESMAT ='S')
         bRes=(cp_WarningMsg(thisform.msgFmt("Il controllo disponibilit� dell'articolo � stato attivato")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc


  add object oARFLDISC_1_39 as StdCombo with uid="WWNHZVRMSS",rtseq=39,rtrep=.f.,left=574,top=369,width=136,height=21;
    , ToolTipText = "Controlla la disponibilit� contabile dell'articolo sui documenti";
    , HelpContextID = 84294473;
    , cFormVar="w_ARFLDISC",RowSource=""+"No,"+"Si,"+"S� con conferma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLDISC_1_39.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARFLDISC_1_39.GetRadio()
    this.Parent.oContained.w_ARFLDISC = this.RadioValue()
    return .t.
  endfunc

  func oARFLDISC_1_39.SetRadio()
    this.Parent.oContained.w_ARFLDISC=trim(this.Parent.oContained.w_ARFLDISC)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLDISC=='N',1,;
      iif(this.Parent.oContained.w_ARFLDISC=='S',2,;
      iif(this.Parent.oContained.w_ARFLDISC=='C',3,;
      0)))
  endfunc

  add object oARCODCLA_1_40 as StdField with uid="CRWPSZIQML",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ARCODCLA", cQueryName = "ARCODCLA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia documento gestita come filtro di stampa/esclusione a livello documenti",;
    HelpContextID = 16184505,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=178, Top=397, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_ARCODCLA"

  func oARCODCLA_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODCLA_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODCLA_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oARCODCLA_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oARCODCLA_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_ARCODCLA
     i_obj.ecpSave()
  endproc

  add object oARSALCOM_1_41 as StdCheck with uid="AADBRBEPHQ",rtseq=41,rtrep=.f.,left=512, top=400, caption="Gestione saldi di commessa",;
    ToolTipText = "Se attivo gestisce i saldi di commessa per quantit�",;
    HelpContextID = 8647853,;
    cFormVar="w_ARSALCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSALCOM_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSALCOM_1_41.GetRadio()
    this.Parent.oContained.w_ARSALCOM = this.RadioValue()
    return .t.
  endfunc

  func oARSALCOM_1_41.SetRadio()
    this.Parent.oContained.w_ARSALCOM=trim(this.Parent.oContained.w_ARSALCOM)
    this.value = ;
      iif(this.Parent.oContained.w_ARSALCOM=='S',1,;
      0)
  endfunc

  add object oARDTINVA_1_42 as StdField with uid="MVYVIFIELK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ARDTINVA", cQueryName = "ARDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 173939527,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=178, Top=423, tabstop=.f.

  add object oARDTOBSO_1_43 as StdField with uid="ARGYVGTINE",rtseq=43,rtrep=.f.,;
    cFormVar = "w_ARDTOBSO", cQueryName = "ARDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale l'articolo � obsoleto",;
    HelpContextID = 21095595,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=379, Top=423, tabstop=.f.


  add object oARFLCOM1_1_44 as StdCombo with uid="UQLQMBYZLP",rtseq=44,rtrep=.f.,left=574,top=423,width=136,height=21;
    , ToolTipText = "Articolo gestito a commessa personalizzata";
    , HelpContextID = 84526281;
    , cFormVar="w_ARFLCOM1",RowSource=""+"Standard,"+"Personalizzato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLCOM1_1_44.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oARFLCOM1_1_44.GetRadio()
    this.Parent.oContained.w_ARFLCOM1 = this.RadioValue()
    return .t.
  endfunc

  func oARFLCOM1_1_44.SetRadio()
    this.Parent.oContained.w_ARFLCOM1=trim(this.Parent.oContained.w_ARFLCOM1)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCOM1=='N',1,;
      iif(this.Parent.oContained.w_ARFLCOM1=='S',2,;
      0))
  endfunc

  func oARFLCOM1_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPGES="F" and .cFunction<>"Query" and .w_ARSALCOM='S')
    endwith
   endif
  endfunc

  func oARFLCOM1_1_44.mHide()
    with this.Parent.oContained
      return (VARTYPE(g_OLDCOM)='L' AND !g_OLDCOM)
    endwith
  endfunc


  add object oARFLCOM1_1_45 as StdCombo with uid="ZBKCEEXCQW",rtseq=45,rtrep=.f.,left=574,top=423,width=136,height=21;
    , ToolTipText = "Articolo gestito a commessa personalizzata";
    , HelpContextID = 84526281;
    , cFormVar="w_ARFLCOM1",RowSource=""+"Standard,"+"Personalizzato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLCOM1_1_45.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oARFLCOM1_1_45.GetRadio()
    this.Parent.oContained.w_ARFLCOM1 = this.RadioValue()
    return .t.
  endfunc

  func oARFLCOM1_1_45.SetRadio()
    this.Parent.oContained.w_ARFLCOM1=trim(this.Parent.oContained.w_ARFLCOM1)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCOM1=='N',1,;
      iif(this.Parent.oContained.w_ARFLCOM1=='S',2,;
      0))
  endfunc

  func oARFLCOM1_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPGES="F" and .cFunction<>"Query" and .w_ARSALCOM='S')
    endwith
   endif
  endfunc

  func oARFLCOM1_1_45.mHide()
    with this.Parent.oContained
      return ((VARTYPE(g_OLDCOM)='L' AND g_OLDCOM) OR (g_MODA<>"S" AND .w_ARPROPRE<>"E"))
    endwith
  endfunc


  add object oARFLCOM1_1_46 as StdCombo with uid="VRYBZSVNQU",rtseq=46,rtrep=.f.,left=574,top=423,width=136,height=21;
    , ToolTipText = "Articolo gestito a commessa personalizzata";
    , HelpContextID = 84526281;
    , cFormVar="w_ARFLCOM1",RowSource=""+"Standard,"+"Personalizzato,"+"Pers. + nettificaz.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLCOM1_1_46.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARFLCOM1_1_46.GetRadio()
    this.Parent.oContained.w_ARFLCOM1 = this.RadioValue()
    return .t.
  endfunc

  func oARFLCOM1_1_46.SetRadio()
    this.Parent.oContained.w_ARFLCOM1=trim(this.Parent.oContained.w_ARFLCOM1)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCOM1=='N',1,;
      iif(this.Parent.oContained.w_ARFLCOM1=='S',2,;
      iif(this.Parent.oContained.w_ARFLCOM1=='C',3,;
      0)))
  endfunc

  func oARFLCOM1_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPGES="F" and .cFunction<>"Query" and .w_ARSALCOM='S')
    endwith
   endif
  endfunc

  func oARFLCOM1_1_46.mHide()
    with this.Parent.oContained
      return ((VARTYPE(g_OLDCOM)='L' AND g_OLDCOM) OR (g_MODA<>"S" AND .w_ARPROPRE="E"))
    endwith
  endfunc


  add object oObj_1_48 as cp_runprogram with uid="ZHZYVSMEQZ",left=192, top=528, width=492,height=17,;
    caption='GSMA_BBK(w_ARCODART,w_TIPBAR,A)',;
   bGlobalFont=.t.,;
    prg="GSMA_BBK(w_arcodart,w_tipbar,'A')",;
    cEvent = "w_TIPBAR Changed,w_ARCODART Changed",;
    nPag=1;
    , ToolTipText = "Check codice a barre";
    , HelpContextID = 221576242


  add object oObj_1_70 as cp_runprogram with uid="MRFIJJUQDK",left=-1, top=550, width=177,height=17,;
    caption='GSMA_BDV',;
   bGlobalFont=.t.,;
    prg="GSMA_BDV",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 262909116


  add object oObj_1_71 as cp_runprogram with uid="RQYTKDBKVH",left=-1, top=528, width=177,height=17,;
    caption='GSMA_BCV(I)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCV('I')",;
    cEvent = "Insert end",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 5339076


  add object oObj_1_72 as cp_runprogram with uid="SDILTXGQQU",left=-1, top=506, width=177,height=17,;
    caption='GSMA_BCV(U)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCV('U')",;
    cEvent = "Update start",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 5336004


  add object oBtn_1_75 as StdButton with uid="ASBRNZBJTY",left=669, top=58, width=48,height=45,;
    CpPicture="BMP\CODICI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle chiavi dell'articolo/servizio selezionato";
    , HelpContextID = 130186970;
    , tabstop=.f., Caption='Co\<dici';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        do GSMA_KCA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART) AND .cFunction<>'Load')
      endwith
    endif
  endfunc


  add object oLinkPC_1_76 as StdButton with uid="ZTNCHVNEGR",left=619, top=105, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione dei listini";
    , HelpContextID = 58645834;
    , tabstop=.f., Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_76.Click()
      this.Parent.oContained.GSMA_MLI.LinkPCClick()
    endproc

  func oLinkPC_1_76.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc


  add object oBtn_1_77 as StdButton with uid="JQDEJOOGQG",left=669, top=105, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai saldi dell'articolo/servizio selezionato";
    , HelpContextID = 26170586;
    , tabstop=.f., Caption='S\<aldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      with this.Parent.oContained
        do GSMA_KCS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_77.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc


  add object oBtn_1_83 as StdButton with uid="ZRGCXZPNPC",left=619, top=152, width=48,height=45,;
    CpPicture="BMP\ULTVEN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere agli ultimi prezzi ordinati per cliente";
    , HelpContextID = 124367882;
    , tabstop=.f., Caption='\<Prezzi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_83.Click()
      do GSMA_KUC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_83.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc


  add object oBtn_1_84 as StdButton with uid="URLEIHOKLU",left=669, top=152, width=48,height=45,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere agli ultimi costi ordinati per fornitore";
    , HelpContextID = 25090010;
    , tabstop=.f., Caption='C\<osti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_84.Click()
      do GSMA_KUF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_84.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc


  add object oObj_1_85 as cp_runprogram with uid="QJGAQNUGYH",left=192, top=506, width=475,height=17,;
    caption='GSMA_BCV(P)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCV('P')",;
    cEvent = "w_ARTIPGES Changed,w_ARPROPRE Changed,w_ARCODDIS Changed",;
    nPag=1;
    , ToolTipText = "Produzione";
    , HelpContextID = 5337284


  add object oObj_1_86 as cp_runprogram with uid="FWMNZFXIFX",left=192, top=572, width=486,height=17,;
    caption='GSMA_BPD(A)',;
   bGlobalFont=.t.,;
    prg="gsma_bpd('A')",;
    cEvent = "w_ARUNMIS1 Changed,w_ARGESMAT Changed,w_ARUNMIS2 Changed",;
    nPag=1;
    , ToolTipText = "Esegue controlli flag gestione lotti";
    , HelpContextID = 5341142


  add object oObj_1_87 as cp_runprogram with uid="TTWVPWKSGT",left=192, top=550, width=160,height=17,;
    caption='GSMA_KCA',;
   bGlobalFont=.t.,;
    prg="GSMA_KCA",;
    cEvent = "CaricaBar",;
    nPag=1;
    , ToolTipText = "Evento scatenato da gsma_bcv (aggiorno codici)";
    , HelpContextID = 145468583


  add object oObj_1_88 as cp_runprogram with uid="DFKUAIXSMA",left=-1, top=572, width=177,height=17,;
    caption='GSMA_BCV(B)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCV('B')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 5340868


  add object oObj_1_90 as cp_runprogram with uid="PHCYNDLOLZ",left=-1, top=594, width=177,height=17,;
    caption='GSMA_BPD(B)',;
   bGlobalFont=.t.,;
    prg="gsma_bpd('B')",;
    cEvent = "Record Updated",;
    nPag=1;
    , ToolTipText = "Esegue controlli flag gestione lotti";
    , HelpContextID = 5340886


  add object oObj_1_96 as cp_runprogram with uid="JUDVSZVFMK",left=388, top=550, width=292,height=18,;
    caption='GSMA_BPD(M)',;
   bGlobalFont=.t.,;
    prg="GSMA_BPD('M')",;
    cEvent = "w_ARGESMAT Changed",;
    nPag=1;
    , HelpContextID = 5338070


  add object oObj_1_97 as cp_runprogram with uid="HTFGSSBTIH",left=192, top=594, width=202,height=17,;
    caption='GSMA_BPD(C)',;
   bGlobalFont=.t.,;
    prg="gsma_bpd('C')",;
    cEvent = "w_ARFLLOTT Changed",;
    nPag=1;
    , ToolTipText = "Esegue controlli flag gestione lotti";
    , HelpContextID = 5340630

  add object oDESCLA_1_99 as StdField with uid="PWTENNSHDQ",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 42074570,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=214, Top=397, InputMask=replicate('X',30)

  add object oTIDESCRI_1_101 as StdField with uid="VJWAEWROHS",rtseq=166,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 1109121,;
   bGlobalFont=.t.,;
    Height=21, Width=215, Left=238, Top=180, InputMask=replicate('X',30)


  add object oBtn_1_103 as StdButton with uid="SCOLEFMZCS",left=619, top=11, width=48,height=45,;
    CpPicture="BMP\CATTURA.BMP", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 21047846;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp', caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_103.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_103.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_103.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S' ) or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_104 as StdButton with uid="YHHCTRXKSP",left=669, top=11, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 100496272;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_104.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_104.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_104.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S' ) or Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_105 as StdButton with uid="BJRFWTRDLX",left=619, top=58, width=48,height=45,;
    CpPicture="BMP\SCANNER.BMP", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 63569190;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp', caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_105.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_105.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S' and g_DOCM='S') or Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_106 as StdButton with uid="YPRRGQGCBA",left=669, top=198, width=48,height=45,;
    CpPicture="BMP\avanzate.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione basi di calcolo";
    , HelpContextID = 52015438;
    , tabstop=.f., Caption='\<Basi calc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_106.Click()
      do GSVA_KBC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_106.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_1_106.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_VEFA<>'S')
     endwith
    endif
  endfunc


  add object oObj_1_113 as cp_runprogram with uid="NTAVFRUUTC",left=-1, top=616, width=177,height=17,;
    caption='GSMA_BVC',;
   bGlobalFont=.t.,;
    prg="GSMA_BVC",;
    cEvent = "w_ARFLCOMM Changed",;
    nPag=1;
    , ToolTipText = "Esegue controllo congruenza tra gestione a commessa";
    , HelpContextID = 262909097


  add object oARCHKUCA_1_115 as StdCombo with uid="EBAPTUAJZL",rtseq=179,rtrep=.f.,left=574,top=291,width=136,height=21;
    , ToolTipText = "Parametrizzazione del controllo prezzo da effettuare sul documento";
    , HelpContextID = 24251207;
    , cFormVar="w_ARCHKUCA",RowSource=""+"nessuno,"+"su U.C.A.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARCHKUCA_1_115.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'U',;
    space(1))))
  endfunc
  func oARCHKUCA_1_115.GetRadio()
    this.Parent.oContained.w_ARCHKUCA = this.RadioValue()
    return .t.
  endfunc

  func oARCHKUCA_1_115.SetRadio()
    this.Parent.oContained.w_ARCHKUCA=trim(this.Parent.oContained.w_ARCHKUCA)
    this.value = ;
      iif(this.Parent.oContained.w_ARCHKUCA=='N',1,;
      iif(this.Parent.oContained.w_ARCHKUCA=='U',2,;
      0))
  endfunc

  add object oStr_1_50 as StdString with uid="WMEGSDGWJZ",Visible=.t., Left=3, Top=9,;
    Alignment=1, Width=54, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="UPEXAQNTFQ",Visible=.t., Left=9, Top=259,;
    Alignment=1, Width=134, Height=15,;
    Caption="Gruppo merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="FKDVCFJUFX",Visible=.t., Left=9, Top=286,;
    Alignment=1, Width=134, Height=15,;
    Caption="Categoria omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="UYSISHFWBT",Visible=.t., Left=257, Top=423,;
    Alignment=1, Width=121, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="PAHPRGWVHH",Visible=.t., Left=9, Top=129,;
    Alignment=1, Width=134, Height=15,;
    Caption="1^Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="MYIEABXYOO",Visible=.t., Left=9, Top=205,;
    Alignment=1, Width=134, Height=15,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="DMNEFGTJZL",Visible=.t., Left=9, Top=156,;
    Alignment=1, Width=134, Height=15,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="NLKOPFNCIN",Visible=.t., Left=359, Top=114,;
    Alignment=0, Width=91, Height=15,;
    Caption="Parametro"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="ACMKLISIPI",Visible=.t., Left=314, Top=114,;
    Alignment=0, Width=35, Height=15,;
    Caption="Oper."  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="PDDUZGMNIA",Visible=.t., Left=207, Top=129,;
    Alignment=1, Width=51, Height=15,;
    Caption="2^U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="ZQKOFUSNXY",Visible=.t., Left=202, Top=129,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="YBOHLANESI",Visible=.t., Left=9, Top=232,;
    Alignment=1, Width=134, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="EDHDYMQKLV",Visible=.t., Left=27, Top=423,;
    Alignment=1, Width=147, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="BFJXHTMRYF",Visible=.t., Left=40, Top=342,;
    Alignment=1, Width=134, Height=15,;
    Caption="Barcode:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="RWNIHQMIRS",Visible=.t., Left=40, Top=369,;
    Alignment=1, Width=134, Height=15,;
    Caption="Check disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="OPTXNQZUFF",Visible=.t., Left=40, Top=315,;
    Alignment=1, Width=134, Height=18,;
    Caption="Gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="DMKAEEFDLE",Visible=.t., Left=433, Top=315,;
    Alignment=1, Width=135, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="CMJDJKRWMS",Visible=.t., Left=433, Top=342,;
    Alignment=1, Width=135, Height=18,;
    Caption="Tipo barcode:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (.w_ARTIPBAR<>'3' OR .cFunction<>'Load')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="KDWOVVBZJX",Visible=.t., Left=433, Top=369,;
    Alignment=1, Width=135, Height=18,;
    Caption="Check dis.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="WUQBNYJZWN",Visible=.t., Left=0, Top=397,;
    Alignment=1, Width=174, Height=18,;
    Caption="Tipologia riga documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="IIRGQPDFCE",Visible=.t., Left=9, Top=180,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="BXQFSNENXS",Visible=.t., Left=458, Top=114,;
    Alignment=0, Width=157, Height=15,;
    Caption="U.M. separate"  ;
  , bGlobalFont=.t.

  add object oStr_1_109 as StdString with uid="CXLMQNGTZK",Visible=.t., Left=460, Top=162,;
    Alignment=0, Width=157, Height=18,;
    Caption="Appl. prezzo valido per U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_110 as StdString with uid="EZMLKKYMEX",Visible=.t., Left=4, Top=40,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_110.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="OMAZMBLWEB",Visible=.t., Left=460, Top=423,;
    Alignment=1, Width=108, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="QHIITNTXAG",Visible=.t., Left=454, Top=290,;
    Alignment=1, Width=114, Height=15,;
    Caption="Controllo prezzo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsma_aarPag2 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_1 as StdField with uid="WIYHUAIJFS",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=75, Top=9, InputMask=replicate('X',20)

  add object oDESC_2_2 as StdField with uid="HHHLTSFEVH",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=262, Top=9, InputMask=replicate('X',40)

  add object oUM1_2_3 as StdField with uid="DYZDVUESTH",rtseq=58,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 143071418,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=94, Top=41, InputMask=replicate('X',3)

  add object oUM2_2_4 as StdField with uid="PZNXKTEZPB",rtseq=59,rtrep=.f.,;
    cFormVar = "w_UM2", cQueryName = "UM2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 143067322,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=445, Top=41, InputMask=replicate('X',3)

  func oUM2_2_4.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARPESNET_2_5 as StdField with uid="ZWJTPPWWKL",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ARPESNET", cQueryName = "ARPESNET",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa netta unitaria in kg.",;
    HelpContextID = 183491418,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=94, Top=69, cSayPict='"99999.999"', cGetPict='"99999.999"'

  add object oARPESLOR_2_6 as StdField with uid="SOHCXXIFAO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ARPESLOR", cQueryName = "ARPESLOR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa lorda unitaria in kg.",;
    HelpContextID = 118498472,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=94, Top=97, cSayPict='"99999.999"', cGetPict='"99999.999"'

  add object oARTIPCO1_2_7 as StdField with uid="KZRIJCXWZR",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ARTIPCO1", cQueryName = "ARTIPCO1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 3925193,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=94, Top=125, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_ARTIPCO1"

  func oARTIPCO1_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
      if .not. empty(.w_ARTPCONF)
        bRes2=.link_2_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oARTIPCO1_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTIPCO1_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oARTIPCO1_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oARTIPCO1_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_ARTIPCO1
     i_obj.ecpSave()
  endproc

  add object oARTPCONF_2_8 as StdField with uid="LFGVMLLLMW",rtseq=63,rtrep=.f.,;
    cFormVar = "w_ARTPCONF", cQueryName = "ARTPCONF",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo confezione",;
    HelpContextID = 84206772,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=94, Top=153, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CON_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_ARTIPCO1", oKey_2_1="TCCODCON", oKey_2_2="this.w_ARTPCONF"

  func oARTPCONF_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARTIPCO1))
    endwith
   endif
  endfunc

  func oARTPCONF_2_8.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTIPCO1))
    endwith
  endfunc

  func oARTPCONF_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTPCONF_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTPCONF_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_COLL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ARTIPCO1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStr(this.Parent.oContained.w_ARTIPCO1)
    endif
    do cp_zoom with 'CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(this.parent,'oARTPCONF_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Confezioni",'',this.parent.oContained
  endproc
  proc oARTPCONF_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TCCODICE=w_ARTIPCO1
     i_obj.w_TCCODCON=this.parent.oContained.w_ARTPCONF
     i_obj.ecpSave()
  endproc

  add object oDESTCF_2_9 as StdField with uid="VFQJQZJMXK",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESTCF", cQueryName = "DESTCF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 234947018,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=160, Top=153, InputMask=replicate('X',30)

  func oDESTCF_2_9.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTIPCO1))
    endwith
  endfunc

  add object oARPZCONF_2_10 as StdField with uid="FLPBQTUBAD",rtseq=65,rtrep=.f.,;
    cFormVar = "w_ARPZCONF", cQueryName = "ARPZCONF",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero pezzi per confezione",;
    HelpContextID = 83567796,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=94, Top=181, cSayPict="v_PQ(11)", cGetPict="v_GQ(11)"

  func oARPZCONF_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARTPCONF))
    endwith
   endif
  endfunc

  func oARPZCONF_2_10.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTPCONF))
    endwith
  endfunc

  add object oARCOCOL1_2_11 as StdField with uid="UGWCIGSLOB",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ARCOCOL1", cQueryName = "ARCOCOL1",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Specificare il numero di confezioni per collo",;
    ToolTipText = "Numero di confezioni per collo riferita alla 1a U.M.",;
    HelpContextID = 84341961,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=289, Top=181, cSayPict='"999999"', cGetPict='"999999"'

  func oARCOCOL1_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARTPCONF))
    endwith
   endif
  endfunc

  func oARCOCOL1_2_11.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTPCONF))
    endwith
  endfunc

  add object oARDIMLUN_2_12 as StdField with uid="IHQJMOJSQV",rtseq=67,rtrep=.f.,;
    cFormVar = "w_ARDIMLUN", cQueryName = "ARDIMLUN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in lunghezza della confezione",;
    HelpContextID = 143858516,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=94, Top=209, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oARDIMLAR_2_13 as StdField with uid="RUVBVMDGLY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ARDIMLAR", cQueryName = "ARDIMLAR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in larghezza della confezione",;
    HelpContextID = 124576936,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=94, Top=237, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oARDIMALT_2_14 as StdField with uid="PJKRFDSYJK",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ARDIMALT", cQueryName = "ARDIMALT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in altezza della confezione",;
    HelpContextID = 40690854,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=94, Top=265, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oVOLUME_2_15 as StdField with uid="YJPJNFLHJF",rtseq=70,rtrep=.f.,;
    cFormVar = "w_VOLUME", cQueryName = "VOLUME",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Volume della confezione",;
    HelpContextID = 241198762,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=185, Top=265

  add object oARUMVOLU_2_16 as StdField with uid="SFRXQZZQWN",rtseq=71,rtrep=.f.,;
    cFormVar = "w_ARUMVOLU", cQueryName = "ARUMVOLU",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura riferita al volume",;
    HelpContextID = 64476325,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=312, Top=293, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUMVOLU"

  func oARUMVOLU_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUMVOLU_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUMVOLU_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUMVOLU_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUMVOLU_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUMVOLU
     i_obj.ecpSave()
  endproc

  add object oARUMDIME_2_17 as StdField with uid="HIBXYAOIOK",rtseq=72,rtrep=.f.,;
    cFormVar = "w_ARUMDIME", cQueryName = "ARUMDIME",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura delle dimensioni",;
    HelpContextID = 184014005,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=312, Top=209, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUMDIME"

  func oARUMDIME_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VOLUME<>0)
    endwith
   endif
  endfunc

  func oARUMDIME_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUMDIME_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUMDIME_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUMDIME_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUMDIME_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUMDIME
     i_obj.ecpSave()
  endproc

  add object oARDESVOL_2_18 as StdField with uid="AQXULPPTLJ",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ARDESVOL", cQueryName = "ARDESVOL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Volume unitario del prodotto",;
    HelpContextID = 219210926,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=94, Top=293, InputMask=replicate('X',15)

  add object oARFLCONA_2_19 as StdCheck with uid="SAGISQZEKR",rtseq=74,rtrep=.f.,left=97, top=318, caption="Articolo con imballaggio CONAI",;
    ToolTipText = "Tipologia articolo in funzione del contributo ambientale (CONAI)",;
    HelpContextID = 84526265,;
    cFormVar="w_ARFLCONA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARFLCONA_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLCONA_2_19.GetRadio()
    this.Parent.oContained.w_ARFLCONA = this.RadioValue()
    return .t.
  endfunc

  func oARFLCONA_2_19.SetRadio()
    this.Parent.oContained.w_ARFLCONA=trim(this.Parent.oContained.w_ARFLCONA)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCONA=='S',1,;
      0)
  endfunc

  add object oARPESNE2_2_20 as StdField with uid="BZZBQTSRDB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ARPESNE2", cQueryName = "ARPESNE2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa netta unitaria in kg. riferita alla 2a U.M.",;
    HelpContextID = 183491384,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=445, Top=69, cSayPict='"99999.999"', cGetPict='"99999.999"'

  func oARPESNE2_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARPESNE2_2_20.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARPESLO2_2_21 as StdField with uid="IOKWKQHYUR",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ARPESLO2", cQueryName = "ARPESLO2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa lorda unitaria in kg. riferita alla 2a U.M.",;
    HelpContextID = 118498504,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=445, Top=97, cSayPict='"99999.999"', cGetPict='"99999.999"'

  func oARPESLO2_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARPESLO2_2_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARTIPCO2_2_22 as StdField with uid="AAQBYFJZPY",rtseq=77,rtrep=.f.,;
    cFormVar = "w_ARTIPCO2", cQueryName = "ARTIPCO2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 3925192,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=445, Top=125, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_ARTIPCO2"

  func oARTIPCO2_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARTIPCO2_2_22.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  func oARTIPCO2_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
      if .not. empty(.w_ARTPCON2)
        bRes2=.link_2_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oARTIPCO2_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTIPCO2_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oARTIPCO2_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oARTIPCO2_2_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_ARTIPCO2
     i_obj.ecpSave()
  endproc

  add object oARTPCON2_2_23 as StdField with uid="JYOMUBEMKH",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ARTPCON2", cQueryName = "ARTPCON2",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo confezione riferita alla 2a U.M.",;
    HelpContextID = 84206792,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=445, Top=153, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CON_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_ARTIPCO2", oKey_2_1="TCCODCON", oKey_2_2="this.w_ARTPCON2"

  func oARTPCON2_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTIPCO2))
    endwith
   endif
  endfunc

  func oARTPCON2_2_23.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTIPCO2))
    endwith
  endfunc

  func oARTPCON2_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTPCON2_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTPCON2_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_COLL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ARTIPCO2)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStr(this.Parent.oContained.w_ARTIPCO2)
    endif
    do cp_zoom with 'CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(this.parent,'oARTPCON2_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Confezioni",'',this.parent.oContained
  endproc
  proc oARTPCON2_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TCCODICE=w_ARTIPCO2
     i_obj.w_TCCODCON=this.parent.oContained.w_ARTPCON2
     i_obj.ecpSave()
  endproc

  add object oDESTCF2_2_24 as StdField with uid="OJYWEHYREP",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESTCF2", cQueryName = "DESTCF2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 234947018,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=513, Top=153, InputMask=replicate('X',30)

  func oDESTCF2_2_24.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTIPCO2))
    endwith
  endfunc

  add object oARPZCON2_2_25 as StdField with uid="AJZMMBQFDN",rtseq=80,rtrep=.f.,;
    cFormVar = "w_ARPZCON2", cQueryName = "ARPZCON2",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero pezzi per confezione riferita alla 2a U.M.",;
    HelpContextID = 83567816,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=445, Top=181, cSayPict="v_PQ(11)", cGetPict="v_GQ(11)"

  func oARPZCON2_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTPCON2))
    endwith
   endif
  endfunc

  func oARPZCON2_2_25.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR  EMPTY(.w_ARTPCON2))
    endwith
  endfunc

  add object oARCOCOL2_2_26 as StdField with uid="VNDJLZAFAN",rtseq=81,rtrep=.f.,;
    cFormVar = "w_ARCOCOL2", cQueryName = "ARCOCOL2",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Specificare il numero di confezioni per collo",;
    ToolTipText = "Numero di confezioni per collo riferita alla 2a U.M.",;
    HelpContextID = 84341960,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=647, Top=181, cSayPict='"999999"', cGetPict='"999999"'

  func oARCOCOL2_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2) AND NOT EMPTY(.w_ARTPCON2))
    endwith
   endif
  endfunc

  func oARCOCOL2_2_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTPCON2))
    endwith
  endfunc

  add object oARDIMLU2_2_27 as StdField with uid="ORQIMKNKCR",rtseq=82,rtrep=.f.,;
    cFormVar = "w_ARDIMLU2", cQueryName = "ARDIMLU2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in lunghezza della confezione riferita alla 2a U.M.",;
    HelpContextID = 143858488,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=445, Top=209, cSayPict='"9999.9"', cGetPict='"9999.9"'

  func oARDIMLU2_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARDIMLU2_2_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARDIMLA2_2_28 as StdField with uid="PXHVUSMAQX",rtseq=83,rtrep=.f.,;
    cFormVar = "w_ARDIMLA2", cQueryName = "ARDIMLA2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in larghezza della confezione riferita alla 2a U.M.",;
    HelpContextID = 124576968,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=445, Top=237, cSayPict='"9999.9"', cGetPict='"9999.9"'

  func oARDIMLA2_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARDIMLA2_2_28.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARDIMAL2_2_29 as StdField with uid="THKWYESAMB",rtseq=84,rtrep=.f.,;
    cFormVar = "w_ARDIMAL2", cQueryName = "ARDIMAL2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in altezza della confezione riferita alla 2a U.M.",;
    HelpContextID = 40690888,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=445, Top=265, cSayPict='"9999.9"', cGetPict='"9999.9"'

  func oARDIMAL2_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARDIMAL2_2_29.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oVOLUM2_2_30 as StdField with uid="KMMGCVUVEG",rtseq=85,rtrep=.f.,;
    cFormVar = "w_VOLUM2", cQueryName = "VOLUM2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Volume della confezione riferito allla 2^ U.M.",;
    HelpContextID = 23094954,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=532, Top=265

  func oVOLUM2_2_30.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARUMVOL2_2_31 as StdField with uid="BVRBPKHUJA",rtseq=86,rtrep=.f.,;
    cFormVar = "w_ARUMVOL2", cQueryName = "ARUMVOL2",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura riferita al volume riferita alla 2a U.M.",;
    HelpContextID = 64476360,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=666, Top=293, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUMVOL2"

  func oARUMVOL2_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARUMVOL2_2_31.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  func oARUMVOL2_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUMVOL2_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUMVOL2_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUMVOL2_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUMVOL2_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUMVOL2
     i_obj.ecpSave()
  endproc

  add object oARUMDIM2_2_32 as StdField with uid="ZWXJXTBNFH",rtseq=87,rtrep=.f.,;
    cFormVar = "w_ARUMDIM2", cQueryName = "ARUMDIM2",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura delle dimensioni riferite alla 2a U.M.",;
    HelpContextID = 184014024,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=666, Top=210, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUMDIM2"

  func oARUMDIM2_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VOLUM2<>0 AND NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARUMDIM2_2_32.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  func oARUMDIM2_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUMDIM2_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUMDIM2_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUMDIM2_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUMDIM2_2_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUMDIM2
     i_obj.ecpSave()
  endproc

  add object oARDESVO2_2_33 as StdField with uid="JYFZGEFRPN",rtseq=88,rtrep=.f.,;
    cFormVar = "w_ARDESVO2", cQueryName = "ARDESVO2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Volume unitario del prodotto riferita alla 2a U.M.",;
    HelpContextID = 219210952,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=445, Top=293, InputMask=replicate('X',15)

  func oARDESVO2_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  func oARDESVO2_2_33.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oARFLCON2_2_34 as StdCheck with uid="WPDCSSCMJF",rtseq=89,rtrep=.f.,left=445, top=318, caption="Articolo con imballaggio CONAI",;
    ToolTipText = "Tipologia articolo in funzione del contributo ambientale (CONAI)",;
    HelpContextID = 84526280,;
    cFormVar="w_ARFLCON2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARFLCON2_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLCON2_2_34.GetRadio()
    this.Parent.oContained.w_ARFLCON2 = this.RadioValue()
    return .t.
  endfunc

  func oARFLCON2_2_34.SetRadio()
    this.Parent.oContained.w_ARFLCON2=trim(this.Parent.oContained.w_ARFLCON2)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCON2=='S',1,;
      0)
  endfunc

  func oARFLCON2_2_34.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc


  add object oARUTISER_2_35 as StdCombo with uid="VIFESWZJOE",rtseq=90,rtrep=.f.,left=113,top=354,width=82,height=22;
    , ToolTipText = "Tipologia utilizzo dati intra";
    , HelpContextID = 257895256;
    , cFormVar="w_ARUTISER",RowSource=""+"Servizi,"+"Beni", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oARUTISER_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oARUTISER_2_35.GetRadio()
    this.Parent.oContained.w_ARUTISER = this.RadioValue()
    return .t.
  endfunc

  func oARUTISER_2_35.SetRadio()
    this.Parent.oContained.w_ARUTISER=trim(this.Parent.oContained.w_ARUTISER)
    this.value = ;
      iif(this.Parent.oContained.w_ARUTISER=='S',1,;
      iif(this.Parent.oContained.w_ARUTISER=='N',2,;
      0))
  endfunc

  func oARUTISER_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc


  add object oARPERSER_2_36 as StdCombo with uid="MAJAXJQOED",rtseq=91,rtrep=.f.,left=502,top=355,width=92,height=22;
    , ToolTipText = "Modalit� di erogazione";
    , HelpContextID = 266328920;
    , cFormVar="w_ARPERSER",RowSource=""+"Istantanea,"+"A pi� riprese", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oARPERSER_2_36.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oARPERSER_2_36.GetRadio()
    this.Parent.oContained.w_ARPERSER = this.RadioValue()
    return .t.
  endfunc

  func oARPERSER_2_36.SetRadio()
    this.Parent.oContained.w_ARPERSER=trim(this.Parent.oContained.w_ARPERSER)
    this.value = ;
      iif(this.Parent.oContained.w_ARPERSER=='I',1,;
      iif(this.Parent.oContained.w_ARPERSER=='R',2,;
      0))
  endfunc

  func oARPERSER_2_36.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
  endfunc

  add object oARNOMENC_2_37 as StdField with uid="VWSAINRXLS",rtseq=92,rtrep=.f.,;
    cFormVar = "w_ARNOMENC", cQueryName = "ARNOMENC",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o incongruente",;
    ToolTipText = "Codice nomenclatura combinata dell'articolo",;
    HelpContextID = 241583287,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=113, Top=386, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_ARNOMENC"

  func oARNOMENC_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S' and (.w_ARDATINT<>'N' OR .w_ARUTISER<>'N'))
    endwith
   endif
  endfunc

  func oARNOMENC_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oARNOMENC_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARNOMENC_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oARNOMENC_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"",'GSMA_AZN.NOMENCLA_VZM',this.parent.oContained
  endproc
  proc oARNOMENC_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_ARNOMENC
     i_obj.ecpSave()
  endproc

  add object oARMOLSUP_2_38 as StdField with uid="ZQNEQJJEVA",rtseq=93,rtrep=.f.,;
    cFormVar = "w_ARMOLSUP", cQueryName = "ARMOLSUP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Moltiplicatore U.M. supplementare rispetto a U.M. principale",;
    HelpContextID = 260680534,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=627, Top=386, cSayPict='"9999.99999"', cGetPict='"9999.99999"'

  func oARMOLSUP_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUMSUPP) AND g_INTR='S' AND .w_ARUTISER<>'S')
    endwith
   endif
  endfunc


  add object oARDATINT_2_39 as StdCombo with uid="FSVPVUIYSJ",rtseq=94,rtrep=.f.,left=200,top=417,width=138,height=22;
    , ToolTipText = "Compilazione campi dati Intrastat: nessuno - solo statistici - statistici e fiscali - solo fiscali";
    , HelpContextID = 168092838;
    , cFormVar="w_ARDATINT",RowSource=""+"Nessuna valorizzazione,"+"Solo dati statistici,"+"Dati statistici e fiscali,"+"Solo dati fiscali", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oARDATINT_2_39.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oARDATINT_2_39.GetRadio()
    this.Parent.oContained.w_ARDATINT = this.RadioValue()
    return .t.
  endfunc

  func oARDATINT_2_39.SetRadio()
    this.Parent.oContained.w_ARDATINT=trim(this.Parent.oContained.w_ARDATINT)
    this.value = ;
      iif(this.Parent.oContained.w_ARDATINT=='N',1,;
      iif(this.Parent.oContained.w_ARDATINT=='S',2,;
      iif(this.Parent.oContained.w_ARDATINT=='F',3,;
      iif(this.Parent.oContained.w_ARDATINT=='I',4,;
      0))))
  endfunc

  func oARDATINT_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oDESCOL_2_72 as StdField with uid="KLZYFRALKE",rtseq=129,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 122814922,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=160, Top=125, InputMask=replicate('X',35)

  add object oDESCOL2_2_73 as StdField with uid="DRLAYPXFUY",rtseq=130,rtrep=.f.,;
    cFormVar = "w_DESCOL2", cQueryName = "DESCOL2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 122814922,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=513, Top=125, InputMask=replicate('X',35)

  func oDESCOL2_2_73.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oUMSUPP_2_76 as StdField with uid="EBMZYKBOMK",rtseq=177,rtrep=.f.,;
    cFormVar = "w_UMSUPP", cQueryName = "UMSUPP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 53475514,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=522, Top=386, InputMask=replicate('X',3)

  add object oDESNOM_2_79 as StdField with uid="AOSZVQYNMJ",rtseq=178,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 105316810,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=200, Top=386, InputMask=replicate('X',35)

  add object oStr_2_41 as StdString with uid="XBYMJUEBGC",Visible=.t., Left=17, Top=9,;
    Alignment=1, Width=56, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="WWVPYBBCDB",Visible=.t., Left=0, Top=209,;
    Alignment=1, Width=90, Height=15,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="RNEFFBCGLU",Visible=.t., Left=0, Top=237,;
    Alignment=1, Width=90, Height=15,;
    Caption="Larghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="LUJVRLBDRA",Visible=.t., Left=0, Top=265,;
    Alignment=1, Width=90, Height=15,;
    Caption="Altezza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="SDOKBYVXGY",Visible=.t., Left=218, Top=209,;
    Alignment=1, Width=92, Height=15,;
    Caption="U.M. dimensioni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="CULCMROGQR",Visible=.t., Left=0, Top=69,;
    Alignment=1, Width=90, Height=15,;
    Caption="Peso netto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="PGKZRGGFZB",Visible=.t., Left=0, Top=97,;
    Alignment=1, Width=90, Height=15,;
    Caption="Peso lordo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="XPGQQYJKIL",Visible=.t., Left=185, Top=69,;
    Alignment=0, Width=49, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="FCWUCECCJZ",Visible=.t., Left=185, Top=97,;
    Alignment=0, Width=46, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="CTDGUMNJDW",Visible=.t., Left=0, Top=153,;
    Alignment=1, Width=90, Height=15,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTIPCO1))
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="XZGVJGDIBV",Visible=.t., Left=0, Top=181,;
    Alignment=1, Width=90, Height=15,;
    Caption="Pezzi:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTPCONF))
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="HINEUQNKRB",Visible=.t., Left=0, Top=293,;
    Alignment=1, Width=90, Height=15,;
    Caption="Volume:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="ARYIVNTAXR",Visible=.t., Left=231, Top=293,;
    Alignment=1, Width=79, Height=15,;
    Caption="U.M. volume:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="OBRIHDAQHM",Visible=.t., Left=170, Top=265,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="HOCVMFPZQU",Visible=.t., Left=0, Top=41,;
    Alignment=1, Width=90, Height=15,;
    Caption="1^ U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="NOLGZDAPKO",Visible=.t., Left=360, Top=209,;
    Alignment=1, Width=81, Height=15,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  func oStr_2_56.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_57 as StdString with uid="XTHQZTYTVU",Visible=.t., Left=360, Top=237,;
    Alignment=1, Width=81, Height=15,;
    Caption="Larghezza:"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_58 as StdString with uid="PZWPAMPJOQ",Visible=.t., Left=360, Top=265,;
    Alignment=1, Width=81, Height=15,;
    Caption="Altezza:"  ;
  , bGlobalFont=.t.

  func oStr_2_58.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="RPMBDUYFYM",Visible=.t., Left=360, Top=69,;
    Alignment=1, Width=81, Height=15,;
    Caption="Peso netto:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_60 as StdString with uid="VTWASIXZVR",Visible=.t., Left=360, Top=97,;
    Alignment=1, Width=81, Height=15,;
    Caption="Peso lordo:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="XNEKONYUMV",Visible=.t., Left=542, Top=69,;
    Alignment=0, Width=113, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="SDNISZTZNW",Visible=.t., Left=542, Top=97,;
    Alignment=0, Width=113, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_63 as StdString with uid="OLILUOXULO",Visible=.t., Left=360, Top=153,;
    Alignment=1, Width=81, Height=15,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTIPCO2))
    endwith
  endfunc

  add object oStr_2_64 as StdString with uid="EPFNUDUHWP",Visible=.t., Left=360, Top=181,;
    Alignment=1, Width=81, Height=15,;
    Caption="Pezzi:"  ;
  , bGlobalFont=.t.

  func oStr_2_64.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2)  OR  EMPTY(.w_ARTPCON2))
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="JGDKZUTHNA",Visible=.t., Left=360, Top=293,;
    Alignment=1, Width=81, Height=15,;
    Caption="Volume:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="JCZVFAJPZL",Visible=.t., Left=513, Top=265,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  func oStr_2_66.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_67 as StdString with uid="HUZDHOYQOC",Visible=.t., Left=360, Top=41,;
    Alignment=1, Width=81, Height=15,;
    Caption="2^ U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_68 as StdString with uid="WOKZCAEQLI",Visible=.t., Left=538, Top=181,;
    Alignment=1, Width=107, Height=15,;
    Caption="Conf. per collo:"  ;
  , bGlobalFont=.t.

  func oStr_2_68.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2) OR EMPTY(.w_ARTPCON2))
    endwith
  endfunc

  add object oStr_2_69 as StdString with uid="QUWOOQOCLQ",Visible=.t., Left=186, Top=181,;
    Alignment=1, Width=102, Height=15,;
    Caption="Conf. per collo:"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARTPCONF))
    endwith
  endfunc

  add object oStr_2_70 as StdString with uid="RARNLYWCNE",Visible=.t., Left=0, Top=125,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="KCNFRIKBYC",Visible=.t., Left=360, Top=125,;
    Alignment=1, Width=81, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  func oStr_2_71.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_74 as StdString with uid="UXJTAGKOSL",Visible=.t., Left=572, Top=210,;
    Alignment=1, Width=92, Height=15,;
    Caption="U.M. dimensioni:"  ;
  , bGlobalFont=.t.

  func oStr_2_74.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_75 as StdString with uid="IJSXNDMXNX",Visible=.t., Left=585, Top=293,;
    Alignment=1, Width=79, Height=15,;
    Caption="U.M. volume:"  ;
  , bGlobalFont=.t.

  func oStr_2_75.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ARUNMIS2))
    endwith
  endfunc

  add object oStr_2_78 as StdString with uid="FYKODLWMGV",Visible=.t., Left=3, Top=340,;
    Alignment=0, Width=41, Height=15,;
    Caption="INTRA"  ;
  , bGlobalFont=.t.

  add object oStr_2_80 as StdString with uid="MXHCBAQFGV",Visible=.t., Left=21, Top=359,;
    Alignment=1, Width=91, Height=15,;
    Caption="Utilizzo intra:"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="ZWHHBAJGCI",Visible=.t., Left=269, Top=359,;
    Alignment=1, Width=226, Height=18,;
    Caption="Modalit� di erogazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_81.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
  endfunc

  add object oStr_2_82 as StdString with uid="JGPNQRHJSC",Visible=.t., Left=8, Top=388,;
    Alignment=1, Width=104, Height=15,;
    Caption="Codice servizio:"  ;
  , bGlobalFont=.t.

  func oStr_2_82.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'S')
    endwith
  endfunc

  add object oStr_2_83 as StdString with uid="PGZRSKEAFC",Visible=.t., Left=37, Top=421,;
    Alignment=1, Width=160, Height=18,;
    Caption="Manutenzione elenchi INTRA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_84 as StdString with uid="VWDTWZATZY",Visible=.t., Left=450, Top=386,;
    Alignment=1, Width=71, Height=15,;
    Caption="U.M.suppl.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="KZXZRWIHHC",Visible=.t., Left=575, Top=386,;
    Alignment=1, Width=50, Height=15,;
    Caption="Moltipl.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="MZJPFIKXGM",Visible=.t., Left=31, Top=388,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nomenclatura:"  ;
  , bGlobalFont=.t.

  func oStr_2_86.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'N')
    endwith
  endfunc

  add object oBox_2_77 as StdBox with uid="KOTXZGSAXH",left=40, top=348, width=676,height=1
enddefine
define class tgsma_aarPag3 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  resizeXpos=342
  resizeYpos=386
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_3_1 as StdField with uid="EILKSDWSHG",rtseq=96,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=59, Top=9, InputMask=replicate('X',20)

  add object oDESC_3_2 as StdField with uid="JTUSEXKHXQ",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=247, Top=9, InputMask=replicate('X',40)

  add object oARCODMAR_3_4 as StdField with uid="ZWPGFXFEHU",rtseq=99,rtrep=.f.,;
    cFormVar = "w_ARCODMAR", cQueryName = "ARCODMAR",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 116847784,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_ARCODMAR"

  func oARCODMAR_3_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODMAR_3_4.ecpDrop(oSource)
    this.Parent.oContained.link_3_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODMAR_3_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oARCODMAR_3_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oARCODMAR_3_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_ARCODMAR
     i_obj.ecpSave()
  endproc

  add object oDESMAR_3_5 as StdField with uid="RSPSGGXWEC",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 36176330,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=36, InputMask=replicate('X',35)


  add object oBtn_3_6 as StdButton with uid="YQZIRWWLPB",left=652, top=36, width=48,height=45,;
    CpPicture="BMP\SCHEDE.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alla scheda magazzino dell'articolo selezionato";
    , HelpContextID = 215913178;
    , Caption='\<Schede';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_3_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S')
     endwith
    endif
  endfunc

  add object oARGRUPRO_3_7 as StdField with uid="ZGRJDWGYZY",rtseq=101,rtrep=.f.,;
    cFormVar = "w_ARGRUPRO", cQueryName = "ARGRUPRO",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo articolo",;
    ToolTipText = "Codice della categoria provvigioni associata all'articolo",;
    HelpContextID = 48477355,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_ARGRUPRO"

  func oARGRUPRO_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oARGRUPRO_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARGRUPRO_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oARGRUPRO_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSMA_AAR.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oARGRUPRO_3_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_ARGRUPRO
     i_obj.ecpSave()
  endproc

  add object oDESGPP_3_8 as StdField with uid="UMCNVAJAZX",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESGPP", cQueryName = "DESGPP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54395338,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=63, InputMask=replicate('X',35)

  add object oARCATSCM_3_10 as StdField with uid="XZYXYJAHRH",rtseq=104,rtrep=.f.,;
    cFormVar = "w_ARCATSCM", cQueryName = "ARCATSCM",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente o non di tipo articolo",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata all'articolo",;
    HelpContextID = 268110675,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_ARCATSCM"

  func oARCATSCM_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATSCM_3_10.ecpDrop(oSource)
    this.Parent.oContained.link_3_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATSCM_3_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oARCATSCM_3_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSMA_AAR.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oARCATSCM_3_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_ARCATSCM
     i_obj.ecpSave()
  endproc

  add object oDESSCM_3_11 as StdField with uid="PDTMSODWBS",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117572042,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=90, InputMask=replicate('X',35)


  add object oBtn_3_15 as StdButton with uid="DSKBUEYYXC",left=652, top=82, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alla disponibilit� nel tempo dell'articolo selezionato";
    , HelpContextID = 78109400;
    , Caption='\<Disp.tem.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_15.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"Q")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_3_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S')
     endwith
    endif
  endfunc

  add object oARCODRIC_3_18 as StdField with uid="XMIBLPPTSN",rtseq=107,rtrep=.f.,;
    cFormVar = "w_ARCODRIC", cQueryName = "ARCODRIC",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe di ricarico dei listini associati all'articolo",;
    HelpContextID = 235473737,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=117, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_ARCODRIC"

  func oARCODRIC_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODRIC_3_18.ecpDrop(oSource)
    this.Parent.oContained.link_3_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODRIC_3_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oARCODRIC_3_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico dei listini",'',this.parent.oContained
  endproc
  proc oARCODRIC_3_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_ARCODRIC
     i_obj.ecpSave()
  endproc

  add object oDESCLR_3_19 as StdField with uid="XIBUNTZQXU",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESCLR", cQueryName = "DESCLR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 25297354,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=117, InputMask=replicate('X',35)

  add object oPERCLR_3_22 as StdField with uid="CHPHQDEIAO",rtseq=109,rtrep=.f.,;
    cFormVar = "w_PERCLR", cQueryName = "PERCLR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25301258,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=541, Top=117, cSayPict='"999.99"', cGetPict='"999.99"'


  add object oLinkPC_3_23 as StdButton with uid="WMQNMAEJFP",left=652, top=127, width=48,height=45,;
    CpPicture="BMP\RITENUTE.bmp", caption="", nPag=3;
    , ToolTipText = "Ricarico prezzi articolo per listino";
    , HelpContextID = 11179056;
    , Caption='\<Ric.prezzi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_3_23.Click()
      this.Parent.oContained.GSMA_MRP.LinkPCClick()
    endproc

  add object oARARTPOS_3_25 as StdCheck with uid="XTLQKQYUKN",rtseq=111,rtrep=.f.,left=541, top=155, caption="Articolo P.O.S.",;
    ToolTipText = "Se attivo: articolo gestito nella vendita dettaglio",;
    HelpContextID = 49550503,;
    cFormVar="w_ARARTPOS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oARARTPOS_3_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARARTPOS_3_25.GetRadio()
    this.Parent.oContained.w_ARARTPOS = this.RadioValue()
    return .t.
  endfunc

  func oARARTPOS_3_25.SetRadio()
    this.Parent.oContained.w_ARARTPOS=trim(this.Parent.oContained.w_ARARTPOS)
    this.value = ;
      iif(this.Parent.oContained.w_ARARTPOS=='S',1,;
      0)
  endfunc

  func oARARTPOS_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oARARTPOS_3_25.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oARCODREP_3_26 as StdField with uid="BUVKTSKIZC",rtseq=112,rtrep=.f.,;
    cFormVar = "w_ARCODREP", cQueryName = "ARCODREP",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice reparto inesistente",;
    ToolTipText = "Codice reparto di vendita dettaglio",;
    HelpContextID = 235473750,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=146, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_ARCODREP"

  func oARCODREP_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S' AND .w_ARARTPOS='S')
    endwith
   endif
  endfunc

  func oARCODREP_3_26.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oARCODREP_3_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODREP_3_26.ecpDrop(oSource)
    this.Parent.oContained.link_3_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODREP_3_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oARCODREP_3_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oARCODREP_3_26.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_ARCODREP
     i_obj.ecpSave()
  endproc

  add object oDESREP_3_27 as StdField with uid="SPMJXBJXHJ",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65208778,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=146, InputMask=replicate('X',40)

  func oDESREP_3_27.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oARPUBWEB_3_29 as StdCheck with uid="DFATIZXQVI",rtseq=124,rtrep=.f.,left=541, top=177, caption="Articolo eCRM Zucchetti",;
    ToolTipText = "Se attivo: articolo pubblicato nell'applicativo eCRM Zucchetti",;
    HelpContextID = 49273672,;
    cFormVar="w_ARPUBWEB", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oARPUBWEB_3_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARPUBWEB_3_29.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_3_29.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      0)
  endfunc

  func oARPUBWEB_3_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ECRM='S')
    endwith
   endif
  endfunc

  func oARPUBWEB_3_29.mHide()
    with this.Parent.oContained
      return (g_ECRM<>'S' or g_IZCP $ 'SA')
    endwith
  endfunc


  add object oARFLPECO_3_30 as StdCombo with uid="YRUABQJRRJ",rtseq=125,rtrep=.f.,left=180,top=173,width=131,height=21;
    , ToolTipText = "Indicare peso/prezzo se l'articolo gestisce barcode a peso variabile";
    , HelpContextID = 29768533;
    , cFormVar="w_ARFLPECO",RowSource=""+"Collo,"+"Peso,"+"Prezzo", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oARFLPECO_3_30.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    iif(this.value =3,'Z',;
    space(1)))))
  endfunc
  func oARFLPECO_3_30.GetRadio()
    this.Parent.oContained.w_ARFLPECO = this.RadioValue()
    return .t.
  endfunc

  func oARFLPECO_3_30.SetRadio()
    this.Parent.oContained.w_ARFLPECO=trim(this.Parent.oContained.w_ARFLPECO)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLPECO=='C',1,;
      iif(this.Parent.oContained.w_ARFLPECO=='P',2,;
      iif(this.Parent.oContained.w_ARFLPECO=='Z',3,;
      0)))
  endfunc

  add object oARCODGRU_3_31 as StdField with uid="ATHJTOKSON",rtseq=126,rtrep=.f.,;
    cFormVar = "w_ARCODGRU", cQueryName = "ARCODGRU",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo di default proposto in caricamento documenti di offerta e kit promozionali",;
    HelpContextID = 217511077,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=200, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_ARCODGRU"

  func oARCODGRU_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE='S')
    endwith
   endif
  endfunc

  func oARCODGRU_3_31.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  func oARCODGRU_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_31('Part',this)
      if .not. empty(.w_ARCODSOT)
        bRes2=.link_3_32('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oARCODGRU_3_31.ecpDrop(oSource)
    this.Parent.oContained.link_3_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODGRU_3_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oARCODGRU_3_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Codici gruppi",'',this.parent.oContained
  endproc
  proc oARCODGRU_3_31.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_ARCODGRU
     i_obj.ecpSave()
  endproc

  add object oARCODSOT_3_32 as StdField with uid="YXLZZCRUEL",rtseq=127,rtrep=.f.,;
    cFormVar = "w_ARCODSOT", cQueryName = "ARCODSOT",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sottogruppo di default proposto in caricamento documenti di offerta e kit promozionali",;
    HelpContextID = 16184486,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=180, Top=228, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_ARCODGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_ARCODSOT"

  func oARCODSOT_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE='S' AND NOT EMPTY(.w_ARCODGRU))
    endwith
   endif
  endfunc

  func oARCODSOT_3_32.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  func oARCODSOT_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODSOT_3_32.ecpDrop(oSource)
    this.Parent.oContained.link_3_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODSOT_3_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_ARCODGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_ARCODGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oARCODSOT_3_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Codici sottogruppi",'',this.parent.oContained
  endproc
  proc oARCODSOT_3_32.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_ARCODGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_ARCODSOT
     i_obj.ecpSave()
  endproc

  add object oARMINVEN_3_33 as StdField with uid="TMJJVADDCE",rtseq=128,rtrep=.f.,;
    cFormVar = "w_ARMINVEN", cQueryName = "ARMINVEN",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'unit� di misura principale non � frazionabile",;
    ToolTipText = "Quantitativo minimo vendibile/ordinabile espresso nell'unit� di misura principale",;
    HelpContextID = 44280660,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=180, Top=253, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oARMINVEN_3_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FLFRAZ<>'S' Or .w_ARMINVEN =Int(.w_ARMINVEN))
    endwith
    return bRes
  endfunc


  add object oLinkPC_3_34 as stdDynamicChildContainer with uid="UWQFXTUJFQ",left=48, top=298, width=482, height=103, bOnScreen=.t.;


  add object oDESSOT_3_40 as StdField with uid="DTTIXTENZG",rtseq=133,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 255984074,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=228, InputMask=replicate('X',35)

  func oDESSOT_3_40.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oDESTIT_3_41 as StdField with uid="UOFQAKEFTS",rtseq=134,rtrep=.f.,;
    cFormVar = "w_DESTIT", cQueryName = "DESTIT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 262209994,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=200, InputMask=replicate('X',35)

  func oDESTIT_3_41.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc


  add object oARPUBWEB_3_44 as StdCombo with uid="BQJGYMHQVU",rtseq=140,rtrep=.f.,left=541,top=254,width=144,height=21;
    , ToolTipText = "Se attivo: articolo gestito dall'applicazione Web";
    , HelpContextID = 49273672;
    , cFormVar="w_ARPUBWEB",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oARPUBWEB_3_44.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARPUBWEB_3_44.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_3_44.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      iif(this.Parent.oContained.w_ARPUBWEB=='N',2,;
      iif(this.Parent.oContained.w_ARPUBWEB=='T',3,;
      0)))
  endfunc

  func oARPUBWEB_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP $ 'SA' )
    endwith
   endif
  endfunc

  func oARPUBWEB_3_44.mHide()
    with this.Parent.oContained
      return (g_IZCP='N' OR g_CPIN='S')
    endwith
  endfunc


  add object oARPUBWEB_3_45 as StdCombo with uid="EDEHBNJSIQ",rtseq=141,rtrep=.f.,left=541,top=254,width=144,height=21;
    , ToolTipText = "Se attivo: articolo gestito dall'applicazione Web";
    , HelpContextID = 49273672;
    , cFormVar="w_ARPUBWEB",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oARPUBWEB_3_45.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARPUBWEB_3_45.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_3_45.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      iif(this.Parent.oContained.w_ARPUBWEB=='N',2,;
      iif(this.Parent.oContained.w_ARPUBWEB=='T',3,;
      0)))
  endfunc

  func oARPUBWEB_3_45.mHide()
    with this.Parent.oContained
      return (g_IZCP='N' and  g_CPIN<>'S' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oARFLAPCA_3_47 as StdCheck with uid="EYKGDCBJDX",rtseq=171,rtrep=.f.,left=541, top=329, caption="Applica contributi accessori",;
    ToolTipText = "Se attivo per l'articolo sar� applicata la combinazione dei contributi accessori indicata nella apposita tabella",;
    HelpContextID = 198589255,;
    cFormVar="w_ARFLAPCA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oARFLAPCA_3_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLAPCA_3_47.GetRadio()
    this.Parent.oContained.w_ARFLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oARFLAPCA_3_47.SetRadio()
    this.Parent.oContained.w_ARFLAPCA=trim(this.Parent.oContained.w_ARFLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLAPCA=='S',1,;
      0)
  endfunc

  func oARFLAPCA_3_47.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc


  add object oLinkPC_3_48 as StdButton with uid="WJXDVFCVBT",left=652, top=352, width=48,height=45,;
    CpPicture="BMP\CONTRACC.BMP", caption="", nPag=3;
    , ToolTipText = "Dettaglio contributo accessori generici";
    , HelpContextID = 135481563;
    , caption='\<Cont.acc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_3_48.Click()
      this.Parent.oContained.GSAR_MAR.LinkPCClick()
    endproc

  func oLinkPC_3_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ARFLAPCA = 'S')
      endwith
    endif
  endfunc

  func oLinkPC_3_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAC='N')
     endwith
    endif
  endfunc

  add object oARCODTIP_3_53 as StdField with uid="UWNCKSOVUI",rtseq=204,rtrep=.f.,;
    cFormVar = "w_ARCODTIP", cQueryName = "ARCODTIP",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica la tipologia di codice articolo (per esempio, TARIC, CPV, EAN, SSC, ...)",;
    HelpContextID = 592726,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=180, Top=436, InputMask=replicate('X',35)

  add object oARCODCLF_3_54 as StdField with uid="MJWYUTIROE",rtseq=205,rtrep=.f.,;
    cFormVar = "w_ARCODCLF", cQueryName = "ARCODCLF",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 16184500,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=645, Top=436, InputMask=replicate('X',5)

  add object oARCODVAL_3_55 as StdField with uid="VCGAGDOJNU",rtseq=206,rtrep=.f.,;
    cFormVar = "w_ARCODVAL", cQueryName = "ARCODVAL",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica il valore del codice articolo corrispondente alla tipologia riportata nell'elemento informativo 2.2.1.3.1 <CodiceTipo>",;
    HelpContextID = 234288302,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=180, Top=463, InputMask=replicate('X',35)

  add object oARFLGESC_3_56 as StdCheck with uid="IKXQKROTUZ",rtseq=207,rtrep=.f.,left=553, top=462, caption="Codice articolo esclusivo",;
    ToolTipText = "Codice articolo esclusivo",;
    HelpContextID = 20331337,;
    cFormVar="w_ARFLGESC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oARFLGESC_3_56.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLGESC_3_56.GetRadio()
    this.Parent.oContained.w_ARFLGESC = this.RadioValue()
    return .t.
  endfunc

  func oARFLGESC_3_56.SetRadio()
    this.Parent.oContained.w_ARFLGESC=trim(this.Parent.oContained.w_ARFLGESC)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLGESC=='S',1,;
      0)
  endfunc

  add object oStr_3_13 as StdString with uid="XFOEJEOKUQ",Visible=.t., Left=1, Top=9,;
    Alignment=1, Width=55, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_14 as StdString with uid="DHJWNOZOHI",Visible=.t., Left=48, Top=36,;
    Alignment=1, Width=129, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="BZSZJVBNNV",Visible=.t., Left=45, Top=63,;
    Alignment=1, Width=132, Height=15,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="IQYHISFTOR",Visible=.t., Left=45, Top=90,;
    Alignment=1, Width=132, Height=15,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="LVGXUKSOCR",Visible=.t., Left=513, Top=117,;
    Alignment=1, Width=25, Height=15,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="WZVHGGJFSV",Visible=.t., Left=45, Top=117,;
    Alignment=1, Width=132, Height=15,;
    Caption="Classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="TCQSYZPKDU",Visible=.t., Left=53, Top=278,;
    Alignment=0, Width=131, Height=18,;
    Caption="Articoli alternativi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_36 as StdString with uid="JZIMXQWCON",Visible=.t., Left=45, Top=146,;
    Alignment=1, Width=132, Height=15,;
    Caption="Codice reparto:"  ;
  , bGlobalFont=.t.

  func oStr_3_36.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_3_37 as StdString with uid="OQBQLNRUXD",Visible=.t., Left=45, Top=171,;
    Alignment=1, Width=132, Height=18,;
    Caption="Tipologia articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_38 as StdString with uid="AIFYDUGJCD",Visible=.t., Left=45, Top=200,;
    Alignment=1, Width=132, Height=15,;
    Caption="Codice gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_3_38.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oStr_3_39 as StdString with uid="ZTPICWREOH",Visible=.t., Left=45, Top=228,;
    Alignment=1, Width=132, Height=15,;
    Caption="Sottogruppo:"  ;
  , bGlobalFont=.t.

  func oStr_3_39.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oStr_3_46 as StdString with uid="NVIRRERPAB",Visible=.t., Left=541, Top=235,;
    Alignment=0, Width=145, Height=18,;
    Caption="Web Application"  ;
  , bGlobalFont=.t.

  func oStr_3_46.mHide()
    with this.Parent.oContained
      return (g_IZCP='N' and  g_CPIN<>'S' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oStr_3_49 as StdString with uid="GPMNXDHWWA",Visible=.t., Left=0, Top=253,;
    Alignment=1, Width=177, Height=18,;
    Caption="Qt� min. vendibile/ordinabile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=5, Top=412,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_3_57 as StdString with uid="YTFHXMDYIQ",Visible=.t., Left=65, Top=437,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_58 as StdString with uid="CJHWPVYXQL",Visible=.t., Left=65, Top=464,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice valore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_59 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=526, Top=438,;
    Alignment=1, Width=117, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_3_52 as StdBox with uid="SJYDLIWYJC",left=5, top=430, width=693,height=2
enddefine
define class tgsma_aarPag4 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_3 as StdField with uid="NPQTBTHUXE",rtseq=117,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=59, Top=9, InputMask=replicate('X',20)

  add object oDESC_4_4 as StdField with uid="HWJROJUJRH",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=246, Top=9, InputMask=replicate('X',40)


  add object oLinkPC_4_6 as stdDynamicChildContainer with uid="QXGHGXYBSE",left=-1, top=33, width=726, height=453, bOnScreen=.t.;


  add object oStr_4_5 as StdString with uid="AIDEAEFDXV",Visible=.t., Left=1, Top=9,;
    Alignment=1, Width=55, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsma_aarPag5 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oARFLLOTT_5_1 as StdCombo with uid="KSEWCQGNFG",rtseq=143,rtrep=.f.,left=157,top=39,width=140,height=21;
    , ToolTipText = "S�: articolo gestito per lotti; s� + consumo autom.: articolo gestito per lotti a consumo automatico; no: non gestito a lotti";
    , HelpContextID = 193346394;
    , cFormVar="w_ARFLLOTT",RowSource=""+"Si,"+"S� + consumo autom.,"+"No", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oARFLLOTT_5_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oARFLLOTT_5_1.GetRadio()
    this.Parent.oContained.w_ARFLLOTT = this.RadioValue()
    return .t.
  endfunc

  func oARFLLOTT_5_1.SetRadio()
    this.Parent.oContained.w_ARFLLOTT=trim(this.Parent.oContained.w_ARFLLOTT)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLLOTT=='S',1,;
      iif(this.Parent.oContained.w_ARFLLOTT=='C',2,;
      iif(this.Parent.oContained.w_ARFLLOTT=='N',3,;
      0)))
  endfunc

  func oARFLLOTT_5_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT = 'S' and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  add object oARFLLMAG_5_2 as StdCheck with uid="GSPPBCPBQW",rtseq=144,rtrep=.f.,left=354, top=36, caption="Forza magazzino lotti",;
    ToolTipText = "Se attivo forza magazzino del lotto a consumo automatico",;
    HelpContextID = 108643507,;
    cFormVar="w_ARFLLMAG", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARFLLMAG_5_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLLMAG_5_2.GetRadio()
    this.Parent.oContained.w_ARFLLMAG = this.RadioValue()
    return .t.
  endfunc

  func oARFLLMAG_5_2.SetRadio()
    this.Parent.oContained.w_ARFLLMAG=trim(this.Parent.oContained.w_ARFLLMAG)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLLMAG=='S',1,;
      0)
  endfunc

  func oARFLLMAG_5_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARFLLOTT='C' AND g_PERLOT = 'S')
    endwith
   endif
  endfunc

  add object oARCLALOT_5_3 as StdField with uid="NFDQZKJBXK",rtseq=145,rtrep=.f.,;
    cFormVar = "w_ARCLALOT", cQueryName = "ARCLALOT",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, classe inesistente o incongruente",;
    ToolTipText = "Classe lotto",;
    HelpContextID = 136967334,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=157, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_BZC", oKey_1_1="CMCODICE", oKey_1_2="this.w_ARCLALOT"

  func oARCLALOT_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT = 'S' And .w_ARFLLOTT<>'N' and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oARCLALOT_5_3.mHide()
    with this.Parent.oContained
      return (.w_ARFLLOTT='N')
    endwith
  endfunc

  func oARCLALOT_5_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCLALOT_5_3.ecpDrop(oSource)
    this.Parent.oContained.link_5_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCLALOT_5_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oARCLALOT_5_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_BZC',"Classi lotti",'GSMDLACM.CMT_MAST_VZM',this.parent.oContained
  endproc
  proc oARCLALOT_5_3.mZoomOnZoom
    local i_obj
    i_obj=GSMD_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_ARCLALOT
     i_obj.ecpSave()
  endproc


  add object oARDISLOT_5_4 as StdCombo with uid="KADSZSFGEN",rtseq=146,rtrep=.f.,left=157,top=96,width=140,height=21;
    , ToolTipText = "Check disponibilit� lotti";
    , HelpContextID = 118285478;
    , cFormVar="w_ARDISLOT",RowSource=""+"Si,"+"S� con conferma,"+"No", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oARDISLOT_5_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oARDISLOT_5_4.GetRadio()
    this.Parent.oContained.w_ARDISLOT = this.RadioValue()
    return .t.
  endfunc

  func oARDISLOT_5_4.SetRadio()
    this.Parent.oContained.w_ARDISLOT=trim(this.Parent.oContained.w_ARDISLOT)
    this.value = ;
      iif(this.Parent.oContained.w_ARDISLOT=='S',1,;
      iif(this.Parent.oContained.w_ARDISLOT=='C',2,;
      iif(this.Parent.oContained.w_ARDISLOT=='N',3,;
      0)))
  endfunc

  func oARDISLOT_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S' And .w_ARFLLOTT='S' and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oARDISLOT_5_4.mHide()
    with this.Parent.oContained
      return (.w_ARFLLOTT='N')
    endwith
  endfunc

  add object oARFLESUL_5_5 as StdCheck with uid="YTRIAUTKSN",rtseq=147,rtrep=.f.,left=354, top=92, caption="Escludi unit� logistica",;
    ToolTipText = "Se attivo: esclude l'articolo dalla gestione unit� logistica",;
    HelpContextID = 253115218,;
    cFormVar="w_ARFLESUL", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARFLESUL_5_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLESUL_5_5.GetRadio()
    this.Parent.oContained.w_ARFLESUL = this.RadioValue()
    return .t.
  endfunc

  func oARFLESUL_5_5.SetRadio()
    this.Parent.oContained.w_ARFLESUL=trim(this.Parent.oContained.w_ARFLESUL)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLESUL=='S',1,;
      0)
  endfunc

  func oARFLESUL_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  add object oARGESMAT_5_6 as StdCheck with uid="DNHFTNKIDS",rtseq=148,rtrep=.f.,left=157, top=123, caption="Matricole",;
    ToolTipText = "Articolo gestito a matricole",;
    HelpContextID = 101758118,;
    cFormVar="w_ARGESMAT", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARGESMAT_5_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARGESMAT_5_6.GetRadio()
    this.Parent.oContained.w_ARGESMAT = this.RadioValue()
    return .t.
  endfunc

  func oARGESMAT_5_6.SetRadio()
    this.Parent.oContained.w_ARGESMAT=trim(this.Parent.oContained.w_ARGESMAT)
    this.value = ;
      iif(this.Parent.oContained.w_ARGESMAT=='S',1,;
      0)
  endfunc

  func oARGESMAT_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oARGESMAT_5_6.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oARCLAMAT_5_7 as StdField with uid="ZGKZDIMJIG",rtseq=149,rtrep=.f.,;
    cFormVar = "w_ARCLAMAT", cQueryName = "ARCLAMAT",;
    bObbl = .t. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, classe insesistente o incongruente",;
    ToolTipText = "Classe matricola",;
    HelpContextID = 120190118,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=157, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_BZC", oKey_1_1="CMCODICE", oKey_1_2="this.w_ARCLAMAT"

  func oARCLAMAT_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARGESMAT='S' And g_MATR='S')
    endwith
   endif
  endfunc

  func oARCLAMAT_5_7.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  func oARCLAMAT_5_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCLAMAT_5_7.ecpDrop(oSource)
    this.Parent.oContained.link_5_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCLAMAT_5_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oARCLAMAT_5_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_BZC',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this.parent.oContained
  endproc
  proc oARCLAMAT_5_7.mZoomOnZoom
    local i_obj
    i_obj=GSMD_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_ARCLAMAT
     i_obj.ecpSave()
  endproc

  add object oARVOCCEN_5_8 as StdField with uid="GBXFEFTUHQ",rtseq=150,rtrep=.f.,;
    cFormVar = "w_ARVOCCEN", cQueryName = "ARVOCCEN",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di costo incongruente o obsoleto",;
    ToolTipText = "Codice della voce di costo utilizzata per la contabilit� analitica dei documenti di acquisto",;
    HelpContextID = 251280212,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=157, Top=190, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ARVOCCEN"

  func oARVOCCEN_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oARVOCCEN_5_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oARVOCCEN_5_8.ecpDrop(oSource)
    this.Parent.oContained.link_5_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARVOCCEN_5_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oARVOCCEN_5_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMACAAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oARVOCCEN_5_8.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ARVOCCEN
     i_obj.ecpSave()
  endproc

  add object oARVOCRIC_5_9 as StdField with uid="XMWZPBYNPN",rtseq=151,rtrep=.f.,;
    cFormVar = "w_ARVOCRIC", cQueryName = "ARVOCRIC",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice della voce di ricavo utilizzata per la contabilit� analitica dei documenti di vendita",;
    HelpContextID = 234502985,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=157, Top=214, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ARVOCRIC"

  func oARVOCRIC_5_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oARVOCRIC_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oARVOCRIC_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARVOCRIC_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oARVOCRIC_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oARVOCRIC_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ARVOCRIC
     i_obj.ecpSave()
  endproc

  add object oARCODCEN_5_10 as StdField with uid="VSWZYBOLAQ",rtseq=152,rtrep=.f.,;
    cFormVar = "w_ARCODCEN", cQueryName = "ARCODCEN",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice centro di costo obsoleto",;
    HelpContextID = 252250964,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=157, Top=238, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ARCODCEN"

  func oARCODCEN_5_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODCEN_5_10.ecpDrop(oSource)
    this.Parent.oContained.link_5_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODCEN_5_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oARCODCEN_5_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oARCODCEN_5_10.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ARCODCEN
     i_obj.ecpSave()
  endproc

  add object oARCODDIS_5_11 as StdField with uid="CJZVXWRMXW",rtseq=153,rtrep=.f.,;
    cFormVar = "w_ARCODDIS", cQueryName = "ARCODDIS",;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati",;
    ToolTipText = "Codice della distinta base associata all'articolo",;
    HelpContextID = 592729,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=157, Top=307, InputMask=replicate('X',20), bHasZoom = .t. , cMenuFile="GSMA_AAR", cLinkFile="DISMBASE", cZoomOnZoom="GSAR_BAK", oKey_1_1="DBCODICE", oKey_1_2="this.w_ARCODDIS"

  func oARCODDIS_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_EACD = 'S' Or g_GPOS = 'S') And .w_ARKITIMB='N')
    endwith
   endif
  endfunc

  func oARCODDIS_5_11.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  func oARCODDIS_5_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODDIS_5_11.ecpDrop(oSource)
    this.Parent.oContained.link_5_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODDIS_5_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oARCODDIS_5_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BAK',"Elenco distinte base - kit",'GSDS_AAR.DISMBASE_VZM',this.parent.oContained
  endproc
  proc oARCODDIS_5_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BAK()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DBCODICE=this.parent.oContained.w_ARCODDIS
     i_obj.ecpSave()
  endproc

  add object oARFLCOMP_5_12 as StdCheck with uid="BAFVOQDTVM",rtseq=154,rtrep=.f.,left=600, top=306, caption="Art. composto",;
    ToolTipText = "Se attivo: l'articolo in distinta/kit verr� esploso nei suoi componenti",;
    HelpContextID = 84526250,;
    cFormVar="w_ARFLCOMP", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARFLCOMP_5_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLCOMP_5_12.GetRadio()
    this.Parent.oContained.w_ARFLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oARFLCOMP_5_12.SetRadio()
    this.Parent.oContained.w_ARFLCOMP=trim(this.Parent.oContained.w_ARFLCOMP)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCOMP=='S',1,;
      0)
  endfunc

  func oARFLCOMP_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_EACD='S' Or g_GPOS = 'S') AND NOT EMPTY(.w_ARCODDIS) And .w_ARKITIMB = 'N')
    endwith
   endif
  endfunc

  func oARFLCOMP_5_12.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  add object oARMAGPRE_5_13 as StdField with uid="JYVULXANVN",rtseq=155,rtrep=.f.,;
    cFormVar = "w_ARMAGPRE", cQueryName = "ARMAGPRE",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino produzione non nettificabile o obsoleto",;
    ToolTipText = "Magazzino di produzione/prelievo materiali",;
    HelpContextID = 64246965,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=157, Top=355, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ARMAGPRE"

  func oARMAGPRE_5_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARMAGPRE_5_13.ecpDrop(oSource)
    this.Parent.oContained.link_5_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARMAGPRE_5_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oARMAGPRE_5_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oARMAGPRE_5_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ARMAGPRE
     i_obj.ecpSave()
  endproc


  add object oARTIPPRE_5_14 as StdCombo with uid="BPQDRFZFHF",rtseq=156,rtrep=.f.,left=516,top=356,width=92,height=21;
    , ToolTipText = "Tipologia di prelievo";
    , HelpContextID = 54256821;
    , cFormVar="w_ARTIPPRE",RowSource=""+"Nessuno,"+"Automatico,"+"Manuale", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oARTIPPRE_5_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oARTIPPRE_5_14.GetRadio()
    this.Parent.oContained.w_ARTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oARTIPPRE_5_14.SetRadio()
    this.Parent.oContained.w_ARTIPPRE=trim(this.Parent.oContained.w_ARTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPPRE=='N',1,;
      iif(this.Parent.oContained.w_ARTIPPRE=='A',2,;
      iif(this.Parent.oContained.w_ARTIPPRE=='M',3,;
      0)))
  endfunc

  func oARTIPPRE_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PROD='S')
    endwith
   endif
  endfunc

  func oARTIPPRE_5_14.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc

  add object oCMTDESCRI_5_16 as StdField with uid="LHPSPMKIRU",rtseq=157,rtrep=.f.,;
    cFormVar = "w_CMTDESCRI", cQueryName = "CMTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 252648200,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=233, Top=143, InputMask=replicate('X',30)

  func oCMTDESCRI_5_16.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oCLTDESCRI_5_18 as StdField with uid="YBUKVIYBOS",rtseq=158,rtrep=.f.,;
    cFormVar = "w_CLTDESCRI", cQueryName = "CLTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 252647944,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=233, Top=66, InputMask=replicate('X',30)

  func oCLTDESCRI_5_18.mHide()
    with this.Parent.oContained
      return (.w_ARFLLOTT='N')
    endwith
  endfunc

  add object oDESVOC_5_21 as StdField with uid="OGVJQXXPYK",rtseq=159,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 4129226,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=190, InputMask=replicate('X',40)

  add object oDESRIC_5_23 as StdField with uid="CHWDGBJFBY",rtseq=160,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 10682826,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=214, InputMask=replicate('X',40)

  add object oDESDIS_5_26 as StdField with uid="MXLUPQUZTF",rtseq=161,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11600330,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=313, Top=307, InputMask=replicate('X',40)

  func oDESDIS_5_26.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  add object oDESPRE_5_28 as StdField with uid="UWJCHOYMAV",rtseq=162,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 236257738,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=224, Top=355, InputMask=replicate('X',30)

  add object oDESCEN_5_40 as StdField with uid="TQRMRCQBCY",rtseq=167,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99746250,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=238, InputMask=replicate('X',40)


  add object oARKITIMB_5_42 as StdCombo with uid="IDXPJDLSIJ",rtseq=169,rtrep=.f.,left=157,top=284,width=153,height=21;
    , ToolTipText = "Specifica se l'articolo � di tipo imballo";
    , HelpContextID = 167539896;
    , cFormVar="w_ARKITIMB",RowSource=""+"No imballo,"+"Imballo a perdere,"+"Imballo a rendere", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oARKITIMB_5_42.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oARKITIMB_5_42.GetRadio()
    this.Parent.oContained.w_ARKITIMB = this.RadioValue()
    return .t.
  endfunc

  func oARKITIMB_5_42.SetRadio()
    this.Parent.oContained.w_ARKITIMB=trim(this.Parent.oContained.w_ARKITIMB)
    this.value = ;
      iif(this.Parent.oContained.w_ARKITIMB=='N',1,;
      iif(this.Parent.oContained.w_ARKITIMB=='P',2,;
      iif(this.Parent.oContained.w_ARKITIMB=='R',3,;
      0)))
  endfunc

  func oARKITIMB_5_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA = 'S')
    endwith
   endif
  endfunc

  func oARKITIMB_5_42.mHide()
    with this.Parent.oContained
      return (g_VEFA <> 'S')
    endwith
  endfunc

  add object oARFLESIM_5_44 as StdCheck with uid="OUWEQQFLRC",rtseq=170,rtrep=.f.,left=313, top=285, caption="Esplosione automatica imballo",;
    ToolTipText = "Attivo: al salvataggio del documento verr� generato in automatico il documento con l'esplosione degli imballi",;
    HelpContextID = 253115219,;
    cFormVar="w_ARFLESIM", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARFLESIM_5_44.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLESIM_5_44.GetRadio()
    this.Parent.oContained.w_ARFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oARFLESIM_5_44.SetRadio()
    this.Parent.oContained.w_ARFLESIM=trim(this.Parent.oContained.w_ARFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLESIM=='S',1,;
      0)
  endfunc

  func oARFLESIM_5_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARKITIMB='N' And Not Empty(.w_ARCODDIS) And .w_DISKIT='I')
    endwith
   endif
  endfunc

  func oARFLESIM_5_44.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oARCLACRI_5_45 as StdField with uid="PAAHNSHCVB",rtseq=175,rtrep=.f.,;
    cFormVar = "w_ARCLACRI", cQueryName = "ARCLACRI",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe criticit� materiali",;
    HelpContextID = 19526833,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=331, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAT_CRI", cZoomOnZoom="GSAR_AMR", oKey_1_1="MTCODICE", oKey_1_2="this.w_ARCLACRI"

  func oARCLACRI_5_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PROD="S")
    endwith
   endif
  endfunc

  func oARCLACRI_5_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCLACRI_5_45.ecpDrop(oSource)
    this.Parent.oContained.link_5_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCLACRI_5_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAT_CRI','*','MTCODICE',cp_AbsName(this.parent,'oARCLACRI_5_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMR',"",'',this.parent.oContained
  endproc
  proc oARCLACRI_5_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MTCODICE=this.parent.oContained.w_ARCLACRI
     i_obj.ecpSave()
  endproc

  add object oMATCRI_5_47 as StdField with uid="EOTPPWCLTN",rtseq=176,rtrep=.f.,;
    cFormVar = "w_MATCRI", cQueryName = "MATCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 169997626,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=224, Top=332, InputMask=replicate('X',35)


  add object oARTIPIMP_5_52 as StdCombo with uid="CAWZZMJXMN",rtseq=195,rtrep=.f.,left=157,top=379,width=146,height=22;
    , HelpContextID = 171697322;
    , cFormVar="w_ARTIPIMP",RowSource=""+"Da componente,"+"Da testata ordine,"+"Forza magazzino", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oARTIPIMP_5_52.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'T',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oARTIPIMP_5_52.GetRadio()
    this.Parent.oContained.w_ARTIPIMP = this.RadioValue()
    return .t.
  endfunc

  func oARTIPIMP_5_52.SetRadio()
    this.Parent.oContained.w_ARTIPIMP=trim(this.Parent.oContained.w_ARTIPIMP)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPIMP=='C',1,;
      iif(this.Parent.oContained.w_ARTIPIMP=='T',2,;
      iif(this.Parent.oContained.w_ARTIPIMP=='F',3,;
      0)))
  endfunc

  add object oARMAGIMP_5_53 as StdField with uid="JTCVKITCRE",rtseq=196,rtrep=.f.,;
    cFormVar = "w_ARMAGIMP", cQueryName = "ARMAGIMP",;
    bObbl = .t. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di impegno componenti",;
    HelpContextID = 181687466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=403, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ARMAGIMP"

  func oARMAGIMP_5_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_53('Part',this)
    endwith
    return bRes
  endfunc

  func oARMAGIMP_5_53.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_ARTIPIMP='F'
    endwith
    return i_bres
  endfunc

  proc oARMAGIMP_5_53.ecpDrop(oSource)
    this.Parent.oContained.link_5_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARMAGIMP_5_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oARMAGIMP_5_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"",'',this.parent.oContained
  endproc
  proc oARMAGIMP_5_53.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ARMAGIMP
     i_obj.ecpSave()
  endproc

  add object oDESMIMP_5_55 as StdField with uid="SOWVULRXKO",rtseq=197,rtrep=.f.,;
    cFormVar = "w_DESMIMP", cQueryName = "DESMIMP",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 111673802,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=224, Top=403, InputMask=replicate('X',30)


  add object oLinkPC_5_57 as StdButton with uid="YBOMCCHFPY",left=673, top=404, width=48,height=45,;
    CpPicture="BMP\CARATTER.BMP", caption="", nPag=5;
    , ToolTipText = "Caratteristiche di default";
    , HelpContextID = 1584458;
    , caption='\<Caratter.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_5_57.Click()
      this.Parent.oContained.GSMA_MAC.LinkPCClick()
    endproc

  func oLinkPC_5_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_CCAR<>'S')
     endwith
    endif
  endfunc

  add object oARCMPCAR_5_58 as StdCheck with uid="MMGEJSXTDS",rtseq=202,rtrep=.f.,left=157, top=427, caption="Componenti caratteristiche",;
    ToolTipText = "Se attivo, il componente della distinta base puo essere gestito a caratteristiche",;
    HelpContextID = 3732648,;
    cFormVar="w_ARCMPCAR", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oARCMPCAR_5_58.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oARCMPCAR_5_58.GetRadio()
    this.Parent.oContained.w_ARCMPCAR = this.RadioValue()
    return .t.
  endfunc

  func oARCMPCAR_5_58.SetRadio()
    this.Parent.oContained.w_ARCMPCAR=trim(this.Parent.oContained.w_ARCMPCAR)
    this.value = ;
      iif(this.Parent.oContained.w_ARCMPCAR=="S",1,;
      0)
  endfunc

  func oARCMPCAR_5_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CCAR='S')
    endwith
   endif
  endfunc

  add object oStr_5_15 as StdString with uid="URQMJXDLZW",Visible=.t., Left=37, Top=143,;
    Alignment=1, Width=116, Height=18,;
    Caption="Classe matricola:"  ;
  , bGlobalFont=.t.

  func oStr_5_15.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oStr_5_17 as StdString with uid="YHTLYCCJIM",Visible=.t., Left=58, Top=66,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classe lotto:"  ;
  , bGlobalFont=.t.

  func oStr_5_17.mHide()
    with this.Parent.oContained
      return (.w_ARFLLOTT='N')
    endwith
  endfunc

  add object oStr_5_19 as StdString with uid="UMQBPPOTVM",Visible=.t., Left=57, Top=39,;
    Alignment=1, Width=96, Height=18,;
    Caption="Gestione lotti:"  ;
  , bGlobalFont=.t.

  add object oStr_5_20 as StdString with uid="PTMAYYSUMN",Visible=.t., Left=61, Top=190,;
    Alignment=1, Width=92, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_22 as StdString with uid="XNLSPCSINJ",Visible=.t., Left=59, Top=216,;
    Alignment=1, Width=94, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="IIIOLPRGKV",Visible=.t., Left=5, Top=163,;
    Alignment=0, Width=49, Height=18,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_25 as StdString with uid="CRYQVUJHGC",Visible=.t., Left=28, Top=10,;
    Alignment=0, Width=191, Height=18,;
    Caption="Magazzino funzioni avanzate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_27 as StdString with uid="NLVQYDEEUR",Visible=.t., Left=48, Top=310,;
    Alignment=1, Width=105, Height=18,;
    Caption="Distinta base\kit:"  ;
  , bGlobalFont=.t.

  func oStr_5_27.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  add object oStr_5_29 as StdString with uid="ABVASRXNLA",Visible=.t., Left=45, Top=357,;
    Alignment=1, Width=108, Height=15,;
    Caption="Mag.preferenziale:"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="SSXZYJFNPG",Visible=.t., Left=454, Top=357,;
    Alignment=1, Width=59, Height=15,;
    Caption="Prelievo:"  ;
  , bGlobalFont=.t.

  func oStr_5_30.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc

  add object oStr_5_32 as StdString with uid="XBUGYYFWXK",Visible=.t., Left=38, Top=96,;
    Alignment=1, Width=115, Height=18,;
    Caption="Disponibilit� lotti:"  ;
  , bGlobalFont=.t.

  func oStr_5_32.mHide()
    with this.Parent.oContained
      return (.w_ARFLLOTT='N')
    endwith
  endfunc

  add object oStr_5_34 as StdString with uid="RZISSHVZNW",Visible=.t., Left=5, Top=262,;
    Alignment=0, Width=192, Height=18,;
    Caption="Produzione\funzioni avanzate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_39 as StdString with uid="FYHJETCULH",Visible=.t., Left=16, Top=240,;
    Alignment=1, Width=137, Height=18,;
    Caption="Centro di costo/ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_43 as StdString with uid="JXEUOWUPXV",Visible=.t., Left=79, Top=285,;
    Alignment=1, Width=74, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_5_43.mHide()
    with this.Parent.oContained
      return (g_VEFA <> 'S')
    endwith
  endfunc

  add object oStr_5_46 as StdString with uid="QZIVFVRCXC",Visible=.t., Left=58, Top=335,;
    Alignment=1, Width=95, Height=15,;
    Caption="Classe criticit�:"  ;
  , bGlobalFont=.t.

  add object oStr_5_54 as StdString with uid="OOHGVTSZEA",Visible=.t., Left=33, Top=404,;
    Alignment=1, Width=120, Height=15,;
    Caption="Mag.impegno comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_56 as StdString with uid="LSCCHYBDBU",Visible=.t., Left=10, Top=379,;
    Alignment=1, Width=143, Height=18,;
    Caption="Tipologia di impegno:"  ;
  , bGlobalFont=.t.

  add object oBox_5_31 as StdBox with uid="GJJJEAUAYD",left=2, top=26, width=678,height=2

  add object oBox_5_33 as StdBox with uid="IXLJACEGLH",left=2, top=180, width=678,height=2

  add object oBox_5_35 as StdBox with uid="QFGGBNTLGP",left=2, top=279, width=678,height=2
enddefine
define class tgsma_aarPag6 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  resizeXpos=316
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_6_1 as stdDynamicChildContainer with uid="AJFPMSPOZX",left=78, top=35, width=530, height=423, bOnScreen=.t.;


  add object oStr_6_4 as StdString with uid="BUEMRZBWWT",Visible=.t., Left=42, Top=11,;
    Alignment=0, Width=191, Height=18,;
    Caption="Selezione attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_6_5 as StdBox with uid="YICDMRIPCF",left=16, top=27, width=678,height=2
enddefine
define class tgsma_aarPag7 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  resizeXpos=496
  resizeYpos=214
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_7_1 as StdField with uid="MNFZSBKPCN",rtseq=191,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=64, Top=16, InputMask=replicate('X',20)

  add object oDESC_7_2 as StdField with uid="LUXAXGLMBH",rtseq=192,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=251, Top=16, InputMask=replicate('X',40)


  add object oLinkPC_7_4 as stdDynamicChildContainer with uid="MOKUBHDBVD",left=0, top=76, width=721, height=267, bOnScreen=.t.;


  add object oARFSRIFE_7_7 as StdField with uid="XSQIXXWMUK",rtseq=200,rtrep=.f.,;
    cFormVar = "w_ARFSRIFE", cQueryName = "ARFSRIFE",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 99433291,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=194, Top=346, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ARFSRIFE"

  func oARFSRIFE_7_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESRIFFS_7_9 as StdField with uid="DWVJHBFGQO",rtseq=201,rtrep=.f.,;
    cFormVar = "w_DESRIFFS", cQueryName = "DESRIFFS",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39648905,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=351, Top=346, InputMask=replicate('X',40)

  add object oStr_7_3 as StdString with uid="DMGMYYYWJF",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=57, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="QQDEYGNNSJ",Visible=.t., Left=41, Top=48,;
    Alignment=0, Width=191, Height=18,;
    Caption="Distinte alternative"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_8 as StdString with uid="OFSJHHKLQI",Visible=.t., Left=31, Top=348,;
    Alignment=1, Width=159, Height=18,;
    Caption="Articolo di riferimento fase:"  ;
  , bGlobalFont=.t.

  add object oBox_7_6 as StdBox with uid="PGMQPBSBTV",left=15, top=64, width=707,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mda",lower(this.oContained.GSMA_MDA.class))=0
        this.oContained.GSMA_MDA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsma_aarPag8 as StdContainer
  Width  = 725
  height = 486
  stdWidth  = 725
  stdheight = 486
  resizeXpos=641
  resizeYpos=326
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_8_1 as stdDynamicChildContainer with uid="GLPHAJYUMZ",left=43, top=56, width=677, height=400, bOnScreen=.t.;


  add object oCODI_8_2 as StdField with uid="MGMRCKKQRA",rtseq=198,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=65, Top=16, InputMask=replicate('X',20)

  add object oDESC_8_3 as StdField with uid="JFXRBNAHPJ",rtseq=199,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=252, Top=16, InputMask=replicate('X',40)

  add object oStr_8_4 as StdString with uid="YWLTJJFBZE",Visible=.t., Left=6, Top=16,;
    Alignment=1, Width=57, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ARTIPART in ('PF','SE','MP','PH','FS')"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ARCODART=ART_ICOL.ARCODART";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsma_aar','ART_ICOL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ARCODART=ART_ICOL.ARCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
