* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bls                                                        *
*              Controllo su articolo in manutenzione prezzi                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_245]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-04                                                      *
* Last revis.: 2000-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bls",oParentObject)
return(i_retval)

define class tgsma_bls as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo articolo da manutenzione prezzi (da GSMA_MLS)
    * --- Se � stata modificata la data di obsolescenza di un articolo in un ordine allora non deve essere visualizzato
    if NOT EMPTY(this.oParentObject.W_DATOBSO) AND this.oParentObject.W_DATOBSO<=this.oParentObject.W_OBTEST AND NOT EMPTY(this.oParentObject.W_LICODART)
      if UPPER(this.oParentObject.W_TIPOPE)="LOAD"
        this.oParentObject.W_LICODART = " "
        this.oParentObject.W_DESART = " "
      else
        oCpToolBar.b4.enabled=.f.
        this.oParentObject.ecpSave()
      endif
      ah_ErrorMsg("Codice articolo obsoleto dal %1",,"",dtoc(this.oParentObject.w_datobso))
    else
      oCpToolBar.b4.enabled=.t.
    endif
    this.bUpdateParentObject=.F.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
