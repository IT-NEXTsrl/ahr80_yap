* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bd2                                                        *
*              Aggiorna mov.di provvigione                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bd2",oParentObject)
return(i_retval)

define class tgsve_bd2 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  MOP_MAST_idx=0
  MOP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Movimenti di Provvigioni (da GSVE_KDP)
    * --- Write into MOP_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPDATSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPDATSCA),'MOP_DETT','MPDATSCA');
      +",MPDATMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPDATMAT),'MOP_DETT','MPDATMAT');
      +",MPPERPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPPERPRA),'MOP_DETT','MPPERPRA');
      +",MPPERPRC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPPERPRC),'MOP_DETT','MPPERPRC');
      +",MPTOTAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPTOTAGE),'MOP_DETT','MPTOTAGE');
      +",MPTOTZON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPTOTZON),'MOP_DETT','MPTOTZON');
      +",MPTIPMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPTIPMAT),'MOP_DETT','MPTIPMAT');
      +",MPDATLIQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPDATLIQ),'MOP_DETT','MPDATLIQ');
          +i_ccchkf ;
      +" where ";
          +"MPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_ROWNUM);
             )
    else
      update (i_cTable) set;
          MPDATSCA = this.oParentObject.w_MPDATSCA;
          ,MPDATMAT = this.oParentObject.w_MPDATMAT;
          ,MPPERPRA = this.oParentObject.w_MPPERPRA;
          ,MPPERPRC = this.oParentObject.w_MPPERPRC;
          ,MPTOTAGE = this.oParentObject.w_MPTOTAGE;
          ,MPTOTZON = this.oParentObject.w_MPTOTZON;
          ,MPTIPMAT = this.oParentObject.w_MPTIPMAT;
          ,MPDATLIQ = this.oParentObject.w_MPDATLIQ;
          &i_ccchkf. ;
       where;
          MPSERIAL = this.oParentObject.w_SERIAL;
          and CPROWNUM = this.oParentObject.w_ROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into MOP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPDESSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MPDESSUP),'MOP_MAST','MPDESSUP');
          +i_ccchkf ;
      +" where ";
          +"MPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MPDESSUP = this.oParentObject.w_MPDESSUP;
          &i_ccchkf. ;
       where;
          MPSERIAL = this.oParentObject.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Rieseguo la Query
     this.oParentObject.oParentObject.oParentObject.NotifyEvent("VariaProv")
    * --- chiudo la maschera di Manutenzione
     this.oParentObject.ecpQuit()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOP_MAST'
    this.cWorkTables[2]='MOP_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
