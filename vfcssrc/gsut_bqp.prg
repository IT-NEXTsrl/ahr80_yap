* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bqp                                                        *
*              Query calendario postit                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-03-13                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bqp",oParentObject)
return(i_retval)

define class tgsut_bqp as StdBatch
  * --- Local variables
  w_COLOR = 0
  w_CRLF = 0
  w_TOOLTIP = space(30)
  w_CODUTE = 0
  * --- WorkFile variables
  GSUT_BQP_idx=0
  postit_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea tabella temporanea per Gadget calendario postit - lanciato da GSUT_BQP
    this.w_CRLF = chr(13)+chr(10)
    this.w_CODUTE = i_codute
    * --- Create temporary table GSUT_BQP
    i_nIdx=cp_AddTableDef('GSUT_BQP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.postit_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"datestart as CalDate, '                                        ' as ToolTip, status as Status, 9999999999 as ActvColor, datestop as DateStop "," from "+i_cTable;
          +" where 1=0";
          )
    this.GSUT_BQP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from postit
    i_nConn=i_TableProp[this.postit_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.postit_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select datestart,status,postit, datestop  from "+i_cTable+" postit ";
          +" where usercode="+cp_ToStrODBC(this.w_CODUTE)+" And (status = 'A' Or status = 'TD')";
           ,"_Curs_postit")
    else
      select datestart,status,postit, datestop from (i_cTable);
       where usercode=this.w_CODUTE And (status = "A" Or status = "TD");
        into cursor _Curs_postit
    endif
    if used('_Curs_postit')
      select _Curs_postit
      locate for 1=1
      do while not(eof())
      this.w_COLOR = Val( SubStr( _Curs_postit.postit, Rat("]",_Curs_postit.postit)+3, Rat(this.w_CRLF,_Curs_postit.postit,6)-3))
      this.w_TOOLTIP = cp_Translate("%A AppuntamentiChr(13)%TD To do")
      * --- Insert into GSUT_BQP
      i_nConn=i_TableProp[this.GSUT_BQP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GSUT_BQP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GSUT_BQP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CalDate"+",ToolTip"+",Status"+",ActvColor"+",DateStop"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(_Curs_postit.datestart),'GSUT_BQP','CalDate');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TOOLTIP),'GSUT_BQP','ToolTip');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_postit.status),'GSUT_BQP','Status');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COLOR),'GSUT_BQP','ActvColor');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_postit.datestop),'GSUT_BQP','DateStop');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CalDate',_Curs_postit.datestart,'ToolTip',this.w_TOOLTIP,'Status',_Curs_postit.status,'ActvColor',this.w_COLOR,'DateStop',_Curs_postit.datestop)
        insert into (i_cTable) (CalDate,ToolTip,Status,ActvColor,DateStop &i_ccchkf. );
           values (;
             _Curs_postit.datestart;
             ,this.w_TOOLTIP;
             ,_Curs_postit.status;
             ,this.w_COLOR;
             ,_Curs_postit.datestop;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_postit
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*GSUT_BQP'
    this.cWorkTables[2]='postit'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_postit')
      use in _Curs_postit
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
