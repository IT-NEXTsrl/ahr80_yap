* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsil_btl                                                        *
*              Relazioni tipo lunghezza                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-20                                                      *
* Last revis.: 2000-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsil_btl",oParentObject)
return(i_retval)

define class tgsil_btl as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_ST__TIPO="C"
        this.oParentObject.w_ST__NDEC = 0
      case this.oParentObject.w_ST__TIPO="N"
        this.oParentObject.w_ST__NDEC = 0
      case this.oParentObject.w_ST__TIPO="D"
        this.oParentObject.w_ST__LUNG = 8
        this.oParentObject.w_ST__NDEC = 0
      case this.oParentObject.w_ST__TIPO="L"
        this.oParentObject.w_ST__LUNG = 1
        this.oParentObject.w_ST__NDEC = 0
      case this.oParentObject.w_ST__TIPO="M"
        this.oParentObject.w_ST__NDEC = 0
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
