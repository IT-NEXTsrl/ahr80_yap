* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aut                                                        *
*              Servizi FAX / Telefono / Mail / PostaLite / Web                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_92]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-17                                                      *
* Last revis.: 2016-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aut"))

* --- Class definition
define class tgsut_aut as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 636
  Height = 485+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-04-21"
  HelpContextID=76104553
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  UTE_NTI_IDX = 0
  cpusers_IDX = 0
  MAGAZZIN_IDX = 0
  GESPENNA_IDX = 0
  CAUMATTI_IDX = 0
  GROUPSJ_IDX = 0
  cpusrgrp_IDX = 0
  cFile = "UTE_NTI"
  cKeySelect = "UTCODICE"
  cKeyWhere  = "UTCODICE=this.w_UTCODICE"
  cKeyWhereODBC = '"UTCODICE="+cp_ToStrODBC(this.w_UTCODICE)';

  cKeyWhereODBCqualified = '"UTE_NTI.UTCODICE="+cp_ToStrODBC(this.w_UTCODICE)';

  cPrg = "gsut_aut"
  cComment = "Servizi FAX / Telefono / Mail / PostaLite / Web"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UTCODICE = 0
  o_UTCODICE = 0
  w_ISALT = .F.
  w_UTCC_EML = space(254)
  w_UTCCNEML = space(254)
  w_UTDESUTE = space(40)
  w_UTDIALOG = space(1)
  o_UTDIALOG = space(1)
  w_DIALOG1 = space(1)
  w_DIALOG2 = space(1)
  w_UTALLEGA = space(1)
  w_TYPEALL = space(10)
  w_UTGESALL = space(1)
  w_UT_FIRMA = space(0)
  w_UTSERVCP = space(1)
  o_UTSERVCP = space(1)
  w_UTPOSLIT = space(1)
  o_UTPOSLIT = space(1)
  w_UTAVVSPL = space(1)
  w_UTSERVWE = space(1)
  w_UTARCHI = space(1)
  w_UTDESUTE = space(40)
  w_UTCODICE = 0
  w_UTDIAFAX = space(1)
  w_UTFRTFAX = space(3)
  w_UTFROFAX = space(1)
  w_UTTIPFAX = space(1)
  w_UTDESUTE = space(40)
  w_UTCODICE = 0
  w_UTDESUTE = space(40)
  w_UTCODICE = 0
  w_UTTELEFO = space(18)
  w_UTTELEF2 = space(18)
  w_UTTIPSKY = space(1)
  w_UTIDUSER = space(254)
  w_UTIDPSSW = space(254)
  w_UTNUMCEL = space(18)
  w_UTATT_IN = space(20)
  w_UTATTOUT = space(20)
  w_UTPHDTIM = ctot('')
  w_UTSELTRA = space(1)
  w_UTSELSNM = space(1)
  w_UTDIRCHI = space(1)
  w_UTTELSUP = space(6)
  o_UTTELSUP = space(6)
  w_ORE = 0
  w_MINUTI = 0
  w_SECONDI = 0
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_UT_OGGET = space(254)
  w_UTMAILFR = space(10)
  w_GRPSCHED = 0
  w_UTESCHED = 0
  w_UTJOBASS = space(1)
  w_UTEMAIL = space(254)
  o_UTEMAIL = space(254)
  w_AMCC_EML = space(254)
  w_AMCCNEML = space(254)
  w_AM_FIRMA = space(0)
  w_AMSHARED = space(1)

  * --- Children pointers
  gsut_mmu = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_aut
  proc SetGlobalVars
  if not btrserr 
    If g_CODUTE=this.w_UTCODICE
      With this
        g_UEFORM = .w_UTMAILFR
        g_UETIAL = .w_UTALLEGA
        g_UEDIAL = .w_UTDIALOG
        g_UEFIRM = .w_UT_FIRMA
        g_TIPFAX = .w_UTTIPFAX
        g_FRTFAX = IIF(.w_UTFRTFAX='RTF', 7, IIF(.w_UTFRTFAX='HTM', 6, 5))
        g_DIAFAX = .w_UTDIAFAX
        g_DESUTE = .w_UTDESUTE
        g_WEUTEENABLED = .w_UTSERVWE
        g_ZCPUENABLED = .w_UTSERVCP
        * Supporto MAIL --- CFGMAIL imposta: g_MITTEN,g_INVIO,g_SRVMAIL,g_SRVPORTA,g_CCEmail,g_CCNEmail,g_UEFIRM
        g_MITTEN = CFGMAIL(g_CODUTE,'S','',.T.)
        g_SkypeService=.w_UTTIPSKY
      Endwith
    Else
        ah_ErrorMsg("Le impostazioni saranno attive al prossimo rientro dell'utente %1",,,Alltrim(Str(this.w_UTCODICE)))
    Endif
  Endif
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'UTE_NTI','gsut_aut')
    stdPageFrame::Init()
    *set procedure to gsut_mmu additive
    with this
      .Pages(1).addobject("oPag","tgsut_autPag1","gsut_aut",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 201128241
      .Pages(2).addobject("oPag","tgsut_autPag2","gsut_aut",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Abilitazione servizi")
      .Pages(2).HelpContextID = 207034850
      .Pages(3).addobject("oPag","tgsut_autPag3","gsut_aut",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("FAX")
      .Pages(3).HelpContextID = 75726250
      .Pages(4).addobject("oPag","tgsut_autPag4","gsut_aut",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Telefono")
      .Pages(4).HelpContextID = 21161893
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUTCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsut_mmu
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='GESPENNA'
    this.cWorkTables[4]='CAUMATTI'
    this.cWorkTables[5]='GROUPSJ'
    this.cWorkTables[6]='cpusrgrp'
    this.cWorkTables[7]='UTE_NTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UTE_NTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UTE_NTI_IDX,3]
  return

  function CreateChildren()
    this.gsut_mmu = CREATEOBJECT('stdDynamicChild',this,'gsut_mmu',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.gsut_mmu.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsut_mmu)
      this.gsut_mmu.DestroyChildrenChain()
      this.gsut_mmu=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsut_mmu.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsut_mmu.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsut_mmu.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsut_mmu.SetKey(;
            .w_UTCODICE,"UMCODUTE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsut_mmu.ChangeRow(this.cRowID+'      1',1;
             ,.w_UTCODICE,"UMCODUTE";
             )
      .WriteTo_gsut_mmu()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.gsut_mmu)
        i_f=.gsut_mmu.BuildFilter()
        if !(i_f==.gsut_mmu.cQueryFilter)
          i_fnidx=.gsut_mmu.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gsut_mmu.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gsut_mmu.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gsut_mmu.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gsut_mmu.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_gsut_mmu()
  if at('gsut_mmu',lower(this.gsut_mmu.class))<>0
    if this.gsut_mmu.w_UMDIALOG<>this.w_UTDIALOG or this.gsut_mmu.w_UTEMAIL<>this.w_UTEMAIL
      this.gsut_mmu.w_UMDIALOG = this.w_UTDIALOG
      this.gsut_mmu.w_UTEMAIL = this.w_UTEMAIL
      this.gsut_mmu.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_UTCODICE = NVL(UTCODICE,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from UTE_NTI where UTCODICE=KeySet.UTCODICE
    *
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UTE_NTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UTE_NTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UTE_NTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UTCODICE',this.w_UTCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = IsAlt()
        .w_DIALOG1 = 'S'
        .w_DIALOG2 = 'P'
        .w_TYPEALL = 'PDF'
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_GRPSCHED = LOOKTAB('GROUPSJ', 'GJCODICE', '1', 1)
        .w_AMSHARED = space(1)
        .w_UTCODICE = NVL(UTCODICE,0)
          .link_1_1('Load')
        .w_UTCC_EML = NVL(UTCC_EML,space(254))
        .w_UTCCNEML = NVL(UTCCNEML,space(254))
        .w_UTDESUTE = NVL(UTDESUTE,space(40))
        .w_UTDIALOG = NVL(UTDIALOG,space(1))
        .w_UTALLEGA = NVL(UTALLEGA,space(1))
        .w_UTGESALL = NVL(UTGESALL,space(1))
        .w_UT_FIRMA = NVL(UT_FIRMA,space(0))
        .w_UTSERVCP = NVL(UTSERVCP,space(1))
        .w_UTPOSLIT = NVL(UTPOSLIT,space(1))
        .w_UTAVVSPL = NVL(UTAVVSPL,space(1))
        .w_UTSERVWE = NVL(UTSERVWE,space(1))
        .w_UTARCHI = NVL(UTARCHI,space(1))
        .w_UTDESUTE = NVL(UTDESUTE,space(40))
        .w_UTCODICE = .w_UTCODICE
        .w_UTDIAFAX = NVL(UTDIAFAX,space(1))
        .w_UTFRTFAX = NVL(UTFRTFAX,space(3))
        .w_UTFROFAX = NVL(UTFROFAX,space(1))
        .w_UTTIPFAX = NVL(UTTIPFAX,space(1))
        .w_UTDESUTE = NVL(UTDESUTE,space(40))
        .w_UTCODICE = .w_UTCODICE
        .w_UTDESUTE = NVL(UTDESUTE,space(40))
        .w_UTCODICE = .w_UTCODICE
        .w_UTTELEFO = NVL(UTTELEFO,space(18))
        .w_UTTELEF2 = NVL(UTTELEF2,space(18))
        .w_UTTIPSKY = NVL(UTTIPSKY,space(1))
        .w_UTIDUSER = NVL(UTIDUSER,space(254))
        .w_UTIDPSSW = NVL(UTIDPSSW,space(254))
        .w_UTNUMCEL = NVL(UTNUMCEL,space(18))
        .w_UTATT_IN = NVL(UTATT_IN,space(20))
        .w_UTATTOUT = NVL(UTATTOUT,space(20))
        .w_UTPHDTIM = NVL(UTPHDTIM,ctot(""))
        .w_UTSELTRA = NVL(UTSELTRA,space(1))
        .w_UTSELSNM = NVL(UTSELSNM,space(1))
        .w_UTDIRCHI = NVL(UTDIRCHI,space(1))
        .w_UTTELSUP = NVL(UTTELSUP,space(6))
        .w_ORE = val(left(.w_UTTELSUP, 2))
        .w_MINUTI = val(substr(.w_UTTELSUP, 3, 2))
        .w_SECONDI = val(right(.w_UTTELSUP, 2))
        .w_UT_OGGET = NVL(UT_OGGET,space(254))
        .w_UTMAILFR = NVL(UTMAILFR,space(10))
        .w_UTESCHED = .w_UTCODICE
          .link_2_22('Load')
        .w_UTJOBASS = NVL(UTJOBASS,space(1))
        .w_UTEMAIL = NVL(UTEMAIL,space(254))
        .w_AMCC_EML = .w_AMCC_EML
        .w_AMCCNEML = .w_AMCCNEML
        .w_AM_FIRMA = .w_AM_FIRMA
        cp_LoadRecExtFlds(this,'UTE_NTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UTCODICE = 0
      .w_ISALT = .f.
      .w_UTCC_EML = space(254)
      .w_UTCCNEML = space(254)
      .w_UTDESUTE = space(40)
      .w_UTDIALOG = space(1)
      .w_DIALOG1 = space(1)
      .w_DIALOG2 = space(1)
      .w_UTALLEGA = space(1)
      .w_TYPEALL = space(10)
      .w_UTGESALL = space(1)
      .w_UT_FIRMA = space(0)
      .w_UTSERVCP = space(1)
      .w_UTPOSLIT = space(1)
      .w_UTAVVSPL = space(1)
      .w_UTSERVWE = space(1)
      .w_UTARCHI = space(1)
      .w_UTDESUTE = space(40)
      .w_UTCODICE = 0
      .w_UTDIAFAX = space(1)
      .w_UTFRTFAX = space(3)
      .w_UTFROFAX = space(1)
      .w_UTTIPFAX = space(1)
      .w_UTDESUTE = space(40)
      .w_UTCODICE = 0
      .w_UTDESUTE = space(40)
      .w_UTCODICE = 0
      .w_UTTELEFO = space(18)
      .w_UTTELEF2 = space(18)
      .w_UTTIPSKY = space(1)
      .w_UTIDUSER = space(254)
      .w_UTIDPSSW = space(254)
      .w_UTNUMCEL = space(18)
      .w_UTATT_IN = space(20)
      .w_UTATTOUT = space(20)
      .w_UTPHDTIM = ctot("")
      .w_UTSELTRA = space(1)
      .w_UTSELSNM = space(1)
      .w_UTDIRCHI = space(1)
      .w_UTTELSUP = space(6)
      .w_ORE = 0
      .w_MINUTI = 0
      .w_SECONDI = 0
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_UT_OGGET = space(254)
      .w_UTMAILFR = space(10)
      .w_GRPSCHED = 0
      .w_UTESCHED = 0
      .w_UTJOBASS = space(1)
      .w_UTEMAIL = space(254)
      .w_AMCC_EML = space(254)
      .w_AMCCNEML = space(254)
      .w_AM_FIRMA = space(0)
      .w_AMSHARED = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_UTCODICE))
          .link_1_1('Full')
          endif
        .w_ISALT = IsAlt()
          .DoRTCalc(3,6,.f.)
        .w_DIALOG1 = 'S'
        .w_DIALOG2 = 'P'
        .w_UTALLEGA = IIF(.w_UTDIALOG='S', IIF(EMPTY(.w_UTALLEGA),'A', .w_UTALLEGA) ,'A')
        .w_TYPEALL = 'PDF'
        .w_UTGESALL = 'N'
          .DoRTCalc(12,14,.f.)
        .w_UTAVVSPL = iif(.w_UTPOSLIT='S',.w_UTAVVSPL,' ')
          .DoRTCalc(16,18,.f.)
        .w_UTCODICE = .w_UTCODICE
          .DoRTCalc(20,20,.f.)
        .w_UTFRTFAX = 'PDF'
        .w_UTFROFAX = 'N'
        .w_UTTIPFAX = 'N'
          .DoRTCalc(24,24,.f.)
        .w_UTCODICE = .w_UTCODICE
          .DoRTCalc(26,26,.f.)
        .w_UTCODICE = .w_UTCODICE
          .DoRTCalc(28,29,.f.)
        .w_UTTIPSKY = 'N'
          .DoRTCalc(31,36,.f.)
        .w_UTSELTRA = 'S'
        .w_UTSELSNM = 'S'
        .w_UTDIRCHI = 'T'
        .w_UTTELSUP = RIGHT('00'+alltrim(STR(.w_ORE,2)), 2)+RIGHT('00'+alltrim(STR(.w_MINUTI,2)), 2)+RIGHT('00'+alltrim(STR(.w_SECONDI,2)), 2)
        .w_ORE = val(left(.w_UTTELSUP, 2))
        .w_MINUTI = val(substr(.w_UTTELSUP, 3, 2))
        .w_SECONDI = val(right(.w_UTTELSUP, 2))
        .w_DTBSO_CAUMATTI = i_DatSys
          .DoRTCalc(45,45,.f.)
        .w_UTMAILFR = 'PDF'
        .w_GRPSCHED = LOOKTAB('GROUPSJ', 'GJCODICE', '1', 1)
        .w_UTESCHED = .w_UTCODICE
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_UTESCHED))
          .link_2_22('Full')
          endif
        .w_UTJOBASS = 'N'
          .DoRTCalc(50,50,.f.)
        .w_AMCC_EML = .w_AMCC_EML
        .w_AMCCNEML = .w_AMCCNEML
        .w_AM_FIRMA = .w_AM_FIRMA
      endif
    endwith
    cp_BlankRecExtFlds(this,'UTE_NTI')
    this.DoRTCalc(54,54,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_aut
    *--- Nascondo box
    If !IsAlt()
       This.oPgfrm.Page4.oPag.oBox_4_28.Visible = .f.
       This.oPgfrm.Page4.oPag.oBox_4_29.Visible = .f.
    EndIf
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oUTCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oUTCC_EML_1_4.enabled = i_bVal
      .Page1.oPag.oUTCCNEML_1_5.enabled = i_bVal
      .Page1.oPag.oUTDESUTE_1_6.enabled = i_bVal
      .Page1.oPag.oDIALOG1_1_8.enabled_(i_bVal)
      .Page1.oPag.oDIALOG2_1_9.enabled = i_bVal
      .Page1.oPag.oUTALLEGA_1_10.enabled_(i_bVal)
      .Page1.oPag.oTYPEALL_1_11.enabled = i_bVal
      .Page1.oPag.oUTGESALL_1_12.enabled = i_bVal
      .Page1.oPag.oUT_FIRMA_1_19.enabled = i_bVal
      .Page2.oPag.oUTSERVCP_2_1.enabled = i_bVal
      .Page2.oPag.oUTPOSLIT_2_3.enabled = i_bVal
      .Page2.oPag.oUTAVVSPL_2_4.enabled = i_bVal
      .Page2.oPag.oUTSERVWE_2_5.enabled = i_bVal
      .Page2.oPag.oUTARCHI_2_6.enabled = i_bVal
      .Page3.oPag.oUTDIAFAX_3_8.enabled_(i_bVal)
      .Page3.oPag.oUTFRTFAX_3_9.enabled = i_bVal
      .Page3.oPag.oUTFROFAX_3_10.enabled = i_bVal
      .Page3.oPag.oUTTIPFAX_3_12.enabled = i_bVal
      .Page4.oPag.oUTTELEFO_4_4.enabled = i_bVal
      .Page4.oPag.oUTTELEF2_4_6.enabled = i_bVal
      .Page2.oPag.oUTTIPSKY_2_16.enabled = i_bVal
      .Page4.oPag.oUTNUMCEL_4_8.enabled = i_bVal
      .Page4.oPag.oUTATT_IN_4_9.enabled = i_bVal
      .Page4.oPag.oUTATTOUT_4_10.enabled = i_bVal
      .Page4.oPag.oUTPHDTIM_4_11.enabled = i_bVal
      .Page4.oPag.oUTSELTRA_4_13.enabled_(i_bVal)
      .Page4.oPag.oUTSELSNM_4_14.enabled_(i_bVal)
      .Page4.oPag.oUTDIRCHI_4_15.enabled_(i_bVal)
      .Page4.oPag.oORE_4_22.enabled = i_bVal
      .Page4.oPag.oMINUTI_4_23.enabled = i_bVal
      .Page4.oPag.oSECONDI_4_24.enabled = i_bVal
      .Page1.oPag.oUT_OGGET_1_27.enabled = i_bVal
      .Page2.oPag.oUTJOBASS_2_23.enabled = i_bVal
      .Page1.oPag.oUTEMAIL_1_30.enabled = i_bVal
      .Page2.oPag.oBtn_2_2.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oUTCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oUTCODICE_1_1.enabled = .t.
      endif
    endwith
    this.gsut_mmu.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'UTE_NTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsut_mmu.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCODICE,"UTCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC_EML,"UTCC_EML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCCNEML,"UTCCNEML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDESUTE,"UTDESUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDIALOG,"UTDIALOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTALLEGA,"UTALLEGA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTGESALL,"UTGESALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UT_FIRMA,"UT_FIRMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTSERVCP,"UTSERVCP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTPOSLIT,"UTPOSLIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTAVVSPL,"UTAVVSPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTSERVWE,"UTSERVWE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTARCHI,"UTARCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDESUTE,"UTDESUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDIAFAX,"UTDIAFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTFRTFAX,"UTFRTFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTFROFAX,"UTFROFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTTIPFAX,"UTTIPFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDESUTE,"UTDESUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDESUTE,"UTDESUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTTELEFO,"UTTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTTELEF2,"UTTELEF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTTIPSKY,"UTTIPSKY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTIDUSER,"UTIDUSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTIDPSSW,"UTIDPSSW",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTNUMCEL,"UTNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTATT_IN,"UTATT_IN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTATTOUT,"UTATTOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTPHDTIM,"UTPHDTIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTSELTRA,"UTSELTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTSELSNM,"UTSELSNM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDIRCHI,"UTDIRCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTTELSUP,"UTTELSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UT_OGGET,"UT_OGGET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTMAILFR,"UTMAILFR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTJOBASS,"UTJOBASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTEMAIL,"UTEMAIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    i_lTable = "UTE_NTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.UTE_NTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.UTE_NTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into UTE_NTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UTE_NTI')
        i_extval=cp_InsertValODBCExtFlds(this,'UTE_NTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(UTCODICE,UTCC_EML,UTCCNEML,UTDESUTE,UTDIALOG"+;
                  ",UTALLEGA,UTGESALL,UT_FIRMA,UTSERVCP,UTPOSLIT"+;
                  ",UTAVVSPL,UTSERVWE,UTARCHI,UTDIAFAX,UTFRTFAX"+;
                  ",UTFROFAX,UTTIPFAX,UTTELEFO,UTTELEF2,UTTIPSKY"+;
                  ",UTIDUSER,UTIDPSSW,UTNUMCEL,UTATT_IN,UTATTOUT"+;
                  ",UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP"+;
                  ",UT_OGGET,UTMAILFR,UTJOBASS,UTEMAIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_UTCODICE)+;
                  ","+cp_ToStrODBC(this.w_UTCC_EML)+;
                  ","+cp_ToStrODBC(this.w_UTCCNEML)+;
                  ","+cp_ToStrODBC(this.w_UTDESUTE)+;
                  ","+cp_ToStrODBC(this.w_UTDIALOG)+;
                  ","+cp_ToStrODBC(this.w_UTALLEGA)+;
                  ","+cp_ToStrODBC(this.w_UTGESALL)+;
                  ","+cp_ToStrODBC(this.w_UT_FIRMA)+;
                  ","+cp_ToStrODBC(this.w_UTSERVCP)+;
                  ","+cp_ToStrODBC(this.w_UTPOSLIT)+;
                  ","+cp_ToStrODBC(this.w_UTAVVSPL)+;
                  ","+cp_ToStrODBC(this.w_UTSERVWE)+;
                  ","+cp_ToStrODBC(this.w_UTARCHI)+;
                  ","+cp_ToStrODBC(this.w_UTDIAFAX)+;
                  ","+cp_ToStrODBC(this.w_UTFRTFAX)+;
                  ","+cp_ToStrODBC(this.w_UTFROFAX)+;
                  ","+cp_ToStrODBC(this.w_UTTIPFAX)+;
                  ","+cp_ToStrODBC(this.w_UTTELEFO)+;
                  ","+cp_ToStrODBC(this.w_UTTELEF2)+;
                  ","+cp_ToStrODBC(this.w_UTTIPSKY)+;
                  ","+cp_ToStrODBC(this.w_UTIDUSER)+;
                  ","+cp_ToStrODBC(this.w_UTIDPSSW)+;
                  ","+cp_ToStrODBC(this.w_UTNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_UTATT_IN)+;
                  ","+cp_ToStrODBC(this.w_UTATTOUT)+;
                  ","+cp_ToStrODBC(this.w_UTPHDTIM)+;
                  ","+cp_ToStrODBC(this.w_UTSELTRA)+;
                  ","+cp_ToStrODBC(this.w_UTSELSNM)+;
                  ","+cp_ToStrODBC(this.w_UTDIRCHI)+;
                  ","+cp_ToStrODBC(this.w_UTTELSUP)+;
                  ","+cp_ToStrODBC(this.w_UT_OGGET)+;
                  ","+cp_ToStrODBC(this.w_UTMAILFR)+;
                  ","+cp_ToStrODBC(this.w_UTJOBASS)+;
                  ","+cp_ToStrODBC(this.w_UTEMAIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UTE_NTI')
        i_extval=cp_InsertValVFPExtFlds(this,'UTE_NTI')
        cp_CheckDeletedKey(i_cTable,0,'UTCODICE',this.w_UTCODICE)
        INSERT INTO (i_cTable);
              (UTCODICE,UTCC_EML,UTCCNEML,UTDESUTE,UTDIALOG,UTALLEGA,UTGESALL,UT_FIRMA,UTSERVCP,UTPOSLIT,UTAVVSPL,UTSERVWE,UTARCHI,UTDIAFAX,UTFRTFAX,UTFROFAX,UTTIPFAX,UTTELEFO,UTTELEF2,UTTIPSKY,UTIDUSER,UTIDPSSW,UTNUMCEL,UTATT_IN,UTATTOUT,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP,UT_OGGET,UTMAILFR,UTJOBASS,UTEMAIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_UTCODICE;
                  ,this.w_UTCC_EML;
                  ,this.w_UTCCNEML;
                  ,this.w_UTDESUTE;
                  ,this.w_UTDIALOG;
                  ,this.w_UTALLEGA;
                  ,this.w_UTGESALL;
                  ,this.w_UT_FIRMA;
                  ,this.w_UTSERVCP;
                  ,this.w_UTPOSLIT;
                  ,this.w_UTAVVSPL;
                  ,this.w_UTSERVWE;
                  ,this.w_UTARCHI;
                  ,this.w_UTDIAFAX;
                  ,this.w_UTFRTFAX;
                  ,this.w_UTFROFAX;
                  ,this.w_UTTIPFAX;
                  ,this.w_UTTELEFO;
                  ,this.w_UTTELEF2;
                  ,this.w_UTTIPSKY;
                  ,this.w_UTIDUSER;
                  ,this.w_UTIDPSSW;
                  ,this.w_UTNUMCEL;
                  ,this.w_UTATT_IN;
                  ,this.w_UTATTOUT;
                  ,this.w_UTPHDTIM;
                  ,this.w_UTSELTRA;
                  ,this.w_UTSELSNM;
                  ,this.w_UTDIRCHI;
                  ,this.w_UTTELSUP;
                  ,this.w_UT_OGGET;
                  ,this.w_UTMAILFR;
                  ,this.w_UTJOBASS;
                  ,this.w_UTEMAIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gsut_aut
    this.SetGlobalVars()
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.UTE_NTI_IDX,i_nConn)
      *
      * update UTE_NTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_NTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UTCC_EML="+cp_ToStrODBC(this.w_UTCC_EML)+;
             ",UTCCNEML="+cp_ToStrODBC(this.w_UTCCNEML)+;
             ",UTDESUTE="+cp_ToStrODBC(this.w_UTDESUTE)+;
             ",UTDIALOG="+cp_ToStrODBC(this.w_UTDIALOG)+;
             ",UTALLEGA="+cp_ToStrODBC(this.w_UTALLEGA)+;
             ",UTGESALL="+cp_ToStrODBC(this.w_UTGESALL)+;
             ",UT_FIRMA="+cp_ToStrODBC(this.w_UT_FIRMA)+;
             ",UTSERVCP="+cp_ToStrODBC(this.w_UTSERVCP)+;
             ",UTPOSLIT="+cp_ToStrODBC(this.w_UTPOSLIT)+;
             ",UTAVVSPL="+cp_ToStrODBC(this.w_UTAVVSPL)+;
             ",UTSERVWE="+cp_ToStrODBC(this.w_UTSERVWE)+;
             ",UTARCHI="+cp_ToStrODBC(this.w_UTARCHI)+;
             ",UTDIAFAX="+cp_ToStrODBC(this.w_UTDIAFAX)+;
             ",UTFRTFAX="+cp_ToStrODBC(this.w_UTFRTFAX)+;
             ",UTFROFAX="+cp_ToStrODBC(this.w_UTFROFAX)+;
             ",UTTIPFAX="+cp_ToStrODBC(this.w_UTTIPFAX)+;
             ",UTTELEFO="+cp_ToStrODBC(this.w_UTTELEFO)+;
             ",UTTELEF2="+cp_ToStrODBC(this.w_UTTELEF2)+;
             ",UTTIPSKY="+cp_ToStrODBC(this.w_UTTIPSKY)+;
             ",UTIDUSER="+cp_ToStrODBC(this.w_UTIDUSER)+;
             ",UTIDPSSW="+cp_ToStrODBC(this.w_UTIDPSSW)+;
             ",UTNUMCEL="+cp_ToStrODBC(this.w_UTNUMCEL)+;
             ",UTATT_IN="+cp_ToStrODBC(this.w_UTATT_IN)+;
             ",UTATTOUT="+cp_ToStrODBC(this.w_UTATTOUT)+;
             ",UTPHDTIM="+cp_ToStrODBC(this.w_UTPHDTIM)+;
             ",UTSELTRA="+cp_ToStrODBC(this.w_UTSELTRA)+;
             ",UTSELSNM="+cp_ToStrODBC(this.w_UTSELSNM)+;
             ",UTDIRCHI="+cp_ToStrODBC(this.w_UTDIRCHI)+;
             ",UTTELSUP="+cp_ToStrODBC(this.w_UTTELSUP)+;
             ",UT_OGGET="+cp_ToStrODBC(this.w_UT_OGGET)+;
             ",UTMAILFR="+cp_ToStrODBC(this.w_UTMAILFR)+;
             ",UTJOBASS="+cp_ToStrODBC(this.w_UTJOBASS)+;
             ",UTEMAIL="+cp_ToStrODBC(this.w_UTEMAIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_NTI')
        i_cWhere = cp_PKFox(i_cTable  ,'UTCODICE',this.w_UTCODICE  )
        UPDATE (i_cTable) SET;
              UTCC_EML=this.w_UTCC_EML;
             ,UTCCNEML=this.w_UTCCNEML;
             ,UTDESUTE=this.w_UTDESUTE;
             ,UTDIALOG=this.w_UTDIALOG;
             ,UTALLEGA=this.w_UTALLEGA;
             ,UTGESALL=this.w_UTGESALL;
             ,UT_FIRMA=this.w_UT_FIRMA;
             ,UTSERVCP=this.w_UTSERVCP;
             ,UTPOSLIT=this.w_UTPOSLIT;
             ,UTAVVSPL=this.w_UTAVVSPL;
             ,UTSERVWE=this.w_UTSERVWE;
             ,UTARCHI=this.w_UTARCHI;
             ,UTDIAFAX=this.w_UTDIAFAX;
             ,UTFRTFAX=this.w_UTFRTFAX;
             ,UTFROFAX=this.w_UTFROFAX;
             ,UTTIPFAX=this.w_UTTIPFAX;
             ,UTTELEFO=this.w_UTTELEFO;
             ,UTTELEF2=this.w_UTTELEF2;
             ,UTTIPSKY=this.w_UTTIPSKY;
             ,UTIDUSER=this.w_UTIDUSER;
             ,UTIDPSSW=this.w_UTIDPSSW;
             ,UTNUMCEL=this.w_UTNUMCEL;
             ,UTATT_IN=this.w_UTATT_IN;
             ,UTATTOUT=this.w_UTATTOUT;
             ,UTPHDTIM=this.w_UTPHDTIM;
             ,UTSELTRA=this.w_UTSELTRA;
             ,UTSELSNM=this.w_UTSELSNM;
             ,UTDIRCHI=this.w_UTDIRCHI;
             ,UTTELSUP=this.w_UTTELSUP;
             ,UT_OGGET=this.w_UT_OGGET;
             ,UTMAILFR=this.w_UTMAILFR;
             ,UTJOBASS=this.w_UTJOBASS;
             ,UTEMAIL=this.w_UTEMAIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsut_mmu : Saving
      this.gsut_mmu.ChangeRow(this.cRowID+'      1',0;
             ,this.w_UTCODICE,"UMCODUTE";
             )
      this.gsut_mmu.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsut_aut
    this.SetGlobalVars()
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gsut_mmu : Deleting
    this.gsut_mmu.ChangeRow(this.cRowID+'      1',0;
           ,this.w_UTCODICE,"UMCODUTE";
           )
    this.gsut_mmu.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.UTE_NTI_IDX,i_nConn)
      *
      * delete UTE_NTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'UTCODICE',this.w_UTCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    if i_bUpd
      with this
        if  .o_UTDIALOG<>.w_UTDIALOG.or. .o_UTEMAIL<>.w_UTEMAIL
          .WriteTo_gsut_mmu()
        endif
        .DoRTCalc(1,8,.t.)
        if .o_UTDIALOG<>.w_UTDIALOG
            .w_UTALLEGA = IIF(.w_UTDIALOG='S', IIF(EMPTY(.w_UTALLEGA),'A', .w_UTALLEGA) ,'A')
        endif
        .DoRTCalc(10,14,.t.)
        if .o_UTPOSLIT<>.w_UTPOSLIT
            .w_UTAVVSPL = iif(.w_UTPOSLIT='S',.w_UTAVVSPL,' ')
        endif
        .DoRTCalc(16,18,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_UTCODICE = .w_UTCODICE
        endif
        .DoRTCalc(20,24,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_UTCODICE = .w_UTCODICE
        endif
        .DoRTCalc(26,26,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_UTCODICE = .w_UTCODICE
        endif
        if .o_UTSERVCP<>.w_UTSERVCP
          .Calculate_BZMRDXUHBZ()
        endif
        .DoRTCalc(28,39,.t.)
            .w_UTTELSUP = RIGHT('00'+alltrim(STR(.w_ORE,2)), 2)+RIGHT('00'+alltrim(STR(.w_MINUTI,2)), 2)+RIGHT('00'+alltrim(STR(.w_SECONDI,2)), 2)
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_ORE = val(left(.w_UTTELSUP, 2))
        endif
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_MINUTI = val(substr(.w_UTTELSUP, 3, 2))
        endif
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_SECONDI = val(right(.w_UTTELSUP, 2))
        endif
        .DoRTCalc(44,47,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_UTESCHED = .w_UTCODICE
          .link_2_22('Full')
        endif
        .DoRTCalc(49,50,.t.)
            .w_AMCC_EML = .w_AMCC_EML
            .w_AMCCNEML = .w_AMCCNEML
            .w_AM_FIRMA = .w_AM_FIRMA
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(54,54,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_MAAXRDUPZR()
    with this
          * --- Gsut_but('DEL')
          gsut_but(this;
              ,'DEL';
             )
    endwith
  endproc
  proc Calculate_OBDBQJGFJS()
    with this
          * --- Gsut_but('INI')
          gsut_but(this;
              ,'INI';
             )
    endwith
  endproc
  proc Calculate_YXYVWPWSYP()
    with this
          * --- Valorizzo la combo dei tipi al caricamento del record
          .w_TYPEALL = .w_UTMAILFR
    endwith
  endproc
  proc Calculate_BZMRDXUHBZ()
    with this
          * --- se disabilito web application
          .w_UTIDUSER = ''
          .w_UTIDPSSW = ''
    endwith
  endproc
  proc Calculate_MKKRJXZDIC()
    with this
          * --- Selezione Web Appl. a nuovo record
     if g_REVI='S'
          .w_UTSERVCP = 'S'
     endif
    endwith
  endproc
  proc Calculate_XHWHVYGMDW()
    with this
          * --- Valorizza DIALOG1
     if g_DOCM<>'S'
          .w_DIALOG1 = .w_UTDIALOG
     endif
    endwith
  endproc
  proc Calculate_VRNSQAYMND()
    with this
          * --- Valorizza DIALOG2
     if g_DOCM='S'
          .w_DIALOG2 = .w_UTDIALOG
     endif
    endwith
  endproc
  proc Calculate_EJWTWOCXSE()
    with this
          * --- Salvataggio valore UTDIALOG
          .w_UTDIALOG = IIF( g_DOCM<>'S', .w_DIALOG1, .w_DIALOG2)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oUTALLEGA_1_10.enabled_(this.oPgFrm.Page1.oPag.oUTALLEGA_1_10.mCond())
    this.oPgFrm.Page2.oPag.oUTSERVCP_2_1.enabled = this.oPgFrm.Page2.oPag.oUTSERVCP_2_1.mCond()
    this.oPgFrm.Page2.oPag.oUTAVVSPL_2_4.enabled = this.oPgFrm.Page2.oPag.oUTAVVSPL_2_4.mCond()
    this.oPgFrm.Page2.oPag.oUTSERVWE_2_5.enabled = this.oPgFrm.Page2.oPag.oUTSERVWE_2_5.mCond()
    this.oPgFrm.Page2.oPag.oUTARCHI_2_6.enabled = this.oPgFrm.Page2.oPag.oUTARCHI_2_6.mCond()
    this.oPgFrm.Page3.oPag.oUTDIAFAX_3_8.enabled_(this.oPgFrm.Page3.oPag.oUTDIAFAX_3_8.mCond())
    this.oPgFrm.Page3.oPag.oUTFRTFAX_3_9.enabled = this.oPgFrm.Page3.oPag.oUTFRTFAX_3_9.mCond()
    this.oPgFrm.Page4.oPag.oUTATT_IN_4_9.enabled = this.oPgFrm.Page4.oPag.oUTATT_IN_4_9.mCond()
    this.oPgFrm.Page4.oPag.oUTATTOUT_4_10.enabled = this.oPgFrm.Page4.oPag.oUTATTOUT_4_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show3
    i_show3=not(g_AGFA<>'S')
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Telefono"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    this.oPgFrm.Page1.oPag.oUTCC_EML_1_4.visible=!this.oPgFrm.Page1.oPag.oUTCC_EML_1_4.mHide()
    this.oPgFrm.Page1.oPag.oUTCCNEML_1_5.visible=!this.oPgFrm.Page1.oPag.oUTCCNEML_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDIALOG1_1_8.visible=!this.oPgFrm.Page1.oPag.oDIALOG1_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDIALOG2_1_9.visible=!this.oPgFrm.Page1.oPag.oDIALOG2_1_9.mHide()
    this.oPgFrm.Page1.oPag.oUTGESALL_1_12.visible=!this.oPgFrm.Page1.oPag.oUTGESALL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oUT_FIRMA_1_19.visible=!this.oPgFrm.Page1.oPag.oUT_FIRMA_1_19.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_2.visible=!this.oPgFrm.Page2.oPag.oBtn_2_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page4.oPag.oUTPHDTIM_4_11.visible=!this.oPgFrm.Page4.oPag.oUTPHDTIM_4_11.mHide()
    this.oPgFrm.Page4.oPag.oUTSELTRA_4_13.visible=!this.oPgFrm.Page4.oPag.oUTSELTRA_4_13.mHide()
    this.oPgFrm.Page4.oPag.oUTSELSNM_4_14.visible=!this.oPgFrm.Page4.oPag.oUTSELSNM_4_14.mHide()
    this.oPgFrm.Page4.oPag.oUTDIRCHI_4_15.visible=!this.oPgFrm.Page4.oPag.oUTDIRCHI_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_16.visible=!this.oPgFrm.Page4.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_18.visible=!this.oPgFrm.Page4.oPag.oStr_4_18.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_19.visible=!this.oPgFrm.Page4.oPag.oStr_4_19.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_20.visible=!this.oPgFrm.Page4.oPag.oStr_4_20.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_21.visible=!this.oPgFrm.Page4.oPag.oStr_4_21.mHide()
    this.oPgFrm.Page4.oPag.oORE_4_22.visible=!this.oPgFrm.Page4.oPag.oORE_4_22.mHide()
    this.oPgFrm.Page4.oPag.oMINUTI_4_23.visible=!this.oPgFrm.Page4.oPag.oMINUTI_4_23.mHide()
    this.oPgFrm.Page4.oPag.oSECONDI_4_24.visible=!this.oPgFrm.Page4.oPag.oSECONDI_4_24.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_25.visible=!this.oPgFrm.Page4.oPag.oStr_4_25.mHide()
    this.oPgFrm.Page1.oPag.oUT_OGGET_1_27.visible=!this.oPgFrm.Page1.oPag.oUT_OGGET_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page2.oPag.oUTJOBASS_2_23.visible=!this.oPgFrm.Page2.oPag.oUTJOBASS_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page1.oPag.oAMCC_EML_1_32.visible=!this.oPgFrm.Page1.oPag.oAMCC_EML_1_32.mHide()
    this.oPgFrm.Page1.oPag.oAMCCNEML_1_33.visible=!this.oPgFrm.Page1.oPag.oAMCCNEML_1_33.mHide()
    this.oPgFrm.Page1.oPag.oAM_FIRMA_1_34.visible=!this.oPgFrm.Page1.oPag.oAM_FIRMA_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_aut
    IF(cEvent='w_TYPEALL Changed')
    this.w_UTMAILFR=this.w_TYPEALL
    ENDIF
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Done")
          .Calculate_MAAXRDUPZR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_OBDBQJGFJS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_YXYVWPWSYP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_MKKRJXZDIC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_XHWHVYGMDW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_VRNSQAYMND()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Update start") or lower(cEvent)==lower("Insert start")
          .Calculate_EJWTWOCXSE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_UTCODICE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_UTCODICE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTCODICE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oUTCODICE_1_1'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_UTCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_UTCODICE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTCODICE = NVL(_Link_.code,0)
      this.w_UTDESUTE = NVL(_Link_.name,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UTCODICE = 0
      endif
      this.w_UTDESUTE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTESCHED
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusrgrp_IDX,3]
    i_lTable = "cpusrgrp"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusrgrp_IDX,2], .t., this.cpusrgrp_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusrgrp_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTESCHED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTESCHED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select groupcode,usercode";
                   +" from "+i_cTable+" "+i_lTable+" where usercode="+cp_ToStrODBC(this.w_UTESCHED);
                   +" and groupcode="+cp_ToStrODBC(this.w_GRPSCHED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'groupcode',this.w_GRPSCHED;
                       ,'usercode',this.w_UTESCHED)
            select groupcode,usercode;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTESCHED = NVL(_Link_.usercode,0)
      this.w_UTESCHED = NVL(_Link_.usercode,0)
    else
      if i_cCtrl<>'Load'
        this.w_UTESCHED = 0
      endif
      this.w_UTESCHED = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusrgrp_IDX,2])+'\'+cp_ToStr(_Link_.groupcode,1)+'\'+cp_ToStr(_Link_.usercode,1)
      cp_ShowWarn(i_cKey,this.cpusrgrp_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTESCHED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value==this.w_UTCODICE)
      this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTCC_EML_1_4.value==this.w_UTCC_EML)
      this.oPgFrm.Page1.oPag.oUTCC_EML_1_4.value=this.w_UTCC_EML
    endif
    if not(this.oPgFrm.Page1.oPag.oUTCCNEML_1_5.value==this.w_UTCCNEML)
      this.oPgFrm.Page1.oPag.oUTCCNEML_1_5.value=this.w_UTCCNEML
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDESUTE_1_6.value==this.w_UTDESUTE)
      this.oPgFrm.Page1.oPag.oUTDESUTE_1_6.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDIALOG1_1_8.RadioValue()==this.w_DIALOG1)
      this.oPgFrm.Page1.oPag.oDIALOG1_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIALOG2_1_9.RadioValue()==this.w_DIALOG2)
      this.oPgFrm.Page1.oPag.oDIALOG2_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTALLEGA_1_10.RadioValue()==this.w_UTALLEGA)
      this.oPgFrm.Page1.oPag.oUTALLEGA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTYPEALL_1_11.RadioValue()==this.w_TYPEALL)
      this.oPgFrm.Page1.oPag.oTYPEALL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTGESALL_1_12.RadioValue()==this.w_UTGESALL)
      this.oPgFrm.Page1.oPag.oUTGESALL_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUT_FIRMA_1_19.value==this.w_UT_FIRMA)
      this.oPgFrm.Page1.oPag.oUT_FIRMA_1_19.value=this.w_UT_FIRMA
    endif
    if not(this.oPgFrm.Page2.oPag.oUTSERVCP_2_1.RadioValue()==this.w_UTSERVCP)
      this.oPgFrm.Page2.oPag.oUTSERVCP_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUTPOSLIT_2_3.RadioValue()==this.w_UTPOSLIT)
      this.oPgFrm.Page2.oPag.oUTPOSLIT_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUTAVVSPL_2_4.RadioValue()==this.w_UTAVVSPL)
      this.oPgFrm.Page2.oPag.oUTAVVSPL_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUTSERVWE_2_5.RadioValue()==this.w_UTSERVWE)
      this.oPgFrm.Page2.oPag.oUTSERVWE_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUTARCHI_2_6.RadioValue()==this.w_UTARCHI)
      this.oPgFrm.Page2.oPag.oUTARCHI_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUTDESUTE_2_7.value==this.w_UTDESUTE)
      this.oPgFrm.Page2.oPag.oUTDESUTE_2_7.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oUTCODICE_2_9.value==this.w_UTCODICE)
      this.oPgFrm.Page2.oPag.oUTCODICE_2_9.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page3.oPag.oUTDIAFAX_3_8.RadioValue()==this.w_UTDIAFAX)
      this.oPgFrm.Page3.oPag.oUTDIAFAX_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oUTFRTFAX_3_9.RadioValue()==this.w_UTFRTFAX)
      this.oPgFrm.Page3.oPag.oUTFRTFAX_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oUTFROFAX_3_10.RadioValue()==this.w_UTFROFAX)
      this.oPgFrm.Page3.oPag.oUTFROFAX_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oUTTIPFAX_3_12.RadioValue()==this.w_UTTIPFAX)
      this.oPgFrm.Page3.oPag.oUTTIPFAX_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oUTDESUTE_3_16.value==this.w_UTDESUTE)
      this.oPgFrm.Page3.oPag.oUTDESUTE_3_16.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page3.oPag.oUTCODICE_3_18.value==this.w_UTCODICE)
      this.oPgFrm.Page3.oPag.oUTCODICE_3_18.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page4.oPag.oUTDESUTE_4_1.value==this.w_UTDESUTE)
      this.oPgFrm.Page4.oPag.oUTDESUTE_4_1.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page4.oPag.oUTCODICE_4_3.value==this.w_UTCODICE)
      this.oPgFrm.Page4.oPag.oUTCODICE_4_3.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page4.oPag.oUTTELEFO_4_4.value==this.w_UTTELEFO)
      this.oPgFrm.Page4.oPag.oUTTELEFO_4_4.value=this.w_UTTELEFO
    endif
    if not(this.oPgFrm.Page4.oPag.oUTTELEF2_4_6.value==this.w_UTTELEF2)
      this.oPgFrm.Page4.oPag.oUTTELEF2_4_6.value=this.w_UTTELEF2
    endif
    if not(this.oPgFrm.Page2.oPag.oUTTIPSKY_2_16.RadioValue()==this.w_UTTIPSKY)
      this.oPgFrm.Page2.oPag.oUTTIPSKY_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUTNUMCEL_4_8.value==this.w_UTNUMCEL)
      this.oPgFrm.Page4.oPag.oUTNUMCEL_4_8.value=this.w_UTNUMCEL
    endif
    if not(this.oPgFrm.Page4.oPag.oUTATT_IN_4_9.RadioValue()==this.w_UTATT_IN)
      this.oPgFrm.Page4.oPag.oUTATT_IN_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUTATTOUT_4_10.RadioValue()==this.w_UTATTOUT)
      this.oPgFrm.Page4.oPag.oUTATTOUT_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUTPHDTIM_4_11.value==this.w_UTPHDTIM)
      this.oPgFrm.Page4.oPag.oUTPHDTIM_4_11.value=this.w_UTPHDTIM
    endif
    if not(this.oPgFrm.Page4.oPag.oUTSELTRA_4_13.RadioValue()==this.w_UTSELTRA)
      this.oPgFrm.Page4.oPag.oUTSELTRA_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUTSELSNM_4_14.RadioValue()==this.w_UTSELSNM)
      this.oPgFrm.Page4.oPag.oUTSELSNM_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUTDIRCHI_4_15.RadioValue()==this.w_UTDIRCHI)
      this.oPgFrm.Page4.oPag.oUTDIRCHI_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oORE_4_22.value==this.w_ORE)
      this.oPgFrm.Page4.oPag.oORE_4_22.value=this.w_ORE
    endif
    if not(this.oPgFrm.Page4.oPag.oMINUTI_4_23.value==this.w_MINUTI)
      this.oPgFrm.Page4.oPag.oMINUTI_4_23.value=this.w_MINUTI
    endif
    if not(this.oPgFrm.Page4.oPag.oSECONDI_4_24.value==this.w_SECONDI)
      this.oPgFrm.Page4.oPag.oSECONDI_4_24.value=this.w_SECONDI
    endif
    if not(this.oPgFrm.Page1.oPag.oUT_OGGET_1_27.value==this.w_UT_OGGET)
      this.oPgFrm.Page1.oPag.oUT_OGGET_1_27.value=this.w_UT_OGGET
    endif
    if not(this.oPgFrm.Page2.oPag.oUTJOBASS_2_23.RadioValue()==this.w_UTJOBASS)
      this.oPgFrm.Page2.oPag.oUTJOBASS_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEMAIL_1_30.value==this.w_UTEMAIL)
      this.oPgFrm.Page1.oPag.oUTEMAIL_1_30.value=this.w_UTEMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCC_EML_1_32.value==this.w_AMCC_EML)
      this.oPgFrm.Page1.oPag.oAMCC_EML_1_32.value=this.w_AMCC_EML
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCCNEML_1_33.value==this.w_AMCCNEML)
      this.oPgFrm.Page1.oPag.oAMCCNEML_1_33.value=this.w_AMCCNEML
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_FIRMA_1_34.value==this.w_AM_FIRMA)
      this.oPgFrm.Page1.oPag.oAM_FIRMA_1_34.value=this.w_AM_FIRMA
    endif
    cp_SetControlsValueExtFlds(this,'UTE_NTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .gsut_mmu.CheckForm()
      if i_bres
        i_bres=  .gsut_mmu.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UTCODICE = this.w_UTCODICE
    this.o_UTDIALOG = this.w_UTDIALOG
    this.o_UTSERVCP = this.w_UTSERVCP
    this.o_UTPOSLIT = this.w_UTPOSLIT
    this.o_UTTELSUP = this.w_UTTELSUP
    this.o_UTEMAIL = this.w_UTEMAIL
    * --- gsut_mmu : Depends On
    this.gsut_mmu.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=cp_IsAdministrator(.t.) OR this.w_UTCODICE=i_CODUTE
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=cp_IsAdministrator(.t.)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=cp_IsAdministrator(.t.)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_autPag1 as StdContainer
  Width  = 632
  height = 486
  stdWidth  = 632
  stdheight = 486
  resizeXpos=343
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTCODICE_1_1 as StdField with uid="WMWUVWMSDT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Utente non definito",;
    ToolTipText = "Codice utente",;
    HelpContextID = 151668363,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=59, Left=120, Top=10, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_UTCODICE"

  func oUTCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTCODICE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTCODICE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oUTCODICE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc


  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="NKTZPQZWKI",left=10, top=95, width=618, height=196, bOnScreen=.t.;
    , bNoBorder=.T.

  add object oUTCC_EML_1_4 as StdField with uid="RXYSGUGFWO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_UTCC_EML", cQueryName = "UTCC_EML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia le mail inviate dal gestionale",;
    HelpContextID = 156350830,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=155, Top=319, InputMask=replicate('X',254)

  func oUTCC_EML_1_4.mHide()
    with this.Parent.oContained
      return (not(.w_AMSHARED="S" and empty(.w_AMCC_EML)))
    endwith
  endfunc

  add object oUTCCNEML_1_5 as StdField with uid="RHGHPEXDPC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_UTCCNEML", cQueryName = "UTCCNEML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia nascosta le mail inviate dal gestionale",;
    HelpContextID = 174176622,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=416, Top=319, InputMask=replicate('X',254)

  func oUTCCNEML_1_5.mHide()
    with this.Parent.oContained
      return (not(.w_AMSHARED="S" and empty(.w_AMCCNEML)))
    endwith
  endfunc

  add object oUTDESUTE_1_6 as StdField with uid="MULOZQLNSJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 99636875,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=180, Top=10, InputMask=replicate('X',40)

  add object oDIALOG1_1_8 as StdRadio with uid="EAGVYWDKAS",rtseq=7,rtrep=.f.,left=155, top=345, width=171,height=35;
    , ToolTipText = "<Automatico> visualizza E-mail solo se necessario - <sempre> visualizza sempre";
    , cFormVar="w_DIALOG1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDIALOG1_1_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Automatico"
      this.Buttons(1).HelpContextID = 129440310
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Sempre"
      this.Buttons(2).HelpContextID = 129440310
      this.Buttons(2).Top=16
      this.SetAll("Width",169)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "<Automatico> visualizza E-mail solo se necessario - <sempre> visualizza sempre")
      StdRadio::init()
    endproc

  func oDIALOG1_1_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oDIALOG1_1_8.GetRadio()
    this.Parent.oContained.w_DIALOG1 = this.RadioValue()
    return .t.
  endfunc

  func oDIALOG1_1_8.SetRadio()
    this.Parent.oContained.w_DIALOG1=trim(this.Parent.oContained.w_DIALOG1)
    this.value = ;
      iif(this.Parent.oContained.w_DIALOG1=='A',1,;
      iif(this.Parent.oContained.w_DIALOG1=='S',2,;
      0))
  endfunc

  func oDIALOG1_1_8.mHide()
    with this.Parent.oContained
      return (g_DOCM='S')
    endwith
  endfunc


  add object oDIALOG2_1_9 as StdCombo with uid="RYUBEHXAAA",rtseq=8,rtrep=.f.,left=155,top=345,width=161,height=22;
    , ToolTipText = "<Automatico> visualizza E-mail solo se necessario - <sempre> visualizza sempre - <Sempre escluso processo> visualizza sempre escluso il processo documentale";
    , HelpContextID = 129440310;
    , cFormVar="w_DIALOG2",RowSource=""+"Automatico,"+"Sempre,"+"Sempre escluso processo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIALOG2_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oDIALOG2_1_9.GetRadio()
    this.Parent.oContained.w_DIALOG2 = this.RadioValue()
    return .t.
  endfunc

  func oDIALOG2_1_9.SetRadio()
    this.Parent.oContained.w_DIALOG2=trim(this.Parent.oContained.w_DIALOG2)
    this.value = ;
      iif(this.Parent.oContained.w_DIALOG2=='A',1,;
      iif(this.Parent.oContained.w_DIALOG2=='S',2,;
      iif(this.Parent.oContained.w_DIALOG2=='P',3,;
      0)))
  endfunc

  func oDIALOG2_1_9.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oUTALLEGA_1_10 as StdRadio with uid="ONBBCJKVTZ",rtseq=9,rtrep=.f.,left=416, top=345, width=171,height=35;
    , ToolTipText = "<Automatico> usa formato allegato - <manuale> richiede formato";
    , cFormVar="w_UTALLEGA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oUTALLEGA_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Automatico"
      this.Buttons(1).HelpContextID = 92743303
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Manuale"
      this.Buttons(2).HelpContextID = 92743303
      this.Buttons(2).Top=16
      this.SetAll("Width",169)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "<Automatico> usa formato allegato - <manuale> richiede formato")
      StdRadio::init()
    endproc

  func oUTALLEGA_1_10.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oUTALLEGA_1_10.GetRadio()
    this.Parent.oContained.w_UTALLEGA = this.RadioValue()
    return .t.
  endfunc

  func oUTALLEGA_1_10.SetRadio()
    this.Parent.oContained.w_UTALLEGA=trim(this.Parent.oContained.w_UTALLEGA)
    this.value = ;
      iif(this.Parent.oContained.w_UTALLEGA=='A',1,;
      iif(this.Parent.oContained.w_UTALLEGA=='M',2,;
      0))
  endfunc

  func oUTALLEGA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTDIALOG='S')
    endwith
   endif
  endfunc


  add object oTYPEALL_1_11 as StdTableCombo with uid="BACORCAHDA",rtseq=10,rtrep=.f.,left=155,top=380,width=161,height=21;
    , ToolTipText = "Permette di selezionare il formato dei file generati che verranno allegati alle E-mail o che verranno archiviati dal document management";
    , HelpContextID = 70182090;
    , cFormVar="w_TYPEALL",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GESPENNA',cKey='GPUNIMIS',cValue='GPUNIMIS',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.



  add object oUTGESALL_1_12 as StdCombo with uid="FIOQCVHPZR",rtseq=11,rtrep=.f.,left=463,top=380,width=161,height=21;
    , ToolTipText = "Operazione da eseguire in presenza di un unico allegato o allegato principale";
    , HelpContextID = 235895150;
    , cFormVar="w_UTGESALL",RowSource=""+"Nessuna operazione,"+"Visualizza,"+"Gestione indice,"+"Invia email", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUTGESALL_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'V',;
    iif(this.value =3,'I',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oUTGESALL_1_12.GetRadio()
    this.Parent.oContained.w_UTGESALL = this.RadioValue()
    return .t.
  endfunc

  func oUTGESALL_1_12.SetRadio()
    this.Parent.oContained.w_UTGESALL=trim(this.Parent.oContained.w_UTGESALL)
    this.value = ;
      iif(this.Parent.oContained.w_UTGESALL=='N',1,;
      iif(this.Parent.oContained.w_UTGESALL=='V',2,;
      iif(this.Parent.oContained.w_UTGESALL=='I',3,;
      iif(this.Parent.oContained.w_UTGESALL=='M',4,;
      0))))
  endfunc

  func oUTGESALL_1_12.mHide()
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
  endfunc

  add object oUT_FIRMA_1_19 as StdMemo with uid="RCWAFHFNMS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_UT_FIRMA", cQueryName = "UT_FIRMA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo da inserire nell'E-mail",;
    HelpContextID = 229439865,;
   bGlobalFont=.t.,;
    Height=79, Width=469, Left=155, Top=406

  func oUT_FIRMA_1_19.mHide()
    with this.Parent.oContained
      return (not(.w_AMSHARED="S" and empty(.w_AM_FIRMA)))
    endwith
  endfunc

  add object oUT_OGGET_1_27 as StdField with uid="IMUHMZVUBU",rtseq=45,rtrep=.f.,;
    cFormVar = "w_UT_OGGET", cQueryName = "UT_OGGET",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto dell'email",;
    HelpContextID = 121374362,;
   bGlobalFont=.t.,;
    Height=21, Width=469, Left=155, Top=293, InputMask=replicate('X',254), cMenuFile="GSAG_APA_Outl3"

  func oUT_OGGET_1_27.mHide()
    with this.Parent.oContained
      return (not .w_ISALT)
    endwith
  endfunc

  add object oUTEMAIL_1_30 as StdField with uid="HBWOJGPSNL",rtseq=50,rtrep=.f.,;
    cFormVar = "w_UTEMAIL", cQueryName = "UTEMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 120035770,;
   bGlobalFont=.t.,;
    Height=21, Width=423, Left=120, Top=37, InputMask=replicate('X',254)

  add object oAMCC_EML_1_32 as StdField with uid="KSRFSEMRBA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_AMCC_EML", cQueryName = "AMCC_EML",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia le mail inviate dal gestionale",;
    HelpContextID = 156352942,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=155, Top=319, InputMask=replicate('X',254)

  func oAMCC_EML_1_32.mHide()
    with this.Parent.oContained
      return (.w_AMSHARED="S" and empty(.w_AMCC_EML))
    endwith
  endfunc

  add object oAMCCNEML_1_33 as StdField with uid="ECCVPSCPEU",rtseq=52,rtrep=.f.,;
    cFormVar = "w_AMCCNEML", cQueryName = "AMCCNEML",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia nascosta le mail inviate dal gestionale",;
    HelpContextID = 174178734,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=416, Top=319, InputMask=replicate('X',254)

  func oAMCCNEML_1_33.mHide()
    with this.Parent.oContained
      return (.w_AMSHARED="S" and empty(.w_AMCCNEML))
    endwith
  endfunc

  add object oAM_FIRMA_1_34 as StdMemo with uid="IHKSPBNTLV",rtseq=53,rtrep=.f.,;
    cFormVar = "w_AM_FIRMA", cQueryName = "AM_FIRMA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo da inserire nell'E-mail",;
    HelpContextID = 229441977,;
   bGlobalFont=.t.,;
    Height=79, Width=469, Left=155, Top=406

  func oAM_FIRMA_1_34.mHide()
    with this.Parent.oContained
      return (.w_AMSHARED="S" and empty(.w_AM_FIRMA))
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="FJXUCKPNZG",Visible=.t., Left=59, Top=10,;
    Alignment=1, Width=56, Height=15,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="HCRDYOLTFJ",Visible=.t., Left=19, Top=69,;
    Alignment=0, Width=251, Height=18,;
    Caption="Impostazioni generali invio e-mail"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="LOOKKLZGUP",Visible=.t., Left=30, Top=382,;
    Alignment=1, Width=121, Height=18,;
    Caption="Formato file generati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QGHVTSXRWJ",Visible=.t., Left=319, Top=347,;
    Alignment=1, Width=92, Height=18,;
    Caption="Allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EGPSTFKBMT",Visible=.t., Left=30, Top=347,;
    Alignment=1, Width=121, Height=18,;
    Caption="Visualizza E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ZSHPXXLWNF",Visible=.t., Left=337, Top=382,;
    Alignment=1, Width=121, Height=18,;
    Caption="Gestione allegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (g_DMIP='S')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="YHKRDHOHUA",Visible=.t., Left=95, Top=321,;
    Alignment=1, Width=56, Height=18,;
    Caption="CC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VKQPELHVKS",Visible=.t., Left=379, Top=321,;
    Alignment=1, Width=32, Height=18,;
    Caption="CCN:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ALMJDEKAVM",Visible=.t., Left=55, Top=406,;
    Alignment=1, Width=96, Height=18,;
    Caption="Firma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="UJMUDIHPTN",Visible=.t., Left=71, Top=295,;
    Alignment=1, Width=80, Height=18,;
    Caption="Oggetto email:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (not .w_ISALT)
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="SGPMKKISQT",Visible=.t., Left=83, Top=40,;
    Alignment=0, Width=35, Height=18,;
    Caption="Email:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="KVELNRVZNA",left=10, top=89, width=618,height=1
enddefine
define class tgsut_autPag2 as StdContainer
  Width  = 632
  height = 486
  stdWidth  = 632
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTSERVCP_2_1 as StdCheck with uid="IREATSAIXK",rtseq=13,rtrep=.f.,left=351, top=89, caption="Abilita servizi Web application",;
    ToolTipText = "Se attivo: abilita servizi Web application per l'utente",;
    HelpContextID = 115426966,;
    cFormVar="w_UTSERVCP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTSERVCP_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTSERVCP_2_1.GetRadio()
    this.Parent.oContained.w_UTSERVCP = this.RadioValue()
    return .t.
  endfunc

  func oUTSERVCP_2_1.SetRadio()
    this.Parent.oContained.w_UTSERVCP=trim(this.Parent.oContained.w_UTSERVCP)
    this.value = ;
      iif(this.Parent.oContained.w_UTSERVCP=='S',1,;
      0)
  endfunc

  func oUTSERVCP_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI<>'S' Or cp_IsAdministrator())
    endwith
   endif
  endfunc


  add object oBtn_2_2 as StdButton with uid="XHLNXVUAZP",left=351, top=118, width=48,height=45,;
    CpPicture="BMP\LOG_PWD.BMP", caption="", nPag=2;
    , ToolTipText = "Autenticazione applicazione web";
    , HelpContextID = 224234311;
    , caption='\<Log/Pwd';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      do GSUT_KLW with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_UTSERVCP='S')
      endwith
    endif
  endfunc

  func oBtn_2_2.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DMIP<>'S' Or g_REVI='S')
     endwith
    endif
  endfunc

  add object oUTPOSLIT_2_3 as StdCheck with uid="QFUBUTJFJA",rtseq=14,rtrep=.f.,left=351, top=174, caption="Abilita servizi PostaLite",;
    ToolTipText = "Se attivo: abilita servizi PostaLite per l'utente",;
    HelpContextID = 50653542,;
    cFormVar="w_UTPOSLIT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTPOSLIT_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTPOSLIT_2_3.GetRadio()
    this.Parent.oContained.w_UTPOSLIT = this.RadioValue()
    return .t.
  endfunc

  func oUTPOSLIT_2_3.SetRadio()
    this.Parent.oContained.w_UTPOSLIT=trim(this.Parent.oContained.w_UTPOSLIT)
    this.value = ;
      iif(this.Parent.oContained.w_UTPOSLIT=='S',1,;
      0)
  endfunc

  add object oUTAVVSPL_2_4 as StdCheck with uid="LWMPOQUZKC",rtseq=15,rtrep=.f.,left=351, top=199, caption="Avvio automatico PostaLite",;
    ToolTipText = "Se attivo: avvia automaticamente PostaLite",;
    HelpContextID = 70330002,;
    cFormVar="w_UTAVVSPL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTAVVSPL_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTAVVSPL_2_4.GetRadio()
    this.Parent.oContained.w_UTAVVSPL = this.RadioValue()
    return .t.
  endfunc

  func oUTAVVSPL_2_4.SetRadio()
    this.Parent.oContained.w_UTAVVSPL=trim(this.Parent.oContained.w_UTAVVSPL)
    this.value = ;
      iif(this.Parent.oContained.w_UTAVVSPL=='S',1,;
      0)
  endfunc

  func oUTAVVSPL_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTPOSLIT='S')
    endwith
   endif
  endfunc

  add object oUTSERVWE_2_5 as StdCheck with uid="AGZIICCTHX",rtseq=16,rtrep=.f.,left=351, top=224, caption="Abilita servizi WE",;
    ToolTipText = "Se attivo: abilita servizi WE per l'utente",;
    HelpContextID = 153008501,;
    cFormVar="w_UTSERVWE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTSERVWE_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTSERVWE_2_5.GetRadio()
    this.Parent.oContained.w_UTSERVWE = this.RadioValue()
    return .t.
  endfunc

  func oUTSERVWE_2_5.SetRadio()
    this.Parent.oContained.w_UTSERVWE=trim(this.Parent.oContained.w_UTSERVWE)
    this.value = ;
      iif(this.Parent.oContained.w_UTSERVWE=='S',1,;
      0)
  endfunc

  func oUTSERVWE_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_WEENABLED='S')
    endwith
   endif
  endfunc

  add object oUTARCHI_2_6 as StdCheck with uid="PUYRANRPNH",rtseq=17,rtrep=.f.,left=351, top=249, caption="Attivazione Archeasy",;
    ToolTipText = "Se attivo: abilita, per l'utente, l'archiviazione con Archeasy",;
    HelpContextID = 134404538,;
    cFormVar="w_UTARCHI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTARCHI_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTARCHI_2_6.GetRadio()
    this.Parent.oContained.w_UTARCHI = this.RadioValue()
    return .t.
  endfunc

  func oUTARCHI_2_6.SetRadio()
    this.Parent.oContained.w_UTARCHI=trim(this.Parent.oContained.w_UTARCHI)
    this.value = ;
      iif(this.Parent.oContained.w_UTARCHI=='S',1,;
      0)
  endfunc

  func oUTARCHI_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (VARTYPE(g_ARCE)='U' OR g_ARCE='S')
    endwith
   endif
  endfunc

  add object oUTDESUTE_2_7 as StdField with uid="ZDQPDVZTYB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 99636875,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=175, Top=10, InputMask=replicate('X',40)

  add object oUTCODICE_2_9 as StdField with uid="KBZOYHGDUZ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151668363,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=59, Left=115, Top=10, cSayPict='"9999"', cGetPict='"9999"'


  add object oUTTIPSKY_2_16 as StdCombo with uid="INHRBTYROJ",rtseq=30,rtrep=.f.,left=351,top=283,width=230,height=21;
    , ToolTipText = "Determina il tipo di utilizzo si Skype (Nessun utilizzo, uso di base oppure avanzato con utilizzo del credito Skype)";
    , HelpContextID = 205171041;
    , cFormVar="w_UTTIPSKY",RowSource=""+"No,"+"Base,"+"Completo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oUTTIPSKY_2_16.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oUTTIPSKY_2_16.GetRadio()
    this.Parent.oContained.w_UTTIPSKY = this.RadioValue()
    return .t.
  endfunc

  func oUTTIPSKY_2_16.SetRadio()
    this.Parent.oContained.w_UTTIPSKY=trim(this.Parent.oContained.w_UTTIPSKY)
    this.value = ;
      iif(this.Parent.oContained.w_UTTIPSKY=='N',1,;
      iif(this.Parent.oContained.w_UTTIPSKY=='S',2,;
      iif(this.Parent.oContained.w_UTTIPSKY=='C',3,;
      0)))
  endfunc

  add object oUTJOBASS_2_23 as StdCheck with uid="MYFVZVXPAD",rtseq=49,rtrep=.f.,left=351, top=44, caption="Esegue solo job assegnati",;
    ToolTipText = "Se attivo: esegue esclusivamente i job assegnati",;
    HelpContextID = 15382169,;
    cFormVar="w_UTJOBASS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUTJOBASS_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTJOBASS_2_23.GetRadio()
    this.Parent.oContained.w_UTJOBASS = this.RadioValue()
    return .t.
  endfunc

  func oUTJOBASS_2_23.SetRadio()
    this.Parent.oContained.w_UTJOBASS=trim(this.Parent.oContained.w_UTJOBASS)
    this.value = ;
      iif(this.Parent.oContained.w_UTJOBASS=='S',1,;
      0)
  endfunc

  func oUTJOBASS_2_23.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S' or .w_UTCODICE<>NVL(.w_UTESCHED,0))
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="QJLBZPFKIA",Visible=.t., Left=54, Top=10,;
    Alignment=1, Width=56, Height=15,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="RFIRAVOEGH",Visible=.t., Left=36, Top=65,;
    Alignment=0, Width=187, Height=18,;
    Caption="Servizi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="IRMNYLLXTN",Visible=.t., Left=73, Top=226,;
    Alignment=1, Width=272, Height=18,;
    Caption="Servizi WE (invio documenti a net-folder):"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="PPAUJLACCX",Visible=.t., Left=27, Top=177,;
    Alignment=1, Width=318, Height=18,;
    Caption="Servizi PostaLite (invio documenti via Postel):"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="TRHZZWUHCA",Visible=.t., Left=73, Top=91,;
    Alignment=1, Width=272, Height=18,;
    Caption="Servizi Web application:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="RTWYFSSNFB",Visible=.t., Left=67, Top=251,;
    Alignment=1, Width=278, Height=18,;
    Caption="Servizi Archeasy (archiviazione documentale):"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="WDVHNTSCRK",Visible=.t., Left=139, Top=283,;
    Alignment=1, Width=206, Height=18,;
    Caption="Servizio Skype:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="BSLWZPPKKM",Visible=.t., Left=151, Top=46,;
    Alignment=1, Width=194, Height=18,;
    Caption="Utente schedulatore:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S' or .w_UTCODICE<>NVL(.w_UTESCHED,0))
    endwith
  endfunc

  add object oBox_2_13 as StdBox with uid="NOORMXYBYE",left=25, top=83, width=554,height=1
enddefine
define class tgsut_autPag3 as StdContainer
  Width  = 632
  height = 486
  stdWidth  = 632
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTDIAFAX_3_8 as StdRadio with uid="JQSGYRTEUG",rtseq=20,rtrep=.f.,left=374, top=272, width=201,height=32;
    , cFormVar="w_UTDIAFAX", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oUTDIAFAX_3_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Automatico"
      this.Buttons(1).HelpContextID = 97801886
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Sempre"
      this.Buttons(2).HelpContextID = 97801886
      this.Buttons(2).Top=15
      this.SetAll("Width",199)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oUTDIAFAX_3_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oUTDIAFAX_3_8.GetRadio()
    this.Parent.oContained.w_UTDIAFAX = this.RadioValue()
    return .t.
  endfunc

  func oUTDIAFAX_3_8.SetRadio()
    this.Parent.oContained.w_UTDIAFAX=trim(this.Parent.oContained.w_UTDIAFAX)
    this.value = ;
      iif(this.Parent.oContained.w_UTDIAFAX=='A',1,;
      iif(this.Parent.oContained.w_UTDIAFAX=='S',2,;
      0))
  endfunc

  func oUTDIAFAX_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTTIPFAX='M')
    endwith
   endif
  endfunc


  add object oUTFRTFAX_3_9 as StdCombo with uid="CGDEAPSSPH",rtseq=21,rtrep=.f.,left=374,top=313,width=164,height=21;
    , ToolTipText = "Formato allegato FAX";
    , HelpContextID = 118322846;
    , cFormVar="w_UTFRTFAX",RowSource=""+"PDF,"+"HTM,"+"RTF", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oUTFRTFAX_3_9.RadioValue()
    return(iif(this.value =1,'PDF',;
    iif(this.value =2,'HTM',;
    iif(this.value =3,'RTF',;
    space(3)))))
  endfunc
  func oUTFRTFAX_3_9.GetRadio()
    this.Parent.oContained.w_UTFRTFAX = this.RadioValue()
    return .t.
  endfunc

  func oUTFRTFAX_3_9.SetRadio()
    this.Parent.oContained.w_UTFRTFAX=trim(this.Parent.oContained.w_UTFRTFAX)
    this.value = ;
      iif(this.Parent.oContained.w_UTFRTFAX=='PDF',1,;
      iif(this.Parent.oContained.w_UTFRTFAX=='HTM',2,;
      iif(this.Parent.oContained.w_UTFRTFAX=='RTF',3,;
      0)))
  endfunc

  func oUTFRTFAX_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTTIPFAX='M')
    endwith
   endif
  endfunc

  add object oUTFROFAX_3_10 as StdCheck with uid="QNOYIUPIFL",rtseq=22,rtrep=.f.,left=374, top=343, caption="Frontespizio",;
    ToolTipText = "Abilitazione frontespizio su FAX",;
    HelpContextID = 113079966,;
    cFormVar="w_UTFROFAX", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oUTFROFAX_3_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oUTFROFAX_3_10.GetRadio()
    this.Parent.oContained.w_UTFROFAX = this.RadioValue()
    return .t.
  endfunc

  func oUTFROFAX_3_10.SetRadio()
    this.Parent.oContained.w_UTFROFAX=trim(this.Parent.oContained.w_UTFROFAX)
    this.value = ;
      iif(this.Parent.oContained.w_UTFROFAX=='S',1,;
      0)
  endfunc


  add object oUTTIPFAX_3_12 as StdCombo with uid="DJWJKWBGKG",rtseq=23,rtrep=.f.,left=206,top=188,width=209,height=21;
    , ToolTipText = "Seleziona il sistema di gestione FAX da utilizzare";
    , HelpContextID = 113596062;
    , cFormVar="w_UTTIPFAX",RowSource=""+"Supporto MAPI,"+"Supporto stampa,"+"Rendering Subsystem,"+"MailToFax,"+"Non gestito", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oUTTIPFAX_3_12.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    iif(this.value =4,'E',;
    iif(this.value =5,'N',;
    space(1)))))))
  endfunc
  func oUTTIPFAX_3_12.GetRadio()
    this.Parent.oContained.w_UTTIPFAX = this.RadioValue()
    return .t.
  endfunc

  func oUTTIPFAX_3_12.SetRadio()
    this.Parent.oContained.w_UTTIPFAX=trim(this.Parent.oContained.w_UTTIPFAX)
    this.value = ;
      iif(this.Parent.oContained.w_UTTIPFAX=='M',1,;
      iif(this.Parent.oContained.w_UTTIPFAX=='S',2,;
      iif(this.Parent.oContained.w_UTTIPFAX=='R',3,;
      iif(this.Parent.oContained.w_UTTIPFAX=='E',4,;
      iif(this.Parent.oContained.w_UTTIPFAX=='N',5,;
      0)))))
  endfunc

  add object oUTDESUTE_3_16 as StdField with uid="UPIDRKNWOI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 99636875,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=172, Top=8, InputMask=replicate('X',40)

  add object oUTCODICE_3_18 as StdField with uid="ASAEKBSGJE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151668363,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=59, Left=112, Top=8, cSayPict='"9999"', cGetPict='"9999"'

  add object oStr_3_2 as StdString with uid="JNORJGZSHQ",Visible=.t., Left=10, Top=60,;
    Alignment=0, Width=462, Height=15,;
    Caption="Configurazione sistema FAX"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_3 as StdString with uid="NVMZXALTOU",Visible=.t., Left=19, Top=92,;
    Alignment=0, Width=472, Height=15,;
    Caption="Selezionare il supporto all'invio FAX che si desidera utilizzare."  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="ATZDYSFMCU",Visible=.t., Left=19, Top=115,;
    Alignment=0, Width=472, Height=15,;
    Caption="Il supporto MAPI utilizza l'oggetto MSMAPI32 per l'invio del FAX."  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="EZOSENQKCW",Visible=.t., Left=19, Top=138,;
    Alignment=0, Width=472, Height=15,;
    Caption="Il supporto stampa utilizza un servizio di FAX come 'Microsoft FAX' o 'FAX'."  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="EIWGDPTCYY",Visible=.t., Left=19, Top=241,;
    Alignment=0, Width=560, Height=16,;
    Caption="Se si utilizza il supporto MAPI specificare il formato del file da allegare."  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="ATOBUOVFNE",Visible=.t., Left=87, Top=269,;
    Alignment=1, Width=281, Height=18,;
    Caption="Se supporto MAPI visualizza finestra fax::"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="ECEXQXJFPH",Visible=.t., Left=193, Top=343,;
    Alignment=1, Width=175, Height=18,;
    Caption="Utilizza frontespizio per FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="ZHYSRXTNFU",Visible=.t., Left=19, Top=161,;
    Alignment=0, Width=472, Height=18,;
    Caption="Il supporto mail to fax utilizza un servizio di FAX come 'MailToFax'."  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="XKUXJRKXAU",Visible=.t., Left=16, Top=315,;
    Alignment=1, Width=352, Height=18,;
    Caption="Se supporto MAPI selezionare formato dell'allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="CSLVMUPOJC",Visible=.t., Left=41, Top=188,;
    Alignment=1, Width=158, Height=18,;
    Caption="Supporto per invio del FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="PSECVGPIWI",Visible=.t., Left=51, Top=8,;
    Alignment=1, Width=56, Height=15,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_1 as StdBox with uid="VKUJRKSXTN",left=10, top=78, width=549,height=1
enddefine
define class tgsut_autPag4 as StdContainer
  Width  = 632
  height = 486
  stdWidth  = 632
  stdheight = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTDESUTE_4_1 as StdField with uid="SKUOCMAGUQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 99636875,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=202, Top=12, InputMask=replicate('X',40)

  add object oUTCODICE_4_3 as StdField with uid="AKGASWYFDW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151668363,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=59, Left=142, Top=12, cSayPict='"9999"', cGetPict='"9999"'

  add object oUTTELEFO_4_4 as StdField with uid="VCHVHYOQMD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_UTTELEFO", cQueryName = "UTTELEFO",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono",;
    HelpContextID = 92362389,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=142, Top=42, InputMask=replicate('X',18)

  add object oUTTELEF2_4_6 as StdField with uid="IHIBGJETBF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_UTTELEF2", cQueryName = "UTTELEF2",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Altro telefono",;
    HelpContextID = 92362360,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=142, Top=70, InputMask=replicate('X',18)

  add object oUTNUMCEL_4_8 as StdField with uid="CHSFHXWOVQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_UTNUMCEL", cQueryName = "UTNUMCEL",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di cellulare",;
    HelpContextID = 60880530,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=142, Top=98, InputMask=replicate('X',18)


  add object oUTATT_IN_4_9 as StdTableCombo with uid="AHGEXGCMFI",rtseq=34,rtrep=.f.,left=142,top=126,width=234,height=21;
    , ToolTipText = "Tipo attivit� telefonate in ingresso";
    , HelpContextID = 267442540;
    , cFormVar="w_UTATT_IN",tablefilter="CA_TIMER='S' AND CARAGGST='T'", bObbl = .f. , nPag = 4;
    , cTable='QUERY\GSAG2KCA.VQR',cKey='CACODICE',cValue='CADESCRI',cOrderBy='CADESCRI',xDefault=space(20);
  , bGlobalFont=.t.


  func oUTATT_IN_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_AGFA='S')
    endwith
   endif
  endfunc


  add object oUTATTOUT_4_10 as StdTableCombo with uid="PJZLLVKMJZ",rtseq=35,rtrep=.f.,left=142,top=154,width=234,height=21;
    , ToolTipText = "Tipo attivit� telefonate in uscita";
    , HelpContextID = 992922;
    , cFormVar="w_UTATTOUT",tablefilter="CA_TIMER='S' AND CARAGGST='T'", bObbl = .f. , nPag = 4;
    , cTable='QUERY\GSAG2KCA.VQR',cKey='CACODICE',cValue='CADESCRI',cOrderBy='CADESCRI',xDefault=space(20);
  , bGlobalFont=.t.


  func oUTATTOUT_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_AGFA='S')
    endwith
   endif
  endfunc

  add object oUTPHDTIM_4_11 as StdField with uid="CPNBEPYWEX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_UTPHDTIM", cQueryName = "UTPHDTIM",;
    bObbl = .f. , nPag = 4, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Fine data importazione",;
    HelpContextID = 201058669,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=142, Top=224

  func oUTPHDTIM_4_11.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oUTSELTRA_4_13 as StdRadio with uid="ADDUBJRACG",rtseq=37,rtrep=.f.,left=142, top=266, width=134,height=37;
    , ToolTipText = "Includi trasferite";
    , cFormVar="w_UTSELTRA", ButtonCount=2, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oUTSELTRA_4_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Includi trasferite"
      this.Buttons(1).HelpContextID = 75581063
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Escludi trasferite"
      this.Buttons(2).HelpContextID = 75581063
      this.Buttons(2).Top=17
      this.SetAll("Width",132)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Includi trasferite")
      StdRadio::init()
    endproc

  func oUTSELTRA_4_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oUTSELTRA_4_13.GetRadio()
    this.Parent.oContained.w_UTSELTRA = this.RadioValue()
    return .t.
  endfunc

  func oUTSELTRA_4_13.SetRadio()
    this.Parent.oContained.w_UTSELTRA=trim(this.Parent.oContained.w_UTSELTRA)
    this.value = ;
      iif(this.Parent.oContained.w_UTSELTRA=='S',1,;
      iif(this.Parent.oContained.w_UTSELTRA=='N',2,;
      0))
  endfunc

  func oUTSELTRA_4_13.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oUTSELSNM_4_14 as StdRadio with uid="YEVETINOBE",rtseq=38,rtrep=.f.,left=142, top=320, width=166,height=40;
    , ToolTipText = "Includi senza numero";
    , cFormVar="w_UTSELSNM", ButtonCount=2, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oUTSELSNM_4_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Includi senza numero"
      this.Buttons(1).HelpContextID = 209631597
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Escludi senza numero"
      this.Buttons(2).HelpContextID = 209631597
      this.Buttons(2).Top=19
      this.SetAll("Width",164)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Includi senza numero")
      StdRadio::init()
    endproc

  func oUTSELSNM_4_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oUTSELSNM_4_14.GetRadio()
    this.Parent.oContained.w_UTSELSNM = this.RadioValue()
    return .t.
  endfunc

  func oUTSELSNM_4_14.SetRadio()
    this.Parent.oContained.w_UTSELSNM=trim(this.Parent.oContained.w_UTSELSNM)
    this.value = ;
      iif(this.Parent.oContained.w_UTSELSNM=='S',1,;
      iif(this.Parent.oContained.w_UTSELSNM=='N',2,;
      0))
  endfunc

  func oUTSELSNM_4_14.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oUTDIRCHI_4_15 as StdRadio with uid="PNPSLFEGBV",rtseq=39,rtrep=.f.,left=357, top=266, width=195,height=54;
    , ToolTipText = "Direzione chiamate (ingresso, uscita o tutte)";
    , cFormVar="w_UTDIRCHI", ButtonCount=3, bObbl=.f., nPag=4;
  , bGlobalFont=.t.

    proc oUTDIRCHI_4_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo chiamate in ingresso"
      this.Buttons(1).HelpContextID = 203139441
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo chiamate in uscita"
      this.Buttons(2).HelpContextID = 203139441
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 203139441
      this.Buttons(3).Top=34
      this.SetAll("Width",193)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Direzione chiamate (ingresso, uscita o tutte)")
      StdRadio::init()
    endproc

  func oUTDIRCHI_4_15.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oUTDIRCHI_4_15.GetRadio()
    this.Parent.oContained.w_UTDIRCHI = this.RadioValue()
    return .t.
  endfunc

  func oUTDIRCHI_4_15.SetRadio()
    this.Parent.oContained.w_UTDIRCHI=trim(this.Parent.oContained.w_UTDIRCHI)
    this.value = ;
      iif(this.Parent.oContained.w_UTDIRCHI=='I',1,;
      iif(this.Parent.oContained.w_UTDIRCHI=='O',2,;
      iif(this.Parent.oContained.w_UTDIRCHI=='T',3,;
      0)))
  endfunc

  func oUTDIRCHI_4_15.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oORE_4_22 as StdField with uid="VTRFKZJMDU",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ORE", cQueryName = "ORE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore",;
    HelpContextID = 75799578,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=357, Top=395, cSayPict='"99"', cGetPict='"99"'

  func oORE_4_22.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oMINUTI_4_23 as StdField with uid="XHSVXIKGHQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MINUTI", cQueryName = "MINUTI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minuti",;
    HelpContextID = 168880838,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=407, Top=395, cSayPict='"99"', cGetPict='"99"'

  func oMINUTI_4_23.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oSECONDI_4_24 as StdField with uid="LHHWFGSOUU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SECONDI", cQueryName = "SECONDI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero secondi",;
    HelpContextID = 190171354,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=464, Top=395, cSayPict='"99"', cGetPict='"99"'

  func oSECONDI_4_24.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_2 as StdString with uid="ALKPFOLIJM",Visible=.t., Left=83, Top=12,;
    Alignment=1, Width=56, Height=15,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_5 as StdString with uid="JMIUBMIJGZ",Visible=.t., Left=59, Top=42,;
    Alignment=1, Width=80, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="JJJYHOOGRG",Visible=.t., Left=47, Top=70,;
    Alignment=1, Width=92, Height=18,;
    Caption="Altro telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="HAJWPTAZQT",Visible=.t., Left=59, Top=98,;
    Alignment=1, Width=80, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="JVCEBAQSSJ",Visible=.t., Left=18, Top=196,;
    Alignment=0, Width=462, Height=18,;
    Caption="Parametri importazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_18 as StdString with uid="GZTPCYGXPN",Visible=.t., Left=87, Top=395,;
    Alignment=1, Width=259, Height=18,;
    Caption="Considera telefonate con durata superiore a:"  ;
  , bGlobalFont=.t.

  func oStr_4_18.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_19 as StdString with uid="QOJSXUIZTZ",Visible=.t., Left=357, Top=373,;
    Alignment=2, Width=34, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  func oStr_4_19.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_20 as StdString with uid="VUEIBKOVNP",Visible=.t., Left=407, Top=373,;
    Alignment=2, Width=34, Height=18,;
    Caption="Minuti"  ;
  , bGlobalFont=.t.

  func oStr_4_20.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_21 as StdString with uid="IUXXTWSWOU",Visible=.t., Left=454, Top=373,;
    Alignment=2, Width=54, Height=18,;
    Caption="Secondi"  ;
  , bGlobalFont=.t.

  func oStr_4_21.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_25 as StdString with uid="NDLKSIYHDN",Visible=.t., Left=13, Top=224,;
    Alignment=1, Width=126, Height=18,;
    Caption="Data fine importazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_25.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_4_26 as StdString with uid="OSMXBPQPBX",Visible=.t., Left=10, Top=126,;
    Alignment=1, Width=129, Height=18,;
    Caption="Tipo attivit� ingresso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="OTXPYYUIMN",Visible=.t., Left=39, Top=154,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo attivit� uscita:"  ;
  , bGlobalFont=.t.

  add object oBox_4_28 as StdBox with uid="FYLSTKYOGP",left=4, top=213, width=592,height=1

  add object oBox_4_29 as StdBox with uid="AMYNIYGZRA",left=4, top=369, width=592,height=61
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aut','UTE_NTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UTCODICE=UTE_NTI.UTCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
