* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bca                                                        *
*              Ricerca articoli alternativi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_294]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-18                                                      *
* Last revis.: 2013-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bca",oParentObject)
return(i_retval)

define class tgsve_bca as StdBatch
  * --- Local variables
  w_CODALT = space(20)
  w_DESALT = space(40)
  w_MESS = space(20)
  w_PADRE = .NULL.
  w_OBJCTRL = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CONTA = 0
  w_DATOBSO = ctod("  /  /  ")
  w_CONTA = 0
  w_DATOBSO = ctod("  /  /  ")
  * --- WorkFile variables
  ART_ALTE_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca Articoli Alternativi (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    * --- indica se i campi mvcodice, mvcodmag, mvcodmat, mvqtamov,mvqtaum1 sono stati variati
    * --- indica se mvcodice in particolare � stato variato(al posto di o_mvcodice)
    * --- w_GIACON vale
    *     'X' Tutto ok
    *     ' ' Devo ancora effettura i controlli
    *     'N' Controllo fallito, riga non valida
    *     'Y' Aperta maschera articoli alternativi e selezionato uno
    *     'O' Risposta Si nel caso controllo opzionale
    *     In quest'ultimo caso non ripeto i controlli.
    * --- La var. w_GSVEBCA segnala l'effettivo cambiamento dei campi che lanciano l'evento.
    this.w_PADRE = This.oParentObject
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    if not empty(this.oParentObject.w_MVCODART) And this.oParentObject.w_GIACON<>"Y" And this.oParentObject.w_GSVEBCA And this.w_PADRE.RowStatus()="A"
      * --- Se cambio il codice devo resettare il falg sullo stato del check
      *     disponibilit�, sempre tranne nel caso l'utente selezioni un articolo
      *     dagli articoli alternativi, in questo caso � implicito che l'utente accetti.
      if this.oParentObject.w_NEWART
        this.oParentObject.w_GIACON = " "
      endif
      * --- Se documento che evade effettivamente non scatta meccanismo,
      *     se il doc. � provvisorio o nei dati azienda � disabilitato il check controllo disponibilit�, non esegue il controllo
      * --- w_NUMGES -> se 1 lanciato il caricamento rapido: non deve fare il controllo di disponibilit�
      do case
        case Empty( this.oParentObject.w_MVFLARIF ) And this.oParentObject.w_MVFLPROV<>"S" AND g_PERDIS="S" And this.oParentObject.w_GIACON<>"O" And this.oParentObject.w_GIACON<>"Y" AND this.oParentObject.w_FLDISP$ "SC" AND (((( this.oParentObject.w_MVFLCASC="-"AND this.oParentObject.w_FLRRIF <>"+") OR this.oParentObject.w_MVFLRISE="+") AND ((this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER)-this.oParentObject.w_MVQTAUM1)<0) OR ; 
 ((this.oParentObject.w_MVF2CASC="-" OR this.oParentObject.w_MVF2RISE="+") AND (( this.oParentObject.w_Q2APER-this.oParentObject.w_Q2RPER )-this.oParentObject.w_MVQTAUM1)<0 AND NOT EMPTY(this.oParentObject.w_MVCODMAT))) AND this.oParentObject.w_MVQTAMOV<>0 And this.oParentObject.w_NUMGES = 0
          this.w_oPART = this.w_oMESS.addmsgpartNL("Articolo: %1%0Magazzino: %2%0Disponibilit� articolo negativa! (%3)")
          this.w_oPART.addParam(ALLTR(this.oParentObject.w_MVCODICE))     
          this.w_oPART.addParam(IIF(((this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER)-this.oParentObject.w_MVQTAUM1)<0 and (( this.oParentObject.w_MVFLCASC="-"AND this.oParentObject.w_FLRRIF <>"+") OR this.oParentObject.w_MVFLRISE="+") , this.oParentObject.w_MVCODMAG, this.oParentObject.w_MVCODMAT))     
          this.w_oPART.addParam(ALLTRIM(STR(IIF(((this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER)-this.oParentObject.w_MVQTAUM1)<0 and (( this.oParentObject.w_MVFLCASC="-"AND this.oParentObject.w_FLRRIF <>"+") OR this.oParentObject.w_MVFLRISE="+"), (this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER)-this.oParentObject.w_MVQTAUM1, ( this.oParentObject.w_Q2APER-this.oParentObject.w_Q2RPER )-this.oParentObject.w_MVQTAUM1),12,3)))     
          this.w_CODALT = SPACE(20)
          * --- Read from ART_ALTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ALTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ALTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODALT,ARDESALT"+;
              " from "+i_cTable+" ART_ALTE where ";
                  +"ARCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODALT,ARDESALT;
              from (i_cTable) where;
                  ARCODICE = this.oParentObject.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODALT = NVL(cp_ToDate(_read_.ARCODALT),cp_NullValue(_read_.ARCODALT))
            this.w_DESALT = NVL(cp_ToDate(_read_.ARDESALT),cp_NullValue(_read_.ARDESALT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CONTA = I_rows
          if this.w_conta=1
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARDTOBSO"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODALT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARDTOBSO;
                from (i_cTable) where;
                    ARCODART = this.w_CODALT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DATOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CONTA = IIF ( Empty( this.w_DATOBSO ) Or this.oParentObject.w_MVDATREG<=this.w_DATOBSO, this.w_CONTA , 0 )
          endif
          * --- testo obsolescenza
          do case
            case this.oParentObject.w_FLDISP="C"
              this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
              if this.w_oMESS.ah_YesNo()
                * --- Notifica Avvenuta Conferma
                this.oParentObject.w_GIACON = "O"
              else
                this.oParentObject.w_GIACON = "N"
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            case this.oParentObject.w_FLDISP="S"
              this.oParentObject.w_GIACON = "N"
              if i_Rows>=1
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.w_oMESS.Ah_ErrorMsg()     
              endif
          endcase
          * --- assegnamento  a .F. nel batch invece che nella before input di mvcodice
          this.oParentObject.w_GSVEBCA = .F.
          this.oParentObject.w_NEWART = .F.
        case Empty( this.oParentObject.w_MVFLARIF ) And this.oParentObject.w_MVFLPROV<>"S" AND g_PERDIS="S" And this.oParentObject.w_GIACON<>"O" And this.oParentObject.w_GIACON<>"Y" AND this.oParentObject.w_FLDISP$ "SC" AND ((( this.oParentObject.w_FLRRIF ="+" OR this.oParentObject.w_MVFLRISE="-") AND (this.oParentObject.w_QTRPER-this.oParentObject.w_MVQTAUM1)<0) OR ; 
 (this.oParentObject.w_MVF2RISE="-" AND (this.oParentObject.w_Q2RPER-this.oParentObject.w_MVQTAUM1)<0 AND NOT EMPTY(this.oParentObject.w_MVCODMAT))) AND this.oParentObject.w_MVQTAMOV<>0 And this.oParentObject.w_NUMGES = 0
          this.w_oPART = this.w_oMESS.addmsgpartNL("Articolo: %1%0Magazzino: %2%0Quantit� riservata articolo negativa! (%3)")
          this.w_oPART.addParam(ALLTR(this.oParentObject.w_MVCODICE))     
          this.w_oPART.addParam(IIF((this.oParentObject.w_QTRPER-this.oParentObject.w_MVQTAUM1)<0 , this.oParentObject.w_MVCODMAG, this.oParentObject.w_MVCODMAT))     
          this.w_oPART.addParam(ALLTRIM(STR(IIF((this.oParentObject.w_QTRPER-this.oParentObject.w_MVQTAUM1)<0, this.oParentObject.w_QTRPER-this.oParentObject.w_MVQTAUM1, this.oParentObject.w_Q2RPER-this.oParentObject.w_MVQTAUM1),12,3)))     
          this.w_CODALT = SPACE(20)
          * --- Read from ART_ALTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ALTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ALTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODALT,ARDESALT"+;
              " from "+i_cTable+" ART_ALTE where ";
                  +"ARCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODALT,ARDESALT;
              from (i_cTable) where;
                  ARCODICE = this.oParentObject.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODALT = NVL(cp_ToDate(_read_.ARCODALT),cp_NullValue(_read_.ARCODALT))
            this.w_DESALT = NVL(cp_ToDate(_read_.ARDESALT),cp_NullValue(_read_.ARDESALT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CONTA = I_rows
          if this.w_conta=1
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARDTOBSO"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODALT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARDTOBSO;
                from (i_cTable) where;
                    ARCODART = this.w_CODALT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DATOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CONTA = IIF ( Empty( this.w_DATOBSO ) Or this.oParentObject.w_MVDATREG<=this.w_DATOBSO, this.w_CONTA , 0 )
          endif
          * --- testo obsolescenza
          do case
            case this.oParentObject.w_FLDISP="C"
              this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
              if this.w_oMESS.ah_YesNo()
                * --- Notifica Avvenuta Conferma
                this.oParentObject.w_GIACON = "O"
              else
                this.oParentObject.w_GIACON = "N"
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            case this.oParentObject.w_FLDISP="S"
              this.oParentObject.w_GIACON = "N"
              if i_Rows>=1
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.w_oMESS.Ah_ErrorMsg()     
              endif
          endcase
          * --- assegnamento  a .F. nel batch invece che nella before input di mvcodice
          this.oParentObject.w_GSVEBCA = .F.
          this.oParentObject.w_NEWART = .F.
        otherwise
          * --- Se quantit� uguale a 0 articolo ancora da valutare, altrimenti tutto ok.
          if this.oParentObject.w_MVQTAMOV<>0 AND this.oParentObject.w_GIACON<>"O"
            this.oParentObject.w_GIACON = "X"
          endif
      endcase
      * --- Aggiorno il transitorio
      this.w_PADRE.Set("w_GIACON", this.oParentObject.w_GIACON)     
    endif
    this.bUpdateParentObject = .F.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la READ FROM ART_ALTE restituisce solo una riga
    *     chiedo con l'articolo trovato,altrimenti chiedo aprire maschera elenco
    do case
      case this.w_CONTA=1
        this.w_oPART = this.w_oMESS.addmsgpart("Seleziono articolo alternativo? (%1)")
        this.w_oPART.addParam(ALLTRIM(this.w_CODALT))     
        if this.w_oMESS.ah_YesNo()
          this.oParentObject.w_MVCODICE = this.w_CODALT
          this.oParentObject.w_GIACON = " "
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CACODART,CADESSUP,CATIPCO3,CATPCON3"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CACODART,CADESSUP,CATIPCO3,CATPCON3;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_MVCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MVCODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            this.oParentObject.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
            this.oParentObject.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.oParentObject.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.oParentObject.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            this.oParentObject.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
            this.oParentObject.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            this.oParentObject.w_MVDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
            this.oParentObject.w_TIPCO3 = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
            this.oParentObject.w_TPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.o_MVCODICE = ""
          this.w_PADRE.mCalc(.T.)     
          this.oParentObject.w_MVDESART = this.w_DESALT
          this.w_PADRE.NotifyEvent("w_MVCODICE Changed")     
        endif
      case this.w_CONTA>1
        this.w_oMESS.AddMsgPart("Seleziono articoli alternativi?")     
        if this.w_oMESS.ah_YesNo()
          * --- Lancio maschera articoli alternativi
          this.w_PADRE.NotifyEvent("ListaAlternativi")     
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ALTE'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='KEY_ARTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
