* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_sad                                                        *
*              Stampa analisi pre-storicizzazione                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_81]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-27                                                      *
* Last revis.: 2007-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgssr_sad",oParentObject))

* --- Class definition
define class tgssr_sad as StdForm
  Top    = 67
  Left   = 85

  * --- Standard Properties
  Width  = 531
  Height = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-16"
  HelpContextID=124347753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gssr_sad"
  cComment = "Stampa analisi pre-storicizzazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODESE = space(4)
  w_TIPODOC = space(5)
  w_CODCONTO = space(15)
  w_STATO = space(1)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_VALNAZ = space(3)
  w_CODAZI = space(4)
  w_DESCCAU = space(35)
  w_DESCONT = space(40)
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPINTES = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_sadPag1","gssr_sad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODESE=space(4)
      .w_TIPODOC=space(5)
      .w_CODCONTO=space(15)
      .w_STATO=space(1)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_VALNAZ=space(3)
      .w_CODAZI=space(4)
      .w_DESCCAU=space(35)
      .w_DESCONT=space(40)
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPINTES=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODESE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_6('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCONTO))
          .link_1_7('Full')
        endif
        .w_STATO = 'T'
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(5,7,.f.)
        .w_CODAZI = i_CODAZI
          .DoRTCalc(9,11,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCONTO_1_7.visible=!this.oPgFrm.Page1.oPag.oCODCONTO_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDESCONT_1_20.visible=!this.oPgFrm.Page1.oPag.oDESCONT_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_1'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPODOC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_6'),i_cWhere,'GSVE_ATD',"Tipi documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_TIPINTES = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCCAU = space(35)
      this.w_TIPINTES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_CODCONTO))
          select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANCODICE',cp_AbsName(oSource.parent,'oCODCONTO_1_7'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'GSSR_ZAD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_CODCONTO)
            select ANCODICE,ANDESCRI,ANTIPCON,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCONT = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPCON = NVL(_Link_.ANTIPCON,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTO = space(15)
      endif
      this.w_DESCONT = space(40)
      this.w_TIPCON = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty (.w_DATOBSO) OR  (.w_DATOBSO >= .w_OBTEST) and (.w_TIPCON = .w_TIPINTES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Intestatario obsoleto, non presente o di tipo diverso da intestatario causale.")
        endif
        this.w_CODCONTO = space(15)
        this.w_DESCONT = space(40)
        this.w_TIPCON = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_1.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_1.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_6.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_6.value=this.w_TIPODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTO_1_7.value==this.w_CODCONTO)
      this.oPgFrm.Page1.oPag.oCODCONTO_1_7.value=this.w_CODCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_8.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_12.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_12.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_13.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_13.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_14.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_14.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCAU_1_19.value==this.w_DESCCAU)
      this.oPgFrm.Page1.oPag.oDESCCAU_1_19.value=this.w_DESCCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONT_1_20.value==this.w_DESCONT)
      this.oPgFrm.Page1.oPag.oDESCONT_1_20.value=this.w_DESCONT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty (.w_DATOBSO) OR  (.w_DATOBSO >= .w_OBTEST) and (.w_TIPCON = .w_TIPINTES))  and not(.w_TIPINTES='N')  and not(empty(.w_CODCONTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCONTO_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intestatario obsoleto, non presente o di tipo diverso da intestatario causale.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgssr_sadPag1 as StdContainer
  Width  = 527
  height = 204
  stdWidth  = 527
  stdheight = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_1 as StdField with uid="QBGAPNSEQE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio da storicizzare",;
    HelpContextID = 217043418,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=111, Top=10, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc

  add object oTIPODOC_1_6 as StdField with uid="XBUUVJGYAH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento.",;
    HelpContextID = 204138806,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=111, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documento",'',this.parent.oContained
  endproc
  proc oTIPODOC_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oCODCONTO_1_7 as StdField with uid="PCIWQGLZZI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCONTO", cQueryName = "CODCONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Intestatario obsoleto, non presente o di tipo diverso da intestatario causale.",;
    ToolTipText = "Intestatario del documento.",;
    HelpContextID = 198061685,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=111, Top=65, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANCODICE", oKey_1_2="this.w_CODCONTO"

  func oCODCONTO_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPINTES='N')
    endwith
  endfunc

  func oCODCONTO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CONTI','*','ANCODICE',cp_AbsName(this.parent,'oCODCONTO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'GSSR_ZAD.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCONTO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCONTO
     i_obj.ecpSave()
  endproc


  add object oSTATO_1_8 as StdCombo with uid="KHDXJWAHID",rtseq=4,rtrep=.f.,left=111,top=94,width=107,height=21;
    , ToolTipText = "Stato del documento.";
    , HelpContextID = 35716058;
    , cFormVar="w_STATO",RowSource=""+"Tutti,"+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_8.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oSTATO_1_8.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_8.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='T',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='S',3,;
      0)))
  endfunc


  add object oObj_1_9 as cp_outputCombo with uid="ZLERXIVPWT",left=111, top=127, width=410,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53649894


  add object oBtn_1_10 as StdButton with uid="MQBTHVVWFJ",left=421, top=155, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 124347658;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="IITKQFQBSW",left=473, top=155, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 124347658;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oINIESE_1_12 as StdField with uid="DMODWSHHHR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio esercizio",;
    HelpContextID = 217023098,;
   bGlobalFont=.t.,;
    Height=23, Width=79, Left=210, Top=10

  add object oFINESE_1_13 as StdField with uid="XDAZANRHSH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine esercizio.",;
    HelpContextID = 217003946,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=322, Top=10

  add object oVALNAZ_1_14 as StdField with uid="TUYKATMZHJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta nazionale esercizio.",;
    HelpContextID = 117023062,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=448, Top=10, InputMask=replicate('X',3)

  add object oDESCCAU_1_19 as StdField with uid="BCHDQZRPDP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCCAU", cQueryName = "DESCCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235869238,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=190, Top=38, InputMask=replicate('X',35)

  add object oDESCONT_1_20 as StdField with uid="SEUMKKOAHE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCONT", cQueryName = "DESCONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 198120502,;
   bGlobalFont=.t.,;
    Height=21, Width=262, Left=233, Top=65, InputMask=replicate('X',40)

  func oDESCONT_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPINTES='N')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="MOFRDPRTRT",Visible=.t., Left=56, Top=10,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="LCNRHHWZSU",Visible=.t., Left=174, Top=10,;
    Alignment=1, Width=35, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="VEZLBBKRJB",Visible=.t., Left=291, Top=10,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="YNVZKUXPVJ",Visible=.t., Left=411, Top=10,;
    Alignment=1, Width=18, Height=18,;
    Caption="In:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NMZVDOUOQG",Visible=.t., Left=24, Top=127,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="XLXAVEVDUQ",Visible=.t., Left=37, Top=65,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPINTES='N')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="SWWFUKSJFE",Visible=.t., Left=33, Top=38,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AOMNBGNRAH",Visible=.t., Left=65, Top=94,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_sad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
