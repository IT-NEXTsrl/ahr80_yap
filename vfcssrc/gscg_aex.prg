* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aex                                                        *
*              Dati comunicazione elenchi clienti / fornitori                  *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-26                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_aex")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_aex")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_aex")
  return

* --- Class definition
define class tgscg_aex as StdPCForm
  Width  = 870
  Height = 201
  Top    = 10
  Left   = 10
  cComment = "Dati comunicazione elenchi clienti / fornitori"
  cPrg = "gscg_aex"
  HelpContextID=76162409
  add object cnt as tcgscg_aex
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_aex as PCContext
  w_DESERIAL = space(10)
  w_CPROWNUM = 0
  w_DE__ANNO = space(4)
  w_DETIPSOG = space(1)
  w_DEPARIVA = space(12)
  w_DECODFIS = space(16)
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_DECONPLU = space(1)
  w_DEESCGEN = space(1)
  w_DESCLF = space(40)
  w_DEDESCON = space(50)
  w_OBTEST = space(10)
  w_DTOBSO = space(8)
  proc Save(oFrom)
    this.w_DESERIAL = oFrom.w_DESERIAL
    this.w_CPROWNUM = oFrom.w_CPROWNUM
    this.w_DE__ANNO = oFrom.w_DE__ANNO
    this.w_DETIPSOG = oFrom.w_DETIPSOG
    this.w_DEPARIVA = oFrom.w_DEPARIVA
    this.w_DECODFIS = oFrom.w_DECODFIS
    this.w_DETIPCON = oFrom.w_DETIPCON
    this.w_DECODCON = oFrom.w_DECODCON
    this.w_DECONPLU = oFrom.w_DECONPLU
    this.w_DEESCGEN = oFrom.w_DEESCGEN
    this.w_DESCLF = oFrom.w_DESCLF
    this.w_DEDESCON = oFrom.w_DEDESCON
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DTOBSO = oFrom.w_DTOBSO
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DESERIAL = this.w_DESERIAL
    oTo.w_CPROWNUM = this.w_CPROWNUM
    oTo.w_DE__ANNO = this.w_DE__ANNO
    oTo.w_DETIPSOG = this.w_DETIPSOG
    oTo.w_DEPARIVA = this.w_DEPARIVA
    oTo.w_DECODFIS = this.w_DECODFIS
    oTo.w_DETIPCON = this.w_DETIPCON
    oTo.w_DECODCON = this.w_DECODCON
    oTo.w_DECONPLU = this.w_DECONPLU
    oTo.w_DEESCGEN = this.w_DEESCGEN
    oTo.w_DESCLF = this.w_DESCLF
    oTo.w_DEDESCON = this.w_DEDESCON
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DTOBSO = this.w_DTOBSO
    PCContext::Load(oTo)
enddefine

define class tcgscg_aex as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 870
  Height = 201
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-09-03"
  HelpContextID=76162409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  DAELCLFO_IDX = 0
  CONTI_IDX = 0
  cFile = "DAELCLFO"
  cKeySelect = "DESERIAL"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cKeyWhereODBCqualified = '"DAELCLFO.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cPrg = "gscg_aex"
  cComment = "Dati comunicazione elenchi clienti / fornitori"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_CPROWNUM = 0
  w_DE__ANNO = space(4)
  o_DE__ANNO = space(4)
  w_DETIPSOG = space(1)
  o_DETIPSOG = space(1)
  w_DEPARIVA = space(12)
  w_DECODFIS = space(16)
  w_DETIPCON = space(1)
  o_DETIPCON = space(1)
  w_DECODCON = space(15)
  o_DECODCON = space(15)
  w_DECONPLU = space(1)
  w_DEESCGEN = space(1)
  w_DESCLF = space(40)
  w_DEDESCON = space(50)
  w_OBTEST = space(10)
  w_DTOBSO = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DESERIAL = this.W_DESERIAL

  * --- Children pointers
  GSCG_AEY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCG_AEY additive
    with this
      .Pages(1).addobject("oPag","tgscg_aexPag1","gscg_aex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 17714954
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDE__ANNO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCG_AEY
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DAELCLFO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DAELCLFO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DAELCLFO_IDX,3]
  return

  function CreateChildren()
    this.GSCG_AEY = CREATEOBJECT('stdDynamicChild',this,'GSCG_AEY',this.oPgFrm.Page1.oPag.oLinkPC_1_20)
    this.GSCG_AEY.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgscg_aex'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCG_AEY)
      this.GSCG_AEY.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCG_AEY.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCG_AEY.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCG_AEY)
      this.GSCG_AEY.DestroyChildrenChain()
      this.GSCG_AEY=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_20')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_AEY.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_AEY.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_AEY.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_AEY.SetKey(;
            .w_DESERIAL,"DESERIAL";
            ,.w_CPROWNUM,"CPROWNUM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_AEY.ChangeRow(this.cRowID+'      1',1;
             ,.w_DESERIAL,"DESERIAL";
             ,.w_CPROWNUM,"CPROWNUM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_AEY)
        i_f=.GSCG_AEY.BuildFilter()
        if !(i_f==.GSCG_AEY.cQueryFilter)
          i_fnidx=.GSCG_AEY.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AEY.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AEY.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AEY.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AEY.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_12_joined
    link_1_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DAELCLFO where DESERIAL=KeySet.DESERIAL
    *
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DAELCLFO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DAELCLFO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DAELCLFO '
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CPROWNUM = 1
        .w_DESCLF = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .op_DESERIAL = .w_DESERIAL
        .w_DE__ANNO = NVL(DE__ANNO,space(4))
        .w_DETIPSOG = NVL(DETIPSOG,space(1))
        .w_DEPARIVA = NVL(DEPARIVA,space(12))
        .w_DECODFIS = NVL(DECODFIS,space(16))
        .w_DETIPCON = NVL(DETIPCON,space(1))
        .w_DECODCON = NVL(DECODCON,space(15))
          if link_1_12_joined
            this.w_DECODCON = NVL(ANCODICE112,NVL(this.w_DECODCON,space(15)))
            this.w_DESCLF = NVL(ANDESCRI112,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_DECONPLU = NVL(DECONPLU,space(1))
        .w_DEESCGEN = NVL(DEESCGEN,space(1))
        .w_DEDESCON = NVL(DEDESCON,space(50))
        .w_OBTEST = date(iif(empty(.w_DE__ANNO),year(i_datsys)-1,val(.w_DE__ANNO)),1,1)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DAELCLFO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DESERIAL = space(10)
      .w_CPROWNUM = 0
      .w_DE__ANNO = space(4)
      .w_DETIPSOG = space(1)
      .w_DEPARIVA = space(12)
      .w_DECODFIS = space(16)
      .w_DETIPCON = space(1)
      .w_DECODCON = space(15)
      .w_DECONPLU = space(1)
      .w_DEESCGEN = space(1)
      .w_DESCLF = space(40)
      .w_DEDESCON = space(50)
      .w_OBTEST = space(10)
      .w_DTOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CPROWNUM = 1
        .w_DE__ANNO = STR(YEAR(i_DATSYS)-1,4)
        .w_DETIPSOG = 'C'
          .DoRTCalc(5,6,.f.)
        .w_DETIPCON = 'C'
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_DECODCON))
          .link_1_12('Full')
          endif
          .DoRTCalc(9,11,.f.)
        .w_DEDESCON = .w_DESCLF
        .w_OBTEST = date(iif(empty(.w_DE__ANNO),year(i_datsys)-1,val(.w_DE__ANNO)),1,1)
      endif
    endwith
    cp_BlankRecExtFlds(this,'DAELCLFO')
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DESERIAL = .w_DESERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDE__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oDETIPSOG_1_5.enabled = i_bVal
      .Page1.oPag.oDEPARIVA_1_7.enabled = i_bVal
      .Page1.oPag.oDECODFIS_1_9.enabled = i_bVal
      .Page1.oPag.oDEESCGEN_1_15.enabled = i_bVal
      .Page1.oPag.oDEDESCON_1_17.enabled = i_bVal
    endwith
    this.GSCG_AEY.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DAELCLFO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_AEY.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__ANNO,"DE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPSOG,"DETIPSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEPARIVA,"DEPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODFIS,"DECODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DETIPCON,"DETIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODCON,"DECODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECONPLU,"DECONPLU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEESCGEN,"DEESCGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESCON,"DEDESCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DAELCLFO_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DAELCLFO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DAELCLFO')
        i_extval=cp_InsertValODBCExtFlds(this,'DAELCLFO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DESERIAL,DE__ANNO,DETIPSOG,DEPARIVA,DECODFIS"+;
                  ",DETIPCON,DECODCON,DECONPLU,DEESCGEN,DEDESCON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DESERIAL)+;
                  ","+cp_ToStrODBC(this.w_DE__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DETIPSOG)+;
                  ","+cp_ToStrODBC(this.w_DEPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DECODFIS)+;
                  ","+cp_ToStrODBC(this.w_DETIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_DECODCON)+;
                  ","+cp_ToStrODBC(this.w_DECONPLU)+;
                  ","+cp_ToStrODBC(this.w_DEESCGEN)+;
                  ","+cp_ToStrODBC(this.w_DEDESCON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DAELCLFO')
        i_extval=cp_InsertValVFPExtFlds(this,'DAELCLFO')
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL)
        INSERT INTO (i_cTable);
              (DESERIAL,DE__ANNO,DETIPSOG,DEPARIVA,DECODFIS,DETIPCON,DECODCON,DECONPLU,DEESCGEN,DEDESCON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_DE__ANNO;
                  ,this.w_DETIPSOG;
                  ,this.w_DEPARIVA;
                  ,this.w_DECODFIS;
                  ,this.w_DETIPCON;
                  ,this.w_DECODCON;
                  ,this.w_DECONPLU;
                  ,this.w_DEESCGEN;
                  ,this.w_DEDESCON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DAELCLFO_IDX,i_nConn)
      *
      * update DAELCLFO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DAELCLFO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DE__ANNO="+cp_ToStrODBC(this.w_DE__ANNO)+;
             ",DETIPSOG="+cp_ToStrODBC(this.w_DETIPSOG)+;
             ",DEPARIVA="+cp_ToStrODBC(this.w_DEPARIVA)+;
             ",DECODFIS="+cp_ToStrODBC(this.w_DECODFIS)+;
             ",DETIPCON="+cp_ToStrODBC(this.w_DETIPCON)+;
             ",DECODCON="+cp_ToStrODBCNull(this.w_DECODCON)+;
             ",DECONPLU="+cp_ToStrODBC(this.w_DECONPLU)+;
             ",DEESCGEN="+cp_ToStrODBC(this.w_DEESCGEN)+;
             ",DEDESCON="+cp_ToStrODBC(this.w_DEDESCON)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DAELCLFO')
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        UPDATE (i_cTable) SET;
              DE__ANNO=this.w_DE__ANNO;
             ,DETIPSOG=this.w_DETIPSOG;
             ,DEPARIVA=this.w_DEPARIVA;
             ,DECODFIS=this.w_DECODFIS;
             ,DETIPCON=this.w_DETIPCON;
             ,DECODCON=this.w_DECODCON;
             ,DECONPLU=this.w_DECONPLU;
             ,DEESCGEN=this.w_DEESCGEN;
             ,DEDESCON=this.w_DEDESCON;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_AEY : Saving
      this.GSCG_AEY.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DESERIAL,"DESERIAL";
             ,this.w_CPROWNUM,"CPROWNUM";
             )
      this.GSCG_AEY.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSCG_AEY : Deleting
    this.GSCG_AEY.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DESERIAL,"DESERIAL";
           ,this.w_CPROWNUM,"CPROWNUM";
           )
    this.GSCG_AEY.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DAELCLFO_IDX,i_nConn)
      *
      * delete DAELCLFO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELCLFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
          .link_1_12('Full')
        .DoRTCalc(9,11,.t.)
        if .o_DECODCON<>.w_DECODCON
            .w_DEDESCON = .w_DESCLF
        endif
        if .o_DE__ANNO<>.w_DE__ANNO
            .w_OBTEST = date(iif(empty(.w_DE__ANNO),year(i_datsys)-1,val(.w_DE__ANNO)),1,1)
        endif
        if .o_DETIPCON<>.w_DETIPCON.or. .o_DETIPSOG<>.w_DETIPSOG.or. .o_DECODCON<>.w_DECODCON
          .Calculate_YKYNWABSSS()
        endif
        if .o_DETIPSOG<>.w_DETIPSOG
          .Calculate_YQJNVDGMYP()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEECF","i_CODAZI,w_DESERIAL")
          .op_DESERIAL = .w_DESERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(14,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_YKYNWABSSS()
    with this
          * --- Variazione identificativo su movimento generato
          GSCG_BEL(this;
             )
    endwith
  endproc
  proc Calculate_YQJNVDGMYP()
    with this
          * --- Esegue mcalc su gscg_aey
          GSCG_AEY_MCALC(this;
             )
    endwith
  endproc
  proc Calculate_ROSCJVPQMD()
    with this
          * --- Controllo flag codifica plurima
          GSCG_BEX(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Record Deleted") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_ROSCJVPQMD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODCON
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DECODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"+ ",link_1_12.ANDTOBSO as ANDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on DAELCLFO.DECODCON=link_1_12.ANCODICE"+" and DAELCLFO.DETIPCON=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and DAELCLFO.DECODCON=link_1_12.ANCODICE(+)"'+'+" and DAELCLFO.DETIPCON=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDE__ANNO_1_3.value==this.w_DE__ANNO)
      this.oPgFrm.Page1.oPag.oDE__ANNO_1_3.value=this.w_DE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPSOG_1_5.RadioValue()==this.w_DETIPSOG)
      this.oPgFrm.Page1.oPag.oDETIPSOG_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPARIVA_1_7.value==this.w_DEPARIVA)
      this.oPgFrm.Page1.oPag.oDEPARIVA_1_7.value=this.w_DEPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODFIS_1_9.value==this.w_DECODFIS)
      this.oPgFrm.Page1.oPag.oDECODFIS_1_9.value=this.w_DECODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDETIPCON_1_11.RadioValue()==this.w_DETIPCON)
      this.oPgFrm.Page1.oPag.oDETIPCON_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDECODCON_1_12.value==this.w_DECODCON)
      this.oPgFrm.Page1.oPag.oDECODCON_1_12.value=this.w_DECODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDECONPLU_1_14.RadioValue()==this.w_DECONPLU)
      this.oPgFrm.Page1.oPag.oDECONPLU_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEESCGEN_1_15.RadioValue()==this.w_DEESCGEN)
      this.oPgFrm.Page1.oPag.oDEESCGEN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_16.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_16.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCON_1_17.value==this.w_DEDESCON)
      this.oPgFrm.Page1.oPag.oDEDESCON_1_17.value=this.w_DEDESCON
    endif
    cp_SetControlsValueExtFlds(this,'DAELCLFO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DE__ANNO)) or not(Val(.w_DE__ANNO)>2005 and Val(.w_DE__ANNO)<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDE__ANNO_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DEPARIVA)) or not(CHKCFP(.w_DEPARIVA, "PI", .w_DETIPCON, .w_DECODCON, space(3))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEPARIVA_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DEPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_DECODFIS, 'CF', .w_DETIPCON, .w_DECODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECODFIS_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO))  and not(empty(.w_DECODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDECODCON_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_AEY.CheckForm()
      if i_bres
        i_bres=  .GSCG_AEY.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aex
      if Empty(this.GSCG_AEY.w_DEIMPIMP) and Empty(this.GSCG_AEY.w_DEIMPAFF) and Empty(this.GSCG_AEY.w_DEIMPNOI) and Empty(this.GSCG_AEY.w_DEIMPESE) and Empty(this.GSCG_AEY.w_DEIMPNIN) and Empty(this.GSCG_AEY.w_DEIMPLOR)
        i_bnoChk=.f.
        i_cErrorMsg=Ah_MsgFormat("Occorre valorizzare almeno un importo")
        i_bRes=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DE__ANNO = this.w_DE__ANNO
    this.o_DETIPSOG = this.w_DETIPSOG
    this.o_DETIPCON = this.w_DETIPCON
    this.o_DECODCON = this.w_DECODCON
    * --- GSCG_AEY : Depends On
    this.GSCG_AEY.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_aexPag1 as StdContainer
  Width  = 866
  height = 201
  stdWidth  = 866
  stdheight = 201
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDE__ANNO_1_3 as StdField with uid="EOBASEXJQB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DE__ANNO", cQueryName = "DE__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno riferimento",;
    HelpContextID = 233510021,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=104, Top=7, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oDE__ANNO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_DE__ANNO)>2005 and Val(.w_DE__ANNO)<2100)
    endwith
    return bRes
  endfunc


  add object oDETIPSOG_1_5 as StdCombo with uid="NATXNJYNXE",rtseq=4,rtrep=.f.,left=327,top=7,width=132,height=21;
    , ToolTipText = "Tipo soggetto (cliente / fornitore)";
    , HelpContextID = 63202429;
    , cFormVar="w_DETIPSOG",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPSOG_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDETIPSOG_1_5.GetRadio()
    this.Parent.oContained.w_DETIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oDETIPSOG_1_5.SetRadio()
    this.Parent.oContained.w_DETIPSOG=trim(this.Parent.oContained.w_DETIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPSOG=='C',1,;
      iif(this.Parent.oContained.w_DETIPSOG=='F',2,;
      0))
  endfunc

  add object oDEPARIVA_1_7 as StdField with uid="QWMAPNDTIE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DEPARIVA", cQueryName = "DEPARIVA",;
    bObbl = .t. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 103013257,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=104, Top=33, cSayPict='REPL("!",12)', cGetPict='REPL("!",11)', InputMask=replicate('X',12)

  func oDEPARIVA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_DEPARIVA, "PI", .w_DETIPCON, .w_DECODCON, space(3)))
    endwith
    return bRes
  endfunc

  add object oDECODFIS_1_9 as StdField with uid="LACTEVFCYP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DECODFIS", cQueryName = "DECODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 167160695,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=327, Top=33, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  func oDECODFIS_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_DECODFIS, 'CF', .w_DETIPCON, .w_DECODCON))
    endwith
    return bRes
  endfunc


  add object oDETIPCON_1_11 as StdCombo with uid="NFSQNKXKGF",rtseq=7,rtrep=.f.,left=104,top=60,width=102,height=21, enabled=.f.;
    , ToolTipText = "Tipo conto (cliente / fornitore)";
    , HelpContextID = 63202436;
    , cFormVar="w_DETIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDETIPCON_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDETIPCON_1_11.GetRadio()
    this.Parent.oContained.w_DETIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDETIPCON_1_11.SetRadio()
    this.Parent.oContained.w_DETIPCON=trim(this.Parent.oContained.w_DETIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DETIPCON=='C',1,;
      iif(this.Parent.oContained.w_DETIPCON=='F',2,;
      0))
  endfunc

  func oDETIPCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DECODCON)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDECODCON_1_12 as StdField with uid="ZRBHLQSDAV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DECODCON", cQueryName = "DECODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente/fornitore",;
    HelpContextID = 50943108,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=327, Top=60, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCON"

  func oDECODCON_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDECONPLU_1_14 as StdCheck with uid="KKQHKBNIKY",rtseq=9,rtrep=.f.,left=104, top=108, caption="Codifica non univoca", enabled=.f.,;
    ToolTipText = "Flag codifica non univoca",;
    HelpContextID = 11097227,;
    cFormVar="w_DECONPLU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDECONPLU_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDECONPLU_1_14.GetRadio()
    this.Parent.oContained.w_DECONPLU = this.RadioValue()
    return .t.
  endfunc

  func oDECONPLU_1_14.SetRadio()
    this.Parent.oContained.w_DECONPLU=trim(this.Parent.oContained.w_DECONPLU)
    this.value = ;
      iif(this.Parent.oContained.w_DECONPLU=='S',1,;
      0)
  endfunc

  add object oDEESCGEN_1_15 as StdCheck with uid="OICEACOGWI",rtseq=10,rtrep=.f.,left=327, top=110, caption="Escludi da generazione",;
    ToolTipText = "Flag escludi da generazione",;
    HelpContextID = 151161724,;
    cFormVar="w_DEESCGEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDEESCGEN_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDEESCGEN_1_15.GetRadio()
    this.Parent.oContained.w_DEESCGEN = this.RadioValue()
    return .t.
  endfunc

  func oDEESCGEN_1_15.SetRadio()
    this.Parent.oContained.w_DEESCGEN=trim(this.Parent.oContained.w_DEESCGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DEESCGEN=='S',1,;
      0)
  endfunc

  add object oDESCLF_1_16 as StdField with uid="ZYVLYCLGUL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108942390,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=466, Top=60, InputMask=replicate('X',40)

  add object oDEDESCON_1_17 as StdField with uid="EMEROTUJFT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DEDESCON", cQueryName = "DEDESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 66020484,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=327, Top=87, InputMask=replicate('X',50)


  add object oLinkPC_1_20 as stdDynamicChildContainer with uid="XCXRLXEQEL",left=2, top=138, width=859, height=59, bOnScreen=.t.;


  add object oStr_1_4 as StdString with uid="MCSMSCPQDR",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=93, Height=18,;
    Caption="Anno riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="HCCHFZGUGX",Visible=.t., Left=229, Top=7,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SHTZUVQHGU",Visible=.t., Left=28, Top=33,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EXDPEBZJES",Visible=.t., Left=238, Top=33,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CJRCENSCGA",Visible=.t., Left=248, Top=60,;
    Alignment=1, Width=78, Height=18,;
    Caption="Codice conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RZPRQKZMCU",Visible=.t., Left=241, Top=87,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SBFFJWOAWJ",Visible=.t., Left=25, Top=60,;
    Alignment=1, Width=75, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aex','DAELCLFO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=DAELCLFO.DESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_aex
proc GSCG_AEY_MCALC(obj)
 obj.GSCG_AEY.mcalc(.t.)
endproc
* --- Fine Area Manuale
