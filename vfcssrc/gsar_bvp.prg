* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bvp                                                        *
*              Controlli in cancellazioni dichiarazioni periodiche             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-07                                                      *
* Last revis.: 2001-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bvp",oParentObject)
return(i_retval)

define class tgsar_bvp as StdBatch
  * --- Local variables
  w_ANNO = space(4)
  w_PERI = 0
  w_ATTI = space(5)
  w_OK = .f.
  w_MESS = space(10)
  w_NUMPER = 0
  w_TIPREG = space(1)
  w_KEYATT    = space(5)
  w_NUMREG = 0
  w_ROWNUM = 0
  * --- WorkFile variables
  CREDDIVA_idx=0
  IVA_PERI_idx=0
  PRI_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla cancellazione della Dihiarazione Periodica (da GSAR_AVP)
    this.w_OK = .T.
    this.w_MESS = " "
    * --- Legge le Liquidazioni degli Anni successivi o dello stesso periodo
    * --- Select from IVA_PERI
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" IVA_PERI ";
          +" where VP__ANNO>"+cp_ToStrODBC(this.oParentObject.w_VP__ANNO)+" OR (VP__ANNO="+cp_ToStrODBC(this.oParentObject.w_VP__ANNO)+" AND VPPERIOD>="+cp_ToStrODBC(this.oParentObject.w_VPPERIOD)+")";
           ,"_Curs_IVA_PERI")
    else
      select * from (i_cTable);
       where VP__ANNO>this.oParentObject.w_VP__ANNO OR (VP__ANNO=this.oParentObject.w_VP__ANNO AND VPPERIOD>=this.oParentObject.w_VPPERIOD);
        into cursor _Curs_IVA_PERI
    endif
    if used('_Curs_IVA_PERI')
      select _Curs_IVA_PERI
      locate for 1=1
      do while not(eof())
      this.w_ANNO = NVL(_Curs_IVA_PERI.VP__ANNO, "    ")
      this.w_PERI = NVL(_Curs_IVA_PERI.VPPERIOD, 0)
      this.w_ATTI = NVL(_Curs_IVA_PERI.VPKEYATT, "     ")
      if this.w_OK=.T.
        do case
          case this.w_ANNO>this.oParentObject.w_VP__ANNO OR (this.w_ANNO=this.oParentObject.w_VP__ANNO AND this.w_PERI>this.oParentObject.w_VPPERIOD)
            this.w_MESS = ah_Msgformat("Esistono dichiarazioni in periodi successivi a quello in eliminazione%0Cancellazione impossibile")
            this.w_OK = .F.
          case this.w_ATTI="#####" AND this.oParentObject.w_VPKEYATT<>this.w_ATTI
            this.w_MESS = ah_Msgformat("Per l'anno e il periodo selezionato � gi� stata generata la dichiarazione riepilogativa%0� necessario prima eliminare quest'ultima")
            this.w_OK = .F.
        endcase
      endif
        select _Curs_IVA_PERI
        continue
      enddo
      use
    endif
    if this.w_OK=.F.
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    else
      * --- Se singola Attivita' o Riepilogativa
      this.w_NUMPER = this.oParentObject.w_VPPERIOD
      this.w_KEYATT    = this.oParentObject.w_VPCODATT
      if this.oParentObject.w_VPKEYATT="#####"
        * --- Elimina Flag Stampata Dichiarazione di Riepilogo
        * --- Try
        local bErr_037C6D88
        bErr_037C6D88=bTrsErr
        this.Try_037C6D88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_037C6D88
        * --- End
      else
        * --- Elimino flag Liquidazione
        * --- Select from gscg2blp
        do vq_exec with 'gscg2blp',this,'_Curs_gscg2blp','',.f.,.t.
        if used('_Curs_gscg2blp')
          select _Curs_gscg2blp
          locate for 1=1
          do while not(eof())
          this.w_TIPREG = NVL(_Curs_GSCG2BLP.TIPREG, " ")
          this.w_NUMREG = NVL(_Curs_GSCG2BLP.NUMREG, 0)
          if this.w_NUMREG<>0 AND NOT EMPTY(this.w_TIPREG)
            * --- Try
            local bErr_03F64410
            bErr_03F64410=bTrsErr
            this.Try_03F64410()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03F64410
            * --- End
          endif
            select _Curs_gscg2blp
            continue
          enddo
          use
        endif
      endif
      if g_ATTIVI<>"S" OR this.oParentObject.w_VPKEYATT="#####"
        * --- Cancello i Riferimenti ai Crediti IVA del Periodo 
        * --- Try
        local bErr_0381E958
        bErr_0381E958=bTrsErr
        this.Try_0381E958()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0381E958
        * --- End
      endif
    endif
  endproc
  proc Try_037C6D88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRI_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRI_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLSTAR ="+cp_NullLink(cp_ToStrODBC(" "),'PRI_MAST','TRFLSTAR');
          +i_ccchkf ;
      +" where ";
          +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
          +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
             )
    else
      update (i_cTable) set;
          TRFLSTAR = " ";
          &i_ccchkf. ;
       where;
          TR__ANNO = this.w_ANNO;
          and TRNUMPER = this.w_NUMPER;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03F64410()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRI_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRI_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLSTAM ="+cp_NullLink(cp_ToStrODBC(" "),'PRI_MAST','TRFLSTAM');
          +i_ccchkf ;
      +" where ";
          +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
          +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
          +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
          +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
             )
    else
      update (i_cTable) set;
          TRFLSTAM = " ";
          &i_ccchkf. ;
       where;
          TR__ANNO = this.w_ANNO;
          and TRNUMPER = this.w_NUMPER;
          and TRTIPREG = this.w_TIPREG;
          and TRNUMREG = this.w_NUMREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0381E958()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CREDDIVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CREDDIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CREDDIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CICREDET ="+cp_NullLink(cp_ToStrODBC(0),'CREDDIVA','CICREDET');
      +",CIDATUTI ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'CREDDIVA','CIDATUTI');
          +i_ccchkf ;
      +" where ";
          +"CI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_VP__ANNO);
          +" and CINUMPER = "+cp_ToStrODBC(this.oParentObject.w_VPPERIOD);
          +" and CIFLTEST = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          CICREDET = 0;
          ,CIDATUTI = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          CI__ANNO = this.oParentObject.w_VP__ANNO;
          and CINUMPER = this.oParentObject.w_VPPERIOD;
          and CIFLTEST = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CREDDIVA'
    this.cWorkTables[2]='IVA_PERI'
    this.cWorkTables[3]='PRI_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_IVA_PERI')
      use in _Curs_IVA_PERI
    endif
    if used('_Curs_gscg2blp')
      use in _Curs_gscg2blp
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
