* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bi3                                                        *
*              Import da documenti collegati                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1026]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2017-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bi3",oParentObject)
return(i_retval)

define class tgsve_bi3 as StdBatch
  * --- Local variables
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_RECTRS = 0
  w_AZZSPE = 0
  w_EVASER = space(10)
  w_EVADOC = space(5)
  w_NOEVA = space(1)
  w_NUMEVA = 0
  w_ALFEVA = space(10)
  w_EVADAT = ctod("  /  /  ")
  w_PUNTAT = 0
  w_NEWDOC = space(10)
  w_INDICE = 0
  w_OKRIG = .f.
  w_NORIG = .f.
  w_COACNUM = 0
  w_FLAGG = space(1)
  w_MODUM2 = space(1)
  w_NOFRAZ = space(1)
  w_IMPLOT = 0
  w_TOTEFF = 0
  w_RESIDUO = 0
  w_TOTMOV = 0
  w_TOTIMP = 0
  w_QTAIMP = 0
  w_RESIM1 = 0
  w_RESIMP = 0
  w_RAPUM = 0
  w_OKDEC = .f.
  w_OKSCA = space(1)
  w_FLAVAL = space(1)
  w_FLCAUM = space(1)
  w_QTAIMPLO = 0
  w_BCONTACC = .f.
  w_RIFCAC = 0
  w_RIFCACORD = 0
  w_ROWINDEX = 0
  w_LOOP = 0
  w_FIRSTLAP = .f.
  w_LFLRRIF = space(1)
  w_TMPCARI = space(1)
  w_GSVE_MMT = .NULL.
  w_NUMREC = 0
  w_ANNULLA = .f.
  w_TDMCALSI = space(1)
  w_TDMCALST = space(1)
  w_DCFLRIPS = space(1)
  w_TFLACCO = space(1)
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_SCADUTA = .f.
  w_OK_LET = .f.
  w_FLSFIN = space(1)
  w_SCOFIN = 0
  w_LFLACCO = space(1)
  w_BMESSAGG01 = .f.
  w_MESSAGG01 = space(254)
  w_BMESSAGG02 = .f.
  w_MESSAGG02 = space(254)
  w_BMESSAGG03 = .f.
  w_MESSAGG03 = space(254)
  w_BMESSAGG04 = .f.
  w_MESSAGG04 = space(254)
  w_BMESSAGG05 = .f.
  w_MESSAGG05 = space(254)
  w_BMESSAGG06 = .f.
  w_MESSAGG06 = space(254)
  w_PADRE = .NULL.
  w_DCCOLLEG = space(5)
  w_DCFLINTE = space(1)
  w_DCFLEVAS = space(1)
  w_MCAUCOL = space(5)
  w_MCODVAL = space(3)
  w_MCODIVE = space(5)
  w_MRIFDIC = space(15)
  w_ARTDES = space(20)
  w_SPEBOL = 0
  w_COVET = space(5)
  w_ASPES = space(30)
  w_MFLCASC = space(1)
  w_VISEVA = 0
  w_FLVEAC = space(1)
  w_SPEIMB = 0
  w_COVE2 = space(5)
  w_COAGE = space(5)
  w_MFLORDI = space(1)
  w_SPETRA = 0
  w_TRAINT = 0
  w_COVE3 = space(5)
  w_COAG2 = space(5)
  w_MFLIMPE = space(1)
  w_APPART = space(40)
  w_COPOR = space(1)
  w_SCCL1 = 0
  w_MFLRISE = space(1)
  w_NRIF = 0
  w_COSPE = space(3)
  w_SCCL2 = 0
  w_MTIPVOC = space(1)
  w_ARIF = space(10)
  w_COCON = space(1)
  w_SCPAG = 0
  w_MFLELGM = space(1)
  w_DRIF = ctod("  /  /  ")
  w_COBAN = space(10)
  w_CONCOR = space(25)
  w_MFLAVAL = space(1)
  w_NEST = 0
  w_COBA2 = space(15)
  w_BANC = space(15)
  w_APPO = space(10)
  w_AEST = space(10)
  w_RIFCLI = space(30)
  w_COPAG = space(5)
  w_MF2CASC = space(1)
  w_DEST = ctod("  /  /  ")
  w_DATPLA = ctod("  /  /  ")
  w_QTCOL = 0
  w_DADIV = ctod("  /  /  ")
  w_MF2ORDI = space(1)
  w_QTPES = 0
  w_APPO1 = ctod("  /  /  ")
  w_MF2IMPE = space(1)
  w_QTLOR = 0
  w_APPO2 = 0
  w_MF2RISE = space(1)
  w_NOAGG = space(40)
  w_APPO3 = space(10)
  w_CNT = 0
  w_CODES = space(5)
  w_APPOSER = space(10)
  RA = space(10)
  w_COORN = space(15)
  w_TIPORN = space(1)
  w_INCOM = ctod("  /  /  ")
  w_APPCOMM = ctod("  /  /  ")
  w_RICON = space(15)
  w_FICOM = ctod("  /  /  ")
  w_TCF = space(1)
  w_CLADO = space(2)
  w_FLACCO = space(1)
  w_FLINTE = space(1)
  w_OK = .f.
  w_CODART = space(20)
  w_CODLIS = space(5)
  w_CATCLI = space(5)
  w_OK0 = .f.
  w_DATREG = ctod("  /  /  ")
  w_CODCON = space(15)
  w_CATART = space(5)
  w_OK1 = .f.
  w_CODVAL = space(3)
  w_ARRSUP = 0
  w_SCOCON = .f.
  w_OK2 = .f.
  w_RN0 = 0
  w_CODGRU = space(5)
  w_ROWNUM = 0
  w_DT0 = ctod("  /  /  ")
  w_S10 = 0
  w_PZ2 = 0
  w_ACCONT = 0
  w_DT1 = ctod("  /  /  ")
  w_S20 = 0
  w_S12 = 0
  w_ACCPRE = 0
  w_ACCSUC = 0
  w_IMPARR = 0
  w_PZ1 = 0
  w_S30 = 0
  w_S22 = 0
  w_FLAPRE = .f.
  w_S11 = 0
  w_S40 = 0
  w_S32 = 0
  w_MESS = space(10)
  w_S21 = 0
  w_PZ0 = 0
  w_S42 = 0
  w_TFLCOMM = space(1)
  w_S31 = 0
  w_QUAN = 0
  w_RN2 = 0
  w_CLADOC = space(2)
  w_S41 = 0
  w_INDIVE1 = 0
  w_INDIVA1 = 0
  w_QTAUM1 = 0
  w_DATEVA = ctod("  /  /  ")
  w_FLSCAF = space(1)
  w_RN1 = 0
  w_QUAN = 0
  w_FLCAPA = space(1)
  w_FLFOSC = space(1)
  w_SCOFIS = 0
  w_MCAORIF = 0
  w_MRIFODL = space(10)
  w_DESRIF = space(18)
  w_IMPLET = .f.
  w_IMPIVE = .f.
  w_APPO4 = 0
  w_APPO7 = space(15)
  w_APPO8 = space(10)
  w_APPO9 = space(1)
  w_APPO10 = space(25)
  w_RAGSOC = space(40)
  w_INDIRI = space(35)
  w_LOCALI = space(30)
  w_CODCAP = space(8)
  w_CODFIS = space(16)
  w_PROVIN = space(2)
  w_CLIENTE = space(15)
  w_APPO6 = space(0)
  w_AGGSAL = space(10)
  w_AGGSAL1 = space(10)
  w_ACCSUC1 = 0
  w_CODSED = space(5)
  w_OVEAC = space(15)
  w_VISPRE = 0
  w_FLVABD = space(1)
  w_ORIMAG = space(5)
  w_ORIMAT = space(5)
  w_CASC = space(1)
  w_ORDI = space(1)
  w_IMPE = space(1)
  w_RISE = space(1)
  w_2CASC = space(1)
  w_2ORDI = space(1)
  w_2IMPE = space(1)
  w_2RISE = space(1)
  w_LGDESCOD = space(41)
  w_LGDESSUP = space(41)
  w_IVACAU = space(5)
  w_FLFOCA = space(1)
  w_RIFORD = space(10)
  w_FLLOTT1 = space(1)
  w_FLLOTT2 = space(1)
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_OLDMAG = space(5)
  w_OLDMAT = space(5)
  w_LODATSCA = ctod("  /  /  ")
  w_LFLUBIC = space(1)
  w_LF2UBIC = space(1)
  w_DESDOC = space(50)
  w_MEMO = space(0)
  w_DESSUP = space(0)
  w_CAAUT = space(1)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_MLCODICE = space(20)
  w_CODMAG = space(5)
  w_QTATRA = 0
  w_QTALOT = 0
  w_QTAMOV = 0
  w_ORCASC = space(1)
  w_DTOBSOART = ctod("  /  /  ")
  w_RESOCON = space(0)
  w_RESOCON1 = space(0)
  w_TESTMESS = 0
  w_FLLMAG = space(1)
  w_ARTPCONF = space(3)
  w_ARTPCON2 = space(3)
  w_CATPCON3 = space(3)
  w_ARTIPCO1 = space(5)
  w_ARTIPCO2 = space(5)
  w_CATIPCO3 = space(5)
  w_CAUIMB = 0
  w_MESBLOK = space(0)
  w_RIFER = space(10)
  w_RIGA = 0
  w_QTAEV1 = 0
  w_QTAMOVLO = 0
  w_QTAARR = 0
  w_UNIMIS = space(3)
  w_LFLFRAZ = space(1)
  w_OKRIF = .f.
  w_FLGEFA = space(1)
  w_IVAINC = space(5)
  w_IVAIMB = space(5)
  w_IVATRA = space(5)
  w_IVABOL = space(5)
  w_DCFLGROR = space(1)
  w_OLDPOSTIN = .f.
  w_MODRIF = space(5)
  w_FLRINC = space(1)
  w_FLRIMB = space(1)
  w_FLRTRA = space(1)
  w_NAZPRO = space(3)
  w_OLDSPEINC = 0
  w_NOCOMM = .f.
  w_AGG_01 = space(15)
  w_AGG_02 = space(15)
  w_AGG_03 = space(15)
  w_AGG_04 = space(15)
  w_AGG_05 = ctod("  /  /  ")
  w_AGG_06 = ctod("  /  /  ")
  w_MAXROWNUM = 0
  w_ROWPRE = 0
  w_CONTRA = space(15)
  w_OPREZZO = 0
  w_PRSERAGG = space(10)
  w_PRSERIAL = space(10)
  w_OR2CASC = space(1)
  w_FL2RRIF = space(1)
  w_INDROW = 0
  w_INDSEA = space(10)
  w_DATOAI = ctod("  /  /  ")
  w_ARCODIVA = space(5)
  w_CATCON = space(5)
  w_ARPRESTA = space(1)
  w_MODUM2 = space(1)
  w_NOFRAZ = space(1)
  w_ARTPROL = space(1)
  w_TESTLIN = space(3)
  w_DESART = space(50)
  w_DESSUPL = space(0)
  w_MAGAZZINODIDEFAULT = space(1)
  w_ORIGVOCTIP = space(1)
  w_ARTIPOPE = space(10)
  w_OLDCODIVA = space(5)
  w_OLDPERIVA = 0
  w_OLDBOLIVA = space(1)
  w_oFLAPCA = space(1)
  w_RICPREZZO = .f.
  w_COEFF = 0
  w_TmpN = 0
  w_RICALCOLA = .f.
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod("  /  /  ")
  w_CF = ctod("  /  /  ")
  w_CV = space(5)
  w_IVACON = space(1)
  w_OKCONTR = .f.
  w_CALPRZ = 0
  w_PROG = space(3)
  w_QTAUM3 = 0
  w_OLDTIPPRO = space(1)
  w_OLDTIPPR2 = space(1)
  w_OTIPPRO = space(1)
  w_OTIPPR2 = space(1)
  w_OIMPPRO = 0
  w_OIMPCAP = 0
  w_OPERPRO = 0
  w_OPROCAP = 0
  w_OTTCODCOM = space(15)
  w_LDECCOM = 0
  w_DTOBSOVC = ctod("  /  /  ")
  w_DTOBSOCC = ctod("  /  /  ")
  w_DTOBSOCM = ctod("  /  /  ")
  w_oFLAPCA = space(1)
  w_LTOTQTA = 0
  w_LTOTEVA = 0
  w_LTOTMOV = 0
  w_LCODICE = space(20)
  w_LUNIMIS = space(3)
  w_LCODCEN = space(15)
  w_LCODCOM = space(15)
  w_LCODATT = space(15)
  w_LVISPRE = 0
  w_LSCONT1 = 0
  w_LSCONT2 = 0
  w_LSCONT3 = 0
  w_LSCONT4 = 0
  w_LSERIAL = space(10)
  w_LROWNUM = 0
  w_LNUMRIF = 0
  w_LQTAEVA = 0
  w_TESTRAG = .f.
  w_POS = 0
  w_Found = .f.
  w_QTATEST = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  BAN_CHE_idx=0
  CAM_AGAZ_idx=0
  CAN_TIER_idx=0
  COC_MAST_idx=0
  DES_DIVE_idx=0
  DOC_COLL_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  MODASPED_idx=0
  PAG_AMEN_idx=0
  PORTI_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  UNIMIS_idx=0
  VALUTE_idx=0
  VETTORI_idx=0
  VOCIIVA_idx=0
  VOC_COST_idx=0
  CONTI_idx=0
  MAGAZZIN_idx=0
  LOTTIART_idx=0
  SEDIAZIE_idx=0
  CON_TRAM_idx=0
  TRADARTI_idx=0
  ALT_DETT_idx=0
  DIPENDEN_idx=0
  CENCOST_idx=0
  PRA_CONT_idx=0
  DIC_INTE_idx=0
  PRE_STAZ_idx=0
  AGENTI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import da Documenti Collegati (da GSVE_MDV, GSAC_MDV, GSOR_MDV)
    * --- La Causale Magazzino viene letta solo la prima volta in testata in quanto ripete quella di Testata Documento
    * --- Aggiorna il Documento Destinazione dall'Import (da GSVE_MDV evento: Importa Doc)
    this.w_PADRE = this.oParentObject
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    if USED("RigheDoc")
      this.w_PADRE.MarkPos()     
      * --- Se w_RECTRS=0 allora non e' ancora stato aggiornato il Corpo del Documento, 
      * --- quindi i dati di testata del Primo Doc. Importato verranno inseriti nella Testata del Doc. Destinazione.
      * --- Conto le righe piene sul documento...
      this.w_RECTRS = this.w_PADRE.NumRow()
      if this.w_PADRE.cFunction="Load" AND this.oParentObject.w_GIAIMP<>"S"
        * --- Inizializza Spese Accessorie (Cumulabili)
        this.oParentObject.w_MVSCONTI = 0
        this.oParentObject.w_MVCAUIMB = 0
        * --- Setto la variabile AZZSPE a 0 in modo che in pagina 4 al primo documento dello
        *     stesso ciclo azzero le spese
        this.w_AZZSPE = 0
      else
        this.w_AZZSPE = 1
      endif
      * --- Se derivo da caricamento rapido ordino solo per MVSERIAL e CPROWNUM
      *     Nella normale importazione documenti Ordino anche per 
      *     Data, Numero e Alfa Documento
      Ordine = IIF(Type("RigheDoc.cMATR")<>"U"," MVSERIAL, CPROWORD"," MVDATDOC, MVNUMDOC, MVALFDOC, MVSERIAL, CPROWORD,MVRIFPRE")
      * --- Assegno ad una variabile L_.. poich� devo utilizzare la Macro con il nome del Cursore 
      *     per le righe importate a 0 da Evadere
      L_Cur_Ev_Row=this.oParentObject.w_Cur_Ev_Row
      * --- Assegno ad una variabile L_.. poich� devo utilizzare la Macro con il nome del Cursore 
      *     per le righe Raggruppate in Importazione 
      *     Creo il cursore contenente le righe raggruppate solo se non esiste gi� per precedente importazione
      L_Cur_Rag_Row=this.oParentObject.w_Cur_Rag_Row
      if Not Used( L_Cur_Rag_Row )
        CREATE CURSOR ( L_Cur_Rag_Row ) ( RESERRIF C(10), REROWRIF N(4), RENUMRIF N(3), ROWDES N(4), QTAEVA N(12,3) )
      endif
      * --- Scarica nel Cursore Ordinato...
       
 SELECT * FROM RigheDoc ; 
 WHERE ((XCHK=1 AND FLDOCU <>"S" And ( ( NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND (NVL(MVIMPEVA,0)<>0 or (this.oParentObject.w_FLGZER="S" and NVL(VISPRE,0)=0)) ) ) )OR ; 
 ( (NVL(OLQTAEVA,0)< NVL(MVQTAMOV,0)) AND NVL(MVQTAEVA,0)<>0 AND MVTIPRIG<>"F") OR ; 
 (MVTIPRIG="F" AND ( ((NVL(OLIMPEVA,0)<NVL(VISPRE,0) AND NVL(VISPRE,0) >0) OR ; 
 (NVL(OLIMPEVA,0)>NVL(VISPRE,0) AND NVL(VISPRE,0) <0) ) AND NVL(MVIMPEVA,0)<>0) OR ; 
 (MVTIPRIG="D" AND NVL(MVQTAEVA,0)<>0)) or (NVL(VISPRE,0)=0 and this.oParentObject.w_FLGZER="S")) AND CPROWNUM<>0 ; 
 INTO CURSOR RigheSort ORDER BY &Ordine
      if this.oParentObject.w_FLGZER <>"S" or !Isalt()
        * --- Creo il Cursore per le righe importate a 0 da Evadere
        if Used( L_Cur_Ev_Row ) and Reccount( L_Cur_Ev_Row ) >0
          * --- Se gi� presente il cursore L_Cur_Ev_Row per righe evase senza import dovuto a precedente importazione parziale,
          *     devo andare in append dello stesso con le nuove righe evase senza import
           
 SELECT MVSERIAL as RESERRIF, CPROWNUM as REROWRIF, -20 as RENUMRIF ; 
 FROM RigheDoc ; 
 WHERE (XCHK=1 AND FLDOCU <>"S") And ; 
 ( ( NVL(MVQTAEVA,0)=0 AND MVTIPRIG<>"F" And Nvl(OLQTAEVA,0)=0) OR ; 
 (MVTIPRIG="F" AND NVL(MVIMPEVA,0)=0 And Nvl(OLIMPEVA,0)=0) ); 
 into array Evadi
          if Type("Evadi")<>"U"
            * --- Dall'Array Evadi inserisco in Append le righe in L_Cur_Ev_Row
            *     
            *     Utilizzando questo tipo di copia da Array a cursore esiste un caso di errore se il 
            *     totale delle celle dell'Array � superiore a 65000
            *     In questo caso poich� i campi del cursore sono 4, bisognerebbe selezionare ed Evadere senza importazione 
            *     circa 16000 righe di documento
            insert into ( L_Cur_Ev_Row ) from array Evadi
          endif
        else
          * --- Prima pressione del tasto import con Evasione Righe senza Import
          *     Se non esiste ancora il cursore per righe evase Senza Import lo creo con una Select
           
 SELECT MVSERIAL as RESERRIF, CPROWNUM as REROWRIF, -20 as RENUMRIF ; 
 FROM RigheDoc ; 
 WHERE (XCHK=1 AND FLDOCU <>"S") And ; 
 ( ( NVL(MVQTAEVA,0)=0 AND MVTIPRIG<>"F" And Nvl(OLQTAEVA,0)=0) OR ; 
 (MVTIPRIG="F" AND NVL(MVIMPEVA,0)=0 And Nvl(OLIMPEVA,0)=0) ); 
 into cursor ( L_Cur_Ev_Row ) Order By 1, 2
          =WRCURSOR( L_Cur_Ev_Row )
          * --- Nel caso sia vuoto lo chiudo altrimenti in caso di una seconda importazione il cursore � gi� in uso
          if Reccount(L_Cur_Ev_Row)=0
            select ( L_Cur_Ev_Row )
            use
          endif
        endif
      endif
      this.w_TESTMESS = 0
      if Used( L_Cur_Ev_Row ) and Reccount( L_Cur_Ev_Row ) >0
        this.w_EVASER = "AAXXYY"
        * --- Eseguo la scan solo sui documenti che non ho gi� controllato
        Select( L_Cur_Ev_Row ) 
 Go top 
 Scan for Not Deleted()
        if RESERRIF <> this.w_EVASER
          this.w_EVASER = RESERRIF
          * --- Controllo se la riga che si sta evadendo senza import fa parte di un documento
          *     che ha check No Evasione nelle Origini.
          *     In questo caso non posso evadere la riga senza importazione.
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVDATDOC,MVNUMDOC,MVALFDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVDATDOC,MVNUMDOC,MVALFDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_EVASER;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EVADOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_EVADAT = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_NUMEVA = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFEVA = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from DOC_COLL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DCFLEVAS"+;
              " from "+i_cTable+" DOC_COLL where ";
                  +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                  +" and DCCOLLEG = "+cp_ToStrODBC(this.w_EVADOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DCFLEVAS;
              from (i_cTable) where;
                  DCCODICE = this.oParentObject.w_MVTIPDOC;
                  and DCCOLLEG = this.w_EVADOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NOEVA = NVL(cp_ToDate(_read_.DCFLEVAS),cp_NullValue(_read_.DCFLEVAS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_NOEVA="S"
            this.w_TESTMESS = 1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Evasione righe senza importazione da documento %1 n. %2 del %3 con check no evasione attivato%0Si considera valida l'impostazione della causale e la riga non sar� evasa")
            this.w_oPART.addParam(this.w_EVADOC)     
            this.w_oPART.addParam(Alltrim(Str(this.w_NUMEVA,15))+IIF(Not Empty(this.w_ALFEVA),"/","")+Alltrim(this.w_ALFEVA))     
            this.w_oPART.addParam(DTOC(this.w_EVADAT))     
            * --- Mi segno la posizione e cancello tutte le righe del documento Evase senza import
            *     Dopodich� mi riposiziono in quel punto poich� la delete for va in fondo al cursore
            *     La Scan for Not deleted() impedisce di ripassare sui record cancellati
            this.w_PUNTAT = Recno()
             
 Select ( L_Cur_Ev_Row ) 
 Delete For RESERRIF = this.w_EVASER 
 Goto this.w_PUNTAT
          endif
        endif
        Select( L_Cur_Ev_Row )
        Endscan
        * --- Elimino fisicamente i record cancellati dal cursore
        *     Attenzione: PACK Elimina i record settati come Deleted della Work area Selezionata
        PACK
        if Reccount( L_Cur_Ev_Row )=0
          * --- Se il cursore delle righe evase senza import dopo il Pack � vuoto lo elimino
           
 Select( L_Cur_Ev_Row ) 
 Use
        endif
      endif
      * --- Testa il Cambio Documento
      * --- w_OKRIG  .T.  se una riga non descrittiva del documento di origine �
      *                              correttamente inserita nel documento di destinazione
      *     w_NORIG .T.  se una riga non descrittiva del documento di origine 
      *                             nella quale � gestito lo split dei lotti non � inserita per
      *                             mancanza di relativi movimenti di carico lotti
      this.w_NORIG = .F.
      this.w_INDICE = 0
      * --- Array righe descrittive da eliminare
       
 DIMENSION Des_Row[1, 2] 
 Des_Row[1, 1] = Space(10) 
 Des_Row[1, 2] = 0
      this.w_MCAUCOL = SPACE(5)
      this.w_MFLCASC = " "
      this.w_MFLORDI = " "
      this.w_MFLIMPE = " "
      this.w_MFLRISE = " "
      this.w_MF2CASC = " "
      this.w_MF2ORDI = " "
      this.w_MF2IMPE = " "
      this.w_MF2RISE = " "
      this.w_MFLELGM = " "
      this.w_TFLCOMM = " "
      this.w_MFLAVAL = " "
      this.w_TCF = " "
      this.w_DATEVA = cp_CharToDate("  -  -  ")
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM,CMFLAVAL,CMVARVAL"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM,CMFLCOMM,CMFLAVAL,CMVARVAL;
          from (i_cTable) where;
              CMCODICE = this.oParentObject.w_MVTCAMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
        this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
        this.w_TFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
        this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
        this.w_FLAGG = NVL(cp_ToDate(_read_.CMVARVAL),cp_NullValue(_read_.CMVARVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CASC = this.w_MFLCASC
      this.w_ORDI = this.w_MFLORDI
      this.w_IMPE = this.w_MFLIMPE
      this.w_RISE = this.w_MFLRISE
      this.w_MFLCASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLCASC)
      this.w_MFLORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLORDI)
      this.w_MFLIMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLIMPE)
      this.w_MFLRISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLRISE)
      * --- Causale Contabile Collegata
      if NOT EMPTY(NVL(this.w_MCAUCOL, " "))
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MCAUCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL;
            from (i_cTable) where;
                CMCODICE = this.w_MCAUCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.w_FLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_2CASC = this.w_MF2CASC
      this.w_2ORDI = this.w_MF2ORDI
      this.w_2IMPE = this.w_MF2IMPE
      this.w_2RISE = this.w_MF2RISE
      this.w_MF2CASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2CASC)
      this.w_MF2ORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2ORDI)
      this.w_MF2IMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2IMPE)
      this.w_MF2RISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MF2RISE)
      * --- Articolo per Descrizioni
      this.w_ARTDES = SPACE(20)
      if NOT EMPTY(g_ARTDES)
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(g_ARTDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART;
            from (i_cTable) where;
                CACODICE = g_ARTDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTDES = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_NEWDOC = "zzzzzzzzzz"
      this.w_PRSERAGG = "zzzzzzzzzz"
      this.w_DCFLINTE = " "
      this.w_DCFLGROR = " "
      this.w_CNT = 0
      this.w_FLAPRE = .F.
      this.w_MCAORIF = 0
      this.w_OLDMAG = "#####"
      this.w_OLDMAT = "#####"
      this.w_OLDCODIVA = Space(10)
      * --- Ricalcolo provvigioni in importazione: dipende sempre da g_PROGEN su parametri provv
      *     RICPROV viene utilizzata in GSVE_BCD per ricalcolare o meno le provvigioni
      this.oParentObject.w_RICPROV = "N"
      if this.oParentObject.w_FLAGEN = "S" And g_PROGEN="S" And this.oParentObject.w_PRZDES = "S"
        * --- Visto che � presente il ricalcolo prezzi evito di rilanciare la CALPRZLI e quinindi il GSVE_BCD 
        *     lanciato da Notifyevent('Ricalcola') in pag 6 ricalcola da contratto
        this.oParentObject.w_RICPROV = "P"
      else
        * --- Se ho attivo solo il Genera Provvigioni sulla causale devo eseguire almeno 
        *     il ricalcolo da contratto in questo batch
        if this.oParentObject.w_FLAGEN = "S" And g_PROGEN="S"
          this.oParentObject.w_RICPROV = "S"
        endif
      endif
      SELECT RigheSort
      WRCURSOR("RigheSort")
      GO TOP
      SCAN FOR CPROWNUM<>0
      * --- Nuovo Documento
      this.w_ROWPRE = CPROWORD
      if this.w_NEWDOC<>MVSERIAL
        * --- w_APPOSER = Serial Documento usato nella Pagina 4
        this.w_INDROW = 0
        if Isalt() and this.w_PRSERAGG<>PRSERAGG
          DIMENSION Rif_Row(1, 2) 
 Rif_Row[1, 1] = 0 
 Rif_Row[1, 2] = 0
        endif
        this.w_APPOSER = MVSERIAL
        this.w_ROWPRE = CPROWORD
        this.w_LFLACCO = NVL(FLACCO," ")
        this.w_OKRIG = .F.
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT RigheSort
        this.w_NEWDOC = MVSERIAL
        * --- Se abilitato scrive riga Descrittiva Riferimenti
        if NOT EMPTY(g_ARTDES)
           
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_NRIF,15,0)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.w_ARIF) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_DRIF) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_AEST) 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]=DTOC(this.w_DEST) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.oParentObject.w_MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.oParentObject.w_MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= ALLTRIM(STR(this.w_NEST,15,0)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
          * --- Nostro Riferimento
          if this.oParentObject.w_FLNSRI="S"
            if this.w_FLVEAC="A" AND this.w_CLADOC<>"OR"
              if this.w_NEST<>0
                 
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(this.w_APPO) 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NEST,15,0))+IIF(EMPTY(this.w_AEST),"", "/"+Alltrim(this.w_AEST)) +SPACE(1)+DTOC(this.w_DEST) 
                this.w_APPART = CALRIFDES("nsd",this.oParentObject.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2)
                * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
                this.w_DESSUP = ""
                if len(ALLTRIM(this.w_APPART)) >40
                  * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
                  *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
                  *     dello spazio nei primi 30 caratteri.
                  *     Il resto viene inserito nella descrizione supplementare.
                  this.w_DESSUP = SUBSTR(this.w_APPART,(RATC(" ",LEFT(this.w_APPART,40))+1))
                  this.w_APPART = LEFT(this.w_APPART, RATC(" ", LEFT(this.w_APPART,40)))
                endif
                this.w_APPO = this.oParentObject.w_TPNSRI
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              if this.w_NRIF<>0
                 
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(this.w_APPO) 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NRIF,15,0))+IIF(EMPTY(this.w_ARIF),"", "/"+Alltrim(this.w_ARIF)) +SPACE(1)+DTOC(this.w_DRIF) 
                this.w_APPART = CALRIFDES("nsd",this.oParentObject.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2)
                * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
                this.w_DESSUP = ""
                if len(ALLTRIM(this.w_APPART))>40
                  * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
                  *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
                  *     dello spazio nei primi 30 caratteri.
                  *     Il resto viene inserito nella descrizione supplementare.
                  this.w_DESSUP = SUBSTR(this.w_APPART,(RATC(" ",LEFT(this.w_APPART,40))+1))
                  this.w_APPART = LEFT(this.w_APPART, RATC(" ", LEFT(this.w_APPART,40)))
                endif
                this.w_RIFER = this.w_APPART
                this.w_OKRIF = .T.
                this.w_APPO = this.oParentObject.w_TPNSRI
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
          * --- Vostro Riferimento
          if this.oParentObject.w_FLVSRI="S" 
            if this.w_FLVEAC="A" AND this.w_CLADOC<>"OR"
              if this.w_NRIF<>0
                 
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(this.w_APPO) 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NRIF,15,0))+IIF(EMPTY(this.w_ARIF),"", "/"+Alltrim(this.w_ARIF)) +SPACE(1)+DTOC(this.w_DRIF) 
                this.w_APPART = CALRIFDES("vsd",this.oParentObject.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2)
                * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
                this.w_DESSUP = ""
                if len(ALLTRIM(this.w_APPART))>40
                  * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
                  *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
                  *     dello spazio nei primi 30 caratteri.
                  *     Il resto viene inserito nella descrizione supplementare.
                  this.w_DESSUP = SUBSTR(this.w_APPART,(RATC(" ",LEFT(this.w_APPART,40))+1))
                  this.w_APPART = LEFT(this.w_APPART, RATC(" ", LEFT(this.w_APPART,40)))
                endif
                this.w_APPO = this.oParentObject.w_TPVSRI
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              if this.w_NEST<>0
                 
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]="N" 
 ARPARAM2[2]=ALLTRIM(STR(this.w_NEST,15,0))+IIF(EMPTY(this.w_AEST),"", "/"+Alltrim(this.w_AEST)) +SPACE(1)+DTOC(this.w_DEST) 
                this.w_APPART = CALRIFDES("vsd",this.oParentObject.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF, @ARPARAM2)
                * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
                this.w_DESSUP = ""
                if len(ALLTRIM(this.w_APPART))>40
                  * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
                  *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
                  *     dello spazio nei primi 30 caratteri.
                  *     Il resto viene inserito nella descrizione supplementare.
                  this.w_DESSUP = SUBSTR(this.w_APPART,(RATC(" ",LEFT(this.w_APPART,40))+1))
                  this.w_APPART = LEFT(this.w_APPART, RATC(" ", LEFT(this.w_APPART,40)))
                endif
                this.w_APPO = this.oParentObject.w_TPVSRI
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
          * --- Rif. Descrittivo
          if this.oParentObject.w_FLRIDE="S" 
            this.w_MEMO = IIF(NOT EMPTY(this.w_MEMO), "R.(" +ALLTRIM(this.w_DCCOLLEG)+") "+ this.w_MEMO, CALRIFDES("dsd",this.oParentObject.w_CODLIN, this.w_MODRIF, @ARPARAM, this.w_DESRIF,.F.) )
            if NOT EMPTY(this.w_MEMO)
              if this.w_CLADOC<>"OR"
                * --- Se il dato � stato inserito dai dati di testata nel trasferimento 
                *     del valore al relativo campo vengono eliminati gli spazi
                this.w_MEMO = this.w_MEMO+" "
              endif
              * --- sbianco la var perch� potrebbe essere sporca degli assegnamenti precedenti
              this.w_DESSUP = ""
              if len(ALLTRIM(this.w_MEMO)) >40
                * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
                *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
                *     dello spazio nei primi 30 caratteri.
                *     Il resto viene inserito nella descrizione supplementare.
                this.w_RIFCLI = LEFT(this.w_MEMO, RATC(" ", LEFT(this.w_MEMO,40)))
                this.w_DESSUP = SUBSTR(this.w_MEMO,(RATC(" ",LEFT(this.w_MEMO,40))+1))
              else
                * --- Se il riferimento � pi� corto di 30 caratteri, inserisco tutto nella descrizione
                this.w_RIFCLI = this.w_MEMO
              endif
              this.w_APPART = ALLTRIM(this.w_RIFCLI)
              this.w_APPO = this.oParentObject.w_TPRDES
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        SELECT RigheSort
        if Isalt() and Not empty(this.w_MEMO) and this.oParentObject.w_FLINOTE="S"
          this.oParentObject.w_MV__NOTE = Alltrim(this.oParentObject.w_MV__NOTE) + this.w_MEMO+ chr(13)
        endif
      endif
      if this.oParentObject.w_MVCLADOC="RF" AND EMPTY(this.w_CLIENTE)
        this.w_CLIENTE = this.w_RICON
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANCODFIS"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CLIENTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANCODFIS;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_MVTIPCON;
                and ANCODICE = this.w_CLIENTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RAGSOC = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          this.w_INDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
          this.w_CODCAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
          this.w_LOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
          this.w_PROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
          this.w_CODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_APPO6 = LEFT(this.w_RAGSOC+SPACE(40),40)+LEFT(this.w_INDIRI+SPACE(35),35)+LEFT(this.w_CODCAP+ SPACE(8), 8) +LEFT(this.w_LOCALI+SPACE(30), 30) +LEFT(this.w_PROVIN+SPACE(2), 2) +LEFT(this.w_CODFIS+SPACE(16), 16)
        this.w_RAGSOC = SUBSTR(this.w_APPO6, 1, 40)
        this.w_INDIRI = SUBSTR(this.w_APPO6, 41, 35)
        this.w_CODCAP = SUBSTR(this.w_APPO6, 76, 8)
        this.w_LOCALI = SUBSTR(this.w_APPO6, 81, 30)
        this.w_PROVIN = SUBSTR(this.w_APPO6, 111, 2)
        this.w_CODFIS = SUBSTR(this.w_APPO6, 113, 16)
        this.oParentObject.w_MV__NOTE = this.w_APPO6
      endif
      * --- Nuova Riga del Temporaneo
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SERRIF = MVSERIAL
      this.w_ROWRIF = CPROWNUM
      this.w_DATREG = this.oParentObject.w_MVDATREG
      this.w_RIFCAC = 0
      this.w_RIFCACORD = 0
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODLOT,MVCODART,MVQTAUM1,CPROWORD,MVQTAEV1,MVUNIMIS,MVQTAMOV,MVCODUBI,MVCODUB2,MVCODMAG,MVRIFCAC"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODLOT,MVCODART,MVQTAUM1,CPROWORD,MVQTAEV1,MVUNIMIS,MVQTAMOV,MVCODUBI,MVCODUB2,MVCODMAG,MVRIFCAC;
          from (i_cTable) where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
        this.w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
        this.w_QTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
        this.w_RIGA = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
        this.oParentObject.w_ORQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
        this.w_UNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
        this.w_QTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_CODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
        this.w_CODUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
        this.w_ORIMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        this.w_RIFCAC = NVL(cp_ToDate(_read_.MVRIFCAC),cp_NullValue(_read_.MVRIFCAC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLLOTT,ARUNMIS1,ARMAGPRE,ARFLLMAG"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLLOTT,ARUNMIS1,ARMAGPRE,ARFLLMAG;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAAUT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.oParentObject.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        this.w_FLLMAG = NVL(cp_ToDate(_read_.ARFLLMAG),cp_NullValue(_read_.ARFLLMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- U.M. Frazionabile
      if NOT EMPTY(NVL(this.w_UNIMIS," "))
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ,UMMODUM2"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ,UMMODUM2;
            from (i_cTable) where;
                UMCODICE = this.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LFLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- leggo campi dalla 1a UM
      if this.w_UNIMIS <>this.oParentObject.w_UNMIS1
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMMODUM2,UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMMODUM2,UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.oParentObject.w_UNMIS1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_QTALOT = 0
      this.w_RESIM1 = 0
      this.w_RESIMP = 0
      this.w_RAPUM = 0
      this.w_BCONTACC = .F.
      if this.w_CAAUT= "C" AND EMPTY(this.w_CODLOT) AND g_PERLOT="S" AND (this.w_CASC="-" OR this.w_RISE="+") AND this.oParentObject.w_MVFLPROV<>"S" And Type("RigheSort.cMATR")="U"
        * --- Ricalcolo la quantit� 1 U.M. effettivamente evasa in base al rapporto 
        *     nel documento di origine
        if this.w_UNIMIS <>this.oParentObject.w_UNMIS1
          * --- Ricalcolo quantit� specificata nello zoom come qta evasa nella 1 U.M.
          this.w_QTAEV1 = (this.w_QTAUM1*NVL(MVQTAEVA,0))/this.w_QTAMOV
          this.w_QTATRA = IIF(this.w_QTAUM1=this.w_QTAEV1,this.w_QTAUM1-this.oParentObject.w_ORQTAEV1,this.w_QTAEV1)
          this.w_QTAIMP = (this.w_QTAUM1*MIN(NVL(MVQTAEVA,0),(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))))/this.w_QTAMOV
          this.w_RESIM1 = (this.w_QTAUM1*(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0)))/this.w_QTAMOV
          this.w_RAPUM = IIF(this.w_QTAMOV>0 AND this.w_LFLFRAZ="S",this.w_QTAUM1/this.w_QTAMOV,0)
        else
          this.w_QTATRA = NVL(MVQTAEVA,0)
          this.w_RESIM1 = (NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))
          this.w_QTAIMP = MIN(NVL(MVQTAEVA,0),(NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0)))
        endif
        this.w_RESIMP = (NVL(MVQTAMOV,0)-NVL(OLQTAEVA,0))
        * --- Variabili Utilizzate:
        *     w_IMPLOT: quantit� importata nelle righe dello split dei lotti
        *     w_TOTIMP: totale qta importata utilizzata per calcolare proporzionalmente la qta importata
        *     w_TOTMOV: totale quantit� utilizzabile dei lotti associati all'articolo in analisi decrementata a ogni iterazione
        *     w_RESIDUO: residuo decimale della qta importata calcolata proporzionalmente da assegnare all'ultima riga dello split
        *     w_TOTEFF: quantit� totale effettivamente utilizzata MIN(w_QTATRA,w_TOTEFF)
        *     w_QTAIMP: totale quantit� importata decrementata ad ogni iterazione
        *     w_QTATRA: totale quanti� trasferibile decrementata ad ogni iterazione 
        *     w_QTALOT: qta movimentata di riga del lotto
        *     w_RESIM1: qta residua da evadere 
        *     w_RAPUM: rapporto tra le unit� di misura
        this.w_IMPLOT = 0
        this.w_TOTEFF = 0
        this.w_TOTMOV = 0
        this.w_TOTIMP = 0
        this.w_RESIDUO = 0
        if this.w_QTATRA>0
          * --- Magazzino Utilizzato Come Filtro
          if this.w_FLLMAG<>"S"
            * --- Se non forza magazzino lotti,  eseguo filtro su query di ricerca lotti
            *     e mantengo magazzino di destinazione
            this.w_CODMAG = IIF(NOT EMPTY(this.w_CODMAG), this.w_CODMAG, CALCMAG(2, this.oParentObject.w_FLMGPR, this.w_ORIMAG, this.oParentObject.w_COMAG, this.oParentObject.w_OMAG, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER))
          endif
          if Not Empty(this.oParentObject.w_MVCAUMAG)
            this.w_OKSCA = "F"
            this.w_FLCAUM = Left(Alltrim(this.w_MFLCASC)+IIF(this.w_MFLRISE="+", "-", IIF(this.w_MFLRISE="-", "+", " ")),1)
            this.w_OKSCA = IIF((this.oParentObject.w_FLAVA1="A" and this.w_FLCAUM="-") or (this.oParentObject.w_FLAVA1="V" and this.w_FLCAUM="+") ,"T","F")
          endif
          if Not Empty(this.w_MCAUCOL) AND this.w_OKSCA="F"
            this.w_FLCAUM = Left(Alltrim(this.w_MF2CASC)+IIF(this.w_MF2RISE="+", "-", IIF(this.w_MF2RISE="-", "+", " ")),1)
            this.w_OKSCA = IIF((this.w_FLAVAL="A" and this.w_FLCAUM="-") or (this.w_FLAVAL="V" and w_MFLCAUM="+") ,"T","F")
          endif
          * --- Eseguo una volta la query per determinare i lotti disponibili
          this.w_PADRE.Exec_Select("Appo","t_MVCODART, t_MVCODUBI,t_MVCODLOT,SUM(NVL(t_MVQTAUM1,0)-NVL(MVQTAUM1,0)) AS t_MVQTAUM1","t_MVCODART="+cp_ToStr(nvl(this.w_CODART,space(15))) ,"t_MVCODART,t_MVCODLOT,t_MVCODUBI","t_MVCODART,t_MVCODLOT,t_MVCODUBI","")     
          vq_exec("..\MADV\EXE\QUERY\GSMD_QCA.VQR",this,"LOTTI")
           
 Select LOCODICE,LOCODUBI,LODATCRE,LODATSCA,(LOQTAESI-NVL(t_MVQTAUM1,0)) As LOQTAESI,CODMAG,CODART,UTDC; 
 From LOTTI Left outer Join APPO On (APPO.t_MVCODART=LOTTI.CODART And APPO.t_MVCODUBI=LOTTI.LOCODUBI ; 
 And APPO.t_MVCODLOT=LOTTI.LOCODICE) Into Cursor Lotti NoFilter
           
 Select Appo 
 Use 
 Select Lotti 
 Sum LOQTAESI To this.w_TOTEFF For LOQTAESI>=this.w_RAPUM 
 
          * --- Quantit� effettiva su cui eseguire proporzione quantit� importata
          this.w_TOTMOV = this.w_TOTEFF
          this.w_TOTIMP = this.w_QTAIMP
          this.w_TOTEFF = MIN(this.w_TOTEFF,this.w_QTATRA)
          =WRCURSOR("LOTTI")
          this.w_OKRIG = .F.
          this.w_QTALOT = 0
          do while this.w_QTATRA>0
            this.w_CODLOT = CERCLOT(this.w_DATREG,this.w_CODART,"LOTTI")
            if NOT EMPTY(this.w_CODLOT)
              SELECT LOTTI
              GO TOP
              LOCATE FOR this.w_CODART=NVL(CODART,SPACE(20)) AND this.w_CODLOT=NVL(LOTTI.LOCODICE,SPACE(20))
              if FOUND()
                * --- Assegno informazioni della riga documento  che variano 
                this.w_OKDEC = .T.
                this.w_QTALOT = NVL(LOTTI.LOQTAESI,0)
                this.w_CODLOT = NVL(LOTTI.LOCODICE,SPACE(20))
                this.w_CODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
                this.w_CODMAG = NVL(LOTTI.CODMAG,SPACE(5))
                if this.w_QTATRA>this.w_QTALOT
                  * --- Consumo per intero la quantit� del lotto se unit� di misura frazionabile
                  * --- Ricalcolo in proporzione la quantit� da memorizzare in MVQTAIMP
                  this.w_IMPLOT = IIF(this.w_TOTIMP>this.w_TOTMOV,this.w_QTALOT,cp_ROUND((this.w_TOTIMP*this.w_QTALOT)/this.w_TOTEFF,3))
                  if this.w_LFLFRAZ="S"
                    * --- Se l'unit� di misura movimentata  � non frazionabile posso creare la riga del documento 
                    *     solo per la parte intera della quantit� movimentata
                    this.w_RESIDUO = this.w_RESIDUO+ (this.w_IMPLOT-INT(this.w_IMPLOT))
                    if this.w_UNIMIS <>this.oParentObject.w_UNMIS1
                      * --- Variabili Utilizzate:
                      *     w_QTAMOVLO: qta del lotto espressa nella U.M. movimentata
                      *     w_QTAIMPLO:  qta importata  del lotto espressa nella U.M. movimentata
                      this.w_QTAMOVLO = (this.w_QTALOT*this.w_QTAMOV)/this.w_QTAUM1
                      this.w_QTAIMPLO = (this.w_IMPLOT*this.w_QTAMOV)/this.w_QTAUM1
                      this.w_QTALOT = (INT(this.w_QTAMOVLO)*this.w_QTAUM1)/this.w_QTAMOV
                      this.w_IMPLOT = (INT(this.w_QTAIMPLO)*this.w_QTAUM1)/this.w_QTAMOV
                    else
                      this.w_QTALOT = INT(this.w_QTALOT)
                      this.w_IMPLOT = INT(this.w_IMPLOT)
                    endif
                    if (INT(this.w_QTALOT)>0 and this.w_IMPLOT>0 ) or (this.w_IMPLOT>0 and this.w_NOFRAZ<>"S")
                      * --- Se la parte intera della qta ricalcolata � >0 decremento contatore quantit� importata
                      this.w_OKDEC = .T.
                    else
                      * --- Se la parte intera della qta ricalcolata � =0 non ho consumato il lotto ma devo eliminarlo dal 
                      *     cursore LOTTI per non essere riconsiderato nello split della riga corrente.
                      this.w_OKDEC = .F.
                    endif
                  else
                    * --- U.M. frazionabile devo decrementare contatatore qta importata
                    this.w_OKDEC = .T.
                  endif
                  * --- Elimino il lotto per intero
                  SELECT LOTTI
                  DELETE
                  if this.w_OKDEC
                    * --- Decremento Totalizzatori
                    this.w_TOTMOV = this.w_TOTMOV-this.w_QTALOT
                    this.w_QTATRA = this.w_QTATRA-this.w_QTALOT
                    this.w_RESIM1 = this.w_RESIM1-this.w_IMPLOT
                    if this.w_RESIM1<0
                      * --- Riassegno qta importata dell' ultima riga se differenze di arrotondamento
                      this.w_IMPLOT = this.w_IMPLOT- ABS(this.w_RESIM1)
                      this.w_RESIM1 = 0
                    endif
                    this.w_QTAIMP = this.w_QTAIMP-this.w_IMPLOT
                    this.w_OKRIF = IIF(this.w_QTATRA=0,.F.,.T.)
                  endif
                  if this.w_RESIDUO>0 AND this.w_TOTMOV=0
                    * --- Se esiste residuo e sono nell'ultima riga possibile aggiungo residuo qta importata
                    this.w_IMPLOT = INT(this.w_IMPLOT+this.w_RESIDUO)
                    this.w_RESIDUO = 0
                  endif
                else
                  * --- Consumo in parte la quantit� del lotto quindi creo una riga pari
                  *     alla quantit� del documento di origine
                  this.w_QTALOT = this.w_QTATRA
                  this.w_IMPLOT = this.w_QTAIMP
                  this.w_RESIM1 = this.w_RESIM1-this.w_IMPLOT
                  this.w_QTATRA = 0
                  this.w_QTAIMP = 0
                endif
                if this.w_QTALOT>0 AND this.w_IMPLOT>0
                  * --- Creo riga del lotto se quantit� significativa
                  this.oParentObject.w_FLLOTT = this.w_CAAUT
                  this.Page_6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              this.w_OKRIG = .T.
            else
              * --- Creo Riga con lotto vuoto
              this.w_NORIG = .T.
              this.w_CODLOT = SPACE(20)
              this.w_CODUBI = SPACE(20)
              this.w_CODMAG = SPACE(5)
              this.w_oPART = this.w_oMESS.addmsgpart("%1")
              this.w_oPART.addParam(ALLTRIM(iif(this.w_OKRIF,CHR(13)+this.w_RIFER+ CHR(13)," ")))     
              this.w_oPART = this.w_oMESS.addmsgpartNL("Riga n. %1 articolo: %2 quantit� 1� U.M. mancante: %3")
              this.w_oPART.addParam(alltrim(str(this.w_RIGA)))     
              this.w_oPART.addParam(alltrim(this.w_CODART))     
              this.w_oPART.addParam(alltrim(this.oParentObject.w_UNMIS1)+" "+alltrim(str(this.w_QTATRA)))     
              this.w_QTATRA = 0
              this.w_OKRIF = .F.
            endif
          enddo
        endif
      else
        this.w_OKRIG = .T.
        if Not MVTIPRIG$"DF" And this.w_DCFLGROR="S"
          * --- Se riga non descrittiva e attivo Check Raggruppa nelle Origini di questo documento,
          *     attivabile solo con check No Evasione, raggruppo gli articoli congruenti dei documenti selezionati
          *     Non � possibile eseguire si Split dei Lotti che raggruppamento articoli (poich� sono in anttesi tra di loro)
          *     sulla causale documento � controllato il check Raggruppa
          *     Se la causale magazzino diminuisce l'esistenza o aumenta il riservato non � attivabile il flag.
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if g_COAC="S" And this.w_RIFCAC>0
            * --- RIFERIMENTO CONTRIBUTO ACCESSORI
            *     La logica � la stessa del KIT, in questo caso l'eplosione � avvenuta a livello di documento di origine
            *     (se ho ordine che esplode, DDT che non esplode ma importa e fattura che esplode, cmq
            *     mantengo il legame
            this.oParentObject.w_MVSERRIF = MVSERIAL
            this.oParentObject.w_MVROWRIF = CPROWNUM
            * --- Non deve considerare chiusa la riga (se qtalot>0)
            * --- Vado a ricercare un articolo di riferimento tra quelli attualmente in
            *     importazione
            this.w_PADRE.Exec_Select("TmpKit", "Curs1.i_RECNO As I_RECNO_,CPROWNUM, t_CPROWORD,t_MVCODICE, t_MVUNIMIS, t_FLFRAZ, t_PERIVA,t_MVCODIVA,t_MVQTAMOV, t_MVQTAIMP,t_MVVALMAG, t_ARFLAPCA" , "t_MVSERRIF="+ cp_ToStr(this.oParentObject.w_MVSERRIF)+" And t_MVROWRIF="+ cp_ToStr(this.w_RIFCAC)+" And CPROWNUM>="+Cp_ToStr( this.w_MAXROWNUM ) ,"I_RECNO_","","")     
            * --- A causa del consumo automatico del lotto vado a legare le righe di contributo
            *     evase in base alle quantit� definite sul padre...
             
 Select TmpKit 
 Go Top
            do while not Eof( "TmpKit" ) 
              if Nvl(TmpKit.I_RECNO_, 0 )>0
                * --- Riga attuale (contributo) ha un padre...
                this.w_BCONTACC = .T.
                this.w_RIFCAC = Nvl( TmpKit.CPROWNUM , 0 )
                this.w_RIFCACORD = Nvl( TmpKit.t_CPROWORD , 0 )
                this.oParentObject.w_ARPUNPAD = .t.
                * --- Vado a recuperare la riga numero del padre...
                if this.w_oFLAPCA<>"S"
                  * --- Memorizzo la riga attuale, mi sposto sulal riga padre, aggiorno per non esplodere e
                  *     ritorno nella riga attuale...
                  this.w_ROWINDEX = this.w_PADRE.RowIndex()
                  this.w_PADRE.SetRow(TmpKit.i_RECNO_)     
                  * --- Se il documento di origine non esplode, altrimenti il calcolo l'ho gia fatto...
                  *     In fase di import, se trovo un contributo accessorio aggiorno il valore
                  *     per gestire l'esplosione automatica dei contributi accessori.
                  *     Se trovo un contributo il padre non lo esplodo pi� a meno di modifiche
                  this.w_PADRE.Set("w_TESTESPLACC" , PADR(TmpKit.T_MVCODICE,41," ") + PADR(this.oParentObject.w_MVTIPCON,1," ")+PADR(iif( empty( this.oParentObject.w_MVCODORN ) , this.oParentObject.w_MVCODCON , this.oParentObject.w_MVCODORN),15," ")+PADR(YEAR(this.oParentObject.w_MVDATDOC),4," ")+PADR(MONTH(this.oParentObject.w_MVDATDOC),2," ")+PADR(DAY(this.oParentObject.w_MVDATDOC),2," ")+PADR(TmpKit.T_MVUNIMIS,3," ") + PADR(this.oParentObject.w_MVCODVAL,3," ") + PADR(TmpKit.T_FLFRAZ,1," ") + PADR(STR(TmpKit.T_PERIVA,6,2),6," ")+ PADR(TmpKit.T_MVCODIVA,5," ") + PADR(STR(TmpKit.T_MVQTAMOV,18, g_PERPQT ),18," ") + PADR(STR(IIF(TmpKit.T_MVVALMAG=0,0,TmpKit.T_MVVALMAG/TmpKit.T_MVQTAMOV),18, this.oParentObject.w_DECUNI ),18," ") + PADR(this.oParentObject.w_TDFLAPCA,1," ") + PADR(this.oParentObject.w_ANFLAPCA,1," ") + PADR(TmpKit.T_ARFLAPCA,1," ") , .t. , .f.)     
                  this.w_PADRE.SetRow(this.w_ROWINDEX)     
                endif
                * --- La quantit� movimentata � sempre quella del padre sul documento...
                this.w_QTALOT = 0
                this.w_IMPLOT = 0
                this.oParentObject.w_UNMIS1 = LEFT(TmpKit.t_MVUNIMIS+SPACE(3), 3)
                * --- Per calcolare il numero di riga corretto eseguo una count sul transitorio 
                *     di tutti i contributi presenti con lo stesso MVRIFCAC
                this.w_PADRE.Exec_Select("TmpKitRow", "Count(*) As Conta" , "t_MVRIFCAC="+ cp_ToStr(this.w_RIFCAC) ,"","","")     
                this.w_RIFCACORD = this.w_RIFCACORD + Nvl( TmpKitRow.Conta , 0 ) + 1
                Use in TmpKitRow
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              Select TmpKit 
 Skip
            enddo
            * --- Rimuovo il cursore
            Use in TmpKit
            * --- Se non trovo padri del contributo lo importo cmq...
            if Not this.w_BCONTACC
              * --- Assegnamento di sicurezza (deve comportarsi come una riga normale...)
              this.w_QTALOT = 0
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      * --- Elimino Riga Descrittiva Dall'array
      if this.w_OKRIG AND g_PERLOT="S" AND this.w_INDICE>0
        this.w_LOOP = 0
        do while this.w_LOOP <this.w_INDICE
          this.w_LOOP = this.w_LOOP + 1
          if Not EMpty(Nvl(Des_Row[ this.w_LOOP , 1] ," ")) and this.oParentObject.w_MVSERRIF=Des_Row[ this.w_LOOP , 1] 
             
 Des_Row[ this.w_LOOP, 1] = SPACE(10) 
 Des_Row[ this.w_LOOP ,2] = 0
          endif
        enddo
      endif
      * --- Crea Dettaglio Matricole se Gestite
      if g_MATR="S" And this.oParentObject.w_GESMAT="S" And this.oParentObject.w_MVDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    ")) And this.oParentObject.w_MVQTAMOV<>0 And this.oParentObject.w_MVFLCASC $ "+-" 
        this.w_FIRSTLAP = .T.
        * --- Nel caso di import da caricamento rapido vince questo rispetto alla query sulle
        *     matricole del documento di origine
        if Type("RigheSort.cMATR")<>"U"
          ah_Msg("Inserimento matricole riga: %1" ,.T., .F.,.F., ALLTRIM(STR(this.oParentObject.w_CPROWORD)) )
          GSAR_BDM(this, this.oParentObject.w_MVCODICE , this.oParentObject.w_MVCODLOT , this.oParentObject.w_MVCODUBI , this.oParentObject.w_MVCODMAG , this.oParentObject.w_MVPREZZO , this.oParentObject.w_MVUNIMIS , this.oParentObject.w_MVSERRIF , this.oParentObject.w_MVROWRIF , this.oParentObject.w_MVCODCEN , this.oParentObject.w_MVCODCOM , this.oParentObject.w_MVCODATT, this.oParentObject.w_MVCODMAT, this.oParentObject.w_MVCODUB2, this.oParentObject.w_MVUNILOG, this.w_PADRE, L_GSAR_MPG_TRSNAME)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if (this.w_LFLRRIF="+" OR this.w_ORCASC="-" ) and this.oParentObject.w_MVTIPRIG="R"
            ah_Msg("Inserimento matricole riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.oParentObject.w_CPROWORD)) )
            GSAR_BIM(this,this.w_PADRE, this.oParentObject.w_MVSERRIF, this.oParentObject.w_MVROWRIF)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      SELECT RigheSort
      ENDSCAN
      WAIT CLEAR
      * --- Chiude i Cursori
      SELECT RigheDoc
      USE
      SELECT RigheSort
      USE
      if USED("APPCUR")
        SELECT APPCUR
        USE
      endif
      if USED("LOTTI")
        SELECT LOTTI
        USE
      endif
      if this.w_NORIG AND g_PERLOT="S" AND this.w_INDICE>0
        * --- Elimino Righe Descrittive
        this.w_LOOP = 0
        do while this.w_LOOP < this.w_INDICE
          this.w_LOOP = this.w_LOOP + 1
          * --- Ricerco la riga..
          this.w_NUMREC = this.w_PADRE.Search( "CPROWNUM=Nvl("+cp_ToStrODBC(Des_Row[ this.w_LOOP , 2])+",0) And Not Deleted()",0 )
          * --- Se la trovo la elimino
          if this.w_NUMREC>=0
            * --- Mi posiziono sulla riga e la cancello...
            this.w_PADRE.SetRow(this.w_NUMREC)     
            this.w_PADRE.DeleteRow()     
          endif
        enddo
      endif
      * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
      this.w_PADRE.RePos(.F.)     
      * --- Ripristino calcolo provvigioni nel caso normale di caricamento manuale righe
      this.oParentObject.w_RICPROV = "C"
      this.w_RESOCON = this.w_oMESS.ComposeMessage()
      if NOT EMPTY(this.w_RESOCON)
        * --- ANNULLA: poich� nella maschera � definita come caller. In questo caso inutilizzata
        *     Gestita nel GSVE_BI2
        if this.w_TESTMESS=0
          this.w_RESOCON1 = ah_Msgformat("Elenco righe documento sulle quali non � disponibile il lotto:%0%1", ALLTRIM(this.w_RESOCON) )
        else
          this.w_RESOCON1 = ALLTRIM(this.w_RESOCON)
        endif
        this.w_RESOCON = SPACE(10)
        do GSVE_KLG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_FLAPRE=.T.
        this.w_MESS = "Sul documento importato � presente un acconto%0Assicurarsi che non sia gi� stato considerato in precedenti importazioni"
        ah_ErrorMsg(this.w_MESS)
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Campi e Variabili
    this.oParentObject.w_CPROWNUM = 0
    this.oParentObject.w_MVNUMRIF = 0
    this.oParentObject.w_MVIMPPRO = 0
    this.oParentObject.w_MVIMPCAP = 0
    this.oParentObject.w_QTAPER = 0
    this.oParentObject.w_CPROWORD = 0
    this.oParentObject.w_MVFLOMAG = " "
    this.oParentObject.w_MVNUMCOL = 0
    this.oParentObject.w_QTRPER = 0
    this.oParentObject.w_MVCODICE = SPACE(20)
    this.oParentObject.w_MVDESSUP = SPACE(10)
    this.oParentObject.w_Q2APER = 0
    this.oParentObject.w_MVDESART = SPACE(40)
    this.oParentObject.w_MVCODIVA = SPACE(5)
    this.oParentObject.w_MVFLTRAS = " "
    this.oParentObject.w_Q2RPER = 0
    this.oParentObject.w_MVCODART = SPACE(20)
    this.oParentObject.w_ARCODART = SPACE(20)
    this.oParentObject.w_MVCODCLA = SPACE(3)
    this.oParentObject.w_MVFLELGM = " "
    this.oParentObject.w_FLFRAZ = " "
    this.oParentObject.w_MVTIPRIG = " "
    this.oParentObject.w_MVCATCON = SPACE(5)
    this.oParentObject.w_UNMIS1 = SPACE(3)
    this.oParentObject.w_MVQTAMOV = 0
    this.oParentObject.w_MVQTAUM1 = 0
    this.oParentObject.w_MVQTAIMP = 0
    this.oParentObject.w_MVQTAIM1 = 0
    this.oParentObject.w_MVCONTRO = SPACE(15)
    this.oParentObject.w_UNMIS2 = SPACE(3)
    this.oParentObject.w_MVUNIMIS = SPACE(3)
    this.oParentObject.w_MVNOMENC = SPACE(8)
    this.oParentObject.w_UNMIS3 = SPACE(3)
    if VARTYPE( this.oParentObject.w_PRESTA ) <> "U"
      * --- w_PRESTA non � presente in tutte le gestioni
      this.oParentObject.w_PRESTA = ""
    endif
    this.oParentObject.w_MVVALMAG = 0
    this.oParentObject.w_MVSERRIF = SPACE(10)
    this.oParentObject.w_STSERRIF = SPACE(10)
    this.oParentObject.w_MVUMSUPP = SPACE(3)
    this.oParentObject.w_OPERAT = " "
    this.oParentObject.w_MVIMPNAZ = 0
    this.oParentObject.w_MVROWRIF = 0
    this.oParentObject.w_MVMOLSUP = 0
    this.oParentObject.w_OPERA3 = " "
    this.oParentObject.w_RIGNET = 0
    this.oParentObject.w_MVFLERIF = " "
    this.oParentObject.w_MVPESNET = 0
    this.oParentObject.w_MOLTIP = 0
    this.oParentObject.w_MVPREZZO = 0
    this.oParentObject.w_MVCODLIS = SPACE(5)
    this.oParentObject.w_MOLTI3 = 0
    this.oParentObject.w_MVVALRIG = 0
    this.oParentObject.w_MVIMPACC = 0
    this.oParentObject.w_MVIMPSCO = 0
    this.oParentObject.w_MVSCONT1 = 0
    this.oParentObject.w_MVCODCOM = SPACE(15)
    this.oParentObject.w_MVSCONT2 = 0
    this.oParentObject.w_MVFLRAGG = " "
    this.oParentObject.w_MVSCONT3 = 0
    this.oParentObject.w_MVPERPRO = 0
    this.oParentObject.w_MVTIPPRO = SPACE(2)
    this.oParentObject.w_MVTIPPR2 = SPACE(2)
    this.oParentObject.w_MVPROCAP = 0
    this.oParentObject.w_FGMRIF = " "
    this.oParentObject.w_MVSCONT4 = 0
    this.oParentObject.w_MVCONIND = SPACE(15)
    this.oParentObject.w_ARTDIS = " "
    this.oParentObject.w_ULTCAR = cp_CharToDate("  -  -  ")
    this.oParentObject.w_MVQTASAL = 0
    this.oParentObject.w_FLDISP = " "
    this.oParentObject.w_ULTSCA = cp_CharToDate("  -  -  ")
    this.oParentObject.w_MVVALULT = 0
    this.oParentObject.w_FLUSEP = " "
    this.oParentObject.w_CLARIF = "  "
    this.oParentObject.w_ODLRIF = SPACE(10)
    this.w_CODMAG = SPACE(5)
    this.oParentObject.w_DOIMPEVA = 0
    this.oParentObject.w_DOQTAEV1 = 0
    this.oParentObject.w_MVCODCES = SPACE(20)
    this.w_ARTPCONF = Space(3)
    this.w_ARTPCON2 = Space(3)
    this.w_CATPCON3 = Space(3)
    this.w_ARTIPCO1 = Space(5)
    this.w_ARTIPCO2 = Space(5)
    this.w_CATIPCO3 = Space(5)
    this.oParentObject.w_MVPAEFOR = SPACE(3)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive riga Riferimenti
    * --- Nuova Riga del Temporaneo
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiungo una riga sul Temporaneo
    this.w_PADRE.AddRow()     
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    if NOT EMPTY(this.oParentObject.w_MVDATEVA) OR this.oParentObject.w_MVCLADOC="OR"
      if NOT EMPTY(this.w_DATEVA)
        if this.w_DATEVA < this.oParentObject.w_MVDATDOC
          this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC
        else
          this.oParentObject.w_MVDATEVA = this.w_DATEVA
        endif
      endif
      if EMPTY(this.w_DATEVA) AND this.oParentObject.w_MVCLADOC="OR"
        this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC
      endif
    endif
    if g_PERLOT="S"
      if this.w_INDICE=0
         
 Des_Row[1, 1] = this.w_NEWDOC 
 Des_Row[1, 2] = this.oParentObject.w_CPROWNUM
        this.w_INDICE = 1
      else
        this.w_INDICE = this.w_INDICE +1
         
 DIMENSION Des_Row[this.w_INDICE, 2] 
 Des_Row[this.w_INDICE, 1] = this.w_NEWDOC 
 Des_Row[this.w_INDICE, 2] = this.oParentObject.w_CPROWNUM
      endif
    endif
    this.oParentObject.w_MVCODICE = g_ARTDES
    this.oParentObject.w_MVTIPRIG = "D"
    this.oParentObject.w_MVCODART = this.w_ARTDES
    this.oParentObject.w_ARCODART = this.w_ARTDES
    this.oParentObject.w_MVDESART = this.w_APPART
    this.oParentObject.w_MVDESSUP = this.w_DESSUP
    this.oParentObject.w_MVFLRAGG = this.oParentObject.w_MVTFRAGG
    this.oParentObject.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.oParentObject.w_MVCODMAG = SPACE(5)
    this.oParentObject.w_MVCODMAT = SPACE(5)
    this.oParentObject.w_MVCAUCOL = this.w_MCAUCOL
    this.oParentObject.w_MVFLCASC = this.w_MFLCASC
    this.oParentObject.w_MVFLORDI = this.w_MFLORDI
    this.oParentObject.w_MVFLIMPE = this.w_MFLIMPE
    this.oParentObject.w_MVFLRISE = this.w_MFLRISE
    this.oParentObject.w_MVF2CASC = this.w_MF2CASC
    this.oParentObject.w_MVF2ORDI = this.w_MF2ORDI
    this.oParentObject.w_MVF2IMPE = this.w_MF2IMPE
    this.oParentObject.w_MVF2RISE = this.w_MF2RISE
    this.oParentObject.w_MVCODCOM = SPACE(15)
    this.oParentObject.w_MVINICOM = cp_CharToDate("  -  -  ")
    this.oParentObject.w_MVFINCOM = cp_CharToDate("  -  -  ")
    this.oParentObject.w_MVKEYSAL = SPACE(20)
    this.oParentObject.w_MVCODCLA = this.w_APPO
    this.oParentObject.w_MVPREZZO = 0
    this.oParentObject.w_LIPREZZO = 0
    this.oParentObject.w_MVQTAMOV = 0
    this.oParentObject.w_MVQTAUM1 = 0
    this.oParentObject.w_MVQTAIMP = 0
    this.oParentObject.w_MVQTAIM1 = 0
    this.oParentObject.w_MVVALRIG = 0
    this.oParentObject.w_MVVALMAG = 0
    this.oParentObject.w_MVIMPNAZ = 0
    this.oParentObject.w_MVVALULT = 0
    this.oParentObject.w_MVFLULPV = " "
    this.oParentObject.w_MVIMPCOM = 0
    this.oParentObject.w_CLARIF = "  "
    this.oParentObject.w_ODLRIF = SPACE(10)
    this.oParentObject.w_MVCODRES = ""
    if VARTYPE( this.oParentObject.w_COGNOME ) <> "U"
      this.oParentObject.w_COGNOME = space(40)
    endif
    if VARTYPE( this.oParentObject.w_NOME ) <> "U"
      this.oParentObject.w_NOME = space(40)
    endif
    if VARTYPE( this.oParentObject.w_DENOM ) <> "U"
      this.oParentObject.w_DENOM = space(80)
    endif
    if VARTYPE( this.oParentObject.w_COST_ORA ) <> "U"
      this.oParentObject.w_COST_ORA = 0
    endif
    if VARTYPE( this.oParentObject.w_MVDATOAI ) <> "U"
      * --- w_MVDATOAI non � presente in tutte le gestioni
      this.oParentObject.w_MVDATOAI = cp_CharToDate("  -  -  ")
    endif
    * --- Carica il Temporaneo
    this.w_PADRE.SaveRow()     
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Testata Documento di Origine
    * --- Inizializza dati Testata
    this.w_FLVEAC = " "
    this.w_NRIF = 0
    this.w_ARIF = Space(10)
    this.w_DRIF = cp_CharToDate("  -  -  ")
    this.w_NEST = 0
    this.w_AEST = Space(10)
    this.w_RIFCLI = SPACE(30)
    this.w_MEMO = SPACE(255)
    this.w_DESSUP = SPACE(255)
    this.w_DEST = cp_CharToDate("  -  -  ")
    this.w_MCODVAL = SPACE(3)
    this.w_MCODIVE = SPACE(5)
    this.w_MRIFDIC = SPACE(15)
    this.w_COVET = SPACE(5)
    this.w_COVE2 = SPACE(5)
    this.w_COVE3 = SPACE(5)
    this.w_COPOR = " "
    this.w_COSPE = SPACE(3)
    this.w_ASPES = SPACE(30)
    this.w_RICON = SPACE(15)
    this.w_COAGE = SPACE(5)
    this.w_COAG2 = SPACE(5)
    this.w_SCCL1 = 0
    this.w_SCCL2 = 0
    this.w_SCPAG = 0
    this.w_COCON = " "
    this.w_COBAN = SPACE(10)
    this.w_CONCOR = SPACE(25)
    this.w_COBA2 = SPACE(15)
    this.w_QTCOL = 0
    this.w_QTPES = 0
    this.w_COPAG = SPACE(5)
    this.w_DADIV = cp_CharToDate("  -  -  ")
    this.w_INCOM = cp_CharToDate("  -  -  ")
    this.w_QTLOR = 0
    this.oParentObject.w_SPEINC = 0
    this.w_NOAGG = " "
    this.w_FICOM = cp_CharToDate("  -  -  ")
    this.w_SPEIMB = 0
    this.w_CODES = SPACE(5)
    this.w_CLADO = SPACE(2)
    this.w_SPETRA = 0
    this.w_TRAINT = 0
    this.w_COORN = SPACE(15)
    this.w_ACCONT = 0
    this.w_TCF = " "
    this.w_CLADOC = "  "
    this.w_FLCAPA = " "
    this.w_FLSCAF = " "
    this.w_FLFOSC = " "
    this.w_SCOFIS = 0
    this.w_SCOFIN = 0
    this.w_SPEBOL = 0
    this.w_DCCOLLEG = SPACE(5)
    this.w_DCFLINTE = " "
    this.w_ACCONT = 0
    this.w_ACCPRE = 0
    this.w_ACCSUC = 0
    this.w_ACCSUC1 = 0
    this.w_MCAORIF = 0
    this.w_MRIFODL = SPACE(10)
    this.w_APPO = SPACE(5)
    this.w_IMPARR = 0
    this.w_DATPLA = cp_CharToDate("  -  -  ")
    this.w_DESDOC = SPACE(50)
    this.w_CODSED = SPACE(5)
    this.w_OVEAC = this.oParentObject.w_MVFLVEAC
    this.w_FLVABD = " "
    this.w_RIFER = SPACE(10)
    this.w_CAUIMB = 0
    this.w_AGG_01 = SPACE(15)
    this.w_AGG_02 = SPACE(15)
    this.w_AGG_03 = SPACE(15)
    this.w_AGG_04 = SPACE(15)
    this.w_AGG_05 = cp_CharToDate("  -  -    ")
    this.w_AGG_06 = cp_CharToDate("  -  -    ")
    * --- Attenzione a w_APPOSER, proviene dalla pagina precedente
    if this.w_ROWPRE<>-1
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_APPOSER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              MVSERIAL = this.w_APPOSER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MEMO = NVL(cp_ToDate(_read_.MV__NOTE),cp_NullValue(_read_.MV__NOTE))
        this.w_ACCONT = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
        this.w_ACCSUC = NVL(cp_ToDate(_read_.MVACCONT),cp_NullValue(_read_.MVACCONT))
        this.w_ACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
        this.w_ACCSUC1 = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
        this.w_ARIF = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_AEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
        this.w_ASPES = NVL(cp_ToDate(_read_.MVASPEST),cp_NullValue(_read_.MVASPEST))
        this.w_MCAORIF = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
        this.w_CAUIMB = NVL(cp_ToDate(_read_.MVCAUIMB),cp_NullValue(_read_.MVCAUIMB))
        this.w_CLADO = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        this.w_COAG2 = NVL(cp_ToDate(_read_.MVCODAG2),cp_NullValue(_read_.MVCODAG2))
        this.w_COAGE = NVL(cp_ToDate(_read_.MVCODAGE),cp_NullValue(_read_.MVCODAGE))
        this.w_COBA2 = NVL(cp_ToDate(_read_.MVCODBA2),cp_NullValue(_read_.MVCODBA2))
        this.w_COBAN = NVL(cp_ToDate(_read_.MVCODBAN),cp_NullValue(_read_.MVCODBAN))
        this.w_RICON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_CODES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
        this.w_MCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
        this.w_COORN = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
        this.w_COPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
        this.w_COPOR = NVL(cp_ToDate(_read_.MVCODPOR),cp_NullValue(_read_.MVCODPOR))
        this.w_CODSED = NVL(cp_ToDate(_read_.MVCODSED),cp_NullValue(_read_.MVCODSED))
        this.w_COSPE = NVL(cp_ToDate(_read_.MVCODSPE),cp_NullValue(_read_.MVCODSPE))
        this.w_MCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
        this.w_COVE2 = NVL(cp_ToDate(_read_.MVCODVE2),cp_NullValue(_read_.MVCODVE2))
        this.w_COVE3 = NVL(cp_ToDate(_read_.MVCODVE3),cp_NullValue(_read_.MVCODVE3))
        this.w_COVET = NVL(cp_ToDate(_read_.MVCODVET),cp_NullValue(_read_.MVCODVET))
        this.w_COCON = NVL(cp_ToDate(_read_.MVCONCON),cp_NullValue(_read_.MVCONCON))
        this.w_DADIV = NVL(cp_ToDate(_read_.MVDATDIV),cp_NullValue(_read_.MVDATDIV))
        this.w_DRIF = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_DEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
        this.w_DATPLA = NVL(cp_ToDate(_read_.MVDATPLA),cp_NullValue(_read_.MVDATPLA))
        this.w_DESDOC = NVL(cp_ToDate(_read_.MVDESDOC),cp_NullValue(_read_.MVDESDOC))
        this.w_FLCAPA = NVL(cp_ToDate(_read_.MVFLCAPA),cp_NullValue(_read_.MVFLCAPA))
        this.w_FLFOCA = NVL(cp_ToDate(_read_.MVFLFOCA),cp_NullValue(_read_.MVFLFOCA))
        this.w_FLFOSC = NVL(cp_ToDate(_read_.MVFLFOSC),cp_NullValue(_read_.MVFLFOSC))
        this.w_FLSCAF = NVL(cp_ToDate(_read_.MVFLSCAF),cp_NullValue(_read_.MVFLSCAF))
        this.w_FLSFIN = NVL(cp_ToDate(_read_.MVFLSFIN),cp_NullValue(_read_.MVFLSFIN))
        this.w_FLVABD = NVL(cp_ToDate(_read_.MVFLVABD),cp_NullValue(_read_.MVFLVABD))
        this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        this.w_OVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        this.w_IMPARR = NVL(cp_ToDate(_read_.MVIMPARR),cp_NullValue(_read_.MVIMPARR))
        this.w_SCOFIN = NVL(cp_ToDate(_read_.MVIMPFIN),cp_NullValue(_read_.MVIMPFIN))
        this.w_IVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
        this.w_IVACAU = NVL(cp_ToDate(_read_.MVIVACAU),cp_NullValue(_read_.MVIVACAU))
        this.w_IVAIMB = NVL(cp_ToDate(_read_.MVIVAIMB),cp_NullValue(_read_.MVIVAIMB))
        this.w_IVAINC = NVL(cp_ToDate(_read_.MVIVAINC),cp_NullValue(_read_.MVIVAINC))
        this.w_IVATRA = NVL(cp_ToDate(_read_.MVIVATRA),cp_NullValue(_read_.MVIVATRA))
        this.w_NOAGG = NVL(cp_ToDate(_read_.MVNOTAGG),cp_NullValue(_read_.MVNOTAGG))
        this.w_CONCOR = NVL(cp_ToDate(_read_.MVNUMCOR),cp_NullValue(_read_.MVNUMCOR))
        this.w_NRIF = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_NEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
        this.w_QTCOL = NVL(cp_ToDate(_read_.MVQTACOL),cp_NullValue(_read_.MVQTACOL))
        this.w_QTLOR = NVL(cp_ToDate(_read_.MVQTALOR),cp_NullValue(_read_.MVQTALOR))
        this.w_QTPES = NVL(cp_ToDate(_read_.MVQTAPES),cp_NullValue(_read_.MVQTAPES))
        this.w_MRIFDIC = NVL(cp_ToDate(_read_.MVRIFDIC),cp_NullValue(_read_.MVRIFDIC))
        this.w_MRIFODL = NVL(cp_ToDate(_read_.MVRIFODL),cp_NullValue(_read_.MVRIFODL))
        this.w_SCCL1 = NVL(cp_ToDate(_read_.MVSCOCL1),cp_NullValue(_read_.MVSCOCL1))
        this.w_SCCL2 = NVL(cp_ToDate(_read_.MVSCOCL2),cp_NullValue(_read_.MVSCOCL2))
        this.w_SCOFIS = NVL(cp_ToDate(_read_.MVSCONTI),cp_NullValue(_read_.MVSCONTI))
        this.w_SCPAG = NVL(cp_ToDate(_read_.MVSCOPAG),cp_NullValue(_read_.MVSCOPAG))
        this.w_SPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
        this.w_OLDSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
        this.w_SPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
        this.w_FICOM = NVL(cp_ToDate(_read_.MVTFICOM),cp_NullValue(_read_.MVTFICOM))
        this.w_INCOM = NVL(cp_ToDate(_read_.MVTINCOM),cp_NullValue(_read_.MVTINCOM))
        this.w_TCF = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_DCCOLLEG = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_APPO = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_TIPORN = NVL(cp_ToDate(_read_.MVTIPORN),cp_NullValue(_read_.MVTIPORN))
        this.w_TRAINT = NVL(cp_ToDate(_read_.MVTRAINT),cp_NullValue(_read_.MVTRAINT))
        this.w_FLRINC = NVL(cp_ToDate(_read_.MVFLRINC),cp_NullValue(_read_.MVFLRINC))
        this.w_FLRIMB = NVL(cp_ToDate(_read_.MVFLRIMB),cp_NullValue(_read_.MVFLRIMB))
        this.w_FLRTRA = NVL(cp_ToDate(_read_.MVFLRTRA),cp_NullValue(_read_.MVFLRTRA))
        this.w_AGG_01 = NVL(cp_ToDate(_read_.MVAGG_01),cp_NullValue(_read_.MVAGG_01))
        this.w_AGG_02 = NVL(cp_ToDate(_read_.MVAGG_02),cp_NullValue(_read_.MVAGG_02))
        this.w_AGG_03 = NVL(cp_ToDate(_read_.MVAGG_03),cp_NullValue(_read_.MVAGG_03))
        this.w_AGG_04 = NVL(cp_ToDate(_read_.MVAGG_04),cp_NullValue(_read_.MVAGG_04))
        this.w_AGG_05 = NVL(cp_ToDate(_read_.MVAGG_05),cp_NullValue(_read_.MVAGG_05))
        this.w_AGG_06 = NVL(cp_ToDate(_read_.MVAGG_06),cp_NullValue(_read_.MVAGG_06))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Verifico se devo sbiancare le spese accessorie, se ho gi� importato una riga da documento di origine
    *     non sommo pi� le spese
    This.oParentObject.Exec_Select("_Tmp_SpeAcc_","Count(*) As Conta","t_MVSERRIF="+cp_ToStr(this.w_APPOSER),"","","")
    if NVL(_Tmp_SpeAcc_.Conta, 0)>0
      * --- Se gi� importate le azzero
      this.w_SPEIMB = 0
      this.w_SPETRA = 0
    endif
    USE IN SELECT("_Tmp_SpeAcc_")
    this.oParentObject.w_MVDESDOC = IIF(EMPTY(this.oParentObject.w_MVDESDOC),this.w_DESDOC,this.oParentObject.w_MVDESDOC)
    * --- Lettura descrizione riferimento di origine
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDDESRIF,TFFLGEFA,TDMODRIF,TDFLAPCA,TDMCALSI,TDMCALST"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_APPO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDDESRIF,TFFLGEFA,TDMODRIF,TDFLAPCA,TDMCALSI,TDMCALST;
        from (i_cTable) where;
            TDTIPDOC = this.w_APPO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESRIF = NVL(cp_ToDate(_read_.TDDESRIF),cp_NullValue(_read_.TDDESRIF))
      this.w_FLGEFA = NVL(cp_ToDate(_read_.TFFLGEFA),cp_NullValue(_read_.TFFLGEFA))
      this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
      this.w_oFLAPCA = NVL(cp_ToDate(_read_.TDFLAPCA),cp_NullValue(_read_.TDFLAPCA))
      this.w_TDMCALSI = NVL(cp_ToDate(_read_.TDMCALSI),cp_NullValue(_read_.TDMCALSI))
      this.w_TDMCALST = NVL(cp_ToDate(_read_.TDMCALST),cp_NullValue(_read_.TDMCALST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo se la causale di origine ha il ricalcolo prezzi
    * --- Read from DOC_COLL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_COLL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DCFLRIPS,DCFLACCO"+;
        " from "+i_cTable+" DOC_COLL where ";
            +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
            +" and DCCOLLEG = "+cp_ToStrODBC(this.w_APPO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DCFLRIPS,DCFLACCO;
        from (i_cTable) where;
            DCCODICE = this.oParentObject.w_MVTIPDOC;
            and DCCOLLEG = this.w_APPO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DCFLRIPS = NVL(cp_ToDate(_read_.DCFLRIPS),cp_NullValue(_read_.DCFLRIPS))
      this.w_TFLACCO = NVL(cp_ToDate(_read_.DCFLACCO),cp_NullValue(_read_.DCFLACCO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TCF<>this.oParentObject.w_MVTIPCON
      * --- Se imposto da altro ciclo svuoto lo sconto finanziario
      this.w_SCOFIN = 0
    endif
    if this.oParentObject.w_MVCODVAL<>this.w_MCODVAL AND this.w_MCAORIF<>0
      * --- Se documenti di differenti valute Riporta alla Valuta del Documento di Destinazione
      *     Unico caso possibile se effettuo un documento in Lire devo importarlo
      *     in un in Euro
      this.oParentObject.w_SPEINC = IIF(this.oParentObject.w_SPEINC=0, 0, cp_ROUND(this.oParentObject.w_SPEINC / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_SPEIMB = IIF(this.w_SPEIMB=0, 0, cp_ROUND(this.w_SPEIMB / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_SPETRA = IIF(this.w_SPETRA=0, 0, cp_ROUND(this.w_SPETRA / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_SPEBOL = IIF(this.w_SPEBOL=0, 0, cp_ROUND(this.w_SPEBOL / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_SCOFIS = IIF(this.w_SCOFIS=0, 0, cp_ROUND(this.w_SCOFIS / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_TRAINT = IIF(this.w_TRAINT=0, 0, cp_ROUND(this.w_TRAINT / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_ACCONT = IIF(this.w_ACCONT=0, 0, cp_ROUND(this.w_ACCONT / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_ACCPRE = IIF(this.w_ACCPRE=0, 0, cp_ROUND(this.w_ACCPRE / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_ACCSUC = IIF(this.w_ACCSUC=0, 0, cp_ROUND(this.w_ACCSUC / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.w_SCOFIN = IIF(this.w_SCOFIN=0, 0, cp_ROUND(this.w_SCOFIN / this.w_MCAORIF, this.oParentObject.w_DECTOT))
    endif
    * --- Acconto Precedente non importato nelle Note di Credito/Ordini e fatture di anticipo originati da contabilizzazione con accorpa acconti
    if this.oParentObject.w_MVCLADOC="OR" OR this.oParentObject.w_MVCLADOC="NC" OR (this.w_CLADOC="OR" AND this.w_FLCAPA<>"S") or this.w_TFLACCO="S"
      this.w_ACCPRE = 0
      this.w_ACCONT = 0
    endif
    * --- Acconto Precedente non importato in AlteregoTop se l'origine � una fattura d'acconto
    if IsAlt() and SOTCATDO(this.w_APPO)="A"
      this.w_ACCPRE = 0
      this.w_ACCONT = 0
    endif
    if NOT EMPTY(this.w_DCCOLLEG)
      * --- Read from DOC_COLL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_COLL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DCFLINTE,DCFLEVAS,DCFLGROR"+;
          " from "+i_cTable+" DOC_COLL where ";
              +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
              +" and DCCOLLEG = "+cp_ToStrODBC(this.w_DCCOLLEG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DCFLINTE,DCFLEVAS,DCFLGROR;
          from (i_cTable) where;
              DCCODICE = this.oParentObject.w_MVTIPDOC;
              and DCCOLLEG = this.w_DCCOLLEG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DCFLINTE = NVL(cp_ToDate(_read_.DCFLINTE),cp_NullValue(_read_.DCFLINTE))
        this.w_DCFLEVAS = NVL(cp_ToDate(_read_.DCFLEVAS),cp_NullValue(_read_.DCFLEVAS))
        this.w_DCFLGROR = NVL(cp_ToDate(_read_.DCFLGROR),cp_NullValue(_read_.DCFLGROR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.w_CLADOC="DT" AND this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC $ "DT-FA-NC" AND NOT EMPTY(this.w_DATPLA)
      * --- Importo la data competenza plafond (prendo tra tutti i documenti quella massima) , solo se documento di origine � un DDT
      this.oParentObject.w_MVDATPLA = MAX(this.w_DATPLA, IIF(this.oParentObject.w_GIAIMP<>"S", i_INIDAT, this.oParentObject.w_MVDATPLA))
    endif
    * --- Se il documento di destinazione � vuoto lo valorizzo con il flag beni deperibili
    *     altrimenti (se documento pieno) l'importazione non modifica il flag impostato
    if this.w_RECTRS=0
      if this.w_TCF=this.oParentObject.w_MVTIPCON
        * --- Al primo giro svuoto il flag sul documento.
        *     Solo se stesso ciclo
        *     Altrimenti mantengo quello del documento di destinazione
        this.oParentObject.w_MVFLVABD = IIF ( this.w_NEWDOC="zzzzzzzzzz" , Space(1) , this.oParentObject.w_MVFLVABD)
        this.oParentObject.w_MVFLVABD = IIF( this.w_FLVABD="S", "S" , this.oParentObject.w_MVFLVABD) 
      endif
      * --- I flag di ripartizione spese vengono copiati dal documento di origine solo se il documento di destinazione � vuoto.
      if this.oParentObject.w_MVFLVEAC=this.w_FLVEAC
        * --- Leggo i flag delle Spese accessorie ripartite del doc. di origine solo se il ciclo V/A � identico 
        this.oParentObject.w_MVFLRINC = this.w_FLRINC
        this.oParentObject.w_MVFLRIMB = this.w_FLRIMB
        this.oParentObject.w_MVFLRTRA = this.w_FLRTRA
      endif
    endif
    if this.oParentObject.w_TDFLIA01="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_01, " "))
      this.oParentObject.w_MVAGG_01 = this.w_AGG_01
    endif
    if this.oParentObject.w_TDFLIA02="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_02, " "))
      this.oParentObject.w_MVAGG_02 = this.w_AGG_02
    endif
    if this.oParentObject.w_TDFLIA03="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_03, " "))
      this.oParentObject.w_MVAGG_03 = this.w_AGG_03
    endif
    if this.oParentObject.w_TDFLIA04="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_04, " "))
      this.oParentObject.w_MVAGG_04 = this.w_AGG_04
    endif
    if this.oParentObject.w_TDFLIA05="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_05, " "))
      this.oParentObject.w_MVAGG_05 = this.w_AGG_05
    endif
    if this.oParentObject.w_TDFLIA06="S" AND EMPTY(NVL(this.oParentObject.w_MVAGG_06, " "))
      this.oParentObject.w_MVAGG_06 = this.w_AGG_06
    endif
    * --- Primo Documento Selezionato (solo in Caricamento Documento)
    if this.w_PADRE.cFunction="Load"
      if (this.w_TCF=this.oParentObject.w_MVTIPCON AND (this.w_RICON=this.oParentObject.w_MVCODCON Or (this.w_COORN = this.oParentObject.w_MVCODCON And this.oParentObject.w_MVCLADOC$"FA-NC")) ) OR this.oParentObject.w_MVCLADOC="RF"
        if this.oParentObject.w_MVFLACCO="S" And this.oParentObject.w_GIAIMP<>"S" And this.w_RECTRS=0
          * --- Se Attivo Dati Accompagnatori
          this.oParentObject.w_MVQTACOL = this.w_QTCOL
          this.oParentObject.w_MVQTAPES = this.w_QTPES
          this.oParentObject.w_MVQTALOR = this.w_QTLOR
          this.oParentObject.w_MVNOTAGG = this.w_NOAGG
          this.oParentObject.w_ASPETT = this.w_ASPES
          * --- Se intestatario di Origine = a Intestatario, attivo in Azienda il Flag Consegna x Conto di,
          *     e non sto caricando una Fattura, Nota di Credito o Ricevuta Fiscale,
          *     Importo anche il Per conto di
          if this.w_RICON=this.oParentObject.w_MVCODCON AND g_PERORN="S" AND NOT this.oParentObject.w_MVCLADOC $ "FA-NC-RF"
            this.oParentObject.w_MVCODORN = this.w_COORN
            this.oParentObject.w_MVTIPORN = this.w_TIPORN
            if Not Empty(this.oParentObject.w_MVCODORN)
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPORN);
                      +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODORN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI;
                  from (i_cTable) where;
                      ANTIPCON = this.oParentObject.w_MVTIPORN;
                      and ANCODICE = this.oParentObject.w_MVCODORN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESORN = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
                this.oParentObject.w_INDORN = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.oParentObject.w_CAPORN = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.oParentObject.w_LOCORN = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
        endif
        this.w_RECTRS = this.w_PADRE.NumRow()
        * --- Solo se non ho gi� inserito righe (e quindi solo se non ho gi� importato un documento)
        if this.w_RECTRS=0
          this.oParentObject.w_XCONORN = IIF(Not Empty(this.oParentObject.w_MVCODORN) And g_XCONDI = "S",this.oParentObject.w_MVCODORN,this.oParentObject.w_MVCODCON)
          this.w_OBJCTRL = this.w_PADRE.GetCtrl("w_XCONORN")
          this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_MVTIPCON
          this.w_OSOURCE.xKey( 2 ) = this.oParentObject.w_XCONORN
          * --- Disabilitazione i post-In (sono riabilitati al termine dell'esecuzione di ecpDrop
          this.w_OLDPOSTIN = i_ServerConn[1,7]
          i_ServerConn[1,7]=.F.
          this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
          i_ServerConn[1,7]= this.w_OLDPOSTIN 
        endif
        * --- Importa i Dati di testata del Primo Doc. di Origine sul Doc. di Destinazione
        * --- w_IMPIVE se .t. importa l'Eventuale Codice IVA Esenzione/Agevolato del Doc. di Origine (DEFAULT Importa sempre)
        this.w_IMPIVE = .T.
        this.w_SCADUTA = .F.
        if this.w_RECTRS=0 and this.oParentObject.w_INTENT<>"S"
          * --- w_IMPLET se .t. importa la lettera di Intento del Doc. di Origine
          this.w_IMPLET = .F.
          this.w_MESS = ""
          if EMPTY(this.w_MRIFDIC)
            * --- Documento di Origine Senza Lettera di Intento
            this.w_IMPLET = .F.
            if NOT EMPTY(this.oParentObject.w_MVRIFDIC)
              * --- Documento di Destinazione con Lettera di Intento e Origine no
              this.w_MESS = "Si desidera mantenere i riferimenti della lettera di intento%0presente sul documento di destinazione?"
              * --- Se rispondo S, w_IMPIVE=.F. (non importo il codice IVA e di conseguenza il Rif. alla Lettera)
              this.w_IMPIVE = NOT ah_YesNo(this.w_MESS)
              * --- Se rispondo N, w_IMPIVE=.T. (importo il codice IVA e non mantengo l'esenzione)
              this.oParentObject.w_MVRIFDIC = IIF(this.w_IMPIVE, SPACE(15), this.oParentObject.w_MVRIFDIC)
              if empty(this.oParentObject.w_MVRIFDIC)
                * --- sbianco le variabili collegate alla lettera d'intento
                this.oParentObject.w_NUMDIC = 0
                this.oParentObject.w_OPEDIC = " "
                this.oParentObject.w_ANNDIC = space(4)
                this.oParentObject.w_DATDIC = cp_CharToDate("  /  /    ")
                this.oParentObject.w_UTIDIC = 0
                this.oParentObject.w_TIPDIC = " "
                this.oParentObject.w_TIPIVA = " "
                this.oParentObject.w_ALFDIC = space(2)
              endif
            endif
          else
            * --- Lettura lettera di intento valida
            DECLARE ARRDIC (14,1)
            * --- Azzero l'Array che verr� riempito dalla Funzione
            ARRDIC(1)=0
            this.w_OK_LET = CAL_LETT(this.w_DRIF,this.oParentObject.w_MVTIPCON,this.oParentObject.w_XCONORN, @ArrDic, this.w_MRIFDIC)
            * --- Documento di Origine con Lettera di Intento; 
            *     chiede se Importare i riferimenti. 
            *     Solo codice Iva se anche il documento di destinazione ha una lettera di intento valida.
            *     Nel caso in cui nel documento di Origine ci sia una lettera di intento non valida alla data dello stesso
            *     (potrebbe succedere se il documento di origine � nato da una importazione di un documento nel quale era valida la lettera)
            *     w_OK_LET � .F. e l'Array � vuoto quindi il controllo qui sotto darebbe errore (DATA > .F.)
            *     Anche in questo caso la lettera non � ad oggi valida
            * --- Controlo se lettera di intento Scaduta sul documento di Origine
            if Not this.w_OK_LET Or this.oParentObject.w_MVDATDOC>ArrDic[12] And Not Empty(Arrdic[12])
              * --- Scaduta
              this.w_MESS = "Si desidera importare i riferimenti della lettera di intento ad oggi scaduta presente sul documento di origine?"
              this.w_SCADUTA = .T.
            else
              * --- Valida e di tipo operazione specifica
              if ArrDic[ 2 ]="O"
                this.w_MESS = "Lettera di intento per operazione specifica che verr� mantenuta%0sul documento di destinazione con possibilit� di modificarla"
              else
                this.w_MESS = "Si desidera importare i riferimenti della lettera di intento%0presente sul documento di origine?"
              endif
            endif
            if EMPTY(this.oParentObject.w_MVRIFDIC)
              * --- Se non ho lettera di intento valida nel documento di destinazione
              *     Importo quella del documento di Origine
              if !this.w_SCADUTA AND ArrDic[ 2 ]="O" 
                AH_ERRORMSG(this.w_MESS)
                this.w_IMPIVE = .T.
              else
                this.w_IMPIVE = ah_YesNo(this.w_MESS)
                if this.w_IMPIVE
                  this.w_OK_LET = .T.
                endif
              endif
              this.w_IMPLET = .T.
            else
              if this.oParentObject.w_MVRIFDIC<>this.w_MRIFDIC
                * --- Se ho lettera di intento valida nel documento di destinazione 
                *     diversa da quella del documento di origine
                *     Mantengo Questa. (Solo i riferimenti)
                if !this.w_SCADUTA AND ArrDic[ 2 ]="O"
                  AH_ERRORMSG(this.w_MESS)
                  this.w_IMPIVE = .T.
                  this.w_IMPLET = .T.
                else
                  this.w_IMPIVE = ah_YesNo(this.w_MESS)
                  * --- Importo quindi anche i riferimenti 
                  this.w_IMPLET = this.w_IMPIVE
                  this.w_OK_LET = this.w_IMPIVE
                endif
              else
                * --- Se in entrambi i documenti � presente la stessa lettera di intento,
                *     mantengo quella che � nel documento di destinazione
                if this.oParentObject.w_MVFLVEAC="V" And this.w_MCODIVE<>this.oParentObject.w_MVCODIVE
                  * --- Se ciclo attivo e il codice Iva esenzione del documento di origine
                  *     � diverso da quello di destinazione chiedo quale devo importare
                  this.w_IMPIVE = ah_YesNo(this.w_MESS)
                else
                  * --- Se ciclo passivo o lettera codice Iva esenzione Uguale non importo niente
                  *     e non chiedo niente poich� la dichiarazione � gi� presente sul documento di destinazione
                  this.w_IMPIVE = .F.
                endif
                this.w_IMPLET = .F.
              endif
            endif
          endif
        endif
        if ((this.w_IMPIVE Or this.oParentObject.w_MVRIFDIC=this.w_MRIFDIC) And this.w_RECTRS = 0 And !Isalt() ) 
          * --- Se la lettera � la stessa importo eventuali codici modificati manualmente nel documento d'origine
          * --- importa i Codici IVA Spese
          if this.oParentObject.w_MVFLVEAC="V"
            this.oParentObject.w_MVIVAINC = this.w_IVAINC
            if ((this.w_FLACCO <>"S" or IsAlt()) and this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_DRIF>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_DRIF<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.oParentObject.w_MVIVAINC+","$this.oParentObject.w_RIIVLSIV))
              this.oParentObject.w_MVIVAINC = g_COIINC
            endif
            this.oParentObject.w_PEIINC = 0
            this.oParentObject.w_BOLINC = " "
            if NOT EMPTY(this.oParentObject.w_MVIVAINC)
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVPERIVA,IVBOLIVA"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVAINC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVPERIVA,IVBOLIVA;
                  from (i_cTable) where;
                      IVCODIVA = this.oParentObject.w_MVIVAINC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                this.oParentObject.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.oParentObject.w_MVIVAIMB = this.w_IVAIMB
            if ((this.w_FLACCO <>"S" or IsAlt()) and this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_DRIF>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_DRIF<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.oParentObject.w_MVIVAIMB+","$this.oParentObject.w_RIIVLSIV))
              this.oParentObject.w_MVIVAIMB = g_COIIMB
            endif
            this.oParentObject.w_PEIIMB = 0
            this.oParentObject.w_BOLIMB = " "
            if NOT EMPTY(this.oParentObject.w_MVIVAIMB)
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVPERIVA,IVBOLIVA"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVAIMB);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVPERIVA,IVBOLIVA;
                  from (i_cTable) where;
                      IVCODIVA = this.oParentObject.w_MVIVAIMB;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                this.oParentObject.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          this.oParentObject.w_MVIVATRA = this.w_IVATRA
          if ((this.w_FLACCO <>"S" or IsAlt()) and this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_DRIF>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_DRIF<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.oParentObject.w_MVIVATRA+","$this.oParentObject.w_RIIVLSIV))
            this.oParentObject.w_MVIVATRA = g_COITRA
          endif
          this.oParentObject.w_PEITRA = 0
          this.oParentObject.w_BOLTRA = " "
          if NOT EMPTY(this.oParentObject.w_MVIVATRA)
            * --- Read from VOCIIVA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOCIIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IVPERIVA,IVBOLIVA"+;
                " from "+i_cTable+" VOCIIVA where ";
                    +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVATRA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IVPERIVA,IVBOLIVA;
                from (i_cTable) where;
                    IVCODIVA = this.oParentObject.w_MVIVATRA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
              this.oParentObject.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.oParentObject.w_MVIVABOL = this.w_IVABOL
          if ((this.w_FLACCO <>"S" or IsAlt()) and this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_DRIF>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_DRIF<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.oParentObject.w_MVIVABOL+","$this.oParentObject.w_RIIVLSIV))
            this.oParentObject.w_MVIVABOL = g_COIBOL
          endif
          this.oParentObject.w_BOLBOL = " "
          if NOT EMPTY(this.oParentObject.w_MVIVABOL)
            * --- Read from VOCIIVA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOCIIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IVBOLIVA"+;
                " from "+i_cTable+" VOCIIVA where ";
                    +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVABOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IVBOLIVA;
                from (i_cTable) where;
                    IVCODIVA = this.oParentObject.w_MVIVABOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.oParentObject.w_MVIVACAU = this.w_IVACAU
          this.oParentObject.w_BOLCAU = " "
          this.oParentObject.w_REVCAU = " "
          if NOT EMPTY(this.oParentObject.w_MVIVACAU)
            * --- Read from VOCIIVA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOCIIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IVBOLIVA,IVREVCHA"+;
                " from "+i_cTable+" VOCIIVA where ";
                    +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVACAU);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IVBOLIVA,IVREVCHA;
                from (i_cTable) where;
                    IVCODIVA = this.oParentObject.w_MVIVACAU;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_BOLCAU = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
              this.oParentObject.w_REVCAU = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if this.w_IMPIVE
            * --- Importa Il Codice IVA Esente/Agevolato del Documento di Origine 
            *     se non sono gi� presenti righe sul dettaglio
            if this.oParentObject.w_MVCODIVE<>this.w_MCODIVE 
              this.oParentObject.w_MVCODIVE = this.w_MCODIVE
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVPERIVA,IVBOLIVA,IVPERIND"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVPERIVA,IVBOLIVA,IVPERIND;
                  from (i_cTable) where;
                      IVCODIVA = this.oParentObject.w_MVCODIVE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                this.oParentObject.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                this.w_INDIVE1 = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.oParentObject.w_MVFLVEAC="V"
                * --- Aggiorna anche i Codici IVA Spese
                if NOT EMPTY(this.oParentObject.w_MVCODIVE)
                  if EMPTY(this.w_MCODIVE)
                    this.oParentObject.w_MVIVAINC = this.oParentObject.w_MVCODIVE
                  endif
                  this.oParentObject.w_PEIINC = 0
                  this.oParentObject.w_BOLINC = " "
                  if NOT EMPTY(this.oParentObject.w_MVIVAINC)
                    * --- Read from VOCIIVA
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "IVPERIVA,IVBOLIVA"+;
                        " from "+i_cTable+" VOCIIVA where ";
                            +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVAINC);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        IVPERIVA,IVBOLIVA;
                        from (i_cTable) where;
                            IVCODIVA = this.oParentObject.w_MVIVAINC;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                      this.oParentObject.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  if EMPTY(this.w_MCODIVE)
                    this.oParentObject.w_MVIVAIMB = this.oParentObject.w_MVCODIVE
                  endif
                  this.oParentObject.w_PEIIMB = 0
                  this.oParentObject.w_BOLIMB = " "
                  if NOT EMPTY(this.oParentObject.w_MVIVAIMB)
                    * --- Read from VOCIIVA
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "IVPERIVA,IVBOLIVA"+;
                        " from "+i_cTable+" VOCIIVA where ";
                            +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVAIMB);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        IVPERIVA,IVBOLIVA;
                        from (i_cTable) where;
                            IVCODIVA = this.oParentObject.w_MVIVAIMB;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                      this.oParentObject.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  if EMPTY(this.w_MCODIVE)
                    this.oParentObject.w_MVIVATRA = this.oParentObject.w_MVCODIVE
                  endif
                  this.oParentObject.w_PEITRA = 0
                  this.oParentObject.w_BOLTRA = " "
                  if NOT EMPTY(this.oParentObject.w_MVIVATRA)
                    * --- Read from VOCIIVA
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "IVPERIVA,IVBOLIVA"+;
                        " from "+i_cTable+" VOCIIVA where ";
                            +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVIVATRA);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        IVPERIVA,IVBOLIVA;
                        from (i_cTable) where;
                            IVCODIVA = this.oParentObject.w_MVIVATRA;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                      this.oParentObject.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                endif
              else
                * --- Riassegno Percentuale Iva
                this.oParentObject.w_INDIVE = this.w_INDIVE1
              endif
            endif
            if this.w_IMPLET And this.w_OK_LET
              * --- Importa la Lettera di Intento del Documento di Origine se trovata valida dalla funzione CAL_LETT
              * --- Parametri
              *     pDatRif : Data di Riferimento per filtro su Lettere di intento
              *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
              *     pCodCon : Codice Cliente/Fornitore
              *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
              *     
              *     pArrDic[ 1 ]   = Numero Dichiarazione
              *     pArrDic[ 2 ]   = Tipo Operazione
              *     pArrDic[ 3 ]   = Anno Dichiarazione
              *     pArrDic[ 4 ]   = Importo Dichiarazione
              *     pArrDic[ 5 ]   = Data Dichiarazione
              *     pArrDic[ 6 ]   = Importo Utilizzato
              *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
              *     pArrDic[ 8 ]   = Codice Iva Agevolata
              *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
              *     pArrDic[ 10 ] = Data Inizio Validit�
              *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
              *     pArrDic[ 12 ] = Data Obsolescenza
              this.oParentObject.w_MVRIFDIC = this.w_MRIFDIC
              this.oParentObject.w_NUMDIC = ArrDic(1)
              this.oParentObject.w_OPEDIC = ArrDic(2)
              this.oParentObject.w_ANNDIC = ArrDic(3)
              this.oParentObject.w_DATDIC = ArrDic(5)
              this.oParentObject.w_UTIDIC = ArrDic(6)
              this.oParentObject.w_TIPDIC = ArrDic(7)
              this.oParentObject.w_TIPIVA = ArrDic(9)
              this.oParentObject.w_ALFDIC = ArrDic(1)
              if this.oParentObject.w_NUMDIC=0
                * --- Read from DIC_INTE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DIC_INTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DINUMDIC,DI__ANNO,DIDATDIC,DITIPCON,DITIPOPE,DIIMPUTI,DITIPIVA,DIALFDIC"+;
                    " from "+i_cTable+" DIC_INTE where ";
                        +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFDIC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DINUMDIC,DI__ANNO,DIDATDIC,DITIPCON,DITIPOPE,DIIMPUTI,DITIPIVA,DIALFDIC;
                    from (i_cTable) where;
                        DISERIAL = this.oParentObject.w_MVRIFDIC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_NUMDIC = NVL(cp_ToDate(_read_.DINUMDIC),cp_NullValue(_read_.DINUMDIC))
                  this.oParentObject.w_ANNDIC = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
                  this.oParentObject.w_DATDIC = NVL(cp_ToDate(_read_.DIDATDIC),cp_NullValue(_read_.DIDATDIC))
                  this.oParentObject.w_TIPDIC = NVL(cp_ToDate(_read_.DITIPCON),cp_NullValue(_read_.DITIPCON))
                  this.oParentObject.w_OPEDIC = NVL(cp_ToDate(_read_.DITIPOPE),cp_NullValue(_read_.DITIPOPE))
                  this.oParentObject.w_UTIDIC = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
                  this.oParentObject.w_TIPIVA = NVL(cp_ToDate(_read_.DITIPIVA),cp_NullValue(_read_.DITIPIVA))
                  this.oParentObject.w_ALFDIC = NVL(cp_ToDate(_read_.DIALFDIC),cp_NullValue(_read_.DIALFDIC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- La lettera � scaduta e quindi l'array non � valorizzato. Rileggo le informazioni dalla tabella
              endif
            endif
          endif
        endif
        if this.oParentObject.w_GIAIMP<>"S" OR this.oParentObject.w_FLFOM="F"
          this.oParentObject.w_GIAIMP = "S"
          * --- Competenza Contabile
          this.oParentObject.w_MVTINCOM = IIF(EMPTY(this.oParentObject.w_MVTINCOM), this.w_INCOM, this.oParentObject.w_MVTINCOM)
          this.oParentObject.w_MVTFICOM = IIF(EMPTY(this.oParentObject.w_MVTFICOM), this.w_FICOM, this.oParentObject.w_MVTFICOM)
          * --- Se Documento INTRA Importa le Spese Trasporto fino al Confine
          if g_INTR="S" AND (this.oParentObject.w_TIPDOC $ "FE-NE" OR (this.oParentObject.w_MVCLADOC $ "DI-DT" AND this.oParentObject.w_FLINTR="S"))
            this.oParentObject.w_MVTRAINT = this.w_TRAINT
          endif
          * --- Importa Dati Accompagnatori
          if this.oParentObject.w_FLIMAC="S" OR this.w_RECTRS=0
            this.oParentObject.w_MVCODVET = this.w_COVET
            this.oParentObject.w_MVCODVE2 = this.w_COVE2
            this.oParentObject.w_MVCODVE3 = this.w_COVE3
            this.oParentObject.w_MVCODPOR = this.w_COPOR
            this.oParentObject.w_MVCODSPE = this.w_COSPE
            this.oParentObject.w_MVCONCON = this.w_COCON
            this.oParentObject.w_DESVET = SPACE(35)
            this.oParentObject.w_DESVE2 = SPACE(35)
            this.oParentObject.w_DESVE3 = SPACE(35)
            this.oParentObject.w_DESPOR = SPACE(30)
            this.oParentObject.w_DESSPE = SPACE(35)
            this.oParentObject.w_MCSIVT1 = SPACE(5)
            this.oParentObject.w_MCSTVT1 = SPACE(5)
            this.oParentObject.w_MCSIVT2 = SPACE(5)
            this.oParentObject.w_MCSTVT2 = SPACE(5)
            if !Isalt()
              this.oParentObject.w_MCSIVT3 = SPACE(5)
              this.oParentObject.w_MCSTVT3 = SPACE(5)
              this.oParentObject.w_MCALSI3 = SPACE(5)
              this.oParentObject.w_MCALST3 = SPACE(5)
              this.oParentObject.w_MCALSI = SPACE(5)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODVET)
              * --- Read from VETTORI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VETTORI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VTDESVET,VTMCCODI,VTMCCODT"+;
                  " from "+i_cTable+" VETTORI where ";
                      +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVET);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VTDESVET,VTMCCODI,VTMCCODT;
                  from (i_cTable) where;
                      VTCODVET = this.oParentObject.w_MVCODVET;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESVET = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
                this.oParentObject.w_MCSIVT1 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
                this.oParentObject.w_MCSTVT1 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODVE2)
              * --- Read from VETTORI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VETTORI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VTDESVET,VTMCCODI,VTMCCODT"+;
                  " from "+i_cTable+" VETTORI where ";
                      +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVE2);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VTDESVET,VTMCCODI,VTMCCODT;
                  from (i_cTable) where;
                      VTCODVET = this.oParentObject.w_MVCODVE2;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESVE2 = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
                this.oParentObject.w_MCSIVT2 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
                this.oParentObject.w_MCSTVT2 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODVE3)
              * --- Read from VETTORI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VETTORI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VTDESVET,VTMCCODI,VTMCCODT"+;
                  " from "+i_cTable+" VETTORI where ";
                      +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVE3);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VTDESVET,VTMCCODI,VTMCCODT;
                  from (i_cTable) where;
                      VTCODVET = this.oParentObject.w_MVCODVE3;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESVE3 = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
                this.oParentObject.w_MCSIVT3 = NVL(cp_ToDate(_read_.VTMCCODI),cp_NullValue(_read_.VTMCCODI))
                this.oParentObject.w_MCSTVT3 = NVL(cp_ToDate(_read_.VTMCCODT),cp_NullValue(_read_.VTMCCODT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODPOR)
              * --- Read from PORTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PORTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PORTI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PODESPOR"+cp_TransInsFldName("PODESPOR")+""+;
                  " from "+i_cTable+" PORTI where ";
                      +"POCODPOR = "+cp_ToStrODBC(this.oParentObject.w_MVCODPOR);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PODESPOR,PODESPOR;
                  from (i_cTable) where;
                      POCODPOR = this.oParentObject.w_MVCODPOR;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESPOR = NVL(cp_ToDate(cp_TransLoadField('_read_.PODESPOR')),cp_NullValue(cp_TransLoadField('_read_.PODESPOR')))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODSPE)
              * --- Read from MODASPED
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MODASPED_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MODASPED_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SPDESSPE"+cp_TransInsFldName("SPDESSPE")+",SPMCCODI,SPMCCODT"+;
                  " from "+i_cTable+" MODASPED where ";
                      +"SPCODSPE = "+cp_ToStrODBC(this.oParentObject.w_MVCODSPE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SPDESSPE,SPDESSPE,SPMCCODI,SPMCCODT;
                  from (i_cTable) where;
                      SPCODSPE = this.oParentObject.w_MVCODSPE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESSPE = NVL(cp_ToDate(cp_TransLoadField('_read_.SPDESSPE')),cp_NullValue(cp_TransLoadField('_read_.SPDESSPE')))
                this.oParentObject.w_MCALSI3 = NVL(cp_ToDate(_read_.SPMCCODI),cp_NullValue(_read_.SPMCCODI))
                this.oParentObject.w_MCALST3 = NVL(cp_ToDate(_read_.SPMCCODT),cp_NullValue(_read_.SPMCCODT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if this.w_RICON=this.oParentObject.w_MVCODCON
              * --- Se Stesso Riferimento (no per conto di...)
              this.oParentObject.w_MVCODDES = this.w_CODES
              this.oParentObject.w_INDDES = SPACE(36)
              this.oParentObject.w_CAPDES = SPACE(8)
              this.oParentObject.w_LOCDES = SPACE(30)
              this.oParentObject.w_NOMDES = SPACE(36)
              this.oParentObject.w_PRODES = "  "
              * --- Tolgo il check caricamento automatico sede in quanto forzo i dati del documento d'origine
              if this.oParentObject.w_FLFOM="F"
                this.oParentObject.w_CARICAU = " "
              endif
              if NOT EMPTY(this.oParentObject.w_MVCODDES)
                * --- Read from DES_DIVE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DES_DIVE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DDPROVIN,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDTIPRIF,DDPREDEF,DDMCCODI,DDMCCODT"+;
                    " from "+i_cTable+" DES_DIVE where ";
                        +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                        +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                        +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_MVCODDES);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DDPROVIN,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDTIPRIF,DDPREDEF,DDMCCODI,DDMCCODT;
                    from (i_cTable) where;
                        DDTIPCON = this.oParentObject.w_MVTIPCON;
                        and DDCODICE = this.oParentObject.w_MVCODCON;
                        and DDCODDES = this.oParentObject.w_MVCODDES;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_PRODES = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
                  this.oParentObject.w_NOMDES = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
                  this.oParentObject.w_INDDES = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                  this.oParentObject.w_CAPDES = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                  this.oParentObject.w_LOCDES = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                  this.oParentObject.w_TIPRIF = NVL(cp_ToDate(_read_.DDTIPRIF),cp_NullValue(_read_.DDTIPRIF))
                  this.oParentObject.w_PREDEF = NVL(cp_ToDate(_read_.DDPREDEF),cp_NullValue(_read_.DDPREDEF))
                  this.oParentObject.w_MCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
                  this.oParentObject.w_MCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
            if NOT EMPTY(this.w_CODSED)
              this.oParentObject.w_MVCODSED = this.w_CODSED
              this.oParentObject.w_INDSED = SPACE(35)
              this.oParentObject.w_CAPSED = SPACE(5)
              this.oParentObject.w_LOCSED = SPACE(30)
              this.oParentObject.w_NOMSED = SPACE(40)
              this.oParentObject.w_PROSED = "  "
              * --- Read from SEDIAZIE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SENOMSED,SEINDIRI,SE___CAP,SELOCALI,SEPROVIN"+;
                  " from "+i_cTable+" SEDIAZIE where ";
                      +"SECODAZI = "+cp_ToStrODBC(i_codazi);
                      +" and SECODDES = "+cp_ToStrODBC(this.oParentObject.w_MVCODSED);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SENOMSED,SEINDIRI,SE___CAP,SELOCALI,SEPROVIN;
                  from (i_cTable) where;
                      SECODAZI = i_codazi;
                      and SECODDES = this.oParentObject.w_MVCODSED;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_NOMSED = NVL(cp_ToDate(_read_.SENOMSED),cp_NullValue(_read_.SENOMSED))
                this.oParentObject.w_INDSED = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
                this.oParentObject.w_CAPSED = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
                this.oParentObject.w_LOCSED = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
                this.oParentObject.w_PROSED = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if !Isalt()
              this.oParentObject.w_MCALSI = SPACE(5)
              this.oParentObject.w_MCALST = SPACE(5)
              this.oParentObject.w_MCALSI = ICASE( NOT EMPTY( this.oParentObject.w_MCALSI4 ) And this.oParentObject.w_CARICAU<>"S", this.oParentObject.w_MCALSI4, NOT EMPTY( this.oParentObject.w_MCALSI3 ), this.oParentObject.w_MCALSI3, NOT EMPTY( this.oParentObject.w_MCALSI2 ), this.oParentObject.w_MCALSI2, this.oParentObject.w_MCALSI1)
              this.oParentObject.w_MCALST = ICASE( NOT EMPTY( this.oParentObject.w_MCALST4 ) And this.oParentObject.w_CARICAU<>"S", this.oParentObject.w_MCALST4, NOT EMPTY( this.oParentObject.w_MCALST3 ), this.oParentObject.w_MCALST3, NOT EMPTY( this.oParentObject.w_MCALST2 ), this.oParentObject.w_MCALST2, this.oParentObject.w_MCALST1)
            endif
          endif
          * --- Arrotondamenti
          this.oParentObject.w_MVIMPARR = 0
          this.oParentObject.w_MVTOTRIT = 0
          this.oParentObject.w_MVTOTENA = 0
          if this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC<>"OR"
            this.oParentObject.w_MVIMPARR = this.w_IMPARR
            if this.oParentObject.w_MVCODVAL<>this.w_MCODVAL AND this.w_MCAORIF<>0 AND this.oParentObject.w_MVIMPARR<>0
              * --- Se documenti di differenti valute Riporta alla Valuta del Documento di Destinazione
              this.oParentObject.w_MVIMPARR = cp_ROUND(this.oParentObject.w_MVIMPARR / this.w_MCAORIF, this.oParentObject.w_DECTOT)
            endif
          endif
          * --- Importa Dati Pagamento
          if (this.oParentObject.w_FLIMPA="S" OR this.w_RECTRS=0)
            * --- Importo comunque i dati della pagina Dati generali + gli agenti
            this.oParentObject.w_MVSCOCL1 = this.w_SCCL1
            this.oParentObject.w_MVSCOCL2 = this.w_SCCL2
            this.oParentObject.w_MVDATDIV = this.w_DADIV
            this.oParentObject.w_MVFLSCAF = IIF(Not this.oParentObject.w_VARVAL $"+-",this.w_FLSCAF," ")
            this.oParentObject.w_DESBAN = SPACE(35)
            this.oParentObject.w_DESBA2 = SPACE(35)
            if NOT EMPTY (this.w_COBAN)
              this.oParentObject.w_MVCODBAN = this.w_COBAN
            endif
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODBAN"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODBAN;
                from (i_cTable) where;
                    ANTIPCON = this.oParentObject.w_MVTIPCON;
                    and ANCODICE = this.oParentObject.w_MVCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_BANC = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_BANC <> this.w_COBAN AND NOT EMPTY( this.w_COBAN)
              this.oParentObject.w_MVNUMCOR = " "
            endif
            if NOT EMPTY (this.w_CONCOR)
              this.oParentObject.w_MVNUMCOR = this.w_CONCOR
            endif
            if EMPTY (this.w_SCPAG) and Empty(this.w_COPAG)
              * --- RIapplico condizioni di pagamento solo se non ho indicato il pagamento nel documento di origine
              * --- Read from PAG_AMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PASCONTO"+;
                  " from "+i_cTable+" PAG_AMEN where ";
                      +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODPAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PASCONTO;
                  from (i_cTable) where;
                      PACODICE = this.oParentObject.w_MVCODPAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SCPAG = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if EMPTY (this.w_COBA2)
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODBA2"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODBA2;
                  from (i_cTable) where;
                      ANTIPCON = this.oParentObject.w_MVTIPCON;
                      and ANCODICE = this.oParentObject.w_MVCODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.oParentObject.w_MVSCOPAG = this.w_SCPAG
            this.oParentObject.w_MVCODBA2 = this.w_COBA2
            if NOT EMPTY(this.oParentObject.w_MVCODBAN)
              * --- Read from BAN_CHE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.BAN_CHE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "BADESBAN"+;
                  " from "+i_cTable+" BAN_CHE where ";
                      +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODBAN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  BADESBAN;
                  from (i_cTable) where;
                      BACODBAN = this.oParentObject.w_MVCODBAN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESBAN = NVL(cp_ToDate(_read_.BADESBAN),cp_NullValue(_read_.BADESBAN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.oParentObject.w_MVCODBA2)
              * --- Read from COC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.COC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "BADESCRI,BADTOBSO,BACONSBF"+;
                  " from "+i_cTable+" COC_MAST where ";
                      +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODBA2);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  BADESCRI,BADTOBSO,BACONSBF;
                  from (i_cTable) where;
                      BACODBAN = this.oParentObject.w_MVCODBA2;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESBA2 = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
                this.oParentObject.w_DTOBSOBC = NVL(cp_ToDate(_read_.BADTOBSO),cp_NullValue(_read_.BADTOBSO))
                this.oParentObject.w_CONSBF = NVL(cp_ToDate(_read_.BACONSBF),cp_NullValue(_read_.BACONSBF))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if g_PERAGE="S"
              this.oParentObject.w_MVCODAGE = this.w_COAGE
              if not empty(this.oParentObject.w_MVCODAGE)
                * --- Read from AGENTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.AGENTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "AGCATPRO,AGSCOPAG"+;
                    " from "+i_cTable+" AGENTI where ";
                        +"AGCODAGE = "+cp_ToStrODBC(this.oParentObject.w_MVCODAGE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    AGCATPRO,AGSCOPAG;
                    from (i_cTable) where;
                        AGCODAGE = this.oParentObject.w_MVCODAGE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_AGEPRO = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
                  this.oParentObject.w_AGSCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.oParentObject.w_MVCODAG2 = this.w_COAG2
              if not empty(this.oParentObject.w_MVCODAG2)
                * --- Read from AGENTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.AGENTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "AGCATPRO,AGSCOPAG"+;
                    " from "+i_cTable+" AGENTI where ";
                        +"AGCODAGE = "+cp_ToStrODBC(this.oParentObject.w_MVCODAG2);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    AGCATPRO,AGSCOPAG;
                    from (i_cTable) where;
                        AGCODAGE = this.oParentObject.w_MVCODAG2;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_AGEPR2 = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
                  this.oParentObject.w_AGSCOPA2 = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
            if NOT EMPTY(this.w_COPAG)
              * --- Se non � vuoto il codice pagamento sul documento di origine,
              *     importo anche questo.
              this.oParentObject.w_MVCODPAG = this.w_COPAG
              this.oParentObject.w_DESPAG = SPACE(30)
              this.oParentObject.w_VALINC = SPACE(3)
              this.oParentObject.w_VALIN2 = SPACE(3)
              this.oParentObject.w_SPEINC = 0
              this.oParentObject.w_SPEIN2 = 0
              if NOT EMPTY(this.oParentObject.w_MVCODPAG)
                * --- Read from PAG_AMEN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PADESCRI"+cp_TransInsFldName("PADESCRI")+",PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PADTOBSO"+;
                    " from "+i_cTable+" PAG_AMEN where ";
                        +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODPAG);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PADESCRI,PADESCRI,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PADTOBSO;
                    from (i_cTable) where;
                        PACODICE = this.oParentObject.w_MVCODPAG;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_DESPAG = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
                  this.oParentObject.w_VALINC = NVL(cp_ToDate(_read_.PAVALINC),cp_NullValue(_read_.PAVALINC))
                  this.oParentObject.w_SPEINC = NVL(cp_ToDate(_read_.PASPEINC),cp_NullValue(_read_.PASPEINC))
                  this.oParentObject.w_VALIN2 = NVL(cp_ToDate(_read_.PAVALIN2),cp_NullValue(_read_.PAVALIN2))
                  this.oParentObject.w_SPEIN2 = NVL(cp_ToDate(_read_.PASPEIN2),cp_NullValue(_read_.PASPEIN2))
                  this.oParentObject.w_DTOBSOPA = NVL(cp_ToDate(_read_.PADTOBSO),cp_NullValue(_read_.PADTOBSO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Ricalcola le Spese di Incasso
                this.w_PADRE.NotifyEvent("CalcolaSpeseIncasso")     
                if empty(this.oParentObject.w_SPEINC) and empty(this.oParentObject.w_MVSPEINC)
                  * --- Se vuote le spese incasso del pagamento mantiene quelle del doc. d'origine
                  this.oParentObject.w_MVSPEINC = this.w_OLDSPEINC
                endif
              endif
              * --- Importa le rate Scadenze solo se ho scadenze confermate 
              *     altrimenti le lascio ricalcolare dal documento di destinazione
              if this.w_FLSCAF = "S" And Not this.oParentObject.w_VARVAL $"+-"
                DIMENSION RA[1000, 11]
                FOR i = 1 TO 51
                RA[i, 1] = cp_CharToDate("  -  -  ")
                RA[i, 2] = 0
                RA[i, 5] = "  "
                RA[i, 6] = "  "
                RA[i, 7] = "  "
                RA[i, 8] = "  "
                RA[i, 9] = " "
                RA[i, 10] = " "
                RA[i, 11] = " "
                ENDFOR
                * --- Passa al Calcolo delle Rate il Parametro
                RA[1000, 2] = -999
                RA[1000, 5] = "F"
                i = 0
                * --- Select from DOC_RATE
                i_nConn=i_TableProp[this.DOC_RATE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
                      +" where RSSERIAL="+cp_ToStrODBC(this.w_APPOSER)+"";
                       ,"_Curs_DOC_RATE")
                else
                  select * from (i_cTable);
                   where RSSERIAL=this.w_APPOSER;
                    into cursor _Curs_DOC_RATE
                endif
                if used('_Curs_DOC_RATE')
                  select _Curs_DOC_RATE
                  locate for 1=1
                  do while not(eof())
                  this.w_APPO1 = _Curs_DOC_RATE.RSDATRAT
                  this.w_APPO2 = _Curs_DOC_RATE.RSIMPRAT
                  if this.oParentObject.w_MVCODVAL<>this.w_MCODVAL AND this.w_MCAORIF<>0 AND this.w_APPO2<>0
                    * --- Se documenti di differenti valute Riporta alla Valuta del Documento di Destinazione
                    this.w_APPO2 = cp_ROUND(this.w_APPO2 / this.w_MCAORIF, this.oParentObject.w_DECTOT)
                  endif
                  this.w_APPO3 = NVL(_Curs_DOC_RATE.RSMODPAG, SPACE(10))
                  this.w_APPO4 = NVL(_Curs_DOC_RATE.RSDESRIG, " ")
                  this.w_APPO7 = NVL(_Curs_DOC_RATE.RSBANNOS, SPACE(15))
                  this.w_APPO8 = NVL(_Curs_DOC_RATE.RSBANAPP, SPACE(10))
                  this.w_APPO9 = NVL(_Curs_DOC_RATE.RSFLSOSP, " ")
                  this.w_APPO10 = NVL(_Curs_DOC_RATE.RSCONCOR, SPACE(25))
                  i = i + 1
                  RA[i, 1] = this.w_APPO1
                  RA[i, 2] = this.w_APPO2
                  RA[i, 5] = this.w_APPO3
                  RA[i, 6] = NVL(_Curs_DOC_RATE.RSFLPROV, " ")
                  RA[i, 7] = this.w_APPO7
                  RA[i, 8] = this.w_APPO8
                  RA[i, 9] = this.w_APPO9
                  RA[i, 10] = this.w_APPO10
                  RA[i, 11] = this.w_APPO4
                    select _Curs_DOC_RATE
                    continue
                  enddo
                  use
                endif
                * --- Aggiorna archivio rate Scadenze (GSVE_BRT)
                this.w_PADRE.GSVE_MRT.NotifyEvent("CaricaRate")
              endif
            endif
          endif
        endif
      endif
    endif
    * --- Importa le Spese Accessorie e/o Sconti Forzati e/o Acconti
    if this.w_SPEIMB<>0 OR this.w_SPETRA<>0 OR this.w_SPEBOL<>0 OR this.w_FLFOSC="S" OR this.w_ACCONT<>0 OR this.w_ACCPRE<>0 OR this.w_FLSFIN="S" OR this.w_CAUIMB<>0 
      * --- ATTENZIONE: Si presume che sia Importabile un solo Documento con Spese Accessorie (non importa se uno solo o il primo dell'elenco)
      if this.w_RECTRS=0 And this.oParentObject.w_MVTIPIMB <> "C"
        * --- Se non ho righe nel dettaglio e il documento di destinazione non gestisce le cauzioni
        *     svuoto il campo che magari � pieno per una precedente importazione ed eliminazione tramite F6
        *     Se invece gestisco le cauzioni, l'utente potrebbe avere gi� inserito manualmente un valore e non posso cancellarlo
        this.oParentObject.w_MVCAUIMB = 0
      endif
      if this.oParentObject.w_MVCLADOC="RF"
        this.oParentObject.w_MVSPEIMB = 0
        this.oParentObject.w_MVSPETRA = 0
        this.oParentObject.w_MVACCSUC = IIF(this.w_CLADOC="OR", IIF(this.w_FLCAPA="S", this.w_ACCSUC,0), this.w_ACCSUC1)
        this.oParentObject.w_MVSPEBOL = this.w_SPEBOL
        this.oParentObject.w_MVCAUIMB = this.w_CAUIMB
      else
        if this.w_TCF=this.oParentObject.w_MVTIPCON
          if this.w_AZZSPE = 0
            * --- Al primo documento che trovo dello stesso ciclo azzero le spese
            this.oParentObject.w_MVSPEIMB = 0
            this.oParentObject.w_MVSPETRA = 0
            this.oParentObject.w_MVACCPRE = 0
            this.oParentObject.w_MVCAUIMB = 0
            this.oParentObject.w_Impon_Cpa = 0
            this.oParentObject.w_Impon_Gen = 0
            * --- Memorizzo il fatto che ho gi� azzerato
            this.w_AZZSPE = 1
          endif
          * --- Solo se importazione dallo stesso ciclo documentale
          *     Altrimenti devono rimanere quelle impostate sul documento di destinazione
          if Isalt()
            if this.oParentObject.w_FLGMSPE="S" and this.w_LFLACCO<>"S"
              if this.oParentObject.w_FLGMSPE="S" and Not Empty(this.w_TDMCALSI)
                this.oParentObject.w_MVSPEIMB = this.oParentObject.w_MVSPEIMB + this.w_SPEIMB
                this.oParentObject.w_Impon_Gen = this.oParentObject.w_Impon_Gen + this.w_SPEIMB
              endif
              if this.oParentObject.w_FLGMSPE="S" and Not Empty(this.w_TDMCALST)
                this.oParentObject.w_MVSPETRA = this.oParentObject.w_MVSPETRA + this.w_SPETRA
                this.oParentObject.w_Impon_Cpa = this.oParentObject.w_Impon_Cpa + this.w_SPETRA
              endif
            endif
          else
            this.oParentObject.w_MVSPEIMB = this.oParentObject.w_MVSPEIMB + this.w_SPEIMB
            this.oParentObject.w_MVSPETRA = this.oParentObject.w_MVSPETRA + this.w_SPETRA
          endif
          this.oParentObject.w_MVACCPRE = this.oParentObject.w_MVACCPRE + (this.w_ACCONT+this.w_ACCPRE)
          this.oParentObject.w_MVSPEBOL = this.w_SPEBOL
          this.oParentObject.w_MVCAUIMB = this.oParentObject.w_MVCAUIMB + this.w_CAUIMB
          * --- Appena uno dei documenti di origine ha il flag forza cauzioni attivo, lo attivo anche su quello di destinazione
          if this.w_FLFOCA = "S"
            this.oParentObject.w_MVFLFOCA = "S"
          endif
        endif
      endif
      if this.w_FLFOSC="S" And this.w_TCF=this.oParentObject.w_MVTIPCON
        this.oParentObject.w_MVFLFOSC = "S"
        this.oParentObject.w_MVSCONTI = this.oParentObject.w_MVSCONTI + this.w_SCOFIS
      endif
      * --- Forza Sconto Finanziario
      if this.w_FLSFIN="S" And this.w_TCF=this.oParentObject.w_MVTIPCON 
        if this.w_LFLACCO <>"S"
          this.oParentObject.w_MVFLSFIN = "S"
          this.oParentObject.w_MVIMPFIN = this.w_SCOFIN
        else
          if this.w_TESTMESS=0
            this.w_TESTMESS = 2
            this.w_oPART = this.w_oMESS.addmsgpartNL("Attenzione lo sconto finanziario presente nella fattura di anticipo N. %1 del %2 non verr� importato nel documento di destinazione")
            this.w_oPART.addParam(Alltrim(Str(this.w_NRIF,15))+IIF(Not Empty(this.w_Arif),"/","")+Alltrim(this.w_Arif))     
            this.w_oPART.addParam(DTOC(this.w_Drif))     
          endif
        endif
      endif
      * --- Testa che sono stati importati acconti per dare avviso
      this.w_FLAPRE = IIF(this.w_ACCONT<>0, .T., this.w_FLAPRE)
    endif
    * --- Controllo dati aggiuntivi
    if !(this.w_NEWDOC = "zzzzzzzzzz" And this.oParentObject.cFunction="Load")
      if !this.w_BMESSAGG01 AND this.oParentObject.w_TDFLIA01="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_01, " ")) AND NVL(this.oParentObject.w_MVAGG_01, " ")<>nvl(this.w_AGG_01,"")
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_01))     
        this.w_BMESSAGG01 = .T.
        this.w_TESTMESS = 3
      endif
      if !this.w_BMESSAGG02 AND this.oParentObject.w_TDFLIA02="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_02, " ")) AND NVL(this.oParentObject.w_MVAGG_02, " ")<>nvl(this.w_AGG_02,"")
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_02))     
        this.w_BMESSAGG02 = .T.
        this.w_TESTMESS = 3
      endif
      if !this.w_BMESSAGG03 AND this.oParentObject.w_TDFLIA03="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_03, " ")) AND NVL(this.oParentObject.w_MVAGG_03, " ")<>nvl(this.w_AGG_03,"")
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_03))     
        this.w_BMESSAGG03 = .T.
        this.w_TESTMESS = 3
      endif
      if !this.w_BMESSAGG04 AND this.oParentObject.w_TDFLIA04="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_04, " ")) AND NVL(this.oParentObject.w_MVAGG_04, " ")<>nvl(this.w_AGG_04,"")
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_04))     
        this.w_BMESSAGG04 = .T.
        this.w_TESTMESS = 3
      endif
      if !this.w_BMESSAGG05 AND this.oParentObject.w_TDFLIA05="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_05, " ")) AND NVL(this.oParentObject.w_MVAGG_05, cp_charToDate("  -  -    "))<>nvl(this.w_AGG_05,cp_charToDate("  -  -    "))
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_05))     
        this.w_BMESSAGG05 = .T.
        this.w_TESTMESS = 3
      endif
      if !this.w_BMESSAGG06 AND this.oParentObject.w_TDFLIA06="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_06, " ")) AND NVL(this.oParentObject.w_MVAGG_06, cp_charToDate("  -  -    "))<>nvl(this.w_AGG_06,cp_charToDate("  -  -    "))
        this.w_oPART = this.w_oMESS.addmsgpartNL("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato%0")
        this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_06))     
        this.w_BMESSAGG06 = .T.
        this.w_TESTMESS = 3
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizioni Variabili
    * --- Data obsolescenza codice pagamento
    * --- Data obsolescenza codice Banca associata al cliente
    * --- Variabili Lotti
    * --- Variabile Unit� Logistica
    * --- Variabili Locali
    * --- Variabili Lotti
    * --- MESBLOK -> Utilizzata nella Maschera GSVE_KLG ma utilizzata solo nel GSVE_BI2
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Riga nel temporaneo
    * --- Aggiungo una riga sul Temporaneo
    this.w_PADRE.AddRow()     
    this.w_PADRE.SaveDependsOn()     
    this.w_PADRE.ChildrenChangeRow()     
    if this.w_TESTRAG
      * --- Se questa pagina � lanciata da Raggruppamento Righe (Pag 7)
      *     Inserisco il CPROWNUM della riga di destinazione nel cursore dove 
      *     memorizzo la chiave Righe di Origine -> Riga Destinazione
       
 Select ( L_Cur_Rag_Row ) 
 Replace ROWDES With this.w_Padre.w_CPROWNUM For ROWDES = 0
    endif
    if g_COAC="S"
      if this.w_RIFCAC>0
        this.oParentObject.w_CPROWORD = this.w_RIFCACORD
      endif
      * --- Mi marco la prima riga importata per poter gestire la riattibuzione
      *     dei contributi sulle righe importate (pag1)
      if this.w_MAXROWNUM=0
        this.w_MAXROWNUM = this.oParentObject.w_CPROWNUM
      endif
    endif
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    SELECT RigheSort
    this.oParentObject.w_STSERRIF = MVSERIAL
    this.oParentObject.w_MVSERRIF = MVSERIAL
    this.oParentObject.w_MVROWRIF = CPROWNUM
    this.w_VISEVA = MVIMPEVA
    this.oParentObject.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.oParentObject.w_MVCAUCOL = this.w_MCAUCOL
    this.oParentObject.w_MVFLCASC = this.w_MFLCASC
    this.oParentObject.w_MVF2CASC = this.w_MF2CASC
    this.oParentObject.w_MVFLORDI = this.w_MFLORDI
    this.oParentObject.w_MVF2ORDI = this.w_MF2ORDI
    this.oParentObject.w_MVFLIMPE = this.w_MFLIMPE
    this.oParentObject.w_MVF2IMPE = this.w_MF2IMPE
    this.oParentObject.w_MVFLRISE = this.w_MFLRISE
    this.oParentObject.w_MVF2RISE = this.w_MF2RISE
    this.oParentObject.w_MV_FLAGG = this.w_FLAGG
    this.w_AGGSAL = ALLTRIM(this.w_CASC+this.w_RISE+this.w_ORDI+this.w_IMPE)
    this.w_AGGSAL1 = ALLTRIM(this.w_2CASC+this.w_2RISE+this.w_2ORDI+this.w_2IMPE)
    this.oParentObject.w_FLCASC = this.w_CASC
    this.oParentObject.w_F2CASC = this.w_2CASC
    this.oParentObject.w_FLORDI = this.w_ORDI
    this.oParentObject.w_F2ORDI = this.w_2ORDI
    this.oParentObject.w_FLIMPE = this.w_IMPE
    this.oParentObject.w_F2IMPE = this.w_2IMPE
    this.oParentObject.w_FLRISE = this.w_RISE
    this.oParentObject.w_F2RISE = this.w_2RISE
    this.oParentObject.w_MVFLELGM = this.w_MFLELGM
    this.oParentObject.w_FLAVA1 = this.w_MFLAVAL
    this.oParentObject.w_FLCOMM = this.w_TFLCOMM
    this.w_APPO1 = 0
    this.w_APPO2 = 0
    this.w_APPO4 = 0
    this.oParentObject.w_MVCODLIS = SPACE(5)
    this.oParentObject.w_MVCONTRA = SPACE(15)
    this.w_ORIMAT = SPACE(5)
    this.oParentObject.w_DOQTAEV1 = DOQTAEV1
    this.oParentObject.w_DOIMPEVA = DOIMPEVA
    this.oParentObject.w_MVRIFORD = " "
    this.w_RIFORD = " "
    this.oParentObject.w_SPEGEN = 0
    if Isalt() and this.w_ROWPRE=-1
      this.w_PRSERIAL = RigheSort.PRSERIAL
      this.oParentObject.w_MVCODIVA = RigheSort.ARCODIVA
      this.oParentObject.w_MVRIFPRE = RigheSort.MVRIFPRE
      this.w_PRSERAGG = RigheSort.PRSERAGG
      * --- Read from PRE_STAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRATTIVI,PRCENCOS,PRNUMPRA,PRCODATT,PRDESPRE,PRDESAGG,PRFINCOM,PRINICOM,PRPREZZO,PRQTAMOV,PRUNIMIS,PRVOCCOS,PRCODRES,PR__DATA,PRRIGPRE,PROREEFF,PRMINEFF,PRPREMIN,PRPREMAX,PRGAZUFF,PRCOSINT,PRTIPDOC,PRQTAUM1"+;
          " from "+i_cTable+" PRE_STAZ where ";
              +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRATTIVI,PRCENCOS,PRNUMPRA,PRCODATT,PRDESPRE,PRDESAGG,PRFINCOM,PRINICOM,PRPREZZO,PRQTAMOV,PRUNIMIS,PRVOCCOS,PRCODRES,PR__DATA,PRRIGPRE,PROREEFF,PRMINEFF,PRPREMIN,PRPREMAX,PRGAZUFF,PRCOSINT,PRTIPDOC,PRQTAUM1;
          from (i_cTable) where;
              PRSERIAL = this.w_PRSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MVCODATT = NVL(cp_ToDate(_read_.PRATTIVI),cp_NullValue(_read_.PRATTIVI))
        this.oParentObject.w_MVCODCEN = NVL(cp_ToDate(_read_.PRCENCOS),cp_NullValue(_read_.PRCENCOS))
        this.oParentObject.w_MVCODCOM = NVL(cp_ToDate(_read_.PRNUMPRA),cp_NullValue(_read_.PRNUMPRA))
        this.oParentObject.w_MVCODICE = NVL(cp_ToDate(_read_.PRCODATT),cp_NullValue(_read_.PRCODATT))
        this.oParentObject.w_MVDESART = NVL(cp_ToDate(_read_.PRDESPRE),cp_NullValue(_read_.PRDESPRE))
        this.oParentObject.w_MVDESSUP = NVL(cp_ToDate(_read_.PRDESAGG),cp_NullValue(_read_.PRDESAGG))
        this.oParentObject.w_MVFINCOM = NVL(cp_ToDate(_read_.PRFINCOM),cp_NullValue(_read_.PRFINCOM))
        this.oParentObject.w_MVINICOM = NVL(cp_ToDate(_read_.PRINICOM),cp_NullValue(_read_.PRINICOM))
        this.oParentObject.w_MVPREZZO = NVL(cp_ToDate(_read_.PRPREZZO),cp_NullValue(_read_.PRPREZZO))
        this.w_APPO1 = NVL(cp_ToDate(_read_.PRQTAMOV),cp_NullValue(_read_.PRQTAMOV))
        this.oParentObject.w_MVUNIMIS = NVL(cp_ToDate(_read_.PRUNIMIS),cp_NullValue(_read_.PRUNIMIS))
        this.oParentObject.w_MVVOCCEN = NVL(cp_ToDate(_read_.PRVOCCOS),cp_NullValue(_read_.PRVOCCOS))
        this.oParentObject.w_MVCODRES = NVL(cp_ToDate(_read_.PRCODRES),cp_NullValue(_read_.PRCODRES))
        this.w_DATOAI = NVL(cp_ToDate(_read_.PR__DATA),cp_NullValue(_read_.PR__DATA))
        this.oParentObject.w_MVRIGPRE = NVL(cp_ToDate(_read_.PRRIGPRE),cp_NullValue(_read_.PRRIGPRE))
        this.oParentObject.w_DEOREEFF = NVL(cp_ToDate(_read_.PROREEFF),cp_NullValue(_read_.PROREEFF))
        this.oParentObject.w_DEMINEFF = NVL(cp_ToDate(_read_.PRMINEFF),cp_NullValue(_read_.PRMINEFF))
        this.oParentObject.w_DEPREMIN = NVL(cp_ToDate(_read_.PRPREMIN),cp_NullValue(_read_.PRPREMIN))
        this.oParentObject.w_DEPREMAX = NVL(cp_ToDate(_read_.PRPREMAX),cp_NullValue(_read_.PRPREMAX))
        this.oParentObject.w_DEGAZUFF = NVL(cp_ToDate(_read_.PRGAZUFF),cp_NullValue(_read_.PRGAZUFF))
        this.oParentObject.w_DECOSINT = NVL(cp_ToDate(_read_.PRCOSINT),cp_NullValue(_read_.PRCOSINT))
        this.w_DCCOLLEG = NVL(cp_ToDate(_read_.PRTIPDOC),cp_NullValue(_read_.PRTIPDOC))
        this.w_QTAUM1 = NVL(cp_ToDate(_read_.PRQTAUM1),cp_NullValue(_read_.PRQTAUM1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MVSERRIF;
              and CPROWNUM = this.oParentObject.w_MVROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MVCATCON = NVL(cp_ToDate(_read_.MVCATCON),cp_NullValue(_read_.MVCATCON))
        this.oParentObject.w_MVCODATT = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
        this.oParentObject.w_MVCODCEN = NVL(cp_ToDate(_read_.MVCODCEN),cp_NullValue(_read_.MVCODCEN))
        this.oParentObject.w_MVCODCES = NVL(cp_ToDate(_read_.MVCODCES),cp_NullValue(_read_.MVCODCES))
        this.oParentObject.w_MVCODCLA = NVL(cp_ToDate(_read_.MVCODCLA),cp_NullValue(_read_.MVCODCLA))
        this.oParentObject.w_MVCODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
        this.oParentObject.w_MVCODICE = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
        this.oParentObject.w_MVCODIVA = NVL(cp_ToDate(_read_.MVCODIVA),cp_NullValue(_read_.MVCODIVA))
        this.oParentObject.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        this.oParentObject.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
        this.w_ORIMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
        this.oParentObject.w_MVCONIND = NVL(cp_ToDate(_read_.MVCONIND),cp_NullValue(_read_.MVCONIND))
        this.w_CONTRA = NVL(cp_ToDate(_read_.MVCONTRA),cp_NullValue(_read_.MVCONTRA))
        this.oParentObject.w_MVCONTRO = NVL(cp_ToDate(_read_.MVCONTRO),cp_NullValue(_read_.MVCONTRO))
        this.w_DATEVA = NVL(cp_ToDate(_read_.MVDATEVA),cp_NullValue(_read_.MVDATEVA))
        this.oParentObject.w_MVDESART = NVL(cp_ToDate(_read_.MVDESART),cp_NullValue(_read_.MVDESART))
        this.oParentObject.w_MVDESSUP = NVL(cp_ToDate(_read_.MVDESSUP),cp_NullValue(_read_.MVDESSUP))
        this.oParentObject.w_MVFINCOM = NVL(cp_ToDate(_read_.MVFINCOM),cp_NullValue(_read_.MVFINCOM))
        this.w_ORCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
        this.oParentObject.w_FGMRIF = NVL(cp_ToDate(_read_.MVFLELGM),cp_NullValue(_read_.MVFLELGM))
        this.oParentObject.w_OLDEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
        this.oParentObject.w_MVFLOMAG = NVL(cp_ToDate(_read_.MVFLOMAG),cp_NullValue(_read_.MVFLOMAG))
        this.oParentObject.w_MVFLRAGG = NVL(cp_ToDate(_read_.MVFLRAGG),cp_NullValue(_read_.MVFLRAGG))
        this.oParentObject.w_FLRRIF = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
        this.oParentObject.w_MVFLTRAS = NVL(cp_ToDate(_read_.MVFLTRAS),cp_NullValue(_read_.MVFLTRAS))
        this.oParentObject.w_MVIMPACC = NVL(cp_ToDate(_read_.MVIMPACC),cp_NullValue(_read_.MVIMPACC))
        this.oParentObject.w_ORDEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
        this.oParentObject.w_MVIMPPRO = NVL(cp_ToDate(_read_.MVIMPPRO),cp_NullValue(_read_.MVIMPPRO))
        this.oParentObject.w_MVIMPSCO = NVL(cp_ToDate(_read_.MVIMPSCO),cp_NullValue(_read_.MVIMPSCO))
        this.oParentObject.w_MVINICOM = NVL(cp_ToDate(_read_.MVINICOM),cp_NullValue(_read_.MVINICOM))
        this.oParentObject.w_MVMOLSUP = NVL(cp_ToDate(_read_.MVMOLSUP),cp_NullValue(_read_.MVMOLSUP))
        this.oParentObject.w_MVNOMENC = NVL(cp_ToDate(_read_.MVNOMENC),cp_NullValue(_read_.MVNOMENC))
        this.oParentObject.w_MVNUMCOL = NVL(cp_ToDate(_read_.MVNUMCOL),cp_NullValue(_read_.MVNUMCOL))
        this.oParentObject.w_MVNUMRIF = NVL(cp_ToDate(_read_.MVNUMRIF),cp_NullValue(_read_.MVNUMRIF))
        this.oParentObject.w_MVPERPRO = NVL(cp_ToDate(_read_.MVPERPRO),cp_NullValue(_read_.MVPERPRO))
        this.oParentObject.w_MVPESNET = NVL(cp_ToDate(_read_.MVPESNET),cp_NullValue(_read_.MVPESNET))
        this.oParentObject.w_MVPREZZO = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
        this.w_APPO2 = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
        this.w_APPO1 = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.oParentObject.w_MVSCONT1 = NVL(cp_ToDate(_read_.MVSCONT1),cp_NullValue(_read_.MVSCONT1))
        this.oParentObject.w_MVSCONT2 = NVL(cp_ToDate(_read_.MVSCONT2),cp_NullValue(_read_.MVSCONT2))
        this.oParentObject.w_MVSCONT3 = NVL(cp_ToDate(_read_.MVSCONT3),cp_NullValue(_read_.MVSCONT3))
        this.oParentObject.w_MVSCONT4 = NVL(cp_ToDate(_read_.MVSCONT4),cp_NullValue(_read_.MVSCONT4))
        this.oParentObject.w_MVTIPPRO = NVL(cp_ToDate(_read_.MVTIPPRO),cp_NullValue(_read_.MVTIPPRO))
        this.oParentObject.w_MVTIPRIG = NVL(cp_ToDate(_read_.MVTIPRIG),cp_NullValue(_read_.MVTIPRIG))
        this.oParentObject.w_MVUMSUPP = NVL(cp_ToDate(_read_.MVUMSUPP),cp_NullValue(_read_.MVUMSUPP))
        this.oParentObject.w_MVUNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
        this.oParentObject.w_MVVOCCEN = NVL(cp_ToDate(_read_.MVVOCCEN),cp_NullValue(_read_.MVVOCCEN))
        this.oParentObject.w_MVUNILOG = NVL(cp_ToDate(_read_.MVUNILOG),cp_NullValue(_read_.MVUNILOG))
        this.oParentObject.w_MVTIPPR2 = NVL(cp_ToDate(_read_.MVTIPPR2),cp_NullValue(_read_.MVTIPPR2))
        this.oParentObject.w_MVPROCAP = NVL(cp_ToDate(_read_.MVPROCAP),cp_NullValue(_read_.MVPROCAP))
        this.oParentObject.w_MVIMPCAP = NVL(cp_ToDate(_read_.MVIMPCAP),cp_NullValue(_read_.MVIMPCAP))
        this.oParentObject.w_MVFLNOAN = NVL(cp_ToDate(_read_.MVFLNOAN),cp_NullValue(_read_.MVFLNOAN))
        this.oParentObject.w_MVPROORD = NVL(cp_ToDate(_read_.MVPROORD),cp_NullValue(_read_.MVPROORD))
        this.oParentObject.w_MVNAZPRO = NVL(cp_ToDate(_read_.MVNAZPRO),cp_NullValue(_read_.MVNAZPRO))
        this.oParentObject.w_MVCODRES = NVL(cp_ToDate(_read_.MVCODRES),cp_NullValue(_read_.MVCODRES))
        this.w_DATOAI = NVL(cp_ToDate(_read_.MVDATOAI),cp_NullValue(_read_.MVDATOAI))
        this.w_NAZPRO = NVL(cp_ToDate(_read_.MVNAZPRO),cp_NullValue(_read_.MVNAZPRO))
        this.oParentObject.w_MVCACONT = NVL(cp_ToDate(_read_.MVCACONT),cp_NullValue(_read_.MVCACONT))
        this.oParentObject.w_MVPAEFOR = NVL(cp_ToDate(_read_.MVPAEFOR),cp_NullValue(_read_.MVPAEFOR))
        this.w_RIFORD = NVL(cp_ToDate(_read_.MVRIFORD),cp_NullValue(_read_.MVRIFORD))
        this.oParentObject.w_MVRIFPRE = NVL(cp_ToDate(_read_.MVRIFPRE),cp_NullValue(_read_.MVRIFPRE))
        this.oParentObject.w_MVRIGPRE = NVL(cp_ToDate(_read_.MVRIGPRE),cp_NullValue(_read_.MVRIGPRE))
        this.w_OR2CASC = NVL(cp_ToDate(_read_.MVF2CASC),cp_NullValue(_read_.MVF2CASC))
        this.w_FL2RRIF = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PRSERAGG = this.oParentObject.w_MVSERRIF
    endif
    if IsAlt()
      * --- Legge dettaglio da tabella esterna
      if this.w_ROWPRE=-1
        this.oParentObject.w_MVTIPRIG = RigheSort.MVTIPRIG
        this.oParentObject.w_MVCODART = RigheSort.MVCODART
        this.oParentObject.w_MVROWRIF = Val(this.w_PRSERIAL)
        if ! this.oParentObject.w_PRTIPCAU $ "A-P"
          * --- Se fattura \proforma d'acconto non devo marcarla come evasa
          this.oParentObject.w_MVNUMRIF = -70
        endif
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNCONTRA,CNMATPRA,CNTIPPRA"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNCONTRA,CNMATPRA,CNTIPPRA;
            from (i_cTable) where;
                CNCODCAN = this.oParentObject.w_MVCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESPRA = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          this.oParentObject.w_CNFLAPON = NVL(cp_ToDate(_read_.CNFLAPON),cp_NullValue(_read_.CNFLAPON))
          this.oParentObject.w_CN__ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
          this.oParentObject.w_CNUFFICI = NVL(cp_ToDate(_read_.CNUFFICI),cp_NullValue(_read_.CNUFFICI))
          this.oParentObject.w_CNFLVALO = NVL(cp_ToDate(_read_.CNFLVALO),cp_NullValue(_read_.CNFLVALO))
          this.oParentObject.w_CNIMPORT = NVL(cp_ToDate(_read_.CNIMPORT),cp_NullValue(_read_.CNIMPORT))
          this.oParentObject.w_CNCALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
          this.oParentObject.w_CNCOECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
          this.oParentObject.w_CNPARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
          this.oParentObject.w_CNFLAMPA = NVL(cp_ToDate(_read_.CNFLAMPA),cp_NullValue(_read_.CNFLAMPA))
          this.oParentObject.w_CONTRACN = NVL(cp_ToDate(_read_.CNCONTRA),cp_NullValue(_read_.CNCONTRA))
          this.oParentObject.w_DEMATPRA = NVL(cp_ToDate(_read_.CNMATPRA),cp_NullValue(_read_.CNMATPRA))
          this.oParentObject.w_DETIPPRA = NVL(cp_ToDate(_read_.CNTIPPRA),cp_NullValue(_read_.CNTIPPRA))
          this.oParentObject.w_DEPARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
          this.oParentObject.w_DECALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
          this.oParentObject.w_DECOECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from ALT_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ALT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DEOREEFF,DEMINEFF,DEPREMIN,DEPREMAX,DEGAZUFF,DEMATPRA,DETIPPRA,DECOSINT,DEPARASS,DECALDIR,DECOECAL"+;
            " from "+i_cTable+" ALT_DETT where ";
                +"DESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
                +" and DEROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DEOREEFF,DEMINEFF,DEPREMIN,DEPREMAX,DEGAZUFF,DEMATPRA,DETIPPRA,DECOSINT,DEPARASS,DECALDIR,DECOECAL;
            from (i_cTable) where;
                DESERIAL = this.oParentObject.w_MVSERRIF;
                and DEROWNUM = this.oParentObject.w_MVROWRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DEOREEFF = NVL(cp_ToDate(_read_.DEOREEFF),cp_NullValue(_read_.DEOREEFF))
          this.oParentObject.w_DEMINEFF = NVL(cp_ToDate(_read_.DEMINEFF),cp_NullValue(_read_.DEMINEFF))
          this.oParentObject.w_DEPREMIN = NVL(cp_ToDate(_read_.DEPREMIN),cp_NullValue(_read_.DEPREMIN))
          this.oParentObject.w_DEPREMAX = NVL(cp_ToDate(_read_.DEPREMAX),cp_NullValue(_read_.DEPREMAX))
          this.oParentObject.w_DEGAZUFF = NVL(cp_ToDate(_read_.DEGAZUFF),cp_NullValue(_read_.DEGAZUFF))
          this.oParentObject.w_DEMATPRA = NVL(cp_ToDate(_read_.DEMATPRA),cp_NullValue(_read_.DEMATPRA))
          this.oParentObject.w_DETIPPRA = NVL(cp_ToDate(_read_.DETIPPRA),cp_NullValue(_read_.DETIPPRA))
          this.oParentObject.w_DECOSINT = NVL(cp_ToDate(_read_.DECOSINT),cp_NullValue(_read_.DECOSINT))
          this.oParentObject.w_DEPARASS = NVL(cp_ToDate(_read_.DEPARASS),cp_NullValue(_read_.DEPARASS))
          this.oParentObject.w_DECALDIR = NVL(cp_ToDate(_read_.DECALDIR),cp_NullValue(_read_.DECALDIR))
          this.oParentObject.w_DECOECAL = NVL(cp_ToDate(_read_.DECOECAL),cp_NullValue(_read_.DECOECAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNCONTRA"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNCONTRA;
            from (i_cTable) where;
                CNCODCAN = this.oParentObject.w_MVCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESPRA = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          this.oParentObject.w_CNFLAPON = NVL(cp_ToDate(_read_.CNFLAPON),cp_NullValue(_read_.CNFLAPON))
          this.oParentObject.w_CN__ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
          this.oParentObject.w_CNUFFICI = NVL(cp_ToDate(_read_.CNUFFICI),cp_NullValue(_read_.CNUFFICI))
          this.oParentObject.w_CNFLVALO = NVL(cp_ToDate(_read_.CNFLVALO),cp_NullValue(_read_.CNFLVALO))
          this.oParentObject.w_CNIMPORT = NVL(cp_ToDate(_read_.CNIMPORT),cp_NullValue(_read_.CNIMPORT))
          this.oParentObject.w_CNCALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
          this.oParentObject.w_CNCOECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
          this.oParentObject.w_CNPARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
          this.oParentObject.w_CNFLAMPA = NVL(cp_ToDate(_read_.CNFLAMPA),cp_NullValue(_read_.CNFLAMPA))
          this.oParentObject.w_CONTRACN = NVL(cp_ToDate(_read_.CNCONTRA),cp_NullValue(_read_.CNCONTRA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLTEMP,UMDURORE"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVUNIMIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLTEMP,UMDURORE;
          from (i_cTable) where;
              UMCODICE = this.oParentObject.w_MVUNIMIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
        this.oParentObject.w_DUR_ORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOSORA,DPCOGNOM,DPNOME"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODRES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOSORA,DPCOGNOM,DPNOME;
          from (i_cTable) where;
              DPCODICE = this.oParentObject.w_MVCODRES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_COST_ORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
        this.oParentObject.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
        this.oParentObject.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from PRA_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRA_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPTIPCON,CPLISCOL,CPSTATUS"+;
          " from "+i_cTable+" PRA_CONT where ";
              +"CPCODCON = "+cp_ToStrODBC(this.oParentObject.w_CONTRACN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPTIPCON,CPLISCOL,CPSTATUS;
          from (i_cTable) where;
              CPCODCON = this.oParentObject.w_CONTRACN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPCONCO = NVL(cp_ToDate(_read_.CPTIPCON),cp_NullValue(_read_.CPTIPCON))
        this.oParentObject.w_LISCOLCO = NVL(cp_ToDate(_read_.CPLISCOL),cp_NullValue(_read_.CPLISCOL))
        this.oParentObject.w_STATUSCO = NVL(cp_ToDate(_read_.CPSTATUS),cp_NullValue(_read_.CPSTATUS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_DENOM = alltrim(this.oParentObject.w_COGNOME)+" "+alltrim(this.oParentObject.w_NOME)
      if this.oParentObject.w_MVRIGPRE="S"
        this.w_INDROW = this.w_INDROW +1
         
 DIMENSION Rif_Row(this.w_INDROW, 2) 
 Rif_Row[this.w_INDROW, 1] = this.oParentObject.w_MVROWRIF 
 Rif_Row[this.w_INDROW, 2] = this.oParentObject.w_CPROWNUM
      endif
      if this.oParentObject.w_MVRIFPRE>0
        this.w_INDSEA = cp_round((ASCAN("Rif_Row",this.oParentObject.w_MVRIFPRE)/2),0)
        if this.w_INDSEA>0
          this.oParentObject.w_MVRIFPRE = Rif_Row[this.w_INDSEA, 2]
        else
          this.oParentObject.w_MVRIFPRE = 0
        endif
      endif
      if Not Empty(this.w_TDMCALSI) and this.oParentObject.w_FLGMSPE="S" and this.w_LFLACCO<>"S"
        this.oParentObject.w_FLGDSPE = "S"
      endif
    endif
    this.oParentObject.w_MVNAZPRO = IIF((this.w_TCF="C" AND this.oParentObject.w_MVTIPCON="F") or (this.w_TCF="F" AND this.oParentObject.w_MVTIPCON="C"),this.oParentObject.w_ONAZPRO,NVL(this.w_NAZPRO,space(3)))
    if VARTYPE( this.oParentObject.w_MVDATOAI ) <> "U"
      * --- w_MVDATOAI non � presente in tutte le gestioni
      this.oParentObject.w_MVDATOAI = this.w_DATOAI
    endif
    if this.oParentObject.w_BOLDOG = "S"
      this.oParentObject.w_MVFLOMAG = "I"
    endif
    if this.oParentObject.w_RICPROV<>"N"
      * --- Se devo ricalcolare le provvigioni reinizializzo i valori come se fossi in caricamento manuale
      *     altrimenti non calcola in modo corretto
      this.oParentObject.w_MVTIPPRO = IIF(this.oParentObject.w_MVTIPPRO$"FO-ES", this.oParentObject.w_MVTIPPRO,IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
      this.oParentObject.w_MVTIPPR2 = IIF(this.oParentObject.w_MVTIPPR2$"FO-ES", this.oParentObject.w_MVTIPPR2,IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")))
    endif
    if this.w_CLADOC="OR"
      * --- Se il documento d'origine � di tipo ordine leggo il livello di raggruppamento dal documento di destinazione
      this.oParentObject.w_MVFLRAGG = this.oParentObject.w_MVTFRAGG
    endif
    * --- Sbianco MVUNILOG se il documento di destinazione non movimenta esistenza o riservato
    this.oParentObject.w_MVUNILOG = IIF(NOT EMPTY(NVL(this.oParentObject.w_MVUNILOG," ")) AND EMPTY(this.oParentObject.w_MVFLCASC) AND EMPTY(this.oParentObject.w_MVFLRISE), SPACE(18), this.oParentObject.w_MVUNILOG)
    this.w_LFLRRIF = this.oParentObject.w_FLRRIF
    this.w_OPREZZO = this.oParentObject.w_MVPREZZO
    SELECT RigheSort
    if this.oParentObject.w_MVTIPRIG="F"
      this.oParentObject.w_MVQTAMOV = 1
      this.oParentObject.w_MVQTAIMP = 1
      this.w_VISEVA = IIF(NVL(MVIMPEVA, 0)<>0, MVIMPEVA, IIF(MVQTAEVA<>0, this.oParentObject.w_MVPREZZO-this.oParentObject.w_ORDEVA, this.w_VISEVA))
    else
      if this.w_QTALOT>0 And Not this.w_BCONTACC
        * --- Se consumo automatico
        this.oParentObject.w_MVCODMAG = this.w_CODMAG
      else
        this.oParentObject.w_MVQTAMOV = IIF( this.oParentObject.w_MVTIPRIG="D", 0 , NVL(MVQTAEVA,0) )
        this.oParentObject.w_MVQTAIMP = IIF( this.oParentObject.w_MVTIPRIG="D", 0 , MIN((MVQTAMOV-OLQTAEVA), MVQTAEVA) )
      endif
      this.w_VISEVA = NVL(MVIMPEVA,0)
    endif
    * --- Per eliminare eventuali differenze di arrotondamento sugli importi evasi
    this.w_VISEVA = IIF(this.w_VISEVA=cp_ROUND(this.oParentObject.w_MVPREZZO-this.oParentObject.w_ORDEVA, IIF(this.w_MCODVAL=g_CODLIR, 0, 2)), this.oParentObject.w_MVPREZZO-this.oParentObject.w_ORDEVA, this.w_VISEVA)
    if this.oParentObject.w_MVCODVAL=this.w_MCODVAL
      this.w_VISEVA = cp_ROUND(this.w_VISEVA, this.oParentObject.w_DECUNI)
    endif
    this.w_FLINTE = NVL(FLINTE," ")
    if Isalt() and this.oParentObject.w_FLGAQV="S"
      this.w_FLACCO = IIF(this.oParentObject.w_MVTIPRIG $ "F-M" AND this.w_FLINTE<>"S", NVL(FLACCO," "), " ")
    else
      this.w_FLACCO = IIF(this.oParentObject.w_MVTIPRIG="F" AND this.w_FLINTE<>"S", NVL(FLACCO," "), " ")
    endif
    this.oParentObject.w_MVFLERIF = " "
    if this.oParentObject.w_MVTIPRIG="D"
      * --- Nel caso di righe descrittive ho due situazioni
      *     a) Riga da evadere  (con check attivo), in questo caso valorizzo MVFLARIF
      *     b) Riga da riportare che non evade (MVQTAEVA<>0) sbianco MVFLARIF per invalidre il meccanismo dell'evasione
      this.oParentObject.w_MVFLARIF = IIF(this.w_FLINTE="S", " ", IIF( XCHK=1 , "+" , " " ) )
    else
      this.oParentObject.w_MVFLARIF = IIF(this.w_FLINTE="S", " ", IIF(this.w_FLACCO="S", "-", "+"))
      if ISALT()
        this.oParentObject.w_MVRIFORD = IIF(this.w_FLACCO = "S" , this.oParentObject.w_MVSERRIF, this.w_RIFORD)
        this.oParentObject.w_LRIFORD = this.oParentObject.w_MVRIFORD
      endif
    endif
    if this.w_ROWPRE<>-1 and Not Empty(this.oParentObject.w_MVRIFORD)
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSPEIMB"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFORD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSPEIMB;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MVRIFORD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_SPEGEN = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.oParentObject.w_CAORIF = iif(this.w_ROWPRE=-1,this.oParentObject.w_MVCAOVAL,this.w_MCAORIF)
    this.oParentObject.w_CLARIF = this.w_CLADOC
    this.oParentObject.w_ODLRIF = this.w_MRIFODL
    if Type("RigheSort.cMATR")<>"U"
      * --- Se da caricamento rapido mantengo il prezzo impostato su di esso
      *     e non lo ricerco sul documento di origine..
      this.oParentObject.w_MVPREZZO = Nvl(RigheSort.VISPRE, 0 )
    else
      * --- Se riga Forfettaria Importa x valore (Prende l'importo del Cursore)
      this.oParentObject.w_MVPREZZO = IIF(this.oParentObject.w_MVTIPRIG="F", this.w_VISEVA, this.oParentObject.w_MVPREZZO)
    endif
    if this.oParentObject.w_MVCODVAL<>this.w_MCODVAL AND this.w_MCAORIF<>0
      * --- Se documenti di differenti valute Riporta alla Valuta del Documento di Destinazione
      this.oParentObject.w_MVPREZZO = IIF(this.oParentObject.w_MVPREZZO=0, 0, cp_ROUND(this.oParentObject.w_MVPREZZO / this.w_MCAORIF, this.oParentObject.w_DECUNI))
      this.oParentObject.w_MVIMPPRO = IIF(this.oParentObject.w_MVIMPPRO=0, 0, cp_ROUND(this.oParentObject.w_MVIMPPRO / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.oParentObject.w_MVIMPCAP = IIF(this.oParentObject.w_MVIMPCAP=0, 0, cp_ROUND(this.oParentObject.w_MVIMPCAP / this.w_MCAORIF, this.oParentObject.w_DECTOT))
      this.oParentObject.w_MVIMPACC = IIF(this.oParentObject.w_MVIMPACC=0, 0, cp_ROUND(this.oParentObject.w_MVIMPACC / this.w_MCAORIF, this.oParentObject.w_DECTOT))
    endif
    * --- Se il tipo Provvigione � forzato, lascio l'importo sulla riga, altrimenti azzero (ricalcoler� il valore corretto all'f10 sul documento)
    this.oParentObject.w_MVIMPPRO = IIF(this.oParentObject.w_MVTIPPRO="FO", this.oParentObject.w_MVIMPPRO, 0)
    this.oParentObject.w_MVIMPCAP = IIF(this.oParentObject.w_MVTIPPR2="FO", this.oParentObject.w_MVIMPCAP, 0)
    * --- Inverte il Segno se Acconto
    this.oParentObject.w_MVPREZZO = this.oParentObject.w_MVPREZZO * IIF(this.oParentObject.w_MVFLARIF="-" AND this.w_FLACCO="S", -1, 1)
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAUNIMIS,CAOPERAT,CAMOLTIP,CACODART,CADESART,CADESSUP,CATPCON3,CATIPCO3,CAMINVEN"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAUNIMIS,CAOPERAT,CAMOLTIP,CACODART,CADESART,CADESSUP,CATPCON3,CATIPCO3,CAMINVEN;
        from (i_cTable) where;
            CACODICE = this.oParentObject.w_MVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.oParentObject.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.oParentObject.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      this.oParentObject.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.oParentObject.w_ARCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.w_DESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
      this.w_DESSUPL = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
      this.w_CATPCON3 = NVL(cp_ToDate(_read_.CATPCON3),cp_NullValue(_read_.CATPCON3))
      this.w_CATIPCO3 = NVL(cp_ToDate(_read_.CATIPCO3),cp_NullValue(_read_.CATIPCO3))
      this.oParentObject.w_CAMINVEN = NVL(cp_ToDate(_read_.CAMINVEN),cp_NullValue(_read_.CAMINVEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_ARTDIS = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
      this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
      this.oParentObject.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
      this.oParentObject.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
      this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.oParentObject.w_FLSERA = NVL(cp_ToDate(_read_.ARTIPSER),cp_NullValue(_read_.ARTIPSER))
      this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.oParentObject.w_VOCCEN = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
      this.oParentObject.w_VOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
      this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
      this.w_DTOBSOART = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
      this.oParentObject.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
      this.w_ARTPCON2 = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
      this.w_ARTPCONF = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
      this.w_ARTIPCO1 = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
      this.w_ARTIPCO2 = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
      this.w_ARTPROL = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
      this.oParentObject.w_CODDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
      this.oParentObject.w_FLESIM = NVL(cp_ToDate(_read_.ARFLESIM),cp_NullValue(_read_.ARFLESIM))
      this.oParentObject.w_FLCESP = NVL(cp_ToDate(_read_.ARFLCESP),cp_NullValue(_read_.ARFLCESP))
      this.w_ARPRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
      this.oParentObject.w_ARFLAPCA = NVL(cp_ToDate(_read_.ARFLAPCA),cp_NullValue(_read_.ARFLAPCA))
      this.oParentObject.w_CODCEN = NVL(cp_ToDate(_read_.ARCODCEN                ),cp_NullValue(_read_.ARCODCEN                ))
      this.oParentObject.w_ARRIPCON = NVL(cp_ToDate(_read_.ARRIPCON),cp_NullValue(_read_.ARRIPCON))
      this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
      this.oParentObject.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
      this.oParentObject.w_DISLOT = NVL(cp_ToDate(_read_.ARDISLOT),cp_NullValue(_read_.ARDISLOT))
      this.w_ARCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
      this.w_ARTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
      this.oParentObject.w_ARMINVEN = NVL(cp_ToDate(_read_.ARMINVEN),cp_NullValue(_read_.ARMINVEN))
      this.w_CATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
      this.oParentObject.w_FLCOM1 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_RMINVEN = iif(this.oParentObject.w_CAMINVEN<>0, this.oParentObject.w_CAMINVEN, this.oParentObject.w_ARMINVEN)
    this.oParentObject.w_MVFLTRAS = IIF(this.oParentObject.w_ARUTISER="S","S",this.oParentObject.w_MVFLTRAS)
    if VARTYPE( this.oParentObject.w_PRESTA ) <> "U"
      * --- w_PRESTA non � presente in tutte le gestioni
      this.oParentObject.w_PRESTA = this.w_ARPRESTA
    endif
    * --- leggo campi dalla 1a UM
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMMODUM2,UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMMODUM2,UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = this.oParentObject.w_UNMIS1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_ARTPADRE = this.oParentObject.w_MVTIPRIG = "A" Or (this.oParentObject.w_ARFLAPCA = "S" And this.oParentObject.w_ANFLAPCA="S" And this.oParentObject.w_TDFLAPCA="S")
    if this.oParentObject.w_MVFLVEAC="V"
      * --- Se vendite leggo il gruppo provvigioni per calcolo provvigioni...
      this.oParentObject.w_ARTPRO = this.w_ARTPROL
    endif
    * --- Controllo obsolescenza articolo
    if NOT EMPTY(this.w_DTOBSOART) AND this.w_DTOBSOART<=this.oParentObject.w_MVDATDOC
      this.w_MESS = "Attenzione: articolo %1 obsoleto dal %2"
      ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(this.oParentObject.w_MVCODART), DTOC(this.w_DTOBSOART) )
    endif
    * --- Controllo traduzione articoli solo se importazione tra intestatari diversi e doc. 
    *     ha intestatario...
    if Not Empty(this.oParentObject.w_MVCODCON) And this.oParentObject.w_MVTIPCON +this.oParentObject.w_MVCODCON <> this.w_TCF + this.w_RICON
      if Not Empty(this.w_RICON)
        * --- Se doc. di origine con intestatario verfico che lingua utilizza.
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODLIN"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TCF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_RICON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODLIN;
            from (i_cTable) where;
                ANTIPCON = this.w_TCF;
                and ANCODICE = this.w_RICON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TESTLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Se la lingue sono diverse o doc. origine senza intestatario
      *     procedo con la traduzione altrimenti leggo le descrizioni 
      *     direttamente dal documento o dal codice articolo
      if this.w_TESTLIN<>this.oParentObject.w_CODLIN Or Empty( this.w_RICON )
        this.w_LGDESCOD = " "
        this.w_LGDESSUP = " "
        * --- Se doc. senza intestatario ricerco la traduzione nella lingua
        *     dell'intestatario doc. destinazione (w_MVCODCON) se la trovo
        *     l'applico altrimenti uso le descrizioni lette sul documento
        * --- Read from TRADARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LGDESCOD,LGDESSUP"+;
            " from "+i_cTable+" TRADARTI where ";
                +"LGCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
                +" and LGCODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LGDESCOD,LGDESSUP;
            from (i_cTable) where;
                LGCODICE = this.oParentObject.w_MVCODICE;
                and LGCODLIN = this.oParentObject.w_CODLIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LGDESCOD = NVL(cp_ToDate(_read_.LGDESCOD),cp_NullValue(_read_.LGDESCOD))
          this.w_LGDESSUP = NVL(cp_ToDate(_read_.LGDESSUP),cp_NullValue(_read_.LGDESSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.oParentObject.w_MVDESART = this.w_LGDESCOD
          this.oParentObject.w_MVDESSUP = this.w_LGDESSUP
        else
          * --- Se doc. origine con intestatario leggo le descrizioni presenti in prima
          *     pagina dei servizi
          if Not Empty( this.w_RICON )
            this.oParentObject.w_MVDESART = this.w_DESART
            this.oParentObject.w_MVDESSUP = this.w_DESSUPL
          endif
        endif
      endif
    endif
    * --- Calcola magazzino in base alle priorita'
    * --- Se nel documento di origine � stata indicata una specifica Ubicazione e il documento d'origine � di carico o riservato deve essere mantenuto 
    *     il magazzino di origine
    this.oParentObject.w_MVCODMAG = IIF(NOT EMPTY(this.w_CODMAG), this.w_CODMAG, IIF(NOT EMPTY(this.w_CODUBI) and (this.w_ORCASC="+" OR this.oParentObject.w_FLRRIF="+") AND this.oParentObject.w_MVFLCASC="-", this.w_ORIMAG,CALCMAG(2, this.oParentObject.w_FLMGPR, this.w_ORIMAG, this.oParentObject.w_COMAG, this.oParentObject.w_OMAG, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER)))
    if Type("RigheSort.cMATR")<>"U" and Type("RigheSort.GPCODMA2")<>"U"
      this.oParentObject.w_COMAT = iif(empty(RigheSort.cMATR) OR EMPTY(RigheSort.GPCODMA2), this.oParentObject.w_COMAT, RigheSort.GPCODMA2)
    endif
    this.oParentObject.w_MVCODMAT = IIF(NOT EMPTY(this.w_CODUB2) and (this.w_OR2CASC="+" OR this.w_FL2RRIF="+") AND this.oParentObject.w_MVF2CASC="-",this.w_ORIMAT,CALCMAG(2, this.oParentObject.w_FLMTPR, this.w_ORIMAT, this.oParentObject.w_COMAT, this.oParentObject.w_OMAT, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER))
    * --- Azzera Magazzino se la Causale non lo Gestisce
    this.oParentObject.w_MVCODMAG = IIF(EMPTY(this.w_AGGSAL), SPACE(5), this.oParentObject.w_MVCODMAG)
    if NOT ISALT() AND NOT EMPTY( this.w_AGGSAL ) AND EMPTY ( this.oParentObject.w_MVCODMAG ) and this.oParentObject.w_MVTIPRIG="R"
      do while EMPTY( this.w_MAGAZZINODIDEFAULT )
        do GSVE_KMG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      enddo
      this.oParentObject.w_MVCODMAG = this.w_MAGAZZINODIDEFAULT
    endif
    if NOT EMPTY(this.oParentObject.w_MVCODMAG)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGPROMAG"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGPROMAG;
          from (i_cTable) where;
              MGCODMAG = this.oParentObject.w_MVCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MPROORD = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.oParentObject.w_MPROORD)
        this.oParentObject.w_MVPROORD = this.oParentObject.w_MPROORD
      endif
    endif
    this.oParentObject.w_MVCODMAT = IIF(EMPTY(this.w_AGGSAL1) OR EMPTY(this.w_MCAUCOL), SPACE(5), this.oParentObject.w_MVCODMAT)
    * --- Unita' di Misura
    this.oParentObject.w_UNMIS1 = LEFT(this.oParentObject.w_UNMIS1+SPACE(3), 3)
    this.oParentObject.w_UNMIS2 = LEFT(this.oParentObject.w_UNMIS2+SPACE(3), 3)
    this.oParentObject.w_UNMIS3 = LEFT(this.oParentObject.w_UNMIS3+SPACE(3), 3)
    * --- In caso di documento con flag Packing LIst attivo vado a leggere i dati relativi
    *     a COllo e Confezione direttamente da Articoli o Chiavi Articoli a secoda dell'unit� di misura
    if this.oParentObject.w_FLPACK="S" OR NOT EMPTY(this.oParentObject.w_MVUNILOG)
      do case
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS3
          * --- Collo e confezione del Codice di Ricerca
          this.oParentObject.w_MVCODCOL = this.w_CATIPCO3
          this.oParentObject.w_CODCONF = this.w_CATPCON3
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS2 
          * --- Collo e confezione della 2^UM Articolo
          this.oParentObject.w_MVCODCOL = this.w_ARTIPCO2
          this.oParentObject.w_CODCONF = this.w_ARTPCON2
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS1
          * --- Collo e confezione della 1^UM Articolo
          this.oParentObject.w_MVCODCOL = this.w_ARTIPCO1
          this.oParentObject.w_CODCONF = this.w_ARTPCONF
      endcase
    endif
    * --- Voce di Costo/Ricavo
    * --- La voce di costo/ricavo deve essere ricalcolata quando la causale di origine e di destinazione differiscono per il segno dell'analitica
    * --- w_VOCTIP: segno dell'analitica della causale di destinazione
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDVOCECR"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_DCCOLLEG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDVOCECR;
        from (i_cTable) where;
            TDTIPDOC = this.w_DCCOLLEG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ORIGVOCTIP = NVL(cp_ToDate(_read_.TDVOCECR),cp_NullValue(_read_.TDVOCECR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if ((g_COMM="S" AND this.oParentObject.w_FLGCOM$"M-S") OR (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S")) AND this.oParentObject.w_MVTIPRIG<>"D"
      this.oParentObject.w_MVVOCCEN = IIF(EMPTY(this.oParentObject.w_MVVOCCEN) OR this.oParentObject.w_VOCTIP<>this.w_ORIGVOCTIP, IIF(this.oParentObject.w_VOCTIP="C", this.oParentObject.w_VOCCEN, this.oParentObject.w_VOCRIC), this.oParentObject.w_MVVOCCEN)
    else
      this.oParentObject.w_MVVOCCEN = SPACE(15)
    endif
    * --- Analitica Documento 
    if g_PERCCR="S" AND this.oParentObject.w_FLANAL="S"
      this.oParentObject.w_MVFLELAN = this.oParentObject.w_FLELAN
      this.oParentObject.w_OCEN = IIF(this.oParentObject.w_MVTIPRIG<>"D" , iif( Not Empty( this.oParentObject.w_MVCODCEN ) , this.oParentObject.w_MVCODCEN, iif( Not Empty( this.oParentObject.w_CODCEN ) , this.oParentObject.w_CODCEN, this.oParentObject.w_OCEN)) , this.oParentObject.w_OCEN)
      if Empty( this.oParentObject.w_MVCODCEN )
        * --- Se il centro di costo non � presente sul documento di origine lo ricalcolo
        this.oParentObject.w_MVCODCEN = IIF(this.oParentObject.w_MVTIPRIG<>"D",iif( not empty( this.oParentObject.w_CODCEN ), this.oParentObject.w_CODCEN, this.oParentObject.w_OCEN ),SPACE(15))
      endif
    else
      this.oParentObject.w_MVCODCEN = SPACE(15)
    endif
    * --- Se GSVE_BI3 lanciato da caricamento rapido (GSAR_BPG) allora
    *     valorizzo i dati dell'analitica non dal documenti di origine ma dalla penna
    *     se i primi sono vuoti...
    if Type("RigheSort.cMATR")<>"U"
      this.oParentObject.w_MVCODCEN = IIF( Empty( Nvl(RigheSort.MVCODCEN, "" ) ) , this.oParentObject.w_MVCODCEN , RigheSort.MVCODCEN )
    endif
    * --- Legge e valorizza dati prevista evasione
    if NOT EMPTY(this.oParentObject.w_MVDATEVA) OR this.oParentObject.w_MVCLADOC="OR"
      if NOT EMPTY(this.w_DATEVA)
        if this.w_DATEVA < this.oParentObject.w_MVDATDOC
          this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC
        else
          this.oParentObject.w_MVDATEVA = this.w_DATEVA
        endif
      endif
      if EMPTY(this.w_DATEVA) AND this.oParentObject.w_MVCLADOC="OR"
        this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC
      endif
    endif
    * --- Per evitare la rilettura se il codice Iva � uguale alla riga precedente
    *     escludo fatture d'acconto
    if this.w_FLVEAC<>this.oParentObject.w_MVFLVEAC AND this.w_RICON<>this.oParentObject.w_MVCODCON or ((this.w_FLACCO <>"S" or IsAlt()) and this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_DRIF>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_DRIF<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.oParentObject.w_MVCODIVA+","$this.oParentObject.w_RIIVLSIV))
      * --- Nel If precedente � stata modificata la condizione:
      *     da
      *     w_FLVEAC<>w_MVFLVEAC AND w_RICON<>w_MVCODCON  or  (Empty(w_MVRIFORD) and w_FLRICIVA AND ( EMPTY(w_RIIVDTIN) OR w_DRIF>=w_RIIVDTIN) AND (EMPTY(w_RIIVDTFI) OR w_DRIF<=w_RIIVDTFI) AND ( EMPTY(w_RIIVLSIV) OR w_MVCODIVA+","$w_RIIVLSIV))
      *     a
      *     w_FLVEAC<>w_MVFLVEAC AND w_RICON<>w_MVCODCON  or  ((Empty(w_MVRIFORD) or IsAlt()) and w_FLRICIVA AND ( EMPTY(w_RIIVDTIN) OR w_DRIF>=w_RIIVDTIN) AND (EMPTY(w_RIIVDTFI) OR w_DRIF<=w_RIIVDTFI) AND ( EMPTY(w_RIIVLSIV) OR w_MVCODIVA+","$w_RIIVLSIV))
      *     In questo modo le fatture d'acconto non hanno un trattamento particolare (basta riprisitnare la condizione per differenziarne il comportamento)
      * --- Import intraciclo, intestatario differente, ricalcolo codice iva
      this.oParentObject.w_MVCODIVA = CALCODIV(this.oParentObject.w_MVCODART, this.oParentObject.w_MVTIPCON, iif(!empty(this.oParentObject.w_MVCODORN),this.oParentObject.w_MVCODORN,this.oParentObject.w_MVCODCON), this.oParentObject.w_MVTIPOPE, this.oParentObject.w_MVDATREG, this.oParentObject.w_ANTIPOPE, this.w_ARCODIVA, this.w_ARTIPOPE)
    endif
    if this.w_OLDCODIVA <> this.oParentObject.w_MVCODIVA And NOT EMPTY(NVL(this.oParentObject.w_MVCODIVA," "))
      this.oParentObject.w_PERIVA = 0
      this.oParentObject.w_BOLIVA = " "
      this.w_OLDCODIVA = this.oParentObject.w_MVCODIVA
      * --- Legge Cod.IVA Articolo
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.oParentObject.w_MVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.oParentObject.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        this.w_INDIVA1 = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OLDPERIVA = this.oParentObject.w_PERIVA
      this.w_OLDBOLIVA = this.oParentObject.w_BOLIVA
    else
      this.oParentObject.w_PERIVA = this.w_OLDPERIVA
      this.oParentObject.w_BOLIVA = this.w_OLDBOLIVA
    endif
    * --- U.M. Frazionabile
    this.oParentObject.w_FLFRAZ = this.w_LFLFRAZ
    if g_COAC="S"
      * --- Se importo all'interno di una fattura devo ricalcolare i contributi accessori 
      *     per peso...
      this.oParentObject.w_TESTPESO = "I"
      * --- Se modulo attivo, causale con ricalcolo contributi, intestatario
      *     con contributi accessori attivo e riga di tipo contributo allora 
      *     non esegui il ricalcolo prezzi anche se nella causale � previsto
      this.w_RICPREZZO = Not ( this.oParentObject.w_TDFLAPCA="S" And this.oParentObject.w_ANFLAPCA="S" And Not Empty( this.oParentObject.w_MVCACONT ))
      this.oParentObject.w_MVRIFCAC = this.w_RIFCAC
      this.oParentObject.w_ARPUNPAD = this.oParentObject.w_MVRIFCAC>0
    else
      this.w_RICPREZZO = .T.
      this.oParentObject.w_ARPUNPAD = .F.
    endif
    if this.w_QTALOT>0
      * --- Se contratti accessori, le quantit� sono gia determinate a pagina 1...
      if this.w_BCONTACC
        this.oParentObject.w_MVQTAMOV = this.w_QTALOT
        this.oParentObject.w_MVQTAIMP = this.w_IMPLOT
      else
        if this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
          * --- Ricalcolo in Proporzione la Qta Movimentata
          this.oParentObject.w_MVQTAMOV = (this.w_APPO1*this.w_QTALOT)/this.w_QTAUM1
          this.oParentObject.w_MVQTAIMP = cp_ROUND((this.w_APPO1*this.w_IMPLOT)/this.w_QTAUM1,g_PERPQT)
          this.w_RESIMP = this.w_RESIMP-this.oParentObject.w_MVQTAIMP
          if this.w_RESIMP>0 AND this.w_NOFRAZ="S"
            this.oParentObject.w_MVQTAIMP = CALQTAIMP(this.oParentObject.w_MVQTAIMP, this.w_RESIMP, this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_NOFRAZ, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
            this.w_RESIMP = 0
          endif
          if this.w_RESIMP<0
            * --- Riassegno qta importata se differenze di arrotondamento
            this.oParentObject.w_MVQTAIMP = this.oParentObject.w_MVQTAIMP-ABS(this.w_RESIMP)
            this.w_RESIMP = 0
          endif
        else
          this.oParentObject.w_MVQTAMOV = this.w_QTALOT
          this.oParentObject.w_MVQTAIMP = this.w_IMPLOT
        endif
      endif
    endif
    if ISALT()
      do case
        case this.oParentObject.w_MVTIPRIG="F" 
          if this.oParentObject.w_MVPREZZO<>0 and this.w_OPREZZO<>0
            this.w_COEFF = this.oParentObject.w_MVPREZZO/this.w_OPREZZO
          endif
        case this.oParentObject.w_MVTIPRIG="M" 
          if this.oParentObject.w_MVQTAIMP<>0 and this.w_APPO1<>0
            this.w_COEFF = this.oParentObject.w_MVQTAMOV/this.w_APPO1
          endif
      endcase
      if this.w_COEFF > 0
        this.oParentObject.w_DEMINEFF = INT(this.oParentObject.w_DEMINEFF * this.w_COEFF) + INT((this.oParentObject.w_DEOREEFF * this.w_COEFF - INT(this.oParentObject.w_DEOREEFF * this.w_COEFF)) * 60)
        this.oParentObject.w_DEOREEFF = INT(this.oParentObject.w_DEOREEFF * this.w_COEFF)
        this.oParentObject.w_DECOSINT = this.oParentObject.w_DECOSINT * this.w_COEFF
      endif
      this.oParentObject.w_MVFLNOAN = Icase(Nvl(this.w_ARPRESTA," ")="A" and this.oParentObject.w_ESCANA="S","S",g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.oParentObject.w_MVFLNOAN $ "S-N",this.oParentObject.w_MVFLNOAN,"N")
    endif
    * --- Nell'assegnamento a MVFLERIF devo tenere conto anche di eventuali righe gia importate
    *     associate alla stessa riga di origine.
    do case
      case EMPTY(this.oParentObject.w_MVFLARIF)
        this.oParentObject.w_MVFLERIF = " "
      case RigheSort.XCHK=1
        this.oParentObject.w_MVFLERIF = "S"
      case this.oParentObject.w_MVTIPRIG="F" AND Abs(this.w_VISEVA)>=(Abs(this.w_OPREZZO) - Abs(this.oParentObject.w_ORDEVA) - Abs( NVL(RigheSort.OLIMPEVA,0)) )
        this.oParentObject.w_MVFLERIF = "S"
      case NOT this.oParentObject.w_MVTIPRIG $ "DF" AND (((this.oParentObject.w_MVQTAIMP+NVL(RigheSort.OLQTAEVA,0))>=(this.w_APPO1-this.w_APPO2)) OR (this.w_QTALOT>0 AND this.w_RESIM1=0))
        this.oParentObject.w_MVFLERIF = "S"
    endcase
    if this.w_ROWPRE=-1
      this.oParentObject.w_MVROWRIF = this.w_ROWPRE
      this.oParentObject.w_MVSERRIF = this.w_PRSERIAL
      this.oParentObject.w_MVCATCON = this.w_CATCON
    endif
    if NOT EMPTY(this.oParentObject.w_MVFLARIF)
      * --- Uniformo MVFLERIF sul dettaglio per evitare di avere righe che abbiano
      *     un comportamento difforme su evasione o riapertura della riga
      this.oParentObject.w_OLDEVAS = IIF( this.oParentObject.w_MVFLERIF="S" , "E" , "R" )
      GSAR_BCM(this, this.w_PADRE , this.oParentObject.w_CPROWNUM , this.oParentObject.w_MVSERRIF, this.oParentObject.w_MVROWRIF , this.oParentObject.w_OLDEVAS )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_MVKEYSAL = IIF(this.oParentObject.w_MVTIPRIG="R" AND (NOT EMPTY(this.w_AGGSAL) OR NOT EMPTY(this.w_AGGSAL1)), this.oParentObject.w_MVCODART, SPACE(20))
    this.oParentObject.w_MVCODMAG = IIF(EMPTY(this.oParentObject.w_MVKEYSAL), SPACE(5), this.oParentObject.w_MVCODMAG)
    this.oParentObject.w_MVCODMAT = IIF(EMPTY(this.oParentObject.w_MVKEYSAL), SPACE(5), this.oParentObject.w_MVCODMAT)
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+" Q"
    msg=""
    this.oParentObject.w_MVQTAUM1 = CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
    * --- Qt� evasa nella prima unit� di misura, utilizzata per stornare la qt� evasa sul documento di origine
    *     (MVQTAUM1,MVQTAIM1)
    if this.oParentObject.w_MVTIPRIG$"F-D"
      this.oParentObject.w_MVQTAUM1 = IIF( this.oParentObject.w_MVTIPRIG="D" ,0 , 1 )
      this.oParentObject.w_MVQTAIM1 = IIF( this.oParentObject.w_MVTIPRIG="D" ,0 , 1 )
    else
      if this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
        * --- Ricalcolo la qt� movimentata del documento di origine nella prima unit� di misura
        *     e la confronto con la qt� nella prima unit� di misura sempre del doc
        *     di origine. Se diverse allora l'utente ha modificato la qt� nella prima unit� di misura 
        *     quindi nel doc. di destinazione calcolo MVQTAUM1 come rapporto dato da:
        *     
        *     ( Qt� 1 Mis. Origine / Qt� movimenta Origine ) * Qta movimentata Destinazione
        *     
        *     Ricalcolo la qt� nella prima unit� di misura del doc. di origine
        this.w_TmpN = CALQTA(this.w_APPO1,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "", this.w_MODUM2, , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
        if this.w_TmpN<>this.w_QTAUM1
          * --- Eseguo la proporzione..
          this.oParentObject.w_MVQTAUM1 = cp_Round(( this.w_QTAUM1 / this.w_APPO1 ) * this.oParentObject.w_MVQTAMOV,5)
          this.oParentObject.w_MVQTAIM1 = cp_Round(( this.w_QTAUM1 / this.w_APPO1 ) * this.oParentObject.w_MVQTAIMP,5)
        else
          this.oParentObject.w_MVQTAIM1 = CALQTA(this.oParentObject.w_MVQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
        endif
        * --- Se servizio generico non faccio la verifica in quanto non esiste rapporto fra l'Um movimentata e la 1a Um
        if this.w_NOFRAZ="S" AND this.oParentObject.w_MVQTAUM1<>INT(this.oParentObject.w_MVQTAUM1) and this.oParentObject.w_FLSERG<>"S"
          if this.w_MODUM2="S" 
            * --- Se sulla 1 UM ho il flag forza UM secondarie e le UM non sono separate arrotondo la 1UM all'intero superiore  
            *     e riproporziono l'UM movimentata
            *     altrimenti arrotondo all'intero 
            if this.oParentObject.w_FLUSEP=" "
              this.Page_8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Se UM separate arrotondo la qt� della 1UM ma lascio invariata la riga 
              this.oParentObject.w_MVQTAUM1 = INT(this.oParentObject.w_MVQTAUM1)+1
              this.oParentObject.w_MVQTAIM1 = INT(this.oParentObject.w_MVQTAIM1)+1
            endif
          else
            if this.oParentObject.w_FLUSEP<>" "
              * --- Se UM separate arrotondo la qt� della 1UM
              this.oParentObject.w_MVQTAUM1 = INT(this.oParentObject.w_MVQTAUM1)+1
              this.oParentObject.w_MVQTAIM1 = INT(this.oParentObject.w_MVQTAIM1)+1
            else
              * --- segnalo errore
              msg="Attenzione: articolo privo di unit� di misura separate e prima unit� di misura non frazionabile%0La quantit� nella 1a UM non pu� essere arrotondata all'intero"
              this.oParentObject.w_MVQTAUM1 = 0
              this.oParentObject.w_MVQTAIM1 = 0
            endif
          endif
        endif
      else
        this.oParentObject.w_MVQTAUM1 = CALQTA(this.oParentObject.w_MVQTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg,this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
        this.oParentObject.w_MVQTAIM1 = CALQTA(this.oParentObject.w_MVQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      endif
    endif
    if NOT EMPTY(msg)
      * --- Errore relativo alla quantit� della 1a UM
      Ah_ErrorMsg("%1",,, ah_msgformat(msg))
    endif
    this.oParentObject.w_UNIMIS2 = IIF(Not Empty(this.oParentObject.w_UNMIS3), this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS2)
    this.oParentObject.w_QTAUM2 = Caunmis2(this.oParentObject.w_MVTIPRIG, this.oParentObject.w_MVQTAMOV, this.oParentObject.w_UNMIS1, IIF(Not Empty(this.oParentObject.w_UNMIS3), this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS2), this.oParentObject.w_MVUNIMIS, IIF(Not Empty(this.oParentObject.w_UNMIS3), this.oParentObject.w_OPERA3, this.oParentObject.w_OPERAT), IIF(Not Empty(this.oParentObject.w_UNMIS3), this.oParentObject.w_MOLTI3, this.oParentObject.w_MOLTIP))
    this.oParentObject.w_MVQTASAL = this.oParentObject.w_MVQTAUM1
    * --- Saldi
    this.oParentObject.w_VALUCA = 0
    this.oParentObject.w_CODVAA = this.oParentObject.w_MVVALNAZ
    this.oParentObject.w_CODVAV = this.oParentObject.w_MVVALNAZ
    if NOT EMPTY(NVL(this.oParentObject.w_MVKEYSAL," ")) AND NOT EMPTY(NVL(this.oParentObject.w_MVCODMAG," "))
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV;
          from (i_cTable) where;
              SLCODICE = this.oParentObject.w_MVKEYSAL;
              and SLCODMAG = this.oParentObject.w_MVCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
        this.oParentObject.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
        this.oParentObject.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
        this.oParentObject.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
        this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
        this.oParentObject.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
        this.oParentObject.w_CODVAV = NVL(cp_ToDate(_read_.SLCODVAV),cp_NullValue(_read_.SLCODVAV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MVFLULCA = IIF(this.oParentObject.w_FLAVA1="A" AND this.oParentObject.w_MVFLCASC $ "+-" AND this.oParentObject.w_MVDATDOC>=this.oParentObject.w_ULTCAR AND this.oParentObject.w_MVFLOMAG<>"S" And this.oParentObject.w_MVPREZZO<>0, "=", " ")
      this.oParentObject.w_MVFLULPV = IIF(this.oParentObject.w_FLAVA1="V" AND this.oParentObject.w_MVFLCASC $ "+-" AND this.oParentObject.w_MVDATDOC>=this.oParentObject.w_ULTSCA AND this.oParentObject.w_MVFLOMAG<>"S" And this.oParentObject.w_MVPREZZO<>0, "=", " ")
      if NOT EMPTY(NVL(this.oParentObject.w_MVCODMAT," ")) AND NOT EMPTY(NVL(this.oParentObject.w_MVCAUCOL," "))
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLQTAPER,SLQTRPER"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLQTAPER,SLQTRPER;
            from (i_cTable) where;
                SLCODICE = this.oParentObject.w_MVKEYSAL;
                and SLCODMAG = this.oParentObject.w_MVCODMAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_Q2APER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
          this.oParentObject.w_Q2RPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Imposto sulla riga lo stesso listino di testata.
    *     Se sul documento di destinazione verr� cambiata la quantit� su riga, 
    *     verr� ricalcolato il prezzo in base al listino di testata e non a quello del listino di Origine
    this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
    this.oParentObject.o_MVCODLIS = this.oParentObject.w_MVTCOLIS
    * --- Ricalcola o no i totali
    this.w_RICALCOLA = .T.
    if this.oParentObject.w_NOPRSC<>"S"
      * --- Se contributo accessorio non ricalcolo mai il prezzo/contratti...
      if this.w_RICPREZZO
        * --- Se non importo dati pagamento, il documento collegato ha attivo il flag no intestatario e ho l'intestatario sul documento ricalcolo i prezzi
        this.w_OKCONTR = this.w_TCF=this.oParentObject.w_MVTIPCON
        if (this.w_DCFLINTE="S" AND NOT EMPTY(this.oParentObject.w_MVCODCON) And Not this.w_OKCONTR) Or this.oParentObject.w_PRZDES="S"
          * --- Il contratto � in questo caso ricalcolato dalla funzione di calcolo prezzi
          this.oParentObject.w_MVCONTRA = Space(15)
          if this.oParentObject.w_PRZDES="S" And this.w_DCFLRIPS<>"S"
            this.oParentObject.w_MVSCONT1 = 0
            this.oParentObject.w_MVSCONT2 = 0
            this.oParentObject.w_MVSCONT3 = 0
            this.oParentObject.w_MVSCONT4 = 0
            this.oParentObject.w_LIPREZZO = 0
            this.oParentObject.w_MVPREZZO = 0
            this.oParentObject.o_MVPREZZO = 0
            * --- Riassegno le o_ in modo che la mCalc() lanciata a fine GSVE_BCD non esegua i ricalcoli dovuti
            *     al cambio di queste variabili: vedi per esempio MVQTAUM1
            this.w_APPO = this.oParentObject.o_MVCODICE
            this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
            this.oParentObject.o_MVQTAMOV = this.oParentObject.w_MVQTAMOV
            this.oParentObject.o_MVUNIMIS = this.oParentObject.w_MVUNIMIS
            * --- Per capire se viene lanciato "Ricalcola". In questo caso non deve ricalcolare i totali
            *     poich� li ricalcola gi� il GSVE_BCD
            this.w_RICALCOLA = .F.
            * --- Ricalcolo Prezzo attivato sulla causale documento di destinazione.
            *     I prezzi verranno ricalcolati in base ai listini/contratti validi sul documento di destinazione.
            this.w_PADRE.NotifyEvent("Ricalcola")     
            this.oParentObject.o_MVCODICE = this.w_APPO
            * --- Per non eseguire i calcoli nel Batch GSVE_BCD
            this.bUpdateParentObject=.f.
          endif
        else
          * --- Gestione contratto di riga e Listino
          if (g_GESCON="S" and not Empty( this.oParentObject.w_MVCODCON ) and not Empty( this.w_CONTRA )) Or Not empty(this.oParentObject.w_MVCODLIS)
            * --- Verifico se il contratto impostato nel documento di origine � ancora valido
            * --- Se importazione incrociata Clienti - Fornitori il contratto � ovviamente non valido e ricalcolo tutto
            if this.w_OKCONTR
              * --- Controllo solo i contratti perch� vengono importati. 
              *     Il Listino valido � quello del documento di destinazione, non viene importato.
              *     Nell'importazione Incrociata Cli/For devo ricalcolare tutti i prezzi
              if Not Empty(this.w_CONTRA)
                * --- Read from CON_TRAM
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CON_TRAM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS"+;
                    " from "+i_cTable+" CON_TRAM where ";
                        +"CONUMERO = "+cp_ToStrODBC(this.w_CONTRA);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                    from (i_cTable) where;
                        CONUMERO = this.w_CONTRA;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
                  this.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
                  this.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
                  this.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
                  this.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
                  this.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
                  this.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if CHKCONTR(this.w_CONTRA,this.oParentObject.w_MVTIPCON,this.oParentObject.w_MVCODCON,this.oParentObject.w_CATCOM,this.oParentObject.w_MVFLSCOR,this.oParentObject.w_MVCODVAL,this.oParentObject.w_MVDATREG,this.w_CT,this.w_CC,this.w_CM,this.w_CV,this.w_CI,this.w_CF,this.w_IVACON)
                  this.oParentObject.w_MVCONTRA = this.w_CONTRA
                else
                  this.oParentObject.w_MVCONTRA = SPACE(15)
                endif
              endif
            else
              * --- Importazione Incrociata Cli - For
              *     Cancello il contratto sulla riga
              this.oParentObject.w_MVCONTRA = Space(15)
            endif
          else
            * --- Non Gestiti i contratti
            this.oParentObject.w_MVCONTRA = SPACE(15)
          endif
        endif
      endif
    else
      * --- Se attivo No Prezzo/Sconto sulla causale Azzero tutto
      this.oParentObject.w_MVSCONT1 = 0
      this.oParentObject.w_MVSCONT2 = 0
      this.oParentObject.w_MVSCONT3 = 0
      this.oParentObject.w_MVSCONT4 = 0
      this.oParentObject.w_MVPREZZO = 0
      this.oParentObject.w_LIPREZZO = 0
      this.oParentObject.w_MVCONTRA = SPACE(15)
    endif
    if Not this.oParentObject.w_MVTIPRIG$"DF" And ( this.oParentObject.w_FLQRIO="S" Or this.oParentObject.w_FLPREV="S" ) And this.oParentObject.w_PRZDES<>"S"
      * --- Se non attivo Ricalcola Prezzi ma attivo Ricalcolo Qt� da Lotto di Riordino 
      *     o Ricalcolo Data Evasione
      *     Ricalcolo Qt� o Data Evasione (non il prezzo). GSVE_BCD('Q')
      *     Reimposto o_MVCODICE altrimenti la mCalc mi ricalcola l'unit� di misura
      this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
      this.w_PADRE.NotifyEvent("RicQtamov")     
      * --- Anche in questo caso devo evitare di ricalcolare il totale visto che mcalc provvede a farlo
      this.w_RICALCOLA = .F.
    endif
    * --- Se attivo 'Ricalcolo Provvigioni' nei parametri e 'Genera Provvigioni' nel documento di destinazione
    if g_PROGEN ="S" And this.oParentObject.w_FLAGEN = "S"
      if this.oParentObject.w_RICPROV = "S"
        * --- Calcolo prima le provvigioni da contratto
        *     Solo nel caso di generazione massiva poich� nel caso di cambio del tipo generazione sui parametri
        *     i documenti, che sono gi� stati caricati a mano, hanno gi� i valori giusti nel caso ci fosse stato un contratto valido
        DECLARE ARRCALC (16,1)
        this.w_PROG = "V"
        * --- Azzero l'Array che verr� riempito dalla Funzione
         
 ARRCALC(1)=0 
 ARRCALC(6)=0 
 ARRCALC(15)=0
        this.w_QTAUM3 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
        DIMENSION pArrUm[9]
        pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_MVUNIMIS 
 pArrUm [3] = this.oParentObject.w_MVQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.oParentObject.w_MVQTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.oParentObject.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
        * --- Lancio la funzione di calcolo prezzi che uso solo per prelevare le informazioni da contratto 
        *     Non passo il contratto perch� deve ricalcolare quello valido al momento della generazione
        this.w_CALPRZ = CalPrzli(Space(20), this.oParentObject.w_MVTIPCON , Space(5) , this.oParentObject.w_MVCODART , "XXXXXX" , this.oParentObject.w_MVQTAUM1 , this.oParentObject.w_MVCODVAL , this.oParentObject.w_MVCAOVAL , this.oParentObject.w_MVDATREG , this.oParentObject.w_CATSCC , "XXXXXX", "     ", this.oParentObject.w_XCONORN, this.oParentObject.w_CATCOM, this.oParentObject.w_MVFLSCOR, " ", "     ",this.w_PROG, @ARRCALC, " ", this.oParentObject.w_MVFLVEAC,"N", @pArrUm )
        if this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVTIPRIG $ "RFMA" AND g_PERAGE="S"
          * --- Percentuale provvigione agente
          *     Ricalcolo solo se in origine non � forzata o esclusa
          if Not(this.oParentObject.w_MVTIPPRO$"FO-ES")
            if ARRCALC(6)<>0 and this.oParentObject.w_MVTIPPRO$"CT-CC-ST-DC-ET" 
              * --- Percentuale Provvigione <>0.
              *     Imposto TIPPRO ='CC' : Calcolata da contratto
              this.oParentObject.w_MVPERPRO = ARRCALC(6)
              this.oParentObject.w_MVIMPPRO = 0
              this.oParentObject.w_MVTIPPRO = "CC"
            else
              * --- Azzera la percentuale provvigione e reimposta il tipo provvigione
              this.oParentObject.w_MVPERPRO = 0
              this.oParentObject.w_MVIMPPRO = 0
              this.w_OLDTIPPRO = this.w_PADRE.Get( "w_MVTIPPRO" )
              * --- Se modifico manualmente il tipo provvigione impostando Da Contratto,
              *     anche se la percentuale � 0 mantengo la scelta dell'utente.
              if NOT ( this.w_OLDTIPPRO<>this.oParentObject.w_MVTIPPRO AND this.oParentObject.w_MVTIPPRO="CC" )
                * --- Se da Contratto o da Tabella Provvigioni con percentuale 0,
                *     reimposto da tabella provvigioni e ricalcolo la percentuale.
                *     (Se da contratto = 0, ricalcolo da tabella provvigioni dove potrebbe esistere una percentuale)
                if EMPTY(this.oParentObject.w_MVCONTRA) AND this.oParentObject.w_MVTIPPRO="CC" AND g_CALPRO="DI"
                  * --- Se contratto vuoto (per cancellazione su riga ), tipo provvigione calcolata da contratto e 
                  *     nei parametri � impostato come Calcolo Provvigioni sui Documenti: Disattivato 
                  *     Reimposto sulla riga Tipo Provvigione: Da Calcolare
                  this.oParentObject.w_MVTIPPRO = "DC"
                endif
              endif
              this.w_PADRE.Set("w_MVTIPPRO" , this.oParentObject.w_MVTIPPRO)     
            endif
          endif
          * --- Percentuale provvigione capoarea
          if Not(this.oParentObject.w_MVTIPPR2$"FO-ES")
            if ARRCALC(15)<>0 and this.oParentObject.w_MVTIPPR2$"CT-CC-ST-DC" 
              this.oParentObject.w_MVPROCAP = ARRCALC(15)
              this.oParentObject.w_MVIMPCAP = 0
              this.oParentObject.w_MVTIPPR2 = "CC"
            else
              * --- Azzera la percentuale provvigione e reimposta il tipo provvigione
              this.oParentObject.w_MVPROCAP = 0
              this.oParentObject.w_MVIMPCAP = 0
              this.w_OLDTIPPR2 = this.w_PADRE.Get( "w_MVTIPPR2" )
              * --- Se modifico manualmente il tipo provvigione impostando Da Contratto,
              *     anche se la percentuale � 0 mantengo la scelta dell'utente.
              if NOT ( this.w_OLDTIPPR2<>this.oParentObject.w_MVTIPPR2 AND this.oParentObject.w_MVTIPPR2="CC" )
                * --- Se da Contratto o da Tabella Provvigioni con percentuale 0,
                *     reimposto da tabella provvigioni e ricalcolo la percentuale.
                *     (Se da contratto = 0, ricalcolo da tabella provvigioni dove potrebbe esistere una percentuale)
                if EMPTY(this.oParentObject.w_MVCONTRA) AND this.oParentObject.w_MVTIPPR2="CC" AND g_CALPRO="DI"
                  * --- Se contratto vuoto (per cancellazione su riga ), tipo provvigione calcolata da contratto e 
                  *     nei parametri � impostato come Calcolo Provvigioni sui Documenti: Disattivato 
                  *     Reimposto sulla riga Tipo Provvigione: Da Calcolare
                  this.oParentObject.w_MVTIPPR2 = "DC"
                endif
              endif
              this.w_PADRE.Set("w_MVTIPPR2" , this.oParentObject.w_MVTIPPR2)     
            endif
          endif
        endif
      endif
      if this.oParentObject.w_MVFLVEAC="V" And Not(this.oParentObject.w_MVTIPPRO$"FO-ES" And this.oParentObject.w_MVTIPPR2$"FO-ES")
        * --- Poich� se ho forzata o esclusa non devo ricalcolare le provvigioni, ma all'evento Calcperc1 
        *     vengono ricalcolate anche in questo caso, tengo i vecchi valori e li riassegno
        *     Questo perch� la Calcperc1 ricalcola entrambi (agente e capoarea), se avessi valori diversi es.:
        *     Agente: Esclusa
        *     Capoarea: da Tabella
        *     sull'agente verrebbe comunque ricalcolato
        this.w_OTIPPRO = this.oParentObject.w_MVTIPPRO
        this.w_OTIPPR2 = this.oParentObject.w_MVTIPPR2
        this.w_OIMPPRO = this.oParentObject.w_MVIMPPRO
        this.w_OIMPCAP = this.oParentObject.w_MVIMPCAP
        this.w_OPERPRO = this.oParentObject.w_MVPERPRO
        this.w_OPROCAP = this.oParentObject.w_MVPROCAP
        * --- Ricalcolo le provvigioni solo se non sono Forzate o Escluse
        this.w_PADRE.NotifyEvent("Calcperc1")     
        if this.w_OTIPPRO $"FO-ES"
          * --- Se nel documento di origine avevo escluso ripristino
          this.oParentObject.w_MVTIPPRO = this.w_OTIPPRO
          this.oParentObject.w_MVIMPPRO = this.w_OIMPPRO
          this.oParentObject.w_MVPERPRO = this.w_OPERPRO
        endif
        if this.w_OTIPPR2 $"FO-ES"
          * --- Se nel documento di origine avevo escluso ripristino
          this.oParentObject.w_MVTIPPR2 = this.w_OTIPPR2
          this.oParentObject.w_MVIMPCAP = this.w_OIMPCAP
          this.oParentObject.w_MVPROCAP = this.w_OPROCAP
        endif
      endif
    endif
    * --- Se eseguo raggruppamento articoli (pag7) svuoto i riferimenti ai documenti di origine
    this.oParentObject.w_MVSERRIF = IIF(this.w_DCFLGROR="S" And Not this.oParentObject.w_MVTIPRIG$"DF", Space(10), this.oParentObject.w_MVSERRIF)
    this.oParentObject.w_STSERRIF = IIF(this.w_DCFLGROR="S" And Not this.oParentObject.w_MVTIPRIG$"DF", Space(10), this.oParentObject.w_MVSERRIF)
    * --- Disabilito l'intestatario se importo righe...
    this.oParentObject.w_FLEDIT = IIF( Not Empty( this.oParentObject.w_MVSERRIF ) , "N" , " " )
    this.oParentObject.w_MVROWRIF = IIF(this.w_DCFLGROR="S" And Not this.oParentObject.w_MVTIPRIG$"DF", 0, this.oParentObject.w_MVROWRIF)
    this.oParentObject.w_MVVALRIG = CAVALRIG(this.oParentObject.w_MVPREZZO,this.oParentObject.w_MVQTAMOV, this.oParentObject.w_MVSCONT1,this.oParentObject.w_MVSCONT2,this.oParentObject.w_MVSCONT3,this.oParentObject.w_MVSCONT4,this.oParentObject.w_DECTOT)
    this.oParentObject.w_MVIMPSCO = 0
    this.oParentObject.w_MVVALMAG = CAVALMAG(this.oParentObject.w_MVFLSCOR, this.oParentObject.w_MVVALRIG, this.oParentObject.w_MVIMPSCO, this.oParentObject.w_MVIMPACC, this.oParentObject.w_PERIVA, this.oParentObject.w_DECTOT, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE )
    this.oParentObject.w_INDIVA = this.w_INDIVA1
    this.oParentObject.w_MVIMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, this.oParentObject.w_PERIVA, this.oParentObject.w_INDIVA )
    * --- Gestione Lotti
    this.oParentObject.w_MVFLLOTT = " "
    this.oParentObject.w_MVF2LOTT = " "
    if EMPTY(this.oParentObject.w_MVCODMAG)
      this.w_LFLUBIC = " "
    else
      * --- Se nel documento di destinazione non c'� un magazzino si mantiene la provincia del doc d'origine
      if this.w_OLDMAG<>this.oParentObject.w_MVCODMAG
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.oParentObject.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LFLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if EMPTY(this.oParentObject.w_MVCODMAT)
      this.w_LF2UBIC = " "
    else
      if this.w_OLDMAT<>this.oParentObject.w_MVCODMAT
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGFLUBIC"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGFLUBIC;
            from (i_cTable) where;
                MGCODMAG = this.oParentObject.w_MVCODMAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LF2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Assegna qui per evitare problemi con l'inizializzazione dei Magazzini (InitRiw)
    this.oParentObject.w_FLUBIC = this.w_LFLUBIC
    this.oParentObject.w_F2UBIC = this.w_LF2UBIC
    this.w_OLDMAG = this.oParentObject.w_MVCODMAG
    this.w_OLDMAT = this.oParentObject.w_MVCODMAT
    if this.oParentObject.w_MVTIPRIG="R" AND ((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (g_PERUBI="S" AND (this.w_LFLUBIC="S" OR this.w_LF2UBIC="S")))
      * --- Se importo da caricamento rapido importo lotti e ubicazioni
      if Type("RigheSort.cMATR")<>"U"
        this.w_CODLOT = RigheSort.t_GPCODLOT
        this.w_CODUBI = RigheSort.t_GPCODUBI
      endif
      * --- Importo Informazioni relative ai lotti
      this.w_FLLOTT1 = IIF((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (this.w_LFLUBIC="S" AND g_PERUBI="S") , Left(Alltrim(this.oParentObject.w_MVFLCASC)+IIF(this.oParentObject.w_MVFLRISE="+", "-", IIF(this.oParentObject.w_MVFLRISE="-", "+", " ")),1)," ")
      this.w_FLLOTT2 = IIF((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (this.w_LF2UBIC="S" AND g_PERUBI="S") , Left(Alltrim(this.oParentObject.w_MVF2CASC)+IIF(this.oParentObject.w_MVF2RISE="+", "-", IIF(this.oParentObject.w_MVF2RISE="-", "+", " ")),1)," ")
      this.oParentObject.w_MVFLLOTT = this.w_FLLOTT1
      this.oParentObject.w_MVF2LOTT = this.w_FLLOTT2
      this.oParentObject.w_MVCODUBI = IIF(g_PERUBI="S" AND this.oParentObject.w_MVFLLOTT $ "+-" AND this.w_LFLUBIC="S" ,this.w_CODUBI,SPACE(20))
      this.oParentObject.w_MVCODUB2 = IIF(g_PERUBI="S" AND this.oParentObject.w_MVF2LOTT $ "+-" AND NOT EMPTY(this.oParentObject.w_MVCODMAT) AND this.w_LF2UBIC="S",this.w_CODUB2,SPACE(20))
      this.oParentObject.w_MVCODLOT = IIF(g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" AND ((this.oParentObject.w_MVFLLOTT $ "+-") OR (this.oParentObject.w_MVF2LOTT $ "+-" )) ,this.w_CODLOT,SPACE(20))
      this.oParentObject.w_MVLOTMAG = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUBI ) , SPACE(5) , this.oParentObject.w_MVCODMAG )
      this.oParentObject.w_MVLOTMAT = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUB2 ) , SPACE(5) , this.oParentObject.w_MVCODMAT )
      if NOT EMPTY(this.oParentObject.w_MVCODLOT)
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT,LODATSCA"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT,LODATSCA;
            from (i_cTable) where;
                LOCODICE = this.oParentObject.w_MVCODLOT;
                and LOCODART = this.oParentObject.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          this.w_LODATSCA = NVL(cp_ToDate(_read_.LODATSCA),cp_NullValue(_read_.LODATSCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if (this.w_LODATSCA<this.oParentObject.w_MVDATREG AND NOT EMPTY(this.w_LODATSCA)) 
          * --- Se lotto Scaduto o non Disponibile non viene importato
          this.oParentObject.w_MVCODLOT = SPACE(20)
        endif
      endif
    endif
    * --- Attivita' di Commessa
    this.oParentObject.w_MVTIPATT = "A"
    this.oParentObject.w_MVFLORCO = IIF(g_COMM="S", IIF(this.oParentObject.w_FLCOMM="I", "+", IIF(this.oParentObject.w_FLCOMM="D", "-", " ")), " ")
    this.oParentObject.w_MVFLCOCO = IIF(g_COMM="S", IIF(this.oParentObject.w_FLCOMM="C", "+", IIF(this.oParentObject.w_FLCOMM="S", "-", " ")), " ")
    * --- Commessa / Attivita'
    if (((g_COMM="S" AND this.oParentObject.w_FLGCOM$"M-S") OR (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S") OR g_PERCAN="S") AND this.oParentObject.w_MVTIPRIG<>"D") OR (this.oParentObject.w_FLPRAT="S" AND ISALT())
      this.w_NOCOMM = EMPTY(this.oParentObject.w_MVCODCOM)
      this.oParentObject.w_MVCODCOM = IIF(EMPTY(this.oParentObject.w_MVCODCOM) AND NVL(this.oParentObject.w_TDFLNORC , "N" )<>"S", this.oParentObject.w_OCOM, this.oParentObject.w_MVCODCOM)
      if g_COMM="S" AND this.oParentObject.w_FLGCOM$"M-S" AND NOT EMPTY(this.oParentObject.w_MVCODCOM)
        this.oParentObject.w_MVCODATT = IIF(EMPTY(this.oParentObject.w_MVCODATT) and this.w_NOCOMM, this.oParentObject.w_OATT, this.oParentObject.w_MVCODATT)
      else
        this.oParentObject.w_MVCODATT = SPACE(15)
      endif
      * --- Se GSVE_BI3 lanciato da caricamento rapido (GSAR_BPG) allora
      *     valorizzo i dati relativi alla gestione progeti non dal documenti di origine 
      *     ma dalla penna se non vuoti...
      if Type("RigheSort.cMATR")<>"U"
        this.oParentObject.w_MVCODATT = IIF( Empty( Nvl( RigheSort.MVCODATT , "") ) , this.oParentObject.w_MVCODATT, RigheSort.MVCODATT )
        this.oParentObject.w_MVCODCOM = IIF( Empty( Nvl( RigheSort.MVCODCOM , "" ) ) , this.oParentObject.w_MVCODCOM , RigheSort.MVCODCOM )
      endif
    else
      this.oParentObject.w_MVCODCOM = SPACE(15)
      this.oParentObject.w_MVCODATT = SPACE(15)
    endif
    this.oParentObject.w_OCOM = IIF(this.oParentObject.w_MVTIPRIG<>"D", this.oParentObject.w_MVCODCOM, this.oParentObject.w_OCOM)
    this.oParentObject.w_OATT = IIF(this.oParentObject.w_MVTIPRIG<>"D", this.oParentObject.w_MVCODATT, this.oParentObject.w_OATT)
    * --- Leggo valuta e decimali commessa...
    *     Lettura ottimizzata svolta solo al cambio della commessa
    if this.oParentObject.w_MVCODCOM <> this.w_OTTCODCOM
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.oParentObject.w_MVCODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo le valute solo se valuta di commessa diversa da valuta di conto..
      if this.oParentObject.w_COCODVAL<>this.oParentObject.w_MVVALNAZ
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_COCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.oParentObject.w_COCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LDECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_LDECCOM = this.oParentObject.w_DECTOP
      endif
      this.w_OTTCODCOM = this.oParentObject.w_MVCODCOM
    endif
    this.oParentObject.w_DECCOM = this.w_LDECCOM
    * --- Legge tipo di Costo
    if NOT EMPTY(this.oParentObject.w_MVVOCCEN)
      * --- Read from VOC_COST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOC_COST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VCTIPCOS,VCDTOBSO"+;
          " from "+i_cTable+" VOC_COST where ";
              +"VCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVVOCCEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VCTIPCOS,VCDTOBSO;
          from (i_cTable) where;
              VCCODICE = this.oParentObject.w_MVVOCCEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
        this.w_DTOBSOVC = NVL(cp_ToDate(_read_.VCDTOBSO),cp_NullValue(_read_.VCDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controllo obsolescenza
      if NOT EMPTY(this.w_DTOBSOVC) AND this.w_DTOBSOVC<=this.oParentObject.w_MVDATDOC
        this.oParentObject.w_MVVOCCEN = SPACE(15)
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_MVCODCEN)
      * --- Read from CENCOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CENCOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDTOBSO"+;
          " from "+i_cTable+" CENCOST where ";
              +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_MVCODCEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDTOBSO;
          from (i_cTable) where;
              CC_CONTO = this.oParentObject.w_MVCODCEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBSOCC = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controllo obsolescenza
      if NOT EMPTY(this.w_DTOBSOCC) AND this.w_DTOBSOCC<=this.oParentObject.w_MVDATDOC
        this.oParentObject.w_MVCODCEN = SPACE(15)
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_MVCODCOM)
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNDTOBSO"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNDTOBSO;
          from (i_cTable) where;
              CNCODCAN = this.oParentObject.w_MVCODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBSOCM = NVL(cp_ToDate(_read_.CNDTOBSO),cp_NullValue(_read_.CNDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controllo obsolescenza
      if NOT EMPTY(this.w_DTOBSOCM) AND this.w_DTOBSOCM<=this.oParentObject.w_MVDATDOC
        this.oParentObject.w_MVCODCOM = SPACE(15)
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_MVCODATT)
      this.oParentObject.w_MVCODCOS = this.oParentObject.w_CODCOS
    else
      this.oParentObject.w_MVCODCOS = SPACE(5)
    endif
    * --- Importo di Commessa
    this.oParentObject.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.oParentObject.w_MVCODCOM),"S", " "), this.oParentObject.w_MVVALNAZ, this.oParentObject.w_COCODVAL, this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.oParentObject.w_DECCOM )
    if this.w_oFLAPCA="S" and g_COAC="S"
      * --- Se riga importata da documento che esplode i contributi la considero esplosa...
      this.oParentObject.w_TESTESPLACC = PADR(this.oParentObject.w_MVCODICE,41," ") + PADR(this.oParentObject.w_MVTIPCON,1," ")+PADR(iif( empty( this.oParentObject.w_MVCODORN ) , this.oParentObject.w_MVCODCON , this.oParentObject.w_MVCODORN),15," ")+PADR(YEAR(this.oParentObject.w_MVDATDOC),4," ")+PADR(MONTH(this.oParentObject.w_MVDATDOC),2," ")+PADR(DAY(this.oParentObject.w_MVDATDOC),2," ")+PADR(this.oParentObject.w_MVUNIMIS,3," ") + PADR(this.oParentObject.w_MVCODVAL,3," ") + PADR(this.oParentObject.w_FLFRAZ,1," ") + PADR(STR(this.oParentObject.w_PERIVA,6,2),6," ")+ PADR(this.oParentObject.w_MVCODIVA,5," ") + PADR(STR(this.oParentObject.w_MVQTAMOV,18, g_PERPQT ),18," ") + PADR(STR(IIF(this.oParentObject.w_MVQTAMOV=0,0,this.oParentObject.w_MVVALMAG/this.oParentObject.w_MVQTAMOV),18, this.oParentObject.w_DECUNI ),18," ") + PADR(this.oParentObject.w_TDFLAPCA,1," ") + PADR(this.oParentObject.w_ANFLAPCA,1," ") + PADR(this.oParentObject.w_ARFLAPCA,1," ") 
    endif
    * --- Aggiorna Totalizzatori
    this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_MVFLOMAG="X" AND this.oParentObject.w_FLSERA<>"S", this.oParentObject.w_MVVALRIG, 0)
    this.oParentObject.w_RIGNET1 = IIF(this.oParentObject.w_MVFLOMAG="X", this.oParentObject.w_MVVALRIG, 0)
    this.oParentObject.w_RIGNET2 = IIF(this.oParentObject.w_MVFLOMAG="X" AND NOT(this.oParentObject.w_PRESTA$"SA") AND NOT (NOT EMPTY(this.oParentObject.w_MVRIFORD) AND this.oParentObject.w_SPEGEN=0), this.oParentObject.w_MVVALRIG, 0)
    this.oParentObject.w_RIGNET3 = IIF(this.oParentObject.w_MVFLOMAG="X" AND this.oParentObject.w_PRESTA="S", this.oParentObject.w_MVVALRIG, 0)
    this.oParentObject.w_RIGNET4 = IIF(this.oParentObject.w_MVFLOMAG="X" AND this.oParentObject.w_PRESTA="A", this.oParentObject.w_MVVALRIG, 0)
    this.oParentObject.w_RIGNET5 = IIF(this.oParentObject.w_MVFLOMAG="X" AND NOT(this.oParentObject.w_PRESTA$"SA") AND (NOT EMPTY(this.oParentObject.w_MVRIFORD) AND this.oParentObject.w_SPEGEN=0), this.oParentObject.w_MVVALRIG, 0)
    if this.w_RICALCOLA
      * --- Solo se non sono gi� stati ricalcolati dal GSVE_BCD lanciato con NotifyEvent "Ricalcola"
      *     In questo caso altrimenti verrebbero ricalcolati doppi
      this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_MVVALRIG
      this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE + this.oParentObject.w_RIGNET
      this.oParentObject.w_TIMPACC = this.oParentObject.w_TIMPACC + this.oParentObject.w_MVIMPACC
      this.oParentObject.w_TOTRIP = this.oParentObject.w_TOTRIP + this.oParentObject.w_RIGNET1
      this.oParentObject.w_TOTRIP2 = this.oParentObject.w_TOTRIP2 + this.oParentObject.w_RIGNET2
      this.oParentObject.w_TOTRIP3 = this.oParentObject.w_TOTRIP3 + this.oParentObject.w_RIGNET3
      this.oParentObject.w_TOTRIP4 = this.oParentObject.w_TOTRIP4 + this.oParentObject.w_RIGNET4
      this.oParentObject.w_TOTRIP5 = this.oParentObject.w_TOTRIP5 + this.oParentObject.w_RIGNET5
    endif
    * --- ---------
    * --- Verifica il flag Evasione nei documenti d'origine della causale documento
    if this.w_DCFLEVAS="S"
      * --- Se No Evasione deve controllare disponibilit� (Caso Import Doc. Riservato)
      this.oParentObject.w_FLRRIF = " "
    endif
    this.oParentObject.w_FLDISP = IIF(g_PERDIS="S", this.oParentObject.w_ARTDIS, " ")
    this.oParentObject.w_MVVALULT = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND(this.oParentObject.w_MVIMPNAZ/this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECTOU))
    this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_DECTOP)
    this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MVPREZZO * this.oParentObject.w_MVQTAMOV) / this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECUNI))
    this.oParentObject.w_OFLDIF = this.oParentObject.w_MVCODICE+this.oParentObject.w_MVUNIMIS+STR(this.oParentObject.w_MVQTAMOV,12,3)+this.oParentObject.w_MVFLOMAG
    * --- Carica il Temporaneo
    this.w_CNT = this.w_CNT + 1
    ah_Msg("Inserimento riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.w_CNT)) )
    * --- Carica il Temporaneo dei Dati
    if this.w_ROWPRE=-1
      this.oParentObject.w_MVROWRIF = this.w_ROWPRE
      this.oParentObject.w_MVSERRIF = this.w_PRSERIAL
      this.oParentObject.w_MVCATCON = this.w_CATCON
    endif
    if Empty(this.oParentObject.w_MVCODATT) and this.oParentObject.w_FLCAUMAG="S" and !empty(this.oParentObject.w_PARCAMAG)
      this.oParentObject.w_MVCAUMAG = this.oParentObject.w_PARCAMAG
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMCAUCOL,CMFLIMPE,CMFLCASC,CMFLORDI,CMFLELGM,CMFLCOMM,CMFLAVAL,CMVARVAL"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMCAUCOL,CMFLIMPE,CMFLCASC,CMFLORDI,CMFLELGM,CMFLCOMM,CMFLAVAL,CMVARVAL;
          from (i_cTable) where;
              CMCODICE = this.oParentObject.w_MVCAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MVCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
        this.oParentObject.w_MVFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.oParentObject.w_MVFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.oParentObject.w_MVFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
        this.oParentObject.w_FLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
        this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
        this.oParentObject.w_MV_FLAGG = NVL(cp_ToDate(_read_.CMVARVAL),cp_NullValue(_read_.CMVARVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MVFLCASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.oParentObject.w_MVFLCASC)
      this.oParentObject.w_MVFLORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.oParentObject.w_MVFLORDI)
      this.oParentObject.w_MVFLIMPE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.oParentObject.w_MVFLIMPE)
      this.oParentObject.w_MVFLRISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.oParentObject.w_MVFLRISE)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_MVFLORCO = IIF(g_COMM="S", IIF(this.oParentObject.w_FLCOMM="I", "+", IIF(this.oParentObject.w_FLCOMM="D", "-", " ")), " ")
      this.oParentObject.w_MVFLCOCO = IIF(g_COMM="S", IIF(this.oParentObject.w_FLCOMM="C", "+", IIF(this.oParentObject.w_FLCOMM="S", "-", " ")), " ")
      if NOT EMPTY(NVL(this.oParentObject.w_MVCAUCOL, " "))
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MCAUCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
            from (i_cTable) where;
                CMCODICE = this.w_MCAUCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MVF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.oParentObject.w_MVF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.oParentObject.w_MVF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.oParentObject.w_MVF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.w_PADRE.SaveRow()     
    this.w_RECTRS = this.w_RECTRS + 1
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Raggruppamento articoli congruenti per
    *     Codice Articolo
    *     Unit� di Misura
    *     
    *     Centro di Costo/Ricavo  |
    *     Commessa                      |  --> solo se attivi nella causale di destinazione
    *     Attivit�                                 |
    *     
    *     Prezzo                             |  
    *     Sconti                              |  --> se Non attivo check Ricalcola Prezzo nella destinazione
    this.w_TESTRAG = .F.
    Local Comando, Frase, Istruzione
    this.w_Found = .F.
    SELECT RigheSort
    if FLGROR = "S"
      * --- Se sono su una riga che verr� raggruppata,
      *     memorizzo la chiave della Riga di Origine nel cursore L_Cur_Rag_Row e imposto a 0
      *     la riga (CPROWNUM) di destinazione che andr� a valorizzare in Pag 6
      *     Il cursore Verr� poi memorizzato in RAG_FATT nel GSVE_BMK
      this.w_LSERIAL = MVSERIAL
      this.w_LROWNUM = CPROWNUM
      this.w_LNUMRIF = -20
      this.w_LQTAEVA = Nvl(MVQTAEVA, 0)
      INSERT INTO ( L_Cur_Rag_Row ) VALUES (this.w_LSERIAL, this.w_LROWNUM, this.w_LNUMRIF, 0, this.w_LQTAEVA)
    endif
    this.w_POS = Recno()
    this.w_LCODICE = MVCODICE
    this.w_LUNIMIS = MVUNIMIS
    this.w_LCODCEN = Nvl(MVCODCEN,Space(15))
    this.w_LCODCOM = Nvl(MVCODCOM,Space(15))
    this.w_LCODATT = Nvl(MVCODATT,Space(15))
    this.w_LVISPRE = Nvl(VISPRE, 0)
    this.w_LSCONT1 = Nvl(MVSCONT1, 0)
    this.w_LSCONT2 = Nvl(MVSCONT2, 0)
    this.w_LSCONT3 = Nvl(MVSCONT3, 0)
    this.w_LSCONT4 = Nvl(MVSCONT4, 0)
    SELECT RigheSort
    Comando= "Locate"
     
 Frase=" For MVCODICE = this.w_LCODICE And MVUNIMIS = this.w_LUNIMIS And " + ; 
 "Nvl(MVCODCEN,Space(15)) = this.w_LCODCEN And " + ; 
 "Nvl(MVCODCOM,Space(15)) = this.w_LCODCOM And " + ; 
 'Nvl(MVCODATT,Space(15)) = this.w_LCODATT And FLGROR = "S" ' + ; 
 IIF(this.oParentObject.w_PRZDES<>"S", ; 
 "And Nvl(VISPRE,0) = this.w_LVISPRE And " + ; 
 "Nvl(MVSCONT1,0) = this.w_LSCONT1 And " + ; 
 "Nvl(MVSCONT2,0) = this.w_LSCONT2 And " + ; 
 "Nvl(MVSCONT3,0) = this.w_LSCONT3 And " + ; 
 "Nvl(MVSCONT4,0) = this.w_LSCONT4" ; 
 , "")
    Istruzione= Comando + Frase + " And Recno() > this.w_POS "
    &Istruzione
    this.w_Found = Found()
    * --- Poich� sto eseguendo una Scan su Righesort, se mediante la locate trovo 
    *     delle righe che validano la condizione significa che non sono sull'ultima di questo tipo (Recno() > w_POS)
    *     Se invece non trovo niente significa che sono sull'ultima riga di questo tipo e quindi devo inserirne una
    *     della somma di tutte le precedenti.
    if Not this.w_Found
      SUM MVQTAEVA, MVQTAMOV, OLQTAEVA TO this.w_LTOTQTA, this.w_LTOTMOV, this.w_LTOTEVA &Frase
      SELECT RigheSort
      Go this.w_POS
      REPLACE ; 
 MVQTAEVA With this.w_LTOTQTA, ; 
 MVQTAMOV With this.w_LTOTMOV, ; 
 OLQTAEVA With this.w_LTOTEVA
      this.w_TESTRAG = .T.
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- in ogni caso anche se non inserisco la riga devo inserire una riga di append 
      *     perch� l'elaborazione si aspetta di essere posizionata su una riga nuova e non
      *     gi� riempita.
      *     Il problema riscontrato � dato dalla ECPDROP lanciata in pag 4:
      *     in questo caso infatti non inserisco nessuna riga nel temporaneo, ma se cambia il documento
      *     viene rieseguita pagina 4 rileggendo i dati della nuova testata. 
      *     L'ECPDROP rilancia la TrsFromWork posizionata sulla riga descrittiva dei riferimenti
      *     causando lo svuotamento della stessa (poich� nel frattempo � stata lanciata pag 2 che azzera le variabili di work).
      this.w_PADRE.AddRow()     
    endif
    SELECT RigheSort
    Go this.w_POS
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se la prima U.M. non � frazionabile ed � attivo il flag Forza seconda U.M. e la Qt� nella Prima U.M. ha decimali
    *     ricalcolo la qt� nella Prima U.M. arrotondata alla unit� superiore
    * --- memorizzo il valore iniziale di mvqtaum1
    this.w_QTATEST = this.oParentObject.w_MVQTAUM1
    this.oParentObject.w_MVQTAUM1 = INT(this.w_QTATEST)+1
    * --- Ricalcolo la quantit� su riga in base alla nuova Qt� nella prima U.M.
    this.oParentObject.w_MVQTAMOV = cp_Round((this.oParentObject.w_MVQTAMOV*this.oParentObject.w_MVQTAUM1)/this.w_QTATEST,g_PERPQT)
    * --- verificare se corretto
    this.oParentObject.w_MVQTAIMP = this.oParentObject.w_MVQTAMOV
    this.oParentObject.w_MVQTAIM1 = this.oParentObject.w_MVQTAUM1
    * --- --
    * --- Per evitare di nuovo il ricalcolo della Qt� nella prima U.M. dovuta al calculate sul campo nei documenti
    this.oParentObject.o_MVQTAMOV = this.oParentObject.w_MVQTAMOV
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sbianco MVUNILOG se il documento di destinazione non movimenta esistenza o riservato
    this.oParentObject.w_MVUNILOG = IIF(NOT EMPTY(NVL(this.oParentObject.w_MVUNILOG," ")) AND EMPTY(this.oParentObject.w_MVFLCASC) AND EMPTY(this.oParentObject.w_MVFLRISE), SPACE(18), this.oParentObject.w_MVUNILOG)
    * --- In caso di documento con flag Packing LIst attivo vado a leggere i dati relativi
    *     a COllo e Confezione direttamente da Articoli o Chiavi Articoli a secoda dell'unit� di misura
    if this.oParentObject.w_FLPACK="S" OR NOT EMPTY(this.oParentObject.w_MVUNILOG)
      do case
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS3
          * --- Collo e confezione del Codice di Ricerca
          this.oParentObject.w_MVCODCOL = this.w_CATIPCO3
          this.oParentObject.w_CODCONF = this.w_CATPCON3
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS2 
          * --- Collo e confezione della 2^UM Articolo
          this.oParentObject.w_MVCODCOL = this.w_ARTIPCO2
          this.oParentObject.w_CODCONF = this.w_ARTPCON2
        case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS1
          * --- Collo e confezione della 1^UM Articolo
          this.oParentObject.w_MVCODCOL = this.w_ARTIPCO1
          this.oParentObject.w_CODCONF = this.w_ARTPCONF
      endcase
    endif
    if NOT EMPTY(NVL(this.oParentObject.w_MVKEYSAL," ")) AND NOT EMPTY(NVL(this.oParentObject.w_MVCODMAG," "))
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV;
          from (i_cTable) where;
              SLCODICE = this.oParentObject.w_MVKEYSAL;
              and SLCODMAG = this.oParentObject.w_MVCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
        this.oParentObject.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
        this.oParentObject.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
        this.oParentObject.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
        this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
        this.oParentObject.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
        this.oParentObject.w_CODVAV = NVL(cp_ToDate(_read_.SLCODVAV),cp_NullValue(_read_.SLCODVAV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MVFLULCA = IIF(this.oParentObject.w_FLAVA1="A" AND this.oParentObject.w_MVFLCASC $ "+-" AND this.oParentObject.w_MVDATDOC>=this.oParentObject.w_ULTCAR AND this.oParentObject.w_MVFLOMAG<>"S" And this.oParentObject.w_MVPREZZO<>0, "=", " ")
      this.oParentObject.w_MVFLULPV = IIF(this.oParentObject.w_FLAVA1="V" AND this.oParentObject.w_MVFLCASC $ "+-" AND this.oParentObject.w_MVDATDOC>=this.oParentObject.w_ULTSCA AND this.oParentObject.w_MVFLOMAG<>"S" And this.oParentObject.w_MVPREZZO<>0, "=", " ")
    endif
    if this.oParentObject.w_MVTIPRIG="R" AND ((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (g_PERUBI="S" AND (this.w_LFLUBIC="S" OR this.w_LF2UBIC="S")))
      * --- Importo Informazioni relative ai lotti
      this.w_FLLOTT1 = IIF((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (this.w_LFLUBIC="S" AND g_PERUBI="S") , Left(Alltrim(this.oParentObject.w_MVFLCASC)+IIF(this.oParentObject.w_MVFLRISE="+", "-", IIF(this.oParentObject.w_MVFLRISE="-", "+", " ")),1)," ")
      this.w_FLLOTT2 = IIF((this.oParentObject.w_FLLOTT $ "SC" AND g_PERLOT="S") OR (this.w_LF2UBIC="S" AND g_PERUBI="S") , Left(Alltrim(this.oParentObject.w_MVF2CASC)+IIF(this.oParentObject.w_MVF2RISE="+", "-", IIF(this.oParentObject.w_MVF2RISE="-", "+", " ")),1)," ")
      this.oParentObject.w_MVFLLOTT = this.w_FLLOTT1
      this.oParentObject.w_MVF2LOTT = this.w_FLLOTT2
      this.oParentObject.w_MVCODUBI = IIF(g_PERUBI="S" AND this.oParentObject.w_MVFLLOTT $ "+-" AND this.w_LFLUBIC="S" ,this.w_CODUBI,SPACE(20))
      this.oParentObject.w_MVCODUB2 = IIF(g_PERUBI="S" AND this.oParentObject.w_MVF2LOTT $ "+-" AND NOT EMPTY(this.oParentObject.w_MVCODMAT) AND this.w_LF2UBIC="S",this.w_CODUB2,SPACE(20))
      this.oParentObject.w_MVCODLOT = IIF(g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" AND ((this.oParentObject.w_MVFLLOTT $ "+-") OR (this.oParentObject.w_MVF2LOTT $ "+-" )) ,this.w_CODLOT,SPACE(20))
      this.oParentObject.w_MVLOTMAG = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUBI ) , SPACE(5) , this.oParentObject.w_MVCODMAG )
      this.oParentObject.w_MVLOTMAT = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUB2 ) , SPACE(5) , this.oParentObject.w_MVCODMAT )
      if NOT EMPTY(this.oParentObject.w_MVCODLOT)
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT,LODATSCA"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT,LODATSCA;
            from (i_cTable) where;
                LOCODICE = this.oParentObject.w_MVCODLOT;
                and LOCODART = this.oParentObject.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          this.w_LODATSCA = NVL(cp_ToDate(_read_.LODATSCA),cp_NullValue(_read_.LODATSCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if (this.w_LODATSCA<this.oParentObject.w_MVDATREG AND NOT EMPTY(this.w_LODATSCA)) 
          * --- Se lotto Scaduto o non Disponibile non viene importato
          this.oParentObject.w_MVCODLOT = SPACE(20)
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject)
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,35)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='DOC_COLL'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='DOC_MAST'
    this.cWorkTables[10]='DOC_RATE'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='MODASPED'
    this.cWorkTables[14]='PAG_AMEN'
    this.cWorkTables[15]='PORTI'
    this.cWorkTables[16]='SALDIART'
    this.cWorkTables[17]='TIP_DOCU'
    this.cWorkTables[18]='UNIMIS'
    this.cWorkTables[19]='VALUTE'
    this.cWorkTables[20]='VETTORI'
    this.cWorkTables[21]='VOCIIVA'
    this.cWorkTables[22]='VOC_COST'
    this.cWorkTables[23]='CONTI'
    this.cWorkTables[24]='MAGAZZIN'
    this.cWorkTables[25]='LOTTIART'
    this.cWorkTables[26]='SEDIAZIE'
    this.cWorkTables[27]='CON_TRAM'
    this.cWorkTables[28]='TRADARTI'
    this.cWorkTables[29]='ALT_DETT'
    this.cWorkTables[30]='DIPENDEN'
    this.cWorkTables[31]='CENCOST'
    this.cWorkTables[32]='PRA_CONT'
    this.cWorkTables[33]='DIC_INTE'
    this.cWorkTables[34]='PRE_STAZ'
    this.cWorkTables[35]='AGENTI'
    return(this.OpenAllTables(35))

  proc CloseCursors()
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
