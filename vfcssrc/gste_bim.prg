* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bim                                                        *
*              Analisi scaduto con interessi di mora                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_260]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2018-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bim",oParentObject)
return(i_retval)

define class tgste_bim as StdBatch
  * --- Local variables
  w_TIPO = space(1)
  w_SAGINT = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ORDATINT = ctod("  /  /  ")
  w_ORDATSCA = ctod("  /  /  ")
  w_ANNO = space(4)
  w_PERIOD = space(1)
  w_SAGRIF = 0
  w_MAGNDP = 0
  w_MAGDEP = 0
  w_MESS = space(10)
  w_COSERIAL = space(10)
  w_ADDINT = space(1)
  w_SAGCON = 0
  w_INTBCE = .f.
  w_SAGGIO = space(1)
  w_ANSPRINT = space(1)
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(31)
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_FLDAVE = space(1)
  w_TOTIMP = 0
  w_CODVAL = space(3)
  w_MODPAG = space(10)
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_FLCRSA = space(1)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_FLSOSP = space(1)
  w_NUMDIS = space(10)
  w_SIMVAL = space(5)
  w_DECTOT = 0
  w_FLRAGG = space(1)
  w_CAOVAL = 0
  w_CODAGE = space(5)
  w_TIPPAG = space(2)
  w_ORDINE = space(1)
  w_DATREG = ctod("  /  /  ")
  w_DESSUP = space(50)
  w_NUMPRO = 0
  w_FLINDI = 0
  w_PTFLRAGG = 0
  w_BANFIL = space(15)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_SEGNO = space(1)
  w_IMPAVE = 0
  w_IMPDAR = 0
  w_CAOAPE = 0
  w_FLVABD = space(1)
  w_DATINT = ctod("  /  /  ")
  w_GIORNI = 0
  w_TASSO = 0
  w_INTERESS = 0
  w_DATINIMO = ctod("  /  /  ")
  w_DATFINMO = ctod("  /  /  ")
  w_FASE = space(1)
  w_CAMBIO = 0
  w_TIPO = space(1)
  w_IMPMIN = 0
  w_GIOTOL = 0
  w_TIPESC = space(1)
  w_SEARCH_ANNO = space(5)
  w_SEARCH_PERIOD = space(5)
  w_LOOP = 0
  * --- WorkFile variables
  CONTI_idx=0
  INT_MORA_idx=0
  PAR_TITE_idx=0
  TMPTPAR_TITE_idx=0
  TMP_PART2_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Analisi Scaduto con Interessi di Mora (da GSTE_SIM)
    * --- Dichiarazione delle variabili della maschera
    * --- Dichiarazione delle variabili locali
    * --- Dichiarazione delle variabili per la scrittura nel cursore
    this.w_FASE = " "
    * --- Determinazione Partite Aperte
    if this.oParentObject.w_FLSALD $ "TR"
      * --- Solo Partite Aperte
      * --- Lancio GSTE_BPA per ottenere la tabella temporanea "TMP_PART_APE" 
      *     contenente le partite aperte filtrate con le principali selezioni.
      GSTE_BPA(this,this.oParentObject.w_SCAINI, this.oParentObject.w_SCAFIN, this.oParentObject.w_TIPCON,this.oParentObject.w_CODCON,this.oParentObject.w_CODCON,"","",; 
 this.oParentObject.w_PAGRB,this.oParentObject.w_PAGBO,this.oParentObject.w_PAGRD,this.oParentObject.w_PAGRI,this.oParentObject.w_PAGMA,this.oParentObject.w_PAGCA,"GSCG_BSA","N", "")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table TMPTPAR_TITE
      i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_GKQEXAQWYH[3]
      indexes_GKQEXAQWYH[1]='TIPCON'
      indexes_GKQEXAQWYH[2]='CODCON'
      indexes_GKQEXAQWYH[3]='MODPAG'
      vq_exec('QUERY\GSTE_SIM_E.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_GKQEXAQWYH,.f.)
      this.TMPTPAR_TITE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Valorizzo esclusioni
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      vq_exec("QUERY\GSTE_SIM.VQR", this, "APERTE")
      * --- Drop temporary table TMPTPAR_TITE
      i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPTPAR_TITE')
      endif
      * --- Legge le partite ancora aperte
      GSTE_BCP(this,"A","APERTE"," " , , , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
        CURTOTAB("APERTE" ,"TMP_PART")
      endif
    endif
    * --- Determinazione Partite di Chiusura
    if this.oParentObject.w_FLSALD $ "TC"
      * --- Legge le partite di chiusura
      * --- Create temporary table TMPTPAR_TITE
      i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE1SIM_E.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPTPAR_TITE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table TMP_PART2
      i_nIdx=cp_AddTableDef('TMP_PART2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTE1SIM.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      vq_exec("QUERY\GSTE10SIM.VQR", this, "CHIUSURE")
      * --- Drop temporary table TMPTPAR_TITE
      i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPTPAR_TITE')
      endif
    endif
    do case
      case this.oParentObject.w_FLSALD="T"
        * --- Elabora Tutte le Partite
        if RECCOUNT("APERTE")>0
          this.w_FASE = "A"
          if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            SELECT APERTE.*, "          " AS COSERIAL, "N" AS ADDINT, 0 AS SAGCON ; 
 FROM APERTE INTO CURSOR PARTITE
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if USED("PARTITE")
          SELECT PARTITE
          USE
        endif
        if RECCOUNT("CHIUSURE")>0
          this.w_FASE = "C"
          if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            SELECT CHIUSURE.*, "          " AS COSERIAL, "N" AS ADDINT, 0 AS SAGCON ; 
 FROM CHIUSURE WHERE NOT DELETED() INTO CURSOR PARTITE
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.oParentObject.w_FLSALD="C"
        * --- Elabora le Partite di Chiusura
        * --- Determinazione Partite legate al Contenzioso
        this.w_FASE = "C"
        if RECCOUNT("CHIUSURE")>0
          if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            SELECT CHIUSURE.*, "          " AS COSERIAL, "N" AS ADDINT, 0 AS SAGCON ; 
 FROM CHIUSURE WHERE NOT DELETED() INTO CURSOR PARTITE
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.oParentObject.w_FLSALD="R"
        * --- Elabora le Partite Aperte
        if RECCOUNT("APERTE")>0
          this.w_FASE = "A"
          if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            SELECT APERTE.*, "          " AS COSERIAL, "N" AS ADDINT, 0 AS SAGCON ; 
 FROM APERTE INTO CURSOR PARTITE
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    if USED("APERTE")
      SELECT APERTE
      USE
    endif
    if USED("CHIUSURE")
      SELECT CHIUSURE
      USE
    endif
    if USED("PARTITE")
      SELECT PARTITE
      USE
    endif
    if USED("CONTENZI")
      SELECT CONTENZI
      USE
    endif
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
    * --- Drop temporary table TMP_PART2
    i_nIdx=cp_GetTableDefIdx('TMP_PART2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART2')
    endif
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    if USED("PART_INT") AND RECCOUNT("PART_INT")>0
      * --- Applico i filtri sul risultato:
      *     - Tipo di Pagamento
      *     - Intervallo numero documento
      *     - intervallo alfa
      *     - intervallo date documento
       
 DELETE FROM PART_INT WHERE Not ; 
 ( ( ( TIPPAG IN (this.oParentObject.w_PAGRD , this.oParentObject.w_PAGBO , this.oParentObject.w_PAGRB , this.oParentObject.w_PAGCA , this.oParentObject.w_PAGRI , this.oParentObject.w_PAGMA ) And Not Empty(Nvl(TIPPAG,"")) ) ) ; 
 And ( Empty( this.oParentObject.w_NDOINI ) Or NUMDOC>= this.oParentObject.w_NDOINI ) And ( Empty( this.oParentObject.w_NDOFIN ) Or NUMDOC<= this.oParentObject.w_NDOFIN ); 
 And ( Empty( this.oParentObject.w_ADOINI ) Or ALFDOC>= this.oParentObject.w_ADOINI ) And ( Empty( this.oParentObject.w_ADOFIN ) Or ALFDOC<= this.oParentObject.w_ADOFIN ); 
 And ( Empty( this.oParentObject.w_DDOINI ) Or CP_TODATE(DATDOC) >= this.oParentObject.w_DDOINI ) And ( Empty( this.oParentObject.w_DDOFIN ) Or CP_TODATE(DATDOC) <= this.oParentObject.w_DDOFIN )) 
 
      * --- Elabora i Cursori e ritorna il cursore da stampare ordinato per codice conto
      SELECT PART_INT
      GO TOP
      SELECT CODVAL AS PTCODVAL, CP_TODATE(DATSCA) AS PTDATSCA, NUMPAR AS PTNUMPAR, TIPCON AS PNTIPCON, ; 
 CODCON AS PNCODCON, FLCRSA AS PTFLCRSA, DATDOC AS PNDATDOC, NUMDOC AS PNNUMDOC, ALFDOC AS PNALFDOC, ; 
 NVL(CODAGE, SPACE(5)) AS PTCODAGE, FLDAVE AS FLDAVE, TOTIMP AS TOTIMP, MODPAG AS PTMODPAG, BANAPP AS PTBANAPP, ; 
 NUMDIS AS PTNUMDIS, FLSOSP AS PTFLSOSP, DECTOT AS VADECTOT, SIMVAL AS VASIMVAL, PTSERIAL AS PTSERIAL, ; 
 PTROWORD AS PTROWORD, CPROWNUM AS CPROWNUM, FLRAGG AS PTFLRAGG, CAOVAL AS PTCAOVAL, DATREG AS DATREG, ; 
 DESSUP AS PNDESSUP, CAOAPE AS PTCAOAPE, FLVABD AS PTFLVABD, DATINT AS PTDATINT, GIORNI AS GIORNI, TASSO AS TASSO, ; 
 INTERESS AS INTERESS, DATINIMO AS DATINIMO, DATFINMO AS DATFINMO, FASE AS FASE, CAMBIO AS CAMBIO, SAGGIO AS SAGGIO; 
 FROM PART_INT WHERE Abs(VAL2MON(INTERESS,CAMBIO,1,DATDOC, CODVAL, GETVALUT(CODVAL, "VADECTOT")))>=this.oParentObject.w_INTMIN INTO CURSOR __TMP__ ORDER BY 5,34,4,3,2,1,28,32,6,7,8,9
    else
      if USED("PART_INT")
        SELECT PART_INT
        USE
      endif
      ah_ErrorMsg("Per le selezioni impostate non ci sono dati da stampare",,"")
      i_retcode = 'stop'
      return
    endif
    if USED("PART_INT")
      SELECT PART_INT
      USE
    endif
    * --- Passaggio delle variabili al report
    L_DATRIL=this.oParentObject.w_DATRIL
    L_SALD=this.oParentObject.w_FLSALD
    L_SCAINI=this.oParentObject.w_SCAINI
    L_SCAFIN=this.oParentObject.w_SCAFIN
    L_NUMINI=this.oParentObject.w_NUMINI
    L_NUMFIN=this.oParentObject.w_NUMFIN
    L_SCACLF=this.oParentObject.w_TIPCON
    L_CONSEL=IIF(this.oParentObject.w_TIPCON="G", this.oParentObject.w_CODCON, " ")
    L_CLISEL=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODCON, " ")
    L_FORSEL=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODCON, " ")
    L_PARD=this.oParentObject.w_PAGRD
    L_PABO=this.oParentObject.w_PAGBO
    L_PARB=this.oParentObject.w_PAGRB
    L_PARI=this.oParentObject.w_PAGRI
    L_PACA=this.oParentObject.w_PAGCA
    L_PAMA=this.oParentObject.w_PAGMA
    L_NDOINI=this.oParentObject.w_NDOINI
    L_NDOFIN=this.oParentObject.w_NDOFIN
    L_ADOINI=this.oParentObject.w_ADOFIN
    L_ADOFIN=this.oParentObject.w_ADOFIN
    L_DDOINI=this.oParentObject.w_DDOINI
    L_DDOFIN=this.oParentObject.w_DDOFIN
    * --- Copia del cursore di stampa sulla tabella temporanea per permettere all utente di modificare la query di estrazione dati dell output utente
    CURTOTAB("__TMP__" ,"TMPTPAR_TITE")
    * --- Lancio del report
    VX_EXEC(""+ALLTRIM(this.oParentObject.w_OQRY)+", "+ALLTRIM(this.oParentObject.w_OREP)+"",THIS)
    * --- Drop temporary table TMPTPAR_TITE
    i_nIdx=cp_GetTableDefIdx('TMPTPAR_TITE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTPAR_TITE')
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("CURS_INT_MORA")
      SELECT CURS_INT_MORA
      USE
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione cursore per calcolo interessi di mora
    if NOT USED("PART_INT")
      CREATE CURSOR PART_INT (DATSCA D(8), NUMPAR C(31), TIPCON C(1), CODCON C(15), FLDAVE C(1), TOTIMP N(18,4), CODVAL C(3), ; 
 MODPAG C(10), BANAPP C(10), BANNOS C(15), NUMDOC N(15,0), ALFDOC C(10), DATDOC D(8), FLCRSA C(1), PTSERIAL C(10), PTROWORD N(4,0), ; 
 CPROWNUM N(3,0), FLSOSP C(1), NUMDIS C(10), SIMVAL C(5), DECTOT N(1,0), FLRAGG C(1), CAOVAL N(12,7), CODAGE C(5), TIPPAG C(2), ; 
 ORDINE C(1), DATREG D(8), DESSUP C(50), NUMPRO N(4,0), FLINDI N(3,0), PTFLRAGG N(3,0), BANFIL C(15), PTSERRIF C(10), PTORDRIF N(4,0), ; 
 PTNUMRIF N(3,0), SEGNO C(1), IMPAVE N(18,4), IMPDAR N(18,4), CAOAPE N(12,7), FLVABD C(1), DATINT D(8), GIORNI N(4,0), TASSO N(6,2), ; 
 INTERESS N(18,4), DATINI D(8), DATFIN D(8), DATINIMO D(8), DATFINMO D(8), FASE C(1), CAMBIO N(12,7), SAGGIO C(1))
    endif
    SELECT PARTITE
    GO TOP
    SCAN
    this.w_DATSCA = CP_TODATE(DATSCA)
    this.w_NUMPAR = NVL(NUMPAR, SPACE(31))
    this.w_LTIPCON = NVL(TIPCON, SPACE(1))
    this.w_LCODCON = NVL(CODCON, SPACE(15))
    this.w_FLDAVE = NVL(FLDAVE, SPACE(1))
    this.w_CODVAL = NVL(CODVAL, SPACE(3))
    this.w_FLCRSA = NVL(FLCRSA, SPACE(1))
    this.w_CAOVAL = NVL(CAOVAL, 0)
    this.w_IMPMIN = NVL(IMPMIN, 0)
    this.w_GIOTOL = NVL(GIOTOL, 0)
    this.w_TIPESC = NVL(TIPESC, " ")
    * --- Variabile utilizzata per gestire in modo corretto il totale partita nella stampa
    this.w_SAGGIO = "S"
    if this.w_CODVAL=g_PERVAL
      this.w_TOTIMP = NVL(TOTIMP, 0)
    else
      if this.oParentObject.w_FLSALD="R"
        if this.w_FLCRSA="A"
          this.w_TOTIMP = VAL2MON(TOTIMP, Nvl(this.w_CAOVAL,0), g_CAOVAL, this.oParentObject.w_DATRIL, g_PERVAL)
        else
          this.w_TOTIMP = VAL2MON(TOTIMP, GETCAM(this.w_CODVAL, this.oParentObject.w_DATRIL, 0), g_CAOVAL, this.oParentObject.w_DATRIL, g_PERVAL)
        endif
      else
        this.w_TOTIMP = VAL2MON(TOTIMP, Nvl(this.w_CAOVAL,0), g_CAOVAL, this.oParentObject.w_DATRIL, g_PERVAL)
      endif
    endif
    this.w_MODPAG = NVL(MODPAG, SPACE(10))
    this.w_BANAPP = NVL(BANAPP, SPACE(10))
    this.w_BANNOS = NVL(BANNOS, SPACE(15))
    this.w_NUMDOC = NVL(NUMDOC, 0)
    this.w_ALFDOC = NVL(ALFDOC, SPACE(10))
    this.w_DATDOC = CP_TODATE(DATDOC)
    this.w_PTSERIAL = NVL(PTSERIAL, SPACE(10))
    this.w_PTROWORD = NVL(PTROWORD, 0)
    this.w_CPROWNUM = NVL(CPROWNUM, 0)
    this.w_FLSOSP = NVL(FLSOSP, SPACE(1))
    this.w_NUMDIS = NVL(NUMDIS, SPACE(10))
    this.w_SIMVAL = NVL(SIMVAL, SPACE(5))
    this.w_DECTOT = NVL(DECTOT, 0)
    this.w_FLRAGG = NVL(FLRAGG, SPACE(1))
    this.w_CODAGE = NVL(CODAGE, SPACE(5))
    this.w_TIPPAG = NVL(TIPPAG, SPACE(2))
    this.w_ORDINE = NVL(ORDINE, SPACE(1))
    this.w_DATREG = CP_TODATE(DATREG)
    this.w_DESSUP = NVL(DESSUP, SPACE(50))
    this.w_NUMPRO = NVL(NUMPRO, 0)
    this.w_FLINDI = NVL(FLINDI, 0)
    this.w_PTFLRAGG = NVL(PTFLRAGG, 0)
    this.w_BANFIL = NVL(BANFIL, SPACE(15))
    this.w_PTSERRIF = NVL(PTSERRIF, SPACE(10))
    this.w_PTORDRIF = NVL(PTORDRIF, 0)
    this.w_PTNUMRIF = NVL(PTNUMRIF, 0)
    this.w_SEGNO = NVL(SEGNO, SPACE(1))
    this.w_IMPAVE = NVL(IMPAVE, 0)
    this.w_IMPDAR = NVL(IMPDAR, 0)
    this.w_CAOAPE = NVL(CAOAPE, 0)
    this.w_FLVABD = NVL(FLVABD, SPACE(1))
    this.w_DATINT = CP_TODATE(DATINT)
    this.w_ANSPRINT = NVL(ANSPRINT,"N")
    this.w_SAGINT = NVL(SAGINT, 0)
    this.w_COSERIAL = NVL(COSERIAL, SPACE(10))
    this.w_ADDINT = NVL(ADDINT, "N")
    this.w_SAGCON = NVL(SAGCON, 0)
    this.w_GIORNI = 0
    this.w_TASSO = 0
    this.w_INTERESS = 0
    this.w_INTBCE = .F.
    * --- Determinazione Tasso di Interesse
    if NOT EMPTY(this.w_COSERIAL)
      * --- Se la partita � legata ad un Contenzioso, l'addebito degli Interessi � guidato da quanto presente nel Contenzioso
      do case
        case this.w_ADDINT="S"
          * --- Saggio Concordato
          this.w_TASSO = this.w_SAGCON
        case this.w_ADDINT="M"
          * --- Saggio di Mora
          this.w_TASSO = IIF(this.w_ANSPRINT="S",this.w_SAGINT,0)
          * --- L'interesse verr� letto dall'anagrafica Saggio Interessi di Mora
          this.w_INTBCE = .T.
        otherwise
          * --- Non Applicati
          this.w_TASSO = 0
      endcase
    else
      this.w_TASSO = IIF(this.w_ANSPRINT="S",0,this.w_SAGINT)
      * --- Se vuoto oppure � stato attivato il flag "Spread su saggio di mora", 
      *     l'interesse verr� letto dall'anagrafica Saggio Interessi di Mora
      this.w_INTBCE = IIF(EMPTY(this.w_SAGINT) OR this.w_ANSPRINT="S",.T.,.F.)
    endif
    if this.w_FASE="A"
      * --- Partite Aperte
      this.w_DATINI = IIF(EMPTY(Nvl(this.w_DATINT,cp_CharToDate("  -  -  "))), this.w_DATSCA, this.w_DATINT)
      this.w_DATFIN = this.oParentObject.w_DATRIL
      this.w_CAMBIO = IIF(this.w_CODVAL=g_CODEUR,1,GETCAM(this.w_CODVAL, this.oParentObject.w_DATRIL, 0))
      * --- con opportuna sistemazione del segno
      this.w_TOTIMP = iif(this.w_SEGNO="A",this.w_TOTIMP*(-1),this.w_TOTIMP)
    else
      * --- Partite di Chiusura
      * --- Lettura Data Scadenza (dalla partita di creazione)
      this.w_ORDATINT = cp_CharToDate("  -  -  ")
      * --- Cerca prima l'insoluto
      if NOT EMPTY(this.w_PTSERRIF)
        this.w_ORDATINT = CP_TODATE(PTDATINT)
      endif
      * --- Se non trova l'insoluto allora fa riferimento alla partita originaria
      if EMPTY(this.w_ORDATINT) AND NOT EMPTY(this.w_PTSERRIF)
        * --- Solo per partite create dopo la 2.2 per le quali � valorizzato il campo PTDATINT
        this.w_ORDATINT = CP_TODATE(PTDATINT)
      endif
      this.w_DATINI = IIF(EMPTY(this.w_ORDATINT), this.w_DATSCA, this.w_ORDATINT)
      this.w_DATFIN = this.w_DATREG
      this.w_CAMBIO = this.w_CAOVAL
      * --- Cambio della sezione per le partite di chiusura
      this.w_FLDAVE = IIF(this.w_FLDAVE="A", "D", "A")
    endif
    * --- Eseguo esclusioni
    if this.w_TIPESC$ "D-E" AND this.w_GIOTOL>0
      * --- Tengo conto dei giorni di tolleranza per la moratoria
      this.w_DATINI = this.w_DATINI+this.w_GIOTOL
    endif
    if this.w_TIPESC $ "I-E" AND this.w_IMPMIN<>0 AND ABS(this.w_TOTIMP)<ABS(this.w_IMPMIN)
      * --- Escludo dal calcolo degli interessi gli impoti minimi
      this.w_TASSO = 0
    endif
    if this.w_INTBCE
      * --- Se il tasso viene letto dall'anagrafica Saggio Interessi di Mora � necessario dettagliare gli interessi per ogni semestre
      this.w_ANNO = STR(YEAR(this.w_DATINI+1),4)
      this.w_PERIOD = IIF(MONTH(this.w_DATINI+1)<7, "P", "S")
      do while this.w_DATINI<this.w_DATFIN
        this.w_SAGRIF = 0
        this.w_MAGNDP = 0
        this.w_MAGDEP = 0
        w_WORKAREA=ALIAS()
        this.w_SEARCH_ANNO = this.w_ANNO
        this.w_SEARCH_PERIOD = this.w_PERIOD
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SAGRIF = IMSAGRIF
        this.w_MAGNDP = IMMAGNDP
        this.w_MAGDEP = IMMAGDEP
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
        * --- Calcolo il numero di giorni per i quali devono essere calcolati gli interessi
        * --- Data inizio Mora
        this.w_DATINIMO = this.w_DATINI+1
        * --- Data fine Mora
        this.w_DATFINMO = IIF(this.w_PERIOD="P", cp_CharToDate("30-06-"+this.w_ANNO), cp_CharToDate("31-12-"+this.w_ANNO))
        if this.w_DATFIN<this.w_DATFINMO
          this.w_GIORNI = this.w_DATFIN - this.w_DATINI
          this.w_DATFINMO = this.w_DATFIN
        else
          this.w_GIORNI = this.w_DATFINMO - this.w_DATINI
        endif
        * --- Determinazione del tasso in funzione del flag Beni Deperibili
        if this.w_TIPESC $ "I-E" AND this.w_IMPMIN<>0 AND ABS(this.w_TOTIMP)<ABS(this.w_IMPMIN)
          * --- Escludo dal calcolo degli interessi gli impoti minimi
          this.w_TASSO = 0
        else
          this.w_TASSO = this.w_SAGRIF+IIF(this.w_FLVABD="S", this.w_MAGDEP, this.w_MAGNDP)+IIF(this.w_ANSPRINT="S",this.w_SAGINT,0)
          this.w_TASSO = IIF(this.w_TASSO<=0,0,this.w_TASSO)
        endif
        * --- Calcolo degli interessi
        this.w_INTERESS = cp_ROUND(this.w_TOTIMP*((this.w_TASSO*this.w_GIORNI)/36500),g_PERPVL)
        * --- Scrittura di un nuovo record nel cursore
        if Abs(VAL2MON(this.w_INTERESS,this.w_CAMBIO,1,this.w_DATDOC, this.w_CODVAL, GETVALUT(this.w_CODVAL, "VADECTOT")))>=this.oParentObject.w_INTMIN 
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_SAGGIO = "N"
        endif
        this.w_DATINI = this.w_DATFINMO
        this.w_ANNO = STR(VAL(this.w_ANNO)+IIF(this.w_PERIOD="S", 1, 0), 4)
        this.w_PERIOD = IIF(this.w_PERIOD="S", "P", "S")
        * --- Rivalorizzo la variabile per l'inserimento della partita con gli interessi relativi al secondo semestre
      enddo
    else
      * --- Se il tasso non � vuoto (nel Contenzioso o nell'anagrafica Clienti/Fornitori), allora non � necessario dettagliare gli interessi per ogni semestre
      if this.w_DATINI<this.w_DATFIN
        * --- Data inizio Mora
        this.w_DATINIMO = this.w_DATINI+1
        * --- Data fine Mora
        this.w_DATFINMO = this.w_DATFIN
        * --- Calcolo il numero di giorni per i quali devono essere calcolati gli interessi
        this.w_GIORNI = this.w_DATFIN - this.w_DATINI
        * --- Calcolo degli interessi
        this.w_INTERESS = cp_ROUND(this.w_TOTIMP*((this.w_TASSO*this.w_GIORNI)/36500), 2)
        * --- Scrittura di un nuovo record nel cursore
        if EMPTY(this.w_COSERIAL) OR this.w_ADDINT<>"N"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Se w_COSERIAL non � vuoto (la partita � lagata ad un Contenzioso) e se nel Contenzioso � specificato che gli Interessi sono Non Applicati
        *     allora la partita non viene inclusa nel cursore di stampa
      endif
    endif
    SELECT PARTITE
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura di un nuovo record nel cursore
     
 INSERT INTO PART_INT (DATSCA, NUMPAR, TIPCON, CODCON, FLDAVE, TOTIMP, CODVAL, MODPAG, ; 
 BANAPP, BANNOS, NUMDOC, ALFDOC, DATDOC, FLCRSA, PTSERIAL, PTROWORD, CPROWNUM, ; 
 FLSOSP, NUMDIS, SIMVAL, DECTOT, FLRAGG, CAOVAL, CODAGE, TIPPAG, ORDINE, DATREG, ; 
 DESSUP, NUMPRO, FLINDI, PTFLRAGG, BANFIL, PTSERRIF, PTORDRIF, PTNUMRIF, SEGNO, ; 
 IMPAVE, IMPDAR, CAOAPE, FLVABD, DATINT, GIORNI, TASSO, INTERESS, DATINIMO, DATFINMO, FASE, CAMBIO, SAGGIO) VALUES ; 
 (this.w_DATSCA, this.w_NUMPAR, this.w_LTIPCON, this.w_LCODCON, this.w_FLDAVE, this.w_TOTIMP, this.w_CODVAL, this.w_MODPAG, ; 
 this.w_BANAPP, this.w_BANNOS, this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_FLCRSA, this.w_PTSERIAL, this.w_PTROWORD, this.w_CPROWNUM, ; 
 this.w_FLSOSP, this.w_NUMDIS, this.w_SIMVAL, this.w_DECTOT, this.w_FLRAGG, this.w_CAOVAL, this.w_CODAGE, this.w_TIPPAG, this.w_ORDINE, this.w_DATREG, ; 
 this.w_DESSUP, this.w_NUMPRO, this.w_FLINDI, this.w_PTFLRAGG, this.w_BANFIL, this.w_PTSERRIF, this.w_PTORDRIF, this.w_PTNUMRIF, this.w_SEGNO, ; 
 this.w_IMPAVE, this.w_IMPDAR, this.w_CAOAPE, this.w_FLVABD, this.w_DATINT, this.w_GIORNI, this.w_TASSO, this.w_INTERESS, this.w_DATINIMO, this.w_DATFINMO, ; 
 this.w_FASE, this.w_CAMBIO, this.w_SAGGIO)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Criterio di applicazione
    *     ==================
    *     1) per intestatario
    *     2) per categoria contabile e mastro
    *     3) per mastro
    *     4) per categoria contabile
    *     5) generica
     
 Dimension ARRESCL(5) 
 ARRESCL[1]="I" 
 ARRESCL[2]="E" 
 ARRESCL[3]="M" 
 ARRESCL[4]="C" 
 ARRESCL[5]="G"
    this.w_LOOP = 0
    do while this.w_LOOP<5
      this.w_LOOP = this.w_LOOP + 1
      this.w_TIPO = ARRESCL(this.w_LOOP)
      * --- Write into TMPTPAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPTPAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTPAR_TITE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="TIPCON,CODCON,MODPAG"
        do vq_exec with 'gsteisim',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTPAR_TITE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPTPAR_TITE.TIPCON = _t2.TIPCON";
                +" and "+"TMPTPAR_TITE.CODCON = _t2.CODCON";
                +" and "+"TMPTPAR_TITE.MODPAG = _t2.MODPAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TIPESC = _t2.TIPESC";
            +",IMPMIN = _t2.IMPMIN";
            +",GIOTOL = _t2.GIOTOL";
        +",TESTESC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPTPAR_TITE','TESTESC');
            +i_ccchkf;
            +" from "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPTPAR_TITE.TIPCON = _t2.TIPCON";
                +" and "+"TMPTPAR_TITE.CODCON = _t2.CODCON";
                +" and "+"TMPTPAR_TITE.MODPAG = _t2.MODPAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE, "+i_cQueryTable+" _t2 set ";
            +"TMPTPAR_TITE.TIPESC = _t2.TIPESC";
            +",TMPTPAR_TITE.IMPMIN = _t2.IMPMIN";
            +",TMPTPAR_TITE.GIOTOL = _t2.GIOTOL";
        +",TMPTPAR_TITE.TESTESC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPTPAR_TITE','TESTESC');
            +Iif(Empty(i_ccchkf),"",",TMPTPAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPTPAR_TITE.TIPCON = t2.TIPCON";
                +" and "+"TMPTPAR_TITE.CODCON = t2.CODCON";
                +" and "+"TMPTPAR_TITE.MODPAG = t2.MODPAG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set (";
            +"TIPESC,";
            +"IMPMIN,";
            +"GIOTOL,";
            +"TESTESC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.TIPESC,";
            +"t2.IMPMIN,";
            +"t2.GIOTOL,";
            +cp_NullLink(cp_ToStrODBC("S"),'TMPTPAR_TITE','TESTESC')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPTPAR_TITE.TIPCON = _t2.TIPCON";
                +" and "+"TMPTPAR_TITE.CODCON = _t2.CODCON";
                +" and "+"TMPTPAR_TITE.MODPAG = _t2.MODPAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTPAR_TITE set ";
            +"TIPESC = _t2.TIPESC";
            +",IMPMIN = _t2.IMPMIN";
            +",GIOTOL = _t2.GIOTOL";
        +",TESTESC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPTPAR_TITE','TESTESC');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                +" and "+i_cTable+".MODPAG = "+i_cQueryTable+".MODPAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TIPESC = (select TIPESC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",IMPMIN = (select IMPMIN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",GIOTOL = (select GIOTOL from "+i_cQueryTable+" where "+i_cWhere+")";
        +",TESTESC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPTPAR_TITE','TESTESC');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    enddo
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules)
      do case
        case this.w_FASE="A"
          vq_exec("QUERY\GSTE0CON.VQR", this, "PARTITE")
        case this.w_FASE="C"
          vq_exec("QUERY\GSTE8CON.VQR", this, "PARTITE")
      endcase
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("CURS_INT_MORA")
      Select CURS_INT_MORA 
 LOCATE FOR IM__ANNO=this.w_SEARCH_ANNO AND IMPERIOD=this.w_SEARCH_PERIOD
      if NOT FOUND( ) AND NOT EMPTY(NVL(this.w_SEARCH_ANNO,"")) AND NOT EMPTY(NVL(this.w_SEARCH_PERIOD,"")) 
        VQ_EXEC("QUERY\INT_MORA.VQR",this,"TMP_CURS_INT_MORA")
        if USED("TMP_CURS_INT_MORA")
          Select TMP_CURS_INT_MORA
          scatter memvar
          Insert into CURS_INT_MORA from memvar
          Select TMP_CURS_INT_MORA 
 Use 
 Select CURS_INT_MORA
        endif
      endif
    else
      VQ_EXEC("QUERY\INT_MORA.VQR",this,"CURS_INT_MORA") 
 wr_CURS_INT_MORA=WRCURSOR("CURS_INT_MORA") 
 Select CURS_INT_MORA
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='INT_MORA'
    this.cWorkTables[3]='PAR_TITE'
    this.cWorkTables[4]='*TMPTPAR_TITE'
    this.cWorkTables[5]='*TMP_PART2'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
