* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bou                                                        *
*              Autonumber nominativo                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2008-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bou",oParentObject)
return(i_retval)

define class tgsar_bou as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiusta il Codice Cliente/Fornitore calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica p_NOM
    * --- Chiamato all'evento New record da (GSAR_ANO)
    if NVL (g_OFNUME," ")="S"
      if LEN(ALLTRIM(p_NOM))<>0
        this.oParentObject.w_NOCODICE = RIGHT(this.oParentObject.w_NOCODICE, LEN(ALLTRIM(p_NOM)))
      endif
    else
      * --- Anche se non gestito, l'autonumber ritorna sempre una stringa di zeri in questo caso, deve essere suotata
      this.oParentObject.w_NOCODICE = SPACE(15)
    endif
    this.oParentObject.op_NOCODICE = this.oParentObject.w_NOCODICE
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
