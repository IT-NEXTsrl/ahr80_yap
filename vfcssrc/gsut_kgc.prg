* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kgc                                                        *
*              Modulo gestione clienti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_43]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-30                                                      *
* Last revis.: 2014-02-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kgc",oParentObject))

* --- Class definition
define class tgsut_kgc as StdForm
  Top    = 99
  Left   = 294

  * --- Standard Properties
  Width  = 357
  Height = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-13"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kgc"
  cComment = "Modulo gestione clienti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DBMS = space(1)
  w_SERVER = space(20)
  w_DATABASE = space(20)
  w_USER_ID = space(20)
  w_PASSWORD = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kgcPag1","gsut_kgc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDBMS_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DBMS=space(1)
      .w_SERVER=space(20)
      .w_DATABASE=space(20)
      .w_USER_ID=space(20)
      .w_PASSWORD=space(20)
        .w_DBMS = 'S'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(Ah_MsgFormat("Questa procedura permette la creazione del file mgc_inf.ini%0Tale file � utilizzato dal filtro ITAHR12 per importare i dati nel%0modulo gestione clienti"))
    endwith
    this.DoRTCalc(2,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(Ah_MsgFormat("Questa procedura permette la creazione del file mgc_inf.ini%0Tale file � utilizzato dal filtro ITAHR12 per importare i dati nel%0modulo gestione clienti"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(Ah_MsgFormat("Questa procedura permette la creazione del file mgc_inf.ini%0Tale file � utilizzato dal filtro ITAHR12 per importare i dati nel%0modulo gestione clienti"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSERVER_1_7.enabled = this.oPgFrm.Page1.oPag.oSERVER_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDBMS_1_6.RadioValue()==this.w_DBMS)
      this.oPgFrm.Page1.oPag.oDBMS_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERVER_1_7.value==this.w_SERVER)
      this.oPgFrm.Page1.oPag.oSERVER_1_7.value=this.w_SERVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATABASE_1_8.value==this.w_DATABASE)
      this.oPgFrm.Page1.oPag.oDATABASE_1_8.value=this.w_DATABASE
    endif
    if not(this.oPgFrm.Page1.oPag.oUSER_ID_1_9.value==this.w_USER_ID)
      this.oPgFrm.Page1.oPag.oUSER_ID_1_9.value=this.w_USER_ID
    endif
    if not(this.oPgFrm.Page1.oPag.oPASSWORD_1_10.value==this.w_PASSWORD)
      this.oPgFrm.Page1.oPag.oPASSWORD_1_10.value=this.w_PASSWORD
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kgcPag1 as StdContainer
  Width  = 353
  height = 246
  stdWidth  = 353
  stdheight = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDBMS_1_6 as StdCombo with uid="GVLYIPNLAY",rtseq=1,rtrep=.f.,left=141,top=70,width=142,height=21;
    , ToolTipText = "Tipologia database da importare";
    , HelpContextID = 26291402;
    , cFormVar="w_DBMS",RowSource=""+"SQL Server,"+"Oracle,"+"IBM DB2,"+"PostgreSQL", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDBMS_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    iif(this.value =3,'D',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oDBMS_1_6.GetRadio()
    this.Parent.oContained.w_DBMS = this.RadioValue()
    return .t.
  endfunc

  func oDBMS_1_6.SetRadio()
    this.Parent.oContained.w_DBMS=trim(this.Parent.oContained.w_DBMS)
    this.value = ;
      iif(this.Parent.oContained.w_DBMS=='S',1,;
      iif(this.Parent.oContained.w_DBMS=='O',2,;
      iif(this.Parent.oContained.w_DBMS=='D',3,;
      iif(this.Parent.oContained.w_DBMS=='P',4,;
      0))))
  endfunc

  add object oSERVER_1_7 as StdField with uid="LMEEKXBTIX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SERVER", cQueryName = "SERVER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Server di database se SQL server - non specificato altrimenti",;
    HelpContextID = 188602586,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=141, Top=98, InputMask=replicate('X',20)

  func oSERVER_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DBMS='S')
    endwith
   endif
  endfunc

  add object oDATABASE_1_8 as StdField with uid="ZIYXPZZTQX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATABASE", cQueryName = "DATABASE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome database se SQL server - nome servizio se Oracle - alias database se DB2",;
    HelpContextID = 58540667,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=141, Top=122, InputMask=replicate('X',20)

  add object oUSER_ID_1_9 as StdField with uid="EBTKNHCDUJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_USER_ID", cQueryName = "USER_ID",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "User id di accesso al database",;
    HelpContextID = 224224582,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=141, Top=145, InputMask=replicate('X',20)

  add object oPASSWORD_1_10 as StdField with uid="LJNRBTASHK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PASSWORD", cQueryName = "PASSWORD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Password di accesso al database",;
    HelpContextID = 48182074,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=141, Top=169, InputMask=replicate('X',20)


  add object oObj_1_11 as cp_runprogram with uid="BTUUMCMSTK",left=-3, top=251, width=77,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSUT_BGC ("load")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_12 as cp_runprogram with uid="HFQWCUFJLU",left=77, top=251, width=90,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSUT_BGC ("save")',;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_13 as cp_setobjprop with uid="LXDORNTKAE",left=173, top=251, width=38,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj='w_PASSWORD',cProp='passwordchar',;
    nPag=1;
    , HelpContextID = 145933286


  add object oBtn_1_14 as StdButton with uid="SQNLIXYMPH",left=246, top=196, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 32035610;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="VJEYGWVSZN",left=298, top=196, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 24746938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_calclbl with uid="MKHHZVXRKN",left=6, top=3, width=340,height=62,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 145933286

  add object oStr_1_1 as StdString with uid="MBNZNFVNNE",Visible=.t., Left=42, Top=71,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo database:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="IMYVQMQCGK",Visible=.t., Left=77, Top=98,;
    Alignment=1, Width=50, Height=15,;
    Caption="Server:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="HCEQFUPYNW",Visible=.t., Left=51, Top=122,;
    Alignment=1, Width=76, Height=15,;
    Caption="Database:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="OTAMEVGHBE",Visible=.t., Left=79, Top=145,;
    Alignment=1, Width=48, Height=15,;
    Caption="User id:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="CTTQWCACMB",Visible=.t., Left=57, Top=169,;
    Alignment=1, Width=70, Height=15,;
    Caption="Password:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kgc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
