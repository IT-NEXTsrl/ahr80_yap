* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bmm                                                        *
*              Controlli finali partite                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_243]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-07                                                      *
* Last revis.: 2012-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bmm",oParentObject,m.pOper)
return(i_retval)

define class tgste_bmm as StdBatch
  * --- Local variables
  pOper = space(10)
  w_PNSERIAL = space(10)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTDATAPE = ctod("  /  /  ")
  w_PTNUMPAR = space(31)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTCODVAL = space(3)
  w_PTTOTIMP = 0
  w_PTFLCRSA = space(1)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_OK = .f.
  w_APPO = 0
  w_CFUNC = space(10)
  w_MESS = space(100)
  w_TOTSAL = 0
  w_TOTALE = 0
  w_TOT = 0
  w_PADRE = .NULL.
  w_ROWNUM = 0
  w_MAXROW = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali in Eliminazione/Modifica Partite (da GSTE_MMP) 
    this.w_PADRE = this.oParentObject
    if this.pOper $ "Edit-Query"
      this.w_OK = .T.
      this.w_CFUNC = this.pOper
      this.w_MESS = ah_Msgformat("Operazione abbandonata")
      this.w_PNSERIAL = this.w_PADRE.w_PTSERIAL
      this.w_PTTIPCON = this.w_PADRE.w_PTTIPCON
      this.w_PTCODCON = this.w_PADRE.w_PTCODCON
      this.w_PTROWORD = this.w_PADRE.w_PTROWORD
      this.w_CPROWNUM = this.w_PADRE.w_CPROWNUM
      this.w_MAXROW = 0
      * --- Cerco il Cprownum pi� alto delle partite associate alla stessa riga contabile
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" PAR_TITE ";
            +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PTSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.oParentObject.w_PTROWORD)+"";
            +" order by CPROWNUM DESC";
             ,"_Curs_PAR_TITE")
      else
        select CPROWNUM from (i_cTable);
         where PTSERIAL=this.oParentObject.w_PTSERIAL AND PTROWORD=this.oParentObject.w_PTROWORD;
         order by CPROWNUM DESC;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        this.w_MAXROW = _Curs_PAR_TITE.CPROWNUM 
        Exit
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
      * --- Verifica eventual esistenza di partite a Saldo
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        if Nvl(this.w_PADRE.Rowstatus(),"N") $ "A"
          * --- Inserisco CPROWNUM corretto ovvero che tiene conto anche delle altre scadenze
          this.w_MAXROW = this.w_MAXROW +1
          this.w_PADRE.set("CPROWNUM",this.w_MAXROW,.T.,.T.)     
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        if Nvl(this.w_PADRE.Rowstatus(),"N") $ "U"
          this.w_PTDATSCA = CP_TODATE(this.w_PADRE.Get("t_PTDATSCA"))
          this.w_PTDATAPE = CP_TODATE(this.w_PADRE.Get("t_PTDATAPE"))
          this.w_PTNUMPAR = Nvl(this.w_PADRE.Get("t_PTNUMPAR"),Space(31))
          this.w_PTCODVAL = Nvl(this.w_PADRE.Get("t_PTCODVAL"),Space(3))
          this.w_PTFLCRSA = Nvl(this.w_PADRE.Get("t_PTFLCRSA")," ")
          this.w_PTTOTIMP = ABS(NVL(this.w_PADRE.Get("t_PTTOTIMP"), 0))
          this.w_TOTALE = NVL(this.w_PADRE.Get("t_OTOTIMP"), 0)-this.oParentObject.w_RESIDUO
          if this.w_PTFLCRSA="C" AND NOT EMPTY(this.w_PTNUMPAR) AND NOT EMPTY(this.w_PTCODVAL) AND this.w_TOTALE>0 
            ah_Msg("Verifica eventuali partite saldate...",.T.)
            * --- Se sono in interrogazione, blocco la cancellazione se esistono partite di saldo.
            if this.w_CFUNC="Query"
              this.w_MESS = ah_Msgformat("Esistono partite di chiusura associate al documento; (data:%1)%0Cancellazione impossibile", DTOC(this.oParentObject.w_DATSCA) )
              this.w_OK = .F.
            else
              if this.oParentObject.w_PTTOTIMP-this.w_TOTALE>0
                * --- Se sono in modifica, d� la possibilit� di modificare la partita creando una nuova partita con l'importo modificato.
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.w_MESS = ah_Msgformat("Importo nuova partita inferiore a totale saldato")
                this.w_OK = .F.
              endif
            endif
            WAIT CLEAR
          endif
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos()     
      this.bUpdateParentObject = .F.
      if this.w_OK = .F.
        * --- Abbandona la Transazione
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    else
      * --- Eseguo Update Importo Partita originaria con totale partite di Saldo collegate
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_TOTALE = Nvl(this.w_PADRE.Get("t_TOTIMP"),0)
        if this.w_TOTALE>0
          this.w_ROWNUM = Nvl(this.w_PADRE.Get("CPROWNUM"),0)
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTTOTIMP ="+cp_NullLink(cp_ToStrODBC(this.w_TOTALE),'PAR_TITE','PTTOTIMP');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                   )
          else
            update (i_cTable) set;
                PTTOTIMP = this.w_TOTALE;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.oParentObject.w_PTSERIAL;
                and PTROWORD = this.oParentObject.w_PTROWORD;
                and CPROWNUM = this.w_ROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos()     
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNAGG_01 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_01),'PNT_MAST','PNAGG_01');
        +",PNAGG_02 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_02),'PNT_MAST','PNAGG_02');
        +",PNAGG_03 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_03),'PNT_MAST','PNAGG_03');
        +",PNAGG_04 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_04),'PNT_MAST','PNAGG_04');
        +",PNAGG_05 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_05),'PNT_MAST','PNAGG_05');
        +",PNAGG_06 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNAGG_06),'PNT_MAST','PNAGG_06');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
               )
      else
        update (i_cTable) set;
            PNAGG_01 = this.oParentObject.w_PNAGG_01;
            ,PNAGG_02 = this.oParentObject.w_PNAGG_02;
            ,PNAGG_03 = this.oParentObject.w_PNAGG_03;
            ,PNAGG_04 = this.oParentObject.w_PNAGG_04;
            ,PNAGG_05 = this.oParentObject.w_PNAGG_05;
            ,PNAGG_06 = this.oParentObject.w_PNAGG_06;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.oParentObject.w_PTSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se sono in modifica, d� la possibilit� di modificare la partita creando una nuova partita con l'importo modificato.
    if ah_YesNo("Esistono partite di chiusura associate al documento; (data:%1)%0Si vuole duplicare la partita?","", DTOC(this.oParentObject.w_DATSCA) )
      this.w_ROWNUM = Nvl(this.w_PADRE.Get("CPROWNUM"),0)
      this.w_PADRE.set("t_TOTIMP",this.w_TOTALE,.T.,.T.)     
      this.w_PADRE.set("i_SRV"," ",.T.,.T.)     
      * --- Cerco il max(CPROWNUM) per creare la nuova partita di creazione
      * --- Valorizzo le variabili di Work della riga da "duplicare"
      this.w_PADRE.WorkFromTrs()     
      * --- --Crea nuova riga con la differenza
      Append Blank
      * --- Scrivo nella nuova riga le variabili di Work della riga "duplicata"
      *     Questo per evitare di scrivere tutte le righe del temporaneo
      this.oParentObject.w_PTTOTIMP = this.oParentObject.w_RESIDUO + this.oParentObject.w_PTTOTIMP- this.oParentObject.w_OTOTIMP
      this.oParentObject.w_TOTIMP = 0
      this.w_MAXROW = this.w_MAXROW +1
      this.w_PADRE.set("i_SRV","A",.T.,.T.)     
      this.w_PADRE.set("CPROWNUM",this.w_MAXROW,.T.,.T.)     
      this.w_PADRE.TrsFromWork()     
    else
      this.w_MESS = ah_Msgformat("Operazione abbandonata")
      this.w_OK = .F.
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='PNT_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
