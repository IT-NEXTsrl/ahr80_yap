* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_buo                                                        *
*              Controllo cicli su unita organizzative                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-05                                                      *
* Last revis.: 2005-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_buo",oParentObject)
return(i_retval)

define class tgsar_buo as StdBatch
  * --- Local variables
  w_NodiDaVisitare = 0
  w_NodiVisitati = 0
  w_ElemFigl = space(5)
  w_ElemPadr = space(5)
  w_Messaggio = space(200)
  * --- WorkFile variables
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --GSAR_BUO - Lanciato da GSAR_AUO algli eventi Insert Start e Update Start.
    * --- --Codice Azienda
    * --- --Codice dell'unit� organizzativa
    * --- --Unit� organizzativa o sede di raggruppamento
    Create Cursor ElencoElementiVisitati (Elemento C(5), Padre C(5)) 
 =WrCursor("ElencoElementiVisitati")
    if ! empty (this.oParentObject.w_SECODRAG) and !bTrsErr
      * --- Select from SEDIAZIE
      i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) AS QUAMMULT  from "+i_cTable+" SEDIAZIE ";
            +" where SECODAZI = "+cp_ToStrODBC(this.oParentObject.w_SECODAZI)+"";
             ,"_Curs_SEDIAZIE")
      else
        select COUNT (*) AS QUAMMULT from (i_cTable);
         where SECODAZI = this.oParentObject.w_SECODAZI;
          into cursor _Curs_SEDIAZIE
      endif
      if used('_Curs_SEDIAZIE')
        select _Curs_SEDIAZIE
        locate for 1=1
        do while not(eof())
        this.w_NodiDaVisitare = QUAMMULT
          select _Curs_SEDIAZIE
          continue
        enddo
        use
      endif
      this.w_NodiVisitati = 0
      this.w_ElemFigl = this.oParentObject.w_SECODDES
      this.w_ElemPadr = this.oParentObject.w_SECODRAG
       
 L_OldArea = Select() 
 SELECT ElencoElementiVisitati 
 APPEND BLANK 
 REPLACE Elemento WITH this.w_ElemFigl 
 REPLACE Padre WITH this.w_ElemPadr 
 Select (L_OldArea)
      do while this.w_NodiDaVisitare >= this.w_NodiVisitati and ! empty(this.w_ElemPadr)
        this.w_NodiVisitati = this.w_NodiVisitati + 1
        * --- --In ogni ciclo l'elemento corrente � l'elemento padre del ciclo precedente.
        *     L'elemento padre deve essere letto dopo aver determinato il nuovo elemento corrente
        this.w_ElemFigl = this.w_ElemPadr
        * --- -- Legge l'elemento padre dell'elemento corrente
        * --- Read from SEDIAZIE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SECODRAG"+;
            " from "+i_cTable+" SEDIAZIE where ";
                +"SECODAZI = "+cp_ToStrODBC(this.oParentObject.w_SECODAZI);
                +" and SECODDES = "+cp_ToStrODBC(this.w_ElemFigl);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SECODRAG;
            from (i_cTable) where;
                SECODAZI = this.oParentObject.w_SECODAZI;
                and SECODDES = this.w_ElemFigl;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ElemPadr = NVL(cp_ToDate(_read_.SECODRAG),cp_NullValue(_read_.SECODRAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ElemPadr = NVL(this.w_ElemPadr,space(5))
        if ! Empty (this.w_ElemPadr)
           
 L_OldArea = Select() 
 Select ElencoElementiVisitati 
 GO TOP 
 Locate for Elemento = this.w_ElemFigl
          if Found()
            this.w_Messaggio = ah_Msgformat("� stato rilevato un ciclo. Impossibile salvare")
             
 Select (L_OldArea)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Messaggio
             
 EXIT
          else
             
 APPEND BLANK 
 REPLACE Elemento WITH this.w_ElemFigl 
 REPLACE Elemento WITH this.w_ElemPadr
          endif
           
 Select(L_OldArea)
        endif
      enddo
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SEDIAZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
