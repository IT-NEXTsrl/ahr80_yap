* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kfd                                                        *
*              Dettaglio cash flow                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-14                                                      *
* Last revis.: 2014-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kfd",oParentObject))

* --- Class definition
define class tgste_kfd as StdForm
  Top    = 5
  Left   = 10

  * --- Standard Properties
  Width  = 485
  Height = 351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-05"
  HelpContextID=48903273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  cPrg = "gste_kfd"
  cComment = "Dettaglio cash flow"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOCONTO = space(1)
  w_CONTOTESO = space(15)
  w_DESCONTO = space(35)
  w_COLSEL1 = 0
  w_ROWSEL1 = 0
  w_RIGA = 0
  w_COLONNA = 0
  w_VALCONTO = space(3)
  w_DESVAL = space(35)
  w_TIPOSEL = space(15)
  w_VALOREPER = 0
  w_VALORETOT = 0
  w_COLDD = 0
  w_ROWDD = 0
  w_CONTOCASSA = space(15)
  w_DESCONTOC = space(35)
  w_ZOOMTIPO = .NULL.
  w_STRPERIODO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kfdPag1","gste_kfd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMTIPO = this.oPgFrm.Pages(1).oPag.ZOOMTIPO
    this.w_STRPERIODO = this.oPgFrm.Pages(1).oPag.STRPERIODO
    DoDefault()
    proc Destroy()
      this.w_ZOOMTIPO = .NULL.
      this.w_STRPERIODO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOCONTO=space(1)
      .w_CONTOTESO=space(15)
      .w_DESCONTO=space(35)
      .w_COLSEL1=0
      .w_ROWSEL1=0
      .w_RIGA=0
      .w_COLONNA=0
      .w_VALCONTO=space(3)
      .w_DESVAL=space(35)
      .w_TIPOSEL=space(15)
      .w_VALOREPER=0
      .w_VALORETOT=0
      .w_COLDD=0
      .w_ROWDD=0
      .w_CONTOCASSA=space(15)
      .w_DESCONTOC=space(35)
      .w_COLSEL1=oParentObject.w_COLSEL1
      .w_ROWSEL1=oParentObject.w_ROWSEL1
        .w_TIPOCONTO = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CONTOTESO))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMTIPO.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
          .DoRTCalc(3,5,.f.)
        .w_RIGA = .w_ROWSEL1
        .w_COLONNA = .w_COLSEL1
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_VALCONTO))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.STRPERIODO.Calculate(.w_STRPERIODO.caption)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
          .DoRTCalc(9,9,.f.)
        .w_TIPOSEL = .w_ZoomTIPO.getVar('TIPO')
        .w_VALOREPER = .w_ZoomTIPO.getVar('VALORE')
        .w_VALORETOT = .w_ZoomTIPO.getVar('TOTALE')
        .w_COLDD = iif(.w_ZoomTIPO.GRD.ActiveColumn=0,.w_COLDD,.w_ZoomTIPO.GRD.ActiveColumn)
        .w_ROWDD = iif(.w_ZoomTIPO.GRD.ActiveRow=0,.w_ROWDD,.w_ZoomTIPO.GRD.ActiveRow)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CONTOCASSA))
          .link_1_25('Full')
        endif
    endwith
    this.DoRTCalc(16,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_COLSEL1=.w_COLSEL1
      .oParentObject.w_ROWSEL1=.w_ROWSEL1
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_3('Full')
        .oPgFrm.Page1.oPag.ZOOMTIPO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(3,7,.t.)
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.STRPERIODO.Calculate(.w_STRPERIODO.caption)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(9,9,.t.)
            .w_TIPOSEL = .w_ZoomTIPO.getVar('TIPO')
            .w_VALOREPER = .w_ZoomTIPO.getVar('VALORE')
            .w_VALORETOT = .w_ZoomTIPO.getVar('TOTALE')
            .w_COLDD = iif(.w_ZoomTIPO.GRD.ActiveColumn=0,.w_COLDD,.w_ZoomTIPO.GRD.ActiveColumn)
            .w_ROWDD = iif(.w_ZoomTIPO.GRD.ActiveRow=0,.w_ROWDD,.w_ZoomTIPO.GRD.ActiveRow)
          .link_1_25('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMTIPO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.STRPERIODO.Calculate(.w_STRPERIODO.caption)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCONTOTESO_1_3.visible=!this.oPgFrm.Page1.oPag.oCONTOTESO_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESCONTO_1_4.visible=!this.oPgFrm.Page1.oPag.oDESCONTO_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCONTOCASSA_1_25.visible=!this.oPgFrm.Page1.oPag.oCONTOCASSA_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDESCONTOC_1_26.visible=!this.oPgFrm.Page1.oPag.oDESCONTOC_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMTIPO.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.STRPERIODO.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CONTOTESO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTOTESO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTOTESO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CONTOTESO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CONTOTESO)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTOTESO = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCONTO = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CONTOTESO = space(15)
      endif
      this.w_DESCONTO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTOTESO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALCONTO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALCONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALCONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALCONTO)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALCONTO = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALCONTO = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALCONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTOCASSA
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTOCASSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTOCASSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONTOCASSA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCONTO;
                       ,'ANCODICE',this.w_CONTOCASSA)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTOCASSA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCONTOC = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CONTOCASSA = space(15)
      endif
      this.w_DESCONTOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTOCASSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCONTOTESO_1_3.value==this.w_CONTOTESO)
      this.oPgFrm.Page1.oPag.oCONTOTESO_1_3.value=this.w_CONTOTESO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONTO_1_4.value==this.w_DESCONTO)
      this.oPgFrm.Page1.oPag.oDESCONTO_1_4.value=this.w_DESCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALCONTO_1_12.value==this.w_VALCONTO)
      this.oPgFrm.Page1.oPag.oVALCONTO_1_12.value=this.w_VALCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_14.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_14.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTOCASSA_1_25.value==this.w_CONTOCASSA)
      this.oPgFrm.Page1.oPag.oCONTOCASSA_1_25.value=this.w_CONTOCASSA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONTOC_1_26.value==this.w_DESCONTOC)
      this.oPgFrm.Page1.oPag.oDESCONTOC_1_26.value=this.w_DESCONTOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_kfdPag1 as StdContainer
  Width  = 481
  height = 351
  stdWidth  = 481
  stdheight = 351
  resizeXpos=369
  resizeYpos=118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONTOTESO_1_3 as StdField with uid="AHNGSFHRKB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONTOTESO", cQueryName = "CONTOTESO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 106890345,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=88, Top=3, InputMask=replicate('X',15), cLinkFile="COC_MAST", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_CONTOTESO"

  func oCONTOTESO_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'C')
    endwith
  endfunc

  func oCONTOTESO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCONTO_1_4 as StdField with uid="BSBFLZNSTQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCONTO", cQueryName = "DESCONTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 5129605,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=216, Top=3, InputMask=replicate('X',35)

  func oDESCONTO_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'C')
    endwith
  endfunc


  add object ZOOMTIPO as cp_zoombox with uid="FMSRBJXCFE",left=-1, top=42, width=466,height=237,;
    caption='ZOOMTIPO',;
   bGlobalFont=.t.,;
    cTable="COC_MAST",cZoomFile="GSTE_KFD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.f.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    nPag=1;
    , HelpContextID = 72871707


  add object oObj_1_7 as cp_runprogram with uid="SWAQIYXQRU",left=-1, top=355, width=177,height=27,;
    caption='GSTE_BFD',;
   bGlobalFont=.t.,;
    prg="GSTE_BFD('1')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 89154474

  add object oVALCONTO_1_12 as StdField with uid="ULDBNWFKEM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VALCONTO", cQueryName = "VALCONTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 5100197,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=137, Top=280, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALCONTO"

  func oVALCONTO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object STRPERIODO as cp_calclbl with uid="QHUNQNGSZD",left=115, top=27, width=208,height=14,;
    caption='STRPERIODO',;
   bGlobalFont=.t.,;
    caption="Periodo",;
    nPag=1;
    , HelpContextID = 205809483

  add object oDESVAL_1_14 as StdField with uid="VDPMFPXPWT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 41859786,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=178, Top=280, InputMask=replicate('X',35)


  add object oObj_1_16 as cp_runprogram with uid="VIMSISVUVS",left=5, top=422, width=177,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BFD('2')",;
    cEvent = "w_zoomtipo selected",;
    nPag=1;
    , HelpContextID = 129094374


  add object oBtn_1_23 as StdButton with uid="GHXPFFDRPI",left=411, top=303, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 41585850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCONTOCASSA_1_25 as StdField with uid="YCHTUXOJKW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CONTOCASSA", cQueryName = "CONTOCASSA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 90129833,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=88, Top=3, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCONTO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONTOCASSA"

  func oCONTOCASSA_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'G')
    endwith
  endfunc

  func oCONTOCASSA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCONTOC_1_26 as StdField with uid="YSKOZGJZGL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCONTOC", cQueryName = "DESCONTOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 5130677,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=216, Top=3, InputMask=replicate('X',35)

  func oDESCONTOC_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'G')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="OZOXSNJUMX",Visible=.t., Left=5, Top=4,;
    Alignment=1, Width=80, Height=18,;
    Caption="Conto banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'C')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="OVHUZCXQWC",Visible=.t., Left=6, Top=280,;
    Alignment=1, Width=126, Height=18,;
    Caption="Importi espressi in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="VQSHYODPEI",Visible=.t., Left=145, Top=3,;
    Alignment=0, Width=207, Height=21,;
    Caption="Scadenze non domiciliate"  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'N')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="HUVOXKDAVI",Visible=.t., Left=6, Top=399,;
    Alignment=0, Width=146, Height=18,;
    Caption="Lancia nuovo drill down"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="NFCISISQHZ",Visible=.t., Left=5, Top=4,;
    Alignment=1, Width=80, Height=18,;
    Caption="Conto cassa:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPOCONTO <> 'G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kfd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
