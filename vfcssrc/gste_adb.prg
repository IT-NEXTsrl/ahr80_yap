* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_adb                                                        *
*              Informazioni aggiuntive bonifici esteri                         *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_139]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-22                                                      *
* Last revis.: 2014-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgste_adb")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgste_adb")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgste_adb")
  return

* --- Class definition
define class tgste_adb as StdPCForm
  Width  = 734
  Height = 344+35
  Top    = 12
  Left   = 7
  cComment = "Informazioni aggiuntive bonifici esteri"
  cPrg = "gste_adb"
  HelpContextID=92943465
  add object cnt as tcgste_adb
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgste_adb as PCContext
  w_DINUMDIS = space(10)
  w_LFLDEFI = space(10)
  w_DIROWORD = 0
  w_DIROWNUM = 0
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_DITIPOPA = space(2)
  w_DICOMMIS = space(2)
  w_DICOSCOM = 0
  w_DIBANBEN = space(10)
  w_DICONCOR = space(25)
  w_CODIBAN = space(34)
  w_CODBIC = space(11)
  w_ORDINE = space(34)
  w_DIORDINE = space(34)
  w_DICONTAT = space(3)
  w_DIFUNUFF = space(17)
  w_DITIPAVV = space(3)
  w_DINOMINA = space(35)
  w_STRUTTUR = space(1)
  w_DITIPDO1 = space(3)
  w_DICODDO1 = space(17)
  w_DIDATDO1 = space(8)
  w_DITIPDO2 = space(3)
  w_DICODDO2 = space(17)
  w_DIDATDO2 = space(8)
  w_DITIPDO3 = space(3)
  w_DICODDO3 = space(17)
  w_DIDATDO3 = space(8)
  w_DITIPDO4 = space(3)
  w_DICODDO4 = space(17)
  w_DIDATDO4 = space(8)
  w_DIALTER1 = space(35)
  w_DIALTER2 = space(35)
  w_DIALTER3 = space(35)
  w_DESBAN = space(42)
  w_DICODCON = space(15)
  w_DESCONT = space(40)
  w_INDIRIZZO = space(35)
  w_CAP = space(9)
  w_LOCALITA = space(30)
  w_PROVINCIA = space(2)
  w_NUMPAR = space(31)
  w_DATSCA = space(8)
  w_BANAPP = space(10)
  w_CODVAL = space(3)
  w_BANNOS = space(15)
  w_SIMVAL = space(5)
  w_TOTSCA = 0
  w_DITIPDES = space(3)
  w_DIDATESE = space(8)
  w_TIPDIS = space(1)
  w_TOTDIS = 0
  w_NOCVS = space(1)
  proc Save(oFrom)
    this.w_DINUMDIS = oFrom.w_DINUMDIS
    this.w_LFLDEFI = oFrom.w_LFLDEFI
    this.w_DIROWORD = oFrom.w_DIROWORD
    this.w_DIROWNUM = oFrom.w_DIROWNUM
    this.w_CODCON = oFrom.w_CODCON
    this.w_TIPCON = oFrom.w_TIPCON
    this.w_DITIPOPA = oFrom.w_DITIPOPA
    this.w_DICOMMIS = oFrom.w_DICOMMIS
    this.w_DICOSCOM = oFrom.w_DICOSCOM
    this.w_DIBANBEN = oFrom.w_DIBANBEN
    this.w_DICONCOR = oFrom.w_DICONCOR
    this.w_CODIBAN = oFrom.w_CODIBAN
    this.w_CODBIC = oFrom.w_CODBIC
    this.w_ORDINE = oFrom.w_ORDINE
    this.w_DIORDINE = oFrom.w_DIORDINE
    this.w_DICONTAT = oFrom.w_DICONTAT
    this.w_DIFUNUFF = oFrom.w_DIFUNUFF
    this.w_DITIPAVV = oFrom.w_DITIPAVV
    this.w_DINOMINA = oFrom.w_DINOMINA
    this.w_STRUTTUR = oFrom.w_STRUTTUR
    this.w_DITIPDO1 = oFrom.w_DITIPDO1
    this.w_DICODDO1 = oFrom.w_DICODDO1
    this.w_DIDATDO1 = oFrom.w_DIDATDO1
    this.w_DITIPDO2 = oFrom.w_DITIPDO2
    this.w_DICODDO2 = oFrom.w_DICODDO2
    this.w_DIDATDO2 = oFrom.w_DIDATDO2
    this.w_DITIPDO3 = oFrom.w_DITIPDO3
    this.w_DICODDO3 = oFrom.w_DICODDO3
    this.w_DIDATDO3 = oFrom.w_DIDATDO3
    this.w_DITIPDO4 = oFrom.w_DITIPDO4
    this.w_DICODDO4 = oFrom.w_DICODDO4
    this.w_DIDATDO4 = oFrom.w_DIDATDO4
    this.w_DIALTER1 = oFrom.w_DIALTER1
    this.w_DIALTER2 = oFrom.w_DIALTER2
    this.w_DIALTER3 = oFrom.w_DIALTER3
    this.w_DESBAN = oFrom.w_DESBAN
    this.w_DICODCON = oFrom.w_DICODCON
    this.w_DESCONT = oFrom.w_DESCONT
    this.w_INDIRIZZO = oFrom.w_INDIRIZZO
    this.w_CAP = oFrom.w_CAP
    this.w_LOCALITA = oFrom.w_LOCALITA
    this.w_PROVINCIA = oFrom.w_PROVINCIA
    this.w_NUMPAR = oFrom.w_NUMPAR
    this.w_DATSCA = oFrom.w_DATSCA
    this.w_BANAPP = oFrom.w_BANAPP
    this.w_CODVAL = oFrom.w_CODVAL
    this.w_BANNOS = oFrom.w_BANNOS
    this.w_SIMVAL = oFrom.w_SIMVAL
    this.w_TOTSCA = oFrom.w_TOTSCA
    this.w_DITIPDES = oFrom.w_DITIPDES
    this.w_DIDATESE = oFrom.w_DIDATESE
    this.w_TIPDIS = oFrom.w_TIPDIS
    this.w_TOTDIS = oFrom.w_TOTDIS
    this.w_NOCVS = oFrom.w_NOCVS
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DINUMDIS = this.w_DINUMDIS
    oTo.w_LFLDEFI = this.w_LFLDEFI
    oTo.w_DIROWORD = this.w_DIROWORD
    oTo.w_DIROWNUM = this.w_DIROWNUM
    oTo.w_CODCON = this.w_CODCON
    oTo.w_TIPCON = this.w_TIPCON
    oTo.w_DITIPOPA = this.w_DITIPOPA
    oTo.w_DICOMMIS = this.w_DICOMMIS
    oTo.w_DICOSCOM = this.w_DICOSCOM
    oTo.w_DIBANBEN = this.w_DIBANBEN
    oTo.w_DICONCOR = this.w_DICONCOR
    oTo.w_CODIBAN = this.w_CODIBAN
    oTo.w_CODBIC = this.w_CODBIC
    oTo.w_ORDINE = this.w_ORDINE
    oTo.w_DIORDINE = this.w_DIORDINE
    oTo.w_DICONTAT = this.w_DICONTAT
    oTo.w_DIFUNUFF = this.w_DIFUNUFF
    oTo.w_DITIPAVV = this.w_DITIPAVV
    oTo.w_DINOMINA = this.w_DINOMINA
    oTo.w_STRUTTUR = this.w_STRUTTUR
    oTo.w_DITIPDO1 = this.w_DITIPDO1
    oTo.w_DICODDO1 = this.w_DICODDO1
    oTo.w_DIDATDO1 = this.w_DIDATDO1
    oTo.w_DITIPDO2 = this.w_DITIPDO2
    oTo.w_DICODDO2 = this.w_DICODDO2
    oTo.w_DIDATDO2 = this.w_DIDATDO2
    oTo.w_DITIPDO3 = this.w_DITIPDO3
    oTo.w_DICODDO3 = this.w_DICODDO3
    oTo.w_DIDATDO3 = this.w_DIDATDO3
    oTo.w_DITIPDO4 = this.w_DITIPDO4
    oTo.w_DICODDO4 = this.w_DICODDO4
    oTo.w_DIDATDO4 = this.w_DIDATDO4
    oTo.w_DIALTER1 = this.w_DIALTER1
    oTo.w_DIALTER2 = this.w_DIALTER2
    oTo.w_DIALTER3 = this.w_DIALTER3
    oTo.w_DESBAN = this.w_DESBAN
    oTo.w_DICODCON = this.w_DICODCON
    oTo.w_DESCONT = this.w_DESCONT
    oTo.w_INDIRIZZO = this.w_INDIRIZZO
    oTo.w_CAP = this.w_CAP
    oTo.w_LOCALITA = this.w_LOCALITA
    oTo.w_PROVINCIA = this.w_PROVINCIA
    oTo.w_NUMPAR = this.w_NUMPAR
    oTo.w_DATSCA = this.w_DATSCA
    oTo.w_BANAPP = this.w_BANAPP
    oTo.w_CODVAL = this.w_CODVAL
    oTo.w_BANNOS = this.w_BANNOS
    oTo.w_SIMVAL = this.w_SIMVAL
    oTo.w_TOTSCA = this.w_TOTSCA
    oTo.w_DITIPDES = this.w_DITIPDES
    oTo.w_DIDATESE = this.w_DIDATESE
    oTo.w_TIPDIS = this.w_TIPDIS
    oTo.w_TOTDIS = this.w_TOTDIS
    oTo.w_NOCVS = this.w_NOCVS
    PCContext::Load(oTo)
enddefine

define class tcgste_adb as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 734
  Height = 344+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-31"
  HelpContextID=92943465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  DIS_BOES_IDX = 0
  BAN_CHE_IDX = 0
  BAN_CONTI_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cFile = "DIS_BOES"
  cKeySelect = "DINUMDIS,DIROWORD,DIROWNUM"
  cKeyWhere  = "DINUMDIS=this.w_DINUMDIS and DIROWORD=this.w_DIROWORD and DIROWNUM=this.w_DIROWNUM"
  cKeyWhereODBC = '"DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';
      +'+" and DIROWORD="+cp_ToStrODBC(this.w_DIROWORD)';
      +'+" and DIROWNUM="+cp_ToStrODBC(this.w_DIROWNUM)';

  cKeyWhereODBCqualified = '"DIS_BOES.DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';
      +'+" and DIS_BOES.DIROWORD="+cp_ToStrODBC(this.w_DIROWORD)';
      +'+" and DIS_BOES.DIROWNUM="+cp_ToStrODBC(this.w_DIROWNUM)';

  cPrg = "gste_adb"
  cComment = "Informazioni aggiuntive bonifici esteri"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DINUMDIS = space(10)
  w_LFLDEFI = space(10)
  o_LFLDEFI = space(10)
  w_DIROWORD = 0
  w_DIROWNUM = 0
  w_CODCON = space(15)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_DITIPOPA = space(2)
  w_DICOMMIS = space(2)
  o_DICOMMIS = space(2)
  w_DICOSCOM = 0
  w_DIBANBEN = space(10)
  o_DIBANBEN = space(10)
  w_DICONCOR = space(25)
  w_CODIBAN = space(34)
  w_CODBIC = space(11)
  w_ORDINE = space(34)
  w_DIORDINE = space(34)
  w_DICONTAT = space(3)
  w_DIFUNUFF = space(17)
  w_DITIPAVV = space(3)
  w_DINOMINA = space(35)
  w_STRUTTUR = .F.
  o_STRUTTUR = .F.
  w_DITIPDO1 = space(3)
  w_DICODDO1 = space(17)
  w_DIDATDO1 = ctod('  /  /  ')
  w_DITIPDO2 = space(3)
  w_DICODDO2 = space(17)
  w_DIDATDO2 = ctod('  /  /  ')
  w_DITIPDO3 = space(3)
  w_DICODDO3 = space(17)
  w_DIDATDO3 = ctod('  /  /  ')
  w_DITIPDO4 = space(3)
  w_DICODDO4 = space(17)
  w_DIDATDO4 = ctod('  /  /  ')
  w_DIALTER1 = space(35)
  w_DIALTER2 = space(35)
  w_DIALTER3 = space(35)
  w_DESBAN = space(42)
  w_DICODCON = space(15)
  o_DICODCON = space(15)
  w_DESCONT = space(40)
  w_INDIRIZZO = space(35)
  w_CAP = space(9)
  w_LOCALITA = space(30)
  w_PROVINCIA = space(2)
  w_NUMPAR = space(31)
  w_DATSCA = ctod('  /  /  ')
  w_BANAPP = space(10)
  w_CODVAL = space(3)
  w_BANNOS = space(15)
  w_SIMVAL = space(5)
  w_TOTSCA = 0
  w_DITIPDES = space(3)
  w_DIDATESE = ctod('  /  /  ')
  w_TIPDIS = space(1)
  w_TOTDIS = 0
  w_NOCVS = space(1)

  * --- Children pointers
  GSTE_MCV = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_adbPag1","gste_adb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta")
      .Pages(1).HelpContextID = 260139415
      .Pages(2).addobject("oPag","tgste_adbPag2","gste_adb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).HelpContextID = 134122081
      .Pages(3).addobject("oPag","tgste_adbPag3","gste_adb",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("CVS per bonifici esteri")
      .Pages(3).HelpContextID = 13155633
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='BAN_CONTI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DIS_BOES'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_BOES_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_BOES_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MCV = CREATEOBJECT('stdDynamicChild',this,'GSTE_MCV',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSTE_MCV.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgste_adb'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSTE_MCV)
      this.GSTE_MCV.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSTE_MCV.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSTE_MCV.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MCV)
      this.GSTE_MCV.DestroyChildrenChain()
      this.GSTE_MCV=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MCV.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MCV.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MCV.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MCV.SetKey(;
            .w_DINUMDIS,"DINUMDIS";
            ,.w_DIROWNUM,"DIROWNUM";
            ,.w_DIROWORD,"DIROWORD";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MCV.ChangeRow(this.cRowID+'      1',1;
             ,.w_DINUMDIS,"DINUMDIS";
             ,.w_DIROWNUM,"DIROWNUM";
             ,.w_DIROWORD,"DIROWORD";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MCV)
        i_f=.GSTE_MCV.BuildFilter()
        if !(i_f==.GSTE_MCV.cQueryFilter)
          i_fnidx=.GSTE_MCV.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MCV.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MCV.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MCV.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MCV.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_BOES where DINUMDIS=KeySet.DINUMDIS
    *                            and DIROWORD=KeySet.DIROWORD
    *                            and DIROWNUM=KeySet.DIROWNUM
    *
    i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_BOES_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_BOES')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_BOES.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_BOES '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  ,'DIROWORD',this.w_DIROWORD  ,'DIROWNUM',this.w_DIROWNUM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODIBAN = space(34)
        .w_CODBIC = space(11)
        .w_STRUTTUR = .T.
        .w_DESBAN = space(42)
        .w_DESCONT = space(40)
        .w_INDIRIZZO = space(35)
        .w_CAP = space(9)
        .w_LOCALITA = space(30)
        .w_PROVINCIA = space(2)
        .w_SIMVAL = space(5)
        .w_DINUMDIS = NVL(DINUMDIS,space(10))
        .w_LFLDEFI = this.oparentobject .w_FLDEFI
        .w_DIROWORD = NVL(DIROWORD,0)
        .w_DIROWNUM = NVL(DIROWNUM,0)
        .w_CODCON = this.oparentobject .w_PTCODCON
        .w_TIPCON = this.oparentobject .w_PTTIPCON
        .w_DITIPOPA = NVL(DITIPOPA,space(2))
        .w_DICOMMIS = NVL(DICOMMIS,space(2))
        .w_DICOSCOM = NVL(DICOSCOM,0)
        .w_DIBANBEN = NVL(DIBANBEN,space(10))
          if link_2_4_joined
            this.w_DIBANBEN = NVL(BACODBAN204,NVL(this.w_DIBANBEN,space(10)))
            this.w_DESBAN = NVL(BADESBAN204,space(42))
          else
          .link_2_4('Load')
          endif
        .w_DICONCOR = NVL(DICONCOR,space(25))
          .link_2_5('Load')
        .w_ORDINE = RIGHT(REPLICATE('0',34)+ALLTRIM(STR(NVL(this.oparentobject .w_PTNUMEFF,0))),34)
        .w_DIORDINE = NVL(DIORDINE,space(34))
        .w_DICONTAT = NVL(DICONTAT,space(3))
        .w_DIFUNUFF = NVL(DIFUNUFF,space(17))
        .w_DITIPAVV = NVL(DITIPAVV,space(3))
        .w_DINOMINA = NVL(DINOMINA,space(35))
        .w_DITIPDO1 = NVL(DITIPDO1,space(3))
        .w_DICODDO1 = NVL(DICODDO1,space(17))
        .w_DIDATDO1 = NVL(cp_ToDate(DIDATDO1),ctod("  /  /  "))
        .w_DITIPDO2 = NVL(DITIPDO2,space(3))
        .w_DICODDO2 = NVL(DICODDO2,space(17))
        .w_DIDATDO2 = NVL(cp_ToDate(DIDATDO2),ctod("  /  /  "))
        .w_DITIPDO3 = NVL(DITIPDO3,space(3))
        .w_DICODDO3 = NVL(DICODDO3,space(17))
        .w_DIDATDO3 = NVL(cp_ToDate(DIDATDO3),ctod("  /  /  "))
        .w_DITIPDO4 = NVL(DITIPDO4,space(3))
        .w_DICODDO4 = NVL(DICODDO4,space(17))
        .w_DIDATDO4 = NVL(cp_ToDate(DIDATDO4),ctod("  /  /  "))
        .w_DIALTER1 = NVL(DIALTER1,space(35))
        .w_DIALTER2 = NVL(DIALTER2,space(35))
        .w_DIALTER3 = NVL(DIALTER3,space(35))
        .w_DICODCON = .w_CODCON
          .link_1_7('Load')
        .w_NUMPAR = this.oparentobject .w_PTNUMPAR
        .w_DATSCA = this.oparentobject .w_PTDATSCA
        .w_BANAPP = this.oparentobject .w_PTBANAPP
        .w_CODVAL = this.oparentobject .w_PTCODVAL
          .link_1_22('Load')
        .w_BANNOS = this.oparentobject .w_PTBANNOS
        .w_TOTSCA = this.oparentobject .w_TOTDIS
        .w_DITIPDES = NVL(DITIPDES,space(3))
        .w_DIDATESE = NVL(cp_ToDate(DIDATESE),ctod("  /  /  "))
        .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
        .w_TOTDIS = this.oparentobject .w_PTTOTIMP
        .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
        cp_LoadRecExtFlds(this,'DIS_BOES')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DINUMDIS = space(10)
      .w_LFLDEFI = space(10)
      .w_DIROWORD = 0
      .w_DIROWNUM = 0
      .w_CODCON = space(15)
      .w_TIPCON = space(1)
      .w_DITIPOPA = space(2)
      .w_DICOMMIS = space(2)
      .w_DICOSCOM = 0
      .w_DIBANBEN = space(10)
      .w_DICONCOR = space(25)
      .w_CODIBAN = space(34)
      .w_CODBIC = space(11)
      .w_ORDINE = space(34)
      .w_DIORDINE = space(34)
      .w_DICONTAT = space(3)
      .w_DIFUNUFF = space(17)
      .w_DITIPAVV = space(3)
      .w_DINOMINA = space(35)
      .w_STRUTTUR = .f.
      .w_DITIPDO1 = space(3)
      .w_DICODDO1 = space(17)
      .w_DIDATDO1 = ctod("  /  /  ")
      .w_DITIPDO2 = space(3)
      .w_DICODDO2 = space(17)
      .w_DIDATDO2 = ctod("  /  /  ")
      .w_DITIPDO3 = space(3)
      .w_DICODDO3 = space(17)
      .w_DIDATDO3 = ctod("  /  /  ")
      .w_DITIPDO4 = space(3)
      .w_DICODDO4 = space(17)
      .w_DIDATDO4 = ctod("  /  /  ")
      .w_DIALTER1 = space(35)
      .w_DIALTER2 = space(35)
      .w_DIALTER3 = space(35)
      .w_DESBAN = space(42)
      .w_DICODCON = space(15)
      .w_DESCONT = space(40)
      .w_INDIRIZZO = space(35)
      .w_CAP = space(9)
      .w_LOCALITA = space(30)
      .w_PROVINCIA = space(2)
      .w_NUMPAR = space(31)
      .w_DATSCA = ctod("  /  /  ")
      .w_BANAPP = space(10)
      .w_CODVAL = space(3)
      .w_BANNOS = space(15)
      .w_SIMVAL = space(5)
      .w_TOTSCA = 0
      .w_DITIPDES = space(3)
      .w_DIDATESE = ctod("  /  /  ")
      .w_TIPDIS = space(1)
      .w_TOTDIS = 0
      .w_NOCVS = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_LFLDEFI = this.oparentobject .w_FLDEFI
          .DoRTCalc(3,4,.f.)
        .w_CODCON = this.oparentobject .w_PTCODCON
        .w_TIPCON = this.oparentobject .w_PTTIPCON
        .w_DITIPOPA = 'BB'
        .w_DICOMMIS = '15'
        .w_DICOSCOM = IIF(.w_DICOMMIS<>'13',.w_DICOSCOM,0)
        .w_DIBANBEN = .w_DIBANBEN
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_DIBANBEN))
          .link_2_4('Full')
          endif
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_DICONCOR))
          .link_2_5('Full')
          endif
          .DoRTCalc(12,13,.f.)
        .w_ORDINE = RIGHT(REPLICATE('0',34)+ALLTRIM(STR(NVL(this.oparentobject .w_PTNUMEFF,0))),34)
          .DoRTCalc(15,15,.f.)
        .w_DICONTAT = 'BC'
        .w_DIFUNUFF = SPACE(17)
        .w_DITIPAVV = 'AP'
        .w_DINOMINA = SPACE(35)
        .w_STRUTTUR = .T.
        .w_DITIPDO1 = SPACE(3)
        .w_DICODDO1 = SPACE(17)
        .w_DIDATDO1 = cp_CharToDate(' - - ')
        .w_DITIPDO2 = SPACE(3)
        .w_DICODDO2 = SPACE(17)
        .w_DIDATDO2 = cp_CharToDate(' - - ')
        .w_DITIPDO3 = SPACE(3)
        .w_DICODDO3 = SPACE(17)
        .w_DIDATDO3 = cp_CharToDate(' - - ')
        .w_DITIPDO4 = SPACE(3)
        .w_DICODDO4 = SPACE(17)
        .w_DIDATDO4 = cp_CharToDate(' - - ')
        .w_DIALTER1 = SPACE(35)
        .w_DIALTER2 = SPACE(35)
        .w_DIALTER3 = SPACE(35)
          .DoRTCalc(36,36,.f.)
        .w_DICODCON = .w_CODCON
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_DICODCON))
          .link_1_7('Full')
          endif
          .DoRTCalc(38,42,.f.)
        .w_NUMPAR = this.oparentobject .w_PTNUMPAR
        .w_DATSCA = this.oparentobject .w_PTDATSCA
        .w_BANAPP = this.oparentobject .w_PTBANAPP
        .w_CODVAL = this.oparentobject .w_PTCODVAL
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_CODVAL))
          .link_1_22('Full')
          endif
        .w_BANNOS = this.oparentobject .w_PTBANNOS
          .DoRTCalc(48,48,.f.)
        .w_TOTSCA = this.oparentobject .w_TOTDIS
        .w_DITIPDES = '203'
        .w_DIDATESE = this.oparentobject .w_DATDIS
        .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
        .w_TOTDIS = this.oparentobject .w_PTTOTIMP
        .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_BOES')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTIPCON_1_6.enabled = i_bVal
      .Page2.oPag.oDITIPOPA_2_1.enabled = i_bVal
      .Page2.oPag.oDICOMMIS_2_2.enabled = i_bVal
      .Page2.oPag.oDICOSCOM_2_3.enabled = i_bVal
      .Page2.oPag.oDIBANBEN_2_4.enabled = i_bVal
      .Page2.oPag.oDICONCOR_2_5.enabled = i_bVal
      .Page2.oPag.oDICONTAT_2_10.enabled = i_bVal
      .Page2.oPag.oDIFUNUFF_2_11.enabled = i_bVal
      .Page2.oPag.oDITIPAVV_2_12.enabled = i_bVal
      .Page2.oPag.oDINOMINA_2_13.enabled = i_bVal
      .Page2.oPag.oSTRUTTUR_2_14.enabled = i_bVal
      .Page2.oPag.oDITIPDO1_2_15.enabled = i_bVal
      .Page2.oPag.oDICODDO1_2_16.enabled = i_bVal
      .Page2.oPag.oDIDATDO1_2_17.enabled = i_bVal
      .Page2.oPag.oDITIPDO2_2_18.enabled = i_bVal
      .Page2.oPag.oDICODDO2_2_19.enabled = i_bVal
      .Page2.oPag.oDIDATDO2_2_20.enabled = i_bVal
      .Page2.oPag.oDITIPDO3_2_21.enabled = i_bVal
      .Page2.oPag.oDICODDO3_2_22.enabled = i_bVal
      .Page2.oPag.oDIDATDO3_2_23.enabled = i_bVal
      .Page2.oPag.oDITIPDO4_2_24.enabled = i_bVal
      .Page2.oPag.oDICODDO4_2_25.enabled = i_bVal
      .Page2.oPag.oDIDATDO4_2_26.enabled = i_bVal
      .Page2.oPag.oDIALTER1_2_27.enabled = i_bVal
      .Page2.oPag.oDIALTER2_2_28.enabled = i_bVal
      .Page2.oPag.oDIALTER3_2_29.enabled = i_bVal
      .Page1.oPag.oDITIPDES_1_27.enabled = i_bVal
      .Page1.oPag.oDIDATESE_1_29.enabled = i_bVal
    endwith
    this.GSTE_MCV.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIS_BOES',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MCV.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMDIS,"DINUMDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIROWORD,"DIROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIROWNUM,"DIROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPOPA,"DITIPOPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOMMIS,"DICOMMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSCOM,"DICOSCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIBANBEN,"DIBANBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICONCOR,"DICONCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIORDINE,"DIORDINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICONTAT,"DICONTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFUNUFF,"DIFUNUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPAVV,"DITIPAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINOMINA,"DINOMINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDO1,"DITIPDO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODDO1,"DICODDO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDO1,"DIDATDO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDO2,"DITIPDO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODDO2,"DICODDO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDO2,"DIDATDO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDO3,"DITIPDO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODDO3,"DICODDO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDO3,"DIDATDO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDO4,"DITIPDO4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODDO4,"DICODDO4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDO4,"DIDATDO4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIALTER1,"DIALTER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIALTER2,"DIALTER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIALTER3,"DIALTER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDES,"DITIPDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATESE,"DIDATESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BOES_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_BOES_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_BOES
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_BOES')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_BOES')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DINUMDIS,DIROWORD,DIROWNUM,DITIPOPA,DICOMMIS"+;
                  ",DICOSCOM,DIBANBEN,DICONCOR,DIORDINE,DICONTAT"+;
                  ",DIFUNUFF,DITIPAVV,DINOMINA,DITIPDO1,DICODDO1"+;
                  ",DIDATDO1,DITIPDO2,DICODDO2,DIDATDO2,DITIPDO3"+;
                  ",DICODDO3,DIDATDO3,DITIPDO4,DICODDO4,DIDATDO4"+;
                  ",DIALTER1,DIALTER2,DIALTER3,DITIPDES,DIDATESE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DINUMDIS)+;
                  ","+cp_ToStrODBC(this.w_DIROWORD)+;
                  ","+cp_ToStrODBC(this.w_DIROWNUM)+;
                  ","+cp_ToStrODBC(this.w_DITIPOPA)+;
                  ","+cp_ToStrODBC(this.w_DICOMMIS)+;
                  ","+cp_ToStrODBC(this.w_DICOSCOM)+;
                  ","+cp_ToStrODBCNull(this.w_DIBANBEN)+;
                  ","+cp_ToStrODBCNull(this.w_DICONCOR)+;
                  ","+cp_ToStrODBC(this.w_DIORDINE)+;
                  ","+cp_ToStrODBC(this.w_DICONTAT)+;
                  ","+cp_ToStrODBC(this.w_DIFUNUFF)+;
                  ","+cp_ToStrODBC(this.w_DITIPAVV)+;
                  ","+cp_ToStrODBC(this.w_DINOMINA)+;
                  ","+cp_ToStrODBC(this.w_DITIPDO1)+;
                  ","+cp_ToStrODBC(this.w_DICODDO1)+;
                  ","+cp_ToStrODBC(this.w_DIDATDO1)+;
                  ","+cp_ToStrODBC(this.w_DITIPDO2)+;
                  ","+cp_ToStrODBC(this.w_DICODDO2)+;
                  ","+cp_ToStrODBC(this.w_DIDATDO2)+;
                  ","+cp_ToStrODBC(this.w_DITIPDO3)+;
                  ","+cp_ToStrODBC(this.w_DICODDO3)+;
                  ","+cp_ToStrODBC(this.w_DIDATDO3)+;
                  ","+cp_ToStrODBC(this.w_DITIPDO4)+;
                  ","+cp_ToStrODBC(this.w_DICODDO4)+;
                  ","+cp_ToStrODBC(this.w_DIDATDO4)+;
                  ","+cp_ToStrODBC(this.w_DIALTER1)+;
                  ","+cp_ToStrODBC(this.w_DIALTER2)+;
                  ","+cp_ToStrODBC(this.w_DIALTER3)+;
                  ","+cp_ToStrODBC(this.w_DITIPDES)+;
                  ","+cp_ToStrODBC(this.w_DIDATESE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_BOES')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_BOES')
        cp_CheckDeletedKey(i_cTable,0,'DINUMDIS',this.w_DINUMDIS,'DIROWORD',this.w_DIROWORD,'DIROWNUM',this.w_DIROWNUM)
        INSERT INTO (i_cTable);
              (DINUMDIS,DIROWORD,DIROWNUM,DITIPOPA,DICOMMIS,DICOSCOM,DIBANBEN,DICONCOR,DIORDINE,DICONTAT,DIFUNUFF,DITIPAVV,DINOMINA,DITIPDO1,DICODDO1,DIDATDO1,DITIPDO2,DICODDO2,DIDATDO2,DITIPDO3,DICODDO3,DIDATDO3,DITIPDO4,DICODDO4,DIDATDO4,DIALTER1,DIALTER2,DIALTER3,DITIPDES,DIDATESE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DINUMDIS;
                  ,this.w_DIROWORD;
                  ,this.w_DIROWNUM;
                  ,this.w_DITIPOPA;
                  ,this.w_DICOMMIS;
                  ,this.w_DICOSCOM;
                  ,this.w_DIBANBEN;
                  ,this.w_DICONCOR;
                  ,this.w_DIORDINE;
                  ,this.w_DICONTAT;
                  ,this.w_DIFUNUFF;
                  ,this.w_DITIPAVV;
                  ,this.w_DINOMINA;
                  ,this.w_DITIPDO1;
                  ,this.w_DICODDO1;
                  ,this.w_DIDATDO1;
                  ,this.w_DITIPDO2;
                  ,this.w_DICODDO2;
                  ,this.w_DIDATDO2;
                  ,this.w_DITIPDO3;
                  ,this.w_DICODDO3;
                  ,this.w_DIDATDO3;
                  ,this.w_DITIPDO4;
                  ,this.w_DICODDO4;
                  ,this.w_DIDATDO4;
                  ,this.w_DIALTER1;
                  ,this.w_DIALTER2;
                  ,this.w_DIALTER3;
                  ,this.w_DITIPDES;
                  ,this.w_DIDATESE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BOES_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_BOES_IDX,i_nConn)
      *
      * update DIS_BOES
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_BOES')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DITIPOPA="+cp_ToStrODBC(this.w_DITIPOPA)+;
             ",DICOMMIS="+cp_ToStrODBC(this.w_DICOMMIS)+;
             ",DICOSCOM="+cp_ToStrODBC(this.w_DICOSCOM)+;
             ",DIBANBEN="+cp_ToStrODBCNull(this.w_DIBANBEN)+;
             ",DICONCOR="+cp_ToStrODBCNull(this.w_DICONCOR)+;
             ",DIORDINE="+cp_ToStrODBC(this.w_DIORDINE)+;
             ",DICONTAT="+cp_ToStrODBC(this.w_DICONTAT)+;
             ",DIFUNUFF="+cp_ToStrODBC(this.w_DIFUNUFF)+;
             ",DITIPAVV="+cp_ToStrODBC(this.w_DITIPAVV)+;
             ",DINOMINA="+cp_ToStrODBC(this.w_DINOMINA)+;
             ",DITIPDO1="+cp_ToStrODBC(this.w_DITIPDO1)+;
             ",DICODDO1="+cp_ToStrODBC(this.w_DICODDO1)+;
             ",DIDATDO1="+cp_ToStrODBC(this.w_DIDATDO1)+;
             ",DITIPDO2="+cp_ToStrODBC(this.w_DITIPDO2)+;
             ",DICODDO2="+cp_ToStrODBC(this.w_DICODDO2)+;
             ",DIDATDO2="+cp_ToStrODBC(this.w_DIDATDO2)+;
             ",DITIPDO3="+cp_ToStrODBC(this.w_DITIPDO3)+;
             ",DICODDO3="+cp_ToStrODBC(this.w_DICODDO3)+;
             ",DIDATDO3="+cp_ToStrODBC(this.w_DIDATDO3)+;
             ",DITIPDO4="+cp_ToStrODBC(this.w_DITIPDO4)+;
             ",DICODDO4="+cp_ToStrODBC(this.w_DICODDO4)+;
             ",DIDATDO4="+cp_ToStrODBC(this.w_DIDATDO4)+;
             ",DIALTER1="+cp_ToStrODBC(this.w_DIALTER1)+;
             ",DIALTER2="+cp_ToStrODBC(this.w_DIALTER2)+;
             ",DIALTER3="+cp_ToStrODBC(this.w_DIALTER3)+;
             ",DITIPDES="+cp_ToStrODBC(this.w_DITIPDES)+;
             ",DIDATESE="+cp_ToStrODBC(this.w_DIDATESE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_BOES')
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  ,'DIROWORD',this.w_DIROWORD  ,'DIROWNUM',this.w_DIROWNUM  )
        UPDATE (i_cTable) SET;
              DITIPOPA=this.w_DITIPOPA;
             ,DICOMMIS=this.w_DICOMMIS;
             ,DICOSCOM=this.w_DICOSCOM;
             ,DIBANBEN=this.w_DIBANBEN;
             ,DICONCOR=this.w_DICONCOR;
             ,DIORDINE=this.w_DIORDINE;
             ,DICONTAT=this.w_DICONTAT;
             ,DIFUNUFF=this.w_DIFUNUFF;
             ,DITIPAVV=this.w_DITIPAVV;
             ,DINOMINA=this.w_DINOMINA;
             ,DITIPDO1=this.w_DITIPDO1;
             ,DICODDO1=this.w_DICODDO1;
             ,DIDATDO1=this.w_DIDATDO1;
             ,DITIPDO2=this.w_DITIPDO2;
             ,DICODDO2=this.w_DICODDO2;
             ,DIDATDO2=this.w_DIDATDO2;
             ,DITIPDO3=this.w_DITIPDO3;
             ,DICODDO3=this.w_DICODDO3;
             ,DIDATDO3=this.w_DIDATDO3;
             ,DITIPDO4=this.w_DITIPDO4;
             ,DICODDO4=this.w_DICODDO4;
             ,DIDATDO4=this.w_DIDATDO4;
             ,DIALTER1=this.w_DIALTER1;
             ,DIALTER2=this.w_DIALTER2;
             ,DIALTER3=this.w_DIALTER3;
             ,DITIPDES=this.w_DITIPDES;
             ,DIDATESE=this.w_DIDATESE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MCV : Saving
      this.GSTE_MCV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DINUMDIS,"DINUMDIS";
             ,this.w_DIROWNUM,"DIROWNUM";
             ,this.w_DIROWORD,"DIROWORD";
             )
      this.GSTE_MCV.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSTE_MCV : Deleting
    this.GSTE_MCV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DINUMDIS,"DINUMDIS";
           ,this.w_DIROWNUM,"DIROWNUM";
           ,this.w_DIROWORD,"DIROWORD";
           )
    this.GSTE_MCV.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BOES_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_BOES_IDX,i_nConn)
      *
      * delete DIS_BOES
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  ,'DIROWORD',this.w_DIROWORD  ,'DIROWNUM',this.w_DIROWNUM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_BOES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_BOES_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_LFLDEFI = this.oparentobject .w_FLDEFI
        .DoRTCalc(3,4,.t.)
            .w_CODCON = this.oparentobject .w_PTCODCON
        if .o_TIPCON<>.w_TIPCON
            .w_TIPCON = this.oparentobject .w_PTTIPCON
        endif
        .DoRTCalc(7,8,.t.)
        if .o_DICOMMIS<>.w_DICOMMIS
            .w_DICOSCOM = IIF(.w_DICOMMIS<>'13',.w_DICOSCOM,0)
        endif
        if .o_DICODCON<>.w_DICODCON
          .link_2_4('Full')
        endif
        if .o_DICODCON<>.w_DICODCON.or. .o_DIBANBEN<>.w_DIBANBEN
          .link_2_5('Full')
        endif
        .DoRTCalc(12,13,.t.)
            .w_ORDINE = RIGHT(REPLICATE('0',34)+ALLTRIM(STR(NVL(this.oparentobject .w_PTNUMEFF,0))),34)
        .DoRTCalc(15,20,.t.)
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DITIPDO1 = SPACE(3)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DICODDO1 = SPACE(17)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIDATDO1 = cp_CharToDate(' - - ')
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DITIPDO2 = SPACE(3)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DICODDO2 = SPACE(17)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIDATDO2 = cp_CharToDate(' - - ')
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DITIPDO3 = SPACE(3)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DICODDO3 = SPACE(17)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIDATDO3 = cp_CharToDate(' - - ')
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DITIPDO4 = SPACE(3)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DICODDO4 = SPACE(17)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIDATDO4 = cp_CharToDate(' - - ')
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIALTER1 = SPACE(35)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIALTER2 = SPACE(35)
        endif
        if .o_STRUTTUR<>.w_STRUTTUR
            .w_DIALTER3 = SPACE(35)
        endif
        .DoRTCalc(36,36,.t.)
            .w_DICODCON = .w_CODCON
          .link_1_7('Full')
        .DoRTCalc(38,42,.t.)
            .w_NUMPAR = this.oparentobject .w_PTNUMPAR
            .w_DATSCA = this.oparentobject .w_PTDATSCA
            .w_BANAPP = this.oparentobject .w_PTBANAPP
            .w_CODVAL = this.oparentobject .w_PTCODVAL
          .link_1_22('Full')
            .w_BANNOS = this.oparentobject .w_PTBANNOS
        .DoRTCalc(48,48,.t.)
            .w_TOTSCA = this.oparentobject .w_TOTDIS
        .DoRTCalc(50,50,.t.)
        if .o_LFLDEFI<>.w_LFLDEFI
            .w_DIDATESE = this.oparentobject .w_DATDIS
        endif
            .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
            .w_TOTDIS = this.oparentobject .w_PTTOTIMP
            .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oDITIPOPA_2_1.enabled = this.oPgFrm.Page2.oPag.oDITIPOPA_2_1.mCond()
    this.oPgFrm.Page2.oPag.oDICOMMIS_2_2.enabled = this.oPgFrm.Page2.oPag.oDICOMMIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDICOSCOM_2_3.enabled = this.oPgFrm.Page2.oPag.oDICOSCOM_2_3.mCond()
    this.oPgFrm.Page2.oPag.oDIBANBEN_2_4.enabled = this.oPgFrm.Page2.oPag.oDIBANBEN_2_4.mCond()
    this.oPgFrm.Page2.oPag.oDICONCOR_2_5.enabled = this.oPgFrm.Page2.oPag.oDICONCOR_2_5.mCond()
    this.oPgFrm.Page2.oPag.oDITIPDO1_2_15.enabled = this.oPgFrm.Page2.oPag.oDITIPDO1_2_15.mCond()
    this.oPgFrm.Page2.oPag.oDICODDO1_2_16.enabled = this.oPgFrm.Page2.oPag.oDICODDO1_2_16.mCond()
    this.oPgFrm.Page2.oPag.oDIDATDO1_2_17.enabled = this.oPgFrm.Page2.oPag.oDIDATDO1_2_17.mCond()
    this.oPgFrm.Page2.oPag.oDITIPDO2_2_18.enabled = this.oPgFrm.Page2.oPag.oDITIPDO2_2_18.mCond()
    this.oPgFrm.Page2.oPag.oDICODDO2_2_19.enabled = this.oPgFrm.Page2.oPag.oDICODDO2_2_19.mCond()
    this.oPgFrm.Page2.oPag.oDIDATDO2_2_20.enabled = this.oPgFrm.Page2.oPag.oDIDATDO2_2_20.mCond()
    this.oPgFrm.Page2.oPag.oDITIPDO3_2_21.enabled = this.oPgFrm.Page2.oPag.oDITIPDO3_2_21.mCond()
    this.oPgFrm.Page2.oPag.oDICODDO3_2_22.enabled = this.oPgFrm.Page2.oPag.oDICODDO3_2_22.mCond()
    this.oPgFrm.Page2.oPag.oDIDATDO3_2_23.enabled = this.oPgFrm.Page2.oPag.oDIDATDO3_2_23.mCond()
    this.oPgFrm.Page2.oPag.oDITIPDO4_2_24.enabled = this.oPgFrm.Page2.oPag.oDITIPDO4_2_24.mCond()
    this.oPgFrm.Page2.oPag.oDICODDO4_2_25.enabled = this.oPgFrm.Page2.oPag.oDICODDO4_2_25.mCond()
    this.oPgFrm.Page2.oPag.oDIDATDO4_2_26.enabled = this.oPgFrm.Page2.oPag.oDIDATDO4_2_26.mCond()
    this.oPgFrm.Page2.oPag.oDIALTER1_2_27.enabled = this.oPgFrm.Page2.oPag.oDIALTER1_2_27.mCond()
    this.oPgFrm.Page2.oPag.oDIALTER2_2_28.enabled = this.oPgFrm.Page2.oPag.oDIALTER2_2_28.mCond()
    this.oPgFrm.Page2.oPag.oDIALTER3_2_29.enabled = this.oPgFrm.Page2.oPag.oDIALTER3_2_29.mCond()
    this.oPgFrm.Page1.oPag.oDITIPDES_1_27.enabled = this.oPgFrm.Page1.oPag.oDITIPDES_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDIDATESE_1_29.enabled = this.oPgFrm.Page1.oPag.oDIDATESE_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DIBANBEN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIBANBEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_DIBANBEN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_DIBANBEN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIBANBEN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIBANBEN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oDIBANBEN_2_4'),i_cWhere,'GSAR_ABA',"Banche",'gsar_aba.BAN_CHE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIBANBEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_DIBANBEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_DIBANBEN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIBANBEN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(42))
    else
      if i_cCtrl<>'Load'
        this.w_DIBANBEN = space(10)
      endif
      this.w_DESBAN = space(42)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIBANBEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.BACODBAN as BACODBAN204"+ ",link_2_4.BADESBAN as BADESBAN204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DIS_BOES.DIBANBEN=link_2_4.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DIS_BOES.DIBANBEN=link_2_4.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICONCOR
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICONCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_DICONCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_DICODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_DIBANBEN);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_TIPCON;
                     ,'CCCODCON',this.w_DICODCON;
                     ,'CCCODBAN',this.w_DIBANBEN;
                     ,'CCCONCOR',trim(this.w_DICONCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICONCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICONCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oDICONCOR_2_5'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_DICODCON<>oSource.xKey(2);
           .or. this.w_DIBANBEN<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_DICODCON);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_DIBANBEN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICONCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_DICONCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_DICODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_DIBANBEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_TIPCON;
                       ,'CCCODCON',this.w_DICODCON;
                       ,'CCCODBAN',this.w_DIBANBEN;
                       ,'CCCONCOR',this.w_DICONCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCCOIBAN,CCCODBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICONCOR = NVL(_Link_.CCCONCOR,space(25))
      this.w_CODIBAN = NVL(_Link_.CCCOIBAN,space(34))
      this.w_CODBIC = NVL(_Link_.CCCODBIC,space(11))
    else
      if i_cCtrl<>'Load'
        this.w_DICONCOR = space(25)
      endif
      this.w_CODIBAN = space(34)
      this.w_CODBIC = space(11)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICONCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DICODCON)
            select ANTIPCON,ANCODICE,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_INDIRIZZO = NVL(_Link_.ANINDIRI,space(35))
      this.w_CAP = NVL(_Link_.AN___CAP,space(9))
      this.w_LOCALITA = NVL(_Link_.ANLOCALI,space(30))
      this.w_PROVINCIA = NVL(_Link_.ANPROVIN,space(2))
      this.w_DESCONT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DICODCON = space(15)
      endif
      this.w_INDIRIZZO = space(35)
      this.w_CAP = space(9)
      this.w_LOCALITA = space(30)
      this.w_PROVINCIA = space(2)
      this.w_DESCONT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_6.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPOPA_2_1.RadioValue()==this.w_DITIPOPA)
      this.oPgFrm.Page2.oPag.oDITIPOPA_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICOMMIS_2_2.RadioValue()==this.w_DICOMMIS)
      this.oPgFrm.Page2.oPag.oDICOMMIS_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICOSCOM_2_3.value==this.w_DICOSCOM)
      this.oPgFrm.Page2.oPag.oDICOSCOM_2_3.value=this.w_DICOSCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDIBANBEN_2_4.value==this.w_DIBANBEN)
      this.oPgFrm.Page2.oPag.oDIBANBEN_2_4.value=this.w_DIBANBEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDICONCOR_2_5.value==this.w_DICONCOR)
      this.oPgFrm.Page2.oPag.oDICONCOR_2_5.value=this.w_DICONCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oCODIBAN_2_6.value==this.w_CODIBAN)
      this.oPgFrm.Page2.oPag.oCODIBAN_2_6.value=this.w_CODIBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODBIC_2_7.value==this.w_CODBIC)
      this.oPgFrm.Page2.oPag.oCODBIC_2_7.value=this.w_CODBIC
    endif
    if not(this.oPgFrm.Page2.oPag.oORDINE_2_8.value==this.w_ORDINE)
      this.oPgFrm.Page2.oPag.oORDINE_2_8.value=this.w_ORDINE
    endif
    if not(this.oPgFrm.Page2.oPag.oDIORDINE_2_9.value==this.w_DIORDINE)
      this.oPgFrm.Page2.oPag.oDIORDINE_2_9.value=this.w_DIORDINE
    endif
    if not(this.oPgFrm.Page2.oPag.oDICONTAT_2_10.RadioValue()==this.w_DICONTAT)
      this.oPgFrm.Page2.oPag.oDICONTAT_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDIFUNUFF_2_11.value==this.w_DIFUNUFF)
      this.oPgFrm.Page2.oPag.oDIFUNUFF_2_11.value=this.w_DIFUNUFF
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPAVV_2_12.RadioValue()==this.w_DITIPAVV)
      this.oPgFrm.Page2.oPag.oDITIPAVV_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDINOMINA_2_13.value==this.w_DINOMINA)
      this.oPgFrm.Page2.oPag.oDINOMINA_2_13.value=this.w_DINOMINA
    endif
    if not(this.oPgFrm.Page2.oPag.oSTRUTTUR_2_14.RadioValue()==this.w_STRUTTUR)
      this.oPgFrm.Page2.oPag.oSTRUTTUR_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPDO1_2_15.RadioValue()==this.w_DITIPDO1)
      this.oPgFrm.Page2.oPag.oDITIPDO1_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICODDO1_2_16.value==this.w_DICODDO1)
      this.oPgFrm.Page2.oPag.oDICODDO1_2_16.value=this.w_DICODDO1
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDATDO1_2_17.value==this.w_DIDATDO1)
      this.oPgFrm.Page2.oPag.oDIDATDO1_2_17.value=this.w_DIDATDO1
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPDO2_2_18.RadioValue()==this.w_DITIPDO2)
      this.oPgFrm.Page2.oPag.oDITIPDO2_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICODDO2_2_19.value==this.w_DICODDO2)
      this.oPgFrm.Page2.oPag.oDICODDO2_2_19.value=this.w_DICODDO2
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDATDO2_2_20.value==this.w_DIDATDO2)
      this.oPgFrm.Page2.oPag.oDIDATDO2_2_20.value=this.w_DIDATDO2
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPDO3_2_21.RadioValue()==this.w_DITIPDO3)
      this.oPgFrm.Page2.oPag.oDITIPDO3_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICODDO3_2_22.value==this.w_DICODDO3)
      this.oPgFrm.Page2.oPag.oDICODDO3_2_22.value=this.w_DICODDO3
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDATDO3_2_23.value==this.w_DIDATDO3)
      this.oPgFrm.Page2.oPag.oDIDATDO3_2_23.value=this.w_DIDATDO3
    endif
    if not(this.oPgFrm.Page2.oPag.oDITIPDO4_2_24.RadioValue()==this.w_DITIPDO4)
      this.oPgFrm.Page2.oPag.oDITIPDO4_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDICODDO4_2_25.value==this.w_DICODDO4)
      this.oPgFrm.Page2.oPag.oDICODDO4_2_25.value=this.w_DICODDO4
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDATDO4_2_26.value==this.w_DIDATDO4)
      this.oPgFrm.Page2.oPag.oDIDATDO4_2_26.value=this.w_DIDATDO4
    endif
    if not(this.oPgFrm.Page2.oPag.oDIALTER1_2_27.value==this.w_DIALTER1)
      this.oPgFrm.Page2.oPag.oDIALTER1_2_27.value=this.w_DIALTER1
    endif
    if not(this.oPgFrm.Page2.oPag.oDIALTER2_2_28.value==this.w_DIALTER2)
      this.oPgFrm.Page2.oPag.oDIALTER2_2_28.value=this.w_DIALTER2
    endif
    if not(this.oPgFrm.Page2.oPag.oDIALTER3_2_29.value==this.w_DIALTER3)
      this.oPgFrm.Page2.oPag.oDIALTER3_2_29.value=this.w_DIALTER3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_45.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_45.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODCON_1_7.value==this.w_DICODCON)
      this.oPgFrm.Page1.oPag.oDICODCON_1_7.value=this.w_DICODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONT_1_8.value==this.w_DESCONT)
      this.oPgFrm.Page1.oPag.oDESCONT_1_8.value=this.w_DESCONT
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZZO_1_9.value==this.w_INDIRIZZO)
      this.oPgFrm.Page1.oPag.oINDIRIZZO_1_9.value=this.w_INDIRIZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_10.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_10.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCALITA_1_11.value==this.w_LOCALITA)
      this.oPgFrm.Page1.oPag.oLOCALITA_1_11.value=this.w_LOCALITA
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVINCIA_1_12.value==this.w_PROVINCIA)
      this.oPgFrm.Page1.oPag.oPROVINCIA_1_12.value=this.w_PROVINCIA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPAR_1_19.value==this.w_NUMPAR)
      this.oPgFrm.Page1.oPag.oNUMPAR_1_19.value=this.w_NUMPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_20.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_20.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oBANAPP_1_21.value==this.w_BANAPP)
      this.oPgFrm.Page1.oPag.oBANAPP_1_21.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oBANNOS_1_23.value==this.w_BANNOS)
      this.oPgFrm.Page1.oPag.oBANNOS_1_23.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_24.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_24.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_1_25.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_1_25.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPDES_1_27.RadioValue()==this.w_DITIPDES)
      this.oPgFrm.Page1.oPag.oDITIPDES_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATESE_1_29.value==this.w_DIDATESE)
      this.oPgFrm.Page1.oPag.oDIDATESE_1_29.value=this.w_DIDATESE
    endif
    cp_SetControlsValueExtFlds(this,'DIS_BOES')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(not empty(.w_DIDATESE))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la data esecuzione/valuta")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MCV.CheckForm()
      if i_bres
        i_bres=  .GSTE_MCV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LFLDEFI = this.w_LFLDEFI
    this.o_TIPCON = this.w_TIPCON
    this.o_DICOMMIS = this.w_DICOMMIS
    this.o_DIBANBEN = this.w_DIBANBEN
    this.o_STRUTTUR = this.w_STRUTTUR
    this.o_DICODCON = this.w_DICODCON
    * --- GSTE_MCV : Depends On
    this.GSTE_MCV.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgste_adbPag1 as StdContainer
  Width  = 732
  height = 348
  stdWidth  = 732
  stdheight = 348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_6 as StdCombo with uid="QXASIRATLD",rtseq=6,rtrep=.f.,left=93,top=13,width=86,height=21;
    , ToolTipText = "Beneficiario";
    , HelpContextID = 38921674;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DICONCOR)
        bRes2=.link_2_5('Full')
      endif
      if .not. empty(.w_DICODCON)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDICODCON_1_7 as StdField with uid="OTRCBLTEHO",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DICODCON", cQueryName = "DICODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 234272380,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=231, Top=13, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODCON"

  func oDICODCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DICONCOR)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCONT_1_8 as StdField with uid="INIOYXXYWB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCONT", cQueryName = "DESCONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 229524790,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=351, Top=13, InputMask=replicate('X',40)

  add object oINDIRIZZO_1_9 as StdField with uid="DEAJHLFTMW",rtseq=39,rtrep=.f.,;
    cFormVar = "w_INDIRIZZO", cQueryName = "INDIRIZZO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del beneficiario",;
    HelpContextID = 149119952,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=351, Top=39, InputMask=replicate('X',35)

  add object oCAP_1_10 as StdField with uid="EETWUNRQKG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. residenza del beneficiario",;
    HelpContextID = 92597978,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=351, Top=65, InputMask=replicate('X',9)

  add object oLOCALITA_1_11 as StdField with uid="IKYYVFGJRG",rtseq=41,rtrep=.f.,;
    cFormVar = "w_LOCALITA", cQueryName = "LOCALITA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "LocalitÓ residenza del beneficiario",;
    HelpContextID = 142299127,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=427, Top=65, InputMask=replicate('X',30)

  add object oPROVINCIA_1_12 as StdField with uid="IQBHNUNLKD",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PROVINCIA", cQueryName = "PROVINCIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia residenza del beneficiario",;
    HelpContextID = 224466767,;
   bGlobalFont=.t.,;
    Height=21, Width=31, Left=650, Top=65, InputMask=replicate('X',2)

  add object oNUMPAR_1_19 as StdField with uid="PTKCCFAEVF",rtseq=43,rtrep=.f.,;
    cFormVar = "w_NUMPAR", cQueryName = "NUMPAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero partita",;
    HelpContextID = 254085674,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=122, Top=107, InputMask=replicate('X',31)

  add object oDATSCA_1_20 as StdField with uid="ZVMQHNIGYZ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza",;
    HelpContextID = 110282,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=563, Top=107

  add object oBANAPP_1_21 as StdField with uid="HRQJJBFUPG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca d'appoggio",;
    HelpContextID = 4460266,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=122, Top=131, InputMask=replicate('X',10)

  add object oBANNOS_1_23 as StdField with uid="HEUZHOEFSB",rtseq=47,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nostra banca",;
    HelpContextID = 222760682,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=563, Top=131, InputMask=replicate('X',15)

  add object oSIMVAL_1_24 as StdField with uid="LCBWVZNYSK",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo valuta",;
    HelpContextID = 85923290,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=122, Top=153, InputMask=replicate('X',5)

  add object oTOTSCA_1_25 as StdField with uid="AVVEVOHRFA",rtseq=49,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo distinta",;
    HelpContextID = 106442,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=179, Top=153, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oDITIPDES_1_27 as StdCombo with uid="YEJVVIKCHV",rtseq=50,rtrep=.f.,left=14,top=178,width=358,height=21;
    , ToolTipText = "Tipo data esecuzione/data valuta beneficiario";
    , HelpContextID = 63199625;
    , cFormVar="w_DITIPDES",RowSource=""+"Data esecuzione ordine,"+"Valuta di addebito,"+"Valuta di accredito del beneficiario,"+"Data disponibilitÓ banca beneficiario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPDES_1_27.RadioValue()
    return(iif(this.value =1,'203',;
    iif(this.value =2,'228',;
    iif(this.value =3,'140',;
    iif(this.value =4,'227',;
    space(3))))))
  endfunc
  func oDITIPDES_1_27.GetRadio()
    this.Parent.oContained.w_DITIPDES = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDES_1_27.SetRadio()
    this.Parent.oContained.w_DITIPDES=trim(this.Parent.oContained.w_DITIPDES)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDES=='203',1,;
      iif(this.Parent.oContained.w_DITIPDES=='228',2,;
      iif(this.Parent.oContained.w_DITIPDES=='140',3,;
      iif(this.Parent.oContained.w_DITIPDES=='227',4,;
      0))))
  endfunc

  func oDITIPDES_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N')
    endwith
   endif
  endfunc

  add object oDIDATESE_1_29 as StdField with uid="GJMSIDJIJF",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DIDATESE", cQueryName = "DIDATESE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data esecuzione/valuta dell'operazione",;
    HelpContextID = 83581307,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=563, Top=180

  func oDIDATESE_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N')
    endwith
   endif
  endfunc

  add object oStr_1_13 as StdString with uid="FCNKXWTYHX",Visible=.t., Left=17, Top=13,;
    Alignment=1, Width=74, Height=18,;
    Caption="Beneficiario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YJBYRTTRCB",Visible=.t., Left=184, Top=13,;
    Alignment=1, Width=45, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ZBWDJMLPKH",Visible=.t., Left=290, Top=39,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ORMCXMOJCH",Visible=.t., Left=197, Top=65,;
    Alignment=1, Width=151, Height=18,;
    Caption="CAP - localitÓ - provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EPPETDBDZU",Visible=.t., Left=9, Top=86,;
    Alignment=0, Width=89, Height=18,;
    Caption="Dati scadenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NSUPWTWAID",Visible=.t., Left=387, Top=180,;
    Alignment=1, Width=174, Height=18,;
    Caption="Data esecuzione/valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XVDOEPANEA",Visible=.t., Left=8, Top=153,;
    Alignment=1, Width=113, Height=18,;
    Caption="Importo distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QVCCDLJBWN",Visible=.t., Left=8, Top=107,;
    Alignment=1, Width=113, Height=18,;
    Caption="Numero partita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TPNGXLFPEK",Visible=.t., Left=465, Top=107,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="FGKTMABAHG",Visible=.t., Left=8, Top=131,;
    Alignment=1, Width=113, Height=18,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NFXBBYOYDL",Visible=.t., Left=475, Top=131,;
    Alignment=1, Width=86, Height=18,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oBox_1_33 as StdBox with uid="NPGBOKJSDL",left=4, top=101, width=688,height=104
enddefine
define class tgste_adbPag2 as StdContainer
  Width  = 732
  height = 348
  stdWidth  = 732
  stdheight = 348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDITIPOPA_2_1 as StdCombo with uid="WWUOVDHWAO",rtseq=7,rtrep=.f.,left=133,top=10,width=217,height=21;
    , ToolTipText = "ModalitÓ di pagamento";
    , HelpContextID = 20686473;
    , cFormVar="w_DITIPOPA",RowSource=""+"Accredito in C/C,"+"Ass. da banca ordinante,"+"Ass. da banca creditore,"+"Trattenere c/o banca creditore", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPOPA_2_1.RadioValue()
    return(iif(this.value =1,'BB',;
    iif(this.value =2,'23',;
    iif(this.value =3,'26',;
    iif(this.value =4,'8 ',;
    space(2))))))
  endfunc
  func oDITIPOPA_2_1.GetRadio()
    this.Parent.oContained.w_DITIPOPA = this.RadioValue()
    return .t.
  endfunc

  func oDITIPOPA_2_1.SetRadio()
    this.Parent.oContained.w_DITIPOPA=trim(this.Parent.oContained.w_DITIPOPA)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPOPA=='BB',1,;
      iif(this.Parent.oContained.w_DITIPOPA=='23',2,;
      iif(this.Parent.oContained.w_DITIPOPA=='26',3,;
      iif(this.Parent.oContained.w_DITIPOPA=='8',4,;
      0))))
  endfunc

  func oDITIPOPA_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N')
    endwith
   endif
  endfunc


  add object oDICOMMIS_2_2 as StdCombo with uid="CQBXPTDLOT",rtseq=8,rtrep=.f.,left=461,top=9,width=136,height=21;
    , ToolTipText = "Commissioni";
    , HelpContextID = 211372425;
    , cFormVar="w_DICOMMIS",RowSource=""+"A carico ordinante,"+"A carico beneficiario,"+"Ripartite", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDICOMMIS_2_2.RadioValue()
    return(iif(this.value =1,'15',;
    iif(this.value =2,'13',;
    iif(this.value =3,'14',;
    space(2)))))
  endfunc
  func oDICOMMIS_2_2.GetRadio()
    this.Parent.oContained.w_DICOMMIS = this.RadioValue()
    return .t.
  endfunc

  func oDICOMMIS_2_2.SetRadio()
    this.Parent.oContained.w_DICOMMIS=trim(this.Parent.oContained.w_DICOMMIS)
    this.value = ;
      iif(this.Parent.oContained.w_DICOMMIS=='15',1,;
      iif(this.Parent.oContained.w_DICOMMIS=='13',2,;
      iif(this.Parent.oContained.w_DICOMMIS=='14',3,;
      0)))
  endfunc

  func oDICOMMIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N')
    endwith
   endif
  endfunc

  add object oDICOSCOM_2_3 as StdField with uid="WWFMQTKTOC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DICOSCOM", cQueryName = "DICOSCOM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale spesa commissioni",;
    HelpContextID = 218543741,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=597, Top=9, cSayPict="v_PV(32+VVL)", cGetPict="v_GV(32+VVL)"

  func oDICOSCOM_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DICOMMIS<>'13' and .w_LFLDEFI='N')
    endwith
   endif
  endfunc

  add object oDIBANBEN_2_4 as StdField with uid="VZAUBSBOFJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DIBANBEN", cQueryName = "DIBANBEN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca beneficiario",;
    HelpContextID = 26950020,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=133, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_DIBANBEN"

  func oDIBANBEN_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N' AND .w_DICOMMIS<>'13')
    endwith
   endif
  endfunc

  func oDIBANBEN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_DICONCOR)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDIBANBEN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIBANBEN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oDIBANBEN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Banche",'gsar_aba.BAN_CHE_VZM',this.parent.oContained
  endproc
  proc oDIBANBEN_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_DIBANBEN
     i_obj.ecpSave()
  endproc

  add object oDICONCOR_2_5 as StdField with uid="DDAPISWINK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DICONCOR", cQueryName = "DICONCOR",;
    bObbl = .f. , nPag = 2, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Numero conto corrente",;
    HelpContextID = 223786616,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=133, Top=62, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_DICODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_DIBANBEN", oKey_4_1="CCCONCOR", oKey_4_2="this.w_DICONCOR"

  func oDICONCOR_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LFLDEFI='N')
    endwith
   endif
  endfunc

  func oDICONCOR_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICONCOR_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICONCOR_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_DICODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_DIBANBEN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_DICODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_DIBANBEN)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oDICONCOR_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oCODIBAN_2_6 as StdField with uid="JGGVRLYFKL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODIBAN", cQueryName = "CODIBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(34), bMultilanguage =  .f.,;
    ToolTipText = "Codice IBAN",;
    HelpContextID = 1876186,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=133, Top=88, InputMask=replicate('X',34)

  add object oCODBIC_2_7 as StdField with uid="TBXZVYDBQX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODBIC", cQueryName = "CODBIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice BIC",;
    HelpContextID = 229875930,;
   bGlobalFont=.t.,;
    Height=21, Width=108, Left=461, Top=88, InputMask=replicate('X',11)

  add object oORDINE_2_8 as StdField with uid="CNTUSECZTM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ORDINE", cQueryName = "ORDINE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(34), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo ordine (numero effetto)",;
    HelpContextID = 190618906,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=133, Top=121, InputMask=replicate('X',34)

  add object oDIORDINE_2_9 as StdField with uid="EJHLBVDBUM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DIORDINE", cQueryName = "DIORDINE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(34), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo ordine (numero effetto)",;
    HelpContextID = 133363333,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=133, Top=121, InputMask=replicate('X',34)


  add object oDICONTAT_2_10 as StdCombo with uid="GXQNEXBSUE",rtseq=16,rtrep=.f.,left=133,top=149,width=133,height=21;
    , ToolTipText = "Contatto";
    , HelpContextID = 207009398;
    , cFormVar="w_DICONTAT",RowSource=""+"Per la banca,"+"Per notifica a terzi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDICONTAT_2_10.RadioValue()
    return(iif(this.value =1,'BC',;
    iif(this.value =2,'NT',;
    space(3))))
  endfunc
  func oDICONTAT_2_10.GetRadio()
    this.Parent.oContained.w_DICONTAT = this.RadioValue()
    return .t.
  endfunc

  func oDICONTAT_2_10.SetRadio()
    this.Parent.oContained.w_DICONTAT=trim(this.Parent.oContained.w_DICONTAT)
    this.value = ;
      iif(this.Parent.oContained.w_DICONTAT=='BC',1,;
      iif(this.Parent.oContained.w_DICONTAT=='NT',2,;
      0))
  endfunc

  add object oDIFUNUFF_2_11 as StdField with uid="GYBPWCDXYA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DIFUNUFF", cQueryName = "DIFUNUFF",;
    bObbl = .f. , nPag = 2, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Funzione o ufficio referente",;
    HelpContextID = 78608764,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=389, Top=149, InputMask=replicate('X',17)


  add object oDITIPAVV_2_12 as StdCombo with uid="MYZHKVMPXT",rtseq=18,rtrep=.f.,left=133,top=177,width=133,height=21;
    , ToolTipText = "Tipo di avviso";
    , HelpContextID = 12867980;
    , cFormVar="w_DITIPAVV",RowSource=""+"Tramite telefono,"+"Via telefax", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPAVV_2_12.RadioValue()
    return(iif(this.value =1,'AP',;
    iif(this.value =2,'AT',;
    space(3))))
  endfunc
  func oDITIPAVV_2_12.GetRadio()
    this.Parent.oContained.w_DITIPAVV = this.RadioValue()
    return .t.
  endfunc

  func oDITIPAVV_2_12.SetRadio()
    this.Parent.oContained.w_DITIPAVV=trim(this.Parent.oContained.w_DITIPAVV)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPAVV=='AP',1,;
      iif(this.Parent.oContained.w_DITIPAVV=='AT',2,;
      0))
  endfunc

  add object oDINOMINA_2_13 as StdField with uid="HWHZCHEZTD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DINOMINA", cQueryName = "DINOMINA",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo referente",;
    HelpContextID = 124126857,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=389, Top=177, InputMask=replicate('X',35)

  add object oSTRUTTUR_2_14 as StdCheck with uid="OOTVSSNRCS",rtseq=20,rtrep=.f.,left=133, top=203, caption="Testo libero",;
    ToolTipText = "Disabilita immissione testo libero",;
    HelpContextID = 68175224,;
    cFormVar="w_STRUTTUR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSTRUTTUR_2_14.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSTRUTTUR_2_14.GetRadio()
    this.Parent.oContained.w_STRUTTUR = this.RadioValue()
    return .t.
  endfunc

  func oSTRUTTUR_2_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_STRUTTUR==.T.,1,;
      0)
  endfunc


  add object oDITIPDO1_2_15 as StdCombo with uid="HZRGEWEQXA",rtseq=21,rtrep=.f.,left=12,top=257,width=135,height=22;
    , ToolTipText = "tipo documento uno";
    , HelpContextID = 205235865;
    , cFormVar="w_DITIPDO1",RowSource=""+"Fattura commerciale,"+"Nota di credito,"+"Commissioni,"+"Nota di addebito,"+"Contratto", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPDO1_2_15.RadioValue()
    return(iif(this.value =1,'380',;
    iif(this.value =2,'381',;
    iif(this.value =3,'382',;
    iif(this.value =4,'383',;
    iif(this.value =5,'384',;
    space(3)))))))
  endfunc
  func oDITIPDO1_2_15.GetRadio()
    this.Parent.oContained.w_DITIPDO1 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDO1_2_15.SetRadio()
    this.Parent.oContained.w_DITIPDO1=trim(this.Parent.oContained.w_DITIPDO1)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDO1=='380',1,;
      iif(this.Parent.oContained.w_DITIPDO1=='381',2,;
      iif(this.Parent.oContained.w_DITIPDO1=='382',3,;
      iif(this.Parent.oContained.w_DITIPDO1=='383',4,;
      iif(this.Parent.oContained.w_DITIPDO1=='384',5,;
      0)))))
  endfunc

  func oDITIPDO1_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR)
    endwith
   endif
  endfunc

  add object oDICODDO1_2_16 as StdField with uid="TZDCAXAEUK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DICODDO1", cQueryName = "DICODDO1",;
    bObbl = .f. , nPag = 2, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Codice identif. doc. uno",;
    HelpContextID = 217495193,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=146, Top=257, InputMask=replicate('X',17)

  func oDICODDO1_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO1))
    endwith
   endif
  endfunc

  add object oDIDATDO1_2_17 as StdField with uid="MBOOWZFMFS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DIDATDO1", cQueryName = "DIDATDO1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento uno",;
    HelpContextID = 201631385,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=257

  func oDIDATDO1_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO1))
    endwith
   endif
  endfunc


  add object oDITIPDO2_2_18 as StdCombo with uid="WPUGPVYCHB",rtseq=24,rtrep=.f.,left=12,top=279,width=135,height=22;
    , ToolTipText = "tipo documento due";
    , HelpContextID = 205235864;
    , cFormVar="w_DITIPDO2",RowSource=""+"Fattura commerciale,"+"Nota di credito,"+"Commissioni,"+"Nota di addebito,"+"Contratto", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPDO2_2_18.RadioValue()
    return(iif(this.value =1,'380',;
    iif(this.value =2,'381',;
    iif(this.value =3,'382',;
    iif(this.value =4,'383',;
    iif(this.value =5,'384',;
    space(3)))))))
  endfunc
  func oDITIPDO2_2_18.GetRadio()
    this.Parent.oContained.w_DITIPDO2 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDO2_2_18.SetRadio()
    this.Parent.oContained.w_DITIPDO2=trim(this.Parent.oContained.w_DITIPDO2)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDO2=='380',1,;
      iif(this.Parent.oContained.w_DITIPDO2=='381',2,;
      iif(this.Parent.oContained.w_DITIPDO2=='382',3,;
      iif(this.Parent.oContained.w_DITIPDO2=='383',4,;
      iif(this.Parent.oContained.w_DITIPDO2=='384',5,;
      0)))))
  endfunc

  func oDITIPDO2_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DIDATDO1))
    endwith
   endif
  endfunc

  add object oDICODDO2_2_19 as StdField with uid="OBEOHDWTEY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DICODDO2", cQueryName = "DICODDO2",;
    bObbl = .f. , nPag = 2, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Codice identif. doc. due",;
    HelpContextID = 217495192,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=146, Top=279, InputMask=replicate('X',17)

  func oDICODDO2_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO2))
    endwith
   endif
  endfunc

  add object oDIDATDO2_2_20 as StdField with uid="IITSVOJAIH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DIDATDO2", cQueryName = "DIDATDO2",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento due",;
    HelpContextID = 201631384,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=279

  func oDIDATDO2_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO2))
    endwith
   endif
  endfunc


  add object oDITIPDO3_2_21 as StdCombo with uid="CRLUSAPJTR",rtseq=27,rtrep=.f.,left=12,top=301,width=135,height=22;
    , ToolTipText = "tipo documento tre";
    , HelpContextID = 205235863;
    , cFormVar="w_DITIPDO3",RowSource=""+"Fattura commerciale,"+"Nota di credito,"+"Commissioni,"+"Nota di addebito,"+"Contratto", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPDO3_2_21.RadioValue()
    return(iif(this.value =1,'380',;
    iif(this.value =2,'381',;
    iif(this.value =3,'382',;
    iif(this.value =4,'383',;
    iif(this.value =5,'384',;
    space(3)))))))
  endfunc
  func oDITIPDO3_2_21.GetRadio()
    this.Parent.oContained.w_DITIPDO3 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDO3_2_21.SetRadio()
    this.Parent.oContained.w_DITIPDO3=trim(this.Parent.oContained.w_DITIPDO3)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDO3=='380',1,;
      iif(this.Parent.oContained.w_DITIPDO3=='381',2,;
      iif(this.Parent.oContained.w_DITIPDO3=='382',3,;
      iif(this.Parent.oContained.w_DITIPDO3=='383',4,;
      iif(this.Parent.oContained.w_DITIPDO3=='384',5,;
      0)))))
  endfunc

  func oDITIPDO3_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DIDATDO2))
    endwith
   endif
  endfunc

  add object oDICODDO3_2_22 as StdField with uid="KPEUYPKQZP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DICODDO3", cQueryName = "DICODDO3",;
    bObbl = .f. , nPag = 2, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Codice identif. doc. tre",;
    HelpContextID = 217495191,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=146, Top=301, InputMask=replicate('X',17)

  func oDICODDO3_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO3))
    endwith
   endif
  endfunc

  add object oDIDATDO3_2_23 as StdField with uid="HZPYBBRTOO",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DIDATDO3", cQueryName = "DIDATDO3",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento tre",;
    HelpContextID = 201631383,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=301

  func oDIDATDO3_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO3))
    endwith
   endif
  endfunc


  add object oDITIPDO4_2_24 as StdCombo with uid="ENYBSONBOI",rtseq=30,rtrep=.f.,left=12,top=323,width=135,height=22;
    , ToolTipText = "tipo documento quattro";
    , HelpContextID = 205235862;
    , cFormVar="w_DITIPDO4",RowSource=""+"Fattura commerciale,"+"Nota di credito,"+"Commissioni,"+"Nota di addebito,"+"Contratto", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDITIPDO4_2_24.RadioValue()
    return(iif(this.value =1,'380',;
    iif(this.value =2,'381',;
    iif(this.value =3,'382',;
    iif(this.value =4,'383',;
    iif(this.value =5,'384',;
    space(3)))))))
  endfunc
  func oDITIPDO4_2_24.GetRadio()
    this.Parent.oContained.w_DITIPDO4 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPDO4_2_24.SetRadio()
    this.Parent.oContained.w_DITIPDO4=trim(this.Parent.oContained.w_DITIPDO4)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPDO4=='380',1,;
      iif(this.Parent.oContained.w_DITIPDO4=='381',2,;
      iif(this.Parent.oContained.w_DITIPDO4=='382',3,;
      iif(this.Parent.oContained.w_DITIPDO4=='383',4,;
      iif(this.Parent.oContained.w_DITIPDO4=='384',5,;
      0)))))
  endfunc

  func oDITIPDO4_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DIDATDO3))
    endwith
   endif
  endfunc

  add object oDICODDO4_2_25 as StdField with uid="NPUVBIIGAG",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DICODDO4", cQueryName = "DICODDO4",;
    bObbl = .f. , nPag = 2, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Codice identif. doc. quattro",;
    HelpContextID = 217495190,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=146, Top=323, InputMask=replicate('X',17)

  func oDICODDO4_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO4))
    endwith
   endif
  endfunc

  add object oDIDATDO4_2_26 as StdField with uid="HKHHQFXPMJ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DIDATDO4", cQueryName = "DIDATDO4",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento quattro",;
    HelpContextID = 201631382,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=323

  func oDIDATDO4_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_STRUTTUR and not empty(.w_DITIPDO4))
    endwith
   endif
  endfunc

  add object oDIALTER1_2_27 as StdField with uid="YDAXRBKIUL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DIALTER1", cQueryName = "DIALTER1",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Informazione alternativa uno",;
    HelpContextID = 184145561,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=389, Top=257, InputMask=replicate('X',35)

  func oDIALTER1_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STRUTTUR)
    endwith
   endif
  endfunc

  add object oDIALTER2_2_28 as StdField with uid="JWTADFTDJI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DIALTER2", cQueryName = "DIALTER2",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Informazione alternativa due",;
    HelpContextID = 184145560,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=389, Top=283, InputMask=replicate('X',35)

  func oDIALTER2_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STRUTTUR and not empty(.w_DIALTER1))
    endwith
   endif
  endfunc

  add object oDIALTER3_2_29 as StdField with uid="MZUXFKLNAZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DIALTER3", cQueryName = "DIALTER3",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Informazione alternativa tre",;
    HelpContextID = 184145559,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=389, Top=309, InputMask=replicate('X',35)

  func oDIALTER3_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STRUTTUR and not empty(.w_DIALTER2))
    endwith
   endif
  endfunc

  add object oDESBAN_2_45 as StdField with uid="TDDRTKCDOQ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(42), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione banca",;
    HelpContextID = 53656266,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=222, Top=36, InputMask=replicate('X',42)

  add object oStr_2_30 as StdString with uid="NGKZSUDHQQ",Visible=.t., Left=58, Top=123,;
    Alignment=1, Width=73, Height=18,;
    Caption="Id. ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="MRISAVWLRU",Visible=.t., Left=79, Top=152,;
    Alignment=1, Width=52, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="EARGBISBVP",Visible=.t., Left=301, Top=152,;
    Alignment=1, Width=85, Height=18,;
    Caption="Ufficio refer.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="DQTBHILRCS",Visible=.t., Left=321, Top=180,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="JZKRLBPWBF",Visible=.t., Left=83, Top=180,;
    Alignment=1, Width=48, Height=18,;
    Caption="Avviso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="PYAOMGOTWD",Visible=.t., Left=13, Top=212,;
    Alignment=0, Width=81, Height=18,;
    Caption="Informazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="BQLHAOMMAI",Visible=.t., Left=389, Top=236,;
    Alignment=0, Width=150, Height=18,;
    Caption="Informazioni alternative:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="UVGSXPXBXX",Visible=.t., Left=15, Top=236,;
    Alignment=2, Width=128, Height=18,;
    Caption="Tipo documento"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="SBDMALJFVN",Visible=.t., Left=150, Top=236,;
    Alignment=2, Width=140, Height=18,;
    Caption="Codice documento"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="UGVXOEVZJQ",Visible=.t., Left=298, Top=236,;
    Alignment=2, Width=69, Height=18,;
    Caption="Data"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="YGDDJUOLLY",Visible=.t., Left=15, Top=12,;
    Alignment=1, Width=116, Height=18,;
    Caption="ModalitÓ pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="SJBLKNVLZQ",Visible=.t., Left=3, Top=37,;
    Alignment=1, Width=128, Height=18,;
    Caption="Banca benef.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="NQDXKYJACU",Visible=.t., Left=48, Top=65,;
    Alignment=1, Width=83, Height=18,;
    Caption="N. C/Corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="DGJKVLRQPG",Visible=.t., Left=56, Top=90,;
    Alignment=1, Width=75, Height=18,;
    Caption="Codice IBAN:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="MESSUGEXPC",Visible=.t., Left=390, Top=90,;
    Alignment=1, Width=69, Height=18,;
    Caption="Codice BIC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="TZWQDSPRPO",Visible=.t., Left=362, Top=9,;
    Alignment=1, Width=96, Height=15,;
    Caption="Commissioni:"  ;
  , bGlobalFont=.t.

  add object oBox_2_37 as StdBox with uid="HIRAEJXXQH",left=10, top=232, width=362,height=25

  add object oBox_2_38 as StdBox with uid="CZPAYFGILB",left=145, top=233, width=2,height=114

  add object oBox_2_40 as StdBox with uid="ZXDREAHPCD",left=293, top=234, width=2,height=114

  add object oBox_2_43 as StdBox with uid="JCWAVMOJWM",left=10, top=254, width=362,height=93
enddefine
define class tgste_adbPag3 as StdContainer
  Width  = 732
  height = 348
  stdWidth  = 732
  stdheight = 348
  resizeYpos=269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="ZLYKSCTGJS",left=2, top=15, width=730, height=304, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_adb','DIS_BOES','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DINUMDIS=DIS_BOES.DINUMDIS";
  +" and "+i_cAliasName2+".DIROWORD=DIS_BOES.DIROWORD";
  +" and "+i_cAliasName2+".DIROWNUM=DIS_BOES.DIROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
