* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_kle                                                        *
*              Legenda analisi storico                                         *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_49]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-08                                                      *
* Last revis.: 2009-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgssr_kle",oParentObject))

* --- Class definition
define class tgssr_kle as StdForm
  Top    = 17
  Left   = 18

  * --- Standard Properties
  Width  = 445
  Height = 407
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-09"
  HelpContextID=216622441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gssr_kle"
  cComment = "Legenda analisi storico"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ORDIMP = space(0)
  w_CONTAB = space(0)
  w_DOCAPEES = space(0)
  w_DOCAPEOUTES = space(0)
  w_DOCAPEOUTESCON = space(0)
  w_DOCCOMMAPE = space(0)
  w_DOCPADRE = space(0)
  w_DOCFIGLI0 = space(0)
  w_PROVV = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_klePag1","gssr_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oORDIMP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ORDIMP=space(0)
      .w_CONTAB=space(0)
      .w_DOCAPEES=space(0)
      .w_DOCAPEOUTES=space(0)
      .w_DOCAPEOUTESCON=space(0)
      .w_DOCCOMMAPE=space(0)
      .w_DOCPADRE=space(0)
      .w_DOCFIGLI0=space(0)
      .w_PROVV=space(0)
        .w_ORDIMP = "Documento con almeno una riga non completamente evasa con causale di magazzino"+chr(13)+"che movimenta l'impegnato o l'ordinato."
        .w_CONTAB = IIF(g_COGE='S',"Documento da contabilizzare.","Documento da confermare.")
        .w_DOCAPEES = "Documento non evaso, ma evadibile; cio� la causale � presente nelle origini"+chr(13)+"di altre causali e non � attivo il flag <No Evasione>."
        .w_DOCAPEOUTES = "Documento fuori dall'esercizio analizzato che evade un documento nell'esercizio"+chr(13)+"analizzato."
        .w_DOCAPEOUTESCON = "Documento gi� contabilizzato che evade un documento"+chr(13)+"all'interno dell'esercizio analizzato."
        .w_DOCCOMMAPE = "Documento avente almeno una riga con causale documento che movimenta la"+chr(13)+"commessa."
        .w_DOCPADRE = "Documento non immediatamente storicizzabile."
        .w_DOCFIGLI0 = "Documento antenato del documento non immediatamente storicizzabile"
        .w_PROVV = "Documento Provvisorio."
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDOCCOMMAPE_1_6.visible=!this.oPgFrm.Page1.oPag.oDOCCOMMAPE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oORDIMP_1_1.value==this.w_ORDIMP)
      this.oPgFrm.Page1.oPag.oORDIMP_1_1.value=this.w_ORDIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTAB_1_2.value==this.w_CONTAB)
      this.oPgFrm.Page1.oPag.oCONTAB_1_2.value=this.w_CONTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCAPEES_1_3.value==this.w_DOCAPEES)
      this.oPgFrm.Page1.oPag.oDOCAPEES_1_3.value=this.w_DOCAPEES
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCAPEOUTES_1_4.value==this.w_DOCAPEOUTES)
      this.oPgFrm.Page1.oPag.oDOCAPEOUTES_1_4.value=this.w_DOCAPEOUTES
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCAPEOUTESCON_1_5.value==this.w_DOCAPEOUTESCON)
      this.oPgFrm.Page1.oPag.oDOCAPEOUTESCON_1_5.value=this.w_DOCAPEOUTESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCCOMMAPE_1_6.value==this.w_DOCCOMMAPE)
      this.oPgFrm.Page1.oPag.oDOCCOMMAPE_1_6.value=this.w_DOCCOMMAPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCPADRE_1_7.value==this.w_DOCPADRE)
      this.oPgFrm.Page1.oPag.oDOCPADRE_1_7.value=this.w_DOCPADRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIGLI0_1_8.value==this.w_DOCFIGLI0)
      this.oPgFrm.Page1.oPag.oDOCFIGLI0_1_8.value=this.w_DOCFIGLI0
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVV_1_28.value==this.w_PROVV)
      this.oPgFrm.Page1.oPag.oPROVV_1_28.value=this.w_PROVV
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgssr_klePag1 as StdContainer
  Width  = 441
  height = 407
  stdWidth  = 441
  stdheight = 407
  resizeXpos=208
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORDIMP_1_1 as StdMemo with uid="NGSNAIXOZJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ORDIMP", cQueryName = "ORDIMP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 137638374,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=31, Width=424, Left=8, Top=19, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oCONTAB_1_2 as StdMemo with uid="ZOGRDGKERQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONTAB", cQueryName = "CONTAB",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 159370790,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=190, Left=8, Top=73, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oDOCAPEES_1_3 as StdMemo with uid="QVWNTXDLSS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DOCAPEES", cQueryName = "DOCAPEES",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 224140937,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=31, Width=424, Left=8, Top=117, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oDOCAPEOUTES_1_4 as StdMemo with uid="IXDEBWBMOF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DOCAPEOUTES", cQueryName = "DOCAPEOUTES",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 43935541,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=31, Width=424, Left=8, Top=172, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oDOCAPEOUTESCON_1_5 as StdMemo with uid="VZLLEJBQBS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DOCAPEOUTESCON", cQueryName = "DOCAPEOUTESCON",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 9738443,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=31, Width=424, Left=8, Top=228, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oDOCCOMMAPE_1_6 as StdMemo with uid="TRHLUEQVQT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DOCCOMMAPE", cQueryName = "DOCCOMMAPE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 179410825,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=31, Width=424, Left=8, Top=282, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  func oDOCCOMMAPE_1_6.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oDOCPADRE_1_7 as StdMemo with uid="GAOKPVQJCR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DOCPADRE", cQueryName = "DOCPADRE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 192618107,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=242, Left=8, Top=336, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oDOCFIGLI0_1_8 as StdMemo with uid="LRQOGYQKAO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DOCFIGLI0", cQueryName = "DOCFIGLI0",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 17751681,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=423, Left=8, Top=379, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.


  add object oBtn_1_9 as StdButton with uid="PXBGFOADEM",left=381, top=328, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 142758934;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPROVV_1_28 as StdMemo with uid="QNRLLLBGUV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PROVV", cQueryName = "PROVV",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 120462858,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=211, Left=221, Top=73, ScrollBars=0,BorderStyle=0,tabstop = .f., readonly = .t.

  add object oStr_1_10 as StdString with uid="UJZMHNAZFP",Visible=.t., Left=37, Top=99,;
    Alignment=0, Width=54, Height=15,;
    Caption="Verde"    , ForeColor=RGB(0,255,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="CXONKRWFKM",Visible=.t., Left=37, Top=153,;
    Alignment=0, Width=53, Height=15,;
    Caption="Giallo"    , ForeColor=RGB(255,255,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="JXWWXPUGAH",Visible=.t., Left=37, Top=56,;
    Alignment=0, Width=60, Height=15,;
    Caption="Rosso"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="OGYIMRGVSQ",Visible=.t., Left=91, Top=3,;
    Alignment=0, Width=44, Height=15,;
    Caption="Blue"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="BQYKHNPQXU",Visible=.t., Left=37, Top=265,;
    Alignment=0, Width=64, Height=15,;
    Caption="Fucsia"    , ForeColor=Rgb(250,10,250);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="AUMAVWHFZY",Visible=.t., Left=35, Top=362,;
    Alignment=0, Width=46, Height=15,;
    Caption="Bianco"    , ForeColor=RGB(255,255,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="IDHBBYUUTC",Visible=.t., Left=92, Top=210,;
    Alignment=0, Width=53, Height=15,;
    Caption="Azzurra"    , ForeColor=RGB(0,255,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="USAJPRNETN",Visible=.t., Left=10, Top=56,;
    Alignment=0, Width=20, Height=15,;
    Caption="( ! )"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LNRXMBNHAE",Visible=.t., Left=10, Top=99,;
    Alignment=0, Width=20, Height=15,;
    Caption="( ! )"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LDFASVTDJJ",Visible=.t., Left=10, Top=153,;
    Alignment=0, Width=20, Height=15,;
    Caption="( ! )"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="XQSUNHUPCK",Visible=.t., Left=10, Top=265,;
    Alignment=0, Width=20, Height=15,;
    Caption="( ! )"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="LMLJTKLYYW",Visible=.t., Left=10, Top=3,;
    Alignment=0, Width=74, Height=15,;
    Caption="Colonna tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="PMXOSBITBV",Visible=.t., Left=10, Top=210,;
    Alignment=0, Width=74, Height=15,;
    Caption="Colonna tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="GLUNKPKTLR",Visible=.t., Left=10, Top=361,;
    Alignment=0, Width=20, Height=15,;
    Caption="><"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RSRZJXSDVP",Visible=.t., Left=35, Top=319,;
    Alignment=2, Width=13, Height=15,;
    Caption="V"    , ForeColor=RGB(255,255,0),Backcolor=Rgb(164,44,228),BackStyle=1;
  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="OQBUPSXCYV",Visible=.t., Left=10, Top=318,;
    Alignment=0, Width=19, Height=15,;
    Caption="><"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NIPEKKADLE",Visible=.t., Left=303, Top=57,;
    Alignment=0, Width=41, Height=15,;
    Caption="Rosso"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="GICQOGNINN",Visible=.t., Left=222, Top=57,;
    Alignment=0, Width=74, Height=15,;
    Caption="Colonna tipo"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
