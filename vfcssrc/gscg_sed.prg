* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sed                                                        *
*              Altri dati                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-24                                                      *
* Last revis.: 2014-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sed",oParentObject))

* --- Class definition
define class tgscg_sed as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 467
  Height = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-24"
  HelpContextID=57288041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_sed"
  cComment = "Altri dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VE37 = 0
  w_VE38 = 0
  w_VF16 = 0
  w_VF17 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sedPag1","gscg_sed",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VE37=0
      .w_VE38=0
      .w_VF16=0
      .w_VF17=0
      .w_VE37=oParentObject.w_VE37
      .w_VE38=oParentObject.w_VE38
      .w_VF16=oParentObject.w_VF16
      .w_VF17=oParentObject.w_VF17
    endwith
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VE37=.w_VE37
      .oParentObject.w_VE38=.w_VE38
      .oParentObject.w_VF16=.w_VF16
      .oParentObject.w_VF17=.w_VF17
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVE37_1_1.value==this.w_VE37)
      this.oPgFrm.Page1.oPag.oVE37_1_1.value=this.w_VE37
    endif
    if not(this.oPgFrm.Page1.oPag.oVE38_1_2.value==this.w_VE38)
      this.oPgFrm.Page1.oPag.oVE38_1_2.value=this.w_VE38
    endif
    if not(this.oPgFrm.Page1.oPag.oVF16_1_6.value==this.w_VF16)
      this.oPgFrm.Page1.oPag.oVF16_1_6.value=this.w_VF16
    endif
    if not(this.oPgFrm.Page1.oPag.oVF17_1_7.value==this.w_VF17)
      this.oPgFrm.Page1.oPag.oVF17_1_7.value=this.w_VF17
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_sedPag1 as StdContainer
  Width  = 463
  height = 163
  stdWidth  = 463
  stdheight = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVE37_1_1 as StdField with uid="RQJEMLNCHW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_VE37", cQueryName = "VE37",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni effettuate nell'anno ma con imposta esigibile in anni successivi",;
    HelpContextID = 53455530,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=315, Top=4

  add object oVE38_1_2 as StdField with uid="FXAOFROQKQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VE38", cQueryName = "VE38",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni effettuate in anni precedenti ma con imposta esigibile nell'anno",;
    HelpContextID = 53389994,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=315, Top=31


  add object oBtn_1_3 as StdButton with uid="SAVQGQIKMH",left=406, top=111, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "premere per iniziare l'elaborazione";
    , HelpContextID = 57259290;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVF16_1_6 as StdField with uid="DEBHEDHEQW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VF16", cQueryName = "VF16",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acquisti effettuati nell'anno ma con imposta esigibile in anni successivi",;
    HelpContextID = 53529002,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=315, Top=58

  add object oVF17_1_7 as StdField with uid="OAUGZCMCME",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VF17", cQueryName = "VF17",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acquisti effettuati in anni precedenti ma con imposta esigibile nell'anno",;
    HelpContextID = 53463466,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=315, Top=85

  add object oStr_1_4 as StdString with uid="TPTWPLZCRB",Visible=.t., Left=38, Top=4,;
    Alignment=1, Width=276, Height=18,;
    Caption="Fatture es. diff. dell'anno non incassate/maturate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="RWUPBQLHNQ",Visible=.t., Left=16, Top=32,;
    Alignment=1, Width=298, Height=18,;
    Caption="Fatture es. diff. di anni precedenti incassate/maturate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ABBPDHJISO",Visible=.t., Left=39, Top=59,;
    Alignment=1, Width=275, Height=18,;
    Caption="Acquisti es. diff. dell'anno non pagati/maturati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DJTPWAFHJD",Visible=.t., Left=13, Top=86,;
    Alignment=1, Width=301, Height=18,;
    Caption="Acquisti es. diff. di anni precedenti pagati/maturati:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sed','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
