* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvt                                                        *
*              Trev. partiche                                                  *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-26                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTreeType
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvt",oParentObject,m.pTreeType)
return(i_retval)

define class tgsut_bvt as StdBatch
  * --- Local variables
  pTreeType = space(20)
  w_CRITPERI = space(1)
  w_LIPATIMG = space(254)
  w_PERIODO = ctod("  /  /  ")
  w_IDMODALL = space(1)
  w_PATHABS = space(254)
  w_IDSERIAL = space(15)
  w_LICLADOC = space(10)
  w_NOMEFILE = space(20)
  w_IDOGGETT = space(100)
  w_IDORIFIL = space(250)
  w_IDALLPRI = space(1)
  w_CODPRAT = space(15)
  w_VALATTPRI = space(150)
  w_IDUTEEXP = 0
  w_IMGPAT = space(254)
  w_TIPOFILE = space(100)
  w_RUOLO = space(1)
  w_LIVELLO = 0
  w_FILCLA = space(1)
  w_FILTIP = space(1)
  w_LIVCOR = 0
  w_posizpunto = 0
  w_ChiaveRicerca = space(20)
  * --- WorkFile variables
  TMPZ_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la creazione del cursore associato alla treeview delle pratiche
    if this.pTreeType <> "Drop" and this.pTreeType <> "ZOOM"
      * --- Sbianco lo zoom degli allegati 
      this.oParentObject.w_PRATICA = "###"
      if empty(this.oParentObject.w_NOMECURS)
        this.oParentObject.w_NOMECURS = sys(2015)
      else
        USE IN SELECT(this.oParentObject.w_NOMECURS) 
        this.oParentObject.w_NOMECURS = sys(2015)
      endif
      L_NomeCursore = this.oParentObject.w_NOMECURS
    endif
    do case
      case this.pTreeType="Pratica"
         
 CREATE CURSOR &L_NomeCursore (NODO1 C(30), NODO2 C(30), NODO3 C(30), ; 
 NODO4 C(30), FOGLIA C(30), NODO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), ; 
 DESCRIF C(40), DESCRI C(40),PRATICA C(20), NOMINAT C(30),NOMINATP C(30), TIPALL C(5), CLAALL C(5), CPBMPNAME C(40), NUMLIV N(2))
        ah_Msg("Fase 1: elaborazione pratiche...",.T.)
        VQ_EXEC("QUERY\GSUT1BVT.VQR",this,"CursDati")
        ah_Msg("Fase 2: creazione struttura...",.T.)
        do while Not Eof( "CursDati" )
           
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, DESCRIF, DESCRI, PRATICA, NOMINAT, NOMINATP, TIPALL, CLAALL, CPBMPNAME, NUMLIV) ; 
 VALUES (CursDati.CNCODCAN ,CursDati.IDTIPALL , "", "",CursDati.IDCLAALL," ", CursDati.CNDESCAN, ; 
 CursDati.TADESCRI,"","",CursDati.TACLADES,"","","","","","","",0)
          if Not Eof( "CursDati" )
             
 Select CursDati 
 Skip
          endif
        enddo
        USE IN SELECT("CursDati") 
        SELECT (L_NomeCursore)
        GO TOP
        cp_Level1(L_NomeCursore,"NODO1,NODO2","")
        * --- Organizzo il cursore nella logica di visualizzazione nell'albero per 3 livelli
        * --- Valorizzo il livello 1
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI1 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NOMINAT="@@@" ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
        * --- Valorizzo il livello 2
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..DESCRI=IIF(Not EMpty(&L_NomeCursore..DESCRI2),&L_NomeCursore..DESCRI2,&L_NomeCursore..NODO2) ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NOMINAT="@@@" ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
        * --- Valorizzo il livello 3
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..DESCRI=IIF(Not EMpty(&L_NomeCursore..DESCRIF),&L_NomeCursore..DESCRIF,&L_NomeCursore..FOGLIA) ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NOMINAT="@@@" ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..CLAALL=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
         
 SELECT (L_NomeCursore) 
 Replace CpBmpName With "BMP\Pratica.ico" For NUMLIV=1 
 Replace CpBmpName With "BMP\doc.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=3
        SELECT (L_NomeCursore)
        GO TOP
        this.oParentObject.w_PRATREV.cCursor = this.oParentObject.w_NOMECURS
        this.oParentObject.w_PRATREV.FillTree()     
      case this.pTreeType="C"
         
 CREATE CURSOR &L_NomeCursore (NODO1 C(30), NODO2 C(30), NODO3 C(30), ; 
 NODO4 C(30), FOGLIA C(30), NODO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), ; 
 DESCRIF C(40), DESCRI C(40),PRATICA C(20), NOMINAT C(30), NOMINATP C(30), TIPALL C(5), CLAALL C(5), CPBMPNAME C(40), NUMLIV N(2))
        ah_Msg("Fase 1: elaborazione pratiche per cliente...",.T.)
        VQ_EXEC("QUERY\GSUT2BVT.VQR",this,"CursDati")
        ah_Msg("Fase 2: creazione struttura...",.T.)
        do while Not Eof( "CursDati" )
           
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, DESCRIF, DESCRI, PRATICA, NOMINAT, NOMINATP, TIPALL, CLAALL, CPBMPNAME, NUMLIV) ; 
 VALUES (CursDati.SECODSES,CursDati.CNCODCAN ,CursDati.IDTIPALL , "",CursDati.IDCLAALL," ",CursDati.NODESCRI, ; 
 CursDati.CNDESCAN, CursDati.TADESCRI,"",CursDati.TACLADES,"","","","","","","",0)
          if Not Eof( "CursDati" )
             
 Select CursDati 
 Skip
          endif
        enddo
        USE IN SELECT("CursDati") 
        SELECT (L_NomeCursore)
        GO TOP
        cp_Level1(L_NomeCursore,"NODO1,NODO2,NODO3","")
        * --- Organizzo il cursore nella logica di visualizzazione nell'albero per 3 livelli
        * --- Valorizzo il livello 1
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..DESCRI=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PRATICA="@@@" ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
        * --- Valorizzo il livello 2
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
        * --- Valorizzo il livello 3
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
        * --- Valorizzo il livello 4
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..DESCRI=IIF(Not EMpty(&L_NomeCursore..DESCRIF),&L_NomeCursore..DESCRIF,&L_NomeCursore..FOGLIA) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..CLAALL=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
         
 SELECT (L_NomeCursore) 
 Replace CpBmpName With "BMP\Utepos.ico" For NUMLIV=1 
 Replace CpBmpName With "BMP\Pratica.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\doc.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=4
        SELECT (L_NomeCursore)
        GO TOP
        this.oParentObject.w_PRATREV.cCursor = this.oParentObject.w_NOMECURS
        this.oParentObject.w_PRATREV.FillTree()     
      case this.pTreeType="T" or this.pTreeType="R"
        this.w_RUOLO = this.pTreeType
        * --- Titolare/cliente/pratica/..
         
 CREATE CURSOR &L_NomeCursore (NODO1 C(30), NODO2 C(30), NODO3 C(30), ; 
 NODO4 C(30), FOGLIA C(30), NODO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), ; 
 DESCRIF C(40), DESCRI C(40),DESCRIP C(40), PRATICA C(20), NOMINAT C(30),NOMINATP C(30), TIPALL C(5), CLAALL C(5), CPBMPNAME C(40), NUMLIV N(2))
        ah_Msg("Fase 1: elaborazione pratiche per cliente...",.T.)
        VQ_EXEC("QUERY\GSUT3BVT.VQR",this,"CursDati")
        ah_Msg("Fase 2: creazione struttura...",.T.)
        do while Not Eof( "CursDati" )
           
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, DESCRIF, DESCRI, DESCRIP, PRATICA, NOMINAT,NOMINATP, TIPALL, CLAALL, CPBMPNAME, NUMLIV) ; 
 VALUES (CursDati.RICODRIS,CursDati.SECODSES,CursDati.CNCODCAN ,CursDati.IDTIPALL , CursDati.IDCLAALL," ",CursDati.DPCOGNOM,CursDati.NODESCRI, ; 
 CursDati.CNDESCAN, CursDati.TADESCRI,CursDati.TACLADES,"","","","","","","","",0)
          if Not Eof( "CursDati" )
             
 Select CursDati 
 Skip
          endif
        enddo
        USE IN SELECT("CursDati") 
        SELECT (L_NomeCursore)
        GO TOP
        cp_Level1(L_NomeCursore,"NODO1,NODO2,NODO3,NODO4","")
        * --- Organizzo il cursore nella logica di visualizzazione nell'albero per 3 livelli
        * --- Valorizzo il livello 1
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..DESCRI=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NOMINAT="@@@" ; 
 , &L_NomeCursore..PRATICA="@@@" ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
        * --- Valorizzo il livello 2
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..PRATICA="@@@" ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
        * --- Valorizzo il livello 3
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
        * --- Valorizzo il livello 4
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..DESCRI=IIF(Not Empty(&L_NomeCursore..DESCRI4),&L_NomeCursore..DESCRI4,&L_NomeCursore..NODO4) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
        * --- Valorizzo il livello 5
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..DESCRI=IIF(Not Empty(&L_NomeCursore..DESCRIF),&L_NomeCursore..DESCRIF,&L_NomeCursore..FOGLIA) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..CLAALL=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=5 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*3+1
         
 SELECT (L_NomeCursore) 
 Replace CpBmpName With iif(this.pTreeType="R","BMP\LegResp.ico","BMP\Titolare.ico") For NUMLIV=1 
 Replace CpBmpName With "BMP\Utepos.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\Pratica.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\doc.ico" For NUMLIV=4 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=5
        SELECT (L_NomeCursore)
        GO TOP
        this.oParentObject.w_PRATREV.cCursor = this.oParentObject.w_NOMECURS
        this.oParentObject.w_PRATREV.FillTree()     
      case this.pTreeType="G" or this.pTreeType="L"
        * --- Giudice/Responsabile/Pratica/..   oppure   Legale contropare\Responsabile\Pratica\..
         
 CREATE CURSOR &L_NomeCursore (NODO1 C(30), NODO2 C(30), NODO3 C(30), ; 
 NODO4 C(30), FOGLIA C(30), NODO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), ; 
 DESCRIF C(40), DESCRI C(40),DESCRIP C(40), PRATICA C(20), NOMINAT C(30),NOMINATP C(30), TIPALL C(5), CLAALL C(5), CPBMPNAME C(40), NUMLIV N(2))
        ah_Msg("Fase 1: elaborazione pratiche ...",.T.)
        VQ_EXEC("QUERY\GSUT4BVT.VQR",this,"CursDati")
        ah_Msg("Fase 2: creazione struttura...",.T.)
        do while Not Eof( "CursDati" )
           
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, DESCRIF, DESCRI, DESCRIP, PRATICA, NOMINAT,NOMINATP, TIPALL, CLAALL, CPBMPNAME, NUMLIV) ; 
 VALUES (CursDati.SECODSES,CursDati.RICODRIS,CursDati.CNCODCAN ,CursDati.IDTIPALL , CursDati.IDCLAALL," ",CursDati.NODESCRI,CursDati.DPCOGNOM, ; 
 CursDati.CNDESCAN, CursDati.TADESCRI,CursDati.TACLADES,"","","","","","","","",0)
          if Not Eof( "CursDati" )
             
 Select CursDati 
 Skip
          endif
        enddo
        USE IN SELECT("CursDati") 
        SELECT (L_NomeCursore)
        GO TOP
        cp_Level1(L_NomeCursore,"NODO1,NODO2,NODO3,NODO4","")
        * --- Organizzo il cursore nella logica di visualizzazione nell'albero per 3 livelli
        * --- Valorizzo il livello 1
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NOMINAT="@@@" ; 
 , &L_NomeCursore..PRATICA="@@@" ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
        * --- Valorizzo il livello 2
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..PRATICA="@@@" ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
        * --- Valorizzo il livello 3
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..DESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
        * --- Valorizzo il livello 4
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..DESCRI=IIF(Not Empty(&L_NomeCursore..DESCRI4),&L_NomeCursore..DESCRI4,&L_NomeCursore..NODO4) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
        * --- Valorizzo il livello 5
        UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..DESCRI=IIF(Not Empty(&L_NomeCursore..DESCRIF),&L_NomeCursore..DESCRIF,&L_NomeCursore..FOGLIA) ; 
 , &L_NomeCursore..NOMINATP=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..DESCRIP=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PRATICA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TIPALL=&L_NomeCursore..NODO4 ; 
 , &L_NomeCursore..CLAALL=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=5 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*3+1
         
 SELECT (L_NomeCursore) 
 Replace CpBmpName With iif(this.pTreeType="G","BMP\conftesta.ico","BMP\LegContr.ico") For NUMLIV=1 
 Replace CpBmpName With "BMP\LegResp.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\Pratica.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\doc.ico" For NUMLIV=4 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=5
        SELECT (L_NomeCursore)
        GO TOP
        this.oParentObject.w_PRATREV.cCursor = this.oParentObject.w_NOMECURS
        this.oParentObject.w_PRATREV.FillTree()     
      case this.pTreeType="Drop"
        USE IN SELECT(this.oParentObject.w_NOMECURS) 
        * --- Drop temporary table TMPZ
        i_nIdx=cp_GetTableDefIdx('TMPZ')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPZ')
        endif
      case this.pTreeType=="ZOOM"
        if ! Used((this.oparentobject.w_PRATREV.ccursor))
          this.oparentobject.NotifyEvent("CalcTree")
        endif
         
 Select (this.oparentobject.w_PRATREV.ccursor)
        this.w_LIVELLO = Nvl(NUMLIV,0)
        * --- Create temporary table TMPZ
        i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSUT_KVT',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPZ_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_LIVCOR = icase(this.oParentObject.w_TIPOTREE="Pratica",3,this.oParentObject.w_TIPOTREE="C",4,5)
        this.w_FILCLA = IIF(this.w_LIVELLO=this.w_LIVCOR,"S","N")
        if this.w_FILCLA="S"
          * --- Delete from TMPZ
          i_nConn=i_TableProp[this.TMPZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"IDCLAALL <> "+cp_ToStrODBC(this.oParentObject.w_CLASALL);
                   )
          else
            delete from (i_cTable) where;
                  IDCLAALL <> this.oParentObject.w_CLASALL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        this.w_LIVCOR = icase(this.oParentObject.w_TIPOTREE="Pratica",2,this.oParentObject.w_TIPOTREE="C",3,4)
        this.w_FILTIP = IIF(this.w_LIVELLO=this.w_LIVCOR,"S","N")
        if this.w_FILTIP="S" or this.w_FILCLA="S"
          * --- Delete from TMPZ
          i_nConn=i_TableProp[this.TMPZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"IDTIPALL <> "+cp_ToStrODBC(this.oParentObject.w_TIPOALL);
                   )
          else
            delete from (i_cTable) where;
                  IDTIPALL <> this.oParentObject.w_TIPOALL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        * --- Select from TMPZ
        i_nConn=i_TableProp[this.TMPZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2],.t.,this.TMPZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMPZ ";
               ,"_Curs_TMPZ")
        else
          select * from (i_cTable);
            into cursor _Curs_TMPZ
        endif
        if used('_Curs_TMPZ')
          select _Curs_TMPZ
          locate for 1=1
          do while not(eof())
          if empty(this.oParentObject.w_CODCLAS)
            this.w_CRITPERI = nvl(_Curs_TMPZ.CDTIPRAG, " ")
            this.w_LIPATIMG = nvl(_Curs_TMPZ.CDPATSTD, " ")
          endif
          this.w_PERIODO = _Curs_TMPZ.PERIODO
          this.w_IDMODALL = nvl(_Curs_TMPZ.IDMODALL, "F")
          this.w_PATHABS = nvl(_Curs_TMPZ.GFPATHFILE, "  ")
          this.w_IDSERIAL = nvl(_Curs_TMPZ.GFKEYINDIC, " ")
          this.w_LICLADOC = nvl(_Curs_TMPZ.GFCLASDOCU, " ")
          this.w_NOMEFILE = rtrim(nvl(_Curs_TMPZ.GFNOMEFILE," "))
          this.w_IDOGGETT = nvl(_Curs_TMPZ.IDOGGETT, " ")
          this.w_IDORIFIL = iif(this.w_IDMODALL="L",nvl(_Curs_TMPZ.IDORIFIL,""),"")
          this.w_IDALLPRI = nvl(_Curs_TMPZ.IDALLPRI, "  ")
          this.w_IDUTEEXP = NVL(_Curs_TMPZ.IDUTEEXP, 0)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_TMPZ
            continue
          enddo
          use
        endif
        this.oParentObject.w_PRATICHE.cCpQueryName = "QUERY\GSUT5KVT"
        this.oParentObject.NotifyEvent("Aggiorna")
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Distingue i due casi .... collegamento o copia file ...
    do case
      case this.w_IDMODALL=="F"
        * --- Gestione del periodo (determina percorsi...) => Determina w_ImgPat
        * --- Verifico se � presente il percorso assoluto di archiviazione dell'allegato
        if not empty(this.w_PATHABS)
          this.w_IMGPAT = alltrim(this.w_PATHABS)
        else
          this.w_IMGPAT = DCMPATAR(this.w_LIPATIMG, this.w_CRITPERI, this.w_PERIODO, this.w_LICLADOC)
        endif
      case this.w_IDMODALL=="L"
        * --- Nel caso di collegamento il percorso � quello in ORIFIL
        this.w_IMGPAT = LEFT(this.w_IDORIFIL, RAT("\", this.w_IDORIFIL))
    endcase
    * --- Acquisizione della directory corrente - PARTE COMUNE
    if (this.oParentObject.w_ControllaEsisteFile="S" AND cp_fileexist(this.w_IMGPAT+this.w_NOMEFILE)) OR this.oParentObject.w_ControllaEsisteFile="N" OR this.w_IDMODALL=="E"
      if empty(this.w_IDOGGETT)
        * --- Passa w_CHIAVERICERCA e determina w_TIPOFILE
        this.w_posizpunto = rat(".",this.w_NOMEFILE)
        this.w_ChiaveRicerca = alltrim(lower(substr(this.w_NOMEFILE,this.w_posizpunto)))
        this.w_TIPOFILE = GETEXTDESC(this.w_ChiaveRicerca)
      else
        this.w_TIPOFILE = this.w_IDOGGETT
      endif
      * --- Aggiorno i campi relativi al Path e alla descrizione del file (allegato)
      * --- Write into TMPZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GFPATHFILE ="+cp_NullLink(cp_ToStrODBC(this.w_IMGPAT),'TMPZ','GFPATHFILE');
        +",IDOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOFILE),'TMPZ','IDOGGETT');
        +",ISFILE ="+cp_NullLink(cp_ToStrODBC(IIF(this.w_IDUTEEXP<>0, "E", "S")),'TMPZ','ISFILE');
            +i_ccchkf ;
        +" where ";
            +"GFKEYINDIC = "+cp_ToStrODBC(this.w_IDSERIAL);
               )
      else
        update (i_cTable) set;
            GFPATHFILE = this.w_IMGPAT;
            ,IDOGGETT = this.w_TIPOFILE;
            ,ISFILE = IIF(this.w_IDUTEEXP<>0, "E", "S");
            &i_ccchkf. ;
         where;
            GFKEYINDIC = this.w_IDSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTreeType)
    this.pTreeType=pTreeType
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TMPZ')
      use in _Curs_TMPZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTreeType"
endproc
