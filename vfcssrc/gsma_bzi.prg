* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bzi                                                        *
*              Elabora zoom dett. inventari                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-29                                                      *
* Last revis.: 2009-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Operaz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bzi",oParentObject,m.w_Operaz)
return(i_retval)

define class tgsma_bzi as StdBatch
  * --- Local variables
  w_Operaz = space(20)
  w_CODAZI = space(5)
  w_VALNAZ = space(3)
  w_DECUNI = 0
  w_ZOOM = space(10)
  w_INVEDETT = .NULL.
  * --- WorkFile variables
  ESERCIZI_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione dei bottoni presenti nello zoom di dettaglio degli inventari
    * --- Questo batch viene utilizzato dalle maschere GSMA_SZI e GSMA_SZA.
    * --- Parametri
    * --- Variabili della maschera (GSMA_SZI)
    * --- Variabili locali
    this.w_ZOOM = this.oParentObject.w_ZoomInv
    * --- Operazioni previste
    do case
      case this.w_Operaz = "ESEGUI"
        * --- Aggiorna lo zoom in base alle selezioni impostate
        this.w_VALNAZ = g_PERVAL
        this.w_DECUNI = 2
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_VALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        VVU = 20*this.w_DECUNI
        This.OparentObject.NotifyEvent("Esegui")
      case this.w_Operaz = "LANCIA"
        * --- Richiama l'anagrafica con il dettaglio relativo alla riga selezionata sullo zoom
        this.w_CODAZI = i_CODAZI
        this.w_VALNAZ = g_PERVAL
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESVALNAZ"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESVALNAZ;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.oParentObject.w_INCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DECUNI = 2
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_VALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        VVU = 20*this.w_DECUNI
        this.w_INVEDETT = GSMA_AID()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_INVEDETT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Inizializza la chiave
        this.w_INVEDETT.w_DINUMINV = this.oParentObject.w_INNUMINV
        this.w_INVEDETT.w_DICODESE = this.oParentObject.w_INCODESE
        this.w_INVEDETT.w_DICODICE = this.oParentObject.w_ZICODICE
        * --- Creazione cursore delle chiavi
        this.w_INVEDETT.QueryKeySet("DINUMINV="+cp_ToStrODBC(this.oParentObject.w_INNUMINV)+" AND DICODESE="+cp_ToStrODBC(this.oParentObject.w_INCODESE) +" AND DICODICE="+cp_ToStrODBC(this.oParentObject.w_ZICODICE), "")     
        * --- Carica il record
        this.w_INVEDETT.LoadRec()     
    endcase
  endproc


  proc Init(oParentObject,w_Operaz)
    this.w_Operaz=w_Operaz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Operaz"
endproc
