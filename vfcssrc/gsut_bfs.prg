* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bfs                                                        *
*              Nome File da carica / salva dati esterni                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-24                                                      *
* Last revis.: 2014-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bfs",oParentObject)
return(i_retval)

define class tgsut_bfs as StdBatch
  * --- Local variables
  w_CODPROD = space(5)
  * --- WorkFile variables
  COD_IMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso di selezione di un modulo o di un archivio con file diversi a secondo del prodotto utilizzato, provvedo alla selezione di quello corretto.
    this.oParentObject.w_NOTE = " "
    this.oParentObject.w_FILENAME = Space(50)
    if len(alltrim(this.oParentObject.w_PROMOD)) > 0
      this.w_CODPROD = iif(ISAHE(),"%AHE%",iif(ISAHR(),"%AHR%","%AET%"))
      * --- Sono in presenza di un file associato ad un modulo o di un archivio con file ascii diversificati in base al prodotto (AHE/AHR) usato
      * --- Select from COD_IMP
      i_nConn=i_TableProp[this.COD_IMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_IMP_idx,2],.t.,this.COD_IMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CINOMFIL,CICODIMP,CIPROMOD, CICONOTE  from "+i_cTable+" COD_IMP ";
            +" where CICODIMP = "+cp_ToStrODBC(this.oParentObject.w_TIPARC)+" and CIPROMOD like "+cp_ToStrODBC(this.w_CODPROD)+"";
             ,"_Curs_COD_IMP")
      else
        select CINOMFIL,CICODIMP,CIPROMOD, CICONOTE from (i_cTable);
         where CICODIMP = this.oParentObject.w_TIPARC and CIPROMOD like this.w_CODPROD;
          into cursor _Curs_COD_IMP
      endif
      if used('_Curs_COD_IMP')
        select _Curs_COD_IMP
        locate for 1=1
        do while not(eof())
        this.oParentObject.w_FILENAME = _Curs_COD_IMP.CINOMFIL
        this.oParentObject.w_NOTE = _Curs_COD_IMP.CICONOTE
          select _Curs_COD_IMP
          continue
        enddo
        use
      endif
      if Empty(this.oParentObject.w_Filename) 
        if empty (this.oParentObject.w_NOTE)
          * --- Read from COD_IMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_IMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_IMP_idx,2],.t.,this.COD_IMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CINOMFIL,CICONOTE"+;
              " from "+i_cTable+" COD_IMP where ";
                  +"CICODIMP = "+cp_ToStrODBC(this.oParentObject.w_TIPARC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CINOMFIL,CICONOTE;
              from (i_cTable) where;
                  CICODIMP = this.oParentObject.w_TIPARC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_FILENAME = NVL(cp_ToDate(_read_.CINOMFIL),cp_NullValue(_read_.CINOMFIL))
            this.oParentObject.w_NOTE = NVL(cp_ToDate(_read_.CICONOTE),cp_NullValue(_read_.CICONOTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from COD_IMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_IMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_IMP_idx,2],.t.,this.COD_IMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CINOMFIL"+;
              " from "+i_cTable+" COD_IMP where ";
                  +"CICODIMP = "+cp_ToStrODBC(this.oParentObject.w_TIPARC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CINOMFIL;
              from (i_cTable) where;
                  CICODIMP = this.oParentObject.w_TIPARC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_FILENAME = NVL(cp_ToDate(_read_.CINOMFIL),cp_NullValue(_read_.CINOMFIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COD_IMP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_COD_IMP')
      use in _Curs_COD_IMP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
