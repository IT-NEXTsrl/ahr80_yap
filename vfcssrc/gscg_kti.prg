* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kti                                                        *
*              Inoltro telematico IVA periodica                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_547]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-08-10                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kti",oParentObject))

* --- Class definition
define class tgscg_kti as StdForm
  Top    = 7
  Left   = 33

  * --- Standard Properties
  Width  = 582
  Height = 369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-07"
  HelpContextID=185981591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  IVA_PERI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_kti"
  cComment = "Inoltro telematico IVA periodica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_KEYATT = space(5)
  o_KEYATT = space(5)
  w_ANNO = space(4)
  o_ANNO = space(4)
  w_PERIOD = 0
  o_PERIOD = 0
  w_DATASTAM = ctod('  /  /  ')
  w_TIPFORN = space(2)
  w_IDSERTEL = space(17)
  w_PROGRDIC = space(6)
  w_DICHCONF = space(10)
  w_DICINTEG = space(10)
  w_FIRMAPRE = space(10)
  w_NOMEFILEFIS = space(20)
  w_NOMEFILE = space(40)
  w_CAMPO68 = space(1)
  w_CAMPO69 = space(1)
  w_CAMPO70 = space(1)
  w_CAMPO72 = space(1)
  o_CAMPO72 = space(1)
  w_CODFISCA = space(16)
  w_CAMPO73 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_ktiPag1","gscg_kti",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='IVA_PERI'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BTI with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_KEYATT=space(5)
      .w_ANNO=space(4)
      .w_PERIOD=0
      .w_DATASTAM=ctod("  /  /  ")
      .w_TIPFORN=space(2)
      .w_IDSERTEL=space(17)
      .w_PROGRDIC=space(6)
      .w_DICHCONF=space(10)
      .w_DICINTEG=space(10)
      .w_FIRMAPRE=space(10)
      .w_NOMEFILEFIS=space(20)
      .w_NOMEFILE=space(40)
      .w_CAMPO68=space(1)
      .w_CAMPO69=space(1)
      .w_CAMPO70=space(1)
      .w_CAMPO72=space(1)
      .w_CODFISCA=space(16)
      .w_CAMPO73=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_KEYATT = IIF(g_ATTIVI='S', '#####', g_CATAZI)
        .w_ANNO = ALLTRIM(STR(YEAR(i_datsys)))
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ANNO))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERIOD))
          .link_1_4('Full')
        endif
        .w_DATASTAM = i_datsys
          .DoRTCalc(6,11,.f.)
        .w_NOMEFILEFIS = 'TRASMIVA'
        .w_NOMEFILE = left(sys(5)+sys(2003)+'\'+.w_NOMEFILEFIS+space(40),40)
          .DoRTCalc(14,17,.f.)
        .w_CODFISCA = space(16)
    endwith
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_KEYATT<>.w_KEYATT.or. .o_ANNO<>.w_ANNO.or. .o_PERIOD<>.w_PERIOD
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        endif
            .w_KEYATT = IIF(g_ATTIVI='S', '#####', g_CATAZI)
        .DoRTCalc(3,17,.t.)
        if .o_CAMPO72<>.w_CAMPO72
            .w_CODFISCA = space(16)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPERIOD_1_4.enabled = this.oPgFrm.Page1.oPag.oPERIOD_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCAMPO68_1_24.enabled = this.oPgFrm.Page1.oPag.oCAMPO68_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCAMPO69_1_25.enabled = this.oPgFrm.Page1.oPag.oCAMPO69_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCODFISCA_1_28.enabled = this.oPgFrm.Page1.oPag.oCODFISCA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCAMPO73_1_29.enabled = this.oPgFrm.Page1.oPag.oCAMPO73_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZTIPFOR";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZTIPFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_TIPFORN = NVL(_Link_.AZTIPFOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TIPFORN = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_lTable = "IVA_PERI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2], .t., this.IVA_PERI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVP',True,'IVA_PERI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VP__ANNO like "+cp_ToStrODBC(trim(this.w_ANNO)+"%");
                   +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);

          i_ret=cp_SQL(i_nConn,"select VPKEYATT,VP__ANNO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VPKEYATT,VP__ANNO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VPKEYATT',this.w_KEYATT;
                     ,'VP__ANNO',trim(this.w_ANNO))
          select VPKEYATT,VP__ANNO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VPKEYATT,VP__ANNO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNO)==trim(_Link_.VP__ANNO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNO) and !this.bDontReportError
            deferred_cp_zoom('IVA_PERI','*','VPKEYATT,VP__ANNO',cp_AbsName(oSource.parent,'oANNO_1_3'),i_cWhere,'GSAR_AVP',"Elenco dichiarazioni IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYATT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VPKEYATT,VP__ANNO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VPKEYATT,VP__ANNO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VPKEYATT,VP__ANNO";
                     +" from "+i_cTable+" "+i_lTable+" where VP__ANNO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VPKEYATT',oSource.xKey(1);
                       ,'VP__ANNO',oSource.xKey(2))
            select VPKEYATT,VP__ANNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VPKEYATT,VP__ANNO";
                   +" from "+i_cTable+" "+i_lTable+" where VP__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VPKEYATT',this.w_KEYATT;
                       ,'VP__ANNO',this.w_ANNO)
            select VPKEYATT,VP__ANNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNO = NVL(_Link_.VP__ANNO,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ANNO = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Val(.w_ANNO)>1900
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANNO = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])+'\'+cp_ToStr(_Link_.VPKEYATT,1)+'\'+cp_ToStr(_Link_.VP__ANNO,1)
      cp_ShowWarn(i_cKey,this.IVA_PERI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIOD
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IVA_PERI_IDX,3]
    i_lTable = "IVA_PERI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2], .t., this.IVA_PERI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVP',True,'IVA_PERI')
        if i_nConn<>0
          i_cWhere = " VPPERIOD="+cp_ToStrODBC(this.w_PERIOD);
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);

          i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPKEYATT,VPPERIOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VP__ANNO',this.w_ANNO;
                     ,'VPKEYATT',this.w_KEYATT;
                     ,'VPPERIOD',this.w_PERIOD)
          select VP__ANNO,VPKEYATT,VPPERIOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PERIOD) and !this.bDontReportError
            deferred_cp_zoom('IVA_PERI','*','VP__ANNO,VPKEYATT,VPPERIOD',cp_AbsName(oSource.parent,'oPERIOD_1_4'),i_cWhere,'GSAR_AVP',"Elenco dichiarazioni IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANNO<>oSource.xKey(1);
           .or. this.w_KEYATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPKEYATT,VPPERIOD";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VP__ANNO,VPKEYATT,VPPERIOD;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPKEYATT,VPPERIOD";
                     +" from "+i_cTable+" "+i_lTable+" where VPPERIOD="+cp_ToStrODBC(oSource.xKey(3));
                     +" and VP__ANNO="+cp_ToStrODBC(this.w_ANNO);
                     +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',oSource.xKey(1);
                       ,'VPKEYATT',oSource.xKey(2);
                       ,'VPPERIOD',oSource.xKey(3))
            select VP__ANNO,VPKEYATT,VPPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VP__ANNO,VPKEYATT,VPPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where VPPERIOD="+cp_ToStrODBC(this.w_PERIOD);
                   +" and VP__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and VPKEYATT="+cp_ToStrODBC(this.w_KEYATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VP__ANNO',this.w_ANNO;
                       ,'VPKEYATT',this.w_KEYATT;
                       ,'VPPERIOD',this.w_PERIOD)
            select VP__ANNO,VPKEYATT,VPPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIOD = NVL(_Link_.VPPERIOD,0)
    else
      if i_cCtrl<>'Load'
        this.w_PERIOD = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IVA_PERI_IDX,2])+'\'+cp_ToStr(_Link_.VP__ANNO,1)+'\'+cp_ToStr(_Link_.VPKEYATT,1)+'\'+cp_ToStr(_Link_.VPPERIOD,1)
      cp_ShowWarn(i_cKey,this.IVA_PERI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_3.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_3.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_4.value==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_4.value=this.w_PERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_5.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_5.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPFORN_1_6.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page1.oPag.oTIPFORN_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIDSERTEL_1_7.value==this.w_IDSERTEL)
      this.oPgFrm.Page1.oPag.oIDSERTEL_1_7.value=this.w_IDSERTEL
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRDIC_1_8.value==this.w_PROGRDIC)
      this.oPgFrm.Page1.oPag.oPROGRDIC_1_8.value=this.w_PROGRDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDICHCONF_1_9.RadioValue()==this.w_DICHCONF)
      this.oPgFrm.Page1.oPag.oDICHCONF_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICINTEG_1_10.RadioValue()==this.w_DICINTEG)
      this.oPgFrm.Page1.oPag.oDICINTEG_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMAPRE_1_11.RadioValue()==this.w_FIRMAPRE)
      this.oPgFrm.Page1.oPag.oFIRMAPRE_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_13.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_13.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO68_1_24.RadioValue()==this.w_CAMPO68)
      this.oPgFrm.Page1.oPag.oCAMPO68_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO69_1_25.RadioValue()==this.w_CAMPO69)
      this.oPgFrm.Page1.oPag.oCAMPO69_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO70_1_26.RadioValue()==this.w_CAMPO70)
      this.oPgFrm.Page1.oPag.oCAMPO70_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO72_1_27.RadioValue()==this.w_CAMPO72)
      this.oPgFrm.Page1.oPag.oCAMPO72_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFISCA_1_28.value==this.w_CODFISCA)
      this.oPgFrm.Page1.oPag.oCODFISCA_1_28.value=this.w_CODFISCA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO73_1_29.RadioValue()==this.w_CAMPO73)
      this.oPgFrm.Page1.oPag.oCAMPO73_1_29.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_CODFISCA) OR CHKCFP(.w_CODFISCA,"CF"))  and (.w_CAMPO72='1')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFISCA_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_KEYATT = this.w_KEYATT
    this.o_ANNO = this.w_ANNO
    this.o_PERIOD = this.w_PERIOD
    this.o_CAMPO72 = this.w_CAMPO72
    return

enddefine

* --- Define pages as container
define class tgscg_ktiPag1 as StdContainer
  Width  = 578
  height = 369
  stdWidth  = 578
  stdheight = 369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_3 as StdField with uid="EIMFZSBUGK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno progressivi registri IVA",;
    HelpContextID = 191499526,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=169, Top=7, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="IVA_PERI", cZoomOnZoom="GSAR_AVP", oKey_1_1="VPKEYATT", oKey_1_2="this.w_KEYATT", oKey_2_1="VP__ANNO", oKey_2_2="this.w_ANNO"

  func oANNO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_PERIOD)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oANNO_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNO_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.IVA_PERI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPKEYATT="+cp_ToStrODBC(this.Parent.oContained.w_KEYATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPKEYATT="+cp_ToStr(this.Parent.oContained.w_KEYATT)
    endif
    do cp_zoom with 'IVA_PERI','*','VPKEYATT,VP__ANNO',cp_AbsName(this.parent,'oANNO_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVP',"Elenco dichiarazioni IVA",'',this.parent.oContained
  endproc
  proc oANNO_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VPKEYATT=w_KEYATT
     i_obj.w_VP__ANNO=this.parent.oContained.w_ANNO
     i_obj.ecpSave()
  endproc

  add object oPERIOD_1_4 as StdField with uid="CICZPUVCUG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PERIOD", cQueryName = "PERIOD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo dei progressivi registri IVA definitivi",;
    HelpContextID = 72631542,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=327, Top=7, cSayPict='"@Z99"', cGetPict='"99"', bHasZoom = .t. , cLinkFile="IVA_PERI", cZoomOnZoom="GSAR_AVP", oKey_1_1="VP__ANNO", oKey_1_2="this.w_ANNO", oKey_2_1="VPKEYATT", oKey_2_2="this.w_KEYATT", oKey_3_1="VPPERIOD", oKey_3_2="this.w_PERIOD"

  func oPERIOD_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_ANNO))
    endwith
   endif
  endfunc

  func oPERIOD_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIOD_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIOD_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.IVA_PERI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_ANNO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPKEYATT="+cp_ToStrODBC(this.Parent.oContained.w_KEYATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VP__ANNO="+cp_ToStr(this.Parent.oContained.w_ANNO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VPKEYATT="+cp_ToStr(this.Parent.oContained.w_KEYATT)
    endif
    do cp_zoom with 'IVA_PERI','*','VP__ANNO,VPKEYATT,VPPERIOD',cp_AbsName(this.parent,'oPERIOD_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVP',"Elenco dichiarazioni IVA",'',this.parent.oContained
  endproc
  proc oPERIOD_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VP__ANNO=w_ANNO
    i_obj.VPKEYATT=w_KEYATT
     i_obj.w_VPPERIOD=this.parent.oContained.w_PERIOD
     i_obj.ecpSave()
  endproc

  add object oDATASTAM_1_5 as StdField with uid="XDFKECOWAY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di presentazione della trasmissione telematica dell'IVA periodica.",;
    HelpContextID = 76308611,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=500, Top=7


  add object oTIPFORN_1_6 as StdCombo with uid="YXONQUGQRO",rtseq=6,rtrep=.f.,left=169,top=36,width=202,height=21, enabled=.f.;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 38873398;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Dichiarazioni via Internet,"+"02: Dichiarazioni via Entratel,"+"03: C.A.F. dipendenti e pensionati,"+"05: C.A.F: imprese,"+"09: Art 3 comma 2,"+"10: Altri intermediari", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPFORN_1_6.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'02',;
    iif(this.value =3,'03',;
    iif(this.value =4,'05',;
    iif(this.value =5,'09',;
    iif(this.value =6,'10',;
    space(2))))))))
  endfunc
  func oTIPFORN_1_6.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_1_6.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='02',2,;
      iif(this.Parent.oContained.w_TIPFORN=='03',3,;
      iif(this.Parent.oContained.w_TIPFORN=='05',4,;
      iif(this.Parent.oContained.w_TIPFORN=='09',5,;
      iif(this.Parent.oContained.w_TIPFORN=='10',6,;
      0))))))
  endfunc

  add object oIDSERTEL_1_7 as StdField with uid="OPVYXJLOMR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IDSERTEL", cQueryName = "IDSERTEL",;
    bObbl = .f. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo assegnato dal servizio telematico",;
    HelpContextID = 192916526,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=169, Top=61, cSayPict="'99999999999999999'", cGetPict="'99999999999999999'", InputMask=replicate('X',17)

  add object oPROGRDIC_1_8 as StdField with uid="LLBRNPZBQS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PROGRDIC", cQueryName = "PROGRDIC",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo dichiarazione all'interno del file inviato",;
    HelpContextID = 192798151,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=169, Top=88, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6)

  add object oDICHCONF_1_9 as StdCheck with uid="VSZARAMVEE",rtseq=9,rtrep=.f.,left=380, top=36, caption="Dich. confermata",;
    ToolTipText = "Comunicazione di mancata corrispondenza dei dati da trasmettere con quelli corrispondenti dalla dichiarazione",;
    HelpContextID = 244471932,;
    cFormVar="w_DICHCONF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDICHCONF_1_9.RadioValue()
    return(iif(this.value =1,'1',;
    ' '))
  endfunc
  func oDICHCONF_1_9.GetRadio()
    this.Parent.oContained.w_DICHCONF = this.RadioValue()
    return .t.
  endfunc

  func oDICHCONF_1_9.SetRadio()
    this.Parent.oContained.w_DICHCONF=trim(this.Parent.oContained.w_DICHCONF)
    this.value = ;
      iif(this.Parent.oContained.w_DICHCONF=='1',1,;
      0)
  endfunc

  add object oDICINTEG_1_10 as StdCheck with uid="ECBZOYIAGB",rtseq=10,rtrep=.f.,left=380, top=57, caption="Dich. integrativa",;
    ToolTipText = "Dichiarazione integrativa",;
    HelpContextID = 196913027,;
    cFormVar="w_DICINTEG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDICINTEG_1_10.RadioValue()
    return(iif(this.value =1,'1',;
    ' '))
  endfunc
  func oDICINTEG_1_10.GetRadio()
    this.Parent.oContained.w_DICINTEG = this.RadioValue()
    return .t.
  endfunc

  func oDICINTEG_1_10.SetRadio()
    this.Parent.oContained.w_DICINTEG=trim(this.Parent.oContained.w_DICINTEG)
    this.value = ;
      iif(this.Parent.oContained.w_DICINTEG=='1',1,;
      0)
  endfunc

  add object oFIRMAPRE_1_11 as StdCheck with uid="BDQUQMJSUE",rtseq=11,rtrep=.f.,left=380, top=78, caption="Firma dichiarante",;
    ToolTipText = "Firma del dichiarante: se valorizzato firma presente, altrimenti assente.",;
    HelpContextID = 259541147,;
    cFormVar="w_FIRMAPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFIRMAPRE_1_11.RadioValue()
    return(iif(this.value =1,'1',;
    ' '))
  endfunc
  func oFIRMAPRE_1_11.GetRadio()
    this.Parent.oContained.w_FIRMAPRE = this.RadioValue()
    return .t.
  endfunc

  func oFIRMAPRE_1_11.SetRadio()
    this.Parent.oContained.w_FIRMAPRE=trim(this.Parent.oContained.w_FIRMAPRE)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMAPRE=='1',1,;
      0)
  endfunc

  add object oNOMEFILE_1_13 as StdField with uid="TTXFBMRMGW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome del percorso e del file per la creazione memorizzazione dati dell'IVA.",;
    HelpContextID = 146800411,;
   bGlobalFont=.t.,;
    Height=21, Width=344, Left=169, Top=115, InputMask=replicate('X',40)


  add object oBtn_1_14 as StdButton with uid="RRSYFUCCUN",left=519, top=115, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file trasmiva.";
    , HelpContextID = 186182614;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .w_NOMEFILE=left(CP_getdir(sys(5)+sys(2003),"Percorso di destinazione")+.w_NOMEFILEFIS+space(40),40)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCAMPO68_1_24 as StdCheck with uid="ZMTPJMQJRH",rtseq=14,rtrep=.f.,left=169, top=173, caption="Dich. predisposta dal contribuente",;
    ToolTipText = "Impegno a trasmettere in via telematica la dichiarazione predisposta dal contribuente",;
    HelpContextID = 161812442,;
    cFormVar="w_CAMPO68", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMPO68_1_24.RadioValue()
    return(iif(this.value =1,'1',;
    space(1)))
  endfunc
  func oCAMPO68_1_24.GetRadio()
    this.Parent.oContained.w_CAMPO68 = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO68_1_24.SetRadio()
    this.Parent.oContained.w_CAMPO68=trim(this.Parent.oContained.w_CAMPO68)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO68=='1',1,;
      0)
  endfunc

  func oCAMPO68_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMPO69 <> '1')
    endwith
   endif
  endfunc

  add object oCAMPO69_1_25 as StdCheck with uid="BGGCFEDIQY",rtseq=15,rtrep=.f.,left=169, top=194, caption="Dich. del contribuente predisposta dal soggetto che la trasmette",;
    ToolTipText = "Impegno a trasmette in via telematica la dichiarazione predisposta dal soggetto che la trasmette",;
    HelpContextID = 161812442,;
    cFormVar="w_CAMPO69", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMPO69_1_25.RadioValue()
    return(iif(this.value =1,'1',;
    space(1)))
  endfunc
  func oCAMPO69_1_25.GetRadio()
    this.Parent.oContained.w_CAMPO69 = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO69_1_25.SetRadio()
    this.Parent.oContained.w_CAMPO69=trim(this.Parent.oContained.w_CAMPO69)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO69=='1',1,;
      0)
  endfunc

  func oCAMPO69_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMPO68 <> '1')
    endwith
   endif
  endfunc

  add object oCAMPO70_1_26 as StdCheck with uid="WVHLCEHJJA",rtseq=16,rtrep=.f.,left=169, top=215, caption="Firma",;
    ToolTipText = "Firma presente se il flag � valorizzato",;
    HelpContextID = 123400230,;
    cFormVar="w_CAMPO70", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMPO70_1_26.RadioValue()
    return(iif(this.value =1,'1',;
    space(1)))
  endfunc
  func oCAMPO70_1_26.GetRadio()
    this.Parent.oContained.w_CAMPO70 = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO70_1_26.SetRadio()
    this.Parent.oContained.w_CAMPO70=trim(this.Parent.oContained.w_CAMPO70)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO70=='1',1,;
      0)
  endfunc

  add object oCAMPO72_1_27 as StdCheck with uid="YWQAMLSYVU",rtseq=17,rtrep=.f.,left=13, top=244, caption="Visto di conformit�",;
    ToolTipText = "Visto di conformit�",;
    HelpContextID = 123400230,;
    cFormVar="w_CAMPO72", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMPO72_1_27.RadioValue()
    return(iif(this.value =1,'1',;
    space(1)))
  endfunc
  func oCAMPO72_1_27.GetRadio()
    this.Parent.oContained.w_CAMPO72 = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO72_1_27.SetRadio()
    this.Parent.oContained.w_CAMPO72=trim(this.Parent.oContained.w_CAMPO72)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO72=='1',1,;
      0)
  endfunc

  add object oCODFISCA_1_28 as StdField with uid="RIWPRZCHHC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODFISCA", cQueryName = "CODFISCA",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del responsabile del CAF o del professionista",;
    HelpContextID = 219124121,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=169, Top=272, InputMask=replicate('X',16)

  func oCODFISCA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMPO72='1')
    endwith
   endif
  endfunc

  func oCODFISCA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_CODFISCA) OR CHKCFP(.w_CODFISCA,"CF"))
    endwith
    return bRes
  endfunc

  add object oCAMPO73_1_29 as StdCheck with uid="IFMWUSGZKC",rtseq=19,rtrep=.f.,left=169, top=300, caption="Firma del resp.del CAF o del professionista",;
    ToolTipText = "Firma presente se il flag � valorizzato",;
    HelpContextID = 123400230,;
    cFormVar="w_CAMPO73", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMPO73_1_29.RadioValue()
    return(iif(this.value =1,'1',;
    space(1)))
  endfunc
  func oCAMPO73_1_29.GetRadio()
    this.Parent.oContained.w_CAMPO73 = this.RadioValue()
    return .t.
  endfunc

  func oCAMPO73_1_29.SetRadio()
    this.Parent.oContained.w_CAMPO73=trim(this.Parent.oContained.w_CAMPO73)
    this.value = ;
      iif(this.Parent.oContained.w_CAMPO73=='1',1,;
      0)
  endfunc

  func oCAMPO73_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMPO72='1')
    endwith
   endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="TQHKMDXUDK",left=473, top=320, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 186010342;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        do GSCG_BTI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ANNO) and not empty(.w_PERIOD) and not empty(.w_NOMEFILE))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="VCFFCFSXTB",left=524, top=320, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 193299014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="DBHOIEWHRI",Visible=.t., Left=12, Top=7,;
    Alignment=1, Width=154, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="MXUJEFFMFQ",Visible=.t., Left=222, Top=7,;
    Alignment=1, Width=102, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IJCBISCUJT",Visible=.t., Left=377, Top=7,;
    Alignment=1, Width=119, Height=18,;
    Caption="Data inoltro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="FHAWYKXQMD",Visible=.t., Left=12, Top=115,;
    Alignment=1, Width=154, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RRKACWRJGP",Visible=.t., Left=12, Top=36,;
    Alignment=1, Width=154, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YLILLLCGED",Visible=.t., Left=12, Top=61,;
    Alignment=1, Width=154, Height=15,;
    Caption="Identificativo assegnato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YNRKNSJMAP",Visible=.t., Left=12, Top=88,;
    Alignment=1, Width=154, Height=15,;
    Caption="Progressivo dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BQCCQSFMGU",Visible=.t., Left=12, Top=146,;
    Alignment=0, Width=504, Height=15,;
    Caption="Ricevuta di presentazione agli intermediari per la trasmissone telematica"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ZHNSXDFWCY",Visible=.t., Left=10, Top=272,;
    Alignment=1, Width=156, Height=18,;
    Caption="Cod.fiscale del CAF/profes.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="MDDKELHEWY",left=9, top=163, width=561,height=1

  add object oBox_1_32 as StdBox with uid="WQGYYSDLML",left=9, top=263, width=561,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kti','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
