* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bab                                                        *
*              Stampa etichette articoli                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_490]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2008-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bab",oParentObject)
return(i_retval)

define class tgsma_bab as StdBatch
  * --- Local variables
  w_CODBAR = space(20)
  w_QUANTI = 0
  w_PREZZO = 0
  w_PREEUR = 0
  w_UNIMIS = space(3)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_FLFRAZ = space(1)
  w_MAX = 0
  w_OLDQUANTI = 0
  w_COUNT = 0
  * --- WorkFile variables
  UNIMIS_idx=0
  OUTPUTMP_idx=0
  TMPSALDI_idx=0
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Etichette (da GSMA_SAB)
    *     La variabile caller w_PARAM specifica se stampare le etichette articoli (A)
    *     o le etichette matricole (M), nel caso di w_PARAM = 'M' l'output utente � diverso
    *     e punta alla gestione fittizia GSMD_SAB
    * --- Controllo se sono stati valorizzati tutti i dati necessari all'elaborazione
    if this.oParentObject.w_NUMCOP="E" AND EMPTY(this.oParentObject.w_CODMAG)
      ah_ErrorMsg("Inserire codice magazzino")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_NUMCOP="M" AND (EMPTY(this.oParentObject.w_CODCA1) AND EMPTY(this.oParentObject.w_CODCA2) AND EMPTY(this.oParentObject.w_CODCA3) AND EMPTY(this.oParentObject.w_CODCA1) AND EMPTY(this.oParentObject.w_CODCA4) AND EMPTY(this.oParentObject.w_CODCA5))
      ah_ErrorMsg("Inserire almeno una causale magazzino")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura articoli 
    if this.oParentObject.w_TUTTIPRE<>"S"
      if NOT EMPTY(this.oParentObject.w_CODLIS)
        * --- Leggo articoli con listini
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSMA_BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Seleziono gli scaglioni con qt� pi� piccola
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSMA3BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Drop temporary table TMPVEND3
        i_nIdx=cp_GetTableDefIdx('TMPVEND3')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND3')
        endif
      else
        * --- Leggo solo articoli senza listino
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSMA8BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      * --- Leggo articoli con listini e tutti i prezzi
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSMA_BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Verifica e modifica scaglioni  sull'etichetta se l'unit� di misura non � la principale
    if this.oParentObject.w_FUNIMI<>"P" AND NOT EMPTY(this.oParentObject.w_CODLIS)
      * --- Select from TMPVEND1
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMPVEND1 ";
             ,"_Curs_TMPVEND1")
      else
        select * from (i_cTable);
          into cursor _Curs_TMPVEND1
      endif
      if used('_Curs_TMPVEND1')
        select _Curs_TMPVEND1
        locate for 1=1
        do while not(eof())
        this.w_UNIMIS = NVL(_Curs_TMPVEND1.UNIMIS,SPACE(3))
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CODBAR = NVL(_Curs_TMPVEND1.CODBAR,SPACE(20))
        this.w_OLDQUANTI = NVL(_Curs_TMPVEND1.QUANTI,0)
        this.w_QUANTI = NVL(_Curs_TMPVEND1.QUANTI,0)
        this.w_MOLTIP = NVL(_Curs_TMPVEND1.MOLTIP,0)
        this.w_MOLTI3 = NVL(_Curs_TMPVEND1.MOLTI3,0)
        this.w_OPERAT = NVL(_Curs_TMPVEND1.OPERAT,"")
        this.w_OPERA3 = NVL(_Curs_TMPVEND1.OPERA3,"")
        * --- Se diverso da Oltre
        if this.w_QUANTI <> 99999999.999
          do case
            case this.oParentObject.w_FUNIMI="S" AND this.w_MOLTIP<>0
              * --- Selezionata 2^ Unita' di Misura
              if this.w_OPERAT="*"
                this.w_QUANTI = this.w_QUANTI * this.w_MOLTIP
              else
                this.w_QUANTI = this.w_QUANTI / this.w_MOLTIP
              endif
            case this.oParentObject.w_FUNIMI="A" AND this.w_MOLTI3<>0
              * --- Selezionata 3^ Unita' di Misura
              if this.w_OPERA3="*"
                this.w_QUANTI = this.w_QUANTI * this.w_MOLTI3
              else
                this.w_QUANTI = this.w_QUANTI / this.w_MOLTI3
              endif
          endcase
        endif
        if this.w_FLFRAZ<>"S" OR (this.w_FLFRAZ="S" AND this.w_QUANTI>0)
          * --- Se frazionabile arrotondo all'intero superiore
          if this.w_FLFRAZ="S"
            this.w_QUANTI = CEILING(this.w_QUANTI)
          endif
          * --- Write into TMPVEND1
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"QUANTI ="+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'TMPVEND1','QUANTI');
                +i_ccchkf ;
            +" where ";
                +"CODBAR = "+cp_ToStrODBC(this.w_CODBAR);
                +" and QUANTI = "+cp_ToStrODBC(this.w_OLDQUANTI);
                   )
          else
            update (i_cTable) set;
                QUANTI = this.w_QUANTI;
                &i_ccchkf. ;
             where;
                CODBAR = this.w_CODBAR;
                and QUANTI = this.w_OLDQUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Delete from TMPVEND1
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CODBAR = "+cp_ToStrODBC(this.w_CODBAR);
                  +" and QUANTI = "+cp_ToStrODBC(this.w_OLDQUANTI);
                   )
          else
            delete from (i_cTable) where;
                  CODBAR = this.w_CODBAR;
                  and QUANTI = this.w_OLDQUANTI;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
          select _Curs_TMPVEND1
          continue
        enddo
        use
      endif
    endif
    * --- Discrimino se stampare le etichette articoli o le etichette matricole
    do case
      case this.oParentObject.w_PARAM = "M"
        * --- Stampa Etichette Matricole
        do case
          case this.oParentObject.w_NUMCOP = "E"
            * --- Numero copie sulla base dell'esistenza
            * --- Create temporary table OUTPUTMP
            i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\MADV\EXE\QUERY\GSMDLBAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.OUTPUTMP_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          case this.oParentObject.w_NUMCOP = "M"
            * --- Numero copie sulla base dei movimenti
            * --- Create temporary table OUTPUTMP
            i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\MADV\EXE\QUERY\GSMDM2BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.OUTPUTMP_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endcase
      case this.oParentObject.w_PARAM = "A"
        * --- Stampa Etichette Articoli
        * --- Duplico le etichette, controllo se devo stampare anche il lotto
        if this.oParentObject.w_STPLOTTI="S" or this.oParentObject.w_STPLOTTI="U" 
          * --- Stampo anche i lotti
          do case
            case this.oParentObject.w_NUMCOP="E"
              * --- Calcola il Numero di Copie in Base all'inventario fisico
              * --- Create temporary table TMPSALDI
              i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('QUERY\GSMAE10BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMPSALDI_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              * --- Calcola il Numero di Copie in Base all'esistenza saldi
              * --- Create temporary table TMPVEND3
              i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('QUERY\GSMAEBAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMPVEND3_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            case this.oParentObject.w_NUMCOP="M"
              * --- Calcola il Numero di Copie in Base ai Movimenti 
              * --- Create temporary table TMPSALDI
              i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('QUERY\GSMAM6BAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMPSALDI_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endcase
          * --- Inserisco il campo QTASTA che contiene il numero di copie per ogni codice
          * --- Create temporary table TMPVEND2
          i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSMAB1BAB',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVEND2_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Aggiorno QTASTA per gli articoli che non hanno lotto
          if this.oParentObject.w_NUMCOP="E"
            * --- Write into TMPVEND2
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND2_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="CODART"
              do vq_exec with 'QUERY\GSMAZBAB',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPVEND2.CODART = _t2.CODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTASTA = _t2.QTASTA";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPVEND2.CODART = _t2.CODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
                  +"TMPVEND2.QTASTA = _t2.QTASTA";
                  +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="TMPVEND2.CODART = t2.CODART";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set (";
                  +"QTASTA";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.QTASTA";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="TMPVEND2.CODART = _t2.CODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
                  +"QTASTA = _t2.QTASTA";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".CODART = "+i_cQueryTable+".CODART";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTASTA = (select QTASTA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Seleziono il numero massimo di duplicati
          * --- Select from QUERY\GSMADBAB
          do vq_exec with 'QUERY\GSMADBAB',this,'_Curs_QUERY_GSMADBAB','',.f.,.t.
          if used('_Curs_QUERY_GSMADBAB')
            select _Curs_QUERY_GSMADBAB
            locate for 1=1
            do while not(eof())
            this.w_MAX = MAXNUM
              select _Curs_QUERY_GSMADBAB
              continue
            enddo
            use
          endif
        else
          * --- Non stampo i lotti
          if this.oParentObject.w_NUMCOP="F"
            this.w_MAX = this.oParentObject.w_COPIE
            * --- Inserisco il campo QTASTA che contiene il numero di copie per ogni codice
            * --- Create temporary table TMPVEND2
            i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSMAGBAB',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPVEND2_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            do case
              case this.oParentObject.w_NUMCOP="E"
                * --- Calcola il Numero di Copie in Base all'esistenza saldi
                * --- Create temporary table TMPSALDI
                i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('QUERY\GSMAEBAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPSALDI_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
              case this.oParentObject.w_NUMCOP="M"
                * --- Calcola il Numero di Copie in Base ai Movimenti 
                * --- Create temporary table TMPSALDI
                i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('QUERY\GSMAMBAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPSALDI_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            endcase
            * --- Inserisco il campo QTASTA che contiene il numero di copie per ogni codice
            * --- Create temporary table TMPVEND2
            i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSMABBAB',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPVEND2_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Seleziono il numero massimo di duplicati
            * --- Select from QUERY\GSMADBAB
            do vq_exec with 'QUERY\GSMADBAB',this,'_Curs_QUERY_GSMADBAB','',.f.,.t.
            if used('_Curs_QUERY_GSMADBAB')
              select _Curs_QUERY_GSMADBAB
              locate for 1=1
              do while not(eof())
              this.w_MAX = MAXNUM
                select _Curs_QUERY_GSMADBAB
                continue
              enddo
              use
            endif
          endif
        endif
        * --- Controllo dimensione MOLTIPXN
        CHKMOLTIPXN(this,this.w_MAX)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Eseguo la join tra TMPVEND2 e MOLTIPXN per effettuare la duplicazione
        * --- Create temporary table OUTPUTMP
        i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSMAABAB.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.OUTPUTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endcase
    L_STAMPQTA = this.oParentObject.w_STAMPQTA
    L_TIPBAR=this.oParentObject.w_TIPBAR
    L_STAMPA1 = .T.
    L_STAMPA2 = .T.
    L_STAMPQTA = this.oParentObject.w_STAMPQTA
    L_STPLOTTI = this.oParentObject.w_STPLOTTI
    L_PREMAS = this.oParentObject.w_PREMAS
    L_PERMUC = this.oParentObject.w_PERMUC
    do case
      case EMPTY(this.oParentObject.w_CODLIS) 
        * --- Non Visualizzo i Prezzi
        L_Val1 = ""
        L_Val2 = ""
        L_STAMPA1 = .F.
        L_STAMPA2 = .F.
        L_Formato1 = "@Z "+V_PV[38]
        L_Formato2 = "@Z "+V_PV[38]
      case (this.oParentObject.w_CODVAL<>g_PERVAL AND this.oParentObject.w_CAOVAL<>0) 
        * --- Moneta EMU - Stampo corrispondente importo in Euro 
        L_Formato1 = "@Z "+V_PU[38 + (20*this.oParentObject.w_DECTOT) ]
        L_Val1 = this.oParentObject.w_SIMVAL
        L_Formato2 = "@Z "+V_PU[38 + (20*this.oParentObject.w_DECEUR) ]
        L_Val2 = "Pari a Euro "
        L_STAMPA1 = .T.
        L_STAMPA2 = .T.
      case this.oParentObject.w_CAOVAL=0 
        * --- Moneta Extra Europea - Non stampo corrispondente importo in Euro oppure no listino
        L_Formato1 = "@Z "+V_PU[38 + (20*this.oParentObject.w_DECTOT) ]
        L_Val1 = this.oParentObject.w_SIMVAL
        L_Formato2 = "@Z "+V_PU[38]
        L_Val2 = ""
        L_STAMPA2 = .F.
      case this.oParentObject.w_CODVAL=this.oParentObject.w_VALUTA
        * --- Euro - Non stampo prezzo nella seconda riga 
        L_Val1 = this.oParentObject.w_SIMVAL
        L_Val2 = ""
        L_Formato1 = "@Z "+V_PU[38 +(20 *this.oParentObject.w_DECEUR)]
        L_Formato2 = "@Z "+V_PU[38]
        L_STAMPA2 = .F.
    endcase
    * --- Lancio il report scelto nell'Output utente
    vq_exec(THIS.oParentObject.w_OQRY, this, "__TMP__")
    SELECT __TMP__
    if this.oParentObject.w_NUMSKIP>0 AND RECCOUNT()>0
       
 SELECT * FROM __TMP__ INTO CURSOR tmpCursor_bab 
 SELECT __TMP__ 
 ZAP
      this.w_COUNT = 0
      do while this.w_COUNT < this.oParentObject.w_NUMSKIP
         
 SELECT __TMP__ 
 APPEND BLANK 
        this.w_COUNT = this.w_COUNT + 1
      enddo
      SELECT *, -1 AS NUM FROM __TMP__ UNION ALL (SELECT *, RECNO() AS NUM FROM tmpCursor_bab) INTO CURSOR __TMP__ ORDER BY NUM
      USE IN SELECT("tmpCursor_bab")
    endif
     
 SELECT __TMP__ 
 CP_CHPRN(alltrim(THIS.oParentObject.w_orep), ,This.oParentObject)
    if used("__tmp__")
      select __tmp__
      use
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='*OUTPUTMP'
    this.cWorkTables[3]='*TMPSALDI'
    this.cWorkTables[4]='*TMPVEND1'
    this.cWorkTables[5]='*TMPVEND2'
    this.cWorkTables[6]='*TMPVEND3'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    if used('_Curs_QUERY_GSMADBAB')
      use in _Curs_QUERY_GSMADBAB
    endif
    if used('_Curs_QUERY_GSMADBAB')
      use in _Curs_QUERY_GSMADBAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
