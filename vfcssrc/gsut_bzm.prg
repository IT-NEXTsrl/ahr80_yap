* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bzm                                                        *
*              Generazione massiva XML documenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-29                                                      *
* Last revis.: 2014-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bzm",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_bzm as StdBatch
  * --- Local variables
  pOPER = space(10)
  NC = space(10)
  w_NRECSEL = 0
  w_MVSERIAL = space(10)
  w_NOMEFILE = space(10)
  w_CHIAVE = space(20)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  w_OPERAZIONE = space(1)
  w_COPATHDS = space(200)
  w_CODAZI = space(5)
  w_NCOUNT = 0
  w_CURS_BARCODE = space(10)
  w_REPNAME = space(50)
  w_RES = 0
  w_TIPFILE = space(10)
  w_INARCHIVIO = space(0)
  w_OBJXML = .NULL.
  w_UploadDocumentsXML = space(254)
  w_XMLdescrittore = space(254)
  w_XMLFILE = space(200)
  w_CATDOC = space(2)
  w_CATEGO = space(2)
  w_CICLO = space(1)
  w_clifor = space(15)
  w_CODAGE = space(5)
  w_CODART = space(20)
  w_CODATT1 = space(15)
  w_CODATT2 = space(15)
  w_CODCOM = space(15)
  w_codese = space(4)
  w_data1 = ctod("  /  /  ")
  w_data2 = ctod("  /  /  ")
  w_datafi = ctod("  /  /  ")
  w_datain = ctod("  /  /  ")
  w_DATOBSO = ctod("  /  /  ")
  w_DQ = space(40)
  w_DT = space(35)
  w_dt = space(35)
  w_fc = space(1)
  w_FILVEAC = space(10)
  w_FLEVAS = space(1)
  w_FLVEAC = space(1)
  w_FLVEAC2 = space(1)
  w_LINGUA = space(3)
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_numfin = 0
  w_numini = 0
  w_REPSEC = space(1)
  w_SERDOC = space(10)
  w_serie1 = space(10)
  w_serie2 = space(10)
  w_stampa = 0
  w_TDFLVEAC = space(1)
  w_TIPATT = space(1)
  w_TIPCON = space(1)
  w_TIPOIN = space(5)
  w_TIPOVAO = space(1)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CATCOM = space(3)
  w_CODDES = space(5)
  w_CODPAG = space(5)
  w_NOSBAN = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  CONTROPA_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione massiva XML documenti (da GSUT_SZM)
    * --- var caller di GSUT_BBA
    this.w_INARCHIVIO = "X"
    * --- Escludi report opzionali
    this.w_REPSEC = "C"
    this.w_CODAZI = i_CODAZI
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COPATHDS"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COPATHDS;
        from (i_cTable) where;
            COCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_COPATHDS = addbs(this.w_COPATHDS)
    if empty(this.w_COPATHDS) or not directory(this.w_COPATHDS)
      ah_errormsg("Errore nell'invio a Infinity stand alone del documento%0Percorso per xml-descrittore non valido","!")
    else
      this.NC = this.oParentObject.w_ZoomDoc.cCursor
      Select (this.NC)
      SUM(xchk) to this.w_NRECSEL
      if this.w_NRECSEL=0
        ah_errormsg("Nessun record selezionato","!")
      else
        if this.pOPER="XML"
          create cursor curbarcode (EXTCODE C(30), ATTRIBUTO C(15), DESCRIZIONE C(40), VALORE C(254))
          this.w_CURS_BARCODE = "curbarcode"
          this.oParentObject.w_CDREPOBC = iif(file(this.oParentObject.w_CDREPOBC), alltrim(this.oParentObject.w_CDREPOBC), "")
          this.w_REPNAME = evl(this.oParentObject.w_CDREPOBC, iif(this.oParentObject.w_CDTIPOBC="F","query\gsut1bcv","query\gsut_bcv"))
        endif
        Select (this.NC)
        scan for xchk=1
        this.w_RES = 0
        this.w_MVSERIAL = MVSERIAL
        * --- Select from DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
              +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+"";
               ,"_Curs_DOC_MAST")
        else
          select * from (i_cTable);
           where MVSERIAL=this.w_MVSERIAL;
            into cursor _Curs_DOC_MAST
        endif
        if used('_Curs_DOC_MAST')
          select _Curs_DOC_MAST
          locate for 1=1
          do while not(eof())
          Select * from _curs_DOC_MAST into cursor _ATTRIB_
            select _Curs_DOC_MAST
            continue
          enddo
          use
        endif
        this.w_UploadDocumentsXML = ""
        this.w_XMLdescrittore = ""
        ah_msg("Elaborazione record " + alltrim(str(this.w_NCOUNT+1)) + " di " + alltrim(str(this.w_NRECSEL)))
        do case
          case this.pOPER="XML"
            * --- Stampa barcode di associazione per acquisizione da Infinity e genera file XML
            this.w_OPERAZIONE = "B"
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.w_NOMEFILE
            L_ArrParam(3)=this.oParentObject.w_CLASSE
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)="_ATTRIB_"
            L_ArrParam(6)=this.oParentObject.w_ARCHIVIO
            L_ArrParam(7)=this.w_MVSERIAL
            L_ArrParam(8)=""
            L_ArrParam(9)=""
            L_ArrParam(10)=.F.
            L_ArrParam(11)=.F.
            L_ArrParam(12)=""
            L_ArrParam(13)=""
            L_ArrParam(14)=.NULL.
            L_ArrParam(15)=this.w_TIPOALLE
            L_ArrParam(16)=this.w_CLASSEALL
            L_ArrParam(17)=""
            L_ArrParam(18)=""
            L_ArrParam(19)=this.w_OPERAZIONE
            L_ArrParam(21)=.F.
            L_ArrParam(22)=""
            L_ArrParam(23)=.T.
            L_ArrParam(24)=.T.
            this.w_RES = GSUT_BBA(this, @L_ArrParam)
          case this.pOPER="PDF"
            * --- Genera PDF da ristampa doc. e genera file XML
            this.w_NOMEFILE = this.w_COPATHDS + this.w_MVSERIAL + ".PDF"
            Select (this.NC)
            this.w_TIPOIN = MVTIPDOC
            this.w_CATDOC = MVCLADOC
            this.w_FLVEAC = MVFLVEAC
            * --- Creo file PDF
            GSVE_BRD(this,"A", 10, this.w_NOMEFILE)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if file(this.w_NOMEFILE)
              Private L_ArrParam
              dimension L_ArrParam(30)
              L_ArrParam(1)="AR"
              L_ArrParam(2)=this.w_NOMEFILE
              L_ArrParam(3)=this.oParentObject.w_CLASSE
              L_ArrParam(4)=i_CODUTE
              L_ArrParam(5)="_ATTRIB_"
              L_ArrParam(6)=""
              L_ArrParam(7)=""
              L_ArrParam(8)=""
              L_ArrParam(9)=""
              L_ArrParam(10)=.F.
              L_ArrParam(11)=.F.
              L_ArrParam(12)=""
              L_ArrParam(13)=""
              L_ArrParam(14)=.NULL.
              L_ArrParam(15)=this.w_TIPOALLE
              L_ArrParam(16)=this.w_CLASSEALL
              L_ArrParam(17)=""
              L_ArrParam(18)=""
              L_ArrParam(19)=""
              L_ArrParam(21)=.F.
              L_ArrParam(22)=""
              L_ArrParam(23)=.T.
              L_ArrParam(24)=.T.
              this.w_RES = GSUT_BBA(this, @L_ArrParam)
            else
              this.w_RES = -999
            endif
        endcase
        if this.w_RES>0
          ah_errormsg("Errore nell'invio a Infinity stand alone: codice errore %1.","!","", alltrim(str(this.w_RES)))
          if ah_yesno("Si desidera annullare l'elaborazione?")
            Select (this.NC)
            go bottom
          endif
        endif
        if this.w_RES<>-999
          if empty(this.w_UploadDocumentsXML) or empty(this.w_XMLdescrittore)
            ah_errormsg("Errore nell'invio a Infinity stand alone: descrittore non creato.","!")
            if ah_yesno("Si desidera annullare l'elaborazione?")
              Select (this.NC)
              go bottom
            endif
          else
            * --- XML descrittore per invio a Infinity stand alone
            this.w_XMLFILE = FORCEEXT(this.w_UploadDocumentsXML,"xml")
            * --- Memorizza ... e imposta
             w_CreateObjErr = .F. 
 w_OldErr=on("ERROR")
            on error w_CreateObjErr=.t.
            this.w_XMLFILE = this.w_COPATHDS+justfname(this.w_XMLFILE)
            STRTOFILE(this.w_XMLdescrittore, this.w_XMLFILE)
            if w_CreateObjErr
              ah_errormsg("Errore nell'invio a Infinity stand alone del documento%0%1","!","", message())
            else
              this.w_NCOUNT = this.w_NCOUNT+1
            endif
            * --- Ripristina OnError
            on error &w_OldErr
          endif
        endif
        endscan
        this.w_TIPFILE = iif(this.pOPER="PDF","xml", "barcode")
        if this.w_NCOUNT=0
          if used(this.w_CURS_BARCODE)
            use in (this.w_CURS_BARCODE)
          endif
          AH_ErrorMsg("Attenzione: nessun %1 per invio a Infinity stand alone � stato generato",48,,this.w_TIPFILE)
        else
          if this.pOPER="XML"
            Select * from (this.w_CURS_BARCODE) into cursor __tmp__
            use in (this.w_CURS_BARCODE)
            Select __tmp__
            cp_chprn(this.w_REPNAME)
          endif
          AH_ErrorMsg("Invio a Infinity stand alone completato.%0Sono stati generati %1 %3 di %2 documenti selezionati.",; 
 48,,alltrim(str(this.w_NCOUNT)), alltrim(str(this.w_NRECSEL)),this.w_TIPFILE)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
