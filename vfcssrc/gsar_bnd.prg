* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bnd                                                        *
*              Elimina allegati                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-11                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bnd",oParentObject)
return(i_retval)

define class tgsar_bnd as StdBatch
  * --- Local variables
  w_NOCODICE = space(20)
  w_NODESCRI = space(40)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se esistono delle scadenze con questo creditore/debitore diverso
    * --- Select from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) as CONTASCADENZE  from "+i_cTable+" PAR_TITE ";
          +" where PTCODBEN=this.oparentobject.w_NOCODICE";
           ,"_Curs_PAR_TITE")
    else
      select count(*) as CONTASCADENZE from (i_cTable);
       where PTCODBEN=this.oparentobject.w_NOCODICE;
        into cursor _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      select _Curs_PAR_TITE
      locate for 1=1
      do while not(eof())
      if CONTASCADENZE>0
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg="Il nominativo � stato utilizzato nelle scadenze. Impossibile cancellare"
        i_Error=i_TrsMsg
        return
      endif
        select _Curs_PAR_TITE
        continue
      enddo
      use
    endif
    * --- --
    if not btrserr
      * --- Elimina Allegati in caso di cancellazione Nominativo (da GSAR_ANO)
      this.w_NOCODICE = this.oparentobject.w_NOCODICE
      this.w_NODESCRI = this.oparentobject.w_NODESCRI
      if g_OFFE="S"
        do GSAR_BOD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
