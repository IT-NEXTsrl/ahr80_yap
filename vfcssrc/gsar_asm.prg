* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_asm                                                        *
*              Categorie sconti/maggiorazioni                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2009-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_asm"))

* --- Class definition
define class tgsar_asm as StdForm
  Top    = 92
  Left   = 45

  * --- Standard Properties
  Width  = 382
  Height = 105+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-30"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  CAT_SCMA_IDX = 0
  cFile = "CAT_SCMA"
  cKeySelect = "CSCODICE"
  cKeyWhere  = "CSCODICE=this.w_CSCODICE"
  cKeyWhereODBC = '"CSCODICE="+cp_ToStrODBC(this.w_CSCODICE)';

  cKeyWhereODBCqualified = '"CAT_SCMA.CSCODICE="+cp_ToStrODBC(this.w_CSCODICE)';

  cPrg = "gsar_asm"
  cComment = "Categorie sconti/maggiorazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CSCODICE = space(5)
  w_CSDESCRI = space(35)
  w_CSTIPCAT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAT_SCMA','gsar_asm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_asmPag1","gsar_asm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categorie")
      .Pages(1).HelpContextID = 11324193
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCSCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAT_SCMA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAT_SCMA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAT_SCMA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CSCODICE = NVL(CSCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAT_SCMA where CSCODICE=KeySet.CSCODICE
    *
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAT_SCMA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAT_SCMA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAT_SCMA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CSCODICE = NVL(CSCODICE,space(5))
        .w_CSDESCRI = NVL(CSDESCRI,space(35))
        .w_CSTIPCAT = NVL(CSTIPCAT,space(1))
        cp_LoadRecExtFlds(this,'CAT_SCMA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CSCODICE = space(5)
      .w_CSDESCRI = space(35)
      .w_CSTIPCAT = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CSTIPCAT = 'C'
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAT_SCMA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCSCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCSDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCSTIPCAT_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCSCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCSCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAT_SCMA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSCODICE,"CSCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSDESCRI,"CSDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSTIPCAT,"CSTIPCAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    i_lTable = "CAT_SCMA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAT_SCMA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSAR_SSM.VQR,QUERY\GSAR_SSM.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAT_SCMA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAT_SCMA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAT_SCMA')
        i_extval=cp_InsertValODBCExtFlds(this,'CAT_SCMA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CSCODICE,CSDESCRI,CSTIPCAT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CSCODICE)+;
                  ","+cp_ToStrODBC(this.w_CSDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CSTIPCAT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAT_SCMA')
        i_extval=cp_InsertValVFPExtFlds(this,'CAT_SCMA')
        cp_CheckDeletedKey(i_cTable,0,'CSCODICE',this.w_CSCODICE)
        INSERT INTO (i_cTable);
              (CSCODICE,CSDESCRI,CSTIPCAT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CSCODICE;
                  ,this.w_CSDESCRI;
                  ,this.w_CSTIPCAT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAT_SCMA_IDX,i_nConn)
      *
      * update CAT_SCMA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_SCMA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CSDESCRI="+cp_ToStrODBC(this.w_CSDESCRI)+;
             ",CSTIPCAT="+cp_ToStrODBC(this.w_CSTIPCAT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_SCMA')
        i_cWhere = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
        UPDATE (i_cTable) SET;
              CSDESCRI=this.w_CSDESCRI;
             ,CSTIPCAT=this.w_CSTIPCAT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAT_SCMA_IDX,i_nConn)
      *
      * delete CAT_SCMA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCSCODICE_1_1.value==this.w_CSCODICE)
      this.oPgFrm.Page1.oPag.oCSCODICE_1_1.value=this.w_CSCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value==this.w_CSDESCRI)
      this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value=this.w_CSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCSTIPCAT_1_5.RadioValue()==this.w_CSTIPCAT)
      this.oPgFrm.Page1.oPag.oCSTIPCAT_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAT_SCMA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(NOT EMPTY(.w_CSCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCSCODICE_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_asmPag1 as StdContainer
  Width  = 378
  height = 105
  stdWidth  = 378
  stdheight = 105
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCSCODICE_1_1 as StdField with uid="XVFHMBOPMH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CSCODICE", cQueryName = "CSCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria sconti / maggiorazioni",;
    HelpContextID = 150335381,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=80, Top=12, InputMask=replicate('X',5)

  func oCSCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT EMPTY(.w_CSCODICE))
    endwith
    return bRes
  endfunc

  add object oCSDESCRI_1_2 as StdField with uid="JOVHSKNVOY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CSDESCRI", cQueryName = "CSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria sconti / maggiorazioni",;
    HelpContextID = 235921297,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=80, Top=41, InputMask=replicate('X',35)


  add object oCSTIPCAT_1_5 as StdCombo with uid="HATPGCPWYZ",rtseq=3,rtrep=.f.,left=80,top=70,width=140,height=21;
    , ToolTipText = "Indica l'archivio in cui � utilizzato";
    , HelpContextID = 238739334;
    , cFormVar="w_CSTIPCAT",RowSource=""+"Categoria cli/for,"+"Categoria articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCSTIPCAT_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oCSTIPCAT_1_5.GetRadio()
    this.Parent.oContained.w_CSTIPCAT = this.RadioValue()
    return .t.
  endfunc

  func oCSTIPCAT_1_5.SetRadio()
    this.Parent.oContained.w_CSTIPCAT=trim(this.Parent.oContained.w_CSTIPCAT)
    this.value = ;
      iif(this.Parent.oContained.w_CSTIPCAT=='C',1,;
      iif(this.Parent.oContained.w_CSTIPCAT=='A',2,;
      0))
  endfunc

  add object oStr_1_3 as StdString with uid="SHGJLAEZMS",Visible=.t., Left=1, Top=12,;
    Alignment=1, Width=77, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="BHUTTNLKAJ",Visible=.t., Left=1, Top=38,;
    Alignment=1, Width=77, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="TKCXLKJJHQ",Visible=.t., Left=1, Top=70,;
    Alignment=1, Width=77, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_asm','CAT_SCMA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CSCODICE=CAT_SCMA.CSCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
