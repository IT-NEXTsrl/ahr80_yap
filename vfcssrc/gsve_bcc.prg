* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcc                                                        *
*              Calcola spese accessorie/scont                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_76]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcc",oParentObject,m.pPARAM)
return(i_retval)

define class tgsve_bcc as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_MERCETOT = 0
  w_SPEACC = 0
  w_NEWSCO = 0
  w_TIPOPE = space(1)
  w_OKWRITE = .f.
  w_OREC = 0
  w_PREC = 0
  w_PRES = 0
  w_RIGEV = 0
  w_MESS = space(1)
  w_ACCSCO = 0
  w_DATCOM = ctod("  /  /  ")
  w_SPEACC_IVA = 0
  w_PADRE = .NULL.
  w_BRIP = .f.
  w_RIGCOMM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola e Ripartisce le Spese Accessorie / Sconti Globali sulle Righe Documento (da GSVE_MDV)
    * --- Calcola: E = Spese Accessorie e Sconti
    this.w_TIPOPE = this.pPARAM
    this.w_PADRE = This.oParentObject
    This.bUpdateParentObject=.F.
    this.w_OKWRITE = .F.
    this.w_PREC = 0
    this.w_PRES = 0
    this.w_SPEACC_IVA = 0
    this.w_SPEACC = 0
    * --- Calcolo Spese di Trasporto e/o Imballo
    * --- Il batch deve essere lanciato come statement, perch� il batch deve essere lanciato da w_PADRE
    GSAR_BST(this.w_PADRE, .F.)
    * --- Totale Spese da Ripartire
    *     Nel caso di Scorporo piede fattura e codice Iva spese presente, calcolo in w_SPEACC_IVA 
    *     la somma delle spese scorporate della loro Iva.
    if Not Empty(this.oParentObject.w_MVIVAINC) 
      if this.oParentObject.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = IIF(this.oParentObject.w_MVFLRINC="S", Calnet( this.oParentObject.w_MVSPEINC, this.oParentObject.w_PEIINC, this.oParentObject.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = IIF(this.oParentObject.w_MVFLRINC="S", this.oParentObject.w_MVSPEINC, 0)
      endif
    else
      this.w_SPEACC = IIF(this.oParentObject.w_MVFLRINC="S", this.oParentObject.w_MVSPEINC, 0)
    endif
    if Not Empty(this.oParentObject.w_MVIVAIMB) 
      if this.oParentObject.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.oParentObject.w_MVFLRIMB="S", Calnet( this.oParentObject.w_MVSPEIMB, this.oParentObject.w_PEIIMB, this.oParentObject.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.oParentObject.w_MVFLRIMB="S", this.oParentObject.w_MVSPEIMB, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.oParentObject.w_MVFLRIMB="S", this.oParentObject.w_MVSPEIMB, 0)
    endif
    if Not Empty(this.oParentObject.w_MVIVATRA) 
      if this.oParentObject.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.oParentObject.w_MVFLRTRA="S", Calnet( this.oParentObject.w_MVSPETRA, this.oParentObject.w_PEITRA, this.oParentObject.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.oParentObject.w_MVFLRTRA="S", this.oParentObject.w_MVSPETRA, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.oParentObject.w_MVFLRTRA="S", this.oParentObject.w_MVSPETRA, 0)
    endif
    if this.oParentObject.w_MVFLVEAC="A"
      * --- Se Acquisti, Ripartisce Spese Bolli e Arrotondamenti
      this.w_SPEACC = this.w_SPEACC+(this.oParentObject.w_MVSPEBOL+this.oParentObject.w_MVIMPARR)
    endif
    this.w_BRIP = .T.
    if Type("this.oParentObject.w_TRIPSPE")<>"U" 
      * --- Se le spese ripartite a seguito della LoadRec (TRIPSPE) il totale delle
      *     spese attualmente ripartite sul dettaglio (TIMPACC+TIMPSCO) e le
      *     spese da ripartire sono tutte a zero non serve svolgere la ripartizione...
      if this.oParentObject.w_TRIPSPE=0 And this.oParentObject.w_TIMPACC=0 And this.oParentObject.w_TIMPSCO=0 And this.w_SPEACC+this.w_SPEACC_IVA=0 And this.oParentObject.w_MVSCONTI=0
        this.w_BRIP = .F.
      endif
    endif
    * --- Totale Righe al netto delle Spese Accessorie
    this.oParentObject.w_TIMPACC = 0
    this.oParentObject.w_TIMPSCO = 0
    this.w_RIGEV = 0
    * --- Se in modifica verifico se le righe sono state rivalorizzate
    *     Se la ripartiazione deve essee svolta.
    if this.w_PADRE.cFunction="Edit" And this.w_BRIP
      * --- Verifico la presenza di righe origine di evasione che siano state rivalorizzate.
      *     La query recupera tutti i documenti di tipo Fattura/note di Credito che abbiano
      *     MVSERRIF=w_MVSERIAL
      *     Il documento di origine deve movimentare la disponibilit� di magazzino
      * --- Select from GSVE_BCC
      do vq_exec with 'GSVE_BCC',this,'_Curs_GSVE_BCC','',.f.,.t.
      if used('_Curs_GSVE_BCC')
        select _Curs_GSVE_BCC
        locate for 1=1
        do while not(eof())
        this.w_RIGEV = Nvl( _Curs_gsve_bcc.CONTA , 0 )
        this.w_RIGCOMM = Nvl( _Curs_gsve_bcc.CONTCOMM , 0 )
          select _Curs_GSVE_BCC
          continue
        enddo
        use
      endif
    endif
    if this.w_RIGEV=0 And not this.w_BRIP And g_COAC="S" And this.oParentObject.w_TDRIPCON="S" And this.w_PADRE.Search( "t_MVRIFCAC>" +Cp_ToStr( 0 ))>0
      * --- Se contributi accessori, e causale che ripartisce sull'articolo il valore dei contributi
      *     allora devo cmq ripartire anche se non ho spese o sconti di piede da ripartire
      *     La ripartizione in questo caso si occuper� solo di aggiungere al valore di riga
      *     dell'articolo il valore dei contributi ad esso associati.
      *     Ricerco quindi sul dettaglio un eventuale contributo non a peso, se presnete devo ripartire.
      this.w_BRIP = .T.
    endif
    if this.w_RIGEV=0 And this.w_BRIP
      this.w_PADRE.MarkPos()     
      this.w_DATCOM = IIF(Empty(this.oParentObject.w_MVDATDOC),w_MVDATREG, this.oParentObject.w_MVDATDOC)
      * --- Segno la posizione sul Temporaneo prima di lanciare il Batch GSAR_BRS il quale cicla sul Trs
      this.w_PADRE.Exec_Select("_Tmp_BCC","Sum( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
      this.w_MERCETOT = Nvl( _Tmp_BCC.ValRig , 0 )
      Use in _Tmp_BCC 
      * --- Lancio il Batch per la ripartizione delle spese e degli sconti
      this.w_MESS = GSAR_BRS(this,this.w_TIPOPE, this.w_PADRE.cTrsName, this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVFLSCOR, this.w_SPEACC, this.oParentObject.w_MVSCONTI, this.w_MERCETOT, this.oParentObject.w_MVCODIVE, this.oParentObject.w_MVCAOVAL, this.w_DATCOM, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.w_SPEACC_IVA , this.w_PADRE, this.oParentObject.w_TDRIPCON)
      * --- Rimetto nel totalizzatore la somma delle spese accessorie
      this.oParentObject.w_TIMPACC = this.w_SPEACC + this.w_SPEACC_IVA
      * --- Rimetto nel totalizzatore il valore degli sconti di piede
      this.oParentObject.w_TIMPSCO = this.oParentObject.w_MVSCONTI
      this.w_PADRE.RePos(.F.)     
      if Not Empty( this.w_MESS )
        ah_ErrorMsg("%1","!","", this.w_MESS)
        this.oParentObject.w_RESCHK = -1
      endif
    else
      if this.w_RIGEV>0 
        if this.w_RIGCOMM<>0
          this.w_MESS = "Sono presenti righe evase che movimentano impegni di commessa%0Impossibile ripartire sconti e spese accessorie"
        else
          this.w_MESS = "Sono presenti righe evase rivalorizzate sul dettaglio%0Impossibile ripartire sconti e spese accessorie"
        endif
        ah_ErrorMsg(this.w_MESS,"!")
        this.oParentObject.w_RESCHK = -1
      endif
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSVE_BCC')
      use in _Curs_GSVE_BCC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
