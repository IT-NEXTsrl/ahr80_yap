* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sfe                                                        *
*              Stampa festivit�                                                *
*                                                                              *
*      Author: TAM Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-04                                                      *
* Last revis.: 2008-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sfe",oParentObject))

* --- Class definition
define class tgsar_sfe as StdForm
  Top    = 18
  Left   = 106

  * --- Standard Properties
  Width  = 513
  Height = 162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-03-13"
  HelpContextID=40466281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  FES_MAST_IDX = 0
  OUT_PUTS_IDX = 0
  cPrg = "gsar_sfe"
  cComment = "Stampa festivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PCODINIZ = space(3)
  w_FEDESINI = space(35)
  w_PCODFINA = space(3)
  w_FEDESFIN = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sfePag1","gsar_sfe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPCODINIZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='FES_MAST'
    this.cWorkTables[2]='OUT_PUTS'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_sfe
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PCODINIZ=space(3)
      .w_FEDESINI=space(35)
      .w_PCODFINA=space(3)
      .w_FEDESFIN=space(35)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PCODINIZ))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_PCODFINA))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCODINIZ
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FES_MAST_IDX,3]
    i_lTable = "FES_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2], .t., this.FES_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCODINIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_MFE',True,'FES_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FECODICE like "+cp_ToStrODBC(trim(this.w_PCODINIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FECODICE',trim(this.w_PCODINIZ))
          select FECODICE,FEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCODINIZ)==trim(_Link_.FECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCODINIZ) and !this.bDontReportError
            deferred_cp_zoom('FES_MAST','*','FECODICE',cp_AbsName(oSource.parent,'oPCODINIZ_1_1'),i_cWhere,'GSTE_MFE',"Festivita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',oSource.xKey(1))
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCODINIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(this.w_PCODINIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',this.w_PCODINIZ)
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCODINIZ = NVL(_Link_.FECODICE,space(3))
      this.w_FEDESINI = NVL(_Link_.FEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PCODINIZ = space(3)
      endif
      this.w_FEDESINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_PCODFINA)) OR  (.w_PCODINIZ<=.w_PCODFINA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PCODINIZ = space(3)
        this.w_FEDESINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])+'\'+cp_ToStr(_Link_.FECODICE,1)
      cp_ShowWarn(i_cKey,this.FES_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCODINIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCODFINA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FES_MAST_IDX,3]
    i_lTable = "FES_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2], .t., this.FES_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCODFINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_MFE',True,'FES_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FECODICE like "+cp_ToStrODBC(trim(this.w_PCODFINA)+"%");

          i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FECODICE',trim(this.w_PCODFINA))
          select FECODICE,FEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCODFINA)==trim(_Link_.FECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCODFINA) and !this.bDontReportError
            deferred_cp_zoom('FES_MAST','*','FECODICE',cp_AbsName(oSource.parent,'oPCODFINA_1_3'),i_cWhere,'GSTE_MFE',"Festivita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',oSource.xKey(1))
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCODFINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(this.w_PCODFINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',this.w_PCODFINA)
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCODFINA = NVL(_Link_.FECODICE,space(3))
      this.w_FEDESFIN = NVL(_Link_.FEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PCODFINA = space(3)
      endif
      this.w_FEDESFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PCODFINA>=.w_PCODINIZ) or (empty(.w_PCODINIZ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PCODFINA = space(3)
        this.w_FEDESFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])+'\'+cp_ToStr(_Link_.FECODICE,1)
      cp_ShowWarn(i_cKey,this.FES_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCODFINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPCODINIZ_1_1.value==this.w_PCODINIZ)
      this.oPgFrm.Page1.oPag.oPCODINIZ_1_1.value=this.w_PCODINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESINI_1_2.value==this.w_FEDESINI)
      this.oPgFrm.Page1.oPag.oFEDESINI_1_2.value=this.w_FEDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPCODFINA_1_3.value==this.w_PCODFINA)
      this.oPgFrm.Page1.oPag.oPCODFINA_1_3.value=this.w_PCODFINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESFIN_1_4.value==this.w_FEDESFIN)
      this.oPgFrm.Page1.oPag.oFEDESFIN_1_4.value=this.w_FEDESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_PCODFINA)) OR  (.w_PCODINIZ<=.w_PCODFINA))  and not(empty(.w_PCODINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCODINIZ_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((.w_PCODFINA>=.w_PCODINIZ) or (empty(.w_PCODINIZ)))  and not(empty(.w_PCODFINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCODFINA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_sfePag1 as StdContainer
  Width  = 509
  height = 162
  stdWidth  = 509
  stdheight = 162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPCODINIZ_1_1 as StdField with uid="ZKMBSSLAIT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PCODINIZ", cQueryName = "PCODINIZ",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Festivit� di inizio selezione",;
    HelpContextID = 261111472,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=117, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="FES_MAST", cZoomOnZoom="GSTE_MFE", oKey_1_1="FECODICE", oKey_1_2="this.w_PCODINIZ"

  func oPCODINIZ_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCODINIZ_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCODINIZ_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FES_MAST','*','FECODICE',cp_AbsName(this.parent,'oPCODINIZ_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_MFE',"Festivita",'',this.parent.oContained
  endproc
  proc oPCODINIZ_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSTE_MFE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FECODICE=this.parent.oContained.w_PCODINIZ
     i_obj.ecpSave()
  endproc

  add object oFEDESINI_1_2 as StdField with uid="CZRQBIZWEE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FEDESINI", cQueryName = "FEDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 66055521,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=168, Top=14, InputMask=replicate('X',35)

  add object oPCODFINA_1_3 as StdField with uid="ALRTVXEXDB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PCODFINA", cQueryName = "PCODFINA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Festivit� di fine selezione",;
    HelpContextID = 79707849,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=117, Top=39, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="FES_MAST", cZoomOnZoom="GSTE_MFE", oKey_1_1="FECODICE", oKey_1_2="this.w_PCODFINA"

  func oPCODFINA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCODFINA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCODFINA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FES_MAST','*','FECODICE',cp_AbsName(this.parent,'oPCODFINA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_MFE',"Festivita",'',this.parent.oContained
  endproc
  proc oPCODFINA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSTE_MFE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FECODICE=this.parent.oContained.w_PCODFINA
     i_obj.ecpSave()
  endproc

  add object oFEDESFIN_1_4 as StdField with uid="BCKZPFLQXJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FEDESFIN", cQueryName = "FEDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 116387164,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=168, Top=39, InputMask=replicate('X',35)


  add object oObj_1_7 as cp_outputCombo with uid="ZLERXIVPWT",left=117, top=86, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137531366


  add object oBtn_1_8 as StdButton with uid="LCDWPTKYIC",left=403, top=113, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 50479638;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="ARSODZEXGY",left=455, top=113, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 50479638;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="ONXAWFDQHT",Visible=.t., Left=8, Top=14,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da festivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YCRKHBCNKW",Visible=.t., Left=23, Top=39,;
    Alignment=1, Width=90, Height=15,;
    Caption="A festivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JNLTWQRLXI",Visible=.t., Left=16, Top=86,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sfe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
