* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bmp                                                        *
*              Manutenzione Pegging commessa                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-16                                                      *
* Last revis.: 2012-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bmp",oParentObject,m.pAzione)
return(i_retval)

define class tgsac_bmp as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_PESERIAL = space(10)
  w_SERIAL = space(10)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CODRIC = space(20)
  w_DESRIC = space(40)
  w_CODODL = space(15)
  w_UNIMIS = space(3)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_QTAABB = 0
  w_QTAESI = 0
  w_ESI = 0
  w_RIS = 0
  w_SERODL = space(15)
  w_ODLORI = space(15)
  * --- WorkFile variables
  MAGAZZIN_idx=0
  KEY_ARTI_idx=0
  CAN_TIER_idx=0
  SALDIART_idx=0
  PEG_SELI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di manutenzione Pegging Commessa (da GSMR_KMP)
    this.Padre = this.oParentObject
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione = "ABB"
        * --- Controlla selezioni
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        do case
          case this.w_nRecSel=0
            ah_ErrorMsg("Non sono stati selezionati elementi da elaborare",48)
          case this.w_nRecSel=1
            * --- Valorizzo le variabili di appoggio (Caller sulla Maschera)
            select(this.NC) 
 locate for xchk=1
            this.w_CODMAG = PECODMAG
            this.w_CODRIC = OLTCODIC
            this.w_CODODL = PEODLORI
            this.w_UNIMIS = ARUNMIS1
            this.w_CODCOM = PECODCOM
            this.w_QTAABB = PEQTAABB
            this.w_SERIAL = PESERIAL
            this.w_SERODL = PESERODL
            this.w_ODLORI = PEODLORI
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDESMAG"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDESMAG;
                from (i_cTable) where;
                    MGCODMAG = this.w_CODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARDESART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODRIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARDESART;
                from (i_cTable) where;
                    ARCODART = this.w_CODRIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESRIC = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from CAN_TIER
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAN_TIER_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CNDESCAN"+;
                " from "+i_cTable+" CAN_TIER where ";
                    +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CNDESCAN;
                from (i_cTable) where;
                    CNCODCAN = this.w_CODCOM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCOM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from SALDIART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SLQTAPER,SLQTRPER"+;
                " from "+i_cTable+" SALDIART where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_CODRIC);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SLQTAPER,SLQTRPER;
                from (i_cTable) where;
                    SLCODICE = this.w_CODRIC;
                    and SLCODMAG = this.w_CODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ESI = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
              this.w_RIS = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_QTAESI = this.w_ESI-this.w_RIS
            * --- Lancio la maschera di Abbinamento
            do gsac_kab with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          otherwise
            ah_ErrorMsg("E' possibile abbinare una sola riga per volta",48)
        endcase
        * --- Riesegue l'interrogazione
        this.Padre.NotifyEvent("Interroga")     
      case this.pAzione = "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "INTERROGA"
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione = "SEP"
        * --- Controlla selezioni
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        do case
          case this.w_nRecSel=0
            ah_ErrorMsg("Non sono stati selezionati elementi da elaborare",48)
          otherwise
            * --- Elimino l'abbinamento
            select(this.NC) 
 scan for xchk=1
            this.w_PESERIAL = PESERIAL
            * --- Delete from PEG_SELI
            i_nConn=i_TableProp[this.PEG_SELI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PESERIAL = "+cp_ToStrODBC(this.w_PESERIAL);
                     )
            else
              delete from (i_cTable) where;
                    PESERIAL = this.w_PESERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            endscan
        endcase
        * --- Riesegue l'interrogazione
        this.Padre.NotifyEvent("Interroga")     
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='PEG_SELI'
    this.cWorkTables[6]='ART_ICOL'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
