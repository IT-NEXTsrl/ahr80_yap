* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aim                                                        *
*              Saggio interessi di mora                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-17                                                      *
* Last revis.: 2008-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aim"))

* --- Class definition
define class tgsar_aim as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 355
  Height = 176+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-11-05"
  HelpContextID=259426455
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  INT_MORA_IDX = 0
  cFile = "INT_MORA"
  cKeySelect = "IM__ANNO,IMPERIOD"
  cKeyWhere  = "IM__ANNO=this.w_IM__ANNO and IMPERIOD=this.w_IMPERIOD"
  cKeyWhereODBC = '"IM__ANNO="+cp_ToStrODBC(this.w_IM__ANNO)';
      +'+" and IMPERIOD="+cp_ToStrODBC(this.w_IMPERIOD)';

  cKeyWhereODBCqualified = '"INT_MORA.IM__ANNO="+cp_ToStrODBC(this.w_IM__ANNO)';
      +'+" and INT_MORA.IMPERIOD="+cp_ToStrODBC(this.w_IMPERIOD)';

  cPrg = "gsar_aim"
  cComment = "Saggio interessi di mora"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IM__ANNO = space(4)
  o_IM__ANNO = space(4)
  w_IMPERIOD = space(1)
  o_IMPERIOD = space(1)
  w_IMDATINI = ctod('  /  /  ')
  w_IMDATFIN = ctod('  /  /  ')
  w_IMSAGRIF = 0
  w_IMMAGNDP = 0
  w_IMMAGDEP = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INT_MORA','gsar_aim')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_aimPag1","gsar_aim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saggio")
      .Pages(1).HelpContextID = 176922842
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIM__ANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INT_MORA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INT_MORA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INT_MORA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_IM__ANNO = NVL(IM__ANNO,space(4))
      .w_IMPERIOD = NVL(IMPERIOD,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INT_MORA where IM__ANNO=KeySet.IM__ANNO
    *                            and IMPERIOD=KeySet.IMPERIOD
    *
    i_nConn = i_TableProp[this.INT_MORA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INT_MORA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INT_MORA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INT_MORA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IM__ANNO',this.w_IM__ANNO  ,'IMPERIOD',this.w_IMPERIOD  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_IM__ANNO = NVL(IM__ANNO,space(4))
        .w_IMPERIOD = NVL(IMPERIOD,space(1))
        .w_IMDATINI = NVL(cp_ToDate(IMDATINI),ctod("  /  /  "))
        .w_IMDATFIN = NVL(cp_ToDate(IMDATFIN),ctod("  /  /  "))
        .w_IMSAGRIF = NVL(IMSAGRIF,0)
        .w_IMMAGNDP = NVL(IMMAGNDP,0)
        .w_IMMAGDEP = NVL(IMMAGDEP,0)
        cp_LoadRecExtFlds(this,'INT_MORA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IM__ANNO = space(4)
      .w_IMPERIOD = space(1)
      .w_IMDATINI = ctod("  /  /  ")
      .w_IMDATFIN = ctod("  /  /  ")
      .w_IMSAGRIF = 0
      .w_IMMAGNDP = 0
      .w_IMMAGDEP = 0
      if .cFunction<>"Filter"
        .w_IM__ANNO = g_CODESE
        .w_IMPERIOD = 'P'
        .w_IMDATINI = cp_CharToDate(IIF(.w_IMPERIOD='P', '01/01/', '01/07/')+.w_IM__ANNO)
        .w_IMDATFIN = cp_CharToDate(IIF(.w_IMPERIOD='P', '30/06/', '31/12/')+.w_IM__ANNO)
      endif
    endwith
    cp_BlankRecExtFlds(this,'INT_MORA')
    this.DoRTCalc(5,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIM__ANNO_1_1.enabled = i_bVal
      .Page1.oPag.oIMPERIOD_1_3.enabled = i_bVal
      .Page1.oPag.oIMSAGRIF_1_8.enabled = i_bVal
      .Page1.oPag.oIMMAGNDP_1_10.enabled = i_bVal
      .Page1.oPag.oIMMAGDEP_1_12.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIM__ANNO_1_1.enabled = .f.
        .Page1.oPag.oIMPERIOD_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIM__ANNO_1_1.enabled = .t.
        .Page1.oPag.oIMPERIOD_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INT_MORA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INT_MORA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IM__ANNO,"IM__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMPERIOD,"IMPERIOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDATINI,"IMDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDATFIN,"IMDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMSAGRIF,"IMSAGRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMMAGNDP,"IMMAGNDP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMMAGDEP,"IMMAGDEP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INT_MORA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
    i_lTable = "INT_MORA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INT_MORA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INT_MORA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INT_MORA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INT_MORA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INT_MORA')
        i_extval=cp_InsertValODBCExtFlds(this,'INT_MORA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IM__ANNO,IMPERIOD,IMDATINI,IMDATFIN,IMSAGRIF"+;
                  ",IMMAGNDP,IMMAGDEP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IM__ANNO)+;
                  ","+cp_ToStrODBC(this.w_IMPERIOD)+;
                  ","+cp_ToStrODBC(this.w_IMDATINI)+;
                  ","+cp_ToStrODBC(this.w_IMDATFIN)+;
                  ","+cp_ToStrODBC(this.w_IMSAGRIF)+;
                  ","+cp_ToStrODBC(this.w_IMMAGNDP)+;
                  ","+cp_ToStrODBC(this.w_IMMAGDEP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INT_MORA')
        i_extval=cp_InsertValVFPExtFlds(this,'INT_MORA')
        cp_CheckDeletedKey(i_cTable,0,'IM__ANNO',this.w_IM__ANNO,'IMPERIOD',this.w_IMPERIOD)
        INSERT INTO (i_cTable);
              (IM__ANNO,IMPERIOD,IMDATINI,IMDATFIN,IMSAGRIF,IMMAGNDP,IMMAGDEP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IM__ANNO;
                  ,this.w_IMPERIOD;
                  ,this.w_IMDATINI;
                  ,this.w_IMDATFIN;
                  ,this.w_IMSAGRIF;
                  ,this.w_IMMAGNDP;
                  ,this.w_IMMAGDEP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INT_MORA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INT_MORA_IDX,i_nConn)
      *
      * update INT_MORA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INT_MORA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IMDATINI="+cp_ToStrODBC(this.w_IMDATINI)+;
             ",IMDATFIN="+cp_ToStrODBC(this.w_IMDATFIN)+;
             ",IMSAGRIF="+cp_ToStrODBC(this.w_IMSAGRIF)+;
             ",IMMAGNDP="+cp_ToStrODBC(this.w_IMMAGNDP)+;
             ",IMMAGDEP="+cp_ToStrODBC(this.w_IMMAGDEP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INT_MORA')
        i_cWhere = cp_PKFox(i_cTable  ,'IM__ANNO',this.w_IM__ANNO  ,'IMPERIOD',this.w_IMPERIOD  )
        UPDATE (i_cTable) SET;
              IMDATINI=this.w_IMDATINI;
             ,IMDATFIN=this.w_IMDATFIN;
             ,IMSAGRIF=this.w_IMSAGRIF;
             ,IMMAGNDP=this.w_IMMAGNDP;
             ,IMMAGDEP=this.w_IMMAGDEP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INT_MORA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INT_MORA_IDX,i_nConn)
      *
      * delete INT_MORA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IM__ANNO',this.w_IM__ANNO  ,'IMPERIOD',this.w_IMPERIOD  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INT_MORA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INT_MORA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_IM__ANNO<>.w_IM__ANNO.or. .o_IMPERIOD<>.w_IMPERIOD
            .w_IMDATINI = cp_CharToDate(IIF(.w_IMPERIOD='P', '01/01/', '01/07/')+.w_IM__ANNO)
        endif
        if .o_IM__ANNO<>.w_IM__ANNO.or. .o_IMPERIOD<>.w_IMPERIOD
            .w_IMDATFIN = cp_CharToDate(IIF(.w_IMPERIOD='P', '30/06/', '31/12/')+.w_IM__ANNO)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oIMMAGDEP_1_12.visible=!this.oPgFrm.Page1.oPag.oIMMAGDEP_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIM__ANNO_1_1.value==this.w_IM__ANNO)
      this.oPgFrm.Page1.oPag.oIM__ANNO_1_1.value=this.w_IM__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPERIOD_1_3.RadioValue()==this.w_IMPERIOD)
      this.oPgFrm.Page1.oPag.oIMPERIOD_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDATINI_1_4.value==this.w_IMDATINI)
      this.oPgFrm.Page1.oPag.oIMDATINI_1_4.value=this.w_IMDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDATFIN_1_6.value==this.w_IMDATFIN)
      this.oPgFrm.Page1.oPag.oIMDATFIN_1_6.value=this.w_IMDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oIMSAGRIF_1_8.value==this.w_IMSAGRIF)
      this.oPgFrm.Page1.oPag.oIMSAGRIF_1_8.value=this.w_IMSAGRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oIMMAGNDP_1_10.value==this.w_IMMAGNDP)
      this.oPgFrm.Page1.oPag.oIMMAGNDP_1_10.value=this.w_IMMAGNDP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMMAGDEP_1_12.value==this.w_IMMAGDEP)
      this.oPgFrm.Page1.oPag.oIMMAGDEP_1_12.value=this.w_IMMAGDEP
    endif
    cp_SetControlsValueExtFlds(this,'INT_MORA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_IM__ANNO)>1900)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIM__ANNO_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IM__ANNO = this.w_IM__ANNO
    this.o_IMPERIOD = this.w_IMPERIOD
    return

enddefine

* --- Define pages as container
define class tgsar_aimPag1 as StdContainer
  Width  = 351
  height = 176
  stdWidth  = 351
  stdheight = 176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIM__ANNO_1_1 as StdField with uid="CEKWSBNJBN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IM__ANNO", cQueryName = "IM__ANNO",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno",;
    HelpContextID = 32230101,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=143, Top=17, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oIM__ANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_IM__ANNO)>1900)
    endwith
    return bRes
  endfunc


  add object oIMPERIOD_1_3 as StdCombo with uid="FCNCASMTBS",rtseq=2,rtrep=.f.,left=191,top=17,width=132,height=21;
    , ToolTipText = "Codice semestre (primo/secondo)";
    , HelpContextID = 232839882;
    , cFormVar="w_IMPERIOD",RowSource=""+"Primo semestre,"+"Secondo semestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIMPERIOD_1_3.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oIMPERIOD_1_3.GetRadio()
    this.Parent.oContained.w_IMPERIOD = this.RadioValue()
    return .t.
  endfunc

  func oIMPERIOD_1_3.SetRadio()
    this.Parent.oContained.w_IMPERIOD=trim(this.Parent.oContained.w_IMPERIOD)
    this.value = ;
      iif(this.Parent.oContained.w_IMPERIOD=='P',1,;
      iif(this.Parent.oContained.w_IMPERIOD=='S',2,;
      0))
  endfunc

  add object oIMDATINI_1_4 as StdField with uid="MESRQUHPWZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IMDATINI", cQueryName = "IMDATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio semestre",;
    HelpContextID = 234625743,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=49

  add object oIMDATFIN_1_6 as StdField with uid="FZORSMNZDK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMDATFIN", cQueryName = "IMDATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine semestre",;
    HelpContextID = 184294100,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=247, Top=49

  add object oIMSAGRIF_1_8 as StdField with uid="ATEVAFACKE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IMSAGRIF", cQueryName = "IMSAGRIF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saggio rifinanziamento della B.C.E.",;
    HelpContextID = 103615180,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=268, Top=83, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oIMMAGNDP_1_10 as StdField with uid="XUYJDFOXGJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IMMAGNDP", cQueryName = "IMMAGNDP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione prodotti non deperibili",;
    HelpContextID = 231953706,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=268, Top=117, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oIMMAGDEP_1_12 as StdField with uid="VOVPRATDRK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMMAGDEP", cQueryName = "IMMAGDEP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione prodotti deperibili",;
    HelpContextID = 131290410,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=268, Top=151, cSayPict='"999.99"', cGetPict='"999.99"'

  func oIMMAGDEP_1_12.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="LTTMEUHQFV",Visible=.t., Left=95, Top=21,;
    Alignment=1, Width=45, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="VXWZSQPWAW",Visible=.t., Left=106, Top=53,;
    Alignment=1, Width=34, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RXPAZRFUZC",Visible=.t., Left=226, Top=53,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="YMLEKPXMVY",Visible=.t., Left=26, Top=87,;
    Alignment=1, Width=238, Height=18,;
    Caption="Saggio rifinanziamento della B.C.E.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BOVREDJHNU",Visible=.t., Left=26, Top=121,;
    Alignment=1, Width=238, Height=18,;
    Caption="% Maggiorazione prodotti non deperibili:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="KYRQWVECSL",Visible=.t., Left=26, Top=155,;
    Alignment=1, Width=238, Height=18,;
    Caption="% Maggiorazione prodotti deperibili:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="KESPGJBOHE",Visible=.t., Left=181, Top=121,;
    Alignment=1, Width=83, Height=18,;
    Caption="Maggiorazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aim','INT_MORA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IM__ANNO=INT_MORA.IM__ANNO";
  +" and "+i_cAliasName2+".IMPERIOD=INT_MORA.IMPERIOD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
