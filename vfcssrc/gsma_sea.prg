* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sea                                                        *
*              Elaborazione analisi ABC                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_64]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2014-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sea",oParentObject))

* --- Class definition
define class tgsma_sea as StdForm
  Top    = 26
  Left   = 39

  * --- Standard Properties
  Width  = 606
  Height = 181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-04-14"
  HelpContextID=211125399
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsma_sea"
  cComment = "Elaborazione analisi ABC"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ESE = space(4)
  w_INV = space(6)
  w_DESC = ctod('  /  /  ')
  w_VALORIZ = space(1)
  w_A = 0
  w_B = 0
  w_C = 0
  w_ELAB = space(1)
  w_FLESCSZ = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_seaPag1","gsma_sea",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='ESERCIZI'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ESE=space(4)
      .w_INV=space(6)
      .w_DESC=ctod("  /  /  ")
      .w_VALORIZ=space(1)
      .w_A=0
      .w_B=0
      .w_C=0
      .w_ELAB=space(1)
      .w_FLESCSZ=space(1)
        .w_AZIENDA = i_codazi
          .DoRTCalc(2,3,.f.)
        .w_ESE = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ESE))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_INV))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_VALORIZ = 'A'
        .w_A = 50
        .w_B = 30
        .w_C = 100-.w_A-.w_B
        .w_ELAB = 'A'
        .w_FLESCSZ = ' '
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
            .w_C = 100-.w_A-.w_B
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_4'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INV
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESE;
                     ,'INNUMINV',trim(this.w_INV))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINV_1_5'),i_cWhere,'gsma_ain',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESE;
                       ,'INNUMINV',this.w_INV)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INV = NVL(_Link_.INNUMINV,space(6))
      this.w_DESC = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_INV = space(6)
      endif
      this.w_DESC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_4.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_4.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINV_1_5.value==this.w_INV)
      this.oPgFrm.Page1.oPag.oINV_1_5.value=this.w_INV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_1_6.value==this.w_DESC)
      this.oPgFrm.Page1.oPag.oDESC_1_6.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIZ_1_7.RadioValue()==this.w_VALORIZ)
      this.oPgFrm.Page1.oPag.oVALORIZ_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oA_1_8.value==this.w_A)
      this.oPgFrm.Page1.oPag.oA_1_8.value=this.w_A
    endif
    if not(this.oPgFrm.Page1.oPag.oB_1_9.value==this.w_B)
      this.oPgFrm.Page1.oPag.oB_1_9.value=this.w_B
    endif
    if not(this.oPgFrm.Page1.oPag.oC_1_10.value==this.w_C)
      this.oPgFrm.Page1.oPag.oC_1_10.value=this.w_C
    endif
    if not(this.oPgFrm.Page1.oPag.oELAB_1_11.RadioValue()==this.w_ELAB)
      this.oPgFrm.Page1.oPag.oELAB_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESCSZ_1_12.RadioValue()==this.w_FLESCSZ)
      this.oPgFrm.Page1.oPag.oFLESCSZ_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINV_1_5.SetFocus()
            i_bnoObbl = !empty(.w_INV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_A>=0 and .w_A+.w_B<=100)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_B)) or not(.w_B>=0 and .w_A+.w_B<=100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oB_1_9.SetFocus()
            i_bnoObbl = !empty(.w_B)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_seaPag1 as StdContainer
  Width  = 602
  height = 181
  stdWidth  = 602
  stdheight = 181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_4 as StdField with uid="BWADSONNJM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 211430470,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=65, Top=39, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_INV)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"",'',this.parent.oContained
  endproc
  proc oESE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_ESE
     i_obj.ecpSave()
  endproc

  add object oINV_1_5 as StdField with uid="SHLRNDQDJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INV", cQueryName = "INV",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero inventario",;
    HelpContextID = 211498886,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=182, Top=39, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_INV"

  func oINV_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oINV_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINV_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINV_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventari",'',this.parent.oContained
  endproc
  proc oINV_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESE
     i_obj.w_INNUMINV=this.parent.oContained.w_INV
     i_obj.ecpSave()
  endproc

  add object oDESC_1_6 as StdField with uid="RPTVTRTEPF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 215875126,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=361, Top=39


  add object oVALORIZ_1_7 as StdCombo with uid="ELAIRNREFA",rtseq=7,rtrep=.f.,left=445,top=39,width=152,height=21;
    , ToolTipText = "Valorizzazione inventario";
    , HelpContextID = 185174870;
    , cFormVar="w_VALORIZ",RowSource=""+"Costo medio pond. anno,"+"Costo ultimo,"+"Costo standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVALORIZ_1_7.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oVALORIZ_1_7.GetRadio()
    this.Parent.oContained.w_VALORIZ = this.RadioValue()
    return .t.
  endfunc

  func oVALORIZ_1_7.SetRadio()
    this.Parent.oContained.w_VALORIZ=trim(this.Parent.oContained.w_VALORIZ)
    this.value = ;
      iif(this.Parent.oContained.w_VALORIZ=='A',1,;
      iif(this.Parent.oContained.w_VALORIZ=='B',2,;
      iif(this.Parent.oContained.w_VALORIZ=='C',3,;
      0)))
  endfunc

  add object oA_1_8 as StdField with uid="VLTULUNVTO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_A", cQueryName = "A",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211126534,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=71, Top=88, cSayPict='"999.99"', cGetPict='"999.99"'

  func oA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_A>=0 and .w_A+.w_B<=100)
    endwith
    return bRes
  endfunc

  add object oB_1_9 as StdField with uid="WOYXYRHXBV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_B", cQueryName = "B",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211126550,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=71, Top=107, cSayPict='"999.99"', cGetPict='"999.99"'

  func oB_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_B>=0 and .w_A+.w_B<=100)
    endwith
    return bRes
  endfunc

  add object oC_1_10 as StdField with uid="VIURYGLNBG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_C", cQueryName = "C",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211126566,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=71, Top=126, cSayPict='"999.99"', cGetPict='"999.99"'


  add object oELAB_1_11 as StdCombo with uid="LYTCVWBOCI",rtseq=11,rtrep=.f.,left=277,top=126,width=152,height=21;
    , ToolTipText = "Singolo articolo o categoria omogenea";
    , HelpContextID = 215737670;
    , cFormVar="w_ELAB",RowSource=""+"Categ. omogenea,"+"Singolo articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oELAB_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oELAB_1_11.GetRadio()
    this.Parent.oContained.w_ELAB = this.RadioValue()
    return .t.
  endfunc

  func oELAB_1_11.SetRadio()
    this.Parent.oContained.w_ELAB=trim(this.Parent.oContained.w_ELAB)
    this.value = ;
      iif(this.Parent.oContained.w_ELAB=='C',1,;
      iif(this.Parent.oContained.w_ELAB=='A',2,;
      0))
  endfunc

  add object oFLESCSZ_1_12 as StdCheck with uid="NMPGSOAONV",rtseq=12,rtrep=.f.,left=71, top=154, caption="Esclude articoli con costo a zero",;
    ToolTipText = "Se attivo esclude dall' elaborazione gli articoli che hanno costo a zero",;
    HelpContextID = 69018966,;
    cFormVar="w_FLESCSZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLESCSZ_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLESCSZ_1_12.GetRadio()
    this.Parent.oContained.w_FLESCSZ = this.RadioValue()
    return .t.
  endfunc

  func oFLESCSZ_1_12.SetRadio()
    this.Parent.oContained.w_FLESCSZ=trim(this.Parent.oContained.w_FLESCSZ)
    this.value = ;
      iif(this.Parent.oContained.w_FLESCSZ=='S',1,;
      0)
  endfunc


  add object oBtn_1_13 as StdButton with uid="DSTCOLPZCF",left=493, top=131, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 211154150;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSMA_BEA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_INV) and not empty(.w_ESE))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="JDQDZDTMTV",left=547, top=131, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 218442822;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_29 as cp_runprogram with uid="TETQZJSSRK",left=78, top=190, width=135,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BEI",;
    cEvent = "w_ESE Changed",;
    nPag=1;
    , HelpContextID = 147747866

  add object oStr_1_15 as StdString with uid="IBDXLMDQKP",Visible=.t., Left=5, Top=8,;
    Alignment=0, Width=332, Height=15,;
    Caption="Inventario"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TKXTHIXIHT",Visible=.t., Left=445, Top=8,;
    Alignment=0, Width=147, Height=15,;
    Caption="Valorizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="RMESOHUDXD",Visible=.t., Left=159, Top=126,;
    Alignment=1, Width=115, Height=15,;
    Caption="Elaborazione per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="XAKTCOHYFM",Visible=.t., Left=3, Top=39,;
    Alignment=1, Width=60, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ROEDXDMVHD",Visible=.t., Left=121, Top=39,;
    Alignment=1, Width=59, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="GMCZVAREIF",Visible=.t., Left=254, Top=39,;
    Alignment=1, Width=105, Height=15,;
    Caption="Movimenti fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="GBYYPBKTAU",Visible=.t., Left=4, Top=88,;
    Alignment=1, Width=65, Height=15,;
    Caption="Classe A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JAZXQNJUOU",Visible=.t., Left=4, Top=107,;
    Alignment=1, Width=65, Height=15,;
    Caption="Classe B:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PJZUTXDMLY",Visible=.t., Left=4, Top=126,;
    Alignment=1, Width=65, Height=15,;
    Caption="Classe C:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KKEZTGZZZH",Visible=.t., Left=10, Top=69,;
    Alignment=0, Width=218, Height=15,;
    Caption="Percentuali di ripartizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="YWVIFAGLAP",Visible=.t., Left=127, Top=126,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WCLYVCUVTH",Visible=.t., Left=127, Top=107,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MGAHNPVVLT",Visible=.t., Left=127, Top=88,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="KFSKOLVUOF",left=5, top=26, width=590,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sea','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
