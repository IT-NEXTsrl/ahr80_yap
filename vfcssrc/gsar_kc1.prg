* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kc1                                                        *
*              Aggiornamento massivo intestatari                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-13                                                      *
* Last revis.: 2016-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kc1",oParentObject))

* --- Class definition
define class tgsar_kc1 as StdForm
  Top    = 6
  Left   = 5

  * --- Standard Properties
  Width  = 797
  Height = 535+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-14"
  HelpContextID=169248919
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  AGENTI_IDX = 0
  CACOCLFO_IDX = 0
  CACOCLFO_IDX = 0
  CATECOMM_IDX = 0
  CATMCONT_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  SISMCOLL_IDX = 0
  TIPCONTR_IDX = 0
  ZONE_IDX = 0
  CAT_FINA_IDX = 0
  cPrg = "gsar_kc1"
  cComment = "Aggiornamento massivo intestatari"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPAGG = space(2)
  o_TIPAGG = space(2)
  w_TIPANA = space(1)
  o_TIPANA = space(1)
  w_ANTIPCON = space(1)
  w_ANAGPUBL = space(1)
  w_NOCONTR = space(1)
  w_INICOD = space(15)
  w_FINCOD = space(15)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_SELEZI = space(1)
  w_STAMPA = space(1)
  w_EMAIL = space(1)
  w_EMPEC = space(1)
  w_CPZ = space(1)
  w_POSTEL = space(1)
  w_FAX = space(1)
  w_WE = space(1)
  w_FIRDIG = space(1)
  w_FLGCPZ = space(1)
  w_FLAPCA = space(1)
  w_CODCUC = space(2)
  w_MCTIPCAT = space(15)
  o_MCTIPCAT = space(15)
  w_MCCODSIS = space(15)
  o_MCCODSIS = space(15)
  w_MCCODCAT = space(5)
  w_MCPERAPP = 0
  o_MCPERAPP = 0
  w_MCFLARID = space(1)
  w_MCDATATT = ctod('  /  /  ')
  w_MCDATSCA = ctod('  /  /  ')
  w_DESCCAT = space(40)
  w_TPDESCRI = space(40)
  w_TPAPPPES = space(1)
  o_TPAPPPES = space(1)
  w_SCDESCRI = space(40)
  w_MC__PRNT = space(254)
  w_CGCONTRI = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_CATCOM = space(3)
  w_CODAGE = space(5)
  w_CODZON = space(3)
  w_NAZION = space(3)
  w_CATCON = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CATCOMD = space(35)
  w_DESAGENTE = space(35)
  w_DESZON = space(35)
  w_DESNAZ = space(35)
  w_DESCCC = space(35)
  w_GRUPPO = space(35)
  w_PIVAVUOTA = space(1)
  w_CFISVUOTA = space(1)
  w_ANCATFIN = space(5)
  w_TIPOFIN = space(1)
  w_CFDESCRIC = space(30)
  w_ANCATDER = space(5)
  w_TIPOFIND = space(1)
  w_CFDESCRID = space(30)
  w_ANCATRAT = space(5)
  w_TIPOFINR = space(1)
  w_CFDESCRIR = space(30)
  w_TIPAGG = space(2)
  w_ZOOM = .NULL.
  w_outputCombo = .NULL.
  w_ZOOM1 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kc1Pag1","gsar_kc1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Abilitazioni")
      .Pages(2).addobject("oPag","tgsar_kc1Pag2","gsar_kc1",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPAGG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    this.w_outputCombo = this.oPgFrm.Pages(1).oPag.outputCombo
    this.w_ZOOM1 = this.oPgFrm.Pages(1).oPag.ZOOM1
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      this.w_outputCombo = .NULL.
      this.w_ZOOM1 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='CACOCLFO'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='CATECOMM'
    this.cWorkTables[5]='CATMCONT'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='NAZIONI'
    this.cWorkTables[8]='SISMCOLL'
    this.cWorkTables[9]='TIPCONTR'
    this.cWorkTables[10]='ZONE'
    this.cWorkTables[11]='CAT_FINA'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAR_BK1(this,"G")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPAGG=space(2)
      .w_TIPANA=space(1)
      .w_ANTIPCON=space(1)
      .w_ANAGPUBL=space(1)
      .w_NOCONTR=space(1)
      .w_INICOD=space(15)
      .w_FINCOD=space(15)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_SELEZI=space(1)
      .w_STAMPA=space(1)
      .w_EMAIL=space(1)
      .w_EMPEC=space(1)
      .w_CPZ=space(1)
      .w_POSTEL=space(1)
      .w_FAX=space(1)
      .w_WE=space(1)
      .w_FIRDIG=space(1)
      .w_FLGCPZ=space(1)
      .w_FLAPCA=space(1)
      .w_CODCUC=space(2)
      .w_MCTIPCAT=space(15)
      .w_MCCODSIS=space(15)
      .w_MCCODCAT=space(5)
      .w_MCPERAPP=0
      .w_MCFLARID=space(1)
      .w_MCDATATT=ctod("  /  /  ")
      .w_MCDATSCA=ctod("  /  /  ")
      .w_DESCCAT=space(40)
      .w_TPDESCRI=space(40)
      .w_TPAPPPES=space(1)
      .w_SCDESCRI=space(40)
      .w_MC__PRNT=space(254)
      .w_CGCONTRI=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_CODAGE=space(5)
      .w_CODZON=space(3)
      .w_NAZION=space(3)
      .w_CATCON=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CATCOMD=space(35)
      .w_DESAGENTE=space(35)
      .w_DESZON=space(35)
      .w_DESNAZ=space(35)
      .w_DESCCC=space(35)
      .w_GRUPPO=space(35)
      .w_PIVAVUOTA=space(1)
      .w_CFISVUOTA=space(1)
      .w_ANCATFIN=space(5)
      .w_TIPOFIN=space(1)
      .w_CFDESCRIC=space(30)
      .w_ANCATDER=space(5)
      .w_TIPOFIND=space(1)
      .w_CFDESCRID=space(30)
      .w_ANCATRAT=space(5)
      .w_TIPOFINR=space(1)
      .w_CFDESCRIR=space(30)
      .w_TIPAGG=space(2)
        .w_TIPAGG = this.oParentObject
        .w_TIPANA = 'C'
        .w_ANTIPCON = .w_TIPANA
        .w_ANAGPUBL = 'T'
        .w_NOCONTR = 'T'
        .w_INICOD = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_INICOD))
          .link_1_7('Full')
        endif
        .w_FINCOD = SPACE(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_FINCOD))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOM.Calculate(.w_TIPANA)
          .DoRTCalc(8,9,.f.)
        .w_SELEZI = 'D'
        .w_STAMPA = 'A'
        .w_EMAIL = 'A'
        .w_EMPEC = 'A'
        .w_CPZ = 'A'
        .w_POSTEL = 'A'
        .w_FAX = 'A'
        .w_WE = 'A'
        .w_FIRDIG = 'A'
        .w_FLGCPZ = 'S'
        .w_FLAPCA = iif(.w_TIPAGG='C','S','N')
        .w_CODCUC = '00'
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MCTIPCAT))
          .link_1_28('Full')
        endif
        .w_MCCODSIS = SPACE( 15 )
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MCCODSIS))
          .link_1_29('Full')
        endif
        .w_MCCODCAT = SPACE( 5 )
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MCCODCAT))
          .link_1_30('Full')
        endif
        .w_MCPERAPP = 100
        .w_MCFLARID = IIF( .w_MCPERAPP < 100 , IIF(.w_TPAPPPES<>"S","I","Q") , "N" )
        .w_MCDATATT = date(year(i_datsys),1,1)
        .w_MCDATSCA = date(year(i_datsys),12,31)
          .DoRTCalc(29,33,.f.)
        .w_CGCONTRI = .w_MCTIPCAT
      .oPgFrm.Page1.oPag.outputCombo.Calculate()
      .oPgFrm.Page1.oPag.ZOOM1.Calculate(.w_TIPANA)
        .DoRTCalc(35,36,.f.)
        if not(empty(.w_CATCOM))
          .link_2_1('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CODAGE))
          .link_2_2('Full')
        endif
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_CODZON))
          .link_2_3('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_NAZION))
          .link_2_4('Full')
        endif
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CATCON))
          .link_2_5('Full')
        endif
        .w_OBTEST = i_DATSYS
          .DoRTCalc(42,48,.f.)
        .w_PIVAVUOTA = 'N'
        .w_CFISVUOTA = 'N'
        .w_ANCATFIN = Space(5)
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_ANCATFIN))
          .link_1_70('Full')
        endif
        .w_TIPOFIN = 'C'
          .DoRTCalc(53,53,.f.)
        .w_ANCATDER = Space(5)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_ANCATDER))
          .link_1_73('Full')
        endif
        .w_TIPOFIND = 'D'
          .DoRTCalc(56,56,.f.)
        .w_ANCATRAT = Space(5)
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_ANCATRAT))
          .link_1_76('Full')
        endif
        .w_TIPOFINR = 'R'
          .DoRTCalc(59,59,.f.)
        .w_TIPAGG = this.oParentObject
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPAGG<>.w_TIPAGG
            .w_TIPANA = 'C'
        endif
        if .o_TIPANA<>.w_TIPANA
            .w_ANTIPCON = .w_TIPANA
        endif
        if .o_TIPAGG<>.w_TIPAGG
            .w_ANAGPUBL = 'T'
        endif
        if .o_TIPAGG<>.w_TIPAGG
            .w_NOCONTR = 'T'
        endif
        if .o_TIPAGG<>.w_TIPAGG
            .w_INICOD = SPACE(15)
          .link_1_7('Full')
        endif
        if .o_TIPAGG<>.w_TIPAGG
            .w_FINCOD = SPACE(15)
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOM.Calculate(.w_TIPANA)
        .DoRTCalc(8,9,.t.)
        if .o_TIPANA<>.w_TIPANA.or. .o_TIPAGG<>.w_TIPAGG
            .w_SELEZI = 'D'
        endif
        .DoRTCalc(11,19,.t.)
        if .o_TIPAGG<>.w_TIPAGG
            .w_FLAPCA = iif(.w_TIPAGG='C','S','N')
        endif
        .DoRTCalc(21,22,.t.)
        if .o_MCTIPCAT<>.w_MCTIPCAT
            .w_MCCODSIS = SPACE( 15 )
          .link_1_29('Full')
        endif
        if .o_MCTIPCAT<>.w_MCTIPCAT.or. .o_MCCODSIS<>.w_MCCODSIS
            .w_MCCODCAT = SPACE( 5 )
          .link_1_30('Full')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_TPAPPPES<>.w_TPAPPPES.or. .o_MCPERAPP<>.w_MCPERAPP
            .w_MCFLARID = IIF( .w_MCPERAPP < 100 , IIF(.w_TPAPPPES<>"S","I","Q") , "N" )
        endif
        .DoRTCalc(27,33,.t.)
        if .o_MCTIPCAT<>.w_MCTIPCAT
            .w_CGCONTRI = .w_MCTIPCAT
        endif
        .oPgFrm.Page1.oPag.outputCombo.Calculate()
        .oPgFrm.Page1.oPag.ZOOM1.Calculate(.w_TIPANA)
        .DoRTCalc(35,40,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(42,50,.t.)
        if .o_TIPAGG<>.w_TIPAGG
            .w_ANCATFIN = Space(5)
          .link_1_70('Full')
        endif
        .DoRTCalc(52,53,.t.)
        if .o_TIPAGG<>.w_TIPAGG
            .w_ANCATDER = Space(5)
          .link_1_73('Full')
        endif
        .DoRTCalc(55,56,.t.)
        if .o_TIPAGG<>.w_TIPAGG
            .w_ANCATRAT = Space(5)
          .link_1_76('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(58,60,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate(.w_TIPANA)
        .oPgFrm.Page1.oPag.outputCombo.Calculate()
        .oPgFrm.Page1.oPag.ZOOM1.Calculate(.w_TIPANA)
    endwith
  return

  proc Calculate_FLLJXTPGGE()
    with this
          * --- Selezione / deselezione
          GSAR_BK1(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_PFCCEHDKCU()
    with this
          * --- Nasconde oggetto stampa
          GSAR_BK1(this;
              ,'H';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPANA_1_3.enabled = this.oPgFrm.Page1.oPag.oTIPANA_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCPZ_1_19.enabled = this.oPgFrm.Page1.oPag.oCPZ_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.enabled = this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.mCond()
    this.oPgFrm.Page1.oPag.oMCFLARID_1_32.enabled = this.oPgFrm.Page1.oPag.oMCFLARID_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPAGG_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPAGG_1_1.mHide()
    this.oPgFrm.Page1.oPag.oANAGPUBL_1_5.visible=!this.oPgFrm.Page1.oPag.oANAGPUBL_1_5.mHide()
    this.oPgFrm.Page1.oPag.oNOCONTR_1_6.visible=!this.oPgFrm.Page1.oPag.oNOCONTR_1_6.mHide()
    this.oPgFrm.Page1.oPag.oSTAMPA_1_16.visible=!this.oPgFrm.Page1.oPag.oSTAMPA_1_16.mHide()
    this.oPgFrm.Page1.oPag.oEMAIL_1_17.visible=!this.oPgFrm.Page1.oPag.oEMAIL_1_17.mHide()
    this.oPgFrm.Page1.oPag.oEMPEC_1_18.visible=!this.oPgFrm.Page1.oPag.oEMPEC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCPZ_1_19.visible=!this.oPgFrm.Page1.oPag.oCPZ_1_19.mHide()
    this.oPgFrm.Page1.oPag.oPOSTEL_1_20.visible=!this.oPgFrm.Page1.oPag.oPOSTEL_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFAX_1_21.visible=!this.oPgFrm.Page1.oPag.oFAX_1_21.mHide()
    this.oPgFrm.Page1.oPag.oWE_1_22.visible=!this.oPgFrm.Page1.oPag.oWE_1_22.mHide()
    this.oPgFrm.Page1.oPag.oFIRDIG_1_23.visible=!this.oPgFrm.Page1.oPag.oFIRDIG_1_23.mHide()
    this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.visible=!this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.mHide()
    this.oPgFrm.Page1.oPag.oFLAPCA_1_25.visible=!this.oPgFrm.Page1.oPag.oFLAPCA_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCODCUC_1_26.visible=!this.oPgFrm.Page1.oPag.oCODCUC_1_26.mHide()
    this.oPgFrm.Page1.oPag.oMCTIPCAT_1_28.visible=!this.oPgFrm.Page1.oPag.oMCTIPCAT_1_28.mHide()
    this.oPgFrm.Page1.oPag.oMCCODSIS_1_29.visible=!this.oPgFrm.Page1.oPag.oMCCODSIS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oMCCODCAT_1_30.visible=!this.oPgFrm.Page1.oPag.oMCCODCAT_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMCPERAPP_1_31.visible=!this.oPgFrm.Page1.oPag.oMCPERAPP_1_31.mHide()
    this.oPgFrm.Page1.oPag.oMCFLARID_1_32.visible=!this.oPgFrm.Page1.oPag.oMCFLARID_1_32.mHide()
    this.oPgFrm.Page1.oPag.oMCDATATT_1_33.visible=!this.oPgFrm.Page1.oPag.oMCDATATT_1_33.mHide()
    this.oPgFrm.Page1.oPag.oMCDATSCA_1_34.visible=!this.oPgFrm.Page1.oPag.oMCDATSCA_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oMC__PRNT_1_46.visible=!this.oPgFrm.Page1.oPag.oMC__PRNT_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_54.visible=!this.oPgFrm.Page1.oPag.oBtn_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oANCATFIN_1_70.visible=!this.oPgFrm.Page1.oPag.oANCATFIN_1_70.mHide()
    this.oPgFrm.Page1.oPag.oCFDESCRIC_1_72.visible=!this.oPgFrm.Page1.oPag.oCFDESCRIC_1_72.mHide()
    this.oPgFrm.Page1.oPag.oANCATDER_1_73.visible=!this.oPgFrm.Page1.oPag.oANCATDER_1_73.mHide()
    this.oPgFrm.Page1.oPag.oCFDESCRID_1_75.visible=!this.oPgFrm.Page1.oPag.oCFDESCRID_1_75.mHide()
    this.oPgFrm.Page1.oPag.oANCATRAT_1_76.visible=!this.oPgFrm.Page1.oPag.oANCATRAT_1_76.mHide()
    this.oPgFrm.Page1.oPag.oCFDESCRIR_1_78.visible=!this.oPgFrm.Page1.oPag.oCFDESCRIR_1_78.mHide()
    this.oPgFrm.Page1.oPag.oTIPAGG_1_79.visible=!this.oPgFrm.Page1.oPag.oTIPAGG_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_FLLJXTPGGE()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.outputCombo.Event(cEvent)
        if lower(cEvent)==lower("w_TIPAGG Changed") or lower(cEvent)==lower("Init")
          .Calculate_PFCCEHDKCU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOM1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=INICOD
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INICOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_INICOD)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPANA;
                     ,'ANCODICE',trim(this.w_INICOD))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INICOD)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_INICOD)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_INICOD)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPANA);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_INICOD) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oINICOD_1_7'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPANA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INICOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_INICOD);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPANA;
                       ,'ANCODICE',this.w_INICOD)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INICOD = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_INICOD = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_FINCOD) or .w_FINCOD>=.w_INICOD
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_INICOD = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INICOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FINCOD
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FINCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FINCOD)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPANA;
                     ,'ANCODICE',trim(this.w_FINCOD))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FINCOD)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FINCOD)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FINCOD)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPANA);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FINCOD) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFINCOD_1_8'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPANA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore di quello iniziale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FINCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FINCOD);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPANA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPANA;
                       ,'ANCODICE',this.w_FINCOD)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FINCOD = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FINCOD = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_INICOD) or .w_FINCOD>=.w_INICOD
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore di quello iniziale")
        endif
        this.w_FINCOD = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FINCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCTIPCAT
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_lTable = "TIPCONTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2], .t., this.TIPCONTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCTIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATP',True,'TIPCONTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_MCTIPCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_MCTIPCAT))
          select TPCODICE,TPDESCRI,TPAPPPES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCTIPCAT)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStrODBC(trim(this.w_MCTIPCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStr(trim(this.w_MCTIPCAT)+"%");

            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCTIPCAT) and !this.bDontReportError
            deferred_cp_zoom('TIPCONTR','*','TPCODICE',cp_AbsName(oSource.parent,'oMCTIPCAT_1_28'),i_cWhere,'GSAR_ATP',"Contributi accessori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCTIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_MCTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_MCTIPCAT)
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCTIPCAT = NVL(_Link_.TPCODICE,space(15))
      this.w_TPDESCRI = NVL(_Link_.TPDESCRI,space(40))
      this.w_TPAPPPES = NVL(_Link_.TPAPPPES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCTIPCAT = space(15)
      endif
      this.w_TPDESCRI = space(40)
      this.w_TPAPPPES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCONTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCTIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODSIS
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SISMCOLL_IDX,3]
    i_lTable = "SISMCOLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2], .t., this.SISMCOLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODSIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCL',True,'SISMCOLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCCODICE like "+cp_ToStrODBC(trim(this.w_MCCODSIS)+"%");

          i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCCODICE',trim(this.w_MCCODSIS))
          select SCCODICE,SCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODSIS)==trim(_Link_.SCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStrODBC(trim(this.w_MCCODSIS)+"%");

            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStr(trim(this.w_MCCODSIS)+"%");

            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCODSIS) and !this.bDontReportError
            deferred_cp_zoom('SISMCOLL','*','SCCODICE',cp_AbsName(oSource.parent,'oMCCODSIS_1_29'),i_cWhere,'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',oSource.xKey(1))
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODSIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(this.w_MCCODSIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',this.w_MCCODSIS)
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODSIS = NVL(_Link_.SCCODICE,space(15))
      this.w_SCDESCRI = NVL(_Link_.SCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODSIS = space(15)
      endif
      this.w_SCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_MCCODSIS,"SCCODCON",.w_CGCONTRI),""))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MCCODSIS = space(15)
        this.w_SCDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])+'\'+cp_ToStr(_Link_.SCCODICE,1)
      cp_ShowWarn(i_cKey,this.SISMCOLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODSIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODCAT
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_MCCODCAT)+"%");
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);
                   +" and CGSISCOL="+cp_ToStrODBC(this.w_MCCODSIS);

          i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGCONTRI,CGSISCOL,CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGCONTRI',this.w_MCTIPCAT;
                     ,'CGSISCOL',this.w_MCCODSIS;
                     ,'CGCODICE',trim(this.w_MCCODCAT))
          select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGCONTRI,CGSISCOL,CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCAT)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGCONTRI,CGSISCOL,CGCODICE',cp_AbsName(oSource.parent,'oMCCODCAT_1_30'),i_cWhere,'GSAR_MCT',"Categorie contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MCTIPCAT<>oSource.xKey(1);
           .or. this.w_MCCODSIS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(3));
                     +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);
                     +" and CGSISCOL="+cp_ToStrODBC(this.w_MCCODSIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',oSource.xKey(1);
                       ,'CGSISCOL',oSource.xKey(2);
                       ,'CGCODICE',oSource.xKey(3))
            select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_MCCODCAT);
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);
                   +" and CGSISCOL="+cp_ToStrODBC(this.w_MCCODSIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',this.w_MCTIPCAT;
                       ,'CGSISCOL',this.w_MCCODSIS;
                       ,'CGCODICE',this.w_MCCODCAT)
            select CGCONTRI,CGSISCOL,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCAT = NVL(_Link_.CGCODICE,space(5))
      this.w_DESCCAT = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCAT = space(5)
      endif
      this.w_DESCCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGCONTRI,1)+'\'+cp_ToStr(_Link_.CGSISCOL,1)+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOM)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_2_1'),i_cWhere,'',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_CATCOMD = NVL(_Link_.CTDESCRI,space(35))
      this.w_GRUPPO = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_CATCOMD = space(35)
      this.w_GRUPPO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_2_2'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGENTE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGENTE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODTOBSO,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_3'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODTOBSO,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_CODZON = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESZON = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NAZION
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NAZION))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNAZION_2_4'),i_cWhere,'',"Nazioni",'GSVE0ANA.NAZIONI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NAZION)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_2_5'),i_cWhere,'',"Categorie contabili",'GSAR0AC2.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCCC = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCCC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATFIN
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_FINA_IDX,3]
    i_lTable = "CAT_FINA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2], .t., this.CAT_FINA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gste_Acf',True,'CAT_FINA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_ANCATFIN)+"%");
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);

          i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCAT,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCAT',this.w_TIPOFIN;
                     ,'CFCODICE',trim(this.w_ANCATFIN))
          select CFTIPCAT,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCAT,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATFIN)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(oSource.parent,'oANCATFIN_1_70'),i_cWhere,'Gste_Acf',"Categorie finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("E' possibile selezionare solo categorie di tipo conti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_ANCATFIN);
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',this.w_TIPOFIN;
                       ,'CFCODICE',this.w_ANCATFIN)
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATFIN = NVL(_Link_.CFCODICE,space(5))
      this.w_CFDESCRIC = NVL(_Link_.CFDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATFIN = space(5)
      endif
      this.w_CFDESCRIC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCAT,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_FINA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATDER
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_FINA_IDX,3]
    i_lTable = "CAT_FINA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2], .t., this.CAT_FINA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATDER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gste_Acf',True,'CAT_FINA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_ANCATDER)+"%");
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);

          i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCAT,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCAT',this.w_TIPOFIND;
                     ,'CFCODICE',trim(this.w_ANCATDER))
          select CFTIPCAT,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCAT,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATDER)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATDER) and !this.bDontReportError
            deferred_cp_zoom('CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(oSource.parent,'oANCATDER_1_73'),i_cWhere,'Gste_Acf',"Categorie finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOFIND<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("E' possibile selezionare solo categorie di tipo deroghe")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATDER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_ANCATDER);
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',this.w_TIPOFIND;
                       ,'CFCODICE',this.w_ANCATDER)
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATDER = NVL(_Link_.CFCODICE,space(5))
      this.w_CFDESCRID = NVL(_Link_.CFDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATDER = space(5)
      endif
      this.w_CFDESCRID = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCAT,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_FINA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATDER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATRAT
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_FINA_IDX,3]
    i_lTable = "CAT_FINA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2], .t., this.CAT_FINA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATRAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gste_Acf',True,'CAT_FINA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_ANCATRAT)+"%");
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFINR);

          i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCAT,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCAT',this.w_TIPOFINR;
                     ,'CFCODICE',trim(this.w_ANCATRAT))
          select CFTIPCAT,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCAT,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATRAT)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATRAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(oSource.parent,'oANCATRAT_1_76'),i_cWhere,'Gste_Acf',"Categorie finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOFINR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("E' possibile selezionare solo categorie di tipo rating")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFINR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATRAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_ANCATRAT);
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFINR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',this.w_TIPOFINR;
                       ,'CFCODICE',this.w_ANCATRAT)
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATRAT = NVL(_Link_.CFCODICE,space(5))
      this.w_CFDESCRIR = NVL(_Link_.CFDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATRAT = space(5)
      endif
      this.w_CFDESCRIR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCAT,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_FINA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATRAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPAGG_1_1.RadioValue()==this.w_TIPAGG)
      this.oPgFrm.Page1.oPag.oTIPAGG_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPANA_1_3.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page1.oPag.oTIPANA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANAGPUBL_1_5.RadioValue()==this.w_ANAGPUBL)
      this.oPgFrm.Page1.oPag.oANAGPUBL_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCONTR_1_6.RadioValue()==this.w_NOCONTR)
      this.oPgFrm.Page1.oPag.oNOCONTR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINICOD_1_7.value==this.w_INICOD)
      this.oPgFrm.Page1.oPag.oINICOD_1_7.value=this.w_INICOD
    endif
    if not(this.oPgFrm.Page1.oPag.oFINCOD_1_8.value==this.w_FINCOD)
      this.oPgFrm.Page1.oPag.oFINCOD_1_8.value=this.w_FINCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_12.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_12.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_13.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_13.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_15.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_16.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEMAIL_1_17.RadioValue()==this.w_EMAIL)
      this.oPgFrm.Page1.oPag.oEMAIL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEMPEC_1_18.RadioValue()==this.w_EMPEC)
      this.oPgFrm.Page1.oPag.oEMPEC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCPZ_1_19.RadioValue()==this.w_CPZ)
      this.oPgFrm.Page1.oPag.oCPZ_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSTEL_1_20.RadioValue()==this.w_POSTEL)
      this.oPgFrm.Page1.oPag.oPOSTEL_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAX_1_21.RadioValue()==this.w_FAX)
      this.oPgFrm.Page1.oPag.oFAX_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWE_1_22.RadioValue()==this.w_WE)
      this.oPgFrm.Page1.oPag.oWE_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRDIG_1_23.RadioValue()==this.w_FIRDIG)
      this.oPgFrm.Page1.oPag.oFIRDIG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.RadioValue()==this.w_FLGCPZ)
      this.oPgFrm.Page1.oPag.oFLGCPZ_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPCA_1_25.RadioValue()==this.w_FLAPCA)
      this.oPgFrm.Page1.oPag.oFLAPCA_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCUC_1_26.RadioValue()==this.w_CODCUC)
      this.oPgFrm.Page1.oPag.oCODCUC_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCTIPCAT_1_28.value==this.w_MCTIPCAT)
      this.oPgFrm.Page1.oPag.oMCTIPCAT_1_28.value=this.w_MCTIPCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODSIS_1_29.value==this.w_MCCODSIS)
      this.oPgFrm.Page1.oPag.oMCCODSIS_1_29.value=this.w_MCCODSIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCAT_1_30.value==this.w_MCCODCAT)
      this.oPgFrm.Page1.oPag.oMCCODCAT_1_30.value=this.w_MCCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMCPERAPP_1_31.value==this.w_MCPERAPP)
      this.oPgFrm.Page1.oPag.oMCPERAPP_1_31.value=this.w_MCPERAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oMCFLARID_1_32.RadioValue()==this.w_MCFLARID)
      this.oPgFrm.Page1.oPag.oMCFLARID_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATATT_1_33.value==this.w_MCDATATT)
      this.oPgFrm.Page1.oPag.oMCDATATT_1_33.value=this.w_MCDATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATSCA_1_34.value==this.w_MCDATSCA)
      this.oPgFrm.Page1.oPag.oMCDATSCA_1_34.value=this.w_MCDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oMC__PRNT_1_46.value==this.w_MC__PRNT)
      this.oPgFrm.Page1.oPag.oMC__PRNT_1_46.value=this.w_MC__PRNT
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_2_1.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_2_1.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE_2_2.value==this.w_CODAGE)
      this.oPgFrm.Page2.oPag.oCODAGE_2_2.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_3.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_3.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oNAZION_2_4.value==this.w_NAZION)
      this.oPgFrm.Page2.oPag.oNAZION_2_4.value=this.w_NAZION
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCON_2_5.value==this.w_CATCON)
      this.oPgFrm.Page2.oPag.oCATCON_2_5.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOMD_2_12.value==this.w_CATCOMD)
      this.oPgFrm.Page2.oPag.oCATCOMD_2_12.value=this.w_CATCOMD
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGENTE_2_14.value==this.w_DESAGENTE)
      this.oPgFrm.Page2.oPag.oDESAGENTE_2_14.value=this.w_DESAGENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_15.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_15.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNAZ_2_16.value==this.w_DESNAZ)
      this.oPgFrm.Page2.oPag.oDESNAZ_2_16.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCCC_2_17.value==this.w_DESCCC)
      this.oPgFrm.Page2.oPag.oDESCCC_2_17.value=this.w_DESCCC
    endif
    if not(this.oPgFrm.Page2.oPag.oPIVAVUOTA_2_19.RadioValue()==this.w_PIVAVUOTA)
      this.oPgFrm.Page2.oPag.oPIVAVUOTA_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCFISVUOTA_2_20.RadioValue()==this.w_CFISVUOTA)
      this.oPgFrm.Page2.oPag.oCFISVUOTA_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCATFIN_1_70.value==this.w_ANCATFIN)
      this.oPgFrm.Page1.oPag.oANCATFIN_1_70.value=this.w_ANCATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRIC_1_72.value==this.w_CFDESCRIC)
      this.oPgFrm.Page1.oPag.oCFDESCRIC_1_72.value=this.w_CFDESCRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oANCATDER_1_73.value==this.w_ANCATDER)
      this.oPgFrm.Page1.oPag.oANCATDER_1_73.value=this.w_ANCATDER
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRID_1_75.value==this.w_CFDESCRID)
      this.oPgFrm.Page1.oPag.oCFDESCRID_1_75.value=this.w_CFDESCRID
    endif
    if not(this.oPgFrm.Page1.oPag.oANCATRAT_1_76.value==this.w_ANCATRAT)
      this.oPgFrm.Page1.oPag.oANCATRAT_1_76.value=this.w_ANCATRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRIR_1_78.value==this.w_CFDESCRIR)
      this.oPgFrm.Page1.oPag.oCFDESCRIR_1_78.value=this.w_CFDESCRIR
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPAGG_1_79.RadioValue()==this.w_TIPAGG)
      this.oPgFrm.Page1.oPag.oTIPAGG_1_79.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_FINCOD) or .w_FINCOD>=.w_INICOD)  and not(empty(.w_INICOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINICOD_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not(empty(.w_INICOD) or .w_FINCOD>=.w_INICOD)  and not(empty(.w_FINCOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFINCOD_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore di quello iniziale")
          case   (empty(.w_MCTIPCAT))  and not(.w_FLAPCA='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCTIPCAT_1_28.SetFocus()
            i_bnoObbl = !empty(.w_MCTIPCAT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_MCCODSIS,"SCCODCON",.w_CGCONTRI),"")))  and not(.w_FLAPCA='N')  and not(empty(.w_MCCODSIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODSIS_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MCPERAPP <= 100)  and not(.w_FLAPCA='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCPERAPP_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCFLARID)) or not(.w_MCPERAPP = 100 AND .w_MCFLARID = "N" OR .w_TPAPPPES="S" AND .w_MCFLARID <> "N" OR .w_TPAPPPES<>"S" AND .w_MCFLARID = "I"))  and not(.w_FLAPCA='N')  and (.w_MCPERAPP < 100)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCFLARID_1_32.SetFocus()
            i_bnoObbl = !empty(.w_MCFLARID)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCDATATT))  and not(.w_FLAPCA='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDATATT_1_33.SetFocus()
            i_bnoObbl = !empty(.w_MCDATATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCDATSCA))  and not(.w_FLAPCA='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDATSCA_1_34.SetFocus()
            i_bnoObbl = !empty(.w_MCDATSCA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODZON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODZON_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPAGG = this.w_TIPAGG
    this.o_TIPANA = this.w_TIPANA
    this.o_MCTIPCAT = this.w_MCTIPCAT
    this.o_MCCODSIS = this.w_MCCODSIS
    this.o_MCPERAPP = this.w_MCPERAPP
    this.o_TPAPPPES = this.w_TPAPPPES
    return

enddefine

* --- Define pages as container
define class tgsar_kc1Pag1 as StdContainer
  Width  = 793
  height = 535
  stdWidth  = 793
  stdheight = 535
  resizeXpos=686
  resizeYpos=250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPAGG_1_1 as StdCombo with uid="GGRSDAVYMY",rtseq=1,rtrep=.f.,left=121,top=8,width=159,height=21;
    , ToolTipText = "Tipologia dati da aggiornare";
    , HelpContextID = 171124938;
    , cFormVar="w_TIPAGG",RowSource=""+"Fatturazione CBI,"+"Web application,"+"Document management,"+"Contributi accessori,"+"Entrate/Uscite previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPAGG_1_1.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'C',;
    iif(this.value =5,'E',;
    space(2)))))))
  endfunc
  func oTIPAGG_1_1.GetRadio()
    this.Parent.oContained.w_TIPAGG = this.RadioValue()
    return .t.
  endfunc

  func oTIPAGG_1_1.SetRadio()
    this.Parent.oContained.w_TIPAGG=trim(this.Parent.oContained.w_TIPAGG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPAGG=='F',1,;
      iif(this.Parent.oContained.w_TIPAGG=='P',2,;
      iif(this.Parent.oContained.w_TIPAGG=='D',3,;
      iif(this.Parent.oContained.w_TIPAGG=='C',4,;
      iif(this.Parent.oContained.w_TIPAGG=='E',5,;
      0)))))
  endfunc

  func oTIPAGG_1_1.mHide()
    with this.Parent.oContained
      return (isahr())
    endwith
  endfunc


  add object oTIPANA_1_3 as StdCombo with uid="RRLNUNGSJU",rtseq=2,rtrep=.f.,left=330,top=8,width=126,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 264448202;
    , cFormVar="w_TIPANA",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPANA_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPANA_1_3.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_1_3.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='F',2,;
      0))
  endfunc

  func oTIPANA_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAGG<>'F')
    endwith
   endif
  endfunc

  func oTIPANA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_INICOD)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_FINCOD)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oANAGPUBL_1_5 as StdCombo with uid="MMQGNNFKPH",rtseq=4,rtrep=.f.,left=591,top=8,width=157,height=21;
    , ToolTipText = "Anagrafiche da visualizzare (solo quelle gi� pubblicate, solo quelle ancora da pubblicare oppure tutte)";
    , HelpContextID = 194909358;
    , cFormVar="w_ANAGPUBL",RowSource=""+"Solo pubblicate,"+"Solo non pubblicate,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANAGPUBL_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oANAGPUBL_1_5.GetRadio()
    this.Parent.oContained.w_ANAGPUBL = this.RadioValue()
    return .t.
  endfunc

  func oANAGPUBL_1_5.SetRadio()
    this.Parent.oContained.w_ANAGPUBL=trim(this.Parent.oContained.w_ANAGPUBL)
    this.value = ;
      iif(this.Parent.oContained.w_ANAGPUBL=='P',1,;
      iif(this.Parent.oContained.w_ANAGPUBL=='N',2,;
      iif(this.Parent.oContained.w_ANAGPUBL=='T',3,;
      0)))
  endfunc

  func oANAGPUBL_1_5.mHide()
    with this.Parent.oContained
      return (! g_IZCP$'SA' OR .w_TIPAGG <>'P' OR !g_CPIN='S')
    endwith
  endfunc


  add object oNOCONTR_1_6 as StdCombo with uid="KYWBIATONU",rtseq=5,rtrep=.f.,left=593,top=8,width=174,height=21;
    , ToolTipText = "Filtra in base alla presenza o meno dei contributi";
    , HelpContextID = 213250858;
    , cFormVar="w_NOCONTR",RowSource=""+"Intestatari con contributi,"+"Intestatari privi di contributi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOCONTR_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oNOCONTR_1_6.GetRadio()
    this.Parent.oContained.w_NOCONTR = this.RadioValue()
    return .t.
  endfunc

  func oNOCONTR_1_6.SetRadio()
    this.Parent.oContained.w_NOCONTR=trim(this.Parent.oContained.w_NOCONTR)
    this.value = ;
      iif(this.Parent.oContained.w_NOCONTR=='S',1,;
      iif(this.Parent.oContained.w_NOCONTR=='N',2,;
      iif(this.Parent.oContained.w_NOCONTR=='T',3,;
      0)))
  endfunc

  func oNOCONTR_1_6.mHide()
    with this.Parent.oContained
      return (g_COAC <>'S'  OR .w_TIPAGG <>'C')
    endwith
  endfunc

  add object oINICOD_1_7 as StdField with uid="MRSFOXHTFW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_INICOD", cQueryName = "INICOD",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Intestatario di inizio selezione",;
    HelpContextID = 212964474,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=121, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPANA", oKey_2_1="ANCODICE", oKey_2_2="this.w_INICOD"

  func oINICOD_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oINICOD_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINICOD_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPANA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPANA)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oINICOD_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc
  proc oINICOD_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPANA
     i_obj.w_ANCODICE=this.parent.oContained.w_INICOD
     i_obj.ecpSave()
  endproc

  add object oFINCOD_1_8 as StdField with uid="NUUZKBQRKK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FINCOD", cQueryName = "FINCOD",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore di quello iniziale",;
    ToolTipText = "Intestatario di fine selezione",;
    HelpContextID = 212945322,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=121, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPANA", oKey_2_1="ANCODICE", oKey_2_2="this.w_FINCOD"

  func oFINCOD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFINCOD_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFINCOD_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPANA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPANA)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFINCOD_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc
  proc oFINCOD_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPANA
     i_obj.w_ANCODICE=this.parent.oContained.w_FINCOD
     i_obj.ecpSave()
  endproc


  add object oBtn_1_9 as StdButton with uid="HVAVLDIYYL",left=741, top=40, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 86749114;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCR1_1_12 as StdField with uid="CHLOPGXLBM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260111818,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=260, Top=38, InputMask=replicate('X',40)

  add object oDESCR2_1_13 as StdField with uid="MINMVUHPXL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243334602,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=260, Top=63, InputMask=replicate('X',40)


  add object ZOOM as cp_szoombox with uid="AZBPFEYSMC",left=6, top=91, width=782,height=283,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR_KC1",bOptions=.f.,bAdvOptions=.f.,cTable="CONTI",bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAR_BZC",bReadOnly=.t.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 189624346

  add object oSELEZI_1_15 as StdRadio with uid="IJDSJFZVFN",rtseq=10,rtrep=.f.,left=15, top=380, width=135,height=34;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 117402842
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 117402842
      this.Buttons(2).Top=16
      this.SetAll("Width",133)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe")
      StdRadio::init()
    endproc

  func oSELEZI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_15.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_15.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oSTAMPA_1_16 as StdCombo with uid="HLKFLFIYUS",rtseq=11,rtrep=.f.,left=108,top=438,width=144,height=22;
    , ToolTipText = "Attiva opzione di stampa da processo documentale per intestatari selezionati";
    , HelpContextID = 261623258;
    , cFormVar="w_STAMPA",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_16.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oSTAMPA_1_16.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_16.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='A',1,;
      iif(this.Parent.oContained.w_STAMPA=='S',2,;
      iif(this.Parent.oContained.w_STAMPA=='N',3,;
      0)))
  endfunc

  func oSTAMPA_1_16.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oEMAIL_1_17 as StdCombo with uid="SDWFEXJQPP",rtseq=12,rtrep=.f.,left=339,top=438,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio email da processo documentale per intestatari selezionati";
    , HelpContextID = 254011974;
    , cFormVar="w_EMAIL",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEMAIL_1_17.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oEMAIL_1_17.GetRadio()
    this.Parent.oContained.w_EMAIL = this.RadioValue()
    return .t.
  endfunc

  func oEMAIL_1_17.SetRadio()
    this.Parent.oContained.w_EMAIL=trim(this.Parent.oContained.w_EMAIL)
    this.value = ;
      iif(this.Parent.oContained.w_EMAIL=='A',1,;
      iif(this.Parent.oContained.w_EMAIL=='S',2,;
      iif(this.Parent.oContained.w_EMAIL=='N',3,;
      0)))
  endfunc

  func oEMAIL_1_17.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oEMPEC_1_18 as StdCombo with uid="WJFSKTGEDE",rtseq=13,rtrep=.f.,left=534,top=438,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio PEC da processo documentale per intestatari selezionati";
    , HelpContextID = 244374086;
    , cFormVar="w_EMPEC",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEMPEC_1_18.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oEMPEC_1_18.GetRadio()
    this.Parent.oContained.w_EMPEC = this.RadioValue()
    return .t.
  endfunc

  func oEMPEC_1_18.SetRadio()
    this.Parent.oContained.w_EMPEC=trim(this.Parent.oContained.w_EMPEC)
    this.value = ;
      iif(this.Parent.oContained.w_EMPEC=='A',1,;
      iif(this.Parent.oContained.w_EMPEC=='S',2,;
      iif(this.Parent.oContained.w_EMPEC=='N',3,;
      0)))
  endfunc

  func oEMPEC_1_18.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oCPZ_1_19 as StdCombo with uid="VBBMVDPHZB",rtseq=14,rtrep=.f.,left=108,top=466,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio a Web Application da processo documentale per intestatari selezionati";
    , HelpContextID = 169639206;
    , cFormVar="w_CPZ",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCPZ_1_19.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCPZ_1_19.GetRadio()
    this.Parent.oContained.w_CPZ = this.RadioValue()
    return .t.
  endfunc

  func oCPZ_1_19.SetRadio()
    this.Parent.oContained.w_CPZ=trim(this.Parent.oContained.w_CPZ)
    this.value = ;
      iif(this.Parent.oContained.w_CPZ=='A',1,;
      iif(this.Parent.oContained.w_CPZ=='S',2,;
      iif(this.Parent.oContained.w_CPZ=='N',3,;
      0)))
  endfunc

  func oCPZ_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP$'SA' or g_CPIN = 'S')
    endwith
   endif
  endfunc

  func oCPZ_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG <>'D')
    endwith
  endfunc


  add object oPOSTEL_1_20 as StdCombo with uid="SBZFCROJGU",rtseq=15,rtrep=.f.,left=339,top=466,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio tramite PostaLite da processo documentale per intestatari selezionati";
    , HelpContextID = 88077066;
    , cFormVar="w_POSTEL",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOSTEL_1_20.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPOSTEL_1_20.GetRadio()
    this.Parent.oContained.w_POSTEL = this.RadioValue()
    return .t.
  endfunc

  func oPOSTEL_1_20.SetRadio()
    this.Parent.oContained.w_POSTEL=trim(this.Parent.oContained.w_POSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_POSTEL=='A',1,;
      iif(this.Parent.oContained.w_POSTEL=='S',2,;
      iif(this.Parent.oContained.w_POSTEL=='N',3,;
      0)))
  endfunc

  func oPOSTEL_1_20.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oFAX_1_21 as StdCombo with uid="TFIZBXCYTF",rtseq=16,rtrep=.f.,left=534,top=466,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio FAX da processo documentale per intestatari selezionati";
    , HelpContextID = 169627222;
    , cFormVar="w_FAX",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFAX_1_21.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFAX_1_21.GetRadio()
    this.Parent.oContained.w_FAX = this.RadioValue()
    return .t.
  endfunc

  func oFAX_1_21.SetRadio()
    this.Parent.oContained.w_FAX=trim(this.Parent.oContained.w_FAX)
    this.value = ;
      iif(this.Parent.oContained.w_FAX=='A',1,;
      iif(this.Parent.oContained.w_FAX=='S',2,;
      iif(this.Parent.oContained.w_FAX=='N',3,;
      0)))
  endfunc

  func oFAX_1_21.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oWE_1_22 as StdCombo with uid="GDJYGMQEJR",rtseq=17,rtrep=.f.,left=339,top=494,width=144,height=22;
    , ToolTipText = "Attiva opzione di invio a net-folder da processo documentale per intestatari selezionati";
    , HelpContextID = 169268070;
    , cFormVar="w_WE",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oWE_1_22.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oWE_1_22.GetRadio()
    this.Parent.oContained.w_WE = this.RadioValue()
    return .t.
  endfunc

  func oWE_1_22.SetRadio()
    this.Parent.oContained.w_WE=trim(this.Parent.oContained.w_WE)
    this.value = ;
      iif(this.Parent.oContained.w_WE=='A',1,;
      iif(this.Parent.oContained.w_WE=='S',2,;
      iif(this.Parent.oContained.w_WE=='N',3,;
      0)))
  endfunc

  func oWE_1_22.mHide()
    with this.Parent.oContained
      return ( g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oFIRDIG_1_23 as StdCombo with uid="IXJFCRJYCU",rtseq=18,rtrep=.f.,left=108,top=494,width=144,height=22;
    , ToolTipText = "Attiva opzione di firma digitale da processo documentale per intestatari selezionati";
    , HelpContextID = 168823210;
    , cFormVar="w_FIRDIG",RowSource=""+"Nessun aggiornamento,"+"Attiva,"+"Disattiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFIRDIG_1_23.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFIRDIG_1_23.GetRadio()
    this.Parent.oContained.w_FIRDIG = this.RadioValue()
    return .t.
  endfunc

  func oFIRDIG_1_23.SetRadio()
    this.Parent.oContained.w_FIRDIG=trim(this.Parent.oContained.w_FIRDIG)
    this.value = ;
      iif(this.Parent.oContained.w_FIRDIG=='A',1,;
      iif(this.Parent.oContained.w_FIRDIG=='S',2,;
      iif(this.Parent.oContained.w_FIRDIG=='N',3,;
      0)))
  endfunc

  func oFIRDIG_1_23.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc


  add object oFLGCPZ_1_24 as StdCombo with uid="VPPTWENFMH",rtseq=19,rtrep=.f.,left=105,top=439,width=114,height=22;
    , ToolTipText = "Se attivo: trasferisce anagrafica cliente a Web Application";
    , HelpContextID = 111261354;
    , cFormVar="w_FLGCPZ",RowSource=""+"Pubblica su web,"+"Non pubblicare su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGCPZ_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    'N')))
  endfunc
  func oFLGCPZ_1_24.GetRadio()
    this.Parent.oContained.w_FLGCPZ = this.RadioValue()
    return .t.
  endfunc

  func oFLGCPZ_1_24.SetRadio()
    this.Parent.oContained.w_FLGCPZ=trim(this.Parent.oContained.w_FLGCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_FLGCPZ=='S',1,;
      iif(this.Parent.oContained.w_FLGCPZ=='N',2,;
      0))
  endfunc

  func oFLGCPZ_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP$'SA' or g_CPIN='S' or g_REVICRM='S')
    endwith
   endif
  endfunc

  func oFLGCPZ_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG <>'P')
    endwith
  endfunc

  add object oFLAPCA_1_25 as StdCheck with uid="YXUJBDTWJY",rtseq=20,rtrep=.f.,left=29, top=438, caption="Applica contributi accessori",;
    ToolTipText = "Abilita contributi accessori",;
    HelpContextID = 6624938,;
    cFormVar="w_FLAPCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAPCA_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAPCA_1_25.GetRadio()
    this.Parent.oContained.w_FLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oFLAPCA_1_25.SetRadio()
    this.Parent.oContained.w_FLAPCA=trim(this.Parent.oContained.w_FLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPCA=='S',1,;
      0)
  endfunc

  func oFLAPCA_1_25.mHide()
    with this.Parent.oContained
      return (g_COAC <>'S' OR .w_TIPAGG <>'C')
    endwith
  endfunc


  add object oCODCUC_1_26 as StdCombo with uid="OSEOINGQBA",rtseq=21,rtrep=.f.,left=98,top=435,width=270,height=21;
    , ToolTipText = "Fattura CBI";
    , HelpContextID = 223470554;
    , cFormVar="w_CODCUC",RowSource=""+"No,"+"Solo testata,"+"Testata + corpo non pubblicato,"+"Testata + corpo pubblicato,"+"Testata + corpo 'white label'", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODCUC_1_26.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'01',;
    iif(this.value =3,'02',;
    iif(this.value =4,'03',;
    iif(this.value =5,'04',;
    space(2)))))))
  endfunc
  func oCODCUC_1_26.GetRadio()
    this.Parent.oContained.w_CODCUC = this.RadioValue()
    return .t.
  endfunc

  func oCODCUC_1_26.SetRadio()
    this.Parent.oContained.w_CODCUC=trim(this.Parent.oContained.w_CODCUC)
    this.value = ;
      iif(this.Parent.oContained.w_CODCUC=='00',1,;
      iif(this.Parent.oContained.w_CODCUC=='01',2,;
      iif(this.Parent.oContained.w_CODCUC=='02',3,;
      iif(this.Parent.oContained.w_CODCUC=='03',4,;
      iif(this.Parent.oContained.w_CODCUC=='04',5,;
      0)))))
  endfunc

  func oCODCUC_1_26.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' OR .w_TIPAGG <>'F')
    endwith
  endfunc

  add object oMCTIPCAT_1_28 as StdField with uid="AQXGZPNXRZ",rtseq=22,rtrep=.t.,;
    cFormVar = "w_MCTIPCAT", cQueryName = "MCTIPCAT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tipo contributo",;
    HelpContextID = 228257510,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=11, Top=484, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIPCONTR", cZoomOnZoom="GSAR_ATP", oKey_1_1="TPCODICE", oKey_1_2="this.w_MCTIPCAT"

  func oMCTIPCAT_1_28.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  func oMCTIPCAT_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
      if .not. empty(.w_MCCODCAT)
        bRes2=.link_1_30('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMCTIPCAT_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCTIPCAT_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCONTR','*','TPCODICE',cp_AbsName(this.parent,'oMCTIPCAT_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATP',"Contributi accessori",'',this.parent.oContained
  endproc
  proc oMCTIPCAT_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TPCODICE=this.parent.oContained.w_MCTIPCAT
     i_obj.ecpSave()
  endproc

  add object oMCCODSIS_1_29 as StdField with uid="FOXRACVXYV",rtseq=23,rtrep=.t.,;
    cFormVar = "w_MCCODSIS", cQueryName = "MCCODSIS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 27918617,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=145, Top=484, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SISMCOLL", cZoomOnZoom="GSAR_MCL", oKey_1_1="SCCODICE", oKey_1_2="this.w_MCCODSIS"

  func oMCCODSIS_1_29.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  func oMCCODSIS_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
      if .not. empty(.w_MCCODCAT)
        bRes2=.link_1_30('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMCCODSIS_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODSIS_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SISMCOLL','*','SCCODICE',cp_AbsName(this.parent,'oMCCODSIS_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this.parent.oContained
  endproc
  proc oMCCODSIS_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SCCODICE=this.parent.oContained.w_MCCODSIS
     i_obj.ecpSave()
  endproc

  add object oMCCODCAT_1_30 as StdField with uid="KAATEPTJUW",rtseq=24,rtrep=.t.,;
    cFormVar = "w_MCCODCAT", cQueryName = "MCCODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria contributo",;
    HelpContextID = 240516838,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=268, Top=484, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGCONTRI", oKey_1_2="this.w_MCTIPCAT", oKey_2_1="CGSISCOL", oKey_2_2="this.w_MCCODSIS", oKey_3_1="CGCODICE", oKey_3_2="this.w_MCCODCAT"

  func oMCCODCAT_1_30.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  func oMCCODCAT_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCAT_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCAT_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CATMCONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStrODBC(this.Parent.oContained.w_MCTIPCAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGSISCOL="+cp_ToStrODBC(this.Parent.oContained.w_MCCODSIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStr(this.Parent.oContained.w_MCTIPCAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGSISCOL="+cp_ToStr(this.Parent.oContained.w_MCCODSIS)
    endif
    do cp_zoom with 'CATMCONT','*','CGCONTRI,CGSISCOL,CGCODICE',cp_AbsName(this.parent,'oMCCODCAT_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"Categorie contributo",'',this.parent.oContained
  endproc
  proc oMCCODCAT_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CGCONTRI=w_MCTIPCAT
    i_obj.CGSISCOL=w_MCCODSIS
     i_obj.w_CGCODICE=this.parent.oContained.w_MCCODCAT
     i_obj.ecpSave()
  endproc

  add object oMCPERAPP_1_31 as StdField with uid="DTPVEDNQVT",rtseq=25,rtrep=.t.,;
    cFormVar = "w_MCPERAPP", cQueryName = "MCPERAPP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale applicazione contributo",;
    HelpContextID = 259993322,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=340, Top=484, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMCPERAPP_1_31.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  func oMCPERAPP_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MCPERAPP <= 100)
    endwith
    return bRes
  endfunc


  add object oMCFLARID_1_32 as StdCombo with uid="FYTLNJTVTS",rtseq=26,rtrep=.t.,left=419,top=484,width=115,height=21;
    , HelpContextID = 7811338;
    , cFormVar="w_MCFLARID",RowSource=""+"Non gestito,"+"Quantit�,"+"Importo", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oMCFLARID_1_32.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'Q',;
    iif(this.value =3,'I',;
    space(1)))))
  endfunc
  func oMCFLARID_1_32.GetRadio()
    this.Parent.oContained.w_MCFLARID = this.RadioValue()
    return .t.
  endfunc

  func oMCFLARID_1_32.SetRadio()
    this.Parent.oContained.w_MCFLARID=trim(this.Parent.oContained.w_MCFLARID)
    this.value = ;
      iif(this.Parent.oContained.w_MCFLARID=='N',1,;
      iif(this.Parent.oContained.w_MCFLARID=='Q',2,;
      iif(this.Parent.oContained.w_MCFLARID=='I',3,;
      0)))
  endfunc

  func oMCFLARID_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCPERAPP < 100)
    endwith
   endif
  endfunc

  func oMCFLARID_1_32.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  func oMCFLARID_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MCPERAPP = 100 AND .w_MCFLARID = "N" OR .w_TPAPPPES="S" AND .w_MCFLARID <> "N" OR .w_TPAPPPES<>"S" AND .w_MCFLARID = "I")
    endwith
    return bRes
  endfunc

  add object oMCDATATT_1_33 as StdField with uid="FBSQKSIZOE",rtseq=27,rtrep=.t.,;
    cFormVar = "w_MCDATATT", cQueryName = "MCDATATT",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 10227994,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=537, Top=484

  func oMCDATATT_1_33.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oMCDATSCA_1_34 as StdField with uid="LFPBJDHHFA",rtseq=28,rtrep=.t.,;
    cFormVar = "w_MCDATSCA", cQueryName = "MCDATSCA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 43782407,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=614, Top=484

  func oMCDATSCA_1_34.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oMC__PRNT_1_46 as StdField with uid="ZSCCHJJJUB",rtseq=33,rtrep=.t.,;
    cFormVar = "w_MC__PRNT", cQueryName = "MC__PRNT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Note da riportare nella stampa documento",;
    HelpContextID = 243547878,;
   bGlobalFont=.t.,;
    Height=21, Width=525, Left=161, Top=510, InputMask=replicate('X',254)

  func oMC__PRNT_1_46.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc


  add object oBtn_1_48 as StdButton with uid="VMPFRPKBFV",left=689, top=486, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'inserimento";
    , HelpContextID = 8240618;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSAR_BK1(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_49 as StdButton with uid="MTXNONHMHL",left=741, top=486, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8240618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_54 as StdButton with uid="NQYKXBIKCI",left=741, top=383, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 225832410;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and (g_IZCP$'SA' or g_CPIN='S' or g_REVICRM='S'))
      endwith
    endif
  endfunc

  func oBtn_1_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPAGG<>'P')
     endwith
    endif
  endfunc


  add object outputCombo as cp_outputCombo with uid="ZLERXIVPWT",left=265, top=389, width=468,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",Visible=.f.,;
    nPag=1;
    , HelpContextID = 189624346


  add object ZOOM1 as cp_szoombox with uid="DBLSLVVPUP",left=6, top=91, width=782,height=283,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR1KC1",bOptions=.f.,bAdvOptions=.f.,cTable="CONTI",bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAR_BZC",bReadOnly=.t.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 189624346

  add object oANCATFIN_1_70 as StdField with uid="ESJGXDKQOR",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ANCATFIN", cQueryName = "ANCATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "E' possibile selezionare solo categorie di tipo conti",;
    ToolTipText = "Categoria finanziaria",;
    HelpContextID = 94112596,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=207, Top=438, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_FINA", cZoomOnZoom="Gste_Acf", oKey_1_1="CFTIPCAT", oKey_1_2="this.w_TIPOFIN", oKey_2_1="CFCODICE", oKey_2_2="this.w_ANCATFIN"

  func oANCATFIN_1_70.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  func oANCATFIN_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATFIN_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATFIN_1_70.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAT_FINA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPOFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPOFIN)
    endif
    do cp_zoom with 'CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(this.parent,'oANCATFIN_1_70'),iif(empty(i_cWhere),.f.,i_cWhere),'Gste_Acf',"Categorie finanziarie",'',this.parent.oContained
  endproc
  proc oANCATFIN_1_70.mZoomOnZoom
    local i_obj
    i_obj=Gste_Acf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCAT=w_TIPOFIN
     i_obj.w_CFCODICE=this.parent.oContained.w_ANCATFIN
     i_obj.ecpSave()
  endproc

  add object oCFDESCRIC_1_72 as StdField with uid="KUKLNAYXXR",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CFDESCRIC", cQueryName = "CFDESCRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 225437793,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=271, Top=438, InputMask=replicate('X',30)

  func oCFDESCRIC_1_72.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oANCATDER_1_73 as StdField with uid="UPGACCOKZZ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_ANCATDER", cQueryName = "ANCATDER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "E' possibile selezionare solo categorie di tipo deroghe",;
    ToolTipText = "Categoria calcolo giorni",;
    HelpContextID = 60558168,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=207, Top=470, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_FINA", cZoomOnZoom="Gste_Acf", oKey_1_1="CFTIPCAT", oKey_1_2="this.w_TIPOFIND", oKey_2_1="CFCODICE", oKey_2_2="this.w_ANCATDER"

  func oANCATDER_1_73.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  func oANCATDER_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATDER_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATDER_1_73.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAT_FINA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPOFIND)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPOFIND)
    endif
    do cp_zoom with 'CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(this.parent,'oANCATDER_1_73'),iif(empty(i_cWhere),.f.,i_cWhere),'Gste_Acf',"Categorie finanziarie",'',this.parent.oContained
  endproc
  proc oANCATDER_1_73.mZoomOnZoom
    local i_obj
    i_obj=Gste_Acf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCAT=w_TIPOFIND
     i_obj.w_CFCODICE=this.parent.oContained.w_ANCATDER
     i_obj.ecpSave()
  endproc

  add object oCFDESCRID_1_75 as StdField with uid="LXFWGQLUHH",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CFDESCRID", cQueryName = "CFDESCRID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 225437777,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=271, Top=470, InputMask=replicate('X',30)

  func oCFDESCRID_1_75.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oANCATRAT_1_76 as StdField with uid="FDXSYPGQJK",rtseq=57,rtrep=.f.,;
    cFormVar = "w_ANCATRAT", cQueryName = "ANCATRAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "E' possibile selezionare solo categorie di tipo rating",;
    ToolTipText = "Categoria rating",;
    HelpContextID = 241431718,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=207, Top=502, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_FINA", cZoomOnZoom="Gste_Acf", oKey_1_1="CFTIPCAT", oKey_1_2="this.w_TIPOFINR", oKey_2_1="CFCODICE", oKey_2_2="this.w_ANCATRAT"

  func oANCATRAT_1_76.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  func oANCATRAT_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATRAT_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATRAT_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAT_FINA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPOFINR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPOFINR)
    endif
    do cp_zoom with 'CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(this.parent,'oANCATRAT_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'Gste_Acf',"Categorie finanziarie",'',this.parent.oContained
  endproc
  proc oANCATRAT_1_76.mZoomOnZoom
    local i_obj
    i_obj=Gste_Acf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCAT=w_TIPOFINR
     i_obj.w_CFCODICE=this.parent.oContained.w_ANCATRAT
     i_obj.ecpSave()
  endproc

  add object oCFDESCRIR_1_78 as StdField with uid="BICUUCKCBJ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CFDESCRIR", cQueryName = "CFDESCRIR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 225437553,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=271, Top=502, InputMask=replicate('X',30)

  func oCFDESCRIR_1_78.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc


  add object oTIPAGG_1_79 as StdCombo with uid="MOVJONOQEO",rtseq=60,rtrep=.f.,left=121,top=8,width=159,height=21;
    , ToolTipText = "Tipologia dati da aggiornare";
    , HelpContextID = 171124938;
    , cFormVar="w_TIPAGG",RowSource=""+"Fatturazione CBI,"+"Web application,"+"Document management,"+"Contributi accessori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPAGG_1_79.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'C',;
    space(2))))))
  endfunc
  func oTIPAGG_1_79.GetRadio()
    this.Parent.oContained.w_TIPAGG = this.RadioValue()
    return .t.
  endfunc

  func oTIPAGG_1_79.SetRadio()
    this.Parent.oContained.w_TIPAGG=trim(this.Parent.oContained.w_TIPAGG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPAGG=='F',1,;
      iif(this.Parent.oContained.w_TIPAGG=='P',2,;
      iif(this.Parent.oContained.w_TIPAGG=='D',3,;
      iif(this.Parent.oContained.w_TIPAGG=='C',4,;
      0))))
  endfunc

  func oTIPAGG_1_79.mHide()
    with this.Parent.oContained
      return (isahe())
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="XHJQHBHNBF",Visible=.t., Left=289, Top=10,;
    Alignment=1, Width=36, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=15, Top=38,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=15, Top=63,;
    Alignment=1, Width=101, Height=18,;
    Caption="A intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UTITHVZAYY",Visible=.t., Left=10, Top=465,;
    Alignment=2, Width=127, Height=18,;
    Caption="Contributo accessorio"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="KJQNMCQQZX",Visible=.t., Left=269, Top=465,;
    Alignment=2, Width=65, Height=18,;
    Caption="Categoria"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="RBYVZGYWKS",Visible=.t., Left=336, Top=465,;
    Alignment=2, Width=85, Height=18,;
    Caption="% di applic."  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="VXCXMQMSXP",Visible=.t., Left=144, Top=465,;
    Alignment=2, Width=117, Height=18,;
    Caption="Sistema collettivo"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="JRVOMXYZEJ",Visible=.t., Left=422, Top=465,;
    Alignment=2, Width=109, Height=18,;
    Caption="Riduzione"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="ASSWZCFGMD",Visible=.t., Left=536, Top=465,;
    Alignment=2, Width=73, Height=18,;
    Caption="Valido dal"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="FEJRQQONRK",Visible=.t., Left=618, Top=465,;
    Alignment=2, Width=67, Height=18,;
    Caption="Fino al"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="GEFRPVWLUY",Visible=.t., Left=1, Top=513,;
    Alignment=1, Width=158, Height=18,;
    Caption="Note per stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_FLAPCA='N')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="FXXUDFUZXT",Visible=.t., Left=521, Top=10,;
    Alignment=1, Width=65, Height=18,;
    Caption="Contributi:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (g_COAC <>'S'  OR .w_TIPAGG <>'C')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="OOFMLKMVFR",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=261, Height=18,;
    Caption="Abilitazione processi documentali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (g_DOCM <>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="NCVKEYXFTQ",Visible=.t., Left=152, Top=391,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG<>'P')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="WHLLZDGAZA",Visible=.t., Left=459, Top=10,;
    Alignment=1, Width=127, Height=18,;
    Caption="Stato pubblicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (! g_IZCP$'SA'  OR .w_TIPAGG <>'P' OR !g_CPIN='S')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="IJXGUUAVRD",Visible=.t., Left=8, Top=435,;
    Alignment=1, Width=85, Height=18,;
    Caption="Fattura CBI:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' OR .w_TIPAGG <>'F')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="APXYTJSEKB",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=112, Height=18,;
    Caption="Aggiorna dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="DQFIJRQZZQ",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=194, Height=18,;
    Caption="Abilitazione contributi accessori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (g_COAC <>'S' OR .w_TIPAGG <>'C')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="USPOKLGWFW",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=150, Height=18,;
    Caption="Abilitazione dati CBI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (g_FAEL <>'S' OR .w_TIPAGG <>'F')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="YXZCZVBZLH",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=225, Height=18,;
    Caption="Abilitazione dati Web Application"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (g_FAEL <>'S' OR .w_TIPAGG <>'P')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="QZQDCGUWQL",Visible=.t., Left=9, Top=414,;
    Alignment=0, Width=272, Height=18,;
    Caption="Abilitazione categorie entrate/uscite previsionali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (g_TESO <>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="XVUFRRIFZQ",Visible=.t., Left=62, Top=441,;
    Alignment=1, Width=143, Height=18,;
    Caption="Categoria finanziaria:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="QIMSIFIRHM",Visible=.t., Left=6, Top=471,;
    Alignment=1, Width=199, Height=18,;
    Caption="Categoria finanziaria deroghe:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="WPYCLKGVAJ",Visible=.t., Left=62, Top=505,;
    Alignment=1, Width=143, Height=18,;
    Caption="Categoria rating:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (g_TESO<>'S' OR .w_TIPAGG <>'E')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="PUBMWGRYGZ",Visible=.t., Left=5, Top=440,;
    Alignment=1, Width=91, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="LFFTKQSRTO",Visible=.t., Left=275, Top=440,;
    Alignment=1, Width=57, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="RBPYNJCLKY",Visible=.t., Left=507, Top=468,;
    Alignment=1, Width=24, Height=18,;
    Caption="FAX:"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="HOGCTPVPYU",Visible=.t., Left=5, Top=468,;
    Alignment=1, Width=91, Height=18,;
    Caption="Web Application:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="ZXBDJAYGAM",Visible=.t., Left=275, Top=468,;
    Alignment=1, Width=57, Height=18,;
    Caption="PostaLite:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="OOXFLUSZFK",Visible=.t., Left=275, Top=496,;
    Alignment=1, Width=57, Height=18,;
    Caption="Net-folder:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return ( g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="OAETTTEXJR",Visible=.t., Left=5, Top=497,;
    Alignment=1, Width=91, Height=18,;
    Caption="Firma digitale:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="QHEYPQOABQ",Visible=.t., Left=501, Top=440,;
    Alignment=1, Width=30, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR .w_TIPAGG <>'D')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="OXPEAWYTTA",Visible=.t., Left=3, Top=443,;
    Alignment=1, Width=100, Height=18,;
    Caption="Web application:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG <>'P')
    endwith
  endfunc

  add object oBox_1_59 as StdBox with uid="CCYZWKIWPZ",left=4, top=430, width=685,height=1
enddefine
define class tgsar_kc1Pag2 as StdContainer
  Width  = 793
  height = 535
  stdWidth  = 793
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCATCOM_2_1 as StdField with uid="RCDPGMCMDR",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale di selezione",;
    HelpContextID = 61927898,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=39, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie commerciali",'',this.parent.oContained
  endproc

  add object oCODAGE_2_2 as StdField with uid="GAWNIHXMJD",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente",;
    ToolTipText = "Agente selezionato",;
    HelpContextID = 204727258,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=161, Top=74, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oCODZON_2_3 as StdField with uid="QJDMIHOTHD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 43705306,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=109, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oNAZION_2_4 as StdField with uid="AFIAUNXGBJ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_NAZION", cQueryName = "NAZION",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione selezionata",;
    HelpContextID = 44732714,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=144, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NAZION"

  func oNAZION_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNAZION_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNAZION_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNAZION_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nazioni",'GSVE0ANA.NAZIONI_VZM',this.parent.oContained
  endproc

  add object oCATCON_2_5 as StdField with uid="HVTGPSGIMF",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 45150682,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=179, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie contabili",'GSAR0AC2.CACOCLFO_VZM',this.parent.oContained
  endproc

  add object oCATCOMD_2_12 as StdField with uid="GEURVMYGRO",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CATCOMD", cQueryName = "CATCOMD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 206507558,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=227, Top=39, InputMask=replicate('X',35)

  add object oDESAGENTE_2_14 as StdField with uid="EANCWGUFWH",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESAGENTE", cQueryName = "DESAGENTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 204667174,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=227, Top=74, InputMask=replicate('X',35)

  add object oDESZON_2_15 as StdField with uid="CMKPWYGMPX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 43646410,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=227, Top=109, InputMask=replicate('X',35)

  add object oDESNAZ_2_16 as StdField with uid="THHWJIIXZU",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126221770,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=227, Top=144, InputMask=replicate('X',35)

  add object oDESCCC_2_17 as StdField with uid="KHIMEAPONY",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESCCC", cQueryName = "DESCCC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242286026,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=227, Top=179, InputMask=replicate('X',35)

  add object oPIVAVUOTA_2_19 as StdCheck with uid="QLIQUVXBHG",rtseq=49,rtrep=.f.,left=550, top=74, caption="P.IVA vuota",;
    ToolTipText = "Se attivo: visualizza anagrafiche con partita IVA vuota",;
    HelpContextID = 188925094,;
    cFormVar="w_PIVAVUOTA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIVAVUOTA_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPIVAVUOTA_2_19.GetRadio()
    this.Parent.oContained.w_PIVAVUOTA = this.RadioValue()
    return .t.
  endfunc

  func oPIVAVUOTA_2_19.SetRadio()
    this.Parent.oContained.w_PIVAVUOTA=trim(this.Parent.oContained.w_PIVAVUOTA)
    this.value = ;
      iif(this.Parent.oContained.w_PIVAVUOTA=='S',1,;
      0)
  endfunc

  add object oCFISVUOTA_2_20 as StdCheck with uid="LWMXCSWZYB",rtseq=50,rtrep=.f.,left=550, top=39, caption="Cod.fiscale vuoto",;
    ToolTipText = "Se attivo: visualizza anagrafiche con codice fiscale vuoto",;
    HelpContextID = 187799670,;
    cFormVar="w_CFISVUOTA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCFISVUOTA_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFISVUOTA_2_20.GetRadio()
    this.Parent.oContained.w_CFISVUOTA = this.RadioValue()
    return .t.
  endfunc

  func oCFISVUOTA_2_20.SetRadio()
    this.Parent.oContained.w_CFISVUOTA=trim(this.Parent.oContained.w_CFISVUOTA)
    this.value = ;
      iif(this.Parent.oContained.w_CFISVUOTA=='S',1,;
      0)
  endfunc

  add object oStr_2_6 as StdString with uid="WTHSDEZPJY",Visible=.t., Left=47, Top=74,;
    Alignment=1, Width=109, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="HSWSDVDMII",Visible=.t., Left=36, Top=179,;
    Alignment=1, Width=120, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="LVHUTRYMYJ",Visible=.t., Left=110, Top=111,;
    Alignment=1, Width=46, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="GZCJKQCEYX",Visible=.t., Left=108, Top=145,;
    Alignment=1, Width=48, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="OPURTLUYAH",Visible=.t., Left=11, Top=40,;
    Alignment=1, Width=145, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kc1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
