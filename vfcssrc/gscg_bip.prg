* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bip                                                        *
*              Calcolo e aggiornamento numero protocollo                       *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-18                                                      *
* Last revis.: 2010-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bip",oParentObject,m.pEXEC,m.pSERIAL)
return(i_retval)

define class tgscg_bip as StdBatch
  * --- Local variables
  pEXEC = space(1)
  pSERIAL = space(10)
  w_PROG = .NULL.
  w_SERIALE = space(10)
  w_CALLER = .NULL.
  w_NUMRER = 0
  w_CODUTE = 0
  w_SERIAL = space(10)
  w_DATREG = ctod("  /  /  ")
  w_ESERCIZ = space(4)
  * --- WorkFile variables
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCG_KPP esegue calcolo e inserimento numero protocollo in primanota
    do case
      case this.pEXEC="I"
        this.w_CALLER = this.oParentObject
        g_MSG= " "
        cp_SetGlobalVar("g_SCHEDULER","S")
        this.w_PROG = GSCG_MPN()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_SERIALE = this.pSERIAL
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_PNSERIAL = this.w_SERIALE
        this.w_PROG.ecpSave()     
        * --- Verifico se posso modificare la registrazione
        if this.w_PROG.HAsCpEvents("EcpEdit")
          * --- Verifico che il numero e alfa protocollo da inserire non siano gi� presenti in un'altra registrazione contabile.
          if this.oParentObject.w_PNTIPREG = "A" AND ! UNIVOC(this.w_SERIALE,SPACE(10),this.oParentObject.w_PNTIPREG,0,SPACE(2),SPACE(4),cp_CharToDate("  /  /    "),this.oParentObject.w_PNPRP,this.oParentObject.w_PNNUMPRO,this.oParentObject.w_PNALFPRO,this.oParentObject.w_PNANNPRO,Space(15),Space(1),"P")
            * --- Numero e alfa gi� presenti
            this.w_PROG.ecpQuit()     
            this.w_PROG = .Null.
            cp_SetGlobalVar("g_SCHEDULER","N")
            if ! empty(g_MSG)
              Ah_ErrorMsg(g_MSG,48)
            endif
            i_retcode = 'stop'
            return
          else
            * --- Procedo alla modifica del numero e alfa protocollo nella registrazione selezionata
            i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
            cp_NextTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
            this.w_PROG.ecpEdit()     
            * --- Aggiorno i campi relativi al numero e all'alfa protocollo
            this.w_PROG.bHeaderUpdated = .T.
            this.w_PROG.OP_PNNUMPRO = this.oParentObject.w_PNNUMPRO
            this.w_PROG.OP_PNALFPRO = this.oParentObject.w_PNALFPRO
            this.w_PROG.w_PNNUMPRO = this.oParentObject.w_PNNUMPRO
            this.w_PROG.w_PNALFPRO = this.oParentObject.w_PNALFPRO
            * --- Inibisco il ricalcolo del protocollo al salvataggio della registrazione (GSVE_BCK())
            this.w_PROG.w_ONUMPRO = this.oParentObject.w_PNNUMPRO
            this.w_PROG.w_OALFPRO = this.oParentObject.w_PNALFPRO
            this.w_PROG.ecpSave()     
            this.w_PROG.ecpQuit()     
            this.w_PROG = .Null.
            cp_SetGlobalVar("g_SCHEDULER","N")
          endif
        endif
        * --- Chiudo la maschera di variazione numero protocollo
        this.w_CALLER.EcpQuit()     
      case this.pEXEC="P"
        * --- Propone il primo progressivo  relativo al 'numero protocollo' disponibile
        this.oParentObject.w_PNNUMPRO = 0
        i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
        cp_AskTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
    endcase
  endproc


  proc Init(oParentObject,pEXEC,pSERIAL)
    this.pEXEC=pEXEC
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PNT_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC,pSERIAL"
endproc
