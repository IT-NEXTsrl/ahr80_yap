* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_btl                                                        *
*              VERIFICA CALENDARI AZIENDALI                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-12                                                      *
* Last revis.: 2008-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_btl",oParentObject)
return(i_retval)

define class tgsar_btl as StdBatch
  * --- Local variables
  w_READ_COD = space(5)
  w_TCDESCRI = space(40)
  * --- WorkFile variables
  TAB_CALE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TCFLDEFA="S"
      this.w_READ_COD = SPACE(5)
      * --- Read from TAB_CALE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TAB_CALE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2],.t.,this.TAB_CALE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TCCODICE,TCDESCRI"+;
          " from "+i_cTable+" TAB_CALE where ";
              +"TCFLDEFA = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TCCODICE,TCDESCRI;
          from (i_cTable) where;
              TCFLDEFA = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_READ_COD = NVL(cp_ToDate(_read_.TCCODICE),cp_NullValue(_read_.TCCODICE))
        this.w_TCDESCRI = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_TCCODICE<>this.w_READ_COD AND !EMPTY(NVL(this.w_READ_COD, " "))
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat("Impossibile impostare come predefinito il calendario attuale. Il calendario %1 ( %2 ) gi� impostato come predefinito.", ALLTRIM(this.w_READ_COD), ALLTRIM(this.w_TCDESCRI) )
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_CALE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
