* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bzp                                                        *
*              Gestioni da manutenzione partite                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_91]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-28                                                      *
* Last revis.: 2014-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pTIPMOV,pROWNUM,pRESIDUO,pTIPMAN,pROWORD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bzp",oParentObject,m.pSERIAL,m.pTIPMOV,m.pROWNUM,m.pRESIDUO,m.pTIPMAN,m.pROWORD)
return(i_retval)

define class tgste_bzp as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pTIPMOV = 0
  pROWNUM = 0
  pRESIDUO = 0
  pTIPMAN = 0
  pROWORD = 0
  w_PROG = .NULL.
  w_MESS = space(10)
  w_FLSALD1 = space(1)
  w_PTNUMPAR = space(31)
  w_PTFLSOSP = space(1)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTNUMEFF = 0
  w_PTTOTIMP = 0
  w_PTCODVAL = space(3)
  w_PTNUMDIS = space(10)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_PTBANAPP = space(10)
  w_PTSERRIF = space(10)
  w_OK = .f.
  w_DATDOC = ctod("  /  /  ")
  w_SERDIS = space(10)
  w_LSERDIS = space(10)
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_NUMPAR1 = space(31)
  w_NEWCON = space(15)
  w_NEWVAL = space(5)
  w_NEWSCA = ctod("  /  /  ")
  w_TIPCON = space(1)
  * --- WorkFile variables
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  SCA_VARI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Due funzioni legate alla maschera manutenzione partite..
    *     a) lancia la divisione scadenze, in questo caso il batch utilzza var. caller ed eseguesu di esse assegnameni
    *     b) Lancio dettagli Partita dipende solo dai parametri
    *     c) Lancio della gestione dalla quale la partita � nata (questa parte � dipendente solo dai parametri)
    * --- Parametri:
    *     pSerial     = Seriale Partita
    *     pTipMov= Coincide con pRowOrd, se origine distinta=-2
    *     pRownum= CPROWNUM partita
    *     pResiduo = Gestione Dividi scadenze da Dettaglio
    *     pRowOrd  = CPROWORD Partita
    *     pTipMan  = Funzione da assolvere
    * --- Funzione da assolvere
    *     pTipMan = 2  Divisione partite
    *     pTipMan = 1  Dettagli Partita
    *     pTipMan = 0 Origine Partita
    if EMPTY(this.pSerial) OR this.pTIPMOV=0 OR this.pROWNUM=0 
      ah_ErrorMsg("Selezionare una riga",,"")
      i_retcode = 'stop'
      return
    endif
    VVP = 20 * g_PERPVL
    * --- Questo oggetto sar� definito come Prima Nota / Manutenzione
    do case
      case this.pTIPMAN=2
        * --- Legge Dati Partita
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTFLSOSP,PTBANAPP,PTNUMEFF,PTNUMDIS,PTROWORD,PTSERRIF"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.pSerial);
                +" and PTROWORD = "+cp_ToStrODBC(this.pTIPMOV);
                +" and CPROWNUM = "+cp_ToStrODBC(this.pROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTFLSOSP,PTBANAPP,PTNUMEFF,PTNUMDIS,PTROWORD,PTSERRIF;
            from (i_cTable) where;
                PTSERIAL = this.pSerial;
                and PTROWORD = this.pTIPMOV;
                and CPROWNUM = this.pROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
          this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
          this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
          this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
          this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
          this.w_PTCODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
          this.w_PTFLSOSP = NVL(cp_ToDate(_read_.PTFLSOSP),cp_NullValue(_read_.PTFLSOSP))
          this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
          this.w_PTNUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
          this.w_PTNUMDIS = NVL(cp_ToDate(_read_.PTNUMDIS),cp_NullValue(_read_.PTNUMDIS))
          this.w_PTROWORD = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
          this.w_PTSERRIF = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_FLSALD1 = this.oParentObject.w_FLSALD
        if this.oParentObject.w_FLSALD="R"
          this.w_PTTOTIMP = this.oParentObject.w_TOTIMP
        endif
        * --- Divisione Scadenze
        do case
          case this.w_PTFLSOSP="S"
            ah_ErrorMsg("Partita sospesa; impossibile dividere",,"")
            i_retcode = 'stop'
            return
          case NOT EMPTY(this.w_PTNUMDIS) OR NOT EMPTY(this.w_PTNUMEFF) OR this.pTIPMOV=-2
            ah_ErrorMsg("Partita presente in distinta; impossibile dividere",,"")
            i_retcode = 'stop'
            return
        endcase
        if CHKPARD(this.w_PTDATSCA, this.w_PTNUMPAR, this.w_PTTIPCON, this.w_PTCODCON, this.w_PTCODVAL,this.w_PTSERIAL,this.w_PTROWORD,this.w_CPROWNUM)>0
          ah_ErrorMsg("Partita presente in distinta; impossibile dividere",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Maschera Divisione Scadenze
        *     Caller passati..
        this.w_PTSERIAL = this.pSerial
        this.w_PTROWORD = this.pTipMov
        this.w_CPROWNUM = this.pRowNum
        this.w_OK = .T.
        if CHKSALP(this.w_PTNUMPAR, this.w_PTDATSCA, this.w_PTTIPCON, this.w_PTCODCON, this.w_PTCODVAL,this.w_PTSERIAL,this.w_PTROWORD,this.w_CPROWNUM)=0
          if NOT ah_YesNo("ATTENZIONE:%0La scadenza selezionata risulta gi� saldata%0Proseguo comunque?")
            i_retcode = 'stop'
            return
          endif
        endif
        do GSTE_KM3 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPMAN=1
        * --- Istanzio l'oggetto
        this.w_PROG = GSTE_MMP()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave della Prima Nota
        this.w_PROG.i_Batch = .t.
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_PTSERIAL = this.pSERIAL
        this.w_PROG.w_PTROWORD = this.pTIPMOV
        this.w_PROG.w_ROWNUM = this.pROWNUM
        this.w_PROG.ecpSave()     
        this.w_PROG.w_RESIDUO = this.pRESIDUO
        this.w_PROG.ecpEdit()     
        this.w_PROG.i_Batch = .f.
      case this.pTIPMAN=0
        do case
          case this.pTIPMOV > 0
            this.w_PROG = GSAR_BZP(This.oparentobject,this.pserial)
          case this.pTIPMOV=-1
            * --- Scadenze Diverse
            * --- Read from SCA_VARI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SCA_VARI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2],.t.,this.SCA_VARI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SCDATDOC"+;
                " from "+i_cTable+" SCA_VARI where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.pSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SCDATDOC;
                from (i_cTable) where;
                    SCCODICE = this.pSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DATDOC = NVL(cp_ToDate(_read_.SCDATDOC),cp_NullValue(_read_.SCDATDOC))
              use
              if i_Rows=0
                ah_ErrorMsg("Scadenza diversa inesistente","!","")
                i_retcode = 'stop'
                return
              endif
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Isalt() and g_COGE<>"S"
              this.w_PROG = GSAR_ASC("A")
            else
              this.w_PROG = GSAR_ASC("S")
            endif
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- inizializzo la chiave della Prima Nota
            this.w_PROG.w_SCCODICE = this.pSERIAL
            * --- creo il curosre delle solo chiavi
            this.w_PROG.QueryKeySet("SCCODICE='"+this.pSERIAL+ "'","")     
          case this.pTIPMOV = -2
            * --- Reg. di prima nota da distinta ?
            if this.pROWORD>0
              * --- Read from PNT_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PNRIFDIS"+;
                  " from "+i_cTable+" PNT_MAST where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.pSerial);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PNRIFDIS;
                  from (i_cTable) where;
                      PNSERIAL = this.pSerial;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SERDIS = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if VAL(this.w_SERDIS)<0
              * --- Read from PAR_TITE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMPAR,PTCODCON,PTCODVAL,PTDATSCA,PTTIPCON"+;
                  " from "+i_cTable+" PAR_TITE where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.pSerial);
                      +" and PTROWORD = "+cp_ToStrODBC(this.pROWORD);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.pROWNUM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMPAR,PTCODCON,PTCODVAL,PTDATSCA,PTTIPCON;
                  from (i_cTable) where;
                      PTSERIAL = this.pSerial;
                      and PTROWORD = this.pROWORD;
                      and CPROWNUM = this.pROWNUM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SERRIF = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
                this.w_ORDRIF = NVL(cp_ToDate(_read_.PTORDRIF),cp_NullValue(_read_.PTORDRIF))
                this.w_NUMRIF = NVL(cp_ToDate(_read_.PTNUMRIF),cp_NullValue(_read_.PTNUMRIF))
                this.w_NUMPAR1 = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
                this.w_NEWCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
                this.w_NEWVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
                this.w_NEWSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
                this.w_TIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if NOT EMPTY(this.w_SERRIF)
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL,PTROWORD,CPROWNUM"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTSERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                        +" and PTORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                        +" and PTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                        +" and PTROWORD = "+cp_ToStrODBC(-3);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL,PTROWORD,CPROWNUM;
                    from (i_cTable) where;
                        PTSERRIF = this.w_SERRIF;
                        and PTORDRIF = this.w_ORDRIF;
                        and PTNUMRIF = this.w_NUMRIF;
                        and PTROWORD = -3;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_SERRIF = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  this.w_ORDRIF = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
                  this.w_NUMRIF = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTSERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                        +" and PTORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                        +" and PTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                        +" and PTROWORD = "+cp_ToStrODBC(-2);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL;
                    from (i_cTable) where;
                        PTSERRIF = this.w_SERRIF;
                        and PTORDRIF = this.w_ORDRIF;
                        and PTNUMRIF = this.w_NUMRIF;
                        and PTROWORD = -2;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LSERDIS = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                * --- Se pregresso non ho i riferimenti
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERIAL"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR1);
                        +" and PTDATSCA = "+cp_ToStrODBC(this.w_NEWSCA);
                        +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                        +" and PTROWORD = "+cp_ToStrODBC(-2);
                        +" and PTCODCON = "+cp_ToStrODBC(this.w_NEWCON);
                        +" and PTCODVAL = "+cp_ToStrODBC(this.w_NEWVAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERIAL;
                    from (i_cTable) where;
                        PTNUMPAR = this.w_NUMPAR1;
                        and PTDATSCA = this.w_NEWSCA;
                        and PTTIPCON = this.w_TIPCON;
                        and PTROWORD = -2;
                        and PTCODCON = this.w_NEWCON;
                        and PTCODVAL = this.w_NEWVAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LSERDIS = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if NOT EMPTY(this.w_LSERDIS)
                this.pSERIAL = this.w_LSERDIS
              else
                * --- se non trovo la distinta devo aprire l'indiretta effetti
                this.pSERIAL = this.w_SERDIS
              endif
            else
              this.pSERIAL = IIF(Not Empty(this.w_SERDIS),this.w_SERDIS,this.pSERIAL)
            endif
            if not empty(this.pSERIAL)
              if VAL(this.pSERIAL)<0
                this.pSERIAL = IIF(VAL(this.w_SERDIS)<0, "0"+RIGHT(this.w_SERDIS, 9), this.w_SERDIS) 
                * --- Contabilizzazione Indiretta Effetti
                this.w_PROG = GSCG_ACI()
                * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
                if !(this.w_PROG.bSec1)
                  i_retcode = 'stop'
                  return
                endif
                * --- creo il cursore delle solo chiavi
                this.w_PROG.w_CISERIAL = this.pSERIAL
                this.w_PROG.QueryKeySet("CISERIAL='"+this.pSERIAL+ "'","")     
              else
                * --- Manutenzione Distinte
                this.w_PROG = GSTE_ADI()
                * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
                if !(this.w_PROG.bSec1)
                  i_retcode = 'stop'
                  return
                endif
                * --- creo il cursore delle solo chiavi
                this.w_PROG.w_DINUMDIS = this.pSERIAL
                this.w_PROG.QueryKeySet("DINUMDIS='"+this.pSERIAL+ "'","")     
              endif
            endif
        endcase
        * --- Carico il record
        if Type("This.w_PROG")="O"
          this.w_PROG.LoadRecWarn()     
          i_retcode = 'stop'
          i_retval = this.w_PROG
          return
        endif
      case this.pTIPMAN=3
        * --- Istanzio l'oggetto
        this.w_PROG = GSTE_MMP()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave della Prima Nota
        this.w_PROG.i_Batch = .t.
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_PTSERIAL = this.pSERIAL
        this.w_PROG.w_PTROWORD = this.pTIPMOV
        this.w_PROG.ecpSave()     
        this.w_PROG.i_Batch = .f.
    endcase
  endproc


  proc Init(oParentObject,pSERIAL,pTIPMOV,pROWNUM,pRESIDUO,pTIPMAN,pROWORD)
    this.pSERIAL=pSERIAL
    this.pTIPMOV=pTIPMOV
    this.pROWNUM=pROWNUM
    this.pRESIDUO=pRESIDUO
    this.pTIPMAN=pTIPMAN
    this.pROWORD=pROWORD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='SCA_VARI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pTIPMOV,pROWNUM,pRESIDUO,pTIPMAN,pROWORD"
endproc
