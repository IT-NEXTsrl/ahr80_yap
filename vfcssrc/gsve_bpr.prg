* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bpr                                                        *
*              Batch che lancia la gest. prov                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bpr",oParentObject)
return(i_retval)

define class tgsve_bpr as StdBatch
  * --- Local variables
  w_cond = .f.
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Zoom Gestione Provvigioni (da GSVE_KPR)
    * --- LANCIA AL NOTIFYEVENT (da GSVE_KPR)
    This.oParentObject.NotifyEvent("Esegui")
    * --- Cambio gli importi non in moneta di Conto nel cursore a VIDEO
    this.w_ZOOM = this.oParentObject.w_ZoomProv
    FOR I=1 TO this.w_ZOOM.grd.columncount
    this.w_COND = UPPER(this.w_ZOOM.grd.columns(i).controlsource)="MPTOTIMP" OR UPPER(this.w_ZOOM.grd.columns(i).controlsource)="MPTOTAGE"
    this.w_COND = UPPER(this.w_ZOOM.grd.columns(i).controlsource)="MPTOTZON" OR this.w_COND
    if this.w_COND
      * --- Imposto il Formato
      This.w_Zoom.grd.ColumnS(i).INPUTMASK=v_pv[20*( this.oParentObject.w_DECIMI +2)]
    endif
    ENDFOR
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
