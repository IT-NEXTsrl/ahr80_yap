* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bss                                                        *
*              Stampa schede analitica                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_36]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2014-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bss",oParentObject)
return(i_retval)

define class tgsca_bss as StdBatch
  * --- Local variables
  * --- WorkFile variables
  TMP_ANA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Schede di Analitica (da GSCA_SSA)
    *     GSCA_SCH.VQR => Manuali Originari
    *     GSCA1SCH.VQR => Primanota Originari
    *     GSCA4SCH.VQR => Documenti Originari
    *     GSCA3SCH.VQR => Manuali Ripartiti
    *     GSCA2SCH.VQR => Primanota Ripartiti
    *     GSCA5SCH.VQR => Documenti Ripartiti
    * --- Flgmov: (E=Effettivo - P=Previsionale)
    * --- Prove: (T=Tutti - O=Originari - E=Escluso Ripartiti)
    * --- Provconf: (N=Confermato - S=Provvisorio)
    * --- Creazione Tabella Temporanea
    * --- Create temporary table TMP_ANA
    i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsca_sch',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ANA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Esecuzione Query Output Utente (su Tabella Temporanea)
    vq_exec(alltrim(this.oParentObject.w_OQRY),this,"__tmp__")
    * --- Variabili utilizzate nel Report
    L_TIPOCF=this.oParentObject.w_TIPOCF
    L_CODCON1=this.oParentObject.w_CODCON1
    L_PNCODCON=this.oParentObject.w_PNCODCON
    L_COD1=this.oParentObject.w_CODCON
    L_DATA1=this.oParentObject.w_DATA1
    L_COD2=this.oParentObject.w_CODCOF
    L_DATA2=this.oParentObject.w_DATA2
    L_TIPO=this.oParentObject.w_FLGMOV
    L_PROVE=this.oParentObject.w_PROVE
    L_LIVELLO1=this.oParentObject.w_INILIV
    L_SIMBOLO=this.oParentObject.w_SIMBOLO
    L_LIVELLO2=this.oParentObject.w_FINLIV
    L_CODVOC=this.oParentObject.w_CODVOC
    L_CODVOC1=this.oParentObject.w_CODVOC1
    L_CODCOM=this.oParentObject.w_CODCOM
    L_CODCOM1=this.oParentObject.w_CODCOM1
    L_COMMESSA="Scheda Analitica"+iif(empty(this.oParentObject.w_CODCOM),""," Commessa: "+IIF(EMPTY(this.oParentObject.w_DESPIA2),this.oParentObject.w_CODCOM,this.oParentObject.w_DESPIA2))
    * --- Lancio Stampa Output Utente
    CP_CHPRN( ALLTRIM(this.oParentObject.w_OREP), " ", this )
    * --- Eliminazione Tabella Temporanea
    * --- Drop temporary table TMP_ANA
    i_nIdx=cp_GetTableDefIdx('TMP_ANA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ANA')
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_ANA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
