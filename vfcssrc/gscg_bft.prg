* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bft                                                        *
*              Generazione file  F24                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-07                                                      *
* Last revis.: 2013-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bft",oParentObject)
return(i_retval)

define class tgscg_bft as StdBatch
  * --- Local variables
  w_INDICE = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cotrollo sui tributi ICI
    * --- Select from gscg_bct
    do vq_exec with 'gscg_bct',this,'_Curs_gscg_bct','',.f.,.t.
    if used('_Curs_gscg_bct')
      select _Curs_gscg_bct
      locate for 1=1
      do while not(eof())
      CHKER=0
      this.w_INDICE = 1
      dimension dati(4,5) 
 chker=0
      * --- Crea l array contenente i tributi ICI da passare alla funzione di controllo
      do while this.w_INDICE<=4
         camp1="IFCODEL"+alltrim(str(this.w_INDICE)) 
 camp2="IFTRIEL"+alltrim(str(this.w_INDICE)) 
 camp3="TIPTRI"+alltrim(str(this.w_INDICE)) 
 camp4="IFIMDEL"+alltrim(str(this.w_INDICE)) 
 camp5="IFIMCEL"+alltrim(str(this.w_INDICE)) 
 dati(this.w_INDICE,1)=&camp1 
 dati(this.w_INDICE,2)=&camp2 
 dati(this.w_INDICE,3)=&camp3 
 dati(this.w_INDICE,4)=&camp4 
 dati(this.w_INDICE,5)=&camp5
        this.w_INDICE = this.w_INDICE + 1
      enddo
      * --- Funzione di controllo correttezza  tributi ICI inseriti
       GSTE_BCT (this, @dati , @CHKER)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if CHKER<>0
        AH_ERRORMSG("Operazione abbandonata",48)
        i_retcode = 'stop'
        return
      endif
        select _Curs_gscg_bct
        continue
      enddo
      use
    endif
    * --- Generazione Flusso Entratel
    if this.oParentObject.w_TIPOFLU="E"
      GSUT_BFE ( this.oparentobject )
    endif
    * --- Generazione Flusso CBI
    if this.oParentObject.w_TIPOFLU="N"
      GSUT_BFA (this.oparentobject)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gscg_bct')
      use in _Curs_gscg_bct
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
