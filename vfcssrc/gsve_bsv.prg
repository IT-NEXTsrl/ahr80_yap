* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bsv                                                        *
*              Stampa versamenti agenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-16                                                      *
* Last revis.: 2000-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bsv",oParentObject)
return(i_retval)

define class tgsve_bsv as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_SCELSTAM = "F"
        vx_exec("QUERY\GSVE_SFI.VQR",this)
      case this.oParentObject.w_SCELSTAM = "P"
        vx_exec("QUERY\GSVE_SCP.VQR",this)
      case this.oParentObject.w_SCELSTAM = "A"
        vx_exec("QUERY\GSVE_SCA.VQR",this)
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
