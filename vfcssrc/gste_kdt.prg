* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kdt                                                        *
*              Dati aggiuntivi primanota                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-26                                                      *
* Last revis.: 2012-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kdt",oParentObject))

* --- Class definition
define class tgste_kdt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 365
  Height = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-26"
  HelpContextID=82457705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cPrg = "gste_kdt"
  cComment = "Dati aggiuntivi primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod('  /  /  ')
  w_PNAGG_06 = ctod('  /  /  ')
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_CAMAGG01 = .NULL.
  w_CAMAGG02 = .NULL.
  w_CAMAGG03 = .NULL.
  w_CAMAGG04 = .NULL.
  w_CAMAGG05 = .NULL.
  w_CAMAGG06 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kdtPag1","gste_kdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNAGG_01_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CAMAGG01 = this.oPgFrm.Pages(1).oPag.CAMAGG01
    this.w_CAMAGG02 = this.oPgFrm.Pages(1).oPag.CAMAGG02
    this.w_CAMAGG03 = this.oPgFrm.Pages(1).oPag.CAMAGG03
    this.w_CAMAGG04 = this.oPgFrm.Pages(1).oPag.CAMAGG04
    this.w_CAMAGG05 = this.oPgFrm.Pages(1).oPag.CAMAGG05
    this.w_CAMAGG06 = this.oPgFrm.Pages(1).oPag.CAMAGG06
    DoDefault()
    proc Destroy()
      this.w_CAMAGG01 = .NULL.
      this.w_CAMAGG02 = .NULL.
      this.w_CAMAGG03 = .NULL.
      this.w_CAMAGG04 = .NULL.
      this.w_CAMAGG05 = .NULL.
      this.w_CAMAGG06 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNAGG_01=space(15)
      .w_PNAGG_02=space(15)
      .w_PNAGG_03=space(15)
      .w_PNAGG_04=space(15)
      .w_PNAGG_05=ctod("  /  /  ")
      .w_PNAGG_06=ctod("  /  /  ")
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_PNAGG_01=oParentObject.w_PNAGG_01
      .w_PNAGG_02=oParentObject.w_PNAGG_02
      .w_PNAGG_03=oParentObject.w_PNAGG_03
      .w_PNAGG_04=oParentObject.w_PNAGG_04
      .w_PNAGG_05=oParentObject.w_PNAGG_05
      .w_PNAGG_06=oParentObject.w_PNAGG_06
      .w_DACAM_01=oParentObject.w_DACAM_01
      .w_DACAM_02=oParentObject.w_DACAM_02
      .w_DACAM_03=oParentObject.w_DACAM_03
      .w_DACAM_04=oParentObject.w_DACAM_04
      .w_DACAM_05=oParentObject.w_DACAM_05
      .w_DACAM_06=oParentObject.w_DACAM_06
      .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
      .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
    this.DoRTCalc(1,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNAGG_01=.w_PNAGG_01
      .oParentObject.w_PNAGG_02=.w_PNAGG_02
      .oParentObject.w_PNAGG_03=.w_PNAGG_03
      .oParentObject.w_PNAGG_04=.w_PNAGG_04
      .oParentObject.w_PNAGG_05=.w_PNAGG_05
      .oParentObject.w_PNAGG_06=.w_PNAGG_06
      .oParentObject.w_DACAM_01=.w_DACAM_01
      .oParentObject.w_DACAM_02=.w_DACAM_02
      .oParentObject.w_DACAM_03=.w_DACAM_03
      .oParentObject.w_DACAM_04=.w_DACAM_04
      .oParentObject.w_DACAM_05=.w_DACAM_05
      .oParentObject.w_DACAM_06=.w_DACAM_06
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page1.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CAMAGG01.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG02.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG03.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG04.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG05.Event(cEvent)
      .oPgFrm.Page1.oPag.CAMAGG06.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNAGG_01_1_1.value==this.w_PNAGG_01)
      this.oPgFrm.Page1.oPag.oPNAGG_01_1_1.value=this.w_PNAGG_01
    endif
    if not(this.oPgFrm.Page1.oPag.oPNAGG_02_1_2.value==this.w_PNAGG_02)
      this.oPgFrm.Page1.oPag.oPNAGG_02_1_2.value=this.w_PNAGG_02
    endif
    if not(this.oPgFrm.Page1.oPag.oPNAGG_03_1_3.value==this.w_PNAGG_03)
      this.oPgFrm.Page1.oPag.oPNAGG_03_1_3.value=this.w_PNAGG_03
    endif
    if not(this.oPgFrm.Page1.oPag.oPNAGG_04_1_4.value==this.w_PNAGG_04)
      this.oPgFrm.Page1.oPag.oPNAGG_04_1_4.value=this.w_PNAGG_04
    endif
    if not(this.oPgFrm.Page1.oPag.oPNAGG_05_1_5.value==this.w_PNAGG_05)
      this.oPgFrm.Page1.oPag.oPNAGG_05_1_5.value=this.w_PNAGG_05
    endif
    if not(this.oPgFrm.Page1.oPag.oPNAGG_06_1_6.value==this.w_PNAGG_06)
      this.oPgFrm.Page1.oPag.oPNAGG_06_1_6.value=this.w_PNAGG_06
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_kdtPag1 as StdContainer
  Width  = 361
  height = 221
  stdWidth  = 361
  stdheight = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNAGG_01_1_1 as StdField with uid="NLDMPNYWUJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PNAGG_01", cQueryName = "PNAGG_01",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 248590119,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=218, Top=3, InputMask=replicate('X',15)

  add object oPNAGG_02_1_2 as StdField with uid="FVGOOVMXGE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PNAGG_02", cQueryName = "PNAGG_02",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 248590120,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=218, Top=28, InputMask=replicate('X',15)

  add object oPNAGG_03_1_3 as StdField with uid="QFPECASIYR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PNAGG_03", cQueryName = "PNAGG_03",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 248590121,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=218, Top=53, InputMask=replicate('X',15)

  add object oPNAGG_04_1_4 as StdField with uid="NUIJEEJKYX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PNAGG_04", cQueryName = "PNAGG_04",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 248590122,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=218, Top=78, InputMask=replicate('X',15)

  add object oPNAGG_05_1_5 as StdField with uid="CAKNRPLCGH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PNAGG_05", cQueryName = "PNAGG_05",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 248590123,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=218, Top=103

  add object oPNAGG_06_1_6 as StdField with uid="UGTDQKRBLY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PNAGG_06", cQueryName = "PNAGG_06",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 248590124,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=218, Top=128


  add object CAMAGG01 as cp_calclbl with uid="JZCKXLHDHV",left=12, top=4, width=202,height=18,;
    caption='Campo aggiuntivo 1',;
   bGlobalFont=.t.,;
    caption="Campo 1",Alignment =1,;
    nPag=1;
    , HelpContextID = 214764436


  add object CAMAGG02 as cp_calclbl with uid="YBFIFIXNYF",left=12, top=30, width=202,height=18,;
    caption='Campo aggiuntivo 2',;
   bGlobalFont=.t.,;
    caption="Campo 2",Alignment =1,;
    nPag=1;
    , HelpContextID = 214764180


  add object CAMAGG03 as cp_calclbl with uid="EELMGPXNWQ",left=12, top=55, width=202,height=18,;
    caption='Campo aggiuntivo 3',;
   bGlobalFont=.t.,;
    caption="Campo 3",alignment =1,;
    nPag=1;
    , HelpContextID = 214763924


  add object CAMAGG04 as cp_calclbl with uid="WWUGRPNAOG",left=12, top=80, width=202,height=18,;
    caption='Campo aggiuntivo 4',;
   bGlobalFont=.t.,;
    caption="Campo 4",alignment =1,;
    nPag=1;
    , HelpContextID = 214763668


  add object CAMAGG05 as cp_calclbl with uid="BLEZPMMBEJ",left=12, top=105, width=202,height=18,;
    caption='Campo aggiuntivo 5',;
   bGlobalFont=.t.,;
    caption="Campo 5",alignment =1,;
    nPag=1;
    , HelpContextID = 214763412


  add object CAMAGG06 as cp_calclbl with uid="ORVYGKKIKW",left=12, top=130, width=202,height=18,;
    caption='Campo aggiuntivo 6',;
   bGlobalFont=.t.,;
    caption="Campo 6",alignment =1,;
    nPag=1;
    , HelpContextID = 214763156


  add object oBtn_1_19 as StdButton with uid="YYYWRVTVKV",left=250, top=165, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 27302634;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="CKNHTSNWUY",left=304, top=165, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 75140282;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kdt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
