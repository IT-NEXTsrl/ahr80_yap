* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_brx                                                        *
*              Emissione bonifici - scrittura                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_215]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2016-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_brx",oParentObject)
return(i_retval)

define class tgste_brx as StdBatch
  * --- Local variables
  w_SepRec = space(2)
  w_DatPre = ctod("  /  /  ")
  w_NomeFile = space(40)
  w_NomeFile5 = space(40)
  w_NomeFile4 = space(40)
  w_NomeFile7 = space(40)
  w_FLIBAN = space(1)
  w_FLQUALI = space(1)
  w_FLSEPA = space(1)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_AziAtt = space(1)
  w_AziSia = space(5)
  w_BanAbi = space(5)
  w_BanCab = space(5)
  w_ConCor = space(12)
  w_CliAbi = space(5)
  w_CliCab = space(5)
  w_CliFis = space(16)
  w_CliCod = space(16)
  w_CLIConCor = space(12)
  w_DatCre = space(6)
  w_DatSca = space(6)
  w_NomSup = space(20)
  w_CodDiv = space(1)
  w_Totale = 0
  w_NumDis = space(7)
  w_NumRec = space(7)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_RifDoc = space(1)
  w_TotDoc = space(1)
  w_CodFis = space(16)
  w_ParIva = space(12)
  w_MESS = space(200)
  w_OK = .f.
  w_CIN = space(1)
  w_CABCLI = space(5)
  w_OK1 = .f.
  w_CLIFIS = space(16)
  w_COFAZI = space(16)
  w_OK3 = .f.
  w_OK4 = .f.
  w_PIVAZI = space(11)
  w_LSERIAL = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SEGNO = space(1)
  w_NUMPAR = space(31)
  w_LDATSCA = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIF = space(40)
  w_BANCA = space(35)
  w_BANCA1 = space(35)
  w_CINABI = space(1)
  w_BBAN = space(30)
  w_BA__IBAN = space(35)
  w_ChekIban = space(2)
  w_AN__IBAN = space(35)
  w_Chek = space(2)
  w_CAUBON = space(5)
  w_CODUNI = space(30)
  w_SERIALE = space(10)
  w_NUMERO = 0
  w_ROWNUM = 0
  w_PTNUMEFF = 0
  w_Cabonurg = space(1)
  w_BANAPP = space(10)
  w_TIPDIS = space(1)
  w_oERRORLOG = .NULL.
  w_ROWEFFE = 0
  w_BLOCCO = .f.
  w_CODCUC = space(8)
  w_PASEUR = space(2)
  w_FLEST = space(1)
  w_LOCALITA = space(254)
  w_PTTOTIMP = 0
  w_IBANSEPA = space(34)
  w_CONTO = space(12)
  w_PAESEBEN = space(2)
  w_IBANDEB = space(34)
  w_DATIAGG = space(254)
  w_DINUMDIS = space(10)
  w_PTRIFDOC = space(140)
  w_TIPRIC = space(1)
  w_TIPORIC = space(1)
  w_PTCODVAL = space(5)
  w_PTBANAPP = space(5)
  w_PTNUMCOR = space(5)
  w_CPROWNUM = 0
  w_STFILXSD = space(254)
  w_MSGVAL = space(0)
  w_FILEXML = space(100)
  w_FLCAU = space(1)
  w_CASTRUTT = space(10)
  w_NOCLOSE = .f.
  * --- WorkFile variables
  ATTIMAST_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  PAR_TITE_idx=0
  PAR_EFFE_idx=0
  VASTRUTT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione file BONIFICI secondo tracciato CBI
    * --- Variabili della maschera
    this.w_SepRec = this.oParentObject.oParentObject.w_SepRec
    this.w_DatPre = this.oParentObject.oParentObject.w_DatPre
    this.w_FLSEPA = this.oParentObject.oParentObject.w_FLSEPA
    this.w_FLIBAN = this.oParentObject.oParentObject.w_FLIBAN
    this.w_FLQUALI = this.oParentObject.oParentObject.w_FLQUALI
    this.w_NomeFile = this.oParentObject.oParentObject.w_NomeFile
    this.w_NomeFile5 = this.oParentObject.oParentObject.w_NomeFile5
    this.w_NomeFile4 = this.oParentObject.oParentObject.w_NomeFile4
    this.w_NomeFile7 = this.oParentObject.oParentObject.w_NomeFile7
    this.w_TIPRIC = this.oParentObject.oParentObject.w_TIPRIC
    * --- Variabili del batch precedente
    * --- Variabili locali
    this.w_Separa = iif(this.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_Decimali = GETVALUT( this.oParentObject.w_Valuta, "VADECTOT" )
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_AziAtt = space(35)
    this.w_AziSia = this.oParentObject.w_CodSia
    this.w_BanAbi = space(5)
    this.w_BanCab = space(5)
    this.w_ConCor = space(12)
    this.w_CliAbi = space(5)
    this.w_CliCab = space(5)
    this.w_CliFis = space(16)
    this.w_CliCod = space(16)
    this.w_CLIConCor = space(12)
    this.w_DatCre = space(6)
    this.w_DatSca = space(6)
    this.w_NomSup = space(20)
    this.w_CodDiv = space(1)
    this.w_Totale = 0
    this.w_NumDis = "0000000"
    this.w_NumRec = "0000000"
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_RifDoc = space(1)
    this.w_TotDoc = space(1)
    this.w_CodFis = space(16)
    this.w_ParIva = space(12)
    this.w_OK = .F.
    this.w_CIN = SPACE(1)
    this.w_CABCLI = SPACE(5)
    this.w_OK3 = .F.
    this.w_OK1 = .F.
    this.w_OK4 = .F.
    this.w_COFAZI = space(16)
    this.w_PIVAZI = "00000000000"
    this.w_CINABI = SPACE(1)
    this.w_BBAN = SPACE(30)
    this.w_BA__IBAN = SPACE(35)
    this.w_ChekIban = space(2)
    this.w_AN__IBAN = SPACE(35)
    this.w_Chek = space(2)
    this.w_CAUBON = SPACE(5)
    this.w_CODUNI = SPACE(30)
    this.w_NUMERO = 0
    this.w_ROWNUM = 0
    this.w_BANCA = this.oParentObject.w_BADESC
    this.w_BANCA1 = STRTRAN(this.w_BANCA, "'", " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1, '"', " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"?" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"!" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"/" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"\" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"*" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"," , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,":" , " ")
    this.w_Cabonurg = iif(This.oParentObject.oParentObject.w_Cabonurg="S","U"," ")
    this.oParentObject.w_GENERA = .F.
    this.w_TIPDIS = this.oParentObject.oParentObject.w_TIPDIS
    this.w_CASTRUTT = this.oParentObject.w_CASTRUTT
    * --- Controllo se nella maschera di generazione flussi � stato attivato il flag "Genera bonifico urgente"
    *     Se � stato attivato posso solo generare distinte con associato un unico effetto
    if this.w_CABONURG="U" And Reccount("__TMP__")>1
      ah_ErrorMsg("Nella distinta sono presenti pi� effetti%0Le distinte di bonifico urgente prevedono un unico effetto%0Generazione annullata",48,"")
      i_retcode = 'stop'
      return
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Creazione file Bonifici
    ah_Msg("Creazione file bonifici...",.T.)
    do case
      case this.oParentObject.w_TIPOBAN="C" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC)+ ALLTRIM(IIF(EMPTY(this.oParentObject.w_COBANC), " ", "\"))
      case this.oParentObject.w_TIPOBAN="D" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1)+ ALLTRIM(IIF(EMPTY(this.w_BANCA1), " ", "\"))
    endcase
    if this.oParentObject.w_TIPOPAG="S" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) +IIF(this.w_TIPDIS<>"BO","\SCT","\BONI"))
        MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) +IIF(this.w_TIPDIS<>"BO","\SCT","\BONI"))
      endif
      this.oParentObject.w_PATHFILE = Addbs(ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(this.w_TIPDIS<>"BO","SCT\","BONI\"))
    endif
    if this.w_TIPDIS="BO"
      if EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        this.w_Handle =fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE1, 0)
      else
        this.w_Handle = fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE2 , 0)
      endif
      if this.w_Handle = -1
        ah_ErrorMsg("Non � possibile creare il file dei bonifici",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Legge Anagrafica Azienda
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura attivita' principale azienda
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDESATT"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(g_CATAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDESATT;
        from (i_cTable) where;
            ATCODATT = g_CATAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AziAtt = NVL(cp_ToDate(_read_.ATDESATT),cp_NullValue(_read_.ATDESATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura degli effetti presenti sul temporaneo e scrittura del file ascii
    this.w_ROWEFFE = 0
    select __tmp__
    go top
    * --- Record - PC (record di testa)
    * --- Codice assegnato dalla Sia all'Azienda Mittente
    this.w_AziSia = left( this.w_AziSia + space(5) , 5)
    * --- Controllo su codice assegnato dalla SIa all'Azienda Mittente (creditrice)
    if empty(NVL(this.w_AziSia,"")) AND this.w_TIPDIS<>"SC"
      this.w_oERRORLOG.AddMsgLog("Codice Sia per azienda mittente non inserito")     
    endif
    this.w_CODCUC = __TMP__.BACODCUC
    if empty(NVL(this.w_CODCUC,"")) AND this.w_TIPDIS="SC"
      this.w_oERRORLOG.AddMsgLog("Codice CUC assegnato all'azienda mittente non inserito")     
      this.w_BLOCCO = .t.
    endif
    * --- Dati banca ricevente
    this.w_CINABI = alltrim(nvl(__tmp__.bacinabi, " "))
    this.w_BanAbi = right("00000" + alltrim(NVL(__tmp__.bacodabi,SPACE(5))), 5)
    this.w_BanCab = right("00000" + alltrim(NVL(__tmp__.bacodcab,SPACE(5))), 5)
    this.w_ConCor = Right("000000000000" + alltrim(NVL(UPPER( __tmp__.baconcor),space(12))) , 12)
    this.w_BA__IBAN = alltrim(nvl(__tmp__.ba__iban, space(35)))
    * --- Dati banca domiciliataria
    this.w_CliAbi = right("00000" + alltrim(NVL(__tmp__.clcodabi,space(5))), 5)
    * --- Controllo su numero del conto corrente 
    * --- Controllo Formale su Numero Conto Corrente
    if EMPTY(alltrim(this.w_ConCor))
      if this.w_OK=.F.
        this.w_oERRORLOG.AddMsgLog("Numero del conto corrente della filiale della banca cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
        this.w_OK = .T.
      endif
    endif
    * --- Controllo su codice banca a cui devono essere inviate le disposizioni
    if this.w_BanAbi="00000" and this.w_FLEST <>"S"
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca a cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
    endif
    * --- Data creazione file
    this.w_DatCre = dtoc(CP_TODATE(this.w_DatPre))
    this.w_DatCre = left(this.w_DatCre,2) + substr(this.w_DatCre,4,2) + right(this.w_DatCre,2)
    * --- Nome supporto
    * --- w_FILDIS - Nome FISICO del file generato, deve essere un valore univoco
    * --- w_NomSup - Nome del supporto di origine scritto nel file (Record EF) deve essere SEMPRE il nome del file generato dalla distinta EFFETTI di origine
    * --- Campo di libera composizione. Deve comunque essere univoco: data,ora,minuti,secondi,SIA
    this.w_NomSup = time()
    this.w_NomSup = substr(this.w_NomSup,1,2) + substr(this.w_NomSup,4,2) + substr(this.w_NomSup,7,2)
    this.w_NomSup = left( this.w_DatCre + this.w_NomSup + this.w_AziSia +space(20), 20)
    this.oParentObject.w_FILDIS = this.w_NomSup
    * --- Codice divisa (valuta)
    this.w_CodDiv = iif( this.oParentObject.w_Valuta=g_PERVAL , "E" , "I")
    * --- Composizione record e scrittura
    this.w_Record = space(1)+"PC"
    this.w_Record = this.w_Record + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup
    this.w_Record = this.w_Record + space(6) + space(59) +IIF(this.w_FLQUALI="S", "1" + "$" + this.w_BanAbi, SPACE(7)) + SPACE(1) +this.w_Cabonurg+ this.w_CodDiv + space(1) + space(5)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ciclo sugli effetti da presentare
    ah_Msg("Scrittura file bonifici...",.T.)
    scan
    this.w_FLEST = __tmp__.BAFLBEST
    this.w_LOCALITA = iif(NVL(__tmp__.DDTIPRIF," ")="PA", alltrim(Nvl(__tmp__.DDPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.DD___CAP," ")) +" "+ alltrim(Nvl(__tmp__.DDLOCALI," "))+" "+alltrim(Nvl(__tmp__.DDINDIRI," ")), Alltrim(Nvl(__tmp__.ANPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.AN___CAP," ")) + " " + alltrim(Nvl(__tmp__.ANLOCALI," "))+" "+alltrim(NVL(__tmp__.ANINDIRI," "))) 
    * --- Non scrivo il messaggio progressivo
    * --- Totale Importi Effetti
    this.w_TOTALE = this.w_TOTALE + NVL(__tmp__.PTTOTIMP,0)
    * --- Record - (10)
    * --- Calcolo numero progressivo disposizione (progressivo Bonifici)
    this.w_NumDis = right( "0000000" + alltrim( str(val(this.w_NumDis)+1) ) , 7)
    this.w_CAUBON = NVL(__TMP__.CACAUBON,SPACE(5))
    * --- Calcolo data scadenza
    if empty(CP_TODATE(__tmp__.DIDATCON))
      this.w_DatSca = space(6)
    else
      this.w_DatSca = dtoc(CP_TODATE(__tmp__.DIDATCON))
      this.w_DatSca = left(this.w_DatSca,2) + substr(this.w_DatSca,4,2) + right(this.w_DatSca,2)
    endif
    * --- Calcolo importo effetto
    this.w_Valore = NVL(__tmp__.PTTOTIMP,0)
    this.w_PTTOTIMP = NVL(__tmp__.PTTOTIMP,0)
    this.w_Lunghezza = 13
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Dati banca domiciliataria
    this.w_CliAbi = right("00000" + alltrim(NVL(__tmp__.clcodabi,space(5))), 5)
    this.w_CliCab = right("00000" + alltrim(NVL(__tmp__.clcodcab,space(5))), 5)
    * --- Controllo su codice ABI della banca ordinante le disposizioni di pagamento
    if this.w_CliAbi="00000" and this.w_FLEST <>"S"
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca a cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
    endif
    * --- Controllo su codice CAB della banca ordinante le disposizioni di pagamento
    if this.w_CliCab="00000" and this.w_FLEST <>"S"
      this.w_oERRORLOG.AddMsgLog("Codice CAB della banca a cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
    endif
    * --- Codice cliente
    this.w_CliCod = left( alltrim(NVL( __tmp__.codclien,space(16))) + space(16) , 16)
    * --- Cerco il conto corrente del fornitore
    this.w_CLIConCor = right("000000000000" + alltrim(NVL(UPPER( __tmp__.PTNUMCOR),space(12))) , 12)
    * --- Controllo Formale su codice CAB Assuntrice.
    if this.w_BanCab="00000" and this.w_FLEST <>"S"
      if this.w_OK=.F.
        this.w_oERRORLOG.AddMsgLog("Codice CAB della banca a cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
        this.w_OK = .T.
      endif
    endif
    * --- Codice Fiscale
    this.w_CodFis = right(alltrim(NVL(__tmp__.ancodfis,space(16))), 16)
    * --- Partita Iva
    this.w_ParIva = right(alltrim(NVL(__tmp__.anpariva,space(12))), 12)
    * --- Controllo segno (non accetto segno DARE perch� Note di Credito)
    if NVL(__tmp__.PTTOTIMP,0) < 0
      if this.w_OK=.F.
        this.w_oERRORLOG.AddMsgLog("Disposizione per il debitore: %1 con importo negativo %2", alltrim(__tmp__.andescri), str(NVL(__tmp__.PTTOTIMP,0)) )     
        this.w_OK = .T.
      endif
    endif
    * --- Composizione record e scrittura
    this.w_Record = " 10" + this.w_NumDis + space(6) + space(6) + this.w_DatSca + this.w_CAUBON + this.w_Valore + "+"
    this.w_Record = this.w_Record + this.w_BanAbi + this.w_BanCab + this.w_ConCor + this.w_CliAbi + this.w_CliCab
    this.w_Record = this.w_Record + this.w_CLIConCor + this.w_AziSia + "5" + this.w_CliCod + "1" + space(4) + this.w_Cabonurg + this.w_CodDiv
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_FLIBAN="S" or this.w_TIPDIS="SC"
      * --- Record - (16)
      * --- Coordinate Ordinante
      * --- Composizione codice BBAN
      * --- Controllo se ho valorizzato il codice IBAN nella tabella conti banche
      if EMPTY(this.w_BA__IBAN)
        this.w_oERRORLOG.AddMsgLog("Codice IBAN dell'ordinante non definito nei conti banche")     
      endif
      if UPPER ( left(this.w_BA__IBAN,2) )<>"IT" And UPPER (left(this.w_BA__IBAN,2) ) <>"SM" AND this.w_FLSEPA="S" and this.w_TIPDIS <> "SC"
        this.w_oERRORLOG.AddMsgLog("Codice paese banca ordinante diverso da 'IT' o diverso da 'SM'")     
      endif
      this.w_PASEUR = SUBSTR(this.w_BA__IBAN, 1,2)
      this.w_BBAN = CALCBBAN(this.w_CINABI, " ", this.w_BANABI, this.w_BANCAB, this.w_CONCOR)
      this.w_PASEUR = iif(empty(this.w_PASEUR),"IT",this.w_PASEUR) 
      this.w_ChekIban = SUBSTR(this.w_BA__IBAN, 3,2)
      this.w_IBANSEPA = this.w_PASEUR+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor
      this.w_Record = " 16" + this.w_NumDis +this.w_PASEUR+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor + space(8) + space(75)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Record - (17)
      * --- Coordinate Beneficiario
      * --- Composizione codice BBAN
      this.w_CONTO = right("000000000000" + alltrim(NVL(UPPER(__tmp__.annumcor),space(12))) , 12)
      this.w_AN__IBAN = ALLTRIM(NVL(__tmp__.an__iban, space(35)))
      this.w_PAESEBEN = SUBSTR(this.w_AN__IBAN, 1,2)
      this.w_Chek = SUBSTR(this.w_AN__IBAN, 3,2)
      this.w_CIN = NVL(__tmp__.ANCINABI, " ")
      this.w_CABCLI = right("00000"+ alltrim(NVL(__tmp__.clcodcab,space(5))), 5)
      this.w_IBANDEB = Nvl(__TMP__.CCCOIBAN," ")
      if UPPER (left(this.w_AN__IBAN,2))<>"IT" AND UPPER (left(this.w_AN__IBAN,2))<>"SM" And this.w_FLSEPA="S" and this.w_TIPDIS <> "SC"
        this.w_oERRORLOG.AddMsgLog("Codice paese banca beneficiario diverso da 'IT' o diverso da 'SM'")     
      endif
      this.w_Record = " 17" + this.w_NumDis +this.w_PAESEBEN+ this.w_Chek + this.w_CIN + this.w_CliAbi +this.w_CABCLI + this.w_CONTO + space(8) + space(75)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Record - (20)
    * --- Controllo Formale su descrizione del creditore.
    if empty(NVL(g_RAGAZI,""))
      if this.w_OK1=.F.
        this.w_oERRORLOG.AddMsgLog("Descrizione del debitore non inserita")     
        this.w_OK1 = .T.
      endif
    endif
    this.w_COFAZI = LEFT((iif(empty(this.w_PIVAZI),this.w_COFAZI,this.w_PIVAZI)+space(16)),16)
    if empty(NVL(this.w_COFAZI,""))
      if this.w_OK1=.F.
        this.w_oERRORLOG.AddMsgLog("Codice fiscale/partita IVA ordinante non inserita")     
        this.w_OK1 = .T.
      endif
    endif
    * --- Composizione record e scrittura
    this.w_Record = " 20" + this.w_NumDis + left(g_RagAzi+space(24),24) + space(6) + left(g_IndAzi+space(24),24) + space(6)
    this.w_Record = this.w_Record + left( trim(g_CapAzi) + " " + trim(g_LocAzi) + " " + trim(g_ProAzi) + space(24) ,24) + space(6)
    this.w_Record = this.w_Record + this.w_COFAZI + space(4)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (30)
    * --- Controllo Formale sul codice fiscale (se persona fisica) oppure su partita IVA.
    if empty(NVL(ANCODFIS,"")) AND empty(NVL(ANPARIVA,""))
      if this.w_OK3=.F.
        this.w_oERRORLOG.AddMsgLog("Codice fiscale/partita IVA del fornitore %1 non inserito", alltrim(__tmp__.andescri))     
        this.w_OK3 = .T.
      endif
    else
      if empty(NVL(ANCODFIS,""))
        this.w_CLIFIS = LEFT(NVL(ANPARIVA,space(16))+ space(16), 16)
      else
        this.w_CLIFIS = LEFT(NVL(ANCODFIS,space(16))+ space(16), 16)
      endif
    endif
    * --- Composizione record e scrittura
    this.w_Record = " 30" + this.w_NumDis + left( NVL(__tmp__.andescri,space(60)) + space(60) , 60)
    this.w_Record = this.w_Record + space(30) + this.w_CliFis + space(4)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (40)
    * --- Controllo su descrizione del cliente debitore
    if empty(NVL(__tmp__.andescri,""))
      this.w_oERRORLOG.AddMsgLog("Descrizione del debitore non inserita")     
    endif
    * --- Composizione record e scrittura
    this.w_Record = " 40"+ this.w_NumDis + left( NVL(__tmp__.anindiri,space(30)) + space(30) ,30)
    if Empty(NVL(__tmp__.an___cap,space(5)))
      this.w_Record = this.w_Record + space(5)
    else
      this.w_Record = this.w_Record + right( "00000" + trim(NVL(__tmp__.an___cap,space(5))) , 5)
    endif
    this.w_Record = this.w_Record + left( NVL(__tmp__.anlocali,space(22)) + space(22) , 22)
    this.w_Record = this.w_Record + " " + left( NVL(__tmp__.anprovin,space(2)) + space(2), 2)
    this.w_Record = this.w_Record + left( NVL(__tmp__.badesban,space(50)) + space(50) , 50)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (50) [non consideriamo la possibilit� di avere records (60)]
    * --- Se � settato il flag ANFLRAGG digito la dicitura 'DOCUMENTI DIVERSI'
    if nvl(__TMP__.MAXAGG,"")<>nvl(__TMP__.MINAGG,"")
      this.w_DATIAGG = ""
    else
      this.w_DATIAGG = __TMP__.DESCRIZ
    endif
    this.w_DINUMDIS = NVL(__TMP__.PTSERIAL, SPACE(10))
    this.w_PTNUMEFF = __TMP__.PTNUMEFF
    this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
    this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
    this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
    this.w_LSERIAL = NVL(__TMP__.PTSERIAL, SPACE(10))
    if NVL(__TMP__.CAFLDSPR, " ")="S"
      this.w_RifDoc = LEFT(this.w_DATIAGG+space(90),90)
      this.w_PTRIFDOC = left(alltrim(this.w_DATIAGG) , 140)
    else
      do case
        case Not Empty(NVL(__TMP__.ANFLRAGG,"")) AND NUMGRUP>1
          this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
          this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
          this.w_RifDoc = ah_Msgformat("DOC:") +" "
          VQ_EXEC("QUERY\GSTE4BRF.VQR",this,"temp1")
          SELECT TEMP1
          GO TOP
          SCAN
          this.w_NUMDOC = NVL(PNNUMDOC, 0)
          this.w_ALFDOC = NVL(PNALFDOC,SPACE(10))
          this.w_DATDOC = CP_TODATE(PNDATDOC)
          this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
          this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp1.pnnumdoc, 0),15,0))
          this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp1.pnalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp1.pnalfdoc,space(10)) ) )
          this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc))+" ", 90)
          this.w_PTRIFDOC = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc))+" ", 140)
          ENDSCAN
          if USED("TEMP1")
            SELECT TEMP1
            USE
          endif
        case NVL(__TMP__.PTFLRAGG, "")="S" 
          if NVL(__TMP__.PNNUMDOC, 0)>0
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(__tmp__.PNNUMDOC, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(__tmp__.PNALFDOC,space(10))) , "", "/" + Alltrim( NVL(__tmp__.PNALFDOC,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC))+" ", 70)
            this.w_PTRIFDOC = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC))+" ", 140)
          else
            this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
            this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
            this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
            this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
            this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PTSERIAL  from "+i_cTable+" PAR_TITE ";
                  +" where PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_SEGNO)+" AND PTROWORD =-1 AND PTFLRAGG='S'";
                  +" order by PTFLRAGG";
                   ,"_Curs_PAR_TITE")
            else
              select PTSERIAL from (i_cTable);
               where PTNUMPAR=this.w_NUMPAR AND PTDATSCA=this.w_LDATSCA AND PTTIPCON=this.w_TIPCON AND PTCODCON=this.w_CODCON AND PT_SEGNO=this.w_SEGNO AND PTROWORD =-1 AND PTFLRAGG="S";
               order by PTFLRAGG;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              this.w_LSERIAL = _Curs_PAR_TITE.PTSERIAL
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            VQ_EXEC("QUERY\GSTE2BRF.VQR",this,"temp")
            SELECT TEMP
            GO TOP
            SCAN
            this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp.ptnumdoc, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp.ptalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp.ptalfdoc,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc))+" ", 90)
            this.w_PTRIFDOC = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc))+" ", 140)
            ENDSCAN
            if USED("TEMP")
              SELECT TEMP
              USE
            endif
          endif
        otherwise
          if EMPTY(NVL(__TMP__.PTDESRIG, SPACE(59)))
            if ALLTRIM(this.w_CAUBON)$"48000-27000"
              this.w_RifDoc = left(ah_Msgformat("Rif. fattura n. %1 del %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(90)), 90) 
              this.w_PTRIFDOC = left(ah_Msgformat("Rif. fattura n. %1 del %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(140)), 140) 
            else
              this.w_RifDoc = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(90)), 90) 
              this.w_PTRIFDOC = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(140)), 140) 
            endif
          else
            this.w_RifDoc = LEFT(__TMP__.PTDESRIG+SPACE(90),90)
            this.w_PTRIFDOC = LEFT(__TMP__.PTDESRIG+SPACE(140),140)
          endif
          * --- Riferimenti documento
          * --- Controllo su numero ricevuta attribuita dal creditore
          if pnnumdoc=0
            this.w_oERRORLOG.AddMsgLog("Numero ricevuta attribuita dal creditore non inserito")     
          endif
      endcase
    endif
    * --- Totale documento
    if Not Empty(this.oParentObject.w_DESCRIZI)
      this.w_TotDoc = ""
    else
      this.w_TotDoc = ah_Msgformat("IMP:")
      this.w_TotDoc = alltrim(this.w_TotDoc) + alltrim(str(NVL(__tmp__.pttotimp,0),18,this.w_Decimali))
    endif
    this.w_RifDoc = left(alltrim(this.w_RifDoc) + this.w_TotDoc + SPACE(90), 90)
    this.w_PTRIFDOC = left(alltrim(this.w_PTRifDoc) + this.w_TotDoc + SPACE(140), 140)
    * --- Composizione record e scrittura
    this.w_Record = " 50" + this.w_NumDis + this.w_RifDoc + space(20)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (70)
    * --- Codice CIN (Anagrafica Clienti/Fornitori)
    this.w_CIN = NVL(__tmp__.ANCINABI, " ")
    this.w_SERIALE = NVL(__TMP__.PTSERIAL,SPACE(10))
    this.w_NUMERO = NVL(__TMP__.DINUMERO, 0)
    this.w_ROWNUM = NVL(__TMP__.CPROWNUM,0)
    this.w_ROWEFFE = this.w_ROWEFFE + 1
    this.w_CODUNI = ALLTRIM(STR(this.w_NUMERO)+this.w_SERIALE+ALLTRIM(STR(this.w_ROWNUM)))
    this.w_CODUNI = left( this.w_CODUNI + space(30) , 30)
    * --- Controllo sul Codice CIN Coordinate Bancarie
    if empty(NVL(__TMP__.ANCINABI,"")) and this.w_TIPDIS <> "SC"
      if this.w_OK4=.F.
        this.w_oERRORLOG.AddMsgLog("CIN di controllo delle coordinate bancarie del cliente/fornitore: %1 non inserito (anagrafica clienti/fornitori)", alltrim(__tmp__.andescri))     
        this.w_OK4 = .T.
      endif
    endif
    * --- Composizione record e scrittura
    this.w_TIPORIC = iif(this.w_TIPRIC="0"," ",this.w_TIPRIC)
    this.w_Record = " 70" + this.w_NumDis + space(59) +this.w_TIPORIC+ this.w_CODUNI+SPACE(10) +this.w_CIN +" "+ SPACE(8)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- DA METTRE INSERT SE FACCIO XML
    if this.w_TIPDIS = "SC"
      this.w_PTCODVAL = __TMP__.PTCODVAL
      this.w_PTNUMCOR = __TMP__.PTNUMCOR
      this.w_PTBANAPP = __TMP__.BANAPP
      this.w_CPROWNUM = __tmp__.Cprownum
      * --- Try
      local bErr_04886510
      bErr_04886510=bTrsErr
      this.Try_04886510()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04886510
      * --- End
    endif
    endscan
    * --- Record - EF (Record di coda)
    * --- Totale importi negativi (totale delle disposizioni : somma importi Ri.Ba)
    this.w_Valore = this.w_Totale
    this.w_Lunghezza = 15
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_Totale = this.w_Valore
    * --- Numero disposizioni (numero totale delle Ri.Ba)
    if this.w_FLIBAN="S" or this.w_TIPDIS="SC"
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * 8) + 2 , 7, 0) ) , 7)
    else
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * 6) + 2 , 7, 0) ) , 7)
    endif
    * --- Composizione record e scrittura
    this.w_Record = space(1)
    this.w_Record = space(1) + "EF" + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup + space(6)
    this.w_Record = this.w_Record + this.w_NumDis + repl("0",15) + this.w_Totale + this.w_NumRec + space(23) + this.w_Cabonurg
    this.w_Record = this.w_Record + this.w_CodDiv + space(6)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    NAMEFILE=ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(EMPTY(ALLTRIM(this.w_NOMEFILE5)), ALLTRIM(this.w_NOMEFILE4), ALLTRIM(this.w_NOMEFILE7))
    * --- Try
    local bErr_048A1338
    bErr_048A1338=bTrsErr
    this.Try_048A1338()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_048A1338
    * --- End
    if this.w_TIPDIS="BO"
      this.w_NOCLOSE = .not. fclose(this.w_Handle)
    else
      this.w_NOCLOSE = .F.
    endif
    if this.w_NOCLOSE OR this.w_oErrorLog.IsFullLog()
      if this.w_TIPDIS="BO"
        this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File bonifici generato con errori%0Stampo situazione messaggi di errore?")     
      else
        this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File Sepa Credit Transfer con errori%0Stampo situazione messaggi di errore?")     
      endif
      if NOT ah_YesNo("Mantengo comunque il file generato?")
        DELETE FILE (NAMEFILE)
        this.oParentObject.w_GENERA = .T.
        this.oParentObject.w_FILE=" "
      endif
    else
      if this.w_TIPDIS="BO"
        ah_ErrorMsg("Generazione file bonifici terminata","","")
      else
        ah_ErrorMsg("Generazione Sepa Credit Transfer","","")
      endif
    endif
  endproc
  proc Try_04886510()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_EFFE
    i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_EFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTTOTIMP"+",PTCODVAL"+",PTBANAPP"+",PTNUMEFF"+",PTDATPRE"+",PTRIFDOC"+",PTFLIBAN"+",PTNUMCOR"+",PTIBADEB"+",PTBONURG"+",PTIBANSE"+",PTMESGID"+",PTLOCALI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DINUMDIS),'PAR_EFFE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-2),'PAR_EFFE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWEFFE),'PAR_EFFE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LDATSCA),'PAR_EFFE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_EFFE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_EFFE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_EFFE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_EFFE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_EFFE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_EFFE','PTNUMEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DatPre),'PAR_EFFE','PTDATPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTRifDoc),'PAR_EFFE','PTRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLIBAN),'PAR_EFFE','PTFLIBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_EFFE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANDEB),'PAR_EFFE','PTIBADEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Cabonurg),'PAR_EFFE','PTBONURG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANSEPA),'PAR_EFFE','PTIBANSE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FILDIS),'PAR_EFFE','PTMESGID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCALITA),'PAR_EFFE','PTLOCALI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_DINUMDIS,'PTROWORD',-2,'CPROWNUM',this.w_ROWEFFE,'PTDATSCA',this.w_LDATSCA,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTBANAPP',this.w_PTBANAPP,'PTNUMEFF',this.w_PTNUMEFF,'PTDATPRE',this.w_DatPre,'PTRIFDOC',this.w_PTRifDoc)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTBANAPP,PTNUMEFF,PTDATPRE,PTRIFDOC,PTFLIBAN,PTNUMCOR,PTIBADEB,PTBONURG,PTIBANSE,PTMESGID,PTLOCALI &i_ccchkf. );
         values (;
           this.w_DINUMDIS;
           ,-2;
           ,this.w_ROWEFFE;
           ,this.w_LDATSCA;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTBANAPP;
           ,this.w_PTNUMEFF;
           ,this.w_DatPre;
           ,this.w_PTRifDoc;
           ,this.w_FLIBAN;
           ,this.w_PTNUMCOR;
           ,this.w_IBANDEB;
           ,this.w_Cabonurg;
           ,this.w_IBANSEPA;
           ,this.oParentObject.w_FILDIS;
           ,this.w_LOCALITA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048A1338()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Generazione XML
    if this.w_TIPDIS = "SC" AND ! this.w_BLOCCO
      this.w_FILEXML = FORCEEXT(ADDBS(JUSTPATH(Alltrim(NAMEFILE)))+Juststem(Alltrim(NAMEFILE)),"xml")
      if Not Empty(this.w_CASTRUTT)
        GSTE_BGF(this,this.w_DINUMDIS,this.w_CASTRUTT,this.w_FILEXML)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if ! cp_fileexist(this.w_FILEXML)
          this.w_oERRORLOG.AddMsgLog("Generazione file Xml non avvenuta")     
        else
          * --- Read from VASTRUTT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VASTRUTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "STFILXSD"+;
              " from "+i_cTable+" VASTRUTT where ";
                  +"STCODICE = "+cp_ToStrODBC(this.w_CASTRUTT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              STFILXSD;
              from (i_cTable) where;
                  STCODICE = this.w_CASTRUTT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STFILXSD = NVL(cp_ToDate(_read_.STFILXSD),cp_NullValue(_read_.STFILXSD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MSGVAL = " "
          if g_ISONAZ="ITA"
            this.w_MSGVAL = ValidateXml(this.w_FILEXML,Fullpath(this.w_STFILXSD))
          endif
          if Not Empty(this.w_MSGVAL)
            this.w_oERRORLOG.AddMsgLog("Errore di validazione XML: %1",Alltrim(this.w_MSGVAL))     
          endif
        endif
      else
        this.w_oERRORLOG.AddMsgLog("Codice Strutta CBI/SEPA non presente nella causale distinta associata")     
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    if this.w_TIPDIS = "SC"
      * --- Delete from PAR_EFFE
      i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_DINUMDIS);
               )
      else
        delete from (i_cTable) where;
              PTSERIAL = this.w_DINUMDIS;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    * --- Scrittura record su File BONI
    if this.w_TIPDIS="BO"
      w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato Bonifici
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- E' richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='PAR_TITE'
    this.cWorkTables[5]='PAR_EFFE'
    this.cWorkTables[6]='VASTRUTT'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
