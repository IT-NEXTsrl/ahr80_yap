* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bfb                                                        *
*              Generazione fabbisogni/PDA                                      *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_190]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-19                                                      *
* Last revis.: 2018-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bfb",oParentObject,m.pOPER)
return(i_retval)

define class tgsac_bfb as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_DATEVA = ctod("  /  /  ")
  w_FLORDI = space(1)
  w_CRIFOR = space(1)
  w_FLIMPE = space(1)
  w_QTAMOV = 0
  w_FBROWNUM = 0
  w_APPO = space(20)
  w_APPO1 = 0
  w_FBSERIAL = space(6)
  w_FBDATREG = ctod("  /  /  ")
  w_CODART = space(20)
  w_FABNET = 0
  w_FLSCOK = .f.
  w_DISPON = 0
  w_UNIMIS = space(3)
  w_TIPART = space(1)
  w_SCOMIN = 0
  w_PUNRIO = 0
  w_LOTRIO = 0
  w_GIOAPP = 0
  w_GIOCOP = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODMAG = space(5)
  w_MAGPRE = space(5)
  w_CODFOR = space(15)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_NUMRIF = 0
  w_CODATT = space(15)
  w_CODCEN = space(15)
  w_CODCOM = space(15)
  w_TIPATT = space(1)
  w_PerGio = 0
  w_PerSet = 0
  w_PerMes = 0
  w_PerTri = 0
  w_OKAPPO = 0
  w_SCRIVIORD = .f.
  w_CODICE = space(20)
  w_PPMAGPRO = space(5)
  w_PP_CICLI = space(1)
  w_EVAGIO = 0
  w_EVASCA = ctod("  /  /  ")
  w_PPNOSCSM = space(1)
  w_GRUMER = space(5)
  w_FLCOMM = space(1)
  w_FOUND = .f.
  GSAC_KGF = .NULL.
  w_PPFLAROB = space(1)
  w_PPCALSTA = space(5)
  w_OrderBy = space(10)
  TipoCalc = space(1)
  DataDiBase = ctod("  /  /  ")
  w_ISARTCOM = .f.
  w_OLDCOM = space(1)
  w_ALTROART = .f.
  w_OLDCODICE = space(20)
  w_CODARTVAR = space(20)
  w_DATEVA1 = ctod("  /  /  ")
  DatRisul = ctod("  /  /  ")
  DatInput = space(8)
  w_DatErr = .f.
  w_DatErrMsg = space(10)
  w_TipoPianificazione = space(1)
  w_LeadT = 0
  w_Find = .f.
  GSAC_KGF = .NULL.
  ZOOMMAGA = .NULL.
  ZOOMART = .NULL.
  w_SELZZART = .f.
  w_SELZZMAG = .f.
  w_SELZZGMG = .f.
  w_SELART = space(1)
  w_NR_R_ART = 0
  w_NRARTSEL = 0
  w_SELMAG = space(1)
  w_NR_R_MAG = 0
  w_NRMAGSEL = 0
  w_SERIALE = space(10)
  w_TABLE = space(10)
  w_GESTPROG = space(13)
  w_PDCATINI = space(5)
  w_PDCATFIN = space(5)
  w_PDCODINI = space(41)
  w_PDCODFIN = space(41)
  w_PDFAMAIN = space(5)
  w_PDFAMAFI = space(5)
  w_PDGRUINI = space(5)
  w_PDGRUFIN = space(5)
  w_PDMAGINI = space(5)
  w_PDMAGFIN = space(5)
  w_PDMARINI = space(5)
  w_PDMARFIN = space(5)
  w_PDCOMINI = space(5)
  w_PDCOMFIN = space(5)
  w_ELAPDF = space(1)
  w_ELAPDS = space(1)
  w_ELASIM = space(1)
  w_STAORD = space(1)
  w_FILTROCOM = space(1)
  * --- WorkFile variables
  FABBIDET_idx=0
  PAR_PROD_idx=0
  ART_ICOL_idx=0
  SALDIART_idx=0
  GRUDMAG_idx=0
  TMPSELART_idx=0
  TMPGRUDMAG_idx=0
  TMP__PDA_idx=0
  TMPSALDICOMM_idx=0
  MAGA_TEMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Fabbisogni/PDA (da GSAC_KGF)
    * --- Inoltre puo' essere eseguito da GSMR_BGP - Generazione Fabbisogni x produzione
    this.w_FBSERIAL = "000001"
    this.w_FBDATREG = this.oParentObject.w_DATREG
    this.w_CODART = " "
    this.w_FABNET = 0
    this.w_FLSCOK = .F.
    this.w_UNIMIS = " "
    this.w_DISPON = 0
    this.w_TIPART = "F"
    this.w_SCOMIN = 0
    this.w_PUNRIO = 0
    this.w_LOTRIO = 0
    this.w_GIOAPP = 0
    this.w_NUMDOC = 0
    this.w_ALFDOC = Space(10)
    this.w_DATDOC = cp_CharToDate("  -  -  ")
    this.w_TIPCON = " "
    this.w_CODCON = SPACE(15)
    this.w_CODMAG = SPACE(5)
    this.w_SERRIF = SPACE(10)
    this.w_ROWRIF = 0
    this.w_NUMRIF = 0
    this.w_CODCOM = SPACE(15)
    this.w_TIPATT = "A"
    this.w_CODATT = SPACE(15)
    this.w_CODCEN = SPACE(15)
    this.w_PerGio = 0
    this.w_PerSet = 0
    this.w_PerMes = 0
    this.w_PerTri = 0
    this.w_OKAPPO = 0
    this.w_CRIFOR = this.oParentObject.w_MCRIFOR
    this.w_PPMAGPRO = SPACE(5)
    this.w_PP_CICLI = " "
    this.w_PPNOSCSM = " "
    * --- Blocca l'elaborazione
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPBLOMRP ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PROD','PPBLOMRP');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("AA");
          +" and PPBLOMRP <> "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          PPBLOMRP = "S";
          &i_ccchkf. ;
       where;
          PPCODICE = "AA";
          and PPBLOMRP <> "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_Rows <= 0
      ah_errormsg("Flag di blocco PDA attivato: elaborazione abortita!",16)
      this.oParentObject.ecpQuit()     
      i_retcode = 'stop'
      return
    endif
    * --- Verifica Presenza Parametri Bidoni Temporali
    this.w_EVAGIO = 0
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPMAGPRO,PP_CICLI,PP___DTF,PPNOSCSM,PPCALSTA,PPFLAROB"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("AA");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPNUMGIO,PPNUMSET,PPNUMMES,PPNUMTRI,PPMAGPRO,PP_CICLI,PP___DTF,PPNOSCSM,PPCALSTA,PPFLAROB;
        from (i_cTable) where;
            PPCODICE = "AA";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PerGio = NVL(cp_ToDate(_read_.PPNUMGIO),cp_NullValue(_read_.PPNUMGIO))
      this.w_PerSet = NVL(cp_ToDate(_read_.PPNUMSET),cp_NullValue(_read_.PPNUMSET))
      this.w_PerMes = NVL(cp_ToDate(_read_.PPNUMMES),cp_NullValue(_read_.PPNUMMES))
      this.w_PerTri = NVL(cp_ToDate(_read_.PPNUMTRI),cp_NullValue(_read_.PPNUMTRI))
      this.w_PPMAGPRO = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_PP_CICLI = NVL(cp_ToDate(_read_.PP_CICLI),cp_NullValue(_read_.PP_CICLI))
      this.w_EVAGIO = NVL(cp_ToDate(_read_.PP___DTF),cp_NullValue(_read_.PP___DTF))
      this.w_PPNOSCSM = NVL(cp_ToDate(_read_.PPNOSCSM),cp_NullValue(_read_.PPNOSCSM))
      this.w_PPCALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.w_PPFLAROB = NVL(cp_ToDate(_read_.PPFLAROB),cp_NullValue(_read_.PPFLAROB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Elabora Bidoni Temporali (Time Buckets)
    ah_Msg("Elaborazione time buckets...")
    do GSAC_BCB with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se non � vuoto il calendario lo utilizzo
    if NOT EMPTY(this.w_PPCALSTA)
      * --- Gestione calendario (parametri PDA)
      this.w_OrderBy = "CAL_AZIE.CAGIORNO ASC"
      vq_exec("QUERY\GSAC6BFB", this, "Calendario")
      select cagiorno, tpperass, dtos(cagiorno) as datidx, canumore>0 as giolav from Calendario into cursor Calend nofilter 
 index on datidx to datidx for giolav 
 use in Calendario
      Select Calend 
 reccount() 
 go top
      this.w_OrderBy = ""
    endif
    * --- Se gestito lo Scaduto Legge anche i Documenti con data Antecendente <n> Giorni la Data Odierna
    this.w_EVASCA = this.w_FBDATREG - IIF(this.w_PP_CICLI="S", this.w_EVAGIO, 0)
    if this.w_PerGio * this.w_PerSet * this.w_PerMes * this.w_PerTri = 0
      this.w_APPO = "Verificare parametri time buckets. Impossibile creare orizzonte temporale"
      ah_ErrorMsg(this.w_APPO,"STOP","")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.w_PPMAGPRO)
      this.w_APPO = "Magazzino di default per il sottoscorta non definito nei parametri"
      ah_ErrorMsg(this.w_APPO,"STOP","")
      i_retcode = 'stop'
      return
    endif
    * --- Gestione filtri di selezione
    this.GSAC_KGF = This.oParentObject
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Eliminazione elaborazione precedente
    ah_Msg("Eliminazione elaborazione precedente...",.T.)
    * --- Try
    local bErr_044C64F8
    bErr_044C64F8=bTrsErr
    this.Try_044C64F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile eliminare gli archivi del fabbisogno")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_044C64F8
    * --- End
    * --- Verifico se esiste almeno un codice gestito a commessa
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARFLCOMM = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ARFLCOMM = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ISARTCOM = i_rows > 0
    this.w_OLDCOM = IIF(VARTYPE(g_PDACOM)="L" AND g_PDACOM, "S", "N")
    if this.w_ISARTCOM
      * --- Creo tabella temporanea per calcolo fabbisogni
      * --- Inserisco nella tabella temporanea solo gli articoli gestiti a commessa
      *     che mi consente di poter calcolare i saldi solo per le commesse
      ah_Msg("Calcolo saldi per articoli personalizzati...")
      * --- Create temporary table TMP__PDA
      i_nIdx=cp_AddTableDef('TMP__PDA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_ISGHMUHNCJ[2]
      indexes_ISGHMUHNCJ[1]='SLCODICE,CRIELA,ARMAGPRE,GRCODMAG'
      indexes_ISGHMUHNCJ[2]='ARDTOBSO'
      vq_exec('GSAC0BFBC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ISGHMUHNCJ,.f.)
      this.TMP__PDA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Creo la strutttura della tabella dei saldi per commessa
      if this.w_OLDCOM="S"
        * --- Create temporary table TMPSALDICOMM
        i_nIdx=cp_AddTableDef('TMPSALDICOMM') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_QZONRWKJVL[2]
        indexes_QZONRWKJVL[1]='SCKEYSAL'
        indexes_QZONRWKJVL[2]='SCCODAR,SCCODMAG,SCCODCOM'
        vq_exec('QUERY\GSAC0QRC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_QZONRWKJVL,.f.)
        this.TMPSALDICOMM_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Leggo le giacenze dal pegging per gli articoli gestiti a commessa
        * --- Leggo ordinato e impegnato in linea per articoli gestiti a commessa
        if g_PROD="S"
          * --- Insert into TMPSALDICOMM
          i_nConn=i_TableProp[this.TMPSALDICOMM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDICOMM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAC_QRC",this.TMPSALDICOMM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPSALDICOMM
          i_nConn=i_TableProp[this.TMPSALDICOMM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDICOMM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSACPQRC",this.TMPSALDICOMM_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Create temporary table TMPSALDICOMM
        i_nIdx=cp_AddTableDef('TMPSALDICOMM') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_MCOQHXWSWI[2]
        indexes_MCOQHXWSWI[1]='SCKEYSAL'
        indexes_MCOQHXWSWI[2]='SCCODAR,SCCODMAG,SCCODCOM'
        vq_exec('QUERY\GSAC0QRC1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_MCOQHXWSWI,.f.)
        this.TMPSALDICOMM_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      if this.w_FILTROCOM<>"S"
        * --- Inserisco gli articoli non gestiti a commessa
        *     questa operazione viene fatta in due volte perch� mi consente di calcolare
        *     il saldo a commessa ed � pi� leggera
        if this.oParentObject.w_CRIELA $ "A-M"
          * --- Insert into TMP__PDA
          i_nConn=i_TableProp[this.TMP__PDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP__PDA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAC0BFB",this.TMP__PDA_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Drop temporary table TMP__PDA
          i_nIdx=cp_GetTableDefIdx('TMP__PDA')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP__PDA')
          endif
          * --- Create temporary table TMP__PDA
          i_nIdx=cp_AddTableDef('TMP__PDA') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_BIYCEITGRO[2]
          indexes_BIYCEITGRO[1]='SLCODICE,CRIELA,ARMAGPRE,GRCODMAG'
          indexes_BIYCEITGRO[2]='ARDTOBSO'
          vq_exec('GSAC0BFBGC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_BIYCEITGRO,.f.)
          this.TMP__PDA_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Insert into TMP__PDA
          i_nConn=i_TableProp[this.TMP__PDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP__PDA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAC0BFBG",this.TMP__PDA_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    else
      * --- Creo tabella temporanea per calcolo fabbisogni
      if this.oParentObject.w_CRIELA $ "A-M"
        * --- Create temporary table TMP__PDA
        i_nIdx=cp_AddTableDef('TMP__PDA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_LKPLIMFCDT[2]
        indexes_LKPLIMFCDT[1]='SLCODICE,CRIELA,ARMAGPRE,GRCODMAG'
        indexes_LKPLIMFCDT[2]='ARDTOBSO'
        vq_exec('GSAC0BFB',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_LKPLIMFCDT,.f.)
        this.TMP__PDA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMP__PDA
        i_nIdx=cp_AddTableDef('TMP__PDA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_NAOYHGPBFY[2]
        indexes_NAOYHGPBFY[1]='SLCODICE,CRIELA,ARMAGPRE,GRCODMAG'
        indexes_NAOYHGPBFY[2]='ARDTOBSO'
        vq_exec('GSAC0BFBG',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_NAOYHGPBFY,.f.)
        this.TMP__PDA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Inizio Elaborazione
    * --- Creazione cursore articoli compresi in inventario
    ah_Msg("Lettura saldi magazzino e documenti...")
    if this.w_ISARTCOM
      if this.w_OLDCOM="S"
        vq_exec("query\GSACCBFB", this, "DETTFABB")
      else
        vq_exec("query\GSACCBFB2", this, "DETTFABB")
      endif
    else
      vq_exec("query\GSAC_BFB", this, "DETTFABB")
    endif
    if g_PROD="S"
      * --- Se Presente Modulo Produzione, aggiunge il Fabbisogno Proveniente dagli ODL
      ah_Msg("Lettura dati da ODL...")
      vq_exec("..\COLA\EXE\QUERY\GSCO_BFB", this, "DETTODL")
      Select * from DETTFABB UNION ALL Select * from DETTODL INTO CURSOR DETTFABB ORDER BY CODART, CODMAG, CODCOM, TIPREC DESC, DATEVA DESC, QTAMOV
      USE IN SELECT("DETTODL")
      SELECT DETTFABB
      =wrcursor("DETTFABB")
    endif
    ah_Msg("Lettura dati per valorizzazioni...")
    if this.oParentObject.w_CRIELA="A"
      vq_exec("query\GSACDBFB", this, "DETTVALO")
    else
      vq_exec("query\GSACDBFB1", this, "DETTVALO")
    endif
    * --- Indicizzo il temporaneo per velocizzare la successiva operazione di Locate
    SELECT DETTVALO
    do case
      case this.oParentObject.w_CRIELA="A"
        INDEX ON PRCODART TAG DETTVAL1
      otherwise
        INDEX ON PRCODART+ARMAGPRE TAG DETTVAL1
    endcase
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if reccount("DETTFABB")>0
      this.w_FBROWNUM = 0
      * --- Try
      local bErr_044942E0
      bErr_044942E0=bTrsErr
      this.Try_044942E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'elaborazione fabbisogno")
      endif
      bTrsErr=bTrsErr or bErr_044942E0
      * --- End
      * --- Try
      local bErr_044D3C08
      bErr_044D3C08=bTrsErr
      this.Try_044D3C08()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        AH_ERRORMSG("Errore durante l'elaborazione fabbisogno",16)
      endif
      bTrsErr=bTrsErr or bErr_044D3C08
      * --- End
    endif
    * --- Chiude i Cursori
    if USED("DETTFABB")
      SELECT DETTFABB
      USE
    endif
    if USED("DETTVALO")
      SELECT DETTVALO
      USE
    endif
    if USED("SCORTAMIN")
      SELECT SCORTAMIN
      USE
    endif
    * --- Qui' lanciare l' elaborazione delle PDA
    do GSAC_BGF with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Sblocca l'elaborazione
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPBLOMRP ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_PROD','PPBLOMRP');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("AA");
          +" and PPBLOMRP = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          PPBLOMRP = "N";
          &i_ccchkf. ;
       where;
          PPCODICE = "AA";
          and PPBLOMRP = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table TMP__PDA
    i_nIdx=cp_GetTableDefIdx('TMP__PDA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP__PDA')
    endif
    * --- Drop temporary table TMPSALDICOMM
    i_nIdx=cp_GetTableDefIdx('TMPSALDICOMM')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDICOMM')
    endif
    if this.pOPER="A"
      * --- Solo se lanciato dalla Maschera
      ah_ErrorMsg("Elaborazione terminata","!","")
      this.oParentObject.ecpQuit()     
    endif
  endproc
  proc Try_044C64F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from FABBIDET
    i_nConn=i_TableProp[this.FABBIDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FBSERIAL = "+cp_ToStrODBC(this.w_FBSERIAL);
             )
    else
      delete from (i_cTable) where;
            FBSERIAL = this.w_FBSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_APPO = "AA"
    * --- Try
    local bErr_0449BB10
    bErr_0449BB10=bTrsErr
    this.Try_0449BB10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0449BB10
    * --- End
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPOPEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEELA');
      +",PPELAMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATREG),'PAR_PROD','PPELAMPS');
      +",PPORAMPS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ORAREG),'PAR_PROD','PPORAMPS');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC(this.w_APPO);
             )
    else
      update (i_cTable) set;
          PPOPEELA = i_CODUTE;
          ,PPELAMPS = this.oParentObject.w_DATREG;
          ,PPORAMPS = this.oParentObject.w_ORAREG;
          &i_ccchkf. ;
       where;
          PPCODICE = this.w_APPO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_044942E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_OKAPPO = this.w_FBROWNUM
    return
  proc Try_044D3C08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Applica i giorni di copertura
    * --- begin transaction
    cp_BeginTrs()
    this.w_CODART = " "
    this.w_FBDATREG = i_DATSYS-999
    * --- Select from GSAC6BFB
    do vq_exec with 'GSAC6BFB',this,'_Curs_GSAC6BFB','',.f.,.t.
    if used('_Curs_GSAC6BFB')
      select _Curs_GSAC6BFB
      locate for 1=1
      do while not(eof())
      this.w_DATEVA = cp_ToDate(_Curs_GSAC6BFB.FBDATEVA)
      this.w_CODARTVAR = IIF(_Curs_GSAC6BFB.FBCRIELA = "A" , _Curs_GSAC6BFB.FBCODART , _Curs_GSAC6BFB.FBCODART+ _Curs_GSAC6BFB.FBCODMAG)
      this.w_ALTROART = this.w_OLDCODICE<>this.w_CODARTVAR or this.w_FBDATREG<this.w_DATEVA
      if this.w_ALTROART
        * --- Riassegna
        this.w_OLDCODICE = this.w_CODARTVAR
        this.w_FBROWNUM = _Curs_GSAC6BFB.FBROWNUM
        this.w_CODART = _Curs_GSAC6BFB.FBCODART
        this.w_CODMAG = NVL(FBCODMAG, SPACE(5))
        this.w_FBDATREG = this.w_DATEVA + _Curs_GSAC6BFB.FBGIOCOP
      else
        * --- Write into FABBIDET
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FABBIDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FABBIDET_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FBFABLOR =FBFABLOR+ "+cp_ToStrODBC(_Curs_GSAC6BFB.FBFABLOR);
          +",FBFABNET =FBFABNET- "+cp_ToStrODBC(_Curs_GSAC6BFB.FBFABLOR);
              +i_ccchkf ;
          +" where ";
              +"FBSERIAL = "+cp_ToStrODBC(this.w_FBSERIAL);
              +" and FBROWNUM = "+cp_ToStrODBC(this.w_FBROWNUM);
                 )
        else
          update (i_cTable) set;
              FBFABLOR = FBFABLOR + _Curs_GSAC6BFB.FBFABLOR;
              ,FBFABNET = FBFABNET - _Curs_GSAC6BFB.FBFABLOR;
              &i_ccchkf. ;
           where;
              FBSERIAL = this.w_FBSERIAL;
              and FBROWNUM = this.w_FBROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Delete from FABBIDET
        i_nConn=i_TableProp[this.FABBIDET_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"FBSERIAL = "+cp_ToStrODBC(this.w_FBSERIAL);
                +" and FBROWNUM = "+cp_ToStrODBC(_Curs_GSAC6BFB.FBROWNUM);
                 )
        else
          delete from (i_cTable) where;
                FBSERIAL = this.w_FBSERIAL;
                and FBROWNUM = _Curs_GSAC6BFB.FBROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
        select _Curs_GSAC6BFB
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0449BB10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_PROD
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPCODICE"+",PPOPEELA"+",PPELAMPS"+",PPORAMPS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_APPO),'PAR_PROD','PPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEELA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATREG),'PAR_PROD','PPELAMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ORAREG),'PAR_PROD','PPORAMPS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPCODICE',this.w_APPO,'PPOPEELA',i_CODUTE,'PPELAMPS',this.oParentObject.w_DATREG,'PPORAMPS',this.oParentObject.w_ORAREG)
      insert into (i_cTable) (PPCODICE,PPOPEELA,PPELAMPS,PPORAMPS &i_ccchkf. );
         values (;
           this.w_APPO;
           ,i_CODUTE;
           ,this.oParentObject.w_DATREG;
           ,this.oParentObject.w_ORAREG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione FABBISOGNO
    SELECT DETTFABB
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART," ")) AND NOT EMPTY(NVL(TIPREC," "))
    if TIPREC="S"
      * --- Cambio Articolo inizializzo e assegno i dati per la valorizzazione
      this.w_CODART = CODART
      this.w_GRUMER = Nvl(GRUMER,"")
      SELECT DETTFABB
      ah_Msg("Elaborazione articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_CODART))
      this.w_SCRIVIORD = .T.
      this.w_FABNET = 0
      this.w_FLSCOK = .F.
      this.w_UNIMIS = NVL(UNIMIS, "   ")
      this.w_DISPON = NVL(QTAMOV, 0)
      * --- Se a Scorta, DISPON conprende solo l' Esistenza - il Riservato
      this.w_TIPART = NVL(TIPART, "F")
      this.w_SCOMIN = 0
      this.w_PUNRIO = 0
      this.w_LOTRIO = 0
      this.w_GIOAPP = 0
      this.w_GIOCOP = 0
      this.w_NUMDOC = 0
      this.w_ALFDOC = Space(10)
      this.w_DATDOC = cp_CharToDate("  -  -  ")
      this.w_MAGPRE = IIF( EMPTY( NVL(CODMAG, SPACE(5))) , IIF(EMPTY(NVL(this.w_PPMAGPRO, SPACE(5))) , g_MAGAZI , this.w_PPMAGPRO) , CODMAG )
      this.w_SERRIF = SPACE(10)
      this.w_ROWRIF = 0
      this.w_NUMRIF = 0
      this.w_CODCOM = SPACE(15)
      this.w_TIPATT = "A"
      this.w_CODATT = SPACE(15)
      this.w_CODCEN = SPACE(15)
      * --- Lettura dati per Valorizzazioni
      *     Sfrutto l'indice per velocizzare la ricerca
      SELECT DETTVALO 
 GO TOP
      SET ORDER TO 1
      do case
        case this.oParentObject.w_CRIELA="A"
          this.w_FOUND = Seek(this.w_CODART)
        otherwise
          this.w_FOUND = Seek(this.w_CODART+this.w_MAGPRE)
      endcase
      if this.w_FOUND
        this.w_SCOMIN = NVL(PRSCOMIN, 0)
        this.w_PUNRIO = NVL(PRPUNRIO, 0)
        this.w_LOTRIO = NVL(PRLOTRIO, 0)
        this.w_GIOAPP = NVL(PRGIOAPP, 0)
        this.w_GIOCOP = NVL(PRGIOCOP, 0)
      endif
      SELECT DETTFABB
    else
      * --- Record successivi (M, B)
      *     Se attivo No Scaduto Scorta Minima considero sempre Data Evasione poich� viene modificata gi� in pagina 3 
      *     e contiene data sistema + Giorni approvvigionamento
      * --- Se manca la data di prevista evasione DATEVA, si comporta come se il flag NO SCADUTO
      *     fosse sempre attivato
      this.w_DATEVA = IIF(this.w_PPNOSCSM="S",CP_TODATE(DATEVA),IIF(TIPREC="B", this.oParentObject.w_DATREG-1, CP_TODATE(DATEVA)))
      this.w_FLORDI = NVL(FLORDI," ")
      this.w_FLIMPE = NVL(FLIMPE," ")
      this.w_QTAMOV = NVL(QTAMOV, 0)
      this.w_TIPCON = " "
      this.w_CODCON = SPACE(15)
      this.w_CODMAG = IIF( EMPTY( NVL(CODMAG, SPACE(5))) , IIF(EMPTY(NVL(this.w_PPMAGPRO, SPACE(5))) , g_MAGAZI , this.w_PPMAGPRO) , CODMAG )
      if this.w_TIPART="F"
        * --- Gestione a FABBISOGNO
        this.w_FABNET = 0
        this.w_NUMDOC = NVL(NUMIMP, 0)
        this.w_ALFDOC = NVL(ALFIMP, Space(10))
        this.w_DATDOC = CP_TODATE(DATIMP)
        this.w_TIPCON = NVL(TIPCON, " ")
        this.w_CODCON = NVL(CODCON, SPACE(15))
        this.w_SERRIF = NVL(SERRIF, SPACE(10))
        this.w_ROWRIF = NVL(ROWRIF, 0)
        this.w_NUMRIF = NVL(NUMRIF, 0)
        this.w_CODCOM = NVL(CODCOM, SPACE(15))
        this.w_TIPATT = NVL(TIPATT, "A")
        this.w_CODATT = NVL(CODATT, SPACE(15))
        this.w_CODCEN = NVL(CODCEN, SPACE(15))
        this.w_FLCOMM = NVL(ARFLCOMM,"N")
        if this.w_FLIMPE $ "+-"
          * --- Se Impegno o Record Base x Gestione a Fabbisogno calcola il Fabbisogno netto all'Ordine
          * --- Calcola lo Scaduto solo se la Disponibilita' e' > 0
          this.w_APPO1 = this.w_DISPON - IIF(TIPREC="B" AND this.w_DISPON>=this.w_SCOMIN, 0, this.w_SCOMIN)
          this.w_FABNET = IIF(this.w_APPO1<0, -this.w_APPO1, 0)
        endif
        * --- Storna La Disponibilita' in regresso:
        *     il valore iniziale � dato dal saldo ad oggi. gli impegni quindi vengono risommati e non detratti 
        *     per avere la disponibilit� effettiva al momento in cui viene esaminato il documento successivo
        this.w_DISPON = this.w_DISPON - this.w_QTAMOV
        if this.w_FABNET>0 AND NOT EMPTY(this.w_CODART)
          * --- Scrivo FABBIDETT
          this.w_FBROWNUM = this.w_FBROWNUM + 1
          this.w_DATEVA = IIF(EMPTY(this.w_DATEVA), this.oParentObject.w_DATREG-1, this.w_DATEVA)
          * --- Insert into FABBIDET
          i_nConn=i_TableProp[this.FABBIDET_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FABBIDET_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"FBSERIAL"+",FBROWNUM"+",FBCODART"+",FBDATEVA"+",FBNUMIMP"+",FBALFDOC"+",FBDATDOC"+",FBCODMAG"+",FBTIPCON"+",FBCODCON"+",FBUNIMIS"+",FBFABLOR"+",FBFABNET"+",FBSERRIF"+",FBROWRIF"+",FBNUMRIF"+",FBCODCOM"+",FBTIPATT"+",FBCODATT"+",FBCODCEN"+",FBGRUMER"+",FBCRIELA"+",FBFLCOMM"+",FBGIOCOP"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_FBSERIAL),'FABBIDET','FBSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FBROWNUM),'FABBIDET','FBROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'FABBIDET','FBCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'FABBIDET','FBDATEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'FABBIDET','FBNUMIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'FABBIDET','FBALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'FABBIDET','FBDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'FABBIDET','FBCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'FABBIDET','FBTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'FABBIDET','FBCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'FABBIDET','FBUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'FABBIDET','FBFABLOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FABNET),'FABBIDET','FBFABNET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'FABBIDET','FBSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF),'FABBIDET','FBROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'FABBIDET','FBNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'FABBIDET','FBCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPATT),'FABBIDET','FBTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'FABBIDET','FBCODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCEN),'FABBIDET','FBCODCEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_GRUMER),'FABBIDET','FBGRUMER');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIELA),'FABBIDET','FBCRIELA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLCOMM),'FABBIDET','FBFLCOMM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_GIOCOP),'FABBIDET','FBGIOCOP');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'FBSERIAL',this.w_FBSERIAL,'FBROWNUM',this.w_FBROWNUM,'FBCODART',this.w_CODART,'FBDATEVA',this.w_DATEVA,'FBNUMIMP',this.w_NUMDOC,'FBALFDOC',this.w_ALFDOC,'FBDATDOC',this.w_DATDOC,'FBCODMAG',this.w_CODMAG,'FBTIPCON',this.w_TIPCON,'FBCODCON',this.w_CODCON,'FBUNIMIS',this.w_UNIMIS,'FBFABLOR',this.w_QTAMOV)
            insert into (i_cTable) (FBSERIAL,FBROWNUM,FBCODART,FBDATEVA,FBNUMIMP,FBALFDOC,FBDATDOC,FBCODMAG,FBTIPCON,FBCODCON,FBUNIMIS,FBFABLOR,FBFABNET,FBSERRIF,FBROWRIF,FBNUMRIF,FBCODCOM,FBTIPATT,FBCODATT,FBCODCEN,FBGRUMER,FBCRIELA,FBFLCOMM,FBGIOCOP &i_ccchkf. );
               values (;
                 this.w_FBSERIAL;
                 ,this.w_FBROWNUM;
                 ,this.w_CODART;
                 ,this.w_DATEVA;
                 ,this.w_NUMDOC;
                 ,this.w_ALFDOC;
                 ,this.w_DATDOC;
                 ,this.w_CODMAG;
                 ,this.w_TIPCON;
                 ,this.w_CODCON;
                 ,this.w_UNIMIS;
                 ,this.w_QTAMOV;
                 ,this.w_FABNET;
                 ,this.w_SERRIF;
                 ,this.w_ROWRIF;
                 ,this.w_NUMRIF;
                 ,this.w_CODCOM;
                 ,this.w_TIPATT;
                 ,this.w_CODATT;
                 ,this.w_CODCEN;
                 ,this.w_GRUMER;
                 ,this.oParentObject.w_CRIELA;
                 ,this.w_FLCOMM;
                 ,this.w_GIOCOP;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT DETTFABB
        endif
      else
        * --- Gestione a SCORTA
        if TIPREC $ "MN" And this.w_FLORDI="+" AND this.w_DATEVA>this.w_FBDATREG+this.w_GIOAPP
          * --- Considera i soli ordini con data evasione oltre OGGI+LT
          *     e storna l'ordinato di questo documento dalla disponibilit� che � calcolata dai saldi
          *     Esistenza-Riservato+Ordinato
          this.w_DISPON = this.w_DISPON - this.w_QTAMOV
        endif
        if this.w_SCRIVIORD and this.w_DISPON <= this.w_PUNRIO and TIPREC="B" and not EMPTY(this.w_CODART)
          * --- Scrivo FABBIDETT
          this.w_SCRIVIORD = .F.
          this.w_FBROWNUM = this.w_FBROWNUM + 1
          this.w_DATEVA = this.oParentObject.w_DATREG
          this.w_FABNET = this.w_LOTRIO
          * --- Insert into FABBIDET
          i_nConn=i_TableProp[this.FABBIDET_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FABBIDET_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"FBSERIAL"+",FBROWNUM"+",FBCODART"+",FBDATEVA"+",FBNUMIMP"+",FBALFDOC"+",FBDATDOC"+",FBCODMAG"+",FBTIPCON"+",FBCODCON"+",FBUNIMIS"+",FBFABLOR"+",FBFABNET"+",FBSERRIF"+",FBROWRIF"+",FBNUMRIF"+",FBCODCOM"+",FBTIPATT"+",FBCODATT"+",FBCODCEN"+",FBGRUMER"+",FBCRIELA"+",FBFLCOMM"+",FBGIOCOP"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_FBSERIAL),'FABBIDET','FBSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FBROWNUM),'FABBIDET','FBROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'FABBIDET','FBCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'FABBIDET','FBDATEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'FABBIDET','FBNUMIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'FABBIDET','FBALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'FABBIDET','FBDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'FABBIDET','FBCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'FABBIDET','FBTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'FABBIDET','FBCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'FABBIDET','FBUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'FABBIDET','FBFABLOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FABNET),'FABBIDET','FBFABNET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'FABBIDET','FBSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF),'FABBIDET','FBROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'FABBIDET','FBNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'FABBIDET','FBCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPATT),'FABBIDET','FBTIPATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'FABBIDET','FBCODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCEN),'FABBIDET','FBCODCEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_GRUMER),'FABBIDET','FBGRUMER');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIELA),'FABBIDET','FBCRIELA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLCOMM),'FABBIDET','FBFLCOMM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_GIOCOP),'FABBIDET','FBGIOCOP');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'FBSERIAL',this.w_FBSERIAL,'FBROWNUM',this.w_FBROWNUM,'FBCODART',this.w_CODART,'FBDATEVA',this.w_DATEVA,'FBNUMIMP',this.w_NUMDOC,'FBALFDOC',this.w_ALFDOC,'FBDATDOC',this.w_DATDOC,'FBCODMAG',this.w_CODMAG,'FBTIPCON',this.w_TIPCON,'FBCODCON',this.w_CODCON,'FBUNIMIS',this.w_UNIMIS,'FBFABLOR',this.w_QTAMOV)
            insert into (i_cTable) (FBSERIAL,FBROWNUM,FBCODART,FBDATEVA,FBNUMIMP,FBALFDOC,FBDATDOC,FBCODMAG,FBTIPCON,FBCODCON,FBUNIMIS,FBFABLOR,FBFABNET,FBSERRIF,FBROWRIF,FBNUMRIF,FBCODCOM,FBTIPATT,FBCODATT,FBCODCEN,FBGRUMER,FBCRIELA,FBFLCOMM,FBGIOCOP &i_ccchkf. );
               values (;
                 this.w_FBSERIAL;
                 ,this.w_FBROWNUM;
                 ,this.w_CODART;
                 ,this.w_DATEVA;
                 ,this.w_NUMDOC;
                 ,this.w_ALFDOC;
                 ,this.w_DATDOC;
                 ,this.w_CODMAG;
                 ,this.w_TIPCON;
                 ,this.w_CODCON;
                 ,this.w_UNIMIS;
                 ,this.w_QTAMOV;
                 ,this.w_FABNET;
                 ,this.w_SERRIF;
                 ,this.w_ROWRIF;
                 ,this.w_NUMRIF;
                 ,this.w_CODCOM;
                 ,this.w_TIPATT;
                 ,this.w_CODATT;
                 ,this.w_CODCEN;
                 ,this.w_GRUMER;
                 ,this.oParentObject.w_CRIELA;
                 ,this.w_FLCOMM;
                 ,this.w_GIOCOP;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristina Scorte
    * --- Seleziona gli articoli per i quali deve essere ripristinata la scorta e non esiste il saldo di magazzino
    do case
      case this.oParentObject.w_CRIELA="A"
        SELECT * FROM DETTVALO INTO CURSOR SCORTE WHERE (PRLOTRIO>0 OR PRSCOMIN>0) AND ;
        (PRCODART NOT IN (SELECT DISTINCT CODICE FROM DETTFABB))
      otherwise
        SELECT * FROM DETTVALO INTO CURSOR SCORTE WHERE (PRLOTRIO>0 OR PRSCOMIN>0) AND ;
        (PRCODART+ARMAGPRE NOT IN (SELECT DISTINCT CODICE+CODMAG FROM DETTFABB))
    endcase
    SELECT SCORTE
    if RECCOUNT()>0
      SCAN
      this.w_CODART = PRCODART
      this.w_GRUMER = Nvl(GRUMER,"")
      this.w_UNIMIS = ARUNMIS1
      this.w_TIPART = ARTIPGES
      this.w_CODMAG = NVL(ARMAGPRE, g_MAGAZI)
      this.w_CODICE = this.w_CODART
      this.w_FLCOMM = NVL(ARFLCOMM, "N")
      SELECT DETTFABB
      * --- Aggiunge i record del saldo
      append blank
      replace codart with this.w_CODART, grumer with this.w_GRUMER, tiprec with "S", codmag with this.w_CODMAG, ;
      tipcon with "X", unimis with this.w_UNIMIS, tipart with this.w_TIPART, codice with this.w_CODICE, ARFLCOMM with this.w_FLCOMM, CODCOM with SPACE(15)
      append blank
      replace codart with this.w_CODART, tiprec with "B", codmag with this.w_CODMAG, ;
      tipcon with "X", unimis with this.w_UNIMIS, tipart with this.w_TIPART, codice with this.w_CODICE, flimpe with "+", ARFLCOMM with this.w_FLCOMM, CODCOM with SPACE(15)
      endscan
    endif
    USE IN SELECT("SCORTE")
    * --- Aggiungo un nuovo campo per evitare di spostare la data evasione in mancanza del fornitore abituale
    Select * from DETTFABB INTO CURSOR DETTFAB1
    USE IN SELECT("DETTFABB")
    SELECT *, "" AS NoSpostaData FROM DETTFAB1 INTO CURSOR DETTFABB ORDER BY CODART, CODMAG, CODCOM, TIPREC DESC, DATEVA DESC, QTAMOV READWRITE
    USE IN SELECT("DETTFAB1")
    if this.w_PP_CICLI="S"
      * --- Sposta in Avanti le date Impegno per gli Impegni che cadono prima della Data di Elaborazione (Solo Art. a fabbisogno)
      this.w_APPO = "N"
      if this.w_PPNOSCSM="S"
        * --- Nel caso di No Scaduto Scorta Minima creo un record nel cursore DETTFABB che contiene un 
        *     movimento di impegnato che sta a simulare la Scorta Minima.
        SELECT * FROM DETTVALO INTO CURSOR SCORTAMIN WHERE PRSCOMIN>0
        SELECT SCORTAMIN
        if RECCOUNT()>0
          SCAN
          this.w_CODART = PRCODART
          this.w_UNIMIS = ARUNMIS1
          this.w_TIPART = ARTIPGES
          this.w_CODMAG = NVL(ARMAGPRE, g_MAGAZI)
          this.w_CODICE = this.w_CODART
          this.w_SCOMIN = NVL(PRSCOMIN, 0)*-1
          this.w_PUNRIO = NVL(PRPUNRIO, 0)*-1
          this.w_GIOAPP = NVL(PRGIOAPP, 0)
          this.w_CODFOR = NVL(PRCODFOR, SPACE(15))
          select DETTFABB
          if NOT EMPTY(this.w_PPCALSTA)
            if EMPTY(this.w_CODFOR)
              this.w_TipoPianificazione = "I"
              this.DatInput = dtos(i_DATSYS)
              this.w_LeadT = 1
            else
              this.w_TipoPianificazione = "A"
              this.DatInput = dtos(i_DATSYS)
              this.w_LeadT = this.w_GIOAPP
            endif
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if NOT EMPTY(this.w_PPCALSTA)
            if Not Empty(this.DatRisul)
              this.w_DATEVA1 = this.DatRisul
            endif
          else
            this.w_DATEVA1 = IIF(Empty(this.w_CODFOR), i_DATSYS-1, i_DATSYS + this.w_GIOAPP )
          endif
          * --- Aggiunge i record del saldo
          SELECT DETTFABB
          append blank
          replace codart with this.w_CODART, tiprec with "M", codmag with this.w_CODMAG, ;
          tipcon with "X", unimis with this.w_UNIMIS, tipart with this.w_TIPART, codice with this.w_CODICE, ;
          QTAMOV with IIF(this.w_TIPART="F",this.w_SCOMIN, this.w_PUNRIO) , FLIMPE with "+", DATEVA With this.w_DATEVA1, NoSpostaData With "S"
          SELECT SCORTAMIN
          ENDSCAN
          this.w_APPO = "S"
        endif
      endif
      this.w_CODFOR = SPACE(15)
      SELECT DETTFABB
      GO TOP
      * --- Nel caso di No Scaduto Scorta Minima Tra i Record di tipo M (movimenti) � presente anche il movimento fittizio che replica la scorta minima
      SCAN FOR NOT EMPTY(NVL(CODART," ")) AND NVL(TIPREC," ") $ "SMN" AND NVL(TIPART, "F")="F"
      this.w_CODART = CODART
      this.w_MAGPRE = NVL(CODMAG, g_MAGAZI)
      if TIPREC="S"
        * --- Lettura dati per Valorizzazioni
        this.w_GIOAPP = 0
        this.w_CODFOR = SPACE(15)
        SELECT DETTVALO 
 GO TOP
        SET ORDER TO 1
        do case
          case this.oParentObject.w_CRIELA="A"
            this.w_FOUND = Seek(this.w_CODART)
          otherwise
            this.w_FOUND = Seek(this.w_CODART+this.w_MAGPRE)
        endcase
        if this.w_FOUND
          this.w_GIOAPP = NVL(PRGIOAPP, 0)
          this.w_CODFOR = NVL(PRCODFOR, SPACE(15))
        endif
        SELECT DETTFABB
      else
        this.w_DATEVA = CP_TODATE(DATEVA)
        if NOT EMPTY(this.w_PPCALSTA)
          this.w_TipoPianificazione = "I"
          this.DatInput = dtos(this.w_DATEVA)
          this.w_LeadT = this.w_GIOAPP
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if NOT EMPTY(this.w_PPCALSTA)
          this.w_DATEVA1 = this.DatRisul
        else
          this.w_DATEVA1 = this.w_DATEVA - this.w_GIOAPP
        endif
        SELECT DETTFABB
        if this.w_DATEVA1<i_DATSYS AND NOT EMPTY(this.w_CODFOR) And Empty(NVL(FLORDI," ")) And NoSpostaData<>"S"
          if NOT EMPTY(this.w_PPCALSTA)
            this.w_TipoPianificazione = "A"
            this.DatInput = dtos(i_DATSYS)
            this.w_LeadT = this.w_GIOAPP
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if NOT EMPTY(this.w_PPCALSTA)
            this.w_DATEVA = this.DatRisul
          else
            this.w_DATEVA = i_DATSYS + this.w_GIOAPP
          endif
          SELECT DETTFABB
          REPLACE DATEVA WITH this.w_DATEVA
          this.w_APPO = "S"
        endif
        SELECT DETTFABB
      endif
      ENDSCAN
      if this.w_APPO="S"
        * --- Se Aggiornati record Riordina il Cursore
        Select * from DETTFABB INTO CURSOR DETTFAB1
        USE IN SELECT("DETTFABB")
        Select * from DETTFAB1 INTO CURSOR DETTFABB ORDER BY CODART, CODMAG, CODCOM, TIPREC DESC, DATEVA DESC, QTAMOV
        USE IN SELECT("DETTFAB1")
        SELECT DETTFABB
        =WRCURSOR("DETTFABB")
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSDB_BLT
    * --- Calcola la data di inizio in base al Lead Time
    SET_NEAR = Set("NEAR")
    SET NEAR ON
    this.DatRisul = {}
    this.w_DatErr = FALSE
    this.w_DatErrMsg = ""
    select Calend
    this.w_Find = seek(this.DatInput)
    if this.w_Find
      * --- Data evasione trovata
      if this.w_TipoPianificazione="I"
        skip -ceiling(this.w_LeadT)
        if bof()
          * --- Data anteriore all'orizzonte di pianificazione
          this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
          this.w_DatErr = TRUE
        endif
      else
        skip +ceiling(this.w_LeadT)
        if eof()
          * --- Data posteriore all'orizzonte di pianificazione
          this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
          this.w_DatErr = TRUE
        endif
      endif
    else
      if EOF()
        * --- Data posteriore all'orizzonte di pianificazione
        this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
        this.w_DatErr = TRUE
      else
        if this.w_TipoPianificazione="I"
          skip -(ceiling(this.w_LeadT)+1)
          if bof()
            * --- Data anteriore all'orizzonte di pianificazione
            this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
            this.w_DatErr = TRUE
          endif
        else
          skip +ceiling(this.w_LeadT)
          if eof()
            * --- Data posteriore all'orizzonte di pianificazione
            this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
            this.w_DatErr = TRUE
          endif
        endif
      endif
    endif
    this.DatRisul = cp_todate(cagiorno)
    SET NEAR &SET_NEAR
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Indica se sono stati effettuati ulteriori filtri sugli articoli 
    *     (da zoom in seconda pagina GSAC_KGF)
    * --- Indica se sono stati effettuati ulteriori filtri sugli articoli (da zoom in GSAC_KGF)
    * --- Indica se sono stati effettuati ulteriori filtri sui magazzini (da zoom in GSAC_KGF)
    * --- Indica se sono stati effettuati ulteriori filtri sui gruppi di magazzino (da zoom in GSAC_KGF)
    * --- Variabili per la gestione dei filtri su zoom
    * --- Variabili usate per gestione path DBF
    if UPPER(ALLTRIM(this.GSAC_KGF.CLASS))="TGSAC_KGF"
      this.w_PDCODINI = this.GSAC_KGF.w_PDCODINI
      this.w_PDCODFIN = this.GSAC_KGF.w_PDCODFIN
      this.w_PDFAMAIN = this.GSAC_KGF.w_PDFAMAIN
      this.w_PDFAMAFI = this.GSAC_KGF.w_PDFAMAFI
      this.w_PDGRUINI = this.GSAC_KGF.w_PDGRUINI
      this.w_PDGRUFIN = this.GSAC_KGF.w_PDGRUFIN
      this.w_PDMAGINI = this.GSAC_KGF.w_PDMAGINI
      this.w_PDMAGFIN = this.GSAC_KGF.w_PDMAGFIN
      this.w_PDMARINI = this.GSAC_KGF.w_PDMARINI
      this.w_PDMARFIN = this.GSAC_KGF.w_PDMARFIN
      this.w_PDCATINI = this.GSAC_KGF.w_PDCATINI
      this.w_PDCATFIN = this.GSAC_KGF.w_PDCATFIN
      this.w_PDCOMINI = this.GSAC_KGF.w_PDCOMINI
      this.w_PDCOMFIN = this.GSAC_KGF.w_PDCOMFIN
    endif
    this.w_PPFLAROB = this.GSAC_KGF.w_PPFLAROB
    this.w_ELAPDF = this.GSAC_KGF.w_ELAPDF
    this.w_ELAPDS = this.GSAC_KGF.w_ELAPDS
    this.w_STAORD = this.GSAC_KGF.w_STAORD
    this.w_FILTROCOM = IIF( Not(Empty(this.w_PDCOMINI+this.w_PDCOMFIN)) , "S" , "N" )
    * --- Preparo le tabelle temporanee per effettuare i filtri di selezione
    * --- Create temporary table TMPSELART
    i_nIdx=cp_AddTableDef('TMPSELART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_GHCDFUSUID[1]
    indexes_GHCDFUSUID[1]='SLCODICE'
    i_nConn=i_TableProp[this.SALDIART_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"SLCODICE "," from "+i_cTable;
          +" where 1=0";
          ,.f.,@indexes_GHCDFUSUID)
    this.TMPSELART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPGRUDMAG
    i_nIdx=cp_AddTableDef('TMPGRUDMAG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.GRUDMAG_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.GRUDMAG_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPGRUDMAG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if UPPER(ALLTRIM(this.GSAC_KGF.CLASS))="TGSAC_KGF"
      this.ZOOMMAGA = this.GSAC_KGF.w_ZOOMMAGA
      this.ZOOMART = this.GSAC_KGF.w_ZOOMART
      this.w_SELZZART = .F.
      this.w_NRARTSEL = 0
      this.w_NR_R_ART = 0
      * --- Verifico se sono stati effettuati ulteriori filtri su articoli da zoom
      L_CurArt = this.ZOOMART.cCursor
      if Used( L_CurArt ) and RecCount( L_CurArt )>0
        SELECT ( L_CurArt )
        this.w_NR_R_ART = RecCount()
        SELECT ( L_CurArt )
        GO TOP
        COUNT FOR XCHK=1 TO this.w_NRARTSEL
        this.w_SELZZART = this.w_NRARTSEL>0 && and this.w_NRARTSEL < this.w_NR_R_ART
      endif
      * --- Verifico se sono stati effettuati filtri di selezione su magazzini/gruppi di magazzino
      this.w_SELZZMAG = .F.
      this.w_NRMAGSEL = 0
      this.w_NR_R_MAG = 0
      if this.oParentObject.w_CRIELA $ "G-M"
        * --- Ritorna il valore della variabile w_SELMAG utilizzata come parametro nella query GSMR2BGPG
        * --- Select from MAGA_TEMP
        i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS NUMREC  from "+i_cTable+" MAGA_TEMP ";
              +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
               ,"_Curs_MAGA_TEMP")
        else
          select COUNT(*) AS NUMREC from (i_cTable);
           where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
            into cursor _Curs_MAGA_TEMP
        endif
        if used('_Curs_MAGA_TEMP')
          select _Curs_MAGA_TEMP
          locate for 1=1
          do while not(eof())
          this.w_NRMAGSEL = _Curs_MAGA_TEMP.NUMREC
            select _Curs_MAGA_TEMP
            continue
          enddo
          use
        endif
        this.w_SELZZMAG = this.oParentObject.w_CRIELA $ "M-G" Or this.w_NRMAGSEL>0
      endif
      if this.w_SELZZART
        * --- Tabella temporanea per ulteriore selezione articoli
        *     Ulteriore filtro di selezione in "Selezioni aggiutive" GSAC_KGF
        Select distinct cacodart as slcodice from ( L_CurArt ) into Cursor SelArt where xchk=1
        SELECT "SelArt"
        GO TOP
        SCAN
        * --- Insert into TMPSELART
        i_nConn=i_TableProp[this.TMPSELART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSELART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPSELART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SLCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(SelArt.slcodice),'TMPSELART','SLCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SLCODICE',SelArt.slcodice)
          insert into (i_cTable) (SLCODICE &i_ccchkf. );
             values (;
               SelArt.slcodice;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        SELECT "SelArt"
        ENDSCAN
        use in select("SelArt")
      endif
      if this.w_SELZZMAG
        if this.oParentObject.w_CRIELA = "G"
          * --- Select from MAGA_TEMP
          i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select GRCODICE, MGCODMAG, GRMGPRIN  from "+i_cTable+" MAGA_TEMP ";
                +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
                 ,"_Curs_MAGA_TEMP")
          else
            select GRCODICE, MGCODMAG, GRMGPRIN from (i_cTable);
             where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
              into cursor _Curs_MAGA_TEMP
          endif
          if used('_Curs_MAGA_TEMP')
            select _Curs_MAGA_TEMP
            locate for 1=1
            do while not(eof())
            * --- Insert into TMPGRUDMAG
            i_nConn=i_TableProp[this.TMPGRUDMAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPGRUDMAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPGRUDMAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"GRCODICE"+",GRCODMAG"+",GRMGPRIN"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.GRCODICE),'TMPGRUDMAG','GRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.MGCODMAG),'TMPGRUDMAG','GRCODMAG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.GRMGPRIN),'TMPGRUDMAG','GRMGPRIN');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'GRCODICE',_Curs_MAGA_TEMP.GRCODICE,'GRCODMAG',_Curs_MAGA_TEMP.MGCODMAG,'GRMGPRIN',_Curs_MAGA_TEMP.GRMGPRIN)
              insert into (i_cTable) (GRCODICE,GRCODMAG,GRMGPRIN &i_ccchkf. );
                 values (;
                   _Curs_MAGA_TEMP.GRCODICE;
                   ,_Curs_MAGA_TEMP.MGCODMAG;
                   ,_Curs_MAGA_TEMP.GRMGPRIN;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_MAGA_TEMP
              continue
            enddo
            use
          endif
        else
          * --- Select from MAGA_TEMP
          i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select DISTINCT MGCODMAG AS MGCODMAG  from "+i_cTable+" MAGA_TEMP ";
                +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
                 ,"_Curs_MAGA_TEMP")
          else
            select DISTINCT MGCODMAG AS MGCODMAG from (i_cTable);
             where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
              into cursor _Curs_MAGA_TEMP
          endif
          if used('_Curs_MAGA_TEMP')
            select _Curs_MAGA_TEMP
            locate for 1=1
            do while not(eof())
            * --- Insert into TMPGRUDMAG
            i_nConn=i_TableProp[this.TMPGRUDMAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPGRUDMAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPGRUDMAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"GRCODICE"+",GRCODMAG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("     "),'TMPGRUDMAG','GRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.MGCODMAG),'TMPGRUDMAG','GRCODMAG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'GRCODICE',"     ",'GRCODMAG',_Curs_MAGA_TEMP.MGCODMAG)
              insert into (i_cTable) (GRCODICE,GRCODMAG &i_ccchkf. );
                 values (;
                   "     ";
                   ,_Curs_MAGA_TEMP.MGCODMAG;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_MAGA_TEMP
              continue
            enddo
            use
          endif
        endif
      endif
    else
      * --- Se arrivo dal MRP ed ho dei filtri li devo applicare anche qui
      this.w_SELZZART = .F.
      this.w_NRARTSEL = 0
      * --- Select from gsac0bfbt
      do vq_exec with 'gsac0bfbt',this,'_Curs_gsac0bfbt','',.f.,.t.
      if used('_Curs_gsac0bfbt')
        select _Curs_gsac0bfbt
        locate for 1=1
        do while not(eof())
        * --- Insert into TMPSELART
        i_nConn=i_TableProp[this.TMPSELART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSELART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPSELART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SLCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_gsac0bfbt.CACODRIC),'TMPSELART','SLCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SLCODICE',_Curs_gsac0bfbt.CACODRIC)
          insert into (i_cTable) (SLCODICE &i_ccchkf. );
             values (;
               _Curs_gsac0bfbt.CACODRIC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_SELZZART = True
          select _Curs_gsac0bfbt
          continue
        enddo
        use
      endif
      this.w_SELZZMAG = .F.
      this.w_NRMAGSEL = 0
      if this.oParentObject.w_CRIELA $ "G-M"
        * --- Ritorna il valore della variabile w_SELMAG utilizzata come parametro nella query GSMR2BGPG
        * --- Select from MAGA_TEMP
        i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS NUMREC  from "+i_cTable+" MAGA_TEMP ";
              +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
               ,"_Curs_MAGA_TEMP")
        else
          select COUNT(*) AS NUMREC from (i_cTable);
           where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
            into cursor _Curs_MAGA_TEMP
        endif
        if used('_Curs_MAGA_TEMP')
          select _Curs_MAGA_TEMP
          locate for 1=1
          do while not(eof())
          this.w_NRMAGSEL = _Curs_MAGA_TEMP.NUMREC
            select _Curs_MAGA_TEMP
            continue
          enddo
          use
        endif
        this.w_SELZZMAG = this.oParentObject.w_CRIELA $ "M-G" Or this.w_NRMAGSEL>0
      endif
      if this.w_SELZZMAG
        if this.oParentObject.w_CRIELA = "G"
          * --- Select from MAGA_TEMP
          i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select GRCODICE, MGCODMAG, GRMGPRIN  from "+i_cTable+" MAGA_TEMP ";
                +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
                 ,"_Curs_MAGA_TEMP")
          else
            select GRCODICE, MGCODMAG, GRMGPRIN from (i_cTable);
             where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
              into cursor _Curs_MAGA_TEMP
          endif
          if used('_Curs_MAGA_TEMP')
            select _Curs_MAGA_TEMP
            locate for 1=1
            do while not(eof())
            * --- Insert into TMPGRUDMAG
            i_nConn=i_TableProp[this.TMPGRUDMAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPGRUDMAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPGRUDMAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"GRCODICE"+",GRCODMAG"+",GRMGPRIN"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.GRCODICE),'TMPGRUDMAG','GRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.MGCODMAG),'TMPGRUDMAG','GRCODMAG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.GRMGPRIN),'TMPGRUDMAG','GRMGPRIN');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'GRCODICE',_Curs_MAGA_TEMP.GRCODICE,'GRCODMAG',_Curs_MAGA_TEMP.MGCODMAG,'GRMGPRIN',_Curs_MAGA_TEMP.GRMGPRIN)
              insert into (i_cTable) (GRCODICE,GRCODMAG,GRMGPRIN &i_ccchkf. );
                 values (;
                   _Curs_MAGA_TEMP.GRCODICE;
                   ,_Curs_MAGA_TEMP.MGCODMAG;
                   ,_Curs_MAGA_TEMP.GRMGPRIN;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_MAGA_TEMP
              continue
            enddo
            use
          endif
        else
          * --- Select from MAGA_TEMP
          i_nConn=i_TableProp[this.MAGA_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGA_TEMP_idx,2],.t.,this.MAGA_TEMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select DISTINCT MGCODMAG AS MGCODMAG  from "+i_cTable+" MAGA_TEMP ";
                +" where MGSELEZI='S' AND MGKEYRIF="+cp_ToStrODBC(this.oParentObject.w_KEYRIF)+"";
                 ,"_Curs_MAGA_TEMP")
          else
            select DISTINCT MGCODMAG AS MGCODMAG from (i_cTable);
             where MGSELEZI="S" AND MGKEYRIF=this.oParentObject.w_KEYRIF;
              into cursor _Curs_MAGA_TEMP
          endif
          if used('_Curs_MAGA_TEMP')
            select _Curs_MAGA_TEMP
            locate for 1=1
            do while not(eof())
            * --- Insert into TMPGRUDMAG
            i_nConn=i_TableProp[this.TMPGRUDMAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPGRUDMAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPGRUDMAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"GRCODICE"+",GRCODMAG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("     "),'TMPGRUDMAG','GRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_MAGA_TEMP.MGCODMAG),'TMPGRUDMAG','GRCODMAG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'GRCODICE',"     ",'GRCODMAG',_Curs_MAGA_TEMP.MGCODMAG)
              insert into (i_cTable) (GRCODICE,GRCODMAG &i_ccchkf. );
                 values (;
                   "     ";
                   ,_Curs_MAGA_TEMP.MGCODMAG;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_MAGA_TEMP
              continue
            enddo
            use
          endif
        endif
      endif
    endif
    this.w_SELART = IIF(this.w_SELZZART, "S", "N")
    this.w_SELMAG = IIF(this.w_SELZZMAG, "S", "N")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='FABBIDET'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='GRUDMAG'
    this.cWorkTables[6]='*TMPSELART'
    this.cWorkTables[7]='*TMPGRUDMAG'
    this.cWorkTables[8]='*TMP__PDA'
    this.cWorkTables[9]='*TMPSALDICOMM'
    this.cWorkTables[10]='MAGA_TEMP'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSAC6BFB')
      use in _Curs_GSAC6BFB
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_gsac0bfbt')
      use in _Curs_gsac0bfbt
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    if used('_Curs_MAGA_TEMP')
      use in _Curs_MAGA_TEMP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
