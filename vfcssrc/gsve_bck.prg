* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bck                                                        *
*              Controlli documento                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_546]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bck",oParentObject)
return(i_retval)

define class tgsve_bck as StdBatch
  * --- Local variables
  w_INIANNO = ctod("  /  /  ")
  w_NR = 0
  w_FINEANNO = ctod("  /  /  ")
  w_DR = ctod("  /  /  ")
  w_SEGNALA = .f.
  w_MESS = space(200)
  w_OK = .f.
  w_CFUNC = space(10)
  w_APPO = space(10)
  w_APPO1 = space(10)
  w_NUMPRO = 0
  w_ALFPRO = space(2)
  w_RIGHERAEENONRICALCOLATE = .f.
  w_TEST = .f.
  w_FLMIMA = space(1)
  w_STACOD = space(1)
  w_oERRORLOG = .NULL.
  w_CREALOG = space(1)
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_ANNULLA = .f.
  w_FLESIG = space(1)
  w_FLIVDF = space(1)
  w_PADRE = .NULL.
  w_AITIPE = space(1)
  w_AITIPV = space(1)
  w_AITIPS = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_oMESS1QTA = .NULL.
  w_oPART1QTA = .NULL.
  w_oMESSQTA = .NULL.
  w_oPARTQTA = .NULL.
  w_TIPPAG = space(2)
  w_PARA = space(1)
  w_NUMVEN = space(1)
  w_TMPS = space(200)
  w_TIPO = space(1)
  w_CAUOBS = ctod("  /  /  ")
  w_INIESE = ctod("  /  /  ")
  w_FINESE = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_oGSVE_MRT = .NULL.
  w_RATAKO = 0
  w_ANIVASOS = space(1)
  w_BMARKPOS = .f.
  w_NUMMAG = 0
  w_SERDOCESP = space(10)
  w_COMPEV = 0
  w_ROWINDEX = 0
  w_SELECT = space(254)
  w_WHERECON = space(254)
  w_PRZCHK = 0
  w_GSVE_MIU = .NULL.
  w_NUMREC = 0
  w_OBJCTRL = .NULL.
  w_DAIM = .f.
  w_ANNULLA = .f.
  w_TRIG = 0
  w_FLDEL = .f.
  w_TIPRIG = space(1)
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_AGGSAL = space(10)
  w_CAUCOL = space(5)
  w_CODMAT = space(5)
  w_AGGSAL1 = space(10)
  w_FLDIMP = space(23)
  w_SERRIF = space(10)
  w_QTAEV1 = 0
  w_IMPEVA = 0
  w_SRV = space(1)
  w_QTAUM1 = 0
  w_CODLOT = space(20)
  w_FLAGLO = space(1)
  w_FLAG2LO = space(1)
  w_COART = space(20)
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_CODART = space(20)
  w_ROWNUM = 0
  w_ROWRIF = 0
  w_ERRLOTT = .f.
  w_FLARIF = space(1)
  w_FLEVAS = space(1)
  w_OldFLEVAS = space(1)
  w_CODODL = space(15)
  w_FLERIF = space(1)
  w_IMPLOT = space(1)
  w_NR = space(6)
  w_ARDTOBSO = ctod("  /  /  ")
  w_ANDTOBSO = ctod("  /  /  ")
  w_CPROWCES = 0
  w_QTA = 0
  w_TIPOCON = space(1)
  w_TIPSOT = space(1)
  w_CONVEA = space(15)
  w_CONNAC = space(15)
  w_CONTRO = space(15)
  w_MESSCESP = space(10)
  w_FLELGM = space(1)
  w_FLDIF = .f.
  w_OLDOK = .f.
  w_SERRDESC = space(10)
  w_LFLARIF = space(1)
  w_UL__SSCC = space(18)
  w_RESULT = space(1)
  w_PESRES = space(1)
  w_ResMsg = .NULL.
  w_CKAGG_01 = space(15)
  w_CKAGG_02 = space(15)
  w_CKAGG_03 = space(15)
  w_CKAGG_04 = space(15)
  w_CKAGG_05 = ctod("  /  /  ")
  w_CKAGG_06 = ctod("  /  /  ")
  w_MESS_AGG = space(200)
  w_ERROR_AGG = .f.
  w_IDFLDAGG = 0
  w_CODICE = space(20)
  w_QTAVEN = 0
  w_QTAMIN = 0
  w_UNMIS1 = space(3)
  w_RIGA = 0
  w_QTAWARN = .f.
  w_QTABLOC = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  SALDIART_idx=0
  MAGAZZIN_idx=0
  DOC_DETT_idx=0
  ART_ICOL_idx=0
  ADDE_SPE_idx=0
  ESERCIZI_idx=0
  CONTI_idx=0
  CONTVEAC_idx=0
  PAR_ALTE_idx=0
  CAU_CONT_idx=0
  VOC_COST_idx=0
  CENCOST_idx=0
  CAN_TIER_idx=0
  PAR_PROD_idx=0
  PAR_RIOR_idx=0
  AZDATINT_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo univocit� documento (da GSVE_MDV, GSAC_MDV, GSOR_MDV)
    * --- Questo Batch viene eseguito in A.M. CheckForm in modo da non appesantire la fase di Transazione
    this.w_CREALOG = "S"
    this.w_RESOCON1 = ""
    this.w_OK = .T.
    this.w_SEGNALA = .F.
    this.w_PADRE = This.oParentObject
    this.w_CFUNC = this.w_PADRE.cFunction
    * --- Read from AZDATINT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZDATINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZDATINT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ITAITIPE,ITAITIPV,ITAITIPS"+;
        " from "+i_cTable+" AZDATINT where ";
            +"ITCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ITAITIPE,ITAITIPV,ITAITIPS;
        from (i_cTable) where;
            ITCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AITIPE = NVL(cp_ToDate(_read_.ITAITIPE),cp_NullValue(_read_.ITAITIPE))
      this.w_AITIPV = NVL(cp_ToDate(_read_.ITAITIPV),cp_NullValue(_read_.ITAITIPV))
      this.w_AITIPS = NVL(cp_ToDate(_read_.ITAITIPS),cp_NullValue(_read_.ITAITIPS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    this.w_oMESS1QTA=createobject("ah_message")
    this.w_oMESSQTA=createobject("ah_message")
    this.w_MESS = CHKCONS(this.oParentObject.w_MVFLVEAC,this.oParentObject.w_MVDATREG,"B","S")
    this.w_OK = Empty( this.w_MESS )
    this.w_RIGHERAEENONRICALCOLATE = .F.
    if this.w_OK And this.w_CFUNC<>"Query"
      * --- Se il N� Doc. � maggiore di 999999999999999
      if this.oParentObject.w_MVNUMDOC> 999999999999999
        this.w_MESS = "Il n� doc. non pu� essere maggiore di 999999999999999"
        ah_ErrorMsg(this.w_MESS)
        this.w_OK = .F.
      endif
      * --- Controllo se data Competenza IVA Uguale o Minore data Documento
      if this.w_OK AND this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVCLADOC $ "FA-NC" AND NOT EMPTY(this.oParentObject.w_MVCAUCON) AND this.oParentObject.w_MVDATCIV>this.oParentObject.w_MVDATREG
        this.w_MESS = "Data competenza IVA superiore alla data documento"
        ah_ErrorMsg(this.w_MESS)
        this.w_OK = .F.
      endif
      if this.w_OK AND EMPTY(this.oParentObject.w_MVCODPAG) AND g_PERPAR="S" AND (NOT this.oParentObject.w_PARCAU $ " N" OR IsAlt()) AND (this.oParentObject.w_PARCLF="S" OR ((EMPTY(this.oParentObject.w_MVCODCON) AND this.oParentObject.w_MVCLADOC $ "DI-RF") AND NOT Isalt()))
        this.w_MESS = "Codice pagamento non definito"
        ah_ErrorMsg(this.w_MESS)
        this.w_OK = .F.
      endif
      if this.w_OK
        this.w_TIPPAG = LEFT(CHTIPPAG(this.oParentObject.w_MVCODPAG, "BO"), 2)
        if (this.w_TIPPAG="BO" AND EMPTY(this.oParentObject.w_MVNUMCOR)) AND ((this.oParentObject.w_MVTIPCON="C" AND this.oParentObject.w_MVCLADOC="NC") OR (this.oParentObject.w_MVTIPCON="F" AND this.oParentObject.w_MVCLADOC<>"NC")) AND this.oParentObject.w_MVFLSCAF<>"S"
          this.w_MESS = "Pagamento di tipo bonifico: inserire il conto corrente"
          ah_ErrorMsg(this.w_MESS)
          this.w_OK = .F.
        endif
      endif
      if this.w_OK AND this.oParentObject.w_MVFLACCO="S" AND (EMPTY(this.oParentObject.w_MVDATTRA) OR EMPTY(this.oParentObject.w_MVORATRA) OR EMPTY(this.oParentObject.w_MVMINTRA))
        ah_ErrorMsg("Mancano la data o ora del trasporto")
      endif
      if this.w_OK AND this.w_CFUNC="Load" AND this.oParentObject.w_FLINTR="S" AND this.oParentObject.w_TIPDOC="NE" AND EMPTY(this.oParentObject.w_MVANNRET)
        * --- Nota Credito INTRA senza specificati Periodi Rettifica, Portebbe trattarsi di Dimenticanza
        if this.oParentObject.w_MVFLVEAC="V"
          this.w_MESS = "Nel documento non sono stati specificati il periodo e/o l'anno di rettifica (bottone dati testata)%0Ai fini INTRA il documento sar� generato come movimento di rettifica cessione%0Confermi ugualmente?"
        else
          this.w_MESS = "Nel documento non sono stati specificati il periodo e/o l'anno di rettifica (bottone dati testata)%0Ai fini INTRA il documento sar� generato come movimento di rettifica acquisto%0Confermi ugualmente?"
        endif
        this.w_OK = ah_YesNo(this.w_MESS)
        if this.w_OK
          * --- Documento di rettifica INTRA verifica se completati i dati correlati
          if EMPTY(this.oParentObject.w_MVANNRET)
            this.oParentObject.w_MVANNRET = ALLTRIM(STR(YEAR(this.oParentObject.w_MVDATREG)))
          endif
          if EMPTY(this.oParentObject.w_MVTIPPER)
            if this.oParentObject.w_MVFLVEAC="V"
              if this.oParentObject.w_ARUTISER="N"
                this.oParentObject.w_MVTIPPER = this.w_AITIPE
              else
                this.oParentObject.w_MVTIPPER = this.w_AITIPS
              endif
            else
              this.oParentObject.w_MVTIPPER = this.w_AITIPV
            endif
          endif
          if EMPTY( this.oParentObject.w_MVPERRET )
            do case
              case this.oParentObject.w_MVTIPPER = "M"
                this.oParentObject.w_MVPERRET = MONTH(this.oParentObject.w_MVDATREG)
              case this.oParentObject.w_MVTIPPER = "T"
                this.oParentObject.w_MVPERRET = INT( MONTH(this.oParentObject.w_MVDATREG) * 4 / 12 + .8)
            endcase
          endif
          if (this.oParentObject.w_MVTIPPER $ "MT" AND this.oParentObject.w_MVPERRET=0) OR (this.oParentObject.w_MVTIPPER="M" AND this.oParentObject.w_MVPERRET>12) OR (this.oParentObject.w_MVTIPPER="T" AND this.oParentObject.w_MVPERRET>4)
            this.w_MESS = "Periodo di rettifica INTRA non inserito o incongruente con il tipo di periodicit� (bottone: dati testata)"
            ah_ErrorMsg(this.w_MESS)
            this.w_OK = .F.
          endif
        endif
      endif
      if this.w_OK AND this.oParentObject.w_FLINTR="S" AND NOT EMPTY(this.oParentObject.w_MVTIPPER)
        * --- Documento di rettifica INTRA verifica se completati i dati correlati
        if EMPTY(this.oParentObject.w_MVTIPPER)
          this.w_MESS = "Sul documento non � stata specificata la periodicit� relativa alla rettifica INTRA (bottone: dati testata)"
          ah_ErrorMsg(this.w_MESS)
          this.w_OK = .F.
        else
          if (this.oParentObject.w_MVTIPPER $ "MT" AND this.oParentObject.w_MVPERRET=0) OR (this.oParentObject.w_MVTIPPER="M" AND this.oParentObject.w_MVPERRET>12) OR (this.oParentObject.w_MVTIPPER="T" AND this.oParentObject.w_MVPERRET>4)
            this.w_MESS = "Periodo di rettifica INTRA non inserito o incongruente con il tipo di periodicit� (bottone: dati testata)"
            ah_ErrorMsg(this.w_MESS)
            this.w_OK = .F.
          endif
        endif
      endif
      * --- Controllo Univocit� Documento (Ciclo Vendite)
      *     Controllo Univocit� Protocollo (Ciclo Acquisti)
      *     Controllo sequenzialit� registri IVA (Vendite/Acquisti)
      *     Controllo univocit� documento (Tutti)
      if this.w_OK
        * --- FLAG NUMERAZIONE VENDITE NELLE CAUSALI D'ACQUISTO
        * --- Negli ordini nn c'� w_FLPDOC
        if TYPE("this.oparentobject.w_FLPDOC")="C" 
 
          this.w_NUMVEN = this.oparentobject.w_FLPDOC
        else
          this.w_NUMVEN = " "
        endif
        if (this.oParentObject.w_MVFLVEAC="V" OR (this.oParentObject.w_MVFLVEAC="A" AND (this.w_NUMVEN="S" OR this.oParentObject.w_MVCLADOC="OR"))) AND this.oParentObject.w_MVCLADOC<>"RF" 
          * --- Ciclo Attivo
          *     Se progressivo libero (w_MVPRD='NN') non controllo niente per le vendite
          *     Per le ricevute fiscali non controllo niente
          if this.oParentObject.op_MVNUMDOC=this.oParentObject.w_MVNUMDOC AND this.oParentObject.op_MVALFDOC=this.oParentObject.w_MVALFDOC AND this.w_CFUNC="Load" And this.oParentObject.w_MVPRD<>"NN"
            * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
            i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
            cp_AskTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
            if this.oParentObject.op_MVNUMDOC<>this.oParentObject.w_MVNUMDOC 
              * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
              *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch e della mInsert()
              this.w_MESS = "Riassegnato progressivo documento n. %1"
              ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(STR(this.oParentObject.w_MVNUMDOC)) )
              this.oParentObject.op_MVNUMDOC = this.oParentObject.w_MVNUMDOC
              this.oParentObject.w_ONUMDOC = this.oParentObject.w_MVNUMDOC
            endif
          endif
          if (this.oParentObject.w_MVNUMDOC<>this.oParentObject.op_MVNUMDOC OR this.oParentObject.w_MVALFDOC<>this.oParentObject.op_MVALFDOC) AND this.oParentObject.w_MVPRD<>"NN"
            this.w_MESS = "Numero e/o alfa documento modificati%0Confermi ugualmente?"
            this.w_OK = ah_YesNo(this.w_MESS)
          endif
          * --- Parametri per determinare se il documento � in sequenza
          this.w_FINEANNO = cp_CharToDate("31-12-"+ALLTR(STR(YEAR(this.oParentObject.w_MVDATDOC),4,0)))
          this.w_INIANNO = cp_CharToDate("01-01-"+ALLTR(STR(YEAR(this.oParentObject.w_MVDATDOC),4,0)))
          * --- Se numerazione libera non effettuo il controllo
          if this.w_OK and (this.oParentObject.w_MVPRD<>"NN" OR this.oParentObject.w_MVCLADOC="DI" OR this.oParentObject.w_MVCLADOC="DT")
            if this.oParentObject.w_MVCLADOC="FA" Or this.oParentObject.w_MVCLADOC="NC"
              * --- Se fattura verifico l'univocit� tra prima nota e documenti
              this.w_PARA = "R"
            else
              * --- altrimenti verifico l'univocit� nei documenti
              this.w_PARA = "L"
            endif
            * --- Se � un documento interno con intestatario o DDT verifica l'uguaglianza di questo anche se � attivata la numerazione libera
            if (this.oParentObject.w_MVCLADOC="DI" OR (this.oParentObject.w_MVCLADOC="DT" AND this.oParentObject.w_MVPRD="NN")) AND NOT EMPTY(NVL(this.oParentObject.w_MVCODCON,SPACE(20)))
              this.w_OK = UNIVOC(SPACE(10),iif( this.w_CFUNC="Load",Space(10),this.oParentObject.w_MVSERIAL),"V",this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVANNDOC, this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVPRD,0,SPACE(2),SPACE(4), this.oParentObject.w_MVCODCON, this.oParentObject.w_MVTIPCON, this.w_PARA, this.oParentObject.w_MVCLADOC)
            else
              if this.oParentObject.w_MVPRD<>"NN" and !( this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC="OR")
                this.w_OK = UNIVOC(SPACE(10),iif( this.w_CFUNC="Load",Space(10),this.oParentObject.w_MVSERIAL),"V",this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVANNDOC, cp_CharToDate("  -  -    "),this.oParentObject.w_MVPRD,0,SPACE(2),SPACE(4), SPACE(20), " ", this.w_PARA)
              endif
            endif
          endif
          * --- Tramite w_FLCHKNUMD inibisco i controlli in modifica se non ho modificato
          *     nessuno dei parametri della funzione
          if this.w_OK AND this.oParentObject.w_FLCHKNUMD
            * --- Controllo che non esistano documenti con numero maggiore e data minore
            *     o con numero minore e data maggiore del documento che si sta salvando
            *     Il filtro avviene sull'anno solare (Questi controlli sono x i registri IVA) e non sull'esercizio
            * --- Nel caso sia in caricamento non filtro sul seriale (la registrazione non � ancora sul
            *     database).
            *     Se MVPRD ='FV' ricerco anche considerando la prima nota..
            * --- Se � un documento interno con intestatario o DDT verifica l'uguaglianza di questo anche se � attivata la numerazione libera
            if (this.oParentObject.w_MVCLADOC="DI" OR (this.oParentObject.w_MVCLADOC="DT" AND this.oParentObject.w_MVPRD="NN"))
              this.w_TMPS = CHKNUMD(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVANNDOC,this.oParentObject.w_MVPRD,IIF(this.w_CFUNC="Edit",this.oParentObject.w_MVSERIAL,Space(10)),IIF(this.oParentObject.w_MVPRD="FV","D","T"), this.oParentObject.w_MVCLADOC, this.oParentObject.w_MVTIPCON, this.oParentObject.w_MVCODCON)
            else
              if this.oParentObject.w_MVPRD<>"NN"
                this.w_TMPS = CHKNUMD(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVANNDOC,this.oParentObject.w_MVPRD,IIF(this.w_CFUNC="Edit",this.oParentObject.w_MVSERIAL,Space(10)),IIF(this.oParentObject.w_MVPRD="FV","D","T"))
              endif
            endif
            if Not Empty( this.w_TMPS )
              this.w_OK = ah_YesNo("%1%0Confermi ugualmente?","", this.w_TMPS)
              this.w_MESS = ah_Msgformat("Operazione abbandonata")
            endif
          endif
        endif
        if this.oParentObject.w_MVCLADOC<>"OR"
          * --- Attenzione la var. w_DFLPP non � presente in gsor_mdv!
          if this.oParentObject.op_MVNUMEST=this.oParentObject.w_MVNUMEST AND this.oParentObject.op_MVALFEST=this.oParentObject.w_MVALFEST AND this.oParentObject.w_MVANNPRO<>"    " AND this.w_CFUNC="Load"
            * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
            i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
            cp_AskTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST")
            if this.oParentObject.op_MVNUMEST<>this.oParentObject.w_MVNUMEST
              * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
              *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch e della mInsert()
              this.w_MESS = "Riassegnato progressivo protocollo n. %1"
              ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(STR(this.oParentObject.w_MVNUMEST)) )
              this.oParentObject.op_MVNUMEST = this.oParentObject.w_MVNUMEST
              this.oParentObject.w_ONUMEST = this.oParentObject.w_MVNUMEST
            endif
          endif
          * --- Se fattura o nota di credito (doc. contabilizzabili)   verifico
          *     l'univocit� del protocollo (anche per la prima nota),(PARAMETRO='P')
          *      altrimenti verifico solo sui documenti (PARAMETRO='N') se la causale
          *      documento gestisce la numerazione protocollo
          if this.w_OK AND (this.oParentObject.w_MVCLADOC="FA" Or this.oParentObject.w_MVCLADOC="NC" OR this.oParentObject.w_DFLPP<>"N")
            if this.oParentObject.w_MVCLADOC="FA" Or this.oParentObject.w_MVCLADOC="NC"
              this.w_PARA = "P"
            else
              this.w_PARA = "N"
            endif
            this.w_OK = UNIVOC(SPACE(10),IIF( this.w_CFUNC="Load",Space(10),this.oParentObject.w_MVSERIAL),this.oParentObject.w_MVFLVEAC,0,SPACE(2),SPACE(4), cp_CharToDate("  -  -    "),this.oParentObject.w_MVPRP,this.oParentObject.w_MVNUMEST,this.oParentObject.w_MVALFEST,this.oParentObject.w_MVANNPRO,Space(15),Space(1),this.w_PARA)
          endif
          * --- Tramite w_FLCHKPRO inibisco i controlli in modifica se non ho modificato
          *     nessuno dei parametri della query
          if this.w_OK AND this.oParentObject.w_FLCHKPRO AND this.oParentObject.w_MVPRP<>"NN" AND this.oParentObject.w_MVFLVEAC="A"
            * --- Controlla che NON vi siano registrazioni (P. Nota) con numero minore e data maggiore
            * --- Oppure che non vi siano registrazioni con numero maggiore e data minore
            * --- I controlli vanno fatti in base all'anno solare e non all'esercizio
            this.w_INIANNO = cp_CharToDate("01-01-"+STR(YEAR(this.oParentObject.w_MVDATDOC),4,0))
            this.w_FINEANNO = cp_CharToDate("31-12-"+STR(YEAR(this.oParentObject.w_MVDATDOC),4,0))
            this.w_MESS = CHKPROT(this.oParentObject.w_MVNUMEST,this.oParentObject.w_MVALFEST,this.oParentObject.w_MVDATREG,this.oParentObject.w_MVANNPRO,this.oParentObject.w_MVPRP)
            if NOT EMPTY(this.w_MESS)
              this.w_oMESS.AddMsgPart(this.w_MESS)     
              this.w_OK = .F.
            endif
            if Not this.w_OK
              this.w_oMESS.AddMsgPart("%0Confermi ugualmente?")     
              this.w_OK = this.w_oMESS.ah_YesNo()
              this.w_MESS = ah_Msgformat("Operazione abbandonata")
            endif
          endif
        endif
        if this.w_OK AND this.oParentObject.w_MVCLADOC<>"OR" AND NOT EMPTY(this.oParentObject.w_MVANNPRO) And ( this.oParentObject.w_MVNUMEST<>this.oParentObject.op_MVNUMEST OR this.oParentObject.w_MVALFEST<>this.oParentObject.op_MVALFEST ) AND this.oParentObject.w_MVPRP<>"NN"
          this.w_MESS = "Numero e/o alfa protocollo modificati%0Confermi ugualmente?"
          this.w_OK = ah_YesNo(this.w_MESS)
        endif
        if this.oParentObject.w_MVFLVEAC="A"
          do case
            case this.w_OK AND this.oParentObject.w_MVSPEBOL<>0 AND EMPTY(this.oParentObject.w_MVIVABOL)
              this.w_MESS = "Inserire codice IVA spese bolli"
              ah_ErrorMsg(this.w_MESS)
              this.w_OK = .F.
            case this.w_OK AND this.oParentObject.w_MVIMPARR<>0 AND EMPTY(this.oParentObject.w_MVIVAARR)
              this.w_MESS = "Inserire codice IVA arrotondamenti"
              ah_ErrorMsg(this.w_MESS)
              this.w_OK = .F.
          endcase
          if this.w_OK And Not Empty( this.oParentObject.w_MVCODCON )
            * --- Se documento contabilizzabile verifico l'univocit� considerando
            *     anche le registrazioni di prima nota con tipo reg. iva posto ad 'A'
            *     Altrimenti verifico solo i documenti con stesso w_MVPRD
            this.w_TIPO = IIF( Not Empty( this.oParentObject.w_MVCAUCON ) , "D" , "Z" )
            this.w_OK = UNIVOC(SPACE(10),IIF( this.w_CFUNC="Load",Space(10),this.oParentObject.w_MVSERIAL),this.oParentObject.w_MVFLVEAC,this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVALFDOC,SPACE(4), this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVPRD,0,SPACE(2),SPACE(4), this.oParentObject.w_MVCODCON, this.oParentObject.w_MVTIPCON, this.w_TIPO, this.oParentObject.w_MVCLADOC)
          endif
        endif
      endif
      if this.w_OK And this.oParentObject.w_FLGCON="S" AND this.oParentObject.w_MVTIPCON="C" AND NOT EMPTY(this.oParentObject.w_MVCODCON) AND this.w_CFUNC="Load"
        * --- Blocco Documenti Da Contenzioso
        this.w_MESS = "Attivato blocco contenzioso per il cliente"
        ah_ErrorMsg(this.w_MESS)
        this.w_OK = .F.
      endif
      if this.w_OK And g_ACQU<>"S" And this.oParentObject.w_MVFLVEAC="A" And Not this.oParentObject.w_MVCLADOC$"DI-DT-OR"
        * --- Documento di Acquisto dal ciclo vendite <> da DDT o DI
        this.w_MESS = "Categoria documento utilizzabile solo da ciclo acquisti"
        ah_ErrorMsg(this.w_MESS)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMDTOBSO"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMDTOBSO;
            from (i_cTable) where;
                CMCODICE = this.oParentObject.w_MVTCAMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CAUOBS = NVL(cp_ToDate(_read_.CMDTOBSO),cp_NullValue(_read_.CMDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CAUOBS <= this.oParentObject.w_MVDATDOC AND Not Empty(Cp_Todate(this.w_CAUOBS))
          this.w_MESS = "La causale di magazzino associata alla causale del documento � obsoleta"
          ah_ErrorMsg(this.w_MESS)
          this.w_OK = .F.
        endif
      endif
      if this.w_OK
        this.w_CODAZI = i_CODAZI
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESINIESE,ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESINIESE,ESFINESE;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.oParentObject.w_MVCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          this.w_FINESE = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
            from (i_cTable) where;
                CMCODICE = this.oParentObject.w_MVTCAMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !(this.oParentObject.w_MVDATREG>=this.w_INIESE AND this.oParentObject.w_MVDATREG<=this.w_FINESE) AND (NOT EMPTY(this.w_FLCASC) OR NOT EMPTY(this.w_FLRISE) OR NOT EMPTY(this.w_FLIMPE) OR NOT EMPTY(this.w_FLORDI))
          this.w_MESS = "Data registrazione non compresa nell'esercizio di competenza%0Il movimento non verr� stampato sul giornale di magazzino (solo warning)"
          ah_ErrorMsg(this.w_MESS)
        endif
      endif
      if this.w_OK
        this.w_oGSVE_MRT = this.w_PADRE.GSVE_MRT
        if this.oParentObject.w_MVFLSCAF = "S"
          * --- Nel caso in cui si imposti il check "scadenze confermate" sulle rate 
          *     del documento � necessario controllare al salvataggio del documento 
          *     se la data scadenza � inferiore alla data del documento e segnalarlo 
          *     all'utente (messaggio di avviso).
          this.w_RATAKO = this.w_oGSVE_MRT.Search("t_RSDATRAT<ctod(' " +dtoc(this.oParentObject.w_MVDATDOC)+ " ') ")
          this.w_MESS = "Rate con data scadenza inferiore alla data del documento.%0Si desidera confermare?"
        else
          * --- Controllo rate con import negativo
          this.w_RATAKO = this.w_oGSVE_MRT.Search("t_RSIMPRAT<0")
          this.w_MESS = "Rate con importo negativo.%0Si desidera confermare?"
        endif
        if this.w_RATAKO>0
          this.w_OK = ah_YesNo(this.w_MESS)
        endif
      endif
      if this.w_OK AND this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVCLADOC $ "FA-NC" AND NOT EMPTY(this.oParentObject.w_MVCAUCON)
        * --- Controllo congruenza Soggetto Pubblico con Esigibilit� Differita
        this.w_FLESIG = ""
        this.w_FLIVDF = ""
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLESIG,ANIVASOS"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLESIG,ANIVASOS;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_MVTIPCON;
                and ANCODICE = this.oParentObject.w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLESIG = NVL(cp_ToDate(_read_.ANFLESIG),cp_NullValue(_read_.ANFLESIG))
          this.w_ANIVASOS = NVL(cp_ToDate(_read_.ANIVASOS),cp_NullValue(_read_.ANIVASOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLIVDF"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCAUCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLIVDF;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_MVCAUCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case (NVL(this.w_FLESIG,"")="S" OR Nvl(this.w_ANIVASOS," ") ="S" ) AND NVL(this.w_FLIVDF,"")=" "
            this.w_MESS = "Cliente di tipo soggetto pubblico o con maturazione temporale IVA in sospensione e causale contabile senza esigibilit� differita.%0Si desidera confermare?"
            this.w_OK = ah_YesNo(this.w_MESS)
        endcase
      endif
    endif
    * --- Scorro il dettaglio documento...
    if this.w_OK
      this.w_PADRE.MarkPos()     
      this.w_BMARKPOS = .T.
      this.w_PADRE.FirstRow()     
      this.w_COMPEV = 0
      this.w_TRIG = 0
      * --- Test se cancellata
      this.w_FLDEL = this.w_CFUNC="Query"
      * --- Array per controllo magazzino
      *     Non serve per righe cancellate
       
 DECLARE ArrGiom(1) 
 ArrGiom(1) = Space(5)
      this.w_NUMMAG = 0
      * --- Primo giro sulle righe non cancellate...
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Secondo giro, se tutto Ok sulle righe cancellate...
      *     (se in cancellazione dell'intero documento non necessario
      if this.w_OK And not this.w_FLDEL
        this.w_PADRE.FirstRowDel()     
        * --- Test Se riga Eliminata
        this.w_FLDEL = .T.
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRowDel()     
          else
            if not Empty( this.w_MESS )
              ah_ErrorMsg("%1",,"", this.w_MESS)
            endif
            Exit
          endif
        enddo
        if this.w_ERRLOTT AND (g_PERLOT="S" OR g_PERUBI="S")
          ah_ErrorMsg("Attenzione, non � stato inserito il dettaglio lotti/ubicazioni.", 48)
        endif
      endif
      this.w_FLDEL = this.w_CFUNC="Query"
      if this.w_OK And this.oParentObject.w_MVFLPROV<>"S" And g_PERDIS="S" 
        * --- Lancio il check disponibilit� articoli
        this.w_OK = GSAR_BDA( This , "S", this.w_PADRE )
      endif
    endif
    if this.w_OK AND g_EACD="S" AND this.oParentObject.w_MVFLPROV="N"
      if this.oParentObject.w_FLARCO="S" AND (NOT EMPTY(this.oParentObject.w_CAUPFI) OR NOT EMPTY(this.oParentObject.w_CAUCOD)) AND this.w_CFUNC <>"Query" 
        * --- Verifica Congruita' Causali
        if NOT EMPTY(this.oParentObject.w_CAUCOD)
          this.w_APPO = " "
          this.w_APPO1 = " "
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLINTE,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLINTE,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_CAUCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_APPO1 = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT (this.w_APPO1="DI" AND (this.w_APPO="N" OR this.w_APPO=this.oParentObject.w_MVFLINTE))
            this.w_MESS = "Causale documento articoli composti incongruente"
            ah_ErrorMsg(this.w_MESS)
            this.w_OK = .F.
          endif
        endif
        if this.w_OK AND NOT EMPTY(this.oParentObject.w_CAUPFI)
          this.w_APPO = " "
          this.w_APPO1 = " "
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLINTE,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUPFI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLINTE,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_CAUPFI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_APPO1 = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT (this.w_APPO1="DI" AND (this.w_APPO="N" OR this.w_APPO=this.oParentObject.w_MVFLINTE))
            this.w_MESS = "Causale documento articoli composti incongruente"
            ah_ErrorMsg(this.w_MESS)
            this.w_OK = .F.
          endif
        endif
      endif
      * --- Determino la presenza di righe con documenti di Carico\Scarico Componenti Non Evasi
      *     SERDOCESP: seriale del documento, se esiste, che ha generato il documento di esplosione che si sta cancellando
      * --- La funzione CHKDOCESP recupera L'MVSERIAL del documento padre
      this.w_SERDOCESP = CHKDOCESP(this.oParentObject.w_MVSERIAL)
      if this.w_OK AND this.w_CFUNC<>"Load" AND (Not Empty(this.w_SERDOCESP) Or (this.oParentObject.w_FLARCO="S" AND ((NOT EMPTY(this.oParentObject.w_CAUPFI) AND NOT EMPTY(this.oParentObject.w_MVRIFESP)) OR this.w_COMPEV>0)))
        * --- Documento Generante Evasione di Componenti/Prodotti Finiti ; Rigenera i Documenti di Evasione
        if NOT EMPTY(this.w_SERDOCESP)
          this.w_oMESS.AddMsgPartNL("Documento generato da esplosione componenti associata ad articoli composti")     
        else
          if this.w_CFUNC="Query"
            this.w_oMESS.AddMsgPartNL("Documento associato a causale di generazione documenti di evasione componenti/prodotti finiti/articoli kit/kit imballi%0I documenti di evasione associati verranno eliminati")     
          else
            if this.oParentObject.w_MVFLSCAF = "S" AND this.oParentObject.w_VALCOM <>"N"
              this.w_oMESS.AddMsgPartNL("Documento associato a causale di generazione documenti di evasione componenti/prodotti finiti/articoli kit/kit imballi%0I documenti di evasione associati verranno rigenerati%0Le rate confermate verranno ricalcolate in funzione del pagamento")     
            else
              this.w_oMESS.AddMsgPartNL("Documento associato a causale di generazione documenti di evasione componenti/prodotti finiti/articoli kit/kit imballi%0I documenti di evasione associati verranno rigenerati")     
            endif
          endif
        endif
        this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
        this.w_OK = this.w_oMESS.ah_YesNo()
        if Not this.w_OK AND this.w_CFUNC<>"Query"
          * --- Chiama subito l'esc
          KEYBOARD "{ESC}"
        endif
      endif
      if this.w_OK AND this.w_CFUNC="Load" AND (Not Empty(this.w_SERDOCESP) Or (this.oParentObject.w_FLARCO="S" AND (NOT EMPTY(this.oParentObject.w_CAUPFI) OR this.w_COMPEV>0)))
        if this.oParentObject.w_MVFLSCAF = "S" AND this.oParentObject.w_VALCOM <>"N" AND EMPTY(this.w_SERDOCESP)
          this.w_oMESS.AddMsgPartNL("Documento associato a causale di generazione documenti di evasione componenti/prodotti finiti/articoli kit/kit imballi%0I documenti di evasione associati verranno generati%0Le rate confermate verranno ricalcolate in funzione del pagamento")     
          this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
          this.w_OK = this.w_oMESS.ah_YesNo()
          if Not this.w_OK AND this.w_CFUNC<>"Query"
            * --- Chiama subito l'esc
            this.w_OK = .F.
          endif
        endif
      endif
    endif
    if Not this.w_FLDEL And g_COAC="S" And ( this.oParentObject.w_MVCLADOC="FA" Or this.oParentObject.w_MVCLADOC="NC" Or this.oParentObject.w_MVCLADOC="RF" )
      if Not this.w_OK
        * --- Nel caso l'utente preme calcola e le righe espose dai contributi di riga
        *     non siano corrette o i dati di riga dell'esplosione corretti ma devo ancora
        *     esplodere, memorizzo quindi il dato dell'analisi...
        * --- Se il dettaglio mi provoca il ricalcolo allora mantengo il valore e non lo cambio
        *     Se il dettaglio mi dice di elimianre solamente allora lo mantengo
        *     Altrimenti, se dettaglio 'N' mantengo w_TESTPESO che � il sunto dell'ultima
        *     esplosione di riga che ho fatto
        this.oParentObject.w_TESTPESO = iif(this.w_PESRES="N" , this.oParentObject.w_TESTPESO , this.w_PESRES )
      else
        * --- Chiamata routine per gestione contributi accessori a peso se:
        *     Dettaglio privo di errori bloccanti
        *     Attivo il modulo
        if this.w_PESRES="I" Or this.w_PESRES="D" Or this.oParentObject.w_TESTPESO="I" Or this.oParentObject.w_TESTPESO="D" 
          * --- Se non devo modificare ma inserire o non pi� gestito,  elimino le righe a peso...
          this.w_RESULT = GSAR_BG1( this, this.w_PADRE , "DTL")
        endif
        * --- I contributi a peso si ricalcolano sempre, se in modifica aggiorno, altrimenti
        *     inserimento e cancellazione si comportano come l'inserimento
        if this.w_PESRES="I" Or this.w_PESRES="U" Or this.w_PESRES="D" Or this.oParentObject.w_TESTPESO="I" Or this.oParentObject.w_TESTPESO="U" Or this.oParentObject.w_TESTPESO="D" 
          * --- Mi marco la riga attuale per svolgere i controlli delle righe inserite per il CONAI
          if this.w_PADRE.FullRow()
            this.w_ROWINDEX = this.w_PADRE.RowIndex()
          else
            * --- Se � sull'ultima riga vuota, si sposta sull'ultima piena
            this.w_PADRE.LastRow()     
            this.w_ROWINDEX = this.w_PADRE.RowIndex()
          endif
          GSAR_BGA(this, "DTL" , this.w_PADRE , 0 , this.oParentObject.w_CPROWORD , 10 , iif(this.w_PESRES="N" , iif(this.oParentObject.w_TESTPESO="D", "I", this.oParentObject.w_TESTPESO) , IIF(this.w_PESRES="D", "I" , this.w_PESRES)) ) 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_ROWINDEX<>this.w_PADRE.RowIndex()
            * --- Mi rimetto nella riga di esplosione...
            this.w_PADRE.SetRow(this.w_ROWINDEX)     
            this.w_PADRE.NextRow()     
            * --- Per le righe inserite devo verificare se tutto � ok...
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.oParentObject.w_TESTPESO = ""
      endif
    endif
    if this.w_OK AND this.w_TRIG=0 AND this.w_CFUNC<>"Query"
      this.w_MESS = "Registrazione senza righe di dettaglio"
      ah_ErrorMsg(this.w_MESS)
      this.w_OK = .F.
    endif
    if this.w_OK And this.oParentObject.w_TDMINVEN<>"E" AND this.w_CFUNC<>"Query" AND !isAlt()
      * --- Lancio il check quantit� minima
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_QTAWARN
        this.w_RESOCON1 = this.w_RESOCON1+this.w_oMESS1QTA.ComposeMessage()
      endif
      if this.w_QTABLOC
        this.w_OK = .F.
        this.w_MESBLOK = this.w_MESBLOK+this.w_oMESSQTA.ComposeMessage()
      endif
    endif
    if this.w_OK AND this.oParentObject.w_TDCHKUCA<>"N"
      this.w_OK = .F.
      this.w_SELECT = "CPROWNUM, t_CPROWORD, t_MVCODICE, t_MVDESART, t_MVKEYSAL, t_MVCODMAG, t_MVUNIMIS, t_MVQTAMOV, t_MVPREZZO, t_MVSCONT1, t_MVSCONT2, t_MVSCONT3"
      this.w_SELECT = this.w_SELECT + ", t_MVSCONT4, this.w_MVSCOCL1 AS t_MVSCOCL1, this.w_MVSCOCL2 AS t_MVSCOCL2, IIF(!EMPTY(t_MVIMPPRO) OR !EMPTY(t_MVPERPRO), 'S', 'N') AS t_MVFLCPRO"
      this.w_SELECT = this.w_SELECT + ", t_MVIMPNAZ, t_MVIMPPRO,  t_MVPERPRO, t_VALUCA AS t_UCA, t_ULTCAR AS t_DATE_UCA, t_MVCODLIS"
      this.w_SELECT = this.w_SELECT + ", this.w_MVCODVAL AS t_MVCODVAL, this.w_MVCAOVAL AS t_MVCAOVAL"
      this.w_SELECT = this.w_SELECT + ", this.w_MVTIPCON AS t_MVTIPCON, this.w_MVCODCON AS t_MVCODCON, this.w_MVDATDOC as t_MVDATDOC, t_MVQTAUM1, t_ARCHKUCA"
      this.w_SELECT = this.w_SELECT + ", t_MVCODART, t_MVIMPSCO"
      this.w_WHERECON = "!EMPTY(t_MVCODICE) AND t_MVTIPRIG not in ('D','A') AND NVL(t_ARCHKUCA,'N')<>'N' AND ( (this.cFunction='Load' AND EMPTY(t_MVSERRIF) )"
      this.w_WHERECON = this.w_WHERECON + "OR ( this.cFunction='Edit' AND (I_srv='A' OR (I_srv='U' AND (EMPTY(t_MVSERRIF) OR t_MVPREZZO<>t_DBPREZZO) ) ) ) )"
      this.w_PADRE.Exec_Select("_tmpUCA_", (this.w_SELECT), (this.w_WHERECON) , "t_CPROWORD", "CPROWNUM", "")     
      if USED("_tmpUCA_")
        wrcursor("_tmpUCA_")
        SELECT "_tmpUCA_"
        GO TOP
        SCAN
        this.w_PRZCHK = (_tmpUCA_.t_MVIMPNAZ / _tmpUCA_.t_MVQTAUM1) - IIF( EMPTY( _tmpUCA_.t_MVPERPRO ) , _tmpUCA_.t_MVIMPPRO , _tmpUCA_.t_MVPREZZO * _tmpUCA_.t_MVPERPRO / 100 )
        SELECT "_tmpUCA_"
        if this.w_PRZCHK >= _tmpUCA_.t_UCA
          * --- Elimino il record perch� il prezzo non � inferiore all'UCA
          DELETE
        endif
        SELECT "_tmpUCA_"
        ENDSCAN
        PACK
        if RECCOUNT( "_tmpUCA_" )>0
          * --- Visualizzo la maschera per la modifica dei prezzi per le righe errate
          this.w_GSVE_MIU = GSVE_MIU()
          if this.w_GSVE_MIU.bSec1
            if USED("_NewPrz_")
              * --- Aggiorno i prezzi in base a quanto impostato dall'utente
              SELECT "_NewPrz_"
              GO TOP
              SCAN
              this.w_PADRE.FirstRow()     
              this.w_PADRE.SetRow()     
              this.w_NUMREC = this.w_PADRE.Search("CPROWNUM="+ALLTRIM(STR(_NewPrz_.CPROWNUM) ) )
              if this.w_NUMREC<>-1
                this.w_PADRE.SetRow(this.w_NUMREC)     
                this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl("w_MVPREZZO")
                * --- Eseguo due valid per forzare il cambio delle variabili di riga
                *     Cambiando il record una sola volta il prezzo viene azzerato
                this.w_OBJCTRL.Value = 0
                this.w_OBJCTRL.bUpd = .t.
                this.w_OBJCTRL.Valid()     
                this.w_OBJCTRL.Value = _NewPrz_.LIPREZZO
                this.w_OBJCTRL.bUpd = .t.
                this.w_OBJCTRL.Valid()     
                this.w_OBJCTRL = .NULL.
                this.w_PADRE.SaveRow()     
              endif
              SELECT "_NewPrz_"
              ENDSCAN
              USE IN SELECT("_NewPrz_")
              this.w_PADRE.FirstRow()     
              this.w_PADRE.SetRow()     
              this.w_OK = .F.
              ah_ErrorMsg("Prezzi di riga aggiornati occorre rieseguire il salvataggio del documento", 48)
            else
              this.w_OK = this.oParentObject.w_TDCHKUCA="A"
              if !this.w_OK
                ah_ErrorMsg("Prezzi di riga non aggiornati e controllo prezzo bloccante. Impossibile salvare il documento", 48)
              else
                this.w_OK = ah_YesNo("Almeno una riga con prezzo inferiore ai valori di controllo e prezzi non aggiornati. Salvare comunque il documento?")
              endif
            endif
          else
            ah_ErrorMsg("Impossibile accedere alla gestione della verifica difformit� prezzi. Impossibile salvare il documento")
            this.w_OK = .F.
          endif
          this.w_GSVE_MIU = .NULL.
        else
          this.w_OK = .T.
        endif
      else
        this.w_OK = .T.
      endif
      USE IN SELECT("_tmpUCA_")
    endif
    if this.w_BMARKPOS
      * --- Repos in coda a tutto per evitare chiamate alla refesh mentre 
      *     possono essere attive operazioni sul transitorio
      this.w_PADRE.RePos(.F.)     
    endif
    * --- Blocco spostato sotto la Repos in quanto anche gsve_bcc (Ripaent) usa markpos e repos e si rilevavano problemi nel riposizionamento
    if this.w_OK and this.w_CFUNC<>"Query"
      this.oParentObject.w_RESCHK = 0
      * --- Esegue la ripartizione delle spese
      this.w_PADRE.NOTIFYEVENT("RipaEnt")     
      if this.oParentObject.w_RESCHK<>0
        this.w_OK = .F.
      endif
    endif
    if this.w_OK AND IsAlt() AND this.w_CFUNC="Load" AND this.w_CREALOG="N" AND this.oParentObject.w_MVTOTRIT=0
      * --- Solo se non ci sono stati errori, solo per AeT, solo in caricamento, solo nel caso di incongruenze (il log � stato creato) e solo se � il primo controllo (nel caso di w_MVTOTRIT<>0 si sta eseguendo il secondo controllo)
      this.w_ANNULLA = .F.
      this.w_ResMsg.AddMsgPartNL("Procedere con il salvataggio dell'attivit� comunque?")     
      this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
      this.w_OK = IIF(Not this.w_ANNULLA, .F., .T.)
    endif
    if ! Empty (this.w_MESBLOK) Or !Empty(this.w_RESOCON1)
      * --- Visualizzo la maschera con all'interno tutti i messagi di errore
      this.w_ANNULLA = False
      this.w_DAIM = False
      do GSVE_KLG with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_OK = this.w_ANNULLA
    endif
    * --- Non esegue la mCalc
    this.bUpdateParentObject = .F.
    * --- Notifica situazione di Errore alla CheckForm (non posso forzare direttamente la i_bRes in quanto locale)
    if Not this.w_OK
      this.oParentObject.w_RESCHK = -1
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sui dettaglio documenti...
    * --- La riga � valida...
    if this.w_PADRE.FullRow()
      this.w_ROWNUM = this.oParentObject.w_CPROWNUM
      this.w_ROWRIF = this.oParentObject.w_MVNUMRIF
      this.w_TRIG = this.w_TRIG + IIF( this.w_FLDEL , 0, 1)
      this.w_TIPRIG = NVL(this.oParentObject.w_MVTIPRIG," ")
      this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
      * --- Check movimenti esplosione distinta base...
      this.w_COMPEV = this.w_COMPEV + IIF( Not this.oParentObject.w_TESCOMP AND Not Empty(Nvl(this.oParentObject.w_MVRIFESC," ")) ,1, 0 )
      this.w_KEYSAL = NVL(this.oParentObject.w_MVKEYSAL, Space(20))
      this.w_CODMAG = NVL(this.oParentObject.w_MVCODMAG, " ")
      this.w_AGGSAL = ALLTRIM(NVL(this.oParentObject.w_MVFLCASC," ")+NVL(this.oParentObject.w_MVFLRISE," ")+NVL(this.oParentObject.w_MVFLORDI," ")+NVL(this.oParentObject.w_MVFLIMPE," "))
      this.w_CAUCOL = NVL(this.oParentObject.w_MVCAUCOL, " ")
      this.w_CODMAT = NVL(this.oParentObject.w_MVCODMAT, " ")
      this.w_AGGSAL1 = ALLTRIM(NVL(this.oParentObject.w_MVF2CASC," ")+NVL(this.oParentObject.w_MVF2RISE," ")+NVL(this.oParentObject.w_MVF2ORDI," ")+NVL(this.oParentObject.w_MVF2IMPE," "))
      this.w_SERRIF = NVL(this.oParentObject.w_MVSERRIF, Space(10))
      this.w_FLARIF = NVL(this.oParentObject.w_MVFLARIF, " ")
      * --- Numerica perch� � un flag sul dettaglio
      this.w_FLEVAS = NVL(this.oParentObject.w_MVFLEVAS, " ")
      this.w_OldFLEVAS = this.w_PADRE.Get("MVFLEVAS")
      if Type("this.w_OldFLEVAS")="N"
        this.w_OldFLEVAS = IIF(this.w_OldFLEVAS=1,"S"," ")
      endif
      this.w_QTAEV1 = NVL(this.oParentObject.w_MVQTAEV1, 0)
      this.w_IMPEVA = NVL(this.oParentObject.w_MVIMPEVA, 0)
      this.w_SRV = iif( this.w_FLDEL , " ", this.w_PADRE.RowStatus() )
      * --- Verifica se variati dati sensibili dich. di produzione
      this.w_FLDIMP = this.oParentObject.w_MVCODICE+this.oParentObject.w_MVUNIMIS<>Left ( this.oParentObject.w_OFLDIF , 23 )
      this.w_QTAUM1 = NVL(this.oParentObject.w_MVQTAUM1, 0)
      this.w_FLDIF = this.oParentObject.w_MVCODICE+this.oParentObject.w_MVUNIMIS+STR(this.oParentObject.w_MVQTAMOV,12,3)+this.oParentObject.w_MVFLOMAG<>this.oParentObject.w_OFLDIF
      this.w_TEST = ALLTRIM(STR(this.oParentObject.w_MVQTAUM1,12,3))+ ALLTRIM(STR(this.oParentObject.w_MVPREZZO,18,5))<>ALLTRIM(this.oParentObject.w_TESTEVA) OR this.w_FLDEL
      this.w_FLELGM = NVL(this.oParentObject.w_MVFLELGM,"")
      this.w_CODART = this.oParentObject.w_MVCODART
      this.w_CODLOT = NVL(this.oParentObject.w_MVCODLOT,Space(20))
      this.w_FLAGLO = NVL(this.oParentObject.w_MVFLLOTT," ")
      this.w_FLAG2LO = NVL(this.oParentObject.w_MVF2LOTT," ")
      this.w_COART = NVL(this.oParentObject.w_MVCODART, Space(20))
      this.w_CODUBI = NVL(this.oParentObject.w_MVCODUBI,Space(20))
      this.w_CODUB2 = NVL(this.oParentObject.w_MVCODUB2,Space(20))
      * --- Controlli non svolti in fase di cancellazione
      if Not this.w_FLDEL
        if this.w_OK AND !EMPTY(NVL(this.w_SERRIF, " ")) and this.oParentObject.w_MVNUMRIF<>-70 and this.oParentObject.w_MVROWRIF>0
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVCODODL"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVCODODL;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERRIF;
                  and CPROWNUM = this.oParentObject.w_MVROWRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODODL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
            use
            if i_Rows=0
              this.w_MESS = ah_Msgformat("Evasione documenti: La riga %1 evade una riga eliminata", this.w_NR )
              this.w_OK = .F.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !EMPTY(this.w_CODODL) and g_COLA="S" and g_PRFA="S" and this.oParentObject.w_MVFLVEAC="A" and this.oParentObject.w_MVCLADOC $ "DT-FA" and this.oParentObject.w_MVTIPRIG<>"D" AND !EMPTY(NVL(this.w_SERRIF, " ")) and this.oParentObject.w_MVNUMRIF<>-70 and this.oParentObject.w_MVROWRIF>0
            * --- Controllo Fasi per rientro conto lavoro
            this.w_FLERIF = this.oParentObject.w_MVFLERIF
            do GSCO1BRL with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if !EMPTY(this.w_CODODL) and g_COLA="S" and this.oParentObject.w_MVCLADOC $ "DT-FA" and this.oParentObject.w_MVFLVEAC="A"
            * --- Read from PAR_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PPIMPLOT"+;
                " from "+i_cTable+" PAR_PROD where ";
                    +"PPCODICE = "+cp_ToStrODBC("PP");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PPIMPLOT;
                from (i_cTable) where;
                    PPCODICE = "PP";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IMPLOT = NVL(cp_ToDate(_read_.PPIMPLOT),cp_NullValue(_read_.PPIMPLOT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            vq_exec ("QUERY\GSVE2BCK.VQR", this, "MATPRO")
            if USED("MATPRO") 
              Select MATPRO
              if RECCOUNT()>0 and MATPRO.COUNTMAT>0
                if VARTYPE(this.w_PADRE.GSAC_MMP.cnt)<>"O"
                  if this.w_IMPLOT="S"
                    this.w_OK = .F.
                    this.w_MESS = ah_Msgformat("Attenzione, non � stato inserito il dettaglio lotti/ubicazioni.%0Impossibile confermare.")
                  else
                    this.w_ERRLOTT = .t.
                  endif
                else
                  MT = this.w_PADRE.GSAC_MMP.cnt.ctrsName
                  Select (MT) 
 go top 
 scan for (t_FLLOTT<>"N" or t_FLUBI="S") and not deleted()
                  if vartype(this.w_PADRE.GSAC_MMP.cnt.GSAC_MLO.cnt) = "O"
                    Select (this.w_PADRE.GSAC_MMP.cnt.GSAC_MLO.cnt.ctrsName) 
 Count for (!empty(t_CLLOTART) or !empty(t_CLCODUBI)) and not deleted() to ncount
                    if ncount = 0
                      if this.w_IMPLOT="S"
                        this.w_OK = .F.
                        this.w_MESS = ah_Msgformat("Attenzione, non � stato inserito il dettaglio lotti/ubicazioni.%0Impossibile confermare.")
                      else
                        this.w_ERRLOTT = .t.
                      endif
                    endif
                  endif
                  endscan
                endif
              endif
            endif
            USE IN Select("MATPRO")
          endif
        endif
        if this.w_OK AND this.w_TIPRIG $ "MFRA" AND this.oParentObject.w_MVCLADOC<>"OR"
          * --- Controllo Presenza dati Analitica 
          if NOT EMPTY(this.oParentObject.w_MVCODCEN)
            if NOT EMPTY(this.oParentObject.w_CENOBSO) AND (this.oParentObject.w_CENOBSO<=this.oParentObject.w_OBTEST)
              this.w_MESS = ah_Msgformat("Centro di costo o ricavo obsoleto su riga %1",this.w_NR)
              this.w_OK = .F.
            endif
          endif
          * --- Controllo Voce di Costo \ Ricavo
          if NOT EMPTY(this.oParentObject.w_MVVOCCEN)
            if NOT EMPTY(this.oParentObject.w_VOCOBSO) AND (this.oParentObject.w_VOCOBSO<=this.oParentObject.w_OBTEST)
              this.w_MESS = ah_Msgformat("Voce di costo o ricavo obsoleta su riga %1",alltrim(this.w_NR) )
              this.w_OK = .F.
            endif
          endif
          if (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.oParentObject.w_MVFLNOAN="N") OR (g_COMM="S" AND this.oParentObject.w_FLGCOM="S" )
            do case
              case EMPTY(this.oParentObject.w_MVVOCCEN)
                this.w_MESS = ah_Msgformat("Verificare riga n.: %1 codice voce di costo non valorizzata", this.w_NR )
                this.w_OK = .F.
              case EMPTY(this.oParentObject.w_MVCODCEN) AND g_PERCCR="S" AND this.oParentObject.w_FLANAL="S" AND this.oParentObject.w_MVFLNOAN="N"
                this.w_MESS = ah_Msgformat("Verificare riga n.: %1 codice centro di costo non valorizzato", this.w_NR )
                this.w_OK = .F.
              case EMPTY(this.oParentObject.w_MVCODCOM) AND g_COMM="S" AND (this.oParentObject.w_FLGCOM="S" OR (this.oParentObject.w_FLGCOM="M" AND this.oParentObject.w_FLCOMM<>"N"))
                this.w_MESS = ah_Msgformat("Verificare riga n.: %1 codice commessa non valorizzato", this.w_NR )
                this.w_OK = .F.
            endcase
          endif
          * --- Controllo Inserita Attivita' di Commessa
          if EMPTY(this.oParentObject.w_MVCODCOM) AND NOT EMPTY(this.oParentObject.w_MVCODATT)
            * --- Se la commessa � vuota sbianco sempre il codice attivit�
            =setvaluelinked("MT", this.w_PADRE,"w_MVCODATT", SPACE(15),SPACE(15),"A")
          endif
          if this.w_OK AND g_COMM="S" AND EMPTY(this.oParentObject.w_MVCODATT) AND NOT EMPTY(this.oParentObject.w_MVCODCOM) and (this.oParentObject.w_FLGCOM="S" OR ( this.oParentObject.w_FLGCOM="M" AND this.oParentObject.w_FLCOMM<>"N") )
            this.w_MESS = ah_Msgformat("Verificare riga n.: %1 attivit� di commessa non definita", this.w_NR)
            this.w_OK = .F.
          endif
        endif
        if this.w_OK AND NOT EMPTY( this.w_COART ) AND NOT ISALT()
          * --- Controllo della data di obsolescenza degli articoli
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDTOBSO"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_COART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDTOBSO;
              from (i_cTable) where;
                  ARCODART = this.w_COART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ARDTOBSO = NVL( this.w_ARDTOBSO , CTOD( "  -  -  " ) )
          if NOT EMPTY(this.w_ARDTOBSO) AND (this.w_ARDTOBSO<=this.oParentObject.w_OBTEST)
            this.w_MESS = ah_Msgformat("Articolo %1 obsoleto su riga %2", ALLTRIM( this.w_COART ) , ALLTRIM(this.w_NR) )
            this.w_OK = .F.
          endif
        endif
        if this.w_OK AND NOT EMPTY( this.oParentObject.w_MVCONTRO )
          * --- Controllo della data di obsolescenza della contropartita contabile
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDTOBSO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCONTRO);
                  +" and ANTIPCON = "+cp_ToStrODBC("G");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDTOBSO;
              from (i_cTable) where;
                  ANCODICE = this.oParentObject.w_MVCONTRO;
                  and ANTIPCON = "G";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ANDTOBSO = NVL( this.w_ANDTOBSO , CTOD( "  -  -  " ) )
          if NOT EMPTY(this.w_ANDTOBSO) AND (this.w_ANDTOBSO<=this.oParentObject.w_OBTEST)
            this.w_MESS = ah_Msgformat("Contropartita %1 obsoleta su riga %2", ALLTRIM( this.oParentObject.w_MVCONTRO ) , ALLTRIM(this.w_NR) )
            this.w_OK = .F.
          endif
        endif
        if this.w_OK AND NOT EMPTY(this.w_KEYSAL)
          * --- Controllo Presenza Magazzino
          do case
            case EMPTY(this.w_CODMAG) AND NOT EMPTY(this.w_AGGSAL) 
              this.w_MESS = ah_Msgformat("Riga movimento: %1 inserire codice magazzino", this.w_NR )
              this.w_OK = .F.
            case NOT EMPTY(this.w_CAUCOL) AND EMPTY(this.w_CODMAT) AND NOT EMPTY(this.w_AGGSAL1)
              this.w_MESS = ah_Msgformat("Riga movimento: %1 inserire codice magazzino collegato", this.w_NR )
              this.w_OK = .F.
          endcase
        endif
        if this.w_FLDIMP
          do case
            case NOT EMPTY(this.w_SERRIF)
              * --- Documento Importato; Impossibile Variare Articolo, UM, Magazzino.
              this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile modificare codice articolo o U.M.%0se importati da altro documento" , this.w_NR)
              this.w_OK = .F.
            case (this.w_QTAEV1<>0 AND this.w_TIPRIG<>"F") OR (this.w_TIPRIG="F" AND this.w_IMPEVA<>0)
              * --- Documento Evaso; Impossibile Variare Articolo, UM, Magazzino.
              this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0Impossibile modificare codice articolo o U.M." , this.w_NR)
              this.w_OK = .F.
          endcase
        endif
        * --- Controllo se la riga � stampata nel Libro Giornale
        if this.w_OK And ((g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" ) OR g_PERUBI="S") AND this.w_TIPRIG $ "RM" AND NOT this.oParentObject.w_TESLOT
          do case
            case EMPTY(this.w_CODLOT) AND (this.w_FLAGLO $ "+-" OR this.w_FLAG2LO $ "+-") AND this.w_QTAUM1<>0 AND g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" 
              this.w_MESS = ah_Msgformat("Riga movimento: %1%0Articolo: %2%0Inserire codice lotto",this.w_NR, ALLTRIM(this.w_COART) )
              this.w_OK = .F.
            case g_PERUBI="S" And EMPTY(this.w_CODUBI) AND NOT EMPTY(this.w_CODMAG) AND this.oParentObject.w_FLUBIC="S" AND this.w_FLAGLO $ "+-"
              * --- Controllo Disponibilita' Lotti/Ubicazioni
              this.w_MESS = ah_Msgformat("Riga movimento: %1%0Magazzino: %2%0Inserire codice ubicazione",this.w_NR, alltrim(this.w_CODMAG))
              this.w_OK = .F.
            case g_PERUBI="S" And EMPTY(this.w_CODUB2) AND NOT EMPTY(this.w_CODMAT) AND this.oParentObject.w_F2UBIC="S" AND this.w_FLAG2LO $ "+-" 
              this.w_MESS = ah_Msgformat("Riga movimento: %1%0Magazzino: %2%0Inserire codice ubicazione collegata",this.w_NR, alltrim(this.w_CODMAT) )
              this.w_OK = .F.
          endcase
        endif
        if this.w_OK AND g_CESP="S" And this.oParentObject.w_MVCLADOC<>"OR" And this.w_TIPRIG $ "FM"
          * --- se il flag cespite � attivo l'abbinamento con un cespite o un movimento cespite � obbligatorio
          if this.oParentObject.w_FLCESP="S"
            this.w_QTA = NVL(this.oParentObject.w_MVQTAMOV,0)
            this.w_CPROWCES = NVL(this.oParentObject.w_CPROWORD,0)
            this.w_TIPOCON = IIF(EMPTY(this.oParentObject.w_MVCODCON),"N",this.oParentObject.w_MVTIPCON)
            if NOT EMPTY(this.oParentObject.w_MVCESSER)
              * --- Controllo di congruenza con il movimento cespite associato
              this.w_MESSCESP = CHKCESP(this.oParentObject.w_MVCESSER, this.oParentObject.w_MVNUMDOC,this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVALFDOC,this.oParentObject.w_MVCODCON,this.oParentObject.w_MVTIPCON, this.w_QTA)
              if Not Empty( this.w_MESSCESP )
                if ALLTRIM(this.w_MESSCESP)=ah_Msgformat("Mancante")
                  * --- Sbianco i  riferimenti dei cespiti sulla riga...
                  this.w_PADRE.Set("w_MVCESSER" , Space(10))     
                  this.w_PADRE.Set("w_MVCODCES" , Space(20))     
                  this.w_PADRE.SetRow()     
                else
                  this.w_MESS = "%1 del movimento cespite associato alla riga n� %2 non congruente%0Confermi ugualmente?"
                  this.w_OK = ah_YesNo(this.w_MESS,"", this.w_MESSCESP, ALLTRIM(STR(this.w_CPROWCES)) )
                  this.w_MESS = ""
                endif
              endif
            endif
            if EMPTY(this.oParentObject.w_MVCODCES) AND this.oParentObject.w_ASSCES<>"N"
              * --- controllo di obbligatoriet� se alla riga di tipo cespite non sono associati n� il movimento cespite n� il codice cespite
              this.w_MESS = ah_Msgformat("Nessun cespite/movimento cespite associato alla riga n� %1%0Impossibile confermare", ALLTRIM(STR(this.w_CPROWCES)) )
              this.w_OK = .F.
            endif
            if this.w_OK AND NOT EMPTY(this.oParentObject.w_CFCATCON) AND NOT EMPTY(this.oParentObject.w_MVCATCON) AND NOT EMPTY(this.oParentObject.w_MVCAUCON) AND this.oParentObject.w_ASSCES<>"N" 
              * --- Controllo sul tipo conto associato al servizio di tipo cespite
              if this.oParentObject.w_MVFLVEAC="V"
                * --- Contropartite Vendite
                * --- Read from CONTVEAC
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTVEAC_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CVCONRIC,CVCONNCC"+;
                    " from "+i_cTable+" CONTVEAC where ";
                        +"CVCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCATCON);
                        +" and CVCODCLI = "+cp_ToStrODBC(this.oParentObject.w_CFCATCON);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CVCONRIC,CVCONNCC;
                    from (i_cTable) where;
                        CVCODART = this.oParentObject.w_MVCATCON;
                        and CVCODCLI = this.oParentObject.w_CFCATCON;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONRIC),cp_NullValue(_read_.CVCONRIC))
                  this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCC),cp_NullValue(_read_.CVCONNCC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                * --- Contropartite Acquisti
                * --- Read from CONTVEAC
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTVEAC_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CVCONACQ,CVCONNCF"+;
                    " from "+i_cTable+" CONTVEAC where ";
                        +"CVCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCATCON);
                        +" and CVCODCLI = "+cp_ToStrODBC(this.oParentObject.w_CFCATCON);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CVCONACQ,CVCONNCF;
                    from (i_cTable) where;
                        CVCODART = this.oParentObject.w_MVCATCON;
                        and CVCODCLI = this.oParentObject.w_CFCATCON;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONACQ),cp_NullValue(_read_.CVCONACQ))
                  this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCF),cp_NullValue(_read_.CVCONNCF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if this.oParentObject.w_TIPREG="N"
                this.w_CONTRO = IIF(EMPTY(this.oParentObject.w_MVCONTRO), IIF(this.oParentObject.w_MVCLADOC = "FA", this.w_CONVEA, this.w_CONNAC), this.oParentObject.w_MVCONTRO)
              else
                this.w_CONTRO = IIF(EMPTY(this.oParentObject.w_MVCONTRO), IIF(this.oParentObject.w_TIPDOC $ "FA-FE-AU-FC", this.w_CONVEA, this.w_CONNAC), this.oParentObject.w_MVCONTRO)
              endif
              if NOT EMPTY(this.w_CONTRO)
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANTIPSOT"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANCODICE = "+cp_ToStrODBC(this.w_CONTRO);
                        +" and ANTIPCON = "+cp_ToStrODBC("G");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANTIPSOT;
                    from (i_cTable) where;
                        ANCODICE = this.w_CONTRO;
                        and ANTIPCON = "G";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_TIPSOT<>"M"
                  if NOT EMPTY(this.oParentObject.w_MVCONTRO)
                    this.w_MESS = ah_Msgformat("Il conto associato alla riga n� %1 non � di tipo cespite%0Impossibile confermare", ALLTRIM(STR(this.w_CPROWCES)) )
                  else
                    this.w_MESS = ah_Msgformat("La contropartita che in fase di contabilizzazione verr� associata alla riga n� %1 non � di tipo cespite%0Impossibile confermare", ALLTRIM(STR(this.w_CPROWCES)) )
                  endif
                  this.w_OK = .F.
                endif
              endif
            endif
          endif
        endif
      endif
      if this.w_OK and Empty(this.oParentObject.w_STSERRIF) and Right(Space(10)+this.w_SERRIF,10) <> Right(Space(10)+this.oParentObject.w_STSERRIF,10) and ((this.w_FLDEL and this.w_SRV <>"A") or this.w_CFUNC="Edit") AND this.w_FLARIF $ "+-"
        * --- Impedisco Modifica e Cancellazione se Riga che evade Documento Storicizzato
        this.w_OK = .F.
        this.w_MESS = ah_Msgformat("Impossibile modificare / cancellare la riga (%1). Evade un documento storicizzato", this.w_NR)
      endif
      if this.w_OK and this.w_CFUNC<>"Query"
        * --- Vengono memorizzati in un Array i Codici magazzino gi� controllati
        *     in modo che non vengano controllati due volte.
        *     Questa operazione viene eseguita per problemi di lentezza 
        *     nel caso di documenti con molte righe
        if (Not Empty(this.w_CODMAG) Or not Empty(this.w_CODMAT)) And this.w_FLELGM ="S"
          if ASCAN(ArrGiom, Left(Alltrim(this.w_CODMAG)+Space(5),5) )=0 
            if NOT EMPTY(this.w_CODMAG) AND ! CHKGIOM(this.oParentObject.w_MVCODESE,this.w_CODMAG,this.oParentObject.w_MVDATREG,this.w_FLELGM)
              this.w_MESS = ah_Msgformat("Registrazione gi� stampata sul giornale magazzino o blocco stampa attivo; impossibile variare/cancellare")
              this.w_OK = .F.
            endif
            if Not Empty(this.w_CODMAG)
               
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.w_CODMAG)+Space(5),5)
              this.w_NUMMAG = 1
            endif
          endif
          if ASCAN(ArrGiom, Left(Alltrim(this.w_CODMAT)+Space(5),5) )=0 
            if ! EMPTY(this.w_CODMAT) AND ! CHKGIOM(this.oParentObject.w_MVCODESE,this.w_CODMAT,this.oParentObject.w_MVDATREG,this.w_FLELGM)
              this.w_MESS = ah_Msgformat("Registrazione gi� stampata sul giornale magazzino o blocco stampa attivo; impossibile variare/cancellare")
              this.w_OK = .F.
            endif
            if Not Empty(this.w_CODMAT)
               
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.w_CODMAT)+Space(5),5)
              this.w_NUMMAG = 1
            endif
          endif
        endif
      endif
      * --- I controlli sull'evasione sono svolti sempre
      if this.w_OK AND this.w_CFUNC<>"Load"
        * --- Se sono in Variazione o Cancellazione Verifico se documento e' Evaso
        *     Se si Inibisco la Registrazione (Unica Eccezione i Documenti Interni)
        if this.oParentObject.w_FLGEFA="S" AND NOT EMPTY(NVL(this.oParentObject.w_MVDATGEN,cp_CharToDate("  -  -  "))) And this.oParentObject.w_MVTIPRIG<>"D"
          this.w_OLDOK = this.w_OK
          this.w_OK = .F.
          if TYPE("this.oParentObject.w_TDORDAPE")="U" OR this.oParentObject.w_TDORDAPE<>"S"
            if this.w_CFUNC="Query"
              this.w_MESS = ah_Msgformat("Il documento ha generato una fattura differita%0Impossibile eliminare")
            else
              this.w_MESS = ah_Msgformat("Il documento ha generato una fattura differita%0Impossibile variare")
            endif
          else
            * --- Se ordine aperto non do il msg
            this.w_OK = this.w_OLDOK
          endif
        else
          if this.oParentObject.w_MVCLADOC $ "OR-DI"
            * --- Se sono in Variazione o Cancellazione Verifico se documento e' Evaso. Se SI Inibisco la Registrazione
            do case
              case this.w_TIPRIG $ "R-M" AND ( (this.w_SRV="U" AND ABS(this.w_QTAEV1)>ABS(this.w_QTAUM1)) OR (this.w_FLDEL AND this.w_QTAEV1<>0) )
                if this.w_CFUNC="Query" or (this.w_FLDEL and this.w_QTAEV1>0 ) 
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare una riga parzialmente/totalmente evasa", this.w_NR)
                else
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile impostare una quantit� inferiore a quella gi� evasa", this.w_NR)
                endif
                this.w_OK = .F.
              case this.w_TIPRIG="F" AND (this.w_FLDEL AND this.w_IMPEVA<>0)
                * --- Quantita' Evasa o Importo evaso se riga Forfettaria
                if this.w_CFUNC="Query" or (this.w_FLDEL and this.w_IMPEVA>0)
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare una riga parzialmente/totalmente evasa", this.w_NR)
                else
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile impostare un valore inferiore a quello gi� evaso", this.w_NR)
                endif
                this.w_OK = .F.
            endcase
          else
            do case
              case this.oParentObject.w_FLGEFA="B" AND this.w_CFUNC<>"Query"
                * --- Controllo Variazione Riga Fattura Differita generata da P.Fatturazione in Variazione
                do case
                  case this.w_FLDEL AND this.w_SRV<>"A" AND (this.w_TIPRIG="R" OR (this.w_TIPRIG $ "FM" AND NOT EMPTY(this.w_SERRIF)))
                    * --- Fattura Differita; Impossibile Eliminare Righe (tranne le Righe Descrittive o aggiunte)
                    this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile eliminare una riga (non descrittiva)%0ad un documento di tipo fattura differita",this.w_NR)
                    this.w_OK = .F.
                  case this.w_SRV="A" AND Not this.w_FLDEL AND this.w_TIPRIG="R"
                    * --- Fattura Differita; Consentito inserire solo Righe Servizi
                    this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile aggiungere nuove righe articoli%0ad un documento di tipo fattura differita",this.w_NR)
                    this.w_OK = .F.
                  case this.w_SRV="U" AND Not this.w_FLDEL AND this.w_FLDIF
                    * --- Fattura Differita; Impossibile Variare Articolo, UM, Qta.
                    this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile modificare codice articolo o U.M. o quantit� o tipo riga%0ad un documento di tipo fattura differita",this.w_NR)
                    this.w_OK = .F.
                endcase
              case ((this.w_TIPRIG<>"F" AND this.w_QTAEV1<>0) OR (this.w_TIPRIG="F" AND this.w_IMPEVA<>0)) AND this.w_TEST
                * --- Quantita' Evasa o Importo evaso se riga Forfettaria
                if this.w_CFUNC="Query"
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare", this.w_NR)
                else
                  this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile variare", this.w_NR)
                endif
                this.w_OK = .F.
            endcase
          endif
        endif
        * --- Se cancellazione riga descrittiva o modifica della stessa disattivando il check evaso
        *     Impossibile eliminare o modificare
        *     OldFlevas � numerica perch� � un flag sul dettaglio 1 attivo 0 Disattivo
        if this.w_OK AND this.w_TIPRIG="D" And (this.w_CFUNC="Query" Or (this.w_SRV ="U" AND this.w_FLEVAS=" " And this.w_OldFLEVAS="S") Or (this.w_FLEVAS="S" And this.w_FLDEL))
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVSERIAL,MVFLARIF"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVSERIAL,MVFLARIF;
              from (i_cTable) where;
                  MVSERRIF = this.oParentObject.w_MVSERIAL;
                  and MVROWRIF = this.w_ROWNUM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERRDESC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            this.w_LFLARIF = NVL(cp_ToDate(_read_.MVFLARIF),cp_NullValue(_read_.MVFLARIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Controllo Righe Descrittive Evase da Altri Documenti
          if Not Empty( this.w_SERRDESC ) And not empty( this.w_LFLARIF )
            if this.w_CFUNC="Query"
              this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare", this.w_NR)
            else
              if this.w_SRV="D"
                this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare", this.w_NR)
              else
                this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile variare", this.w_NR)
              endif
            endif
            this.w_OK = .F.
          endif
        endif
      endif
      if this.w_OK AND (Not Empty(this.w_CODMAG) or Not Empty(this.w_CODMAT) )
        this.w_MESS = CHKCONS("M",this.oParentObject.w_MVDATREG,"B","N")
        this.w_OK = Empty( this.w_MESS )
      endif
      * --- Controllo assegnamento unit� logistica
      if this.w_OK AND this.oParentObject.w_FLPACK="S" AND g_MADV="S" 
        this.w_UL__SSCC = this.oParentObject.w_MVUNILOG
        if NOT EMPTY(NVL(this.w_UL__SSCC," "))
          this.w_OK = CheckSSCC("L", this.w_UL__SSCC, .t., .t., this.w_CODART, this.w_CODLOT, NVL(this.oParentObject.w_MVCODCOL,SPACE(5)))
        endif
      endif
      if this.w_OK And Not Empty( this.oParentObject.w_MVSERRIF ) And Not Empty( this.oParentObject.w_MVFLARIF ) And this.w_SRV<>"A"
        * --- Gestione MVFLERIF.
        *     Questo pezzo di codice mi serve per gestire la multi utenza. Se la riga
        *     evasa ha cambiato stato (MVFLEVAS ha un valore diverso) ora rispetto
        *     a quando � stata evasa da questo documento devo adeguare MVFLERIF
        *     
        *     Se riga modificata o cancellata con MVFLERIF vuoto
        *     e legata ad una riga di origine evasa allora imposto MVFLERIF a 'S' affinch�
        *     la query di aggiornamento massivo del documento di origine non riapra la riga
        *     (GSVE2AGG)
        if Empty(this.oParentObject.w_MVFLERIF) And this.oParentObject.w_OLDEVAS="S"
          this.w_PADRE.Set("w_MVFLERIF" , "S")     
        endif
        * --- Se riga modificata o cancellata con MVFLERIF pieno
        *     e legata ad una riga di origine non evasa allora imposto MVFLERIF a ' ' affinch�
        *     la query di aggiornamento massivo del documento di origine non chiuda la riga
        if this.oParentObject.w_MVFLERIF="S" And Empty( this.oParentObject.w_OLDEVAS )
          * --- Non � necessario ricercare eventuali righe che chiudono la riga sul documento
          *     in quanto se vi fossero, in virt� della MAX sulla query, l'avrebbero cmq vinta
          *     rispetto a questa riga.
          this.w_PADRE.Set("w_MVFLERIF" , " ")     
        endif
      endif
      if this.w_OK And Not this.w_FLDEL And g_COAC="S" 
        * --- Chiamata routine per gestione contributi accessori se:
        *     Riga corretto
        *     Attivo il modulo
        *     Articolo a magazzino
        * --- Se � cambiato qualcosa devo invocarlo sempre
        *     (la routine elimina anche eventuali righe di contributo non pi� valide
        this.w_RESULT = GSAR_BG1( this, this.w_PADRE , "ROW" , this.oParentObject.w_CPROWNUM )
        if this.w_RESULT = "E"
          * --- Ci sono righe evase che devono essere ricalcolate.
          this.w_RIGHERAEENONRICALCOLATE = .T.
        endif
        * --- E' cambiato qualcosa sulla riga che deve indurre la ri determianzione dei contributi accessori
        if this.w_RESULT="I" or this.w_RESULT="U"
          GSAR_BGA(this, "ROW" , this.w_PADRE , this.oParentObject.w_CPROWNUM , this.oParentObject.w_CPROWORD , 1 , this.w_RESULT ) 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Per la gestione a peso devo sapere se le righe provocano un ricalcolo o no..
        this.w_PESRES = iif( this.w_PESRES="I" Or this.w_PESRES="D", this.w_PESRES , IIF( this.w_RESULT="I" ,"I" , IIF( this.w_RESULT$"UE" ,"I" , IIF( this.w_RESULT="D" ,"D" , "N" )) ))
      endif
      if this.w_OK AND IsAlt() AND this.w_CFUNC="Load" AND this.oParentObject.w_MVTOTRIT=0
        * --- Controllo minimi e massimi in fatturazione
        * --- Solo se non ci sono stati errori, solo per AeT, solo in caricamento e solo se � il primo controllo (nel caso di w_MVTOTRIT<>0 si sta eseguendo il secondo controllo)
        this.w_FLMIMA = " "
        * --- Lettura da parametri gestione studio
        * --- Read from PAR_ALTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAFLMIMA"+;
            " from "+i_cTable+" PAR_ALTE where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAFLMIMA;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLMIMA = NVL(cp_ToDate(_read_.PAFLMIMA),cp_NullValue(_read_.PAFLMIMA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLMIMA="S"
          * --- Se � attivo il flag controllo minimi e massimi in fatturazione dei parametri gestione studio
          this.w_STACOD = " "
          * --- Lettura da tariffario
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSTACOD"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSTACOD;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STACOD = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT this.w_STACOD="S"
            * --- Se non � una notifica
            if (this.oParentObject.w_DEPREMIN<>0 AND this.oParentObject.w_DEPREMAX<>0) AND (this.oParentObject.w_MVPREZZO<this.oParentObject.w_DEPREMIN OR this.oParentObject.w_MVPREZZO>this.oParentObject.w_DEPREMAX)
              * --- Escluso il caso in cui il minimo e il massimo sono entrambi 0
              * --- Istanzia l'oggetto per la gestione delle messaggistiche di errore
              if this.w_CREALOG="S"
                this.w_CREALOG = "N"
              endif
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("L'importo della riga %1 (%2 - %3 ) di %4 %5 non � congruente con quanto indicato nel tariffario", Alltrim(Str(this.oParentObject.w_CPROWORD)), Alltrim(this.oParentObject.w_MVCODICE), Alltrim(this.oParentObject.w_MVDESART), Alltrim(Str(this.oParentObject.w_MVPREZZO,18,this.oParentObject.w_DECUNI)), Alltrim(this.oParentObject.w_SIMVAL2)))     
              this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("(Tariffa min. = %1, Tariffa max = %2)", Alltrim(Str(this.oParentObject.w_DEPREMIN)), Alltrim(Str(this.oParentObject.w_DEPREMAX))))     
              this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
            endif
          endif
        endif
      endif
      if this.w_OK AND !EMPTY(this.w_SERRIF) AND this.w_CFUNC<>"Query"
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVAGG_01,MVAGG_02,MVAGG_03,MVAGG_04,MVAGG_05,MVAGG_06"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVAGG_01,MVAGG_02,MVAGG_03,MVAGG_04,MVAGG_05,MVAGG_06;
            from (i_cTable) where;
                MVSERIAL = this.w_SERRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CKAGG_01 = NVL(cp_ToDate(_read_.MVAGG_01),cp_NullValue(_read_.MVAGG_01))
          this.w_CKAGG_02 = NVL(cp_ToDate(_read_.MVAGG_02),cp_NullValue(_read_.MVAGG_02))
          this.w_CKAGG_03 = NVL(cp_ToDate(_read_.MVAGG_03),cp_NullValue(_read_.MVAGG_03))
          this.w_CKAGG_04 = NVL(cp_ToDate(_read_.MVAGG_04),cp_NullValue(_read_.MVAGG_04))
          this.w_CKAGG_05 = NVL(cp_ToDate(_read_.MVAGG_05),cp_NullValue(_read_.MVAGG_05))
          this.w_CKAGG_06 = NVL(cp_ToDate(_read_.MVAGG_06),cp_NullValue(_read_.MVAGG_06))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Verifico che non vi siano altri documenti selezionati con campi aggiuntivi differenti da 
        *     quelli impostati nel documento selezionato
        Local L_MACRO
        this.w_IDFLDAGG = 1
        do while this.w_IDFLDAGG<=6
          local L_MACRO
          L_MACRO = "this.w_CKAGG_" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2) + "<> this.oParentObject.w_MVAGG_" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2)
          L_MACRO = L_MACRO + " AND ( this.oParentObject.w_TDFLRA" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2) +" ='A' OR ( this.oParentObject.w_TDFLRA" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2) +" ='S' AND !EMPTY(NVL(this.w_CKAGG_" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2) + " , ' ') ) ) )"
          if &L_MACRO
            L_MACRO = "this.oparentObject.w_DACAM_" + RIGHT("00" + ALLTRIM(STR(this.w_IDFLDAGG)), 2)
            this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("La riga <%1> ha il dato aggiuntivo %2 incongruente con quello impostato nel documento. Impossibile confermare", Alltrim(Str(this.oParentObject.w_CPROWORD)), ALLTRIM( &L_MACRO )))     
            this.w_MESBLOK = this.w_MESBLOK+this.w_ResMsg.ComposeMessage()
          endif
          this.w_IDFLDAGG = this.w_IDFLDAGG + 1
          release L_MACRO
        enddo
        if this.w_ERROR_AGG
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("La riga <%1> ha il dato aggiuntivo %2 incongruente con quello impostato nel documento. Impossibile confermare", Alltrim(Str(this.oParentObject.w_CPROWORD)), ALLTRIM( this.w_MESS_AGG )))     
          this.w_MESBLOK = this.w_MESBLOK+this.w_ResMsg.ComposeMessage()
        endif
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow()
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Se tutto ok passo alla prossima riga altrimenti esco...
      if this.w_OK
        this.w_PADRE.NextRow()     
      else
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1",,"", this.w_MESS)
        endif
        Exit
      endif
    enddo
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico che la quantit� venduta per articolo sia superiore al quantitativo minimo vendibile impostato in anagrafica articoli/codici di ricerca
    if this.oParentObject.w_TDRIOTOT="R"
      if this.oParentObject.w_MVFLVEAC="V"
        this.w_PADRE.Exec_Select("Trs_qtaven", "t_MVQTAUM1 As QTAVEN, t_RMINVEN As RMINVEN, t_MVCODICE As CODICE, t_UNMIS1  AS UNMIS1, t_CPROWORD AS RIGA" , "Nvl( t_RMINVEN ,0  )<>0 " ,"t_CPROWORD")     
      else
        this.w_PADRE.Exec_Select("Trs_qtaven", "t_MVQTAUM1 As QTAVEN, t_RMINVEN As RMINVEN, t_MVCODICE As CODICE, t_UNMIS1  AS UNMIS1, t_CPROWORD AS RIGA" , "Nvl( t_RMINVEN ,0  )<>0 and Nvl(t_CATIPCON,' ')='F' " ,"t_CPROWORD")     
      endif
    else
      if this.oParentObject.w_MVFLVEAC="V"
        this.w_PADRE.Exec_Select("Trs_qtaven", "Sum(t_MVQTAUM1) As QTAVEN, Max(t_RMINVEN) As RMINVEN, Max(t_MVCODICE) As CODICE, Max(t_UNMIS1) AS UNMIS1, max(t_CPROWORD) AS RIGA " , "Nvl( t_RMINVEN ,0  )<>0 " ,"t_CPROWORD","t_MVCODICE ","")     
      else
        this.w_PADRE.Exec_Select("Trs_qtaven", "Sum(t_MVQTAUM1) As QTAVEN, Max(t_RMINVEN) As RMINVEN, Max(t_MVCODICE) As CODICE, Max(t_UNMIS1) AS UNMIS1, max(t_CPROWORD) AS RIGA " , "Nvl( t_RMINVEN ,0  )<>0 and Nvl(t_CATIPCON,' ')='F'" ,"t_CPROWORD","t_MVCODICE ","")     
      endif
    endif
    do while Not Eof( "Trs_qtaven" )
      this.w_QTAVEN = Nvl(Trs_qtaven.QTAVEN,0)
      this.w_QTAMIN = Nvl(Trs_qtaven.RMINVEN,0)
      this.w_CODICE = NVL(Trs_qtaven.CODICE, SPACE(20))
      this.w_UNMIS1 = NVL(Trs_qtaven.UNMIS1, SPACE(3))
      this.w_RIGA = Nvl(Trs_qtaven.RIGA,0)
      if this.w_QTAVEN<this.w_QTAMIN
        if this.oParentObject.w_TDMINVEN="A"
          * --- Solo warning
          this.w_QTAWARN = .T.
          if this.oParentObject.w_TDRIOTOT="R"
            if this.oParentObject.w_MVCLADOC="OR"
              this.w_oPART1QTA = this.w_oMESS1QTA.addmsgpartNL("Riga %1: Articolo: %2%0Quantit� ordinata (%5 %3) inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca (%5 %4)")
            else
              this.w_oPART1QTA = this.w_oMESS1QTA.addmsgpartNL("Riga %1: Articolo: %2%0Quantit� venduta (%5 %3) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%5 %4)")
            endif
            this.w_oPART1QTA.AddParam(ALLTR( STR(this.w_RIGA)))     
            this.w_oPART1QTA.AddParam(ALLTR( this.w_CODICE ))     
            this.w_oPART1QTA.AddParam(ALLTR( STR(this.w_QTAVEN,12,3)))     
            this.w_oPART1QTA.AddParam(ALLTR( STR(this.w_QTAMIN,12,3)))     
            this.w_oPART1QTA.AddParam(ALLTR( this.w_UNMIS1))     
          else
            if this.oParentObject.w_MVCLADOC="OR"
              this.w_oPART1QTA = this.w_oMESS1QTA.addmsgpartNL("Articolo: %1%0Quantit� ordinata (%4 %2) inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
            else
              this.w_oPART1QTA = this.w_oMESS1QTA.addmsgpartNL("Articolo: %1%0Quantit� venduta (%4 %2) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
            endif
            this.w_oPART1QTA.AddParam(ALLTR( this.w_CODICE ))     
            this.w_oPART1QTA.AddParam(ALLTR( STR(this.w_QTAVEN,12,3)))     
            this.w_oPART1QTA.AddParam(ALLTR( STR(this.w_QTAMIN,12,3)))     
            this.w_oPART1QTA.AddParam(ALLTR( this.w_UNMIS1))     
          endif
        else
          * --- Bloccante
          this.w_QTABLOC = .T.
          if this.oParentObject.w_TDRIOTOT="R"
            if this.oParentObject.w_MVCLADOC="OR"
              this.w_oPARTQTA = this.w_oMESSQTA.addmsgpartNL("Riga %1: Articolo: %2%0Quantit� ordinata (%5 %3) inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca (%5 %4)")
            else
              this.w_oPARTQTA = this.w_oMESSQTA.addmsgpartNL("Riga %1: Articolo: %2%0Quantit� venduta (%5 %3) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%5 %4)")
            endif
            this.w_oPARTQTA.AddParam(ALLTR( STR(this.w_RIGA)))     
            this.w_oPARTQTA.AddParam(ALLTR( this.w_CODICE ))     
            this.w_oPARTQTA.AddParam(ALLTR( STR(this.w_QTAVEN,12,3)))     
            this.w_oPARTQTA.AddParam(ALLTR( STR(this.w_QTAMIN,12,3)))     
            this.w_oPARTQTA.AddParam(ALLTR( this.w_UNMIS1))     
          else
            if this.oParentObject.w_MVCLADOC="OR"
              this.w_oPARTQTA = this.w_oMESSQTA.addmsgpartNL("Articolo: %1%0Quantit� ordinata (%4 %2) inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
            else
              this.w_oPARTQTA = this.w_oMESSQTA.addmsgpartNL("Articolo: %1%0Quantit� venduta (%4 %2) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
            endif
            this.w_oPARTQTA.AddParam(ALLTR( this.w_CODICE ))     
            this.w_oPARTQTA.AddParam(ALLTR( STR(this.w_QTAVEN,12,3)))     
            this.w_oPARTQTA.AddParam(ALLTR( STR(this.w_QTAMIN,12,3)))     
            this.w_oPARTQTA.AddParam(ALLTR( this.w_UNMIS1))     
          endif
        endif
      endif
       
 Select trs_qtaven 
 Skip
    enddo
    * --- Rimuovo il temporaneo
    USE IN SELECT("trs_qtaven")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject)
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,21)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='ADDE_SPE'
    this.cWorkTables[11]='ESERCIZI'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='CONTVEAC'
    this.cWorkTables[14]='PAR_ALTE'
    this.cWorkTables[15]='CAU_CONT'
    this.cWorkTables[16]='VOC_COST'
    this.cWorkTables[17]='CENCOST'
    this.cWorkTables[18]='CAN_TIER'
    this.cWorkTables[19]='PAR_PROD'
    this.cWorkTables[20]='PAR_RIOR'
    this.cWorkTables[21]='AZDATINT'
    return(this.OpenAllTables(21))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
