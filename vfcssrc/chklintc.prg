* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chklintc                                                        *
*              Controlli lett. intento                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_3]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2006-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE,pANNO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tchklintc",oParentObject,m.pTIPOPE,m.pANNO)
return(i_retval)

define class tchklintc as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  pANNO = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola le date di inizio e fine validita' della lettera di intento e l'importo quando
    * --- l'utente varia il campo "tipo operazione".
    * --- Parametri
    * --- Ricalcola le date
    if this.pTIPOPE $ "IO"
      this.oParentObject.w_DIDATINI = cp_CharToDate("01-01-"+this.pANNO)
      this.oParentObject.w_DIDATFIN = cp_CharToDate("31-12-"+this.pANNO)
    endif
    * --- Ricalcola l'importo
    if this.pTIPOPE $ "OD"
      this.oParentObject.w_DIIMPDIC = 0
    endif
  endproc


  proc Init(oParentObject,pTIPOPE,pANNO)
    this.pTIPOPE=pTIPOPE
    this.pANNO=pANNO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE,pANNO"
endproc
