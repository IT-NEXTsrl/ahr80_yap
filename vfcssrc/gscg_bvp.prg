* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bvp                                                        *
*              Visualizza primanota                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_194]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2010-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Param
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bvp",oParentObject,m.Param)
return(i_retval)

define class tgscg_bvp as StdBatch
  * --- Local variables
  Param = space(1)
  w_Zoom = space(10)
  w_NOT1 = space(50)
  w_LOOP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Param = 'R'  :  esegue query ed il refresh dello zoom
    *     Param = 'S'  :  stampa elenco dello zoom
    *     Param = 'Z'  :  modifica zoom all'init della maschera
    * --- Variabili
    this.oParentObject.w_CODLIR = g_CODLIR
    this.oParentObject.w_CODEUR = g_CODEUR
    this.w_Zoom = this.oParentObject.w_ZoomPNot
    NC = this.w_Zoom.cCursor
    do case
      case this.Param = "R"
        This.OparentObject.NotifyEvent("Calcola")
        * --- Filtro sul campo riferimento descrittivo
        this.w_NOT1 = this.oParentObject.w_NOTE
        if NOT EMPTY(this.w_NOT1) 
          FIL = "'%"+ALLTRIM(UPPER(this.w_NOT1))+"%'"
          SELECT (NC)
          GO TOP
          delete from &NC where UPPER(NVL(PNDESSUP,SPACE(30))) not like &FIL
        endif
        * --- Se si proviene da Visualizza movimenti c/terzi
        if this.oParentObject.w_PARAMETRO="M"
          * --- Aggiorna i totali
          this.oParentObject.w_TOTDAR = 0
          this.oParentObject.w_TOTAVE = 0
          SELECT (NC)
          GO TOP
          SUM TOTDAR, TOTAVE TO this.oParentObject.w_TOTDAR, this.oParentObject.w_TOTAVE
        endif
        * --- Modifica e refresh dello zoom
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Param = "S"
        * --- Stampa elenco zoom
        L_REGINI = this.oParentObject.w_NREGINI
        L_REGFIN = this.oParentObject.w_NREGFIN
        L_CODUTINI = this.oParentObject.w_CODUTINI
        L_CODUTFIN = this.oParentObject.w_CODUTFIN
        L_DREGINI = this.oParentObject.w_DREGINI
        L_DREGFIN = this.oParentObject.w_DREGFIN
        L_DOCINI = this.oParentObject.w_DOCINI
        L_DOCFIN = this.oParentObject.w_DOCFIN
        L_ALFDOCINI = this.oParentObject.w_ALFDOCINI
        L_ALFDOCFIN = this.oParentObject.w_ALFDOCFIN
        L_DADOCINI = this.oParentObject.w_DADOCINI
        L_DADOCFIN = this.oParentObject.w_DADOCFIN
        L_CODCAU = this.oParentObject.w_CODCAU
        L_CODESE = this.oParentObject.w_CODESE
        L_CODCLF = this.oParentObject.w_CODCLF
        L_FLRIFE = this.oParentObject.w_FLRIFE
        L_DESCAU = this.oParentObject.w_DESCAU
        L_DESCLF = this.oParentObject.w_DESCLF
        L_DECTOP = this.oParentObject.w_DECTOP
        L_TOTDAR = this.oParentObject.w_TOTDAR
        L_TOTAVE = this.oParentObject.w_TOTAVE
        L_SALDOD = this.oParentObject.w_SALDOD
        L_SALDOA = this.oParentObject.w_SALDOA
        Select * from (NC) into cursor __Tmp__
        Select __Tmp__
        if this.oParentObject.w_PARAMETRO="P"
          * --- Stampa registrazioni di primanota
          CP_CHPRN("QUERY\GSCG_KVP.FRX")
        else
          * --- Stampa movimenti c/terzi
          CP_CHPRN("QUERY\GSCG2KVP.FRX")
        endif
      case this.Param = "Z"
        * --- Modifica e refresh dello zoom alla Init della maschera
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica zoom (eliminazione colonne)
    this.w_LOOP = 1
    do while this.w_LOOP <= this.w_Zoom.grd.ColumnCount
      do case
        case this.oParentObject.w_PARAMETRO="P" AND INLIST( UPPER( this.w_Zoom.grd.Columns[ this.w_LOOP ].ControlSource), "TOTDAR", "TOTAVE", "PNSERIAL" )
          this.w_Zoom.grd.Columns[ this.w_LOOP ].Visible = .F.
        case this.oParentObject.w_PARAMETRO="M" AND INLIST( UPPER( this.w_Zoom.grd.Columns[ this.w_LOOP ].ControlSource), "PNNUMDOC", "PNALFDOC", "PNDATDOC", "PNNUMPRO", "PNALFPRO", "VACODVAL", "PNFLPROV", "PNDESSUP", "PNSERIAL" )
          this.w_Zoom.grd.Columns[ this.w_LOOP ].Visible = .F.
      endcase
      this.w_LOOP = this.w_LOOP + 1
    enddo
    SELECT (NC)
    GO TOP
    * --- Refresh della griglia
    this.w_Zoom.refresh()
  endproc


  proc Init(oParentObject,Param)
    this.Param=Param
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Param"
endproc
