* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_btr                                                        *
*              Check delete tip.righe documento                                *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-05                                                      *
* Last revis.: 2004-03-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_btr",oParentObject)
return(i_retval)

define class tgsar_btr as StdBatch
  * --- Local variables
  w_CODICE = space(20)
  w_MESS = space(100)
  w_ROW = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  * --- WorkFile variables
  ART_ICOL_idx=0
  TIP_DOCU_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico, in caso di cancellazione, se la tipologia riga � utilizzata in
    *     articoli, causali docuemnti e dettaglio documenti.
    * --- Verifico la presenza in articoli
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARCODART"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODCLA = "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARCODART;
        from (i_cTable) where;
            ARCODCLA = this.oParentObject.w_TRCODCLA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODICE = NVL(cp_ToDate(_read_.ARCODART),cp_NullValue(_read_.ARCODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty( this.w_CODICE )
      * --- Articolo
      this.w_MESS = ah_Msgformat("Tipologia riga non eliminabile, presente sull'anagrafica articolo/servizio %1", this.w_CODICE)
    else
      * --- Verifico nelle casuali documento
      * --- Select from TIP_DOCU
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select TDTIPDOC  from "+i_cTable+" TIP_DOCU ";
            +" where TDTPNDOC= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDTPVDOC= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDTPRDES= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDESCCL1= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDESCCL2= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDESCCL3= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDESCCL4= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDESCCL5= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDSTACL1= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDSTACL2= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDSTACL3= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDSTACL4= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+" Or TDSTACL5= "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA)+"       ";
             ,"_Curs_TIP_DOCU")
      else
        select TDTIPDOC from (i_cTable);
         where TDTPNDOC= this.oParentObject.w_TRCODCLA Or TDTPVDOC= this.oParentObject.w_TRCODCLA Or TDTPRDES= this.oParentObject.w_TRCODCLA Or TDESCCL1= this.oParentObject.w_TRCODCLA Or TDESCCL2= this.oParentObject.w_TRCODCLA Or TDESCCL3= this.oParentObject.w_TRCODCLA Or TDESCCL4= this.oParentObject.w_TRCODCLA Or TDESCCL5= this.oParentObject.w_TRCODCLA Or TDSTACL1= this.oParentObject.w_TRCODCLA Or TDSTACL2= this.oParentObject.w_TRCODCLA Or TDSTACL3= this.oParentObject.w_TRCODCLA Or TDSTACL4= this.oParentObject.w_TRCODCLA Or TDSTACL5= this.oParentObject.w_TRCODCLA       ;
          into cursor _Curs_TIP_DOCU
      endif
      if used('_Curs_TIP_DOCU')
        select _Curs_TIP_DOCU
        locate for 1=1
        do while not(eof())
        this.w_CODICE = Nvl ( _Curs_TIP_DOCU.TDTIPDOC ,"" )
          select _Curs_TIP_DOCU
          continue
        enddo
        use
      endif
      if Not Empty( this.w_CODICE )
        this.w_MESS = ah_Msgformat("Tipologia riga non eliminabile, presente sulla causale documento", this.w_CODICE)
      else
        * --- Verifico sulle righe documento
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,CPROWORD"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVCODCLA = "+cp_ToStrODBC(this.oParentObject.w_TRCODCLA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,CPROWORD;
            from (i_cTable) where;
                MVCODCLA = this.oParentObject.w_TRCODCLA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODICE = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_ROW = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty( this.w_CODICE )
          * --- Leggo i riferimenti documento...
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MESS = ah_Msgformat("Tipologia riga non eliminabile, presente sulla riga %1 del documento di tipo %2%0(Documento n. %3 del %4)", Alltrim(Str(this.w_ROW)), w_TIPDOC, ALLTRIM(STR(this.w_NUMDOC,15,0))+ iif(NOT EMPTY(this.w_ALFDOC), "/"+ALLTRIM(this.w_ALFDOC),""), DTOC(this.w_DATDOC) )
        endif
      endif
    endif
    this.oParentObject.w_HASEVENT = Empty( this.w_CODICE )
    if Not this.oParentObject.w_HASEVENT
      ah_ErrorMsg("%1",,"", this.w_MESS)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
