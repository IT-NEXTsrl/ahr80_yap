* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_klw                                                        *
*              Autenticazione infinity project                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-14                                                      *
* Last revis.: 2014-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_klw",oParentObject))

* --- Class definition
define class tgsut_klw as StdForm
  Top    = 25
  Left   = 33

  * --- Standard Properties
  Width  = 626
  Height = 209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-17"
  HelpContextID=216613737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  UTE_NTI_IDX = 0
  AUT_LPE_IDX = 0
  CPUSRGRP_IDX = 0
  PARA_EDS_IDX = 0
  cPrg = "gsut_klw"
  cComment = "Autenticazione infinity project"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_UTCODICE = 0
  w_UTDESUTE = space(40)
  w_AZIENDA = space(10)
  w_CDINFURL = space(254)
  w_CDAPPCOD = space(5)
  w_UTIDUSER = space(254)
  w_XXPASSWD = space(254)
  o_XXPASSWD = space(254)
  w_UTIDPSSW = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_klwPag1","gsut_klw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUTIDUSER_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='AUT_LPE'
    this.cWorkTables[3]='CPUSRGRP'
    this.cWorkTables[4]='PARA_EDS'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UTCODICE=0
      .w_UTDESUTE=space(40)
      .w_AZIENDA=space(10)
      .w_CDINFURL=space(254)
      .w_CDAPPCOD=space(5)
      .w_UTIDUSER=space(254)
      .w_XXPASSWD=space(254)
      .w_UTIDPSSW=space(254)
      .w_UTCODICE=oParentObject.w_UTCODICE
      .w_UTDESUTE=oParentObject.w_UTDESUTE
      .w_UTIDUSER=oParentObject.w_UTIDUSER
      .w_UTIDPSSW=oParentObject.w_UTIDPSSW
          .DoRTCalc(1,2,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_9('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate('*')
    endwith
    this.DoRTCalc(4,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_UTCODICE=.w_UTCODICE
      .oParentObject.w_UTDESUTE=.w_UTDESUTE
      .oParentObject.w_UTIDUSER=.w_UTIDUSER
      .oParentObject.w_UTIDPSSW=.w_UTIDPSSW
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_9('Full')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate('*')
        if .o_XXPASSWD<>.w_XXPASSWD
          .Calculate_YSIDYAOZHI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate('*')
    endwith
  return

  proc Calculate_YSIDYAOZHI()
    with this
          * --- Cifra password
          .w_UTIDPSSW = CifraCnf( ALLTRIM(.w_XXPASSWD) , 'C' )
    endwith
  endproc
  proc Calculate_EYFXYUGQKX()
    with this
          * --- Decifra password
          .w_XXPASSWD = CifraCnf( ALLTRIM(.w_UTIDPSSW) , 'D' )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YSIDYAOZHI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_EYFXYUGQKX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_EDS_IDX,3]
    i_lTable = "PARA_EDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2], .t., this.PARA_EDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODAZI,CDINFURL,CDAPPCOD";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODAZI',this.w_AZIENDA)
            select CDCODAZI,CDINFURL,CDAPPCOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.CDCODAZI,space(10))
      this.w_CDINFURL = NVL(_Link_.CDINFURL,space(254))
      this.w_CDAPPCOD = NVL(_Link_.CDAPPCOD,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(10)
      endif
      this.w_CDINFURL = space(254)
      this.w_CDAPPCOD = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_EDS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PARA_EDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value==this.w_UTCODICE)
      this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDESUTE_1_2.value==this.w_UTDESUTE)
      this.oPgFrm.Page1.oPag.oUTDESUTE_1_2.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCDINFURL_1_10.value==this.w_CDINFURL)
      this.oPgFrm.Page1.oPag.oCDINFURL_1_10.value=this.w_CDINFURL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDAPPCOD_1_11.value==this.w_CDAPPCOD)
      this.oPgFrm.Page1.oPag.oCDAPPCOD_1_11.value=this.w_CDAPPCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIDUSER_1_13.value==this.w_UTIDUSER)
      this.oPgFrm.Page1.oPag.oUTIDUSER_1_13.value=this.w_UTIDUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oXXPASSWD_1_14.value==this.w_XXPASSWD)
      this.oPgFrm.Page1.oPag.oXXPASSWD_1_14.value=this.w_XXPASSWD
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_UTCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_UTCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDINFURL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDINFURL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CDINFURL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKUSRINF(.w_UTIDUSER, .w_UTCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTIDUSER_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_XXPASSWD = this.w_XXPASSWD
    return

enddefine

* --- Define pages as container
define class tgsut_klwPag1 as StdContainer
  Width  = 622
  height = 209
  stdWidth  = 622
  stdheight = 209
  resizeXpos=259
  resizeYpos=71
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTCODICE_1_1 as StdField with uid="TTXYNRTTYX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente",;
    HelpContextID = 11159179,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=180, Top=9, cSayPict="'9999'", cGetPict="'9999'"

  add object oUTDESUTE_1_2 as StdField with uid="AAAOTEAHAP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 227563147,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=242, Top=9, InputMask=replicate('X',40)


  add object oBtn_1_3 as StdButton with uid="APIWEFNJEL",left=509, top=158, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare login/password";
    , HelpContextID = 142767638;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="ZJIUAZYUNY",left=566, top=158, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142767638;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCDINFURL_1_10 as StdField with uid="BMSXGUBRTZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CDINFURL", cQueryName = "CDINFURL",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo internet del server di Infinity",;
    HelpContextID = 214537586,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=180, Top=38, InputMask=replicate('X',254)

  add object oCDAPPCOD_1_11 as StdField with uid="PMFPBZPKOA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CDAPPCOD", cQueryName = "CDAPPCOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice applicazione registrata su Infinity",;
    HelpContextID = 191567210,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=180, Top=64, InputMask=replicate('X',5)

  add object oUTIDUSER_1_13 as StdField with uid="GPKLNZGWVZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UTIDUSER", cQueryName = "UTIDUSER",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Utente infinity",;
    HelpContextID = 72374632,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=180, Top=103, InputMask=replicate('X',254)

  func oUTIDUSER_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKUSRINF(.w_UTIDUSER, .w_UTCODICE))
    endwith
    return bRes
  endfunc

  add object oXXPASSWD_1_14 as StdField with uid="CWSADSZUKK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_XXPASSWD", cQueryName = "XXPASSWD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Password utente infinity",;
    HelpContextID = 74638662,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=180, Top=127, InputMask=replicate('X',254)


  add object oObj_1_16 as cp_setobjprop with uid="LXUPGKSHBE",left=749, top=252, width=204,height=19,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_XXPASSWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 216172298

  add object oStr_1_5 as StdString with uid="VDFPFVJAYN",Visible=.t., Left=20, Top=127,;
    Alignment=1, Width=158, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VCAHIROQMH",Visible=.t., Left=126, Top=11,;
    Alignment=1, Width=52, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FLGKZZFDXH",Visible=.t., Left=112, Top=103,;
    Alignment=1, Width=66, Height=18,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="WWGTAFYPGB",Visible=.t., Left=45, Top=66,;
    Alignment=1, Width=133, Height=18,;
    Caption="Applicazione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="OSHEIFVCQE",Visible=.t., Left=45, Top=40,;
    Alignment=1, Width=133, Height=18,;
    Caption="Server Infinity URL:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="IPYBJEHIZG",left=11, top=93, width=603,height=62
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_klw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
