* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_mrv                                                        *
*              Regole validazione                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_93]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-29                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_mrv"))

* --- Class definition
define class tgscp_mrv as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 577
  Height = 355+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=113874281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ZREGVALI_IDX = 0
  VALUTE_IDX = 0
  cFile = "ZREGVALI"
  cKeySelect = "RECODICE"
  cKeyWhere  = "RECODICE=this.w_RECODICE"
  cKeyDetail  = "RECODICE=this.w_RECODICE"
  cKeyWhereODBC = '"RECODICE="+cp_ToStrODBC(this.w_RECODICE)';

  cKeyDetailWhereODBC = '"RECODICE="+cp_ToStrODBC(this.w_RECODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ZREGVALI.RECODICE="+cp_ToStrODBC(this.w_RECODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ZREGVALI.REORDINE'
  cPrg = "gscp_mrv"
  cComment = "Regole validazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RECODICE = 0
  w_REDESCRI = space(80)
  w_REVALIMP = space(5)
  w_REORDINE = 0
  w_REDESSIN = space(40)
  w_RELIVCONF = 0
  w_RE__STOP = space(1)
  w_REVALORE = space(0)
  w_RETABEL1 = space(2)
  w_RETABEL2 = space(2)
  w_RECAMPO1 = space(30)
  w_RECAMPO2 = space(30)
  w_REOPERAT = space(2)
  w_VADESVAL = space(35)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ZREGVALI','gscp_mrv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_mrvPag1","gscp_mrv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Regole validazione")
      .Pages(1).HelpContextID = 99070955
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRECODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ZREGVALI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZREGVALI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZREGVALI_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RECODICE = NVL(RECODICE,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ZREGVALI where RECODICE=KeySet.RECODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2],this.bLoadRecFilter,this.ZREGVALI_IDX,"gscp_mrv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZREGVALI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZREGVALI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ZREGVALI '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RECODICE',this.w_RECODICE  )
      select * from (i_cTable) ZREGVALI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VADESVAL = space(35)
        .w_RECODICE = NVL(RECODICE,0)
        .w_REDESCRI = NVL(REDESCRI,space(80))
        .w_REVALIMP = NVL(REVALIMP,space(5))
          if link_1_4_joined
            this.w_REVALIMP = NVL(VACODVAL104,NVL(this.w_REVALIMP,space(5)))
            this.w_VADESVAL = NVL(VADESVAL104,space(35))
          else
          .link_1_4('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ZREGVALI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_REORDINE = NVL(REORDINE,0)
          .w_REDESSIN = NVL(REDESSIN,space(40))
          .w_RELIVCONF = NVL(RELIVCONF,0)
          .w_RE__STOP = NVL(RE__STOP,space(1))
          .w_REVALORE = NVL(REVALORE,space(0))
          .w_RETABEL1 = NVL(RETABEL1,space(2))
          .w_RETABEL2 = NVL(RETABEL2,space(2))
          .w_RECAMPO1 = NVL(RECAMPO1,space(30))
          .w_RECAMPO2 = NVL(RECAMPO2,space(30))
          .w_REOPERAT = NVL(REOPERAT,space(2))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RECODICE=0
      .w_REDESCRI=space(80)
      .w_REVALIMP=space(5)
      .w_REORDINE=0
      .w_REDESSIN=space(40)
      .w_RELIVCONF=0
      .w_RE__STOP=space(1)
      .w_REVALORE=space(0)
      .w_RETABEL1=space(2)
      .w_RETABEL2=space(2)
      .w_RECAMPO1=space(30)
      .w_RECAMPO2=space(30)
      .w_REOPERAT=space(2)
      .w_VADESVAL=space(35)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_REVALIMP = g_PERVAL
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_REVALIMP))
         .link_1_4('Full')
        endif
        .w_REORDINE = CPROWNUM * 10
        .DoRTCalc(5,5,.f.)
        .w_RELIVCONF = 8
        .DoRTCalc(7,8,.f.)
        .w_RETABEL1 = 'OM'
        .w_RETABEL2 = 'EX'
        .DoRTCalc(11,12,.f.)
        .w_REOPERAT = '='
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZREGVALI')
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRECODICE_1_2.enabled = i_bVal
      .Page1.oPag.oREDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oREVALIMP_1_4.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRECODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRECODICE_1_2.enabled = .t.
        .Page1.oPag.oREDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ZREGVALI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RECODICE,"RECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REDESCRI,"REDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_REVALIMP,"REVALIMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
    i_lTable = "ZREGVALI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ZREGVALI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_REORDINE N(6);
      ,t_REDESSIN C(40);
      ,t_RELIVCONF N(3);
      ,t_RE__STOP N(3);
      ,CPROWNUM N(10);
      ,t_REVALORE M(10);
      ,t_RETABEL1 C(2);
      ,t_RETABEL2 C(2);
      ,t_RECAMPO1 C(30);
      ,t_RECAMPO2 C(30);
      ,t_REOPERAT C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscp_mrvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREORDINE_2_1.controlsource=this.cTrsName+'.t_REORDINE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oREDESSIN_2_2.controlsource=this.cTrsName+'.t_REDESSIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.controlsource=this.cTrsName+'.t_RELIVCONF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.controlsource=this.cTrsName+'.t_RE__STOP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(85)
    this.AddVLine(406)
    this.AddVLine(500)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREORDINE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
      *
      * insert into ZREGVALI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZREGVALI')
        i_extval=cp_InsertValODBCExtFlds(this,'ZREGVALI')
        i_cFldBody=" "+;
                  "(RECODICE,REDESCRI,REVALIMP,REORDINE,REDESSIN"+;
                  ",RELIVCONF,RE__STOP,REVALORE,RETABEL1,RETABEL2"+;
                  ",RECAMPO1,RECAMPO2,REOPERAT,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RECODICE)+","+cp_ToStrODBC(this.w_REDESCRI)+","+cp_ToStrODBCNull(this.w_REVALIMP)+","+cp_ToStrODBC(this.w_REORDINE)+","+cp_ToStrODBC(this.w_REDESSIN)+;
             ","+cp_ToStrODBC(this.w_RELIVCONF)+","+cp_ToStrODBC(this.w_RE__STOP)+","+cp_ToStrODBC(this.w_REVALORE)+","+cp_ToStrODBC(this.w_RETABEL1)+","+cp_ToStrODBC(this.w_RETABEL2)+;
             ","+cp_ToStrODBC(this.w_RECAMPO1)+","+cp_ToStrODBC(this.w_RECAMPO2)+","+cp_ToStrODBC(this.w_REOPERAT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZREGVALI')
        i_extval=cp_InsertValVFPExtFlds(this,'ZREGVALI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RECODICE',this.w_RECODICE)
        INSERT INTO (i_cTable) (;
                   RECODICE;
                  ,REDESCRI;
                  ,REVALIMP;
                  ,REORDINE;
                  ,REDESSIN;
                  ,RELIVCONF;
                  ,RE__STOP;
                  ,REVALORE;
                  ,RETABEL1;
                  ,RETABEL2;
                  ,RECAMPO1;
                  ,RECAMPO2;
                  ,REOPERAT;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RECODICE;
                  ,this.w_REDESCRI;
                  ,this.w_REVALIMP;
                  ,this.w_REORDINE;
                  ,this.w_REDESSIN;
                  ,this.w_RELIVCONF;
                  ,this.w_RE__STOP;
                  ,this.w_REVALORE;
                  ,this.w_RETABEL1;
                  ,this.w_RETABEL2;
                  ,this.w_RECAMPO1;
                  ,this.w_RECAMPO2;
                  ,this.w_REOPERAT;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_REORDINE) or Empty(t_REDESSIN))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ZREGVALI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " REDESCRI="+cp_ToStrODBC(this.w_REDESCRI)+;
                 ",REVALIMP="+cp_ToStrODBCNull(this.w_REVALIMP)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ZREGVALI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  REDESCRI=this.w_REDESCRI;
                 ,REVALIMP=this.w_REVALIMP;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_REORDINE) or Empty(t_REDESSIN))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ZREGVALI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ZREGVALI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " REDESCRI="+cp_ToStrODBC(this.w_REDESCRI)+;
                     ",REVALIMP="+cp_ToStrODBCNull(this.w_REVALIMP)+;
                     ",REORDINE="+cp_ToStrODBC(this.w_REORDINE)+;
                     ",REDESSIN="+cp_ToStrODBC(this.w_REDESSIN)+;
                     ",RELIVCONF="+cp_ToStrODBC(this.w_RELIVCONF)+;
                     ",RE__STOP="+cp_ToStrODBC(this.w_RE__STOP)+;
                     ",REVALORE="+cp_ToStrODBC(this.w_REVALORE)+;
                     ",RETABEL1="+cp_ToStrODBC(this.w_RETABEL1)+;
                     ",RETABEL2="+cp_ToStrODBC(this.w_RETABEL2)+;
                     ",RECAMPO1="+cp_ToStrODBC(this.w_RECAMPO1)+;
                     ",RECAMPO2="+cp_ToStrODBC(this.w_RECAMPO2)+;
                     ",REOPERAT="+cp_ToStrODBC(this.w_REOPERAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ZREGVALI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      REDESCRI=this.w_REDESCRI;
                     ,REVALIMP=this.w_REVALIMP;
                     ,REORDINE=this.w_REORDINE;
                     ,REDESSIN=this.w_REDESSIN;
                     ,RELIVCONF=this.w_RELIVCONF;
                     ,RE__STOP=this.w_RE__STOP;
                     ,REVALORE=this.w_REVALORE;
                     ,RETABEL1=this.w_RETABEL1;
                     ,RETABEL2=this.w_RETABEL2;
                     ,RECAMPO1=this.w_RECAMPO1;
                     ,RECAMPO2=this.w_RECAMPO2;
                     ,REOPERAT=this.w_REOPERAT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_REORDINE) or Empty(t_REDESSIN))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ZREGVALI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_REORDINE) or Empty(t_REDESSIN))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZREGVALI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZREGVALI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_REVALORE with this.w_REVALORE
      replace t_RETABEL1 with this.w_RETABEL1
      replace t_RETABEL2 with this.w_RETABEL2
      replace t_RECAMPO1 with this.w_RECAMPO1
      replace t_RECAMPO2 with this.w_RECAMPO2
      replace t_REOPERAT with this.w_REOPERAT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=REVALIMP
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REVALIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_REVALIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_REVALIMP))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REVALIMP)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_REVALIMP) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oREVALIMP_1_4'),i_cWhere,'',"Valuta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REVALIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_REVALIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_REVALIMP)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REVALIMP = NVL(_Link_.VACODVAL,space(5))
      this.w_VADESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_REVALIMP = space(5)
      endif
      this.w_VADESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REVALIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.VACODVAL as VACODVAL104"+ ",link_1_4.VADESVAL as VADESVAL104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on ZREGVALI.REVALIMP=link_1_4.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and ZREGVALI.REVALIMP=link_1_4.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRECODICE_1_2.value==this.w_RECODICE)
      this.oPgFrm.Page1.oPag.oRECODICE_1_2.value=this.w_RECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value==this.w_REDESCRI)
      this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value=this.w_REDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oREVALIMP_1_4.value==this.w_REVALIMP)
      this.oPgFrm.Page1.oPag.oREVALIMP_1_4.value=this.w_REVALIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREORDINE_2_1.value==this.w_REORDINE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREORDINE_2_1.value=this.w_REORDINE
      replace t_REORDINE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREORDINE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESSIN_2_2.value==this.w_REDESSIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESSIN_2_2.value=this.w_REDESSIN
      replace t_REDESSIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oREDESSIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.RadioValue()==this.w_RELIVCONF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.SetRadio()
      replace t_RELIVCONF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.RadioValue()==this.w_RE__STOP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.SetRadio()
      replace t_RE__STOP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'ZREGVALI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_REVALIMP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oREVALIMP_1_4.SetFocus()
            i_bnoObbl = !empty(.w_REVALIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_REORDINE) or Empty(t_REDESSIN)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_REORDINE) or Empty(.w_REDESSIN))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_REORDINE) or Empty(t_REDESSIN)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_REORDINE=0
      .w_REDESSIN=space(40)
      .w_RELIVCONF=0
      .w_RE__STOP=space(1)
      .w_REVALORE=space(0)
      .w_RETABEL1=space(2)
      .w_RETABEL2=space(2)
      .w_RECAMPO1=space(30)
      .w_RECAMPO2=space(30)
      .w_REOPERAT=space(2)
      .DoRTCalc(1,3,.f.)
        .w_REORDINE = CPROWNUM * 10
      .DoRTCalc(5,5,.f.)
        .w_RELIVCONF = 8
      .DoRTCalc(7,8,.f.)
        .w_RETABEL1 = 'OM'
        .w_RETABEL2 = 'EX'
      .DoRTCalc(11,12,.f.)
        .w_REOPERAT = '='
    endwith
    this.DoRTCalc(14,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_REORDINE = t_REORDINE
    this.w_REDESSIN = t_REDESSIN
    this.w_RELIVCONF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.RadioValue(.t.)
    this.w_RE__STOP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.RadioValue(.t.)
    this.w_REVALORE = t_REVALORE
    this.w_RETABEL1 = t_RETABEL1
    this.w_RETABEL2 = t_RETABEL2
    this.w_RECAMPO1 = t_RECAMPO1
    this.w_RECAMPO2 = t_RECAMPO2
    this.w_REOPERAT = t_REOPERAT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_REORDINE with this.w_REORDINE
    replace t_REDESSIN with this.w_REDESSIN
    replace t_RELIVCONF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRELIVCONF_2_4.ToRadio()
    replace t_RE__STOP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRE__STOP_2_5.ToRadio()
    replace t_REVALORE with this.w_REVALORE
    replace t_RETABEL1 with this.w_RETABEL1
    replace t_RETABEL2 with this.w_RETABEL2
    replace t_RECAMPO1 with this.w_RECAMPO1
    replace t_RECAMPO2 with this.w_RECAMPO2
    replace t_REOPERAT with this.w_REOPERAT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscp_mrvPag1 as StdContainer
  Width  = 573
  height = 355
  stdWidth  = 573
  stdheight = 355
  resizeXpos=307
  resizeYpos=294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRECODICE_1_2 as StdField with uid="NHDSVZMRPR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RECODICE", cQueryName = "RECODICE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 113894747,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=101, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oREDESCRI_1_3 as StdField with uid="LCJDTQFXMG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_REDESCRI", cQueryName = "REDESCRI",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 28308831,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=164, Top=11, InputMask=replicate('X',80)

  add object oREVALIMP_1_4 as StdField with uid="THSOPNQCHO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REVALIMP", cQueryName = "REVALIMP",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Specifica in quale valuta sono espressi gli importi monetari delle regole",;
    HelpContextID = 146991770,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=513, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_REVALIMP"

  func oREVALIMP_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oREVALIMP_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREVALIMP_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oREVALIMP_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valuta",'',this.parent.oContained
  endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=66, width=555,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="REORDINE",Label1="Ordine",Field2="REDESSIN",Label2="Descrizione regola",Field3="RELIVCONF",Label3="Livello",Field4="RE__STOP",Label4="Stop",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202216570

  add object oStr_1_1 as StdString with uid="LECMIBQMOQ",Visible=.t., Left=9, Top=13,;
    Alignment=1, Width=87, Height=18,;
    Caption="Gruppo regole:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ZIIIMPJYMZ",Visible=.t., Left=377, Top=41,;
    Alignment=1, Width=128, Height=18,;
    Caption="Importi espressi in:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=85,;
    width=551+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.5000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=86,width=550+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.5000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscp_mrvBodyRow as CPBodyRowCnt
  Width=541
  Height=int(fontmetric(1,"Arial",9,"")*1*1.5000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oREORDINE_2_1 as StdTrsField with uid="OCSEPTBZJH",rtseq=4,rtrep=.t.,;
    cFormVar="w_REORDINE",value=0,;
    ToolTipText = "Ordine di applicazione della regola",;
    HelpContextID = 114140507,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=70, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oREDESSIN_2_2 as StdTrsField with uid="USGZOYQUML",rtseq=5,rtrep=.t.,;
    cFormVar="w_REDESSIN",value=space(40),;
    ToolTipText = "Descrizione della regola",;
    HelpContextID = 240126620,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=72, Top=0, InputMask=replicate('X',40)

  add object oBtn_2_3 as StdButton with uid="PGYTOUNVSV",width=24,height=19,;
   left=367, top=1,;
    caption="...", nPag=2;
    , ToolTipText = "Permette di definire la regola";
    , HelpContextID = 113673258;
  , bGlobalFont=.t.

    proc oBtn_2_3.Click()
      do gscp_kre with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRELIVCONF_2_4 as StdTrsCombo with uid="EPTCFWKTKB",rtrep=.t.,;
    cFormVar="w_RELIVCONF", RowSource=""+"Verde,"+"Giallo,"+"Rosso,"+"Nero" , ;
    ToolTipText = "Livello di conformit� della regola",;
    HelpContextID = 31750596,;
    Height=22, Width=63, Left=418, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRELIVCONF_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RELIVCONF,&i_cF..t_RELIVCONF),this.value)
    return(iif(xVal =1,8,;
    iif(xVal =2,6,;
    iif(xVal =3,4,;
    iif(xVal =4,2,;
    0)))))
  endfunc
  func oRELIVCONF_2_4.GetRadio()
    this.Parent.oContained.w_RELIVCONF = this.RadioValue()
    return .t.
  endfunc

  func oRELIVCONF_2_4.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_RELIVCONF==8,1,;
      iif(this.Parent.oContained.w_RELIVCONF==6,2,;
      iif(this.Parent.oContained.w_RELIVCONF==4,3,;
      iif(this.Parent.oContained.w_RELIVCONF==2,4,;
      0)))))
  endfunc

  func oRELIVCONF_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oRE__STOP_2_5 as StdTrsCheck with uid="MMRKKFAWTW",rtrep=.t.,;
    cFormVar="w_RE__STOP",  caption="",;
    ToolTipText = "Se attivo: se la condizione della regola � verificata non vengono eseguite le altre regole",;
    HelpContextID = 46900582,;
    Left=489, Top=0, Width=47,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AUTOSIZE=.F.;
   , bGlobalFont=.t.


  func oRE__STOP_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RE__STOP,&i_cF..t_RE__STOP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oRE__STOP_2_5.GetRadio()
    this.Parent.oContained.w_RE__STOP = this.RadioValue()
    return .t.
  endfunc

  func oRE__STOP_2_5.ToRadio()
    this.Parent.oContained.w_RE__STOP=trim(this.Parent.oContained.w_RE__STOP)
    return(;
      iif(this.Parent.oContained.w_RE__STOP=='S',1,;
      0))
  endfunc

  func oRE__STOP_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oREORDINE_2_1.When()
    return(.t.)
  proc oREORDINE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oREORDINE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_mrv','ZREGVALI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RECODICE=ZREGVALI.RECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
