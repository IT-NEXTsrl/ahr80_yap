* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aca                                                        *
*              Giorni calendario                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-25                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_aca"))

* --- Class definition
define class tgsar_aca as StdForm
  Top    = 34
  Left   = 47

  * --- Standard Properties
  Width  = 525
  Height = 146+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-18"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  CAL_AZIE_IDX = 0
  TAB_CALE_IDX = 0
  cFile = "CAL_AZIE"
  cKeySelect = "CACODCAL,CAGIORNO"
  cKeyWhere  = "CACODCAL=this.w_CACODCAL and CAGIORNO=this.w_CAGIORNO"
  cKeyWhereODBC = '"CACODCAL="+cp_ToStrODBC(this.w_CACODCAL)';
      +'+" and CAGIORNO="+cp_ToStrODBC(this.w_CAGIORNO,"D")';

  cKeyWhereODBCqualified = '"CAL_AZIE.CACODCAL="+cp_ToStrODBC(this.w_CACODCAL)';
      +'+" and CAL_AZIE.CAGIORNO="+cp_ToStrODBC(this.w_CAGIORNO,"D")';

  cPrg = "gsar_aca"
  cComment = "Giorni calendario"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CACODCAL = space(5)
  o_CACODCAL = space(5)
  w_DESCAL = space(40)
  w_CAGIORNO = ctod('  /  /  ')
  w_NUMORE = 0
  w_CADESGIO = space(40)
  w_CANUMORE = 0
  o_CANUMORE = 0
  w_CAGIOLAV = space(1)
  o_CAGIOLAV = space(1)
  w_CAFLCHIU = space(1)
  o_CAFLCHIU = space(1)
  w_CAPROFIL = space(2)
  w_CAORAIN1 = space(5)
  o_CAORAIN1 = space(5)
  w_CAORAFI1 = space(5)
  o_CAORAFI1 = space(5)
  w_CAFLSOSP = space(1)
  w_CAORAIN2 = space(5)
  o_CAORAIN2 = space(5)
  w_CAORAFI2 = space(5)
  o_CAORAFI2 = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAL_AZIE','gsar_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_acaPag1","gsar_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calendario")
      .Pages(1).HelpContextID = 188537816
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODCAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TAB_CALE'
    this.cWorkTables[2]='CAL_AZIE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAL_AZIE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAL_AZIE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CACODCAL = NVL(CACODCAL,space(5))
      .w_CAGIORNO = NVL(CAGIORNO,ctod("  /  /  "))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsar_aca
    * INIZIO Evita errore su db2 per query con query CAGIORNO=NULL
    if this.w_CAGIORNO>=i_inidat AND this.w_CAGIORNO<=i_findat
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAL_AZIE where CACODCAL=KeySet.CACODCAL
    *                            and CAGIORNO=KeySet.CAGIORNO
    *
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAL_AZIE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAL_AZIE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAL_AZIE '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODCAL',this.w_CACODCAL  ,'CAGIORNO',this.w_CAGIORNO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCAL = space(40)
        .w_NUMORE = 0
        .w_CACODCAL = NVL(CACODCAL,space(5))
          if link_1_2_joined
            this.w_CACODCAL = NVL(TCCODICE102,NVL(this.w_CACODCAL,space(5)))
            this.w_DESCAL = NVL(TCDESCRI102,space(40))
            this.w_NUMORE = NVL(TCNUMORE102,0)
          else
          .link_1_2('Load')
          endif
        .w_CAGIORNO = NVL(cp_ToDate(CAGIORNO),ctod("  /  /  "))
        .w_CADESGIO = NVL(CADESGIO,space(40))
        .w_CANUMORE = NVL(CANUMORE,0)
        .w_CAGIOLAV = NVL(CAGIOLAV,space(1))
        .w_CAFLCHIU = NVL(CAFLCHIU,space(1))
        .w_CAPROFIL = NVL(CAPROFIL,space(2))
        .w_CAORAIN1 = NVL(CAORAIN1,space(5))
        .w_CAORAFI1 = NVL(CAORAFI1,space(5))
        .w_CAFLSOSP = NVL(CAFLSOSP,space(1))
        .w_CAORAIN2 = NVL(CAORAIN2,space(5))
        .w_CAORAFI2 = NVL(CAORAFI2,space(5))
        cp_LoadRecExtFlds(this,'CAL_AZIE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_aca
    Else
        this.BlankRec()
        if this.cFunction <> 'Query'
          ah_ErrorMsg("Inserire una data compresa tra %1 e %2",,,ALLTRIM(DTOC(i_inidat)),ALLTRIM(DTOC(i_findat)))
        endif
    endif
    *FINE Evita errore su db2 per query con query CAGIORNO=NULL
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODCAL = space(5)
      .w_DESCAL = space(40)
      .w_CAGIORNO = ctod("  /  /  ")
      .w_NUMORE = 0
      .w_CADESGIO = space(40)
      .w_CANUMORE = 0
      .w_CAGIOLAV = space(1)
      .w_CAFLCHIU = space(1)
      .w_CAPROFIL = space(2)
      .w_CAORAIN1 = space(5)
      .w_CAORAFI1 = space(5)
      .w_CAFLSOSP = space(1)
      .w_CAORAIN2 = space(5)
      .w_CAORAFI2 = space(5)
      if .cFunction<>"Filter"
        .w_CACODCAL = IIF(isAHR(), 'ZZCAP', '')
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CACODCAL))
          .link_1_2('Full')
          endif
          .DoRTCalc(2,5,.f.)
        .w_CANUMORE = IIF(.w_NUMORE<>0,.w_NUMORE,(IIF(.w_CAGIOLAV='S', IIF(!EMPTY(.w_CAORAIN1) AND !EMPTY(.w_CAORAFI1), cp_ROUND((cp_ROUND(CTOT(.w_CAORAFI1)-CTOT(.w_CAORAIN1),0))/3600,2), .w_CANUMORE) + IIF(!EMPTY(.w_CAORAIN2) AND !EMPTY(.w_CAORAFI2), cp_ROUND(cp_ROUND((CTOT(.w_CAORAFI2)-CTOT(.w_CAORAIN2)),0)/3600,2), 0), 0)))
        .w_CAGIOLAV = iif(.w_CAFLCHIU='S'or .w_CANUMORE=0,'N','S')
        .w_CAFLCHIU = 'N'
          .DoRTCalc(9,9,.f.)
        .w_CAORAIN1 = IIF(.w_CAGIOLAV='S', formattime(.w_CAORAIN1, ":"), "")
        .w_CAORAFI1 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAIN1), "", formattime(.w_CAORAFI1, ":")), "")
          .DoRTCalc(12,12,.f.)
        .w_CAORAIN2 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAFI1), "", formattime(.w_CAORAIN2, ":") ), "")
        .w_CAORAFI2 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAIN2), "", formattime(.w_CAORAFI2, ":") ), "")
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAL_AZIE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODCAL_1_2.enabled = i_bVal
      .Page1.oPag.oCAGIORNO_1_5.enabled = i_bVal
      .Page1.oPag.oCADESGIO_1_7.enabled = i_bVal
      .Page1.oPag.oCANUMORE_1_9.enabled = i_bVal
      .Page1.oPag.oCAGIOLAV_1_10.enabled = i_bVal
      .Page1.oPag.oCAFLCHIU_1_11.enabled = i_bVal
      .Page1.oPag.oCAORAIN1_1_14.enabled = i_bVal
      .Page1.oPag.oCAORAFI1_1_16.enabled = i_bVal
      .Page1.oPag.oCAFLSOSP_1_17.enabled = i_bVal
      .Page1.oPag.oCAORAIN2_1_19.enabled = i_bVal
      .Page1.oPag.oCAORAFI2_1_21.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODCAL_1_2.enabled = .f.
        .Page1.oPag.oCAGIORNO_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODCAL_1_2.enabled = .t.
        .Page1.oPag.oCAGIORNO_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAL_AZIE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODCAL,"CACODCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGIORNO,"CAGIORNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESGIO,"CADESGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CANUMORE,"CANUMORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGIOLAV,"CAGIOLAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCHIU,"CAFLCHIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPROFIL,"CAPROFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAORAIN1,"CAORAIN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAORAFI1,"CAORAFI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLSOSP,"CAFLSOSP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAORAIN2,"CAORAIN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAORAFI2,"CAORAFI2",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    i_lTable = "CAL_AZIE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAL_AZIE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SCG with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAL_AZIE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAL_AZIE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAL_AZIE')
        i_extval=cp_InsertValODBCExtFlds(this,'CAL_AZIE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CACODCAL,CAGIORNO,CADESGIO,CANUMORE,CAGIOLAV"+;
                  ",CAFLCHIU,CAPROFIL,CAORAIN1,CAORAFI1,CAFLSOSP"+;
                  ",CAORAIN2,CAORAFI2 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_CACODCAL)+;
                  ","+cp_ToStrODBC(this.w_CAGIORNO)+;
                  ","+cp_ToStrODBC(this.w_CADESGIO)+;
                  ","+cp_ToStrODBC(this.w_CANUMORE)+;
                  ","+cp_ToStrODBC(this.w_CAGIOLAV)+;
                  ","+cp_ToStrODBC(this.w_CAFLCHIU)+;
                  ","+cp_ToStrODBC(this.w_CAPROFIL)+;
                  ","+cp_ToStrODBC(this.w_CAORAIN1)+;
                  ","+cp_ToStrODBC(this.w_CAORAFI1)+;
                  ","+cp_ToStrODBC(this.w_CAFLSOSP)+;
                  ","+cp_ToStrODBC(this.w_CAORAIN2)+;
                  ","+cp_ToStrODBC(this.w_CAORAFI2)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAL_AZIE')
        i_extval=cp_InsertValVFPExtFlds(this,'CAL_AZIE')
        cp_CheckDeletedKey(i_cTable,0,'CACODCAL',this.w_CACODCAL,'CAGIORNO',this.w_CAGIORNO)
        INSERT INTO (i_cTable);
              (CACODCAL,CAGIORNO,CADESGIO,CANUMORE,CAGIOLAV,CAFLCHIU,CAPROFIL,CAORAIN1,CAORAFI1,CAFLSOSP,CAORAIN2,CAORAFI2  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CACODCAL;
                  ,this.w_CAGIORNO;
                  ,this.w_CADESGIO;
                  ,this.w_CANUMORE;
                  ,this.w_CAGIOLAV;
                  ,this.w_CAFLCHIU;
                  ,this.w_CAPROFIL;
                  ,this.w_CAORAIN1;
                  ,this.w_CAORAFI1;
                  ,this.w_CAFLSOSP;
                  ,this.w_CAORAIN2;
                  ,this.w_CAORAFI2;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAL_AZIE_IDX,i_nConn)
      *
      * update CAL_AZIE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAL_AZIE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CADESGIO="+cp_ToStrODBC(this.w_CADESGIO)+;
             ",CANUMORE="+cp_ToStrODBC(this.w_CANUMORE)+;
             ",CAGIOLAV="+cp_ToStrODBC(this.w_CAGIOLAV)+;
             ",CAFLCHIU="+cp_ToStrODBC(this.w_CAFLCHIU)+;
             ",CAPROFIL="+cp_ToStrODBC(this.w_CAPROFIL)+;
             ",CAORAIN1="+cp_ToStrODBC(this.w_CAORAIN1)+;
             ",CAORAFI1="+cp_ToStrODBC(this.w_CAORAFI1)+;
             ",CAFLSOSP="+cp_ToStrODBC(this.w_CAFLSOSP)+;
             ",CAORAIN2="+cp_ToStrODBC(this.w_CAORAIN2)+;
             ",CAORAFI2="+cp_ToStrODBC(this.w_CAORAFI2)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAL_AZIE')
        i_cWhere = cp_PKFox(i_cTable  ,'CACODCAL',this.w_CACODCAL  ,'CAGIORNO',this.w_CAGIORNO  )
        UPDATE (i_cTable) SET;
              CADESGIO=this.w_CADESGIO;
             ,CANUMORE=this.w_CANUMORE;
             ,CAGIOLAV=this.w_CAGIOLAV;
             ,CAFLCHIU=this.w_CAFLCHIU;
             ,CAPROFIL=this.w_CAPROFIL;
             ,CAORAIN1=this.w_CAORAIN1;
             ,CAORAFI1=this.w_CAORAFI1;
             ,CAFLSOSP=this.w_CAFLSOSP;
             ,CAORAIN2=this.w_CAORAIN2;
             ,CAORAFI2=this.w_CAORAFI2;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAL_AZIE_IDX,i_nConn)
      *
      * delete CAL_AZIE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CACODCAL',this.w_CACODCAL  ,'CAGIORNO',this.w_CAGIORNO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_CAORAFI1<>.w_CAORAFI1.or. .o_CAORAFI2<>.w_CAORAFI2.or. .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAORAIN2<>.w_CAORAIN2.or. .o_CAGIOLAV<>.w_CAGIOLAV.or. .o_CACODCAL<>.w_CACODCAL
            .w_CANUMORE = IIF(.w_NUMORE<>0,.w_NUMORE,(IIF(.w_CAGIOLAV='S', IIF(!EMPTY(.w_CAORAIN1) AND !EMPTY(.w_CAORAFI1), cp_ROUND((cp_ROUND(CTOT(.w_CAORAFI1)-CTOT(.w_CAORAIN1),0))/3600,2), .w_CANUMORE) + IIF(!EMPTY(.w_CAORAIN2) AND !EMPTY(.w_CAORAFI2), cp_ROUND(cp_ROUND((CTOT(.w_CAORAFI2)-CTOT(.w_CAORAIN2)),0)/3600,2), 0), 0)))
        endif
        if .o_CAFLCHIU<>.w_CAFLCHIU.or. .o_CANUMORE<>.w_CANUMORE
            .w_CAGIOLAV = iif(.w_CAFLCHIU='S'or .w_CANUMORE=0,'N','S')
        endif
        if .o_CAGIOLAV<>.w_CAGIOLAV
            .w_CAFLCHIU = 'N'
        endif
        .DoRTCalc(9,9,.t.)
        if .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAGIOLAV<>.w_CAGIOLAV
            .w_CAORAIN1 = IIF(.w_CAGIOLAV='S', formattime(.w_CAORAIN1, ":"), "")
        endif
        if .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAORAFI1<>.w_CAORAFI1.or. .o_CAGIOLAV<>.w_CAGIOLAV
            .w_CAORAFI1 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAIN1), "", formattime(.w_CAORAFI1, ":")), "")
        endif
        .DoRTCalc(12,12,.t.)
        if .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAORAFI1<>.w_CAORAFI1.or. .o_CAORAIN2<>.w_CAORAIN2.or. .o_CAGIOLAV<>.w_CAGIOLAV
            .w_CAORAIN2 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAFI1), "", formattime(.w_CAORAIN2, ":") ), "")
        endif
        if .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAORAFI1<>.w_CAORAFI1.or. .o_CAORAIN2<>.w_CAORAIN2.or. .o_CAORAFI2<>.w_CAORAFI2.or. .o_CAGIOLAV<>.w_CAGIOLAV
            .w_CAORAFI2 = IIF(.w_CAGIOLAV='S', IIF(EMPTY(.w_CAORAIN2), "", formattime(.w_CAORAFI2, ":") ), "")
        endif
        if .o_CAORAFI1<>.w_CAORAFI1.or. .o_CAORAFI2<>.w_CAORAFI2.or. .o_CAORAIN1<>.w_CAORAIN1.or. .o_CAORAIN2<>.w_CAORAIN2.or. .o_CAGIOLAV<>.w_CAGIOLAV
          .Calculate_OHNZBAYEST()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_OHNZBAYEST()
    with this
          * --- Calcola ore lavorative
          .w_CANUMORE = IIF(.w_CANUMORE>=(23.98),24,.w_CANUMORE)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAGIOLAV_1_10.enabled = this.oPgFrm.Page1.oPag.oCAGIOLAV_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCAFLCHIU_1_11.enabled = this.oPgFrm.Page1.oPag.oCAFLCHIU_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCAORAIN1_1_14.enabled = this.oPgFrm.Page1.oPag.oCAORAIN1_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCAORAFI1_1_16.enabled = this.oPgFrm.Page1.oPag.oCAORAFI1_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCAORAIN2_1_19.enabled = this.oPgFrm.Page1.oPag.oCAORAIN2_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCAORAFI2_1_21.enabled = this.oPgFrm.Page1.oPag.oCAORAFI2_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAFLSOSP_1_17.visible=!this.oPgFrm.Page1.oPag.oCAFLSOSP_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODCAL
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CACODCAL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCNUMORE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CACODCAL))
          select TCCODICE,TCDESCRI,TCNUMORE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCAL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODCAL) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oCACODCAL_1_2'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCNUMORE";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI,TCNUMORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCNUMORE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CACODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CACODCAL)
            select TCCODICE,TCDESCRI,TCNUMORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCAL = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
      this.w_NUMORE = NVL(_Link_.TCNUMORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CACODCAL = space(5)
      endif
      this.w_DESCAL = space(40)
      this.w_NUMORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.TCCODICE as TCCODICE102"+ ",link_1_2.TCDESCRI as TCDESCRI102"+ ",link_1_2.TCNUMORE as TCNUMORE102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on CAL_AZIE.CACODCAL=link_1_2.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and CAL_AZIE.CACODCAL=link_1_2.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODCAL_1_2.value==this.w_CACODCAL)
      this.oPgFrm.Page1.oPag.oCACODCAL_1_2.value=this.w_CACODCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_3.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_3.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAGIORNO_1_5.value==this.w_CAGIORNO)
      this.oPgFrm.Page1.oPag.oCAGIORNO_1_5.value=this.w_CAGIORNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESGIO_1_7.value==this.w_CADESGIO)
      this.oPgFrm.Page1.oPag.oCADESGIO_1_7.value=this.w_CADESGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCANUMORE_1_9.value==this.w_CANUMORE)
      this.oPgFrm.Page1.oPag.oCANUMORE_1_9.value=this.w_CANUMORE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAGIOLAV_1_10.RadioValue()==this.w_CAGIOLAV)
      this.oPgFrm.Page1.oPag.oCAGIOLAV_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCHIU_1_11.RadioValue()==this.w_CAFLCHIU)
      this.oPgFrm.Page1.oPag.oCAFLCHIU_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAORAIN1_1_14.value==this.w_CAORAIN1)
      this.oPgFrm.Page1.oPag.oCAORAIN1_1_14.value=this.w_CAORAIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oCAORAFI1_1_16.value==this.w_CAORAFI1)
      this.oPgFrm.Page1.oPag.oCAORAFI1_1_16.value=this.w_CAORAFI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLSOSP_1_17.RadioValue()==this.w_CAFLSOSP)
      this.oPgFrm.Page1.oPag.oCAFLSOSP_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAORAIN2_1_19.value==this.w_CAORAIN2)
      this.oPgFrm.Page1.oPag.oCAORAIN2_1_19.value=this.w_CAORAIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAORAFI2_1_21.value==this.w_CAORAFI2)
      this.oPgFrm.Page1.oPag.oCAORAFI2_1_21.value=this.w_CAORAFI2
    endif
    cp_SetControlsValueExtFlds(this,'CAL_AZIE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CAGIORNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAGIORNO_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CAGIORNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CANUMORE>=0 AND .w_CANUMORE<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCANUMORE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAORAIN1)) or not(chktimewnd(1, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2)))  and (.w_CAGIOLAV='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAORAIN1_1_14.SetFocus()
            i_bnoObbl = !empty(.w_CAORAIN1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido oppure orario iniziale maggiore dell'orario finale")
          case   ((empty(.w_CAORAFI1)) or not(chktimewnd(2, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2)))  and (!EMPTY(.w_CAORAIN1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAORAFI1_1_16.SetFocus()
            i_bnoObbl = !empty(.w_CAORAFI1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido o orario finale minore dell'orario iniziale")
          case   not(chktimewnd(3, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))  and (!EMPTY(.w_CAORAIN1) AND !EMPTY(.w_CAORAFI1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAORAIN2_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))  and (!EMPTY(.w_CAORAIN2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAORAFI2_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido oppure orario iniziale maggiore dell'orario finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODCAL = this.w_CACODCAL
    this.o_CANUMORE = this.w_CANUMORE
    this.o_CAGIOLAV = this.w_CAGIOLAV
    this.o_CAFLCHIU = this.w_CAFLCHIU
    this.o_CAORAIN1 = this.w_CAORAIN1
    this.o_CAORAFI1 = this.w_CAORAFI1
    this.o_CAORAIN2 = this.w_CAORAIN2
    this.o_CAORAFI2 = this.w_CAORAFI2
    return

enddefine

* --- Define pages as container
define class tgsar_acaPag1 as StdContainer
  Width  = 525
  height = 146
  stdWidth  = 525
  stdheight = 146
  resizeXpos=359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODCAL_1_2 as StdField with uid="DOIQDDSLTK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODCAL", cQueryName = "CACODCAL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice calendario",;
    HelpContextID = 251003278,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=109, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_CACODCAL"

  func oCACODCAL_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCAL_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODCAL_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oCACODCAL_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oCACODCAL_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CACODCAL
     i_obj.ecpSave()
  endproc

  add object oDESCAL_1_3 as StdField with uid="RCNMQUXYNN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 103873994,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=197, Top=12, InputMask=replicate('X',40)

  add object oCAGIORNO_1_5 as StdField with uid="OKWUJTIOUN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CAGIORNO", cQueryName = "CACODCAL,CAGIORNO",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data",;
    HelpContextID = 256622987,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=109, Top=37

  add object oCADESGIO_1_7 as StdField with uid="YDGOEGUIJQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CADESGIO", cQueryName = "CADESGIO",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione giorno",;
    HelpContextID = 99618421,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=197, Top=37, InputMask=replicate('X',40)

  add object oCANUMORE_1_9 as StdField with uid="UIMQZHIYFH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CANUMORE", cQueryName = "CANUMORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative ( 0 =giorno festivo)",;
    HelpContextID = 39801237,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=67, cSayPict='"99.99"', cGetPict='"99.99"'

  func oCANUMORE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CANUMORE>=0 AND .w_CANUMORE<=24)
    endwith
    return bRes
  endfunc

  add object oCAGIOLAV_1_10 as StdCheck with uid="HAJPWCREDY",rtseq=7,rtrep=.f.,left=196, top=65, caption="Giorno lavorativo",;
    HelpContextID = 88850820,;
    cFormVar="w_CAGIOLAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAGIOLAV_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAGIOLAV_1_10.GetRadio()
    this.Parent.oContained.w_CAGIOLAV = this.RadioValue()
    return .t.
  endfunc

  func oCAGIOLAV_1_10.SetRadio()
    this.Parent.oContained.w_CAGIOLAV=trim(this.Parent.oContained.w_CAGIOLAV)
    this.value = ;
      iif(this.Parent.oContained.w_CAGIOLAV=='S',1,;
      0)
  endfunc

  func oCAGIOLAV_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CANUMORE<>0 OR .w_CAFLCHIU='N')
    endwith
   endif
  endfunc

  add object oCAFLCHIU_1_11 as StdCheck with uid="NRUUWZXCOI",rtseq=8,rtrep=.f.,left=324, top=65, caption="Giorno chiusura",;
    HelpContextID = 100085371,;
    cFormVar="w_CAFLCHIU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLCHIU_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLCHIU_1_11.GetRadio()
    this.Parent.oContained.w_CAFLCHIU = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCHIU_1_11.SetRadio()
    this.Parent.oContained.w_CAFLCHIU=trim(this.Parent.oContained.w_CAFLCHIU)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLCHIU=='S',1,;
      0)
  endfunc

  func oCAFLCHIU_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAGIOLAV='N')
    endwith
   endif
  endfunc

  add object oCAORAIN1_1_14 as StdField with uid="FWWOFLPMME",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAORAIN1", cQueryName = "CAORAIN1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido oppure orario iniziale maggiore dell'orario finale",;
    HelpContextID = 153239977,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=94, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oCAORAIN1_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAGIOLAV='S')
    endwith
   endif
  endfunc

  func oCAORAIN1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))
    endwith
    return bRes
  endfunc

  add object oCAORAFI1_1_16 as StdField with uid="WKSHOXRXJW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CAORAFI1", cQueryName = "CAORAFI1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido o orario finale minore dell'orario iniziale",;
    HelpContextID = 64863831,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=265, Top=94, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oCAORAFI1_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CAORAIN1))
    endwith
   endif
  endfunc

  func oCAORAFI1_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))
    endwith
    return bRes
  endfunc

  add object oCAFLSOSP_1_17 as StdCheck with uid="QREVYFWPSB",rtseq=12,rtrep=.f.,left=324, top=94, caption="Sospensione attivit� forense",;
    ToolTipText = "Sospensione attivit� forense",;
    HelpContextID = 34132362,;
    cFormVar="w_CAFLSOSP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLSOSP_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLSOSP_1_17.GetRadio()
    this.Parent.oContained.w_CAFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oCAFLSOSP_1_17.SetRadio()
    this.Parent.oContained.w_CAFLSOSP=trim(this.Parent.oContained.w_CAFLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLSOSP=='S',1,;
      0)
  endfunc

  func oCAFLSOSP_1_17.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oCAORAIN2_1_19 as StdField with uid="DTSENTVYWN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAORAIN2", cQueryName = "CAORAIN2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    HelpContextID = 153239976,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=121, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oCAORAIN2_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CAORAIN1) AND !EMPTY(.w_CAORAFI1))
    endwith
   endif
  endfunc

  func oCAORAIN2_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))
    endwith
    return bRes
  endfunc

  add object oCAORAFI2_1_21 as StdField with uid="FBMCAHRADS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CAORAFI2", cQueryName = "CAORAFI2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido oppure orario iniziale maggiore dell'orario finale",;
    HelpContextID = 64863832,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=265, Top=121, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oCAORAFI2_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CAORAIN2))
    endwith
   endif
  endfunc

  func oCAORAFI2_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_CAORAIN1, .w_CAORAFI1, .w_CAORAIN2, .w_CAORAFI2))
    endwith
    return bRes
  endfunc

  add object oStr_1_1 as StdString with uid="XCWAIFILUD",Visible=.t., Left=35, Top=12,;
    Alignment=1, Width=71, Height=15,;
    Caption="Calendario:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="AGCGCVQNXZ",Visible=.t., Left=53, Top=37,;
    Alignment=1, Width=53, Height=15,;
    Caption="Giorno:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="YLVLTIIUER",Visible=.t., Left=12, Top=67,;
    Alignment=1, Width=94, Height=15,;
    Caption="Ore lavorative:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="QGPYOVIPBO",Visible=.t., Left=12, Top=95,;
    Alignment=1, Width=94, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="IWMZJNYINV",Visible=.t., Left=163, Top=95,;
    Alignment=1, Width=94, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SGKUZQCGJE",Visible=.t., Left=12, Top=124,;
    Alignment=1, Width=94, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YQCDKTVJME",Visible=.t., Left=163, Top=124,;
    Alignment=1, Width=94, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aca','CAL_AZIE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODCAL=CAL_AZIE.CACODCAL";
  +" and "+i_cAliasName2+".CAGIORNO=CAL_AZIE.CAGIORNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
