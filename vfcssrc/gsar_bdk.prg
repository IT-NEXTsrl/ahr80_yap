* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdk                                                        *
*              Elimina documenti                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-11                                                      *
* Last revis.: 2011-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERDOC,w_ONLYCHK,w_NORIC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdk",oParentObject,m.w_SERDOC,m.w_ONLYCHK,m.w_NORIC)
return(i_retval)

define class tgsar_bdk as StdBatch
  * --- Local variables
  w_SERDOC = space(10)
  w_ONLYCHK = .f.
  w_NORIC = .f.
  w_RETVAL = space(254)
  w_COMMIT = .f.
  w_MVFLOFFE = space(1)
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVPRP = space(2)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVANNPRO = space(4)
  w_MVCLADOC = space(2)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  CON_PAGA_idx=0
  MOVIMATR_idx=0
  ALT_DETT_idx=0
  VDATRITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per eliminazione massiva di un documento
    *     con esecuzioni dei controlli preventivi
    * --- Se passato a .t. esegue simulazione di cancellazione documento senza cancellarlo
    * --- Se attivo non ricalcola i progressi
     
 Dimension L_ARRKEY(1,2) 
 L_ARRKEY[1,1]="MVSERIAL" 
 L_ARRKEY[1,2]=this.w_SERDOC 
 bTrsOk=.t.
    * --- Eseguo controlli in cancellazione
    this.w_RETVAL = GSAR_BAD(This,"D",@l_arrkey,.NULL.,@bTrsOk)
    if bTrsOk
      * --- storno i saldi
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        this.w_COMMIT = .t.
        * --- begin transaction
        cp_BeginTrs()
      endif
      this.w_RETVAL = flr_docum(@l_arrkey, "PRE", "D", @bTrsOk)
      if not bTrsOk
        bTrsErr=.t.
      else
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVFLOFFE,MVFLVEAC,MVANNDOC,MVPRD,MVNUMDOC,MVALFDOC,MVPRP,MVNUMEST,MVALFEST,MVANNPRO,MVCLADOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVFLOFFE,MVFLVEAC,MVANNDOC,MVPRD,MVNUMDOC,MVALFDOC,MVPRP,MVNUMEST,MVALFEST,MVANNPRO,MVCLADOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVFLOFFE = NVL(cp_ToDate(_read_.MVFLOFFE),cp_NullValue(_read_.MVFLOFFE))
          w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
          this.w_MVANNDOC = NVL(cp_ToDate(_read_.MVANNDOC),cp_NullValue(_read_.MVANNDOC))
          this.w_MVPRD = NVL(cp_ToDate(_read_.MVPRD),cp_NullValue(_read_.MVPRD))
          this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_MVPRP = NVL(cp_ToDate(_read_.MVPRP),cp_NullValue(_read_.MVPRP))
          this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
          this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
          this.w_MVANNPRO = NVL(cp_ToDate(_read_.MVANNPRO),cp_NullValue(_read_.MVANNPRO))
          this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Elimina Documento
        if ! this.w_ONLYCHK
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MVSERIAL,CPROWNUM"
            do vq_exec with 'GSAR_BDK',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.MVQTAIMP";
                +",MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.MVQTAIM1";
                +",MVIMPEVA = DOC_DETT.MVIMPEVA-_t2.MVIMPIMP";
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf;
                +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                +"DOC_DETT.MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.MVQTAIMP";
                +",DOC_DETT.MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.MVQTAIM1";
                +",DOC_DETT.MVIMPEVA = DOC_DETT.MVIMPEVA-_t2.MVIMPIMP";
            +",DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                +"MVQTAEVA,";
                +"MVQTAEV1,";
                +"MVIMPEVA,";
                +"MVFLEVAS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"MVQTAEVA-t2.MVQTAIMP,";
                +"MVQTAEV1-t2.MVQTAIM1,";
                +"MVIMPEVA-t2.MVIMPIMP,";
                +cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                +"MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.MVQTAIMP";
                +",MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.MVQTAIM1";
                +",MVIMPEVA = DOC_DETT.MVIMPEVA-_t2.MVIMPIMP";
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVQTAEVA = (select "+i_cTable+".MVQTAEVA-MVQTAIMP from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVQTAEV1 = (select "+i_cTable+".MVQTAEV1-MVQTAIM1 from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVIMPEVA = (select "+i_cTable+".MVIMPEVA-MVIMPIMP from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          gsar_bed(this,this.w_SERDOC,-20)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Cerco di portare indietro i progressivi
          if !this.w_NORIC and Not this.w_MVFLOFFE $ "DFI"
            GSAR_BRP(this,w_MVFLVEAC, this.w_MVANNDOC, this.w_MVPRD, this.w_MVNUMDOC, this.w_MVALFDOC, this.w_MVPRP, this.w_MVNUMEST, this.w_MVALFEST, this.w_MVANNPRO, this.w_MVCLADOC)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.w_RETVAL = " "
      endif
      if btrserr
        if this.w_COMMIT
          this.w_RETVAL = message()
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- accept error
          bTrsErr=.f.
        endif
      else
        if this.w_COMMIT
          * --- commit
          cp_EndTrs(.t.)
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  proc Init(oParentObject,w_SERDOC,w_ONLYCHK,w_NORIC)
    this.w_SERDOC=w_SERDOC
    this.w_ONLYCHK=w_ONLYCHK
    this.w_NORIC=w_NORIC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='CON_PAGA'
    this.cWorkTables[5]='MOVIMATR'
    this.cWorkTables[6]='ALT_DETT'
    this.cWorkTables[7]='VDATRITE'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERDOC,w_ONLYCHK,w_NORIC"
endproc
