* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdt                                                        *
*              Zoom generazione dichiarazione di intento                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-04                                                      *
* Last revis.: 2017-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdt",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bdt as StdBatch
  * --- Local variables
  pParame = space(1)
  w_ZOOM = .NULL.
  w_ZOOM1 = .NULL.
  w_ZOOM2 = .NULL.
  w_NFLTATTR = 0
  w_STAMPAQUADROA = space(1)
  w_RESFDF = 0
  w_QUADRO = 0
  w_MODELLO = space(20)
  w_MESS = space(50)
  w_ANTIPCON = space(1)
  w_DIPROTEC = space(17)
  w_DIPRODOC = 0
  w_CONFERMA = .f.
  w_SERIALE = space(10)
  w_NomeCursore = space(10)
  w_DICINTE_Temp = space(10)
  w_TipoGest = space(3)
  w_FILEORIGINE = space(0)
  w_DATCOM = ctod("  /  /  ")
  * --- WorkFile variables
  DIC_INTE_idx=0
  GEDICDEM_idx=0
  GEDICEME_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_STAMPAQUADROA = "N"
    this.w_DATCOM = Date(2017,03,01)
    do case
      case this.pParame="Z"
        this.w_ZOOM = This.oParentObject.w_CalcZoom
        this.w_ZOOM1 = This.oParentObject.w_IntZoom
        this.w_ZOOM2 = This.oParentObject.w_RettZoom
        this.w_ZOOM.VISIBLE = .F.
        this.w_ZOOM1.VISIBLE = .F.
        this.w_ZOOM2.VISIBLE = .F.
        if This.oParentObject.cFunction="Query"
          * --- Nel caso di interroga lo zoom deve essere vuoto nel caso non abbia generato
          *     altrimenti deve contenere le dichiarazioni di intento generate
          this.w_ZOOM1.VISIBLE = .T.
        else
          if This.oParentObject.cFunction="Load"
            if this.oParentObject.w_DGFLGCOR="1"
              this.w_ZOOM2.VISIBLE = .T.
            else
              this.w_ZOOM.VISIBLE = .T.
            endif
          else
            if this.oParentObject.w_DGGENERA="S"
              this.w_ZOOM1.VISIBLE = .T.
            else
              if this.oParentObject.w_DGFLGCOR="1"
                this.w_ZOOM2.VISIBLE = .T.
              else
                this.w_ZOOM.VISIBLE = .T.
              endif
            endif
          endif
        endif
        if Reccount(this.w_Zoom.cCursor)=0 Or This.oParentObject.cFunction="Query" Or this.oParentObject.w_Dggenera="S" 
          This.oParentObject.NotifyEvent("Interroga")
        endif
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        this.w_ZOOM = iif(this.oParentObject.w_Dgflgcor="1",This.oParentObject.w_RettZoom,This.oParentObject.w_CalcZoom)
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="S",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="M"
        Opengest("A","Gscg_Adi","Diserial",this.oParentObject.w_DISERIAL)
      case this.pParame="F"
        if Ah_YesNo("Si vuole stampare anche il quadro A?")
          this.w_STAMPAQUADROA = "S"
        endif
        this.w_NomeCursore = "DICINTE"
        this.w_DICINTE_Temp = ADDBS(tempadhoc())+"DICINTE-Temp\"
        Vq_Exec("QUERY\GSCG2ADE", this, this.w_NomeCursore)
        if RECCOUNT (this.w_NomeCursore)>0
          * --- Se non esiste crea la cartella per i file temporanei
          if NOT DIRECTORY (this.w_DICINTE_Temp)
            MD (this.w_DICINTE_Temp)
          endif
          * --- --Lancia Batch di creazione immagini 
          this.w_TipoGest = "Dicinte"
          GSAR_BDI (this,this.w_NomeCursore,this.w_DICINTE_Temp,this.w_TipoGest )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Dobbiamo duplicare i record del cursore per poter stampare i vari frontespizi
          Select *,"1" as PAGINA From (this.w_NomeCursore) into cursor "Frontespizio"
          * --- Aggiorno l'immagine da associare al cursore 'Frontespizio'
          this.w_FILEORIGINE = ALLTRIM(ADDBS(ALLTRIM(Sys(5)+Sys(2003))))+"BMP\DIC_INTE\DICINTE_FRONTESPIZIO."+iif(Type("g_FORCEJPG")="C",g_FORCEJPG,"PNG")
          =Wrcursor("Frontespizio")
          Update Frontespizio set Immagine=this.w_FILEORIGINE
          * --- Unisco il cursore del frontespizio con quello del corpo della dichiarazione di intento
          * --- Verifico se l'utente ha deciso di stampare anche il quadro A
          if this.w_STAMPAQUADROA="S"
            VQ_EXEC("QUERY\GSCG4ADE", this, "QuadroA")
            GSAR_BDI (this,"QuadroA",this.w_DICINTE_Temp,this.w_TipoGest )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Unisco il cursore del frontespizio con quello del corpo della dichiarazione di intento
            Select *,"2" as Pagina,"A" as Ordina from (this.w_NomeCursore); 
 Union all; 
 Select *,"A" as Ordina from Frontespizio; 
 Union all; 
 Select *,"2" as Pagina,"B" as Ordina from QuadroA; 
 into cursor __TMP__ order by Ordina,Dinumdoc,Diserial,Pagina
          else
            Select *,"2" as Pagina from (this.w_NomeCursore); 
 Union all; 
 Select * from Frontespizio; 
 into cursor __TMP__ order by Dinumdoc,Diserial,Pagina
          endif
          * --- Lancio la stampa
          CP_CHPRN("QUERY\GSCG_ADE.FRX","",this.oParentObject)
          * --- Distrugge la cartella dei file temporanei
          if DIRECTORY (this.w_DICINTE_Temp) 
            DELETE file ( this.w_DICINTE_Temp +"*.*")
            RD (this.w_DICINTE_Temp)
          endif
        else
          ah_ErrorMsg("Non ci sono dati da stampare")
        endif
        use in Select (this.w_NomeCursore)
        use in Select ("Frontespizio")
        use in Select ("QuadroA")
        use in Select ("__TMP__")
      case this.pParame="A"
        This.oParentObject.NotifyEvent("Esegui")
      case this.pParame="P"
        this.w_CONFERMA = .F.
        this.w_DIPROTEC = This.oParentObject.w_Diprotec
        this.w_DIPRODOC = This.oParentObject.w_Diprodoc
        do GSCG_KPT with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONFERMA
          * --- Write into DIC_INTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DIPROTEC ="+cp_NullLink(cp_ToStrODBC(this.w_DIPROTEC),'DIC_INTE','DIPROTEC');
            +",DIPRODOC ="+cp_NullLink(cp_ToStrODBC(this.w_DIPRODOC),'DIC_INTE','DIPRODOC');
                +i_ccchkf ;
            +" where ";
                +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
                   )
          else
            update (i_cTable) set;
                DIPROTEC = this.w_DIPROTEC;
                ,DIPRODOC = this.w_DIPRODOC;
                &i_ccchkf. ;
             where;
                DISERIAL = this.oParentObject.w_DISERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          This.oParentObject.NotifyEvent("AggProtocollo")
        endif
      case this.pParame="G"
        this.w_ANTIPCON = "F"
        Opengest("A","Gsar_Afr","Antipcon",this.w_ANTIPCON,"Ancodice",this.oParentObject.w_DICODICE)
      case this.pParame="N"
        * --- Se � stato attivato il check "Integrativa" e valorizzato il protocollo da integrare
        *     verifico se non esiste gi� una comunicazione integrativa con i medesimi riferimenti
        if this.oParentObject.w_Dgflgcor="1" And Not Empty(this.oParentObject.w_Dgprotec) And Not Empty(this.oParentObject.w_Dgprodoc)
          * --- Read from GEDICEME
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.GEDICEME_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GEDICEME_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DGSERIAL"+;
              " from "+i_cTable+" GEDICEME where ";
                  +"DGFLGCOR = "+cp_ToStrODBC("1");
                  +" and DGPROTEC = "+cp_ToStrODBC(this.oParentObject.w_DGPROTEC);
                  +" and DGPRODOC = "+cp_ToStrODBC(this.oParentObject.w_DGPRODOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DGSERIAL;
              from (i_cTable) where;
                  DGFLGCOR = "1";
                  and DGPROTEC = this.oParentObject.w_DGPROTEC;
                  and DGPRODOC = this.oParentObject.w_DGPRODOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERIALE = NVL(cp_ToDate(_read_.DGSERIAL),cp_NullValue(_read_.DGSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty(this.w_Seriale) And this.w_Seriale<>this.oParentObject.w_DgSerial
            Ah_ErrorMsg("Esiste gi� una comunicazione integrativa per il protocollo inserito")
            this.oParentObject.w_DGPROTEC = Space(17)
            this.oParentObject.w_DGPRODOC = 0
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_ZOOM = This.oParentObject.w_RettZoom.cCursor
        Select (this.w_Zoom) 
 Zap
        This.oParentObject.NotifyEvent("Rettifica")
      case this.pParame="R"
        if Ah_yesno("Scartando la dichiarazione di intento%0quest'ultima verr� eliminata dalla generazione.%0Si vuole confermare?")
          * --- Delete from GEDICDEM
          i_nConn=i_TableProp[this.GEDICDEM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GEDICDEM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DGSERIAL);
                  +" and DDSERDIC = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
                   )
          else
            delete from (i_cTable) where;
                  DDSERIAL = this.oParentObject.w_DGSERIAL;
                  and DDSERDIC = this.oParentObject.w_DISERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Con il passaggio da vecchie release DDSERDIC potrebbe essere a 10 caratteri
          * --- Delete from GEDICDEM
          i_nConn=i_TableProp[this.GEDICDEM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GEDICDEM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DGSERIAL);
                  +" and DDSERDIC = "+cp_ToStrODBC(right (alltrim(this.oParentObject.w_DISERIAL),10));
                   )
          else
            delete from (i_cTable) where;
                  DDSERIAL = this.oParentObject.w_DGSERIAL;
                  and DDSERDIC = right (alltrim(this.oParentObject.w_DISERIAL),10);

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        This.oParentObject.NotifyEvent("AggProtocollo")
      case this.pParame="C"
        this.oParentObject.w_HASEVENT = .T.
        * --- Prima di effettuare la cancellazione verifico se esiste all'interno della generazione
        *     una dichiarazione di intento che � stata protocollata. Il controllo lo effettuo
        *     solo nel caso in cui la generazione non � integrativa
        if this.oParentObject.w_Dgflgcor="0"
          * --- Select from Gscg5Ade
          do vq_exec with 'Gscg5Ade',this,'_Curs_Gscg5Ade','',.f.,.t.
          if used('_Curs_Gscg5Ade')
            select _Curs_Gscg5Ade
            locate for 1=1
            do while not(eof())
            * --- La query restituisce un record oppure niente,nel caso restituisca un record
            *     devo segnalare l'errore all'utente
            this.oParentObject.w_HASEVENT = .F.
              select _Curs_Gscg5Ade
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DIC_INTE'
    this.cWorkTables[2]='GEDICDEM'
    this.cWorkTables[3]='GEDICEME'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_Gscg5Ade')
      use in _Curs_Gscg5Ade
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
