* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bv2                                                        *
*              Calcola saldi IVA                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_343]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-21                                                      *
* Last revis.: 2013-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bv2",oParentObject)
return(i_retval)

define class tgscg_bv2 as StdBatch
  * --- Local variables
  w_NUMOPE = 0
  w_CODATT = space(5)
  w_CODAZI = space(5)
  w_ANNO = space(4)
  w_DATFIN = ctod("  /  /  ")
  w_NUMPER = 0
  w_DATFI2 = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_VALSAL = space(3)
  w_PVLSAL = 0
  w_DATAINIP = ctod("  /  /  ")
  w_DATAFINP = ctod("  /  /  ")
  w_FLTRASP = .f.
  w_DATAINIZ = ctod("  /  /  ")
  w_FLDEFI = space(5)
  w_DATAINIP2 = ctod("  /  /  ")
  w_DATINI2 = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_IMPSTA = 0
  w_DATBLO2 = ctod("  /  /  ")
  w_IVASTA = 0
  w_CONTA = 0
  w_IVISTA = 0
  w_MESS = space(90)
  w_MACSTA = 0
  w_APPO = space(10)
  w_IMPPRE = 0
  w_APPO1 = space(10)
  w_IVAPRE = 0
  w_OK = .f.
  w_TIPOPE = space(1)
  w_IVIPRE = 0
  w_TIPREG = space(1)
  w_MACPRE = 0
  w_NUMREG = 0
  w_IMPSEG = 0
  w_CODIVA = space(5)
  w_IVASEG = 0
  w_NEWREC = space(10)
  w_IVISEG = 0
  w_PERIVA = 0
  w_MACSEG = 0
  w_PERIND = 0
  w_IMPFAD = 0
  w_MACIVA = space(1)
  w_IVAFAD = 0
  w_IMPONI = 0
  w_IVIFAD = 0
  w_IMPIVA = 0
  w_MACFAD = 0
  w_VALNAZ = space(3)
  w_IMPIND = 0
  w_DATREG = ctod("  /  /  ")
  w_IVAIND = 0
  w_COMIVA = ctod("  /  /  ")
  w_IVIIND = 0
  w_FLIVDF = space(1)
  w_FLAUTR = space(1)
  w_MACIND = 0
  w_DETIVA = 0
  w_CORVEN = 0
  w_INDIVA = 0
  w_TOTVEN = 0
  w_TOTVENP = 0
  w_RESVEN = 0
  w_OLDPER = 0
  w_DATIN2 = ctod("  /  /  ")
  w_APPO2 = space(10)
  w_APPO3 = space(10)
  w_APPO4 = space(10)
  w_DTRIF = ctod("  /  /  ")
  w_ESER = space(10)
  w_CODCON = space(20)
  w_TIPDOC = space(2)
  w_FLINTR = space(1)
  w_CODCAU = space(5)
  w_TESTVAL = 0
  w_TESTVA1 = 0
  w_FLVAFF = space(1)
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  PRI_DETT_idx=0
  PRI_MAST_idx=0
  ESERCIZI_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola i Saldi IVA (da GSCG_BRI e GSCG_BDI)
    * --- w_OKCAL: (Se .T. Informa il Programma chiamante che il Calcolo dei Saldi e' avventuto con Successo)
    * --- Assegna le variabili di Filtro In Funzione del Programma di Provenienza
    WITH this.oParentObject
    * --- Calcola Saldi per Registri IVA e Liquidazione in Prova (GSCG_BRI, GSCG_BDI)
    this.w_ANNO = .oParentObject.w_ANNO
    this.w_CODAZI = .oParentObject.w_CODAZI
    this.w_NUMPER = .oParentObject.w_NUMPER
    this.w_CODATT = .w_ATTIVI
    this.w_DATINI = .w_INIPER
    this.w_DATFIN = .w_FINPER
    * --- Se da Acconto Iva FLDEFI = 'A' e data fine selezione  20/12
    this.w_FLDEFI = .oParentObject.w_FLDEFI
    this.w_DATFI2 = .w_FINPE2
    this.w_DATINI2 = cp_CharToDate("01-11-"+ALLTRIM(this.w_ANNO))
    * --- NUMOPE: 1 solo registrazioni di primanota caso comune anche ai registri Iva.
    *     2 solo acconto iva:Documenti FA e NC confermati sia ACQ che VEN
    *     3 solo acconto iva:Documenti FA e NC provvisori solo VEN
    this.w_NUMOPE = .w_NUMOPE
    ENDWITH
    * --- Eventuale Gestione IVA Trasportatori
    this.w_FLTRASP = .F.
    this.w_DATAINIZ = this.w_DATINI
    do trim_prec with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se da Acconto Iva la data di fine selezione � il 20-12 
    * --- SE � trimestrale o del Mese 3,6,9,12 deve elaborare le registrazioni AUTOTRASPORTO
    this.w_FLTRASP = IIF(g_TIPDEN="T" OR MOD(this.w_NUMPER, 3)=0, .T., .F.)
    * --- Valuta di Riverimento dei Saldi IVA
    * --- Assegnato a g_PERVAL in quanto non e' possibile eseguire la ricostruzione in un periodo diverso dall'esercizio in corso
    this.w_VALSAL = g_PERVAL
    this.w_PVLSAL = g_PERPVL
    * --- Calcola Inizio mese Precedente
    this.w_DATIN2 = this.w_DATINI - 1
    this.w_DATIN2 = cp_CharToDate("01-" + RIGHT("00"+ALLTRIM(STR(MONTH(this.w_DATIN2))), 2) + "-" + RIGHT("0000"+ALLTRIM(STR(YEAR(this.w_DATIN2))), 4))
    * --- Verifica che non esistano piu' di 1 Tipo e Num.Registri
    this.w_OK = .T.
    this.w_MESS = " "
    * --- Select from CONTAREG
    do vq_exec with 'CONTAREG',this,'_Curs_CONTAREG','',.f.,.t.
    if used('_Curs_CONTAREG')
      select _Curs_CONTAREG
      locate for 1=1
      do while not(eof())
      if NVL(_Curs_CONTAREG.CONTA,0)>1
        this.w_OK = .F.
        this.w_APPO = NVL(_Curs_CONTAREG.ATTIPREG," ")
        do case
          case this.w_APPO="E"
            this.w_MESS = "Verificare registro IVA corrisp./ventilazione - num.: %1 presente in pi� attivit�"
          case this.w_APPO="C"
            this.w_MESS = "Verificare registro IVA corrisp./scorporo - num.: %1 presente in pi� attivit�"
          case this.w_APPO="A"
            this.w_MESS = "Verificare registro IVA acquisti - num.: %1 presente in pi� attivit�"
          otherwise
            this.w_MESS = "Verificare registro IVA vendite - num.: %1 presente in pi� attivit�"
        endcase
        ah_ErrorMsg(this.w_MESS,,"", ALLTR(STR(NVL(_Curs_CONTAREG.ATNUMREG,0))) )
      endif
        select _Curs_CONTAREG
        continue
      enddo
      use
    endif
    if this.w_OK=.F.
      i_retcode = 'stop'
      return
    endif
    if this.w_FLDEFI="A"
      VQ_EXEC("QUERY\GSAR_QAI.VQR",this,"REGIVA")
    else
      * --- Inizia Elaborazione (Legge le Registrazioni IVA dalla Primanota)
      VQ_EXEC("QUERY\GSAR_QSI.VQR",this,"REGIVA")
    endif
    if USED("REGIVA")
      SELECT REGIVA
      this.w_OK = .T.
      * --- Verifica se nella Selezioni esistono Registrazioni non Confermate
      this.w_CONTA = 0
      COUNT FOR NVL(FLPROV,"N")="S" TO this.w_CONTA
      if this.w_CONTA>0
        this.w_OK = ah_YesNo("Per le selezioni impostate esistono registrazioni non confermate;%0Esse non verranno computate ai fini del calcolo dei saldi%0Proseguo comunque?")
      endif
      if this.w_OK=.T.
        * --- Try
        local bErr_03A86BF0
        bErr_03A86BF0=bTrsErr
        this.Try_03A86BF0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_OK = .F.
          if NOT EMPTY(i_errmsg)
            ah_ErrorMsg(i_errmsg,,"")
          else
            ah_ErrorMsg("Errore durante l'elaborazione; operazione abbandonata",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_03A86BF0
        * --- End
      endif
    endif
    * --- Elimina i Cursori
    if USED("CursElab") AND this.w_FLDEFI<>"A"
      SELECT CursElab
      USE
    endif
    if USED("APPOVENT")
      SELECT APPOVENT
      USE
    endif
    if USED("REGIVA")
      SELECT REGIVA
      USE
    endif
    if USED("OLDVEN")
      SELECT OLDVEN
      USE
    endif
    * --- Calcolo Saldi Ok 
    if this.w_OK=.T.
      this.oParentObject.w_OKCAL = .T.
    endif
  endproc
  proc Try_03A86BF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OK = .T.
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Saldi
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR CursElab ;
    (TIPOPE C(1), TIPREG C(1), NUMREG N(2,0), CODIVA C(5), PERIVA N(6,2), PERIND N(6,2), MACIVA C(1), ;
    IMPSTA N(18,4), IVASTA N(18,4), IVISTA N(18,4), MACSTA N(18,4), ;
    IMPPRE N(18,4), IVAPRE N(18,4), IVIPRE N(18,4), MACPRE N(18,4), ;
    IMPSEG N(18,4), IVASEG N(18,4), IVISEG N(18,4), MACSEG N(18,4), ;
    IMPFAD N(18,4), IVAFAD N(18,4), IVIFAD N(18,4), MACFAD N(18,4), ;
    IMPIND N(18,4), IVAIND N(18,4), IVIIND N(18,4), MACIND N(18,4), ;
    CODCON C(20), TIPDOC C(2), CODCAU C(5),FLVAFF C(1))
    * --- Array che conterr� il Totale dei Corrispettivi da Ventilare (la posizione indica il numero del registro)
    DIMENSION CORVEN[99]
    FOR i = 1 TO 99
    CORVEN[i] = 0
    ENDFOR
    * --- Azzera Variabili di Appoggio
    this.w_IMPSTA = 0
    this.w_IMPSEG = 0
    this.w_IMPIND = 0
    this.w_IVASTA = 0
    this.w_IVASEG = 0
    this.w_IVAIND = 0
    this.w_IVISTA = 0
    this.w_IVISEG = 0
    this.w_IVIIND = 0
    this.w_MACSTA = 0
    this.w_MACSEG = 0
    this.w_MACIND = 0
    this.w_IMPPRE = 0
    this.w_IMPFAD = 0
    this.w_CORVEN = 0
    this.w_IVAPRE = 0
    this.w_IVAFAD = 0
    this.w_TOTVEN = 0
    this.w_TOTVENP = 0
    this.w_IVIPRE = 0
    this.w_IVIFAD = 0
    this.w_RESVEN = 0
    this.w_MACPRE = 0
    this.w_MACFAD = 0
    this.w_CONTA = 0
    this.w_NEWREC = "XXXXXX"
    this.w_TIPOPE = "A"
    this.w_CODCON = SPACE(20)
    this.w_TIPDOC = SPACE(2)
    this.w_TESTVAL = 0
    this.w_TESTVA1 = 0
    * --- Lettura Cursore (Tranne registro Corrispettivi Ventilazione)
    SELECT REGIVA
    GO TOP
    SCAN FOR NVL(TIPREG," ") $ "VAC" AND NVL(NUMREG,0)>0 AND NOT EMPTY(NVL(CODIVA,"")) AND NVL(FLPROV," ")<>"S"
    if this.w_NEWREC<>TIPOPE+TIPREG+STR(NUMREG, 2,0)+CODIVA+CODCAU+FLINTR
      * --- Cambio Record
      if this.w_CONTA>0
        * --- Controllo la valorizzazione dei campi poich� in certi casi andrebbe ad aggiornare i saldi con valori a 0
        this.w_TESTVAL = this.w_IMPSTA+this.w_IMPSEG+this.w_IMPIND+this.w_IVASTA+this.w_IVASEG+this.w_IVAIND+this.w_IVISTA+this.w_IVISEG+this.w_IVIIND+this.w_MACSTA
        this.w_TESTVA1 = this.w_MACSEG+this.w_MACIND+this.w_IMPPRE+this.w_IMPFAD+this.w_IVAPRE+this.w_IVIFAD+this.w_MACPRE+this.w_MACFAD
        if this.w_TESTVAL+this.w_TESTVA1<>0
          * --- Se Trovati in Precedenza Scrivo il Cursore di Elaborazione
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT REGIVA
        endif
      endif
      this.w_TIPOPE = TIPOPE
      this.w_CODCON = NVL(CODCON,SPACE(2))
      this.w_TIPDOC = NVL(TIPDOC,SPACE(2))
      this.w_CODCAU = CODCAU
      this.w_TIPREG = TIPREG
      this.w_NUMREG = NUMREG
      this.w_CODIVA = CODIVA
      do case
        case this.w_TIPREG="C"
          this.w_MESS = "Elaborazione registro IVA corrisp./scorporo - num. %1%0Cod.IVA %2"
        case this.w_TIPREG="A"
          this.w_MESS = "Elaborazione registro IVA acquisti - num. %1%0Cod.IVA %2"
        otherwise
          this.w_MESS = "Elaborazione registro IVA vendite - num. %1%0Cod.IVA %2"
      endcase
      ah_Msg(this.w_MESS,.T.,.F.,.F., ALLTR(STR(this.w_NUMREG)), this.w_CODIVA)
      this.w_MACIVA = NVL(MACIVA, "N")
      this.w_FLVAFF = NVL(FLVAFF, "N")
      this.w_PERIVA = NVL(PERIVA, 0)
      this.w_PERIND = NVL(PERIND, 0)
      this.w_CONTA = 0
      this.w_NEWREC = TIPOPE+TIPREG+STR(NUMREG, 2,0)+CODIVA+CODCAU+FLINTR
      * --- Azzera Variabili di Appoggio
      this.w_IMPSTA = 0
      this.w_IMPSEG = 0
      this.w_IMPIND = 0
      this.w_IVASTA = 0
      this.w_IVASEG = 0
      this.w_IVAIND = 0
      this.w_IVISTA = 0
      this.w_IVISEG = 0
      this.w_IVIIND = 0
      this.w_MACSTA = 0
      this.w_MACSEG = 0
      this.w_MACIND = 0
      this.w_IMPPRE = 0
      this.w_IMPFAD = 0
      this.w_IVAPRE = 0
      this.w_IVAFAD = 0
      this.w_IVIPRE = 0
      this.w_IVIFAD = 0
      this.w_MACPRE = 0
      this.w_MACFAD = 0
    endif
    this.w_CONTA = this.w_CONTA + 1
    this.w_DATREG = CP_TODATE(DATREG)
    this.w_COMIVA = IIF(EMPTY(CP_TODATE(COMIVA)) , this.w_DATREG, CP_TODATE(COMIVA))
    * --- Flag Esig.Differita: F = Fattura Esig.Differita; I = Incasso/Pag.Es.Diff.; ' ' NO Esig.Diff.
    this.w_FLIVDF = IIF(NVL(FLPDIF," ")="S", "I", IIF(NVL(FLIVDF, " ")="S", "F", " "))
    * --- Flag Autotrasportatori
    this.w_FLAUTR = NVL(FLAUTR, " ")
    * --- Se Nota di Credito gli Importi sono gia' definiti Negativi sulla Query
    this.w_IMPONI = NVL(IMPONI, 0)
    this.w_IMPIVA = NVL(IMPIVA, 0)
    this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
    * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
    * --- Prevediamo solo LIRE o EURO
    if CAOVAL<>1
      this.w_IMPONI = VAL2MON(this.w_IMPONI, CAOVAL,1,I_DATSYS, g_PERVAL, GETVALUT(g_PERVAL, "VADECTOT"))
      this.w_IMPIVA = VAL2MON(this.w_IMPIVA,CAOVAL,1,I_DATSYS, g_PERVAL, GETVALUT(g_PERVAL, "VADECTOT"))
    endif
    * --- Calcola le Parti Detraibile e Indetraibile delI' IVA
    this.w_DETIVA = this.w_IMPIVA
    this.w_INDIVA = 0
    if this.w_PERIND>0 AND this.w_IMPIVA<>0 
      * --- Calcola la Parte Indetraibile sull'Ammontare dell'Aliquota
      this.w_INDIVA = cp_ROUND((this.w_IMPIVA * this.w_PERIND) / 100, this.w_PVLSAL)
      * --- Totale IVA Detraibile
      this.w_DETIVA = this.w_IMPIVA - this.w_INDIVA
    endif
    * --- Registri Acquisti, Corrispettivi/Scorporo, Vendite
    if this.w_DATREG>=this.w_DATINI AND this.w_DATREG<=this.w_DATFIN AND this.w_COMIVA>=this.w_DATINI AND this.w_COMIVA<=this.w_DATFIN AND this.w_FLIVDF=" " AND this.w_FLAUTR=" " AND this.w_TIPOPE<>"C"
      * --- Documenti del Periodo
      this.w_IMPSTA = this.w_IMPSTA + this.w_IMPONI
      this.w_IVASTA = this.w_IVASTA + this.w_DETIVA
      this.w_IVISTA = this.w_IVISTA + this.w_INDIVA
    endif
    if this.w_DATREG>=this.w_DATINI2 AND this.w_DATREG<=this.w_DATFIN AND this.w_COMIVA>=this.w_DATINI2 AND this.w_COMIVA<=this.w_DATFIN AND this.w_FLIVDF=" " AND this.w_FLAUTR=" " AND this.w_TIPOPE="C"
      * --- Documenti del Periodo:solo per operazioni effettuate infatti controllo su DATINI2 e non su DATINI e TIPOPE='C'
      this.w_IMPSTA = this.w_IMPSTA + this.w_IMPONI
      this.w_IVASTA = this.w_IVASTA + this.w_DETIVA
      this.w_IVISTA = this.w_IVISTA + this.w_INDIVA
    endif
    * --- Per IVA Autotrasportatori
    if this.w_FLTRASP=.T.
      if this.w_DATREG>=this.w_DATAINIP AND this.w_DATREG<=this.w_DATAFINP AND this.w_COMIVA>=this.w_DATAINIP AND this.w_COMIVA<=this.w_DATAFINP AND this.w_FLIVDF=" " AND this.w_FLAUTR="S"
        * --- Documenti del Trimestre prec. con FLAG IVA Autotrasportatori
        this.w_IMPSTA = this.w_IMPSTA + this.w_IMPONI
        this.w_IVASTA = this.w_IVASTA + this.w_DETIVA
        this.w_IVISTA = this.w_IVISTA + this.w_INDIVA
      endif
    endif
    if this.w_DATREG>=this.w_DATINI AND this.w_DATREG<=this.w_DATFIN AND this.w_COMIVA<this.w_DATINI AND this.w_COMIVA>=this.w_DATIN2 AND this.w_FLIVDF=" " AND this.w_FLAUTR=" "
      * --- Periodo Precedente
      this.w_IMPPRE = this.w_IMPPRE + this.w_IMPONI
      this.w_IVAPRE = this.w_IVAPRE + this.w_DETIVA
      this.w_IVIPRE = this.w_IVIPRE + this.w_INDIVA
      * --- Aggiunge ai Documenti del Periodo
      this.w_IMPSTA = this.w_IMPSTA + this.w_IMPONI
      this.w_IVASTA = this.w_IVASTA + this.w_DETIVA
      this.w_IVISTA = this.w_IVISTA + this.w_INDIVA
    endif
    if this.w_DATREG>this.w_DATFIN AND this.w_DATREG<=this.w_DATFI2 AND this.w_COMIVA>=this.w_DATINI AND this.w_COMIVA<=this.w_DATFIN AND this.w_FLIVDF=" " AND this.w_FLAUTR=" "
      * --- Periodo Seguente
      this.w_IMPSEG = this.w_IMPSEG + this.w_IMPONI
      this.w_IVASEG = this.w_IVASEG + this.w_DETIVA
      this.w_IVISEG = this.w_IVISEG + this.w_INDIVA
    endif
    if this.w_DATREG>=this.w_DATINI AND this.w_DATREG<=this.w_DATFIN AND this.w_FLIVDF="F" AND this.w_FLAUTR=" "
      * --- Fatture ad Esigibilita' Differita
      this.w_IMPFAD = this.w_IMPFAD + this.w_IMPONI
      this.w_IVAFAD = this.w_IVAFAD + this.w_DETIVA
      this.w_IVIFAD = this.w_IVIFAD + this.w_INDIVA
      * --- Aggiunge ai Documenti del Periodo
      this.w_IMPSTA = this.w_IMPSTA + this.w_IMPONI
      this.w_IVASTA = this.w_IVASTA + this.w_DETIVA
      this.w_IVISTA = this.w_IVISTA + this.w_INDIVA
    endif
    if this.w_DATREG>=this.w_DATINI AND this.w_DATREG<=this.w_DATFIN AND this.w_FLIVDF="I" AND this.w_FLAUTR=" "
      * --- Incassi/Pagamenti ad Esigibilita' Differita
      this.w_IMPIND = this.w_IMPIND + this.w_IMPONI
      this.w_IVAIND = this.w_IVAIND + this.w_DETIVA
      this.w_IVIIND = this.w_IVIIND + this.w_INDIVA
    endif
    SELECT REGIVA
    ENDSCAN
    if this.w_CONTA>0
      this.w_TESTVAL = this.w_IMPSTA+this.w_IMPSEG+this.w_IMPIND+this.w_IVASTA+this.w_IVASEG+this.w_IVAIND+this.w_IVISTA+this.w_IVISEG+this.w_IVIIND+this.w_MACSTA
      this.w_TESTVA1 = this.w_MACSEG+this.w_MACIND+this.w_IMPPRE+this.w_IMPFAD+this.w_IVAPRE+this.w_IVIFAD+this.w_MACPRE+this.w_MACFAD
      if this.w_TESTVAL+this.w_TESTVA1<>0
        * --- Se Trovati in Precedenza Scrivo il Cursore di Elaborazione
        *     Devono essere valorizzati i campi relativi ai valori altrimenti registrerebbe nei saldi con valori a 0
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT REGIVA
      endif
    endif
    * --- Chiude REGIVA (Lo riutilizza per i Corrispettivi/Ventilazione)
    if USED("REGIVA")
      SELECT REGIVA
      USE
    endif
    * --- Alla Fine Elabora i Corrispettivi in Ventilazione
    ah_Msg("Elaborazione registro IVA corrispettivi/ventilazione - num.: %1",.T.,.F.,.F., ALLTR(STR(this.w_NUMREG)) )
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiude REGIVA (non serve piu')
    if USED("REGIVA")
      SELECT REGIVA
      USE
    endif
    * --- Elimina i Precedenti Saldi
    ah_Msg("Azzeramento precedenti saldi...",.T.)
    if this.w_FLDEFI="A"
      * --- Nel caso di Acconto IVA viene creata la tabella temporanea PRI_TEMP nel Batch chiamante GSCG_BDI
    else
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
            +" where ATCODATT="+cp_ToStrODBC(this.w_CODATT)+"";
             ,"_Curs_ATTIDETT")
      else
        select * from (i_cTable);
         where ATCODATT=this.w_CODATT;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_TIPREG = NVL(_Curs_ATTIDETT.ATTIPREG, " ")
        this.w_NUMREG = NVL(_Curs_ATTIDETT.ATNUMREG, 0)
        if NOT EMPTY(this.w_TIPREG) AND this.w_NUMREG<>0
          * --- Try
          local bErr_03D6C710
          bErr_03D6C710=bTrsErr
          this.Try_03D6C710()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03D6C710
          * --- End
        endif
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
    endif
    if USED("CursElab")
      SELECT TIPOPE, TIPREG, NUMREG, MAX(CODIVA) AS CODIVA FROM CursElab ;
      WHERE TIPOPE ="A" ;
      GROUP BY TIPOPE, TIPREG, NUMREG INTO CURSOR APPOVENT ;
      ORDER BY TIPOPE, TIPREG, NUMREG
      SELECT APPOVENT
      if RECCOUNT()>0 AND this.w_FLDEFI<>"A"
        * --- Solo se Ricostruzione Saldi IVA
        GO TOP
        SCAN FOR NOT EMPTY(NVL(TIPREG, " ")) AND NVL(NUMREG,0)<>0
        this.w_TIPREG = TIPREG
        this.w_NUMREG = NUMREG
        this.w_APPO = IIF(this.w_TIPREG="E", CORVEN[this.w_NUMREG], 0)
        do case
          case this.w_TIPREG="E"
            this.w_MESS = "Scrittura testata saldi registro IVA: corrisp./ventilazione - num. %1"
          case this.w_TIPREG="C"
            this.w_MESS = "Scrittura testata saldi registro IVA: corrisp./scorporo - num. %1"
          case this.w_TIPREG="A"
            this.w_MESS = "Scrittura testata saldi registro IVA: acquisti - num. %1"
          otherwise
            this.w_MESS = "Scrittura testata saldi registro IVA: vendite - num. %1"
        endcase
        ah_Msg(this.w_MESS,.T.,.F.,.F., ALLTR(STR(this.w_NUMREG)) )
        * --- Try
        local bErr_03D32D50
        bErr_03D32D50=bTrsErr
        this.Try_03D32D50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PRI_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRI_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TRIMPVEN ="+cp_NullLink(cp_ToStrODBC(this.w_APPO),'PRI_MAST','TRIMPVEN');
                +i_ccchkf ;
            +" where ";
                +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
                +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                   )
          else
            update (i_cTable) set;
                TRIMPVEN = this.w_APPO;
                &i_ccchkf. ;
             where;
                TR__ANNO = this.w_ANNO;
                and TRNUMPER = this.w_NUMPER;
                and TRTIPREG = this.w_TIPREG;
                and TRNUMREG = this.w_NUMREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03D32D50
        * --- End
        SELECT APPOVENT
        ENDSCAN
        SELECT CursElab
        GO TOP
        SCAN FOR NOT EMPTY(NVL(TIPREG, " ")) AND NVL(NUMREG,0)<>0 AND NOT EMPTY(NVL(CODIVA,""))
        this.w_TIPOPE = TIPOPE
        this.w_TIPREG = TIPREG
        this.w_NUMREG = NUMREG
        this.w_CODIVA = CODIVA
        this.w_IMPSTA = IMPSTA
        this.w_IMPSEG = IMPSEG
        this.w_IMPIND = IMPIND
        this.w_IVASTA = IVASTA
        this.w_IVASEG = IVASEG
        this.w_IVAIND = IVAIND
        this.w_IVISTA = IVISTA
        this.w_IVISEG = IVISEG
        this.w_IVIIND = IVIIND
        this.w_MACSTA = MACSTA
        this.w_MACSEG = MACSEG
        this.w_MACIND = MACIND
        this.w_IMPPRE = IMPPRE
        this.w_IMPFAD = IMPFAD
        this.w_IVAPRE = IVAPRE
        this.w_IVAFAD = IVAFAD
        this.w_IVIPRE = IVIPRE
        this.w_IVIFAD = IVIFAD
        this.w_MACPRE = MACPRE
        this.w_MACFAD = MACFAD
        if this.w_TIPREG="E" AND this.w_RESVEN<>0 AND this.w_IVASTA<>0
          * --- Eventuale Resto Corrispettivi da Ventilare da Ripartite
          this.w_IVASTA = this.w_IVASTA + this.w_RESVEN
          this.w_RESVEN = 0
        endif
        if this.w_TIPREG = "A" AND NVL(MACIVA,"N") = "S" AND NVL(FLVAFF,"N") <> "N" 
          * --- Se acquisti con codice iva escluso e monteacquisti azzero iva
          this.w_IVASTA = 0
          this.w_IVASEG = 0
          this.w_IVAIND = 0
          this.w_IVISTA = 0
          this.w_IVISEG = 0
          this.w_IVIIND = 0
          this.w_IVAPRE = 0
          this.w_IVAFAD = 0
          this.w_IVIPRE = 0
          this.w_IVIFAD = 0
        endif
        do case
          case this.w_TIPREG="E"
            this.w_MESS = "Scrittura dettaglio saldi registro IVA: corrisp./ventilazione - num. %1"
          case this.w_TIPREG="C"
            this.w_MESS = "Scrittura dettaglio saldi registro IVA: corrisp./scorporo - num. %1"
          case this.w_TIPREG="A"
            this.w_MESS = "Scrittura dettaglio saldi registro IVA: acquisti - num. %1"
          otherwise
            this.w_MESS = "Scrittura dettaglio saldi registro IVA: vendite - num. %1"
        endcase
        ah_Msg(this.w_MESS,.T.,.F.,.F., ALLTR(STR(this.w_NUMREG)) )
        * --- Insert into PRI_DETT
        i_nConn=i_TableProp[this.PRI_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TR__ANNO"+",TRNUMPER"+",TRTIPREG"+",TRNUMREG"+",TRCODIVA"+",TRIMPSTA"+",TRIVASTA"+",TRIVISTA"+",TRMACSTA"+",TRIMPPRE"+",TRIVAPRE"+",TRIVIPRE"+",TRMACPRE"+",TRIMPSEG"+",TRIVASEG"+",TRIVISEG"+",TRMACSEG"+",TRIMPFAD"+",TRIVAFAD"+",TRIVIFAD"+",TRMACFAD"+",TRIMPIND"+",TRIVAIND"+",TRIVIIND"+",TRMACIND"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_ANNO),'PRI_DETT','TR__ANNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPER),'PRI_DETT','TRNUMPER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_DETT','TRTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_DETT','TRNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'PRI_DETT','TRCODIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSTA),'PRI_DETT','TRIMPSTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVASTA),'PRI_DETT','TRIVASTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVISTA),'PRI_DETT','TRIVISTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACSTA),'PRI_DETT','TRMACSTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'PRI_DETT','TRIMPPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVAPRE),'PRI_DETT','TRIVAPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVIPRE),'PRI_DETT','TRIVIPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACPRE),'PRI_DETT','TRMACPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSEG),'PRI_DETT','TRIMPSEG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVASEG),'PRI_DETT','TRIVASEG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVISEG),'PRI_DETT','TRIVISEG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACSEG),'PRI_DETT','TRMACSEG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFAD),'PRI_DETT','TRIMPFAD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVAFAD),'PRI_DETT','TRIVAFAD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVIFAD),'PRI_DETT','TRIVIFAD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACFAD),'PRI_DETT','TRMACFAD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPIND),'PRI_DETT','TRIMPIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVAIND),'PRI_DETT','TRIVAIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVIIND),'PRI_DETT','TRIVIIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACIND),'PRI_DETT','TRMACIND');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TR__ANNO',this.w_ANNO,'TRNUMPER',this.w_NUMPER,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODIVA',this.w_CODIVA,'TRIMPSTA',this.w_IMPSTA,'TRIVASTA',this.w_IVASTA,'TRIVISTA',this.w_IVISTA,'TRMACSTA',this.w_MACSTA,'TRIMPPRE',this.w_IMPPRE,'TRIVAPRE',this.w_IVAPRE,'TRIVIPRE',this.w_IVIPRE)
          insert into (i_cTable) (TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG,TRCODIVA,TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND &i_ccchkf. );
             values (;
               this.w_ANNO;
               ,this.w_NUMPER;
               ,this.w_TIPREG;
               ,this.w_NUMREG;
               ,this.w_CODIVA;
               ,this.w_IMPSTA;
               ,this.w_IVASTA;
               ,this.w_IVISTA;
               ,this.w_MACSTA;
               ,this.w_IMPPRE;
               ,this.w_IVAPRE;
               ,this.w_IVIPRE;
               ,this.w_MACPRE;
               ,this.w_IMPSEG;
               ,this.w_IVASEG;
               ,this.w_IVISEG;
               ,this.w_MACSEG;
               ,this.w_IMPFAD;
               ,this.w_IVAFAD;
               ,this.w_IVIFAD;
               ,this.w_MACFAD;
               ,this.w_IMPIND;
               ,this.w_IVAIND;
               ,this.w_IVIIND;
               ,this.w_MACIND;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        SELECT CursElab
        ENDSCAN
      endif
    endif
    * --- Verifica la Presenza della Valuta nei Saldi
    if this.w_FLDEFI<>"A"
      * --- Select from PRI_MAST
      i_nConn=i_TableProp[this.PRI_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2],.t.,this.PRI_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRI_MAST ";
             ,"_Curs_PRI_MAST")
      else
        select * from (i_cTable);
          into cursor _Curs_PRI_MAST
      endif
      if used('_Curs_PRI_MAST')
        select _Curs_PRI_MAST
        locate for 1=1
        do while not(eof())
        if EMPTY(NVL(_Curs_PRI_MAST.TRCODVAL, "")) AND NOT EMPTY(NVL(_Curs_PRI_MAST.TR__ANNO, "")) AND NVL(_Curs_PRI_MAST.TRNUMPER, 0)<>0
          this.w_APPO = _Curs_PRI_MAST.TR__ANNO
          this.w_APPO1 = _Curs_PRI_MAST.TRNUMPER
          this.w_APPO2 = _Curs_PRI_MAST.TRTIPREG
          this.w_APPO3 = _Curs_PRI_MAST.TRNUMREG
          this.w_DTRIF = "01-"+RIGHT("00"+ALLTRIM(STR(IIF(g_TIPDEN="M", this.w_APPO1, (this.w_APPO1 * 3) - 2))),2)+"-"+ALLTRIM(this.w_APPO)
          this.w_DTRIF = cp_CharToDate(this.w_DTRIF)
          this.w_ESER = CALCESER(this.w_DTRIF, g_CODESE)
          this.w_APPO4 = g_CODLIR
          if NOT EMPTY(this.w_ESER)
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESVALNAZ"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                    +" and ESCODESE = "+cp_ToStrODBC(this.w_ESER);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESVALNAZ;
                from (i_cTable) where;
                    ESCODAZI = this.w_CODAZI;
                    and ESCODESE = this.w_ESER;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO4 = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Write into PRI_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRI_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TRCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_APPO4),'PRI_MAST','TRCODVAL');
                +i_ccchkf ;
            +" where ";
                +"TR__ANNO = "+cp_ToStrODBC(this.w_APPO);
                +" and TRNUMPER = "+cp_ToStrODBC(this.w_APPO1);
                +" and TRTIPREG = "+cp_ToStrODBC(this.w_APPO2);
                +" and TRNUMREG = "+cp_ToStrODBC(this.w_APPO3);
                   )
          else
            update (i_cTable) set;
                TRCODVAL = this.w_APPO4;
                &i_ccchkf. ;
             where;
                TR__ANNO = this.w_APPO;
                and TRNUMPER = this.w_APPO1;
                and TRTIPREG = this.w_APPO2;
                and TRNUMREG = this.w_APPO3;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_PRI_MAST
          continue
        enddo
        use
      endif
      ah_Msg("Aggiorna ultima data di calcolo nelle attivit�",.T.)
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATCAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'ATTIDETT','ATDATCAL');
            +i_ccchkf ;
        +" where ";
            +"ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
               )
      else
        update (i_cTable) set;
            ATDATCAL = this.w_DATFIN;
            &i_ccchkf. ;
         where;
            ATCODATT = this.w_CODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    WAIT CLEAR
  endproc
  proc Try_03D6C710()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRI_DETT
    i_nConn=i_TableProp[this.PRI_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
            +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
            +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
            +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
             )
    else
      delete from (i_cTable) where;
            TR__ANNO = this.w_ANNO;
            and TRNUMPER = this.w_NUMPER;
            and TRTIPREG = this.w_TIPREG;
            and TRNUMREG = this.w_NUMREG;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Select from PRI_MAST
    i_nConn=i_TableProp[this.PRI_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2],.t.,this.PRI_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRI_MAST ";
          +" where TR__ANNO= "+cp_ToStrODBC(this.w_ANNO)+" AND TRNUMPER= "+cp_ToStrODBC(this.w_NUMPER)+" AND TRTIPREG="+cp_ToStrODBC(this.w_TIPREG)+" AND TRNUMREG="+cp_ToStrODBC(this.w_NUMREG)+"";
           ,"_Curs_PRI_MAST")
    else
      select * from (i_cTable);
       where TR__ANNO= this.w_ANNO AND TRNUMPER= this.w_NUMPER AND TRTIPREG=this.w_TIPREG AND TRNUMREG=this.w_NUMREG;
        into cursor _Curs_PRI_MAST
    endif
    if used('_Curs_PRI_MAST')
      select _Curs_PRI_MAST
      locate for 1=1
      do while not(eof())
      if _Curs_PRI_MAST.TRFLSTAM<>"S" AND _Curs_PRI_MAST.TRFLSTAR<>"S" 
        * --- Delete from PRI_MAST
        i_nConn=i_TableProp[this.PRI_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
                +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                 )
        else
          delete from (i_cTable) where;
                TR__ANNO = this.w_ANNO;
                and TRNUMPER = this.w_NUMPER;
                and TRTIPREG = this.w_TIPREG;
                and TRNUMREG = this.w_NUMREG;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Write into PRI_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRI_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRIMPVEN ="+cp_NullLink(cp_ToStrODBC(0),'PRI_MAST','TRIMPVEN');
              +i_ccchkf ;
          +" where ";
              +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
              +" and TRNUMPER = "+cp_ToStrODBC(this.w_NUMPER);
              +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
              +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                 )
        else
          update (i_cTable) set;
              TRIMPVEN = 0;
              &i_ccchkf. ;
           where;
              TR__ANNO = this.w_ANNO;
              and TRNUMPER = this.w_NUMPER;
              and TRTIPREG = this.w_TIPREG;
              and TRNUMREG = this.w_NUMREG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PRI_MAST
        continue
      enddo
      use
    endif
    return
  proc Try_03D32D50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRI_MAST
    i_nConn=i_TableProp[this.PRI_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TR__ANNO"+",TRNUMPER"+",TRTIPREG"+",TRNUMREG"+",TRCODVAL"+",TRIMPVEN"+",TRFLSTAM"+",TRFLSTAR"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANNO),'PRI_MAST','TR__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPER),'PRI_MAST','TRNUMPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_MAST','TRTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_MAST','TRNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALSAL),'PRI_MAST','TRCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'PRI_MAST','TRIMPVEN');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRI_MAST','TRFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRI_MAST','TRFLSTAR');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRI_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PRI_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PRI_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PRI_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TR__ANNO',this.w_ANNO,'TRNUMPER',this.w_NUMPER,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODVAL',this.w_VALSAL,'TRIMPVEN',this.w_APPO,'TRFLSTAM'," ",'TRFLSTAR'," ",'UTCC',i_CODUTE,'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0,'UTDV',cp_CharToDate("  -  -    "))
      insert into (i_cTable) (TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG,TRCODVAL,TRIMPVEN,TRFLSTAM,TRFLSTAR,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_ANNO;
           ,this.w_NUMPER;
           ,this.w_TIPREG;
           ,this.w_NUMREG;
           ,this.w_VALSAL;
           ,this.w_APPO;
           ," ";
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive il Cursore di Elaborazione
    INSERT INTO CursElab (TIPOPE, TIPREG, NUMREG, CODIVA, PERIVA, PERIND, MACIVA, ;
    IMPSTA, IVASTA, IVISTA, MACSTA, IMPPRE, IVAPRE, IVIPRE, MACPRE, ;
    IMPSEG, IVASEG, IVISEG, MACSEG, IMPFAD, IVAFAD, IVIFAD, MACFAD, ;
    IMPIND, IVAIND, IVIIND, MACIND, CODCON, TIPDOC, CODCAU,FLVAFF) ;
    VALUES (this.w_TIPOPE, this.w_TIPREG, this.w_NUMREG, this.w_CODIVA, this.w_PERIVA, this.w_PERIND, this.w_MACIVA, ;
    this.w_IMPSTA, this.w_IVASTA, this.w_IVISTA, this.w_MACSTA, this.w_IMPPRE, this.w_IVAPRE, this.w_IVIPRE, this.w_MACPRE, ;
    this.w_IMPSEG, this.w_IVASEG, this.w_IVISEG, this.w_MACSEG, this.w_IMPFAD, this.w_IVAFAD, this.w_IVIFAD, this.w_MACFAD, ;
    this.w_IMPIND, this.w_IVAIND, this.w_IVIIND, this.w_MACIND, this.w_CODCON, this.w_TIPDOC, this.w_CODCAU,this.w_FLVAFF)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura Cursore (Solo registro Corrispettivi Ventilazione)
    this.w_TIPREG = "E"
    * --- Select from ATTIDETT
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATNUMREG  from "+i_cTable+" ATTIDETT ";
          +" where ATCODATT="+cp_ToStrODBC(this.w_CODATT)+" AND ATTIPREG="+cp_ToStrODBC(this.w_TIPREG)+"";
          +" order by ATNUMREG";
           ,"_Curs_ATTIDETT")
    else
      select ATNUMREG from (i_cTable);
       where ATCODATT=this.w_CODATT AND ATTIPREG=this.w_TIPREG;
       order by ATNUMREG;
        into cursor _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      select _Curs_ATTIDETT
      locate for 1=1
      do while not(eof())
      this.w_CORVEN = 0
      this.w_NUMREG = _Curs_ATTIDETT.ATNUMREG
      * --- Inizia Elaborazione (Legge le Registrazioni Corrispettivi/Ventilazione dalla Primanota)
      VQ_EXEC("QUERY\GSAR1QSI.VQR",this,"REGIVA")
      if USED("REGIVA")
        SELECT REGIVA
        GO TOP
        SCAN FOR NVL(TIPREG," ") = "E" AND NVL(NUMREG, 0)<>0
        this.w_IMPONI = NVL(IMPONI, 0)
        this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
        * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
        if this.w_VALNAZ<>this.w_VALSAL AND GETCAM(this.w_VALNAZ, I_DATSYS)<>0 
          this.w_IMPONI = VAL2MON(this.w_IMPONI,GETCAM(this.w_VALNAZ,I_DATSYS),1, I_DATSYS, g_PERVAL, GETVALUT(g_PERVAL, "VADECTOT"))
        endif
        this.w_CORVEN = this.w_CORVEN + this.w_IMPONI
        ENDSCAN
        CORVEN[this.w_NUMREG] = this.w_CORVEN
      endif
      * --- Se almeno esiste il Registro Corrispettivi...
      if this.w_NUMREG<>0
        * --- Legge i Saldi del Reg. IVA Corr./Ventilazione del Periodo Precedente
        * --- E Carica per ciascuna Aliquota le Aliquote IVA,Percentuale,%Indetr., Monte Acquisti(5)
        this.w_OLDPER = this.w_NUMPER - 1
        VQ_EXEC("QUERY\GSAR2QSI.VQR",this,"OLDVEN")
        * --- Leggo la valuta di Riferimento
        this.w_VALNAZ = g_PERVAL
        * --- Read from PRI_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRI_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2],.t.,this.PRI_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRCODVAL"+;
            " from "+i_cTable+" PRI_MAST where ";
                +"TR__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                +" and TRNUMPER = "+cp_ToStrODBC(this.w_OLDPER);
                +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRCODVAL;
            from (i_cTable) where;
                TR__ANNO = this.w_ANNO;
                and TRNUMPER = this.w_OLDPER;
                and TRTIPREG = this.w_TIPREG;
                and TRNUMREG = this.w_NUMREG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALNAZ = NVL(cp_ToDate(_read_.TRCODVAL),cp_NullValue(_read_.TRCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_VALNAZ = IIF(EMPTY(this.w_VALNAZ), g_PERVAL, this.w_VALNAZ)
        * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte gli Importi
        if this.w_VALNAZ<>this.w_VALSAL AND GETCAM(this.w_VALNAZ, I_DATSYS)<>0 
          SELECT OLDVEN
          if RECCOUNT()>0
            GO TOP
            SCAN
            REPLACE MACSTA WITH VAL2MON(NVL(MACSTA,0), GETCAM(this.w_VALNAZ, I_DATSYS), 1, I_DATSYS, g_perval , GETVALUT( g_perval , "VADECTOT"))
            REPLACE MACPRE WITH VAL2MON(NVL(MACPRE,0),GETCAM(this.w_VALNAZ, I_DATSYS), 1, I_DATSYS, g_perval , GETVALUT( g_perval , "VADECTOT"))
            REPLACE MACSEG WITH VAL2MON(NVL(MACSEG,0),GETCAM(this.w_VALNAZ, I_DATSYS), 1, I_DATSYS, g_perval , GETVALUT( g_perval , "VADECTOT"))
            REPLACE MACFAD WITH VAL2MON(NVL(MACFAD,0),GETCAM(this.w_VALNAZ, I_DATSYS), 1, I_DATSYS, g_perval , GETVALUT(g_perval , "VADECTOT"))
            REPLACE MACIND WITH VAL2MON(NVL(MACIND,0),GETCAM(this.w_VALNAZ, I_DATSYS), 1, I_DATSYS, g_perval , GETVALUT(g_perval , "VADECTOT"))
            ENDSCAN
          endif
        endif
        * --- Rilegge le Aliquote IVA Monte Acquisti per calcolare il M.Acq.Ventilazione
        * --- E le mette in UNION con quelle del Periodo Precedente
        SELECT CODIVA, PERIVA, PERIND, ;
        SUM(IMPSTA+IVASTA+IVISTA) AS MACSTA, SUM(IMPPRE+IVAPRE+IVIPRE) AS MACPRE, ;
        SUM(IMPSEG+IVASEG+IVISEG) AS MACSEG, SUM(IMPFAD+IVAFAD+IVIFAD) AS MACFAD, ;
        SUM(IMPIND+IVAIND+IVIIND) AS MACIND, "A" AS ORIGIN FROM CursElab ;
        WHERE TIPOPE="A" AND TIPREG="A" AND MACIVA="S" ;
        GROUP BY CODIVA, PERIVA, PERIND ;
        UNION ;
        SELECT CODIVA, PERIVA, PERIND, MACSTA, MACPRE, MACSEG, MACFAD, MACIND, "B" AS ORIGIN FROM OLDVEN ;
        INTO CURSOR VENT2
        * --- Quindi ragguppa i Valori Monte Acquisti per aliquota IVA
        SELECT CODIVA, PERIVA, PERIND, SUM(MACSTA) AS MACSTA, SUM(MACPRE) AS MACPRE, ;
        SUM(MACSEG) AS MACSEG, SUM(MACFAD) AS MACFAD, SUM (MACIND) AS MACIND,; 
 SUM(IIF(ORIGIN="A",MACSTA,(MACSTA+MACPRE+MACSEG-MACFAD+MACIND))) AS TOTVEN FROM VENT2 ;
        GROUP BY CODIVA, PERIVA, PERIND ORDER BY CODIVA ;
        INTO CURSOR APPOVENT
        SELECT APPOVENT
        GO TOP
        SUM TOTVEN TO this.w_TOTVEN FOR (TOTVEN)>=0
        SUM TOTVEN TO this.w_TOTVENP
        if USED("VENT2")
          SELECT VENT2
          USE
        endif
        if this.w_TOTVENP=0
          * --- Errore! se ci sono Registrazioni di Corr./Ventilazione devono esistere anche le relative Reg. Acquisto
          * --- Che aggiornano il Monte Acquisti
        else
          if USED("APPOVENT")
            this.w_APPO = 0
            SELECT APPOVENT
            GO TOP
            SCAN FOR NOT EMPTY(NVL(CODIVA," "))
            * --- Azzera Variabili di Appoggio
            this.w_IMPSTA = 0
            this.w_IMPSEG = 0
            this.w_IMPIND = 0
            this.w_IVASTA = 0
            this.w_IVASEG = 0
            this.w_IVAIND = 0
            this.w_IVISTA = 0
            this.w_IVISEG = 0
            this.w_IVIIND = 0
            this.w_IMPPRE = 0
            this.w_IMPFAD = 0
            this.w_IVAPRE = 0
            this.w_IVAFAD = 0
            this.w_IVIPRE = 0
            this.w_IVIFAD = 0
            this.w_CODIVA = CODIVA
            this.w_PERIVA = NVL(PERIVA, 0)
            this.w_PERIND = NVL(PERIND, 0)
            * --- Importi per Monte Acquisti
            this.w_MACSTA = NVL(MACSTA, 0)
            this.w_MACFAD = NVL(MACFAD, 0)
            this.w_MACIND = NVL(MACIND, 0)
            if this.w_MACSTA>0
              this.w_APPO1 = cp_ROUND((this.w_CORVEN*(this.w_MACSTA)/this.w_TOTVEN), this.w_PVLSAL)
              this.w_IMPSTA = cp_ROUND(this.w_APPO1/(1+(this.w_PERIVA/100)), this.w_PVLSAL)
              this.w_IVASTA = this.w_APPO1 - this.w_IMPSTA
              this.w_IVISTA = cp_ROUND((this.w_IVASTA * this.w_PERIND) / 100, this.w_PVLSAL)
              this.w_IVASTA = this.w_IVASTA - this.w_IVISTA
              * --- Per Gestione Resti
              this.w_APPO = this.w_APPO + (this.w_IMPSTA + this.w_IVISTA + this.w_IVASTA)
            endif
            this.w_MACPRE = NVL(MACPRE, 0)
            if this.w_MACPRE>0
              this.w_APPO1 = cp_ROUND((this.w_CORVEN*this.w_MACPRE)/this.w_TOTVEN, this.w_PVLSAL)
              this.w_IVAPRE = cp_ROUND(this.w_APPO1 - (this.w_APPO1/(1+(this.w_PERIVA/100))), this.w_PVLSAL)
              this.w_IVIPRE = cp_ROUND((this.w_IVAPRE * this.w_PERIND) / 100, this.w_PVLSAL)
              this.w_IVAPRE = this.w_IVAPRE - this.w_IVIPRE
              this.w_IMPPRE = this.w_APPO1 - (this.w_IVAPRE+this.w_IVIPRE)
            endif
            this.w_MACSEG = NVL(MACSEG, 0)
            if this.w_MACSEG>0
              this.w_APPO1 = cp_ROUND((this.w_CORVEN*this.w_MACSEG)/this.w_TOTVEN, this.w_PVLSAL)
              this.w_IVASEG = cp_ROUND(this.w_APPO1 - (this.w_APPO1/(1+(this.w_PERIVA/100))), this.w_PVLSAL)
              this.w_IVISEG = cp_ROUND((this.w_IVASEG * this.w_PERIND) / 100, this.w_PVLSAL)
              this.w_IVASEG = this.w_IVASEG - this.w_IVISEG
              this.w_IMPSEG = this.w_APPO1 - (this.w_IVASEG+this.w_IVISEG)
            endif
            this.w_MACFAD = 0
            this.w_MACIND = 0
            * --- Scrive il Cursore di Elaborazione
            INSERT INTO CursElab (TIPOPE, TIPREG, NUMREG, CODIVA, PERIVA, PERIND, MACIVA, ;
            IMPSTA, IVASTA, IVISTA, MACSTA, IMPPRE, IVAPRE, IVIPRE, MACPRE, ;
            IMPSEG, IVASEG, IVISEG, MACSEG, IMPFAD, IVAFAD, IVIFAD, MACFAD, ;
            IMPIND, IVAIND, IVIIND, MACIND,FLVAFF) ;
            VALUES ("A", this.w_TIPREG, this.w_NUMREG, this.w_CODIVA, this.w_PERIVA, this.w_PERIND, "S", ;
            this.w_IMPSTA, this.w_IVASTA, this.w_IVISTA, this.w_MACSTA, this.w_IMPPRE, this.w_IVAPRE, this.w_IVIPRE, this.w_MACPRE, ;
            this.w_IMPSEG, this.w_IVASEG, this.w_IVISEG, this.w_MACSEG, this.w_IMPFAD, this.w_IVAFAD, this.w_IVIFAD, this.w_MACFAD, ;
            this.w_IMPIND, this.w_IVAIND, this.w_IVIIND, this.w_MACIND,this.w_FLVAFF)
            SELECT APPOVENT
            ENDSCAN
            * --- Eventuale Resto (da sommare sulla prima Aliquota)
            this.w_RESVEN = this.w_CORVEN - this.w_APPO
            if this.w_FLDEFI="A"
              * --- Per elaborazione Acconto IVA la variabile RESVEN serve all'interno del batch GSCG_BDI
              this.oParentObject.w_RESVEN1=this.w_RESVEN
            endif
          endif
        endif
      endif
      if USED("APPOVENT")
        SELECT APPOVENT
        USE
      endif
      if USED("OLDVEN")
        SELECT OLDVEN
        USE
      endif
      if USED("REGIVA")
        SELECT REGIVA
        USE
      endif
        select _Curs_ATTIDETT
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='PRI_DETT'
    this.cWorkTables[4]='PRI_MAST'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='CONTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_CONTAREG')
      use in _Curs_CONTAREG
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_PRI_MAST')
      use in _Curs_PRI_MAST
    endif
    if used('_Curs_PRI_MAST')
      use in _Curs_PRI_MAST
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
