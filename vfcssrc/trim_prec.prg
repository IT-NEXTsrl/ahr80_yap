* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: trim_prec                                                       *
*              Date trimestre precedente (IVA trasportatori)                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-31                                                      *
* Last revis.: 2002-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("ttrim_prec",oParentObject)
return(i_retval)

define class ttrim_prec as StdBatch
  * --- Local variables
  w_MESE = 0
  w_ANNO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MESE = MONTH(this.oParentObject.w_DATAINIZ)
    this.w_ANNO = YEAR(this.oParentObject.w_DATAINIZ)
    do case
      case (this.w_MESE = 12) OR (this.w_MESE = 11) OR (this.w_MESE = 10)
        this.oParentObject.w_DATAINIP = cp_CharToDate("01-07-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
        this.oParentObject.w_DATAFINP = cp_CharToDate("30-09-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
      case (this.w_MESE = 9) OR (this.w_MESE = 8) OR (this.w_MESE = 7)
        this.oParentObject.w_DATAINIP = cp_CharToDate("01-04-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
        this.oParentObject.w_DATAFINP = cp_CharToDate("30-06-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
      case (this.w_MESE = 6) OR (this.w_MESE = 5) OR (this.w_MESE = 4)
        this.oParentObject.w_DATAINIP = cp_CharToDate("01-01-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
        this.oParentObject.w_DATAFINP = cp_CharToDate("31-03-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
      case (this.w_MESE = 3) OR (this.w_MESE = 2) OR (this.w_MESE = 1)
        this.w_ANNO = this.w_ANNO-1
        this.oParentObject.w_DATAINIP = cp_CharToDate("01-10-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
        this.oParentObject.w_DATAFINP = cp_CharToDate("31-12-" + RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)), 4))
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
