* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bat                                                        *
*              Controlli attivit�                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-07                                                      *
* Last revis.: 2017-12-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bat",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsar_bat as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_OK = .f.
  w_TROV = .f.
  w_MESS = space(10)
  w_APPO = space(5)
  w_CONTRCOD = space(5)
  w_TRIG = 0
  w_CONT = 0
  w_CONTV = 0
  w_CONTVS = 0
  w_CONTA = 0
  w_CONTR = 0
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_PADRE = .NULL.
  w_FIGLIO = .NULL.
  w_DATESTAMPA = space(1)
  w_CHIELD = .NULL.
  w_DATULTSTA = ctod("  /  /  ")
  pCODATT = space(5)
  w_CONT1 = 0
  * --- WorkFile variables
  IVA_PERI_idx=0
  PRI_MAST_idx=0
  ATTIMAST_idx=0
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla Cancellazione Righe registri associati all'attivita' (da GSAR_MAT)
    this.w_CONTRCOD = " "
    this.w_OK = .T.
    this.w_TROV = .F.
    this.w_MESS = " "
    this.w_TRIG = 0
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
    do case
      case this.pTipOpe="D"
        * --- Cancellazione Intera Attivita'
        if NOT EMPTY(this.oParentObject.w_ATCODATT)
          * --- Select from IVA_PERI
          i_nConn=i_TableProp[this.IVA_PERI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select VPCODATT  from "+i_cTable+" IVA_PERI ";
                +" where VPKEYATT="+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+"";
                 ,"_Curs_IVA_PERI")
          else
            select VPCODATT from (i_cTable);
             where VPKEYATT=this.oParentObject.w_ATCODATT;
              into cursor _Curs_IVA_PERI
          endif
          if used('_Curs_IVA_PERI')
            select _Curs_IVA_PERI
            locate for 1=1
            do while not(eof())
            this.w_APPO = NVL(_Curs_IVA_PERI.VPCODATT, "     ")
            if this.w_OK=.T. AND this.w_APPO=this.oParentObject.w_ATCODATT
              this.w_OK = .F.
              this.w_MESS = "Per l'attivit� sono presenti delle dichiarazioni IVA; impossibile eliminare"
            endif
              select _Curs_IVA_PERI
              continue
            enddo
            use
          endif
        endif
      case this.pTipOpe="R"
        if NOT EMPTY(this.oParentObject.w_ATCODATT) AND NOT EMPTY(this.oParentObject.w_ATTIPREG) AND NOT EMPTY(this.oParentObject.w_ATNUMREG) 
          * --- Select from PRI_MAST
          i_nConn=i_TableProp[this.PRI_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2],.t.,this.PRI_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRI_MAST ";
                +" where TRTIPREG="+cp_ToStrODBC(this.oParentObject.w_ATTIPREG)+" AND TRNUMREG="+cp_ToStrODBC(this.oParentObject.w_ATNUMREG)+"";
                 ,"_Curs_PRI_MAST")
          else
            select * from (i_cTable);
             where TRTIPREG=this.oParentObject.w_ATTIPREG AND TRNUMREG=this.oParentObject.w_ATNUMREG;
              into cursor _Curs_PRI_MAST
          endif
          if used('_Curs_PRI_MAST')
            select _Curs_PRI_MAST
            locate for 1=1
            do while not(eof())
            this.w_TROV = .T.
            if NVL(_Curs_PRI_MAST.TRFLSTAM, " ")="S"
              this.w_OK = .F.
              this.w_MESS = "Per alcuni registri cancellati, � gi� stata stampata la liquidazione IVA; impossibile eliminare"
            endif
              select _Curs_PRI_MAST
              continue
            enddo
            use
          endif
        endif
        if this.w_OK=.T. AND this.w_TROV=.T.
          * --- Trovati Saldi IVA
          this.w_OK = ah_YesNo("Per alcuni registri in cancellazione, sono presenti saldi; proseguo comunque?")
          this.w_MESS = "Operazione abbandonata"
        endif
      case this.pTipOpe="I"
        * --- Read from ATTIMAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATCODATT"+;
            " from "+i_cTable+" ATTIMAST where ";
                +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_ATCODATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATCODATT;
            from (i_cTable) where;
                ATCODATT = this.oParentObject.w_ATCODATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONTRCOD = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_ATCODATT=this.w_CONTRCOD
          this.w_MESS = "Chiave gi� utilizzata "
          this.w_OK = .F.
        else
          * --- Read from ATTIMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATCODATT"+;
              " from "+i_cTable+" ATTIMAST where ";
                  +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_ATCODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATCODATT;
              from (i_cTable) where;
                  ATCODATT = this.oParentObject.w_ATCODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONTRCOD = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_ATCODATT=this.w_CONTRCOD
            this.w_MESS = "Chiave gi� utilizzata "
            this.w_OK = .F.
          else
            this.w_PADRE.FirstRow()     
            * --- Cicla sulle Righe del Documento
            do while Not this.w_PADRE.Eof_Trs()
              if this.w_PADRE.Get("T_atnumreg")<>0
                this.w_TRIG = this.w_TRIG + 1
              endif
              this.w_PADRE.NextRow()     
            enddo
            if this.w_OK=.T. AND this.w_TRIG=0 
              this.w_MESS = "Inserire almeno una riga di dettaglio"
              this.w_OK = .F.
            endif
          endif
        endif
      case this.pTipOpe="F"
        this.oParentObject.w_CTRL = 0
        this.w_CONT = 0
        this.w_PADRE.FirstRow()     
        * --- Cicla sulle Righe del Documento
        do while Not this.w_PADRE.Eof_Trs()
          this.w_CONT = this.w_CONT+this.w_PADRE.Get("t_ATFLREGI")
          this.w_CONT1 = this.w_CONT1 +this.w_PADRE.Get("t_ATLIMCOM")
          this.w_PADRE.NextRow()     
        enddo
        if this.w_CONT>1 or this.w_CONT1>1
          if this.w_CONT>1 
            this.oParentObject.w_CTRL = 1
          else
            this.oParentObject.w_CTRL = 2
          endif
          this.w_OK = .F.
        endif
        * --- Controllo che non sia presente lo stesso Registro Iva in pi� attivit�
        if this.w_OK
          this.oParentObject.w_PIUATT = .F.
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_TIPREG = IIF(this.w_PADRE.Get("t_ATTIPREG")=1, "V", IIF(this.w_PADRE.Get("t_ATTIPREG")=2, "A", IIF(this.w_PADRE.Get("t_ATTIPREG")=3, "C", 4)))
            this.w_NUMREG = this.w_PADRE.Get("t_ATNUMREG")
            * --- Select from ATTIDETT
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select ATCODATT  from "+i_cTable+" ATTIDETT ";
                  +" where ATTIPREG="+cp_ToStrODBC(this.w_TIPREG)+" AND ATNUMREG="+cp_ToStrODBC(this.w_NUMREG)+" AND ATCODATT<>"+cp_ToStrODBC(this.oParentObject.w_ATCODATT)+"";
                   ,"_Curs_ATTIDETT")
            else
              select ATCODATT from (i_cTable);
               where ATTIPREG=this.w_TIPREG AND ATNUMREG=this.w_NUMREG AND ATCODATT<>this.oParentObject.w_ATCODATT;
                into cursor _Curs_ATTIDETT
            endif
            if used('_Curs_ATTIDETT')
              select _Curs_ATTIDETT
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_PIUATT = .T.
              this.w_OK = .F.
                select _Curs_ATTIDETT
                continue
              enddo
              use
            endif
            this.w_PADRE.NextRow()     
          enddo
        endif
        * --- Controlli sul regime del margine
        if this.w_OK
          this.oParentObject.w_CHKMARG = .F.
          if this.oParentObject.w_ATREGMAR = "S"
            this.w_DATESTAMPA = "S"
            this.w_DATULTSTA = CTOD("  -  -    ")
            this.pCODATT = this.oParentObject.w_ATCODATT
            this.w_FIGLIO = this.w_PADRE.GSAR_MPM
            this.w_FIGLIO.LinkPCClick(.T.)     
            this.w_FIGLIO = this.w_PADRE.GSAR_MPM
            this.w_FIGLIO.cnt.MarkPos()     
            this.w_FIGLIO.cnt.FirstRow()     
            do while Not this.w_FIGLIO.cnt.Eof_Trs()
              if this.w_FIGLIO.cnt.FullRow()
                this.w_DATULTSTA = iif(this.w_FIGLIO.cnt.Get("t_MADATSTA") > this.w_DATULTSTA, this.w_FIGLIO.cnt.Get("t_MADATSTA") ,this.w_DATULTSTA)
              endif
              this.w_FIGLIO.cnt.NextRow()     
            enddo
            this.w_FIGLIO.cnt.RePos()     
            this.w_PADRE.FirstRow()     
            this.w_CONTV = 0
            this.w_CONTA = 0
            this.w_CONTR = 0
            this.w_CONTVS = 0
            do while Not this.w_PADRE.Eof_Trs()
              * --- Conto il numero di registri tipo Vendite con check R.M. attivo
              this.w_CONTV = this.w_CONTV + iif(this.w_PADRE.Get("t_ATMARREG") = 1 and this.w_PADRE.Get("t_ATTIPREG") = 1,1,0)
              * --- Conto il numero di registri tipo Vendite con check R.M. non attivo
              this.w_CONTVS = this.w_CONTVS + iif(this.w_PADRE.Get("t_ATMARREG") = 0 and this.w_PADRE.Get("t_ATTIPREG") = 1,1,0)
              * --- Conto il numero di registri tipo Acquisti con check R.M. attivo
              this.w_CONTA = this.w_CONTA + iif(this.w_PADRE.Get("t_ATMARREG") = 1 and this.w_PADRE.Get("t_ATTIPREG") = 2,1,0)
              * --- Conto il numero di registri tipo Corrispettivi con check R.M. non attivo
              this.w_CONTR = this.w_CONTR + iif(this.w_PADRE.Get("t_ATMARREG") = 0 and this.w_PADRE.Get("t_ATTIPREG") >= 3,1,0)
              * --- Controllo la congruit� fra le date di stampa in definitiva dei registri con la data di ultima stampa del margine
              if this.w_PADRE.Get("t_ATMARREG") = 1 and this.w_PADRE.Get("t_ATDATSTA") < this.w_DATULTSTA and not(empty(this.w_DATULTSTA))
                * --- Situazione non congrua, ho data di stampa in definitvo del margine maggiore della data di ultima stampa del registro
                this.w_DATESTAMPA = "N"
                this.w_TIPREG = this.w_PADRE.Get("t_ATTIPREG")
                this.w_NUMREG = this.w_PADRE.Get("t_ATNUMREG")
              endif
              this.w_PADRE.NextRow()     
            enddo
            if this.w_CONTV = 0 or this.w_CONTA = 0 
              this.oParentObject.w_CHKMARG = .T.
              this.w_OK = .F.
              Ah_ErrorMsg("Attivit� con check attivo sul regime del margine: deve essere presente almeno un registro iva acquisti ed un registro iva vendite con check regime del margine attivo")
            endif
            if this.w_CONTR = 0 and this.w_CONTVS = 0
              this.oParentObject.w_CHKMARG = .T.
              this.w_OK = .F.
              Ah_ErrorMsg("Attivit� con check attivo sul regime del margine: deve essere presente almeno un registro iva di tipo corrispettivi o vendite con il check regime del margine disattivo")
            endif
            if this.w_DATESTAMPA = "N"
              this.oParentObject.w_CHKMARG = .T.
              this.w_OK = .F.
              Ah_ErrorMsg("La data di ultima stampa del registro IVA tipo %1 numero %2 non pu� essere inferiore alla data di ultima stampa del margine",48,"",iif(this.w_TIPREG=1,"Vendite",iif(this.w_TIPREG=2,"Acquisti","Corrispettivi")),str(this.w_NUMREG,2))
            endif
          else
            this.w_PADRE.FirstRow()     
            this.w_CONT = 0
            do while Not this.w_PADRE.Eof_Trs()
              * --- Verifico che non vi siano registri con il check R.M. attivo su attivit� che non gestice il Regime del Margine
              this.w_CONT = this.w_CONT + iif(this.w_PADRE.Get("t_ATMARREG") = 1,1,0)
              this.w_PADRE.NextRow()     
            enddo
            if this.w_CONT > 0 
              this.oParentObject.w_CHKMARG = .T.
              this.w_OK = .F.
              Ah_ErrorMsg("Sono presenti registri con flag 'Regime del margine' attivo, su attivit� che non lo gestisce.%0Attivit� non coerente")
            endif
          endif
        endif
    endcase
    this.w_PADRE.RePos()     
    if this.w_OK=.F.
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat(this.w_MESS)
    endif
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='IVA_PERI'
    this.cWorkTables[2]='PRI_MAST'
    this.cWorkTables[3]='ATTIMAST'
    this.cWorkTables[4]='ATTIDETT'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_IVA_PERI')
      use in _Curs_IVA_PERI
    endif
    if used('_Curs_PRI_MAST')
      use in _Curs_PRI_MAST
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
