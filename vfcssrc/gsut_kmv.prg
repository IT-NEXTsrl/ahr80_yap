* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kmv                                                        *
*              Valore propriet�                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-12                                                      *
* Last revis.: 2015-12-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kmv",oParentObject))

* --- Class definition
define class tgsut_kmv as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 436
  Height = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-09"
  HelpContextID=199836521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  ELE_EXPR_IDX = 0
  cPrg = "gsut_kmv"
  cComment = "Valore propriet�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VALORE = space(254)
  w_EEDESCRI = space(40)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_EE__EXPR = space(254)
  w_EE_ESCLU = space(1)
  w_SELSTART = 0
  w_SELLENGTH = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kmvPag1","gsut_kmv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVALORE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ELE_EXPR'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALORE=space(254)
      .w_EEDESCRI=space(40)
      .w_TIPO=space(1)
      .w_EE__EXPR=space(254)
      .w_EE_ESCLU=space(1)
      .w_SELSTART=0
      .w_SELLENGTH=0
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_EEDESCRI))
          .link_1_5('Full')
        endif
        .w_TIPO = ''
          .DoRTCalc(4,5,.f.)
        .w_SELSTART = 1
    endwith
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kmv
    *-- Aggiornamento variabile w_VALORE
    *-- Se vengo dai gadget la combo tabellare va rieseguita
    Local oCtrl
    Do Case
        Case Upper(This.oParentObject.cPrg) = 'GSUT_MGA'
          This.w_Valore = Alltrim(This.oParentObject.w_GAVALORE)
          This.w_EE_ESCLU = 'G'
          m.oCtrl = This.GetCtrl("w_EEDESCRI")
          m.oCtrl.Popola()
        Case Upper(This.oParentObject.cPrg) = 'GSUT_MGC'
          This.w_Valore = Alltrim(This.oParentObject.w_GCVALORE)
          This.w_EE_ESCLU = 'G'
          m.oCtrl = This.GetCtrl("w_EEDESCRI")
          m.oCtrl.Popola()
        Otherwise
          This.w_Valore = Alltrim(This.oParentObject.w_PRVALVAR)
    EndCase
    m.oCtrl = .Null.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gsut_kmv
    *-- Aggiornamento variabili padre
    Do Case
        Case Upper(This.oParentObject.cPrg) = 'GSUT_MGA'
          This.oParentObject.w_GAVALORE = Alltrim(This.w_Valore)
        Case Upper(This.oParentObject.cPrg) = 'GSUT_MGC'
          This.oParentObject.w_GCVALORE = Alltrim(This.w_Valore)
        Otherwise
          This.oParentObject.w_PRVALVAR = Alltrim(This.w_Valore)
    EndCase
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPO<>.w_TIPO
          .link_1_5('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UFFNRGWKTM()
    with this
          * --- Aggiungo espressione
          .w_VALORE = STUFF( .w_VALORE, .w_SELSTART, .w_SELLENGTH, Alltrim(.w_EE__EXPR))
          .w_SELSTART = .w_SELSTART + LEN(Alltrim(.w_EE__EXPR))
          .w_SELLENGTH = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_kmv
    If Lower(cEvent) = 'w_valore lostfocus'
       *-- Valorizzo il salvataggio della selezione del testo
       Local oCtrl
       m.oCtrl = This.GetCtrl("w_VALORE")
       This.w_SELSTART = m.oCtrl.SelStart+1
       This.w_SELLENGTH = m.oCtrl.SelLength
       m.oCtrl = .NULL.
    EndIf
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("AddExpr")
          .Calculate_UFFNRGWKTM()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_kmv
    If Lower(cEvent)='w_tipo changed'
        Local oCtrl, l_eVal
        m.oCtrl= This.GetCtrl("w_EEDESCRI")
        m.oCtrl.Popola()
        m.oCtrl=.Null.
        If !Empty(This.w_EEDESCRI) And Type(This.w_EE__EXPR)<>This.w_Tipo
          *-- L'espressione non varia solo se � contenuta anche nel nuovo tipo ('Tutti')
          This.w_EE__EXPR = ''
          This.w_EEDESCRI = ''
        Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EEDESCRI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELE_EXPR_IDX,3]
    i_lTable = "ELE_EXPR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_EXPR_IDX,2], .t., this.ELE_EXPR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_EXPR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EEDESCRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_AEE',True,'ELE_EXPR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EEDESCRI like "+cp_ToStrODBC(trim(this.w_EEDESCRI)+"%");

          i_ret=cp_SQL(i_nConn,"select EEDESCRI,EE__EXPR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EEDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EEDESCRI',trim(this.w_EEDESCRI))
          select EEDESCRI,EE__EXPR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EEDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EEDESCRI)==trim(_Link_.EEDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EEDESCRI) and !this.bDontReportError
            deferred_cp_zoom('ELE_EXPR','*','EEDESCRI',cp_AbsName(oSource.parent,'oEEDESCRI_1_5'),i_cWhere,'GSUT_AEE',"Elenco espressioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EEDESCRI,EE__EXPR";
                     +" from "+i_cTable+" "+i_lTable+" where EEDESCRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EEDESCRI',oSource.xKey(1))
            select EEDESCRI,EE__EXPR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EEDESCRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EEDESCRI,EE__EXPR";
                   +" from "+i_cTable+" "+i_lTable+" where EEDESCRI="+cp_ToStrODBC(this.w_EEDESCRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EEDESCRI',this.w_EEDESCRI)
            select EEDESCRI,EE__EXPR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EEDESCRI = NVL(_Link_.EEDESCRI,space(40))
      this.w_EE__EXPR = NVL(_Link_.EE__EXPR,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_EEDESCRI = space(40)
      endif
      this.w_EE__EXPR = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELE_EXPR_IDX,2])+'\'+cp_ToStr(_Link_.EEDESCRI,1)
      cp_ShowWarn(i_cKey,this.ELE_EXPR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EEDESCRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVALORE_1_1.value==this.w_VALORE)
      this.oPgFrm.Page1.oPag.oVALORE_1_1.value=this.w_VALORE
    endif
    if not(this.oPgFrm.Page1.oPag.oEEDESCRI_1_5.RadioValue()==this.w_EEDESCRI)
      this.oPgFrm.Page1.oPag.oEEDESCRI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_6.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsut_kmv
      If i_bRes
         this.oParentObject.SetUpdateRow()
      Endif   
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPO = this.w_TIPO
    return

enddefine

* --- Define pages as container
define class tgsut_kmvPag1 as StdContainer
  Width  = 432
  height = 221
  stdWidth  = 432
  stdheight = 221
  resizeXpos=253
  resizeYpos=94
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVALORE_1_1 as StdField with uid="VCSKOUXNNX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_VALORE", cQueryName = "VALORE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Valore propriet�",;
    HelpContextID = 243974998,;
   bGlobalFont=.t.,;
    Height=126, Width=417, Left=8, Top=40, InputMask=replicate('X',254)


  add object oBtn_1_2 as StdButton with uid="MBXAIGGBZE",left=327, top=171, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 199807770;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="ISDICPRYHO",left=377, top=171, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 159544854;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oEEDESCRI_1_5 as StdExprTableCombo with uid="TAXJMMMZKF",rtseq=2,rtrep=.f.,left=213,top=12,width=178,height=22;
    , ToolTipText = "Espressione da inserire";
    , HelpContextID = 210781839;
    , cFormVar="w_EEDESCRI",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="ELE_EXPR";
    , cTable='query\gsut_kmv.vqr',cKey='EEDESCRI',cValue='EEDESCRI',cOrderBy='EEDESCRI',xDefault=space(40);
  , bGlobalFont=.t.


  func oEEDESCRI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oEEDESCRI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oTIPO_1_6 as StdCombo with uid="ZYJHWLHOAR",value=6,rtseq=3,rtrep=.f.,left=41,top=12,width=83,height=22;
    , ToolTipText = "Tipo dell'espressione";
    , HelpContextID = 194311370;
    , cFormVar="w_TIPO",RowSource=""+"Booleano,"+"Carattere,"+"Data,"+"Numerico,"+"Datetime,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_6.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    iif(this.value =4,'N',;
    iif(this.value =5,'T',;
    iif(this.value =6,'',;
    space(1))))))))
  endfunc
  func oTIPO_1_6.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_6.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='B',1,;
      iif(this.Parent.oContained.w_TIPO=='C',2,;
      iif(this.Parent.oContained.w_TIPO=='D',3,;
      iif(this.Parent.oContained.w_TIPO=='N',4,;
      iif(this.Parent.oContained.w_TIPO=='T',5,;
      iif(this.Parent.oContained.w_TIPO=='',6,;
      0))))))
  endfunc


  add object oBtn_1_8 as StdButton with uid="FJLLXYQEJW",left=398, top=10, width=27,height=25,;
    CpPicture="BMP\PLUS.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inserire l'espressione nel testo";
    , HelpContextID = 199400186;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("AddExpr")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_EEDESCRI))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="OULJSUPCKO",left=8, top=171, width=48,height=45,;
    CpPicture="BMP\RUN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per testare l'espressione";
    , HelpContextID = 191736010;
    , Caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        EvalExpr(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_VALORE))
      endwith
    endif
  endfunc

  add object oStr_1_9 as StdString with uid="OPSOBGJTJD",Visible=.t., Left=0, Top=16,;
    Alignment=1, Width=38, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="EKBVNHPZZT",Visible=.t., Left=127, Top=16,;
    Alignment=1, Width=81, Height=18,;
    Caption="Espressione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kmv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kmv
*-- Combo tabellare sull'elenco delle espressioni
define class StdExprTableCombo as StdTableCombo

  Proc Init()
    DoDefault()
    This.Popola()
  EndProc

  Proc Popola()
		Local i_curs,i_bCharKey, i_fk,i_fd, l_nRem
		i_curs=Sys(2015)
		*--- Inizio richiesta parametri combotabellare
		vq_exec(This.cTable,Thisform,i_curs)
		i_fk=This.cKey
		i_fd=This.cValue
		If Used(i_curs)
			Select (i_curs)
			l_nRem = This.nValues
			Do While l_nRem>0
			  This.RemoveItem(1)
			  l_nRem = l_nRem-1
			EndDO
			If Reccount()=0
				This.AddItem(This.cDescEmptyElement)
				This.nValues=1
				Dimension This.combovalues[1]
				This.combovalues[1] = This.xEmptyKey
			Else
				This.nValues=Reccount()&&+1
				Dimension This.combovalues[MAX(1,this.nValues)]
				i_bCharKey=Type(i_fk)='C'
				*This.AddItem(This.cDescEmptyElement)
				*--- Zucchetti Aulla
				*IF ISNULL(This.xEmptyKey)
				*	This.combovalues[1] = Iif(i_bCharKey,Space(1),0)
				*Else
				*	This.combovalues[1] = This.xEmptyKey
				*ENDIF
				*--- Zucchetti Aulla
				Do While !Eof()
					This.AddItem(Iif(Type(i_fd)='C',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
					If i_bCharKey
						*This.combovalues[recno()+1]=Trim(&i_fk)
				    This.combovalues[recno()]=Trim(&i_fk)
					Else
						*This.combovalues[recno()+1]=&i_fk
				    This.combovalues[recno()]=&i_fk
					Endif
					Skip
				Enddo
				Use
			Endif
		Endif
  Endproc
  
Enddefine

*-- Valutazione espressione in w_VALORE
Proc EvalExpr(oObj)
  Local l_OldErr, bErr, l_Msg, l_Macro, l_Expr, l_SubStr
	m.bErr = .F.
	m.l_OldErr = On('Error')
	On Error m.bErr = .T.
  m.l_Expr = Alltrim(oObj.w_VALORE)
  m.l_Expr = Alltrim(StrTran(m.l_Expr, '#CONST', '', 1, 1, 1))
  m.l_Expr = Alltrim(StrTran(m.l_Expr, '#LAST', '', 1, 1, 1))
  m.l_Expr = StrTran(m.l_Expr, ' #HELP ', ' #help ', 1, 1, 1)
  If At('#help ', m.l_Expr) <> 0
    m.l_Expr = Left( m.l_Expr, At('#help ', m.l_Expr)-1)
  Endif
  l_Macro = Eval(m.l_Expr)
	On Error &l_OldErr
	If m.bErr
    m.l_Msg = Ah_MsgFormat('Espressione non corretta o al momento non valutabile:%0%1',Alltrim(Message()))
	Else
    m.l_Msg = Alltrim(Tran(l_Macro))
  Endif
  Ah_ErrorMsg(m.l_Msg, Iif(m.bErr, '!','i'))
Endproc


* --- Fine Area Manuale
