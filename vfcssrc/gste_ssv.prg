* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_ssv                                                        *
*              Stampa scadenze diverse                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_6]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_ssv",oParentObject))

* --- Class definition
define class tgste_ssv as StdForm
  Top    = 44
  Left   = 105

  * --- Standard Properties
  Width  = 500
  Height = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=90846313
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  SCA_VARI_IDX = 0
  cPrg = "gste_ssv"
  cComment = "Stampa scadenze diverse"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHIAVE = 0
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_CODICE1 = space(10)
  w_CODICE2 = space(10)
  w_DES1 = space(35)
  w_DES2 = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_ssvPag1","gste_ssv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='SCA_VARI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_ssv
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHIAVE=0
      .w_TIPO=space(1)
      .w_CODICE1=space(10)
      .w_CODICE2=space(10)
      .w_DES1=space(35)
      .w_DES2=space(35)
        .w_CHIAVE = -1
        .w_TIPO = 'C'
        .w_CODICE1 = ''
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODICE1))
          .link_1_5('Full')
        endif
        .w_CODICE2 = ''
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODICE2))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CODICE1 = ''
          .link_1_5('Full')
        endif
        if .o_TIPO<>.w_TIPO
            .w_CODICE2 = ''
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE1
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_lTable = "SCA_VARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2], .t., this.SCA_VARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASC',True,'SCA_VARI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCCODICE like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");
                   +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);

          i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCCODSEC,SCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCCODSEC',this.w_CHIAVE;
                     ,'SCCODICE',trim(this.w_CODICE1))
          select SCCODSEC,SCCODICE,SCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCCODSEC,SCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.SCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('SCA_VARI','*','SCCODSEC,SCCODICE',cp_AbsName(oSource.parent,'oCODICE1_1_5'),i_cWhere,'GSAR_ASC',"Scadenze varie",'GSTE_SSV.SCA_VARI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHIAVE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il primo codice � pi� grande del secondo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODSEC',oSource.xKey(1);
                       ,'SCCODICE',oSource.xKey(2))
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(this.w_CODICE1);
                   +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODSEC',this.w_CHIAVE;
                       ,'SCCODICE',this.w_CODICE1)
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.SCCODICE,space(10))
      this.w_DES1 = NVL(_Link_.SCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(10)
      endif
      this.w_DES1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_codice2) or .w_codice1<=.w_codice2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il primo codice � pi� grande del secondo")
        endif
        this.w_CODICE1 = space(10)
        this.w_DES1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])+'\'+cp_ToStr(_Link_.SCCODSEC,1)+'\'+cp_ToStr(_Link_.SCCODICE,1)
      cp_ShowWarn(i_cKey,this.SCA_VARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SCA_VARI_IDX,3]
    i_lTable = "SCA_VARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2], .t., this.SCA_VARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASC',True,'SCA_VARI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCCODICE like "+cp_ToStrODBC(trim(this.w_CODICE2)+"%");
                   +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);

          i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCCODSEC,SCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCCODSEC',this.w_CHIAVE;
                     ,'SCCODICE',trim(this.w_CODICE2))
          select SCCODSEC,SCCODICE,SCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCCODSEC,SCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE2)==trim(_Link_.SCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE2) and !this.bDontReportError
            deferred_cp_zoom('SCA_VARI','*','SCCODSEC,SCCODICE',cp_AbsName(oSource.parent,'oCODICE2_1_6'),i_cWhere,'GSAR_ASC',"Scadenze varie",'GSTE_SSV.SCA_VARI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHIAVE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il primo codice � pi� grande del secondo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODSEC',oSource.xKey(1);
                       ,'SCCODICE',oSource.xKey(2))
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODSEC,SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(this.w_CODICE2);
                   +" and SCCODSEC="+cp_ToStrODBC(this.w_CHIAVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODSEC',this.w_CHIAVE;
                       ,'SCCODICE',this.w_CODICE2)
            select SCCODSEC,SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE2 = NVL(_Link_.SCCODICE,space(10))
      this.w_DES2 = NVL(_Link_.SCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE2 = space(10)
      endif
      this.w_DES2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_codice1) or .w_codice1<=.w_codice2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il primo codice � pi� grande del secondo")
        endif
        this.w_CODICE2 = space(10)
        this.w_DES2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SCA_VARI_IDX,2])+'\'+cp_ToStr(_Link_.SCCODSEC,1)+'\'+cp_ToStr(_Link_.SCCODICE,1)
      cp_ShowWarn(i_cKey,this.SCA_VARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_2.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_5.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_5.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE2_1_6.value==this.w_CODICE2)
      this.oPgFrm.Page1.oPag.oCODICE2_1_6.value=this.w_CODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDES1_1_7.value==this.w_DES1)
      this.oPgFrm.Page1.oPag.oDES1_1_7.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page1.oPag.oDES2_1_8.value==this.w_DES2)
      this.oPgFrm.Page1.oPag.oDES2_1_8.value=this.w_DES2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_codice2) or .w_codice1<=.w_codice2)  and not(empty(.w_CODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo codice � pi� grande del secondo")
          case   not(empty(.w_codice1) or .w_codice1<=.w_codice2)  and not(empty(.w_CODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo codice � pi� grande del secondo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPO = this.w_TIPO
    return

enddefine

* --- Define pages as container
define class tgste_ssvPag1 as StdContainer
  Width  = 496
  height = 185
  stdWidth  = 496
  stdheight = 185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_2 as StdCombo with uid="VRNDYXYTYW",rtseq=2,rtrep=.f.,left=109,top=10,width=95,height=21;
    , ToolTipText = "Tipo selezionato";
    , HelpContextID = 85321162;
    , cFormVar="w_TIPO",RowSource=""+"Cliente,"+"Fornitore,"+"Generico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPO_1_2.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_2.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='C',1,;
      iif(this.Parent.oContained.w_TIPO=='F',2,;
      iif(this.Parent.oContained.w_TIPO=='G',3,;
      0)))
  endfunc

  add object oCODICE1_1_5 as StdField with uid="BYXSXGITSA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il primo codice � pi� grande del secondo",;
    ToolTipText = "Scadenza di inizio selezione",;
    HelpContextID = 68378406,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=110, Top=53, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SCA_VARI", cZoomOnZoom="GSAR_ASC", oKey_1_1="SCCODSEC", oKey_1_2="this.w_CHIAVE", oKey_2_1="SCCODICE", oKey_2_2="this.w_CODICE1"

  func oCODICE1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SCA_VARI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SCCODSEC="+cp_ToStrODBC(this.Parent.oContained.w_CHIAVE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SCCODSEC="+cp_ToStr(this.Parent.oContained.w_CHIAVE)
    endif
    do cp_zoom with 'SCA_VARI','*','SCCODSEC,SCCODICE',cp_AbsName(this.parent,'oCODICE1_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASC',"Scadenze varie",'GSTE_SSV.SCA_VARI_VZM',this.parent.oContained
  endproc
  proc oCODICE1_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SCCODSEC=w_CHIAVE
     i_obj.w_SCCODICE=this.parent.oContained.w_CODICE1
     i_obj.ecpSave()
  endproc

  add object oCODICE2_1_6 as StdField with uid="XDQFGKXPNO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODICE2", cQueryName = "CODICE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il primo codice � pi� grande del secondo",;
    ToolTipText = "Scadenza di fine selezione",;
    HelpContextID = 68378406,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=110, Top=77, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SCA_VARI", cZoomOnZoom="GSAR_ASC", oKey_1_1="SCCODSEC", oKey_1_2="this.w_CHIAVE", oKey_2_1="SCCODICE", oKey_2_2="this.w_CODICE2"

  func oCODICE2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SCA_VARI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SCCODSEC="+cp_ToStrODBC(this.Parent.oContained.w_CHIAVE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SCCODSEC="+cp_ToStr(this.Parent.oContained.w_CHIAVE)
    endif
    do cp_zoom with 'SCA_VARI','*','SCCODSEC,SCCODICE',cp_AbsName(this.parent,'oCODICE2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASC',"Scadenze varie",'GSTE_SSV.SCA_VARI_VZM',this.parent.oContained
  endproc
  proc oCODICE2_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SCCODSEC=w_CHIAVE
     i_obj.w_SCCODICE=this.parent.oContained.w_CODICE2
     i_obj.ecpSave()
  endproc

  add object oDES1_1_7 as StdField with uid="QOFDPBDCZW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 87276234,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=53, InputMask=replicate('X',35)

  add object oDES2_1_8 as StdField with uid="ABPUJBHMUG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DES2", cQueryName = "DES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 87210698,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=208, Top=77, InputMask=replicate('X',35)


  add object oObj_1_9 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=110, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87151334


  add object oBtn_1_10 as StdButton with uid="VFGHQZGTYZ",left=390, top=136, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50943270;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="NNJPLJOJWG",left=441, top=136, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83528890;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="NXPDRHQIBG",Visible=.t., Left=4, Top=53,;
    Alignment=1, Width=104, Height=15,;
    Caption="Da scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WCVZVJVMEJ",Visible=.t., Left=4, Top=77,;
    Alignment=1, Width=104, Height=15,;
    Caption="A scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="UKBYUCWWLM",Visible=.t., Left=4, Top=110,;
    Alignment=1, Width=104, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="THNHIQMGPJ",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=104, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_ssv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
