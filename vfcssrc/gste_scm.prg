* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_scm                                                        *
*              Stampa congruit� partite/mastrini                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_214]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2009-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_scm",oParentObject))

* --- Class definition
define class tgste_scm as StdForm
  Top    = 15
  Left   = 11

  * --- Standard Properties
  Width  = 598
  Height = 299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-15"
  HelpContextID=177589143
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gste_scm"
  cComment = "Stampa congruit� partite/mastrini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODESE = space(10)
  w_TIPCON = space(1)
  w_DACODCON = space(15)
  w_ACODCON = space(15)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_REGINI = ctod('  /  /  ')
  w_REGFIN = ctod('  /  /  ')
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_CONTROL = space(2)
  w_TOLLER = 0
  w_ORDINAM = space(1)
  w_FLPART = space(1)
  w_ONUME = 0
  w_DADESCON = space(40)
  o_DADESCON = space(40)
  w_ADESCON = space(40)
  o_ADESCON = space(40)
  w_PARTSN = space(1)
  w_RAGSOCINI = space(60)
  w_RAGSOCFIN = space(60)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_scmPag1","gste_scm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='ESERCIZI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_scm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODESE=space(10)
      .w_TIPCON=space(1)
      .w_DACODCON=space(15)
      .w_ACODCON=space(15)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_REGINI=ctod("  /  /  ")
      .w_REGFIN=ctod("  /  /  ")
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_CONTROL=space(2)
      .w_TOLLER=0
      .w_ORDINAM=space(1)
      .w_FLPART=space(1)
      .w_ONUME=0
      .w_DADESCON=space(40)
      .w_ADESCON=space(40)
      .w_PARTSN=space(1)
      .w_RAGSOCINI=space(60)
      .w_RAGSOCFIN=space(60)
        .w_CODAZI = i_CODAZI
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .w_TIPCON = 'C'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_DACODCON))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ACODCON))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_REGINI = .w_INIESE
        .w_REGFIN = .w_FINESE
          .DoRTCalc(10,11,.f.)
        .w_CONTROL = 'NC'
        .w_TOLLER = 1
        .w_ORDINAM = 'C'
        .w_FLPART = 'N'
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
          .DoRTCalc(16,19,.f.)
        .w_RAGSOCINI = .w_DADESCON
        .w_RAGSOCFIN = .w_ADESCON
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CODESE = g_CODESE
          .link_1_2('Full')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(3,19,.t.)
        if .o_DADESCON<>.w_DADESCON
            .w_RAGSOCINI = .w_DADESCON
        endif
        if .o_ADESCON<>.w_ADESCON
            .w_RAGSOCFIN = .w_ADESCON
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTOLLER_1_15.visible=!this.oPgFrm.Page1.oPag.oTOLLER_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(10))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(10)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCON
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DACODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDACODCON_1_4'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DACODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DADESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCON = space(15)
      endif
      this.w_DADESCON = space(40)
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ACODCON)) OR  (.w_DACODCON<=.w_ACODCON)) and .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
        endif
        this.w_DACODCON = space(15)
        this.w_DADESCON = space(40)
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_ACODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ACODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oACODCON_1_5'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_ACODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ADESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ACODCON = space(15)
      endif
      this.w_ADESCON = space(40)
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ACODCON>=.w_DACODCON) or (empty(.w_DACODCON))) and .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
        endif
        this.w_ACODCON = space(15)
        this.w_ADESCON = space(40)
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_3.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCON_1_4.value==this.w_DACODCON)
      this.oPgFrm.Page1.oPag.oDACODCON_1_4.value=this.w_DACODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oACODCON_1_5.value==this.w_ACODCON)
      this.oPgFrm.Page1.oPag.oACODCON_1_5.value=this.w_ACODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oREGINI_1_9.value==this.w_REGINI)
      this.oPgFrm.Page1.oPag.oREGINI_1_9.value=this.w_REGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oREGFIN_1_10.value==this.w_REGFIN)
      this.oPgFrm.Page1.oPag.oREGFIN_1_10.value=this.w_REGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_12.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_12.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_13.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_13.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTROL_1_14.RadioValue()==this.w_CONTROL)
      this.oPgFrm.Page1.oPag.oCONTROL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOLLER_1_15.value==this.w_TOLLER)
      this.oPgFrm.Page1.oPag.oTOLLER_1_15.value=this.w_TOLLER
    endif
    if not(this.oPgFrm.Page1.oPag.oORDINAM_1_16.RadioValue()==this.w_ORDINAM)
      this.oPgFrm.Page1.oPag.oORDINAM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPART_1_17.RadioValue()==this.w_FLPART)
      this.oPgFrm.Page1.oPag.oFLPART_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESCON_1_23.value==this.w_DADESCON)
      this.oPgFrm.Page1.oPag.oDADESCON_1_23.value=this.w_DADESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oADESCON_1_28.value==this.w_ADESCON)
      this.oPgFrm.Page1.oPag.oADESCON_1_28.value=this.w_ADESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_ACODCON)) OR  (.w_DACODCON<=.w_ACODCON)) and .w_PARTSN='S')  and not(empty(.w_DACODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCON_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
          case   not(((.w_ACODCON>=.w_DACODCON) or (empty(.w_DACODCON))) and .w_PARTSN='S')  and not(empty(.w_ACODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACODCON_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite")
          case   ((empty(.w_REGINI)) or not((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_REGINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della finale")
          case   ((empty(.w_REGFIN)) or not((.w_regini<=.w_regfin) or (empty(.w_regini))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGFIN_1_10.SetFocus()
            i_bnoObbl = !empty(.w_REGFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DADESCON = this.w_DADESCON
    this.o_ADESCON = this.w_ADESCON
    return

enddefine

* --- Define pages as container
define class tgste_scmPag1 as StdContainer
  Width  = 594
  height = 299
  stdWidth  = 594
  stdheight = 299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_3 as StdCombo with uid="WDXDHXNHSV",rtseq=3,rtrep=.f.,left=112,top=12,width=92,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori, conti generici";
    , HelpContextID = 36824522;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Conti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPCON_1_3.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_3.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DACODCON)
        bRes2=.link_1_4('Full')
      endif
      if .not. empty(.w_ACODCON)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDACODCON_1_4 as StdField with uid="ICXFDOKTCK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DACODCON", cQueryName = "DACODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite",;
    ToolTipText = "Cliente/fornitore/conto iniziale di selezione",;
    HelpContextID = 36258180,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=112, Top=41, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DACODCON"

  func oDACODCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCON_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCON_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDACODCON_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oDACODCON_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DACODCON
     i_obj.ecpSave()
  endproc

  add object oACODCON_1_5 as StdField with uid="UROVHFNDXX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ACODCON", cQueryName = "ACODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure non gestisce le partite",;
    ToolTipText = "Cliente/fornitore/conto finale di selezione",;
    HelpContextID = 235864838,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=112, Top=68, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ACODCON"

  func oACODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oACODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oACODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oACODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ACODCON
     i_obj.ecpSave()
  endproc

  add object oREGINI_1_9 as StdField with uid="JPZWSXEPCG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_REGINI", cQueryName = "REGINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della finale",;
    ToolTipText = "Data registrazione iniziale da ricercare per tutti i saldi",;
    HelpContextID = 121403882,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=112, Top=95

  func oREGINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN)))
    endwith
    return bRes
  endfunc

  add object oREGFIN_1_10 as StdField with uid="PDJGLOUCJU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_REGFIN", cQueryName = "REGFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della finale",;
    ToolTipText = "Data registrazione finale da ricercare per tutti i saldi",;
    HelpContextID = 42957290,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=216, Top=95

  func oREGFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_regini<=.w_regfin) or (empty(.w_regini)))
    endwith
    return bRes
  endfunc

  add object oSCAINI_1_12 as StdField with uid="BUACHACMJF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data iniziale scadenza da ricercare per saldo partite",;
    HelpContextID = 121428954,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=112, Top=122

  func oSCAINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_13 as StdField with uid="AVZCQICAXG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data finale scadenza da ricercare per saldo partite",;
    HelpContextID = 42982362,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=216, Top=122

  func oSCAFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
    endwith
    return bRes
  endfunc


  add object oCONTROL_1_14 as StdCombo with uid="LVYKEJADST",rtseq=12,rtrep=.f.,left=112,top=151,width=210,height=21;
    , ToolTipText = "Tipo di controllo da effettuare su partite/mastrini";
    , HelpContextID = 252641062;
    , cFormVar="w_CONTROL",RowSource=""+"Mancanza quadratura mastrini,"+"Mancanza quadratura saldi,"+"Mancanza quadratura mastrini >x%,"+"Mancanza quadratura saldi >x%,"+"Nessun controllo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCONTROL_1_14.RadioValue()
    return(iif(this.value =1,'QM',;
    iif(this.value =2,'QS',;
    iif(this.value =3,'MM',;
    iif(this.value =4,'MS',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCONTROL_1_14.GetRadio()
    this.Parent.oContained.w_CONTROL = this.RadioValue()
    return .t.
  endfunc

  func oCONTROL_1_14.SetRadio()
    this.Parent.oContained.w_CONTROL=trim(this.Parent.oContained.w_CONTROL)
    this.value = ;
      iif(this.Parent.oContained.w_CONTROL=='QM',1,;
      iif(this.Parent.oContained.w_CONTROL=='QS',2,;
      iif(this.Parent.oContained.w_CONTROL=='MM',3,;
      iif(this.Parent.oContained.w_CONTROL=='MS',4,;
      iif(this.Parent.oContained.w_CONTROL=='NC',5,;
      0)))))
  endfunc

  add object oTOLLER_1_15 as StdField with uid="RIVWEXYEJB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TOLLER", cQueryName = "TOLLER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Tolleranza per controllo quadratura",;
    HelpContextID = 248061898,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=432, Top=150, cSayPict='"999.99"', cGetPict='"999.99"'

  func oTOLLER_1_15.mHide()
    with this.Parent.oContained
      return (.w_CONTROL $ 'QMQSNC')
    endwith
  endfunc


  add object oORDINAM_1_16 as StdCombo with uid="BLRPXTCIGE",rtseq=14,rtrep=.f.,left=112,top=180,width=210,height=21;
    , ToolTipText = "Ordinamento di stampa";
    , HelpContextID = 12804838;
    , cFormVar="w_ORDINAM",RowSource=""+"Codice,"+"Descrizione,"+"% Mancanza quadratura mastrini,"+"% Mancanza quadratura saldi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDINAM_1_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    iif(this.value =3,'M',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oORDINAM_1_16.GetRadio()
    this.Parent.oContained.w_ORDINAM = this.RadioValue()
    return .t.
  endfunc

  func oORDINAM_1_16.SetRadio()
    this.Parent.oContained.w_ORDINAM=trim(this.Parent.oContained.w_ORDINAM)
    this.value = ;
      iif(this.Parent.oContained.w_ORDINAM=='C',1,;
      iif(this.Parent.oContained.w_ORDINAM=='D',2,;
      iif(this.Parent.oContained.w_ORDINAM=='M',3,;
      iif(this.Parent.oContained.w_ORDINAM=='S',4,;
      0))))
  endfunc

  add object oFLPART_1_17 as StdCheck with uid="TPLNEPLCFD",rtseq=15,rtrep=.f.,left=328, top=178, caption="Solo movimenti contabili a partite",;
    ToolTipText = "Se attivato il saldo mastrini determinato solo dai movimenti che gestiscono partite",;
    HelpContextID = 201581482,;
    cFormVar="w_FLPART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPART_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPART_1_17.GetRadio()
    this.Parent.oContained.w_FLPART = this.RadioValue()
    return .t.
  endfunc

  func oFLPART_1_17.SetRadio()
    this.Parent.oContained.w_FLPART=trim(this.Parent.oContained.w_FLPART)
    this.value = ;
      iif(this.Parent.oContained.w_FLPART=='S',1,;
      0)
  endfunc


  add object oObj_1_18 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=221, width=442,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181284122


  add object oBtn_1_19 as StdButton with uid="SQNLIXYMPH",left=489, top=249, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217492186;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do GSTE_BPQ with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="VJEYGWVSZN",left=540, top=249, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184906566;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDADESCON_1_23 as StdField with uid="XZILIWPJAW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DADESCON", cQueryName = "DADESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51335556,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=258, Top=41, InputMask=replicate('X',40)

  add object oADESCON_1_28 as StdField with uid="RYWPVEYYDP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ADESCON", cQueryName = "ADESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 236807174,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=258, Top=68, InputMask=replicate('X',40)

  add object oStr_1_7 as StdString with uid="HRHDNGMHAE",Visible=.t., Left=7, Top=126,;
    Alignment=1, Width=103, Height=18,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="PMYSREYHZS",Visible=.t., Left=189, Top=126,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EUDLYBOGMQ",Visible=.t., Left=11, Top=225,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DKKXSABCFP",Visible=.t., Left=7, Top=15,;
    Alignment=1, Width=103, Height=17,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VNGFGUEZQD",Visible=.t., Left=7, Top=45,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="SOQNUXLCBD",Visible=.t., Left=7, Top=45,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="MREIEMXUSI",Visible=.t., Left=7, Top=45,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="BHKFLYFLSC",Visible=.t., Left=7, Top=68,;
    Alignment=1, Width=103, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="YNRFGBWWYN",Visible=.t., Left=7, Top=68,;
    Alignment=1, Width=103, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="CHEJNYLZNR",Visible=.t., Left=7, Top=69,;
    Alignment=1, Width=103, Height=18,;
    Caption="A conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="UAZRXOSAOT",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data reg. da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="VTPZLRHEBF",Visible=.t., Left=189, Top=99,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="UIWOFFAQHK",Visible=.t., Left=7, Top=151,;
    Alignment=1, Width=103, Height=18,;
    Caption="Controllo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="KYFNREIYJM",Visible=.t., Left=7, Top=180,;
    Alignment=1, Width=103, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NURAKENXOE",Visible=.t., Left=331, Top=151,;
    Alignment=1, Width=99, Height=18,;
    Caption="% Tolleranza:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_CONTROL $ 'QMQSNC')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_scm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
