* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_aca                                                        *
*              Codici di ricerca articoli/servizi                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_128]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2018-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_aca"))

* --- Class definition
define class tgsma_aca as StdForm
  Top    = 12
  Left   = 25

  * --- Standard Properties
  Width  = 667
  Height = 411+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-27"
  HelpContextID=158696599
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=64

  * --- Constant Properties
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  UNIMIS_IDX = 0
  TRADARTI_IDX = 0
  TIP_COLL_IDX = 0
  cFile = "KEY_ARTI"
  cKeySelect = "CACODICE"
  cKeyWhere  = "CACODICE=this.w_CACODICE"
  cKeyWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cKeyWhereODBCqualified = '"KEY_ARTI.CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cPrg = "gsma_aca"
  cComment = "Codici di ricerca articoli/servizi"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0ACA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CACODICE = space(20)
  o_CACODICE = space(20)
  w_CACODART = space(20)
  o_CACODART = space(20)
  w_CAFLIMBA = space(1)
  o_CAFLIMBA = space(1)
  w_CATIPCON = space(1)
  o_CATIPCON = space(1)
  w_CADESART = space(40)
  w_CADESSUP = space(0)
  w_CA__TIPO = space(1)
  o_CA__TIPO = space(1)
  w_CACODCON = space(15)
  o_CACODCON = space(15)
  w_DESCON = space(36)
  w_INDIRI = space(35)
  w____CAP = space(8)
  w_LOCALI = space(28)
  w_DESART = space(40)
  o_DESART = space(40)
  w_UNMIS1 = space(3)
  o_UNMIS1 = space(3)
  w_FLFRAZ = space(1)
  o_FLFRAZ = space(1)
  w_CAUNIMIS = space(3)
  o_CAUNIMIS = space(3)
  w_F2FRAZ = space(1)
  o_F2FRAZ = space(1)
  w_CATIPBAR = space(1)
  o_CATIPBAR = space(1)
  w_CANUMDEQ = space(1)
  w_CAFLSTAM = space(1)
  w_CAPUBWEB = space(1)
  w_TIPART = space(2)
  w_CODICE = space(41)
  w_CAOPERAT = space(1)
  w_CAMOLTIP = 0
  w_CADTOBSO = ctod('  /  /  ')
  w_CAMINVEN = 0
  w_CATIPCO3 = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CADTINVA = ctod('  /  /  ')
  w_TIPKEY = space(2)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CATIPMA3 = space(5)
  w_CAPESNE3 = 0
  w_CAPESLO3 = 0
  w_CADESVO3 = space(15)
  w_CAUMVOL3 = space(3)
  w_CATPCON3 = space(3)
  w_CAPZCON3 = 0
  w_CACOCOL3 = 0
  w_CADIMLU3 = 0
  w_CADIMLA3 = 0
  w_CADIMAL3 = 0
  w_CAUMDIM3 = space(3)
  w_CAFLCON3 = space(1)
  w_COD1 = space(20)
  w_DES1 = space(40)
  w_CODESC = space(5)
  w_CALENSCF = 0
  w_DESCOL = space(35)
  w_OCODCON = space(15)
  w_OTIPCON = space(15)
  w_ARPUBWEB = space(1)
  w_ARFLAPCA = space(1)
  w_CACODFAS = space(50)
  w_ARFLPECO = space(1)
  w_CACODTIP = space(35)
  w_CACODCLF = space(5)
  w_CACODVAL = space(35)
  w_CAFLGESC = space(1)

  * --- Children pointers
  GSMA_MTA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'KEY_ARTI','gsma_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_acaPag1","gsma_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice")
      .Pages(1).HelpContextID = 183158746
      .Pages(2).addobject("oPag","tgsma_acaPag2","gsma_aca",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Traduzioni")
      .Pages(2).HelpContextID = 80708475
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsma_aca
    WITH THIS.PARENT
     If IsAlt()
       .cComment = CP_TRANSLATE('Codici di ricerca voci del tariffario')
     Else
       .cComment = CP_TRANSLATE('Codici articoli/servizi')
     Endif
    Endwith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='TRADARTI'
    this.cWorkTables[5]='TIP_COLL'
    this.cWorkTables[6]='KEY_ARTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.KEY_ARTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.KEY_ARTI_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MTA = CREATEOBJECT('stdDynamicChild',this,'GSMA_MTA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSMA_MTA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MTA)
      this.GSMA_MTA.DestroyChildrenChain()
      this.GSMA_MTA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MTA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MTA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MTA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MTA.SetKey(;
            .w_CACODICE,"LGCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MTA.ChangeRow(this.cRowID+'      1',1;
             ,.w_CACODICE,"LGCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_MTA)
        i_f=.GSMA_MTA.BuildFilter()
        if !(i_f==.GSMA_MTA.cQueryFilter)
          i_fnidx=.GSMA_MTA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MTA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MTA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MTA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MTA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CACODICE = NVL(CACODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from KEY_ARTI where CACODICE=KeySet.CACODICE
    *
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('KEY_ARTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "KEY_ARTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' KEY_ARTI '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCON = space(36)
        .w_INDIRI = space(35)
        .w____CAP = space(8)
        .w_LOCALI = space(28)
        .w_DESART = space(40)
        .w_UNMIS1 = space(3)
        .w_FLFRAZ = space(1)
        .w_F2FRAZ = space(1)
        .w_TIPART = space(2)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CODESC = space(5)
        .w_DESCOL = space(35)
        .w_ARPUBWEB = space(1)
        .w_ARFLAPCA = space(1)
        .w_ARFLPECO = space(1)
        .w_CACODICE = NVL(CACODICE,space(20))
        .w_CACODART = NVL(CACODART,space(20))
          if link_1_2_joined
            this.w_CACODART = NVL(ARCODART102,NVL(this.w_CACODART,space(20)))
            this.w_DESART = NVL(ARDESART102,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO102),ctod("  /  /  "))
            this.w_UNMIS1 = NVL(ARUNMIS1102,space(3))
            this.w_TIPART = NVL(ARTIPART102,space(2))
            this.w_ARPUBWEB = NVL(ARPUBWEB102,space(1))
            this.w_ARFLAPCA = NVL(ARFLAPCA102,space(1))
            this.w_ARFLPECO = NVL(ARFLPECO102,space(1))
            this.w_CACODTIP = NVL(ARCODTIP102,space(35))
            this.w_CACODVAL = NVL(ARCODVAL102,space(35))
            this.w_CACODCLF = NVL(ARCODCLF102,space(5))
            this.w_CAFLGESC = NVL(ARFLGESC102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_CAFLIMBA = NVL(CAFLIMBA,space(1))
        .w_CATIPCON = NVL(CATIPCON,space(1))
        .w_CADESART = NVL(CADESART,space(40))
        .w_CADESSUP = NVL(CADESSUP,space(0))
        .w_CA__TIPO = NVL(CA__TIPO,space(1))
        .w_CACODCON = NVL(CACODCON,space(15))
          if link_1_8_joined
            this.w_CACODCON = NVL(ANCODICE108,NVL(this.w_CACODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI108,space(36))
            this.w_INDIRI = NVL(ANINDIRI108,space(35))
            this.w____CAP = NVL(AN___CAP108,space(8))
            this.w_LOCALI = NVL(ANLOCALI108,space(28))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO108),ctod("  /  /  "))
            this.w_CODESC = NVL(ANCODESC108,space(5))
          else
          .link_1_8('Load')
          endif
          .link_1_14('Load')
        .w_CAUNIMIS = NVL(CAUNIMIS,space(3))
          if link_1_16_joined
            this.w_CAUNIMIS = NVL(UMCODICE116,NVL(this.w_CAUNIMIS,space(3)))
            this.w_F2FRAZ = NVL(UMFLFRAZ116,space(1))
          else
          .link_1_16('Load')
          endif
        .w_CATIPBAR = NVL(CATIPBAR,space(1))
        .w_CANUMDEQ = NVL(CANUMDEQ,space(1))
        .w_CAFLSTAM = NVL(CAFLSTAM,space(1))
        .w_CAPUBWEB = NVL(CAPUBWEB,space(1))
        .w_CODICE = .w_CACODICE
        .w_CAOPERAT = NVL(CAOPERAT,space(1))
        .w_CAMOLTIP = NVL(CAMOLTIP,0)
        .w_CADTOBSO = NVL(cp_ToDate(CADTOBSO),ctod("  /  /  "))
        .w_CAMINVEN = NVL(CAMINVEN,0)
        .w_CATIPCO3 = NVL(CATIPCO3,space(5))
          if link_1_29_joined
            this.w_CATIPCO3 = NVL(TCCODICE129,NVL(this.w_CATIPCO3,space(5)))
            this.w_DESCOL = NVL(TCDESCRI129,space(35))
          else
          .link_1_29('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CADTINVA = NVL(cp_ToDate(CADTINVA),ctod("  /  /  "))
        .w_TIPKEY = iif(.w_CATIPCON='M','FM',iif(.w_CATIPCON='I','FO',iif(.w_CATIPCON='D','DE','')))
        .w_CATIPMA3 = NVL(CATIPMA3,space(5))
        .w_CAPESNE3 = NVL(CAPESNE3,0)
        .w_CAPESLO3 = NVL(CAPESLO3,0)
        .w_CADESVO3 = NVL(CADESVO3,space(15))
        .w_CAUMVOL3 = NVL(CAUMVOL3,space(3))
        .w_CATPCON3 = NVL(CATPCON3,space(3))
        .w_CAPZCON3 = NVL(CAPZCON3,0)
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_CACOCOL3 = NVL(CACOCOL3,0)
        .w_CADIMLU3 = NVL(CADIMLU3,0)
        .w_CADIMLA3 = NVL(CADIMLA3,0)
        .w_CADIMAL3 = NVL(CADIMAL3,0)
        .w_CAUMDIM3 = NVL(CAUMDIM3,space(3))
        .w_CAFLCON3 = NVL(CAFLCON3,space(1))
        .w_COD1 = .w_CACODICE
        .w_DES1 = .w_CADESART
        .w_CALENSCF = NVL(CALENSCF,0)
        .w_OCODCON = .w_CACODCON
        .w_OTIPCON = .w_CATIPCON
        .w_CACODFAS = NVL(CACODFAS,space(50))
        .w_CACODTIP = NVL(CACODTIP,space(35))
        .w_CACODCLF = NVL(CACODCLF,space(5))
        .w_CACODVAL = NVL(CACODVAL,space(35))
        .w_CAFLGESC = NVL(CAFLGESC,space(1))
        cp_LoadRecExtFlds(this,'KEY_ARTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODICE = space(20)
      .w_CACODART = space(20)
      .w_CAFLIMBA = space(1)
      .w_CATIPCON = space(1)
      .w_CADESART = space(40)
      .w_CADESSUP = space(0)
      .w_CA__TIPO = space(1)
      .w_CACODCON = space(15)
      .w_DESCON = space(36)
      .w_INDIRI = space(35)
      .w____CAP = space(8)
      .w_LOCALI = space(28)
      .w_DESART = space(40)
      .w_UNMIS1 = space(3)
      .w_FLFRAZ = space(1)
      .w_CAUNIMIS = space(3)
      .w_F2FRAZ = space(1)
      .w_CATIPBAR = space(1)
      .w_CANUMDEQ = space(1)
      .w_CAFLSTAM = space(1)
      .w_CAPUBWEB = space(1)
      .w_TIPART = space(2)
      .w_CODICE = space(41)
      .w_CAOPERAT = space(1)
      .w_CAMOLTIP = 0
      .w_CADTOBSO = ctod("  /  /  ")
      .w_CAMINVEN = 0
      .w_CATIPCO3 = space(5)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CADTINVA = ctod("  /  /  ")
      .w_TIPKEY = space(2)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CATIPMA3 = space(5)
      .w_CAPESNE3 = 0
      .w_CAPESLO3 = 0
      .w_CADESVO3 = space(15)
      .w_CAUMVOL3 = space(3)
      .w_CATPCON3 = space(3)
      .w_CAPZCON3 = 0
      .w_CACOCOL3 = 0
      .w_CADIMLU3 = 0
      .w_CADIMLA3 = 0
      .w_CADIMAL3 = 0
      .w_CAUMDIM3 = space(3)
      .w_CAFLCON3 = space(1)
      .w_COD1 = space(20)
      .w_DES1 = space(40)
      .w_CODESC = space(5)
      .w_CALENSCF = 0
      .w_DESCOL = space(35)
      .w_OCODCON = space(15)
      .w_OTIPCON = space(15)
      .w_ARPUBWEB = space(1)
      .w_ARFLAPCA = space(1)
      .w_CACODFAS = space(50)
      .w_ARFLPECO = space(1)
      .w_CACODTIP = space(35)
      .w_CACODCLF = space(5)
      .w_CACODVAL = space(35)
      .w_CAFLGESC = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CACODART = SPACE(20)
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CACODART))
          .link_1_2('Full')
          endif
        .w_CAFLIMBA = 'N'
        .w_CATIPCON = IIF(IsAlt(),IIF(.w_TIPART='DE','D',IIF(.w_TIPART='FO','I','M')),'R')
        .w_CADESART = .w_DESART
          .DoRTCalc(6,6,.f.)
        .w_CA__TIPO = IIF(.w_CATIPCON='I', 'F', IIF(.w_CATIPCON $ 'CF', 'R', .w_CATIPCON))
        .w_CACODCON = SPACE(15)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CACODCON))
          .link_1_8('Full')
          endif
        .DoRTCalc(9,14,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_14('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_CAUNIMIS))
          .link_1_16('Full')
          endif
          .DoRTCalc(17,17,.f.)
        .w_CATIPBAR = '0'
        .w_CANUMDEQ = IIF(((.w_FLFRAZ<>'S' and empty(nvl(.w_CAUNIMIS,''))) or (not empty(nvl(.w_CAUNIMIS,'')) AND .w_F2FRAZ<>'S')) and Not Empty(.w_CANUMDEQ) ,.w_CANUMDEQ,'0')
        .w_CAFLSTAM = 'S'
        .w_CAPUBWEB = 'N'
          .DoRTCalc(22,22,.f.)
        .w_CODICE = .w_CACODICE
        .w_CAOPERAT = '*'
        .w_CAMOLTIP = IIF( Not Empty ( .w_CAUNIMIS ) ,  1 , 0 )
        .DoRTCalc(26,28,.f.)
          if not(empty(.w_CATIPCO3))
          .link_1_29('Full')
          endif
          .DoRTCalc(29,33,.f.)
        .w_TIPKEY = iif(.w_CATIPCON='M','FM',iif(.w_CATIPCON='I','FO',iif(.w_CATIPCON='D','DE','')))
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
          .DoRTCalc(36,49,.f.)
        .w_COD1 = .w_CACODICE
        .w_DES1 = .w_CADESART
          .DoRTCalc(52,54,.f.)
        .w_OCODCON = .w_CACODCON
        .w_OTIPCON = .w_CATIPCON
      endif
    endwith
    cp_BlankRecExtFlds(this,'KEY_ARTI')
    this.DoRTCalc(57,64,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCACODART_1_2.enabled = i_bVal
      .Page1.oPag.oCAFLIMBA_1_3.enabled = i_bVal
      .Page1.oPag.oCATIPCON_1_4.enabled = i_bVal
      .Page1.oPag.oCADESART_1_5.enabled = i_bVal
      .Page1.oPag.oCADESSUP_1_6.enabled = i_bVal
      .Page1.oPag.oCACODCON_1_8.enabled = i_bVal
      .Page1.oPag.oCAUNIMIS_1_16.enabled = i_bVal
      .Page1.oPag.oCATIPBAR_1_18.enabled = i_bVal
      .Page1.oPag.oCANUMDEQ_1_19.enabled = i_bVal
      .Page1.oPag.oCAFLSTAM_1_20.enabled = i_bVal
      .Page1.oPag.oCAPUBWEB_1_21.enabled = i_bVal
      .Page1.oPag.oCAOPERAT_1_24.enabled = i_bVal
      .Page1.oPag.oCAMOLTIP_1_25.enabled = i_bVal
      .Page1.oPag.oCADTOBSO_1_26.enabled = i_bVal
      .Page1.oPag.oCAMINVEN_1_27.enabled = i_bVal
      .Page1.oPag.oCATIPCO3_1_29.enabled = i_bVal
      .Page1.oPag.oCADTINVA_1_46.enabled = i_bVal
      .Page1.oPag.oCACODTIP_1_85.enabled = i_bVal
      .Page1.oPag.oCACODCLF_1_86.enabled = i_bVal
      .Page1.oPag.oCACODVAL_1_87.enabled = i_bVal
      .Page1.oPag.oCAFLGESC_1_91.enabled = i_bVal
      .Page1.oPag.oBtn_1_64.enabled = .Page1.oPag.oBtn_1_64.mCond()
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODICE_1_1.enabled = .t.
        .Page1.oPag.oCACODART_1_2.enabled = .t.
        .Page1.oPag.oCADESART_1_5.enabled = .t.
        .Page1.oPag.oCACODCON_1_8.enabled = .t.
      endif
    endwith
    this.GSMA_MTA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'KEY_ARTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MTA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODICE,"CACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODART,"CACODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLIMBA,"CAFLIMBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPCON,"CATIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESART,"CADESART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESSUP,"CADESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__TIPO,"CA__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODCON,"CACODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAUNIMIS,"CAUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPBAR,"CATIPBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CANUMDEQ,"CANUMDEQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLSTAM,"CAFLSTAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPUBWEB,"CAPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAOPERAT,"CAOPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAMOLTIP,"CAMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADTOBSO,"CADTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAMINVEN,"CAMINVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPCO3,"CATIPCO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADTINVA,"CADTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPMA3,"CATIPMA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPESNE3,"CAPESNE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPESLO3,"CAPESLO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESVO3,"CADESVO3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAUMVOL3,"CAUMVOL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATPCON3,"CATPCON3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPZCON3,"CAPZCON3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACOCOL3,"CACOCOL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIMLU3,"CADIMLU3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIMLA3,"CADIMLA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADIMAL3,"CADIMAL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAUMDIM3,"CAUMDIM3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLCON3,"CAFLCON3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CALENSCF,"CALENSCF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODFAS,"CACODFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODTIP,"CACODTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODCLF,"CACODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODVAL,"CACODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLGESC,"CAFLGESC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    i_lTable = "KEY_ARTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.KEY_ARTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SCA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into KEY_ARTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'KEY_ARTI')
        i_extval=cp_InsertValODBCExtFlds(this,'KEY_ARTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CACODICE,CACODART,CAFLIMBA,CATIPCON,CADESART"+;
                  ",CADESSUP,CA__TIPO,CACODCON,CAUNIMIS,CATIPBAR"+;
                  ",CANUMDEQ,CAFLSTAM,CAPUBWEB,CAOPERAT,CAMOLTIP"+;
                  ",CADTOBSO,CAMINVEN,CATIPCO3,UTCC,UTCV"+;
                  ",UTDC,UTDV,CADTINVA,CATIPMA3,CAPESNE3"+;
                  ",CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3"+;
                  ",CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3"+;
                  ",CAFLCON3,CALENSCF,CACODFAS,CACODTIP,CACODCLF"+;
                  ",CACODVAL,CAFLGESC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CACODICE)+;
                  ","+cp_ToStrODBCNull(this.w_CACODART)+;
                  ","+cp_ToStrODBC(this.w_CAFLIMBA)+;
                  ","+cp_ToStrODBC(this.w_CATIPCON)+;
                  ","+cp_ToStrODBC(this.w_CADESART)+;
                  ","+cp_ToStrODBC(this.w_CADESSUP)+;
                  ","+cp_ToStrODBC(this.w_CA__TIPO)+;
                  ","+cp_ToStrODBCNull(this.w_CACODCON)+;
                  ","+cp_ToStrODBCNull(this.w_CAUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_CATIPBAR)+;
                  ","+cp_ToStrODBC(this.w_CANUMDEQ)+;
                  ","+cp_ToStrODBC(this.w_CAFLSTAM)+;
                  ","+cp_ToStrODBC(this.w_CAPUBWEB)+;
                  ","+cp_ToStrODBC(this.w_CAOPERAT)+;
                  ","+cp_ToStrODBC(this.w_CAMOLTIP)+;
                  ","+cp_ToStrODBC(this.w_CADTOBSO)+;
                  ","+cp_ToStrODBC(this.w_CAMINVEN)+;
                  ","+cp_ToStrODBCNull(this.w_CATIPCO3)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CADTINVA)+;
                  ","+cp_ToStrODBC(this.w_CATIPMA3)+;
                  ","+cp_ToStrODBC(this.w_CAPESNE3)+;
                  ","+cp_ToStrODBC(this.w_CAPESLO3)+;
                  ","+cp_ToStrODBC(this.w_CADESVO3)+;
                  ","+cp_ToStrODBC(this.w_CAUMVOL3)+;
                  ","+cp_ToStrODBC(this.w_CATPCON3)+;
                  ","+cp_ToStrODBC(this.w_CAPZCON3)+;
                  ","+cp_ToStrODBC(this.w_CACOCOL3)+;
                  ","+cp_ToStrODBC(this.w_CADIMLU3)+;
                  ","+cp_ToStrODBC(this.w_CADIMLA3)+;
                  ","+cp_ToStrODBC(this.w_CADIMAL3)+;
                  ","+cp_ToStrODBC(this.w_CAUMDIM3)+;
                  ","+cp_ToStrODBC(this.w_CAFLCON3)+;
                  ","+cp_ToStrODBC(this.w_CALENSCF)+;
                  ","+cp_ToStrODBC(this.w_CACODFAS)+;
                  ","+cp_ToStrODBC(this.w_CACODTIP)+;
                  ","+cp_ToStrODBC(this.w_CACODCLF)+;
                  ","+cp_ToStrODBC(this.w_CACODVAL)+;
                  ","+cp_ToStrODBC(this.w_CAFLGESC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'KEY_ARTI')
        i_extval=cp_InsertValVFPExtFlds(this,'KEY_ARTI')
        cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE)
        INSERT INTO (i_cTable);
              (CACODICE,CACODART,CAFLIMBA,CATIPCON,CADESART,CADESSUP,CA__TIPO,CACODCON,CAUNIMIS,CATIPBAR,CANUMDEQ,CAFLSTAM,CAPUBWEB,CAOPERAT,CAMOLTIP,CADTOBSO,CAMINVEN,CATIPCO3,UTCC,UTCV,UTDC,UTDV,CADTINVA,CATIPMA3,CAPESNE3,CAPESLO3,CADESVO3,CAUMVOL3,CATPCON3,CAPZCON3,CACOCOL3,CADIMLU3,CADIMLA3,CADIMAL3,CAUMDIM3,CAFLCON3,CALENSCF,CACODFAS,CACODTIP,CACODCLF,CACODVAL,CAFLGESC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CACODICE;
                  ,this.w_CACODART;
                  ,this.w_CAFLIMBA;
                  ,this.w_CATIPCON;
                  ,this.w_CADESART;
                  ,this.w_CADESSUP;
                  ,this.w_CA__TIPO;
                  ,this.w_CACODCON;
                  ,this.w_CAUNIMIS;
                  ,this.w_CATIPBAR;
                  ,this.w_CANUMDEQ;
                  ,this.w_CAFLSTAM;
                  ,this.w_CAPUBWEB;
                  ,this.w_CAOPERAT;
                  ,this.w_CAMOLTIP;
                  ,this.w_CADTOBSO;
                  ,this.w_CAMINVEN;
                  ,this.w_CATIPCO3;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CADTINVA;
                  ,this.w_CATIPMA3;
                  ,this.w_CAPESNE3;
                  ,this.w_CAPESLO3;
                  ,this.w_CADESVO3;
                  ,this.w_CAUMVOL3;
                  ,this.w_CATPCON3;
                  ,this.w_CAPZCON3;
                  ,this.w_CACOCOL3;
                  ,this.w_CADIMLU3;
                  ,this.w_CADIMLA3;
                  ,this.w_CADIMAL3;
                  ,this.w_CAUMDIM3;
                  ,this.w_CAFLCON3;
                  ,this.w_CALENSCF;
                  ,this.w_CACODFAS;
                  ,this.w_CACODTIP;
                  ,this.w_CACODCLF;
                  ,this.w_CACODVAL;
                  ,this.w_CAFLGESC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.KEY_ARTI_IDX,i_nConn)
      *
      * update KEY_ARTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'KEY_ARTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CACODART="+cp_ToStrODBCNull(this.w_CACODART)+;
             ",CAFLIMBA="+cp_ToStrODBC(this.w_CAFLIMBA)+;
             ",CATIPCON="+cp_ToStrODBC(this.w_CATIPCON)+;
             ",CADESART="+cp_ToStrODBC(this.w_CADESART)+;
             ",CADESSUP="+cp_ToStrODBC(this.w_CADESSUP)+;
             ",CA__TIPO="+cp_ToStrODBC(this.w_CA__TIPO)+;
             ",CACODCON="+cp_ToStrODBCNull(this.w_CACODCON)+;
             ",CAUNIMIS="+cp_ToStrODBCNull(this.w_CAUNIMIS)+;
             ",CATIPBAR="+cp_ToStrODBC(this.w_CATIPBAR)+;
             ",CANUMDEQ="+cp_ToStrODBC(this.w_CANUMDEQ)+;
             ",CAFLSTAM="+cp_ToStrODBC(this.w_CAFLSTAM)+;
             ",CAPUBWEB="+cp_ToStrODBC(this.w_CAPUBWEB)+;
             ",CAOPERAT="+cp_ToStrODBC(this.w_CAOPERAT)+;
             ",CAMOLTIP="+cp_ToStrODBC(this.w_CAMOLTIP)+;
             ",CADTOBSO="+cp_ToStrODBC(this.w_CADTOBSO)+;
             ",CAMINVEN="+cp_ToStrODBC(this.w_CAMINVEN)+;
             ",CATIPCO3="+cp_ToStrODBCNull(this.w_CATIPCO3)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CADTINVA="+cp_ToStrODBC(this.w_CADTINVA)+;
             ",CATIPMA3="+cp_ToStrODBC(this.w_CATIPMA3)+;
             ",CAPESNE3="+cp_ToStrODBC(this.w_CAPESNE3)+;
             ",CAPESLO3="+cp_ToStrODBC(this.w_CAPESLO3)+;
             ",CADESVO3="+cp_ToStrODBC(this.w_CADESVO3)+;
             ",CAUMVOL3="+cp_ToStrODBC(this.w_CAUMVOL3)+;
             ",CATPCON3="+cp_ToStrODBC(this.w_CATPCON3)+;
             ",CAPZCON3="+cp_ToStrODBC(this.w_CAPZCON3)+;
             ",CACOCOL3="+cp_ToStrODBC(this.w_CACOCOL3)+;
             ",CADIMLU3="+cp_ToStrODBC(this.w_CADIMLU3)+;
             ",CADIMLA3="+cp_ToStrODBC(this.w_CADIMLA3)+;
             ",CADIMAL3="+cp_ToStrODBC(this.w_CADIMAL3)+;
             ",CAUMDIM3="+cp_ToStrODBC(this.w_CAUMDIM3)+;
             ",CAFLCON3="+cp_ToStrODBC(this.w_CAFLCON3)+;
             ",CALENSCF="+cp_ToStrODBC(this.w_CALENSCF)+;
             ",CACODFAS="+cp_ToStrODBC(this.w_CACODFAS)+;
             ",CACODTIP="+cp_ToStrODBC(this.w_CACODTIP)+;
             ",CACODCLF="+cp_ToStrODBC(this.w_CACODCLF)+;
             ",CACODVAL="+cp_ToStrODBC(this.w_CACODVAL)+;
             ",CAFLGESC="+cp_ToStrODBC(this.w_CAFLGESC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'KEY_ARTI')
        i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        UPDATE (i_cTable) SET;
              CACODART=this.w_CACODART;
             ,CAFLIMBA=this.w_CAFLIMBA;
             ,CATIPCON=this.w_CATIPCON;
             ,CADESART=this.w_CADESART;
             ,CADESSUP=this.w_CADESSUP;
             ,CA__TIPO=this.w_CA__TIPO;
             ,CACODCON=this.w_CACODCON;
             ,CAUNIMIS=this.w_CAUNIMIS;
             ,CATIPBAR=this.w_CATIPBAR;
             ,CANUMDEQ=this.w_CANUMDEQ;
             ,CAFLSTAM=this.w_CAFLSTAM;
             ,CAPUBWEB=this.w_CAPUBWEB;
             ,CAOPERAT=this.w_CAOPERAT;
             ,CAMOLTIP=this.w_CAMOLTIP;
             ,CADTOBSO=this.w_CADTOBSO;
             ,CAMINVEN=this.w_CAMINVEN;
             ,CATIPCO3=this.w_CATIPCO3;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CADTINVA=this.w_CADTINVA;
             ,CATIPMA3=this.w_CATIPMA3;
             ,CAPESNE3=this.w_CAPESNE3;
             ,CAPESLO3=this.w_CAPESLO3;
             ,CADESVO3=this.w_CADESVO3;
             ,CAUMVOL3=this.w_CAUMVOL3;
             ,CATPCON3=this.w_CATPCON3;
             ,CAPZCON3=this.w_CAPZCON3;
             ,CACOCOL3=this.w_CACOCOL3;
             ,CADIMLU3=this.w_CADIMLU3;
             ,CADIMLA3=this.w_CADIMLA3;
             ,CADIMAL3=this.w_CADIMAL3;
             ,CAUMDIM3=this.w_CAUMDIM3;
             ,CAFLCON3=this.w_CAFLCON3;
             ,CALENSCF=this.w_CALENSCF;
             ,CACODFAS=this.w_CACODFAS;
             ,CACODTIP=this.w_CACODTIP;
             ,CACODCLF=this.w_CACODCLF;
             ,CACODVAL=this.w_CACODVAL;
             ,CAFLGESC=this.w_CAFLGESC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MTA : Saving
      this.GSMA_MTA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CACODICE,"LGCODICE";
             )
      this.GSMA_MTA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMA_MTA : Deleting
    this.GSMA_MTA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CACODICE,"LGCODICE";
           )
    this.GSMA_MTA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.KEY_ARTI_IDX,i_nConn)
      *
      * delete KEY_ARTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CA__TIPO<>.w_CA__TIPO
            .w_CACODART = SPACE(20)
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_CACODART<>.w_CACODART
            .w_CATIPCON = IIF(IsAlt(),IIF(.w_TIPART='DE','D',IIF(.w_TIPART='FO','I','M')),'R')
        endif
        if .o_DESART<>.w_DESART
            .w_CADESART = .w_DESART
        endif
        .DoRTCalc(6,6,.t.)
        if .o_CATIPCON<>.w_CATIPCON
            .w_CA__TIPO = IIF(.w_CATIPCON='I', 'F', IIF(.w_CATIPCON $ 'CF', 'R', .w_CATIPCON))
        endif
        if .o_CATIPCON<>.w_CATIPCON
            .w_CACODCON = SPACE(15)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,13,.t.)
          .link_1_14('Full')
        .DoRTCalc(15,18,.t.)
        if .o_CAUNIMIS<>.w_CAUNIMIS.or. .o_FLFRAZ<>.w_FLFRAZ.or. .o_F2FRAZ<>.w_F2FRAZ
            .w_CANUMDEQ = IIF(((.w_FLFRAZ<>'S' and empty(nvl(.w_CAUNIMIS,''))) or (not empty(nvl(.w_CAUNIMIS,'')) AND .w_F2FRAZ<>'S')) and Not Empty(.w_CANUMDEQ) ,.w_CANUMDEQ,'0')
        endif
        .DoRTCalc(20,22,.t.)
            .w_CODICE = .w_CACODICE
        .DoRTCalc(24,24,.t.)
        if .o_UNMIS1<>.w_UNMIS1.or. .o_CAUNIMIS<>.w_CAUNIMIS
            .w_CAMOLTIP = IIF( Not Empty ( .w_CAUNIMIS ) ,  1 , 0 )
        endif
        .DoRTCalc(26,33,.t.)
        if .o_CATIPCON<>.w_CATIPCON
            .w_TIPKEY = iif(.w_CATIPCON='M','FM',iif(.w_CATIPCON='I','FO',iif(.w_CATIPCON='D','DE','')))
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(35,49,.t.)
            .w_COD1 = .w_CACODICE
            .w_DES1 = .w_CADESART
        if .o_CATIPBAR<>.w_CATIPBAR
          .Calculate_NMFDQGKMES()
        endif
        if .o_CAFLIMBA<>.w_CAFLIMBA.or. .o_CATIPBAR<>.w_CATIPBAR
          .Calculate_FMTKNNYGHP()
        endif
        .DoRTCalc(52,54,.t.)
        if .o_CACODICE<>.w_CACODICE
            .w_OCODCON = .w_CACODCON
        endif
        if .o_CACODICE<>.w_CACODICE
            .w_OTIPCON = .w_CATIPCON
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(57,64,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
    endwith
  return

  proc Calculate_NMFDQGKMES()
    with this
          * --- Check flag imballo
          .w_CAFLIMBA = IIF( NOT INLIST( .w_CATIPBAR, '2', 'C' ), 'N', .w_CAFLIMBA )
    endwith
  endproc
  proc Calculate_FMTKNNYGHP()
    with this
          * --- Sbianca tipo collo
          .w_CATIPCO3 = IIF(.w_CAFLIMBA <> 'S', SPACE(5), .w_CATIPCO3)
          .w_DESCOL = IIF(.w_CAFLIMBA <> 'S', SPACE(35), .w_DESCOL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACODART_1_2.enabled = this.oPgFrm.Page1.oPag.oCACODART_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCATIPCON_1_4.enabled = this.oPgFrm.Page1.oPag.oCATIPCON_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCADESART_1_5.enabled = this.oPgFrm.Page1.oPag.oCADESART_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCACODCON_1_8.enabled = this.oPgFrm.Page1.oPag.oCACODCON_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCAUNIMIS_1_16.enabled = this.oPgFrm.Page1.oPag.oCAUNIMIS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.enabled = this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.enabled = this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCAOPERAT_1_24.enabled = this.oPgFrm.Page1.oPag.oCAOPERAT_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCAMOLTIP_1_25.enabled = this.oPgFrm.Page1.oPag.oCAMOLTIP_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.enabled = this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAFLIMBA_1_3.visible=!this.oPgFrm.Page1.oPag.oCAFLIMBA_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATIPBAR_1_18.visible=!this.oPgFrm.Page1.oPag.oCATIPBAR_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.visible=!this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCAFLSTAM_1_20.visible=!this.oPgFrm.Page1.oPag.oCAFLSTAM_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.visible=!this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.visible=!this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_64.visible=!this.oPgFrm.Page1.oPag.oBtn_1_64.mHide()
    this.oPgFrm.Page1.oPag.oCODESC_1_65.visible=!this.oPgFrm.Page1.oPag.oCODESC_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oDESCOL_1_70.visible=!this.oPgFrm.Page1.oPag.oDESCOL_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsma_aca
    * --- Controllo per evitare che venga modificata la Provenienza o il Tipo codifica
    * --- su un codice di ricerca che contiene suffisso legato al Cliente/Fornitore
    * --- Sarebbe impossibile modificare il codice in quanto campo chiave
    If This.w_CALENSCF <>0 And this.cFunction='Edit'
       If cEvent='w_CACODCON Changed'
          Ah_ErrorMsg('Impossibile modificare provenienza%0Il codice � comprensivo di suffisso','!')
          This.w_CATIPCON = This.w_OTIPCON
          This.w_CACODCON = This.w_OCODCON
       Endif
       If cEvent='w_CATIPCON Changed'
          Ah_ErrorMsg('Impossibile modificare tipo codifica%0Il codice � comprensivo di suffisso','!')
          This.w_CATIPCON = This.w_OTIPCON
          This.w_CACODCON = This.w_OCODCON
       Endif
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODART
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CACODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CACODART))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CACODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CACODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCACODART_1_2'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA_ACA.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARTIPART,ARPUBWEB,ARFLAPCA,ARFLPECO,ARCODTIP,ARCODVAL,ARCODCLF,ARFLGESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARPUBWEB = NVL(_Link_.ARPUBWEB,space(1))
      this.w_ARFLAPCA = NVL(_Link_.ARFLAPCA,space(1))
      this.w_ARFLPECO = NVL(_Link_.ARFLPECO,space(1))
      this.w_CACODTIP = NVL(_Link_.ARCODTIP,space(35))
      this.w_CACODVAL = NVL(_Link_.ARCODVAL,space(35))
      this.w_CACODCLF = NVL(_Link_.ARCODCLF,space(5))
      this.w_CAFLGESC = NVL(_Link_.ARFLGESC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UNMIS1 = space(3)
      this.w_TIPART = space(2)
      this.w_ARPUBWEB = space(1)
      this.w_ARFLAPCA = space(1)
      this.w_ARFLPECO = space(1)
      this.w_CACODTIP = space(35)
      this.w_CACODVAL = space(35)
      this.w_CACODCLF = space(5)
      this.w_CAFLGESC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CACODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UNMIS1 = space(3)
        this.w_TIPART = space(2)
        this.w_ARPUBWEB = space(1)
        this.w_ARFLAPCA = space(1)
        this.w_ARFLPECO = space(1)
        this.w_CACODTIP = space(35)
        this.w_CACODVAL = space(35)
        this.w_CACODCLF = space(5)
        this.w_CAFLGESC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 12 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+12<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.ARCODART as ARCODART102"+ ",link_1_2.ARDESART as ARDESART102"+ ",link_1_2.ARDTOBSO as ARDTOBSO102"+ ",link_1_2.ARUNMIS1 as ARUNMIS1102"+ ",link_1_2.ARTIPART as ARTIPART102"+ ",link_1_2.ARPUBWEB as ARPUBWEB102"+ ",link_1_2.ARFLAPCA as ARFLAPCA102"+ ",link_1_2.ARFLPECO as ARFLPECO102"+ ",link_1_2.ARCODTIP as ARCODTIP102"+ ",link_1_2.ARCODVAL as ARCODVAL102"+ ",link_1_2.ARCODCLF as ARCODCLF102"+ ",link_1_2.ARFLGESC as ARFLGESC102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on KEY_ARTI.CACODART=link_1_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and KEY_ARTI.CACODART=link_1_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CATIPCON;
                     ,'ANCODICE',trim(this.w_CACODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CATIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCACODCON_1_8'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CATIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CATIPCON;
                       ,'ANCODICE',this.w_CACODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(36))
      this.w_INDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w____CAP = NVL(_Link_.AN___CAP,space(8))
      this.w_LOCALI = NVL(_Link_.ANLOCALI,space(28))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CODESC = NVL(_Link_.ANCODESC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CACODCON = space(15)
      endif
      this.w_DESCON = space(36)
      this.w_INDIRI = space(35)
      this.w____CAP = space(8)
      this.w_LOCALI = space(28)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODESC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CACODCON = space(15)
        this.w_DESCON = space(36)
        this.w_INDIRI = space(35)
        this.w____CAP = space(8)
        this.w_LOCALI = space(28)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODESC = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANINDIRI as ANINDIRI108"+ ",link_1_8.AN___CAP as AN___CAP108"+ ",link_1_8.ANLOCALI as ANLOCALI108"+ ",link_1_8.ANDTOBSO as ANDTOBSO108"+ ",link_1_8.ANCODESC as ANCODESC108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on KEY_ARTI.CACODCON=link_1_8.ANCODICE"+" and KEY_ARTI.CATIPCON=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and KEY_ARTI.CACODCON=link_1_8.ANCODICE(+)"'+'+" and KEY_ARTI.CATIPCON=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUNIMIS
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CAUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CAUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCAUNIMIS_1_16'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CAUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CAUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_F2FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUNIMIS = space(3)
      endif
      this.w_F2FRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.UMCODICE as UMCODICE116"+ ",link_1_16.UMFLFRAZ as UMFLFRAZ116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on KEY_ARTI.CAUNIMIS=link_1_16.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and KEY_ARTI.CAUNIMIS=link_1_16.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CATIPCO3
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATIPCO3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CATIPCO3)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CATIPCO3))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATIPCO3)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATIPCO3) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCATIPCO3_1_29'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATIPCO3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CATIPCO3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATIPCO3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATIPCO3 = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATIPCO3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_COLL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.TCCODICE as TCCODICE129"+ ",link_1_29.TCDESCRI as TCDESCRI129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on KEY_ARTI.CATIPCO3=link_1_29.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and KEY_ARTI.CATIPCO3=link_1_29.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_1.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_1.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODART_1_2.value==this.w_CACODART)
      this.oPgFrm.Page1.oPag.oCACODART_1_2.value=this.w_CACODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLIMBA_1_3.RadioValue()==this.w_CAFLIMBA)
      this.oPgFrm.Page1.oPag.oCAFLIMBA_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCON_1_4.RadioValue()==this.w_CATIPCON)
      this.oPgFrm.Page1.oPag.oCATIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESART_1_5.value==this.w_CADESART)
      this.oPgFrm.Page1.oPag.oCADESART_1_5.value=this.w_CADESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESSUP_1_6.value==this.w_CADESSUP)
      this.oPgFrm.Page1.oPag.oCADESSUP_1_6.value=this.w_CADESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODCON_1_8.value==this.w_CACODCON)
      this.oPgFrm.Page1.oPag.oCACODCON_1_8.value=this.w_CACODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRI_1_10.value==this.w_INDIRI)
      this.oPgFrm.Page1.oPag.oINDIRI_1_10.value=this.w_INDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.o___CAP_1_11.value==this.w____CAP)
      this.oPgFrm.Page1.oPag.o___CAP_1_11.value=this.w____CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCALI_1_12.value==this.w_LOCALI)
      this.oPgFrm.Page1.oPag.oLOCALI_1_12.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_13.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_13.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_1_14.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_1_14.value=this.w_UNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUNIMIS_1_16.value==this.w_CAUNIMIS)
      this.oPgFrm.Page1.oPag.oCAUNIMIS_1_16.value=this.w_CAUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPBAR_1_18.RadioValue()==this.w_CATIPBAR)
      this.oPgFrm.Page1.oPag.oCATIPBAR_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.RadioValue()==this.w_CANUMDEQ)
      this.oPgFrm.Page1.oPag.oCANUMDEQ_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLSTAM_1_20.RadioValue()==this.w_CAFLSTAM)
      this.oPgFrm.Page1.oPag.oCAFLSTAM_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.RadioValue()==this.w_CAPUBWEB)
      this.oPgFrm.Page1.oPag.oCAPUBWEB_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOPERAT_1_24.RadioValue()==this.w_CAOPERAT)
      this.oPgFrm.Page1.oPag.oCAOPERAT_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMOLTIP_1_25.value==this.w_CAMOLTIP)
      this.oPgFrm.Page1.oPag.oCAMOLTIP_1_25.value=this.w_CAMOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCADTOBSO_1_26.value==this.w_CADTOBSO)
      this.oPgFrm.Page1.oPag.oCADTOBSO_1_26.value=this.w_CADTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMINVEN_1_27.value==this.w_CAMINVEN)
      this.oPgFrm.Page1.oPag.oCAMINVEN_1_27.value=this.w_CAMINVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.value==this.w_CATIPCO3)
      this.oPgFrm.Page1.oPag.oCATIPCO3_1_29.value=this.w_CATIPCO3
    endif
    if not(this.oPgFrm.Page1.oPag.oCADTINVA_1_46.value==this.w_CADTINVA)
      this.oPgFrm.Page1.oPag.oCADTINVA_1_46.value=this.w_CADTINVA
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD1_2_2.value==this.w_COD1)
      this.oPgFrm.Page2.oPag.oCOD1_2_2.value=this.w_COD1
    endif
    if not(this.oPgFrm.Page2.oPag.oDES1_2_3.value==this.w_DES1)
      this.oPgFrm.Page2.oPag.oDES1_2_3.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESC_1_65.value==this.w_CODESC)
      this.oPgFrm.Page1.oPag.oCODESC_1_65.value=this.w_CODESC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOL_1_70.value==this.w_DESCOL)
      this.oPgFrm.Page1.oPag.oDESCOL_1_70.value=this.w_DESCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODFAS_1_79.value==this.w_CACODFAS)
      this.oPgFrm.Page1.oPag.oCACODFAS_1_79.value=this.w_CACODFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODTIP_1_85.value==this.w_CACODTIP)
      this.oPgFrm.Page1.oPag.oCACODTIP_1_85.value=this.w_CACODTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODCLF_1_86.value==this.w_CACODCLF)
      this.oPgFrm.Page1.oPag.oCACODCLF_1_86.value=this.w_CACODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODVAL_1_87.value==this.w_CACODVAL)
      this.oPgFrm.Page1.oPag.oCACODVAL_1_87.value=this.w_CACODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLGESC_1_91.RadioValue()==this.w_CAFLGESC)
      this.oPgFrm.Page1.oPag.oCAFLGESC_1_91.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'KEY_ARTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CACODICE)) or not(IIF(.cFunction='Load',g_FLCESC='S' Or chkcodar('K',.w_CACODICE),.T.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CACODART)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODART_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CACODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   ((empty(.w_CACODCON)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_CATIPCON$"CF")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODCON_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CACODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   ((empty(.w_CATIPBAR)) or not(CALCBAR(.w_CODICE, .w_CATIPBAR, 0)))  and not(IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATIPBAR_1_18.SetFocus()
            i_bnoObbl = !empty(.w_CATIPBAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(g_perpqt>=val(.w_CANUMDEQ))  and not(!(.w_CATIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_TIPART$'FM-FO-DE'))  and ((.w_FLFRAZ<>'S' and empty(nvl(.w_CAUNIMIS,''))) or (not empty(nvl(.w_CAUNIMIS,'')) AND .w_F2FRAZ<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCANUMDEQ_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero dei decimali selezionato � maggiore dei decimali impostati nei dati azienda.")
          case   (empty(.w_CAMOLTIP))  and (.w_CA__TIPO<>'D' AND .w_UNMIS1<>.w_CAUNIMIS AND Not Empty( .w_CAUNIMIS ) )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMOLTIP_1_25.SetFocus()
            i_bnoObbl = !empty(.w_CAMOLTIP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLFRAZ <>'S' Or .w_CAMINVEN =Int(.w_CAMINVEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMINVEN_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'unit� di misura principale non � frazionabile")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MTA.CheckForm()
      if i_bres
        i_bres=  .GSMA_MTA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsma_aca
      * --- Gestione Suffisso codice di Ricerca. al salvataggio controllo se devo inserirlo
      
      If .w_CATIPBAR='0' and g_FLCESC='S'And Not Empty(.w_CODESC) And .cFunction = 'Load'
         If Len(Alltrim(.w_CACODICE) + Alltrim(.w_CODESC) ) > 20
            If Ah_YesNo('Il codice comprensivo del suffisso %1 supera 20 caratteri%0Eseguo troncamento degli ultimi caratteri?','', Alltrim(.w_CODESC))
               .w_CALENSCF = Len(Alltrim(.w_CODESC))
               .w_CACODICE = SubStr(Alltrim(.w_CACODICE),1,20 - .w_CALENSCF) + Alltrim(.w_CODESC)
            Else
               i_bRes = .f.
            Endif
         Else
         .w_CALENSCF = Len(Alltrim(.w_CODESC))
         .w_CACODICE = Alltrim(.w_CACODICE) + Alltrim(.w_CODESC)
         Endif
      Endif
      
      if i_bRes=.t.
         * Controllo di Congruita' Articolo
         i_bRes = CHKTIPO(.w_TIPART, .w_CATIPCON)
      endif
      if i_bRes
        i_bRes =CHKMEMO(.w_CADESSUP)
      endif
      
      if i_bRes and g_COAC='S'
        L_NUMEROCONTRIBUTICONAI = GSAR_BE3( THIS , "KEY" )
        IF L_NUMEROCONTRIBUTICONAI > 0
          IF !EMPTY(THIS.w_CAUNIMIS)  AND (THIS.w_CAPESNE3 = 0 OR THIS.w_CAPESLO3 = 0) AND THIS.w_ARFLAPCA = 'S'
            i_bRes = AH_YESNO("Sull'articolo sono stati inseriti contributi accessori a peso, ma nel codice il peso netto o lordo%0relativo all'unit� di misura alternativa non � stato specificato.%0Si desidera confermare?")
            i_cErrorMsg = ''
          ENDIF
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODICE = this.w_CACODICE
    this.o_CACODART = this.w_CACODART
    this.o_CAFLIMBA = this.w_CAFLIMBA
    this.o_CATIPCON = this.w_CATIPCON
    this.o_CA__TIPO = this.w_CA__TIPO
    this.o_CACODCON = this.w_CACODCON
    this.o_DESART = this.w_DESART
    this.o_UNMIS1 = this.w_UNMIS1
    this.o_FLFRAZ = this.w_FLFRAZ
    this.o_CAUNIMIS = this.w_CAUNIMIS
    this.o_F2FRAZ = this.w_F2FRAZ
    this.o_CATIPBAR = this.w_CATIPBAR
    * --- GSMA_MTA : Depends On
    this.GSMA_MTA.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=Not (this.w_CATIPCON $ 'RMIDA' AND (this.w_CACODICE= this.w_CACODART))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Codice di tipo interno, impossibile eliminare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsma_acaPag1 as StdContainer
  Width  = 663
  height = 411
  stdWidth  = 663
  stdheight = 411
  resizeXpos=350
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_1_1 as StdField with uid="VIBXVFEYUH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca",;
    HelpContextID = 150406549,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=170, Left=87, Top=8, InputMask=replicate('X',20)

  func oCACODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.cFunction='Load',g_FLCESC='S' Or chkcodar('K',.w_CACODICE),.T.))
    endwith
    return bRes
  endfunc

  add object oCACODART_1_2 as StdField with uid="ZMGPFLZMIJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODART", cQueryName = "CACODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Codice associato",;
    HelpContextID = 16188806,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=365, Top=8, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CACODART"

  func oCACODART_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCACODART_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODART_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODART_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCACODART_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA_ACA.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCACODART_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CACODART
     i_obj.ecpSave()
  endproc

  add object oCAFLIMBA_1_3 as StdCheck with uid="TRDYGPDAGM",rtseq=3,rtrep=.f.,left=539, top=8, caption="Imballo",;
    ToolTipText = "Se attivo: il codice di ricerca � di tipo imballo",;
    HelpContextID = 78239129,;
    cFormVar="w_CAFLIMBA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLIMBA_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCAFLIMBA_1_3.GetRadio()
    this.Parent.oContained.w_CAFLIMBA = this.RadioValue()
    return .t.
  endfunc

  func oCAFLIMBA_1_3.SetRadio()
    this.Parent.oContained.w_CAFLIMBA=trim(this.Parent.oContained.w_CAFLIMBA)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLIMBA=='S',1,;
      0)
  endfunc

  func oCAFLIMBA_1_3.mHide()
    with this.Parent.oContained
      return (NOT INLIST(.w_CATIPBAR,'2','C'))
    endwith
  endfunc


  add object oCATIPCON_1_4 as StdCombo with uid="YKPEMEDBZO",rtseq=4,rtrep=.f.,left=525,top=61,width=134,height=21;
    , ToolTipText = "Indica la provenienza del codice: interna, cliente o fornitore o altro";
    , HelpContextID = 238810508;
    , cFormVar="w_CATIPCON",RowSource=""+"Interna,"+"Cliente,"+"Fornitore,"+"A quantit� e valore,"+"A valore,"+"Descrittivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPCON_1_4.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'M',;
    iif(this.value =5,'I',;
    iif(this.value =6,'D',;
    space(1))))))))
  endfunc
  func oCATIPCON_1_4.GetRadio()
    this.Parent.oContained.w_CATIPCON = this.RadioValue()
    return .t.
  endfunc

  func oCATIPCON_1_4.SetRadio()
    this.Parent.oContained.w_CATIPCON=trim(this.Parent.oContained.w_CATIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPCON=='R',1,;
      iif(this.Parent.oContained.w_CATIPCON=='C',2,;
      iif(this.Parent.oContained.w_CATIPCON=='F',3,;
      iif(this.Parent.oContained.w_CATIPCON=='M',4,;
      iif(this.Parent.oContained.w_CATIPCON=='I',5,;
      iif(this.Parent.oContained.w_CATIPCON=='D',6,;
      0))))))
  endfunc

  func oCATIPCON_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CACODICE<>.w_CACODART AND NOT ISALT())
    endwith
   endif
  endfunc

  func oCATIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CACODCON)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCADESART_1_5 as StdField with uid="BZDEGNBLGN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CADESART", cQueryName = "CADESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del codice",;
    HelpContextID = 1111430,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=87, Top=33, InputMask=replicate('X',40)

  func oCADESART_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DESART))
    endwith
   endif
  endfunc

  add object oCADESSUP_1_6 as StdMemo with uid="CYCKFJPCIQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CADESSUP", cQueryName = "CADESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione supplementare",;
    HelpContextID = 32442998,;
   bGlobalFont=.t.,;
    Height=59, Width=289, Left=87, Top=61

  add object oCACODCON_1_8 as StdField with uid="WBANCBNTJW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CACODCON", cQueryName = "CACODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente / fornitore di provenienza",;
    HelpContextID = 251069836,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=512, Top=91, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CATIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CACODCON"

  func oCACODCON_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPCON$"CF")
    endwith
   endif
  endfunc

  func oCACODCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CATIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CATIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCACODCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCACODCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CATIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CACODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_9 as StdField with uid="QIJOXOJJMO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 55706058,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=231, Left=427, Top=114, InputMask=replicate('X',36)

  add object oINDIRI_1_10 as StdField with uid="YBBTVSSINT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 136112250,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=231, Left=427, Top=135, InputMask=replicate('X',35)

  add object o___CAP_1_11 as StdField with uid="MISLWQZKGU",rtseq=11,rtrep=.f.,;
    cFormVar = "w____CAP", cQueryName = "___CAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 36775450,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=427, Top=155, InputMask=replicate('X',8)

  add object oLOCALI_1_12 as StdField with uid="DFTCBZHXNU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(28), bMultilanguage =  .f.,;
    HelpContextID = 142931786,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=170, Left=489, Top=156, InputMask=replicate('X',28)

  add object oDESART_1_13 as StdField with uid="XBHQTNLAOL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220463562,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=380, Top=33, InputMask=replicate('X',40)

  add object oUNMIS1_1_14 as StdField with uid="VPHIREHMPA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_UNMIS1", cQueryName = "UNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 808890,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=87, Top=227, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNMIS1"

  func oUNMIS1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAUNIMIS_1_16 as StdField with uid="XHABLYISZV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CAUNIMIS", cQueryName = "CAUNIMIS",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura alternativa",;
    HelpContextID = 190388857,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=160, Top=227, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_CAUNIMIS"

  func oCAUNIMIS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CA__TIPO<>'D' )
    endwith
   endif
  endfunc

  func oCAUNIMIS_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUNIMIS_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUNIMIS_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCAUNIMIS_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oCAUNIMIS_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_CAUNIMIS
     i_obj.ecpSave()
  endproc


  add object oCATIPBAR_1_18 as StdCombo with uid="ECUVXHSMCU",rtseq=18,rtrep=.f.,left=87,top=128,width=149,height=21;
    , HelpContextID = 255587720;
    , cFormVar="w_CATIPBAR",RowSource=""+"No,"+"EAN 8,"+"EAN 13,"+"ALFA 39,"+"UPC A,"+"UPC E,"+"2D5,"+"2D5 interleave,"+"Farmaceutico,"+"198,"+"EAN 13 peso variabile,"+"EAN 13 peso var. + digit,"+"EAN / UCC 14", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPBAR_1_18.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    iif(this.value =6,'5',;
    iif(this.value =7,'6',;
    iif(this.value =8,'7',;
    iif(this.value =9,'8',;
    iif(this.value =10,'9',;
    iif(this.value =11,'A',;
    iif(this.value =12,'B',;
    iif(this.value =13,'C',;
    space(1)))))))))))))))
  endfunc
  func oCATIPBAR_1_18.GetRadio()
    this.Parent.oContained.w_CATIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oCATIPBAR_1_18.SetRadio()
    this.Parent.oContained.w_CATIPBAR=trim(this.Parent.oContained.w_CATIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPBAR=='0',1,;
      iif(this.Parent.oContained.w_CATIPBAR=='1',2,;
      iif(this.Parent.oContained.w_CATIPBAR=='2',3,;
      iif(this.Parent.oContained.w_CATIPBAR=='3',4,;
      iif(this.Parent.oContained.w_CATIPBAR=='4',5,;
      iif(this.Parent.oContained.w_CATIPBAR=='5',6,;
      iif(this.Parent.oContained.w_CATIPBAR=='6',7,;
      iif(this.Parent.oContained.w_CATIPBAR=='7',8,;
      iif(this.Parent.oContained.w_CATIPBAR=='8',9,;
      iif(this.Parent.oContained.w_CATIPBAR=='9',10,;
      iif(this.Parent.oContained.w_CATIPBAR=='A',11,;
      iif(this.Parent.oContained.w_CATIPBAR=='B',12,;
      iif(this.Parent.oContained.w_CATIPBAR=='C',13,;
      0)))))))))))))
  endfunc

  func oCATIPBAR_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCATIPBAR_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CALCBAR(.w_CODICE, .w_CATIPBAR, 0))
    endwith
    return bRes
  endfunc


  add object oCANUMDEQ_1_19 as StdCombo with uid="WZWQASMHMR",rtseq=19,rtrep=.f.,left=311,top=128,width=65,height=22;
    , HelpContextID = 44018295;
    , cFormVar="w_CANUMDEQ",RowSource=""+"Zero,"+"Uno,"+"Due,"+"Tre", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Il numero dei decimali selezionato � maggiore dei decimali impostati nei dati azienda.";
  , bGlobalFont=.t.


  func oCANUMDEQ_1_19.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    space(1))))))
  endfunc
  func oCANUMDEQ_1_19.GetRadio()
    this.Parent.oContained.w_CANUMDEQ = this.RadioValue()
    return .t.
  endfunc

  func oCANUMDEQ_1_19.SetRadio()
    this.Parent.oContained.w_CANUMDEQ=trim(this.Parent.oContained.w_CANUMDEQ)
    this.value = ;
      iif(this.Parent.oContained.w_CANUMDEQ=='0',1,;
      iif(this.Parent.oContained.w_CANUMDEQ=='1',2,;
      iif(this.Parent.oContained.w_CANUMDEQ=='2',3,;
      iif(this.Parent.oContained.w_CANUMDEQ=='3',4,;
      0))))
  endfunc

  func oCANUMDEQ_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLFRAZ<>'S' and empty(nvl(.w_CAUNIMIS,''))) or (not empty(nvl(.w_CAUNIMIS,'')) AND .w_F2FRAZ<>'S'))
    endwith
   endif
  endfunc

  func oCANUMDEQ_1_19.mHide()
    with this.Parent.oContained
      return (!(.w_CATIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_TIPART$'FM-FO-DE'))
    endwith
  endfunc

  func oCANUMDEQ_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (g_perpqt>=val(.w_CANUMDEQ))
    endwith
    return bRes
  endfunc

  add object oCAFLSTAM_1_20 as StdCheck with uid="FNXAQIFKOZ",rtseq=20,rtrep=.f.,left=87, top=156, caption="Stampa etichette",;
    ToolTipText = "Se attivo: il codice compare nella stampa etichette",;
    HelpContextID = 218748301,;
    cFormVar="w_CAFLSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLSTAM_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLSTAM_1_20.GetRadio()
    this.Parent.oContained.w_CAFLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oCAFLSTAM_1_20.SetRadio()
    this.Parent.oContained.w_CAFLSTAM=trim(this.Parent.oContained.w_CAFLSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLSTAM=='S',1,;
      0)
  endfunc

  func oCAFLSTAM_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCAPUBWEB_1_21 as StdCheck with uid="IJBNWFSRXC",rtseq=21,rtrep=.f.,left=87, top=178, caption="Pubblica su web",;
    ToolTipText = "Se attivo: codice di ricerca gestito dall'applicazione Web application",;
    HelpContextID = 82823784,;
    cFormVar="w_CAPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAPUBWEB_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAPUBWEB_1_21.GetRadio()
    this.Parent.oContained.w_CAPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCAPUBWEB_1_21.SetRadio()
    this.Parent.oContained.w_CAPUBWEB=trim(this.Parent.oContained.w_CAPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_CAPUBWEB=='S',1,;
      0)
  endfunc

  func oCAPUBWEB_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(ALLTRIM(.w_CACODICE)==ALLTRIM(.w_CACODART)) AND .w_CATIPBAR='0' AND .w_ARPUBWEB <> 'N' AND (g_IZCP<>'N' OR g_CPIN='S' OR g_REVI='S'))
    endwith
   endif
  endfunc

  func oCAPUBWEB_1_21.mHide()
    with this.Parent.oContained
      return (g_ECRM = 'S' or Isalt())
    endwith
  endfunc


  add object oCAOPERAT_1_24 as StdCombo with uid="IVBDGSMHZX",rtseq=24,rtrep=.f.,left=226,top=227,width=35,height=21;
    , ToolTipText = "Operatore (moltip. o divis.) tra l'U.M. principale e quella alternativa";
    , HelpContextID = 266683782;
    , cFormVar="w_CAOPERAT",RowSource=""+"X,"+"/", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAOPERAT_1_24.RadioValue()
    return(iif(this.value =1,'*',;
    iif(this.value =2,'/',;
    space(1))))
  endfunc
  func oCAOPERAT_1_24.GetRadio()
    this.Parent.oContained.w_CAOPERAT = this.RadioValue()
    return .t.
  endfunc

  func oCAOPERAT_1_24.SetRadio()
    this.Parent.oContained.w_CAOPERAT=trim(this.Parent.oContained.w_CAOPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_CAOPERAT=='*',1,;
      iif(this.Parent.oContained.w_CAOPERAT=='/',2,;
      0))
  endfunc

  func oCAOPERAT_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CA__TIPO<>'D' AND .w_UNMIS1<>.w_CAUNIMIS)
    endwith
   endif
  endfunc

  add object oCAMOLTIP_1_25 as StdField with uid="WCNSQFKOVG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CAMOLTIP", cQueryName = "CAMOLTIP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di rapporto tra l'U.M. principale e quella alternativa",;
    HelpContextID = 42572406,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=275, Top=227, cSayPict='"99999.9999"', cGetPict='"99999.9999"'

  func oCAMOLTIP_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CA__TIPO<>'D' AND .w_UNMIS1<>.w_CAUNIMIS AND Not Empty( .w_CAUNIMIS ) )
    endwith
   endif
  endfunc

  add object oCADTOBSO_1_26 as StdField with uid="CFJJNKPRIO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CADTOBSO", cQueryName = "CADTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di obsolescenza",;
    HelpContextID = 255980939,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=580, Top=238, tabstop=.f.

  add object oCAMINVEN_1_27 as StdField with uid="PETABLSIBL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CAMINVEN", cQueryName = "CAMINVEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'unit� di misura principale non � frazionabile",;
    ToolTipText = "Quantitativo minimo vendibile/ordinabile espresso nell'unit� di misura principale",;
    HelpContextID = 77830772,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=275, Top=253, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oCAMINVEN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FLFRAZ <>'S' Or .w_CAMINVEN =Int(.w_CAMINVEN))
    endwith
    return bRes
  endfunc

  add object oCATIPCO3_1_29 as StdField with uid="OJJQDWRKKA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CATIPCO3", cQueryName = "CATIPCO3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia collo",;
    HelpContextID = 238810535,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CATIPCO3"

  func oCATIPCO3_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLIMBA = 'S')
    endwith
   endif
  endfunc

  func oCATIPCO3_1_29.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_CATIPBAR,'2','C'))
    endwith
  endfunc

  func oCATIPCO3_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATIPCO3_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATIPCO3_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCATIPCO3_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCATIPCO3_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CATIPCO3
     i_obj.ecpSave()
  endproc

  add object oCADTINVA_1_46 as StdField with uid="ZBPZIGCZJY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CADTINVA", cQueryName = "CADTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 207489639,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=580, Top=213, tabstop=.f.


  add object oObj_1_57 as cp_runprogram with uid="ZHZYVSMEQZ",left=143, top=439, width=520,height=19,;
    caption='GSMA_BBK(w_CACODICE,w_CATIPBAR,B)',;
   bGlobalFont=.t.,;
    prg="GSMA_BBK(w_cacodice,w_catipbar,'B')",;
    cEvent = "w_CATIPBAR Changed,w_CACODICE Changed",;
    nPag=1;
    , HelpContextID = 167968936


  add object oBtn_1_64 as StdButton with uid="UXHQKIGYEP",left=610, top=282, width=48,height=45,;
    CpPicture="BMP\UNIMIS.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per impostare i dati tecnici relativi alla U.M. alternativa";
    , HelpContextID = 5622393;
    , tabstop=.f., caption='\<U.M. Alt.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      do GSMA_KUM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_64.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CAUNIMIS) AND NOT IsAlt())
      endwith
    endif
  endfunc

  func oBtn_1_64.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CAUNIMIS) OR IsAlt())
     endwith
    endif
  endfunc

  add object oCODESC_1_65 as StdField with uid="QACGXGNQPJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CODESC", cQueryName = "CODESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice suffisso cliente/fornitore",;
    HelpContextID = 235988954,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=580, Top=180, InputMask=replicate('X',5)

  func oCODESC_1_65.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESC) Or g_FLCESC<>'S' Or .w_CATIPBAR<>'0')
    endwith
  endfunc

  add object oDESCOL_1_70 as StdField with uid="KLZYFRALKE",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89260490,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=168, Top=280, InputMask=replicate('X',35)

  func oDESCOL_1_70.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_CATIPBAR,'2','C'))
    endwith
  endfunc

  add object oCACODFAS_1_79 as StdField with uid="TNJCIDKFXW",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CACODFAS", cQueryName = "CACODFAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 200738183,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=87, Top=305, InputMask=replicate('X',50)

  add object oCACODTIP_1_85 as StdField with uid="UWNCKSOVUI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CACODTIP", cQueryName = "CACODTIP",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica la tipologia di codice articolo (per esempio, TARIC, CPV, EAN, SSC, ...)",;
    HelpContextID = 34142838,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=87, Top=359, InputMask=replicate('X',35)

  add object oCACODCLF_1_86 as StdField with uid="MJWYUTIROE",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CACODCLF", cQueryName = "CACODCLF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 251069844,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=580, Top=359, InputMask=replicate('X',5)

  add object oCACODVAL_1_87 as StdField with uid="VCGAGDOJNU",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CACODVAL", cQueryName = "CACODVAL",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica il valore del codice articolo corrispondente alla tipologia riportata nell'elemento informativo 2.2.1.3.1 <CodiceTipo>",;
    HelpContextID = 200738190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=87, Top=387, InputMask=replicate('X',35)

  add object oCAFLGESC_1_91 as StdCheck with uid="IKXQKROTUZ",rtseq=64,rtrep=.f.,left=485, top=387, caption="Codice articolo esclusivo",;
    ToolTipText = "Codice articolo esclusivo",;
    HelpContextID = 214554007,;
    cFormVar="w_CAFLGESC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLGESC_1_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLGESC_1_91.GetRadio()
    this.Parent.oContained.w_CAFLGESC = this.RadioValue()
    return .t.
  endfunc

  func oCAFLGESC_1_91.SetRadio()
    this.Parent.oContained.w_CAFLGESC=trim(this.Parent.oContained.w_CAFLGESC)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLGESC=='S',1,;
      0)
  endfunc

  add object oStr_1_28 as StdString with uid="DVOYSVZAOK",Visible=.t., Left=14, Top=8,;
    Alignment=1, Width=71, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="XAYADRLQKT",Visible=.t., Left=261, Top=8,;
    Alignment=1, Width=102, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="VFFETFLJRM",Visible=.t., Left=431, Top=61,;
    Alignment=1, Width=91, Height=15,;
    Caption="Tipo codifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="WRUZVCLKMR",Visible=.t., Left=2, Top=33,;
    Alignment=1, Width=83, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="TTJNDIWAVH",Visible=.t., Left=14, Top=128,;
    Alignment=1, Width=71, Height=15,;
    Caption="Barcode:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="ZZBOZZWYZO",Visible=.t., Left=87, Top=204,;
    Alignment=0, Width=65, Height=15,;
    Caption="1^U.M.art."  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="CIAJRKOZVF",Visible=.t., Left=275, Top=204,;
    Alignment=0, Width=91, Height=15,;
    Caption="Parametro"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NRHYNJEBIN",Visible=.t., Left=140, Top=227,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="RGBWKNUOHN",Visible=.t., Left=226, Top=204,;
    Alignment=0, Width=41, Height=15,;
    Caption="Oper."  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="DGUUOYCXBT",Visible=.t., Left=160, Top=204,;
    Alignment=0, Width=58, Height=15,;
    Caption="U.M.cod."  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="SCFJWSNTEQ",Visible=.t., Left=455, Top=238,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="WJKEAIDVFJ",Visible=.t., Left=417, Top=91,;
    Alignment=1, Width=91, Height=15,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="UIUCUADDEU",Visible=.t., Left=475, Top=213,;
    Alignment=1, Width=103, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="OSZKKVHRQH",Visible=.t., Left=496, Top=181,;
    Alignment=1, Width=82, Height=18,;
    Caption="Suff. codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESC) Or g_FLCESC<>'S' Or .w_CATIPBAR<>'0')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="NIZZJSOETE",Visible=.t., Left=7, Top=280,;
    Alignment=1, Width=78, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_CAFLIMBA <> 'S' OR NOT INLIST(.w_CATIPBAR,'2','C'))
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="HKXXFJCHCU",Visible=.t., Left=58, Top=227,;
    Alignment=0, Width=27, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="PJMCFMQDDN",Visible=.t., Left=261, Top=8,;
    Alignment=1, Width=102, Height=18,;
    Caption="Codice associato:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="GPMNXDHWWA",Visible=.t., Left=100, Top=254,;
    Alignment=1, Width=167, Height=18,;
    Caption="Qt� min. vendibile/ordinabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="QLWDKFDNFK",Visible=.t., Left=6, Top=307,;
    Alignment=1, Width=79, Height=18,;
    Caption="Codifica fase:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="RDUOUVEPBH",Visible=.t., Left=241, Top=128,;
    Alignment=1, Width=68, Height=18,;
    Caption="N.decimali:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (!(.w_CATIPBAR$'A-B') OR .w_ARFLPECO<>'P' OR (.w_TIPART$'FM-FO-DE'))
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=4, Top=332,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="YTFHXMDYIQ",Visible=.t., Left=-1, Top=359,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="CJHWPVYXQL",Visible=.t., Left=-1, Top=387,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=408, Top=360,;
    Alignment=1, Width=170, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_1_84 as StdBox with uid="SJYDLIWYJC",left=4, top=350, width=655,height=2
enddefine
define class tgsma_acaPag2 as StdContainer
  Width  = 663
  height = 411
  stdWidth  = 663
  stdheight = 411
  resizeXpos=338
  resizeYpos=198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="USGIDAGDOL",left=0, top=90, width=641, height=224, bOnScreen=.t.;


  add object oCOD1_2_2 as StdField with uid="CRUFEFJFNY",rtseq=50,rtrep=.f.,;
    cFormVar = "w_COD1", cQueryName = "COD1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca",;
    HelpContextID = 162207782,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=170, Left=90, Top=8, InputMask=replicate('X',20)

  add object oDES1_2_3 as StdField with uid="TLKSYNXWLE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione alternativa",;
    HelpContextID = 162266678,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=90, Top=34, InputMask=replicate('X',40)

  add object oStr_2_4 as StdString with uid="SZLWGBAWLL",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=80, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="ANZPYJMYNI",Visible=.t., Left=1, Top=34,;
    Alignment=1, Width=86, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="ZFGIPXWNKP",Visible=.t., Left=5, Top=75,;
    Alignment=0, Width=363, Height=15,;
    Caption="Traduzioni in lingua"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_aca','KEY_ARTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODICE=KEY_ARTI.CACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
