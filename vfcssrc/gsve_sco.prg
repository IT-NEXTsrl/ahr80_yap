* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_sco                                                        *
*              Stampa contratti di vendita                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_39]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_sco",oParentObject))

* --- Class definition
define class tgsve_sco as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 511
  Height = 278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=177589655
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  CATECOMM_IDX = 0
  CON_TRAM_IDX = 0
  cPrg = "gsve_sco"
  cComment = "Stampa contratti di vendita"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCLF = space(1)
  w_CONTRINI = space(15)
  w_CONTRFIN = space(15)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  o_DATA2 = ctod('  /  /  ')
  w_RIFER = space(1)
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_CATCOM = space(3)
  o_CATCOM = space(3)
  w_DESCRINI = space(50)
  w_DESCRFIN = space(50)
  w_DESCRI = space(40)
  w_DESCAT = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DAT_INI = ctod('  /  /  ')
  w_DAT_FIN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_scoPag1","gsve_sco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONTRINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='CON_TRAM'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsve_sco
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCLF=space(1)
      .w_CONTRINI=space(15)
      .w_CONTRFIN=space(15)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_RIFER=space(1)
      .w_CODICE=space(15)
      .w_CATCOM=space(3)
      .w_DESCRINI=space(50)
      .w_DESCRFIN=space(50)
      .w_DESCRI=space(40)
      .w_DESCAT=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DAT_INI=ctod("  /  /  ")
      .w_DAT_FIN=ctod("  /  /  ")
        .w_TIPCLF = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CONTRINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CONTRFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_RIFER = 'C'
        .w_CODICE = SPACE(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODICE))
          .link_1_7('Full')
        endif
        .w_CATCOM = SPACE(3)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CATCOM))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
          .DoRTCalc(9,12,.f.)
        .w_OBTEST = .w_DATA1
          .DoRTCalc(14,14,.f.)
        .w_DAT_INI = IIF( Empty( .w_DATA1 ) , i_INIDAT ,  .w_DATA1)
        .w_DAT_FIN = IIF( Empty( .w_DATA2 ) , i_FINDAT , .w_DATA2 )
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_RIFER = 'C'
        if .o_CATCOM<>.w_CATCOM
            .w_CODICE = SPACE(15)
          .link_1_7('Full')
        endif
        if .o_CODICE<>.w_CODICE
            .w_CATCOM = SPACE(3)
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(9,12,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        .DoRTCalc(14,14,.t.)
        if .o_DATA1<>.w_DATA1
            .w_DAT_INI = IIF( Empty( .w_DATA1 ) , i_INIDAT ,  .w_DATA1)
        endif
        if .o_DATA2<>.w_DATA2
            .w_DAT_FIN = IIF( Empty( .w_DATA2 ) , i_FINDAT , .w_DATA2 )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODICE_1_7.enabled = this.oPgFrm.Page1.oPag.oCODICE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCATCOM_1_8.enabled = this.oPgFrm.Page1.oPag.oCATCOM_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CONTRINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_CONTRINI)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCLF;
                     ,'CONUMERO',trim(this.w_CONTRINI))
          select COTIPCLF,CONUMERO,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTRINI)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTRINI) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oCONTRINI_1_2'),i_cWhere,'GSAR_BCZ',"Contratti",'GSVE_MCO.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_CONTRINI);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCLF;
                       ,'CONUMERO',this.w_CONTRINI)
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRINI = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCRINI = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRINI = space(15)
      endif
      this.w_DESCRINI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CONTRFIN) OR .w_CONTRINI<=.w_CONTRFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
        endif
        this.w_CONTRINI = space(15)
        this.w_DESCRINI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_CONTRFIN)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCLF;
                     ,'CONUMERO',trim(this.w_CONTRFIN))
          select COTIPCLF,CONUMERO,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTRFIN)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTRFIN) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oCONTRFIN_1_3'),i_cWhere,'GSAR_BCZ',"Contratti",'GSVE_MCO.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_CONTRFIN);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCLF;
                       ,'CONUMERO',this.w_CONTRFIN)
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRFIN = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCRFIN = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRFIN = space(15)
      endif
      this.w_DESCRFIN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CONTRFIN>=.w_CONTRINI or empty(.w_CONTRINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
        endif
        this.w_CONTRFIN = space(15)
        this.w_DESCRFIN = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RIFER);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RIFER;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RIFER);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_RIFER);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_7'),i_cWhere,'GSAR_BZC',"Elenco clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RIFER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RIFER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RIFER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RIFER;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(NOT EMPTY(.w_CATCOM) OR NOT EMPTY(.w_CODICE)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
        endif
        this.w_CODICE = space(15)
        this.w_DESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_1_8'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAT = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_CATCOM) OR NOT EMPTY(.w_CODICE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente o la categoria commerciale")
        endif
        this.w_CATCOM = space(3)
        this.w_DESCAT = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCONTRINI_1_2.value==this.w_CONTRINI)
      this.oPgFrm.Page1.oPag.oCONTRINI_1_2.value=this.w_CONTRINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTRFIN_1_3.value==this.w_CONTRFIN)
      this.oPgFrm.Page1.oPag.oCONTRFIN_1_3.value=this.w_CONTRFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_4.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_4.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_5.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_5.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_7.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_7.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOM_1_8.value==this.w_CATCOM)
      this.oPgFrm.Page1.oPag.oCATCOM_1_8.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRINI_1_10.value==this.w_DESCRINI)
      this.oPgFrm.Page1.oPag.oDESCRINI_1_10.value=this.w_DESCRINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRFIN_1_11.value==this.w_DESCRFIN)
      this.oPgFrm.Page1.oPag.oDESCRFIN_1_11.value=this.w_DESCRFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_15.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_15.value=this.w_DESCAT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_CONTRFIN) OR .w_CONTRINI<=.w_CONTRFIN))  and not(empty(.w_CONTRINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONTRINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
          case   not((.w_CONTRFIN>=.w_CONTRINI or empty(.w_CONTRINI)))  and not(empty(.w_CONTRFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONTRFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale o inesistente")
          case   not(.w_DATA2>=.w_DATA1 OR EMPTY(.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date scorretto")
          case   not((NOT EMPTY(.w_CATCOM) OR NOT EMPTY(.w_CODICE)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (EMPTY(.w_CATCOM))  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
          case   not(NOT EMPTY(.w_CATCOM) OR NOT EMPTY(.w_CODICE))  and (EMPTY(.w_CODICE))  and not(empty(.w_CATCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCOM_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cliente o la categoria commerciale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA1 = this.w_DATA1
    this.o_DATA2 = this.w_DATA2
    this.o_CODICE = this.w_CODICE
    this.o_CATCOM = this.w_CATCOM
    return

enddefine

* --- Define pages as container
define class tgsve_scoPag1 as StdContainer
  Width  = 507
  height = 278
  stdWidth  = 507
  stdheight = 278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONTRINI_1_2 as StdField with uid="MOOWPGDEYV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONTRINI", cQueryName = "CONTRINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale o inesistente",;
    ToolTipText = "Contratto di inizio selezione",;
    HelpContextID = 116457105,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCLF", oKey_2_1="CONUMERO", oKey_2_2="this.w_CONTRINI"

  func oCONTRINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTRINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTRINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oCONTRINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Contratti",'GSVE_MCO.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oCONTRINI_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_TIPCLF
     i_obj.w_CONUMERO=this.parent.oContained.w_CONTRINI
     i_obj.ecpSave()
  endproc

  add object oCONTRFIN_1_3 as StdField with uid="OIEDUZJRSN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CONTRFIN", cQueryName = "CONTRFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale o inesistente",;
    ToolTipText = "Contratto di fine selezione",;
    HelpContextID = 101646708,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=60, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCLF", oKey_2_1="CONUMERO", oKey_2_2="this.w_CONTRFIN"

  func oCONTRFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTRFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTRFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oCONTRFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Contratti",'GSVE_MCO.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oCONTRFIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_TIPCLF
     i_obj.w_CONUMERO=this.parent.oContained.w_CONTRFIN
     i_obj.ecpSave()
  endproc

  add object oDATA1_1_4 as StdField with uid="ZKOUAMJGDG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date scorretto",;
    ToolTipText = "Inizio validit� listini",;
    HelpContextID = 233591606,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=118, Top=90

  add object oDATA2_1_5 as StdField with uid="IJEXIQZPBA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date scorretto",;
    ToolTipText = "Fine validit� listini",;
    HelpContextID = 234640182,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=118, Top=114

  func oDATA2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATA2>=.w_DATA1 OR EMPTY(.w_DATA2))
    endwith
    return bRes
  endfunc

  add object oCODICE_1_7 as StdField with uid="FYQNDVTVEX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cliente (non obsoleto) o la categoria commerciale",;
    ToolTipText = "Codice del cliente",;
    HelpContextID = 200056538,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=118, Top=148, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RIFER", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CATCOM))
    endwith
   endif
  endfunc

  func oCODICE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RIFER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RIFER)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'',this.parent.oContained
  endproc
  proc oCODICE_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RIFER
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oCATCOM_1_8 as StdField with uid="YTXYITXSFT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cliente o la categoria commerciale",;
    ToolTipText = "Categoria commerciale associata al messaggio",;
    HelpContextID = 53587162,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=118, Top=173, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODICE))
    endwith
   endif
  endfunc

  func oCATCOM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oDESCRINI_1_10 as StdField with uid="FVCEVJQQZT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRINI", cQueryName = "DESCRINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 117553281,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=240, Top=35, InputMask=replicate('X',50)

  add object oDESCRFIN_1_11 as StdField with uid="LAHPZWPEOT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRFIN", cQueryName = "DESCRFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 100550532,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=240, Top=60, InputMask=replicate('X',50)

  add object oDESCRI_1_14 as StdField with uid="TSPHCKYHRO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117553354,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=252, Top=148, InputMask=replicate('X',40)

  add object oDESCAT_1_15 as StdField with uid="NVZQJIAHHE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 219265226,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=181, Top=173, InputMask=replicate('X',35)


  add object oObj_1_16 as cp_outputCombo with uid="ZLERXIVPWT",left=118, top=203, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181283610


  add object oBtn_1_17 as StdButton with uid="MEYGRNIYLE",left=399, top=229, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217491674;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="YHZLBUCOHT",left=449, top=229, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184907078;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="ZWMPWEZFOE",Visible=.t., Left=10, Top=173,;
    Alignment=1, Width=106, Height=15,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="WFUWMNGDMZ",Visible=.t., Left=24, Top=12,;
    Alignment=0, Width=175, Height=15,;
    Caption="Selezione validit� contratti"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="UDYIDFNYFR",Visible=.t., Left=10, Top=203,;
    Alignment=1, Width=106, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="CBIAGHAHSY",Visible=.t., Left=10, Top=90,;
    Alignment=1, Width=106, Height=15,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WEQOIINTPO",Visible=.t., Left=-196, Top=-108,;
    Alignment=1, Width=101, Height=15,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JHUJPQDYAS",Visible=.t., Left=10, Top=114,;
    Alignment=1, Width=106, Height=15,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="MXBLQSDRMB",Visible=.t., Left=10, Top=148,;
    Alignment=1, Width=106, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QHZHCERJXG",Visible=.t., Left=36, Top=35,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="OWBCIYPQFX",Visible=.t., Left=44, Top=60,;
    Alignment=1, Width=72, Height=15,;
    Caption="A contratto:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="HQSRRGDOLX",left=22, top=27, width=177,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_sco','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
