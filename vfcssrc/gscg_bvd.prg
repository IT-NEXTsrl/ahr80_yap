* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bvd                                                        *
*              Visualizza dich. di intento                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2008-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bvd",oParentObject,m.pSERIAL,m.pTipo)
return(i_retval)

define class tgscg_bvd as StdBatch
  * --- Local variables
  pSERIAL = space(15)
  pTipo = space(1)
  w_PROG = .NULL.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la dichiarazione di Intento da Maschera GSCG_KLI (Lanciata da Clienti o Fornitori)
    * --- Gestione Dichiarazione di Intenti
    this.w_PROG = GSCG_ADI()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Mi metto in interroga su una dichiarazione esistente
    if Not Empty(this.pSerial) And this.pTipo="D"
      this.w_PROG.ecpFilter()     
      this.w_PROG.w_DISERIAL = this.pSERIAL
      this.w_PROG.ecpSave()     
    endif
    * --- Carico la dichiarazione per l'intestatario selezionato
    if this.pTipo = "C"
      this.w_PROG.ecpLoad()     
      this.w_PROG.w_DITIPCON = this.oParentObject.w_ANTIPCON
      this.w_PROG.w_DICODICE = this.oParentObject.w_ANCODICE
      this.w_OBJCTRL = this.w_PROG.GetCtrl("w_DICODICE")
      this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_ANTIPCON
      this.w_OSOURCE.xKey( 2 ) = this.oParentObject.w_ANCODICE
      this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
      this.w_PROG.Notifyevent("w_DITIPCON Changed")     
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pTipo)
    this.pSERIAL=pSERIAL
    this.pTipo=pTipo
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pTipo"
endproc
