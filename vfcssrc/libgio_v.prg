* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: LIBGIO_V                                                        *
*              Libro giornale Vert.                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 5/4/01                                                          *
* Last revis.: 15/1/10                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_ODESCRI,DATIDOC,w_SIMVAL,L_PROLIB1,w_ODESCRI
private w_DARE,w_AVERE,w_DARE,w_AVERE,w_DAYARRO1
private w_ARRO1,w_DARIGA,L_DATA,w_DAYARRO,w_xDAYARRO
private w_TDELDAY,w_TDAYDAR,w_TDAYAVE,PREFIS,NUMPAG
private p_RAGAZI,p_PARTO,w_DATIAZ,p_CODO,w_Set
private w_DARIGA,L_PROLIB1,L_STAMPA,P_VALUTA,PREFIS
private NUMPAG,p_RAGAZI,p_PARTO,w_DATIAZ,p_CODO
private P_VALUTA,w_ARRO,w_xARRO,w_TOTDAR,w_TOTAVE

w_ODESCRI = space(10)
DATIDOC = space(11)
w_SIMVAL = space(5)
L_PROLIB1 = 0
w_ODESCRI = space(10)
w_DARE = 0
w_AVERE = 0
w_DARE = 0
w_AVERE = 0
w_DAYARRO1 = 0
w_ARRO1 = 0
w_DARIGA = 0
L_DATA = space(10)
w_DAYARRO = 0
w_xDAYARRO = 0
w_TDELDAY = ctod("  /  /  ")
w_TDAYDAR = 0
w_TDAYAVE = 0
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(90)
p_PARTO = space(16)
w_DATIAZ = space(90)
p_CODO = space(16)
w_Set = space(10)
w_DARIGA = 0
L_PROLIB1 = 0
L_STAMPA = space(3)
P_VALUTA = space(10)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(90)
p_PARTO = space(16)
w_DATIAZ = space(90)
p_CODO = space(16)
P_VALUTA = space(10)
w_ARRO = 0
w_xARRO = 0
w_TOTDAR = 0
w_TOTAVE = 0

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  i_formh14 = 8
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do LIBG4O_V with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do LIBG4O_V with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure LIBG4O_V
* === Procedure LIBG4O_V
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2, i_cond3

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (Cp_todate(PNDATREG))
    i_cond2 = (PNSERIAL)
    i_cond3 = (DESCRIGA)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do LIBG4O_V with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(Cp_todate(PNDATREG)) .or. i_frm_brk
        i_cond1 = (Cp_todate(PNDATREG))
        i_frm_brk = .T.
        do LIBG5O_V with 1.00, 3
      endif
      if i_cond2<>(PNSERIAL) .or. i_frm_brk
        i_cond2 = (PNSERIAL)
        i_frm_brk = .T.
        do LIBG5O_V with 1.01, 1
      endif
      if i_cond3<>(DESCRIGA) .or. i_frm_brk
        i_cond3 = (DESCRIGA)
        i_frm_brk = .T.
          do LIBG5O_V with 1.02, 0
        if CCDESCRI<>DESCRIGA .or. SQUAD='S'
          do LIBG5O_V with 1.03, 1
        endif
        if .t.
          do LIBG5O_V with 1.04, 0
        endif
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
      do LIBG5O_V with 1.05, 1
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond3<>(DESCRIGA) .or. i_cond2<>(PNSERIAL) .or. i_cond1<>(Cp_todate(PNDATREG))      
        go i_prevrec
        do LIBG5O_V with 1.06, 0
        do cplu_go with i_currec
      endif
      if eof()  .or. i_cond2<>(PNSERIAL) .or. i_cond1<>(Cp_todate(PNDATREG))      
        go i_prevrec
          do LIBG5O_V with 1.07, 0
        if L_totday='S' and w_DAYARRO<>0
          do LIBG5O_V with 1.08, 1
        endif
        do cplu_go with i_currec
      endif
      if eof()  .or. i_cond1<>(Cp_todate(PNDATREG))      
        go i_prevrec
          do LIBG5O_V with 1.09, 0
        if .t.
          do LIBG5O_V with 1.10, 0
        endif
        if L_totday='S'
          do LIBG5O_V with 1.11, 1
        endif
        if .t.
          do LIBG5O_V with 1.12, 0
        endif
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=11
    do LIBG5O_V with 11.00, 8
  case i_form_id=12
    do LIBG5O_V with 12.00, 8
  case i_form_id=14
    if i_row+8>w_t_stnrig
      * --- stampa il piede di pagina
      do LIBG4O_V with 99, 8
    endif
    do LIBG5O_V with 14.00, 1
    if L_totday<>'S' and  w_ARRO<>0
      do LIBG5O_V with 14.01, 1
    endif
    if .T.
      do LIBG5O_V with 14.02, 4
    endif
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do LIBG4O_V with 13, 0
        else
          do LIBG5O_V with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do LIBG4O_V with 12, 0
        else
          do LIBG5O_V with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure LIBG5O_V
* === Procedure LIBG5O_V
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do LIBG4O_V with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
  case i_form_id=1.09
    do frm1_09
  case i_form_id=1.10
    do frm1_10
  case i_form_id=1.11
    do frm1_11
  case i_form_id=1.12
    do frm1_12
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
  * --- 12� form
  case i_form_id=12.0
    do frm12_0
  * --- 14� form
  case i_form_id=14.0
    do frm14_0
  case i_form_id=14.01
    do frm14_01
  case i_form_id=14.02
    do frm14_02
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),0,at_x(7),"- Registrazioni del :",i_fn
  DATREG = Cp_todate(PNDATREG)
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),23,at_x(184),transform(DATREG,Repl('X',8)),i_fn
return

* --- frm1_01
procedure frm1_01
  w_ODESCRI = 'ZZZZZZ'
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),10,at_x(81),transform(w_ODESCRI,""),i_fn
   ENDIF
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),20,at_x(167),transform(CCDESCRI,Repl('X',30)),i_fn
  if not empty(pnnumdoc)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),56,at_x(455),"Num Doc.",i_fn
  endif
  DATIDOC = alltrim(TRAN(pnnumdoc,'@Z '+"99999999999999999"))+iif(empty(pnalfdoc),'',' / '+alltrim(pnalfdoc))
  if not empty(pnnumdoc)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),65,at_x(521),transform(DATIDOC,Repl('X',26)),i_fn
  endif
  if !empty(Cp_todate(PNDATDOC))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),92,at_x(741),"Del:",i_fn
  endif
  DATDOC = Cp_todate(PNDATDOC)
  if !empty(Cp_todate(PNDATDOC))
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),97,at_x(781),transform(DATDOC,Repl('X',10)),i_fn
  endif
  if PNVALNAZ<>L_VALUTA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),113,at_x(907),"Convertita da:",i_fn
  endif
  w_SIMVAL = NVL(LOOKTAB('VALUTE','VASIMVAL','VACODVAL',PNVALNAZ),' ')
  if PNVALNAZ<>L_VALUTA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(76-75),127,at_x(1019),transform(w_SIMVAL,""),i_fn
  endif
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  if CCDESCRI<>DESCRIGA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(133-132),32,at_x(256),transform(DESCRIGA,""),i_fn
  endif
  if SQUAD='S'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(133-132),113,at_x(907),"Manca Quadratura!",i_fn
  endif
return

* --- frm1_04
procedure frm1_04
return

* --- frm1_05
procedure frm1_05
  L_PROLIB1 = L_PROLIB1+1
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),4,at_x(32),transform(L_PROLIB1,"999999999"),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),16,at_x(131),transform(ANDESCRI,Repl('X',40)),i_fn
  if PNDESRIG<>w_ODESCRI
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),54,at_x(433),transform(PNDESRIG,Repl('X',40)),i_fn
  endif
  w_ODESCRI = IIF(w_ODESCRI<>PNDESRIG, PNDESRIG, w_ODESCRI)
  IF .F.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),96,at_x(768),transform(w_ODESCRI,""),i_fn
   ENDIF
  w_DARE = IIF(PNVALNAZ=L_VALUTA,PNIMPDAR,(VAL2MON(PNIMPDAR,GETCAM(PNVALNAZ,GETVALUT(g_PERVAL,'VADATEUR')), L_CAOVAL, I_DATSYS,g_PERVAL)))
  if w_DARE<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),98,at_x(786),transform(w_DARE,L_FORMAT),i_fn
   w_TOTDAR = w_TOTDAR+w_DARE
  endif
  w_AVERE = IIF(PNVALNAZ=L_VALUTA,PNIMPAVE,(VAL2MON(PNIMPAVE,GETCAM(PNVALNAZ,GETVALUT(g_PERVAL,'VADATEUR')), L_CAOVAL, I_DATSYS,g_PERVAL)))
  if w_AVERE<>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),114,at_x(916),transform(w_AVERE,L_FORMAT),i_fn
   w_TOTAVE = w_TOTAVE+w_AVERE
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),130,at_x(1042),transform(w_DARE,""),i_fn
   endif
   w_TDAYDAR = w_TDAYDAR+w_DARE
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),132,at_x(1059),transform(w_AVERE,""),i_fn
   endif
   w_TDAYAVE = w_TDAYAVE+w_AVERE
  w_DAYARRO1 = IIF(PNVALNAZ<>L_VALUTA AND SQUAD<>'S' , w_AVERE-w_DARE,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),134,at_x(1076),transform(w_DAYARRO1,""),i_fn
   endif
   w_DAYARRO = w_DAYARRO+w_DAYARRO1
  w_ARRO1 = IIF(PNVALNAZ<>L_VALUTA AND SQUAD<>'S' , w_AVERE-w_DARE,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),136,at_x(1095),transform(w_ARRO1,""),i_fn
   endif
   w_ARRO = w_ARRO+w_ARRO1
  w_DARIGA = w_DARIGA+1
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),139,at_x(1114),transform(w_DARIGA,""),i_fn
   endif
  L_DATA = PNDATREG
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(190-189),141,at_x(1130),transform(L_DATA,""),i_fn
   endif
return

* --- frm1_06
procedure frm1_06
return

* --- frm1_07
procedure frm1_07
return

* --- frm1_08
procedure frm1_08
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(266-265),7,at_x(62),"Differenze di conversione:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(266-265),53,at_x(430),transform(w_DAYARRO,L_FORMAT),i_fn
  w_xDAYARRO = Abs(w_DAYARRO)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(266-265),81,at_x(650),transform(w_xDAYARRO,L_FORMAT),i_fn
return

* --- frm1_09
procedure frm1_09
return

* --- frm1_10
procedure frm1_10
return

* --- frm1_11
procedure frm1_11
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),15,at_x(126),"Totali del giorno:",i_fn
  w_TDELDAY = Cp_todate(PNDATREG)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),34,at_x(276),transform(w_TDELDAY,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),46,at_x(370),"Dare",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),51,at_x(410),transform(w_TDAYDAR,L_FORMAT),i_fn
   w_TDAYDAR = 0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),72,at_x(582),"Avere",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(342-341),78,at_x(630),transform(w_TDAYAVE,L_FORMAT),i_fn
   w_TDAYAVE = 0
return

* --- frm1_12
procedure frm1_12
return

* --- 11� form
procedure frm11_0
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),1,at_x(8),"LIBRO GIORNALE",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),81,at_x(655),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),103,at_x(828),"Pag.",i_fn
  endif
  NUMPAG = L_PRPALG+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),116,at_x(935),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),0,at_x(7),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),95,at_x(764),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),107,at_x(863),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(7),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),92,at_x(740),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),107,at_x(863),transform(p_CODO,""),i_fn
  endif
  if .f.
  if len(trim('&w_t_st10'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_st10,""
  endif
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),0,at_x(4),transform(w_Set,""),i_fn
   endif
  w_DARIGA = L_PROLG1
  if .f.
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),2,at_x(20),transform(w_DARIGA,""),i_fn
  endif
  L_PROLIB1 = L_PROLG1
  if .f.
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),11,at_x(92),transform(L_PROLIB1,""),i_fn
  endif
  L_STAMPA = 1
  if .f.
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),21,at_x(172),transform(L_STAMPA,""),i_fn
  endif
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),80,at_x(647),"Importi espressi in :",i_fn
  P_VALUTA = NVL(LOOKTAB('VALUTE','VASIMVAL','VACODVAL',PNVALNAZ),' ')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),107,at_x(863),transform(P_VALUTA,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),5,at_x(45),"  N.  Conto",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),32,at_x(259),"Causale",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),54,at_x(433),"Descrizione Operazione",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),114,at_x(914),"Dare",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(134-0),129,at_x(1034),"Avere",i_fn
return

* --- 12� form
procedure frm12_0
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),1,at_x(8),"LIBRO GIORNALE",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),81,at_x(655),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),103,at_x(829),"Pag.",i_fn
  endif
  NUMPAG = L_PRPALG+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),116,at_x(935),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),0,at_x(7),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),95,at_x(765),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),107,at_x(863),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(7),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),92,at_x(741),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),107,at_x(863),transform(p_CODO,""),i_fn
  endif
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),80,at_x(647),"Importi espressi in :",i_fn
  P_VALUTA = NVL(LOOKTAB('VALUTE','VASIMVAL','VACODVAL',PNVALNAZ),' ')
  i_fn = ""
  do F_Say with i_row+5,i_row+at_y(95-0),107,at_x(863),transform(P_VALUTA,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),5,at_x(44),"  N.  Conto",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),32,at_x(258),"Causale",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),54,at_x(433),"Descrizione Operazione",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),114,at_x(914),"Dare",i_fn
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(133-0),129,at_x(1033),"Avere",i_fn
return

* --- 14� form
procedure frm14_0
  if i_row+i_formh14>w_t_stnrig
    * stampa il piede di pagina
    do LIBG4O_V with 13, 0
  endif
return

* --- frm14_01
procedure frm14_01
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(41-40),7,at_x(62),"Differenze di conversione:",i_fn
  if w_ARRO>0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(41-40),50,at_x(404),transform(w_ARRO,L_FORMAT),i_fn
  endif
  w_xARRO = Abs(w_ARRO)
  if w_ARRO<0
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(41-40),78,at_x(626),transform(w_xARRO,L_FORMAT),i_fn
  endif
return

* --- frm14_02
procedure frm14_02
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(98-78),14,at_x(118),"Totali progressivi:",i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(98-78),45,at_x(360),"Dare",i_fn
  w_TOTDAR = Round(w_TOTDAR+IIF(w_ARRO>0,w_ARRO, 0)+L_DARLG1,L_dectot)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(98-78),50,at_x(400),transform(w_TOTDAR,L_FORMAT),i_fn
   w_TOTDAR = 0
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(98-78),71,at_x(572),"Avere",i_fn
  w_TOTAVE = Round(w_TOTAVE+IIF(w_ARRO<0, ABS(w_ARRO), 0)+L_AVELG1,L_dectot)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(98-78),77,at_x(620),transform(w_TOTAVE,L_FORMAT),i_fn
   w_TOTAVE = 0
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(119-78),19,at_x(158),"�������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(119-78),56,at_x(455),"��������������������������������������",i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(138-78),8,at_x(70),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e'",i_fn
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(138-78),77,at_x(622),"da considerarsi annullato",i_fn
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- Fine Area Manuale 
