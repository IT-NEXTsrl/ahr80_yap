* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_cpe                                                        *
*              Verifica codice fiscale partita IVA                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-05                                                      *
* Last revis.: 2008-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_C
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_cpe",oParentObject,m.w_C)
return(i_retval)

define class tgsar_cpe as StdBatch
  * --- Local variables
  w_C = space(1)
  w_TIPERR = 0
  w_TESTCFAHR = .f.
  w_TESTPIAHR = .f.
  w_TESTCFAHE = .f.
  w_TESTPIAHE = .f.
  * --- WorkFile variables
  GSAR_CPE_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina clienti/fornitori con codice fiscale errato o partita iva errata
    * --- Leggo tutti i clienti/fornitori
    this.w_TESTCFAHR = (upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR1QCL.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR3QCL.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR1QFR.VQR" ) AND g_APPLICATION="ADHOC REVOLUTION"
    this.w_TESTCFAHE = (upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR_ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR2ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR4ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR5ECC.VQR" ) AND g_APPLICATION="ad hoc ENTERPRISE"
    this.w_TESTPIAHR = (upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR1QCE.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR1QFS.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR3QCL.VQR") AND g_APPLICATION="ADHOC REVOLUTION"
    this.w_TESTPIAHE = (upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR1ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR3ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR4ECC.VQR" OR upper (alltrim(this.oparentobject.oparentobject.w_OQRY))="QUERY\GSAR5ECC.VQR" ) AND g_APPLICATION="ad hoc ENTERPRISE"
    * --- Create temporary table GSAR_CPE
    i_nIdx=cp_AddTableDef('GSAR_CPE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CONTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"ANTIPCON,ANCODICE,ANCODFIS,ANPARIVA,ANNAZION , ANCOGNOM,AN__NOME ,ANLOCNAS ,ANPRONAS,ANDATNAS ,AN_SESSO,ANPERFIS,0 AS TIPERR,0 AS TIPERR1 "," from "+i_cTable;
          +" where ANTIPCON = "+cp_ToStrODBC(this.w_C)+"";
          )
    this.GSAR_CPE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from GSAR_CPE
    i_nConn=i_TableProp[this.GSAR_CPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GSAR_CPE_idx,2],.t.,this.GSAR_CPE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ANTIPCON,ANCODICE,ANCODFIS,ANPARIVA,ANNAZION,ANCOGNOM,AN__NOME ,ANLOCNAS ,ANPRONAS,ANDATNAS ,AN_SESSO,ANPERFIS  from "+i_cTable+" GSAR_CPE ";
           ,"_Curs_GSAR_CPE")
    else
      select ANTIPCON,ANCODICE,ANCODFIS,ANPARIVA,ANNAZION,ANCOGNOM,AN__NOME ,ANLOCNAS ,ANPRONAS,ANDATNAS ,AN_SESSO,ANPERFIS from (i_cTable);
        into cursor _Curs_GSAR_CPE
    endif
    if used('_Curs_GSAR_CPE')
      select _Curs_GSAR_CPE
      locate for 1=1
      do while not(eof())
      if this.w_TESTCFAHR OR this.w_TESTCFAHE
        * --- Controllo codice fiscale
        this.w_TIPERR = 0
        this.w_TIPERR = CHKCFPS(_Curs_GSAR_CPE.ANCODFIS, "CF", _Curs_GSAR_CPE.ANTIPCON , _Curs_GSAR_CPE.ANCODICE, " ",_Curs_GSAR_CPE.ANCOGNOM,_Curs_GSAR_CPE.AN__NOME,_Curs_GSAR_CPE.ANLOCNAS ,_Curs_GSAR_CPE.ANPRONAS , _Curs_GSAR_CPE.ANDATNAS , _Curs_GSAR_CPE.AN_SESSO ,_Curs_GSAR_CPE.ANPERFIS ,this.oparentobject.oparentobject.w_FLCODFIS)
        if this.w_TIPERR>0
          * --- Write into GSAR_CPE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.GSAR_CPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GSAR_CPE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.GSAR_CPE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCODFIS ="+cp_NullLink(cp_ToStrODBC(REPLICATE("@", 16)),'GSAR_CPE','ANCODFIS');
            +",TIPERR ="+cp_NullLink(cp_ToStrODBC(this.w_TIPERR),'GSAR_CPE','TIPERR');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(_Curs_GSAR_CPE.ANCODICE);
                +" and ANTIPCON = "+cp_ToStrODBC(_Curs_GSAR_CPE.ANTIPCON);
                   )
          else
            update (i_cTable) set;
                ANCODFIS = REPLICATE("@", 16);
                ,TIPERR = this.w_TIPERR;
                &i_ccchkf. ;
             where;
                ANCODICE = _Curs_GSAR_CPE.ANCODICE;
                and ANTIPCON = _Curs_GSAR_CPE.ANTIPCON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      if this.w_TESTPIAHR OR this.w_TESTPIAHE
        * --- Controllo Partita IVA
        this.w_TIPERR = 0
        this.w_TIPERR = CHKCFPS(_Curs_GSAR_CPE.ANPARIVA, "PI", _Curs_GSAR_CPE.ANTIPCON , _Curs_GSAR_CPE.ANCODICE,_Curs_GSAR_CPE.ANNAZION," "," "," "," ",null," "," ",this.oparentobject.oparentobject.w_FLPARIVA)
        if this.w_TIPERR>0
          * --- Write into GSAR_CPE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.GSAR_CPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GSAR_CPE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.GSAR_CPE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANPARIVA ="+cp_NullLink(cp_ToStrODBC(REPLICATE("@", 12)),'GSAR_CPE','ANPARIVA');
            +",TIPERR1 ="+cp_NullLink(cp_ToStrODBC(this.w_TIPERR),'GSAR_CPE','TIPERR1');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(_Curs_GSAR_CPE.ANCODICE);
                +" and ANTIPCON = "+cp_ToStrODBC(_Curs_GSAR_CPE.ANTIPCON);
                   )
          else
            update (i_cTable) set;
                ANPARIVA = REPLICATE("@", 12);
                ,TIPERR1 = this.w_TIPERR;
                &i_ccchkf. ;
             where;
                ANCODICE = _Curs_GSAR_CPE.ANCODICE;
                and ANTIPCON = _Curs_GSAR_CPE.ANTIPCON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_GSAR_CPE
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_C)
    this.w_C=w_C
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*GSAR_CPE'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSAR_CPE')
      use in _Curs_GSAR_CPE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_C"
endproc
