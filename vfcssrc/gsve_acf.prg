* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_acf                                                        *
*              Manutenzione rischio clienti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2009-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_acf"))

* --- Class definition
define class tgsve_acf as StdForm
  Top    = 23
  Left   = 24

  * --- Standard Properties
  Width  = 589
  Height = 315+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-03-10"
  HelpContextID=109720169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  SIT_FIDI_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  cFile = "SIT_FIDI"
  cKeySelect = "FICODCLI"
  cKeyWhere  = "FICODCLI=this.w_FICODCLI"
  cKeyWhereODBC = '"FICODCLI="+cp_ToStrODBC(this.w_FICODCLI)';

  cKeyWhereODBCqualified = '"SIT_FIDI.FICODCLI="+cp_ToStrODBC(this.w_FICODCLI)';

  cPrg = "gsve_acf"
  cComment = "Manutenzione rischio clienti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(15)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_FICODCLI = space(15)
  w_DATRIF = ctod('  /  /  ')
  o_DATRIF = ctod('  /  /  ')
  w_GIORIS = 0
  w_FIDATELA = ctod('  /  /  ')
  o_FIDATELA = ctod('  /  /  ')
  w_DATULT = ctod('  /  /  ')
  w_IMPFID = 0
  w_FIIMPPAP = 0
  w_FIIMPESO = 0
  w_FIIMPESC = 0
  w_FIIMPORD = 0
  w_FIIMPDDT = 0
  w_FIIMPFAT = 0
  w_DESCLI = space(40)
  w_FIDRES1 = 0
  w_FIDRES2 = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPCLI = space(1)
  w_CODCLI = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SIT_FIDI','gsve_acf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_acfPag1","gsve_acf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Rischio")
      .Pages(1).HelpContextID = 157314070
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFICODCLI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='SIT_FIDI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SIT_FIDI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SIT_FIDI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FICODCLI = NVL(FICODCLI,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SIT_FIDI where FICODCLI=KeySet.FICODCLI
    *
    i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SIT_FIDI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SIT_FIDI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SIT_FIDI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FICODCLI',this.w_FICODCLI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPCON = 'C'
        .w_DATRIF = i_DATSYS
        .w_GIORIS = 0
        .w_IMPFID = 0
        .w_DESCLI = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
          .link_1_1('Load')
        .w_FICODCLI = NVL(FICODCLI,space(15))
          .link_1_3('Load')
        .w_FIDATELA = NVL(cp_ToDate(FIDATELA),ctod("  /  /  "))
        .w_DATULT = .w_DATRIF-.w_GIORIS
        .w_FIIMPPAP = NVL(FIIMPPAP,0)
        .w_FIIMPESO = NVL(FIIMPESO,0)
        .w_FIIMPESC = NVL(FIIMPESC,0)
        .w_FIIMPORD = NVL(FIIMPORD,0)
        .w_FIIMPDDT = NVL(FIIMPDDT,0)
        .w_FIIMPFAT = NVL(FIIMPFAT,0)
        .w_FIDRES1 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
        .w_FIDRES2 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
        .w_TIPCLI = 'C'
        .w_CODCLI = .w_FICODCLI
        cp_LoadRecExtFlds(this,'SIT_FIDI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(15)
      .w_TIPCON = space(1)
      .w_FICODCLI = space(15)
      .w_DATRIF = ctod("  /  /  ")
      .w_GIORIS = 0
      .w_FIDATELA = ctod("  /  /  ")
      .w_DATULT = ctod("  /  /  ")
      .w_IMPFID = 0
      .w_FIIMPPAP = 0
      .w_FIIMPESO = 0
      .w_FIIMPESC = 0
      .w_FIIMPORD = 0
      .w_FIIMPDDT = 0
      .w_FIIMPFAT = 0
      .w_DESCLI = space(40)
      .w_FIDRES1 = 0
      .w_FIDRES2 = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPCLI = space(1)
      .w_CODCLI = space(15)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .w_TIPCON = 'C'
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_FICODCLI))
          .link_1_3('Full')
          endif
        .w_DATRIF = i_DATSYS
          .DoRTCalc(5,6,.f.)
        .w_DATULT = .w_DATRIF-.w_GIORIS
          .DoRTCalc(8,15,.f.)
        .w_FIDRES1 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
        .w_FIDRES2 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
        .w_OBTEST = i_datsys
          .DoRTCalc(19,19,.f.)
        .w_TIPCLI = 'C'
        .w_CODCLI = .w_FICODCLI
      endif
    endwith
    cp_BlankRecExtFlds(this,'SIT_FIDI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFICODCLI_1_3.enabled = i_bVal
      .Page1.oPag.oDATRIF_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_5.enabled = .Page1.oPag.oBtn_1_5.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oFICODCLI_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFICODCLI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SIT_FIDI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FICODCLI,"FICODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIDATELA,"FIDATELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPPAP,"FIIMPPAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPESO,"FIIMPESO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPESC,"FIIMPESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPORD,"FIIMPORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPDDT,"FIIMPDDT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIIMPFAT,"FIIMPFAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
    i_lTable = "SIT_FIDI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SIT_FIDI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SIT_FIDI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SIT_FIDI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SIT_FIDI')
        i_extval=cp_InsertValODBCExtFlds(this,'SIT_FIDI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FICODCLI,FIDATELA,FIIMPPAP,FIIMPESO,FIIMPESC"+;
                  ",FIIMPORD,FIIMPDDT,FIIMPFAT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_FICODCLI)+;
                  ","+cp_ToStrODBC(this.w_FIDATELA)+;
                  ","+cp_ToStrODBC(this.w_FIIMPPAP)+;
                  ","+cp_ToStrODBC(this.w_FIIMPESO)+;
                  ","+cp_ToStrODBC(this.w_FIIMPESC)+;
                  ","+cp_ToStrODBC(this.w_FIIMPORD)+;
                  ","+cp_ToStrODBC(this.w_FIIMPDDT)+;
                  ","+cp_ToStrODBC(this.w_FIIMPFAT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SIT_FIDI')
        i_extval=cp_InsertValVFPExtFlds(this,'SIT_FIDI')
        cp_CheckDeletedKey(i_cTable,0,'FICODCLI',this.w_FICODCLI)
        INSERT INTO (i_cTable);
              (FICODCLI,FIDATELA,FIIMPPAP,FIIMPESO,FIIMPESC,FIIMPORD,FIIMPDDT,FIIMPFAT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FICODCLI;
                  ,this.w_FIDATELA;
                  ,this.w_FIIMPPAP;
                  ,this.w_FIIMPESO;
                  ,this.w_FIIMPESC;
                  ,this.w_FIIMPORD;
                  ,this.w_FIIMPDDT;
                  ,this.w_FIIMPFAT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SIT_FIDI_IDX,i_nConn)
      *
      * update SIT_FIDI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SIT_FIDI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FIDATELA="+cp_ToStrODBC(this.w_FIDATELA)+;
             ",FIIMPPAP="+cp_ToStrODBC(this.w_FIIMPPAP)+;
             ",FIIMPESO="+cp_ToStrODBC(this.w_FIIMPESO)+;
             ",FIIMPESC="+cp_ToStrODBC(this.w_FIIMPESC)+;
             ",FIIMPORD="+cp_ToStrODBC(this.w_FIIMPORD)+;
             ",FIIMPDDT="+cp_ToStrODBC(this.w_FIIMPDDT)+;
             ",FIIMPFAT="+cp_ToStrODBC(this.w_FIIMPFAT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SIT_FIDI')
        i_cWhere = cp_PKFox(i_cTable  ,'FICODCLI',this.w_FICODCLI  )
        UPDATE (i_cTable) SET;
              FIDATELA=this.w_FIDATELA;
             ,FIIMPPAP=this.w_FIIMPPAP;
             ,FIIMPESO=this.w_FIIMPESO;
             ,FIIMPESC=this.w_FIIMPESC;
             ,FIIMPORD=this.w_FIIMPORD;
             ,FIIMPDDT=this.w_FIIMPDDT;
             ,FIIMPFAT=this.w_FIIMPFAT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SIT_FIDI_IDX,i_nConn)
      *
      * delete SIT_FIDI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FICODCLI',this.w_FICODCLI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
    if i_bUpd
      with this
        if .o_TIPCON<>.w_TIPCON
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,6,.t.)
        if .o_DATRIF<>.w_DATRIF
            .w_DATULT = .w_DATRIF-.w_GIORIS
        endif
        .DoRTCalc(8,15,.t.)
            .w_FIDRES1 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
            .w_FIDRES2 = .w_IMPFID-(.w_FIIMPPAP+.w_FIIMPESC+.w_FIIMPESO+.w_FIIMPORD+.w_FIIMPDDT+.w_FIIMPFAT)
        .DoRTCalc(18,19,.t.)
            .w_TIPCLI = 'C'
            .w_CODCLI = .w_FICODCLI
        if .o_FIDATELA<>.w_FIDATELA
          .Calculate_NMPSQZMJHR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NMPSQZMJHR()
    with this
          * --- Calcolo data riferimento
          .w_DATRIF = .w_FIDATELA
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFIIMPORD_1_13.visible=!this.oPgFrm.Page1.oPag.oFIIMPORD_1_13.mHide()
    this.oPgFrm.Page1.oPag.oFIIMPDDT_1_14.visible=!this.oPgFrm.Page1.oPag.oFIIMPDDT_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oFIDRES1_1_29.visible=!this.oPgFrm.Page1.oPag.oFIDRES1_1_29.mHide()
    this.oPgFrm.Page1.oPag.oFIDRES2_1_30.visible=!this.oPgFrm.Page1.oPag.oFIDRES2_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZGIORIS";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZGIORIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(15))
      this.w_GIORIS = NVL(_Link_.AZGIORIS,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(15)
      endif
      this.w_GIORIS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FICODCLI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FICODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FICODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FICODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FICODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FICODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFICODCLI_1_3'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FICODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FICODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FICODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANVALFID,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FICODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_IMPFID = NVL(_Link_.ANVALFID,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FICODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_IMPFID = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FICODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFICODCLI_1_3.value==this.w_FICODCLI)
      this.oPgFrm.Page1.oPag.oFICODCLI_1_3.value=this.w_FICODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIF_1_4.value==this.w_DATRIF)
      this.oPgFrm.Page1.oPag.oDATRIF_1_4.value=this.w_DATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDATELA_1_7.value==this.w_FIDATELA)
      this.oPgFrm.Page1.oPag.oFIDATELA_1_7.value=this.w_FIDATELA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPFID_1_9.value==this.w_IMPFID)
      this.oPgFrm.Page1.oPag.oIMPFID_1_9.value=this.w_IMPFID
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPPAP_1_10.value==this.w_FIIMPPAP)
      this.oPgFrm.Page1.oPag.oFIIMPPAP_1_10.value=this.w_FIIMPPAP
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPESO_1_11.value==this.w_FIIMPESO)
      this.oPgFrm.Page1.oPag.oFIIMPESO_1_11.value=this.w_FIIMPESO
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPESC_1_12.value==this.w_FIIMPESC)
      this.oPgFrm.Page1.oPag.oFIIMPESC_1_12.value=this.w_FIIMPESC
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPORD_1_13.value==this.w_FIIMPORD)
      this.oPgFrm.Page1.oPag.oFIIMPORD_1_13.value=this.w_FIIMPORD
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPDDT_1_14.value==this.w_FIIMPDDT)
      this.oPgFrm.Page1.oPag.oFIIMPDDT_1_14.value=this.w_FIIMPDDT
    endif
    if not(this.oPgFrm.Page1.oPag.oFIIMPFAT_1_15.value==this.w_FIIMPFAT)
      this.oPgFrm.Page1.oPag.oFIIMPFAT_1_15.value=this.w_FIIMPFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_18.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_18.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDRES1_1_29.value==this.w_FIDRES1)
      this.oPgFrm.Page1.oPag.oFIDRES1_1_29.value=this.w_FIDRES1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIDRES2_1_30.value==this.w_FIDRES2)
      this.oPgFrm.Page1.oPag.oFIDRES2_1_30.value=this.w_FIDRES2
    endif
    cp_SetControlsValueExtFlds(this,'SIT_FIDI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FICODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFICODCLI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_FICODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_DATRIF = this.w_DATRIF
    this.o_FIDATELA = this.w_FIDATELA
    return

enddefine

* --- Define pages as container
define class tgsve_acfPag1 as StdContainer
  Width  = 585
  height = 315
  stdWidth  = 585
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFICODCLI_1_3 as StdField with uid="TTEKWKTOPP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FICODCLI", cQueryName = "FICODCLI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 251049057,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=105, Top=14, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FICODCLI"

  func oFICODCLI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oFICODCLI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFICODCLI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFICODCLI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oFICODCLI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FICODCLI
     i_obj.ecpSave()
  endproc

  add object oDATRIF_1_4 as StdField with uid="UBUJFQMFMO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATRIF", cQueryName = "DATRIF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data utilizzata in combinazioni con i gg di tolleranza per determinare gli effetti scaduti",;
    HelpContextID = 73225014,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=81


  add object oBtn_1_5 as StdButton with uid="IKKQBTULZB",left=189, top=80, width=48,height=45,;
    CpPicture="bmp\calcola.bmp", caption="", nPag=1;
    , ToolTipText = "Rielabora il rischio del cliente ad oggi, per impostare un'altra data modificare la data di riferimento";
    , HelpContextID = 214954790;
    , tabstop=.f., Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSVE_BER(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODCLI))
      endwith
    endif
  endfunc

  add object oFIDATELA_1_7 as StdField with uid="BFDEDCSCML",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FIDATELA", cQueryName = "FIDATELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'ultima elaborazione del fido",;
    HelpContextID = 201630825,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=290

  add object oIMPFID_1_9 as StdField with uid="VMSWZETMWT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMPFID", cQueryName = "IMPFID",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente letto dalla anagrafica",;
    HelpContextID = 38870918,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=80, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIIMPPAP_1_10 as StdField with uid="YZEEKGWCBA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FIIMPPAP", cQueryName = "FIIMPPAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi partite aperte esclusi effetti in distinta",;
    HelpContextID = 247966630,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=110, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIIMPESO_1_11 as StdField with uid="KWBAANXXFH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FIIMPESO", cQueryName = "FIIMPESO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi effetti in distinta in scadenza",;
    HelpContextID = 63417253,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=140, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIIMPESC_1_12 as StdField with uid="THKDZSNRFI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FIIMPESC", cQueryName = "FIIMPESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi effetti scaduti (data elab. - n.gg.tolleranza)",;
    HelpContextID = 63417241,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=170, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIIMPORD_1_13 as StdField with uid="TXMLSAKQHC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FIIMPORD", cQueryName = "FIIMPORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi ordini aperti, intestati al cliente",;
    HelpContextID = 231189402,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=200, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIIMPORD_1_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFIIMPDDT_1_14 as StdField with uid="APHJFKPPLF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FIIMPDDT", cQueryName = "FIIMPDDT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi DDT non fatturati, intestati al cliente",;
    HelpContextID = 46640042,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=230, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIIMPDDT_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFIIMPFAT_1_15 as StdField with uid="DFJXOUKQIL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FIIMPFAT", cQueryName = "FIIMPFAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi fatture non contabilizzate, intestate al cliente",;
    HelpContextID = 80194474,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=260, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oDESCLI_1_18 as StdField with uid="CFKJFDMHLC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 125716278,;
   bGlobalFont=.t.,;
    Height=21, Width=334, Left=239, Top=14, InputMask=replicate('X',40)

  add object oFIDRES1_1_29 as StdField with uid="JULNZCTWYR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FIDRES1", cQueryName = "FIDRES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 18635606,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=290, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES1_1_29.mHide()
    with this.Parent.oContained
      return (.w_FIDRES1<0)
    endwith
  endfunc

  add object oFIDRES2_1_30 as StdField with uid="NWFRIVOZUT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FIDRES2", cQueryName = "FIDRES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 18635606,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=432, Top=290, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES2_1_30.mHide()
    with this.Parent.oContained
      return (.w_FIDRES2>=0)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ODILJHAXHE",Visible=.t., Left=11, Top=14,;
    Alignment=1, Width=92, Height=15,;
    Caption="Codice cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="ESTORZSMKA",Visible=.t., Left=11, Top=290,;
    Alignment=1, Width=92, Height=15,;
    Caption="Elaborato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZCHKMKITDM",Visible=.t., Left=276, Top=80,;
    Alignment=1, Width=153, Height=15,;
    Caption="Importo fido:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="GHHWRGMOOF",Visible=.t., Left=255, Top=110,;
    Alignment=1, Width=174, Height=15,;
    Caption="Partite aperte (escl.effetti):"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="IWHEWVNWID",Visible=.t., Left=276, Top=140,;
    Alignment=1, Width=153, Height=15,;
    Caption="Effetti in scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="NKOQXKLEVD",Visible=.t., Left=276, Top=170,;
    Alignment=1, Width=153, Height=15,;
    Caption="Effetti scaduti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="NZCIXRHPZC",Visible=.t., Left=276, Top=200,;
    Alignment=1, Width=153, Height=15,;
    Caption="Ordini in essere:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="GTQOVGJHTX",Visible=.t., Left=276, Top=230,;
    Alignment=1, Width=153, Height=15,;
    Caption="DDT non fatturati:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="HPWKIMKHQR",Visible=.t., Left=276, Top=260,;
    Alignment=1, Width=153, Height=15,;
    Caption="Fatture non contabilizzate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SYZVCJQEZZ",Visible=.t., Left=267, Top=48,;
    Alignment=0, Width=282, Height=15,;
    Caption="Totali"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="CEMKJTRGZU",Visible=.t., Left=276, Top=290,;
    Alignment=1, Width=153, Height=15,;
    Caption="Fido disponibile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="SZJWLGKRMB",Visible=.t., Left=0, Top=81,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="RDOEEPSYMK",left=267, top=66, width=306,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_acf','SIT_FIDI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FICODCLI=SIT_FIDI.FICODCLI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
