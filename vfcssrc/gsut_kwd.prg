* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kwd                                                        *
*              Cambio password                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-10-07                                                      *
* Last revis.: 2013-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kwd",oParentObject))

* --- Class definition
define class tgsut_kwd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 438
  Height = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-20"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  cpusrgrp_IDX = 0
  POL_SIC_IDX = 0
  cPrg = "gsut_kwd"
  cComment = "Cambio password"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PWD = space(20)
  w_PWDC = space(20)
  w_POLSIC = space(3)
  w_ATTIVO = space(1)
  w_NUMLIM = 0
  w_COMPLEX = 0
  w_NORIP = space(1)
  w_NORIPA = space(1)
  w_MAXPWD = 0
  w_NUMLIA = 0
  w_COMPLEA = 0
  w_NOMEUTE = space(254)
  o_NOMEUTE = space(254)
  w_UTE = 0
  o_UTE = 0
  w_DESUTE = space(20)
  o_DESUTE = space(20)
  w_NOCODUTE = space(1)
  o_NOCODUTE = space(1)
  w_NEW = .F.
  w_CODE = 0
  w_NAME = space(20)
  w_LOGIN = space(20)
  w_CODUTE = space(1)
  w_UTEADMIN = .F.
  w_SOURCE = space(3)
  w_CHANGED = .F.
  w_CHANGE = .F.
  w_OLDNAME = space(20)
  w_VALIDPWD = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwdPag1","gsut_kwd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPWD_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='cpusrgrp'
    this.cWorkTables[3]='POL_SIC'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        cp_grpusr_b(this,"UsrOk")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PWD=space(20)
      .w_PWDC=space(20)
      .w_POLSIC=space(3)
      .w_ATTIVO=space(1)
      .w_NUMLIM=0
      .w_COMPLEX=0
      .w_NORIP=space(1)
      .w_NORIPA=space(1)
      .w_MAXPWD=0
      .w_NUMLIA=0
      .w_COMPLEA=0
      .w_NOMEUTE=space(254)
      .w_UTE=0
      .w_DESUTE=space(20)
      .w_NOCODUTE=space(1)
      .w_NEW=.f.
      .w_CODE=0
      .w_NAME=space(20)
      .w_LOGIN=space(20)
      .w_CODUTE=space(1)
      .w_UTEADMIN=.f.
      .w_SOURCE=space(3)
      .w_CHANGED=.f.
      .w_CHANGE=.f.
      .w_OLDNAME=space(20)
      .w_VALIDPWD=0
      .w_PWD=oParentObject.w_PWD
      .w_NOMEUTE=oParentObject.w_NOMEUTE
      .w_UTE=oParentObject.w_UTE
      .w_DESUTE=oParentObject.w_DESUTE
      .w_NOCODUTE=oParentObject.w_NOCODUTE
      .w_CHANGED=oParentObject.w_CHANGED
          .DoRTCalc(1,2,.f.)
        .w_POLSIC = 'AHE'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_POLSIC))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(cp_translate(MSG_NEW_PASSWORD))
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(cp_translate(MSG_CONFIRM_PWD))
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_CODE))
          .DoRTCalc(4,15,.f.)
        .w_NEW = .F.
        .w_CODE = .w_UTE
        .w_NAME = .w_DESUTE
        .w_LOGIN = LEFT(.w_NOMEUTE,20)
        .w_CODUTE = .w_NOCODUTE
        .w_UTEADMIN = cp_isadministrator(.F.,.w_CODE)
        .w_SOURCE = 'KWD'
          .DoRTCalc(23,23,.f.)
        .w_CHANGE = .F.
        .w_OLDNAME = TRIM(LEFT(.w_LOGIN,20))
        .w_VALIDPWD = iif((.w_UTEADMIN AND .w_COMPLEA=1 AND .w_NUMLIA=0) OR (not .w_UTEADMIN AND .w_COMPLEX=1 AND .w_NUMLIM=0),0,iif((.w_UTEADMIN AND .w_COMPLEA=1) OR (not .w_UTEADMIN AND .w_COMPLEX=1),1,2))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PWD=.w_PWD
      .oParentObject.w_NOMEUTE=.w_NOMEUTE
      .oParentObject.w_UTE=.w_UTE
      .oParentObject.w_DESUTE=.w_DESUTE
      .oParentObject.w_NOCODUTE=.w_NOCODUTE
      .oParentObject.w_CHANGED=.w_CHANGED
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_5('Full')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(cp_translate(MSG_NEW_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(cp_translate(MSG_CONFIRM_PWD))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_CODE))
        .DoRTCalc(4,16,.t.)
        if .o_UTE<>.w_UTE
            .w_CODE = .w_UTE
        endif
        if .o_DESUTE<>.w_DESUTE
            .w_NAME = .w_DESUTE
        endif
        if .o_NOMEUTE<>.w_NOMEUTE
            .w_LOGIN = LEFT(.w_NOMEUTE,20)
        endif
        if .o_NOCODUTE<>.w_NOCODUTE
            .w_CODUTE = .w_NOCODUTE
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(cp_translate(MSG_NEW_PASSWORD))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(cp_translate(MSG_CONFIRM_PWD))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(cp_translate(MSG_CODE))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNOMEUTE_1_20.visible=!this.oPgFrm.Page1.oPag.oNOMEUTE_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oUTE_1_22.visible=!this.oPgFrm.Page1.oPag.oUTE_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_23.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POLSIC
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_lTable = "POL_SIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2], .t., this.POL_SIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POLSIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POLSIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERIAL,PSATTIVO,PSCMPLES,PSCMPLEA,PSLUNMIN,PSLUNMIA,PS_NONUM,PS_NORIP,PSNORIPA,PSNUMPWD";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERIAL="+cp_ToStrODBC(this.w_POLSIC);
                   +" and PSSERIAL="+cp_ToStrODBC(this.w_POLSIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERIAL',this.w_POLSIC;
                       ,'PSSERIAL',this.w_POLSIC)
            select PSSERIAL,PSATTIVO,PSCMPLES,PSCMPLEA,PSLUNMIN,PSLUNMIA,PS_NONUM,PS_NORIP,PSNORIPA,PSNUMPWD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POLSIC = NVL(_Link_.PSSERIAL,space(3))
      this.w_ATTIVO = NVL(_Link_.PSATTIVO,space(1))
      this.w_COMPLEX = NVL(_Link_.PSCMPLES,0)
      this.w_COMPLEA = NVL(_Link_.PSCMPLEA,0)
      this.w_NUMLIM = NVL(_Link_.PSLUNMIN,0)
      this.w_NUMLIA = NVL(_Link_.PSLUNMIA,0)
      this.w_CODUTE = NVL(_Link_.PS_NONUM,space(1))
      this.w_NORIP = NVL(_Link_.PS_NORIP,space(1))
      this.w_NORIPA = NVL(_Link_.PSNORIPA,space(1))
      this.w_MAXPWD = NVL(_Link_.PSNUMPWD,0)
    else
      if i_cCtrl<>'Load'
        this.w_POLSIC = space(3)
      endif
      this.w_ATTIVO = space(1)
      this.w_COMPLEX = 0
      this.w_COMPLEA = 0
      this.w_NUMLIM = 0
      this.w_NUMLIA = 0
      this.w_CODUTE = space(1)
      this.w_NORIP = space(1)
      this.w_NORIPA = space(1)
      this.w_MAXPWD = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])+'\'+cp_ToStr(_Link_.PSSERIAL,1)+'\'+cp_ToStr(_Link_.PSSERIAL,1)
      cp_ShowWarn(i_cKey,this.POL_SIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POLSIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPWD_1_1.value==this.w_PWD)
      this.oPgFrm.Page1.oPag.oPWD_1_1.value=this.w_PWD
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDC_1_3.value==this.w_PWDC)
      this.oPgFrm.Page1.oPag.oPWDC_1_3.value=this.w_PWDC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEUTE_1_20.value==this.w_NOMEUTE)
      this.oPgFrm.Page1.oPag.oNOMEUTE_1_20.value=this.w_NOMEUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTE_1_22.value==this.w_UTE)
      this.oPgFrm.Page1.oPag.oUTE_1_22.value=this.w_UTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_23.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_23.value=this.w_DESUTE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_UTE))  and not(.w_NOCODUTE <> 'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTE_1_22.SetFocus()
            i_bnoObbl = !empty(.w_UTE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non definito")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kwd
      *-- Verifico errori da batch prima di effettuare il salvataggio
      IF i_bRes 
          i_bRes=cp_grpusr_b(this,'UsrCheck')
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NOMEUTE = this.w_NOMEUTE
    this.o_UTE = this.w_UTE
    this.o_DESUTE = this.w_DESUTE
    this.o_NOCODUTE = this.w_NOCODUTE
    return

enddefine

* --- Define pages as container
define class tgsut_kwdPag1 as StdContainer
  Width  = 434
  height = 135
  stdWidth  = 434
  stdheight = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPWD_1_1 as CheckSafePwd with uid="VMWTJATSIB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PWD", cQueryName = "PWD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nuova password",;
    HelpContextID = 31762186,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=31, Top=62, InputMask=replicate('X',20), PasswordChar='*'

  add object oPWDC_1_3 as StdField with uid="ZARQWPPFZU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PWDC", cQueryName = "PWDC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Conferma nuova password",;
    HelpContextID = 27371274,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=236, Top=62, InputMask=replicate('X',20), PasswordChar='*'


  add object oBtn_1_14 as StdButton with uid="OQBIDCNZRB",left=261, top=106, width=77,height=22,;
    caption="Ok", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 32035610;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="IMQMRMOAOO",left=348, top=106, width=77,height=22,;
    caption="Annulla", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 22220550;
    , Caption='\<Annulla';
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_setCtrlObjprop with uid="OXGLOGRFGP",left=554, top=-15, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Nuova password",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_17 as cp_setCtrlObjprop with uid="DTASHCFBDZ",left=554, top=11, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Conferma password",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 145933286


  add object oObj_1_19 as cp_setCtrlObjprop with uid="STMQUKDDXO",left=554, top=37, width=78,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Caption",cObj="Codice",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 145933286

  add object oNOMEUTE_1_20 as StdField with uid="YYMYRDCJOM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NOMEUTE", cQueryName = "NOMEUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 129032406,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=31, Top=21, InputMask=replicate('X',254)

  func oNOMEUTE_1_20.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE = 'N')
    endwith
  endfunc

  add object oUTE_1_22 as StdField with uid="DOBBSYFCFJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_UTE", cQueryName = "UTE",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Utente non definito",;
    ToolTipText = "Codice utente",;
    HelpContextID = 31758778,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=31, Top=21, cSayPict='"9999"', cGetPict='"9999"'

  func oUTE_1_22.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'N')
    endwith
  endfunc

  add object oDESUTE_1_23 as StdField with uid="WDOPCYWMCC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 145831478,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=96, Top=21, InputMask=replicate('X',20)

  func oDESUTE_1_23.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'N')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="TBTOLTNNCI",Visible=.t., Left=31, Top=47,;
    Alignment=0, Width=112, Height=18,;
    Caption="Nuova password"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="KFAUJJWAOH",Visible=.t., Left=236, Top=47,;
    Alignment=0, Width=124, Height=18,;
    Caption="Conferma password"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="TFIGKZFVDU",Visible=.t., Left=31, Top=6,;
    Alignment=0, Width=42, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RNXJMCEJOQ",Visible=.t., Left=31, Top=6,;
    Alignment=0, Width=42, Height=18,;
    Caption="Login"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_NOCODUTE <> 'S')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="SKKEFSPQBV",Visible=.t., Left=96, Top=5,;
    Alignment=0, Width=112, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kwd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kwd
define class CheckSafePwd as StdField

proc interactivechange()
LOCAL mi,ma,sp,nu,c1,c2,c3,ch,i_i,psw
LOCAL compless, limite, valid
	     mi=0     &&contatore lettere minuscole
	     ma=0     &&contatore lettere maiuscole
	     nu=0     &&contatore numeri
	     sp=0     &&contatore caratteri speciali
	     c1='abcdefghijklmnopqrstuvwxyz'
	     c2='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	     c3='0123456789'
		 psw=ALLTRIM(this.value)
		 FOR i_i=1 TO LEN(psw)
		   ch=ALLTRIM(SUBSTR(psw,i_i,1))
		   DO CASE 
		       CASE ch$c1
		              mi=mi+1
		       CASE ch$c2
		              ma=ma+1
		       CASE ch$c3
		              nu=nu+1
		       OTHERWISE 
		              sp=sp+1
		   ENDCASE
		 NEXT 
         mi=IIF(mi>0,1,0)
         ma=IIF(ma>0,1,0)
         nu=IIF(nu>0,1,0)
         sp=IIF(sp>0,1,0)
         IF thisform.w_UTEADMIN
            compless=thisform.w_complea
            limite=thisform.w_numlia
         ELSE
            compless=thisform.w_complex
            limite=thisform.w_numlim
         ENDIF
         IF compless=1
            valid=0
         ELSE
            valid=(compless*2)
         ENDIF
         IF (mi+ma+nu+sp >= compless AND (LEN(alltrim(this.value))>=limite and LEN(alltrim(this.value))>=valid)) OR thisform.w_attivo<>'S' OR (LEN(alltrim(this.value))=0 AND limite=0 AND compless=1)
            this.backcolor=RGB(0,255,0)
            thisform.w_ValidPwd=0
         ELSE
            IF mi+ma+nu+sp < compless or LEN(alltrim(this.value))<limite
              this.backcolor=RGB(255,0,0)
              thisform.w_ValidPwd=2
            ELSE
              this.backcolor=RGB(255,255,0)
              thisform.w_ValidPwd=1
            ENDIF
         ENDIF
ENDPROC

enddefine
* --- Fine Area Manuale
