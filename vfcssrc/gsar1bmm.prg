* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bmm                                                        *
*              Controllo cancellazione dettaglio                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-21                                                      *
* Last revis.: 2008-04-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bmm",oParentObject,m.pOPER)
return(i_retval)

define class tgsar1bmm as StdBatch
  * --- Local variables
  pOPER = space(2)
  * --- WorkFile variables
  ASS_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Disabilita f6
    if this.pOPER="CH"
      * --- Sono in modifica e ho cambiato il codice
      if this.oParentObject.cFunction="Edit"
        * --- Select from ASS_ATTR
        i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ASS_ATTR ";
              +" where ASMODFAM="+cp_ToStrODBC(this.oParentObject.w_MACODICE)+" And ASCODATT="+cp_ToStrODBC(this.oParentObject.o_MACODATT)+"";
               ,"_Curs_ASS_ATTR")
        else
          select * from (i_cTable);
           where ASMODFAM=this.oParentObject.w_MACODICE And ASCODATT=this.oParentObject.o_MACODATT;
            into cursor _Curs_ASS_ATTR
        endif
        if used('_Curs_ASS_ATTR')
          select _Curs_ASS_ATTR
          locate for 1=1
          do while not(eof())
          if !ah_yesno("Il codice: %1 � gi� stato associato%0Confermi la modifica?"," ",alltrim(this.oParentObject.o_MACODATT))
            this.oParentObject.w_MACODATT = this.oParentObject.o_MACODATT
          endif
          exit
            select _Curs_ASS_ATTR
            continue
          enddo
          use
        endif
      endif
    else
      * --- Select from ASS_ATTR
      i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ASS_ATTR ";
            +" where ASMODFAM="+cp_ToStrODBC(this.oParentObject.w_MACODICE)+" And ASCODATT="+cp_ToStrODBC(this.oParentObject.w_MACODATT)+"";
             ,"_Curs_ASS_ATTR")
      else
        select * from (i_cTable);
         where ASMODFAM=this.oParentObject.w_MACODICE And ASCODATT=this.oParentObject.w_MACODATT;
          into cursor _Curs_ASS_ATTR
      endif
      if used('_Curs_ASS_ATTR')
        select _Curs_ASS_ATTR
        locate for 1=1
        do while not(eof())
        if ah_yesno("Il codice: %1 � gi� stato associato%0Confermi la cancellazione?"," ",alltrim(this.oParentObject.w_MACODATT))
          bOK=.T.
        else
          bOK=.F.
        endif
        exit
          select _Curs_ASS_ATTR
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ASS_ATTR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
