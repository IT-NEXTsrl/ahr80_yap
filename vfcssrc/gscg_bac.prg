* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bac                                                        *
*              Stampa incongruenze contabili                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_266]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bac",oParentObject)
return(i_retval)

define class tgscg_bac as StdBatch
  * --- Local variables
  w_PNSERIAL = space(10)
  w_OSERIAL = space(10)
  w_NUMERO = 0
  w_RIGA = 0
  w_CODCAU = space(5)
  w_DESCAU = space(35)
  w_CODUTE = 0
  w_NUMRER = 0
  w_DATREG = ctod("  /  /  ")
  w_COMPET = space(4)
  w_PCODESE = space(4)
  w_FLPROV = space(1)
  w_NUMREG = 0
  w_VALNAZ = space(3)
  w_TIPREG = space(1)
  w_FLIVDF = space(1)
  w_TIPCLF = space(1)
  w_TIPDOC = space(2)
  w_FLSALD = space(1)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_FLPART = space(1)
  w_FLAG = space(1)
  w_SEZBIL = space(1)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_APPO = space(0)
  w_CCFLCOMP = space(1)
  w_CCTIPREG = space(1)
  w_CCFLIVDF  = space(1)
  w_CCNUMREG = 0
  w_CCTIPDOC = space(2)
  w_CCFLRIFE = space(1)
  w_CCFLPART = space(1)
  w_ESVALNAZ = space(3)
  w_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  w_CONTA = 0
  w_TESTATA = space(7)
  w_Messaggio = space(10)
  w_ESERIAL = space(10)
  w_ENUMERO = 0
  w_EFLIVDF = space(1)
  w_EFLSALD = space(1)
  w_EFLSALI = space(1)
  w_EFLSALF = space(1)
  w_DIFF = space(1)
  w_INIESE = ctod("  /  /  ")
  w_FINESE = ctod("  /  /  ")
  w_oMess = .NULL.
  w_oMess1 = .NULL.
  w_CAURIG = space(5)
  * --- WorkFile variables
  CAU_CONT_idx=0
  ESERCIZI_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Stampa incongruenze  Contabili (da GSCG_SCC)
    this.w_oMess=createobject("Ah_Message")
    this.w_oMess1=createobject("Ah_Message")
    this.w_oMess1.AddMsgPartNL("ATTENZIONE")     
    this.w_oMess1.AddMsgPartNL("Si desidera correggere in automatico le seguenti incongruenze riscontrate?")     
    CREATE CURSOR APPOM (PNSERIAL C(10),NUMRER N(6,0), DATREG D(8), COMPET C(4), CODUTE N(4,0), CODCAU C(5), DESCAU C(35), NUMERO N(4,0), RIGA N(4,0),TESTATA C(7),MSG M(10))
    CREATE CURSOR ERROR (PNSERIAL C(10),NUMERO N(4,0), FLIVDF C(1),FLSALD C(1) ,FLSALI C(1) ,FLSALF C(1), DIFF C(1))
    vq_exec("QUERY\GSCG1SCC.VQR", this, "APPCUR")
    vq_exec("QUERY\GSCG2SCC.VQR", this, "APPIVA")
    * --- Controllo se ho dati da stampare
    if RECCOUNT("APPCUR")=0 AND RECCOUNT("APPIVA")=0 
      ah_ErrorMsg("Non ci sono dati da verificare",,"")
      select APPCUR
      use
      i_retcode = 'stop'
      return
    else
      * --- Analizza dati di Testata
      this.w_OSERIAL = "@@@@%%%���"
      this.w_CONTA = 0
      SELECT APPCUR
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PNSERIAL,""))
      if this.w_OSERIAL<>PNSERIAL
        this.w_OSERIAL = NVL(PNSERIAL,SPACE(10))
        this.w_PNSERIAL = NVL(PNSERIAL,SPACE(10))
        this.w_NUMERO = 0
        this.w_RIGA = 0
        this.w_TESTATA = SPACE(7)
        this.w_CODCAU = NVL(CODCAU,SPACE(5))
        this.w_DESCAU = NVL(DESCAU,SPACE(35))
        this.w_CODUTE = NVL(CODUTE,0)
        this.w_NUMRER = NVL(NUMRER,0)
        this.w_DATREG = NVL(CP_TODATE(DATREG),cp_CharToDate("  -  -  "))
        this.w_COMPET = NVL(COMPET,SPACE(4))
        this.w_FLPROV = NVL(FLPROV,SPACE(1))
        this.w_NUMREG = NVL(NUMREG,0)
        this.w_VALNAZ = NVL(VALNAZ,SPACE(3))
        this.w_TIPREG = NVL(TIPREG,SPACE(1))
        this.w_FLIVDF = NVL(FLIVDF,SPACE(1))
        this.w_TIPCLF = NVL(TIPCLF,SPACE(1))
        this.w_TIPDOC = NVL(TIPDOC,SPACE(2))
        * --- Controlla la congruenza dell'esercizio di competenza con la data Registrazione nel caso non sia attivato il flag esercizio di Competenza
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLCOMP,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLRIFE,CCFLPART,CCFLSALI,CCFLSALF"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLCOMP,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLRIFE,CCFLPART,CCFLSALI,CCFLSALF;
            from (i_cTable) where;
                CCCODICE = this.w_CODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CCFLCOMP = NVL(cp_ToDate(_read_.CCFLCOMP),cp_NullValue(_read_.CCFLCOMP))
          this.w_CCTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
          w_CCFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
          this.w_CCNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
          this.w_CCTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
          this.w_CCFLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
          this.w_CCFLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
          this.w_CCFLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESVALNAZ,ESINIESE,ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_COMPET);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESVALNAZ,ESINIESE,ESFINESE;
            from (i_cTable) where;
                ESCODAZI = i_CODAZI;
                and ESCODESE = this.w_COMPET;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ESVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
          this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          this.w_FINESE = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if (this.w_DATREG < this.w_INIESE OR this.w_DATREG >this.w_FINESE) AND this.w_CCFLCOMP<>"S" 
 
          this.w_oMess.AddMsgPartNL("L'esercizio di competenza non � congruente con la data registrazione")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
        endif
        * --- Controlla la congruenza del Tipo Registro IVA con quello impostato sulla Causale Contabile
        if this.w_TIPREG <> this.w_CCTIPREG
          this.w_oMess.AddMsgPartNL("Il tipo registro IVA non � congruente con quello impostato sulla causale contabile")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
        endif
        * --- Controlla la congruenza del Flag Esigibilit� Differita con quello impostato sulla Causale Contabile
        if  this.w_FLIVDF <> w_CCFLIVDF
          this.w_oMess.AddMsgPartNL("Il flag esigibilit� differita non � congruente con quello impostato sulla causale contabile")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
          this.w_oMess1.AddMsgPartNL("- Flag esigibilit� differita incongruente con le impostazioni della causale contabile")     
          INSERT INTO ERROR (PNSERIAL, NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF,DIFF) VALUES(this.w_PNSERIAL, 0, w_CCFLIVDF," "," "," ","A")
        endif
        * --- Controlla la congruenza del Tipo Documento con quello impostato sulla Causale Contabile
        if NOT(this.w_TIPDOC=this.w_CCTIPDOC OR ( EMPTY(this.w_TIPDOC) AND this.w_CCTIPDOC="N"))
          this.w_oMess.AddMsgPartNL("Il tipo documento non � congruente con quello impostato sulla causale contabile")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
        endif
        * --- Controlla la congruenza del Tipo Cliente/Fornitore con quello impostato sulla Causale Contabile
        if NOT(this.w_TIPCLF = this.w_CCFLRIFE OR ( EMPTY(this.w_TIPCLF) AND this.w_CCFLRIFE="N"))
          this.w_oMess.AddMsgPartNL("Il tipo cliente/fornitore non � congruente con quello impostato sulla causale contabile")     
          this.w_APPO = ALLTRIM(this.w_APPO) + IIF(EMPTY(this.w_APPO),"",CHR(13))+ " "
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
        endif
        * --- Controlla la congruenza della Valuta Nazionale con quella impostata per l'Esercizio
        if this.w_VALNAZ<>this.w_ESVALNAZ
          this.w_oMess.AddMsgPartNL("La valuta nazionale non � congruente con quella impostata per l'esercizio")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_TESTATA = ah_MsgFormat("Testata")
        endif
        this.w_APPO = this.w_oMess.ComposeMessage()
        INSERT INTO APPOM(PNSERIAL,NUMRER, DATREG,COMPET,CODUTE, CODCAU, DESCAU, NUMERO, RIGA,TESTATA,MSG) VALUES (this.w_PNSERIAL,this.w_NUMRER,this.w_DATREG,this.w_COMPET,this.w_CODUTE,this.w_CODCAU,this.w_DESCAU, this.w_NUMERO, this.w_RIGA, this.w_TESTATA,this.w_APPO)
      endif
      ENDSCAN
      * --- Controlla dati di dettaglio
      SELECT APPCUR
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PNSERIAL,""))
      this.w_APPO=SPACE(0)
      this.w_PNSERIAL = NVL(PNSERIAL,SPACE(10))
      * --- Causale di riga
      this.w_CAURIG = NVL(PNCAURIG,SPACE(5))
      this.w_NUMERO = 0
      this.w_RIGA = 0
      this.w_TESTATA = SPACE(7)
      * --- Causale di testata
      this.w_CODCAU = NVL(CODCAU,SPACE(5))
      this.w_DESCAU = NVL(DESCAU,SPACE(35))
      this.w_CODUTE = NVL(CODUTE,0)
      this.w_NUMRER = NVL(NUMRER,0)
      this.w_DATREG = NVL(CP_TODATE(DATREG),cp_CharToDate("  -  -  "))
      this.w_COMPET = NVL(COMPET,SPACE(4))
      this.w_FLPROV = NVL(FLPROV,SPACE(1))
      this.w_NUMREG = NVL(NUMREG,0)
      this.w_VALNAZ = NVL(VALNAZ,SPACE(3))
      this.w_TIPREG = NVL(TIPREG,SPACE(1))
      this.w_FLIVDF = NVL(FLIVDF,SPACE(1))
      this.w_TIPCLF = NVL(TIPCLF,SPACE(1))
      this.w_TIPDOC = NVL(TIPDOC,SPACE(2))
      this.w_FLSALD = NVL(FLSALD," ")
      this.w_FLSALI = NVL(FLSALI," ")
      this.w_FLSALF = NVL(FLSALF," ")
      this.w_FLPART = NVL(FLPART," ")
      this.w_SEZBIL = NVL(SEZBIL, " ")
      this.w_IMPDAR = NVL(IMPDAR,0)
      this.w_IMPAVE = NVL(IMPAVE,0)
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLCOMP,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLRIFE,CCFLPART,CCFLSALI,CCFLSALF"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLCOMP,CCTIPREG,CCFLIVDF,CCNUMREG,CCTIPDOC,CCFLRIFE,CCFLPART,CCFLSALI,CCFLSALF;
          from (i_cTable) where;
              CCCODICE = this.w_CODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCFLCOMP = NVL(cp_ToDate(_read_.CCFLCOMP),cp_NullValue(_read_.CCFLCOMP))
        this.w_CCTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
        w_CCFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
        this.w_CCNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
        this.w_CCTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        this.w_CCFLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
        this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
        this.w_CCFLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
        this.w_CCFLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controlla se sono valorizzati contemporaneamente le sezioni dare e avere 
      if NOT EMPTY(this.w_IMPDAR) AND NOT EMPTY(this.w_IMPAVE)
        this.w_oMess.AddMsgPartNL("Le sezioni dare e avere sono valorizzate contemporaneamente")     
        this.w_CONTA = this.w_CONTA + 1
        this.w_NUMERO = NVL(NUMERO,0)
        this.w_RIGA = NVL(RIGA,0)
      endif
      * --- Controlla il Flag Test Partite rispetto alla testata della causale
      if (this.w_FLPART<>"N" AND NOT EMPTY(this.w_FLPART)) AND this.w_CCFLPART= "N" AND this.w_CODCAU == this.w_CAURIG
        this.w_oMess.AddMsgPartNL("Il flag partite � incongruente con le impostazioni sulla causale contabile")     
        this.w_CONTA = this.w_CONTA + 1
        this.w_NUMERO = NVL(NUMERO,0)
        this.w_RIGA = NVL(RIGA,0)
      endif
      if this.w_FLPROV<>"S"
        * --- Controlla se il Saldo periodo � valorizzato (nel caso di registrazioni confermate)
        if this.w_FLSALD<>"+"
          this.w_oMess.AddMsgPartNL("Il saldo periodo non � valorizzato anche se la registrazione � confermata")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_NUMERO = NVL(NUMERO,0)
          this.w_RIGA = NVL(RIGA,0)
          this.w_oMess1.AddMsgPartNL("- Saldo periodo non valorizzato in caso di registrazione confermata")     
          INSERT INTO ERROR (PNSERIAL, NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF, DIFF) VALUES(this.w_PNSERIAL,this.w_NUMERO," ","+ "," "," ","B")
          SELECT APPCUR
        endif
        * --- Controlla se i Saldi iniziali e finali sono  valorizzati (nel caso di registrazioni confermate e se sono attivi i rispettivi flag nella causale contabile )
        if (this.w_CCFLSALI<>"S" AND this.w_FLSALI="+") OR (this.w_CCFLSALI="S" AND ((this.w_FLSALI="+" AND this.w_SEZBIL="T") OR (this.w_FLSALI<>"+" AND this.w_SEZBIL<>"T")))
          this.w_oMess.AddMsgPartNL("Il saldo iniziale non � congruente con il rispettivo flag della causale contabile")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_NUMERO = NVL(NUMERO,0)
          this.w_RIGA = NVL(RIGA,0)
          this.w_oMess1.AddMsgPartNL("- Saldo iniziale non congruente con il rispettivo flag della causale contabile")     
          if this.w_CCFLSALI="S"
            * --- Se Conto Transitorio deve togliere il Flag
            this.w_FLAG = IIF(this.w_SEZBIL="T", " ", "+")
            INSERT INTO ERROR (PNSERIAL,NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF, DIFF) VALUES(this.w_PNSERIAL,this.w_NUMERO, " "," ",this.w_FLAG," ","C")
          else
            INSERT INTO ERROR (PNSERIAL, NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF, DIFF) VALUES(this.w_PNSERIAL,this.w_NUMERO, " "," "," "," ","C")
          endif
        endif
        if (this.w_CCFLSALF<>"S" AND this.w_FLSALF ="+") OR (this.w_CCFLSALF="S" AND ((this.w_FLSALF="+" AND this.w_SEZBIL="T") OR (this.w_FLSALF<>"+" AND this.w_SEZBIL<>"T")))
          this.w_oMess.AddMsgPartNL("Il saldo finale non � congruente con il rispettivo flag della causale contabile")     
          this.w_CONTA = this.w_CONTA + 1
          this.w_NUMERO = NVL(NUMERO,0)
          this.w_RIGA = NVL(RIGA,0)
          this.w_oMess1.AddMsgPartNL("- Saldo finale non congruente con il rispettivo flag della causale contabile")     
          if this.w_CCFLSALF="S"
            * --- Se Conto Transitorio deve togliere il Flag
            this.w_FLAG = IIF(this.w_SEZBIL="T", " ", "+")
            INSERT INTO ERROR (PNSERIAL, NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF, DIFF) VALUES(this.w_PNSERIAL, this.w_NUMERO, " "," "," ",this.w_FLAG,"D")
          else
            INSERT INTO ERROR (PNSERIAL, NUMERO, FLIVDF,FLSALD,FLSALI,FLSALF, DIFF) VALUES(this.w_PNSERIAL, this.w_NUMERO, " "," "," "," ","D")
          endif
        endif
      endif
      this.w_APPO = this.w_oMess.ComposeMessage()
      INSERT INTO APPOM (PNSERIAL,NUMRER, DATREG,COMPET,CODUTE, CODCAU, DESCAU, NUMERO, RIGA,TESTATA,MSG) VALUES (this.w_PNSERIAL,this.w_NUMRER,this.w_DATREG,this.w_COMPET,this.w_CODUTE,this.w_CODCAU,this.w_DESCAU, this.w_NUMERO,this.w_RIGA,this.w_TESTATA,this.w_APPO)
      SELECT APPCUR
      ENDSCAN
      if Reccount("APPIVA")<>0
        this.w_CONTA = this.w_CONTA + Reccount("APPIVA")
      endif
    endif
    if this.w_CONTA=0
      ah_ErrorMsg("Nell'intervallo selezionato non sono state riscontrate incongruenze",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Lancio il report
    if Used("APPOM")
       
 SELECT PNSERIAL ,NUMRER ,cp_todate(DATREG) as DATREG , COMPET ,CODUTE , CODCAU,DESCAU , NUMERO ,RIGA , TESTATA ,MSG ; 
 FROM APPIVA WHERE NOT EMPTY(MSG) UNION ALL ; 
 SELECT PNSERIAL ,NUMRER ,DATREG , COMPET ,CODUTE , CODCAU,DESCAU , NUMERO ,RIGA , TESTATA ,MSG; 
 FROM APPOM WHERE NOT EMPTY(MSG) ORDER BY 3, 2, 1 INTO CURSOR __TMP__ 
    else
      SELECT * FROM APPIVA ORDER BY 3, 2, 1 INTO CURSOR __TMP__ 
    endif
    L_DATINI=this.oParentObject.w_DATINI
    L_DATFIN=this.oParentObject.w_DATFIN
    L_CODESE=this.oParentObject.w_CODESE
    if USED("__tmp__")
      select __tmp__
      CP_CHPRN("QUERY\GSCG_SAC.FRX", " ", this)
    endif
    * --- Richiesta conferma
    * --- Esegue le correzioni di alcune incongruenze
    this.w_Messaggio = this.w_oMess1.ComposeMessage()
    if NOT EMPTY(this.w_Messaggio)and ah_YesNo(this.w_Messaggio)
      * --- Try
      local bErr_03EE8660
      bErr_03EE8660=bTrsErr
      this.Try_03EE8660()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("IMPOSSIBILE EFFETTUARE LA CORREZIONE%0- OPERAZIONE ANNULLATA",,"")
      endif
      bTrsErr=bTrsErr or bErr_03EE8660
      * --- End
    endif
    if used("__tmp__")
      select __tmp__
      use
    endif
    if used("APPOM")
      select APPOM
      use
    endif
    if used("APPIVA")
      select APPIVA
      use
    endif
    if used("ERROR")
      select ERROR
      use
    endif
  endproc
  proc Try_03EE8660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT ERROR
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PNSERIAL,""))
    this.w_ESERIAL = NVL(PNSERIAL,SPACE(10))
    this.w_ENUMERO = NVL(NUMERO,0)
    this.w_EFLIVDF = NVL(FLIVDF," ")
    this.w_EFLSALD = NVL(FLSALD," ")
    this.w_EFLSALI = NVL(FLSALI," ")
    this.w_EFLSALF = NVL(FLSALF," ")
    this.w_DIFF = NVL(DIFF," ")
    do case
      case this.w_DIFF="A"
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNFLIVDF ="+cp_NullLink(cp_ToStrODBC(this.w_EFLIVDF),'PNT_MAST','PNFLIVDF');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_ESERIAL);
                 )
        else
          update (i_cTable) set;
              PNFLIVDF = this.w_EFLIVDF;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_ESERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_DIFF="B"
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_EFLSALD),'PNT_DETT','PNFLSALD');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_ESERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ENUMERO);
                 )
        else
          update (i_cTable) set;
              PNFLSALD = this.w_EFLSALD;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_ESERIAL;
              and CPROWNUM = this.w_ENUMERO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_DIFF="C"
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNFLSALI ="+cp_NullLink(cp_ToStrODBC(this.w_EFLSALI),'PNT_DETT','PNFLSALI');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_ESERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ENUMERO);
                 )
        else
          update (i_cTable) set;
              PNFLSALI = this.w_EFLSALI;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_ESERIAL;
              and CPROWNUM = this.w_ENUMERO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_DIFF="D"
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNFLSALF ="+cp_NullLink(cp_ToStrODBC(this.w_EFLSALF),'PNT_DETT','PNFLSALF');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_ESERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ENUMERO);
                 )
        else
          update (i_cTable) set;
              PNFLSALF = this.w_EFLSALF;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_ESERIAL;
              and CPROWNUM = this.w_ENUMERO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    ENDSCAN 
    ah_ErrorMsg("Aggiornamento completato",,"")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='PNT_DETT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
