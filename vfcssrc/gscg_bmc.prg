* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bmc                                                        *
*              Carica movimenti C\C da primanota                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_111]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-12                                                      *
* Last revis.: 2002-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bmc",oParentObject)
return(i_retval)

define class tgscg_bmc as StdBatch
  * --- Local variables
  w_FLCRDE = space(1)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_CONBAN = space(15)
  w_CAUMOV = space(5)
  w_DATVAL = ctod("  /  /  ")
  w_CALFES = space(3)
  w_TIPCAU1 = space(1)
  w_COSCOM = 0
  w_CODCAU = space(5)
  w_CODCON = space(15)
  w_BACODVAL = space(3)
  w_TESTCON = .f.
  w_PADRE = .NULL.
  * --- WorkFile variables
  CAUPRI_idx=0
  CAUPRI1_idx=0
  CCC_DETT_idx=0
  CCC_MAST_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza Movimenti di c\c
    * --- Lanciato dal  bottone calcola del programma Gscg_Mmc
    this.w_PADRE = this.oParentObject
    WITH this.oParentObject.oParentObject
    this.oParentObject.w_DATREG = .w_PNDATREG
    this.oParentObject.w_DATDOC = .w_PNDATDOC
    this.oParentObject.w_CCNUMDOC = .w_PNNUMDOC
    this.oParentObject.w_CCNUMREG = .w_PNNUMRER
    this.oParentObject.w_CCALFDOC = .w_PNALFDOC
    this.oParentObject.w_CC__ANNO = .w_PNCOMPET
    this.oParentObject.w_CCCODVAL = .w_PNCODVAL
    this.oParentObject.w_CCCAOVAL = .w_PNCAOVAL
    this.oParentObject.w_DECTOT = .w_DECTOT
    this.w_CODCAU = .w_PNCODCAU
    ENDWITH
    SELECT (this.oParentObject.oParentObject.cTrsName)
    this.w_IMPDAR = t_PNIMPDAR
    this.w_IMPAVE = t_PNIMPAVE
    this.w_CODCON = t_PNCODCON
    if this.w_PADRE.cFunction="Load"
      this.w_CONBAN = NVL(t_CONBAN,SPACE(15))
      this.w_CAUMOV = NVL(t_CAUMOV,SPACE(5))
      this.w_CALFES = t_CALFES
      this.w_BACODVAL = NVL(t_BAVAL,SPACE(3))
    else
      * --- Select from CAUPRI
      i_nConn=i_TableProp[this.CAUPRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2],.t.,this.CAUPRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAUPRI ";
            +" where APCODCAU="+cp_ToStrODBC(this.w_CODCAU)+" AND APCODCON="+cp_ToStrODBC(this.w_CODCON)+"";
             ,"_Curs_CAUPRI")
      else
        select * from (i_cTable);
         where APCODCAU=this.w_CODCAU AND APCODCON=this.w_CODCON;
          into cursor _Curs_CAUPRI
      endif
      if used('_Curs_CAUPRI')
        select _Curs_CAUPRI
        locate for 1=1
        do while not(eof())
        this.w_CONBAN = NVL(_Curs_CAUPRI.APCONBAN,SPACE(15))
        this.w_CAUMOV = NVL(_Curs_CAUPRI.APCAUMOV,SPACE(5))
          select _Curs_CAUPRI
          continue
        enddo
        use
      endif
      if EMPTY(this.w_CONBAN) OR EMPTY(this.w_CAUMOV) 
        * --- Select from CAUPRI1
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAUPRI1 ";
              +" where APCODCAU="+cp_ToStrODBC(this.w_CODCAU)+" AND APCODCON="+cp_ToStrODBC(this.w_CODCON)+"";
               ,"_Curs_CAUPRI1")
        else
          select * from (i_cTable);
           where APCODCAU=this.w_CODCAU AND APCODCON=this.w_CODCON;
            into cursor _Curs_CAUPRI1
        endif
        if used('_Curs_CAUPRI1')
          select _Curs_CAUPRI1
          locate for 1=1
          do while not(eof())
          this.w_CONBAN = NVL(_Curs_CAUPRI1.APCONBAN,SPACE(15))
          this.w_CAUMOV = NVL(_Curs_CAUPRI1.APCAUMOV,SPACE(5))
            select _Curs_CAUPRI1
            continue
          enddo
          use
        endif
      endif
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACALFES,BACODVAL"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACALFES,BACODVAL;
          from (i_cTable) where;
              BACODBAN = this.w_CONBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
        this.w_BACODVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_TESTCON = .T.
    * --- Select from gscg_bmc
    do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
    if used('_Curs_gscg_bmc')
      select _Curs_gscg_bmc
      locate for 1=1
      do while not(eof())
      this.w_TIPCAU1 = NVL(_Curs_gscg_bmc.CATIPCON," ")
      this.w_COSCOM = NVL(_Curs_gscg_bmc.CAIMPCOM,0)
      this.w_FLCRDE = NVL(_Curs_gscg_bmc.CAFLCRDE," ")
        select _Curs_gscg_bmc
        continue
      enddo
      use
    endif
    if (this.w_IMPDAR<>0 AND this.w_FLCRDE="D") OR (this.w_IMPAVE<>0 AND this.w_FLCRDE="C")
      this.w_TESTCON = .F.
    endif
    if NOT EMPTY(this.w_CONBAN) AND NOT EMPTY(this.w_CAUMOV) AND (this.oParentObject.w_CCCODVAL=this.w_BACODVAL) AND (this.w_CODCON=this.oParentObject.w_CONCOL) AND this.w_TESTCON 
      this.w_DATVAL = CALCFEST(this.oParentObject.w_DATREG, this.w_CALFES, this.w_CONBAN, this.w_CAUMOV, this.w_TIPCAU1, 1)
      This.oParentObject.oParentObject.w_CONBAN=this.w_CONBAN
      This.oParentObject.oParentObject.w_CAUMOV=this.w_CAUMOV
      this.w_PADRE.MarkPos()     
      * --- Azzera il Transitorio
      SELECT (this.w_PADRE.cTrsName)
      GO TOP
      DELETE ALL
      this.w_PADRE.InitRow()     
      this.oParentObject.w_CPROWNUM = 1
      this.oParentObject.w_CPROWORD = 10
      this.oParentObject.w_CCNUMCOR = NVL(this.w_CONBAN,SPACE(15))
      this.oParentObject.w_NUMCOR = this.w_CONBAN
      this.oParentObject.w_CCCODCAU = this.w_CAUMOV
      this.oParentObject.w_CCDATVAL = this.w_DATVAL
      this.oParentObject.w_CALFIR = this.w_CALFES
      this.oParentObject.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
      this.oParentObject.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
      this.oParentObject.w_CCFLCOMM = "+"
      this.oParentObject.w_CCCOSCOM = this.w_COSCOM
      this.oParentObject.w_CCCOMVAL = cp_ROUND(this.oParentObject.w_CCCOSCOM*this.oParentObject.w_CCCAOVAL, this.oParentObject.w_DECTOT)
      this.oParentObject.w_CCIMPDEB = IIF(this.w_IMPAVE=0,0,this.w_IMPAVE-ABS(this.oParentObject.w_CCCOSCOM))
      this.oParentObject.w_CCIMPCRE = IIF(this.w_IMPDAR=0,0,this.w_IMPDAR+ABS(this.oParentObject.w_CCCOSCOM))
      this.oParentObject.w_CCIMPDEB = IIF(G_PERVAL<>this.oParentObject.w_CCCODVAL,cp_ROUND(this.oParentObject.w_CCIMPDEB*this.oParentObject.w_CCCAOVAL,this.oParentObject.w_DECTOT),this.oParentObject.w_CCIMPDEB)
      this.oParentObject.w_CCIMPCRE = IIF(G_PERVAL<>this.oParentObject.w_CCCODVAL,cp_ROUND(this.oParentObject.w_CCIMPCRE*this.oParentObject.w_CCCAOVAL,this.oParentObject.w_DECTOT),this.oParentObject.w_CCIMPCRE)
      this.oParentObject.w_TIPCAU = this.w_TIPCAU1
      * --- Aggiorno Totalizzatori
      this.oParentObject.w_TOTDEB = this.oParentObject.w_CCIMPDEB
      this.oParentObject.w_TOTCRE = this.oParentObject.w_CCIMPCRE
      this.oParentObject.w_TOTCOM = this.oParentObject.w_CCCOSCOM
      this.oParentObject.w_TOTALE = this.oParentObject.w_TOTCRE-(this.oParentObject.w_TOTDEB+this.oParentObject.w_TOTCOM)
      this.w_PADRE.TrsFromWork()     
      * --- Questa Parte derivata dal Metodo LoadRec
      * --- Flag Notifica Riga Variata
      if i_SRV<>"A" AND this.w_PADRE.cFunction="Edit"
        replace i_SRV with "U"
      endif
      * --- Questa Parte derivata dal Metodo LoadRec
      this.w_PADRE.RePos(.T.)     
    else
      do case
        case EMPTY(this.w_CAUMOV)
          ah_ErrorMsg("Nessun modello contabile associato alla causale",,"")
        case EMPTY(this.w_CONBAN)
          ah_ErrorMsg("Conto corrente non specificato nel modello contabile",,"")
        case  (this.w_CODCON<>this.oParentObject.w_CONCOL) 
          ah_ErrorMsg("Conto corrente non collegato al conto indicato in primanota",,"")
        case (this.oParentObject.w_CCCODVAL<>this.w_BACODVAL) 
          ah_ErrorMsg("Valuta conto corrente incongruente con la valuta della registrazione contabile",,"")
        case (this.w_IMPDAR<>0 AND this.w_FLCRDE="D") OR (this.w_IMPAVE<>0 AND this.w_FLCRDE="C")
          ah_ErrorMsg("Sezione di primanota incongruente con il tipo di operazione della causale movimenti di C\C",,"")
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CAUPRI'
    this.cWorkTables[2]='CAUPRI1'
    this.cWorkTables[3]='CCC_DETT'
    this.cWorkTables[4]='CCC_MAST'
    this.cWorkTables[5]='COC_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CAUPRI')
      use in _Curs_CAUPRI
    endif
    if used('_Curs_CAUPRI1')
      use in _Curs_CAUPRI1
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
