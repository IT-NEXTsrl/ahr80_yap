* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sdp                                                        *
*              Stampa risorse umane                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-25                                                      *
* Last revis.: 2013-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sdp",oParentObject))

* --- Class definition
define class tgsar_sdp as StdForm
  Top    = 5
  Left   = 8

  * --- Standard Properties
  Width  = 490
  Height = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-08"
  HelpContextID=194414743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsar_sdp"
  cComment = "Stampa risorse umane"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_NOMPRG = space(10)
  w_desprg = space(35)
  w_CODINI = space(5)
  o_CODINI = space(5)
  w_CODFIN = space(5)
  o_CODFIN = space(5)
  w_TIPO = space(1)
  w_PDATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  w_DettGruppi = space(1)
  w_TIPO1 = space(1)
  w_DENOM_PART = space(100)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_DENOM_PART2 = space(100)
  w_COGNPART2 = space(40)
  w_NOMEPART2 = space(40)
  w_DESCRPART2 = space(40)
  w_DPTIPRIS2 = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DettGruppi = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsar_sdp
  * --- Per gestione sicurezza (a seconda del parametro)
  * --- gestisco e mostro all'utente un nome di maschera diverso..
  func getSecuritycode()
     return(ICASE(this.oParentObject='P','gsar_spe',this.oParentObject='G','gsar_sgr',this.cPrg))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sdpPag1","gsar_sdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATIPRIS=space(1)
      .w_NOMPRG=space(10)
      .w_desprg=space(35)
      .w_CODINI=space(5)
      .w_CODFIN=space(5)
      .w_TIPO=space(1)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_DettGruppi=space(1)
      .w_TIPO1=space(1)
      .w_DENOM_PART=space(100)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_DENOM_PART2=space(100)
      .w_COGNPART2=space(40)
      .w_NOMEPART2=space(40)
      .w_DESCRPART2=space(40)
      .w_DPTIPRIS2=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DettGruppi=space(1)
        .w_PATIPRIS = IIF(TYPE(".oParentObject")="O",.OparentObject.w_DPTIPRIS,IIF(TYPE(".oParentObject")="C",.oParentObject,"P"))
        .w_NOMPRG = upper(.getSecuritycode())
        .w_desprg = IIF(.w_PATIPRIS='P',Ah_MsgFormat('Stampa persone'),IIF(.w_PATIPRIS='R',Ah_MsgFormat('Stampa risorse'),Ah_MsgFormat('Stampa gruppi')))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODINI))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODFIN))
          .link_1_5('Full')
        endif
        .w_TIPO = 'T'
        .w_PDATINIZ = i_datsys
        .w_obsodat1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_DettGruppi = 'N'
        .w_TIPO1 = IIF(.w_TIPO='T', ' ', .w_TIPO)
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
          .DoRTCalc(12,15,.f.)
        .w_DENOM_PART2 = IIF(.w_DPTIPRIS2='P',alltrim(.w_COGNPART2)+' '+alltrim(.w_NOMEPART2),ALLTRIM(.w_DESCRPART2))
          .DoRTCalc(17,20,.f.)
        .w_OB_TEST = i_INIDAT
        .w_DettGruppi = IIF(.w_PATIPRIS='G','S','N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CODINI<>.w_CODINI.or. .o_PATIPRIS<>.w_PATIPRIS
          .link_1_4('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_PATIPRIS<>.w_PATIPRIS
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(6,9,.t.)
            .w_TIPO1 = IIF(.w_TIPO='T', ' ', .w_TIPO)
        if .o_CODINI<>.w_CODINI
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .DoRTCalc(12,15,.t.)
        if .o_CODFIN<>.w_CODFIN
            .w_DENOM_PART2 = IIF(.w_DPTIPRIS2='P',alltrim(.w_COGNPART2)+' '+alltrim(.w_NOMEPART2),ALLTRIM(.w_DESCRPART2))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_BFHATBCQHN()
      endwith
      this.DoRTCalc(17,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return

  proc Calculate_AXROSJSFQU()
    with this
          * --- Cambia caption form
          .cComment = AH_MSGFORMAT(.w_DESPRG)
    endwith
  endproc
  proc Calculate_BFHATBCQHN()
    with this
          * --- Cambia cprg
          .cPrg = lower(.w_NOMPRG)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDettGruppi_1_11.enabled = this.oPgFrm.Page1.oPag.oDettGruppi_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_4.visible=!this.oPgFrm.Page1.oPag.oCODINI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCODFIN_1_5.visible=!this.oPgFrm.Page1.oPag.oCODFIN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTIPO_1_7.visible=!this.oPgFrm.Page1.oPag.oTIPO_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDettGruppi_1_11.visible=!this.oPgFrm.Page1.oPag.oDettGruppi_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDENOM_PART_1_17.visible=!this.oPgFrm.Page1.oPag.oDENOM_PART_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDENOM_PART2_1_23.visible=!this.oPgFrm.Page1.oPag.oDENOM_PART2_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_AXROSJSFQU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_PATIPRIS;
                     ,'DPCODICE',trim(this.w_CODINI))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODINI_1_4'),i_cWhere,'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PATIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore di quello finale!")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_PATIPRIS;
                       ,'DPCODICE',this.w_CODINI)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODINI) OR EMPTY(.w_CODFIN) OR (.w_CODINI<=.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore di quello finale!")
        endif
        this.w_CODINI = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_PATIPRIS;
                     ,'DPCODICE',trim(this.w_CODFIN))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_5'),i_cWhere,'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PATIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_PATIPRIS;
                       ,'DPCODICE',this.w_CODFIN)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART2 = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART2 = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART2 = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS2 = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(5)
      endif
      this.w_COGNPART2 = space(40)
      this.w_NOMEPART2 = space(40)
      this.w_DESCRPART2 = space(40)
      this.w_DPTIPRIS2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODINI) OR EMPTY(.w_CODFIN) OR (.w_CODINI<=.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(5)
        this.w_COGNPART2 = space(40)
        this.w_NOMEPART2 = space(40)
        this.w_DESCRPART2 = space(40)
        this.w_DPTIPRIS2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_4.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_4.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_5.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_5.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_7.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_8.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_8.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_9.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDettGruppi_1_11.RadioValue()==this.w_DettGruppi)
      this.oPgFrm.Page1.oPag.oDettGruppi_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_17.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_17.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART2_1_23.value==this.w_DENOM_PART2)
      this.oPgFrm.Page1.oPag.oDENOM_PART2_1_23.value=this.w_DENOM_PART2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_CODINI) OR EMPTY(.w_CODFIN) OR (.w_CODINI<=.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale deve essere minore di quello finale!")
          case   not(EMPTY(.w_CODINI) OR EMPTY(.w_CODFIN) OR (.w_CODINI<=.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    return

enddefine

* --- Define pages as container
define class tgsar_sdpPag1 as StdContainer
  Width  = 486
  height = 200
  stdWidth  = 486
  stdheight = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_4 as StdField with uid="QJYWRUDDRU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale deve essere minore di quello finale!",;
    ToolTipText = "Codice persona di inizio selezione",;
    HelpContextID = 104588250,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_PATIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_PATIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_PATIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODINI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_PATIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_5 as StdField with uid="DDUJMCTOFL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona di fine selezione",;
    HelpContextID = 26141658,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_PATIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_PATIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_PATIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_PATIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc


  add object oTIPO_1_7 as StdCombo with uid="GAKDCZIXEC",rtseq=6,rtrep=.f.,left=104,top=64,width=121,height=21;
    , ToolTipText = "Tipologia della risorsa (interno, esterno, tutti)";
    , HelpContextID = 199939894;
    , cFormVar="w_TIPO",RowSource=""+"Esterno,"+"Interno,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_7.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPO_1_7.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_7.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='E',1,;
      iif(this.Parent.oContained.w_TIPO=='I',2,;
      iif(this.Parent.oContained.w_TIPO=='T',3,;
      0)))
  endfunc

  func oTIPO_1_7.mHide()
    with this.Parent.oContained
      return (IsAlt() OR .w_PATIPRIS<>'P')
    endwith
  endfunc

  add object oPDATINIZ_1_8 as StdField with uid="KXZLKOTRZF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 243196496,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=104, Top=92

  add object oobsodat1_1_9 as StdCheck with uid="TANZHKUOWP",rtseq=8,rtrep=.f.,left=197, top=92, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 55386647,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_9.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_9.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=104, top=120, width=375,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 164458522

  add object oDettGruppi_1_11 as StdCheck with uid="ZRCQBKFGIY",rtseq=9,rtrep=.f.,left=305, top=92, caption="Dettaglio gruppi",;
    ToolTipText = "Se attivo, stampa il gruppo di appartenenza",;
    HelpContextID = 42115750,;
    cFormVar="w_DettGruppi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDettGruppi_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDettGruppi_1_11.GetRadio()
    this.Parent.oContained.w_DettGruppi = this.RadioValue()
    return .t.
  endfunc

  func oDettGruppi_1_11.SetRadio()
    this.Parent.oContained.w_DettGruppi=trim(this.Parent.oContained.w_DettGruppi)
    this.value = ;
      iif(this.Parent.oContained.w_DettGruppi=='S',1,;
      0)
  endfunc

  func oDettGruppi_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATIPRIS $ 'P-R')
    endwith
   endif
  endfunc

  func oDettGruppi_1_11.mHide()
    with this.Parent.oContained
      return (NOT (.w_PATIPRIS $ 'P-R') OR iif(isahe(),.w_onum=IIF(TYPE(".w_onum")='C','3',3),.w_onume=IIF(TYPE(".w_onume")='C','3',3)))
    endwith
  endfunc


  add object oBtn_1_13 as StdButton with uid="WMJFLJSYCM",left=380, top=150, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 200666586;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+","+alltrim(THIS.parent.ocontained.w_OREP)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="CWNKUUZMPU",left=431, top=150, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201732166;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDENOM_PART_1_17 as StdField with uid="SKXCKTCAOH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 4519017,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=172, Top=10, InputMask=replicate('X',100)

  func oDENOM_PART_1_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDENOM_PART2_1_23 as StdField with uid="AGHAPIIOHV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DENOM_PART2", cQueryName = "DENOM_PART2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 4314217,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=172, Top=37, InputMask=replicate('X',100)

  func oDENOM_PART2_1_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="LJJKJMMLRW",Visible=.t., Left=12, Top=64,;
    Alignment=1, Width=88, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt() OR .w_PATIPRIS<>'P')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="VGVPMEQFIP",Visible=.t., Left=8, Top=120,;
    Alignment=1, Width=92, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="INIASOYGMH",Visible=.t., Left=12, Top=10,;
    Alignment=1, Width=88, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="PGKSCPDYJG",Visible=.t., Left=12, Top=37,;
    Alignment=1, Width=88, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="ELGVYCEQRO",Visible=.t., Left=6, Top=94,;
    Alignment=1, Width=94, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
