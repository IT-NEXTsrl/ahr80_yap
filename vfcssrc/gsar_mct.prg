* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mct                                                        *
*              Categorie contributo                                            *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-26                                                      *
* Last revis.: 2016-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mct"))

* --- Class definition
define class tgsar_mct as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 634
  Height = 438+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-01-04"
  HelpContextID=97089385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CATMCONT_IDX = 0
  CATDCONT_IDX = 0
  TIPCONTR_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CATMCONT_IDX = 0
  SISMCOLL_IDX = 0
  VALUTE_IDX = 0
  cFile = "CATMCONT"
  cFileDetail = "CATDCONT"
  cKeySelect = "CGCODICE"
  cKeyWhere  = "CGCODICE=this.w_CGCODICE"
  cKeyDetail  = "CGCODICE=this.w_CGCODICE"
  cKeyWhereODBC = '"CGCODICE="+cp_ToStrODBC(this.w_CGCODICE)';

  cKeyDetailWhereODBC = '"CGCODICE="+cp_ToStrODBC(this.w_CGCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CATDCONT.CGCODICE="+cp_ToStrODBC(this.w_CGCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CATDCONT.CPROWORD '
  cPrg = "gsar_mct"
  cComment = "Categorie contributo"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CGCODICE = space(5)
  w_CGDESCRI = space(40)
  w_CGCONTRI = space(15)
  o_CGCONTRI = space(15)
  w_CGTIPCAT = space(1)
  o_CGTIPCAT = space(1)
  w_CGSISCOL = space(15)
  w_TIPART = space(2)
  w_CGSERVIZ = space(20)
  w_CPROWORD = 0
  w_CGDATINI = ctod('  /  /  ')
  o_CGDATINI = ctod('  /  /  ')
  w_CGUNIMIS = space(3)
  o_CGUNIMIS = space(3)
  w_CGUNIMIS = space(3)
  w_CGIMPNET = 0
  w_CGPERCEN = 0
  w_CGIMPZER = space(1)
  w_CGCATSEM = space(5)
  w_DESTIPCO = space(40)
  w_DESART = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_TIPCAT = space(1)
  w_CGDATFIN = ctod('  /  /  ')
  w_ARFLSERG = space(1)
  w_ARTIPSER = space(1)
  w_ARCONTRI = space(15)
  w_CGDTINVA = ctod('  /  /  ')
  w_CGDTOBSO = ctod('  /  /  ')
  w_DTOBSO1 = ctod('  /  /  ')
  w_SCDESCRI = space(40)
  w_TPAPPPES = space(1)
  w_ARTIPART = space(2)
  w_TPUNMIKG = space(3)
  w_ARUNMIS1 = space(3)
  w_ARFLSERG = space(1)
  w_ZEROCHECK = space(1)
  w_TPCODVAL = space(3)
  o_TPCODVAL = space(3)
  w_CALCPICP = 0
  w_DECUNI = 0
  w_CG__NOTE = space(0)
  w_CGDESCRI1 = space(40)
  w_CGSISCOL = space(15)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CATMCONT','gsar_mct')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mctPag1","gsar_mct",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categorie contributo")
      .Pages(1).HelpContextID = 69255064
      .Pages(2).addobject("oPag","tgsar_mctPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note")
      .Pages(2).HelpContextID = 89965354
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCGCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='TIPCONTR'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='CATMCONT'
    this.cWorkTables[5]='SISMCOLL'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='CATMCONT'
    this.cWorkTables[8]='CATDCONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CATMCONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CATMCONT_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CGCODICE = NVL(CGCODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CATMCONT where CGCODICE=KeySet.CGCODICE
    *
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2],this.bLoadRecFilter,this.CATMCONT_IDX,"gsar_mct")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CATMCONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CATMCONT.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CATDCONT.","CATMCONT.")
      i_cTable = i_cTable+' CATMCONT '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CGCODICE',this.w_CGCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TIPART = 'FM'
        .w_DESTIPCO = space(40)
        .w_DESART = space(40)
        .w_OBTEST = i_datsys
        .w_DTOBSO = ctod("  /  /  ")
        .w_TIPCAT = 'S'
        .w_ARFLSERG = space(1)
        .w_ARTIPSER = space(1)
        .w_ARCONTRI = space(15)
        .w_SCDESCRI = space(40)
        .w_TPAPPPES = space(1)
        .w_ARTIPART = space(2)
        .w_TPUNMIKG = space(3)
        .w_ARUNMIS1 = space(3)
        .w_ARFLSERG = space(1)
        .w_TPCODVAL = space(3)
        .w_DECUNI = 0
        .w_CGCODICE = NVL(CGCODICE,space(5))
        .w_CGDESCRI = NVL(CGDESCRI,space(40))
        .w_CGCONTRI = NVL(CGCONTRI,space(15))
          if link_1_4_joined
            this.w_CGCONTRI = NVL(TPCODICE104,NVL(this.w_CGCONTRI,space(15)))
            this.w_DESTIPCO = NVL(TPDESCRI104,space(40))
            this.w_TPAPPPES = NVL(TPAPPPES104,space(1))
            this.w_TPUNMIKG = NVL(TPUNMIKG104,space(3))
            this.w_TPCODVAL = NVL(TPCODVAL104,space(3))
          else
          .link_1_4('Load')
          endif
        .w_CGTIPCAT = NVL(CGTIPCAT,space(1))
        .w_CGSISCOL = NVL(CGSISCOL,space(15))
          if link_1_6_joined
            this.w_CGSISCOL = NVL(SCCODICE106,NVL(this.w_CGSISCOL,space(15)))
            this.w_SCDESCRI = NVL(SCDESCRI106,space(40))
          else
          .link_1_6('Load')
          endif
        .w_CGSERVIZ = NVL(CGSERVIZ,space(20))
          if link_1_9_joined
            this.w_CGSERVIZ = NVL(ARCODART109,NVL(this.w_CGSERVIZ,space(20)))
            this.w_DESART = NVL(ARDESART109,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(ARDTOBSO109),ctod("  /  /  "))
            this.w_ARFLSERG = NVL(ARFLSERG109,space(1))
            this.w_ARTIPSER = NVL(ARTIPSER109,space(1))
            this.w_ARCONTRI = NVL(ARCONTRI109,space(15))
            this.w_ARTIPART = NVL(ARTIPART109,space(2))
            this.w_ARUNMIS1 = NVL(ARUNMIS1109,space(3))
            this.w_ARFLSERG = NVL(ARFLSERG109,space(1))
          else
          .link_1_9('Load')
          endif
        .w_CGDTINVA = NVL(cp_ToDate(CGDTINVA),ctod("  /  /  "))
        .w_CGDTOBSO = NVL(cp_ToDate(CGDTOBSO),ctod("  /  /  "))
          .link_1_36('Load')
        .w_CALCPICP = DEFPIP(.w_DECUNI)
        .w_CG__NOTE = NVL(CG__NOTE,space(0))
        .w_CGSISCOL = NVL(CGSISCOL,space(15))
          if link_1_40_joined
            this.w_CGSISCOL = NVL(SCCODICE140,NVL(this.w_CGSISCOL,space(15)))
            this.w_SCDESCRI = NVL(SCDESCRI140,space(40))
          else
          .link_1_40('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CATMCONT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CATDCONT where CGCODICE=KeySet.CGCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CATDCONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATDCONT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CATDCONT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CATDCONT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CATDCONT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CGCODICE',this.w_CGCODICE  )
        select * from (i_cTable) CATDCONT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DTOBSO1 = ctod("  /  /  ")
          .w_CGDESCRI1 = space(40)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CGDATINI = NVL(cp_ToDate(CGDATINI),ctod("  /  /  "))
          .w_CGUNIMIS = NVL(CGUNIMIS,space(3))
          * evitabile
          *.link_2_3('Load')
          .w_CGUNIMIS = NVL(CGUNIMIS,space(3))
          * evitabile
          *.link_2_4('Load')
          .w_CGIMPNET = NVL(CGIMPNET,0)
          .w_CGPERCEN = NVL(CGPERCEN,0)
          .w_CGIMPZER = NVL(CGIMPZER,space(1))
          .w_CGCATSEM = NVL(CGCATSEM,space(5))
          .link_2_8('Load')
          .w_CGDATFIN = NVL(cp_ToDate(CGDATFIN),ctod("  /  /  "))
        .w_ZEROCHECK = .w_CGIMPZER
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CALCPICP = DEFPIP(.w_DECUNI)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CGCODICE=space(5)
      .w_CGDESCRI=space(40)
      .w_CGCONTRI=space(15)
      .w_CGTIPCAT=space(1)
      .w_CGSISCOL=space(15)
      .w_TIPART=space(2)
      .w_CGSERVIZ=space(20)
      .w_CPROWORD=10
      .w_CGDATINI=ctod("  /  /  ")
      .w_CGUNIMIS=space(3)
      .w_CGUNIMIS=space(3)
      .w_CGIMPNET=0
      .w_CGPERCEN=0
      .w_CGIMPZER=space(1)
      .w_CGCATSEM=space(5)
      .w_DESTIPCO=space(40)
      .w_DESART=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_TIPCAT=space(1)
      .w_CGDATFIN=ctod("  /  /  ")
      .w_ARFLSERG=space(1)
      .w_ARTIPSER=space(1)
      .w_ARCONTRI=space(15)
      .w_CGDTINVA=ctod("  /  /  ")
      .w_CGDTOBSO=ctod("  /  /  ")
      .w_DTOBSO1=ctod("  /  /  ")
      .w_SCDESCRI=space(40)
      .w_TPAPPPES=space(1)
      .w_ARTIPART=space(2)
      .w_TPUNMIKG=space(3)
      .w_ARUNMIS1=space(3)
      .w_ARFLSERG=space(1)
      .w_ZEROCHECK=space(1)
      .w_TPCODVAL=space(3)
      .w_CALCPICP=0
      .w_DECUNI=0
      .w_CG__NOTE=space(0)
      .w_CGDESCRI1=space(40)
      .w_CGSISCOL=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_CGCONTRI))
         .link_1_4('Full')
        endif
        .w_CGTIPCAT = 'S'
        .w_CGSISCOL = SPACE( 15 )
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CGSISCOL))
         .link_1_6('Full')
        endif
        .w_TIPART = 'FM'
        .w_CGSERVIZ = Space(20)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CGSERVIZ))
         .link_1_9('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_CGDATINI = DATE(YEAR(i_DATSYS),1,1)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CGUNIMIS))
         .link_2_3('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CGUNIMIS))
         .link_2_4('Full')
        endif
        .DoRTCalc(12,15,.f.)
        if not(empty(.w_CGCATSEM))
         .link_2_8('Full')
        endif
        .DoRTCalc(16,17,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(19,19,.f.)
        .w_TIPCAT = 'S'
        .DoRTCalc(21,33,.f.)
        .w_ZEROCHECK = .w_CGIMPZER
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_TPCODVAL))
         .link_1_36('Full')
        endif
        .w_CALCPICP = DEFPIP(.w_DECUNI)
        .DoRTCalc(37,39,.f.)
        .w_CGSISCOL = SPACE( 15 )
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CGSISCOL))
         .link_1_40('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CATMCONT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCGCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCGDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCGCONTRI_1_4.enabled = i_bVal
      .Page1.oPag.oCGTIPCAT_1_5.enabled = i_bVal
      .Page1.oPag.oCGSISCOL_1_6.enabled = i_bVal
      .Page1.oPag.oCGSERVIZ_1_9.enabled = i_bVal
      .Page1.oPag.oCGDTINVA_1_23.enabled = i_bVal
      .Page1.oPag.oCGDTOBSO_1_25.enabled = i_bVal
      .Page2.oPag.oCG__NOTE_4_1.enabled = i_bVal
      .Page1.oPag.oCGSISCOL_1_40.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCGCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCGCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CATMCONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGCODICE,"CGCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGDESCRI,"CGDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGCONTRI,"CGCONTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGTIPCAT,"CGTIPCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGSISCOL,"CGSISCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGSERVIZ,"CGSERVIZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGDTINVA,"CGDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGDTOBSO,"CGDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CG__NOTE,"CG__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGSISCOL,"CGSISCOL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    i_lTable = "CATMCONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CATMCONT_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CGDATINI D(8);
      ,t_CGUNIMIS C(3);
      ,t_CGIMPNET N(18,5);
      ,t_CGPERCEN N(8,5);
      ,t_CGIMPZER N(3);
      ,t_CGCATSEM C(5);
      ,t_CGDESCRI1 C(40);
      ,CPROWNUM N(10);
      ,t_CGDATFIN D(8);
      ,t_DTOBSO1 D(8);
      ,t_ZEROCHECK C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mctbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGDATINI_2_2.controlsource=this.cTrsName+'.t_CGDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.controlsource=this.cTrsName+'.t_CGUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.controlsource=this.cTrsName+'.t_CGUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPNET_2_5.controlsource=this.cTrsName+'.t_CGIMPNET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGPERCEN_2_6.controlsource=this.cTrsName+'.t_CGPERCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.controlsource=this.cTrsName+'.t_CGIMPZER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGCATSEM_2_8.controlsource=this.cTrsName+'.t_CGCATSEM'
    this.oPgFRm.Page1.oPag.oCGDESCRI1_2_13.controlsource=this.cTrsName+'.t_CGDESCRI1'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(63)
    this.AddVLine(146)
    this.AddVLine(213)
    this.AddVLine(347)
    this.AddVLine(436)
    this.AddVLine(480)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CATMCONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CATMCONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CATMCONT')
        i_extval=cp_InsertValODBCExtFlds(this,'CATMCONT')
        local i_cFld
        i_cFld=" "+;
                  "(CGCODICE,CGDESCRI,CGCONTRI,CGTIPCAT,CGSISCOL"+;
                  ",CGSERVIZ,CGDTINVA,CGDTOBSO,CG__NOTE"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CGCODICE)+;
                    ","+cp_ToStrODBC(this.w_CGDESCRI)+;
                    ","+cp_ToStrODBCNull(this.w_CGCONTRI)+;
                    ","+cp_ToStrODBC(this.w_CGTIPCAT)+;
                    ","+cp_ToStrODBCNull(this.w_CGSISCOL)+;
                    ","+cp_ToStrODBCNull(this.w_CGSERVIZ)+;
                    ","+cp_ToStrODBC(this.w_CGDTINVA)+;
                    ","+cp_ToStrODBC(this.w_CGDTOBSO)+;
                    ","+cp_ToStrODBC(this.w_CG__NOTE)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CATMCONT')
        i_extval=cp_InsertValVFPExtFlds(this,'CATMCONT')
        cp_CheckDeletedKey(i_cTable,0,'CGCODICE',this.w_CGCODICE)
        INSERT INTO (i_cTable);
              (CGCODICE,CGDESCRI,CGCONTRI,CGTIPCAT,CGSISCOL,CGSERVIZ,CGDTINVA,CGDTOBSO,CG__NOTE &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CGCODICE;
                  ,this.w_CGDESCRI;
                  ,this.w_CGCONTRI;
                  ,this.w_CGTIPCAT;
                  ,this.w_CGSISCOL;
                  ,this.w_CGSERVIZ;
                  ,this.w_CGDTINVA;
                  ,this.w_CGDTOBSO;
                  ,this.w_CG__NOTE;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CATDCONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATDCONT_IDX,2])
      *
      * insert into CATDCONT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CGCODICE,CPROWORD,CGDATINI,CGUNIMIS,CGIMPNET"+;
                  ",CGPERCEN,CGIMPZER,CGCATSEM,CGDATFIN,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CGCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CGDATINI)+","+cp_ToStrODBCNull(this.w_CGUNIMIS)+","+cp_ToStrODBC(this.w_CGIMPNET)+;
             ","+cp_ToStrODBC(this.w_CGPERCEN)+","+cp_ToStrODBC(this.w_CGIMPZER)+","+cp_ToStrODBCNull(this.w_CGCATSEM)+","+cp_ToStrODBC(this.w_CGDATFIN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CGCODICE',this.w_CGCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CGCODICE,this.w_CPROWORD,this.w_CGDATINI,this.w_CGUNIMIS,this.w_CGIMPNET"+;
                ",this.w_CGPERCEN,this.w_CGIMPZER,this.w_CGCATSEM,this.w_CGDATFIN,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gsar_mct
    * Se vengono aggiunte delle rghe e la testata non � presente nelle aziende
    * di destinazione, occorre forzarne la modifica
    IF g_sinc="S"
      this.bHeaderUpdated = .t.
      this.bUpdated = .t.
    ENDIF
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CATMCONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CATMCONT
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CATMCONT')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CGDESCRI="+cp_ToStrODBC(this.w_CGDESCRI)+;
             ",CGCONTRI="+cp_ToStrODBCNull(this.w_CGCONTRI)+;
             ",CGTIPCAT="+cp_ToStrODBC(this.w_CGTIPCAT)+;
             ",CGSISCOL="+cp_ToStrODBCNull(this.w_CGSISCOL)+;
             ",CGSERVIZ="+cp_ToStrODBCNull(this.w_CGSERVIZ)+;
             ",CGDTINVA="+cp_ToStrODBC(this.w_CGDTINVA)+;
             ",CGDTOBSO="+cp_ToStrODBC(this.w_CGDTOBSO)+;
             ",CG__NOTE="+cp_ToStrODBC(this.w_CG__NOTE)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CATMCONT')
          i_cWhere = cp_PKFox(i_cTable  ,'CGCODICE',this.w_CGCODICE  )
          UPDATE (i_cTable) SET;
              CGDESCRI=this.w_CGDESCRI;
             ,CGCONTRI=this.w_CGCONTRI;
             ,CGTIPCAT=this.w_CGTIPCAT;
             ,CGSISCOL=this.w_CGSISCOL;
             ,CGSERVIZ=this.w_CGSERVIZ;
             ,CGDTINVA=this.w_CGDTINVA;
             ,CGDTOBSO=this.w_CGDTOBSO;
             ,CG__NOTE=this.w_CG__NOTE;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND (!Empty(t_CGCATSEM) Or t_CGIMPNET<>0 Or t_CGPERCEN<>0 Or t_ZEROCHECK='S')) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CATDCONT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CATDCONT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CATDCONT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CATDCONT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CGDATINI="+cp_ToStrODBC(this.w_CGDATINI)+;
                     ",CGUNIMIS="+cp_ToStrODBCNull(this.w_CGUNIMIS)+;
                     ",CGIMPNET="+cp_ToStrODBC(this.w_CGIMPNET)+;
                     ",CGPERCEN="+cp_ToStrODBC(this.w_CGPERCEN)+;
                     ",CGIMPZER="+cp_ToStrODBC(this.w_CGIMPZER)+;
                     ",CGCATSEM="+cp_ToStrODBCNull(this.w_CGCATSEM)+;
                     ",CGDATFIN="+cp_ToStrODBC(this.w_CGDATFIN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CGDATINI=this.w_CGDATINI;
                     ,CGUNIMIS=this.w_CGUNIMIS;
                     ,CGIMPNET=this.w_CGIMPNET;
                     ,CGPERCEN=this.w_CGPERCEN;
                     ,CGIMPZER=this.w_CGIMPZER;
                     ,CGCATSEM=this.w_CGCATSEM;
                     ,CGDATFIN=this.w_CGDATFIN;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_mct
    if not(bTrsErr)And this.w_CGTIPCAT = 'S'
      * --- Aggiorna data di fine validit�
      this.NotifyEvent('Datfin')
    Endif
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND (!Empty(t_CGCATSEM) Or t_CGIMPNET<>0 Or t_CGPERCEN<>0 Or t_ZEROCHECK='S')) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CATDCONT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CATDCONT_IDX,2])
        *
        * delete CATDCONT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CATMCONT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
        *
        * delete CATMCONT
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND (!Empty(t_CGCATSEM) Or t_CGIMPNET<>0 Or t_CGPERCEN<>0 Or t_ZEROCHECK='S')) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CGCONTRI<>.w_CGCONTRI
          .w_CGSISCOL = SPACE( 15 )
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_CGTIPCAT<>.w_CGTIPCAT
          .w_CGSERVIZ = Space(20)
          .link_1_9('Full')
        endif
        .DoRTCalc(8,33,.t.)
          .w_ZEROCHECK = .w_CGIMPZER
          .link_1_36('Full')
        if .o_TPCODVAL<>.w_TPCODVAL
          .w_CALCPICP = DEFPIP(.w_DECUNI)
        endif
        .DoRTCalc(37,39,.t.)
        if .o_CGCONTRI<>.w_CGCONTRI
          .w_CGSISCOL = SPACE( 15 )
          .link_1_40('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CGDATFIN with this.w_CGDATFIN
      replace t_DTOBSO1 with this.w_DTOBSO1
      replace t_ZEROCHECK with this.w_ZEROCHECK
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_QINHBCPJEZ()
    with this
          * --- GSAR_BCT(TIPCAT)
          gsar_bct(this;
              ,'TIPCAT';
             )
    endwith
  endproc
  proc Calculate_VYGBEFPNMT()
    with this
          * --- GSAR_BCT(CHKDAT)
          gsar_bct(this;
              ,'CHKDAT';
             )
    endwith
  endproc
  proc Calculate_IPXZJZPQTW()
    with this
          * --- GSAR_BCT(DATFIN)
          gsar_bct(this;
              ,'DATFIN';
             )
    endwith
  endproc
  proc Calculate_IWVYYXYCBJ()
    with this
          * --- GSAR_BCT(TIPCAT)
          gsar_bct(this;
              ,'CONTRI';
             )
    endwith
  endproc
  proc Calculate_PCLDHLHBFP()
    with this
          * --- w_CGIMPZER Changed
          .w_CGIMPNET = 0
          .w_CGPERCEN = 0
    endwith
  endproc
  proc Calculate_SAYSFMFWIX()
    with this
          * --- GSAR_BCT(CHKDEL)
          gsar_bct(this;
              ,'CHKDEL';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCGCONTRI_1_4.enabled = this.oPgFrm.Page1.oPag.oCGCONTRI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCGTIPCAT_1_5.enabled = this.oPgFrm.Page1.oPag.oCGTIPCAT_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.enabled = this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGUNIMIS_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGUNIMIS_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGUNIMIS_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGUNIMIS_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPNET_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPNET_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGPERCEN_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGPERCEN_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPZER_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPZER_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGCATSEM_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGCATSEM_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCGSISCOL_1_6.visible=!this.oPgFrm.Page1.oPag.oCGSISCOL_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.visible=!this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_13.visible=!this.oPgFrm.Page1.oPag.oDESART_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCGSISCOL_1_40.visible=!this.oPgFrm.Page1.oPag.oCGSISCOL_1_40.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CGTIPCAT Changed")
          .Calculate_QINHBCPJEZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CGDATINI Changed") or lower(cEvent)==lower("w_CGUNIMIS Changed")
          .Calculate_VYGBEFPNMT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Datfin")
          .Calculate_IPXZJZPQTW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CGCONTRI Changed")
          .Calculate_IWVYYXYCBJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CGIMPZER Changed")
          .Calculate_PCLDHLHBFP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_SAYSFMFWIX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CGCONTRI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_lTable = "TIPCONTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2], .t., this.TIPCONTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCONTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATP',True,'TIPCONTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_CGCONTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_CGCONTRI))
          select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCONTRI)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStrODBC(trim(this.w_CGCONTRI)+"%");

            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStr(trim(this.w_CGCONTRI)+"%");

            select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGCONTRI) and !this.bDontReportError
            deferred_cp_zoom('TIPCONTR','*','TPCODICE',cp_AbsName(oSource.parent,'oCGCONTRI_1_4'),i_cWhere,'GSAR_ATP',"Contributi accessori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCONTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_CGCONTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_CGCONTRI)
            select TPCODICE,TPDESCRI,TPAPPPES,TPUNMIKG,TPCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCONTRI = NVL(_Link_.TPCODICE,space(15))
      this.w_DESTIPCO = NVL(_Link_.TPDESCRI,space(40))
      this.w_TPAPPPES = NVL(_Link_.TPAPPPES,space(1))
      this.w_TPUNMIKG = NVL(_Link_.TPUNMIKG,space(3))
      this.w_TPCODVAL = NVL(_Link_.TPCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CGCONTRI = space(15)
      endif
      this.w_DESTIPCO = space(40)
      this.w_TPAPPPES = space(1)
      this.w_TPUNMIKG = space(3)
      this.w_TPCODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCONTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCONTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPCONTR_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.TPCODICE as TPCODICE104"+ ",link_1_4.TPDESCRI as TPDESCRI104"+ ",link_1_4.TPAPPPES as TPAPPPES104"+ ",link_1_4.TPUNMIKG as TPUNMIKG104"+ ",link_1_4.TPCODVAL as TPCODVAL104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on CATMCONT.CGCONTRI=link_1_4.TPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and CATMCONT.CGCONTRI=link_1_4.TPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGSISCOL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SISMCOLL_IDX,3]
    i_lTable = "SISMCOLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2], .t., this.SISMCOLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGSISCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCL',True,'SISMCOLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCCODICE like "+cp_ToStrODBC(trim(this.w_CGSISCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCCODICE',trim(this.w_CGSISCOL))
          select SCCODICE,SCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGSISCOL)==trim(_Link_.SCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStrODBC(trim(this.w_CGSISCOL)+"%");

            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStr(trim(this.w_CGSISCOL)+"%");

            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGSISCOL) and !this.bDontReportError
            deferred_cp_zoom('SISMCOLL','*','SCCODICE',cp_AbsName(oSource.parent,'oCGSISCOL_1_6'),i_cWhere,'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',oSource.xKey(1))
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGSISCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(this.w_CGSISCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',this.w_CGSISCOL)
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGSISCOL = NVL(_Link_.SCCODICE,space(15))
      this.w_SCDESCRI = NVL(_Link_.SCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CGSISCOL = space(15)
      endif
      this.w_SCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_CGSISCOL,"SCCODCON",.w_CGCONTRI),""))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CGSISCOL = space(15)
        this.w_SCDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])+'\'+cp_ToStr(_Link_.SCCODICE,1)
      cp_ShowWarn(i_cKey,this.SISMCOLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGSISCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SISMCOLL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.SCCODICE as SCCODICE106"+ ",link_1_6.SCDESCRI as SCDESCRI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on CATMCONT.CGSISCOL=link_1_6.SCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and CATMCONT.CGSISCOL=link_1_6.SCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGSERVIZ
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGSERVIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CGSERVIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CGSERVIZ))
          select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGSERVIZ)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CGSERVIZ)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CGSERVIZ)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGSERVIZ) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCGSERVIZ_1_9'),i_cWhere,'GSMA_AAS',"Servizi",'GSAR_MCT.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGSERVIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CGSERVIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CGSERVIZ)
            select ARCODART,ARDESART,ARDTOBSO,ARFLSERG,ARTIPSER,ARCONTRI,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGSERVIZ = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_ARFLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_ARTIPSER = NVL(_Link_.ARTIPSER,space(1))
      this.w_ARCONTRI = NVL(_Link_.ARCONTRI,space(15))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARUNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_ARFLSERG = NVL(_Link_.ARFLSERG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CGSERVIZ = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_ARFLSERG = space(1)
      this.w_ARTIPSER = space(1)
      this.w_ARCONTRI = space(15)
      this.w_ARTIPART = space(2)
      this.w_ARUNMIS1 = space(3)
      this.w_ARFLSERG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTIPSER = 'S' And !EMPTY(.w_CGCONTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o contributo non specificato")
        endif
        this.w_CGSERVIZ = space(20)
        this.w_DESART = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_ARFLSERG = space(1)
        this.w_ARTIPSER = space(1)
        this.w_ARCONTRI = space(15)
        this.w_ARTIPART = space(2)
        this.w_ARUNMIS1 = space(3)
        this.w_ARFLSERG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGSERVIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.ARCODART as ARCODART109"+ ",link_1_9.ARDESART as ARDESART109"+ ",link_1_9.ARDTOBSO as ARDTOBSO109"+ ",link_1_9.ARFLSERG as ARFLSERG109"+ ",link_1_9.ARTIPSER as ARTIPSER109"+ ",link_1_9.ARCONTRI as ARCONTRI109"+ ",link_1_9.ARTIPART as ARTIPART109"+ ",link_1_9.ARUNMIS1 as ARUNMIS1109"+ ",link_1_9.ARFLSERG as ARFLSERG109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on CATMCONT.CGSERVIZ=link_1_9.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and CATMCONT.CGSERVIZ=link_1_9.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGUNIMIS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CGUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CGUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCGUNIMIS_2_3'),i_cWhere,'',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CGUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CGUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CGUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGUNIMIS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CGUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CGUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCGUNIMIS_2_4'),i_cWhere,'',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CGUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CGUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CGUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGCATSEM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCATSEM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_CGCATSEM)+"%");
                   +" and CGTIPCAT="+cp_ToStrODBC(this.w_TIPCAT);
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_CGCONTRI);

          i_ret=cp_SQL(i_nConn,"select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGTIPCAT,CGCONTRI,CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGTIPCAT',this.w_TIPCAT;
                     ,'CGCONTRI',this.w_CGCONTRI;
                     ,'CGCODICE',trim(this.w_CGCATSEM))
          select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGTIPCAT,CGCONTRI,CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCATSEM)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCATSEM) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGTIPCAT,CGCONTRI,CGCODICE',cp_AbsName(oSource.parent,'oCGCATSEM_2_8'),i_cWhere,'GSAR_MCT',"Categorie contributo",'GSAR_CC1.CATMCONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCAT<>oSource.xKey(1);
           .or. this.w_CGCONTRI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(3));
                     +" and CGTIPCAT="+cp_ToStrODBC(this.w_TIPCAT);
                     +" and CGCONTRI="+cp_ToStrODBC(this.w_CGCONTRI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGTIPCAT',oSource.xKey(1);
                       ,'CGCONTRI',oSource.xKey(2);
                       ,'CGCODICE',oSource.xKey(3))
            select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCATSEM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_CGCATSEM);
                   +" and CGTIPCAT="+cp_ToStrODBC(this.w_TIPCAT);
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_CGCONTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGTIPCAT',this.w_TIPCAT;
                       ,'CGCONTRI',this.w_CGCONTRI;
                       ,'CGCODICE',this.w_CGCATSEM)
            select CGTIPCAT,CGCONTRI,CGCODICE,CGDTOBSO,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCATSEM = NVL(_Link_.CGCODICE,space(5))
      this.w_DTOBSO1 = NVL(cp_ToDate(_Link_.CGDTOBSO),ctod("  /  /  "))
      this.w_CGDESCRI1 = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CGCATSEM = space(5)
      endif
      this.w_DTOBSO1 = ctod("  /  /  ")
      this.w_CGDESCRI1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY( .w_DTOBSO1 ) OR .w_DTOBSO1 > .w_CGDATINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CGCATSEM = space(5)
        this.w_DTOBSO1 = ctod("  /  /  ")
        this.w_CGDESCRI1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGTIPCAT,1)+'\'+cp_ToStr(_Link_.CGCONTRI,1)+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCATSEM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TPCODVAL
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_TPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_TPCODVAL)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TPCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_TPCODVAL = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGSISCOL
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SISMCOLL_IDX,3]
    i_lTable = "SISMCOLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2], .t., this.SISMCOLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGSISCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCL',True,'SISMCOLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SCCODICE like "+cp_ToStrODBC(trim(this.w_CGSISCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SCCODICE',trim(this.w_CGSISCOL))
          select SCCODICE,SCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGSISCOL)==trim(_Link_.SCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStrODBC(trim(this.w_CGSISCOL)+"%");

            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" SCDESCRI like "+cp_ToStr(trim(this.w_CGSISCOL)+"%");

            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGSISCOL) and !this.bDontReportError
            deferred_cp_zoom('SISMCOLL','*','SCCODICE',cp_AbsName(oSource.parent,'oCGSISCOL_1_40'),i_cWhere,'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',oSource.xKey(1))
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGSISCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODICE,SCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODICE="+cp_ToStrODBC(this.w_CGSISCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODICE',this.w_CGSISCOL)
            select SCCODICE,SCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGSISCOL = NVL(_Link_.SCCODICE,space(15))
      this.w_SCDESCRI = NVL(_Link_.SCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CGSISCOL = space(15)
      endif
      this.w_SCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_CGSISCOL,"SCCODCON",.w_CGCONTRI),""))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CGSISCOL = space(15)
        this.w_SCDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])+'\'+cp_ToStr(_Link_.SCCODICE,1)
      cp_ShowWarn(i_cKey,this.SISMCOLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGSISCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SISMCOLL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SISMCOLL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.SCCODICE as SCCODICE140"+ ",link_1_40.SCDESCRI as SCDESCRI140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on CATMCONT.CGSISCOL=link_1_40.SCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and CATMCONT.CGSISCOL=link_1_40.SCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCGCODICE_1_1.value==this.w_CGCODICE)
      this.oPgFrm.Page1.oPag.oCGCODICE_1_1.value=this.w_CGCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDESCRI_1_3.value==this.w_CGDESCRI)
      this.oPgFrm.Page1.oPag.oCGDESCRI_1_3.value=this.w_CGDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCONTRI_1_4.value==this.w_CGCONTRI)
      this.oPgFrm.Page1.oPag.oCGCONTRI_1_4.value=this.w_CGCONTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCGTIPCAT_1_5.RadioValue()==this.w_CGTIPCAT)
      this.oPgFrm.Page1.oPag.oCGTIPCAT_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCGSISCOL_1_6.value==this.w_CGSISCOL)
      this.oPgFrm.Page1.oPag.oCGSISCOL_1_6.value=this.w_CGSISCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.value==this.w_CGSERVIZ)
      this.oPgFrm.Page1.oPag.oCGSERVIZ_1_9.value=this.w_CGSERVIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPCO_1_11.value==this.w_DESTIPCO)
      this.oPgFrm.Page1.oPag.oDESTIPCO_1_11.value=this.w_DESTIPCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_13.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_13.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDTINVA_1_23.value==this.w_CGDTINVA)
      this.oPgFrm.Page1.oPag.oCGDTINVA_1_23.value=this.w_CGDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDTOBSO_1_25.value==this.w_CGDTOBSO)
      this.oPgFrm.Page1.oPag.oCGDTOBSO_1_25.value=this.w_CGDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESCRI_1_28.value==this.w_SCDESCRI)
      this.oPgFrm.Page1.oPag.oSCDESCRI_1_28.value=this.w_SCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTPCODVAL_1_36.value==this.w_TPCODVAL)
      this.oPgFrm.Page1.oPag.oTPCODVAL_1_36.value=this.w_TPCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oCG__NOTE_4_1.value==this.w_CG__NOTE)
      this.oPgFrm.Page2.oPag.oCG__NOTE_4_1.value=this.w_CG__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDESCRI1_2_13.value==this.w_CGDESCRI1)
      this.oPgFrm.Page1.oPag.oCGDESCRI1_2_13.value=this.w_CGDESCRI1
      replace t_CGDESCRI1 with this.oPgFrm.Page1.oPag.oCGDESCRI1_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCGSISCOL_1_40.value==this.w_CGSISCOL)
      this.oPgFrm.Page1.oPag.oCGSISCOL_1_40.value=this.w_CGSISCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDATINI_2_2.value==this.w_CGDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDATINI_2_2.value=this.w_CGDATINI
      replace t_CGDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDATINI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.value==this.w_CGUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.value=this.w_CGUNIMIS
      replace t_CGUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.value==this.w_CGUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.value=this.w_CGUNIMIS
      replace t_CGUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPNET_2_5.value==this.w_CGIMPNET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPNET_2_5.value=this.w_CGIMPNET
      replace t_CGIMPNET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPNET_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGPERCEN_2_6.value==this.w_CGPERCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGPERCEN_2_6.value=this.w_CGPERCEN
      replace t_CGPERCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGPERCEN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.RadioValue()==this.w_CGIMPZER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.SetRadio()
      replace t_CGIMPZER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCATSEM_2_8.value==this.w_CGCATSEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCATSEM_2_8.value=this.w_CGCATSEM
      replace t_CGCATSEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCATSEM_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'CATMCONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CGCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CGCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGCONTRI))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGCONTRI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CGCONTRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGSISCOL) or not(!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_CGSISCOL,"SCCODCON",.w_CGCONTRI),""))))  and not(.w_CGTIPCAT = 'C')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGSISCOL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CGSISCOL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARTIPSER = 'S' And !EMPTY(.w_CGCONTRI))  and not(.w_CGTIPCAT <> 'S')  and (.w_CGTIPCAT = 'S')  and not(empty(.w_CGSERVIZ))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGSERVIZ_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o contributo non specificato")
          case   not(!EMPTY(NVL(LOOKTAB("SISDCOLL","SCCODCON","SCCODICE",.w_CGSISCOL,"SCCODCON",.w_CGCONTRI),"")))  and not(.w_CGTIPCAT = 'S')  and not(empty(.w_CGSISCOL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGSISCOL_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mct
       IF !EMPTY(this.w_CGSERVIZ) and this.w_TPAPPPES='S' and this.w_CGTIPCAT = 'S' and i_bRes
       * Se il codice contributo ha il flag applicazione su peso
         * accetta unicamente servizi di tipo fuori magazzino con flag generico off e u.m. KG
         IF this.w_ARTIPART<>this.w_TIPART and i_bRes
          i_bnoChk = .f.
          i_bRes = .f.
          IF UPPER(g_APPLICATION) = "AD HOC ENTERPRISE"
            i_cErrorMsg = Ah_MsgFormat("Il codice contributo con applicazione su peso consente unicamente servizi di tipo fuori magazzino")
          ELSE
            i_cErrorMsg = Ah_MsgFormat("Il codice contributo con applicazione su peso consente unicamente servizi a quantit� e valore")
          ENDIF
         ENDIF
         IF this.w_ARFLSERG='S' and i_bRes
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Il codice contributo con applicazione su peso consente unicamente servizi senza l'attivazione del flag unit� di misura differenti")
         ENDIF
         IF this.w_ARUNMIS1<>this.w_TPUNMIKG and i_bRes
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg =Ah_MsgFormat( "Il codice contributo con applicazione su peso consente unicamente di inserire servizi con l'unit� di misura indicata nel tipo contributo come corrispondente al KG")
         ENDIF
       ENDIF
       IF this.w_CGTIPCAT='S' and EMPTY(this.w_CGSERVIZ) and i_bRes
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Se il tipo categoria � semplice occorre specificare il codice del servizio")
       endif
      
       IF this.w_CGTIPCAT='S' and this.w_ARCONTRI<>this.w_CGCONTRI and i_bRes
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Il codice contributo del servizio non corrisponde al tipo contributo indicato")
       ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND (!Empty(t_CGCATSEM) Or t_CGIMPNET<>0 Or t_CGPERCEN<>0 Or t_ZEROCHECK='S'));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CGDATINI) and (not(Empty(.w_CPROWORD)) AND (!Empty(.w_CGCATSEM) Or .w_CGIMPNET<>0 Or .w_CGPERCEN<>0 Or .w_ZEROCHECK='S'))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGDATINI_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_CGUNIMIS) and (.w_TPAPPPES <> 'S' AND .w_CGTIPCAT='S' AND .w_CGIMPZER <> 'S') and (not(Empty(.w_CPROWORD)) AND (!Empty(.w_CGCATSEM) Or .w_CGIMPNET<>0 Or .w_CGPERCEN<>0 Or .w_ZEROCHECK='S'))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGUNIMIS_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_CGPERCEN <= 100 AND (.w_TPAPPPES <> 'S' OR EMPTY( .w_CGPERCEN ))) and (.w_CGTIPCAT = 'S' And .w_CGIMPNET=0 AND ( .w_TPAPPPES <> 'S' OR !EMPTY( .w_CGPERCEN )) AND .w_CGIMPZER<>'S') and (not(Empty(.w_CPROWORD)) AND (!Empty(.w_CGCATSEM) Or .w_CGIMPNET<>0 Or .w_CGPERCEN<>0 Or .w_ZEROCHECK='S'))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGPERCEN_2_6
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(EMPTY( .w_DTOBSO1 ) OR .w_DTOBSO1 > .w_CGDATINI) and (.w_CGTIPCAT = 'C') and not(empty(.w_CGCATSEM)) and (not(Empty(.w_CPROWORD)) AND (!Empty(.w_CGCATSEM) Or .w_CGIMPNET<>0 Or .w_CGPERCEN<>0 Or .w_ZEROCHECK='S'))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCATSEM_2_8
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) AND (!Empty(.w_CGCATSEM) Or .w_CGIMPNET<>0 Or .w_CGPERCEN<>0 Or .w_ZEROCHECK='S')
        * --- Area Manuale = Check Row
        * --- gsar_mct
        IF this.w_CGTIPCAT='S' AND this.w_CGIMPNET=0 AND this.w_CGPERCEN=0 AND this.w_CGIMPZER<>'S' AND i_bRes
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = Ah_MsgFormat("Importo e percentuale mancanti: attivare il flag 'Zero'")
        ENDIF
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CGCONTRI = this.w_CGCONTRI
    this.o_CGTIPCAT = this.w_CGTIPCAT
    this.o_CGDATINI = this.w_CGDATINI
    this.o_CGUNIMIS = this.w_CGUNIMIS
    this.o_TPCODVAL = this.w_TPCODVAL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND (!Empty(t_CGCATSEM) Or t_CGIMPNET<>0 Or t_CGPERCEN<>0 Or t_ZEROCHECK='S'))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CGDATINI=ctod("  /  /  ")
      .w_CGUNIMIS=space(3)
      .w_CGUNIMIS=space(3)
      .w_CGIMPNET=0
      .w_CGPERCEN=0
      .w_CGIMPZER=space(1)
      .w_CGCATSEM=space(5)
      .w_CGDATFIN=ctod("  /  /  ")
      .w_DTOBSO1=ctod("  /  /  ")
      .w_ZEROCHECK=space(1)
      .w_CGDESCRI1=space(40)
      .DoRTCalc(1,8,.f.)
        .w_CGDATINI = DATE(YEAR(i_DATSYS),1,1)
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_CGUNIMIS))
        .link_2_3('Full')
      endif
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_CGUNIMIS))
        .link_2_4('Full')
      endif
      .DoRTCalc(12,15,.f.)
      if not(empty(.w_CGCATSEM))
        .link_2_8('Full')
      endif
      .DoRTCalc(16,33,.f.)
        .w_ZEROCHECK = .w_CGIMPZER
    endwith
    this.DoRTCalc(35,40,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CGDATINI = t_CGDATINI
    this.w_CGUNIMIS = t_CGUNIMIS
    this.w_CGUNIMIS = t_CGUNIMIS
    this.w_CGIMPNET = t_CGIMPNET
    this.w_CGPERCEN = t_CGPERCEN
    this.w_CGIMPZER = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.RadioValue(.t.)
    this.w_CGCATSEM = t_CGCATSEM
    this.w_CGDATFIN = t_CGDATFIN
    this.w_DTOBSO1 = t_DTOBSO1
    this.w_ZEROCHECK = t_ZEROCHECK
    this.w_CGDESCRI1 = t_CGDESCRI1
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CGDATINI with this.w_CGDATINI
    replace t_CGUNIMIS with this.w_CGUNIMIS
    replace t_CGUNIMIS with this.w_CGUNIMIS
    replace t_CGIMPNET with this.w_CGIMPNET
    replace t_CGPERCEN with this.w_CGPERCEN
    replace t_CGIMPZER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPZER_2_7.ToRadio()
    replace t_CGCATSEM with this.w_CGCATSEM
    replace t_CGDATFIN with this.w_CGDATFIN
    replace t_DTOBSO1 with this.w_DTOBSO1
    replace t_ZEROCHECK with this.w_ZEROCHECK
    replace t_CGDESCRI1 with this.w_CGDESCRI1
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mctPag1 as StdContainer
  Width  = 630
  height = 438
  stdWidth  = 630
  stdheight = 438
  resizeXpos=227
  resizeYpos=215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCGCODICE_1_1 as StdField with uid="COQWDEYLPV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CGCODICE", cQueryName = "CGCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria contributo",;
    HelpContextID = 130679915,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=20, InputMask=replicate('X',5)

  add object oCGDESCRI_1_3 as StdField with uid="FUQOZCMHZE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CGDESCRI", cQueryName = "CGDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45093999,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=20, InputMask=replicate('X',40)

  add object oCGCONTRI_1_4 as StdField with uid="IMFRFTYBGB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CGCONTRI", cQueryName = "CGCONTRI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contributo accessorio",;
    HelpContextID = 57279599,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=153, Top=47, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIPCONTR", cZoomOnZoom="GSAR_ATP", oKey_1_1="TPCODICE", oKey_1_2="this.w_CGCONTRI"

  func oCGCONTRI_1_4.mCond()
    with this.Parent.oContained
      if (.cFunction='Load')
        if .nLastRow>1
          return (.f.)
        endif
      else
        return (.f.)
      endif
    endwith
  endfunc

  func oCGCONTRI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CGCATSEM)
        bRes2=.link_2_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCGCONTRI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCONTRI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCONTR','*','TPCODICE',cp_AbsName(this.parent,'oCGCONTRI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATP',"Contributi accessori",'',this.parent.oContained
  endproc
  proc oCGCONTRI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TPCODICE=this.parent.oContained.w_CGCONTRI
    i_obj.ecpSave()
  endproc


  add object oCGTIPCAT_1_5 as StdCombo with uid="BPQTPSCEUI",rtseq=4,rtrep=.f.,left=153,top=77,width=119,height=21;
    , ToolTipText = "Tipo categoria (semplice\composto)";
    , HelpContextID = 42275962;
    , cFormVar="w_CGTIPCAT",RowSource=""+"Semplice,"+"Composta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCGTIPCAT_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGTIPCAT,&i_cF..t_CGTIPCAT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oCGTIPCAT_1_5.GetRadio()
    this.Parent.oContained.w_CGTIPCAT = this.RadioValue()
    return .t.
  endfunc

  func oCGTIPCAT_1_5.ToRadio()
    this.Parent.oContained.w_CGTIPCAT=trim(this.Parent.oContained.w_CGTIPCAT)
    return(;
      iif(this.Parent.oContained.w_CGTIPCAT=='S',1,;
      iif(this.Parent.oContained.w_CGTIPCAT=='C',2,;
      0)))
  endfunc

  func oCGTIPCAT_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCGTIPCAT_1_5.mCond()
    with this.Parent.oContained
      return (.cfunction <> 'Edit')
    endwith
  endfunc

  add object oCGSISCOL_1_6 as StdField with uid="FQGIVTRLSH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CGSISCOL", cQueryName = "CGSISCOL",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice sistema collettivo",;
    HelpContextID = 223017870,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=153, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SISMCOLL", cZoomOnZoom="GSAR_MCL", oKey_1_1="SCCODICE", oKey_1_2="this.w_CGSISCOL"

  func oCGSISCOL_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'C')
    endwith
    endif
  endfunc

  func oCGSISCOL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGSISCOL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGSISCOL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SISMCOLL','*','SCCODICE',cp_AbsName(this.parent,'oCGSISCOL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this.parent.oContained
  endproc
  proc oCGSISCOL_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SCCODICE=this.parent.oContained.w_CGSISCOL
    i_obj.ecpSave()
  endproc

  add object oCGSERVIZ_1_9 as StdField with uid="AFQWQLLQSK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CGSERVIZ", cQueryName = "CGSERVIZ",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o contributo non specificato",;
    ToolTipText = "Codice servizio fuori magazzino",;
    HelpContextID = 173996928,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=153, Top=138, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_CGSERVIZ"

  func oCGSERVIZ_1_9.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'S')
    endwith
  endfunc

  func oCGSERVIZ_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGTIPCAT <> 'S')
    endwith
    endif
  endfunc

  func oCGSERVIZ_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGSERVIZ_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGSERVIZ_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCGSERVIZ_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSAR_MCT.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCGSERVIZ_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CGSERVIZ
    i_obj.ecpSave()
  endproc

  add object oDESTIPCO_1_11 as StdField with uid="XVSCAKDQNC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESTIPCO", cQueryName = "DESTIPCO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 253756037,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=47, InputMask=replicate('X',40)

  add object oDESART_1_13 as StdField with uid="IEQJWUZSMR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 60621366,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=138, InputMask=replicate('X',40)

  func oDESART_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGTIPCAT <> 'S')
    endwith
    endif
  endfunc

  add object oCGDTINVA_1_23 as StdField with uid="HLCKZZCQKB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CGDTINVA", cQueryName = "CGDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 220140647,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=276, Top=410, TABSTOP=.F.

  add object oCGDTOBSO_1_25 as StdField with uid="IJRXMXIYEM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CGDTOBSO", cQueryName = "CGDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine validit�",;
    HelpContextID = 25105525,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=541, Top=410, TABSTOP=.F.

  add object oSCDESCRI_1_28 as StdField with uid="KKSZSSJEET",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SCDESCRI", cQueryName = "SCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45093231,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=107, InputMask=replicate('X',40)

  add object oTPCODVAL_1_36 as StdField with uid="OSBKFNCVKZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_TPCODVAL", cQueryName = "TPCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta del contributo",;
    HelpContextID = 80350850,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=558, Top=75, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_TPCODVAL"

  func oTPCODVAL_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCGSISCOL_1_40 as StdField with uid="ZLCBOGLMRE",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CGSISCOL", cQueryName = "CGSISCOL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice sistema collettivo",;
    HelpContextID = 223017870,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=153, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SISMCOLL", cZoomOnZoom="GSAR_MCL", oKey_1_1="SCCODICE", oKey_1_2="this.w_CGSISCOL"

  func oCGSISCOL_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'S')
    endwith
    endif
  endfunc

  func oCGSISCOL_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGSISCOL_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGSISCOL_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SISMCOLL','*','SCCODICE',cp_AbsName(this.parent,'oCGSISCOL_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCL',"Sistemi collettivi",'GSAR_CC2.SISMCOLL_VZM',this.parent.oContained
  endproc
  proc oCGSISCOL_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SCCODICE=this.parent.oContained.w_CGSISCOL
    i_obj.ecpSave()
  endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=168, width=616,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="CGDATINI",Label2=" Inizio validit� ",Field3="CGUNIMIS",Label3="U.M.  ",Field4="",Label4="",Field5="CGIMPNET",Label5=" Importo netto IVA ",Field6="CGPERCEN",Label6=" Quota percent. ",Field7="CGIMPZER",Label7="    Zero",Field8="CGCATSEM",Label8="Categoria contributo ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219001466

  add object oStr_1_2 as StdString with uid="ECFUFOFUBQ",Visible=.t., Left=17, Top=20,;
    Alignment=1, Width=133, Height=18,;
    Caption="Codice categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="BBZSGFKIBJ",Visible=.t., Left=17, Top=47,;
    Alignment=1, Width=133, Height=18,;
    Caption="Contributo accessorio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SIASBXQIOI",Visible=.t., Left=17, Top=138,;
    Alignment=1, Width=133, Height=18,;
    Caption="Codice servizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_CGTIPCAT <> 'S')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="MLDQLJGYLI",Visible=.t., Left=17, Top=77,;
    Alignment=1, Width=133, Height=18,;
    Caption="Tipo categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZUOFNRYFUZ",Visible=.t., Left=175, Top=410,;
    Alignment=1, Width=101, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RPCRYBPCRK",Visible=.t., Left=396, Top=410,;
    Alignment=1, Width=143, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BRVWCEJTXK",Visible=.t., Left=17, Top=109,;
    Alignment=1, Width=133, Height=18,;
    Caption="Sistema collettivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="AJMQQQOLJQ",Visible=.t., Left=400, Top=78,;
    Alignment=1, Width=154, Height=18,;
    Caption="Valuta del contributo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=187,;
    width=609+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=188,width=608+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='UNIMIS|CATMCONT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCGDESCRI1_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oCGUNIMIS_2_3
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oCGUNIMIS_2_4
      case cFile='CATMCONT'
        oDropInto=this.oBodyCol.oRow.oCGCATSEM_2_8
    endcase
    return(oDropInto)
  EndFunc


  add object oCGDESCRI1_2_13 as StdTrsField with uid="LZCIKPFZRG",rtseq=39,rtrep=.t.,;
    cFormVar="w_CGDESCRI1",value=space(40),enabled=.f.,;
    HelpContextID = 45094783,;
    cTotal="", bFixedPos=.t., cQueryName = "CGDESCRI1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=318, Left=292, Top=383, InputMask=replicate('X',40)

  add object oStr_2_12 as StdString with uid="KXEIJDZATY",Visible=.t., Left=82, Top=385,;
    Alignment=1, Width=203, Height=18,;
    Caption="Categoria contributo semplice:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsar_mctPag2 as StdContainer
    Width  = 630
    height = 438
    stdWidth  = 630
    stdheight = 438
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCG__NOTE_4_1 as StdMemo with uid="BYDIGDIMKB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CG__NOTE", cQueryName = "CG__NOTE",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 242992235,;
   bGlobalFont=.t.,;
    Height=387, Width=583, Left=10, Top=13
enddefine

* --- Defining Body row
define class tgsar_mctBodyRow as CPBodyRowCnt
  Width=599
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KYNSGCRFEM",rtseq=8,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 251329898,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCGDATINI_2_2 as StdTrsField with uid="SWCMRKHOCU",rtseq=9,rtrep=.t.,;
    cFormVar="w_CGDATINI",value=ctod("  /  /  "),;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 121891729,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=78, Left=54, Top=0

  add object oCGUNIMIS_2_3 as StdTrsField with uid="WFTFYVMAJX",rtseq=10,rtrep=.t.,;
    cFormVar="w_CGUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 65395591,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=150, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_CGUNIMIS"

  func oCGUNIMIS_2_3.mCond()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S' AND .w_CGTIPCAT='S' AND .w_CGIMPZER <> 'S')
    endwith
  endfunc

  func oCGUNIMIS_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGIMPZER = 'S')
    endwith
    endif
  endfunc

  func oCGUNIMIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGUNIMIS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCGUNIMIS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCGUNIMIS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'',this.parent.oContained
  endproc

  add object oCGUNIMIS_2_4 as StdTrsField with uid="MRATGRZJFA",rtseq=11,rtrep=.t.,;
    cFormVar="w_CGUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 65395591,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=134, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_CGUNIMIS"

  func oCGUNIMIS_2_4.mCond()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S' AND .w_CGTIPCAT='S')
    endwith
  endfunc

  func oCGUNIMIS_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CGIMPZER <> 'S')
    endwith
    endif
  endfunc

  func oCGUNIMIS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGUNIMIS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCGUNIMIS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCGUNIMIS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'',this.parent.oContained
  endproc

  add object oCGIMPNET_2_5 as StdTrsField with uid="HVFYBAJVCU",rtseq=12,rtrep=.t.,;
    cFormVar="w_CGIMPNET",value=0,;
    ToolTipText = "Importo netto IVA",;
    HelpContextID = 227042426,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=131, Left=201, Top=0, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)], BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1), bUseMenu=.t.

  func oCGIMPNET_2_5.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'S' And .w_CGPERCEN=0 AND .w_CGIMPZER<>'S')
    endwith
  endfunc

  add object oCGPERCEN_2_6 as StdTrsField with uid="FCZKLJWYPU",rtseq=13,rtrep=.t.,;
    cFormVar="w_CGPERCEN",value=0,;
    ToolTipText = "Quota percentuale alternativa all'importo netto",;
    HelpContextID = 44094580,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=85, Left=336, Top=0, cSayPict=["999.99999"], cGetPict=["999.99999"]

  func oCGPERCEN_2_6.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'S' And .w_CGIMPNET=0 AND ( .w_TPAPPPES <> 'S' OR !EMPTY( .w_CGPERCEN )) AND .w_CGIMPZER<>'S')
    endwith
  endfunc

  func oCGPERCEN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CGPERCEN <= 100 AND (.w_TPAPPPES <> 'S' OR EMPTY( .w_CGPERCEN )))
    endwith
    return bRes
  endfunc

  add object oCGIMPZER_2_7 as StdTrsCheck with uid="UVHFNDYUVS",rtrep=.t.,;
    cFormVar="w_CGIMPZER",  caption="",;
    ToolTipText = "Consente di aggiungere una riga servizio a prezzo zero",;
    HelpContextID = 159933560,;
    Left=425, Top=-1, Width=41,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oCGIMPZER_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGIMPZER,&i_cF..t_CGIMPZER),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCGIMPZER_2_7.GetRadio()
    this.Parent.oContained.w_CGIMPZER = this.RadioValue()
    return .t.
  endfunc

  func oCGIMPZER_2_7.ToRadio()
    this.Parent.oContained.w_CGIMPZER=trim(this.Parent.oContained.w_CGIMPZER)
    return(;
      iif(this.Parent.oContained.w_CGIMPZER=='S',1,;
      0))
  endfunc

  func oCGIMPZER_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCGIMPZER_2_7.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'S')
    endwith
  endfunc

  add object oCGCATSEM_2_8 as StdTrsField with uid="WGULQABBCS",rtseq=15,rtrep=.t.,;
    cFormVar="w_CGCATSEM",value=space(5),;
    ToolTipText = "Codice categoria contributo semplice",;
    HelpContextID = 45876339,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=469, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGTIPCAT", oKey_1_2="this.w_TIPCAT", oKey_2_1="CGCONTRI", oKey_2_2="this.w_CGCONTRI", oKey_3_1="CGCODICE", oKey_3_2="this.w_CGCATSEM"

  func oCGCATSEM_2_8.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCAT = 'C')
    endwith
  endfunc

  func oCGCATSEM_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCATSEM_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCGCATSEM_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CATMCONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPCAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStrODBC(this.Parent.oContained.w_CGCONTRI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPCAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStr(this.Parent.oContained.w_CGCONTRI)
    endif
    do cp_zoom with 'CATMCONT','*','CGTIPCAT,CGCONTRI,CGCODICE',cp_AbsName(this.parent,'oCGCATSEM_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"Categorie contributo",'GSAR_CC1.CATMCONT_VZM',this.parent.oContained
  endproc
  proc oCGCATSEM_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CGTIPCAT=w_TIPCAT
    i_obj.CGCONTRI=w_CGCONTRI
     i_obj.w_CGCODICE=this.parent.oContained.w_CGCATSEM
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mct','CATMCONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CGCODICE=CATMCONT.CGCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
