* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_blp                                                        *
*              Agg.riordino in artic.\magaz                                    *
*                                                                              *
*      Author: Zucchetti Tam S.p.A                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-22                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_blp",oParentObject,m.pEvent)
return(i_retval)

define class tgsma_blp as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_GSMA_SZR = .NULL.
  w_ZOOM = .NULL.
  ZOOM = .NULL.
  w_MESS = space(254)
  w_ARTICOLO = space(20)
  w_VARIANTE = space(20)
  w_CODVAR = space(20)
  w_PUNRIO = 0
  w_LOTRIO = 0
  w_AGGLOT = space(1)
  w_PROEST = space(1)
  w_PROVEN = space(1)
  w_COUNT = 0
  w_CONTA = 0
  w_RIGA = 0
  w_CHECKED = .f.
  * --- WorkFile variables
  PAR_RIOR_idx=0
  RIORDETT_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna PAR_RIOR
    * --- Scorro la tabella del Dettaglio Riordino per reperire il Lotto ed il Punto di Riordino
    * --- degli articoli elaborati.
    * --- Se il flag  'Aggiorna Riordino' presente su Dati Articolo\Magazzini � attivato, allora i campi
    * --- Lotto e Punto di Riordino dell'Anagrafica saranno aggiornati
    this.w_GSMA_SZR = this.oParentObject
    this.ZOOM = this.w_GSMA_SZR.w_ZoomRio
    this.w_ZOOM= this.w_GSMA_SZR.w_ZoomRio.cCursor
    do case
      case this.pEvent="AGG"
        ah_msg( "Aggiornamento dati in corso...")
        SELECT * FROM (this.W_ZOOM) WHERE XCHK=1 INTO CURSOR Agg
        =wrcursor("AGG")
        SELECT Agg 
 GO TOP
        SCAN
        this.w_COUNT = RECNO()
        this.w_PUNRIO = nvl(DIPUNAGG,0)
        this.w_LOTRIO = nvl(DILOTAGG,0)
        this.w_ARTICOLO = NVL(DICODART,SPACE(20))
        this.w_CODVAR = ""
        this.w_VARIANTE = ""
        this.w_PROVEN = NVL(DIPROVEN,"")
        * --- Leggo la provenienza dell'articolo.Se esterna il Lotto di Riordino ed il Punto non possono essere = 0
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARPROPRE"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARPROPRE;
            from (i_cTable) where;
                ARCODART = this.w_ARTICOLO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PROEST = NVL(cp_ToDate(_read_.ARPROPRE),cp_NullValue(_read_.ARPROPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.oParentObject.w_TIPAGG="L"
            if this.w_LOTRIO<>0
              * --- Write into PAR_RIOR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(this.w_LOTRIO),'PAR_RIOR','PRLOTRIO');
                +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
                    +i_ccchkf ;
                +" where ";
                    +"PRCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                       )
              else
                update (i_cTable) set;
                    PRLOTRIO = this.w_LOTRIO;
                    ,PRTIPCON = "F";
                    &i_ccchkf. ;
                 where;
                    PRCODART = this.w_ARTICOLO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              UPDATE Agg SET LOTAGG="S" where DICODART=this.w_ARTICOLO and DIPROVEN=this.w_PROVEN
            endif
          case this.oParentObject.w_TIPAGG="P"
            if this.w_PUNRIO<>0
              * --- Write into PAR_RIOR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRPUNRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PUNRIO),'PAR_RIOR','PRPUNRIO');
                +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
                    +i_ccchkf ;
                +" where ";
                    +"PRCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                       )
              else
                update (i_cTable) set;
                    PRPUNRIO = this.w_PUNRIO;
                    ,PRTIPCON = "F";
                    &i_ccchkf. ;
                 where;
                    PRCODART = this.w_ARTICOLO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              UPDATE Agg SET PUNAGG="S" where DICODART=this.w_ARTICOLO and DIPROVEN=this.w_PROVEN
            endif
          otherwise
            if this.w_LOTRIO<>0
              * --- Write into PAR_RIOR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(this.w_LOTRIO),'PAR_RIOR','PRLOTRIO');
                +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
                    +i_ccchkf ;
                +" where ";
                    +"PRCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                       )
              else
                update (i_cTable) set;
                    PRLOTRIO = this.w_LOTRIO;
                    ,PRTIPCON = "F";
                    &i_ccchkf. ;
                 where;
                    PRCODART = this.w_ARTICOLO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              UPDATE Agg SET LOTAGG="S" where DICODART=this.w_ARTICOLO and DIPROVEN=this.w_PROVEN
            endif
            if this.w_PUNRIO<>0
              * --- Write into PAR_RIOR
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRPUNRIO ="+cp_NullLink(cp_ToStrODBC(this.w_PUNRIO),'PAR_RIOR','PRPUNRIO');
                +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
                    +i_ccchkf ;
                +" where ";
                    +"PRCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                       )
              else
                update (i_cTable) set;
                    PRPUNRIO = this.w_PUNRIO;
                    ,PRTIPCON = "F";
                    &i_ccchkf. ;
                 where;
                    PRCODART = this.w_ARTICOLO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              UPDATE Agg SET PUNAGG="S" where DICODART=this.w_ARTICOLO and DICODVAR=this.w_CODVAR and DIPROVEN=this.w_PROVEN
            endif
        endcase
        SELECT Agg 
 GOTO this.w_COUNT
        ENDSCAN
        * --- --Effettuare la stampa dei dati elaborati
        SELECT * FROM Agg into cursor __tmp__
        ah_msg( "Stampa dati elaborazione in corso..." )
        CP_CHPRN("..\exe\query\GSMA_SZR","",this.oParentObject)
        this.w_MESS = "Aggiornamento Completato!"
        if used("Agg")
          select Agg
          use
        endif
        this.oParentObject.ecpQuit()     
      case this.pEvent="CHE"
        SELECT (this.w_ZOOM)
        this.w_RIGA = RECNO()
        * --- Propongo la quantit� da aggiornare
        if NVL(TIPGES," ")<>"S"
          cp_errormsg("Articolo di tipologia non a scorta, impossibile selezionare")
          Select (this.w_ZOOM) 
 GOTO this.w_RIGA
          UPDATE (this.w_ZOOM) set XCHK= 0 where TIPGES<>"S"
          this.ZOOM.Grd.Refresh()     
          Select (this.w_ZOOM) 
 GOTO this.w_RIGA
        else
          REPLACE DIPUNAGG with DIPUNRIO, DILOTAGG with DILOTRIO
          CALCULATE SUM(XCHK) TO this.w_CONTA
          if this.w_CONTA>0
            this.oParentObject.w_AGGIORNA = .T.
          else
            this.oParentObject.w_AGGIORNA = .F.
          endif
          this.ZOOM.Grd.Refresh()     
          Select (this.w_ZOOM) 
 GOTO this.w_RIGA
        endif
      case this.pEvent="SEL"
        * --- --SELEZIONA\DESELEZIONA 
        if used(this.w_ZOOM)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.w_ZOOM) SET xChk=1, DIPUNAGG = DIPUNRIO, DILOTAGG = DILOTRIO where xChk<>2
            this.oParentObject.w_AGGIORNA = .T.
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.w_ZOOM) SET xChk=0, DIPUNAGG=0, DILOTAGG=0 where xChk<>2
            this.oParentObject.w_AGGIORNA = .F.
          endif
          Select (this.w_ZOOM) 
 GO TOP
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_RIOR'
    this.cWorkTables[2]='RIORDETT'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
