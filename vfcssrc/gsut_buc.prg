* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_buc                                                        *
*              Gestione lista utenti attivi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-12                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_buc",oParentObject,m.pOperazione)
return(i_retval)

define class tgsut_buc as StdBatch
  * --- Local variables
  pOperazione = space(10)
  w_ZOOM = .NULL.
  w_PUNPAD = .NULL.
  w_STRCONN = space(20)
  w_DATECHK = ctod("  /  /  ")
  w_TIMECHK = space(8)
  w_UCDESOPE = space(50)
  * --- WorkFile variables
  AHEUSRCO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Lista Utenti Attivi
    * --- Variabili Locali
    * --- Inizializza ,,
    this.w_PUNPAD = this.oParentObject
    this.w_ZOOM = this.oParentObject.w_ZUTENTI
    * --- -------------------------------------------
    do case
      case this.pOperazione = "DELUTENTI"
        * --- Elimina i Codici Selezionati
        NC = this.w_ZOOM.cCursor
        SELECT &NC
        GO TOP
        * --- deve esistere almeno un Record Selezionato
        COUNT FOR XCHK=1 TO w_CONTA
        if w_CONTA=0
          ah_ErrorMsg("Non sono stati selezionati utenti da cancellare dalla lista",48)
          i_retcode = 'stop'
          return
        endif
        * --- Cicla sui record Selezionati
        if ah_YesNo ("Confermi richiesta di cancellazione degli utenti selezionati?")
          SELECT &NC
          GO TOP
          SCAN FOR XCHK=1
          this.w_STRCONN = nvl( &NC..UCCODCON , space(20) )
          * --- Try
          local bErr_03BFCE68
          bErr_03BFCE68=bTrsErr
          this.Try_03BFCE68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03BFCE68
          * --- End
          ENDSCAN
          * --- Aggiorna lista
          This.OparentObject.NotifyEvent("Aggiorna")
        endif
      case this.pOperazione = "SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        UPDATE &NC SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
      case this.pOperazione = "AGGIORNA"
        CHKZUUSR(this,"UPDATE")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PUNPAD.NotiFyEvent("Aggiorna")     
    endcase
  endproc
  proc Try_03BFCE68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if TYPE("g_DEBUGBLACKBOX")="L" AND g_DEBUGBLACKBOX AND cp_ExistTableDef("AHEUSRCK")
      this.w_UCDESOPE = cp_Translate("Forzata eliminazione")
      local w_value_UCORAUAT 
 w_value_UCORAUAT=" "
      this.w_DATECHK = cp_Date(@w_value_UCORAUAT)
      this.w_TIMECHK = w_value_UCORAUAT
      INSAHEUSRCK(this.w_UCDESOPE,this.w_STRCONN,this.w_DATECHK,this.w_TIMECHK)
    endif
    * --- Delete from AHEUSRCO
    i_nConn=i_TableProp[this.AHEUSRCO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEUSRCO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"UCCODCON = "+cp_ToStrODBC(this.w_STRCONN);
             )
    else
      delete from (i_cTable) where;
            UCCODCON = this.w_STRCONN;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOperazione)
    this.pOperazione=pOperazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AHEUSRCO'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperazione"
endproc
