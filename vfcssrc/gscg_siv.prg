* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_siv                                                        *
*              Importi IVA                                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-07                                                      *
* Last revis.: 2008-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_siv",oParentObject))

* --- Class definition
define class tgscg_siv as StdForm
  Top    = 22
  Left   = 81

  * --- Standard Properties
  Width  = 372
  Height = 109
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-01-18"
  HelpContextID=258614633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_siv"
  cComment = "Importi IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_IMPONI = 0
  w_IMPIVA = 0
  w_CODIVA = space(5)
  w_DEC = 0
  w_TIPREG = space(1)
  w_IVAPER = 0
  w_VALNAZ = space(5)
  w_CODVAL = space(3)
  w_TEMPN = 0
  w_CONFERMA = space(10)
  w_CAOVAL = 0
  w_CAONAZ = 0
  w_DATRIF = ctod('  /  /  ')
  w_TEST = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sivPag1","gscg_siv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMPONI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IMPONI=0
      .w_IMPIVA=0
      .w_CODIVA=space(5)
      .w_DEC=0
      .w_TIPREG=space(1)
      .w_IVAPER=0
      .w_VALNAZ=space(5)
      .w_CODVAL=space(3)
      .w_TEMPN=0
      .w_CONFERMA=space(10)
      .w_CAOVAL=0
      .w_CAONAZ=0
      .w_DATRIF=ctod("  /  /  ")
      .w_TEST=.f.
      .w_IMPONI=oParentObject.w_IMPONI
      .w_IMPIVA=oParentObject.w_IMPIVA
      .w_CODIVA=oParentObject.w_CODIVA
      .w_DEC=oParentObject.w_DEC
      .w_TIPREG=oParentObject.w_TIPREG
      .w_IVAPER=oParentObject.w_IVAPER
      .w_VALNAZ=oParentObject.w_VALNAZ
      .w_CODVAL=oParentObject.w_CODVAL
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_CAOVAL=oParentObject.w_CAOVAL
      .w_CAONAZ=oParentObject.w_CAONAZ
      .w_DATRIF=oParentObject.w_DATRIF
          .DoRTCalc(1,9,.f.)
        .w_CONFERMA = .T.
          .DoRTCalc(11,13,.f.)
        .w_TEST = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_IMPONI=.w_IMPONI
      .oParentObject.w_IMPIVA=.w_IMPIVA
      .oParentObject.w_CODIVA=.w_CODIVA
      .oParentObject.w_DEC=.w_DEC
      .oParentObject.w_TIPREG=.w_TIPREG
      .oParentObject.w_IVAPER=.w_IVAPER
      .oParentObject.w_VALNAZ=.w_VALNAZ
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_CAOVAL=.w_CAOVAL
      .oParentObject.w_CAONAZ=.w_CAONAZ
      .oParentObject.w_DATRIF=.w_DATRIF
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SRNVPYQXRV()
    with this
          * --- Calcola imposta e/o esegue scorporo
          .w_TEMPN = IIF( .w_TIPREG='C' ,.w_IMPONI - (.w_IMPONI/(1+(.w_IVAPER/100))) , (.w_IMPONI*.w_IVAPER)/100)
          .w_IMPIVA = IVAROUND(.w_TEMPN, .w_DEC, IIF(.w_TEMPN<0, 0, 1),.w_CODVAL)
          .w_IMPONI = IIF( .w_TIPREG='C' ,  .w_IMPONI - .w_IMPIVA , .w_IMPONI )
    endwith
  endproc
  proc Calculate_DHLIQUEANF()
    with this
          * --- Calcolo imponibile
          .w_IMPONI = IIF(not .w_TEST ,MON2VAL( this.oparentobject.oparentobject .w_IVIMPONI , .w_CAOVAL , .w_CAONAZ , .w_DATRIF , .w_VALNAZ , .w_DEC ), .w_IMPONI)
          .w_IMPIVA = IIF(not .w_TEST , MON2VAL( this.oparentobject.oparentobject .w_IVIMPIVA , .w_CAOVAL , .w_CAONAZ , .w_DATRIF , .w_VALNAZ , .w_DEC ) , .w_IMPIVA )
          .w_TEST = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIMPIVA_1_2.enabled = this.oPgFrm.Page1.oPag.oIMPIVA_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_IMPONI Changed")
          .Calculate_SRNVPYQXRV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_IMPONI Gotfocus")
          .Calculate_DHLIQUEANF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIMPONI_1_1.value==this.w_IMPONI)
      this.oPgFrm.Page1.oPag.oIMPONI_1_1.value=this.w_IMPONI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIVA_1_2.value==this.w_IMPIVA)
      this.oPgFrm.Page1.oPag.oIMPIVA_1_2.value=this.w_IMPIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_3.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_3.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_14.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_14.value=this.w_CODVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_sivPag1 as StdContainer
  Width  = 368
  height = 109
  stdWidth  = 368
  stdheight = 109
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMPONI_1_1 as StdField with uid="PQYQZUOOYQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IMPONI", cQueryName = "IMPONI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 248130694,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=74, Top=23, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oIMPIVA_1_2 as StdField with uid="FCYDSGVJQB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IMPIVA", cQueryName = "IMPIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta",;
    HelpContextID = 121908358,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=219, Top=23, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPIVA_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVAPER<>0)
    endwith
   endif
  endfunc

  add object oCODIVA_1_3 as StdField with uid="RANWBBMHQF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA",;
    HelpContextID = 121859622,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=13, Top=23, InputMask=replicate('X',5)


  add object oBtn_1_7 as StdButton with uid="ZGGYGCZZRP",left=253, top=58, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 22258246;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="LAYJJYFVLP",left=310, top=58, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 251297210;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODVAL_1_14 as StdField with uid="YHLDSZHYJN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta importi",;
    HelpContextID = 16805414,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=145, Top=64, InputMask=replicate('X',3)

  add object oStr_1_4 as StdString with uid="PSGKJCXJTH",Visible=.t., Left=13, Top=5,;
    Alignment=0, Width=52, Height=15,;
    Caption="Cod.IVA"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="FXWQXDDFWB",Visible=.t., Left=74, Top=5,;
    Alignment=0, Width=94, Height=15,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="NEHFRMUGSK",Visible=.t., Left=219, Top=5,;
    Alignment=0, Width=54, Height=15,;
    Caption="Imposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IXXJOBDKAP",Visible=.t., Left=41, Top=66,;
    Alignment=1, Width=101, Height=15,;
    Caption="Valuta importi:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_siv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
