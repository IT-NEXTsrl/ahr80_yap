* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kas                                                        *
*              Selezione attributi                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-30                                                      *
* Last revis.: 2012-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kas",oParentObject))

* --- Class definition
define class tgsar_kas as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 481
  Height = 395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-17"
  HelpContextID=132740969
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  FAM_ATTR_IDX = 0
  cPrg = "gsar_kas"
  cComment = "Selezione attributi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CACODMOD = space(20)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_AUTOMA = space(1)
  w_ZCONTI = .F.
  w_FR_CAMPO = space(50)
  w_FR_TABLE = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_FILTER = space(254)
  w_ZATTRIB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kasPag1","gsar_kas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZATTRIB = this.oPgFrm.Pages(1).oPag.ZATTRIB
    DoDefault()
    proc Destroy()
      this.w_ZATTRIB = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FAM_ATTR'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODMOD=space(20)
      .w_CACODGRU=space(10)
      .w_CACODFAM=space(10)
      .w_AUTOMA=space(1)
      .w_ZCONTI=.f.
      .w_FR_CAMPO=space(50)
      .w_FR_TABLE=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_FILTER=space(254)
      .w_AUTOMA=oParentObject.w_AUTOMA
      .w_FILTER=oParentObject.w_FILTER
        .w_CACODMOD = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_MODATTR,this.oParentObject.oParentObject.w_CACODMOD)
        .w_CACODGRU = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_ASCODATT,this.oParentObject.oParentObject.w_CACODGRU)
        .w_CACODFAM = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_ASCODFAM,this.oParentObject.oParentObject.w_CACODFAM)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CACODFAM))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.ZATTRIB.Calculate()
          .DoRTCalc(4,7,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AUTOMA=.w_AUTOMA
      .oParentObject.w_FILTER=.w_FILTER
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsar_kas
    This.oParentObject.w_CONFE=.t.
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CACODMOD = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_MODATTR,this.oParentObject.oParentObject.w_CACODMOD)
            .w_CACODGRU = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_ASCODATT,this.oParentObject.oParentObject.w_CACODGRU)
            .w_CACODFAM = IIF(UPPER(this.oParentObject.oParentObject.CLASS)$"TCGSAR_MRA-TCGSAR_MR2",this.oParentObject.oParentObject.w_ASCODFAM,this.oParentObject.oParentObject.w_CACODFAM)
          .link_1_3('Full')
        .oPgFrm.Page1.oPag.ZATTRIB.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZATTRIB.Calculate()
    endwith
  return

  proc Calculate_FFZSTENYMP()
    with this
          * --- Salvataggio attributi selezionati
          gsar_bmg(this;
              ,'SAVAT';
             )
    endwith
  endproc
  proc Calculate_KSSPLVRQXO()
    with this
          * --- Doppio click su riga zoom
          gsar_bmg(this;
              ,'SELEC';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_kas
    if cEvent = "FormLoad"
      IF UPPER( this.oparentobject.w_PUNPAD.CLASS ) = "TCGSAR_MRA" OR UPPER( this.oparentobject.w_PUNPAD.CLASS ) = "TCGSAR_MR2"
        if not Empty(this.oparentobject.w_PUNPAD.w_ASCODFAM) and not empty(this.oparentobject.w_PUNPAD.w_ASCODATT)
          if not empty(this.oparentobject.w_PUNPAD.w_FR_TABLE)
            this.w_ZATTRIB.cTable = alltrim(this.oparentobject.w_PUNPAD.w_FR_TABLE)
            * this.width = 800
            * this.height = 600
          endif
          if not empty(this.oparentobject.w_PUNPAD.w_FR__ZOOM)
            this.w_ZATTRIB.cZoomFile = alltrim(this.oparentobject.w_PUNPAD.w_FR__ZOOM)
          endif
        endif
      ELSE
        if not Empty(this.oparentobject.w_PUNPAD.w_CACODFAM) and not empty(this.oparentobject.w_PUNPAD.w_CACODGRU)
          if not empty(this.oparentobject.w_PUNPAD.w_FR_TABLE)
            this.w_ZATTRIB.cTable = alltrim(this.oparentobject.w_PUNPAD.w_FR_TABLE)
            * this.width = 800
            * this.height = 600
          endif
          if not empty(this.oparentobject.w_PUNPAD.w_FR__ZOOM)
            this.w_ZATTRIB.cZoomFile = alltrim(this.oparentobject.w_PUNPAD.w_FR__ZOOM)
          endif
        endif    
      ENDIF
    endif
    
    if cEvent = "ZATTRIB after query"
       if not empty( this.oParentObject.w_FILTER )
         L_FILTER = this.oParentObject.w_FILTER
         DELETE FROM (this.w_ZATTRIB.cCursor) where not (&L_FILTER)
         select (this.w_ZATTRIB.cCursor)
         GO TOP
       endif
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZATTRIB.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_FFZSTENYMP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zattrib selected")
          .Calculate_KSSPLVRQXO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_kas
    if cEvent="Init"
      select * from (this.w_ZATTRIB.cCursor) into cursor _conteggiorecord_
      if RECCOUNT("_conteggiorecord_")=1
        if TYPE("g_SCHEDULER")="U"
         public g_SCHEDULER
        endif
        g_SCHEDULER='S'
        select (this.w_ZATTRIB.cCursor)
        go top
        replace xChk with 1
      endif
      if used ("_conteggiorecord_")
         select _conteggiorecord_
         use
      endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODFAM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FR_CAMPO,FR_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_CACODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_CACODFAM)
            select FRCODICE,FR_CAMPO,FR_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODFAM = NVL(_Link_.FRCODICE,space(10))
      this.w_FR_CAMPO = NVL(_Link_.FR_CAMPO,space(50))
      this.w_FR_TABLE = NVL(_Link_.FR_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CACODFAM = space(10)
      endif
      this.w_FR_CAMPO = space(50)
      this.w_FR_TABLE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kasPag1 as StdContainer
  Width  = 477
  height = 395
  stdWidth  = 477
  stdheight = 395
  resizeXpos=331
  resizeYpos=234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZATTRIB as cp_szoombox with uid="VJRQVPCZVK",left=5, top=8, width=465,height=323,;
    caption='ZATTRIB',;
   bGlobalFont=.t.,;
    cMenuFile="",cZoomOnZoom="",cTable="FAZDATTR",cZoomFile="GSAR_KAS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 110104470


  add object oBtn_1_6 as StdButton with uid="GQMUPTTTCY",left=6, top=337, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli attributi";
    , HelpContextID = 226640406;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="ZCYHLDNLVF",left=54, top=337, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli attributi";
    , HelpContextID = 226640406;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="GUQBLQMZBT",left=102, top=337, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione di tutti gli attributi";
    , HelpContextID = 226640406;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="EXCMYMDFSJ",left=368, top=337, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'aggiornamento";
    , HelpContextID = 226640406;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="IIRYAWBJZR",left=421, top=337, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226640406;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kas','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
