* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut2kxp                                                        *
*              Corporate Portal - invio documenti                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_215]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-31                                                      *
* Last revis.: 2015-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut2kxp",oParentObject))

* --- Class definition
define class tgsut2kxp as StdForm
  Top    = 68
  Left   = 20

  * --- Standard Properties
  Width  = 611
  Height = 302+35
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-19"
  HelpContextID=250199191
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=143

  * --- Constant Properties
  _IDX = 0
  ZGRUPPI_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  ZRUOLI_IDX = 0
  ZFOLDERS_IDX = 0
  cPrg = "gsut2kxp"
  cComment = "Corporate Portal - invio documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OTIPGRU = space(1)
  w_MASKREJECTED = .F.
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_RISERVATO = space(1)
  o_RISERVATO = space(1)
  w_TIPDES1 = space(1)
  o_TIPDES1 = space(1)
  w_TIPCON1 = space(1)
  w_CODGRU1 = 0
  w_CODAGE1 = space(5)
  o_CODAGE1 = space(5)
  w_CODCON1 = space(15)
  o_CODCON1 = space(15)
  w_CODROL1 = 0
  w_READ1 = space(1)
  w_WRITE1 = space(1)
  w_DELETE1 = space(1)
  w_ZCPFTITLE = space(200)
  o_ZCPFTITLE = space(200)
  w_FOLDCOMP = 0
  w_FOLDCOMU = 0
  w_ZCPCFOLDER = 0
  o_ZCPCFOLDER = 0
  w_ZCPTFOLDER = space(1)
  w_ZCPDFOLDER = space(50)
  w_ZCPPFOLDER = space(255)
  o_ZCPPFOLDER = space(255)
  w_ZCPFDESCRI = space(0)
  w_DESGRU1 = space(50)
  w_DESCON1 = space(40)
  w_DESAGE1 = space(35)
  w_TIPDES2 = space(1)
  o_TIPDES2 = space(1)
  w_TIPCON2 = space(1)
  w_CODGRU2 = 0
  w_DESGRU2 = space(50)
  w_CODCON2 = space(15)
  w_DESCON2 = space(40)
  w_CODAGE2 = space(5)
  w_DESAGE2 = space(35)
  w_CODROL2 = 0
  w_READ2 = space(1)
  w_WRITE2 = space(1)
  w_DELETE2 = space(1)
  w_TIPDES3 = space(1)
  o_TIPDES3 = space(1)
  w_TIPCON3 = space(1)
  w_CODGRU3 = 0
  w_DESGRU3 = space(50)
  w_CODCON3 = space(15)
  w_DESCON3 = space(40)
  w_CODAGE3 = space(5)
  w_DESAGE3 = space(35)
  w_CODROL3 = 0
  w_READ3 = space(1)
  w_WRITE3 = space(1)
  w_DELETE3 = space(1)
  w_TIPDES4 = space(1)
  o_TIPDES4 = space(1)
  w_CODGRU4 = 0
  w_DESGRU4 = space(50)
  w_TIPCON4 = space(1)
  w_CODCON4 = space(15)
  w_DESCON4 = space(40)
  w_CODAGE4 = space(5)
  w_DESAGE4 = space(35)
  w_CODROL4 = 0
  w_READ4 = space(1)
  w_WRITE4 = space(1)
  w_DELETE4 = space(1)
  w_TIPDES5 = space(1)
  o_TIPDES5 = space(1)
  w_CODGRU5 = 0
  w_DESGRU5 = space(50)
  w_TIPCON5 = space(1)
  w_CODCON5 = space(15)
  w_DESCON5 = space(40)
  w_CODAGE5 = space(5)
  w_DESAGE5 = space(35)
  w_CODROL5 = 0
  w_READ5 = space(1)
  w_WRITE5 = space(1)
  w_DELETE5 = space(1)
  w_TIPDES6 = space(1)
  o_TIPDES6 = space(1)
  w_CODGRU6 = 0
  w_DESGRU6 = space(50)
  w_TIPCON6 = space(1)
  w_CODCON6 = space(15)
  w_DESCON6 = space(40)
  w_CODAGE6 = space(5)
  w_DESAGE6 = space(35)
  w_CODROL6 = 0
  w_READ6 = space(1)
  w_WRITE6 = space(1)
  w_DELETE6 = space(1)
  w_TIPDES7 = space(1)
  o_TIPDES7 = space(1)
  w_CODGRU7 = 0
  w_DESGRU7 = space(50)
  w_TIPCON7 = space(1)
  w_CODCON7 = space(15)
  w_DESCON7 = space(40)
  w_CODAGE7 = space(5)
  w_DESAGE7 = space(35)
  w_CODROL7 = 0
  w_READ7 = space(1)
  w_WRITE7 = space(1)
  w_DELETE7 = space(1)
  w_TIPDES8 = space(1)
  o_TIPDES8 = space(1)
  w_CODGRU8 = 0
  w_DESGRU8 = space(50)
  w_TIPCON8 = space(1)
  w_CODCON8 = space(15)
  w_DESCON8 = space(40)
  w_CODAGE8 = space(5)
  w_DESAGE8 = space(35)
  w_CODROL8 = 0
  w_READ8 = space(1)
  w_WRITE8 = space(1)
  w_DELETE8 = space(1)
  w_TIPDES9 = space(1)
  o_TIPDES9 = space(1)
  w_CODGRU9 = 0
  w_DESGRU9 = space(50)
  w_TIPCON9 = space(1)
  w_CODCON9 = space(15)
  w_DESCON9 = space(40)
  w_DESAGE9 = space(35)
  w_CODROL9 = 0
  w_READ9 = space(1)
  w_WRITE9 = space(1)
  w_DELETE9 = space(1)
  w_TIPDES10 = space(1)
  o_TIPDES10 = space(1)
  w_CODGRU10 = 0
  w_DESGRU10 = space(50)
  w_TIPCON10 = space(1)
  w_CODCON10 = space(15)
  w_DESCON10 = space(40)
  w_CODAGE10 = space(5)
  w_DESAGE10 = space(35)
  w_CODROL10 = 0
  w_READ10 = space(1)
  w_WRITE10 = space(1)
  w_DELETE10 = space(1)
  w_GRUPTYPE = space(1)
  w_GRUPTYPE2 = space(1)
  w_GRUPTYPE3 = space(1)
  w_GRUPTYPE4 = space(1)
  w_GRUPTYPE5 = space(1)
  w_GRUPTYPE6 = space(1)
  w_GRUPTYPE7 = space(1)
  w_GRUPTYPE8 = space(1)
  w_GRUPTYPE9 = space(1)
  w_GRUPTYPE10 = space(1)
  w_CODAGE9 = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut2kxpPag1","gsut2kxp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Identificazione")
      .Pages(2).addobject("oPag","tgsut2kxpPag2","gsut2kxp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Altri permessi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRISERVATO_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ZGRUPPI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='ZRUOLI'
    this.cWorkTables[5]='ZFOLDERS'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OTIPGRU=space(1)
      .w_MASKREJECTED=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_RISERVATO=space(1)
      .w_TIPDES1=space(1)
      .w_TIPCON1=space(1)
      .w_CODGRU1=0
      .w_CODAGE1=space(5)
      .w_CODCON1=space(15)
      .w_CODROL1=0
      .w_READ1=space(1)
      .w_WRITE1=space(1)
      .w_DELETE1=space(1)
      .w_ZCPFTITLE=space(200)
      .w_FOLDCOMP=0
      .w_FOLDCOMU=0
      .w_ZCPCFOLDER=0
      .w_ZCPTFOLDER=space(1)
      .w_ZCPDFOLDER=space(50)
      .w_ZCPPFOLDER=space(255)
      .w_ZCPFDESCRI=space(0)
      .w_DESGRU1=space(50)
      .w_DESCON1=space(40)
      .w_DESAGE1=space(35)
      .w_TIPDES2=space(1)
      .w_TIPCON2=space(1)
      .w_CODGRU2=0
      .w_DESGRU2=space(50)
      .w_CODCON2=space(15)
      .w_DESCON2=space(40)
      .w_CODAGE2=space(5)
      .w_DESAGE2=space(35)
      .w_CODROL2=0
      .w_READ2=space(1)
      .w_WRITE2=space(1)
      .w_DELETE2=space(1)
      .w_TIPDES3=space(1)
      .w_TIPCON3=space(1)
      .w_CODGRU3=0
      .w_DESGRU3=space(50)
      .w_CODCON3=space(15)
      .w_DESCON3=space(40)
      .w_CODAGE3=space(5)
      .w_DESAGE3=space(35)
      .w_CODROL3=0
      .w_READ3=space(1)
      .w_WRITE3=space(1)
      .w_DELETE3=space(1)
      .w_TIPDES4=space(1)
      .w_CODGRU4=0
      .w_DESGRU4=space(50)
      .w_TIPCON4=space(1)
      .w_CODCON4=space(15)
      .w_DESCON4=space(40)
      .w_CODAGE4=space(5)
      .w_DESAGE4=space(35)
      .w_CODROL4=0
      .w_READ4=space(1)
      .w_WRITE4=space(1)
      .w_DELETE4=space(1)
      .w_TIPDES5=space(1)
      .w_CODGRU5=0
      .w_DESGRU5=space(50)
      .w_TIPCON5=space(1)
      .w_CODCON5=space(15)
      .w_DESCON5=space(40)
      .w_CODAGE5=space(5)
      .w_DESAGE5=space(35)
      .w_CODROL5=0
      .w_READ5=space(1)
      .w_WRITE5=space(1)
      .w_DELETE5=space(1)
      .w_TIPDES6=space(1)
      .w_CODGRU6=0
      .w_DESGRU6=space(50)
      .w_TIPCON6=space(1)
      .w_CODCON6=space(15)
      .w_DESCON6=space(40)
      .w_CODAGE6=space(5)
      .w_DESAGE6=space(35)
      .w_CODROL6=0
      .w_READ6=space(1)
      .w_WRITE6=space(1)
      .w_DELETE6=space(1)
      .w_TIPDES7=space(1)
      .w_CODGRU7=0
      .w_DESGRU7=space(50)
      .w_TIPCON7=space(1)
      .w_CODCON7=space(15)
      .w_DESCON7=space(40)
      .w_CODAGE7=space(5)
      .w_DESAGE7=space(35)
      .w_CODROL7=0
      .w_READ7=space(1)
      .w_WRITE7=space(1)
      .w_DELETE7=space(1)
      .w_TIPDES8=space(1)
      .w_CODGRU8=0
      .w_DESGRU8=space(50)
      .w_TIPCON8=space(1)
      .w_CODCON8=space(15)
      .w_DESCON8=space(40)
      .w_CODAGE8=space(5)
      .w_DESAGE8=space(35)
      .w_CODROL8=0
      .w_READ8=space(1)
      .w_WRITE8=space(1)
      .w_DELETE8=space(1)
      .w_TIPDES9=space(1)
      .w_CODGRU9=0
      .w_DESGRU9=space(50)
      .w_TIPCON9=space(1)
      .w_CODCON9=space(15)
      .w_DESCON9=space(40)
      .w_DESAGE9=space(35)
      .w_CODROL9=0
      .w_READ9=space(1)
      .w_WRITE9=space(1)
      .w_DELETE9=space(1)
      .w_TIPDES10=space(1)
      .w_CODGRU10=0
      .w_DESGRU10=space(50)
      .w_TIPCON10=space(1)
      .w_CODCON10=space(15)
      .w_DESCON10=space(40)
      .w_CODAGE10=space(5)
      .w_DESAGE10=space(35)
      .w_CODROL10=0
      .w_READ10=space(1)
      .w_WRITE10=space(1)
      .w_DELETE10=space(1)
      .w_GRUPTYPE=space(1)
      .w_GRUPTYPE2=space(1)
      .w_GRUPTYPE3=space(1)
      .w_GRUPTYPE4=space(1)
      .w_GRUPTYPE5=space(1)
      .w_GRUPTYPE6=space(1)
      .w_GRUPTYPE7=space(1)
      .w_GRUPTYPE8=space(1)
      .w_GRUPTYPE9=space(1)
      .w_GRUPTYPE10=space(1)
      .w_CODAGE9=space(5)
      .w_MASKREJECTED=oParentObject.w_MASKREJECTED
      .w_RISERVATO=oParentObject.w_RISERVATO
      .w_TIPDES1=oParentObject.w_TIPDES1
      .w_TIPCON1=oParentObject.w_TIPCON1
      .w_CODGRU1=oParentObject.w_CODGRU1
      .w_CODAGE1=oParentObject.w_CODAGE1
      .w_CODCON1=oParentObject.w_CODCON1
      .w_CODROL1=oParentObject.w_CODROL1
      .w_READ1=oParentObject.w_READ1
      .w_WRITE1=oParentObject.w_WRITE1
      .w_DELETE1=oParentObject.w_DELETE1
      .w_ZCPFTITLE=oParentObject.w_ZCPFTITLE
      .w_FOLDCOMP=oParentObject.w_FOLDCOMP
      .w_FOLDCOMU=oParentObject.w_FOLDCOMU
      .w_ZCPCFOLDER=oParentObject.w_ZCPCFOLDER
      .w_ZCPTFOLDER=oParentObject.w_ZCPTFOLDER
      .w_ZCPDFOLDER=oParentObject.w_ZCPDFOLDER
      .w_ZCPPFOLDER=oParentObject.w_ZCPPFOLDER
      .w_ZCPFDESCRI=oParentObject.w_ZCPFDESCRI
      .w_DESGRU1=oParentObject.w_DESGRU1
      .w_DESCON1=oParentObject.w_DESCON1
      .w_DESAGE1=oParentObject.w_DESAGE1
      .w_TIPDES2=oParentObject.w_TIPDES2
      .w_TIPCON2=oParentObject.w_TIPCON2
      .w_CODGRU2=oParentObject.w_CODGRU2
      .w_DESGRU2=oParentObject.w_DESGRU2
      .w_CODCON2=oParentObject.w_CODCON2
      .w_DESCON2=oParentObject.w_DESCON2
      .w_CODAGE2=oParentObject.w_CODAGE2
      .w_DESAGE2=oParentObject.w_DESAGE2
      .w_CODROL2=oParentObject.w_CODROL2
      .w_READ2=oParentObject.w_READ2
      .w_WRITE2=oParentObject.w_WRITE2
      .w_DELETE2=oParentObject.w_DELETE2
      .w_TIPDES3=oParentObject.w_TIPDES3
      .w_TIPCON3=oParentObject.w_TIPCON3
      .w_CODGRU3=oParentObject.w_CODGRU3
      .w_DESGRU3=oParentObject.w_DESGRU3
      .w_CODCON3=oParentObject.w_CODCON3
      .w_DESCON3=oParentObject.w_DESCON3
      .w_CODAGE3=oParentObject.w_CODAGE3
      .w_DESAGE3=oParentObject.w_DESAGE3
      .w_CODROL3=oParentObject.w_CODROL3
      .w_READ3=oParentObject.w_READ3
      .w_WRITE3=oParentObject.w_WRITE3
      .w_DELETE3=oParentObject.w_DELETE3
      .w_TIPDES4=oParentObject.w_TIPDES4
      .w_CODGRU4=oParentObject.w_CODGRU4
      .w_DESGRU4=oParentObject.w_DESGRU4
      .w_TIPCON4=oParentObject.w_TIPCON4
      .w_CODCON4=oParentObject.w_CODCON4
      .w_DESCON4=oParentObject.w_DESCON4
      .w_CODAGE4=oParentObject.w_CODAGE4
      .w_DESAGE4=oParentObject.w_DESAGE4
      .w_CODROL4=oParentObject.w_CODROL4
      .w_READ4=oParentObject.w_READ4
      .w_WRITE4=oParentObject.w_WRITE4
      .w_DELETE4=oParentObject.w_DELETE4
      .w_TIPDES5=oParentObject.w_TIPDES5
      .w_CODGRU5=oParentObject.w_CODGRU5
      .w_DESGRU5=oParentObject.w_DESGRU5
      .w_TIPCON5=oParentObject.w_TIPCON5
      .w_CODCON5=oParentObject.w_CODCON5
      .w_DESCON5=oParentObject.w_DESCON5
      .w_CODAGE5=oParentObject.w_CODAGE5
      .w_DESAGE5=oParentObject.w_DESAGE5
      .w_CODROL5=oParentObject.w_CODROL5
      .w_READ5=oParentObject.w_READ5
      .w_WRITE5=oParentObject.w_WRITE5
      .w_DELETE5=oParentObject.w_DELETE5
      .w_TIPDES6=oParentObject.w_TIPDES6
      .w_CODGRU6=oParentObject.w_CODGRU6
      .w_DESGRU6=oParentObject.w_DESGRU6
      .w_TIPCON6=oParentObject.w_TIPCON6
      .w_CODCON6=oParentObject.w_CODCON6
      .w_DESCON6=oParentObject.w_DESCON6
      .w_CODAGE6=oParentObject.w_CODAGE6
      .w_DESAGE6=oParentObject.w_DESAGE6
      .w_CODROL6=oParentObject.w_CODROL6
      .w_READ6=oParentObject.w_READ6
      .w_WRITE6=oParentObject.w_WRITE6
      .w_DELETE6=oParentObject.w_DELETE6
      .w_TIPDES7=oParentObject.w_TIPDES7
      .w_CODGRU7=oParentObject.w_CODGRU7
      .w_DESGRU7=oParentObject.w_DESGRU7
      .w_TIPCON7=oParentObject.w_TIPCON7
      .w_CODCON7=oParentObject.w_CODCON7
      .w_DESCON7=oParentObject.w_DESCON7
      .w_CODAGE7=oParentObject.w_CODAGE7
      .w_DESAGE7=oParentObject.w_DESAGE7
      .w_CODROL7=oParentObject.w_CODROL7
      .w_READ7=oParentObject.w_READ7
      .w_WRITE7=oParentObject.w_WRITE7
      .w_DELETE7=oParentObject.w_DELETE7
      .w_TIPDES8=oParentObject.w_TIPDES8
      .w_CODGRU8=oParentObject.w_CODGRU8
      .w_DESGRU8=oParentObject.w_DESGRU8
      .w_TIPCON8=oParentObject.w_TIPCON8
      .w_CODCON8=oParentObject.w_CODCON8
      .w_DESCON8=oParentObject.w_DESCON8
      .w_CODAGE8=oParentObject.w_CODAGE8
      .w_DESAGE8=oParentObject.w_DESAGE8
      .w_CODROL8=oParentObject.w_CODROL8
      .w_READ8=oParentObject.w_READ8
      .w_WRITE8=oParentObject.w_WRITE8
      .w_DELETE8=oParentObject.w_DELETE8
      .w_TIPDES9=oParentObject.w_TIPDES9
      .w_CODGRU9=oParentObject.w_CODGRU9
      .w_DESGRU9=oParentObject.w_DESGRU9
      .w_TIPCON9=oParentObject.w_TIPCON9
      .w_CODCON9=oParentObject.w_CODCON9
      .w_DESCON9=oParentObject.w_DESCON9
      .w_DESAGE9=oParentObject.w_DESAGE9
      .w_CODROL9=oParentObject.w_CODROL9
      .w_READ9=oParentObject.w_READ9
      .w_WRITE9=oParentObject.w_WRITE9
      .w_DELETE9=oParentObject.w_DELETE9
      .w_TIPDES10=oParentObject.w_TIPDES10
      .w_CODGRU10=oParentObject.w_CODGRU10
      .w_DESGRU10=oParentObject.w_DESGRU10
      .w_TIPCON10=oParentObject.w_TIPCON10
      .w_CODCON10=oParentObject.w_CODCON10
      .w_DESCON10=oParentObject.w_DESCON10
      .w_CODAGE10=oParentObject.w_CODAGE10
      .w_DESAGE10=oParentObject.w_DESAGE10
      .w_CODROL10=oParentObject.w_CODROL10
      .w_READ10=oParentObject.w_READ10
      .w_WRITE10=oParentObject.w_WRITE10
      .w_DELETE10=oParentObject.w_DELETE10
      .w_CODAGE9=oParentObject.w_CODAGE9
        .w_OTIPGRU = 'O'
        .w_MASKREJECTED = .F.
        .w_OBTEST = IIF(VarType(g_ZCPOBTEST)='D', g_ZCPOBTEST, i_DATSYS)
          .DoRTCalc(4,4,.f.)
        .w_RISERVATO = "S"
          .DoRTCalc(6,6,.f.)
        .w_TIPCON1 = IIF(.w_TIPDES1$'CF' and .w_riservato='S',.w_TIPDES1, ' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODGRU1))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODAGE1))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODCON1))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODROL1))
          .link_1_11('Full')
        endif
        .w_READ1 = "S"
        .DoRTCalc(13,18,.f.)
        if not(empty(.w_ZCPCFOLDER))
          .link_1_18('Full')
        endif
          .DoRTCalc(19,26,.f.)
        .w_TIPCON2 = IIF(.w_TIPDES2$'CF',.w_TIPDES2, ' ')
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CODGRU2))
          .link_2_8('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_CODCON2))
          .link_2_10('Full')
        endif
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_CODAGE2))
          .link_2_12('Full')
        endif
        .DoRTCalc(33,34,.f.)
        if not(empty(.w_CODROL2))
          .link_2_14('Full')
        endif
        .w_READ2 = "S"
          .DoRTCalc(36,38,.f.)
        .w_TIPCON3 = IIF(.w_TIPDES3$'CF',.w_TIPDES3, ' ')
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CODGRU3))
          .link_2_20('Full')
        endif
        .DoRTCalc(41,42,.f.)
        if not(empty(.w_CODCON3))
          .link_2_22('Full')
        endif
        .DoRTCalc(43,44,.f.)
        if not(empty(.w_CODAGE3))
          .link_2_24('Full')
        endif
        .DoRTCalc(45,46,.f.)
        if not(empty(.w_CODROL3))
          .link_2_26('Full')
        endif
        .w_READ3 = "S"
        .DoRTCalc(48,51,.f.)
        if not(empty(.w_CODGRU4))
          .link_2_31('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_TIPCON4 = IIF(.w_TIPDES4$'CF',.w_TIPDES4, ' ')
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODCON4))
          .link_2_34('Full')
        endif
        .DoRTCalc(55,56,.f.)
        if not(empty(.w_CODAGE4))
          .link_2_36('Full')
        endif
        .DoRTCalc(57,58,.f.)
        if not(empty(.w_CODROL4))
          .link_2_38('Full')
        endif
        .w_READ4 = "S"
        .DoRTCalc(60,63,.f.)
        if not(empty(.w_CODGRU5))
          .link_2_43('Full')
        endif
          .DoRTCalc(64,64,.f.)
        .w_TIPCON5 = IIF(.w_TIPDES5$'CF',.w_TIPDES5, ' ')
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_CODCON5))
          .link_2_46('Full')
        endif
        .DoRTCalc(67,68,.f.)
        if not(empty(.w_CODAGE5))
          .link_2_48('Full')
        endif
        .DoRTCalc(69,70,.f.)
        if not(empty(.w_CODROL5))
          .link_2_50('Full')
        endif
        .w_READ5 = "S"
        .DoRTCalc(72,75,.f.)
        if not(empty(.w_CODGRU6))
          .link_2_55('Full')
        endif
          .DoRTCalc(76,76,.f.)
        .w_TIPCON6 = IIF(.w_TIPDES6$'CF',.w_TIPDES6, ' ')
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_CODCON6))
          .link_2_58('Full')
        endif
        .DoRTCalc(79,80,.f.)
        if not(empty(.w_CODAGE6))
          .link_2_60('Full')
        endif
        .DoRTCalc(81,82,.f.)
        if not(empty(.w_CODROL6))
          .link_2_62('Full')
        endif
        .w_READ6 = "S"
        .DoRTCalc(84,87,.f.)
        if not(empty(.w_CODGRU7))
          .link_2_67('Full')
        endif
          .DoRTCalc(88,88,.f.)
        .w_TIPCON7 = IIF(.w_TIPDES7$'CF',.w_TIPDES7, ' ')
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_CODCON7))
          .link_2_70('Full')
        endif
        .DoRTCalc(91,92,.f.)
        if not(empty(.w_CODAGE7))
          .link_2_72('Full')
        endif
        .DoRTCalc(93,94,.f.)
        if not(empty(.w_CODROL7))
          .link_2_74('Full')
        endif
        .w_READ7 = "S"
        .DoRTCalc(96,99,.f.)
        if not(empty(.w_CODGRU8))
          .link_2_79('Full')
        endif
          .DoRTCalc(100,100,.f.)
        .w_TIPCON8 = IIF(.w_TIPDES8$'CF',.w_TIPDES8, ' ')
        .DoRTCalc(102,102,.f.)
        if not(empty(.w_CODCON8))
          .link_2_82('Full')
        endif
        .DoRTCalc(103,104,.f.)
        if not(empty(.w_CODAGE8))
          .link_2_84('Full')
        endif
        .DoRTCalc(105,106,.f.)
        if not(empty(.w_CODROL8))
          .link_2_86('Full')
        endif
        .w_READ8 = "S"
        .DoRTCalc(108,111,.f.)
        if not(empty(.w_CODGRU9))
          .link_2_91('Full')
        endif
          .DoRTCalc(112,112,.f.)
        .w_TIPCON9 = IIF(.w_TIPDES9$'CF',.w_TIPDES9, ' ')
        .DoRTCalc(114,114,.f.)
        if not(empty(.w_CODCON9))
          .link_2_94('Full')
        endif
        .DoRTCalc(115,117,.f.)
        if not(empty(.w_CODROL9))
          .link_2_97('Full')
        endif
        .w_READ9 = "S"
        .DoRTCalc(119,122,.f.)
        if not(empty(.w_CODGRU10))
          .link_2_102('Full')
        endif
          .DoRTCalc(123,123,.f.)
        .w_TIPCON10 = IIF(.w_TIPDES10$'CF',.w_TIPDES10, ' ')
        .DoRTCalc(125,125,.f.)
        if not(empty(.w_CODCON10))
          .link_2_105('Full')
        endif
        .DoRTCalc(126,127,.f.)
        if not(empty(.w_CODAGE10))
          .link_2_107('Full')
        endif
        .DoRTCalc(128,129,.f.)
        if not(empty(.w_CODROL10))
          .link_2_109('Full')
        endif
        .w_READ10 = "S"
        .DoRTCalc(131,143,.f.)
        if not(empty(.w_CODAGE9))
          .link_2_125('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_113.enabled = this.oPgFrm.Page2.oPag.oBtn_2_113.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_114.enabled = this.oPgFrm.Page2.oPag.oBtn_2_114.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MASKREJECTED=.w_MASKREJECTED
      .oParentObject.w_RISERVATO=.w_RISERVATO
      .oParentObject.w_TIPDES1=.w_TIPDES1
      .oParentObject.w_TIPCON1=.w_TIPCON1
      .oParentObject.w_CODGRU1=.w_CODGRU1
      .oParentObject.w_CODAGE1=.w_CODAGE1
      .oParentObject.w_CODCON1=.w_CODCON1
      .oParentObject.w_CODROL1=.w_CODROL1
      .oParentObject.w_READ1=.w_READ1
      .oParentObject.w_WRITE1=.w_WRITE1
      .oParentObject.w_DELETE1=.w_DELETE1
      .oParentObject.w_ZCPFTITLE=.w_ZCPFTITLE
      .oParentObject.w_FOLDCOMP=.w_FOLDCOMP
      .oParentObject.w_FOLDCOMU=.w_FOLDCOMU
      .oParentObject.w_ZCPCFOLDER=.w_ZCPCFOLDER
      .oParentObject.w_ZCPTFOLDER=.w_ZCPTFOLDER
      .oParentObject.w_ZCPDFOLDER=.w_ZCPDFOLDER
      .oParentObject.w_ZCPPFOLDER=.w_ZCPPFOLDER
      .oParentObject.w_ZCPFDESCRI=.w_ZCPFDESCRI
      .oParentObject.w_DESGRU1=.w_DESGRU1
      .oParentObject.w_DESCON1=.w_DESCON1
      .oParentObject.w_DESAGE1=.w_DESAGE1
      .oParentObject.w_TIPDES2=.w_TIPDES2
      .oParentObject.w_TIPCON2=.w_TIPCON2
      .oParentObject.w_CODGRU2=.w_CODGRU2
      .oParentObject.w_DESGRU2=.w_DESGRU2
      .oParentObject.w_CODCON2=.w_CODCON2
      .oParentObject.w_DESCON2=.w_DESCON2
      .oParentObject.w_CODAGE2=.w_CODAGE2
      .oParentObject.w_DESAGE2=.w_DESAGE2
      .oParentObject.w_CODROL2=.w_CODROL2
      .oParentObject.w_READ2=.w_READ2
      .oParentObject.w_WRITE2=.w_WRITE2
      .oParentObject.w_DELETE2=.w_DELETE2
      .oParentObject.w_TIPDES3=.w_TIPDES3
      .oParentObject.w_TIPCON3=.w_TIPCON3
      .oParentObject.w_CODGRU3=.w_CODGRU3
      .oParentObject.w_DESGRU3=.w_DESGRU3
      .oParentObject.w_CODCON3=.w_CODCON3
      .oParentObject.w_DESCON3=.w_DESCON3
      .oParentObject.w_CODAGE3=.w_CODAGE3
      .oParentObject.w_DESAGE3=.w_DESAGE3
      .oParentObject.w_CODROL3=.w_CODROL3
      .oParentObject.w_READ3=.w_READ3
      .oParentObject.w_WRITE3=.w_WRITE3
      .oParentObject.w_DELETE3=.w_DELETE3
      .oParentObject.w_TIPDES4=.w_TIPDES4
      .oParentObject.w_CODGRU4=.w_CODGRU4
      .oParentObject.w_DESGRU4=.w_DESGRU4
      .oParentObject.w_TIPCON4=.w_TIPCON4
      .oParentObject.w_CODCON4=.w_CODCON4
      .oParentObject.w_DESCON4=.w_DESCON4
      .oParentObject.w_CODAGE4=.w_CODAGE4
      .oParentObject.w_DESAGE4=.w_DESAGE4
      .oParentObject.w_CODROL4=.w_CODROL4
      .oParentObject.w_READ4=.w_READ4
      .oParentObject.w_WRITE4=.w_WRITE4
      .oParentObject.w_DELETE4=.w_DELETE4
      .oParentObject.w_TIPDES5=.w_TIPDES5
      .oParentObject.w_CODGRU5=.w_CODGRU5
      .oParentObject.w_DESGRU5=.w_DESGRU5
      .oParentObject.w_TIPCON5=.w_TIPCON5
      .oParentObject.w_CODCON5=.w_CODCON5
      .oParentObject.w_DESCON5=.w_DESCON5
      .oParentObject.w_CODAGE5=.w_CODAGE5
      .oParentObject.w_DESAGE5=.w_DESAGE5
      .oParentObject.w_CODROL5=.w_CODROL5
      .oParentObject.w_READ5=.w_READ5
      .oParentObject.w_WRITE5=.w_WRITE5
      .oParentObject.w_DELETE5=.w_DELETE5
      .oParentObject.w_TIPDES6=.w_TIPDES6
      .oParentObject.w_CODGRU6=.w_CODGRU6
      .oParentObject.w_DESGRU6=.w_DESGRU6
      .oParentObject.w_TIPCON6=.w_TIPCON6
      .oParentObject.w_CODCON6=.w_CODCON6
      .oParentObject.w_DESCON6=.w_DESCON6
      .oParentObject.w_CODAGE6=.w_CODAGE6
      .oParentObject.w_DESAGE6=.w_DESAGE6
      .oParentObject.w_CODROL6=.w_CODROL6
      .oParentObject.w_READ6=.w_READ6
      .oParentObject.w_WRITE6=.w_WRITE6
      .oParentObject.w_DELETE6=.w_DELETE6
      .oParentObject.w_TIPDES7=.w_TIPDES7
      .oParentObject.w_CODGRU7=.w_CODGRU7
      .oParentObject.w_DESGRU7=.w_DESGRU7
      .oParentObject.w_TIPCON7=.w_TIPCON7
      .oParentObject.w_CODCON7=.w_CODCON7
      .oParentObject.w_DESCON7=.w_DESCON7
      .oParentObject.w_CODAGE7=.w_CODAGE7
      .oParentObject.w_DESAGE7=.w_DESAGE7
      .oParentObject.w_CODROL7=.w_CODROL7
      .oParentObject.w_READ7=.w_READ7
      .oParentObject.w_WRITE7=.w_WRITE7
      .oParentObject.w_DELETE7=.w_DELETE7
      .oParentObject.w_TIPDES8=.w_TIPDES8
      .oParentObject.w_CODGRU8=.w_CODGRU8
      .oParentObject.w_DESGRU8=.w_DESGRU8
      .oParentObject.w_TIPCON8=.w_TIPCON8
      .oParentObject.w_CODCON8=.w_CODCON8
      .oParentObject.w_DESCON8=.w_DESCON8
      .oParentObject.w_CODAGE8=.w_CODAGE8
      .oParentObject.w_DESAGE8=.w_DESAGE8
      .oParentObject.w_CODROL8=.w_CODROL8
      .oParentObject.w_READ8=.w_READ8
      .oParentObject.w_WRITE8=.w_WRITE8
      .oParentObject.w_DELETE8=.w_DELETE8
      .oParentObject.w_TIPDES9=.w_TIPDES9
      .oParentObject.w_CODGRU9=.w_CODGRU9
      .oParentObject.w_DESGRU9=.w_DESGRU9
      .oParentObject.w_TIPCON9=.w_TIPCON9
      .oParentObject.w_CODCON9=.w_CODCON9
      .oParentObject.w_DESCON9=.w_DESCON9
      .oParentObject.w_DESAGE9=.w_DESAGE9
      .oParentObject.w_CODROL9=.w_CODROL9
      .oParentObject.w_READ9=.w_READ9
      .oParentObject.w_WRITE9=.w_WRITE9
      .oParentObject.w_DELETE9=.w_DELETE9
      .oParentObject.w_TIPDES10=.w_TIPDES10
      .oParentObject.w_CODGRU10=.w_CODGRU10
      .oParentObject.w_DESGRU10=.w_DESGRU10
      .oParentObject.w_TIPCON10=.w_TIPCON10
      .oParentObject.w_CODCON10=.w_CODCON10
      .oParentObject.w_DESCON10=.w_DESCON10
      .oParentObject.w_CODAGE10=.w_CODAGE10
      .oParentObject.w_DESAGE10=.w_DESAGE10
      .oParentObject.w_CODROL10=.w_CODROL10
      .oParentObject.w_READ10=.w_READ10
      .oParentObject.w_WRITE10=.w_WRITE10
      .oParentObject.w_DELETE10=.w_DELETE10
      .oParentObject.w_CODAGE9=.w_CODAGE9
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_MASKREJECTED = .F.
        .DoRTCalc(3,5,.t.)
        if .o_CODAGE1<>.w_CODAGE1.or. .o_CODCON1<>.w_CODCON1
            .w_TIPDES1 = iif(empty(.w_CODCON1)  AND empty(.w_CODAGE1),'G',.w_TIPDES1)
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES1<>.w_TIPDES1
            .w_TIPCON1 = IIF(.w_TIPDES1$'CF' and .w_riservato='S',.w_TIPDES1, ' ')
        endif
        if .o_TIPDES1<>.w_TIPDES1.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU1 = IIF(.w_TIPDES1='C',20, IIF(.w_TIPDES1='A',30, IIF(.w_TIPDES1='F',40, 0)))
          .link_1_8('Full')
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES1<>.w_TIPDES1
            .w_CODAGE1 = iif(.w_TIPDES1='A' and .w_RISERVATO='S',.w_CODAGE1,space(5))
          .link_1_9('Full')
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES1<>.w_TIPDES1
            .w_CODCON1 = iif(.w_TIPDES1 $ 'CF',.w_CODCON1,space(15))
          .link_1_10('Full')
        endif
          .link_1_11('Full')
        .DoRTCalc(12,12,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES1<>.w_TIPDES1
            .w_WRITE1 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES1<>.w_TIPDES1
            .w_DELETE1 = 'N'
        endif
        .DoRTCalc(15,17,.t.)
        if .o_TIPDES1<>.w_TIPDES1.or. .o_RISERVATO<>.w_RISERVATO
            .w_ZCPCFOLDER = IIF(.w_RISERVATO<>'S',0, IIF(.w_TIPDES1='G', .w_FOLDCOMU, .w_FOLDCOMP))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,25,.t.)
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES2 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
            .w_TIPCON2 = IIF(.w_TIPDES2$'CF',.w_TIPDES2, ' ')
        if .o_TIPDES2<>.w_TIPDES2.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU2 = IIF(.w_TIPDES2='C',20, IIF(.w_TIPDES2='A',30, IIF(.w_TIPDES2='F',40, IIF(.w_TIPDES2='X', 0, 0))))
          .link_2_8('Full')
        endif
        .DoRTCalc(29,29,.t.)
        if .o_TIPDES2<>.w_TIPDES2.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON2 = space(15)
          .link_2_10('Full')
        endif
        .DoRTCalc(31,31,.t.)
        if .o_TIPDES2<>.w_TIPDES2.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE2 = space(5)
          .link_2_12('Full')
        endif
        .DoRTCalc(33,33,.t.)
          .link_2_14('Full')
        .DoRTCalc(35,35,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES2<>.w_TIPDES2
            .w_WRITE2 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES2<>.w_TIPDES2
            .w_DELETE2 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES3 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
            .w_TIPCON3 = IIF(.w_TIPDES3$'CF',.w_TIPDES3, ' ')
        if .o_TIPDES3<>.w_TIPDES3.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU3 = IIF(.w_TIPDES3='C',20, IIF(.w_TIPDES3='A',30, IIF(.w_TIPDES3='F',40, IIF(.w_TIPDES3='X', 0, 0))))
          .link_2_20('Full')
        endif
        .DoRTCalc(41,41,.t.)
        if .o_TIPDES3<>.w_TIPDES3.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON3 = space(15)
          .link_2_22('Full')
        endif
        .DoRTCalc(43,43,.t.)
        if .o_TIPDES3<>.w_TIPDES3.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE3 = space(5)
          .link_2_24('Full')
        endif
        .DoRTCalc(45,45,.t.)
          .link_2_26('Full')
        .DoRTCalc(47,47,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES3<>.w_TIPDES3
            .w_WRITE3 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES3<>.w_TIPDES3
            .w_DELETE3 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES4 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES4<>.w_TIPDES4.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU4 = IIF(.w_TIPDES4='C',20, IIF(.w_TIPDES4='A',30, IIF(.w_TIPDES4='F',40, IIF(.w_TIPDES4='X', 0, 0))))
          .link_2_31('Full')
        endif
        .DoRTCalc(52,52,.t.)
            .w_TIPCON4 = IIF(.w_TIPDES4$'CF',.w_TIPDES4, ' ')
        if .o_TIPDES4<>.w_TIPDES4.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON4 = space(15)
          .link_2_34('Full')
        endif
        .DoRTCalc(55,55,.t.)
        if .o_TIPDES4<>.w_TIPDES4.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE4 = space(5)
          .link_2_36('Full')
        endif
        .DoRTCalc(57,57,.t.)
          .link_2_38('Full')
        .DoRTCalc(59,59,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES4<>.w_TIPDES4
            .w_WRITE4 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES4<>.w_TIPDES4
            .w_DELETE4 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES5 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES5<>.w_TIPDES5.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU5 = IIF(.w_TIPDES5='C',20, IIF(.w_TIPDES5='A',30, IIF(.w_TIPDES5='F',40, IIF(.w_TIPDES5='X', 0, 0))))
          .link_2_43('Full')
        endif
        .DoRTCalc(64,64,.t.)
            .w_TIPCON5 = IIF(.w_TIPDES5$'CF',.w_TIPDES5, ' ')
        if .o_TIPDES5<>.w_TIPDES5.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON5 = space(15)
          .link_2_46('Full')
        endif
        .DoRTCalc(67,67,.t.)
        if .o_TIPDES5<>.w_TIPDES5.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE5 = space(5)
          .link_2_48('Full')
        endif
        .DoRTCalc(69,69,.t.)
          .link_2_50('Full')
        .DoRTCalc(71,71,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES5<>.w_TIPDES5
            .w_WRITE5 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES5<>.w_TIPDES5
            .w_DELETE5 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES6 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES6<>.w_TIPDES6.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU6 = IIF(.w_TIPDES6='C',20, IIF(.w_TIPDES6='A',30, IIF(.w_TIPDES6='F',40, IIF(.w_TIPDES6='X', 0, 0))))
          .link_2_55('Full')
        endif
        .DoRTCalc(76,76,.t.)
            .w_TIPCON6 = IIF(.w_TIPDES6$'CF',.w_TIPDES6, ' ')
        if .o_TIPDES6<>.w_TIPDES6.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON6 = space(15)
          .link_2_58('Full')
        endif
        .DoRTCalc(79,79,.t.)
        if .o_TIPDES6<>.w_TIPDES6.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE6 = space(5)
          .link_2_60('Full')
        endif
        .DoRTCalc(81,81,.t.)
          .link_2_62('Full')
        .DoRTCalc(83,83,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES6<>.w_TIPDES6
            .w_WRITE6 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES6<>.w_TIPDES6
            .w_DELETE6 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES7 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES7<>.w_TIPDES7.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU7 = IIF(.w_TIPDES7='C',20, IIF(.w_TIPDES7='A',30, IIF(.w_TIPDES7='F',40, IIF(.w_TIPDES7='X', 0, 0))))
          .link_2_67('Full')
        endif
        .DoRTCalc(88,88,.t.)
            .w_TIPCON7 = IIF(.w_TIPDES7$'CF',.w_TIPDES7, ' ')
        if .o_TIPDES7<>.w_TIPDES7.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON7 = space(15)
          .link_2_70('Full')
        endif
        .DoRTCalc(91,91,.t.)
        if .o_TIPDES7<>.w_TIPDES7.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE7 = space(5)
          .link_2_72('Full')
        endif
        .DoRTCalc(93,93,.t.)
          .link_2_74('Full')
        .DoRTCalc(95,95,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES7<>.w_TIPDES7
            .w_WRITE7 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES7<>.w_TIPDES7
            .w_DELETE7 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES8 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES8<>.w_TIPDES8.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU8 = IIF(.w_TIPDES8='C',20, IIF(.w_TIPDES8='A',30, IIF(.w_TIPDES8='F',40, IIF(.w_TIPDES8='X', 0, 0))))
          .link_2_79('Full')
        endif
        .DoRTCalc(100,100,.t.)
            .w_TIPCON8 = IIF(.w_TIPDES8$'CF',.w_TIPDES8, ' ')
        if .o_TIPDES8<>.w_TIPDES8.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON8 = space(15)
          .link_2_82('Full')
        endif
        .DoRTCalc(103,103,.t.)
        if .o_TIPDES8<>.w_TIPDES8.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE8 = space(5)
          .link_2_84('Full')
        endif
        .DoRTCalc(105,105,.t.)
          .link_2_86('Full')
        .DoRTCalc(107,107,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES8<>.w_TIPDES8
            .w_WRITE8 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES8<>.w_TIPDES8
            .w_DELETE8 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES9 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES9<>.w_TIPDES9.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU9 = IIF(.w_TIPDES9='C',20, IIF(.w_TIPDES9='A',30, IIF(.w_TIPDES9='F',40, IIF(.w_TIPDES9='X', 0, 0))))
          .link_2_91('Full')
        endif
        .DoRTCalc(112,112,.t.)
            .w_TIPCON9 = IIF(.w_TIPDES9$'CF',.w_TIPDES9, ' ')
        if .o_TIPDES9<>.w_TIPDES9.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON9 = space(15)
          .link_2_94('Full')
        endif
        .DoRTCalc(115,116,.t.)
          .link_2_97('Full')
        .DoRTCalc(118,118,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES9<>.w_TIPDES9
            .w_WRITE9 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES9<>.w_TIPDES9
            .w_DELETE9 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO
            .w_TIPDES10 = IIF (.w_RISERVATO='N' ,'X' ,'')
        endif
        if .o_TIPDES10<>.w_TIPDES10.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODGRU10 = IIF(.w_TIPDES10='C',20, IIF(.w_TIPDES10='A',30, IIF(.w_TIPDES10='F',40, IIF(.w_TIPDES10='X', 0, 0))))
          .link_2_102('Full')
        endif
        .DoRTCalc(123,123,.t.)
            .w_TIPCON10 = IIF(.w_TIPDES10$'CF',.w_TIPDES10, ' ')
        if .o_TIPDES10<>.w_TIPDES10.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODCON10 = space(15)
          .link_2_105('Full')
        endif
        .DoRTCalc(126,126,.t.)
        if .o_TIPDES10<>.w_TIPDES10.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE10 = space(5)
          .link_2_107('Full')
        endif
        .DoRTCalc(128,128,.t.)
          .link_2_109('Full')
        .DoRTCalc(130,130,.t.)
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES10<>.w_TIPDES10
            .w_WRITE10 = 'N'
        endif
        if .o_RISERVATO<>.w_RISERVATO.or. .o_TIPDES10<>.w_TIPDES10
            .w_DELETE10 = 'N'
        endif
        .DoRTCalc(133,142,.t.)
        if .o_TIPDES9<>.w_TIPDES9.or. .o_RISERVATO<>.w_RISERVATO
            .w_CODAGE9 = space(5)
          .link_2_125('Full')
        endif
        if .o_ZCPCFOLDER<>.w_ZCPCFOLDER.or. .o_ZCPPFOLDER<>.w_ZCPPFOLDER
          .Calculate_UDGNBYYVTK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UDGNBYYVTK()
    with this
          * --- Verifica tipo folder selezionato
          gsut2bxp(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPDES1_1_6.enabled = this.oPgFrm.Page1.oPag.oTIPDES1_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODGRU1_1_8.enabled = this.oPgFrm.Page1.oPag.oCODGRU1_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODAGE1_1_9.enabled = this.oPgFrm.Page1.oPag.oCODAGE1_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCODCON1_1_10.enabled = this.oPgFrm.Page1.oPag.oCODCON1_1_10.mCond()
    this.oPgFrm.Page1.oPag.oREAD1_1_12.enabled = this.oPgFrm.Page1.oPag.oREAD1_1_12.mCond()
    this.oPgFrm.Page1.oPag.oWRITE1_1_13.enabled = this.oPgFrm.Page1.oPag.oWRITE1_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDELETE1_1_14.enabled = this.oPgFrm.Page1.oPag.oDELETE1_1_14.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES2_2_6.enabled = this.oPgFrm.Page2.oPag.oTIPDES2_2_6.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU2_2_8.enabled = this.oPgFrm.Page2.oPag.oCODGRU2_2_8.mCond()
    this.oPgFrm.Page2.oPag.oCODCON2_2_10.enabled = this.oPgFrm.Page2.oPag.oCODCON2_2_10.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE2_2_12.enabled = this.oPgFrm.Page2.oPag.oCODAGE2_2_12.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES3_2_18.enabled = this.oPgFrm.Page2.oPag.oTIPDES3_2_18.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU3_2_20.enabled = this.oPgFrm.Page2.oPag.oCODGRU3_2_20.mCond()
    this.oPgFrm.Page2.oPag.oCODCON3_2_22.enabled = this.oPgFrm.Page2.oPag.oCODCON3_2_22.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE3_2_24.enabled = this.oPgFrm.Page2.oPag.oCODAGE3_2_24.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES4_2_30.enabled = this.oPgFrm.Page2.oPag.oTIPDES4_2_30.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU4_2_31.enabled = this.oPgFrm.Page2.oPag.oCODGRU4_2_31.mCond()
    this.oPgFrm.Page2.oPag.oCODCON4_2_34.enabled = this.oPgFrm.Page2.oPag.oCODCON4_2_34.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE4_2_36.enabled = this.oPgFrm.Page2.oPag.oCODAGE4_2_36.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES5_2_42.enabled = this.oPgFrm.Page2.oPag.oTIPDES5_2_42.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU5_2_43.enabled = this.oPgFrm.Page2.oPag.oCODGRU5_2_43.mCond()
    this.oPgFrm.Page2.oPag.oCODCON5_2_46.enabled = this.oPgFrm.Page2.oPag.oCODCON5_2_46.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE5_2_48.enabled = this.oPgFrm.Page2.oPag.oCODAGE5_2_48.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES6_2_54.enabled = this.oPgFrm.Page2.oPag.oTIPDES6_2_54.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU6_2_55.enabled = this.oPgFrm.Page2.oPag.oCODGRU6_2_55.mCond()
    this.oPgFrm.Page2.oPag.oCODCON6_2_58.enabled = this.oPgFrm.Page2.oPag.oCODCON6_2_58.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE6_2_60.enabled = this.oPgFrm.Page2.oPag.oCODAGE6_2_60.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES7_2_66.enabled = this.oPgFrm.Page2.oPag.oTIPDES7_2_66.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU7_2_67.enabled = this.oPgFrm.Page2.oPag.oCODGRU7_2_67.mCond()
    this.oPgFrm.Page2.oPag.oCODCON7_2_70.enabled = this.oPgFrm.Page2.oPag.oCODCON7_2_70.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE7_2_72.enabled = this.oPgFrm.Page2.oPag.oCODAGE7_2_72.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES8_2_78.enabled = this.oPgFrm.Page2.oPag.oTIPDES8_2_78.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU8_2_79.enabled = this.oPgFrm.Page2.oPag.oCODGRU8_2_79.mCond()
    this.oPgFrm.Page2.oPag.oCODCON8_2_82.enabled = this.oPgFrm.Page2.oPag.oCODCON8_2_82.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE8_2_84.enabled = this.oPgFrm.Page2.oPag.oCODAGE8_2_84.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES9_2_90.enabled = this.oPgFrm.Page2.oPag.oTIPDES9_2_90.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU9_2_91.enabled = this.oPgFrm.Page2.oPag.oCODGRU9_2_91.mCond()
    this.oPgFrm.Page2.oPag.oCODCON9_2_94.enabled = this.oPgFrm.Page2.oPag.oCODCON9_2_94.mCond()
    this.oPgFrm.Page2.oPag.oTIPDES10_2_101.enabled = this.oPgFrm.Page2.oPag.oTIPDES10_2_101.mCond()
    this.oPgFrm.Page2.oPag.oCODGRU10_2_102.enabled = this.oPgFrm.Page2.oPag.oCODGRU10_2_102.mCond()
    this.oPgFrm.Page2.oPag.oCODCON10_2_105.enabled = this.oPgFrm.Page2.oPag.oCODCON10_2_105.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE10_2_107.enabled = this.oPgFrm.Page2.oPag.oCODAGE10_2_107.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE9_2_125.enabled = this.oPgFrm.Page2.oPag.oCODAGE9_2_125.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_113.enabled = this.oPgFrm.Page2.oPag.oBtn_2_113.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODGRU1_1_8.visible=!this.oPgFrm.Page1.oPag.oCODGRU1_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODAGE1_1_9.visible=!this.oPgFrm.Page1.oPag.oCODAGE1_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCODCON1_1_10.visible=!this.oPgFrm.Page1.oPag.oCODCON1_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESGRU1_1_23.visible=!this.oPgFrm.Page1.oPag.oDESGRU1_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESCON1_1_24.visible=!this.oPgFrm.Page1.oPag.oDESCON1_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE1_1_25.visible=!this.oPgFrm.Page1.oPag.oDESAGE1_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU2_2_8.visible=!this.oPgFrm.Page2.oPag.oCODGRU2_2_8.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU2_2_9.visible=!this.oPgFrm.Page2.oPag.oDESGRU2_2_9.mHide()
    this.oPgFrm.Page2.oPag.oCODCON2_2_10.visible=!this.oPgFrm.Page2.oPag.oCODCON2_2_10.mHide()
    this.oPgFrm.Page2.oPag.oDESCON2_2_11.visible=!this.oPgFrm.Page2.oPag.oDESCON2_2_11.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE2_2_12.visible=!this.oPgFrm.Page2.oPag.oCODAGE2_2_12.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE2_2_13.visible=!this.oPgFrm.Page2.oPag.oDESAGE2_2_13.mHide()
    this.oPgFrm.Page2.oPag.oREAD2_2_15.visible=!this.oPgFrm.Page2.oPag.oREAD2_2_15.mHide()
    this.oPgFrm.Page2.oPag.oWRITE2_2_16.visible=!this.oPgFrm.Page2.oPag.oWRITE2_2_16.mHide()
    this.oPgFrm.Page2.oPag.oDELETE2_2_17.visible=!this.oPgFrm.Page2.oPag.oDELETE2_2_17.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU3_2_20.visible=!this.oPgFrm.Page2.oPag.oCODGRU3_2_20.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU3_2_21.visible=!this.oPgFrm.Page2.oPag.oDESGRU3_2_21.mHide()
    this.oPgFrm.Page2.oPag.oCODCON3_2_22.visible=!this.oPgFrm.Page2.oPag.oCODCON3_2_22.mHide()
    this.oPgFrm.Page2.oPag.oDESCON3_2_23.visible=!this.oPgFrm.Page2.oPag.oDESCON3_2_23.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE3_2_24.visible=!this.oPgFrm.Page2.oPag.oCODAGE3_2_24.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE3_2_25.visible=!this.oPgFrm.Page2.oPag.oDESAGE3_2_25.mHide()
    this.oPgFrm.Page2.oPag.oREAD3_2_27.visible=!this.oPgFrm.Page2.oPag.oREAD3_2_27.mHide()
    this.oPgFrm.Page2.oPag.oWRITE3_2_28.visible=!this.oPgFrm.Page2.oPag.oWRITE3_2_28.mHide()
    this.oPgFrm.Page2.oPag.oDELETE3_2_29.visible=!this.oPgFrm.Page2.oPag.oDELETE3_2_29.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU4_2_31.visible=!this.oPgFrm.Page2.oPag.oCODGRU4_2_31.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU4_2_32.visible=!this.oPgFrm.Page2.oPag.oDESGRU4_2_32.mHide()
    this.oPgFrm.Page2.oPag.oCODCON4_2_34.visible=!this.oPgFrm.Page2.oPag.oCODCON4_2_34.mHide()
    this.oPgFrm.Page2.oPag.oDESCON4_2_35.visible=!this.oPgFrm.Page2.oPag.oDESCON4_2_35.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE4_2_36.visible=!this.oPgFrm.Page2.oPag.oCODAGE4_2_36.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE4_2_37.visible=!this.oPgFrm.Page2.oPag.oDESAGE4_2_37.mHide()
    this.oPgFrm.Page2.oPag.oREAD4_2_39.visible=!this.oPgFrm.Page2.oPag.oREAD4_2_39.mHide()
    this.oPgFrm.Page2.oPag.oWRITE4_2_40.visible=!this.oPgFrm.Page2.oPag.oWRITE4_2_40.mHide()
    this.oPgFrm.Page2.oPag.oDELETE4_2_41.visible=!this.oPgFrm.Page2.oPag.oDELETE4_2_41.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU5_2_43.visible=!this.oPgFrm.Page2.oPag.oCODGRU5_2_43.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU5_2_44.visible=!this.oPgFrm.Page2.oPag.oDESGRU5_2_44.mHide()
    this.oPgFrm.Page2.oPag.oCODCON5_2_46.visible=!this.oPgFrm.Page2.oPag.oCODCON5_2_46.mHide()
    this.oPgFrm.Page2.oPag.oDESCON5_2_47.visible=!this.oPgFrm.Page2.oPag.oDESCON5_2_47.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE5_2_48.visible=!this.oPgFrm.Page2.oPag.oCODAGE5_2_48.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE5_2_49.visible=!this.oPgFrm.Page2.oPag.oDESAGE5_2_49.mHide()
    this.oPgFrm.Page2.oPag.oREAD5_2_51.visible=!this.oPgFrm.Page2.oPag.oREAD5_2_51.mHide()
    this.oPgFrm.Page2.oPag.oWRITE5_2_52.visible=!this.oPgFrm.Page2.oPag.oWRITE5_2_52.mHide()
    this.oPgFrm.Page2.oPag.oDELETE5_2_53.visible=!this.oPgFrm.Page2.oPag.oDELETE5_2_53.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU6_2_55.visible=!this.oPgFrm.Page2.oPag.oCODGRU6_2_55.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU6_2_56.visible=!this.oPgFrm.Page2.oPag.oDESGRU6_2_56.mHide()
    this.oPgFrm.Page2.oPag.oCODCON6_2_58.visible=!this.oPgFrm.Page2.oPag.oCODCON6_2_58.mHide()
    this.oPgFrm.Page2.oPag.oDESCON6_2_59.visible=!this.oPgFrm.Page2.oPag.oDESCON6_2_59.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE6_2_60.visible=!this.oPgFrm.Page2.oPag.oCODAGE6_2_60.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE6_2_61.visible=!this.oPgFrm.Page2.oPag.oDESAGE6_2_61.mHide()
    this.oPgFrm.Page2.oPag.oREAD6_2_63.visible=!this.oPgFrm.Page2.oPag.oREAD6_2_63.mHide()
    this.oPgFrm.Page2.oPag.oWRITE6_2_64.visible=!this.oPgFrm.Page2.oPag.oWRITE6_2_64.mHide()
    this.oPgFrm.Page2.oPag.oDELETE6_2_65.visible=!this.oPgFrm.Page2.oPag.oDELETE6_2_65.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU7_2_67.visible=!this.oPgFrm.Page2.oPag.oCODGRU7_2_67.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU7_2_68.visible=!this.oPgFrm.Page2.oPag.oDESGRU7_2_68.mHide()
    this.oPgFrm.Page2.oPag.oCODCON7_2_70.visible=!this.oPgFrm.Page2.oPag.oCODCON7_2_70.mHide()
    this.oPgFrm.Page2.oPag.oDESCON7_2_71.visible=!this.oPgFrm.Page2.oPag.oDESCON7_2_71.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE7_2_72.visible=!this.oPgFrm.Page2.oPag.oCODAGE7_2_72.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE7_2_73.visible=!this.oPgFrm.Page2.oPag.oDESAGE7_2_73.mHide()
    this.oPgFrm.Page2.oPag.oREAD7_2_75.visible=!this.oPgFrm.Page2.oPag.oREAD7_2_75.mHide()
    this.oPgFrm.Page2.oPag.oWRITE7_2_76.visible=!this.oPgFrm.Page2.oPag.oWRITE7_2_76.mHide()
    this.oPgFrm.Page2.oPag.oDELETE7_2_77.visible=!this.oPgFrm.Page2.oPag.oDELETE7_2_77.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU8_2_79.visible=!this.oPgFrm.Page2.oPag.oCODGRU8_2_79.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU8_2_80.visible=!this.oPgFrm.Page2.oPag.oDESGRU8_2_80.mHide()
    this.oPgFrm.Page2.oPag.oCODCON8_2_82.visible=!this.oPgFrm.Page2.oPag.oCODCON8_2_82.mHide()
    this.oPgFrm.Page2.oPag.oDESCON8_2_83.visible=!this.oPgFrm.Page2.oPag.oDESCON8_2_83.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE8_2_84.visible=!this.oPgFrm.Page2.oPag.oCODAGE8_2_84.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE8_2_85.visible=!this.oPgFrm.Page2.oPag.oDESAGE8_2_85.mHide()
    this.oPgFrm.Page2.oPag.oREAD8_2_87.visible=!this.oPgFrm.Page2.oPag.oREAD8_2_87.mHide()
    this.oPgFrm.Page2.oPag.oWRITE8_2_88.visible=!this.oPgFrm.Page2.oPag.oWRITE8_2_88.mHide()
    this.oPgFrm.Page2.oPag.oDELETE8_2_89.visible=!this.oPgFrm.Page2.oPag.oDELETE8_2_89.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU9_2_91.visible=!this.oPgFrm.Page2.oPag.oCODGRU9_2_91.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU9_2_92.visible=!this.oPgFrm.Page2.oPag.oDESGRU9_2_92.mHide()
    this.oPgFrm.Page2.oPag.oCODCON9_2_94.visible=!this.oPgFrm.Page2.oPag.oCODCON9_2_94.mHide()
    this.oPgFrm.Page2.oPag.oDESCON9_2_95.visible=!this.oPgFrm.Page2.oPag.oDESCON9_2_95.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE9_2_96.visible=!this.oPgFrm.Page2.oPag.oDESAGE9_2_96.mHide()
    this.oPgFrm.Page2.oPag.oREAD9_2_98.visible=!this.oPgFrm.Page2.oPag.oREAD9_2_98.mHide()
    this.oPgFrm.Page2.oPag.oWRITE9_2_99.visible=!this.oPgFrm.Page2.oPag.oWRITE9_2_99.mHide()
    this.oPgFrm.Page2.oPag.oDELETE9_2_100.visible=!this.oPgFrm.Page2.oPag.oDELETE9_2_100.mHide()
    this.oPgFrm.Page2.oPag.oCODGRU10_2_102.visible=!this.oPgFrm.Page2.oPag.oCODGRU10_2_102.mHide()
    this.oPgFrm.Page2.oPag.oDESGRU10_2_103.visible=!this.oPgFrm.Page2.oPag.oDESGRU10_2_103.mHide()
    this.oPgFrm.Page2.oPag.oCODCON10_2_105.visible=!this.oPgFrm.Page2.oPag.oCODCON10_2_105.mHide()
    this.oPgFrm.Page2.oPag.oDESCON10_2_106.visible=!this.oPgFrm.Page2.oPag.oDESCON10_2_106.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE10_2_107.visible=!this.oPgFrm.Page2.oPag.oCODAGE10_2_107.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE10_2_108.visible=!this.oPgFrm.Page2.oPag.oDESAGE10_2_108.mHide()
    this.oPgFrm.Page2.oPag.oREAD10_2_110.visible=!this.oPgFrm.Page2.oPag.oREAD10_2_110.mHide()
    this.oPgFrm.Page2.oPag.oWRITE10_2_111.visible=!this.oPgFrm.Page2.oPag.oWRITE10_2_111.mHide()
    this.oPgFrm.Page2.oPag.oDELETE10_2_112.visible=!this.oPgFrm.Page2.oPag.oDELETE10_2_112.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_113.visible=!this.oPgFrm.Page2.oPag.oBtn_2_113.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_114.visible=!this.oPgFrm.Page2.oPag.oBtn_2_114.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE9_2_125.visible=!this.oPgFrm.Page2.oPag.oCODAGE9_2_125.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_UDGNBYYVTK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRU1
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU1);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU1)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU1) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU1_1_8'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU1)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU1 = NVL(_Link_.code,0)
      this.w_DESGRU1 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU1 = 0
      endif
      this.w_DESGRU1 = space(50)
      this.w_GRUPTYPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE1
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE1)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE1))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE1)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE1) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE1_1_9'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE1)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE1 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE1 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE1 = space(5)
      endif
      this.w_DESAGE1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente inesistente o obsoleto")
        endif
        this.w_CODAGE1 = space(5)
        this.w_DESAGE1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON1
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON1);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON1;
                     ,'ANCODICE',trim(this.w_CODCON1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON1_1_10'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente/fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON1;
                       ,'ANCODICE',this.w_CODCON1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON1 = space(15)
      endif
      this.w_DESCON1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente/fornitore inesistente o obsoleto")
        endif
        this.w_CODCON1 = space(15)
        this.w_DESCON1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL1
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL1);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU1;
                       ,'GRROLE',this.w_CODROL1)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL1 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL1 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCPCFOLDER
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_lTable = "ZFOLDERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2], .t., this.ZFOLDERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCPCFOLDER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZFOLDERS')
        if i_nConn<>0
          i_cWhere = " FOSERIAL="+cp_ToStrODBC(this.w_ZCPCFOLDER);

          i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOSERIAL',this.w_ZCPCFOLDER)
          select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = " 1=2";

            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = " 1=2";

            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZCPCFOLDER) and !this.bDontReportError
            deferred_cp_zoom('ZFOLDERS','*','FOSERIAL',cp_AbsName(oSource.parent,'oZCPCFOLDER_1_18'),i_cWhere,'',"Elenco folders",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',oSource.xKey(1))
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCPCFOLDER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(this.w_ZCPCFOLDER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',this.w_ZCPCFOLDER)
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCPCFOLDER = NVL(_Link_.FOSERIAL,0)
      this.w_ZCPDFOLDER = NVL(_Link_.FODESCRI,space(50))
      this.w_ZCPPFOLDER = NVL(_Link_.FO__PATH,space(255))
      this.w_ZCPTFOLDER = NVL(_Link_.FO__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ZCPCFOLDER = 0
      endif
      this.w_ZCPDFOLDER = space(50)
      this.w_ZCPPFOLDER = space(255)
      this.w_ZCPTFOLDER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPDES1<>'G' or .w_ZCPTFOLDER<>'C') and ((.w_RISERVATO='N' and .w_ZCPTFOLDER<>'C') or .w_RISERVATO='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice folder non valido o folder di tipo <company folder> ancora da associare al cliente, fornitore o agente")
        endif
        this.w_ZCPCFOLDER = 0
        this.w_ZCPDFOLDER = space(50)
        this.w_ZCPPFOLDER = space(255)
        this.w_ZCPTFOLDER = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])+'\'+cp_ToStr(_Link_.FOSERIAL,1)
      cp_ShowWarn(i_cKey,this.ZFOLDERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCPCFOLDER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU2
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU2);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU2)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU2) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU2_2_8'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU2)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU2 = NVL(_Link_.code,0)
      this.w_DESGRU2 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE2 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU2 = 0
      endif
      this.w_DESGRU2 = space(50)
      this.w_GRUPTYPE2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON2
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON2)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON2);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON2;
                     ,'ANCODICE',trim(this.w_CODCON2))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON2)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON2) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON2_2_10'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON2<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON2);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON2);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON2;
                       ,'ANCODICE',this.w_CODCON2)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON2 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON2 = space(15)
      endif
      this.w_DESCON2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON2 = space(15)
        this.w_DESCON2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE2
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE2)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE2))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE2)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE2) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE2_2_12'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE2)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE2 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE2 = space(5)
      endif
      this.w_DESAGE2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE2 = space(5)
        this.w_DESAGE2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL2
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL2);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU2;
                       ,'GRROLE',this.w_CODROL2)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL2 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL2 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU3
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU3);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU3)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU3) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU3_2_20'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU3)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU3 = NVL(_Link_.code,0)
      this.w_DESGRU3 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE3 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU3 = 0
      endif
      this.w_DESGRU3 = space(50)
      this.w_GRUPTYPE3 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON3
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON3)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON3);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON3;
                     ,'ANCODICE',trim(this.w_CODCON3))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON3)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON3) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON3_2_22'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON3<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON3);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON3);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON3;
                       ,'ANCODICE',this.w_CODCON3)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON3 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON3 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON3 = space(15)
      endif
      this.w_DESCON3 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON3 = space(15)
        this.w_DESCON3 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE3
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE3)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE3))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE3)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE3) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE3_2_24'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE3)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE3 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE3 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE3 = space(5)
      endif
      this.w_DESAGE3 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE3 = space(5)
        this.w_DESAGE3 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL3
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL3);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU3;
                       ,'GRROLE',this.w_CODROL3)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL3 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL3 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU4
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU4);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU4)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU4) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU4_2_31'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU4)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU4 = NVL(_Link_.code,0)
      this.w_DESGRU4 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE4 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU4 = 0
      endif
      this.w_DESGRU4 = space(50)
      this.w_GRUPTYPE4 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON4
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON4)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON4);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON4;
                     ,'ANCODICE',trim(this.w_CODCON4))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON4)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON4) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON4_2_34'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON4<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON4);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON4);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON4;
                       ,'ANCODICE',this.w_CODCON4)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON4 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON4 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON4 = space(15)
      endif
      this.w_DESCON4 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON4 = space(15)
        this.w_DESCON4 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE4
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE4)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE4))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE4)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE4) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE4_2_36'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE4)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE4 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE4 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE4 = space(5)
      endif
      this.w_DESAGE4 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE4 = space(5)
        this.w_DESAGE4 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL4
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL4);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU4;
                       ,'GRROLE',this.w_CODROL4)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL4 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL4 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU5
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU5);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU5)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU5) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU5_2_43'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU5)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU5 = NVL(_Link_.code,0)
      this.w_DESGRU5 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE5 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU5 = 0
      endif
      this.w_DESGRU5 = space(50)
      this.w_GRUPTYPE5 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON5
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON5)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON5);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON5;
                     ,'ANCODICE',trim(this.w_CODCON5))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON5)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON5) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON5_2_46'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON5<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON5);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON5);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON5;
                       ,'ANCODICE',this.w_CODCON5)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON5 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON5 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON5 = space(15)
      endif
      this.w_DESCON5 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON5 = space(15)
        this.w_DESCON5 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE5
  func Link_2_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE5)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE5))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE5)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE5) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE5_2_48'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE5)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE5 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE5 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE5 = space(5)
      endif
      this.w_DESAGE5 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE5 = space(5)
        this.w_DESAGE5 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL5
  func Link_2_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL5);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU5;
                       ,'GRROLE',this.w_CODROL5)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL5 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL5 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU6
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU6);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU6)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU6) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU6_2_55'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU6)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU6 = NVL(_Link_.code,0)
      this.w_DESGRU6 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE6 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU6 = 0
      endif
      this.w_DESGRU6 = space(50)
      this.w_GRUPTYPE6 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON6
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON6)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON6);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON6;
                     ,'ANCODICE',trim(this.w_CODCON6))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON6)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON6) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON6_2_58'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON6<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON6);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON6);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON6;
                       ,'ANCODICE',this.w_CODCON6)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON6 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON6 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON6 = space(15)
      endif
      this.w_DESCON6 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o non valido")
        endif
        this.w_CODCON6 = space(15)
        this.w_DESCON6 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE6
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE6)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE6))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE6)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE6) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE6_2_60'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE6)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE6 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE6 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE6 = space(5)
      endif
      this.w_DESAGE6 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o non valido")
        endif
        this.w_CODAGE6 = space(5)
        this.w_DESAGE6 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL6
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL6);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU6;
                       ,'GRROLE',this.w_CODROL6)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL6 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL6 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU7
  func Link_2_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU7);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU7)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU7) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU7_2_67'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU7)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU7 = NVL(_Link_.code,0)
      this.w_DESGRU7 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE7 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU7 = 0
      endif
      this.w_DESGRU7 = space(50)
      this.w_GRUPTYPE7 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON7
  func Link_2_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON7)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON7);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON7;
                     ,'ANCODICE',trim(this.w_CODCON7))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON7)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON7) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON7_2_70'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON7<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON7);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON7);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON7;
                       ,'ANCODICE',this.w_CODCON7)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON7 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON7 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON7 = space(15)
      endif
      this.w_DESCON7 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON7 = space(15)
        this.w_DESCON7 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE7
  func Link_2_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE7)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE7))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE7)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE7) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE7_2_72'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE7)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE7 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE7 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE7 = space(5)
      endif
      this.w_DESAGE7 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE7 = space(5)
        this.w_DESAGE7 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL7
  func Link_2_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL7);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU7;
                       ,'GRROLE',this.w_CODROL7)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL7 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL7 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU8
  func Link_2_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU8);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU8)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU8) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU8_2_79'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU8)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU8 = NVL(_Link_.code,0)
      this.w_DESGRU8 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE8 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU8 = 0
      endif
      this.w_DESGRU8 = space(50)
      this.w_GRUPTYPE8 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON8
  func Link_2_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON8)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON8);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON8;
                     ,'ANCODICE',trim(this.w_CODCON8))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON8)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON8) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON8_2_82'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON8<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON8);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON8);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON8;
                       ,'ANCODICE',this.w_CODCON8)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON8 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON8 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON8 = space(15)
      endif
      this.w_DESCON8 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON8 = space(15)
        this.w_DESCON8 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE8
  func Link_2_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE8)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE8))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE8)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE8) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE8_2_84'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE8)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE8 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE8 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE8 = space(5)
      endif
      this.w_DESAGE8 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE8 = space(5)
        this.w_DESAGE8 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL8
  func Link_2_86(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL8);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU8;
                       ,'GRROLE',this.w_CODROL8)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL8 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL8 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU9
  func Link_2_91(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU9);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU9)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU9) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU9_2_91'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU9)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU9 = NVL(_Link_.code,0)
      this.w_DESGRU9 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE9 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU9 = 0
      endif
      this.w_DESGRU9 = space(50)
      this.w_GRUPTYPE9 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON9
  func Link_2_94(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON9)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON9);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON9;
                     ,'ANCODICE',trim(this.w_CODCON9))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON9)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON9) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON9_2_94'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON9<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON9);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON9);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON9;
                       ,'ANCODICE',this.w_CODCON9)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON9 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON9 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON9 = space(15)
      endif
      this.w_DESCON9 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON9 = space(15)
        this.w_DESCON9 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL9
  func Link_2_97(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL9);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU9;
                       ,'GRROLE',this.w_CODROL9)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL9 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL9 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU10
  func Link_2_102(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU10) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRU10);

          i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRU10)
          select code,name,grptype;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU10) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oCODGRU10_2_102'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU10)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name,grptype";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRU10);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRU10)
            select code,name,grptype;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU10 = NVL(_Link_.code,0)
      this.w_DESGRU10 = NVL(_Link_.name,space(50))
      this.w_GRUPTYPE10 = NVL(_Link_.grptype,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU10 = 0
      endif
      this.w_DESGRU10 = space(50)
      this.w_GRUPTYPE10 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU10 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON10
  func Link_2_105(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON10) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON10)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON10);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON10;
                     ,'ANCODICE',trim(this.w_CODCON10))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON10)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON10) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON10_2_105'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON10<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON10);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON10)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON10);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON10);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON10;
                       ,'ANCODICE',this.w_CODCON10)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON10 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON10 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON10 = space(15)
      endif
      this.w_DESCON10 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON10 = space(15)
        this.w_DESCON10 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON10 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE10
  func Link_2_107(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE10) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE10)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE10))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE10)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE10) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE10_2_107'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE10)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE10);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE10)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE10 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE10 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE10 = space(5)
      endif
      this.w_DESAGE10 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE10 = space(5)
        this.w_DESAGE10 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE10 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROL10
  func Link_2_109(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROL10) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROL10)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_CODROL10);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_CODGRU10);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_CODGRU10;
                       ,'GRROLE',this.w_CODROL10)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROL10 = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODROL10 = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROL10 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE9
  func Link_2_125(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE9)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE9))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE9)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE9) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE9_2_125'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE9)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE9 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE9 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE9 = space(5)
      endif
      this.w_DESAGE9 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODAGE9 = space(5)
        this.w_DESAGE9 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRISERVATO_1_5.RadioValue()==this.w_RISERVATO)
      this.oPgFrm.Page1.oPag.oRISERVATO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDES1_1_6.RadioValue()==this.w_TIPDES1)
      this.oPgFrm.Page1.oPag.oTIPDES1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU1_1_8.value==this.w_CODGRU1)
      this.oPgFrm.Page1.oPag.oCODGRU1_1_8.value=this.w_CODGRU1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAGE1_1_9.value==this.w_CODAGE1)
      this.oPgFrm.Page1.oPag.oCODAGE1_1_9.value=this.w_CODAGE1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON1_1_10.value==this.w_CODCON1)
      this.oPgFrm.Page1.oPag.oCODCON1_1_10.value=this.w_CODCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oREAD1_1_12.RadioValue()==this.w_READ1)
      this.oPgFrm.Page1.oPag.oREAD1_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oWRITE1_1_13.RadioValue()==this.w_WRITE1)
      this.oPgFrm.Page1.oPag.oWRITE1_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDELETE1_1_14.RadioValue()==this.w_DELETE1)
      this.oPgFrm.Page1.oPag.oDELETE1_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZCPFTITLE_1_15.value==this.w_ZCPFTITLE)
      this.oPgFrm.Page1.oPag.oZCPFTITLE_1_15.value=this.w_ZCPFTITLE
    endif
    if not(this.oPgFrm.Page1.oPag.oZCPCFOLDER_1_18.value==this.w_ZCPCFOLDER)
      this.oPgFrm.Page1.oPag.oZCPCFOLDER_1_18.value=this.w_ZCPCFOLDER
    endif
    if not(this.oPgFrm.Page1.oPag.oZCPDFOLDER_1_20.value==this.w_ZCPDFOLDER)
      this.oPgFrm.Page1.oPag.oZCPDFOLDER_1_20.value=this.w_ZCPDFOLDER
    endif
    if not(this.oPgFrm.Page1.oPag.oZCPPFOLDER_1_21.value==this.w_ZCPPFOLDER)
      this.oPgFrm.Page1.oPag.oZCPPFOLDER_1_21.value=this.w_ZCPPFOLDER
    endif
    if not(this.oPgFrm.Page1.oPag.oZCPFDESCRI_1_22.value==this.w_ZCPFDESCRI)
      this.oPgFrm.Page1.oPag.oZCPFDESCRI_1_22.value=this.w_ZCPFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU1_1_23.value==this.w_DESGRU1)
      this.oPgFrm.Page1.oPag.oDESGRU1_1_23.value=this.w_DESGRU1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON1_1_24.value==this.w_DESCON1)
      this.oPgFrm.Page1.oPag.oDESCON1_1_24.value=this.w_DESCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE1_1_25.value==this.w_DESAGE1)
      this.oPgFrm.Page1.oPag.oDESAGE1_1_25.value=this.w_DESAGE1
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES2_2_6.RadioValue()==this.w_TIPDES2)
      this.oPgFrm.Page2.oPag.oTIPDES2_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU2_2_8.value==this.w_CODGRU2)
      this.oPgFrm.Page2.oPag.oCODGRU2_2_8.value=this.w_CODGRU2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU2_2_9.value==this.w_DESGRU2)
      this.oPgFrm.Page2.oPag.oDESGRU2_2_9.value=this.w_DESGRU2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON2_2_10.value==this.w_CODCON2)
      this.oPgFrm.Page2.oPag.oCODCON2_2_10.value=this.w_CODCON2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON2_2_11.value==this.w_DESCON2)
      this.oPgFrm.Page2.oPag.oDESCON2_2_11.value=this.w_DESCON2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE2_2_12.value==this.w_CODAGE2)
      this.oPgFrm.Page2.oPag.oCODAGE2_2_12.value=this.w_CODAGE2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE2_2_13.value==this.w_DESAGE2)
      this.oPgFrm.Page2.oPag.oDESAGE2_2_13.value=this.w_DESAGE2
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD2_2_15.RadioValue()==this.w_READ2)
      this.oPgFrm.Page2.oPag.oREAD2_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE2_2_16.RadioValue()==this.w_WRITE2)
      this.oPgFrm.Page2.oPag.oWRITE2_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE2_2_17.RadioValue()==this.w_DELETE2)
      this.oPgFrm.Page2.oPag.oDELETE2_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES3_2_18.RadioValue()==this.w_TIPDES3)
      this.oPgFrm.Page2.oPag.oTIPDES3_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU3_2_20.value==this.w_CODGRU3)
      this.oPgFrm.Page2.oPag.oCODGRU3_2_20.value=this.w_CODGRU3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU3_2_21.value==this.w_DESGRU3)
      this.oPgFrm.Page2.oPag.oDESGRU3_2_21.value=this.w_DESGRU3
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON3_2_22.value==this.w_CODCON3)
      this.oPgFrm.Page2.oPag.oCODCON3_2_22.value=this.w_CODCON3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON3_2_23.value==this.w_DESCON3)
      this.oPgFrm.Page2.oPag.oDESCON3_2_23.value=this.w_DESCON3
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE3_2_24.value==this.w_CODAGE3)
      this.oPgFrm.Page2.oPag.oCODAGE3_2_24.value=this.w_CODAGE3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE3_2_25.value==this.w_DESAGE3)
      this.oPgFrm.Page2.oPag.oDESAGE3_2_25.value=this.w_DESAGE3
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD3_2_27.RadioValue()==this.w_READ3)
      this.oPgFrm.Page2.oPag.oREAD3_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE3_2_28.RadioValue()==this.w_WRITE3)
      this.oPgFrm.Page2.oPag.oWRITE3_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE3_2_29.RadioValue()==this.w_DELETE3)
      this.oPgFrm.Page2.oPag.oDELETE3_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES4_2_30.RadioValue()==this.w_TIPDES4)
      this.oPgFrm.Page2.oPag.oTIPDES4_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU4_2_31.value==this.w_CODGRU4)
      this.oPgFrm.Page2.oPag.oCODGRU4_2_31.value=this.w_CODGRU4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU4_2_32.value==this.w_DESGRU4)
      this.oPgFrm.Page2.oPag.oDESGRU4_2_32.value=this.w_DESGRU4
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON4_2_34.value==this.w_CODCON4)
      this.oPgFrm.Page2.oPag.oCODCON4_2_34.value=this.w_CODCON4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON4_2_35.value==this.w_DESCON4)
      this.oPgFrm.Page2.oPag.oDESCON4_2_35.value=this.w_DESCON4
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE4_2_36.value==this.w_CODAGE4)
      this.oPgFrm.Page2.oPag.oCODAGE4_2_36.value=this.w_CODAGE4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE4_2_37.value==this.w_DESAGE4)
      this.oPgFrm.Page2.oPag.oDESAGE4_2_37.value=this.w_DESAGE4
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD4_2_39.RadioValue()==this.w_READ4)
      this.oPgFrm.Page2.oPag.oREAD4_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE4_2_40.RadioValue()==this.w_WRITE4)
      this.oPgFrm.Page2.oPag.oWRITE4_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE4_2_41.RadioValue()==this.w_DELETE4)
      this.oPgFrm.Page2.oPag.oDELETE4_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES5_2_42.RadioValue()==this.w_TIPDES5)
      this.oPgFrm.Page2.oPag.oTIPDES5_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU5_2_43.value==this.w_CODGRU5)
      this.oPgFrm.Page2.oPag.oCODGRU5_2_43.value=this.w_CODGRU5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU5_2_44.value==this.w_DESGRU5)
      this.oPgFrm.Page2.oPag.oDESGRU5_2_44.value=this.w_DESGRU5
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON5_2_46.value==this.w_CODCON5)
      this.oPgFrm.Page2.oPag.oCODCON5_2_46.value=this.w_CODCON5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON5_2_47.value==this.w_DESCON5)
      this.oPgFrm.Page2.oPag.oDESCON5_2_47.value=this.w_DESCON5
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE5_2_48.value==this.w_CODAGE5)
      this.oPgFrm.Page2.oPag.oCODAGE5_2_48.value=this.w_CODAGE5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE5_2_49.value==this.w_DESAGE5)
      this.oPgFrm.Page2.oPag.oDESAGE5_2_49.value=this.w_DESAGE5
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD5_2_51.RadioValue()==this.w_READ5)
      this.oPgFrm.Page2.oPag.oREAD5_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE5_2_52.RadioValue()==this.w_WRITE5)
      this.oPgFrm.Page2.oPag.oWRITE5_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE5_2_53.RadioValue()==this.w_DELETE5)
      this.oPgFrm.Page2.oPag.oDELETE5_2_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES6_2_54.RadioValue()==this.w_TIPDES6)
      this.oPgFrm.Page2.oPag.oTIPDES6_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU6_2_55.value==this.w_CODGRU6)
      this.oPgFrm.Page2.oPag.oCODGRU6_2_55.value=this.w_CODGRU6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU6_2_56.value==this.w_DESGRU6)
      this.oPgFrm.Page2.oPag.oDESGRU6_2_56.value=this.w_DESGRU6
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON6_2_58.value==this.w_CODCON6)
      this.oPgFrm.Page2.oPag.oCODCON6_2_58.value=this.w_CODCON6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON6_2_59.value==this.w_DESCON6)
      this.oPgFrm.Page2.oPag.oDESCON6_2_59.value=this.w_DESCON6
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE6_2_60.value==this.w_CODAGE6)
      this.oPgFrm.Page2.oPag.oCODAGE6_2_60.value=this.w_CODAGE6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE6_2_61.value==this.w_DESAGE6)
      this.oPgFrm.Page2.oPag.oDESAGE6_2_61.value=this.w_DESAGE6
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD6_2_63.RadioValue()==this.w_READ6)
      this.oPgFrm.Page2.oPag.oREAD6_2_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE6_2_64.RadioValue()==this.w_WRITE6)
      this.oPgFrm.Page2.oPag.oWRITE6_2_64.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE6_2_65.RadioValue()==this.w_DELETE6)
      this.oPgFrm.Page2.oPag.oDELETE6_2_65.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES7_2_66.RadioValue()==this.w_TIPDES7)
      this.oPgFrm.Page2.oPag.oTIPDES7_2_66.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU7_2_67.value==this.w_CODGRU7)
      this.oPgFrm.Page2.oPag.oCODGRU7_2_67.value=this.w_CODGRU7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU7_2_68.value==this.w_DESGRU7)
      this.oPgFrm.Page2.oPag.oDESGRU7_2_68.value=this.w_DESGRU7
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON7_2_70.value==this.w_CODCON7)
      this.oPgFrm.Page2.oPag.oCODCON7_2_70.value=this.w_CODCON7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON7_2_71.value==this.w_DESCON7)
      this.oPgFrm.Page2.oPag.oDESCON7_2_71.value=this.w_DESCON7
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE7_2_72.value==this.w_CODAGE7)
      this.oPgFrm.Page2.oPag.oCODAGE7_2_72.value=this.w_CODAGE7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE7_2_73.value==this.w_DESAGE7)
      this.oPgFrm.Page2.oPag.oDESAGE7_2_73.value=this.w_DESAGE7
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD7_2_75.RadioValue()==this.w_READ7)
      this.oPgFrm.Page2.oPag.oREAD7_2_75.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE7_2_76.RadioValue()==this.w_WRITE7)
      this.oPgFrm.Page2.oPag.oWRITE7_2_76.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE7_2_77.RadioValue()==this.w_DELETE7)
      this.oPgFrm.Page2.oPag.oDELETE7_2_77.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES8_2_78.RadioValue()==this.w_TIPDES8)
      this.oPgFrm.Page2.oPag.oTIPDES8_2_78.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU8_2_79.value==this.w_CODGRU8)
      this.oPgFrm.Page2.oPag.oCODGRU8_2_79.value=this.w_CODGRU8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU8_2_80.value==this.w_DESGRU8)
      this.oPgFrm.Page2.oPag.oDESGRU8_2_80.value=this.w_DESGRU8
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON8_2_82.value==this.w_CODCON8)
      this.oPgFrm.Page2.oPag.oCODCON8_2_82.value=this.w_CODCON8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON8_2_83.value==this.w_DESCON8)
      this.oPgFrm.Page2.oPag.oDESCON8_2_83.value=this.w_DESCON8
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE8_2_84.value==this.w_CODAGE8)
      this.oPgFrm.Page2.oPag.oCODAGE8_2_84.value=this.w_CODAGE8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE8_2_85.value==this.w_DESAGE8)
      this.oPgFrm.Page2.oPag.oDESAGE8_2_85.value=this.w_DESAGE8
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD8_2_87.RadioValue()==this.w_READ8)
      this.oPgFrm.Page2.oPag.oREAD8_2_87.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE8_2_88.RadioValue()==this.w_WRITE8)
      this.oPgFrm.Page2.oPag.oWRITE8_2_88.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE8_2_89.RadioValue()==this.w_DELETE8)
      this.oPgFrm.Page2.oPag.oDELETE8_2_89.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES9_2_90.RadioValue()==this.w_TIPDES9)
      this.oPgFrm.Page2.oPag.oTIPDES9_2_90.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU9_2_91.value==this.w_CODGRU9)
      this.oPgFrm.Page2.oPag.oCODGRU9_2_91.value=this.w_CODGRU9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU9_2_92.value==this.w_DESGRU9)
      this.oPgFrm.Page2.oPag.oDESGRU9_2_92.value=this.w_DESGRU9
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON9_2_94.value==this.w_CODCON9)
      this.oPgFrm.Page2.oPag.oCODCON9_2_94.value=this.w_CODCON9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON9_2_95.value==this.w_DESCON9)
      this.oPgFrm.Page2.oPag.oDESCON9_2_95.value=this.w_DESCON9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE9_2_96.value==this.w_DESAGE9)
      this.oPgFrm.Page2.oPag.oDESAGE9_2_96.value=this.w_DESAGE9
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD9_2_98.RadioValue()==this.w_READ9)
      this.oPgFrm.Page2.oPag.oREAD9_2_98.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE9_2_99.RadioValue()==this.w_WRITE9)
      this.oPgFrm.Page2.oPag.oWRITE9_2_99.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE9_2_100.RadioValue()==this.w_DELETE9)
      this.oPgFrm.Page2.oPag.oDELETE9_2_100.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDES10_2_101.RadioValue()==this.w_TIPDES10)
      this.oPgFrm.Page2.oPag.oTIPDES10_2_101.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU10_2_102.value==this.w_CODGRU10)
      this.oPgFrm.Page2.oPag.oCODGRU10_2_102.value=this.w_CODGRU10
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU10_2_103.value==this.w_DESGRU10)
      this.oPgFrm.Page2.oPag.oDESGRU10_2_103.value=this.w_DESGRU10
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON10_2_105.value==this.w_CODCON10)
      this.oPgFrm.Page2.oPag.oCODCON10_2_105.value=this.w_CODCON10
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON10_2_106.value==this.w_DESCON10)
      this.oPgFrm.Page2.oPag.oDESCON10_2_106.value=this.w_DESCON10
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE10_2_107.value==this.w_CODAGE10)
      this.oPgFrm.Page2.oPag.oCODAGE10_2_107.value=this.w_CODAGE10
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE10_2_108.value==this.w_DESAGE10)
      this.oPgFrm.Page2.oPag.oDESAGE10_2_108.value=this.w_DESAGE10
    endif
    if not(this.oPgFrm.Page2.oPag.oREAD10_2_110.RadioValue()==this.w_READ10)
      this.oPgFrm.Page2.oPag.oREAD10_2_110.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWRITE10_2_111.RadioValue()==this.w_WRITE10)
      this.oPgFrm.Page2.oPag.oWRITE10_2_111.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDELETE10_2_112.RadioValue()==this.w_DELETE10)
      this.oPgFrm.Page2.oPag.oDELETE10_2_112.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE9_2_125.value==this.w_CODAGE9)
      this.oPgFrm.Page2.oPag.oCODAGE9_2_125.value=this.w_CODAGE9
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES1<>'A')  and (.w_TIPDES1='A' and .w_RISERVATO<>'N')  and not(empty(.w_CODAGE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODAGE1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES1$'CF'))  and (.w_TIPDES1$'CF' and .w_RISERVATO<>'N')  and not(empty(.w_CODCON1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON1_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente/fornitore inesistente o obsoleto")
          case   (empty(.w_ZCPFTITLE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCPFTITLE_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ZCPFTITLE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ZCPCFOLDER)) or not((.w_TIPDES1<>'G' or .w_ZCPTFOLDER<>'C') and ((.w_RISERVATO='N' and .w_ZCPTFOLDER<>'C') or .w_RISERVATO='S')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCPCFOLDER_1_18.SetFocus()
            i_bnoObbl = !empty(.w_ZCPCFOLDER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice folder non valido o folder di tipo <company folder> ancora da associare al cliente, fornitore o agente")
          case   (empty(.w_ZCPFDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCPFDESCRI_1_22.SetFocus()
            i_bnoObbl = !empty(.w_ZCPFDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES2$'CF'))  and (.w_TIPDES2$'CF')  and not(empty(.w_CODCON2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON2_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES2<>'A')  and (.w_TIPDES2='A')  and not(empty(.w_CODAGE2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE2_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES3$'CF'))  and (.w_TIPDES3$'CF')  and not(empty(.w_CODCON3))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON3_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES3<>'A')  and (.w_TIPDES3='A')  and not(empty(.w_CODAGE3))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE3_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES4$'CF'))  and (.w_TIPDES4$'CF')  and not(empty(.w_CODCON4))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON4_2_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES4<>'A')  and (.w_TIPDES4='A')  and not(empty(.w_CODAGE4))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE4_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES5$'CF'))  and (.w_TIPDES5$'CF')  and not(empty(.w_CODCON5))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON5_2_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES5<>'A')  and (.w_TIPDES5='A')  and not(empty(.w_CODAGE5))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE5_2_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES6$'CF'))  and (.w_TIPDES6$'CF')  and not(empty(.w_CODCON6))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON6_2_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o non valido")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES6<>'A')  and (.w_TIPDES6='A')  and not(empty(.w_CODAGE6))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE6_2_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o non valido")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES7$'CF'))  and (.w_TIPDES7$'CF')  and not(empty(.w_CODCON7))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON7_2_70.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES7<>'A')  and (.w_TIPDES7='A')  and not(empty(.w_CODAGE7))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE7_2_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES8$'CF'))  and (.w_TIPDES8$'CF')  and not(empty(.w_CODCON8))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON8_2_82.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES8<>'A')  and (.w_TIPDES8='A')  and not(empty(.w_CODAGE8))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE8_2_84.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES9$'CF'))  and (.w_TIPDES9$'CF')  and not(empty(.w_CODCON9))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON9_2_94.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(Not(.w_TIPDES10$'CF'))  and (.w_TIPDES10$'CF')  and not(empty(.w_CODCON10))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON10_2_105.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES10<>'A')  and (.w_TIPDES10='A')  and not(empty(.w_CODAGE10))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE10_2_107.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(.w_DATOBSO > .w_OBTEST or empty(.w_DATOBSO))  and not(.w_TIPDES9<>'A')  and (.w_TIPDES9='A')  and not(empty(.w_CODAGE9))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGE9_2_125.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut2kxp
      if .w_RISERVATO='S'
         Local lIndice,lIndex
         LOCAL TestMacro , Macro2
         for lIndice=1 to 10
           lIndex=alltrim(str(lIndice,2,0))
           TestMacro=.w_TIPDES&lIndex
           Macro2=.w_CODCON&lIndex
           if i_bRes and TestMacro='C' and empty(Macro2)
             =ah_ErrorMsg("� stato indicato un destinatario di tipo <cliente> senza l'indicazione del codice cliente",'!')
             i_bRes=.F.
           endif
           if i_bRes and TestMacro='F' and empty(Macro2)
             =ah_ErrorMsg("� stato indicato un destinatario di tipo <fornitore> senza l'indicazione del codice fornitore",'!')
             i_bRes=.F.
           endif
           Macro2=.w_CODAGE&lIndex
           if i_bRes and TestMacro='A' and empty(Macro2)
             =ah_ErrorMsg("� stato indicato un destinatario di tipo <agente> senza l'indicazione del codice agente",'!')
             i_bRes=.F.
           endif
            Macro2=this.w_CODGRU&lIndex
           if TestMacro='G' AND empty(Macro2)
             =ah_ErrorMsg("� stato indicato un destinatario di tipo <gruppo> senza l'indicazione del codice gruppo",'!')
             i_bRes=.F.
           endif
           if not(i_Bres)
             exit
           endif
         next
      endif
      * Controlla che ci sia sempre il folder
      if i_bRes
         if empty(.w_ZCPCFOLDER)
           =ah_ErrorMsg("Occorre indicare sempre un folder (CPZ Folder) di destinazione",'!')
           i_bRes=.F.
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RISERVATO = this.w_RISERVATO
    this.o_TIPDES1 = this.w_TIPDES1
    this.o_CODAGE1 = this.w_CODAGE1
    this.o_CODCON1 = this.w_CODCON1
    this.o_ZCPFTITLE = this.w_ZCPFTITLE
    this.o_ZCPCFOLDER = this.w_ZCPCFOLDER
    this.o_ZCPPFOLDER = this.w_ZCPPFOLDER
    this.o_TIPDES2 = this.w_TIPDES2
    this.o_TIPDES3 = this.w_TIPDES3
    this.o_TIPDES4 = this.w_TIPDES4
    this.o_TIPDES5 = this.w_TIPDES5
    this.o_TIPDES6 = this.w_TIPDES6
    this.o_TIPDES7 = this.w_TIPDES7
    this.o_TIPDES8 = this.w_TIPDES8
    this.o_TIPDES9 = this.w_TIPDES9
    this.o_TIPDES10 = this.w_TIPDES10
    return

enddefine

* --- Define pages as container
define class tgsut2kxpPag1 as StdContainer
  Width  = 607
  height = 302
  stdWidth  = 607
  stdheight = 302
  resizeXpos=195
  resizeYpos=240
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_1_33 as image with uid="WLNVKTWYRU",left=402, top=176, width=190,height=70,;
      Picture="..\exe\bmp\ZCorporate.jpg"

  add object oRISERVATO_1_5 as StdRadio with uid="XMRFLJRLCV",rtseq=5,rtrep=.f.,left=280, top=10, width=165,height=22;
    , ToolTipText = "Se attivo: documento riservato altrimenti pubblico";
    , cFormVar="w_RISERVATO", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.t., FntItalic=.f., FntUnderline=.f., FntStrikeThru=.f.

    proc oRISERVATO_1_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Riservato"
      this.Buttons(1).HelpContextID = 95141798
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Riservato","Arial",8,"B")*FontMetric(6,"Arial",8,"B"))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Pubblico"
      this.Buttons(2).HelpContextID = 95141798
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Pubblico","Arial",8,"B")*FontMetric(6,"Arial",8,"B"))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",22)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Se attivo: documento riservato altrimenti pubblico")
      StdRadio::init()
    endproc

  func oRISERVATO_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    'N')))
  endfunc
  func oRISERVATO_1_5.GetRadio()
    this.Parent.oContained.w_RISERVATO = this.RadioValue()
    return .t.
  endfunc

  func oRISERVATO_1_5.SetRadio()
    this.Parent.oContained.w_RISERVATO=trim(this.Parent.oContained.w_RISERVATO)
    this.value = ;
      iif(this.Parent.oContained.w_RISERVATO=='S',1,;
      iif(this.Parent.oContained.w_RISERVATO=='N',2,;
      0))
  endfunc


  add object oTIPDES1_1_6 as StdCombo with uid="RCIZSZDLHV",rtseq=6,rtrep=.f.,left=16,top=47,width=108,height=21;
    , ToolTipText = "Tipologia destinatario";
    , HelpContextID = 159184074;
    , cFormVar="w_TIPDES1",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo", bObbl = .f. , nPag = 1;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES1_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    space(1))))))
  endfunc
  func oTIPDES1_1_6.GetRadio()
    this.Parent.oContained.w_TIPDES1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES1_1_6.SetRadio()
    this.Parent.oContained.w_TIPDES1=trim(this.Parent.oContained.w_TIPDES1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES1=='C',1,;
      iif(this.Parent.oContained.w_TIPDES1=='F',2,;
      iif(this.Parent.oContained.w_TIPDES1=='A',3,;
      iif(this.Parent.oContained.w_TIPDES1=='G',4,;
      0))))
  endfunc

  func oTIPDES1_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU1_1_8 as StdField with uid="OHCKHSVELV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODGRU1", cQueryName = "CODGRU1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice gruppo inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 111849434,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=48, Left=129, Top=48, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU1"

  func oCODGRU1_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES1='G' and .w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  func oCODGRU1_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'G')
    endwith
  endfunc

  func oCODGRU1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
      if .not. empty(.w_CODROL1)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU1_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU1_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU1_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oCODAGE1_1_9 as StdField with uid="HBCLXHBRWM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODAGE1", cQueryName = "CODAGE1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente o obsoleto",;
    ToolTipText = "Codice agente",;
    HelpContextID = 123776986,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=42, Left=129, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE1"

  func oCODAGE1_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES1='A' and .w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  func oCODAGE1_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'A')
    endwith
  endfunc

  func oCODAGE1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE1_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE1_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE1_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oCODCON1_1_10 as StdField with uid="VFYLNYBUGF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODCON1", cQueryName = "CODCON1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente/fornitore inesistente o obsoleto",;
    ToolTipText = "Codice cliente/fornitore",;
    HelpContextID = 232697818,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=102, Left=129, Top=48, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON1", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON1"

  func oCODCON1_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES1$'CF' and .w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  func oCODCON1_1_10.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES1$'CF'))
    endwith
  endfunc

  func oCODCON1_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON1_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON1_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON1)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON1_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oREAD1_1_12 as StdCheck with uid="KRMBAQSMKM",rtseq=12,rtrep=.f.,left=486, top=48, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 230549738,;
    cFormVar="w_READ1", bObbl = .f. , nPag = 1;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD1_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD1_1_12.GetRadio()
    this.Parent.oContained.w_READ1 = this.RadioValue()
    return .t.
  endfunc

  func oREAD1_1_12.SetRadio()
    this.Parent.oContained.w_READ1=trim(this.Parent.oContained.w_READ1)
    this.value = ;
      iif(this.Parent.oContained.w_READ1=='S',1,;
      0)
  endfunc

  func oREAD1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oWRITE1_1_13 as StdCheck with uid="ENAHKTPDYF",rtseq=13,rtrep=.f.,left=509, top=48, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 191716250,;
    cFormVar="w_WRITE1", bObbl = .f. , nPag = 1;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE1_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE1_1_13.GetRadio()
    this.Parent.oContained.w_WRITE1 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE1_1_13.SetRadio()
    this.Parent.oContained.w_WRITE1=trim(this.Parent.oContained.w_WRITE1)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE1=='S',1,;
      0)
  endfunc

  func oWRITE1_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oDELETE1_1_14 as StdCheck with uid="SEXVBFRXHB",rtseq=14,rtrep=.f.,left=533, top=48, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 109853130,;
    cFormVar="w_DELETE1", bObbl = .f. , nPag = 1;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE1_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE1_1_14.GetRadio()
    this.Parent.oContained.w_DELETE1 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE1_1_14.SetRadio()
    this.Parent.oContained.w_DELETE1=trim(this.Parent.oContained.w_DELETE1)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE1=='S',1,;
      0)
  endfunc

  func oDELETE1_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oZCPFTITLE_1_15 as StdField with uid="WOTYGVRDBM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ZCPFTITLE", cQueryName = "ZCPFTITLE",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome file da inviare sul portale",;
    HelpContextID = 225774130,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=508, Left=85, Top=84, InputMask=replicate('X',200)

  add object oZCPCFOLDER_1_18 as StdField with uid="OSORZTJTVN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ZCPCFOLDER", cQueryName = "ZCPCFOLDER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice folder non valido o folder di tipo <company folder> ancora da associare al cliente, fornitore o agente",;
    ToolTipText = "Folder di destinazione su portale",;
    HelpContextID = 225289174,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=72, Left=85, Top=117, cSayPict='"@Z 9999999999"', bHasZoom = .t. , cLinkFile="ZFOLDERS", oKey_1_1="FOSERIAL", oKey_1_2="this.w_ZCPCFOLDER"

  func oZCPCFOLDER_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oZCPCFOLDER_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZCPCFOLDER_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZFOLDERS','*','FOSERIAL',cp_AbsName(this.parent,'oZCPCFOLDER_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco folders",'',this.parent.oContained
  endproc

  add object oZCPDFOLDER_1_20 as StdField with uid="UUCHHXRTQJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ZCPDFOLDER", cQueryName = "ZCPDFOLDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione folder DMS di destinazione...",;
    HelpContextID = 225223638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=432, Left=160, Top=117, InputMask=replicate('X',50)

  add object oZCPPFOLDER_1_21 as StdField with uid="QGJQHSVOIC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ZCPPFOLDER", cQueryName = "ZCPPFOLDER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    ToolTipText = "Folder DMS di destinazione...",;
    HelpContextID = 224437206,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=508, Left=85, Top=142, InputMask=replicate('X',255)

  add object oZCPFDESCRI_1_22 as StdMemo with uid="OKESNJRMDN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ZCPFDESCRI", cQueryName = "ZCPFDESCRI",;
    bObbl = .t. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione file da inviare sul portale",;
    HelpContextID = 126528519,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=107, Width=308, Left=85, Top=175

  add object oDESGRU1_1_23 as StdField with uid="YZZEZURLRU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESGRU1", cQueryName = "DESGRU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 111790538,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=291, Left=180, Top=48, InputMask=replicate('X',50)

  func oDESGRU1_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'G')
    endwith
  endfunc

  add object oDESCON1_1_24 as StdField with uid="OYHIZCJMCG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCON1", cQueryName = "DESCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 232638922,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=237, Left=233, Top=48, InputMask=replicate('X',40)

  func oDESCON1_1_24.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES1$'CF'))
    endwith
  endfunc

  add object oDESAGE1_1_25 as StdField with uid="YBFGTDIKJJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESAGE1", cQueryName = "DESAGE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 123718090,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=222, Left=178, Top=48, InputMask=replicate('X',35)

  func oDESAGE1_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'A')
    endwith
  endfunc


  add object oBtn_1_38 as StdButton with uid="WHSENRFQRK",left=491, top=256, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare...";
    , HelpContextID = 250227942;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not(Empty(.w_ZCPFTITLE)) and (Not(Empty(.w_ZCPPFOLDER)) or .w_ZCPTFOLDER='C'))
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not(g_APPLICATION='ADHOC REVOLUTION'))
     endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="SJNEQIMFBD",left=545, top=256, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 257516614;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not(g_APPLICATION='ADHOC REVOLUTION'))
     endwith
    endif
  endfunc

  add object oStr_1_26 as StdString with uid="QDHRDEFYNB",Visible=.t., Left=12, Top=86,;
    Alignment=1, Width=69, Height=17,;
    Caption="Nome file:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="XIQKJLOHJY",Visible=.t., Left=13, Top=175,;
    Alignment=1, Width=68, Height=17,;
    Caption="Descrizione:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="WQIOZTWBTD",Visible=.t., Left=129, Top=32,;
    Alignment=0, Width=50, Height=17,;
    Caption="Gruppo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'G')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="YOYANUTISB",Visible=.t., Left=129, Top=32,;
    Alignment=0, Width=91, Height=17,;
    Caption="Cliente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES1$'C'))
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="JUHJRSVSXC",Visible=.t., Left=129, Top=32,;
    Alignment=0, Width=38, Height=17,;
    Caption="Agente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPDES1<>'A')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="HNMNZPEZZZ",Visible=.t., Left=17, Top=33,;
    Alignment=0, Width=85, Height=16,;
    Caption="Destinatario"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="TOTERTKEIJ",Visible=.t., Left=14, Top=119,;
    Alignment=1, Width=67, Height=17,;
    Caption="CPZ folder:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="LLCWVQYFHF",Visible=.t., Left=211, Top=12,;
    Alignment=1, Width=63, Height=17,;
    Caption="Permessi:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="WYTBFKFAPY",Visible=.t., Left=484, Top=33,;
    Alignment=2, Width=18, Height=17,;
    Caption="R"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="IDFHNAYBUQ",Visible=.t., Left=503, Top=33,;
    Alignment=2, Width=25, Height=17,;
    Caption="W"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="NRIQXVPREV",Visible=.t., Left=530, Top=33,;
    Alignment=2, Width=19, Height=17,;
    Caption="D"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="LEZZCSCRIJ",Visible=.t., Left=129, Top=32,;
    Alignment=0, Width=91, Height=17,;
    Caption="Fornitore"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES1$'F'))
    endwith
  endfunc
enddefine
define class tgsut2kxpPag2 as StdContainer
  Width  = 607
  height = 302
  stdWidth  = 607
  stdheight = 302
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPDES2_2_6 as StdCombo with uid="GTPOVJXSLF",rtseq=26,rtrep=.f.,left=5,top=31,width=108,height=21;
    , HelpContextID = 159184074;
    , cFormVar="w_TIPDES2",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES2_2_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES2_2_6.GetRadio()
    this.Parent.oContained.w_TIPDES2 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES2_2_6.SetRadio()
    this.Parent.oContained.w_TIPDES2=trim(this.Parent.oContained.w_TIPDES2)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES2=='C',1,;
      iif(this.Parent.oContained.w_TIPDES2=='F',2,;
      iif(this.Parent.oContained.w_TIPDES2=='A',3,;
      iif(this.Parent.oContained.w_TIPDES2=='G',4,;
      iif(this.Parent.oContained.w_TIPDES2=='X',5,;
      0)))))
  endfunc

  func oTIPDES2_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU2_2_8 as StdField with uid="QFMFZOBXQE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODGRU2", cQueryName = "CODGRU2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice gruppo inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 111849434,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=32, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU2"

  func oCODGRU2_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES2='G')
    endwith
   endif
  endfunc

  func oCODGRU2_2_8.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2<>'G')
    endwith
  endfunc

  func oCODGRU2_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
      if .not. empty(.w_CODROL2)
        bRes2=.link_2_14('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU2_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU2_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU2_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU2_2_9 as StdField with uid="UOBFCBKKIL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRU2", cQueryName = "DESGRU2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 111790538,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=32, InputMask=replicate('X',50)

  func oDESGRU2_2_9.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2<>'G')
    endwith
  endfunc

  add object oCODCON2_2_10 as StdField with uid="WWPQTPATOW",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODCON2", cQueryName = "CODCON2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 232697818,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON2", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON2"

  func oCODCON2_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES2$'CF')
    endwith
   endif
  endfunc

  func oCODCON2_2_10.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES2$'CF'))
    endwith
  endfunc

  func oCODCON2_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON2_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON2_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON2)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON2)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON2_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON2_2_11 as StdField with uid="TFNSZAFAXW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCON2", cQueryName = "DESCON2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 232638922,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=32, InputMask=replicate('X',40)

  func oDESCON2_2_11.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES2$'CF'))
    endwith
  endfunc

  add object oCODAGE2_2_12 as StdField with uid="RITQPMEKEB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODAGE2", cQueryName = "CODAGE2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 123776986,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE2"

  func oCODAGE2_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES2='A')
    endwith
   endif
  endfunc

  func oCODAGE2_2_12.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2<>'A')
    endwith
  endfunc

  func oCODAGE2_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE2_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE2_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE2_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE2_2_13 as StdField with uid="LPNYKSHYGQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESAGE2", cQueryName = "DESAGE2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 123718090,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=32, InputMask=replicate('X',35)

  func oDESAGE2_2_13.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2<>'A')
    endwith
  endfunc

  add object oREAD2_2_15 as StdCheck with uid="EYGYOQNSNY",rtseq=35,rtrep=.f.,left=506, top=34, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 229501162,;
    cFormVar="w_READ2", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD2_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD2_2_15.GetRadio()
    this.Parent.oContained.w_READ2 = this.RadioValue()
    return .t.
  endfunc

  func oREAD2_2_15.SetRadio()
    this.Parent.oContained.w_READ2=trim(this.Parent.oContained.w_READ2)
    this.value = ;
      iif(this.Parent.oContained.w_READ2=='S',1,;
      0)
  endfunc

  func oREAD2_2_15.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2='X')
    endwith
  endfunc

  add object oWRITE2_2_16 as StdCheck with uid="WDFFCUYBPB",rtseq=36,rtrep=.f.,left=544, top=34, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 174939034,;
    cFormVar="w_WRITE2", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE2_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE2_2_16.GetRadio()
    this.Parent.oContained.w_WRITE2 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE2_2_16.SetRadio()
    this.Parent.oContained.w_WRITE2=trim(this.Parent.oContained.w_WRITE2)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE2=='S',1,;
      0)
  endfunc

  func oWRITE2_2_16.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2='X')
    endwith
  endfunc

  add object oDELETE2_2_17 as StdCheck with uid="NYNUAIRVTB",rtseq=37,rtrep=.f.,left=581, top=34, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 109853130,;
    cFormVar="w_DELETE2", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE2_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE2_2_17.GetRadio()
    this.Parent.oContained.w_DELETE2 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE2_2_17.SetRadio()
    this.Parent.oContained.w_DELETE2=trim(this.Parent.oContained.w_DELETE2)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE2=='S',1,;
      0)
  endfunc

  func oDELETE2_2_17.mHide()
    with this.Parent.oContained
      return (.w_TIPDES2='X')
    endwith
  endfunc


  add object oTIPDES3_2_18 as StdCombo with uid="IIPGKIVCFO",rtseq=38,rtrep=.f.,left=5,top=57,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES3",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES3_2_18.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES3_2_18.GetRadio()
    this.Parent.oContained.w_TIPDES3 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES3_2_18.SetRadio()
    this.Parent.oContained.w_TIPDES3=trim(this.Parent.oContained.w_TIPDES3)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES3=='C',1,;
      iif(this.Parent.oContained.w_TIPDES3=='F',2,;
      iif(this.Parent.oContained.w_TIPDES3=='A',3,;
      iif(this.Parent.oContained.w_TIPDES3=='G',4,;
      iif(this.Parent.oContained.w_TIPDES3=='X',5,;
      0)))))
  endfunc

  func oTIPDES3_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU3_2_20 as StdField with uid="KFLDFFVZSK",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODGRU3", cQueryName = "CODGRU3",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice gruppo inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=57, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU3"

  func oCODGRU3_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES3='G')
    endwith
   endif
  endfunc

  func oCODGRU3_2_20.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3<>'G')
    endwith
  endfunc

  func oCODGRU3_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
      if .not. empty(.w_CODROL3)
        bRes2=.link_2_26('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU3_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU3_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU3_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU3_2_21 as StdField with uid="HUHCCOCLPW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESGRU3", cQueryName = "DESGRU3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=57, InputMask=replicate('X',50)

  func oDESGRU3_2_21.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3<>'G')
    endwith
  endfunc

  add object oCODCON3_2_22 as StdField with uid="MDXRZLIKPO",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CODCON3", cQueryName = "CODCON3",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=57, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON3", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON3"

  func oCODCON3_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES3$'CF')
    endwith
   endif
  endfunc

  func oCODCON3_2_22.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES3$'CF'))
    endwith
  endfunc

  func oCODCON3_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON3_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON3_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON3)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON3)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON3_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON3_2_23 as StdField with uid="JSUJUAEXHT",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCON3", cQueryName = "DESCON3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=57, InputMask=replicate('X',40)

  func oDESCON3_2_23.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES3$'CF'))
    endwith
  endfunc

  add object oCODAGE3_2_24 as StdField with uid="XTPZSXLFYO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODAGE3", cQueryName = "CODAGE3",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 123776986,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=57, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE3"

  func oCODAGE3_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES3='A')
    endwith
   endif
  endfunc

  func oCODAGE3_2_24.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3<>'A')
    endwith
  endfunc

  func oCODAGE3_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE3_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE3_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE3_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE3_2_25 as StdField with uid="YIDLJFGRDX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESAGE3", cQueryName = "DESAGE3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 123718090,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=57, InputMask=replicate('X',35)

  func oDESAGE3_2_25.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3<>'A')
    endwith
  endfunc

  add object oREAD3_2_27 as StdCheck with uid="LSKSRAGWAL",rtseq=47,rtrep=.f.,left=506, top=59, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 228452586,;
    cFormVar="w_READ3", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD3_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD3_2_27.GetRadio()
    this.Parent.oContained.w_READ3 = this.RadioValue()
    return .t.
  endfunc

  func oREAD3_2_27.SetRadio()
    this.Parent.oContained.w_READ3=trim(this.Parent.oContained.w_READ3)
    this.value = ;
      iif(this.Parent.oContained.w_READ3=='S',1,;
      0)
  endfunc

  func oREAD3_2_27.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3='X')
    endwith
  endfunc

  add object oWRITE3_2_28 as StdCheck with uid="MFBPIFRSZS",rtseq=48,rtrep=.f.,left=544, top=59, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 158161818,;
    cFormVar="w_WRITE3", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE3_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE3_2_28.GetRadio()
    this.Parent.oContained.w_WRITE3 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE3_2_28.SetRadio()
    this.Parent.oContained.w_WRITE3=trim(this.Parent.oContained.w_WRITE3)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE3=='S',1,;
      0)
  endfunc

  func oWRITE3_2_28.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3='X')
    endwith
  endfunc

  add object oDELETE3_2_29 as StdCheck with uid="IXICDVARJY",rtseq=49,rtrep=.f.,left=581, top=59, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 109853130,;
    cFormVar="w_DELETE3", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE3_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE3_2_29.GetRadio()
    this.Parent.oContained.w_DELETE3 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE3_2_29.SetRadio()
    this.Parent.oContained.w_DELETE3=trim(this.Parent.oContained.w_DELETE3)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE3=='S',1,;
      0)
  endfunc

  func oDELETE3_2_29.mHide()
    with this.Parent.oContained
      return (.w_TIPDES3='X')
    endwith
  endfunc


  add object oTIPDES4_2_30 as StdCombo with uid="DMEDRBCHFE",rtseq=50,rtrep=.f.,left=5,top=82,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES4",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES4_2_30.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES4_2_30.GetRadio()
    this.Parent.oContained.w_TIPDES4 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES4_2_30.SetRadio()
    this.Parent.oContained.w_TIPDES4=trim(this.Parent.oContained.w_TIPDES4)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES4=='C',1,;
      iif(this.Parent.oContained.w_TIPDES4=='F',2,;
      iif(this.Parent.oContained.w_TIPDES4=='A',3,;
      iif(this.Parent.oContained.w_TIPDES4=='G',4,;
      iif(this.Parent.oContained.w_TIPDES4=='X',5,;
      0)))))
  endfunc

  func oTIPDES4_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU4_2_31 as StdField with uid="TZLETJLGIV",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CODGRU4", cQueryName = "CODGRU4",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=82, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU4"

  func oCODGRU4_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES4='G')
    endwith
   endif
  endfunc

  func oCODGRU4_2_31.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4<>'G')
    endwith
  endfunc

  func oCODGRU4_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
      if .not. empty(.w_CODROL4)
        bRes2=.link_2_38('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU4_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU4_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU4_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU4_2_32 as StdField with uid="WACXDLXCQP",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESGRU4", cQueryName = "DESGRU4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=82, InputMask=replicate('X',50)

  func oDESGRU4_2_32.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4<>'G')
    endwith
  endfunc

  add object oCODCON4_2_34 as StdField with uid="VPFHQUHPCB",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODCON4", cQueryName = "CODCON4",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=82, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON4", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON4"

  func oCODCON4_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES4$'CF')
    endwith
   endif
  endfunc

  func oCODCON4_2_34.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES4$'CF'))
    endwith
  endfunc

  func oCODCON4_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON4_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON4_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON4)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON4)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON4_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON4_2_35 as StdField with uid="KEGGPZCMIY",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESCON4", cQueryName = "DESCON4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=82, InputMask=replicate('X',40)

  func oDESCON4_2_35.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES4$'CF'))
    endwith
  endfunc

  add object oCODAGE4_2_36 as StdField with uid="LEEPQRLVEJ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CODAGE4", cQueryName = "CODAGE4",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=82, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE4"

  func oCODAGE4_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES4='A')
    endwith
   endif
  endfunc

  func oCODAGE4_2_36.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4<>'A')
    endwith
  endfunc

  func oCODAGE4_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE4_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE4_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE4_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE4_2_37 as StdField with uid="UGNXIBMPYB",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESAGE4", cQueryName = "DESAGE4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=82, InputMask=replicate('X',35)

  func oDESAGE4_2_37.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4<>'A')
    endwith
  endfunc

  add object oREAD4_2_39 as StdCheck with uid="XLKDMHQXBQ",rtseq=59,rtrep=.f.,left=506, top=83, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 227404010,;
    cFormVar="w_READ4", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD4_2_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD4_2_39.GetRadio()
    this.Parent.oContained.w_READ4 = this.RadioValue()
    return .t.
  endfunc

  func oREAD4_2_39.SetRadio()
    this.Parent.oContained.w_READ4=trim(this.Parent.oContained.w_READ4)
    this.value = ;
      iif(this.Parent.oContained.w_READ4=='S',1,;
      0)
  endfunc

  func oREAD4_2_39.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4='X')
    endwith
  endfunc

  add object oWRITE4_2_40 as StdCheck with uid="EGOTVDWKKE",rtseq=60,rtrep=.f.,left=544, top=83, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 141384602,;
    cFormVar="w_WRITE4", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE4_2_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE4_2_40.GetRadio()
    this.Parent.oContained.w_WRITE4 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE4_2_40.SetRadio()
    this.Parent.oContained.w_WRITE4=trim(this.Parent.oContained.w_WRITE4)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE4=='S',1,;
      0)
  endfunc

  func oWRITE4_2_40.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4='X')
    endwith
  endfunc

  add object oDELETE4_2_41 as StdCheck with uid="ASTGMPPGQI",rtseq=61,rtrep=.f.,left=581, top=83, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE4", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE4_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE4_2_41.GetRadio()
    this.Parent.oContained.w_DELETE4 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE4_2_41.SetRadio()
    this.Parent.oContained.w_DELETE4=trim(this.Parent.oContained.w_DELETE4)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE4=='S',1,;
      0)
  endfunc

  func oDELETE4_2_41.mHide()
    with this.Parent.oContained
      return (.w_TIPDES4='X')
    endwith
  endfunc


  add object oTIPDES5_2_42 as StdCombo with uid="ONSSHBQDMP",rtseq=62,rtrep=.f.,left=5,top=107,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES5",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES5_2_42.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES5_2_42.GetRadio()
    this.Parent.oContained.w_TIPDES5 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES5_2_42.SetRadio()
    this.Parent.oContained.w_TIPDES5=trim(this.Parent.oContained.w_TIPDES5)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES5=='C',1,;
      iif(this.Parent.oContained.w_TIPDES5=='F',2,;
      iif(this.Parent.oContained.w_TIPDES5=='A',3,;
      iif(this.Parent.oContained.w_TIPDES5=='G',4,;
      iif(this.Parent.oContained.w_TIPDES5=='X',5,;
      0)))))
  endfunc

  func oTIPDES5_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU5_2_43 as StdField with uid="MKMIPGNBDX",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CODGRU5", cQueryName = "CODGRU5",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente o obsoleto",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=107, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU5"

  func oCODGRU5_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES5='G')
    endwith
   endif
  endfunc

  func oCODGRU5_2_43.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5<>'G')
    endwith
  endfunc

  func oCODGRU5_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_43('Part',this)
      if .not. empty(.w_CODROL5)
        bRes2=.link_2_50('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU5_2_43.ecpDrop(oSource)
    this.Parent.oContained.link_2_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU5_2_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU5_2_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU5_2_44 as StdField with uid="KJHYFEDBDY",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESGRU5", cQueryName = "DESGRU5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=107, InputMask=replicate('X',50)

  func oDESGRU5_2_44.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5<>'G')
    endwith
  endfunc

  add object oCODCON5_2_46 as StdField with uid="GJLCSKMXCC",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CODCON5", cQueryName = "CODCON5",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON5", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON5"

  func oCODCON5_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES5$'CF')
    endwith
   endif
  endfunc

  func oCODCON5_2_46.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES5$'CF'))
    endwith
  endfunc

  func oCODCON5_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON5_2_46.ecpDrop(oSource)
    this.Parent.oContained.link_2_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON5_2_46.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON5)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON5)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON5_2_46'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON5_2_47 as StdField with uid="PCXXDFOVWQ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESCON5", cQueryName = "DESCON5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=107, InputMask=replicate('X',40)

  func oDESCON5_2_47.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES5$'CF'))
    endwith
  endfunc

  add object oCODAGE5_2_48 as StdField with uid="DAWPKFIQIL",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CODAGE5", cQueryName = "CODAGE5",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE5"

  func oCODAGE5_2_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES5='A')
    endwith
   endif
  endfunc

  func oCODAGE5_2_48.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5<>'A')
    endwith
  endfunc

  func oCODAGE5_2_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE5_2_48.ecpDrop(oSource)
    this.Parent.oContained.link_2_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE5_2_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE5_2_48'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE5_2_49 as StdField with uid="PVMGEWVUXZ",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESAGE5", cQueryName = "DESAGE5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=107, InputMask=replicate('X',35)

  func oDESAGE5_2_49.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5<>'A')
    endwith
  endfunc

  add object oREAD5_2_51 as StdCheck with uid="LQJHLQLOPP",rtseq=71,rtrep=.f.,left=506, top=108, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 226355434,;
    cFormVar="w_READ5", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD5_2_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD5_2_51.GetRadio()
    this.Parent.oContained.w_READ5 = this.RadioValue()
    return .t.
  endfunc

  func oREAD5_2_51.SetRadio()
    this.Parent.oContained.w_READ5=trim(this.Parent.oContained.w_READ5)
    this.value = ;
      iif(this.Parent.oContained.w_READ5=='S',1,;
      0)
  endfunc

  func oREAD5_2_51.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5='X')
    endwith
  endfunc

  add object oWRITE5_2_52 as StdCheck with uid="XIJLAWBOEJ",rtseq=72,rtrep=.f.,left=544, top=108, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 124607386,;
    cFormVar="w_WRITE5", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE5_2_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE5_2_52.GetRadio()
    this.Parent.oContained.w_WRITE5 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE5_2_52.SetRadio()
    this.Parent.oContained.w_WRITE5=trim(this.Parent.oContained.w_WRITE5)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE5=='S',1,;
      0)
  endfunc

  func oWRITE5_2_52.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5='X')
    endwith
  endfunc

  add object oDELETE5_2_53 as StdCheck with uid="JIARUTQUCG",rtseq=73,rtrep=.f.,left=581, top=108, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE5", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE5_2_53.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE5_2_53.GetRadio()
    this.Parent.oContained.w_DELETE5 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE5_2_53.SetRadio()
    this.Parent.oContained.w_DELETE5=trim(this.Parent.oContained.w_DELETE5)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE5=='S',1,;
      0)
  endfunc

  func oDELETE5_2_53.mHide()
    with this.Parent.oContained
      return (.w_TIPDES5='X')
    endwith
  endfunc


  add object oTIPDES6_2_54 as StdCombo with uid="XRRQLCTMLU",rtseq=74,rtrep=.f.,left=5,top=132,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES6",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES6_2_54.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES6_2_54.GetRadio()
    this.Parent.oContained.w_TIPDES6 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES6_2_54.SetRadio()
    this.Parent.oContained.w_TIPDES6=trim(this.Parent.oContained.w_TIPDES6)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES6=='C',1,;
      iif(this.Parent.oContained.w_TIPDES6=='F',2,;
      iif(this.Parent.oContained.w_TIPDES6=='A',3,;
      iif(this.Parent.oContained.w_TIPDES6=='G',4,;
      iif(this.Parent.oContained.w_TIPDES6=='X',5,;
      0)))))
  endfunc

  func oTIPDES6_2_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU6_2_55 as StdField with uid="XIHNSGZKQI",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CODGRU6", cQueryName = "CODGRU6",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=132, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU6"

  func oCODGRU6_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES6='G')
    endwith
   endif
  endfunc

  func oCODGRU6_2_55.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6<>'G')
    endwith
  endfunc

  func oCODGRU6_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
      if .not. empty(.w_CODROL6)
        bRes2=.link_2_62('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU6_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU6_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU6_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU6_2_56 as StdField with uid="FBFARTYKHE",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESGRU6", cQueryName = "DESGRU6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=132, InputMask=replicate('X',50)

  func oDESGRU6_2_56.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6<>'G')
    endwith
  endfunc

  add object oCODCON6_2_58 as StdField with uid="VMNNYHROUJ",rtseq=78,rtrep=.f.,;
    cFormVar = "w_CODCON6", cQueryName = "CODCON6",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=132, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON6", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON6"

  func oCODCON6_2_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES6$'CF')
    endwith
   endif
  endfunc

  func oCODCON6_2_58.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES6$'CF'))
    endwith
  endfunc

  func oCODCON6_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON6_2_58.ecpDrop(oSource)
    this.Parent.oContained.link_2_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON6_2_58.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON6)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON6)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON6_2_58'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON6_2_59 as StdField with uid="XFGBDIAEWG",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESCON6", cQueryName = "DESCON6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=132, InputMask=replicate('X',40)

  func oDESCON6_2_59.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES6$'CF'))
    endwith
  endfunc

  add object oCODAGE6_2_60 as StdField with uid="GHMBDPBIMH",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CODAGE6", cQueryName = "CODAGE6",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE6"

  func oCODAGE6_2_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES6='A')
    endwith
   endif
  endfunc

  func oCODAGE6_2_60.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6<>'A')
    endwith
  endfunc

  func oCODAGE6_2_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE6_2_60.ecpDrop(oSource)
    this.Parent.oContained.link_2_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE6_2_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE6_2_60'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE6_2_61 as StdField with uid="CPKAGAQNRX",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESAGE6", cQueryName = "DESAGE6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=132, InputMask=replicate('X',35)

  func oDESAGE6_2_61.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6<>'A')
    endwith
  endfunc

  add object oREAD6_2_63 as StdCheck with uid="URRUFQXLIE",rtseq=83,rtrep=.f.,left=506, top=133, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 225306858,;
    cFormVar="w_READ6", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD6_2_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD6_2_63.GetRadio()
    this.Parent.oContained.w_READ6 = this.RadioValue()
    return .t.
  endfunc

  func oREAD6_2_63.SetRadio()
    this.Parent.oContained.w_READ6=trim(this.Parent.oContained.w_READ6)
    this.value = ;
      iif(this.Parent.oContained.w_READ6=='S',1,;
      0)
  endfunc

  func oREAD6_2_63.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6='X')
    endwith
  endfunc

  add object oWRITE6_2_64 as StdCheck with uid="LFWFJVUHYR",rtseq=84,rtrep=.f.,left=544, top=133, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 107830170,;
    cFormVar="w_WRITE6", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE6_2_64.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE6_2_64.GetRadio()
    this.Parent.oContained.w_WRITE6 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE6_2_64.SetRadio()
    this.Parent.oContained.w_WRITE6=trim(this.Parent.oContained.w_WRITE6)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE6=='S',1,;
      0)
  endfunc

  func oWRITE6_2_64.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6='X')
    endwith
  endfunc

  add object oDELETE6_2_65 as StdCheck with uid="LGCJGWMJXK",rtseq=85,rtrep=.f.,left=581, top=133, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE6", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE6_2_65.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE6_2_65.GetRadio()
    this.Parent.oContained.w_DELETE6 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE6_2_65.SetRadio()
    this.Parent.oContained.w_DELETE6=trim(this.Parent.oContained.w_DELETE6)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE6=='S',1,;
      0)
  endfunc

  func oDELETE6_2_65.mHide()
    with this.Parent.oContained
      return (.w_TIPDES6='X')
    endwith
  endfunc


  add object oTIPDES7_2_66 as StdCombo with uid="PBPPHYIYZP",rtseq=86,rtrep=.f.,left=5,top=157,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES7",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES7_2_66.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES7_2_66.GetRadio()
    this.Parent.oContained.w_TIPDES7 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES7_2_66.SetRadio()
    this.Parent.oContained.w_TIPDES7=trim(this.Parent.oContained.w_TIPDES7)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES7=='C',1,;
      iif(this.Parent.oContained.w_TIPDES7=='F',2,;
      iif(this.Parent.oContained.w_TIPDES7=='A',3,;
      iif(this.Parent.oContained.w_TIPDES7=='G',4,;
      iif(this.Parent.oContained.w_TIPDES7=='X',5,;
      0)))))
  endfunc

  func oTIPDES7_2_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU7_2_67 as StdField with uid="NOETFKBVBH",rtseq=87,rtrep=.f.,;
    cFormVar = "w_CODGRU7", cQueryName = "CODGRU7",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=157, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU7"

  func oCODGRU7_2_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES7='G')
    endwith
   endif
  endfunc

  func oCODGRU7_2_67.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7<>'G')
    endwith
  endfunc

  func oCODGRU7_2_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_67('Part',this)
      if .not. empty(.w_CODROL7)
        bRes2=.link_2_74('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU7_2_67.ecpDrop(oSource)
    this.Parent.oContained.link_2_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU7_2_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU7_2_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU7_2_68 as StdField with uid="HVBFPNVOCO",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESGRU7", cQueryName = "DESGRU7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=157, InputMask=replicate('X',50)

  func oDESGRU7_2_68.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7<>'G')
    endwith
  endfunc

  add object oCODCON7_2_70 as StdField with uid="UCVGCLOTKJ",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CODCON7", cQueryName = "CODCON7",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=157, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON7", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON7"

  func oCODCON7_2_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES7$'CF')
    endwith
   endif
  endfunc

  func oCODCON7_2_70.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES7$'CF'))
    endwith
  endfunc

  func oCODCON7_2_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON7_2_70.ecpDrop(oSource)
    this.Parent.oContained.link_2_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON7_2_70.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON7)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON7)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON7_2_70'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON7_2_71 as StdField with uid="WQXPZPFUTT",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESCON7", cQueryName = "DESCON7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=157, InputMask=replicate('X',40)

  func oDESCON7_2_71.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES7$'CF'))
    endwith
  endfunc

  add object oCODAGE7_2_72 as StdField with uid="GYYWKOWJSP",rtseq=92,rtrep=.f.,;
    cFormVar = "w_CODAGE7", cQueryName = "CODAGE7",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE7"

  func oCODAGE7_2_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES7='A')
    endwith
   endif
  endfunc

  func oCODAGE7_2_72.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7<>'A')
    endwith
  endfunc

  func oCODAGE7_2_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_72('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE7_2_72.ecpDrop(oSource)
    this.Parent.oContained.link_2_72('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE7_2_72.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE7_2_72'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE7_2_73 as StdField with uid="MGXWVYBPKK",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESAGE7", cQueryName = "DESAGE7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=157, InputMask=replicate('X',35)

  func oDESAGE7_2_73.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7<>'A')
    endwith
  endfunc

  add object oREAD7_2_75 as StdCheck with uid="RNILZFMEIZ",rtseq=95,rtrep=.f.,left=506, top=157, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 224258282,;
    cFormVar="w_READ7", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD7_2_75.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD7_2_75.GetRadio()
    this.Parent.oContained.w_READ7 = this.RadioValue()
    return .t.
  endfunc

  func oREAD7_2_75.SetRadio()
    this.Parent.oContained.w_READ7=trim(this.Parent.oContained.w_READ7)
    this.value = ;
      iif(this.Parent.oContained.w_READ7=='S',1,;
      0)
  endfunc

  func oREAD7_2_75.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7='X')
    endwith
  endfunc

  add object oWRITE7_2_76 as StdCheck with uid="RDFXSKHIDP",rtseq=96,rtrep=.f.,left=544, top=157, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 91052954,;
    cFormVar="w_WRITE7", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE7_2_76.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE7_2_76.GetRadio()
    this.Parent.oContained.w_WRITE7 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE7_2_76.SetRadio()
    this.Parent.oContained.w_WRITE7=trim(this.Parent.oContained.w_WRITE7)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE7=='S',1,;
      0)
  endfunc

  func oWRITE7_2_76.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7='X')
    endwith
  endfunc

  add object oDELETE7_2_77 as StdCheck with uid="AOSVORNUDP",rtseq=97,rtrep=.f.,left=581, top=157, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE7", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE7_2_77.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE7_2_77.GetRadio()
    this.Parent.oContained.w_DELETE7 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE7_2_77.SetRadio()
    this.Parent.oContained.w_DELETE7=trim(this.Parent.oContained.w_DELETE7)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE7=='S',1,;
      0)
  endfunc

  func oDELETE7_2_77.mHide()
    with this.Parent.oContained
      return (.w_TIPDES7='X')
    endwith
  endfunc


  add object oTIPDES8_2_78 as StdCombo with uid="CSAGSMFDBB",rtseq=98,rtrep=.f.,left=5,top=182,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES8",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES8_2_78.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES8_2_78.GetRadio()
    this.Parent.oContained.w_TIPDES8 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES8_2_78.SetRadio()
    this.Parent.oContained.w_TIPDES8=trim(this.Parent.oContained.w_TIPDES8)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES8=='C',1,;
      iif(this.Parent.oContained.w_TIPDES8=='F',2,;
      iif(this.Parent.oContained.w_TIPDES8=='A',3,;
      iif(this.Parent.oContained.w_TIPDES8=='G',4,;
      iif(this.Parent.oContained.w_TIPDES8=='X',5,;
      0)))))
  endfunc

  func oTIPDES8_2_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU8_2_79 as StdField with uid="KZBKOQMLES",rtseq=99,rtrep=.f.,;
    cFormVar = "w_CODGRU8", cQueryName = "CODGRU8",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=182, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU8"

  func oCODGRU8_2_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES8='G')
    endwith
   endif
  endfunc

  func oCODGRU8_2_79.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8<>'G')
    endwith
  endfunc

  func oCODGRU8_2_79.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_79('Part',this)
      if .not. empty(.w_CODROL8)
        bRes2=.link_2_86('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU8_2_79.ecpDrop(oSource)
    this.Parent.oContained.link_2_79('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU8_2_79.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU8_2_79'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU8_2_80 as StdField with uid="IHQKJDQDSK",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESGRU8", cQueryName = "DESGRU8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=182, InputMask=replicate('X',50)

  func oDESGRU8_2_80.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8<>'G')
    endwith
  endfunc

  add object oCODCON8_2_82 as StdField with uid="UJBXCXEFVV",rtseq=102,rtrep=.f.,;
    cFormVar = "w_CODCON8", cQueryName = "CODCON8",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=182, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON8", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON8"

  func oCODCON8_2_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES8$'CF')
    endwith
   endif
  endfunc

  func oCODCON8_2_82.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES8$'CF'))
    endwith
  endfunc

  func oCODCON8_2_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_82('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON8_2_82.ecpDrop(oSource)
    this.Parent.oContained.link_2_82('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON8_2_82.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON8)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON8)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON8_2_82'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON8_2_83 as StdField with uid="AREOLEJMIH",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESCON8", cQueryName = "DESCON8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=182, InputMask=replicate('X',40)

  func oDESCON8_2_83.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES8$'CF'))
    endwith
  endfunc

  add object oCODAGE8_2_84 as StdField with uid="DUOWQVZSUH",rtseq=104,rtrep=.f.,;
    cFormVar = "w_CODAGE8", cQueryName = "CODAGE8",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE8"

  func oCODAGE8_2_84.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES8='A')
    endwith
   endif
  endfunc

  func oCODAGE8_2_84.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8<>'A')
    endwith
  endfunc

  func oCODAGE8_2_84.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_84('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE8_2_84.ecpDrop(oSource)
    this.Parent.oContained.link_2_84('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE8_2_84.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE8_2_84'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE8_2_85 as StdField with uid="HIPUBLAHZL",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESAGE8", cQueryName = "DESAGE8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=182, InputMask=replicate('X',35)

  func oDESAGE8_2_85.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8<>'A')
    endwith
  endfunc

  add object oREAD8_2_87 as StdCheck with uid="METAEADANQ",rtseq=107,rtrep=.f.,left=506, top=184, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 223209706,;
    cFormVar="w_READ8", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD8_2_87.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD8_2_87.GetRadio()
    this.Parent.oContained.w_READ8 = this.RadioValue()
    return .t.
  endfunc

  func oREAD8_2_87.SetRadio()
    this.Parent.oContained.w_READ8=trim(this.Parent.oContained.w_READ8)
    this.value = ;
      iif(this.Parent.oContained.w_READ8=='S',1,;
      0)
  endfunc

  func oREAD8_2_87.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8='X')
    endwith
  endfunc

  add object oWRITE8_2_88 as StdCheck with uid="LWJXTHGVNS",rtseq=108,rtrep=.f.,left=544, top=184, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 74275738,;
    cFormVar="w_WRITE8", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE8_2_88.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE8_2_88.GetRadio()
    this.Parent.oContained.w_WRITE8 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE8_2_88.SetRadio()
    this.Parent.oContained.w_WRITE8=trim(this.Parent.oContained.w_WRITE8)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE8=='S',1,;
      0)
  endfunc

  func oWRITE8_2_88.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8='X')
    endwith
  endfunc

  add object oDELETE8_2_89 as StdCheck with uid="GXACNYAJFF",rtseq=109,rtrep=.f.,left=581, top=184, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE8", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE8_2_89.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE8_2_89.GetRadio()
    this.Parent.oContained.w_DELETE8 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE8_2_89.SetRadio()
    this.Parent.oContained.w_DELETE8=trim(this.Parent.oContained.w_DELETE8)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE8=='S',1,;
      0)
  endfunc

  func oDELETE8_2_89.mHide()
    with this.Parent.oContained
      return (.w_TIPDES8='X')
    endwith
  endfunc


  add object oTIPDES9_2_90 as StdCombo with uid="DCKXXKTXFL",rtseq=110,rtrep=.f.,left=5,top=206,width=108,height=21;
    , HelpContextID = 109251382;
    , cFormVar="w_TIPDES9",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES9_2_90.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES9_2_90.GetRadio()
    this.Parent.oContained.w_TIPDES9 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES9_2_90.SetRadio()
    this.Parent.oContained.w_TIPDES9=trim(this.Parent.oContained.w_TIPDES9)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES9=='C',1,;
      iif(this.Parent.oContained.w_TIPDES9=='F',2,;
      iif(this.Parent.oContained.w_TIPDES9=='A',3,;
      iif(this.Parent.oContained.w_TIPDES9=='G',4,;
      iif(this.Parent.oContained.w_TIPDES9=='X',5,;
      0)))))
  endfunc

  func oTIPDES9_2_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU9_2_91 as StdField with uid="CZDHATGOQD",rtseq=111,rtrep=.f.,;
    cFormVar = "w_CODGRU9", cQueryName = "CODGRU9",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 156586022,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=207, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU9"

  func oCODGRU9_2_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES9='G')
    endwith
   endif
  endfunc

  func oCODGRU9_2_91.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9<>'G')
    endwith
  endfunc

  func oCODGRU9_2_91.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_91('Part',this)
      if .not. empty(.w_CODROL9)
        bRes2=.link_2_97('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU9_2_91.ecpDrop(oSource)
    this.Parent.oContained.link_2_91('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU9_2_91.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU9_2_91'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU9_2_92 as StdField with uid="GTFRIKIICE",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESGRU9", cQueryName = "DESGRU9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 156644918,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=207, InputMask=replicate('X',50)

  func oDESGRU9_2_92.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9<>'G')
    endwith
  endfunc

  add object oCODCON9_2_94 as StdField with uid="FAKIBKRZNO",rtseq=114,rtrep=.f.,;
    cFormVar = "w_CODCON9", cQueryName = "CODCON9",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 35737638,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=207, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON9", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON9"

  func oCODCON9_2_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES9$'CF')
    endwith
   endif
  endfunc

  func oCODCON9_2_94.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES9$'CF'))
    endwith
  endfunc

  func oCODCON9_2_94.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_94('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON9_2_94.ecpDrop(oSource)
    this.Parent.oContained.link_2_94('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON9_2_94.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON9)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON9)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON9_2_94'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON9_2_95 as StdField with uid="TEWYWUAQKK",rtseq=115,rtrep=.f.,;
    cFormVar = "w_DESCON9", cQueryName = "DESCON9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 35796534,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=207, InputMask=replicate('X',40)

  func oDESCON9_2_95.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES9$'CF'))
    endwith
  endfunc

  add object oDESAGE9_2_96 as StdField with uid="MDPOZHOXJN",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DESAGE9", cQueryName = "DESAGE9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 144717366,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=207, InputMask=replicate('X',35)

  func oDESAGE9_2_96.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9<>'A')
    endwith
  endfunc

  add object oREAD9_2_98 as StdCheck with uid="RQVSJJLETL",rtseq=118,rtrep=.f.,left=506, top=207, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 222161130,;
    cFormVar="w_READ9", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD9_2_98.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD9_2_98.GetRadio()
    this.Parent.oContained.w_READ9 = this.RadioValue()
    return .t.
  endfunc

  func oREAD9_2_98.SetRadio()
    this.Parent.oContained.w_READ9=trim(this.Parent.oContained.w_READ9)
    this.value = ;
      iif(this.Parent.oContained.w_READ9=='S',1,;
      0)
  endfunc

  func oREAD9_2_98.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9='X')
    endwith
  endfunc

  add object oWRITE9_2_99 as StdCheck with uid="YZZFZUUIMH",rtseq=119,rtrep=.f.,left=544, top=207, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 57498522,;
    cFormVar="w_WRITE9", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE9_2_99.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE9_2_99.GetRadio()
    this.Parent.oContained.w_WRITE9 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE9_2_99.SetRadio()
    this.Parent.oContained.w_WRITE9=trim(this.Parent.oContained.w_WRITE9)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE9=='S',1,;
      0)
  endfunc

  func oWRITE9_2_99.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9='X')
    endwith
  endfunc

  add object oDELETE9_2_100 as StdCheck with uid="ZYEZHVWLMP",rtseq=120,rtrep=.f.,left=581, top=207, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 158582326,;
    cFormVar="w_DELETE9", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE9_2_100.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE9_2_100.GetRadio()
    this.Parent.oContained.w_DELETE9 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE9_2_100.SetRadio()
    this.Parent.oContained.w_DELETE9=trim(this.Parent.oContained.w_DELETE9)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE9=='S',1,;
      0)
  endfunc

  func oDELETE9_2_100.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9='X')
    endwith
  endfunc


  add object oTIPDES10_2_101 as StdCombo with uid="BETBYKZUNI",rtseq=121,rtrep=.f.,left=5,top=230,width=108,height=21;
    , HelpContextID = 159184026;
    , cFormVar="w_TIPDES10",RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo,"+"Nessuno", bObbl = .f. , nPag = 2;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPDES10_2_101.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oTIPDES10_2_101.GetRadio()
    this.Parent.oContained.w_TIPDES10 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDES10_2_101.SetRadio()
    this.Parent.oContained.w_TIPDES10=trim(this.Parent.oContained.w_TIPDES10)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDES10=='C',1,;
      iif(this.Parent.oContained.w_TIPDES10=='F',2,;
      iif(this.Parent.oContained.w_TIPDES10=='A',3,;
      iif(this.Parent.oContained.w_TIPDES10=='G',4,;
      iif(this.Parent.oContained.w_TIPDES10=='X',5,;
      0)))))
  endfunc

  func oTIPDES10_2_101.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RISERVATO<>'N')
    endwith
   endif
  endfunc

  add object oCODGRU10_2_102 as StdField with uid="SAYIXMHFBJ",rtseq=122,rtrep=.f.,;
    cFormVar = "w_CODGRU10", cQueryName = "CODGRU10",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non valido",;
    ToolTipText = "Codice gruppo...",;
    HelpContextID = 111849386,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=231, cSayPict='"@Z 999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_CODGRU10"

  func oCODGRU10_2_102.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES10='G')
    endwith
   endif
  endfunc

  func oCODGRU10_2_102.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10<>'G')
    endwith
  endfunc

  func oCODGRU10_2_102.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_102('Part',this)
      if .not. empty(.w_CODROL10)
        bRes2=.link_2_109('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODGRU10_2_102.ecpDrop(oSource)
    this.Parent.oContained.link_2_102('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU10_2_102.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oCODGRU10_2_102'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRU10_2_103 as StdField with uid="YRHNMHDYDO",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DESGRU10", cQueryName = "DESGRU10",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 111790490,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=231, InputMask=replicate('X',50)

  func oDESGRU10_2_103.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10<>'G')
    endwith
  endfunc

  add object oCODCON10_2_105 as StdField with uid="XHDERQJFHD",rtseq=125,rtrep=.f.,;
    cFormVar = "w_CODCON10", cQueryName = "CODCON10",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 232697770,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=231, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON10", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON10"

  func oCODCON10_2_105.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES10$'CF')
    endwith
   endif
  endfunc

  func oCODCON10_2_105.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES10$'CF'))
    endwith
  endfunc

  func oCODCON10_2_105.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_105('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON10_2_105.ecpDrop(oSource)
    this.Parent.oContained.link_2_105('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON10_2_105.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON10)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON10)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON10_2_105'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCON10_2_106 as StdField with uid="SEKVVNJRCE",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESCON10", cQueryName = "DESCON10",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cli/for",;
    HelpContextID = 232638874,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=231, InputMask=replicate('X',40)

  func oDESCON10_2_106.mHide()
    with this.Parent.oContained
      return (Not(.w_TIPDES10$'CF'))
    endwith
  endfunc

  add object oCODAGE10_2_107 as StdField with uid="SWOIXECVIR",rtseq=127,rtrep=.f.,;
    cFormVar = "w_CODAGE10", cQueryName = "CODAGE10",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 123776938,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE10"

  func oCODAGE10_2_107.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES10='A')
    endwith
   endif
  endfunc

  func oCODAGE10_2_107.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10<>'A')
    endwith
  endfunc

  func oCODAGE10_2_107.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_107('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE10_2_107.ecpDrop(oSource)
    this.Parent.oContained.link_2_107('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE10_2_107.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE10_2_107'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDESAGE10_2_108 as StdField with uid="MLJUEEQQHX",rtseq=128,rtrep=.f.,;
    cFormVar = "w_DESAGE10", cQueryName = "DESAGE10",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 123718042,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=233, Left=267, Top=231, InputMask=replicate('X',35)

  func oDESAGE10_2_108.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10<>'A')
    endwith
  endfunc

  add object oREAD10_2_110 as StdCheck with uid="HGHWXDTJGJ",rtseq=130,rtrep=.f.,left=506, top=231, caption="",;
    ToolTipText = "Diritti lettura...",;
    HelpContextID = 230549738,;
    cFormVar="w_READ10", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oREAD10_2_110.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oREAD10_2_110.GetRadio()
    this.Parent.oContained.w_READ10 = this.RadioValue()
    return .t.
  endfunc

  func oREAD10_2_110.SetRadio()
    this.Parent.oContained.w_READ10=trim(this.Parent.oContained.w_READ10)
    this.value = ;
      iif(this.Parent.oContained.w_READ10=='S',1,;
      0)
  endfunc

  func oREAD10_2_110.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10='X')
    endwith
  endfunc

  add object oWRITE10_2_111 as StdCheck with uid="TQMFFTLCDX",rtseq=131,rtrep=.f.,left=544, top=231, caption="",;
    ToolTipText = "Diritti scrittura...",;
    HelpContextID = 191716250,;
    cFormVar="w_WRITE10", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oWRITE10_2_111.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oWRITE10_2_111.GetRadio()
    this.Parent.oContained.w_WRITE10 = this.RadioValue()
    return .t.
  endfunc

  func oWRITE10_2_111.SetRadio()
    this.Parent.oContained.w_WRITE10=trim(this.Parent.oContained.w_WRITE10)
    this.value = ;
      iif(this.Parent.oContained.w_WRITE10=='S',1,;
      0)
  endfunc

  func oWRITE10_2_111.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10='X')
    endwith
  endfunc

  add object oDELETE10_2_112 as StdCheck with uid="QQZRUDCXFT",rtseq=132,rtrep=.f.,left=581, top=231, caption="",;
    ToolTipText = "Diritti cancellazione...",;
    HelpContextID = 109853082,;
    cFormVar="w_DELETE10", bObbl = .f. , nPag = 2;
   , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDELETE10_2_112.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDELETE10_2_112.GetRadio()
    this.Parent.oContained.w_DELETE10 = this.RadioValue()
    return .t.
  endfunc

  func oDELETE10_2_112.SetRadio()
    this.Parent.oContained.w_DELETE10=trim(this.Parent.oContained.w_DELETE10)
    this.value = ;
      iif(this.Parent.oContained.w_DELETE10=='S',1,;
      0)
  endfunc

  func oDELETE10_2_112.mHide()
    with this.Parent.oContained
      return (.w_TIPDES10='X')
    endwith
  endfunc


  add object oBtn_2_113 as StdButton with uid="VLFIFYGIUW",left=500, top=257, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare...";
    , HelpContextID = 250227942;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_113.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_113.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not(Empty(.w_ZCPFTITLE)) and (Not(Empty(.w_ZCPPFOLDER)) or .w_ZCPTFOLDER='C'))
      endwith
    endif
  endfunc

  func oBtn_2_113.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not(g_APPLICATION='ADHOC REVOLUTION'))
     endwith
    endif
  endfunc


  add object oBtn_2_114 as StdButton with uid="DFYKSHAABW",left=554, top=257, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 257516614;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_114.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_114.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not(g_APPLICATION='ADHOC REVOLUTION'))
     endwith
    endif
  endfunc

  add object oCODAGE9_2_125 as StdField with uid="CFTGALGMRE",rtseq=143,rtrep=.f.,;
    cFormVar = "w_CODAGE9", cQueryName = "CODAGE9",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    HelpContextID = 144658470,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=147, Left=117, Top=207, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE9"

  func oCODAGE9_2_125.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDES9='A')
    endwith
   endif
  endfunc

  func oCODAGE9_2_125.mHide()
    with this.Parent.oContained
      return (.w_TIPDES9<>'A')
    endwith
  endfunc

  func oCODAGE9_2_125.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_125('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE9_2_125.ecpDrop(oSource)
    this.Parent.oContained.link_2_125('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE9_2_125.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE9_2_125'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oStr_2_2 as StdString with uid="NIGZNSODZI",Visible=.t., Left=10, Top=12,;
    Alignment=0, Width=24, Height=17,;
    Caption="Tipo"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_3 as StdString with uid="JZGIWJFWUS",Visible=.t., Left=122, Top=12,;
    Alignment=0, Width=57, Height=17,;
    Caption="Destinatario"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="JBBPNACLOP",Visible=.t., Left=504, Top=12,;
    Alignment=2, Width=18, Height=17,;
    Caption="R"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="JSNYDIGROR",Visible=.t., Left=578, Top=12,;
    Alignment=2, Width=19, Height=17,;
    Caption="D"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_115 as StdString with uid="NPCSNTCNNQ",Visible=.t., Left=537, Top=12,;
    Alignment=2, Width=25, Height=17,;
    Caption="W"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_1 as StdBox with uid="KHABXTTDFL",left=4, top=8, width=594,height=22
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut2kxp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
