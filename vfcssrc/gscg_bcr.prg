* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcr                                                        *
*              Maschera reg. IVA                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_45]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2000-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcr",oParentObject,m.pTipOpe)
return(i_retval)

define class tgscg_bcr as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_APPO = 0
  w_DATA = ctod("  /  /  ")
  w_APPO1 = space(10)
  w_DATAF = ctod("  /  /  ")
  w_APPO2 = space(4)
  w_MESE = 0
  * --- WorkFile variables
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il periodo successivo all'ultima denuncia iva stampata Normale (da GSCG_SRI)
    * --- Calcola Prossimo Periodo/Date Inizio e Fine
    if this.pTipOpe="R"
      * --- Se Campio Tipo o Numero Registro Legge Attivita' Associata al Registro, Ultima Stampa e Periodo
      this.oParentObject.w_CODATT = SPACE(5)
      this.oParentObject.w_ULTDAT = cp_CharToDate("  -  -  ")
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
            +" where ATTIPREG="+cp_ToStrODBC(this.oParentObject.w_TIPREG)+" AND ATNUMREG="+cp_ToStrODBC(this.oParentObject.w_NUMREG)+"";
             ,"_Curs_ATTIDETT")
      else
        select * from (i_cTable);
         where ATTIPREG=this.oParentObject.w_TIPREG AND ATNUMREG=this.oParentObject.w_NUMREG;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        * --- Il codice attivit� � in corrispondenza uno a uno con il numero registri
        this.oParentObject.w_ULTDAT = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
        this.oParentObject.w_CODATT = NVL(_Curs_ATTIDETT.ATCODATT, SPACE(5))
        this.oParentObject.w_INTLIG = NVL(_Curs_ATTIDETT.ATSTAINT, SPACE(1))
        this.oParentObject.w_PREFIS = NVL(_Curs_ATTIDETT.ATPREFIS, SPACE(20))
        this.oParentObject.w_PRPARI = NVL(_Curs_ATTIDETT.ATPRPARI, 0)
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      this.w_DATA = IIF(EMPTY(this.oParentObject.w_ULTDAT), i_DATSYS, this.oParentObject.w_ULTDAT+1)
      this.w_MESE = IIF(EMPTY(this.oParentObject.w_ULTDAT), 1, MONTH(this.oParentObject.w_ULTDAT+1))
      this.oParentObject.w_ANNO = ALLTRIM(STR(YEAR(this.w_DATA),4,0))
      * --- Se ultima stampa registro riguarda l'anno precedente, azzero il numero di pagine
      if VAL(this.oParentObject.w_ANNO) > YEAR(this.oParentObject.w_ULTDAT) AND NOT EMPTY(this.oParentObject.w_ULTDAT)
        this.oParentObject.w_PRPARI = 0
      endif
      * --- Numero Periodo (Mensile o Trimestrale)
      this.oParentObject.w_NUMPER = IIF(g_TIPDEN="M", this.w_MESE, INT((this.w_MESE+2)/3))
      * --- Calcola le date di Inizio e Fine Periodo di Default
      this.oParentObject.w_DATINI = IIF(EMPTY(this.oParentObject.w_ULTDAT), cp_CharToDate("  -  -  "), this.oParentObject.w_ULTDAT + 1)
      if VAL(this.oParentObject.w_ANNO)>1900 AND this.oParentObject.w_NUMPER>0
        if (this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M")
          if EMPTY(this.oParentObject.w_DATINI)
            this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
            this.w_APPO2 = this.oParentObject.w_ANNO
            this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
            this.oParentObject.w_DATINI = cp_CharToDate(this.w_APPO1)
          endif
          this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
          this.w_APPO2 = this.oParentObject.w_ANNO
          if this.w_APPO>12
            this.w_APPO = 1
            this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
          endif
          this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
          this.oParentObject.w_DATFIN = cp_CharToDate(this.w_APPO1)-1
          this.oParentObject.w_FINPER = this.oParentObject.w_DATFIN
        endif
      endif
    else
      if EMPTY(this.oParentObject.w_CODATT)
        * --- Il Registro non e' associato ad alcuna Attivita'
        * --- Non da messaggi x che' porebbe dipendere sia da Tipreg che da Numreg, comunque verra' ricontrollato durante l'elaborazione
        ah_ErrorMsg("Il registro selezionato non � associato ad alcuna attivit�",,"")
      endif
    endif
    if this.pTipOpe="P" AND VAL(this.oParentObject.w_ANNO)>1900 AND this.oParentObject.w_NUMPER>0
      * --- Cambio Anno o Periodo, Ricalcola le Date inizio/Fine
      if (this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M")
        this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
        this.w_APPO2 = this.oParentObject.w_ANNO
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.oParentObject.w_DATINI = cp_CharToDate(this.w_APPO1)
        this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
        this.w_APPO2 = this.oParentObject.w_ANNO
        if this.w_APPO>12
          this.w_APPO = 1
          this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
        endif
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.oParentObject.w_DATFIN = cp_CharToDate(this.w_APPO1)-1
        this.oParentObject.w_FINPER = this.oParentObject.w_DATFIN
      endif
      if this.oParentObject.w_ULTDAT>=this.oParentObject.w_DATINI AND this.oParentObject.w_ULTDAT<this.oParentObject.w_DATFIN AND NOT EMPTY(this.oParentObject.w_ULTDAT)
        * --- Se Ultima Data Stampa Compresa nel Periodo, Riconsidera la Data Iniziale
        this.oParentObject.w_DATINI = this.oParentObject.w_ULTDAT + 1
      endif
    endif
    if this.pTipOpe $ "IF" AND VAL(this.oParentObject.w_ANNO)>1900 AND this.oParentObject.w_NUMPER>0
      * --- Cambio Data Iniziale/Finale
      if (this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M")
        this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
        this.w_APPO2 = this.oParentObject.w_ANNO
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.w_DATA = cp_CharToDate(this.w_APPO1)
        this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
        this.w_APPO2 = this.oParentObject.w_ANNO
        if this.w_APPO>12
          this.w_APPO = 1
          this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
        endif
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.w_DATAF = cp_CharToDate(this.w_APPO1)-1
        this.oParentObject.w_FINPER = this.w_DATAF
        if this.pTipOpe="I" AND (this.oParentObject.w_DATINI<this.w_DATA OR this.oParentObject.w_DATINI>this.w_DATAF)
          ah_ErrorMsg("Data iniziale fuori dei limiti del periodo",,"")
          this.oParentObject.w_DATINI = this.w_DATA
        endif
        if this.pTipOpe="F" AND (this.oParentObject.w_DATFIN<this.w_DATA OR this.oParentObject.w_DATFIN>this.w_DATAF)
          ah_ErrorMsg("Data iniziale fuori dei limiti del periodo",,"")
          this.oParentObject.w_DATFIN = this.w_DATAF
        endif
        if this.oParentObject.w_DATINI>this.oParentObject.w_DATFIN
          if this.pTipOpe="I"
            this.oParentObject.w_DATFIN = this.w_DATAF
          else
            this.oParentObject.w_DATINI = this.w_DATA
          endif
        endif
      endif
    endif
    if this.pTipOpe="Z"
      this.oParentObject.w_FLSTOT = this.oParentObject.w_FLDEFI
    endif
    this.oParentObject.w_FLSTOT = IIF(this.oParentObject.w_DATFIN=this.oParentObject.w_FINPER, this.oParentObject.w_FLSTOT, " ")
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIDETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
