* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bmc                                                        *
*              Check sui mastri                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-18                                                      *
* Last revis.: 2009-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPCON,w_CODCON
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bmc",oParentObject,m.w_TIPCON,m.w_CODCON)
return(i_retval)

define class tgsar1bmc as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_RIGHEANALITICA = 0
  * --- WorkFile variables
  COLLCENB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa routine viene lanciata da altre due routine la Gscg_bps (Sincronizza piano dei conti di analitica e contabilit�)
    *     e la Gsar_bmc (Inserimento automatico - Lanciato solo se l'applicazione � 
    *     AdHoc Enterprise)
    * --- Il controllo che se esista almeno un record sulla tabella Collcenb deve
    *     essere effettuato solo nel caso in cui la routine sia lanciata dall'anagrafica
    *     dei conti
    if UPPER (this.oparentobject.oparentobject.class)="TGSAR_API"
      this.w_RIGHEANALITICA = this.oparentobject.oparentobject.GSAR_mrb.NumRow()
    else
      this.w_RIGHEANALITICA = 0
    endif
    if this.w_RIGHEANALITICA=0
      * --- Insert into COLLCENB
      i_nConn=i_TableProp[this.COLLCENB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COLLCENB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENB_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MRTIPCON"+",MRCODICE"+",MRCODBUN"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'COLLCENB','MRTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENB','MRCODICE');
        +","+cp_NullLink(cp_ToStrODBC("   "),'COLLCENB','MRCODBUN');
        +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENB','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',this.w_TIPCON,'MRCODICE',this.w_CODCON,'MRCODBUN',"   ",'CPROWNUM',1)
        insert into (i_cTable) (MRTIPCON,MRCODICE,MRCODBUN,CPROWNUM &i_ccchkf. );
           values (;
             this.w_TIPCON;
             ,this.w_CODCON;
             ,"   ";
             ,1;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_TIPCON,w_CODCON)
    this.w_TIPCON=w_TIPCON
    this.w_CODCON=w_CODCON
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COLLCENB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPCON,w_CODCON"
endproc
