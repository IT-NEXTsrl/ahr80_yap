* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mir                                                        *
*              Imballi resi                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-12                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mir")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mir")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mir")
  return

* --- Class definition
define class tgsar_mir as StdPCForm
  Width  = 731
  Height = 259
  Top    = 91
  Left   = 142
  cComment = "Imballi resi"
  cPrg = "gsar_mir"
  HelpContextID=264861545
  add object cnt as tcgsar_mir
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mir as PCContext
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVDESART = space(40)
  w_MVQTAUM1 = 0
  w_MVUNIMIS = space(3)
  w_COSSTA = 0
  w_MVSERIAL = space(10)
  w_OBTEST = space(8)
  w_OBJ_GEST = space(10)
  w_IMBREN = 0
  w_MVCODART = space(20)
  w_TEST = space(1)
  w_CODART = space(20)
  w_CODESC = space(5)
  w_COSSTA1 = 0
  w_KITIMB = space(1)
  w_TOTCAU = 0
  proc Save(i_oFrom)
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MVCODICE = i_oFrom.w_MVCODICE
    this.w_MVDESART = i_oFrom.w_MVDESART
    this.w_MVQTAUM1 = i_oFrom.w_MVQTAUM1
    this.w_MVUNIMIS = i_oFrom.w_MVUNIMIS
    this.w_COSSTA = i_oFrom.w_COSSTA
    this.w_MVSERIAL = i_oFrom.w_MVSERIAL
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_OBJ_GEST = i_oFrom.w_OBJ_GEST
    this.w_IMBREN = i_oFrom.w_IMBREN
    this.w_MVCODART = i_oFrom.w_MVCODART
    this.w_TEST = i_oFrom.w_TEST
    this.w_CODART = i_oFrom.w_CODART
    this.w_CODESC = i_oFrom.w_CODESC
    this.w_COSSTA1 = i_oFrom.w_COSSTA1
    this.w_KITIMB = i_oFrom.w_KITIMB
    this.w_TOTCAU = i_oFrom.w_TOTCAU
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MVCODICE = this.w_MVCODICE
    i_oTo.w_MVDESART = this.w_MVDESART
    i_oTo.w_MVQTAUM1 = this.w_MVQTAUM1
    i_oTo.w_MVUNIMIS = this.w_MVUNIMIS
    i_oTo.w_COSSTA = this.w_COSSTA
    i_oTo.w_MVSERIAL = this.w_MVSERIAL
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_OBJ_GEST = this.w_OBJ_GEST
    i_oTo.w_IMBREN = this.w_IMBREN
    i_oTo.w_MVCODART = this.w_MVCODART
    i_oTo.w_TEST = this.w_TEST
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_CODESC = this.w_CODESC
    i_oTo.w_COSSTA1 = this.w_COSSTA1
    i_oTo.w_KITIMB = this.w_KITIMB
    i_oTo.w_TOTCAU = this.w_TOTCAU
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mir as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 731
  Height = 259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=264861545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DOC_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  cFile = "DOC_DETT"
  cKeySelect = "MVSERIAL"
  cKeyWhere  = "MVSERIAL=this.w_MVSERIAL"
  cKeyDetail  = "MVSERIAL=this.w_MVSERIAL"
  cKeyWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cKeyDetailWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DOC_DETT.MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DOC_DETT.CPROWORD '
  cPrg = "gsar_mir"
  cComment = "Imballi resi"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVDESART = space(40)
  w_MVQTAUM1 = 0
  w_MVUNIMIS = space(3)
  w_COSSTA = 0
  w_MVSERIAL = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_OBJ_GEST = space(10)
  w_IMBREN = 0
  w_MVCODART = space(20)
  w_TEST = .F.
  w_CODART = space(20)
  w_CODESC = space(5)
  w_COSSTA1 = 0
  w_KITIMB = space(1)
  w_TOTCAU = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mirPag1","gsar_mir",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='DOC_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DOC_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DOC_DETT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mir'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DOC_DETT where MVSERIAL=KeySet.MVSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2],this.bLoadRecFilter,this.DOC_DETT_IDX,"gsar_mir")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DOC_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DOC_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DOC_DETT '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MVSERIAL',this.w_MVSERIAL  )
      select * from (i_cTable) DOC_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_IMBREN = 1
        .w_TOTCAU = 0
        .w_MVSERIAL = NVL(MVSERIAL,space(10))
        .w_OBJ_GEST = this.oParentObject
        .w_CODESC = .w_OBJ_GEST .w_CODESC
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DOC_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTCAU = 0
      scan
        with this
          .w_COSSTA = 0
        .w_TEST = .T.
          .w_CODART = space(20)
          .w_KITIMB = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MVCODICE = NVL(MVCODICE,space(20))
          if link_2_2_joined
            this.w_MVCODICE = NVL(CACODICE202,NVL(this.w_MVCODICE,space(20)))
            this.w_MVDESART = NVL(CADESART202,space(40))
            this.w_MVCODART = NVL(CACODART202,space(20))
            this.w_CODART = NVL(CACODART202,space(20))
          else
          .link_2_2('Load')
          endif
          .w_MVDESART = NVL(MVDESART,space(40))
          .w_MVQTAUM1 = NVL(MVQTAUM1,0)
          .w_MVUNIMIS = NVL(MVUNIMIS,space(3))
          .w_MVCODART = NVL(MVCODART,space(20))
          if link_2_7_joined
            this.w_MVCODART = NVL(ARCODART207,NVL(this.w_MVCODART,space(20)))
            this.w_MVUNIMIS = NVL(ARUNMIS1207,space(3))
            this.w_KITIMB = NVL(ARKITIMB207,space(1))
          else
          .link_2_7('Load')
          endif
          .link_2_9('Load')
        .w_COSSTA1 = IIF(.w_KITIMB='R',.w_COSSTA*.w_MVQTAUM1,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTCAU = .w_TOTCAU+.w_COSSTA1
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBJ_GEST = this.oParentObject
        .w_CODESC = .w_OBJ_GEST .w_CODESC
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_mir
    * -- In interrroga e modifica elimino dal transitorio le righe che riguardano
    * -- articoli kit e prodotti finiti. Devo vedere solo imballi a rendere
    If g_VEFA = 'S' And this.cFunction <>'Load'
       this.NotifyEvent("SoloImballi")
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CPROWORD=10
      .w_MVCODICE=space(20)
      .w_MVDESART=space(40)
      .w_MVQTAUM1=0
      .w_MVUNIMIS=space(3)
      .w_COSSTA=0
      .w_MVSERIAL=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_OBJ_GEST=space(10)
      .w_IMBREN=0
      .w_MVCODART=space(20)
      .w_TEST=.f.
      .w_CODART=space(20)
      .w_CODESC=space(5)
      .w_COSSTA1=0
      .w_KITIMB=space(1)
      .w_TOTCAU=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_MVCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        .w_OBTEST = i_DATSYS
        .w_OBJ_GEST = this.oParentObject
        .w_IMBREN = 1
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MVCODART))
         .link_2_7('Full')
        endif
        .w_TEST = .T.
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODART))
         .link_2_9('Full')
        endif
        .w_CODESC = .w_OBJ_GEST .w_CODESC
        .w_COSSTA1 = IIF(.w_KITIMB='R',.w_COSSTA*.w_MVQTAUM1,0)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DOC_DETT')
    this.DoRTCalc(16,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DOC_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVSERIAL,"MVSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MVCODICE C(20);
      ,t_MVDESART C(40);
      ,t_MVQTAUM1 N(12,3);
      ,t_COSSTA N(18,5);
      ,CPROWNUM N(10);
      ,t_MVUNIMIS C(3);
      ,t_MVCODART C(20);
      ,t_TEST L(1);
      ,t_CODART C(20);
      ,t_COSSTA1 N(18,5);
      ,t_KITIMB C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mirbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.controlsource=this.cTrsName+'.t_MVCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVDESART_2_3.controlsource=this.cTrsName+'.t_MVDESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAUM1_2_4.controlsource=this.cTrsName+'.t_MVQTAUM1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOSSTA_2_6.controlsource=this.cTrsName+'.t_COSSTA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(214)
    this.AddVLine(462)
    this.AddVLine(561)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
      *
      * insert into DOC_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DOC_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'DOC_DETT')
        i_cFldBody=" "+;
                  "(CPROWORD,MVCODICE,MVDESART,MVQTAUM1,MVUNIMIS"+;
                  ",MVSERIAL,MVCODART,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MVCODICE)+","+cp_ToStrODBC(this.w_MVDESART)+","+cp_ToStrODBC(this.w_MVQTAUM1)+","+cp_ToStrODBC(this.w_MVUNIMIS)+;
             ","+cp_ToStrODBC(this.w_MVSERIAL)+","+cp_ToStrODBCNull(this.w_MVCODART)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DOC_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'DOC_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MVSERIAL',this.w_MVSERIAL)
        INSERT INTO (i_cTable) (;
                   CPROWORD;
                  ,MVCODICE;
                  ,MVDESART;
                  ,MVQTAUM1;
                  ,MVUNIMIS;
                  ,MVSERIAL;
                  ,MVCODART;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPROWORD;
                  ,this.w_MVCODICE;
                  ,this.w_MVDESART;
                  ,this.w_MVQTAUM1;
                  ,this.w_MVUNIMIS;
                  ,this.w_MVSERIAL;
                  ,this.w_MVCODART;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsar_mir
    * Elusione check riga piena
    * Anche per i cancellati
        LOCAL l_delete
        L_delete=SET('delete')
         set delete off
        Update (this.cTrsName) Set t_TEST=.f.
         set delete &l_delete
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_MVCODICE) And t_MVQTAUM1<>0 And t_TEST) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_MVCODICE) And t_MVQTAUM1<>0 And t_TEST) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DOC_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DOC_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MVCODICE="+cp_ToStrODBCNull(this.w_MVCODICE)+;
                     ",MVDESART="+cp_ToStrODBC(this.w_MVDESART)+;
                     ",MVQTAUM1="+cp_ToStrODBC(this.w_MVQTAUM1)+;
                     ",MVUNIMIS="+cp_ToStrODBC(this.w_MVUNIMIS)+;
                     ",MVCODART="+cp_ToStrODBCNull(this.w_MVCODART)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DOC_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MVCODICE=this.w_MVCODICE;
                     ,MVDESART=this.w_MVDESART;
                     ,MVQTAUM1=this.w_MVQTAUM1;
                     ,MVUNIMIS=this.w_MVUNIMIS;
                     ,MVCODART=this.w_MVCODART;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsar_mir
    * Elusione check riga piena
        L_delete=SET('delete')
         set delete off
        Update (this.cTrsName) Set t_TEST=.t.
         set delete &l_delete
     * --- Necessario per allineare variabili di work dopo la scan del
     * --- temporaneo eseguita nella GSAR_MIR.mReplace
     this.WorkFromTrs()
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_MVCODICE) And t_MVQTAUM1<>0 And t_TEST) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DOC_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_MVCODICE) And t_MVQTAUM1<>0 And t_TEST) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
          .w_OBJ_GEST = this.oParentObject
        .DoRTCalc(10,10,.t.)
          .link_2_7('Full')
        .DoRTCalc(12,12,.t.)
          .link_2_9('Full')
          .w_CODESC = .w_OBJ_GEST .w_CODESC
          .w_TOTCAU = .w_TOTCAU-.w_cossta1
          .w_COSSTA1 = IIF(.w_KITIMB='R',.w_COSSTA*.w_MVQTAUM1,0)
          .w_TOTCAU = .w_TOTCAU+.w_cossta1
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MVUNIMIS with this.w_MVUNIMIS
      replace t_MVCODART with this.w_MVCODART
      replace t_TEST with this.w_TEST
      replace t_CODART with this.w_CODART
      replace t_COSSTA1 with this.w_COSSTA1
      replace t_KITIMB with this.w_KITIMB
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_mir
    * -Per eludere la cancellazione che altrimenti andrebbe a eliminare i documenti
    * -di esplosione componenti
    
    IF Upper(CEVENT)='DELETE START'
    * Elusione check riga piena
    * Anche per i cancellati
        LOCAL l_delete
        L_delete=SET('delete')
         set delete off
        Update (this.cTrsName) Set t_TEST=.f.
         set delete &l_delete
    ENDIF
    IF Upper(CEVENT)='DELETE END'
    * Elusione check riga piena
        L_delete=SET('delete')
         set delete off
        Update (this.cTrsName) Set t_TEST=.t.
         set delete &l_delete
     * --- Necessario per allineare variabili di work dopo la scan del
     * --- temporaneo eseguita nella GSAR_MIR.mReplace
     this.WorkFromTrs()
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MVCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MVCODICE))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMVCODICE_2_2'),i_cWhere,'GSMA_BZA',"Imballi a rendere",'GSVA_MKI.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MVCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MVCODICE)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_MVDESART = NVL(_Link_.CADESART,space(40))
      this.w_MVCODART = NVL(_Link_.CACODART,space(20))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODICE = space(20)
      endif
      this.w_MVDESART = space(40)
      this.w_MVCODART = space(20)
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Nvl(LookTab('ART_ICOL','ARKITIMB','ARCODART',.w_MVCODART),'N')='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo non di tipo imballo a rendere")
        endif
        this.w_MVCODICE = space(20)
        this.w_MVDESART = space(40)
        this.w_MVCODART = space(20)
        this.w_CODART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CACODART as CACODART202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DOC_DETT.MVCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DOC_DETT.MVCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCODART
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARKITIMB";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARUNMIS1,ARKITIMB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_MVUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_KITIMB = NVL(_Link_.ARKITIMB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_MVUNIMIS = space(3)
      this.w_KITIMB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.ARCODART as ARCODART207"+ ",link_2_7.ARUNMIS1 as ARUNMIS1207"+ ",link_2_7.ARKITIMB as ARKITIMB207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on DOC_DETT.MVCODART=link_2_7.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and DOC_DETT.MVCODART=link_2_7.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRCOSSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART)
            select PRCODART,PRCOSSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.PRCODART,space(20))
      this.w_COSSTA = NVL(_Link_.PRCOSSTA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_COSSTA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTCAU_3_1.value==this.w_TOTCAU)
      this.oPgFrm.Page1.oPag.oTOTCAU_3_1.value=this.w_TOTCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value==this.w_MVCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value=this.w_MVCODICE
      replace t_MVCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVDESART_2_3.value==this.w_MVDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVDESART_2_3.value=this.w_MVDESART
      replace t_MVDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVDESART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAUM1_2_4.value==this.w_MVQTAUM1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAUM1_2_4.value=this.w_MVQTAUM1
      replace t_MVQTAUM1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAUM1_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSSTA_2_6.value==this.w_COSSTA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSSTA_2_6.value=this.w_COSSTA
      replace t_COSSTA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSSTA_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'DOC_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(Nvl(LookTab('ART_ICOL','ARKITIMB','ARCODART',.w_MVCODART),'N')='R') and not(empty(.w_MVCODICE)) and (Not Empty(.w_MVCODICE) And .w_MVQTAUM1<>0 And .w_TEST)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Articolo non di tipo imballo a rendere")
        case   empty(.w_MVQTAUM1) and (Not Empty(.w_MVCODICE) And .w_MVQTAUM1<>0 And .w_TEST)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVQTAUM1_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if Not Empty(.w_MVCODICE) And .w_MVQTAUM1<>0 And .w_TEST
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_MVCODICE) And t_MVQTAUM1<>0 And t_TEST)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MVCODICE=space(20)
      .w_MVDESART=space(40)
      .w_MVQTAUM1=0
      .w_MVUNIMIS=space(3)
      .w_COSSTA=0
      .w_MVCODART=space(20)
      .w_TEST=.f.
      .w_CODART=space(20)
      .w_COSSTA1=0
      .w_KITIMB=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_MVCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(3,11,.f.)
      if not(empty(.w_MVCODART))
        .link_2_7('Full')
      endif
        .w_TEST = .T.
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_CODART))
        .link_2_9('Full')
      endif
      .DoRTCalc(14,14,.f.)
        .w_COSSTA1 = IIF(.w_KITIMB='R',.w_COSSTA*.w_MVQTAUM1,0)
    endwith
    this.DoRTCalc(16,17,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MVCODICE = t_MVCODICE
    this.w_MVDESART = t_MVDESART
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_COSSTA = t_COSSTA
    this.w_MVCODART = t_MVCODART
    this.w_TEST = t_TEST
    this.w_CODART = t_CODART
    this.w_COSSTA1 = t_COSSTA1
    this.w_KITIMB = t_KITIMB
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MVCODICE with this.w_MVCODICE
    replace t_MVDESART with this.w_MVDESART
    replace t_MVQTAUM1 with this.w_MVQTAUM1
    replace t_MVUNIMIS with this.w_MVUNIMIS
    replace t_COSSTA with this.w_COSSTA
    replace t_MVCODART with this.w_MVCODART
    replace t_TEST with this.w_TEST
    replace t_CODART with this.w_CODART
    replace t_COSSTA1 with this.w_COSSTA1
    replace t_KITIMB with this.w_KITIMB
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTCAU = .w_TOTCAU-.w_cossta1
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mirPag1 as StdContainer
  Width  = 727
  height = 259
  stdWidth  = 727
  stdheight = 259
  resizeXpos=434
  resizeYpos=206
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_6 as cp_runprogram with uid="PDJFSCDCKO",left=1, top=265, width=163,height=27,;
    caption='GSVA_BSI',;
   bGlobalFont=.t.,;
    prg="GSVA_BSI",;
    cEvent = "SoloImballi",;
    nPag=1;
    , HelpContextID = 141377711


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=14, width=707,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="MVCODICE",Label2="Imballo",Field3="MVDESART",Label3="Descrizione",Field4="MVQTAUM1",Label4="Quantit�",Field5="COSSTA",Label5="Costo standard",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51229306

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=33,;
    width=703+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=34,width=702+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMVCODICE_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTCAU_3_1 as StdField with uid="DKPMKALQFS",rtseq=17,rtrep=.f.,;
    cFormVar="w_TOTCAU",value=0,enabled=.f.,;
    ToolTipText = "Totale cauzione: sommatoria costo standard degli imballi a rendere",;
    HelpContextID = 108061386,;
    cQueryName = "TOTCAU",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=563, Top=229, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  add object oStr_3_2 as StdString with uid="GSWUWFUSUQ",Visible=.t., Left=420, Top=230,;
    Alignment=1, Width=140, Height=18,;
    Caption="Cauzione totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsar_mirBodyRow as CPBodyRowCnt
  Width=693
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="NPGSHZIBAZ",rtseq=1,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 83557738,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0

  add object oMVCODICE_2_2 as StdTrsField with uid="SAYMFEGLVK",rtseq=2,rtrep=.t.,;
    cFormVar="w_MVCODICE",value=space(20),;
    ToolTipText = "Imballo reso",;
    HelpContextID = 231347211,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Articolo non di tipo imballo a rendere",;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=49, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MVCODICE"

  func oMVCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMVCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Imballi a rendere",'GSVA_MKI.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMVCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MVCODICE
    i_obj.ecpSave()
  endproc

  add object oMVDESART_2_3 as StdTrsField with uid="XFWABLHNZU",rtseq=3,rtrep=.t.,;
    cFormVar="w_MVDESART",value=space(40),;
    HelpContextID = 156228582,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=244, Left=204, Top=0, InputMask=replicate('X',40)

  add object oMVQTAUM1_2_4 as StdTrsField with uid="IPXMYWYVVH",rtseq=4,rtrep=.t.,;
    cFormVar="w_MVQTAUM1",value=0,;
    HelpContextID = 106957833,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=96, Left=452, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)]

  add object oCOSSTA_2_6 as StdTrsField with uid="BKWKPLKNYW",rtseq=6,rtrep=.t.,;
    cFormVar="w_COSSTA",value=0,enabled=.f.,;
    ToolTipText = "Costo standard in valuta di conto",;
    HelpContextID = 154203098,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=551, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mir','DOC_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MVSERIAL=DOC_DETT.MVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_mir
* --- Ridefinisco la classe per poter ridefinire il metodo ShowChildernChain
define class tgsar_mir as StdPCForm
  Width  = 731
  Height = 259
  Top    = 91
  Left   = 142
  cComment = "Imballi resi"
  HelpContextID=264861545
  add object cnt as tcgsar_mir
  Proc Init()
   dodefault()
   this.cComment=ah_Msgformat("Imballi resi")

   * Ridefinita la funzione remmando il SetFocus poich� questo figlio integrato
   * viene aperto ma non visualizzato al salvataggio dei documenti per calcolare le
   * cauzioni e generare il documento. Alla SetFocus veniva visualizzato lo stesso.
   Func TerminateEdit()
    local bRes,ac
    *ac=this.activeControl
    this.bOkToTerminate=.f.
    this.__dummy__.enabled=.t.
    * Solo se premo effettivamente il bottone all'Esc esegue questa set focus
    If this.cnt.w_OBJ_GEST.w_SHOWMMT='S'
      this.__dummy__.SetFocus()
    Endif
    return(this.bOkToTerminate)
   endfunc
  enddefine


* --- Fine Area Manuale
