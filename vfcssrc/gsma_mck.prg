* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mck                                                        *
*              Dettagli confezioni P.L.                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_140]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mck")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mck")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mck")
  return

* --- Class definition
define class tgsma_mck as StdPCForm
  Width  = 704
  Height = 177
  Top    = 10
  Left   = 10
  cComment = "Dettagli confezioni P.L."
  cPrg = "gsma_mck"
  HelpContextID=171279511
  add object cnt as tcgsma_mck
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mck as PCContext
  w_PCSERIAL = space(10)
  w_PCNUMCOL = 0
  w_TIPCOL = space(5)
  w_DATDOC = space(8)
  w_OBTEST = space(8)
  w_CPROWNUM = 0
  w_PCQTACON = 0
  w_PCCODCON = space(3)
  w_PCDESCON = space(30)
  w_PCVOLUME = 0
  w_PCUMVOLU = space(3)
  w_PCPESNET = 0
  w_PCPESLOR = 0
  w_CPROWORD = 0
  w_PCCODICE = space(20)
  w_CODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_PCUNIMIS = space(3)
  w_PCQTAART = 0
  w_PCDESART = space(40)
  w_FLFRAZ = space(1)
  w_FLSERG = space(1)
  proc Save(i_oFrom)
    this.w_PCSERIAL = i_oFrom.w_PCSERIAL
    this.w_PCNUMCOL = i_oFrom.w_PCNUMCOL
    this.w_TIPCOL = i_oFrom.w_TIPCOL
    this.w_DATDOC = i_oFrom.w_DATDOC
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_PCQTACON = i_oFrom.w_PCQTACON
    this.w_PCCODCON = i_oFrom.w_PCCODCON
    this.w_PCDESCON = i_oFrom.w_PCDESCON
    this.w_PCVOLUME = i_oFrom.w_PCVOLUME
    this.w_PCUMVOLU = i_oFrom.w_PCUMVOLU
    this.w_PCPESNET = i_oFrom.w_PCPESNET
    this.w_PCPESLOR = i_oFrom.w_PCPESLOR
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PCCODICE = i_oFrom.w_PCCODICE
    this.w_CODART = i_oFrom.w_CODART
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_PCUNIMIS = i_oFrom.w_PCUNIMIS
    this.w_PCQTAART = i_oFrom.w_PCQTAART
    this.w_PCDESART = i_oFrom.w_PCDESART
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_FLSERG = i_oFrom.w_FLSERG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PCSERIAL = this.w_PCSERIAL
    i_oTo.w_PCNUMCOL = this.w_PCNUMCOL
    i_oTo.w_TIPCOL = this.w_TIPCOL
    i_oTo.w_DATDOC = this.w_DATDOC
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_PCQTACON = this.w_PCQTACON
    i_oTo.w_PCCODCON = this.w_PCCODCON
    i_oTo.w_PCDESCON = this.w_PCDESCON
    i_oTo.w_PCVOLUME = this.w_PCVOLUME
    i_oTo.w_PCUMVOLU = this.w_PCUMVOLU
    i_oTo.w_PCPESNET = this.w_PCPESNET
    i_oTo.w_PCPESLOR = this.w_PCPESLOR
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PCCODICE = this.w_PCCODICE
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_PCUNIMIS = this.w_PCUNIMIS
    i_oTo.w_PCQTAART = this.w_PCQTAART
    i_oTo.w_PCDESART = this.w_PCDESART
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mck as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 704
  Height = 177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=171279511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAC_CONF_IDX = 0
  CON_COLL_IDX = 0
  UNIMIS_IDX = 0
  KEY_ARTI_IDX = 0
  PAC_DETT_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "PAC_CONF"
  cKeySelect = "PCSERIAL,PCNUMCOL"
  cKeyWhere  = "PCSERIAL=this.w_PCSERIAL and PCNUMCOL=this.w_PCNUMCOL"
  cKeyDetail  = "PCSERIAL=this.w_PCSERIAL and PCNUMCOL=this.w_PCNUMCOL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"PCSERIAL="+cp_ToStrODBC(this.w_PCSERIAL)';
      +'+" and PCNUMCOL="+cp_ToStrODBC(this.w_PCNUMCOL)';

  cKeyDetailWhereODBC = '"PCSERIAL="+cp_ToStrODBC(this.w_PCSERIAL)';
      +'+" and PCNUMCOL="+cp_ToStrODBC(this.w_PCNUMCOL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PAC_CONF.PCSERIAL="+cp_ToStrODBC(this.w_PCSERIAL)';
      +'+" and PAC_CONF.PCNUMCOL="+cp_ToStrODBC(this.w_PCNUMCOL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAC_CONF.CPROWORD '
  cPrg = "gsma_mck"
  cComment = "Dettagli confezioni P.L."
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PCSERIAL = space(10)
  w_PCNUMCOL = 0
  w_TIPCOL = space(5)
  w_DATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CPROWNUM = 0
  w_PCQTACON = 0
  w_PCCODCON = space(3)
  w_PCDESCON = space(30)
  w_PCVOLUME = 0
  w_PCUMVOLU = space(3)
  w_PCPESNET = 0
  w_PCPESLOR = 0
  w_CPROWORD = 0
  w_PCCODICE = space(20)
  o_PCCODICE = space(20)
  w_CODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_PCUNIMIS = space(3)
  w_PCQTAART = 0
  w_PCDESART = space(40)
  w_FLFRAZ = space(1)
  w_FLSERG = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_mckPag1","gsma_mck",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CON_COLL'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='PAC_DETT'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='PAC_CONF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAC_CONF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAC_CONF_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsma_mck'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAC_CONF where PCSERIAL=KeySet.PCSERIAL
    *                            and PCNUMCOL=KeySet.PCNUMCOL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2],this.bLoadRecFilter,this.PAC_CONF_IDX,"gsma_mck")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAC_CONF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAC_CONF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAC_CONF '
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PCSERIAL',this.w_PCSERIAL  ,'PCNUMCOL',this.w_PCNUMCOL  )
      select * from (i_cTable) PAC_CONF where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PCSERIAL = NVL(PCSERIAL,space(10))
        .w_PCNUMCOL = NVL(PCNUMCOL,0)
        .w_TIPCOL = this.oParentObject .w_PLTIPCOL
        .w_DATDOC = this.oParentObject .w_PLDATDOC
        .w_OBTEST = IIF(EMPTY(.w_DATDOC), i_DATSYS, .w_DATDOC)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PAC_CONF')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CODART = space(20)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_FLFRAZ = space(1)
        .w_FLSERG = 'N'
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_PCQTACON = NVL(PCQTACON,0)
          .w_PCCODCON = NVL(PCCODCON,space(3))
          * evitabile
          *.link_2_3('Load')
          .w_PCDESCON = NVL(PCDESCON,space(30))
          .w_PCVOLUME = NVL(PCVOLUME,0)
          .w_PCUMVOLU = NVL(PCUMVOLU,space(3))
          * evitabile
          *.link_2_6('Load')
          .w_PCPESNET = NVL(PCPESNET,0)
          .w_PCPESLOR = NVL(PCPESLOR,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PCCODICE = NVL(PCCODICE,space(20))
          if link_2_10_joined
            this.w_PCCODICE = NVL(CACODICE210,NVL(this.w_PCCODICE,space(20)))
            this.w_PCDESART = NVL(CADESART210,space(40))
            this.w_CODART = NVL(CACODART210,space(20))
            this.w_MOLTI3 = NVL(CAMOLTIP210,0)
            this.w_UNMIS3 = NVL(CAUNIMIS210,space(3))
          else
          .link_2_10('Load')
          endif
          .link_2_11('Load')
          .w_PCUNIMIS = NVL(PCUNIMIS,space(3))
          if link_2_16_joined
            this.w_PCUNIMIS = NVL(UMCODICE216,NVL(this.w_PCUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ216,space(1))
          else
          .link_2_16('Load')
          endif
          .w_PCQTAART = NVL(PCQTAART,0)
          .w_PCDESART = NVL(PCDESART,space(40))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TIPCOL = this.oParentObject .w_PLTIPCOL
        .w_DATDOC = this.oParentObject .w_PLDATDOC
        .w_OBTEST = IIF(EMPTY(.w_DATDOC), i_DATSYS, .w_DATDOC)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PCSERIAL=space(10)
      .w_PCNUMCOL=0
      .w_TIPCOL=space(5)
      .w_DATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CPROWNUM=0
      .w_PCQTACON=0
      .w_PCCODCON=space(3)
      .w_PCDESCON=space(30)
      .w_PCVOLUME=0
      .w_PCUMVOLU=space(3)
      .w_PCPESNET=0
      .w_PCPESLOR=0
      .w_CPROWORD=10
      .w_PCCODICE=space(20)
      .w_CODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_PCUNIMIS=space(3)
      .w_PCQTAART=0
      .w_PCDESART=space(40)
      .w_FLFRAZ=space(1)
      .w_FLSERG=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_TIPCOL = this.oParentObject .w_PLTIPCOL
        .w_DATDOC = this.oParentObject .w_PLDATDOC
        .w_OBTEST = IIF(EMPTY(.w_DATDOC), i_DATSYS, .w_DATDOC)
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_PCCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_PCUMVOLU))
         .link_2_6('Full')
        endif
        .DoRTCalc(12,15,.f.)
        if not(empty(.w_PCCODICE))
         .link_2_10('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODART))
         .link_2_11('Full')
        endif
        .DoRTCalc(17,20,.f.)
        .w_PCUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_PCUNIMIS))
         .link_2_16('Full')
        endif
        .DoRTCalc(22,24,.f.)
        .w_FLSERG = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAC_CONF')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPCCODICE_2_10.enabled = i_bVal
      .Page1.oPag.oPCUNIMIS_2_16.enabled = i_bVal
      .Page1.oPag.oPCQTAART_2_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAC_CONF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSERIAL,"PCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCNUMCOL,"PCNUMCOL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PCQTACON N(6);
      ,t_PCCODCON C(3);
      ,t_PCDESCON C(30);
      ,t_PCVOLUME N(10,2);
      ,t_PCUMVOLU C(3);
      ,t_PCPESNET N(9,3);
      ,t_PCPESLOR N(9,3);
      ,t_CPROWORD N(6);
      ,t_PCCODICE C(20);
      ,t_PCUNIMIS C(3);
      ,t_PCQTAART N(12,3);
      ,t_PCDESART C(40);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(3);
      ,t_CODART C(20);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_FLFRAZ C(1);
      ,t_FLSERG C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mckbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTACON_2_2.controlsource=this.cTrsName+'.t_PCQTACON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODCON_2_3.controlsource=this.cTrsName+'.t_PCCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCDESCON_2_4.controlsource=this.cTrsName+'.t_PCDESCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCVOLUME_2_5.controlsource=this.cTrsName+'.t_PCVOLUME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCUMVOLU_2_6.controlsource=this.cTrsName+'.t_PCUMVOLU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESNET_2_7.controlsource=this.cTrsName+'.t_PCPESNET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESLOR_2_8.controlsource=this.cTrsName+'.t_PCPESLOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oPCCODICE_2_10.controlsource=this.cTrsName+'.t_PCCODICE'
    this.oPgFRm.Page1.oPag.oPCUNIMIS_2_16.controlsource=this.cTrsName+'.t_PCUNIMIS'
    this.oPgFRm.Page1.oPag.oPCQTAART_2_17.controlsource=this.cTrsName+'.t_PCQTAART'
    this.oPgFRm.Page1.oPag.oPCDESART_2_18.controlsource=this.cTrsName+'.t_PCDESART'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(116)
    this.AddVLine(341)
    this.AddVLine(421)
    this.AddVLine(465)
    this.AddVLine(542)
    this.AddVLine(619)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTACON_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2])
      *
      * insert into PAC_CONF
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAC_CONF')
        i_extval=cp_InsertValODBCExtFlds(this,'PAC_CONF')
        i_cFldBody=" "+;
                  "(PCSERIAL,PCNUMCOL,PCQTACON,PCCODCON,PCDESCON"+;
                  ",PCVOLUME,PCUMVOLU,PCPESNET,PCPESLOR,CPROWORD"+;
                  ",PCCODICE,PCUNIMIS,PCQTAART,PCDESART,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PCSERIAL)+","+cp_ToStrODBC(this.w_PCNUMCOL)+","+cp_ToStrODBC(this.w_PCQTACON)+","+cp_ToStrODBCNull(this.w_PCCODCON)+","+cp_ToStrODBC(this.w_PCDESCON)+;
             ","+cp_ToStrODBC(this.w_PCVOLUME)+","+cp_ToStrODBCNull(this.w_PCUMVOLU)+","+cp_ToStrODBC(this.w_PCPESNET)+","+cp_ToStrODBC(this.w_PCPESLOR)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBCNull(this.w_PCCODICE)+","+cp_ToStrODBCNull(this.w_PCUNIMIS)+","+cp_ToStrODBC(this.w_PCQTAART)+","+cp_ToStrODBC(this.w_PCDESART)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAC_CONF')
        i_extval=cp_InsertValVFPExtFlds(this,'PAC_CONF')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PCSERIAL',this.w_PCSERIAL,'PCNUMCOL',this.w_PCNUMCOL,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   PCSERIAL;
                  ,PCNUMCOL;
                  ,PCQTACON;
                  ,PCCODCON;
                  ,PCDESCON;
                  ,PCVOLUME;
                  ,PCUMVOLU;
                  ,PCPESNET;
                  ,PCPESLOR;
                  ,CPROWORD;
                  ,PCCODICE;
                  ,PCUNIMIS;
                  ,PCQTAART;
                  ,PCDESART;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PCSERIAL;
                  ,this.w_PCNUMCOL;
                  ,this.w_PCQTACON;
                  ,this.w_PCCODCON;
                  ,this.w_PCDESCON;
                  ,this.w_PCVOLUME;
                  ,this.w_PCUMVOLU;
                  ,this.w_PCPESNET;
                  ,this.w_PCPESLOR;
                  ,this.w_CPROWORD;
                  ,this.w_PCCODICE;
                  ,this.w_PCUNIMIS;
                  ,this.w_PCQTAART;
                  ,this.w_PCDESART;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PCCODCON)) AND t_PCQTACON<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAC_CONF')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAC_CONF')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PCCODCON)) AND t_PCQTACON<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAC_CONF
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAC_CONF')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PCQTACON="+cp_ToStrODBC(this.w_PCQTACON)+;
                     ",PCCODCON="+cp_ToStrODBCNull(this.w_PCCODCON)+;
                     ",PCDESCON="+cp_ToStrODBC(this.w_PCDESCON)+;
                     ",PCVOLUME="+cp_ToStrODBC(this.w_PCVOLUME)+;
                     ",PCUMVOLU="+cp_ToStrODBCNull(this.w_PCUMVOLU)+;
                     ",PCPESNET="+cp_ToStrODBC(this.w_PCPESNET)+;
                     ",PCPESLOR="+cp_ToStrODBC(this.w_PCPESLOR)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PCCODICE="+cp_ToStrODBCNull(this.w_PCCODICE)+;
                     ",PCUNIMIS="+cp_ToStrODBCNull(this.w_PCUNIMIS)+;
                     ",PCQTAART="+cp_ToStrODBC(this.w_PCQTAART)+;
                     ",PCDESART="+cp_ToStrODBC(this.w_PCDESART)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAC_CONF')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PCQTACON=this.w_PCQTACON;
                     ,PCCODCON=this.w_PCCODCON;
                     ,PCDESCON=this.w_PCDESCON;
                     ,PCVOLUME=this.w_PCVOLUME;
                     ,PCUMVOLU=this.w_PCUMVOLU;
                     ,PCPESNET=this.w_PCPESNET;
                     ,PCPESLOR=this.w_PCPESLOR;
                     ,CPROWORD=this.w_CPROWORD;
                     ,PCCODICE=this.w_PCCODICE;
                     ,PCUNIMIS=this.w_PCUNIMIS;
                     ,PCQTAART=this.w_PCQTAART;
                     ,PCDESART=this.w_PCDESART;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PCCODCON)) AND t_PCQTACON<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAC_CONF
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PCCODCON)) AND t_PCQTACON<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAC_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_CONF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_TIPCOL = this.oParentObject .w_PLTIPCOL
          .w_DATDOC = this.oParentObject .w_PLDATDOC
          .w_OBTEST = IIF(EMPTY(.w_DATDOC), i_DATSYS, .w_DATDOC)
        .DoRTCalc(6,15,.t.)
          .link_2_11('Full')
        .DoRTCalc(17,20,.t.)
        if .o_PCCODICE<>.w_PCCODICE
          .w_PCUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_16('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_CODART with this.w_CODART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_FLSERG with this.w_FLSERG
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_COLL_IDX,3]
    i_lTable = "CON_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2], .t., this.CON_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'CON_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODCON like "+cp_ToStrODBC(trim(this.w_PCCODCON)+"%");
                   +" and TCCODICE="+cp_ToStrODBC(this.w_TIPCOL);

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE,TCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',this.w_TIPCOL;
                     ,'TCCODCON',trim(this.w_PCCODCON))
          select TCCODICE,TCCODCON,TCDESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE,TCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCODCON)==trim(_Link_.TCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCODCON) and !this.bDontReportError
            deferred_cp_zoom('CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(oSource.parent,'oPCCODCON_2_3'),i_cWhere,'GSAR_MTO',"Tpi confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCOL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TCCODICE,TCCODCON,TCDESCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TCCODICE="+cp_ToStrODBC(this.w_TIPCOL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1);
                       ,'TCCODCON',oSource.xKey(2))
            select TCCODICE,TCCODCON,TCDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(this.w_PCCODCON);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_TIPCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCOL;
                       ,'TCCODCON',this.w_PCCODCON)
            select TCCODICE,TCCODCON,TCDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODCON = NVL(_Link_.TCCODCON,space(3))
      this.w_PCDESCON = NVL(_Link_.TCDESCON,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODCON = space(3)
      endif
      this.w_PCDESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODCON,1)
      cp_ShowWarn(i_cKey,this.CON_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCUMVOLU
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCUMVOLU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PCUMVOLU)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PCUMVOLU))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCUMVOLU)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCUMVOLU) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPCUMVOLU_2_6'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCUMVOLU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PCUMVOLU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PCUMVOLU)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCUMVOLU = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PCUMVOLU = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCUMVOLU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCCODICE
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PCCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PCCODICE))
          select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPCCODICE_2_10'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_MCK.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PCCODICE)
            select CACODICE,CADESART,CACODART,CAMOLTIP,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_PCDESART = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODICE = space(20)
      endif
      this.w_PCDESART = space(40)
      this.w_CODART = space(20)
      this.w_MOLTI3 = 0
      this.w_UNMIS3 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.CACODICE as CACODICE210"+ ",link_2_10.CADESART as CADESART210"+ ",link_2_10.CACODART as CACODART210"+ ",link_2_10.CAMOLTIP as CAMOLTIP210"+ ",link_2_10.CAUNIMIS as CAUNIMIS210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on PAC_CONF.PCCODICE=link_2_10.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and PAC_CONF.PCCODICE=link_2_10.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARUNMIS1,ARUNMIS2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCUNIMIS
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PCUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PCUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPCUNIMIS_2_16'),i_cWhere,'GSAR_AUM',"",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PCUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PCUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PCUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_PCUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura incongruente")
        endif
        this.w_PCUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.UMCODICE as UMCODICE216"+ ",link_2_16.UMFLFRAZ as UMFLFRAZ216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on PAC_CONF.PCUNIMIS=link_2_16.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and PAC_CONF.PCUNIMIS=link_2_16.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPCCODICE_2_10.value==this.w_PCCODICE)
      this.oPgFrm.Page1.oPag.oPCCODICE_2_10.value=this.w_PCCODICE
      replace t_PCCODICE with this.oPgFrm.Page1.oPag.oPCCODICE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCUNIMIS_2_16.value==this.w_PCUNIMIS)
      this.oPgFrm.Page1.oPag.oPCUNIMIS_2_16.value=this.w_PCUNIMIS
      replace t_PCUNIMIS with this.oPgFrm.Page1.oPag.oPCUNIMIS_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCQTAART_2_17.value==this.w_PCQTAART)
      this.oPgFrm.Page1.oPag.oPCQTAART_2_17.value=this.w_PCQTAART
      replace t_PCQTAART with this.oPgFrm.Page1.oPag.oPCQTAART_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDESART_2_18.value==this.w_PCDESART)
      this.oPgFrm.Page1.oPag.oPCDESART_2_18.value=this.w_PCDESART
      replace t_PCDESART with this.oPgFrm.Page1.oPag.oPCDESART_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTACON_2_2.value==this.w_PCQTACON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTACON_2_2.value=this.w_PCQTACON
      replace t_PCQTACON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTACON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODCON_2_3.value==this.w_PCCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODCON_2_3.value=this.w_PCCODCON
      replace t_PCCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCDESCON_2_4.value==this.w_PCDESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCDESCON_2_4.value=this.w_PCDESCON
      replace t_PCDESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCDESCON_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVOLUME_2_5.value==this.w_PCVOLUME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVOLUME_2_5.value=this.w_PCVOLUME
      replace t_PCVOLUME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVOLUME_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUMVOLU_2_6.value==this.w_PCUMVOLU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUMVOLU_2_6.value=this.w_PCUMVOLU
      replace t_PCUMVOLU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUMVOLU_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESNET_2_7.value==this.w_PCPESNET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESNET_2_7.value=this.w_PCPESNET
      replace t_PCPESNET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESNET_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESLOR_2_8.value==this.w_PCPESLOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESLOR_2_8.value=this.w_PCPESLOR
      replace t_PCPESLOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPESLOR_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'PAC_CONF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(CHKUNIMI(.w_PCUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)) and not(empty(.w_PCUNIMIS)) and (not(Empty(.w_PCCODCON)) AND .w_PCQTACON<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oPCUNIMIS_2_16
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura incongruente")
      endcase
      if not(Empty(.w_PCCODCON)) AND .w_PCQTACON<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PCCODICE = this.w_PCCODICE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PCCODCON)) AND t_PCQTACON<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PCQTACON=0
      .w_PCCODCON=space(3)
      .w_PCDESCON=space(30)
      .w_PCVOLUME=0
      .w_PCUMVOLU=space(3)
      .w_PCPESNET=0
      .w_PCPESLOR=0
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_PCCODICE=space(20)
      .w_CODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_PCUNIMIS=space(3)
      .w_PCQTAART=0
      .w_PCDESART=space(40)
      .w_FLFRAZ=space(1)
      .w_FLSERG=space(1)
      .DoRTCalc(1,8,.f.)
      if not(empty(.w_PCCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(9,11,.f.)
      if not(empty(.w_PCUMVOLU))
        .link_2_6('Full')
      endif
      .DoRTCalc(12,15,.f.)
      if not(empty(.w_PCCODICE))
        .link_2_10('Full')
      endif
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_CODART))
        .link_2_11('Full')
      endif
      .DoRTCalc(17,20,.f.)
        .w_PCUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_PCUNIMIS))
        .link_2_16('Full')
      endif
      .DoRTCalc(22,24,.f.)
        .w_FLSERG = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_PCQTACON = t_PCQTACON
    this.w_PCCODCON = t_PCCODCON
    this.w_PCDESCON = t_PCDESCON
    this.w_PCVOLUME = t_PCVOLUME
    this.w_PCUMVOLU = t_PCUMVOLU
    this.w_PCPESNET = t_PCPESNET
    this.w_PCPESLOR = t_PCPESLOR
    this.w_CPROWORD = t_CPROWORD
    this.w_PCCODICE = t_PCCODICE
    this.w_CODART = t_CODART
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_PCUNIMIS = t_PCUNIMIS
    this.w_PCQTAART = t_PCQTAART
    this.w_PCDESART = t_PCDESART
    this.w_FLFRAZ = t_FLFRAZ
    this.w_FLSERG = t_FLSERG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_PCQTACON with this.w_PCQTACON
    replace t_PCCODCON with this.w_PCCODCON
    replace t_PCDESCON with this.w_PCDESCON
    replace t_PCVOLUME with this.w_PCVOLUME
    replace t_PCUMVOLU with this.w_PCUMVOLU
    replace t_PCPESNET with this.w_PCPESNET
    replace t_PCPESLOR with this.w_PCPESLOR
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PCCODICE with this.w_PCCODICE
    replace t_CODART with this.w_CODART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_PCUNIMIS with this.w_PCUNIMIS
    replace t_PCQTAART with this.w_PCQTAART
    replace t_PCDESART with this.w_PCDESART
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_FLSERG with this.w_FLSERG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mckPag1 as StdContainer
  Width  = 700
  height = 177
  stdWidth  = 700
  stdheight = 177
  resizeXpos=286
  resizeYpos=101
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_8 as StdButton with uid="XVFSKZRQBE",left=644, top=122, width=48,height=45,;
    CpPicture="bmp\right.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per spostare le confezioni in un altro collo";
    , HelpContextID = 219157978;
    , tabstop=.f., Caption='\<Sposta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSMA_KSK with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=12, top=0, width=681,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="PCQTACON",Label1="Qta conf.",Field2="PCCODCON",Label2="Conf.",Field3="PCDESCON",Label3="Descrizione confezione",Field4="PCVOLUME",Label4="Volume",Field5="PCUMVOLU",Label5="In",Field6="PCPESNET",Label6="Peso netto",Field7="PCPESLOR",Label7="Peso lordo",Field8="CPROWORD",Label8="Riga",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49500550

  add object oStr_1_6 as StdString with uid="KKUHXKACPG",Visible=.t., Left=9, Top=123,;
    Alignment=0, Width=46, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SBGGCLSHRN",Visible=.t., Left=436, Top=123,;
    Alignment=1, Width=35, Height=18,;
    Caption="Qta.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=2,top=20,;
    width=677+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=3,top=21,width=676+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CON_COLL|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPCCODICE_2_10.Refresh()
      this.Parent.oPCUNIMIS_2_16.Refresh()
      this.Parent.oPCQTAART_2_17.Refresh()
      this.Parent.oPCDESART_2_18.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CON_COLL'
        oDropInto=this.oBodyCol.oRow.oPCCODCON_2_3
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oPCUMVOLU_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPCCODICE_2_10 as StdTrsField with uid="VOVOQRKVMR",rtseq=15,rtrep=.t.,;
    cFormVar="w_PCCODICE",value=space(20),;
    ToolTipText = "Articolo presente nella confezione",;
    HelpContextID = 137822917,;
    cTotal="", bFixedPos=.t., cQueryName = "PCCODICE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=126, Left=55, Top=123, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PCCODICE"

  func oPCCODICE_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCODICE_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPCCODICE_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPCCODICE_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_MCK.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPCCODICE_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PCCODICE
    i_obj.ecpSave()
  endproc

  add object oPCUNIMIS_2_16 as StdTrsField with uid="KRQAVVFZTA",rtseq=21,rtrep=.t.,;
    cFormVar="w_PCUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura dell'articolo",;
    HelpContextID = 65462967,;
    cTotal="", bFixedPos=.t., cQueryName = "PCUNIMIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura incongruente",;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=42, Left=471, Top=123, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PCUNIMIS"

  func oPCUNIMIS_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCUNIMIS_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPCUNIMIS_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPCUNIMIS_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oPCUNIMIS_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PCUNIMIS
    i_obj.ecpSave()
  endproc

  add object oPCQTAART_2_17 as StdTrsField with uid="NFXGSTPQTJ",rtseq=22,rtrep=.t.,;
    cFormVar="w_PCQTAART",value=0,;
    ToolTipText = "Quantit� articolo presente nella confezione",;
    HelpContextID = 262069578,;
    cTotal="", bFixedPos=.t., cQueryName = "PCQTAART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=78, Left=515, Top=123, cSayPict=["999999999.999"], cGetPict=["999999999.999"]

  add object oPCDESART_2_18 as StdTrsField with uid="SWYVYJRUHY",rtseq=23,rtrep=.t.,;
    cFormVar="w_PCDESART",value=space(40),enabled=.f.,;
    HelpContextID = 256963254,;
    cTotal="", bFixedPos=.t., cQueryName = "PCDESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=252, Left=181, Top=123, InputMask=replicate('X',40)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mckBodyRow as CPBodyRowCnt
  Width=667
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPCQTACON_2_2 as StdTrsField with uid="PXGZBMESVE",rtseq=7,rtrep=.t.,;
    cFormVar="w_PCQTACON",value=0,;
    ToolTipText = "Numero di confezioni presenti nel collo",;
    HelpContextID = 27188548,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=59, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oPCCODCON_2_3 as StdTrsField with uid="JZGJDXWGLW",rtseq=8,rtrep=.t.,;
    cFormVar="w_PCCODCON",value=space(3),;
    ToolTipText = "Tipo confezione",;
    HelpContextID = 29949252,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=42, Left=60, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CON_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_TIPCOL", oKey_2_1="TCCODCON", oKey_2_2="this.w_PCCODCON"

  func oPCCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPCCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_COLL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPCOL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStr(this.Parent.oContained.w_TIPCOL)
    endif
    do cp_zoom with 'CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(this.parent,'oPCCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tpi confezioni",'',this.parent.oContained
  endproc
  proc oPCCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TCCODICE=w_TIPCOL
     i_obj.w_TCCODCON=this.parent.oContained.w_PCCODCON
    i_obj.ecpSave()
  endproc

  add object oPCDESCON_2_4 as StdTrsField with uid="YMIJBBVRFW",rtseq=9,rtrep=.t.,;
    cFormVar="w_PCDESCON",value=space(30),enabled=.f.,;
    HelpContextID = 45026628,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=223, Left=103, Top=0, InputMask=replicate('X',30)

  add object oPCVOLUME_2_5 as StdTrsField with uid="CJNAHLNQTC",rtseq=10,rtrep=.t.,;
    cFormVar="w_PCVOLUME",value=0,;
    ToolTipText = "Volume confezione",;
    HelpContextID = 71970107,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=78, Left=328, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oPCUMVOLU_2_6 as StdTrsField with uid="OEQUZQZUNY",rtseq=11,rtrep=.t.,;
    cFormVar="w_PCUMVOLU",value=space(3),;
    ToolTipText = "Unit� di misura del volume",;
    HelpContextID = 250092875,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=409, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PCUMVOLU"

  func oPCUMVOLU_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCUMVOLU_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPCUMVOLU_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPCUMVOLU_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oPCUMVOLU_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PCUMVOLU
    i_obj.ecpSave()
  endproc

  add object oPCPESNET_2_7 as StdTrsField with uid="KIMJUYFVUD",rtseq=12,rtrep=.t.,;
    cFormVar="w_PCPESNET",value=0,;
    ToolTipText = "Peso netto in kg.",;
    HelpContextID = 38810294,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=73, Left=453, Top=0, cSayPict=["999999.999"], cGetPict=["999999.999"]

  add object oPCPESLOR_2_8 as StdTrsField with uid="XEKSPXPEGH",rtseq=13,rtrep=.t.,;
    cFormVar="w_PCPESLOR",value=0,;
    ToolTipText = "Peso lordo in kg.",;
    HelpContextID = 196070728,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=73, Left=531, Top=0, cSayPict=["999999.999"], cGetPict=["999999.999"]

  add object oCPROWORD_2_9 as StdTrsField with uid="GRTVPPZWCJ",rtseq=14,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Riga di riferimento",;
    HelpContextID = 17172118,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=607, Top=0, cSayPict=["999999"], cGetPict=["999999"], TabStop=.f.
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPCQTACON_2_2.When()
    return(.t.)
  proc oPCQTACON_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPCQTACON_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mck','PAC_CONF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PCSERIAL=PAC_CONF.PCSERIAL";
  +" and "+i_cAliasName2+".PCNUMCOL=PAC_CONF.PCNUMCOL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
