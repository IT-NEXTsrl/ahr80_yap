* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdv                                                        *
*              Dett.voci di ricl.mastri-conti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_13]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-14                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdv")
  return

* --- Class definition
define class tgscg_mdv as StdPCForm
  Width  = 669
  Height = 215
  Top    = 106
  Left   = 15
  cComment = "Dett.voci di ricl.mastri-conti"
  cPrg = "gscg_mdv"
  HelpContextID=80356713
  add object cnt as tcgscg_mdv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdv as PCContext
  w_DVCODVOC = space(15)
  w_DVROWORD = 0
  w_OBTEST = space(8)
  w_DVFLMACO = space(1)
  w_DVCODMAS = space(15)
  w_DVCODCON = space(15)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DESCRI = space(40)
  w_DV_SEGNO = space(1)
  w_DV__DAVE = space(1)
  w_NUMLIV = 0
  w_DATAOBSO = space(8)
  proc Save(i_oFrom)
    this.w_DVCODVOC = i_oFrom.w_DVCODVOC
    this.w_DVROWORD = i_oFrom.w_DVROWORD
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DVFLMACO = i_oFrom.w_DVFLMACO
    this.w_DVCODMAS = i_oFrom.w_DVCODMAS
    this.w_DVCODCON = i_oFrom.w_DVCODCON
    this.w_DESMAS = i_oFrom.w_DESMAS
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_DV_SEGNO = i_oFrom.w_DV_SEGNO
    this.w_DV__DAVE = i_oFrom.w_DV__DAVE
    this.w_NUMLIV = i_oFrom.w_NUMLIV
    this.w_DATAOBSO = i_oFrom.w_DATAOBSO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DVCODVOC = this.w_DVCODVOC
    i_oTo.w_DVROWORD = this.w_DVROWORD
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DVFLMACO = this.w_DVFLMACO
    i_oTo.w_DVCODMAS = this.w_DVCODMAS
    i_oTo.w_DVCODCON = this.w_DVCODCON
    i_oTo.w_DESMAS = this.w_DESMAS
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_DV_SEGNO = this.w_DV_SEGNO
    i_oTo.w_DV__DAVE = this.w_DV__DAVE
    i_oTo.w_NUMLIV = this.w_NUMLIV
    i_oTo.w_DATAOBSO = this.w_DATAOBSO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 669
  Height = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DETTRICL_IDX = 0
  MASTRI_IDX = 0
  CONTI_IDX = 0
  cFile = "DETTRICL"
  cKeySelect = "DVCODVOC,DVROWORD"
  cKeyWhere  = "DVCODVOC=this.w_DVCODVOC and DVROWORD=this.w_DVROWORD"
  cKeyDetail  = "DVCODVOC=this.w_DVCODVOC and DVROWORD=this.w_DVROWORD"
  cKeyWhereODBC = '"DVCODVOC="+cp_ToStrODBC(this.w_DVCODVOC)';
      +'+" and DVROWORD="+cp_ToStrODBC(this.w_DVROWORD)';

  cKeyDetailWhereODBC = '"DVCODVOC="+cp_ToStrODBC(this.w_DVCODVOC)';
      +'+" and DVROWORD="+cp_ToStrODBC(this.w_DVROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DETTRICL.DVCODVOC="+cp_ToStrODBC(this.w_DVCODVOC)';
      +'+" and DETTRICL.DVROWORD="+cp_ToStrODBC(this.w_DVROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DETTRICL.CPROWNUM '
  cPrg = "gscg_mdv"
  cComment = "Dett.voci di ricl.mastri-conti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DVCODVOC = space(15)
  w_DVROWORD = 0
  w_OBTEST = ctod('  /  /  ')
  w_DVFLMACO = space(1)
  o_DVFLMACO = space(1)
  w_DVCODMAS = space(15)
  w_DVCODCON = space(15)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DESCRI = space(40)
  w_DV_SEGNO = space(1)
  w_DV__DAVE = space(1)
  w_NUMLIV = 0
  w_DATAOBSO = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mdvPag1","gscg_mdv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DETTRICL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DETTRICL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DETTRICL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DETTRICL where DVCODVOC=KeySet.DVCODVOC
    *                            and DVROWORD=KeySet.DVROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DETTRICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2],this.bLoadRecFilter,this.DETTRICL_IDX,"gscg_mdv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DETTRICL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DETTRICL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DETTRICL '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DVCODVOC',this.w_DVCODVOC  ,'DVROWORD',this.w_DVROWORD  )
      select * from (i_cTable) DETTRICL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = I_DATSYS
        .w_DATAOBSO = ctod("  /  /  ")
        .w_DVCODVOC = NVL(DVCODVOC,space(15))
        .w_DVROWORD = NVL(DVROWORD,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DETTRICL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESMAS = space(40)
          .w_DESCON = space(40)
          .w_NUMLIV = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DVFLMACO = NVL(DVFLMACO,space(1))
          .w_DVCODMAS = NVL(DVCODMAS,space(15))
          if link_2_2_joined
            this.w_DVCODMAS = NVL(MCCODICE202,NVL(this.w_DVCODMAS,space(15)))
            this.w_DESMAS = NVL(MCDESCRI202,space(40))
            this.w_NUMLIV = NVL(MCNUMLIV202,0)
          else
          .link_2_2('Load')
          endif
          .w_DVCODCON = NVL(DVCODCON,space(15))
          if link_2_3_joined
            this.w_DVCODCON = NVL(ANCODICE203,NVL(this.w_DVCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI203,space(40))
            this.w_DATAOBSO = NVL(cp_ToDate(ANDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, SPACE(40)))
          .w_DV_SEGNO = NVL(DV_SEGNO,space(1))
          .w_DV__DAVE = NVL(DV__DAVE,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DVCODVOC=space(15)
      .w_DVROWORD=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DVFLMACO=space(1)
      .w_DVCODMAS=space(15)
      .w_DVCODCON=space(15)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DESCRI=space(40)
      .w_DV_SEGNO=space(1)
      .w_DV__DAVE=space(1)
      .w_NUMLIV=0
      .w_DATAOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_OBTEST = I_DATSYS
        .DoRTCalc(4,4,.f.)
        .w_DVCODMAS = SPACE(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DVCODMAS))
         .link_2_2('Full')
        endif
        .w_DVCODCON = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_DVCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,8,.f.)
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, SPACE(40)))
        .w_DV_SEGNO = '+'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DETTRICL')
    this.DoRTCalc(11,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DETTRICL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DETTRICL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DVCODVOC,"DVCODVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DVROWORD,"DVROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DVFLMACO C(1);
      ,t_DVCODMAS C(15);
      ,t_DVCODCON C(15);
      ,t_DESCRI C(40);
      ,t_DV_SEGNO C(1);
      ,t_DV__DAVE C(1);
      ,CPROWNUM N(10);
      ,t_DESMAS C(40);
      ,t_DESCON C(40);
      ,t_NUMLIV N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1.controlsource=this.cTrsName+'.t_DVFLMACO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_2.controlsource=this.cTrsName+'.t_DVCODMAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.controlsource=this.cTrsName+'.t_DVCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDV_SEGNO_2_7.controlsource=this.cTrsName+'.t_DV_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDV__DAVE_2_8.controlsource=this.cTrsName+'.t_DV__DAVE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(30)
    this.AddVLine(165)
    this.AddVLine(297)
    this.AddVLine(583)
    this.AddVLine(610)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DETTRICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DETTRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2])
      *
      * insert into DETTRICL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DETTRICL')
        i_extval=cp_InsertValODBCExtFlds(this,'DETTRICL')
        i_cFldBody=" "+;
                  "(DVCODVOC,DVROWORD,DVFLMACO,DVCODMAS,DVCODCON"+;
                  ",DV_SEGNO,DV__DAVE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DVCODVOC)+","+cp_ToStrODBC(this.w_DVROWORD)+","+cp_ToStrODBC(this.w_DVFLMACO)+","+cp_ToStrODBCNull(this.w_DVCODMAS)+","+cp_ToStrODBCNull(this.w_DVCODCON)+;
             ","+cp_ToStrODBC(this.w_DV_SEGNO)+","+cp_ToStrODBC(this.w_DV__DAVE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DETTRICL')
        i_extval=cp_InsertValVFPExtFlds(this,'DETTRICL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DVCODVOC',this.w_DVCODVOC,'DVROWORD',this.w_DVROWORD)
        INSERT INTO (i_cTable) (;
                   DVCODVOC;
                  ,DVROWORD;
                  ,DVFLMACO;
                  ,DVCODMAS;
                  ,DVCODCON;
                  ,DV_SEGNO;
                  ,DV__DAVE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DVCODVOC;
                  ,this.w_DVROWORD;
                  ,this.w_DVFLMACO;
                  ,this.w_DVCODMAS;
                  ,this.w_DVCODCON;
                  ,this.w_DV_SEGNO;
                  ,this.w_DV__DAVE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DETTRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DVFLMACO<>space(1) AND (t_DVCODMAS<>SPACE(15) OR t_DVCODCON<>SPACE(15))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DETTRICL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DETTRICL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DVFLMACO<>space(1) AND (t_DVCODMAS<>SPACE(15) OR t_DVCODCON<>SPACE(15))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DETTRICL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DETTRICL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DVFLMACO="+cp_ToStrODBC(this.w_DVFLMACO)+;
                     ",DVCODMAS="+cp_ToStrODBCNull(this.w_DVCODMAS)+;
                     ",DVCODCON="+cp_ToStrODBCNull(this.w_DVCODCON)+;
                     ",DV_SEGNO="+cp_ToStrODBC(this.w_DV_SEGNO)+;
                     ",DV__DAVE="+cp_ToStrODBC(this.w_DV__DAVE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DETTRICL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DVFLMACO=this.w_DVFLMACO;
                     ,DVCODMAS=this.w_DVCODMAS;
                     ,DVCODCON=this.w_DVCODCON;
                     ,DV_SEGNO=this.w_DV_SEGNO;
                     ,DV__DAVE=this.w_DV__DAVE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DETTRICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DVFLMACO<>space(1) AND (t_DVCODMAS<>SPACE(15) OR t_DVCODCON<>SPACE(15))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DETTRICL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DVFLMACO<>space(1) AND (t_DVCODMAS<>SPACE(15) OR t_DVCODCON<>SPACE(15))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DETTRICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTRICL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_DVFLMACO<>.w_DVFLMACO
          .w_DVCODMAS = SPACE(15)
          .link_2_2('Full')
        endif
        if .o_DVFLMACO<>.w_DVFLMACO
          .w_DVCODCON = SPACE(15)
          .link_2_3('Full')
        endif
        .DoRTCalc(7,8,.t.)
          .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, SPACE(40)))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESMAS with this.w_DESMAS
      replace t_DESCON with this.w_DESCON
      replace t_NUMLIV with this.w_NUMLIV
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODMAS_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODMAS_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODCON_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DVCODMAS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_DVCODMAS)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_DVCODMAS))
          select MCCODICE,MCDESCRI,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODMAS)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_DVCODMAS)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_DVCODMAS)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODMAS) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oDVCODMAS_2_2'),i_cWhere,'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_DVCODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_DVCODMAS)
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODMAS = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_NUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_DVCODMAS = space(15)
      endif
      this.w_DESMAS = space(40)
      this.w_NUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NUMLIV=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il mastro deve essere di livello 1")
        endif
        this.w_DVCODMAS = space(15)
        this.w_DESMAS = space(40)
        this.w_NUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.MCCODICE as MCCODICE202"+ ",link_2_2.MCDESCRI as MCDESCRI202"+ ",link_2_2.MCNUMLIV as MCNUMLIV202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DETTRICL.DVCODMAS=link_2_2.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DETTRICL.DVCODMAS=link_2_2.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DVCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DVFLMACO;
                     ,'ANCODICE',trim(this.w_DVCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DVFLMACO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDVCODCON_2_3'),i_cWhere,'GSAR_API',"Conti",'GSAR0MDV.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DVFLMACO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DVCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DVFLMACO;
                       ,'ANCODICE',this.w_DVCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DVCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"+ ",link_2_3.ANDTOBSO as ANDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on DETTRICL.DVCODCON=link_2_3.ANCODICE"+" and DETTRICL.DVFLMACO=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and DETTRICL.DVCODCON=link_2_3.ANCODICE(+)"'+'+" and DETTRICL.DVFLMACO=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1.value==this.w_DVFLMACO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1.value=this.w_DVFLMACO
      replace t_DVFLMACO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_2.value==this.w_DVCODMAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_2.value=this.w_DVCODMAS
      replace t_DVCODMAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value==this.w_DVCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value=this.w_DVCODCON
      replace t_DVCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV_SEGNO_2_7.value==this.w_DV_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV_SEGNO_2_7.value=this.w_DV_SEGNO
      replace t_DV_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV_SEGNO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV__DAVE_2_8.value==this.w_DV__DAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV__DAVE_2_8.value=this.w_DV__DAVE
      replace t_DV__DAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV__DAVE_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'DETTRICL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DVFLMACO $ 'MG') and (.w_DVFLMACO<>space(1) AND (.w_DVCODMAS<>SPACE(15) OR .w_DVCODCON<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: M o G")
        case   not(.w_NUMLIV=1) and (.w_DVFLMACO='M') and not(empty(.w_DVCODMAS)) and (.w_DVFLMACO<>space(1) AND (.w_DVCODMAS<>SPACE(15) OR .w_DVCODCON<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il mastro deve essere di livello 1")
        case   not(.w_DV_SEGNO $ "+-") and (.w_DVFLMACO<>space(1) AND (.w_DVCODMAS<>SPACE(15) OR .w_DVCODCON<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV_SEGNO_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: + o -")
        case   not(.w_DV__DAVE $ 'DA ') and (.w_DVFLMACO<>space(1) AND (.w_DVCODMAS<>SPACE(15) OR .w_DVCODCON<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDV__DAVE_2_8
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: D o A o vuoto")
      endcase
      if .w_DVFLMACO<>space(1) AND (.w_DVCODMAS<>SPACE(15) OR .w_DVCODCON<>SPACE(15))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DVFLMACO = this.w_DVFLMACO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DVFLMACO<>space(1) AND (t_DVCODMAS<>SPACE(15) OR t_DVCODCON<>SPACE(15)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DVFLMACO=space(1)
      .w_DVCODMAS=space(15)
      .w_DVCODCON=space(15)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DESCRI=space(40)
      .w_DV_SEGNO=space(1)
      .w_DV__DAVE=space(1)
      .w_NUMLIV=0
      .DoRTCalc(1,4,.f.)
        .w_DVCODMAS = SPACE(15)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_DVCODMAS))
        .link_2_2('Full')
      endif
        .w_DVCODCON = SPACE(15)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_DVCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,8,.f.)
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, SPACE(40)))
        .w_DV_SEGNO = '+'
    endwith
    this.DoRTCalc(11,13,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DVFLMACO = t_DVFLMACO
    this.w_DVCODMAS = t_DVCODMAS
    this.w_DVCODCON = t_DVCODCON
    this.w_DESMAS = t_DESMAS
    this.w_DESCON = t_DESCON
    this.w_DESCRI = t_DESCRI
    this.w_DV_SEGNO = t_DV_SEGNO
    this.w_DV__DAVE = t_DV__DAVE
    this.w_NUMLIV = t_NUMLIV
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DVFLMACO with this.w_DVFLMACO
    replace t_DVCODMAS with this.w_DVCODMAS
    replace t_DVCODCON with this.w_DVCODCON
    replace t_DESMAS with this.w_DESMAS
    replace t_DESCON with this.w_DESCON
    replace t_DESCRI with this.w_DESCRI
    replace t_DV_SEGNO with this.w_DV_SEGNO
    replace t_DV__DAVE with this.w_DV__DAVE
    replace t_NUMLIV with this.w_NUMLIV
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mdvPag1 as StdContainer
  Width  = 665
  height = 215
  stdWidth  = 665
  stdheight = 215
  resizeXpos=521
  resizeYpos=89
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=-1, width=649,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DVFLMACO",Label1="Tipo",Field2="DVCODMAS",Label2="Mastro",Field3="DVCODCON",Label3="Conto",Field4="DESCRI",Label4="Descrizione",Field5="DV_SEGNO",Label5="+/-",Field6="DV__DAVE",Label6="Vinc.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235734138

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=18,;
    width=645+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=19,width=644+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MASTRI|CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oDVCODMAS_2_2
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oDVCODCON_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mdvBodyRow as CPBodyRowCnt
  Width=635
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDVFLMACO_2_1 as StdTrsField with uid="SENSDACGNR",rtseq=4,rtrep=.t.,;
    cFormVar="w_DVFLMACO",value=space(1),;
    ToolTipText = "Digitare: M =mastro o G =conto generico",;
    HelpContextID = 22451589,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire: M o G",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=-2, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  proc oDVFLMACO_2_1.mDefault
    with this.Parent.oContained
      if empty(.w_DVFLMACO)
        .w_DVFLMACO = 'G'
      endif
    endwith
  endproc

  func oDVFLMACO_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DVFLMACO $ 'MG')
      if .not. empty(.w_DVCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDVCODMAS_2_2 as StdTrsField with uid="ECHUJUKKFQ",rtseq=5,rtrep=.t.,;
    cFormVar="w_DVCODMAS",value=space(15),;
    ToolTipText = "Codice del mastro contabile",;
    HelpContextID = 214525321,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il mastro deve essere di livello 1",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=30, Top=0, cSayPict=[p_MAS], cGetPict=[p_MAS], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_DVCODMAS"

  func oDVCODMAS_2_2.mCond()
    with this.Parent.oContained
      return (.w_DVFLMACO='M')
    endwith
  endfunc

  func oDVCODMAS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODMAS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODMAS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oDVCODMAS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this.parent.oContained
  endproc
  proc oDVCODMAS_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_DVCODMAS
    i_obj.ecpSave()
  endproc

  add object oDVCODCON_2_3 as StdTrsField with uid="BNYASAIKSO",rtseq=6,rtrep=.t.,;
    cFormVar="w_DVCODCON",value=space(15),;
    ToolTipText = "Codice del conto generico",;
    HelpContextID = 46753156,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=162, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DVFLMACO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DVCODCON"

  func oDVCODCON_2_3.mCond()
    with this.Parent.oContained
      return (.w_DVFLMACO='G' and (EMPTY(.w_DATAOBSO) OR .w_OBTEST<.w_DATAOBSO))
    endwith
  endfunc

  func oDVCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DVFLMACO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DVFLMACO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDVCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'GSAR0MDV.CONTI_VZM',this.parent.oContained
  endproc
  proc oDVCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DVFLMACO
     i_obj.w_ANCODICE=this.parent.oContained.w_DVCODCON
    i_obj.ecpSave()
  endproc

  add object oDESCRI_2_6 as StdTrsField with uid="JYWVDFDYFM",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 161371190,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=282, Left=294, Top=0, InputMask=replicate('X',40)

  add object oDV_SEGNO_2_7 as StdTrsField with uid="PZVOCGLZIK",rtseq=10,rtrep=.t.,;
    cFormVar="w_DV_SEGNO",value=space(1),;
    ToolTipText = "Segno + o - per calcoli",;
    HelpContextID = 153148027,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire: + o -",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=580, Top=0, InputMask=replicate('X',1)

  func oDV_SEGNO_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DV_SEGNO $ "+-")
    endwith
    return bRes
  endfunc

  add object oDV__DAVE_2_8 as StdTrsField with uid="BNRBPJQSFN",rtseq=11,rtrep=.t.,;
    cFormVar="w_DV__DAVE",value=space(1),;
    ToolTipText = "Vincolo: D =dare; A=avere; <vuoto> =nessun vincolo",;
    HelpContextID = 14361979,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire: D o A o vuoto",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=607, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oDV__DAVE_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DV__DAVE $ 'DA ')
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oDVFLMACO_2_1.When()
    return(.t.)
  proc oDVFLMACO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDVFLMACO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdv','DETTRICL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DVCODVOC=DETTRICL.DVCODVOC";
  +" and "+i_cAliasName2+".DVROWORD=DETTRICL.DVROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
