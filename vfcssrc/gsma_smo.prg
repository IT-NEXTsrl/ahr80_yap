* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_smo                                                        *
*              Stampa brogliaccio magazzino                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_22]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_smo",oParentObject))

* --- Class definition
define class tgsma_smo as StdForm
  Top    = 5
  Left   = 19

  * --- Standard Properties
  Width  = 619
  Height = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=76907671
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cpusers_IDX = 0
  MAGAZZIN_IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  CAM_AGAZ_IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsma_smo"
  cComment = "Stampa brogliaccio magazzino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(10)
  w_CODESE = space(4)
  w_CODUTE = 0
  o_CODUTE = 0
  w_DESUTE = space(20)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_NUMFIN = 0
  w_SELDET = space(10)
  w_SELPRO = space(10)
  w_TIPMOV = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_TIPCLF = space(10)
  w_CLIFOR = space(15)
  w_DESCLF = space(40)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CODART = space(20)
  w_DESART = space(40)
  w_CODCAU = space(5)
  w_DESCAU = space(35)
  w_CODAGE = space(5)
  w_DESAGE = space(35)
  w_CODVAL = space(3)
  w_DESVAL = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_smoPag1","gsma_smo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='CAM_AGAZ'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='ART_ICOL'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_smo
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(10)
      .w_CODESE=space(4)
      .w_CODUTE=0
      .w_DESUTE=space(20)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_SELDET=space(10)
      .w_SELPRO=space(10)
      .w_TIPMOV=space(10)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_TIPCLF=space(10)
      .w_CLIFOR=space(15)
      .w_DESCLF=space(40)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_CODCAU=space(5)
      .w_DESCAU=space(35)
      .w_CODAGE=space(5)
      .w_DESAGE=space(35)
      .w_CODVAL=space(3)
      .w_DESVAL=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_ONUME=0
        .w_AZIENDA = i_CODAZI
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODUTE))
          .link_1_3('Full')
        endif
        .w_DESUTE = IIF(.w_CODUTE<>0,.w_DESUTE,'Tutti')
          .DoRTCalc(5,8,.f.)
        .w_SELDET = 'N'
        .w_SELPRO = 'N'
        .w_TIPMOV = 'T'
          .DoRTCalc(12,14,.f.)
        .w_TIPCLF = ''
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CLIFOR))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODMAG))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_CODART))
          .link_1_20('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CODCAU))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_CODAGE))
          .link_1_24('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_CODVAL))
          .link_1_26('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
          .DoRTCalc(27,27,.f.)
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CODUTE<>.w_CODUTE
            .w_DESUTE = IIF(.w_CODUTE<>0,.w_DESUTE,'Tutti')
        endif
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLIFOR_1_16.enabled = this.oPgFrm.Page1.oPag.oCLIFOR_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_2'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_datini = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_datfin = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_datini = ctod("  /  /  ")
      this.w_datfin = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_3'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLIFOR
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipClf);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TipClf;
                     ,'ANCODICE',trim(this.w_CLIFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipClf);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TipClf);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIFOR_1_16'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipClf<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TipClf);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipClf);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TipClf;
                       ,'ANCODICE',this.w_CLIFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLIFOR = space(15)
      endif
      this.w_DESCLF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_18'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_20'),i_cWhere,'GSMA_BZA',"Chiavi articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCAU))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_22'),i_cWhere,'GSMA_ACM',"Causali di magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCAU)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODAGE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_1_24'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_CODVAL)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_26'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_2.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_2.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_3.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_3.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_4.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_4.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_5.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_5.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_6.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_6.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_7.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_7.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELDET_1_9.RadioValue()==this.w_SELDET)
      this.oPgFrm.Page1.oPag.oSELDET_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPRO_1_10.RadioValue()==this.w_SELPRO)
      this.oPgFrm.Page1.oPag.oSELPRO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMOV_1_11.RadioValue()==this.w_TIPMOV)
      this.oPgFrm.Page1.oPag.oTIPMOV_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_12.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_12.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_13.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_13.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_14.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_14.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLF_1_15.RadioValue()==this.w_TIPCLF)
      this.oPgFrm.Page1.oPag.oTIPCLF_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_16.value==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_16.value=this.w_CLIFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_17.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_17.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_18.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_18.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_19.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_19.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_20.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_20.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_21.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_21.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_22.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_22.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_23.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_23.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAGE_1_24.value==this.w_CODAGE)
      this.oPgFrm.Page1.oPag.oCODAGE_1_24.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_25.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_25.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_26.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_26.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_27.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_27.value=this.w_DESVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DATFIN) or .w_datini<=.w_datfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_DATFIN) or .w_datini<=.w_datfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_numfin) or .w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
          case   not(empty(.w_numfin) or .w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODUTE = this.w_CODUTE
    return

enddefine

* --- Define pages as container
define class tgsma_smoPag1 as StdContainer
  Width  = 615
  height = 398
  stdWidth  = 615
  stdheight = 398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_2 as StdField with uid="BXAKJGKHLJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 15787994,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=29, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oCODESE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_CODESE
     i_obj.ecpSave()
  endproc

  add object oCODUTE_1_3 as StdField with uid="KWNVABRSUG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente selezionato",;
    HelpContextID = 13690842,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=253, Top=29, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESUTE_1_4 as StdField with uid="IFLQVYFVRC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 13631946,;
   bGlobalFont=.t.,;
    Height=21, Width=154, Left=305, Top=29, InputMask=replicate('X',20)

  add object oDATINI_1_5 as StdField with uid="KUPKMXHOXS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di inizio selezione (vuota=no selezione)",;
    HelpContextID = 222033354,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=108, Top=53

  func oDATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFIN) or .w_datini<=.w_datfin)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_6 as StdField with uid="YOIGPADVBJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di fine selezione (vuota=no selezione)",;
    HelpContextID = 143586762,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=108, Top=77

  func oDATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFIN) or .w_datini<=.w_datfin)
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_7 as StdField with uid="JYDCOYLTNT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione/documento di inizio selezione",;
    HelpContextID = 222056746,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=305, Top=53, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_numfin) or .w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_8 as StdField with uid="MHAGPKZZRU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione/documento di fine selezione (0=no selezione)",;
    HelpContextID = 143610154,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=305, Top=77, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_numfin) or .w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSELDET_1_9 as StdCheck with uid="GAJZCQZTNA",rtseq=9,rtrep=.f.,left=471, top=30, caption="Dati aggiuntivi",;
    ToolTipText = "Stampa descrizione aggiuntiva",;
    HelpContextID = 47280346,;
    cFormVar="w_SELDET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELDET_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELDET_1_9.GetRadio()
    this.Parent.oContained.w_SELDET = this.RadioValue()
    return .t.
  endfunc

  func oSELDET_1_9.SetRadio()
    this.Parent.oContained.w_SELDET=trim(this.Parent.oContained.w_SELDET)
    this.value = ;
      iif(this.Parent.oContained.w_SELDET=='S',1,;
      0)
  endfunc

  add object oSELPRO_1_10 as StdCheck with uid="HIZHJKNGVF",rtseq=10,rtrep=.f.,left=471, top=52, caption="Provvisori",;
    ToolTipText = "Stampa anche i movimenti provvisori",;
    HelpContextID = 116748506,;
    cFormVar="w_SELPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELPRO_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELPRO_1_10.GetRadio()
    this.Parent.oContained.w_SELPRO = this.RadioValue()
    return .t.
  endfunc

  func oSELPRO_1_10.SetRadio()
    this.Parent.oContained.w_SELPRO=trim(this.Parent.oContained.w_SELPRO)
    this.value = ;
      iif(this.Parent.oContained.w_SELPRO=='S',1,;
      0)
  endfunc


  add object oTIPMOV_1_11 as StdCombo with uid="HZWLVMHBGL",rtseq=11,rtrep=.f.,left=471,top=77,width=104,height=21;
    , ToolTipText = "Stampa soltanto i movimenti fiscali, soltanto quelli non fiscali o tutti";
    , HelpContextID = 2632906;
    , cFormVar="w_TIPMOV",RowSource=""+"Fiscali,"+"Non fiscali,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMOV_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(10)))))
  endfunc
  func oTIPMOV_1_11.GetRadio()
    this.Parent.oContained.w_TIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oTIPMOV_1_11.SetRadio()
    this.Parent.oContained.w_TIPMOV=trim(this.Parent.oContained.w_TIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMOV=='S',1,;
      iif(this.Parent.oContained.w_TIPMOV=='N',2,;
      iif(this.Parent.oContained.w_TIPMOV=='T',3,;
      0)))
  endfunc

  add object oNUMDOC_1_12 as StdField with uid="WSDQIIPPBJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 53563690,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=108, Top=133, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOC_1_13 as StdField with uid="OPUPGYSKHF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento selezionato",;
    HelpContextID = 53594874,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=253, Top=133, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDATDOC_1_14 as StdField with uid="YODCAPJCQT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento selezionata",;
    HelpContextID = 53540298,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=477, Top=133


  add object oTIPCLF_1_15 as StdCombo with uid="XKEGFRUVTV",value=3,rtseq=15,rtrep=.f.,left=5,top=162,width=98,height=21;
    , ToolTipText = "Tipo movimento selezionato";
    , HelpContextID = 6433994;
    , cFormVar="w_TIPCLF",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLF_1_15.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oTIPCLF_1_15.GetRadio()
    this.Parent.oContained.w_TIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLF_1_15.SetRadio()
    this.Parent.oContained.w_TIPCLF=trim(this.Parent.oContained.w_TIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLF=='C',1,;
      iif(this.Parent.oContained.w_TIPCLF=='F',2,;
      iif(this.Parent.oContained.w_TIPCLF=='',3,;
      0)))
  endfunc

  func oTIPCLF_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CLIFOR)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCLIFOR_1_16 as StdField with uid="SSMVXQGFTQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CLIFOR", cQueryName = "CLIFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente o fornitore selezionato",;
    HelpContextID = 70228698,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=108, Top=162, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TipClf", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIFOR"

  func oCLIFOR_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF $ "CF")
    endwith
   endif
  endfunc

  func oCLIFOR_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIFOR_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIFOR_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TipClf)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TipClf)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIFOR_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCLIFOR_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TipClf
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIFOR
     i_obj.ecpSave()
  endproc

  add object oDESCLF_1_17 as StdField with uid="YQAKRLNYIR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 6422986,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=253, Top=162, InputMask=replicate('X',40)

  add object oCODMAG_1_18 as StdField with uid="LWWLJUWSIC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 583642,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=218, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_19 as StdField with uid="FFZLWMSBUI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 524746,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=253, Top=218, InputMask=replicate('X',30)

  add object oCODART_1_20 as StdField with uid="DIQGJRYZVG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'articolo da stampare",;
    HelpContextID = 33875930,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=108, Top=241, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Chiavi articoli",'',this.parent.oContained
  endproc
  proc oCODART_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_21 as StdField with uid="TFJCMMMMEH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 33817034,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=253, Top=241, InputMask=replicate('X',40)

  add object oCODCAU_1_22 as StdField with uid="GINXTGWFZZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di magazzino selezionata",;
    HelpContextID = 34793434,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=264, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCAU_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali di magazzino",'',this.parent.oContained
  endproc
  proc oCODCAU_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_23 as StdField with uid="WNDZGYROMQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34734538,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=253, Top=264, InputMask=replicate('X',35)

  add object oCODAGE_1_24 as StdField with uid="HQGZHKHUGA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Agente selezionato",;
    HelpContextID = 28633050,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=287, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oCODAGE_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_25 as StdField with uid="IZNQLJSTVW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28574154,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=253, Top=287, InputMask=replicate('X',35)

  add object oCODVAL_1_26 as StdField with uid="DOUDUJWXNN",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 184543194,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=310, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_27 as StdField with uid="XQISYEDKMM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 184484298,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=253, Top=310, InputMask=replicate('X',35)


  add object oObj_1_28 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=350, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 13530138


  add object oBtn_1_29 as StdButton with uid="KFREMQATUY",left=508, top=348, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 49738202;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="VQIDRGIXMU",left=560, top=348, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 84225094;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_31 as StdString with uid="FRMDQYTEVZ",Visible=.t., Left=6, Top=287,;
    Alignment=1, Width=100, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="LFAPPYBJQF",Visible=.t., Left=11, Top=53,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="CRSDGGLHNT",Visible=.t., Left=11, Top=77,;
    Alignment=1, Width=95, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="JGTFACJOPV",Visible=.t., Left=192, Top=53,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="KCOENFGIYX",Visible=.t., Left=192, Top=77,;
    Alignment=1, Width=107, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="LMNOJOTWZU",Visible=.t., Left=6, Top=264,;
    Alignment=1, Width=100, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QOKBETLBGH",Visible=.t., Left=11, Top=29,;
    Alignment=1, Width=95, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FCLNAWAIOH",Visible=.t., Left=185, Top=29,;
    Alignment=1, Width=66, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="EIAUOBWAYZ",Visible=.t., Left=4, Top=133,;
    Alignment=1, Width=102, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VGUSTODLRW",Visible=.t., Left=244, Top=133,;
    Alignment=0, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="UFLXGMDOPO",Visible=.t., Left=362, Top=133,;
    Alignment=1, Width=113, Height=15,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="NXCISOCKHU",Visible=.t., Left=6, Top=218,;
    Alignment=1, Width=100, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="APJQFDCAQF",Visible=.t., Left=6, Top=241,;
    Alignment=1, Width=100, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="ULZJMFEJOV",Visible=.t., Left=6, Top=310,;
    Alignment=1, Width=100, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="LBGNBFZNHI",Visible=.t., Left=14, Top=6,;
    Alignment=0, Width=592, Height=15,;
    Caption="Selezione registrazioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="ULYRUVUDEI",Visible=.t., Left=14, Top=110,;
    Alignment=0, Width=595, Height=15,;
    Caption="Selezione documento e cliente o fornitore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="TPYTZJKUKQ",Visible=.t., Left=14, Top=194,;
    Alignment=0, Width=592, Height=15,;
    Caption="Altre selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="ATNLVAFLCY",Visible=.t., Left=6, Top=350,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_46 as StdBox with uid="FLBQOXUDDE",left=9, top=21, width=602,height=1

  add object oBox_1_48 as StdBox with uid="VLGJGBUHJD",left=9, top=125, width=599,height=1

  add object oBox_1_50 as StdBox with uid="KLOSAGWGZC",left=8, top=209, width=599,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_smo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
