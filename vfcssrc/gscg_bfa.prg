* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bfa                                                        *
*              GENERAZIONE FLUSSO F24                                          *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-25                                                      *
* Last revis.: 2007-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bfa",oParentObject)
return(i_retval)

define class tgscg_bfa as StdBatch
  * --- Local variables
  w_CODCONALF = .f.
  w_RECINSERT = .f.
  w_RIFTRIB2 = space(2)
  w_CODSIA = space(5)
  w_RECORD = space(120)
  w_NomeFile = space(200)
  w_DATCRE = ctod("  /  /  ")
  w_Handle = 0
  w_NRECORD = 0
  w_NOMSUP = space(20)
  w_DATSUP = space(8)
  w_Separa = space(2)
  w_CODSIA = space(5)
  w_CODBAN = space(10)
  w_CODCON = space(15)
  w_BANABI = space(5)
  w_PROGDEL = space(7)
  w_SALDOFINALE = 0
  w_TOTCREVER = 0
  w_CODFIS = space(16)
  w_PERFIS = space(1)
  w_NOME = space(25)
  w_RAGSOCOG = space(40)
  w_SESSO = space(1)
  w_COMNASC = space(30)
  w_PROVNASC = space(2)
  w_DATNASC = ctod("  /  /  ")
  w_DATNASCC = space(8)
  w_PROTDEL = 0
  w_DATARIF = ctod("  /  /  ")
  w_CODESE = space(4)
  w_COMFIS = space(30)
  w_PROFIS = space(2)
  w_INDFIS = space(30)
  w_DATPAG = ctod("  /  /  ")
  w_INIPER = ctod("  /  /  ")
  w_FINEPER = ctod("  /  /  ")
  w_APPCOIN = space(1)
  w_CODTRIB = space(5)
  w_RIFTRIB = space(8)
  w_IMPDEBV = 0
  w_IMPCREV = 0
  w_CODUFF = space(3)
  w_CODATTO = space(11)
  w_TOTDER = 0
  w_TOTCER = 0
  w_SALDER = 0
  w_RAVVEDIM = 0
  w_IMMOBVAR = 0
  w_ACCONTO = 0
  w_SALDO = 0
  w_NUMFAB = 0
  w_DETRICI = 0
  w_MESS = space(100)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_AZBANABI = space(5)
  w_AZBANCAB = space(5)
  w_SE___CAP = space(9)
  w_SELOCALI = space(30)
  w_SEPROVIN = space(30)
  w_SEINDIRI = space(35)
  w_SECODDES = space(5)
  w_Decimali = 0
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_ValoreS = space(15)
  w_ValoreS1 = space(15)
  w_ValoreS2 = space(15)
  w_ValoreS3 = space(15)
  w_CODEL = space(4)
  w_CODEL1 = space(4)
  w_CODEL2 = space(4)
  w_CODEL3 = space(4)
  w_CODEL4 = space(4)
  w_CONTA = 0
  w_DETRAICI = space(1)
  w_CODFIS2 = space(16)
  w_CODIDEN = space(2)
  w_VFBANPAS = space(15)
  w_VFBANPRO = space(15)
  w_ABITES = space(5)
  w_CABTES = space(5)
  w_CONCOR = space(12)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_AZBANPRO = space(15)
  w_CIN = space(1)
  w_CODABI = space(5)
  w_CODCAB = space(5)
  w_CONTOCOR = space(12)
  w_INDICE = 0
  w_CARATTERE = space(1)
  w_SECODDES1 = space(5)
  w_CODSEDE = space(5)
  w_CAUTRIB = space(5)
  w_MATRICO = space(22)
  w_MESEIN = space(2)
  w_ANNOIN = space(4)
  w_MESEFI = space(2)
  w_ANNOFI = space(4)
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_CODREG = space(2)
  w_CODTRIB = space(5)
  w_IMDRE = 0
  w_IMCRE = 0
  w_CODENTE = space(2)
  w_CODCONT = space(2)
  w_CAUSALE = space(1)
  w_NUMRIF = space(7)
  w_CODENTE1 = space(5)
  w_CODPOS = space(12)
  w_CONTA = 0
  w_SALDACC = 0
  w_MFSERIAL = space(10)
  * --- WorkFile variables
  COC_MAST_idx=0
  AZIENDA_idx=0
  BAN_CHE_idx=0
  MODCPAG_idx=0
  MOD_PAG_idx=0
  MODEPAG_idx=0
  ESERCIZI_idx=0
  MODVPAG_idx=0
  MODIPAG_idx=0
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -Batch per la creazione del file ASCII per le deleghe F24-
    * --- Dati del Contribuente
    * --- Dati Domicilio Fiscale
    * --- Record Sezione Erario
    * --- Record Saldo Sezione Erario
    * --- Sez. ICI e altri tributi locali
    * --- Variabili per scritture record 50
    * --- Variabili utilizzate per la conversione degli importi
    this.w_Decimali = 2
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_CONTA = 0
    * --- Sez. Dati altri soggetti
    * --- Read from MODVPAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODVPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2],.t.,this.MODVPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VFDTPRES,VFBANPAS,VFBANPRO"+;
        " from "+i_cTable+" MODVPAG where ";
            +"VFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VFDTPRES,VFBANPAS,VFBANPRO;
        from (i_cTable) where;
            VFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATCRE = NVL(cp_ToDate(_read_.VFDTPRES),cp_NullValue(_read_.VFDTPRES))
      this.w_VFBANPAS = NVL(cp_ToDate(_read_.VFBANPAS),cp_NullValue(_read_.VFBANPAS))
      this.w_VFBANPRO = NVL(cp_ToDate(_read_.VFBANPRO),cp_NullValue(_read_.VFBANPRO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo congruenza date:
    *      Data creazione flusso e Data versamento del modello F24
    if this.oParentObject.w_DATACREA > this.w_DATCRE
      AH_ERRORMSG("Data generazione flusso  successiva alla data versamento modello F24.%0Generazione flusso interrotta!",48)
      i_retcode = 'stop'
      return
    endif
    this.w_CODCONALF = .F.
    this.w_RECINSERT = .F.
    this.w_SALDOFINALE = 0
    this.w_TOTCREVER = 0
    this.w_PROGDEL = "0000000"
    this.w_PROTDEL = "0000000"
    this.w_Record = space(120)
    this.w_Separa = iif(this.oParentObject.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_NomeFile = this.oParentObject.w_NomeFile
    this.w_DatSup = RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+ALLTRIM(STR(YEAR(CP_TODATE(this.oParentObject.w_DATACREA))))
    * --- Nome supporto
    * --- Campo di libera composizione. Deve comunque essere univoco: Data invio flusso+ seriale del modello F24+'F24'
    this.w_NOMSUP = left( this.w_Datsup + this.oParentObject.w_Seriale + "F4" +space(20), 20)
    this.w_NomeFile = alltrim(this.w_Nomefile)+alltrim(this.w_NOMSUP)
    this.w_DatSup = RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+RIGHT(ALLTRIM(STR(YEAR(CP_TODATE(this.oParentObject.w_DATACREA)))),2)
    NomeFile = '"'+this.w_NomeFile+'"' 
 NomeFile = &NomeFile
    this.w_Handle = fcreate(NomeFile, 0)
    if this.w_Handle = -1
      AH_ERRORMSG("Non � possibile creare il file per flusso F24",48)
      i_retcode = 'stop'
      return
    endif
    this.w_NRECORD = 0
    * --- Leggo i dati relativi al record di testa
    if g_APPLICATION <> "ad hoc ENTERPRISE"
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZCONBAN,AZCOFAZI,AZPIVAZI,AZBANPRO"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZCONBAN,AZCOFAZI,AZPIVAZI,AZBANPRO;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AZCONBAN),cp_NullValue(_read_.AZCONBAN))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_AZBANPRO = NVL(cp_ToDate(_read_.AZBANPRO),cp_NullValue(_read_.AZBANPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZCONTES,AZCOFAZI,AZPIVAZI,AZBANPRO"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZCONTES,AZCOFAZI,AZPIVAZI,AZBANPRO;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.AZCONTES),cp_NullValue(_read_.AZCONTES))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_AZBANPRO = NVL(cp_ToDate(_read_.AZBANPRO),cp_NullValue(_read_.AZBANPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if EMPTY(this.w_VFBANPAS) OR EMPTY(this.w_VFBANPRO)
      AH_ERRORMSG("Attenzione: banca passiva e/o banca proponente non specificate sul modello%0Nel file generato verranno indicati i dati della banca passiva e/o proponente presenti nei parametri IVA",48)
      * --- Messaggio di avvertimento
      if EMPTY(this.w_VFBANPAS)
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca passiva
        if g_APPLICATION <> "ad hoc ENTERPRISE"
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACONCOR,BACINABI;
              from (i_cTable) where;
                  BACODBAN = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
            this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BANUMCOR = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACONCOR,BACINABI;
              from (i_cTable) where;
                  BANUMCOR = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
            this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      else
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca passiva
        if g_APPLICATION <> "ad hoc ENTERPRISE"
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_VFBANPAS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACONCOR,BACINABI;
              from (i_cTable) where;
                  BACODBAN = this.w_VFBANPAS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
            this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BANUMCOR = "+cp_ToStrODBC(this.w_VFBANPAS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACONCOR,BACINABI;
              from (i_cTable) where;
                  BANUMCOR = this.w_VFBANPAS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
            this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      if EMPTY(this.w_VFBANPRO)
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca proponente
        if g_APPLICATION <> "ad hoc ENTERPRISE"
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACODSIA"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_AZBANPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACODSIA;
              from (i_cTable) where;
                  BACODBAN = this.w_AZBANPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACODSIA"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BANUMCOR = "+cp_ToStrODBC(this.w_AZBANPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACODSIA;
              from (i_cTable) where;
                  BANUMCOR = this.w_AZBANPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      else
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca proponente
        if g_APPLICATION <> "ad hoc ENTERPRISE"
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACODSIA"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_VFBANPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACODSIA;
              from (i_cTable) where;
                  BACODBAN = this.w_VFBANPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACODABI,BACODCAB,BACODSIA"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BANUMCOR = "+cp_ToStrODBC(this.w_VFBANPRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACODABI,BACODCAB,BACODSIA;
              from (i_cTable) where;
                  BANUMCOR = this.w_VFBANPRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
            this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
            this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
    else
      if g_APPLICATION <> "ad hoc ENTERPRISE"
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca passiva
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_VFBANPAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACODABI,BACODCAB,BACONCOR,BACINABI;
            from (i_cTable) where;
                BACODBAN = this.w_VFBANPAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
          this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
          this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
          this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca proponente
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACODABI,BACODCAB,BACODSIA"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_VFBANPRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACODABI,BACODCAB,BACODSIA;
            from (i_cTable) where;
                BACODBAN = this.w_VFBANPRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
          this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
          this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca passiva
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACODABI,BACODCAB,BACONCOR,BACINABI"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BANUMCOR = "+cp_ToStrODBC(this.w_VFBANPAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACODABI,BACODCAB,BACONCOR,BACINABI;
            from (i_cTable) where;
                BANUMCOR = this.w_VFBANPAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ABITES = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
          this.w_CABTES = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
          this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
          this.w_CIN = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Effettuo una lettura sulla tabella Conti di tesoreria con il campo Banca proponente
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACODABI,BACODCAB,BACODSIA"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BANUMCOR = "+cp_ToStrODBC(this.w_VFBANPRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACODABI,BACODCAB,BACODSIA;
            from (i_cTable) where;
                BANUMCOR = this.w_VFBANPRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BANPROABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
          this.w_BANPROCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
          this.w_BANPROSIA = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if EMPTY(this.w_ABITES) OR EMPTY(this.w_CABTES) OR EMPTY(this.w_CONCOR)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      AH_ERRORMSG("Attenzione: codice ABI, CAB, numero conto corrente della banca passiva mancanti%0Impossibile generare il file",48)
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.w_BANPROABI) OR EMPTY(this.w_BANPROCAB) OR EMPTY(this.w_BANPROSIA)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      AH_ERRORMSG("Attenzione: codice ABI, CAB, codice SIA della banca proponente mancanti%0Impossibile generare il file",48)
      i_retcode = 'stop'
      return
    endif
    * --- Verifica che i caratteri del codice SIA siano alfabetici o numerici 
    this.w_INDICE = 1
    do while this.w_indice <= len (this.w_BANPROSIA)
      this.w_CARATTERE = substr (this.w_BANPROSIA,this.w_INDICE,this.w_INDICE)
      if NOT (ISDIGIT (this.w_CARATTERE ) OR ISALPHA(this.w_CARATTERE))
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        AH_ERRORMSG("Attenzione: il codice SIA del conto corrente specificato per il versamento � errato%0Impossibile generare il file",48)
        i_retcode = 'stop'
        return
      endif
      this.w_INDICE = this.w_INDICE + 1
    enddo
    * --- Se l attestazione di pagamento e 'mittente fisic' allora la ragione sociale dell azienda deve obbligatoriamente essere impostata
    if this.oParentObject.w_INATTPAG="2" AND EMPTY (g_RAGAZI)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      AH_ERRORMSG("Non � definita la denominazione/ragione sociale del mittente",48)
      i_retcode = 'stop'
      return
    endif
    * --- Controllo che il codice fiscale o la Partiva IVA siano stati insertiti
    *     nei dati Azienda altrimenti non genero il relativo file
    this.w_COFAZI = LEFT(iif(empty(this.w_PIVAZI),this.w_COFAZI,this.w_PIVAZI)+space(20),20)
    if empty(this.w_COFAZI)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      AH_ERRORMSG("Partita IVA/codice fiscale del mittente non inserito%0Impossibile generare il file",48)
      i_retcode = 'stop'
      return
    endif
    this.w_AZBANABI = right(space(5)+NVL(this.w_BANPROABI,SPACE(5)),5)
    this.w_AZBANCAB = right(space(5)+NVL(this.w_BANPROCAB,SPACE(5)),5)
    this.w_CIN = right(space(1)+NVL(this.w_CIN,SPACE(1)),1)
    this.w_CODABI = right(space(5)+NVL(this.w_ABITES,SPACE(5)),5)
    this.w_CODCAB = right(space(5)+NVL(this.w_CABTES,SPACE(5)),5)
    this.w_CONTOCOR = right(space(12)+NVL(this.w_CONCOR,SPACE(12)),12)
    if empty(NVL(g_RAGAZI,""))
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      AH_ERRORMSG("Ragione sociale non inserita%0Impossibile generare il file",48)
      i_retcode = 'stop'
      return
    endif
    * --- Verifico se l'utente ha selezionato :
    *     "2 - Titolare del C/C di pagam. corrisp. al contribuente". In questo caso
    *     devo verificare che il codice fiscale impostato sulla maschera sia uguale
    *     al codice fiscale presente nella prima pagina del modello F24/2006
    if this.oParentObject.w_TITCCPAG="2"
      * --- Read from MODCPAG
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MODCPAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2],.t.,this.MODCPAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CFCODFIS"+;
          " from "+i_cTable+" MODCPAG where ";
              +"CFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CFCODFIS;
          from (i_cTable) where;
              CFSERIAL = this.oParentObject.w_SERIALE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODFIS = NVL(cp_ToDate(_read_.CFCODFIS),cp_NullValue(_read_.CFCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if right(space(16)+this.oParentObject.w_CFTITCC,16)<>right(space(16)+this.w_CODFIS,16)
        if NOT AH_YESNO("Codice fiscale del titolare c/c di addebito non coincidente con il codice fiscale del contribuente %0 Il flusso generato potrebbe non essere accettato %0 Si vuole proseguire?")
          AH_ERRORMSG("Operazione abbandonata",48)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Controllo se l'utente nella maschera di elaborazione ha selezionato come
    *     Attestazione di Pagamento: Mittente fisico
    if this.oParentObject.w_INATTPAG="2"
      * --- Verifico se nella tabella Sedi Azienda � presente almeno una sede con
      *     tipologia destinatario stampa
      * --- Read from MOD_PAG
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_PAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MFDESSTA"+;
          " from "+i_cTable+" MOD_PAG where ";
              +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MFDESSTA;
          from (i_cTable) where;
              MFSERIAL = this.oParentObject.w_SERIALE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SECODDES1 = NVL(cp_ToDate(_read_.MFDESSTA),cp_NullValue(_read_.MFDESSTA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from SEDIAZIE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SECODDES"+;
          " from "+i_cTable+" SEDIAZIE where ";
              +"SECODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and SECODDES = "+cp_ToStrODBC(this.w_SECODDES1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SECODDES;
          from (i_cTable) where;
              SECODAZI = i_CODAZI;
              and SECODDES = this.w_SECODDES1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SE___CAP = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
        this.w_SELOCALI = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
        this.w_SEPROVIN = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
        this.w_SEINDIRI = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
        this.w_SECODDES = NVL(cp_ToDate(_read_.SECODDES),cp_NullValue(_read_.SECODDES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controllo se esiste una sede associata all'azienda di tipologia Domicilio Fiscale
      if Not Empty(this.w_SECODDES)
        * --- Verifico che nella sede associata siano presenti tutti i dati necessari per la
        *     scrittura del record 50-03
        if EMPTY(this.w_SE___CAP)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Codice avviamento postale non presente sulla sede aziendale specificata quale destinataria predefinita di stampa%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.w_SELOCALI)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Localit� non presente sulla sede aziendale specificata quale destinataria predefinita di stampa%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.w_SEPROVIN)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Provincia non presente sulla sede aziendale specificata quale destinataria predefinita di stampa%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.w_SEINDIRI)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Indirizzo non presente sulla sede aziendale specificata quale destinataria predefinita di stampa%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
      else
        if Empty(g_Capazi)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Codice avviamento postale non inserito nei dati azienda%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if Empty(g_Locazi)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Localit� non inserita nei dati azienda%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if EMPTY(g_Proazi)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Provincia non inserita nei dati azienda%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        if EMPTY(g_Indazi)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AH_ERRORMSG("Indirizzo non inserito nei dati azienda%0Impossibile generare il file",48)
          i_retcode = 'stop'
          return
        endif
        this.w_SE___CAP = g_Capazi
        this.w_SELOCALI = g_Locazi
        this.w_SEPROVIN = g_Proazi
        this.w_SEINDIRI = g_Indazi
      endif
      this.w_SELOCALI = UPPER(this.w_SELOCALI)
      this.w_SEPROVIN = UPPER(this.w_SEPROVIN)
      this.w_SEINDIRI = UPPER(this.w_SEINDIRI)
    endif
    ah_msg("Inizio generazione flusso") 
    * --- Record di testa
    this.w_RECORD = " F4"+this.w_BANPROSIA+this.w_CODABI+this.w_DATSUP+this.w_NOMSUP+space(6)+space(59)+"2"+"$"+this.w_BANPROSIA+space(2)+"E"+" "+space(5)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NRECORD = 1
    this.w_RECORD = space(120)
    this.w_PROGDEL = RIGHT("0000000"+ALLTRIM(STR(VAL(this.w_PROGDEL)+1)),7)
    this.w_PROTDEL = RIGHT("0000000"+ALLTRIM(STR(VAL(this.w_PROTDEL)+1)),7)
    * --- Read from MODCPAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODCPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2],.t.,this.MODCPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CFCODFIS,CFPERFIS,CF__NOME,CFRAGSOC,CF_SESSO,CFLOCNAS,CFPRONAS,CFDATNAS,CFLOCALI,CFPROVIN,CFINDIRI,CFCODIDE,CFSECCOD"+;
        " from "+i_cTable+" MODCPAG where ";
            +"CFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CFCODFIS,CFPERFIS,CF__NOME,CFRAGSOC,CF_SESSO,CFLOCNAS,CFPRONAS,CFDATNAS,CFLOCALI,CFPROVIN,CFINDIRI,CFCODIDE,CFSECCOD;
        from (i_cTable) where;
            CFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODFIS = NVL(cp_ToDate(_read_.CFCODFIS),cp_NullValue(_read_.CFCODFIS))
      this.w_PERFIS = NVL(cp_ToDate(_read_.CFPERFIS),cp_NullValue(_read_.CFPERFIS))
      this.w_NOME = NVL(cp_ToDate(_read_.CF__NOME),cp_NullValue(_read_.CF__NOME))
      this.w_RAGSOCOG = NVL(cp_ToDate(_read_.CFRAGSOC),cp_NullValue(_read_.CFRAGSOC))
      this.w_SESSO = NVL(cp_ToDate(_read_.CF_SESSO),cp_NullValue(_read_.CF_SESSO))
      this.w_COMNASC = NVL(cp_ToDate(_read_.CFLOCNAS),cp_NullValue(_read_.CFLOCNAS))
      this.w_PROVNASC = NVL(cp_ToDate(_read_.CFPRONAS),cp_NullValue(_read_.CFPRONAS))
      this.w_DATNASC = NVL(cp_ToDate(_read_.CFDATNAS),cp_NullValue(_read_.CFDATNAS))
      this.w_COMFIS = NVL(cp_ToDate(_read_.CFLOCALI),cp_NullValue(_read_.CFLOCALI))
      this.w_PROFIS = NVL(cp_ToDate(_read_.CFPROVIN),cp_NullValue(_read_.CFPROVIN))
      this.w_INDFIS = NVL(cp_ToDate(_read_.CFINDIRI),cp_NullValue(_read_.CFINDIRI))
      this.w_CODIDEN = NVL(cp_ToDate(_read_.CFCODIDE),cp_NullValue(_read_.CFCODIDE))
      this.w_CODFIS2 = NVL(cp_ToDate(_read_.CFSECCOD),cp_NullValue(_read_.CFSECCOD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DATNASC = NVL(this.w_DATNASC,cp_CharToDate(" - - "))
    * --- Controllo se � vuota la data di nascita
    if EMPTY(this.w_DATNASC)
      this.w_DATNASCC = space(8)
    else
      this.w_DATNASCC = ALLTRIM(STR(YEAR(CP_TODATE(this.w_DATNASC)))) +RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.w_DATNASC)))),2)+ RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.w_DATNASC)))),2)
    endif
    this.w_CODFIS = iif(this.oParentObject.w_TITCCPAG="2",right(space(16)+this.oParentObject.w_CFTITCC,16),LEFT(NVL(this.w_CODFIS,SPACE(16))+space(16),16))
    this.w_RAGSOCOG = left(NVL(this.w_RAGSOCOG,SPACE(25))+space(40),24)
    if this.w_PERFIS="S"
      this.w_NOME = left(NVL(this.w_NOME,SPACE(25))+space(20),20)
      this.w_COMNASC = left(NVL(this.w_COMNASC,SPACE(30))+space(25),25)
      this.w_PROVNASC = right(space(2)+NVL(this.w_PROVNASC,SPACE(2)),2)
    else
      * --- Continuazione della ragione sociale
      this.w_NOME = NVL(this.w_RAGSOCOG,SPACE(25))
      this.w_NOME = left(SUBSTR(this.w_NOME,25,16)+space(20),20)
      * --- Il valore del sesso vieno messo a space(1)
      this.w_SESSO = space(1)
      * --- In questo caso sia il comune che la provincia vengono lasciati volutamente vuoti
      this.w_COMNASC = space(25)
      this.w_PROVNASC = space(2)
    endif
    this.w_RECORD = " 10"+this.w_PROGDEL+this.w_CODFIS+this.w_RAGSOCOG+this.w_NOME+this.w_SESSO+this.w_COMNASC
    this.w_RECORD = this.w_RECORD + this.w_PROVNASC+this.w_DATNASCC+this.w_PROTDEL+space(7)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NRECORD = this.w_NRECORD+1
    this.w_RECORD = space(120)
    this.w_COMFIS = left(nvl(this.w_COMFIS,space(30))+space(25),25)
    this.w_PROFIS = right(space(2)+nvl(this.w_PROFIS,space(2)),2)
    this.w_INDFIS = left(nvl(this.w_INDFIS,space(30))+space(35),35)
    * --- FLAG Anno di imposta non coincidente con anno solare
    this.w_DATARIF = cp_CharToDate("01-"+this.oParentObject.w_MESE+"-"+this.oParentObject.w_ANNO)
    this.w_CODESE = CALCESER(this.w_DATARIF,g_CODESE)
    this.w_DATCRE = NVL(this.w_DATCRE,cp_CharToDate(" - - "))
    this.w_DatSup = ALLTRIM(STR(YEAR(CP_TODATE(this.w_DATCRE)))) +RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.w_DATCRE)))),2)+ RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.w_DATCRE)))),2)
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESINIESE,ESFINESE"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_CODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESINIESE,ESFINESE;
        from (i_cTable) where;
            ESCODAZI = i_codazi;
            and ESCODESE = this.w_CODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INIPER = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
      this.w_FINEPER = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if right(dtoc(this.w_INIPER,1),4)<>"0101" or right(dtoc(this.w_FINEPER,1),4)<>"1231"
      this.w_APPCOIN = "1"
    else
      this.w_APPCOIN = "0"
    endif
    this.w_RECORD = " 20"+this.w_PROGDEL+this.w_COMFIS+this.w_PROFIS+this.w_INDFIS+this.w_DATSUP+this.w_APPCOIN+left(alltrim(this.w_CODFIS2)+space(16),16)+left(alltrim(this.w_CODIDEN)+space(2),2)+SPACE(21)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NRECORD = this.w_NRECORD+1
    this.w_RECORD = space(120)
    this.w_RIFTRIB2 = "0"
    i="1"
    * --- Read from MOD_PAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MFCODUFF,MFCODATT"+;
        " from "+i_cTable+" MOD_PAG where ";
            +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MFCODUFF,MFCODATT;
        from (i_cTable) where;
            MFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODUFF = NVL(cp_ToDate(_read_.MFCODUFF),cp_NullValue(_read_.MFCODUFF))
      this.w_CODATTO = NVL(cp_ToDate(_read_.MFCODATT),cp_NullValue(_read_.MFCODATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from MODEPAG
    i_nConn=i_TableProp[this.MODEPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODEPAG_idx,2],.t.,this.MODEPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MODEPAG ";
          +" where EFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MODEPAG")
    else
      select * from (i_cTable);
       where EFSERIAL = this.oParentObject.w_SERIALE;
        into cursor _Curs_MODEPAG
    endif
    if used('_Curs_MODEPAG')
      select _Curs_MODEPAG
      locate for 1=1
      do while not(eof())
      do while VAL(i) <= 6
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      enddo
        select _Curs_MODEPAG
        continue
      enddo
      use
    endif
    this.w_RECORD = space(120)
    * --- Read from MODEPAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODEPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODEPAG_idx,2],.t.,this.MODEPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "EFTOTDER,EFTOTCER,EFSALDER"+;
        " from "+i_cTable+" MODEPAG where ";
            +"EFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        EFTOTDER,EFTOTCER,EFSALDER;
        from (i_cTable) where;
            EFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.EFTOTDER),cp_NullValue(_read_.EFTOTDER))
      this.w_TOTCER = NVL(cp_ToDate(_read_.EFTOTCER),cp_NullValue(_read_.EFTOTCER))
      this.w_SALDER = NVL(cp_ToDate(_read_.EFSALDER),cp_NullValue(_read_.EFSALDER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"02"+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(62)
    if this.w_CONTA>0
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
    endif
    this.w_RECINSERT = .F.
    this.w_RECORD = SPACE(120)
    this.w_RIFTRIB2 = "00"
    K="1"
    * --- Select from MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_PAG ";
          +" where MFSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MOD_PAG")
    else
      select * from (i_cTable);
       where MFSERIAL=this.oParentObject.w_SERIALE;
        into cursor _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      select _Curs_MOD_PAG
      locate for 1=1
      do while not(eof())
      do while VAL(K) <= 4
        this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
        this.w_CODSEDE = LEFT(NVL(_Curs_MOD_PAG.MFCDSED&K,SPACE(4))+SPACE(4),4)
        this.w_CAUTRIB = LEFT(NVL(_Curs_MOD_PAG.MFCCONT&K,SPACE(4))+SPACE(4),4)
        this.w_MATRICO = LEFT(NVL(_Curs_MOD_PAG.MFMINPS&K,SPACE(17))+SPACE(17),17)
        this.w_MESEIN = RIGHT("00"+alltrim(NVL(_Curs_MOD_PAG.MFDAMES&K,"00")),2)
        this.w_ANNOIN = RIGHT("0000"+alltrim(NVL(_Curs_MOD_PAG.MFDAANN&K,"0000")),4)
        this.w_MESEFI = RIGHT("00"+alltrim(NVL(_Curs_MOD_PAG.MF_AMES&K,"00")),2)
        this.w_ANNOFI = RIGHT("0000"+alltrim(NVL(_Curs_MOD_PAG.MFAN&K,"0000")),4)
        this.w_IMPDEB = NVL(_Curs_MOD_PAG.MFIMPSD&K,0)
        this.w_Valore = this.w_IMPDEB
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS1 = this.w_VALORES
        this.w_IMPCRE = NVL(_Curs_MOD_PAG.MFIMPSC&K,0)
        this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMPCRE
        this.w_Valore = this.w_IMPCRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS2 = this.w_VALORES
        this.w_RECORD = " 40"+this.w_PROGDEL+"03"+this.w_RIFTRIB2+right("0000"+alltrim(this.w_CODSEDE),4)+this.w_CAUTRIB+this.w_MATRICO+this.w_MESEIN
        this.w_RECORD = this.w_RECORD+this.w_ANNOIN+this.w_MESEFI+this.w_ANNOFI+this.w_VALORES1
        this.w_RECORD = this.w_RECORD+this.w_VALORES2+space(39)
        K=ALLTRIM(STR(VAL(K) +1))
        if not empty(this.w_CODSEDE)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NRECORD = this.w_NRECORD+1
          this.w_RECINSERT = .T.
        endif
      enddo
        select _Curs_MOD_PAG
        continue
      enddo
      use
    endif
    this.w_RECORD = SPACE(120)
    * --- Read from MOD_PAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MFTOTDPS,MFTOTCPS,MFSALDPS"+;
        " from "+i_cTable+" MOD_PAG where ";
            +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MFTOTDPS,MFTOTCPS,MFSALDPS;
        from (i_cTable) where;
            MFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.MFTOTDPS),cp_NullValue(_read_.MFTOTDPS))
      this.w_TOTCER = NVL(cp_ToDate(_read_.MFTOTCPS),cp_NullValue(_read_.MFTOTCPS))
      this.w_SALDER = NVL(cp_ToDate(_read_.MFSALDPS),cp_NullValue(_read_.MFSALDPS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDOFINALE+this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"04"+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(62)
    if this.w_RECINSERT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
      this.w_RECINSERT = .F.
    endif
    this.w_RECORD = SPACE(120)
    this.w_RIFTRIB2 = "00"
    this.w_RIFTRIB = SPACE(8)
    this.w_RECINSERT = .F.
    K="1"
    * --- Select from MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_PAG ";
          +" where MFSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MOD_PAG")
    else
      select * from (i_cTable);
       where MFSERIAL=this.oParentObject.w_SERIALE;
        into cursor _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      select _Curs_MOD_PAG
      locate for 1=1
      do while not(eof())
      do while VAL(K) <= 4
        this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
        this.w_CODREG = LEFT(NVL(_Curs_MOD_PAG.MFCODRE&K,SPACE(2))+SPACE(2),2)
        this.w_CODTRIB = LEFT(NVL(_Curs_MOD_PAG.MFTRIRE&K,SPACE(4))+SPACE(4),4)
        if NOT EMPTY(NVL(_Curs_MOD_PAG.MFMESER&K,SPACE(2)))
          this.w_RIFTRIB = "00"+ALLTRIM(NVL(_Curs_MOD_PAG.MFMESER&K,"00"))+RIGHT(SPACE(4)+NVL(_Curs_MOD_PAG.MFANNRE&K,SPACE(4)),4)
        else
          this.w_RIFTRIB = RIGHT("0000"+ALLTRIM(NVL(_Curs_MOD_PAG.MFRATRE&K,"0000")),4)+RIGHT(SPACE(4)+NVL(_Curs_MOD_PAG.MFANNRE&K,SPACE(4)),4)
        endif
        this.w_IMDRE = NVL(_Curs_MOD_PAG.MFIMDRE&K,0)
        this.w_Valore = this.w_IMDRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS1 = this.w_VALORES
        this.w_IMCRE = NVL(_Curs_MOD_PAG.MFIMCRE&K,0)
        this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMCRE
        this.w_Valore = this.w_IMCRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS2 = this.w_VALORES
        this.w_RECORD = " 40"+this.w_PROGDEL+"05"+this.w_CODREG+this.w_RIFTRIB2+this.w_CODTRIB+this.w_RIFTRIB
        this.w_RECORD = this.w_RECORD+this.w_VALORES1+this.w_VALORES2+space(62)
        if NOT EMPTY(this.w_CODREG)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NRECORD = this.w_NRECORD+1
          this.w_RECINSERT = .T.
        endif
        K=ALLTRIM(STR(VAL(K) +1))
      enddo
        select _Curs_MOD_PAG
        continue
      enddo
      use
    endif
    this.w_RECORD = SPACE(120)
    * --- Read from MOD_PAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MFTOTDRE,MFTOTCRE,MFSALDRE"+;
        " from "+i_cTable+" MOD_PAG where ";
            +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MFTOTDRE,MFTOTCRE,MFSALDRE;
        from (i_cTable) where;
            MFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.MFTOTDRE),cp_NullValue(_read_.MFTOTDRE))
      this.w_TOTCER = NVL(cp_ToDate(_read_.MFTOTCRE),cp_NullValue(_read_.MFTOTCRE))
      this.w_SALDER = NVL(cp_ToDate(_read_.MFSALDRE),cp_NullValue(_read_.MFSALDRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDOFINALE+this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"06"+SPACE(2)+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(60)
    if this.w_RECINSERT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
      this.w_RECINSERT = .F.
    endif
    this.w_RECORD = SPACE(120)
    this.w_RIFTRIB2 = "00"
    this.w_RIFTRIB = SPACE(8)
    this.w_CODTRIB = SPACE(5)
    this.w_RECINSERT = .F.
    K="1"
    this.w_DETRAICI = "N"
    * --- Select from MODIPAG
    i_nConn=i_TableProp[this.MODIPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODIPAG_idx,2],.t.,this.MODIPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MODIPAG ";
          +" where IFSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MODIPAG")
    else
      select * from (i_cTable);
       where IFSERIAL=this.oParentObject.w_SERIALE;
        into cursor _Curs_MODIPAG
    endif
    if used('_Curs_MODIPAG')
      select _Curs_MODIPAG
      locate for 1=1
      do while not(eof())
      do while VAL(K) <= 4
        this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
        this.w_CODENTE = LEFT(NVL(_Curs_MODIPAG.IFCODEL&K,SPACE(4))+SPACE(4),4)
        this.w_CODTRIB = LEFT(NVL(_Curs_MODIPAG.IFTRIEL&K,SPACE(4))+SPACE(4),4)
        if INLIST(this.w_CODTRIB,"3901","3902","3903","3904","3905","3906","3907")
          this.w_DETRICI = _Curs_MODIPAG.IFDETICI
        else
          this.w_DETRICI = 0
        endif
        this.w_Valore = this.w_DETRICI
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Valorizza w_VALORES1 solo per il primo record con codice fisso "04-07" mentre per i restanti record w_VALORES1 non viene valorizzato
        if (this.w_CODTRIB="3901" OR this.w_CODTRIB="3904" ) AND this.w_DETRAICI="N"
          this.w_ValoreS1 =  this.w_VALORES
          this.w_DETRAICI = "S"
        else
          this.w_ValoreS1 = "000000000000000"
        endif
        if NOT EMPTY(NVL(_Curs_MODIPAG.IFMESER&k,""))
          this.w_RIFTRIB = "00"+alltrim(NVL(_Curs_MODIPAG.IFMESER&K,"00"))+LEFT(NVL(_Curs_MODIPAG.IFANNEL&K,SPACE(4))+SPACE(4),4)
        else
          this.w_RIFTRIB = RIGHT("0000"+alltrim(NVL(_Curs_MODIPAG.IFRATEL&K,"0000")),4)+LEFT(NVL(_Curs_MODIPAG.IFANNEL&K,SPACE(4))+SPACE(4),4)
        endif
        this.w_IMDRE = NVL(_Curs_MODIPAG.IFIMDEL&K,0)
        this.w_Valore = this.w_IMDRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS2 = this.w_VALORES
        this.w_IMCRE = NVL(_Curs_MODIPAG.IFIMCEL&K,0)
        this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMCRE
        this.w_Valore = this.w_IMCRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS3 = this.w_VALORES
        * --- Inserite le modifiche come richiesto dal nuovo tracciato del 12.12.02
        this.w_RAVVEDIM = NVL(_Curs_MODIPAG.IFRAVVE&K,0)
        this.w_IMMOBVAR = NVL(_Curs_MODIPAG.IFIMMOV&K,0)
        this.w_ACCONTO = NVL(_Curs_MODIPAG.IFACCON&K,0)
        this.w_SALDO = NVL(_Curs_MODIPAG.IFTSALD&K,0)
        this.w_NUMFAB = NVL(_Curs_MODIPAG.IFNUMFA&K,0)
        this.w_RECORD = " 40"+this.w_PROGDEL+"07"+this.w_CODENTE+this.w_RIFTRIB2+this.w_CODTRIB+this.w_RIFTRIB
        this.w_RECORD = this.w_RECORD+this.w_VALORES2+this.w_VALORES3
        this.w_RECORD = this.w_RECORD+right("0"+str(this.w_RAVVEDIM),1)+right("0"+str(this.w_IMMOBVAR),1)+right("0"+str(this.w_ACCONTO),1)
        this.w_RECORD = this.w_RECORD+right("0"+str(this.w_SALDO),1)+right("000"+alltrim(str(this.w_NUMFAB)),3)+this.w_VALORES1+space(38)
        if NOT EMPTY(this.w_CODENTE)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NRECORD = this.w_NRECORD+1
          this.w_RECINSERT = this.w_RECINSERT or this.w_DETRICI<>0
        endif
        K=ALLTRIM(STR(VAL(K) +1))
      enddo
        select _Curs_MODIPAG
        continue
      enddo
      use
    endif
    this.w_RECORD = SPACE(120)
    * --- Read from MODIPAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODIPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODIPAG_idx,2],.t.,this.MODIPAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IFTOTDEL,IFTOTCEL,IFSALDEL,IFCODEL1,IFCODEL2,IFCODEL3,IFCODEL4"+;
        " from "+i_cTable+" MODIPAG where ";
            +"IFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IFTOTDEL,IFTOTCEL,IFSALDEL,IFCODEL1,IFCODEL2,IFCODEL3,IFCODEL4;
        from (i_cTable) where;
            IFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.IFTOTDEL),cp_NullValue(_read_.IFTOTDEL))
      this.w_TOTCER = NVL(cp_ToDate(_read_.IFTOTCEL),cp_NullValue(_read_.IFTOTCEL))
      this.w_SALDER = NVL(cp_ToDate(_read_.IFSALDEL),cp_NullValue(_read_.IFSALDEL))
      this.w_CODEL1 = NVL(cp_ToDate(_read_.IFCODEL1),cp_NullValue(_read_.IFCODEL1))
      this.w_CODEL2 = NVL(cp_ToDate(_read_.IFCODEL2),cp_NullValue(_read_.IFCODEL2))
      this.w_CODEL3 = NVL(cp_ToDate(_read_.IFCODEL3),cp_NullValue(_read_.IFCODEL3))
      this.w_CODEL4 = NVL(cp_ToDate(_read_.IFCODEL4),cp_NullValue(_read_.IFCODEL4))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODEL = Alltrim(this.w_CODEL1)+Alltrim(this.w_CODEL2)+Alltrim(this.w_CODEL3)+Alltrim(this.w_CODEL4)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDOFINALE+this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"08"+SPACE(4)+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(58)
    if Not Empty(this.w_CODEL)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
    endif
    this.w_RECORD = SPACE(120)
    this.w_RIFTRIB2 = "00"
    this.w_RIFTRIB = SPACE(8)
    this.w_CODTRIB = SPACE(5)
    this.w_RECINSERT = .F.
    K="1"
    * --- Select from MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_PAG ";
          +" where MFSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MOD_PAG")
    else
      select * from (i_cTable);
       where MFSERIAL=this.oParentObject.w_SERIALE;
        into cursor _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      select _Curs_MOD_PAG
      locate for 1=1
      do while not(eof())
      do while VAL(K) <= 3
        this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
        this.w_CODENTE = LEFT(NVL(_Curs_MOD_PAG.MFSINAI&K,SPACE(2))+SPACE(5),5)
        this.w_CODTRIB = LEFT(NVL(_Curs_MOD_PAG.MF_NPOS&K,SPACE(5))+SPACE(8),8)
        this.w_CODCONT = LEFT(NVL(_Curs_MOD_PAG.MF_PACC&K,SPACE(2))+SPACE(2),2)
        if isalpha(substr(this.w_CODCONT,1,1)) or isalpha(substr(this.w_CODCONT,2,1))
          this.w_CODCONALF = .T.
          this.w_MESS = "Attenzione: nel flusso generato alcuni valori non rispettano il formato previsto dalla corrispondente posizione"
        endif
        this.w_CAUSALE = LEFT(NVL(_Curs_MOD_PAG.MFCAUSA&K,SPACE(1))+SPACE(1),1)
        this.w_NUMRIF = left(NVL(_Curs_MOD_PAG.MF_NRIF&K,SPACE(7))+space(6),6)
        this.w_IMDRE = NVL(_Curs_MOD_PAG.MFIMDIL&K,0)
        this.w_Valore = this.w_IMDRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS1 = this.w_VALORES
        this.w_IMCRE = NVL(_Curs_MOD_PAG.MFIMCIL&K,0)
        this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMCRE
        this.w_Valore = this.w_IMCRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS2 = this.w_VALORES
        this.w_RECORD = " 40"+this.w_PROGDEL+"09"+this.w_RIFTRIB2+right("00000"+alltrim(this.w_CODENTE),5)+right("00000000"+alltrim(this.w_CODTRIB),8)+this.w_CODCONT+this.w_CAUSALE
        this.w_RECORD = this.w_RECORD+space(4)+right("000000"+alltrim(this.w_NUMRIF),6)+this.w_VALORES1
        this.w_RECORD = this.w_RECORD+this.w_VALORES2+space(50)
        if NOT EMPTY(this.w_CODENTE)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NRECORD = this.w_NRECORD+1
          this.w_RECINSERT = .T.
        endif
        K=ALLTRIM(STR(VAL(K) +1))
      enddo
        select _Curs_MOD_PAG
        continue
      enddo
      use
    endif
    this.w_RECORD = SPACE(120)
    * --- Read from MOD_PAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MFTDINAI,MFTCINAI,MFSALINA"+;
        " from "+i_cTable+" MOD_PAG where ";
            +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MFTDINAI,MFTCINAI,MFSALINA;
        from (i_cTable) where;
            MFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.MFTDINAI),cp_NullValue(_read_.MFTDINAI))
      this.w_TOTCER = NVL(cp_ToDate(_read_.MFTCINAI),cp_NullValue(_read_.MFTCINAI))
      this.w_SALDER = NVL(cp_ToDate(_read_.MFSALINA),cp_NullValue(_read_.MFSALINA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDOFINALE+this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"10"+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(62)
    if this.w_RECINSERT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
      this.w_RECINSERT = .F.
    endif
    this.w_RECORD = SPACE(120)
    this.w_RIFTRIB2 = "00"
    this.w_RIFTRIB = SPACE(8)
    this.w_CODTRIB = SPACE(5)
    this.w_RECINSERT = .F.
    K="1"
    * --- Select from MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_PAG ";
          +" where MFSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
           ,"_Curs_MOD_PAG")
    else
      select * from (i_cTable);
       where MFSERIAL=this.oParentObject.w_SERIALE;
        into cursor _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      select _Curs_MOD_PAG
      locate for 1=1
      do while not(eof())
      do while VAL(K) <= 3
        this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
        this.w_CODENTE1 = LEFT(NVL(_Curs_MOD_PAG.MFCDENTE,SPACE(5))+SPACE(4),4)
        this.w_CODSEDE = LEFT(NVL(_Curs_MOD_PAG.MFSDENT&K,SPACE(5))+SPACE(5),5)
        this.w_CAUTRIB = LEFT(NVL(_Curs_MOD_PAG.MFCCOAE&K,SPACE(5))+SPACE(4),4)
        this.w_CODPOS = LEFT(NVL(_Curs_MOD_PAG.MFCDPOS&K,SPACE(12))+SPACE(9),9)
        this.w_MESEIN = LEFT(alltrim(NVL(_Curs_MOD_PAG.MFMSINE&K,"00"))+"00",2)
        this.w_ANNOIN = LEFT(alltrim(NVL(_Curs_MOD_PAG.MFANINE&K,"0000"))+"0000",4)
        this.w_MESEFI = LEFT(alltrim(NVL(_Curs_MOD_PAG.MFMSFIE&K,"00"))+"00",2)
        this.w_ANNOFI = LEFT(alltrim(NVL(_Curs_MOD_PAG.MFANF&K,"0000"))+"0000",4)
        this.w_IMDRE = NVL(_Curs_MOD_PAG.MFIMDAE&K,0)
        this.w_Valore = this.w_IMDRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ValoreS1 = this.w_VALORES
        this.w_IMCRE = NVL(_Curs_MOD_PAG.MFIMCAE&K,0)
        * --- Modifica inserita come richiesto nel nuovo tracciato del 12.12.02
        this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMCRE
        this.w_Valore = this.w_IMCRE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Se il campo "codice ente" � valorizzato con "0003" (INPDAP)  allora a posizioone 64-78  (importo a credito compensato) si mette 0
        this.w_ValoreS2 = IIF ( _Curs_MOD_PAG.MFCDENTE<>"0003" , this.w_VALORES , "000000000000000" )
        this.w_RECORD = " 40"+this.w_PROGDEL+"11"+this.w_RIFTRIB2+this.w_CODENTE1+this.w_CODSEDE+this.w_CAUTRIB+right(repl("0",9)+alltrim(this.w_CODPOS),9)+this.w_MESEIN
        this.w_RECORD = this.w_RECORD+this.w_ANNOIN+this.w_MESEFI+this.w_ANNOFI+this.w_VALORES1+this.w_VALORES2+space(42)
        if NOT EMPTY(this.w_CODPOS)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NRECORD = this.w_NRECORD+1
          this.w_RECINSERT = .T.
        endif
        K=ALLTRIM(STR(VAL(K) +1))
      enddo
        select _Curs_MOD_PAG
        continue
      enddo
      use
    endif
    this.w_RECORD = SPACE(120)
    * --- Read from MOD_PAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2],.t.,this.MOD_PAG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MFTDAENT,MFTCAENT,MFSALAEN"+;
        " from "+i_cTable+" MOD_PAG where ";
            +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MFTDAENT,MFTCAENT,MFSALAEN;
        from (i_cTable) where;
            MFSERIAL = this.oParentObject.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TOTDER = NVL(cp_ToDate(_read_.MFTDAENT),cp_NullValue(_read_.MFTDAENT))
      this.w_TOTCER = NVL(cp_ToDate(_read_.MFTCAENT),cp_NullValue(_read_.MFTCAENT))
      this.w_SALDER = NVL(cp_ToDate(_read_.MFSALAEN),cp_NullValue(_read_.MFSALAEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TOTDER = NVL(this.w_TOTDER,0)
    this.w_Valore = this.w_TOTDER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_TOTCER = NVL(this.w_TOTCER,0)
    this.w_Valore = this.w_TOTCER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_SALDER = NVL(this.w_SALDER,0)
    this.w_SALDOFINALE = this.w_SALDOFINALE+this.w_SALDER
    SEGNO=iif(this.w_SALDER<0,"N","P")
    this.w_Valore = ABS(this.w_SALDER)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS3 = this.w_VALORES
    this.w_RECORD = " 40"+this.w_PROGDEL+"12"+this.w_CODENTE1+this.w_VALORES1+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+SEGNO+this.w_VALORES3+space(58)
    if this.w_RECINSERT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
      this.w_RECINSERT = .F.
    endif
    * --- Record riservati a modelli F24 accise
    if this.oParentObject.w_MFTIPMOD= "ASSISE06" 
      this.w_MFSERIAL = this.oParentObject.w_SERIALE
      this.w_SALDACC = 0
      this.w_CONTA = 0
      VQ_EXEC("QUERY\GSCGASMF.VQR", this, "Ass") 
 Select Ass 
 Go Top
      Scan
      this.w_CONTA = this.w_CONTA + 1
      this.w_RECORD = " 40"+this.w_PROGDEL+"13"+ right ("00"+alltrim (str(this.w_CONTA)),2)+ " "+alltrim(AFCODENT)+alltrim(AFCODPRO)
      this.w_RECORD = this.w_RECORD + substr (alltrim(AFTRIBUT),1,4) + substr (alltrim(AFCODICE)+space(14),1,14)
      this.w_RECORD = this.w_RECORD + right ( "00" + alltrim(AF__MESE) , 2) + substr (alltrim (AF__ANNO) + space(4),1,4)
      this.w_RECORD = this.w_RECORD + right ("000000000000000"+alltrim(str (AFIMPORT * 100)),15) +"000000000000000" + space (48)
      this.w_SALDACC = this.w_SALDACC + AFIMPORT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
      ENDSCAN
      this.w_RECORD = " 40"+this.w_PROGDEL+"14"+ right ("000000000000000"+alltrim (str (this.w_SALDACC * 100)),15) +"000000000000000"+"P"+ right ("000000000000000"+alltrim (str (this.w_SALDACC * 100)),15) + space (62)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
    endif
    * --- Generazione Record Pagamento
    this.w_RECORD = SPACE(120)
    this.w_DATCRE = NVL(this.w_DATCRE,cp_CharToDate(" - - "))
    this.w_DatSup = ALLTRIM(STR(YEAR(CP_TODATE(this.w_DATCRE)))) +RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.w_DATCRE)))),2)+ RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.w_DATCRE)))),2)
    this.w_Valore = this.w_SALDOFINALE
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_Valore = this.w_TOTCREVER
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_RECORD = " 50"+this.w_PROGDEL+"01"+this.w_CODABI+this.w_CODCAB+this.w_CONCOR+this.w_CIN+this.w_VALORES1
    this.w_RECORD = this.w_RECORD+this.oParentObject.w_FLGFIRM+space(2)+right(space(16)+this.oParentObject.w_CFTITCC,16)+this.oParentObject.w_TITCCPAG+this.w_DATSUP+this.w_VALORES2
    this.w_RECORD = this.w_RECORD+space(2)+"3"+space(24)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NRECORD = this.w_NRECORD+1
    this.w_RECORD = SPACE(120)
    this.w_RECORD = " 50"+this.w_PROGDEL+"02"+substr (this.w_COFAZI,1,16)+"    "+this.w_AZBANABI+this.w_AZBANCAB+space(20)+this.oParentObject.w_INATTPAG+UPPER(left(g_RagAzi+space(45),45))+space(12)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NRECORD = this.w_NRECORD+1
    * --- Controllo se l'utente nella maschera di elaborazione ha selezionato come
    *     Attestazione di Pagamento: Mittente fisico
    if this.oParentObject.w_INATTPAG="2"
      this.w_RECORD = SPACE(120)
      this.w_RECORD = " 50"+this.w_PROGDEL+"03"+right( "00000" + trim(this.w_SE___CAP) , 5)+left(this.w_SELOCALI+ space(25) , 25)
      this.w_RECORD = this.w_RECORD+left(this.w_SEPROVIN+space(2), 2)+left(this.w_SEINDIRI+ space(34) ,34)+space(42)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NRECORD = this.w_NRECORD+1
    endif
    * --- Record di coda
    this.w_RECORD = SPACE(120)
    this.w_NRECORD = this.w_NRECORD+1
    this.w_DatSup = RIGHT("00"+ALLTRIM(STR(DAY(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+RIGHT("00"+ALLTRIM(STR(MONTH(CP_TODATE(this.oParentObject.w_DATACREA)))),2)+RIGHT(ALLTRIM(STR(YEAR(CP_TODATE(this.oParentObject.w_DATACREA)))),2)
    this.w_Valore = this.w_SALDOFINALE
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_RECORD = " EF"+this.w_BANPROSIA+this.w_CODABI+this.w_DATSUP+this.w_NOMSUP+SPACE(6)+"0000001"+this.w_VALORES1
    this.w_RECORD = this.w_RECORD+"000000000000000"+right("0000000"+alltrim(str(this.w_NRECORD,7)),7)+SPACE(24)+"E"+SPACE(6)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorno il nome del supporto nella gestione del modello F24
    * --- Write into MOD_PAG
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOD_PAG_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MFNOMSUP ="+cp_NullLink(cp_ToStrODBC(this.w_NOMSUP),'MOD_PAG','MFNOMSUP');
          +i_ccchkf ;
      +" where ";
          +"MFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             )
    else
      update (i_cTable) set;
          MFNOMSUP = this.w_NOMSUP;
          &i_ccchkf. ;
       where;
          MFSERIAL = this.oParentObject.w_SERIALE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Chiusura del file ascii contenente i flussi
    ah_msg("Fine generazione flusso") 
    if .not. fclose(this.w_Handle) 
      NAMEFILE='"'+this.w_NOMEFILE+'"'
      DELETE FILE &NAMEFILE
      AH_ERRORMSG("Generazione file ascii per modello F24 fallita",48)
    endif
    * --- Controllo sul titolare del c/c di pagamento e il saldo finale della delega F24
    if this.oParentObject.w_TITCCPAG="1" and this.w_SALDOFINALE<>0
      NAMEFILE='"'+this.w_NOMEFILE+'"'
      DELETE FILE &NAMEFILE
      AH_ERRORMSG("Attenzione: selezione sul titolare del c/c di addebito incongruente con il saldo del modello%0Impossibile generare il file",48)
    else
      Ah_ErrorMsg("� stato generato il flusso: %1",48,"",this.w_NOMEFILE) 
 
      if this.w_CODCONALF
        AH_ERRORMSG(this.w_MESS,48)
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione record Sez. Erario
    this.w_CODTRIB = left(nvl(_Curs_MODEPAG.EFTRIER&i,space(5))+space(4),4)
    if not empty(NVL(_Curs_MODEPAG.EFMESER&i,""))
      this.w_RIFTRIB = "00"+alltrim(nvl(_Curs_MODEPAG.EFMESER&i,"00"))+left(nvl(_Curs_MODEPAG.EFANNER&i,space(4))+space(4),4)
    else
      this.w_RIFTRIB = right("0000"+alltrim(nvl(_Curs_MODEPAG.EFRATER&i,"0000")),4)+left(nvl(_Curs_MODEPAG.EFANNER&i,space(4))+space(4),4)
    endif
    this.w_IMPDEBV = nvl(_Curs_MODEPAG.EFIMDER&i,0)
    this.w_Valore = this.w_IMPDEBV
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS1 = this.w_VALORES
    this.w_IMPCREV = nvl(_Curs_MODEPAG.EFIMCER&i,0)
    this.w_TOTCREVER = this.w_TOTCREVER+this.w_IMPCREV
    this.w_Valore = this.w_IMPCREV
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ValoreS2 = this.w_VALORES
    this.w_RIFTRIB2 = RIGHT("00"+ALLTRIM(STR(VAL(this.w_RIFTRIB2)+1)),2)
    if not empty(this.w_CODTRIB)
      this.w_RECORD = " 40"+this.w_PROGDEL+"01"+this.w_RIFTRIB2+this.w_CODTRIB+this.w_RIFTRIB+this.w_VALORES1
      this.w_RECORD = this.w_RECORD+this.w_VALORES2+RIGHT(SPACE(3)+NVL(this.w_CODUFF,""),3)
      this.w_RECORD = this.w_RECORD +IIF (EMPTY ( NVL(this.w_CODATTO," ") ) ,REPL(" ",11) , RIGHT(REPL("0",11)+trim(NVL(this.w_CODATTO,"0")),11)) +SPACE(50)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTA = this.w_CONTA + 1
      this.w_NRECORD = this.w_NRECORD+1
      this.w_RECORD = SPACE(120)
    endif
    i=ALLTRIM(STR(VAL(i)+1))
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiudo e cancello il file relativo al flusso F24
    fclose(this.w_Handle)
    NAMEFILE='"'+this.w_NOMEFILE+'"'
    DELETE FILE &NAMEFILE
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converto gli importi nel formato richiesto dal tracciato
    this.w_Lunghezza = 15
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    * --- Risultato
    this.w_ValoreS = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='MODCPAG'
    this.cWorkTables[5]='MOD_PAG'
    this.cWorkTables[6]='MODEPAG'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='MODVPAG'
    this.cWorkTables[9]='MODIPAG'
    this.cWorkTables[10]='SEDIAZIE'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_MODEPAG')
      use in _Curs_MODEPAG
    endif
    if used('_Curs_MOD_PAG')
      use in _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      use in _Curs_MOD_PAG
    endif
    if used('_Curs_MODIPAG')
      use in _Curs_MODIPAG
    endif
    if used('_Curs_MOD_PAG')
      use in _Curs_MOD_PAG
    endif
    if used('_Curs_MOD_PAG')
      use in _Curs_MOD_PAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
