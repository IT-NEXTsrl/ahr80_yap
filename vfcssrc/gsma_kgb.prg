* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kgb                                                        *
*              Generazione massiva barcode                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_277]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-09                                                      *
* Last revis.: 2014-04-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kgb",oParentObject))

* --- Class definition
define class tgsma_kgb as StdForm
  Top    = 5
  Left   = 13

  * --- Standard Properties
  Width  = 467
  Height = 515+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-04-07"
  HelpContextID=32144233
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  VOCIIVA_IDX = 0
  GRUMERC_IDX = 0
  CACOARTI_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  CLA_RICA_IDX = 0
  CAT_SCMA_IDX = 0
  MARCHI_IDX = 0
  GRUPRO_IDX = 0
  VOC_COST_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  TIP_COLL_IDX = 0
  TIPICONF_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  REP_ARTI_IDX = 0
  TIPCODIV_IDX = 0
  TIPCONTR_IDX = 0
  CATMCONT_IDX = 0
  UNIMIS_IDX = 0
  cPrg = "gsma_kgb"
  cComment = "Generazione massiva barcode"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPARTI = space(2)
  o_TIPARTI = space(2)
  w_PROVART = space(1)
  o_PROVART = space(1)
  w_ARTOBS = space(1)
  o_ARTOBS = space(1)
  w_ARTINI = space(20)
  o_ARTINI = space(20)
  w_DESART = space(35)
  w_ARTFIN = space(20)
  o_ARTFIN = space(20)
  w_DESART1 = space(35)
  w_IVAFIL = space(5)
  o_IVAFIL = space(5)
  w_IVADES = space(35)
  w_GRMFIL = space(5)
  o_GRMFIL = space(5)
  w_GRMDES = space(35)
  w_CATFIL = space(5)
  o_CATFIL = space(5)
  w_CATDES = space(35)
  w_FAMFIL = space(5)
  o_FAMFIL = space(5)
  w_FAMDES = space(35)
  w_CATOFIL = space(5)
  o_CATOFIL = space(5)
  w_CATODES = space(35)
  w_PROFIL = space(15)
  o_PROFIL = space(15)
  w_PRODES = space(35)
  w_FORFIL = space(15)
  o_FORFIL = space(15)
  w_FORDES = space(35)
  w_MARFIL = space(5)
  o_MARFIL = space(5)
  w_MARDES = space(35)
  w_COLFIL = space(5)
  o_COLFIL = space(5)
  w_CONFIL = space(3)
  o_CONFIL = space(3)
  w_COLDES = space(35)
  w_CONDES = space(35)
  w_REPFIL = space(3)
  o_REPFIL = space(3)
  w_REPDES = space(35)
  w_SELECTALL = space(1)
  o_SELECTALL = space(1)
  w_TIPBAR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_FILTERS = .F.
  w_DATOBSO = ctod('  /  /  ')
  w_TIPSEL = space(1)
  w_ARTIPART = space(2)
  w_TIPCON = space(1)
  w_ZoomArt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kgbPag1","gsma_kgb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsma_kgbPag2","gsma_kgb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Articoli")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPARTI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomArt = this.oPgFrm.Pages(2).oPag.ZoomArt
    DoDefault()
    proc Destroy()
      this.w_ZoomArt = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[25]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='PAR_RIOR'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CACOARTI'
    this.cWorkTables[6]='FAM_ARTI'
    this.cWorkTables[7]='CATEGOMO'
    this.cWorkTables[8]='CLA_RICA'
    this.cWorkTables[9]='CAT_SCMA'
    this.cWorkTables[10]='MARCHI'
    this.cWorkTables[11]='GRUPRO'
    this.cWorkTables[12]='VOC_COST'
    this.cWorkTables[13]='CONTI'
    this.cWorkTables[14]='KEY_ARTI'
    this.cWorkTables[15]='TIP_COLL'
    this.cWorkTables[16]='TIPICONF'
    this.cWorkTables[17]='INVENTAR'
    this.cWorkTables[18]='ESERCIZI'
    this.cWorkTables[19]='LISTINI'
    this.cWorkTables[20]='VALUTE'
    this.cWorkTables[21]='REP_ARTI'
    this.cWorkTables[22]='TIPCODIV'
    this.cWorkTables[23]='TIPCONTR'
    this.cWorkTables[24]='CATMCONT'
    this.cWorkTables[25]='UNIMIS'
    return(this.OpenAllTables(25))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSMA_BVA with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPARTI=space(2)
      .w_PROVART=space(1)
      .w_ARTOBS=space(1)
      .w_ARTINI=space(20)
      .w_DESART=space(35)
      .w_ARTFIN=space(20)
      .w_DESART1=space(35)
      .w_IVAFIL=space(5)
      .w_IVADES=space(35)
      .w_GRMFIL=space(5)
      .w_GRMDES=space(35)
      .w_CATFIL=space(5)
      .w_CATDES=space(35)
      .w_FAMFIL=space(5)
      .w_FAMDES=space(35)
      .w_CATOFIL=space(5)
      .w_CATODES=space(35)
      .w_PROFIL=space(15)
      .w_PRODES=space(35)
      .w_FORFIL=space(15)
      .w_FORDES=space(35)
      .w_MARFIL=space(5)
      .w_MARDES=space(35)
      .w_COLFIL=space(5)
      .w_CONFIL=space(3)
      .w_COLDES=space(35)
      .w_CONDES=space(35)
      .w_REPFIL=space(3)
      .w_REPDES=space(35)
      .w_SELECTALL=space(1)
      .w_TIPBAR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_FILTERS=.f.
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPSEL=space(1)
      .w_ARTIPART=space(2)
      .w_TIPCON=space(1)
        .w_TIPARTI = '  '
        .w_PROVART = ' '
        .w_ARTOBS = 'V'
        .w_ARTINI = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTINI)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ARTINI))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_ARTFIN = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTFIN)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ARTFIN))
          .link_1_8('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_IVAFIL))
          .link_1_10('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_GRMFIL))
          .link_1_12('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CATFIL))
          .link_1_16('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_FAMFIL))
          .link_1_18('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_CATOFIL))
          .link_1_22('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_PROFIL))
          .link_1_24('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_FORFIL))
          .link_1_28('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_MARFIL))
          .link_1_30('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_COLFIL))
          .link_1_38('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CONFIL))
          .link_1_39('Full')
        endif
        .DoRTCalc(26,28,.f.)
        if not(empty(.w_REPFIL))
          .link_1_42('Full')
        endif
      .oPgFrm.Page2.oPag.ZoomArt.Calculate()
          .DoRTCalc(29,30,.f.)
        .w_TIPBAR = '1'
        .w_OBTEST = i_datsys
        .w_FILTERS = .T.
          .DoRTCalc(34,34,.f.)
        .w_TIPSEL = 'A'
        .w_ARTIPART = '  '
        .w_TIPCON = 'F'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_ARTOBS<>.w_ARTOBS
            .w_ARTINI = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTINI)
          .link_1_6('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_ARTOBS<>.w_ARTOBS
            .w_ARTFIN = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTFIN)
          .link_1_8('Full')
        endif
        .oPgFrm.Page2.oPag.ZoomArt.Calculate()
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4
        l_Dep1= .o_TIPARTI<>.w_TIPARTI .or. .o_PROVART<>.w_PROVART .or. .o_ARTOBS<>.w_ARTOBS .or. .o_ARTINI<>.w_ARTINI .or. .o_ARTFIN<>.w_ARTFIN
        l_Dep2= .o_IVAFIL<>.w_IVAFIL .or. .o_GRMFIL<>.w_GRMFIL .or. .o_CATFIL<>.w_CATFIL .or. .o_FAMFIL<>.w_FAMFIL .or. .o_CATOFIL<>.w_CATOFIL
        l_Dep3= .o_PROFIL<>.w_PROFIL .or. .o_FORFIL<>.w_FORFIL .or. .o_MARFIL<>.w_MARFIL .or. .o_COLFIL<>.w_COLFIL .or. .o_CONFIL<>.w_CONFIL
        l_Dep4= .o_REPFIL<>.w_REPFIL
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4
          .Calculate_LGEWANBFUS()
        endif
        if .o_SELECTALL<>.w_SELECTALL
          .Calculate_MOLZSPWHKR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomArt.Calculate()
    endwith
  return

  proc Calculate_DWPKTEKVGU()
    with this
          * --- Aggiornamento zoom se filtri cambiati
      if .w_FILTERS
          GSMA_BGB(this;
              ,'F';
             )
      endif
          .w_FILTERS = .F.
    endwith
  endproc
  proc Calculate_LGEWANBFUS()
    with this
          * --- Registra se i filtri sono variati
          .w_FILTERS = .T.
    endwith
  endproc
  proc Calculate_MOLZSPWHKR()
    with this
          * --- seleziona/deseleziona tutto
          GSMA_BGB(this;
              ,'S';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oREPFIL_1_42.enabled = this.oPgFrm.Page1.oPag.oREPFIL_1_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Articoli"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oTIPARTI_1_1.visible=!this.oPgFrm.Page1.oPag.oTIPARTI_1_1.mHide()
    this.oPgFrm.Page1.oPag.oGRMFIL_1_12.visible=!this.oPgFrm.Page1.oPag.oGRMFIL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oGRMDES_1_13.visible=!this.oPgFrm.Page1.oPag.oGRMDES_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCOLDES_1_40.visible=!this.oPgFrm.Page1.oPag.oCOLDES_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCONDES_1_41.visible=!this.oPgFrm.Page1.oPag.oCONDES_1_41.mHide()
    this.oPgFrm.Page1.oPag.oREPFIL_1_42.visible=!this.oPgFrm.Page1.oPag.oREPFIL_1_42.mHide()
    this.oPgFrm.Page1.oPag.oREPDES_1_43.visible=!this.oPgFrm.Page1.oPag.oREPDES_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomArt.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_DWPKTEKVGU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ARTINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_1_6'),i_cWhere,'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESART = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (.w_ARTOBS='T' OR (.w_ARTOBS='V' AND (Empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) OR (.w_ARTOBS='O' AND Not Empty(.w_DATOBSO)  AND .w_DATOBSO<=.w_OBTEST))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto o inesistente")
        endif
        this.w_ARTINI = space(20)
        this.w_DESART = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_1_8'),i_cWhere,'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESART1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (.w_ARTOBS='T' OR (.w_ARTOBS='V' AND (Empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) OR (.w_ARTOBS='O' AND Not Empty(.w_DATOBSO)  AND .w_DATOBSO<=.w_OBTEST))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto o inesistente")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESART1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IVAFIL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVAFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVAFIL))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVAFIL)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IVAFIL) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVAFIL_1_10'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAFIL)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAFIL = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVADES = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IVAFIL = space(5)
      endif
      this.w_IVADES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto")
        endif
        this.w_IVAFIL = space(5)
        this.w_IVADES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRMFIL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRMFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRMFIL))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRMFIL)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRMFIL) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRMFIL_1_12'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRMFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRMFIL)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRMFIL = NVL(_Link_.GMCODICE,space(5))
      this.w_GRMDES = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRMFIL = space(5)
      endif
      this.w_GRMDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATFIL))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIL)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIL) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATFIL_1_16'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATFIL)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIL = NVL(_Link_.C1CODICE,space(5))
      this.w_CATDES = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIL = space(5)
      endif
      this.w_CATDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMFIL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMFIL))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMFIL)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMFIL) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMFIL_1_18'),i_cWhere,'GSAR_AFA',"Codici famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMFIL)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMFIL = NVL(_Link_.FACODICE,space(5))
      this.w_FAMDES = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMFIL = space(5)
      endif
      this.w_FAMDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOFIL
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOFIL))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOFIL)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOFIL) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOFIL_1_22'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOFIL)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOFIL = NVL(_Link_.OMCODICE,space(5))
      this.w_CATODES = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOFIL = space(5)
      endif
      this.w_CATODES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROFIL
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PROFIL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PROFIL))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROFIL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROFIL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPROFIL_1_24'),i_cWhere,'GSAR_BZC',"Produttori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PROFIL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PROFIL)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROFIL = NVL(_Link_.ANCODICE,space(15))
      this.w_PRODES = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PROFIL = space(15)
      endif
      this.w_PRODES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORFIL
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORFIL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORFIL))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORFIL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORFIL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORFIL_1_28'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORFIL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORFIL)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORFIL = NVL(_Link_.ANCODICE,space(15))
      this.w_FORDES = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FORFIL = space(15)
      endif
      this.w_FORDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIL
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIL))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIL)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIL) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIL_1_30'),i_cWhere,'GSAR_AMH',"Codici marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIL)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIL = NVL(_Link_.MACODICE,space(5))
      this.w_MARDES = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIL = space(5)
      endif
      this.w_MARDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COLFIL
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COLFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_COLFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_COLFIL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COLFIL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COLFIL) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCOLFIL_1_38'),i_cWhere,'GSAR_MTO',"Codici tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COLFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_COLFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_COLFIL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COLFIL = NVL(_Link_.TCCODICE,space(5))
      this.w_COLDES = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COLFIL = space(5)
      endif
      this.w_COLDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COLFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONFIL
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATC',True,'TIPICONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CONFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CONFIL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONFIL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONFIL) and !this.bDontReportError
            deferred_cp_zoom('TIPICONF','*','TCCODICE',cp_AbsName(oSource.parent,'oCONFIL_1_39'),i_cWhere,'GSAR_ATC',"Codici tipi confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CONFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CONFIL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONFIL = NVL(_Link_.TCCODICE,space(3))
      this.w_CONDES = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CONFIL = space(3)
      endif
      this.w_CONDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=REPFIL
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REPFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_REPFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_REPFIL))
          select RECODREP,REDESREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REPFIL)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_REPFIL) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oREPFIL_1_42'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REPFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_REPFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_REPFIL)
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REPFIL = NVL(_Link_.RECODREP,space(3))
      this.w_REPDES = NVL(_Link_.REDESREP,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_REPFIL = space(3)
      endif
      this.w_REPDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REPFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPARTI_1_1.RadioValue()==this.w_TIPARTI)
      this.oPgFrm.Page1.oPag.oTIPARTI_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVART_1_2.RadioValue()==this.w_PROVART)
      this.oPgFrm.Page1.oPag.oPROVART_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTOBS_1_3.RadioValue()==this.w_ARTOBS)
      this.oPgFrm.Page1.oPag.oARTOBS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTINI_1_6.value==this.w_ARTINI)
      this.oPgFrm.Page1.oPag.oARTINI_1_6.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oARTFIN_1_8.value==this.w_ARTFIN)
      this.oPgFrm.Page1.oPag.oARTFIN_1_8.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART1_1_9.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oDESART1_1_9.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page1.oPag.oIVAFIL_1_10.value==this.w_IVAFIL)
      this.oPgFrm.Page1.oPag.oIVAFIL_1_10.value=this.w_IVAFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oIVADES_1_11.value==this.w_IVADES)
      this.oPgFrm.Page1.oPag.oIVADES_1_11.value=this.w_IVADES
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMFIL_1_12.value==this.w_GRMFIL)
      this.oPgFrm.Page1.oPag.oGRMFIL_1_12.value=this.w_GRMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMDES_1_13.value==this.w_GRMDES)
      this.oPgFrm.Page1.oPag.oGRMDES_1_13.value=this.w_GRMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIL_1_16.value==this.w_CATFIL)
      this.oPgFrm.Page1.oPag.oCATFIL_1_16.value=this.w_CATFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDES_1_17.value==this.w_CATDES)
      this.oPgFrm.Page1.oPag.oCATDES_1_17.value=this.w_CATDES
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMFIL_1_18.value==this.w_FAMFIL)
      this.oPgFrm.Page1.oPag.oFAMFIL_1_18.value=this.w_FAMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMDES_1_19.value==this.w_FAMDES)
      this.oPgFrm.Page1.oPag.oFAMDES_1_19.value=this.w_FAMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOFIL_1_22.value==this.w_CATOFIL)
      this.oPgFrm.Page1.oPag.oCATOFIL_1_22.value=this.w_CATOFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCATODES_1_23.value==this.w_CATODES)
      this.oPgFrm.Page1.oPag.oCATODES_1_23.value=this.w_CATODES
    endif
    if not(this.oPgFrm.Page1.oPag.oPROFIL_1_24.value==this.w_PROFIL)
      this.oPgFrm.Page1.oPag.oPROFIL_1_24.value=this.w_PROFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oPRODES_1_25.value==this.w_PRODES)
      this.oPgFrm.Page1.oPag.oPRODES_1_25.value=this.w_PRODES
    endif
    if not(this.oPgFrm.Page1.oPag.oFORFIL_1_28.value==this.w_FORFIL)
      this.oPgFrm.Page1.oPag.oFORFIL_1_28.value=this.w_FORFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFORDES_1_29.value==this.w_FORDES)
      this.oPgFrm.Page1.oPag.oFORDES_1_29.value=this.w_FORDES
    endif
    if not(this.oPgFrm.Page1.oPag.oMARFIL_1_30.value==this.w_MARFIL)
      this.oPgFrm.Page1.oPag.oMARFIL_1_30.value=this.w_MARFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oMARDES_1_31.value==this.w_MARDES)
      this.oPgFrm.Page1.oPag.oMARDES_1_31.value=this.w_MARDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLFIL_1_38.value==this.w_COLFIL)
      this.oPgFrm.Page1.oPag.oCOLFIL_1_38.value=this.w_COLFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFIL_1_39.value==this.w_CONFIL)
      this.oPgFrm.Page1.oPag.oCONFIL_1_39.value=this.w_CONFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLDES_1_40.value==this.w_COLDES)
      this.oPgFrm.Page1.oPag.oCOLDES_1_40.value=this.w_COLDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCONDES_1_41.value==this.w_CONDES)
      this.oPgFrm.Page1.oPag.oCONDES_1_41.value=this.w_CONDES
    endif
    if not(this.oPgFrm.Page1.oPag.oREPFIL_1_42.value==this.w_REPFIL)
      this.oPgFrm.Page1.oPag.oREPFIL_1_42.value=this.w_REPFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oREPDES_1_43.value==this.w_REPDES)
      this.oPgFrm.Page1.oPag.oREPDES_1_43.value=this.w_REPDES
    endif
    if not(this.oPgFrm.Page2.oPag.oSELECTALL_2_6.RadioValue()==this.w_SELECTALL)
      this.oPgFrm.Page2.oPag.oSELECTALL_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPBAR_2_7.RadioValue()==this.w_TIPBAR)
      this.oPgFrm.Page2.oPag.oTIPBAR_2_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (.w_ARTOBS='T' OR (.w_ARTOBS='V' AND (Empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) OR (.w_ARTOBS='O' AND Not Empty(.w_DATOBSO)  AND .w_DATOBSO<=.w_OBTEST)))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto o inesistente")
          case   not(((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (.w_ARTOBS='T' OR (.w_ARTOBS='V' AND (Empty(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) OR (.w_ARTOBS='O' AND Not Empty(.w_DATOBSO)  AND .w_DATOBSO<=.w_OBTEST)))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto o inesistente")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.))  and not(empty(.w_IVAFIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVAFIL_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPARTI = this.w_TIPARTI
    this.o_PROVART = this.w_PROVART
    this.o_ARTOBS = this.w_ARTOBS
    this.o_ARTINI = this.w_ARTINI
    this.o_ARTFIN = this.w_ARTFIN
    this.o_IVAFIL = this.w_IVAFIL
    this.o_GRMFIL = this.w_GRMFIL
    this.o_CATFIL = this.w_CATFIL
    this.o_FAMFIL = this.w_FAMFIL
    this.o_CATOFIL = this.w_CATOFIL
    this.o_PROFIL = this.w_PROFIL
    this.o_FORFIL = this.w_FORFIL
    this.o_MARFIL = this.w_MARFIL
    this.o_COLFIL = this.w_COLFIL
    this.o_CONFIL = this.w_CONFIL
    this.o_REPFIL = this.w_REPFIL
    this.o_SELECTALL = this.w_SELECTALL
    return

enddefine

* --- Define pages as container
define class tgsma_kgbPag1 as StdContainer
  Width  = 463
  height = 515
  stdWidth  = 463
  stdheight = 515
  resizeXpos=348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPARTI_1_1 as StdCombo with uid="OQEKYITJPE",value=1,rtseq=1,rtrep=.f.,left=170,top=9,width=131,height=21;
    , ToolTipText = "Tipo articolo";
    , HelpContextID = 125555510;
    , cFormVar="w_TIPARTI",RowSource=""+"Tutti,"+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPARTI_1_1.RadioValue()
    return(iif(this.value =1,'  ',;
    iif(this.value =2,'PF',;
    iif(this.value =3,'SE',;
    iif(this.value =4,'MP',;
    iif(this.value =5,'PH',;
    space(2)))))))
  endfunc
  func oTIPARTI_1_1.GetRadio()
    this.Parent.oContained.w_TIPARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPARTI_1_1.SetRadio()
    this.Parent.oContained.w_TIPARTI=trim(this.Parent.oContained.w_TIPARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARTI=='',1,;
      iif(this.Parent.oContained.w_TIPARTI=='PF',2,;
      iif(this.Parent.oContained.w_TIPARTI=='SE',3,;
      iif(this.Parent.oContained.w_TIPARTI=='MP',4,;
      iif(this.Parent.oContained.w_TIPARTI=='PH',5,;
      0)))))
  endfunc

  func oTIPARTI_1_1.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc


  add object oPROVART_1_2 as StdCombo with uid="CQGEYTHWLH",value=1,rtseq=2,rtrep=.f.,left=170,top=37,width=131,height=21;
    , ToolTipText = "Provenienza articolo";
    , HelpContextID = 75549686;
    , cFormVar="w_PROVART",RowSource=""+"Tutti,"+"Interna,"+"Esterna,"+"C/Lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVART_1_2.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'I',;
    iif(this.value =3,'E',;
    iif(this.value =4,'L',;
    space(1))))))
  endfunc
  func oPROVART_1_2.GetRadio()
    this.Parent.oContained.w_PROVART = this.RadioValue()
    return .t.
  endfunc

  func oPROVART_1_2.SetRadio()
    this.Parent.oContained.w_PROVART=trim(this.Parent.oContained.w_PROVART)
    this.value = ;
      iif(this.Parent.oContained.w_PROVART=='',1,;
      iif(this.Parent.oContained.w_PROVART=='I',2,;
      iif(this.Parent.oContained.w_PROVART=='E',3,;
      iif(this.Parent.oContained.w_PROVART=='L',4,;
      0))))
  endfunc


  add object oARTOBS_1_3 as StdCombo with uid="LQUEDYBAOC",rtseq=3,rtrep=.f.,left=170,top=65,width=101,height=21;
    , ToolTipText = "Tipo di articolo: solo validi, solo obsoleti, tutti";
    , HelpContextID = 175498490;
    , cFormVar="w_ARTOBS",RowSource=""+"Solo validi,"+"Solo obsoleti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTOBS_1_3.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'O',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARTOBS_1_3.GetRadio()
    this.Parent.oContained.w_ARTOBS = this.RadioValue()
    return .t.
  endfunc

  func oARTOBS_1_3.SetRadio()
    this.Parent.oContained.w_ARTOBS=trim(this.Parent.oContained.w_ARTOBS)
    this.value = ;
      iif(this.Parent.oContained.w_ARTOBS=='V',1,;
      iif(this.Parent.oContained.w_ARTOBS=='O',2,;
      iif(this.Parent.oContained.w_ARTOBS=='T',3,;
      0)))
  endfunc

  add object oARTINI_1_6 as StdField with uid="CGGTFZRXYO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto o inesistente",;
    ToolTipText = "Codice articolo/servizio iniziale",;
    HelpContextID = 62645498,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=170, Top=94, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART_1_7 as StdField with uid="MSUWPXJTWC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142868938,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=94, InputMask=replicate('X',35)

  add object oARTFIN_1_8 as StdField with uid="LXUPTNNEWH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto o inesistente",;
    ToolTipText = "Codice articolo/servizio finale",;
    HelpContextID = 252634362,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=170, Top=122, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART1_1_9 as StdField with uid="NARWPVVQDH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142868938,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=122, InputMask=replicate('X',35)

  add object oIVAFIL_1_10 as StdField with uid="GVNSUHDTUL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IVAFIL", cQueryName = "IVAFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto",;
    ToolTipText = "Codice IVA",;
    HelpContextID = 17830010,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVAFIL"

  func oIVAFIL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVAFIL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVAFIL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVAFIL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oIVAFIL_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVAFIL
     i_obj.ecpSave()
  endproc

  add object oIVADES_1_11 as StdField with uid="VFQRTDSPTW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IVADES", cQueryName = "IVADES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173150330,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=150, InputMask=replicate('X',35)

  add object oGRMFIL_1_12 as StdField with uid="PRTZOHWUZW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GRMFIL", cQueryName = "GRMFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico",;
    HelpContextID = 17781914,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=178, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRMFIL"

  func oGRMFIL_1_12.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oGRMFIL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRMFIL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRMFIL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRMFIL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRMFIL_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRMFIL
     i_obj.ecpSave()
  endproc

  add object oGRMDES_1_13 as StdField with uid="MRDLTRJBSO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GRMDES", cQueryName = "GRMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173102234,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=178, InputMask=replicate('X',35)

  func oGRMDES_1_13.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATFIL_1_16 as StdField with uid="XBFFRKXSTI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATFIL", cQueryName = "CATFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categ. contabile",;
    HelpContextID = 17757658,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=208, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATFIL"

  func oCATFIL_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIL_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIL_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATFIL_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oCATFIL_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATFIL
     i_obj.ecpSave()
  endproc

  add object oCATDES_1_17 as StdField with uid="XTEAKXXLNB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CATDES", cQueryName = "CATDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173077978,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=208, InputMask=replicate('X',35)

  add object oFAMFIL_1_18 as StdField with uid="PMUNPVFYIV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FAMFIL", cQueryName = "FAMFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia",;
    HelpContextID = 17786282,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=236, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMFIL"

  func oFAMFIL_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMFIL_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMFIL_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMFIL_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Codici famiglie",'',this.parent.oContained
  endproc
  proc oFAMFIL_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMFIL
     i_obj.ecpSave()
  endproc

  add object oFAMDES_1_19 as StdField with uid="CGXLYUHRXC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FAMDES", cQueryName = "FAMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173106602,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=236, InputMask=replicate('X',35)

  add object oCATOFIL_1_22 as StdField with uid="YAZRXVZRLN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CATOFIL", cQueryName = "CATOFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categ. omogenea",;
    HelpContextID = 70645210,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=264, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOFIL"

  func oCATOFIL_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOFIL_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOFIL_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOFIL_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOFIL_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOFIL
     i_obj.ecpSave()
  endproc

  add object oCATODES_1_23 as StdField with uid="RCAUBZPPJS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CATODES", cQueryName = "CATODES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 128584230,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=264, InputMask=replicate('X',35)

  add object oPROFIL_1_24 as StdField with uid="OSTIWNMGOI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PROFIL", cQueryName = "PROFIL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice produttore",;
    HelpContextID = 17773578,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=170, Top=292, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PROFIL"

  func oPROFIL_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROFIL_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROFIL_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPROFIL_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Produttori",'',this.parent.oContained
  endproc
  proc oPROFIL_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PROFIL
     i_obj.ecpSave()
  endproc

  add object oPRODES_1_25 as StdField with uid="UXPEAGUHOB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PRODES", cQueryName = "PRODES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173093898,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=292, InputMask=replicate('X',35)

  add object oFORFIL_1_28 as StdField with uid="YODBPGPIZH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FORFIL", cQueryName = "FORFIL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore abituale",;
    HelpContextID = 17762218,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=170, Top=320, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORFIL"

  func oFORFIL_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORFIL_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORFIL_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORFIL_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORFIL_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORFIL
     i_obj.ecpSave()
  endproc

  add object oFORDES_1_29 as StdField with uid="HJMOBGXIYG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FORDES", cQueryName = "FORDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173082538,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=320, InputMask=replicate('X',35)

  add object oMARFIL_1_30 as StdField with uid="ZHQUBXRTJK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MARFIL", cQueryName = "MARFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 17765690,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=348, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIL"

  func oMARFIL_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIL_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIL_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIL_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Codici marchi",'',this.parent.oContained
  endproc
  proc oMARFIL_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARFIL
     i_obj.ecpSave()
  endproc

  add object oMARDES_1_31 as StdField with uid="GZJRQHFKFN",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MARDES", cQueryName = "MARDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173086010,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=348, InputMask=replicate('X',35)


  add object oBtn_1_34 as StdButton with uid="XAOFRYAIID",left=358, top=467, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 144778006;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSMA_BGB(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="KYQESPJULE",left=410, top=467, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24826810;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLFIL_1_38 as StdField with uid="SRLDIEZGFW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COLFIL", cQueryName = "COLFIL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia collo di selezione",;
    HelpContextID = 17786842,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=376, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_COLFIL"

  func oCOLFIL_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOLFIL_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOLFIL_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCOLFIL_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Codici tipologie colli",'',this.parent.oContained
  endproc
  proc oCOLFIL_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_COLFIL
     i_obj.ecpSave()
  endproc

  add object oCONFIL_1_39 as StdField with uid="SEPUWZSWSC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CONFIL", cQueryName = "CONFIL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice confezione di selezione",;
    HelpContextID = 17778650,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=404, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPICONF", cZoomOnZoom="GSAR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CONFIL"

  func oCONFIL_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONFIL_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONFIL_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPICONF','*','TCCODICE',cp_AbsName(this.parent,'oCONFIL_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATC',"Codici tipi confezioni",'',this.parent.oContained
  endproc
  proc oCONFIL_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CONFIL
     i_obj.ecpSave()
  endproc

  add object oCOLDES_1_40 as StdField with uid="NBFQVIXYVZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COLDES", cQueryName = "COLDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173107162,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=376, InputMask=replicate('X',35)

  func oCOLDES_1_40.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCONDES_1_41 as StdField with uid="XHRUZOIOMR",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CONDES", cQueryName = "CONDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173098970,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=404, InputMask=replicate('X',35)

  func oCONDES_1_41.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oREPFIL_1_42 as StdField with uid="UQMNHIQAHQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_REPFIL", cQueryName = "REPFIL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice reparto",;
    HelpContextID = 17772778,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=432, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_REPFIL"

  func oREPFIL_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPFIL_1_42.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oREPFIL_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oREPFIL_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREPFIL_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oREPFIL_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oREPFIL_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_REPFIL
     i_obj.ecpSave()
  endproc

  add object oREPDES_1_43 as StdField with uid="UNTSKOGGHC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_REPDES", cQueryName = "REPDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173093098,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=331, Top=432, InputMask=replicate('X',35)

  func oREPDES_1_43.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="PHXWTWHRDJ",Visible=.t., Left=46, Top=94,;
    Alignment=1, Width=120, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="KXXUPOONKM",Visible=.t., Left=46, Top=122,;
    Alignment=1, Width=120, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="EJCXMUIDIK",Visible=.t., Left=46, Top=150,;
    Alignment=1, Width=120, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QPPQEBMYFK",Visible=.t., Left=46, Top=178,;
    Alignment=1, Width=120, Height=18,;
    Caption="Gr.merceologico:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="BFATWZNSLW",Visible=.t., Left=46, Top=208,;
    Alignment=1, Width=120, Height=18,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CUAXWLJUPY",Visible=.t., Left=46, Top=236,;
    Alignment=1, Width=120, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="UOILFNIOXV",Visible=.t., Left=46, Top=264,;
    Alignment=1, Width=120, Height=18,;
    Caption="Cat.omogenea:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="IUASSYVPKA",Visible=.t., Left=46, Top=292,;
    Alignment=1, Width=120, Height=18,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="NHIUVTQTZB",Visible=.t., Left=46, Top=320,;
    Alignment=1, Width=120, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="DMSIMQDKTU",Visible=.t., Left=46, Top=348,;
    Alignment=1, Width=120, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="KCGICDYKJY",Visible=.t., Left=46, Top=376,;
    Alignment=1, Width=120, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="MZBTQCFRXA",Visible=.t., Left=46, Top=404,;
    Alignment=1, Width=120, Height=18,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="TMRPMZQBFI",Visible=.t., Left=46, Top=432,;
    Alignment=1, Width=120, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="GXHLJUJXIN",Visible=.t., Left=46, Top=67,;
    Alignment=1, Width=120, Height=18,;
    Caption="Filtro obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ULJBAZMVZP",Visible=.t., Left=46, Top=39,;
    Alignment=1, Width=120, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="GVKNOWZSTN",Visible=.t., Left=46, Top=11,;
    Alignment=1, Width=120, Height=18,;
    Caption="Tipo articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (g_PROD<>'S')
    endwith
  endfunc
enddefine
define class tgsma_kgbPag2 as StdContainer
  Width  = 463
  height = 515
  stdWidth  = 463
  stdheight = 515
  resizeXpos=170
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomArt as cp_szoombox with uid="QFHXEJVQOH",left=0, top=2, width=458,height=455,;
    caption='ZOOMART',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="ART_ICOL",bNoZoomGridShape=.f.,cZoomFile="GSMA_KGB",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bQueryOnLoad=.t.,;
    cEvent = "Init,Filtri",;
    nPag=2;
    , HelpContextID = 74959254


  add object oBtn_2_2 as StdButton with uid="AHAMZOFFER",left=358, top=467, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 157168026;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSMA_BGB(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="MNHKJBNIOE",left=410, top=467, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24826810;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELECTALL_2_6 as StdRadio with uid="LCHDSMCWCG",rtseq=30,rtrep=.f.,left=12, top=460, width=181,height=33;
    , cFormVar="w_SELECTALL", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELECTALL_2_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 158362574
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 158362574
      this.Buttons(2).Top=15
      this.SetAll("Width",179)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELECTALL_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELECTALL_2_6.GetRadio()
    this.Parent.oContained.w_SELECTALL = this.RadioValue()
    return .t.
  endfunc

  func oSELECTALL_2_6.SetRadio()
    this.Parent.oContained.w_SELECTALL=trim(this.Parent.oContained.w_SELECTALL)
    this.value = ;
      iif(this.Parent.oContained.w_SELECTALL=='S',1,;
      iif(this.Parent.oContained.w_SELECTALL=='D',2,;
      0))
  endfunc


  add object oTIPBAR_2_7 as StdCombo with uid="NELTDQQVSE",rtseq=31,rtrep=.f.,left=281,top=467,width=64,height=22;
    , HelpContextID = 194194634;
    , cFormVar="w_TIPBAR",RowSource=""+"Ean 8,"+"Ean 13", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPBAR_2_7.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    '08')))
  endfunc
  func oTIPBAR_2_7.GetRadio()
    this.Parent.oContained.w_TIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPBAR_2_7.SetRadio()
    this.Parent.oContained.w_TIPBAR=trim(this.Parent.oContained.w_TIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPBAR=='1',1,;
      iif(this.Parent.oContained.w_TIPBAR=='2',2,;
      0))
  endfunc

  add object oStr_2_8 as StdString with uid="SBWJOUFKWD",Visible=.t., Left=184, Top=471,;
    Alignment=1, Width=96, Height=18,;
    Caption="Codifica barcode:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kgb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
