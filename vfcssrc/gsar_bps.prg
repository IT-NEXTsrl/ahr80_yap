* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bps                                                        *
*              Carica clienti pos/offerte                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-17                                                      *
* Last revis.: 2014-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bps",oParentObject)
return(i_retval)

define class tgsar_bps as StdBatch
  * --- Local variables
  w_DANOM = .f.
  w_CODICE = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso di Modulo POS e check Cliente POS attivati lancia il batch per il caricamento automeatico Clienti Negozio
    this.w_CODICE = this.oParentObject.w_ANCODICE
    this.w_DANOM = this.oparentobject.w_DANOM
    if g_GPOS="S" AND this.oParentObject.w_ANCLIPOS="S" AND !this.w_DANOM
      GSPS_BCP(this,this.w_CODICE,"C")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (g_OFFE="S" or g_AGEN="S" or g_PRAT="S") and !this.w_DANOM
      GSAR_BOL(this,"C",this.oParentObject.w_ANCODICE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
