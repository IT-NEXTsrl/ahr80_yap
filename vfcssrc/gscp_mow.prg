* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_mow                                                        *
*              Ordini via web                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_162]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2016-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_mow"))

* --- Class definition
define class tgscp_mow as StdTrsForm
  Top    = 4
  Left   = 13

  * --- Standard Properties
  Width  = 757
  Height = 430+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-20"
  HelpContextID=164205929
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=100

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ZORDWEBM_IDX = 0
  ZORDWEBD_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  DES_DIVE_IDX = 0
  VETTORI_IDX = 0
  PORTI_IDX = 0
  MODASPED_IDX = 0
  PAG_AMEN_IDX = 0
  NAZIONI_IDX = 0
  CPUSERS_IDX = 0
  cFile = "ZORDWEBM"
  cFileDetail = "ZORDWEBD"
  cKeySelect = "ORSERIAL"
  cKeyWhere  = "ORSERIAL=this.w_ORSERIAL"
  cKeyDetail  = "ORSERIAL=this.w_ORSERIAL"
  cKeyWhereODBC = '"ORSERIAL="+cp_ToStrODBC(this.w_ORSERIAL)';

  cKeyDetailWhereODBC = '"ORSERIAL="+cp_ToStrODBC(this.w_ORSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"ZORDWEBD.ORSERIAL="+cp_ToStrODBC(this.w_ORSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ZORDWEBD.CPROWORD '
  cPrg = "gscp_mow"
  cComment = "Ordini via web"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OR_STATO = 0
  w_ORSERIAL = space(10)
  w_ORTIPDOC = space(5)
  w_ORNUMDOC = 0
  w_ORALFDOC = space(10)
  w_ORCODICE = space(40)
  w_CPROWORD = 0
  o_CPROWORD = 0
  w_ORTIPRIG = space(1)
  w_ORCODART = space(20)
  w_ORDESSUP = space(0)
  w_ORUNIMIS = space(3)
  w_ORQTAMOV = 0
  w_ORQTAUM1 = 0
  w_ORPREZZO = 0
  w_ORCODIVA = space(5)
  w_ORFLOMAG = space(1)
  w_ORDATEVA = ctod('  /  /  ')
  w_ORCODLIS = space(5)
  w_ORCONTRA = space(15)
  w_TDDESDOC = space(35)
  w_ORNUMEST = 0
  w_ORALFEST = space(10)
  w_ORDATEST = ctod('  /  /  ')
  w_ORTIPCON = space(1)
  w_ORCODCON = space(15)
  w_ORCODVAL = space(3)
  o_ORCODVAL = space(3)
  w_ANDESCRI = space(40)
  w_ORDATDOC = ctod('  /  /  ')
  w_ORSCOCL2 = 0
  w_ORFLSCOR = space(1)
  w_ORSCOPAG = 0
  w_ORSCOCL1 = 0
  w_ORSPEINC = 0
  w_ORSPETRA = 0
  w_ORSPEIMB = 0
  w_ORCODPAG = space(5)
  w_ORDESART = space(40)
  w_ORTOTORD = 0
  w_VASIMVAL = space(5)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_OR_EMAIL = space(0)
  w_ORFLMAIL = space(1)
  w_ORMERISP = space(0)
  w_DDCODNAZ = space(3)
  w_ANTIPCON = space(1)
  w_ORCONCON = space(1)
  w_PADESCRI = space(30)
  w_ORSCONTI = 0
  w_ORTDTEVA = ctod('  /  /  ')
  w_VASIMVAL = space(5)
  w_ORSCOIN1 = 0
  w_ORSCOIN2 = 0
  w_ORSCOIN3 = 0
  w_ORSCOIN4 = 0
  w_VADECUNI = 0
  w_VADECTOT = 0
  w_CALCPICT = 0
  w_CALCPICP = 0
  w_ORCODAGE = space(5)
  w_AGDESAGE = space(35)
  w_ORSCONT1 = 0
  w_ORSCONT2 = 0
  w_ORSCONT3 = 0
  w_ORSCONT4 = 0
  w_CPROWORD = 0
  w_ORSCONT1 = 0
  w_ORSCONT2 = 0
  w_ORSCONT3 = 0
  w_ORSCONT4 = 0
  w_ORCODDES = space(5)
  w_ORCODSPE = space(3)
  w_ORCODVET = space(5)
  w_ORCODPOR = space(1)
  w_OR__NOTE = space(0)
  w_DDNOMDES = space(40)
  w_DDINDIRI = space(35)
  w_DD___CAP = space(5)
  w_DDLOCALI = space(30)
  w_DDPROVIN = space(2)
  w_VTDESVET = space(35)
  w_PODESPOR = space(30)
  w_SPDESSPE = space(35)
  w_CONCONDES = space(20)
  w_NADESNAZ = space(35)
  w_ORCODVIS = space(40)
  w_ORLOGSTM = space(5)
  w_ORLOGSTD = space(5)
  w_ORUMNODI = 0
  w_ORTOTRIG = 0
  w_ORTOTGEN = 0
  w_ORTOTRIP = 0
  w_ORRIFEST = space(40)
  w_ORVALNAZ = space(3)
  w_ORCAOVAL = 0
  w_ORACCONT = 0
  w_ORVALACC = space(3)
  w_ORSPEBOL = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscp_mow
  proc ecpQuery()
        * ----
        DoDefault()
        this.oPgFrm.Pages(4).caption=''
        this.oPgFrm.Pages(4).enabled=.f.
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ZORDWEBM','gscp_mow')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_mowPag1","gscp_mow",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documento")
      .Pages(1).HelpContextID = 42079642
      .Pages(2).addobject("oPag","tgscp_mowPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati generali")
      .Pages(2).HelpContextID = 156657420
      .Pages(3).addobject("oPag","tgscp_mowPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Spedizione")
      .Pages(3).HelpContextID = 147743627
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='VETTORI'
    this.cWorkTables[7]='PORTI'
    this.cWorkTables[8]='MODASPED'
    this.cWorkTables[9]='PAG_AMEN'
    this.cWorkTables[10]='NAZIONI'
    this.cWorkTables[11]='CPUSERS'
    this.cWorkTables[12]='ZORDWEBM'
    this.cWorkTables[13]='ZORDWEBD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(13))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZORDWEBM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZORDWEBM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ORSERIAL = NVL(ORSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_4_13_joined
    link_4_13_joined=.f.
    local link_4_35_joined
    link_4_35_joined=.f.
    local link_5_3_joined
    link_5_3_joined=.f.
    local link_5_5_joined
    link_5_5_joined=.f.
    local link_5_7_joined
    link_5_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ZORDWEBM where ORSERIAL=KeySet.ORSERIAL
    *
    i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2],this.bLoadRecFilter,this.ZORDWEBM_IDX,"gscp_mow")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZORDWEBM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZORDWEBM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ZORDWEBD.","ZORDWEBM.")
      i_cTable = i_cTable+' ZORDWEBM '
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_13_joined=this.AddJoinedLink_4_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_35_joined=this.AddJoinedLink_4_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_3_joined=this.AddJoinedLink_5_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_5_joined=this.AddJoinedLink_5_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_7_joined=this.AddJoinedLink_5_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ORSERIAL',this.w_ORSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TDDESDOC = space(35)
        .w_ANDESCRI = space(40)
        .w_VASIMVAL = space(5)
        .w_DDCODNAZ = space(3)
        .w_ANTIPCON = 'C'
        .w_PADESCRI = space(30)
        .w_VASIMVAL = space(5)
        .w_VADECUNI = 0
        .w_VADECTOT = 0
        .w_AGDESAGE = space(35)
        .w_DDNOMDES = space(40)
        .w_DDINDIRI = space(35)
        .w_DD___CAP = space(5)
        .w_DDLOCALI = space(30)
        .w_DDPROVIN = space(2)
        .w_VTDESVET = space(35)
        .w_PODESPOR = space(30)
        .w_SPDESSPE = space(35)
        .w_NADESNAZ = space(35)
        .w_ORTOTGEN = 0
        .w_OR_STATO = NVL(OR_STATO,0)
        .w_ORSERIAL = NVL(ORSERIAL,space(10))
        .w_ORTIPDOC = NVL(ORTIPDOC,space(5))
          .link_1_3('Load')
        .w_ORNUMDOC = NVL(ORNUMDOC,0)
        .w_ORALFDOC = NVL(ORALFDOC,space(10))
        .w_ORNUMEST = NVL(ORNUMEST,0)
        .w_ORALFEST = NVL(ORALFEST,space(10))
        .w_ORDATEST = NVL(cp_ToDate(ORDATEST),ctod("  /  /  "))
        .w_ORTIPCON = NVL(ORTIPCON,space(1))
        .w_ORCODCON = NVL(ORCODCON,space(15))
          if link_1_17_joined
            this.w_ORCODCON = NVL(ANCODICE117,NVL(this.w_ORCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI117,space(40))
          else
          .link_1_17('Load')
          endif
        .w_ORCODVAL = NVL(ORCODVAL,space(3))
          if link_1_18_joined
            this.w_ORCODVAL = NVL(VACODVAL118,NVL(this.w_ORCODVAL,space(3)))
            this.w_VASIMVAL = NVL(VASIMVAL118,space(5))
            this.w_VADECUNI = NVL(VADECUNI118,0)
            this.w_VADECTOT = NVL(VADECTOT118,0)
          else
          .link_1_18('Load')
          endif
        .w_ORDATDOC = NVL(cp_ToDate(ORDATDOC),ctod("  /  /  "))
        .w_ORSCOCL2 = NVL(ORSCOCL2,0)
        .w_ORFLSCOR = NVL(ORFLSCOR,space(1))
        .w_ORSCOPAG = NVL(ORSCOPAG,0)
        .w_ORSCOCL1 = NVL(ORSCOCL1,0)
        .w_ORSPEINC = NVL(ORSPEINC,0)
        .w_ORSPETRA = NVL(ORSPETRA,0)
        .w_ORSPEIMB = NVL(ORSPEIMB,0)
        .w_ORCODPAG = NVL(ORCODPAG,space(5))
          if link_4_13_joined
            this.w_ORCODPAG = NVL(PACODICE413,NVL(this.w_ORCODPAG,space(5)))
            this.w_PADESCRI = NVL(PADESCRI413,space(30))
          else
          .link_4_13('Load')
          endif
        .w_ORTOTORD = NVL(ORTOTORD,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_OR_EMAIL = NVL(OR_EMAIL,space(0))
        .w_ORFLMAIL = NVL(ORFLMAIL,space(1))
        .w_ORMERISP = NVL(ORMERISP,space(0))
          .link_4_29('Load')
        .w_ORCONCON = NVL(ORCONCON,space(1))
        .w_ORSCONTI = NVL(ORSCONTI,0)
        .w_ORTDTEVA = NVL(cp_ToDate(ORTDTEVA),ctod("  /  /  "))
        .w_CALCPICT = DEFPIC(.w_VADECTOT)
        .w_CALCPICP = DEFPIP(.w_VADECUNI)
        .w_ORCODAGE = NVL(ORCODAGE,space(5))
          if link_4_35_joined
            this.w_ORCODAGE = NVL(AGCODAGE435,NVL(this.w_ORCODAGE,space(5)))
            this.w_AGDESAGE = NVL(AGDESAGE435,space(35))
          else
          .link_4_35('Load')
          endif
        .w_ORCODDES = NVL(ORCODDES,space(5))
          .link_5_1('Load')
        .w_ORCODSPE = NVL(ORCODSPE,space(3))
          if link_5_3_joined
            this.w_ORCODSPE = NVL(SPCODSPE503,NVL(this.w_ORCODSPE,space(3)))
            this.w_SPDESSPE = NVL(SPDESSPE503,space(35))
          else
          .link_5_3('Load')
          endif
        .w_ORCODVET = NVL(ORCODVET,space(5))
          if link_5_5_joined
            this.w_ORCODVET = NVL(VTCODVET505,NVL(this.w_ORCODVET,space(5)))
            this.w_VTDESVET = NVL(VTDESVET505,space(35))
          else
          .link_5_5('Load')
          endif
        .w_ORCODPOR = NVL(ORCODPOR,space(1))
          if link_5_7_joined
            this.w_ORCODPOR = NVL(POCODPOR507,NVL(this.w_ORCODPOR,space(1)))
            this.w_PODESPOR = NVL(PODESPOR507,space(30))
          else
          .link_5_7('Load')
          endif
        .w_OR__NOTE = NVL(OR__NOTE,space(0))
        .w_CONCONDES = iif(.w_ORCONCON='E','Franco Fabbrica',iif(.w_ORCONCON='F','Franco Vettore',iif(.w_ORCONCON='C','Costo e Nolo',iif(.w_ORCONCON='D','Reso',''))))
        .w_ORLOGSTM = NVL(ORLOGSTM,space(5))
        .w_ORTOTRIP = .w_ORTOTGEN
        .w_ORRIFEST = NVL(ORRIFEST,space(40))
        .w_ORVALNAZ = NVL(ORVALNAZ,space(3))
        .w_ORCAOVAL = NVL(ORCAOVAL,0)
        .w_ORACCONT = NVL(ORACCONT,0)
        .w_ORVALACC = NVL(ORVALACC,space(3))
        .w_ORSPEBOL = NVL(ORSPEBOL,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ZORDWEBM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ZORDWEBD where ORSERIAL=KeySet.ORSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ZORDWEBD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ZORDWEBD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ZORDWEBD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ZORDWEBD"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'ORSERIAL',this.w_ORSERIAL  )
        select * from (i_cTable) ZORDWEBD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_ORTOTGEN = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_ORCODICE = NVL(ORCODICE,space(40))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ORTIPRIG = NVL(ORTIPRIG,space(1))
          .w_ORCODART = NVL(ORCODART,space(20))
          .w_ORDESSUP = NVL(ORDESSUP,space(0))
          .w_ORUNIMIS = NVL(ORUNIMIS,space(3))
          .w_ORQTAMOV = NVL(ORQTAMOV,0)
          .w_ORQTAUM1 = NVL(ORQTAUM1,0)
          .w_ORPREZZO = NVL(ORPREZZO,0)
          .w_ORCODIVA = NVL(ORCODIVA,space(5))
          .w_ORFLOMAG = NVL(ORFLOMAG,space(1))
          .w_ORDATEVA = NVL(cp_ToDate(ORDATEVA),ctod("  /  /  "))
          .w_ORCODLIS = NVL(ORCODLIS,space(5))
          .w_ORCONTRA = NVL(ORCONTRA,space(15))
          .w_ORDESART = NVL(ORDESART,space(40))
          .w_ORSCOIN1 = NVL(ORSCOIN1,0)
          .w_ORSCOIN2 = NVL(ORSCOIN2,0)
          .w_ORSCOIN3 = NVL(ORSCOIN3,0)
          .w_ORSCOIN4 = NVL(ORSCOIN4,0)
          .w_ORSCONT1 = NVL(ORSCONT1,0)
          .w_ORSCONT2 = NVL(ORSCONT2,0)
          .w_ORSCONT3 = NVL(ORSCONT3,0)
          .w_ORSCONT4 = NVL(ORSCONT4,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ORSCONT1 = NVL(ORSCONT1,0)
          .w_ORSCONT2 = NVL(ORSCONT2,0)
          .w_ORSCONT3 = NVL(ORSCONT3,0)
          .w_ORSCONT4 = NVL(ORSCONT4,0)
        .w_ORCODVIS = IIF(.w_ORTIPRIG='D',g_ARTDES,.w_ORCODICE)
          .w_ORLOGSTD = NVL(ORLOGSTD,space(5))
          .w_ORUMNODI = NVL(ORUMNODI,0)
        .w_ORTOTRIG = .w_ORQTAMOV* cp_ROUND(.w_ORPREZZO * (1+.w_ORSCONT1/100)*(1+.w_ORSCONT2/100)*(1+.w_ORSCONT3/100)*(1+.w_ORSCONT4/100), .w_VADECTOT)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_ORTOTGEN = .w_ORTOTGEN+.w_ORTOTRIG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CALCPICT = DEFPIC(.w_VADECTOT)
        .w_CALCPICP = DEFPIP(.w_VADECUNI)
        .w_CONCONDES = iif(.w_ORCONCON='E','Franco Fabbrica',iif(.w_ORCONCON='F','Franco Vettore',iif(.w_ORCONCON='C','Costo e Nolo',iif(.w_ORCONCON='D','Reso',''))))
        .w_ORTOTRIP = .w_ORTOTGEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_29.enabled = .oPgFrm.Page1.oPag.oBtn_2_29.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_OR_STATO=0
      .w_ORSERIAL=space(10)
      .w_ORTIPDOC=space(5)
      .w_ORNUMDOC=0
      .w_ORALFDOC=space(10)
      .w_ORCODICE=space(40)
      .w_CPROWORD=10
      .w_ORTIPRIG=space(1)
      .w_ORCODART=space(20)
      .w_ORDESSUP=space(0)
      .w_ORUNIMIS=space(3)
      .w_ORQTAMOV=0
      .w_ORQTAUM1=0
      .w_ORPREZZO=0
      .w_ORCODIVA=space(5)
      .w_ORFLOMAG=space(1)
      .w_ORDATEVA=ctod("  /  /  ")
      .w_ORCODLIS=space(5)
      .w_ORCONTRA=space(15)
      .w_TDDESDOC=space(35)
      .w_ORNUMEST=0
      .w_ORALFEST=space(10)
      .w_ORDATEST=ctod("  /  /  ")
      .w_ORTIPCON=space(1)
      .w_ORCODCON=space(15)
      .w_ORCODVAL=space(3)
      .w_ANDESCRI=space(40)
      .w_ORDATDOC=ctod("  /  /  ")
      .w_ORSCOCL2=0
      .w_ORFLSCOR=space(1)
      .w_ORSCOPAG=0
      .w_ORSCOCL1=0
      .w_ORSPEINC=0
      .w_ORSPETRA=0
      .w_ORSPEIMB=0
      .w_ORCODPAG=space(5)
      .w_ORDESART=space(40)
      .w_ORTOTORD=0
      .w_VASIMVAL=space(5)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      .w_OR_EMAIL=space(0)
      .w_ORFLMAIL=space(1)
      .w_ORMERISP=space(0)
      .w_DDCODNAZ=space(3)
      .w_ANTIPCON=space(1)
      .w_ORCONCON=space(1)
      .w_PADESCRI=space(30)
      .w_ORSCONTI=0
      .w_ORTDTEVA=ctod("  /  /  ")
      .w_VASIMVAL=space(5)
      .w_ORSCOIN1=0
      .w_ORSCOIN2=0
      .w_ORSCOIN3=0
      .w_ORSCOIN4=0
      .w_VADECUNI=0
      .w_VADECTOT=0
      .w_CALCPICT=0
      .w_CALCPICP=0
      .w_ORCODAGE=space(5)
      .w_AGDESAGE=space(35)
      .w_ORSCONT1=0
      .w_ORSCONT2=0
      .w_ORSCONT3=0
      .w_ORSCONT4=0
      .w_CPROWORD=10
      .w_ORSCONT1=0
      .w_ORSCONT2=0
      .w_ORSCONT3=0
      .w_ORSCONT4=0
      .w_ORCODDES=space(5)
      .w_ORCODSPE=space(3)
      .w_ORCODVET=space(5)
      .w_ORCODPOR=space(1)
      .w_OR__NOTE=space(0)
      .w_DDNOMDES=space(40)
      .w_DDINDIRI=space(35)
      .w_DD___CAP=space(5)
      .w_DDLOCALI=space(30)
      .w_DDPROVIN=space(2)
      .w_VTDESVET=space(35)
      .w_PODESPOR=space(30)
      .w_SPDESSPE=space(35)
      .w_CONCONDES=space(20)
      .w_NADESNAZ=space(35)
      .w_ORCODVIS=space(40)
      .w_ORLOGSTM=space(5)
      .w_ORLOGSTD=space(5)
      .w_ORUMNODI=0
      .w_ORTOTRIG=0
      .w_ORTOTGEN=0
      .w_ORTOTRIP=0
      .w_ORRIFEST=space(40)
      .w_ORVALNAZ=space(3)
      .w_ORCAOVAL=0
      .w_ORACCONT=0
      .w_ORVALACC=space(3)
      .w_ORSPEBOL=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_ORTIPDOC))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,25,.f.)
        if not(empty(.w_ORCODCON))
         .link_1_17('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_ORCODVAL))
         .link_1_18('Full')
        endif
        .DoRTCalc(27,36,.f.)
        if not(empty(.w_ORCODPAG))
         .link_4_13('Full')
        endif
        .DoRTCalc(37,47,.f.)
        if not(empty(.w_DDCODNAZ))
         .link_4_29('Full')
        endif
        .w_ANTIPCON = 'C'
        .DoRTCalc(49,59,.f.)
        .w_CALCPICT = DEFPIC(.w_VADECTOT)
        .w_CALCPICP = DEFPIP(.w_VADECUNI)
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_ORCODAGE))
         .link_4_35('Full')
        endif
        .DoRTCalc(63,73,.f.)
        if not(empty(.w_ORCODDES))
         .link_5_1('Full')
        endif
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_ORCODSPE))
         .link_5_3('Full')
        endif
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_ORCODVET))
         .link_5_5('Full')
        endif
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_ORCODPOR))
         .link_5_7('Full')
        endif
        .DoRTCalc(77,85,.f.)
        .w_CONCONDES = iif(.w_ORCONCON='E','Franco Fabbrica',iif(.w_ORCONCON='F','Franco Vettore',iif(.w_ORCONCON='C','Costo e Nolo',iif(.w_ORCONCON='D','Reso',''))))
        .DoRTCalc(87,87,.f.)
        .w_ORCODVIS = IIF(.w_ORTIPRIG='D',g_ARTDES,.w_ORCODICE)
        .DoRTCalc(89,91,.f.)
        .w_ORTOTRIG = .w_ORQTAMOV* cp_ROUND(.w_ORPREZZO * (1+.w_ORSCONT1/100)*(1+.w_ORSCONT2/100)*(1+.w_ORSCONT3/100)*(1+.w_ORSCONT4/100), .w_VADECTOT)
        .DoRTCalc(93,93,.f.)
        .w_ORTOTRIP = .w_ORTOTGEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZORDWEBM')
    this.DoRTCalc(95,100,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_29.enabled = this.oPgFrm.Page1.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oORNUMEST_1_10.enabled = i_bVal
      .Page2.oPag.oORSCOCL1_4_5.enabled = i_bVal
      .Page2.oPag.oORTOTORD_4_17.enabled = i_bVal
      .Page2.oPag.oORSCONTI_4_34.enabled = i_bVal
      .Page1.oPag.oORSCONT1_2_20.enabled = i_bVal
      .Page1.oPag.oORSCONT2_2_21.enabled = i_bVal
      .Page1.oPag.oORSCONT3_2_22.enabled = i_bVal
      .Page1.oPag.oORSCONT4_2_23.enabled = i_bVal
      .Page1.oPag.oORSCONT1_2_25.enabled = i_bVal
      .Page1.oPag.oORSCONT2_2_26.enabled = i_bVal
      .Page1.oPag.oORSCONT3_2_27.enabled = i_bVal
      .Page1.oPag.oORSCONT4_2_28.enabled = i_bVal
      .Page1.oPag.oORRIFEST_1_34.enabled = i_bVal
      .Page2.oPag.oORVALACC_4_41.enabled = i_bVal
      .Page1.oPag.oBtn_2_29.enabled = .Page1.oPag.oBtn_2_29.mCond()
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ZORDWEBM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OR_STATO,"OR_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSERIAL,"ORSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORTIPDOC,"ORTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORNUMDOC,"ORNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORALFDOC,"ORALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORNUMEST,"ORNUMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORALFEST,"ORALFEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORDATEST,"ORDATEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORTIPCON,"ORTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODCON,"ORCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODVAL,"ORCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORDATDOC,"ORDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSCOCL2,"ORSCOCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORFLSCOR,"ORFLSCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSCOPAG,"ORSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSCOCL1,"ORSCOCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSPEINC,"ORSPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSPETRA,"ORSPETRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSPEIMB,"ORSPEIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODPAG,"ORCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORTOTORD,"ORTOTORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OR_EMAIL,"OR_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORFLMAIL,"ORFLMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORMERISP,"ORMERISP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCONCON,"ORCONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSCONTI,"ORSCONTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORTDTEVA,"ORTDTEVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODAGE,"ORCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODDES,"ORCODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODSPE,"ORCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODVET,"ORCODVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCODPOR,"ORCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OR__NOTE,"OR__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORLOGSTM,"ORLOGSTM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORRIFEST,"ORRIFEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORVALNAZ,"ORVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORCAOVAL,"ORCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORACCONT,"ORACCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORVALACC,"ORVALACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ORSPEBOL,"ORSPEBOL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2])
    i_lTable = "ZORDWEBM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ZORDWEBM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_ORUNIMIS C(3);
      ,t_ORQTAMOV N(12,3);
      ,t_ORPREZZO N(18,5);
      ,t_ORFLOMAG N(3);
      ,t_ORDESART C(40);
      ,t_ORSCONT1 N(6,2);
      ,t_ORSCONT2 N(6,2);
      ,t_ORSCONT3 N(6,2);
      ,t_ORSCONT4 N(6,2);
      ,t_ORCODVIS C(40);
      ,CPROWNUM N(10);
      ,t_ORCODICE C(40);
      ,t_ORTIPRIG C(1);
      ,t_ORCODART C(20);
      ,t_ORDESSUP M(10);
      ,t_ORQTAUM1 N(12,3);
      ,t_ORCODIVA C(5);
      ,t_ORDATEVA D(8);
      ,t_ORCODLIS C(5);
      ,t_ORCONTRA C(15);
      ,t_ORSCOIN1 N(6,2);
      ,t_ORSCOIN2 N(6,2);
      ,t_ORSCOIN3 N(6,2);
      ,t_ORSCOIN4 N(6,2);
      ,t_ORLOGSTD C(5);
      ,t_ORUMNODI N(20,6);
      ,t_ORTOTRIG N(18,5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscp_mowbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oORUNIMIS_2_6.controlsource=this.cTrsName+'.t_ORUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oORQTAMOV_2_7.controlsource=this.cTrsName+'.t_ORQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oORPREZZO_2_9.controlsource=this.cTrsName+'.t_ORPREZZO'
    this.oPgFRm.Page1.oPag.oORFLOMAG_2_11.controlsource=this.cTrsName+'.t_ORFLOMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oORDESART_2_15.controlsource=this.cTrsName+'.t_ORDESART'
    this.oPgFRm.Page1.oPag.oORSCONT1_2_20.controlsource=this.cTrsName+'.t_ORSCONT1'
    this.oPgFRm.Page1.oPag.oORSCONT2_2_21.controlsource=this.cTrsName+'.t_ORSCONT2'
    this.oPgFRm.Page1.oPag.oORSCONT3_2_22.controlsource=this.cTrsName+'.t_ORSCONT3'
    this.oPgFRm.Page1.oPag.oORSCONT4_2_23.controlsource=this.cTrsName+'.t_ORSCONT4'
    this.oPgFRm.Page1.oPag.oCPROWORD_2_24.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oORSCONT1_2_25.controlsource=this.cTrsName+'.t_ORSCONT1'
    this.oPgFRm.Page1.oPag.oORSCONT2_2_26.controlsource=this.cTrsName+'.t_ORSCONT2'
    this.oPgFRm.Page1.oPag.oORSCONT3_2_27.controlsource=this.cTrsName+'.t_ORSCONT3'
    this.oPgFRm.Page1.oPag.oORSCONT4_2_28.controlsource=this.cTrsName+'.t_ORSCONT4'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oORCODVIS_2_30.controlsource=this.cTrsName+'.t_ORCODVIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(61)
    this.AddVLine(219)
    this.AddVLine(421)
    this.AddVLine(455)
    this.AddVLine(545)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ZORDWEBM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZORDWEBM')
        i_extval=cp_InsertValODBCExtFlds(this,'ZORDWEBM')
        local i_cFld
        i_cFld=" "+;
                  "(OR_STATO,ORSERIAL,ORTIPDOC,ORNUMDOC,ORALFDOC"+;
                  ",ORNUMEST,ORALFEST,ORDATEST,ORTIPCON,ORCODCON"+;
                  ",ORCODVAL,ORDATDOC,ORSCOCL2,ORFLSCOR,ORSCOPAG"+;
                  ",ORSCOCL1,ORSPEINC,ORSPETRA,ORSPEIMB,ORCODPAG"+;
                  ",ORTOTORD,UTCC,UTDC,UTDV,UTCV"+;
                  ",OR_EMAIL,ORFLMAIL,ORMERISP,ORCONCON,ORSCONTI"+;
                  ",ORTDTEVA,ORCODAGE,ORCODDES,ORCODSPE,ORCODVET"+;
                  ",ORCODPOR,OR__NOTE,ORLOGSTM,ORRIFEST,ORVALNAZ"+;
                  ",ORCAOVAL,ORACCONT,ORVALACC,ORSPEBOL"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_OR_STATO)+;
                    ","+cp_ToStrODBC(this.w_ORSERIAL)+;
                    ","+cp_ToStrODBCNull(this.w_ORTIPDOC)+;
                    ","+cp_ToStrODBC(this.w_ORNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_ORALFDOC)+;
                    ","+cp_ToStrODBC(this.w_ORNUMEST)+;
                    ","+cp_ToStrODBC(this.w_ORALFEST)+;
                    ","+cp_ToStrODBC(this.w_ORDATEST)+;
                    ","+cp_ToStrODBC(this.w_ORTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODCON)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODVAL)+;
                    ","+cp_ToStrODBC(this.w_ORDATDOC)+;
                    ","+cp_ToStrODBC(this.w_ORSCOCL2)+;
                    ","+cp_ToStrODBC(this.w_ORFLSCOR)+;
                    ","+cp_ToStrODBC(this.w_ORSCOPAG)+;
                    ","+cp_ToStrODBC(this.w_ORSCOCL1)+;
                    ","+cp_ToStrODBC(this.w_ORSPEINC)+;
                    ","+cp_ToStrODBC(this.w_ORSPETRA)+;
                    ","+cp_ToStrODBC(this.w_ORSPEIMB)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODPAG)+;
                    ","+cp_ToStrODBC(this.w_ORTOTORD)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_OR_EMAIL)+;
                    ","+cp_ToStrODBC(this.w_ORFLMAIL)+;
                    ","+cp_ToStrODBC(this.w_ORMERISP)+;
                    ","+cp_ToStrODBC(this.w_ORCONCON)+;
                    ","+cp_ToStrODBC(this.w_ORSCONTI)+;
                    ","+cp_ToStrODBC(this.w_ORTDTEVA)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODAGE)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODDES)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODSPE)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODVET)+;
                    ","+cp_ToStrODBCNull(this.w_ORCODPOR)+;
                    ","+cp_ToStrODBC(this.w_OR__NOTE)+;
                    ","+cp_ToStrODBC(this.w_ORLOGSTM)+;
                    ","+cp_ToStrODBC(this.w_ORRIFEST)+;
                    ","+cp_ToStrODBC(this.w_ORVALNAZ)+;
                    ","+cp_ToStrODBC(this.w_ORCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_ORACCONT)+;
                    ","+cp_ToStrODBC(this.w_ORVALACC)+;
                    ","+cp_ToStrODBC(this.w_ORSPEBOL)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZORDWEBM')
        i_extval=cp_InsertValVFPExtFlds(this,'ZORDWEBM')
        cp_CheckDeletedKey(i_cTable,0,'ORSERIAL',this.w_ORSERIAL)
        INSERT INTO (i_cTable);
              (OR_STATO,ORSERIAL,ORTIPDOC,ORNUMDOC,ORALFDOC,ORNUMEST,ORALFEST,ORDATEST,ORTIPCON,ORCODCON,ORCODVAL,ORDATDOC,ORSCOCL2,ORFLSCOR,ORSCOPAG,ORSCOCL1,ORSPEINC,ORSPETRA,ORSPEIMB,ORCODPAG,ORTOTORD,UTCC,UTDC,UTDV,UTCV,OR_EMAIL,ORFLMAIL,ORMERISP,ORCONCON,ORSCONTI,ORTDTEVA,ORCODAGE,ORCODDES,ORCODSPE,ORCODVET,ORCODPOR,OR__NOTE,ORLOGSTM,ORRIFEST,ORVALNAZ,ORCAOVAL,ORACCONT,ORVALACC,ORSPEBOL &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_OR_STATO;
                  ,this.w_ORSERIAL;
                  ,this.w_ORTIPDOC;
                  ,this.w_ORNUMDOC;
                  ,this.w_ORALFDOC;
                  ,this.w_ORNUMEST;
                  ,this.w_ORALFEST;
                  ,this.w_ORDATEST;
                  ,this.w_ORTIPCON;
                  ,this.w_ORCODCON;
                  ,this.w_ORCODVAL;
                  ,this.w_ORDATDOC;
                  ,this.w_ORSCOCL2;
                  ,this.w_ORFLSCOR;
                  ,this.w_ORSCOPAG;
                  ,this.w_ORSCOCL1;
                  ,this.w_ORSPEINC;
                  ,this.w_ORSPETRA;
                  ,this.w_ORSPEIMB;
                  ,this.w_ORCODPAG;
                  ,this.w_ORTOTORD;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
                  ,this.w_OR_EMAIL;
                  ,this.w_ORFLMAIL;
                  ,this.w_ORMERISP;
                  ,this.w_ORCONCON;
                  ,this.w_ORSCONTI;
                  ,this.w_ORTDTEVA;
                  ,this.w_ORCODAGE;
                  ,this.w_ORCODDES;
                  ,this.w_ORCODSPE;
                  ,this.w_ORCODVET;
                  ,this.w_ORCODPOR;
                  ,this.w_OR__NOTE;
                  ,this.w_ORLOGSTM;
                  ,this.w_ORRIFEST;
                  ,this.w_ORVALNAZ;
                  ,this.w_ORCAOVAL;
                  ,this.w_ORACCONT;
                  ,this.w_ORVALACC;
                  ,this.w_ORSPEBOL;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZORDWEBD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBD_IDX,2])
      *
      * insert into ZORDWEBD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(ORSERIAL,ORCODICE,CPROWORD,ORTIPRIG,ORCODART"+;
                  ",ORDESSUP,ORUNIMIS,ORQTAMOV,ORQTAUM1,ORPREZZO"+;
                  ",ORCODIVA,ORFLOMAG,ORDATEVA,ORCODLIS,ORCONTRA"+;
                  ",ORDESART,ORSCOIN1,ORSCOIN2,ORSCOIN3,ORSCOIN4"+;
                  ",ORSCONT1,ORSCONT2,ORSCONT3,ORSCONT4,ORLOGSTD"+;
                  ",ORUMNODI,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ORSERIAL)+","+cp_ToStrODBC(this.w_ORCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_ORTIPRIG)+","+cp_ToStrODBC(this.w_ORCODART)+;
             ","+cp_ToStrODBC(this.w_ORDESSUP)+","+cp_ToStrODBC(this.w_ORUNIMIS)+","+cp_ToStrODBC(this.w_ORQTAMOV)+","+cp_ToStrODBC(this.w_ORQTAUM1)+","+cp_ToStrODBC(this.w_ORPREZZO)+;
             ","+cp_ToStrODBC(this.w_ORCODIVA)+","+cp_ToStrODBC(this.w_ORFLOMAG)+","+cp_ToStrODBC(this.w_ORDATEVA)+","+cp_ToStrODBC(this.w_ORCODLIS)+","+cp_ToStrODBC(this.w_ORCONTRA)+;
             ","+cp_ToStrODBC(this.w_ORDESART)+","+cp_ToStrODBC(this.w_ORSCOIN1)+","+cp_ToStrODBC(this.w_ORSCOIN2)+","+cp_ToStrODBC(this.w_ORSCOIN3)+","+cp_ToStrODBC(this.w_ORSCOIN4)+;
             ","+cp_ToStrODBC(this.w_ORSCONT1)+","+cp_ToStrODBC(this.w_ORSCONT2)+","+cp_ToStrODBC(this.w_ORSCONT3)+","+cp_ToStrODBC(this.w_ORSCONT4)+","+cp_ToStrODBC(this.w_ORLOGSTD)+;
             ","+cp_ToStrODBC(this.w_ORUMNODI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'ORSERIAL',this.w_ORSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_ORSERIAL,this.w_ORCODICE,this.w_CPROWORD,this.w_ORTIPRIG,this.w_ORCODART"+;
                ",this.w_ORDESSUP,this.w_ORUNIMIS,this.w_ORQTAMOV,this.w_ORQTAUM1,this.w_ORPREZZO"+;
                ",this.w_ORCODIVA,this.w_ORFLOMAG,this.w_ORDATEVA,this.w_ORCODLIS,this.w_ORCONTRA"+;
                ",this.w_ORDESART,this.w_ORSCOIN1,this.w_ORSCOIN2,this.w_ORSCOIN3,this.w_ORSCOIN4"+;
                ",this.w_ORSCONT1,this.w_ORSCONT2,this.w_ORSCONT3,this.w_ORSCONT4,this.w_ORLOGSTD"+;
                ",this.w_ORUMNODI,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ZORDWEBM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ZORDWEBM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " OR_STATO="+cp_ToStrODBC(this.w_OR_STATO)+;
             ",ORTIPDOC="+cp_ToStrODBCNull(this.w_ORTIPDOC)+;
             ",ORNUMDOC="+cp_ToStrODBC(this.w_ORNUMDOC)+;
             ",ORALFDOC="+cp_ToStrODBC(this.w_ORALFDOC)+;
             ",ORNUMEST="+cp_ToStrODBC(this.w_ORNUMEST)+;
             ",ORALFEST="+cp_ToStrODBC(this.w_ORALFEST)+;
             ",ORDATEST="+cp_ToStrODBC(this.w_ORDATEST)+;
             ",ORTIPCON="+cp_ToStrODBC(this.w_ORTIPCON)+;
             ",ORCODCON="+cp_ToStrODBCNull(this.w_ORCODCON)+;
             ",ORCODVAL="+cp_ToStrODBCNull(this.w_ORCODVAL)+;
             ",ORDATDOC="+cp_ToStrODBC(this.w_ORDATDOC)+;
             ",ORSCOCL2="+cp_ToStrODBC(this.w_ORSCOCL2)+;
             ",ORFLSCOR="+cp_ToStrODBC(this.w_ORFLSCOR)+;
             ",ORSCOPAG="+cp_ToStrODBC(this.w_ORSCOPAG)+;
             ",ORSCOCL1="+cp_ToStrODBC(this.w_ORSCOCL1)+;
             ",ORSPEINC="+cp_ToStrODBC(this.w_ORSPEINC)+;
             ",ORSPETRA="+cp_ToStrODBC(this.w_ORSPETRA)+;
             ",ORSPEIMB="+cp_ToStrODBC(this.w_ORSPEIMB)+;
             ",ORCODPAG="+cp_ToStrODBCNull(this.w_ORCODPAG)+;
             ",ORTOTORD="+cp_ToStrODBC(this.w_ORTOTORD)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",OR_EMAIL="+cp_ToStrODBC(this.w_OR_EMAIL)+;
             ",ORFLMAIL="+cp_ToStrODBC(this.w_ORFLMAIL)+;
             ",ORMERISP="+cp_ToStrODBC(this.w_ORMERISP)+;
             ",ORCONCON="+cp_ToStrODBC(this.w_ORCONCON)+;
             ",ORSCONTI="+cp_ToStrODBC(this.w_ORSCONTI)+;
             ",ORTDTEVA="+cp_ToStrODBC(this.w_ORTDTEVA)+;
             ",ORCODAGE="+cp_ToStrODBCNull(this.w_ORCODAGE)+;
             ",ORCODDES="+cp_ToStrODBCNull(this.w_ORCODDES)+;
             ",ORCODSPE="+cp_ToStrODBCNull(this.w_ORCODSPE)+;
             ",ORCODVET="+cp_ToStrODBCNull(this.w_ORCODVET)+;
             ",ORCODPOR="+cp_ToStrODBCNull(this.w_ORCODPOR)+;
             ",OR__NOTE="+cp_ToStrODBC(this.w_OR__NOTE)+;
             ",ORLOGSTM="+cp_ToStrODBC(this.w_ORLOGSTM)+;
             ",ORRIFEST="+cp_ToStrODBC(this.w_ORRIFEST)+;
             ",ORVALNAZ="+cp_ToStrODBC(this.w_ORVALNAZ)+;
             ",ORCAOVAL="+cp_ToStrODBC(this.w_ORCAOVAL)+;
             ",ORACCONT="+cp_ToStrODBC(this.w_ORACCONT)+;
             ",ORVALACC="+cp_ToStrODBC(this.w_ORVALACC)+;
             ",ORSPEBOL="+cp_ToStrODBC(this.w_ORSPEBOL)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ZORDWEBM')
          i_cWhere = cp_PKFox(i_cTable  ,'ORSERIAL',this.w_ORSERIAL  )
          UPDATE (i_cTable) SET;
              OR_STATO=this.w_OR_STATO;
             ,ORTIPDOC=this.w_ORTIPDOC;
             ,ORNUMDOC=this.w_ORNUMDOC;
             ,ORALFDOC=this.w_ORALFDOC;
             ,ORNUMEST=this.w_ORNUMEST;
             ,ORALFEST=this.w_ORALFEST;
             ,ORDATEST=this.w_ORDATEST;
             ,ORTIPCON=this.w_ORTIPCON;
             ,ORCODCON=this.w_ORCODCON;
             ,ORCODVAL=this.w_ORCODVAL;
             ,ORDATDOC=this.w_ORDATDOC;
             ,ORSCOCL2=this.w_ORSCOCL2;
             ,ORFLSCOR=this.w_ORFLSCOR;
             ,ORSCOPAG=this.w_ORSCOPAG;
             ,ORSCOCL1=this.w_ORSCOCL1;
             ,ORSPEINC=this.w_ORSPEINC;
             ,ORSPETRA=this.w_ORSPETRA;
             ,ORSPEIMB=this.w_ORSPEIMB;
             ,ORCODPAG=this.w_ORCODPAG;
             ,ORTOTORD=this.w_ORTOTORD;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,OR_EMAIL=this.w_OR_EMAIL;
             ,ORFLMAIL=this.w_ORFLMAIL;
             ,ORMERISP=this.w_ORMERISP;
             ,ORCONCON=this.w_ORCONCON;
             ,ORSCONTI=this.w_ORSCONTI;
             ,ORTDTEVA=this.w_ORTDTEVA;
             ,ORCODAGE=this.w_ORCODAGE;
             ,ORCODDES=this.w_ORCODDES;
             ,ORCODSPE=this.w_ORCODSPE;
             ,ORCODVET=this.w_ORCODVET;
             ,ORCODPOR=this.w_ORCODPOR;
             ,OR__NOTE=this.w_OR__NOTE;
             ,ORLOGSTM=this.w_ORLOGSTM;
             ,ORRIFEST=this.w_ORRIFEST;
             ,ORVALNAZ=this.w_ORVALNAZ;
             ,ORCAOVAL=this.w_ORCAOVAL;
             ,ORACCONT=this.w_ORACCONT;
             ,ORVALACC=this.w_ORVALACC;
             ,ORSPEBOL=this.w_ORSPEBOL;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_ORCODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ZORDWEBD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ZORDWEBD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ZORDWEBD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ORCODICE="+cp_ToStrODBC(this.w_ORCODICE)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ORTIPRIG="+cp_ToStrODBC(this.w_ORTIPRIG)+;
                     ",ORCODART="+cp_ToStrODBC(this.w_ORCODART)+;
                     ",ORDESSUP="+cp_ToStrODBC(this.w_ORDESSUP)+;
                     ",ORUNIMIS="+cp_ToStrODBC(this.w_ORUNIMIS)+;
                     ",ORQTAMOV="+cp_ToStrODBC(this.w_ORQTAMOV)+;
                     ",ORQTAUM1="+cp_ToStrODBC(this.w_ORQTAUM1)+;
                     ",ORPREZZO="+cp_ToStrODBC(this.w_ORPREZZO)+;
                     ",ORCODIVA="+cp_ToStrODBC(this.w_ORCODIVA)+;
                     ",ORFLOMAG="+cp_ToStrODBC(this.w_ORFLOMAG)+;
                     ",ORDATEVA="+cp_ToStrODBC(this.w_ORDATEVA)+;
                     ",ORCODLIS="+cp_ToStrODBC(this.w_ORCODLIS)+;
                     ",ORCONTRA="+cp_ToStrODBC(this.w_ORCONTRA)+;
                     ",ORDESART="+cp_ToStrODBC(this.w_ORDESART)+;
                     ",ORSCOIN1="+cp_ToStrODBC(this.w_ORSCOIN1)+;
                     ",ORSCOIN2="+cp_ToStrODBC(this.w_ORSCOIN2)+;
                     ",ORSCOIN3="+cp_ToStrODBC(this.w_ORSCOIN3)+;
                     ",ORSCOIN4="+cp_ToStrODBC(this.w_ORSCOIN4)+;
                     ",ORSCONT1="+cp_ToStrODBC(this.w_ORSCONT1)+;
                     ",ORSCONT2="+cp_ToStrODBC(this.w_ORSCONT2)+;
                     ",ORSCONT3="+cp_ToStrODBC(this.w_ORSCONT3)+;
                     ",ORSCONT4="+cp_ToStrODBC(this.w_ORSCONT4)+;
                     ",ORLOGSTD="+cp_ToStrODBC(this.w_ORLOGSTD)+;
                     ",ORUMNODI="+cp_ToStrODBC(this.w_ORUMNODI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ORCODICE=this.w_ORCODICE;
                     ,CPROWORD=this.w_CPROWORD;
                     ,ORTIPRIG=this.w_ORTIPRIG;
                     ,ORCODART=this.w_ORCODART;
                     ,ORDESSUP=this.w_ORDESSUP;
                     ,ORUNIMIS=this.w_ORUNIMIS;
                     ,ORQTAMOV=this.w_ORQTAMOV;
                     ,ORQTAUM1=this.w_ORQTAUM1;
                     ,ORPREZZO=this.w_ORPREZZO;
                     ,ORCODIVA=this.w_ORCODIVA;
                     ,ORFLOMAG=this.w_ORFLOMAG;
                     ,ORDATEVA=this.w_ORDATEVA;
                     ,ORCODLIS=this.w_ORCODLIS;
                     ,ORCONTRA=this.w_ORCONTRA;
                     ,ORDESART=this.w_ORDESART;
                     ,ORSCOIN1=this.w_ORSCOIN1;
                     ,ORSCOIN2=this.w_ORSCOIN2;
                     ,ORSCOIN3=this.w_ORSCOIN3;
                     ,ORSCOIN4=this.w_ORSCOIN4;
                     ,ORSCONT1=this.w_ORSCONT1;
                     ,ORSCONT2=this.w_ORSCONT2;
                     ,ORSCONT3=this.w_ORSCONT3;
                     ,ORSCONT4=this.w_ORSCONT4;
                     ,ORLOGSTD=this.w_ORLOGSTD;
                     ,ORUMNODI=this.w_ORUMNODI;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_ORCODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ZORDWEBD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBD_IDX,2])
        *
        * delete ZORDWEBD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2])
        *
        * delete ZORDWEBM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_ORCODICE))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZORDWEBM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZORDWEBM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,24,.t.)
          .link_1_17('Full')
          .link_1_18('Full')
        .DoRTCalc(27,35,.t.)
          .link_4_13('Full')
        .DoRTCalc(37,46,.t.)
          .link_4_29('Full')
        .DoRTCalc(48,59,.t.)
        if .o_ORCODVAL<>.w_ORCODVAL
          .w_CALCPICT = DEFPIC(.w_VADECTOT)
        endif
        if .o_ORCODVAL<>.w_ORCODVAL
          .w_CALCPICP = DEFPIP(.w_VADECUNI)
        endif
          .link_4_35('Full')
        .DoRTCalc(63,72,.t.)
          .link_5_1('Full')
          .link_5_3('Full')
          .link_5_5('Full')
          .link_5_7('Full')
        .DoRTCalc(77,85,.t.)
          .w_CONCONDES = iif(.w_ORCONCON='E','Franco Fabbrica',iif(.w_ORCONCON='F','Franco Vettore',iif(.w_ORCONCON='C','Costo e Nolo',iif(.w_ORCONCON='D','Reso',''))))
        .DoRTCalc(87,87,.t.)
        if .o_CPROWORD<>.w_CPROWORD
          .w_ORCODVIS = IIF(.w_ORTIPRIG='D',g_ARTDES,.w_ORCODICE)
        endif
        .DoRTCalc(89,91,.t.)
          .w_ORTOTGEN = .w_ORTOTGEN-.w_ortotrig
          .w_ORTOTRIG = .w_ORQTAMOV* cp_ROUND(.w_ORPREZZO * (1+.w_ORSCONT1/100)*(1+.w_ORSCONT2/100)*(1+.w_ORSCONT3/100)*(1+.w_ORSCONT4/100), .w_VADECTOT)
          .w_ORTOTGEN = .w_ORTOTGEN+.w_ortotrig
        .DoRTCalc(93,93,.t.)
          .w_ORTOTRIP = .w_ORTOTGEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(95,100,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ORCODICE with this.w_ORCODICE
      replace t_ORTIPRIG with this.w_ORTIPRIG
      replace t_ORCODART with this.w_ORCODART
      replace t_ORDESSUP with this.w_ORDESSUP
      replace t_ORQTAUM1 with this.w_ORQTAUM1
      replace t_ORCODIVA with this.w_ORCODIVA
      replace t_ORDATEVA with this.w_ORDATEVA
      replace t_ORCODLIS with this.w_ORCODLIS
      replace t_ORCONTRA with this.w_ORCONTRA
      replace t_ORSCOIN1 with this.w_ORSCOIN1
      replace t_ORSCOIN2 with this.w_ORSCOIN2
      replace t_ORSCOIN3 with this.w_ORSCOIN3
      replace t_ORSCOIN4 with this.w_ORSCOIN4
      replace t_ORLOGSTD with this.w_ORLOGSTD
      replace t_ORUMNODI with this.w_ORUMNODI
      replace t_ORTOTRIG with this.w_ORTOTRIG
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oORNUMEST_1_10.visible=!this.oPgFrm.Page1.oPag.oORNUMEST_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oORALFEST_1_12.visible=!this.oPgFrm.Page1.oPag.oORALFEST_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oORDATEST_1_14.visible=!this.oPgFrm.Page1.oPag.oORDATEST_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_1.visible=!this.oPgFrm.Page1.oPag.oStr_3_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_2.visible=!this.oPgFrm.Page1.oPag.oStr_3_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_3.visible=!this.oPgFrm.Page1.oPag.oStr_3_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_4.visible=!this.oPgFrm.Page1.oPag.oStr_3_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_5.visible=!this.oPgFrm.Page1.oPag.oStr_3_5.mHide()
    this.oPgFrm.Page2.oPag.oORCODAGE_4_35.visible=!this.oPgFrm.Page2.oPag.oORCODAGE_4_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_36.visible=!this.oPgFrm.Page2.oPag.oStr_4_36.mHide()
    this.oPgFrm.Page2.oPag.oAGDESAGE_4_37.visible=!this.oPgFrm.Page2.oPag.oAGDESAGE_4_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_7.visible=!this.oPgFrm.Page1.oPag.oStr_3_7.mHide()
    this.oPgFrm.Page3.oPag.oOR__NOTE_5_10.visible=!this.oPgFrm.Page3.oPag.oOR__NOTE_5_10.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_11.visible=!this.oPgFrm.Page3.oPag.oStr_5_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_8.visible=!this.oPgFrm.Page1.oPag.oStr_3_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_9.visible=!this.oPgFrm.Page1.oPag.oStr_3_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_10.visible=!this.oPgFrm.Page1.oPag.oStr_3_10.mHide()
    this.oPgFrm.Page1.oPag.oORRIFEST_1_34.visible=!this.oPgFrm.Page1.oPag.oORRIFEST_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.visible=!this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT1_2_20.visible=!this.oPgFrm.Page1.oPag.oORSCONT1_2_20.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT2_2_21.visible=!this.oPgFrm.Page1.oPag.oORSCONT2_2_21.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT3_2_22.visible=!this.oPgFrm.Page1.oPag.oORSCONT3_2_22.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT4_2_23.visible=!this.oPgFrm.Page1.oPag.oORSCONT4_2_23.mHide()
    this.oPgFrm.Page1.oPag.oCPROWORD_2_24.visible=!this.oPgFrm.Page1.oPag.oCPROWORD_2_24.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT1_2_25.visible=!this.oPgFrm.Page1.oPag.oORSCONT1_2_25.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT2_2_26.visible=!this.oPgFrm.Page1.oPag.oORSCONT2_2_26.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT3_2_27.visible=!this.oPgFrm.Page1.oPag.oORSCONT3_2_27.mHide()
    this.oPgFrm.Page1.oPag.oORSCONT4_2_28.visible=!this.oPgFrm.Page1.oPag.oORSCONT4_2_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_29.visible=!this.oPgFrm.Page1.oPag.oBtn_2_29.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ORTIPDOC
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDCATDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDCATDOC="+cp_ToStrODBC(this.w_ORTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDCATDOC',this.w_ORTIPDOC)
            select TDCATDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORTIPDOC = NVL(_Link_.TDCATDOC,space(5))
      this.w_TDDESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ORTIPDOC = space(5)
      endif
      this.w_TDDESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDCATDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORCODCON
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ORCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ORTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ORTIPCON;
                       ,'ANCODICE',this.w_ORCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.ANCODICE as ANCODICE117"+ ",link_1_17.ANDESCRI as ANDESCRI117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on ZORDWEBM.ORCODCON=link_1_17.ANCODICE"+" and ZORDWEBM.ORTIPCON=link_1_17.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODCON=link_1_17.ANCODICE(+)"'+'+" and ZORDWEBM.ORTIPCON=link_1_17.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ORCODVAL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ORCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ORCODVAL)
            select VACODVAL,VASIMVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_VASIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_VADECUNI = NVL(_Link_.VADECUNI,0)
      this.w_VADECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_ORCODVAL = space(3)
      endif
      this.w_VASIMVAL = space(5)
      this.w_VADECUNI = 0
      this.w_VADECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.VACODVAL as VACODVAL118"+ ",link_1_18.VASIMVAL as VASIMVAL118"+ ",link_1_18.VADECUNI as VADECUNI118"+ ",link_1_18.VADECTOT as VADECTOT118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on ZORDWEBM.ORCODVAL=link_1_18.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODVAL=link_1_18.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ORCODPAG
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ORCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ORCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_PADESCRI = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODPAG = space(5)
      endif
      this.w_PADESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_13.PACODICE as PACODICE413"+ ","+cp_TransLinkFldName('link_4_13.PADESCRI')+" as PADESCRI413"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_13 on ZORDWEBM.ORCODPAG=link_4_13.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_13"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODPAG=link_4_13.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DDCODNAZ
  func Link_4_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DDCODNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DDCODNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_DDCODNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_DDCODNAZ)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DDCODNAZ = NVL(_Link_.NACODNAZ,space(3))
      this.w_NADESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DDCODNAZ = space(3)
      endif
      this.w_NADESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DDCODNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORCODAGE
  func Link_4_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ORCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ORCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_AGDESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODAGE = space(5)
      endif
      this.w_AGDESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_35.AGCODAGE as AGCODAGE435"+ ",link_4_35.AGDESAGE as AGDESAGE435"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_35 on ZORDWEBM.ORCODAGE=link_4_35.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_35"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODAGE=link_4_35.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ORCODDES
  func Link_5_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_ORCODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_ORCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_ANTIPCON;
                       ,'DDCODICE',this.w_ORCODCON;
                       ,'DDCODDES',this.w_ORCODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_DDNOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_DDINDIRI = NVL(_Link_.DDINDIRI,space(35))
      this.w_DD___CAP = NVL(_Link_.DD___CAP,space(5))
      this.w_DDLOCALI = NVL(_Link_.DDLOCALI,space(30))
      this.w_DDPROVIN = NVL(_Link_.DDPROVIN,space(2))
      this.w_DDCODNAZ = NVL(_Link_.DDCODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODDES = space(5)
      endif
      this.w_DDNOMDES = space(40)
      this.w_DDINDIRI = space(35)
      this.w_DD___CAP = space(5)
      this.w_DDLOCALI = space(30)
      this.w_DDPROVIN = space(2)
      this.w_DDCODNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORCODSPE
  func Link_5_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_ORCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_ORCODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_SPDESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODSPE = space(3)
      endif
      this.w_SPDESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_3.SPCODSPE as SPCODSPE503"+ ","+cp_TransLinkFldName('link_5_3.SPDESSPE')+" as SPDESSPE503"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_3 on ZORDWEBM.ORCODSPE=link_5_3.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_3"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODSPE=link_5_3.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ORCODVET
  func Link_5_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_ORCODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_ORCODVET)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_VTDESVET = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODVET = space(5)
      endif
      this.w_VTDESVET = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VETTORI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_5.VTCODVET as VTCODVET505"+ ",link_5_5.VTDESVET as VTDESVET505"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_5 on ZORDWEBM.ORCODVET=link_5_5.VTCODVET"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_5"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODVET=link_5_5.VTCODVET(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ORCODPOR
  func Link_5_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_ORCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_ORCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_PODESPOR = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODPOR = space(1)
      endif
      this.w_PODESPOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_7.POCODPOR as POCODPOR507"+ ","+cp_TransLinkFldName('link_5_7.PODESPOR')+" as PODESPOR507"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_7 on ZORDWEBM.ORCODPOR=link_5_7.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_7"
          i_cKey=i_cKey+'+" and ZORDWEBM.ORCODPOR=link_5_7.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oORTIPDOC_1_3.value==this.w_ORTIPDOC)
      this.oPgFrm.Page1.oPag.oORTIPDOC_1_3.value=this.w_ORTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oORNUMDOC_1_5.value==this.w_ORNUMDOC)
      this.oPgFrm.Page1.oPag.oORNUMDOC_1_5.value=this.w_ORNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oORALFDOC_1_7.value==this.w_ORALFDOC)
      this.oPgFrm.Page1.oPag.oORALFDOC_1_7.value=this.w_ORALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.RadioValue()==this.w_ORFLOMAG)
      this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.SetRadio()
      replace t_ORFLOMAG with this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_9.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_9.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oORNUMEST_1_10.value==this.w_ORNUMEST)
      this.oPgFrm.Page1.oPag.oORNUMEST_1_10.value=this.w_ORNUMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oORALFEST_1_12.value==this.w_ORALFEST)
      this.oPgFrm.Page1.oPag.oORALFEST_1_12.value=this.w_ORALFEST
    endif
    if not(this.oPgFrm.Page1.oPag.oORDATEST_1_14.value==this.w_ORDATEST)
      this.oPgFrm.Page1.oPag.oORDATEST_1_14.value=this.w_ORDATEST
    endif
    if not(this.oPgFrm.Page1.oPag.oORCODCON_1_17.value==this.w_ORCODCON)
      this.oPgFrm.Page1.oPag.oORCODCON_1_17.value=this.w_ORCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oORCODVAL_1_18.value==this.w_ORCODVAL)
      this.oPgFrm.Page1.oPag.oORCODVAL_1_18.value=this.w_ORCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_20.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_20.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oORDATDOC_1_24.value==this.w_ORDATDOC)
      this.oPgFrm.Page1.oPag.oORDATDOC_1_24.value=this.w_ORDATDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oORSCOCL2_4_1.value==this.w_ORSCOCL2)
      this.oPgFrm.Page2.oPag.oORSCOCL2_4_1.value=this.w_ORSCOCL2
    endif
    if not(this.oPgFrm.Page2.oPag.oORSCOPAG_4_3.value==this.w_ORSCOPAG)
      this.oPgFrm.Page2.oPag.oORSCOPAG_4_3.value=this.w_ORSCOPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oORSCOCL1_4_5.value==this.w_ORSCOCL1)
      this.oPgFrm.Page2.oPag.oORSCOCL1_4_5.value=this.w_ORSCOCL1
    endif
    if not(this.oPgFrm.Page2.oPag.oORSPEINC_4_7.value==this.w_ORSPEINC)
      this.oPgFrm.Page2.oPag.oORSPEINC_4_7.value=this.w_ORSPEINC
    endif
    if not(this.oPgFrm.Page2.oPag.oORSPETRA_4_9.value==this.w_ORSPETRA)
      this.oPgFrm.Page2.oPag.oORSPETRA_4_9.value=this.w_ORSPETRA
    endif
    if not(this.oPgFrm.Page2.oPag.oORSPEIMB_4_11.value==this.w_ORSPEIMB)
      this.oPgFrm.Page2.oPag.oORSPEIMB_4_11.value=this.w_ORSPEIMB
    endif
    if not(this.oPgFrm.Page2.oPag.oORCODPAG_4_13.value==this.w_ORCODPAG)
      this.oPgFrm.Page2.oPag.oORCODPAG_4_13.value=this.w_ORCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oORTOTORD_4_17.value==this.w_ORTOTORD)
      this.oPgFrm.Page2.oPag.oORTOTORD_4_17.value=this.w_ORTOTORD
    endif
    if not(this.oPgFrm.Page2.oPag.oVASIMVAL_4_21.value==this.w_VASIMVAL)
      this.oPgFrm.Page2.oPag.oVASIMVAL_4_21.value=this.w_VASIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oPADESCRI_4_32.value==this.w_PADESCRI)
      this.oPgFrm.Page2.oPag.oPADESCRI_4_32.value=this.w_PADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oORSCONTI_4_34.value==this.w_ORSCONTI)
      this.oPgFrm.Page2.oPag.oORSCONTI_4_34.value=this.w_ORSCONTI
    endif
    if not(this.oPgFrm.Page1.oPag.oORTDTEVA_1_25.value==this.w_ORTDTEVA)
      this.oPgFrm.Page1.oPag.oORTDTEVA_1_25.value=this.w_ORTDTEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oORCODAGE_4_35.value==this.w_ORCODAGE)
      this.oPgFrm.Page2.oPag.oORCODAGE_4_35.value=this.w_ORCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oAGDESAGE_4_37.value==this.w_AGDESAGE)
      this.oPgFrm.Page2.oPag.oAGDESAGE_4_37.value=this.w_AGDESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT1_2_20.value==this.w_ORSCONT1)
      this.oPgFrm.Page1.oPag.oORSCONT1_2_20.value=this.w_ORSCONT1
      replace t_ORSCONT1 with this.oPgFrm.Page1.oPag.oORSCONT1_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT2_2_21.value==this.w_ORSCONT2)
      this.oPgFrm.Page1.oPag.oORSCONT2_2_21.value=this.w_ORSCONT2
      replace t_ORSCONT2 with this.oPgFrm.Page1.oPag.oORSCONT2_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT3_2_22.value==this.w_ORSCONT3)
      this.oPgFrm.Page1.oPag.oORSCONT3_2_22.value=this.w_ORSCONT3
      replace t_ORSCONT3 with this.oPgFrm.Page1.oPag.oORSCONT3_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT4_2_23.value==this.w_ORSCONT4)
      this.oPgFrm.Page1.oPag.oORSCONT4_2_23.value=this.w_ORSCONT4
      replace t_ORSCONT4 with this.oPgFrm.Page1.oPag.oORSCONT4_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_2_24.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_2_24.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oCPROWORD_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT1_2_25.value==this.w_ORSCONT1)
      this.oPgFrm.Page1.oPag.oORSCONT1_2_25.value=this.w_ORSCONT1
      replace t_ORSCONT1 with this.oPgFrm.Page1.oPag.oORSCONT1_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT2_2_26.value==this.w_ORSCONT2)
      this.oPgFrm.Page1.oPag.oORSCONT2_2_26.value=this.w_ORSCONT2
      replace t_ORSCONT2 with this.oPgFrm.Page1.oPag.oORSCONT2_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT3_2_27.value==this.w_ORSCONT3)
      this.oPgFrm.Page1.oPag.oORSCONT3_2_27.value=this.w_ORSCONT3
      replace t_ORSCONT3 with this.oPgFrm.Page1.oPag.oORSCONT3_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT4_2_28.value==this.w_ORSCONT4)
      this.oPgFrm.Page1.oPag.oORSCONT4_2_28.value=this.w_ORSCONT4
      replace t_ORSCONT4 with this.oPgFrm.Page1.oPag.oORSCONT4_2_28.value
    endif
    if not(this.oPgFrm.Page3.oPag.oORCODDES_5_1.value==this.w_ORCODDES)
      this.oPgFrm.Page3.oPag.oORCODDES_5_1.value=this.w_ORCODDES
    endif
    if not(this.oPgFrm.Page3.oPag.oORCODSPE_5_3.value==this.w_ORCODSPE)
      this.oPgFrm.Page3.oPag.oORCODSPE_5_3.value=this.w_ORCODSPE
    endif
    if not(this.oPgFrm.Page3.oPag.oORCODVET_5_5.value==this.w_ORCODVET)
      this.oPgFrm.Page3.oPag.oORCODVET_5_5.value=this.w_ORCODVET
    endif
    if not(this.oPgFrm.Page3.oPag.oORCODPOR_5_7.value==this.w_ORCODPOR)
      this.oPgFrm.Page3.oPag.oORCODPOR_5_7.value=this.w_ORCODPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oOR__NOTE_5_10.value==this.w_OR__NOTE)
      this.oPgFrm.Page3.oPag.oOR__NOTE_5_10.value=this.w_OR__NOTE
    endif
    if not(this.oPgFrm.Page3.oPag.oDDNOMDES_5_12.value==this.w_DDNOMDES)
      this.oPgFrm.Page3.oPag.oDDNOMDES_5_12.value=this.w_DDNOMDES
    endif
    if not(this.oPgFrm.Page3.oPag.oDDINDIRI_5_13.value==this.w_DDINDIRI)
      this.oPgFrm.Page3.oPag.oDDINDIRI_5_13.value=this.w_DDINDIRI
    endif
    if not(this.oPgFrm.Page3.oPag.oDD___CAP_5_14.value==this.w_DD___CAP)
      this.oPgFrm.Page3.oPag.oDD___CAP_5_14.value=this.w_DD___CAP
    endif
    if not(this.oPgFrm.Page3.oPag.oDDLOCALI_5_15.value==this.w_DDLOCALI)
      this.oPgFrm.Page3.oPag.oDDLOCALI_5_15.value=this.w_DDLOCALI
    endif
    if not(this.oPgFrm.Page3.oPag.oDDPROVIN_5_16.value==this.w_DDPROVIN)
      this.oPgFrm.Page3.oPag.oDDPROVIN_5_16.value=this.w_DDPROVIN
    endif
    if not(this.oPgFrm.Page3.oPag.oVTDESVET_5_17.value==this.w_VTDESVET)
      this.oPgFrm.Page3.oPag.oVTDESVET_5_17.value=this.w_VTDESVET
    endif
    if not(this.oPgFrm.Page3.oPag.oPODESPOR_5_18.value==this.w_PODESPOR)
      this.oPgFrm.Page3.oPag.oPODESPOR_5_18.value=this.w_PODESPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oSPDESSPE_5_19.value==this.w_SPDESSPE)
      this.oPgFrm.Page3.oPag.oSPDESSPE_5_19.value=this.w_SPDESSPE
    endif
    if not(this.oPgFrm.Page3.oPag.oCONCONDES_5_20.value==this.w_CONCONDES)
      this.oPgFrm.Page3.oPag.oCONCONDES_5_20.value=this.w_CONCONDES
    endif
    if not(this.oPgFrm.Page1.oPag.oORTOTGEN_3_11.value==this.w_ORTOTGEN)
      this.oPgFrm.Page1.oPag.oORTOTGEN_3_11.value=this.w_ORTOTGEN
    endif
    if not(this.oPgFrm.Page2.oPag.oORTOTRIP_4_38.value==this.w_ORTOTRIP)
      this.oPgFrm.Page2.oPag.oORTOTRIP_4_38.value=this.w_ORTOTRIP
    endif
    if not(this.oPgFrm.Page1.oPag.oORRIFEST_1_34.value==this.w_ORRIFEST)
      this.oPgFrm.Page1.oPag.oORRIFEST_1_34.value=this.w_ORRIFEST
    endif
    if not(this.oPgFrm.Page1.oPag.oORVALNAZ_1_36.value==this.w_ORVALNAZ)
      this.oPgFrm.Page1.oPag.oORVALNAZ_1_36.value=this.w_ORVALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oORCAOVAL_1_38.value==this.w_ORCAOVAL)
      this.oPgFrm.Page1.oPag.oORCAOVAL_1_38.value=this.w_ORCAOVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oORACCONT_4_39.value==this.w_ORACCONT)
      this.oPgFrm.Page2.oPag.oORACCONT_4_39.value=this.w_ORACCONT
    endif
    if not(this.oPgFrm.Page2.oPag.oORVALACC_4_41.value==this.w_ORVALACC)
      this.oPgFrm.Page2.oPag.oORVALACC_4_41.value=this.w_ORVALACC
    endif
    if not(this.oPgFrm.Page2.oPag.oORSPEBOL_4_44.value==this.w_ORSPEBOL)
      this.oPgFrm.Page2.oPag.oORSPEBOL_4_44.value=this.w_ORSPEBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORUNIMIS_2_6.value==this.w_ORUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORUNIMIS_2_6.value=this.w_ORUNIMIS
      replace t_ORUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORUNIMIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORQTAMOV_2_7.value==this.w_ORQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORQTAMOV_2_7.value=this.w_ORQTAMOV
      replace t_ORQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORQTAMOV_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORPREZZO_2_9.value==this.w_ORPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORPREZZO_2_9.value=this.w_ORPREZZO
      replace t_ORPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORPREZZO_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORDESART_2_15.value==this.w_ORDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORDESART_2_15.value=this.w_ORDESART
      replace t_ORDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORDESART_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORCODVIS_2_30.value==this.w_ORCODVIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORCODVIS_2_30.value=this.w_ORCODVIS
      replace t_ORCODVIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oORCODVIS_2_30.value
    endif
    cp_SetControlsValueExtFlds(this,'ZORDWEBM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.F.)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Documento in sola lettura. Impossibile registrare le variazioni."))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_ORCODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CPROWORD = this.w_CPROWORD
    this.o_ORCODVAL = this.w_ORCODVAL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_ORCODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ORCODICE=space(40)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ORTIPRIG=space(1)
      .w_ORCODART=space(20)
      .w_ORDESSUP=space(0)
      .w_ORUNIMIS=space(3)
      .w_ORQTAMOV=0
      .w_ORQTAUM1=0
      .w_ORPREZZO=0
      .w_ORCODIVA=space(5)
      .w_ORFLOMAG=space(1)
      .w_ORDATEVA=ctod("  /  /  ")
      .w_ORCODLIS=space(5)
      .w_ORCONTRA=space(15)
      .w_ORDESART=space(40)
      .w_ORSCOIN1=0
      .w_ORSCOIN2=0
      .w_ORSCOIN3=0
      .w_ORSCOIN4=0
      .w_ORSCONT1=0
      .w_ORSCONT2=0
      .w_ORSCONT3=0
      .w_ORSCONT4=0
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ORSCONT1=0
      .w_ORSCONT2=0
      .w_ORSCONT3=0
      .w_ORSCONT4=0
      .w_ORCODVIS=space(40)
      .w_ORLOGSTD=space(5)
      .w_ORUMNODI=0
      .w_ORTOTRIG=0
      .DoRTCalc(1,87,.f.)
        .w_ORCODVIS = IIF(.w_ORTIPRIG='D',g_ARTDES,.w_ORCODICE)
      .DoRTCalc(89,91,.f.)
        .w_ORTOTRIG = .w_ORQTAMOV* cp_ROUND(.w_ORPREZZO * (1+.w_ORSCONT1/100)*(1+.w_ORSCONT2/100)*(1+.w_ORSCONT3/100)*(1+.w_ORSCONT4/100), .w_VADECTOT)
    endwith
    this.DoRTCalc(93,100,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ORCODICE = t_ORCODICE
    this.w_CPROWORD = t_CPROWORD
    this.w_ORTIPRIG = t_ORTIPRIG
    this.w_ORCODART = t_ORCODART
    this.w_ORDESSUP = t_ORDESSUP
    this.w_ORUNIMIS = t_ORUNIMIS
    this.w_ORQTAMOV = t_ORQTAMOV
    this.w_ORQTAUM1 = t_ORQTAUM1
    this.w_ORPREZZO = t_ORPREZZO
    this.w_ORCODIVA = t_ORCODIVA
    this.w_ORFLOMAG = this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.RadioValue(.t.)
    this.w_ORDATEVA = t_ORDATEVA
    this.w_ORCODLIS = t_ORCODLIS
    this.w_ORCONTRA = t_ORCONTRA
    this.w_ORDESART = t_ORDESART
    this.w_ORSCOIN1 = t_ORSCOIN1
    this.w_ORSCOIN2 = t_ORSCOIN2
    this.w_ORSCOIN3 = t_ORSCOIN3
    this.w_ORSCOIN4 = t_ORSCOIN4
    this.w_ORSCONT1 = t_ORSCONT1
    this.w_ORSCONT2 = t_ORSCONT2
    this.w_ORSCONT3 = t_ORSCONT3
    this.w_ORSCONT4 = t_ORSCONT4
    this.w_CPROWORD = t_CPROWORD
    this.w_ORSCONT1 = t_ORSCONT1
    this.w_ORSCONT2 = t_ORSCONT2
    this.w_ORSCONT3 = t_ORSCONT3
    this.w_ORSCONT4 = t_ORSCONT4
    this.w_ORCODVIS = t_ORCODVIS
    this.w_ORLOGSTD = t_ORLOGSTD
    this.w_ORUMNODI = t_ORUMNODI
    this.w_ORTOTRIG = t_ORTOTRIG
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ORCODICE with this.w_ORCODICE
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ORTIPRIG with this.w_ORTIPRIG
    replace t_ORCODART with this.w_ORCODART
    replace t_ORDESSUP with this.w_ORDESSUP
    replace t_ORUNIMIS with this.w_ORUNIMIS
    replace t_ORQTAMOV with this.w_ORQTAMOV
    replace t_ORQTAUM1 with this.w_ORQTAUM1
    replace t_ORPREZZO with this.w_ORPREZZO
    replace t_ORCODIVA with this.w_ORCODIVA
    replace t_ORFLOMAG with this.oPgFrm.Page1.oPag.oORFLOMAG_2_11.ToRadio()
    replace t_ORDATEVA with this.w_ORDATEVA
    replace t_ORCODLIS with this.w_ORCODLIS
    replace t_ORCONTRA with this.w_ORCONTRA
    replace t_ORDESART with this.w_ORDESART
    replace t_ORSCOIN1 with this.w_ORSCOIN1
    replace t_ORSCOIN2 with this.w_ORSCOIN2
    replace t_ORSCOIN3 with this.w_ORSCOIN3
    replace t_ORSCOIN4 with this.w_ORSCOIN4
    replace t_ORSCONT1 with this.w_ORSCONT1
    replace t_ORSCONT2 with this.w_ORSCONT2
    replace t_ORSCONT3 with this.w_ORSCONT3
    replace t_ORSCONT4 with this.w_ORSCONT4
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ORSCONT1 with this.w_ORSCONT1
    replace t_ORSCONT2 with this.w_ORSCONT2
    replace t_ORSCONT3 with this.w_ORSCONT3
    replace t_ORSCONT4 with this.w_ORSCONT4
    replace t_ORCODVIS with this.w_ORCODVIS
    replace t_ORLOGSTD with this.w_ORLOGSTD
    replace t_ORUMNODI with this.w_ORUMNODI
    replace t_ORTOTRIG with this.w_ORTOTRIG
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_ORTOTGEN = .w_ORTOTGEN-.w_ortotrig
        .SetControlsValue()
      endwith
  EndProc
  func CanEdit()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Documento in sola lettura. Impossibile registrare le variazioni."))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile caricare un nuovo documento"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Documento in sola lettura. Impossibile cancellarlo."))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscp_mowPag1 as StdContainer
  Width  = 753
  height = 430
  stdWidth  = 753
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORTIPDOC_1_3 as StdField with uid="ETWFLXNWTV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ORTIPDOC", cQueryName = "ORTIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del documento",;
    HelpContextID = 260375081,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=108, Top=45, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDCATDOC", oKey_1_2="this.w_ORTIPDOC"

  func oORTIPDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORNUMDOC_1_5 as StdField with uid="ARLFPWQJSW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ORNUMDOC", cQueryName = "ORNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento ordine web",;
    HelpContextID = 257991209,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=108, Top=15, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oORALFDOC_1_7 as StdField with uid="PXLWBERWCO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORALFDOC", cQueryName = "ORALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfanumerico ordine web",;
    HelpContextID = 250008105,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=238, Top=15, InputMask=replicate('X',10)

  add object oTDDESDOC_1_9 as StdField with uid="PSNUORMZNQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 263189625,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=160, Top=45, InputMask=replicate('X',35)

  add object oORNUMEST_1_10 as StdField with uid="HSUFBQJYMX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ORNUMEST", cQueryName = "ORNUMEST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ordine assegnato dal cliente",;
    HelpContextID = 6332986,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=534, Top=15, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oORNUMEST_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
    endif
  endfunc

  add object oORALFEST_1_12 as StdField with uid="EHDWZRSQPP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ORALFEST", cQueryName = "ORALFEST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfanumerico ordine assegnato dal cliente",;
    HelpContextID = 266785338,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=665, Top=15, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oORALFEST_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
    endif
  endfunc

  add object oORDATEST_1_14 as StdField with uid="SCQECDVIOR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ORDATEST", cQueryName = "ORDATEST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ordine assegnata dal cliente",;
    HelpContextID = 12321338,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=665, Top=45

  func oORDATEST_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
    endif
  endfunc

  add object oORCODCON_1_17 as StdField with uid="JMCOMPULUL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ORCODCON", cQueryName = "ORCODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente intestatario dell'ordine web",;
    HelpContextID = 231338548,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=108, Top=75, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ORTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ORCODCON"

  func oORCODCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ORCODDES)
        bRes2=.link_5_1('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oORCODVAL_1_18 as StdField with uid="LYFBUMRREB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ORCODVAL", cQueryName = "ORCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta ordine web",;
    HelpContextID = 13234738,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=567, Top=75, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_ORCODVAL"

  func oORCODVAL_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oANDESCRI_1_20 as StdField with uid="TECNDEWAPM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246414671,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=230, Top=75, InputMask=replicate('X',40)

  add object oORDATDOC_1_24 as StdField with uid="PUIKKQMFBW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ORDATDOC", cQueryName = "ORDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ordine",;
    HelpContextID = 263979561,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=374, Top=15

  add object oORTDTEVA_1_25 as StdField with uid="LONFSMGJYV",rtseq=52,rtrep=.f.,;
    cFormVar = "w_ORTDTEVA", cQueryName = "ORTDTEVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di prevista evasione",;
    HelpContextID = 255851993,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=108, Top=104

  add object oORRIFEST_1_34 as StdField with uid="OKLJLFQLKJ",rtseq=95,rtrep=.f.,;
    cFormVar = "w_ORRIFEST", cQueryName = "ORRIFEST",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 266658362,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=525, Top=45, InputMask=replicate('X',40)

  func oORRIFEST_1_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=10)
    endwith
    endif
  endfunc

  add object oORVALNAZ_1_36 as StdField with uid="DSEROSLNNV",rtseq=96,rtrep=.f.,;
    cFormVar = "w_ORVALNAZ", cQueryName = "ORVALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 155001408,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=714, Top=74, InputMask=replicate('X',3)

  add object oORCAOVAL_1_38 as StdField with uid="MLADWPHMKF",rtseq=97,rtrep=.f.,;
    cFormVar = "w_ORCAOVAL", cQueryName = "ORCAOVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 23851570,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=567, Top=104


  add object oBtn_1_40 as StdButton with uid="ASZABIWNPH",left=699, top=104, width=48,height=45,;
    CpPicture="bmp\Controlla.bmp", caption="", nPag=1;
    , ToolTipText = "Controllo difformit� prezzo/sconti/maggiorazioni tra ordine web e ordine gestionale";
    , HelpContextID = 215074178;
    , Caption='\<Contr.Dif.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      do gscp_kow with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(g_typebutton=11 or g_typebutton=1))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=21, top=142, width=670,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="ORCODVIS",Label2="Articolo",Field3="ORDESART",Label3="Descrizione",Field4="ORUNIMIS",Label4="U.M.",Field5="ORQTAMOV",Label5="Quantit�",Field6="ORPREZZO",Label6="Prezzo unitario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 151884922

  add object oStr_1_4 as StdString with uid="OEOFMOKOLY",Visible=.t., Left=12, Top=45,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="AHOBRWWIZI",Visible=.t., Left=54, Top=15,;
    Alignment=1, Width=52, Height=18,;
    Caption="N. doc.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="XJAPBFTZLO",Visible=.t., Left=226, Top=15,;
    Alignment=2, Width=12, Height=18,;
    Caption="/"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="LMLWZUTGCJ",Visible=.t., Left=483, Top=15,;
    Alignment=1, Width=47, Height=18,;
    Caption="N.rif.:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="SYCQZJBYFU",Visible=.t., Left=653, Top=15,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="RXDRTKIZLJ",Visible=.t., Left=633, Top=45,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (g_typebutton<>10)
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="VLZWXJYGTH",Visible=.t., Left=508, Top=78,;
    Alignment=1, Width=58, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ADPYKZLGIP",Visible=.t., Left=43, Top=76,;
    Alignment=1, Width=63, Height=15,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_ORTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="IXCTWVFNCM",Visible=.t., Left=44, Top=76,;
    Alignment=1, Width=62, Height=15,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_ORTIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="GYLNWSHTFM",Visible=.t., Left=325, Top=15,;
    Alignment=1, Width=47, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="OQUGTWKVZU",Visible=.t., Left=48, Top=104,;
    Alignment=1, Width=58, Height=18,;
    Caption="Evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QVWTUONEKV",Visible=.t., Left=475, Top=362,;
    Alignment=1, Width=54, Height=18,;
    Caption="Omaggio:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="QDASKCMNQR",Visible=.t., Left=464, Top=45,;
    Alignment=1, Width=59, Height=18,;
    Caption="Rif. est.:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (g_typebutton=10)
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="OLIEYZWMGQ",Visible=.t., Left=604, Top=78,;
    Alignment=1, Width=109, Height=18,;
    Caption="Cod.val.nazionale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DHBCRQVRUE",Visible=.t., Left=501, Top=108,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=11,top=161,;
    width=666+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=12,top=162,width=665+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oORFLOMAG_2_11.Refresh()
      this.Parent.oORSCONT1_2_20.Refresh()
      this.Parent.oORSCONT2_2_21.Refresh()
      this.Parent.oORSCONT3_2_22.Refresh()
      this.Parent.oORSCONT4_2_23.Refresh()
      this.Parent.oCPROWORD_2_24.Refresh()
      this.Parent.oORSCONT1_2_25.Refresh()
      this.Parent.oORSCONT2_2_26.Refresh()
      this.Parent.oORSCONT3_2_27.Refresh()
      this.Parent.oORSCONT4_2_28.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oORFLOMAG_2_11 as StdTrsCombo with uid="RHMLMLSCBX",rtrep=.t.,;
    cFormVar="w_ORFLOMAG", RowSource=""+"Sconto merce,"+"Imponibile,"+"Imponibile+IVA,"+"No omaggio" , enabled=.f.,;
    ToolTipText = "Test omaggio",;
    HelpContextID = 142025261,;
    Height=26, Width=137, Left=534, Top=360,;
    cTotal="", cQueryName = "ORFLOMAG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oORFLOMAG_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ORFLOMAG,&i_cF..t_ORFLOMAG),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'I',;
    iif(xVal =3,'E',;
    iif(xVal =4,'X',;
    space(1))))))
  endfunc
  func oORFLOMAG_2_11.GetRadio()
    this.Parent.oContained.w_ORFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oORFLOMAG_2_11.ToRadio()
    this.Parent.oContained.w_ORFLOMAG=trim(this.Parent.oContained.w_ORFLOMAG)
    return(;
      iif(this.Parent.oContained.w_ORFLOMAG=='S',1,;
      iif(this.Parent.oContained.w_ORFLOMAG=='I',2,;
      iif(this.Parent.oContained.w_ORFLOMAG=='E',3,;
      iif(this.Parent.oContained.w_ORFLOMAG=='X',4,;
      0)))))
  endfunc

  func oORFLOMAG_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oORFLOMAG_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
    endif
  endfunc

  add object oORSCONT1_2_20 as StdTrsField with uid="KKDMQGGOLJ",rtseq=64,rtrep=.t.,;
    cFormVar="w_ORSCONT1",value=0,;
    ToolTipText = "1^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265879,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=65, Top=377, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT1_2_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
    endif
  endfunc

  add object oORSCONT2_2_21 as StdTrsField with uid="GNDCRCIPAT",rtseq=65,rtrep=.t.,;
    cFormVar="w_ORSCONT2",value=0,;
    ToolTipText = "2^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265880,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=127, Top=377, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT2_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
    endif
  endfunc

  add object oORSCONT3_2_22 as StdTrsField with uid="BSIRPBFDCB",rtseq=66,rtrep=.t.,;
    cFormVar="w_ORSCONT3",value=0,;
    ToolTipText = "3^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265881,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=189, Top=377, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT3_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
    endif
  endfunc

  add object oORSCONT4_2_23 as StdTrsField with uid="ZGLELOYCLS",rtseq=67,rtrep=.t.,;
    cFormVar="w_ORSCONT4",value=0,;
    ToolTipText = "4^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265882,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=251, Top=377, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT4_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
    endif
  endfunc

  add object oCPROWORD_2_24 as StdTrsField with uid="NOKJWYNMJC",rtseq=68,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 184213354,;
    cTotal="", bFixedPos=.t., cQueryName = "CPROWORD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=7, Top=377, cSayPict=["99999"], cGetPict=["99999"]

  func oCPROWORD_2_24.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
    endif
  endfunc

  add object oORSCONT1_2_25 as StdTrsField with uid="ZGDSKHADNV",rtseq=69,rtrep=.t.,;
    cFormVar="w_ORSCONT1",value=0,;
    ToolTipText = "1^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265879,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=204, Top=401, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT1_2_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
    endif
  endfunc

  add object oORSCONT2_2_26 as StdTrsField with uid="DMWNDFTWGM",rtseq=70,rtrep=.t.,;
    cFormVar="w_ORSCONT2",value=0,;
    ToolTipText = "2^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265880,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=266, Top=401, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT2_2_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
    endif
  endfunc

  add object oORSCONT3_2_27 as StdTrsField with uid="QENDMUYFNO",rtseq=71,rtrep=.t.,;
    cFormVar="w_ORSCONT3",value=0,;
    ToolTipText = "3^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265881,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=328, Top=401, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT3_2_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
    endif
  endfunc

  add object oORSCONT4_2_28 as StdTrsField with uid="WBMOIUAXEF",rtseq=72,rtrep=.t.,;
    cFormVar="w_ORSCONT4",value=0,;
    ToolTipText = "4^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 158265882,;
    cTotal="", bFixedPos=.t., cQueryName = "ORSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=390, Top=401, cSayPict=["999.99"], cGetPict=["999.99"]

  func oORSCONT4_2_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
    endif
  endfunc

  add object oBtn_2_29 as StdButton with uid="HRZPRLMIVV",width=48,height=45,;
   left=699, top=147,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 164205834;
    , TabStop=.f., Caption='\<Righe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      do GSCP_KDR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(g_typebutton=11 or g_typebutton=1))
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oORTOTGEN_3_11 as StdField with uid="OCRESHCSHL",rtseq=93,rtrep=.f.,;
    cFormVar="w_ORTOTGEN",value=0,enabled=.f.,;
    HelpContextID = 221576652,;
    cQueryName = "ORTOTGEN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=534, Top=398, cSayPict=[v_GV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oStr_3_1 as StdString with uid="ZXSJVSWNEC",Visible=.t., Left=66, Top=356,;
    Alignment=0, Width=174, Height=18,;
    Caption="Sconti / maggiorazioni di riga"  ;
  , bGlobalFont=.t.

  func oStr_3_1.mHide()
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
  endfunc

  add object oStr_3_2 as StdString with uid="ELMDTJTBCP",Visible=.t., Left=11, Top=356,;
    Alignment=0, Width=33, Height=18,;
    Caption="Riga"  ;
  , bGlobalFont=.t.

  func oStr_3_2.mHide()
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
  endfunc

  add object oStr_3_3 as StdString with uid="JFQEHSCWZM",Visible=.t., Left=115, Top=379,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_3.mHide()
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
  endfunc

  add object oStr_3_4 as StdString with uid="TXAYMYLVBI",Visible=.t., Left=175, Top=379,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_4.mHide()
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
  endfunc

  add object oStr_3_5 as StdString with uid="OMEFFUXJHM",Visible=.t., Left=238, Top=379,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_5.mHide()
    with this.Parent.oContained
      return (g_typebutton=1)
    endwith
  endfunc

  add object oStr_3_6 as StdString with uid="GWSZOMPLYD",Visible=.t., Left=449, Top=401,;
    Alignment=1, Width=82, Height=18,;
    Caption="Totale righe:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="KABLJEKQXF",Visible=.t., Left=102, Top=401,;
    Alignment=1, Width=98, Height=18,;
    Caption="Sconti / magg.ni:"  ;
  , bGlobalFont=.t.

  func oStr_3_7.mHide()
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
  endfunc

  add object oStr_3_8 as StdString with uid="DKDNOIYDWN",Visible=.t., Left=253, Top=403,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_8.mHide()
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
  endfunc

  add object oStr_3_9 as StdString with uid="WXBXWYHQHD",Visible=.t., Left=313, Top=403,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_9.mHide()
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
  endfunc

  add object oStr_3_10 as StdString with uid="AIGFYCPKRY",Visible=.t., Left=376, Top=403,;
    Alignment=2, Width=14, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_10.mHide()
    with this.Parent.oContained
      return (g_typebutton<>1)
    endwith
  endfunc
enddefine

  define class tgscp_mowPag2 as StdContainer
    Width  = 753
    height = 430
    stdWidth  = 753
    stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORSCOCL2_4_1 as StdField with uid="BTTXXICVKY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ORSCOCL2", cQueryName = "ORSCOCL2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativo) applicata al cliente",;
    HelpContextID = 26283496,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=176, Top=51, cSayPict='"9999.99"', cGetPict='"9999.99"'

  add object oORSCOPAG_4_3 as StdField with uid="IDXSQMNQEB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ORSCOPAG", cQueryName = "ORSCOPAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativo) applicata al pagamento",;
    HelpContextID = 191820333,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=319, Top=51, cSayPict='"9999.99"', cGetPict='"9999.99"'

  add object oORSCOCL1_4_5 as StdField with uid="FSVEIVNAEU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ORSCOCL1", cQueryName = "ORSCOCL1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ Maggiorazione (se positiva) o sconto (se negativo) applicata al cliente",;
    HelpContextID = 26283497,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=101, Top=51, cSayPict='"9999.99"', cGetPict='"9999.99"'

  add object oORSPEINC_4_7 as StdField with uid="IQRFNISGLT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ORSPEINC", cQueryName = "ORSPEINC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di incasso lette dal codice pagamento",;
    HelpContextID = 64746025,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=96, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORSPETRA_4_9 as StdField with uid="TTIMYVCPVT",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ORSPETRA", cQueryName = "ORSPETRA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di trasporto",;
    HelpContextID = 249295399,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=144, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORSPEIMB_4_11 as StdField with uid="FOEAVORPFJ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ORSPEIMB", cQueryName = "ORSPEIMB",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di imballo",;
    HelpContextID = 64746024,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=120, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORCODPAG_4_13 as StdField with uid="IYJLDZOWJU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ORCODPAG", cQueryName = "ORCODPAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento associato all'ordine web",;
    HelpContextID = 181006893,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=101, Top=17, InputMask=replicate('X',5), cLinkFile="PAG_AMEN", oKey_1_1="PACODICE", oKey_1_2="this.w_ORCODPAG"

  func oORCODPAG_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORTOTORD_4_17 as StdField with uid="EVYUSQCLAC",rtseq=38,rtrep=.f.,;
    cFormVar = "w_ORTOTORD", cQueryName = "ORTOTORD",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale documento",;
    HelpContextID = 181076522,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=200, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oVASIMVAL_4_21 as StdField with uid="KFKIBHHVAD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_VASIMVAL", cQueryName = "VASIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Valuta ordine web",;
    HelpContextID = 22340002,;
   bGlobalFont=.t.,;
    Height=21, Width=43, Left=541, Top=200, InputMask=replicate('X',5)

  add object oPADESCRI_4_32 as StdField with uid="WMVIDZGNEB",rtseq=50,rtrep=.f.,;
    cFormVar = "w_PADESCRI", cQueryName = "PADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 246411583,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=157, Top=17, InputMask=replicate('X',30)

  add object oORSCONTI_4_34 as StdField with uid="JHFHUAZUHO",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ORSCONTI", cQueryName = "ORSCONTI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale sconti globali (cliente + pagamento)",;
    HelpContextID = 158265903,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=72, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORCODAGE_4_35 as StdField with uid="YQBHZJKUOC",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ORCODAGE", cQueryName = "ORCODAGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 70651349,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=101, Top=81, InputMask=replicate('X',5), cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ORCODAGE"

  func oORCODAGE_4_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORTIPCON<>'C')
    endwith
    endif
  endfunc

  func oORCODAGE_4_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oAGDESAGE_4_37 as StdField with uid="AXHUSRTRXF",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AGDESAGE", cQueryName = "AGDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 55577013,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=157, Top=81, InputMask=replicate('X',35)

  func oAGDESAGE_4_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORTIPCON<>'C')
    endwith
    endif
  endfunc

  add object oORTOTRIP_4_38 as StdField with uid="VHVVUBVWZJ",rtseq=94,rtrep=.f.,;
    cFormVar = "w_ORTOTRIP", cQueryName = "ORTOTRIP",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37027274,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=49, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORACCONT_4_39 as StdField with uid="QPXRBADMAY",rtseq=98,rtrep=.f.,;
    cFormVar = "w_ORACCONT", cQueryName = "ORACCONT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162386490,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=232, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oORVALACC_4_41 as StdField with uid="STNKXHPPLQ",rtseq=99,rtrep=.f.,;
    cFormVar = "w_ORVALACC", cQueryName = "ORVALACC",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 205333033,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=427, Top=232, InputMask=replicate('X',3)

  add object oORSPEBOL_4_44 as StdField with uid="ODNPSOHHTV",rtseq=100,rtrep=.f.,;
    cFormVar = "w_ORSPEBOL", cQueryName = "ORSPEBOL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese bolli",;
    HelpContextID = 215740978,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=590, Top=164, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oStr_4_4 as StdString with uid="ASRLTZBCDZ",Visible=.t., Left=247, Top=55,;
    Alignment=1, Width=69, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_6 as StdString with uid="JDUVMCWUDW",Visible=.t., Left=455, Top=76,;
    Alignment=1, Width=128, Height=18,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="YSAUQDHUPC",Visible=.t., Left=466, Top=100,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="QNQUQAMOYP",Visible=.t., Left=474, Top=148,;
    Alignment=1, Width=109, Height=18,;
    Caption="Spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="FAGMFJKJEQ",Visible=.t., Left=470, Top=124,;
    Alignment=1, Width=113, Height=18,;
    Caption="Spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="LTDYJTGFAE",Visible=.t., Left=27, Top=21,;
    Alignment=1, Width=73, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="VTKTRDZRAX",Visible=.t., Left=3, Top=55,;
    Alignment=1, Width=97, Height=18,;
    Caption="Magg./sconti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="TGBHMTCQNF",Visible=.t., Left=159, Top=55,;
    Alignment=1, Width=11, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="VKRTDLLMMW",Visible=.t., Left=482, Top=204,;
    Alignment=1, Width=44, Height=18,;
    Caption="Totale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_19 as StdString with uid="KQWYOAMCQN",Visible=.t., Left=467, Top=14,;
    Alignment=0, Width=130, Height=18,;
    Caption="Riepilogo ordine web"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="YGCERMABEM",Visible=.t., Left=467, Top=53,;
    Alignment=1, Width=116, Height=18,;
    Caption="Totale netto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_36 as StdString with uid="MHFTISZNYZ",Visible=.t., Left=39, Top=84,;
    Alignment=1, Width=54, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_4_36.mHide()
    with this.Parent.oContained
      return (.w_ORTIPCON<>'C')
    endwith
  endfunc

  add object oStr_4_40 as StdString with uid="CSWIMABINF",Visible=.t., Left=470, Top=235,;
    Alignment=1, Width=113, Height=18,;
    Caption="Acconto contestuale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="OUOJLJDJYU",Visible=.t., Left=242, Top=235,;
    Alignment=1, Width=181, Height=18,;
    Caption="Valuta acconto contestuale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="QFLSAIOIOK",Visible=.t., Left=483, Top=169,;
    Alignment=1, Width=100, Height=15,;
    Caption="Spese bolli:"  ;
  , bGlobalFont=.t.

  add object oBox_4_20 as StdBox with uid="ZANMDPUPBY",left=467, top=34, width=260,height=1
enddefine

  define class tgscp_mowPag3 as StdContainer
    Width  = 753
    height = 430
    stdWidth  = 753
    stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORCODDES_5_1 as StdField with uid="RBVZHTNKWC",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ORCODDES", cQueryName = "ORCODDES",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice destinazione",;
    HelpContextID = 248115769,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=16, InputMask=replicate('X',5), cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_ORCODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_ORCODDES"

  func oORCODDES_5_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORCODSPE_5_3 as StdField with uid="JCTBNOCLIN",rtseq=74,rtrep=.f.,;
    cFormVar = "w_ORCODSPE", cQueryName = "ORCODSPE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del mezzo di spedizione",;
    HelpContextID = 231338539,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=181, InputMask=replicate('X',3), cLinkFile="MODASPED", oKey_1_1="SPCODSPE", oKey_1_2="this.w_ORCODSPE"

  func oORCODSPE_5_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORCODVET_5_5 as StdField with uid="CVBXKFGYLL",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ORCODVET", cQueryName = "ORCODVET",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del vettore incaricato del trasporto",;
    HelpContextID = 255200710,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=115, InputMask=replicate('X',5), cLinkFile="VETTORI", oKey_1_1="VTCODVET", oKey_1_2="this.w_ORCODVET"

  func oORCODVET_5_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORCODPOR_5_7 as StdField with uid="OGGVJHKDGK",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ORCODPOR", cQueryName = "ORCODPOR",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Tipo porto",;
    HelpContextID = 181006904,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=148, InputMask=replicate('X',1), cLinkFile="PORTI", oKey_1_1="POCODPOR", oKey_1_2="this.w_ORCODPOR"

  func oORCODPOR_5_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oOR__NOTE_5_10 as StdMemo with uid="QUVMSMLUFA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_OR__NOTE", cQueryName = "OR__NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note cliente associate all'ordine",;
    HelpContextID = 175878699,;
   bGlobalFont=.t.,;
    Height=123, Width=260, Left=442, Top=115, Enabled=.T., ReadOnly=.T.

  func oOR__NOTE_5_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_typebutton=1 OR g_CPIN='S')
    endwith
    endif
  endfunc

  add object oDDNOMDES_5_12 as StdField with uid="DHSSTCEQKK",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DDNOMDES", cQueryName = "DDNOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 257594249,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=125, Top=38, InputMask=replicate('X',40)

  add object oDDINDIRI_5_13 as StdField with uid="SQHOUUHGRU",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DDINDIRI", cQueryName = "DDINDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63521663,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=125, Top=62, InputMask=replicate('X',35)

  add object oDD___CAP_5_14 as StdField with uid="QSPVLGNSOL",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DD___CAP", cQueryName = "DD___CAP",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 260809606,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=125, Top=85, InputMask=replicate('X',5)

  add object oDDLOCALI_5_15 as StdField with uid="SPVYCBCTXO",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DDLOCALI", cQueryName = "DDLOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 71666817,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=174, Top=85, InputMask=replicate('X',30)

  add object oDDPROVIN_5_16 as StdField with uid="ROPNRZCPSO",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DDPROVIN", cQueryName = "DDPROVIN",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 243420284,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=383, Top=85, InputMask=replicate('X',2)

  add object oVTDESVET_5_17 as StdField with uid="IZUYWIVXBF",rtseq=83,rtrep=.f.,;
    cFormVar = "w_VTDESVET", cQueryName = "VTDESVET",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240122710,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=174, Top=115, InputMask=replicate('X',35)

  add object oPODESPOR_5_18 as StdField with uid="HOWEPDIVKU",rtseq=84,rtrep=.f.,;
    cFormVar = "w_PODESPOR", cQueryName = "PODESPOR",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 196083528,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=174, Top=148, InputMask=replicate('X',30)

  add object oSPDESSPE_5_19 as StdField with uid="MTOTQTXQBC",rtseq=85,rtrep=.f.,;
    cFormVar = "w_SPDESSPE", cQueryName = "SPDESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 246415467,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=174, Top=181, InputMask=replicate('X',35)

  add object oCONCONDES_5_20 as StdField with uid="YOOYOPGJMS",rtseq=86,rtrep=.f.,;
    cFormVar = "w_CONCONDES", cQueryName = "CONCONDES",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Condizioni di consegna",;
    HelpContextID = 158245787,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=125, Top=217, InputMask=replicate('X',20)

  add object oStr_5_2 as StdString with uid="QKTRUEGUWE",Visible=.t., Left=44, Top=20,;
    Alignment=1, Width=77, Height=18,;
    Caption="Spedire a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_4 as StdString with uid="BQBYWRLXFQ",Visible=.t., Left=44, Top=185,;
    Alignment=1, Width=77, Height=18,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_5_6 as StdString with uid="NDTOCDYXDP",Visible=.t., Left=44, Top=119,;
    Alignment=1, Width=77, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_5_8 as StdString with uid="TKIANDTRMQ",Visible=.t., Left=44, Top=152,;
    Alignment=1, Width=77, Height=18,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="AQAUQFYWMJ",Visible=.t., Left=11, Top=219,;
    Alignment=1, Width=110, Height=18,;
    Caption="Cond. di consegna:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="HSQKVWHXQG",Visible=.t., Left=436, Top=98,;
    Alignment=1, Width=35, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  func oStr_5_11.mHide()
    with this.Parent.oContained
      return (g_typebutton=1 OR g_CPIN='S')
    endwith
  endfunc

  add object oStr_5_22 as StdString with uid="KXRVHWONHS",Visible=.t., Left=50, Top=42,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_5_23 as StdString with uid="FSIRHFYRWE",Visible=.t., Left=64, Top=66,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="NFUBEENVTA",Visible=.t., Left=38, Top=89,;
    Alignment=1, Width=83, Height=18,;
    Caption="CAP/loc./prov.:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgscp_mowBodyRow as CPBodyRowCnt
  Width=656
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="NBQTGQEKCY",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 184213354,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oORUNIMIS_2_6 as StdTrsField with uid="MLOPJYIXUY",rtseq=11,rtrep=.t.,;
    cFormVar="w_ORUNIMIS",value=space(3),;
    HelpContextID = 132509127,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=401, Top=0, InputMask=replicate('X',3)

  add object oORQTAMOV_2_7 as StdTrsField with uid="OQJYDBYHVG",rtseq=12,rtrep=.t.,;
    cFormVar="w_ORQTAMOV",value=0,;
    HelpContextID = 127914556,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=434, Top=0, cSayPict=["999999999.999"], cGetPict=["999999999.999"]

  add object oORPREZZO_2_9 as StdTrsField with uid="CJNRMPHUSE",rtseq=14,rtrep=.t.,;
    cFormVar="w_ORPREZZO",value=0,;
    HelpContextID = 186793419,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=525, Top=0, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  add object oORDESART_2_15 as StdTrsField with uid="BAHMRDEYQW",rtseq=37,rtrep=.t.,;
    cFormVar="w_ORDESART",value=space(40),;
    HelpContextID = 212861498,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=196, Left=200, Top=0, InputMask=replicate('X',40)

  add object oORCODVIS_2_30 as StdTrsField with uid="QRYMUOMIBY",rtseq=88,rtrep=.t.,;
    cFormVar="w_ORCODVIS",value=space(40),;
    HelpContextID = 255200711,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=40, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_mow','ZORDWEBM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ORSERIAL=ZORDWEBM.ORSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
