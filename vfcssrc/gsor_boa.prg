* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_boa                                                        *
*              Check causale mag. ordini aperti                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-16                                                      *
* Last revis.: 2006-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_boa",oParentObject)
return(i_retval)

define class tgsor_boa as StdBatch
  * --- Local variables
  w_OK = .f.
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo causale di magazzino X ordini aperti
    this.w_OK = .T.
    * --- Se movimenta esistenza o ordinato o impegnato o riservato, controllo se �
    *     utilizzato in qualche causale con flag ordine aperto
    if ! EMPTY(this.oParentObject.w_CMFLCASC+this.oParentObject.w_CMFLORDI+this.oParentObject.w_CMFLIMPE+this.oParentObject.w_CMFLRISE)
      * --- Select from TIP_DOCU
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select TDTIPDOC  from "+i_cTable+" TIP_DOCU ";
            +" where TDCAUMAG = "+cp_ToStrODBC(this.oParentObject.w_CMCODICE)+" AND TDORDAPE = 'S'";
             ,"_Curs_TIP_DOCU")
      else
        select TDTIPDOC from (i_cTable);
         where TDCAUMAG = this.oParentObject.w_CMCODICE AND TDORDAPE = "S";
          into cursor _Curs_TIP_DOCU
      endif
      if used('_Curs_TIP_DOCU')
        select _Curs_TIP_DOCU
        locate for 1=1
        do while not(eof())
        * --- La causale � utilizzata in una causale doc ordine aperto
        this.w_OK = .F.
          select _Curs_TIP_DOCU
          continue
        enddo
        use
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
