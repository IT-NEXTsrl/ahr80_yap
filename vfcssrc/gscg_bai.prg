* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bai                                                        *
*              Elabora/stampa acconto IVA                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_223]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2015-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bai",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bai as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_NOSPLIT = space(10)
  w_ANNO = space(4)
  w_NUMPER = 0
  w_KEYATT = space(5)
  w_IMPO15 = 0
  w_IMPO16 = 0
  w_IMPO12 = 0
  w_IMPO13 = 0
  w_MESS = space(50)
  w_APPO = 0
  w_APPO1 = 0
  w_IMPEUR = space(1)
  w_LOCAZI = space(35)
  w_INDAZI = space(25)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_PAGINE = 0
  w_CODAZI = space(5)
  w_CODATT = space(5)
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_MCONCES = space(3)
  w_MTIPLIQ = space(1)
  w_MTIPDIC = space(1)
  w_MFLTEST = space(1)
  w_CODATT = space(5)
  w_DESATT = space(35)
  w_FLDEFI = space(1)
  w_DENMAG = 0
  w_IVACAR = space(1)
  w_IVACOF = space(16)
  w_MCOFAZI = space(16)
  w_MPIVAZI = space(12)
  w_ATTIPREG = space(1)
  w_ATNUMREG = 0
  w_VIMPOR6 = 0
  w_VPIMPO7A = 0
  w_VPIMPO7B = 0
  w_VPIMPO7C = 0
  w_VIMPOR8 = 0
  w_VIMPOR9 = 0
  w_VIMPO10 = 0
  w_VIMPO11 = 0
  w_VIMPO12 = 0
  w_VIMPO13 = 0
  w_VIMPO15 = 0
  w_VIMPO16 = 0
  w_MACIVA1 = 0
  w_CRERES = 0
  w_VDICSOC = space(1)
  w_ACCSTO = 0
  w_ACCONTO = 0
  w_F2DEFI = space(1)
  w_METOD = space(1)
  w_SIMVA1 = space(5)
  w_PEAIVA1 = 0
  w_OK = .f.
  w_STORICO = space(1)
  w_FLPROR = space(1)
  w_FLAGRI = space(1)
  w_PERPRO = 0
  w_ACCIVA2 = 0
  w_RDESABI = space(80)
  w_RDESFIL = space(40)
  w_RINDIRI = space(50)
  w_RCAP = space(5)
  w_RDESBAN = space(35)
  w_RCODABI = space(5)
  w_RCODCAB = space(5)
  w_CODBAN1 = space(15)
  w_RCONCES = space(3)
  w_RDATVER = ctod("  /  /  ")
  w_OKELAB = space(1)
  w_STAREG = space(1)
  w_STOPELA = .f.
  w_LIMPO12 = 0
  w_LIMPO13 = 0
  w_LNUMPER = 0
  w_LIMPEUR = space(1)
  w_CHANGEPER = .f.
  w_OLDNUM = 0
  w_ANNO1 = space(4)
  w_NUMPER1 = 0
  w_MINIVE = 0
  w_MINIVA = 0
  w_MINVER = 0
  w_PRCRERIM = 0
  w_PRCREUTI = 0
  * --- WorkFile variables
  IVA_PERI_idx=0
  VALUTE_idx=0
  DAT_IVAN_idx=0
  AZIENDA_idx=0
  ATTIDETT_idx=0
  COC_MAST_idx=0
  COD_ABI_idx=0
  COD_CAB_idx=0
  DAT_IVAN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa/Calcola Acconto IVA (da GSCG_SAI)
    * --- Variabili definite all'interno del batch GSCG_BDI come caller
    if this.oParentObject.w_METODO="A"
      if this.oParentObject.w_ACCIVA=0 OR this.oParentObject.w_ACCIVA<this.oParentObject.w_MACIVA
        ah_ErrorMsg("Acconto da versare (%1) inferiore al minimo da versare",,"",ALLTRIM(STR(this.oParentObject.w_ACCIVA,18,2)))
        this.oParentObject.w_ACCIVA = 0
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_OKELAB = "F"
    this.w_NOSPLIT = this.oParentObject.w_NOSPLIT1
    this.w_METOD = this.oParentObject.w_METODO
    this.w_CODAZI = this.oParentObject.w_CODAZI1
    this.w_CODATT = this.oParentObject.w_CODATT1
    this.w_PRPARI = this.oParentObject.w_PRPARI2
    this.w_INTLIG = this.oParentObject.w_INTLIG2
    this.w_PREFIS = this.oParentObject.w_PREFIS2
    this.w_STAREG = "V"
    this.w_STORICO = "N"
    this.w_FLPROR = " "
    this.w_VIMPOR6 = 0
    this.w_FLAGRI = " "
    this.w_PERPRO = 0
    * --- Controllo se nella maschera � presente la valuta
    this.w_ANNO = STR(YEAR(this.oParentObject.w_DATSTA) -1, 4, 0)
    this.w_NUMPER = IIF(g_TIPDEN="M", 12, 4)
    this.w_KEYATT = IIF(g_ATTIVI="S", "#####", g_CATAZI)
    * --- Nel caso di acconto lancio la liquidazione periodica con 'A' in modo da poterla differenziare dalle altre
    *     Forzo il tipo liquidazione Riepilogativa MTIPLIQ='R' in modo che elabori tutte le attivit�
    this.w_FLDEFI = "A"
    this.w_MTIPLIQ = "R"
    this.w_MACIVA1 = this.oParentObject.w_MACIVA
    this.w_SIMVA1 = this.oParentObject.w_SIMVAL
    this.w_PEAIVA1 = this.oParentObject.w_PEAIVA
    this.w_ACCIVA2 = this.oParentObject.w_ACCIVA
    * --- Variabili utilizzate nel batch GSCG_BA2 per stampa Operaizoni effettuate
    this.w_CODBAN1 = this.oParentObject.w_CODBAN
    this.w_RCONCES = this.oParentObject.w_CONCES1
    this.w_RDATVER = this.oParentObject.w_DATVER
    if EMPTY(this.oParentObject.w_VALIVA)
      ah_ErrorMsg("Valuta non specificata in parametri IVA",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.w_KEYATT)
      ah_ErrorMsg("Codice attivita principale non specificata in parametri IVA",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Leggo l'ultima Dichiarazione del precedente Anno
    this.w_LIMPO12 = 0
    this.w_LIMPO13 = 0
    this.w_IMPO15 = 0
    this.w_IMPO16 = 0
    this.w_APPO = 0
    this.w_IMPEUR = " "
    this.w_LNUMPER = 0
    this.w_LIMPEUR = " "
    * --- Read from IVA_PERI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VPIMPO12,VPIMPO13,VPPERIOD,VPCODVAL"+;
        " from "+i_cTable+" IVA_PERI where ";
            +"VP__ANNO = "+cp_ToStrODBC(this.w_ANNO);
            +" and VPPERIOD = "+cp_ToStrODBC(this.w_NUMPER);
            +" and VPKEYATT = "+cp_ToStrODBC(this.w_KEYATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VPIMPO12,VPIMPO13,VPPERIOD,VPCODVAL;
        from (i_cTable) where;
            VP__ANNO = this.w_ANNO;
            and VPPERIOD = this.w_NUMPER;
            and VPKEYATT = this.w_KEYATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IMPO12 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
      this.w_IMPO13 = NVL(cp_ToDate(_read_.VPIMPO13),cp_NullValue(_read_.VPIMPO13))
      this.w_APPO = NVL(cp_ToDate(_read_.VPPERIOD),cp_NullValue(_read_.VPPERIOD))
      this.w_IMPEUR = NVL(cp_ToDate(_read_.VPCODVAL),cp_NullValue(_read_.VPCODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CHANGEPER = .F.
    if this.oParentObject.w_METODO="S" 
      if g_TIPDEN="T" and i_Rows<>0
        * --- Read from IVA_PERI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IVA_PERI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VPIMPO12,VPIMPO13,VPPERIOD,VPCODVAL"+;
            " from "+i_cTable+" IVA_PERI where ";
                +"VP__ANNO = "+cp_ToStrODBC(this.w_ANNO);
                +" and VPPERIOD = "+cp_ToStrODBC(12);
                +" and VPKEYATT = "+cp_ToStrODBC(this.w_KEYATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VPIMPO12,VPIMPO13,VPPERIOD,VPCODVAL;
            from (i_cTable) where;
                VP__ANNO = this.w_ANNO;
                and VPPERIOD = 12;
                and VPKEYATT = this.w_KEYATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LIMPO12 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
          this.w_LIMPO13 = NVL(cp_ToDate(_read_.VPIMPO13),cp_NullValue(_read_.VPIMPO13))
          this.w_LNUMPER = NVL(cp_ToDate(_read_.VPPERIOD),cp_NullValue(_read_.VPPERIOD))
          this.w_LIMPEUR = NVL(cp_ToDate(_read_.VPCODVAL),cp_NullValue(_read_.VPCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          * --- Se esiste la liquidazione anno precedente del periodo 12 significa 
          *     che nell'anno precedente avevo una periodicit� mensile
          this.w_CHANGEPER = .T.
          this.w_IMPO12 = this.w_LIMPO12
          this.w_IMPO13 = this.w_LIMPO13
          this.w_APPO = this.w_LNUMPER
          this.w_IMPEUR = this.w_LIMPEUR
        endif
      else
        if i_Rows =0
          * --- Non ho trovato l'ultimo periodo dell'anno precedente
          *     verifico cambio periodicit�
          this.w_CHANGEPER = .T.
        endif
      endif
      if this.w_CHANGEPER
        * --- Metodo Storico Con cambiamento di tipo di Mensilit�
        *     � nell�ipotesi di passaggio da liquidazioni trimestrali a liquidazioni mensili, la base di calcolo dell�acconto in applicazione del metodo storico,
        *     � pari ad un terzo dell�IVA dovuta in sede di dichiarazione annuale dell�anno precedente, o ad un terzo dell�IVA relativa alla liquidazione del
        *     quarto trimestre, sempre dell�anno precedente, per i contribuenti di cui all�art. 74, comma 4 del D.P.R. 633/1972;
        *     � nell�ipotesi di passaggio da mensile a trimestrale, la base di calcolo dell�acconto in applicazione del metodo storico � pari all�IVA dovuta
        *     con riferimento alla liquidazione di ottobre, novembre e dicembre dell�anno precedente.
        this.w_OLDNUM = IIF(this.w_NUMPER=12,4,12 )
        * --- Select from GSCG_BAI
        do vq_exec with 'GSCG_BAI',this,'_Curs_GSCG_BAI','',.f.,.t.
        if used('_Curs_GSCG_BAI')
          select _Curs_GSCG_BAI
          locate for 1=1
          do while not(eof())
          this.w_APPO = this.w_NUMPER
          this.w_IMPO12 = iif(this.w_OLDNUM=12,Nvl(_Curs_GSCG_BAI.VPIMPO12,0),Nvl(_Curs_GSCG_BAI.VPIMPO12,0)/3)
          this.w_IMPO13 = iif(this.w_OLDNUM=12,Nvl(_Curs_GSCG_BAI.VPIMPO13,0),Nvl(_Curs_GSCG_BAI.VPIMPO13,0)/3)
          this.w_IMPEUR = Nvl(_Curs_GSCG_BAI.VPCODVAL ,0)
            select _Curs_GSCG_BAI
            continue
          enddo
          use
        endif
      endif
    endif
    this.w_APPO1 = this.w_APPO
    * --- Utilizzati nella Stampa
    L_ANNO = STR(YEAR(this.oParentObject.w_DATSTA), 4, 0)
    L_PEAIVA = this.oParentObject.w_PEAIVA
    L_OLDACC = this.oParentObject.w_OLDACC
    L_NEWACC = this.oParentObject.w_ACCIVA
    L_VALIVA = this.oParentObject.w_VALIVA
    L_DECTOT = this.oParentObject.w_DECTOT
    L_OLDVAL = IIF(this.w_IMPEUR="S", g_CODEUR, g_CODLIR)
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_PAGINE=0
    L_PRPARI=this.w_PRPARI
    L_INTLIG=this.w_INTLIG
    L_PREFIS=this.w_PREFIS
    L_INDAZI=this.w_INDAZI
    L_LOCAZI=this.w_LOCAZI
    L_CAPAZI=this.w_CAPAZI
    L_PROAZI=this.w_PROAZI
    L_COFAZI=this.w_COFAZI
    L_PIVAZI=this.w_PIVAZI
    L_DESMET=this.oParentObject.w_DESMET
    L_METODO=this.w_METOD
    if NOT EMPTY(this.oParentObject.w_CODBAN)
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BADESCRI,BACODABI,BACODCAB"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_CODBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BADESCRI,BACODABI,BACODCAB;
          from (i_cTable) where;
              BACODBAN = this.oParentObject.w_CODBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RDESBAN = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
        this.w_RCODABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
        this.w_RCODCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from COD_ABI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COD_ABI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2],.t.,this.COD_ABI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ABDESABI"+;
          " from "+i_cTable+" COD_ABI where ";
              +"ABCODABI = "+cp_ToStrODBC(this.w_RCODABI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ABDESABI;
          from (i_cTable) where;
              ABCODABI = this.w_RCODABI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RDESABI = NVL(cp_ToDate(_read_.ABDESABI),cp_NullValue(_read_.ABDESABI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from COD_CAB
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COD_CAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2],.t.,this.COD_CAB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FIDESFIL,FIINDIRI,FI___CAP"+;
          " from "+i_cTable+" COD_CAB where ";
              +"FICODABI = "+cp_ToStrODBC(this.w_RCODABI);
              +" and FICODCAB = "+cp_ToStrODBC(this.w_RCODCAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FIDESFIL,FIINDIRI,FI___CAP;
          from (i_cTable) where;
              FICODABI = this.w_RCODABI;
              and FICODCAB = this.w_RCODCAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RDESFIL = NVL(cp_ToDate(_read_.FIDESFIL),cp_NullValue(_read_.FIDESFIL))
        this.w_RINDIRI = NVL(cp_ToDate(_read_.FIINDIRI),cp_NullValue(_read_.FIINDIRI))
        this.w_RCAP = NVL(cp_ToDate(_read_.FI___CAP),cp_NullValue(_read_.FI___CAP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_OK = .T.
    this.w_PAGINE = 0
    if this.w_APPO<>0
      * --- Calcola Acconto (ATTENZIONE: I dati delle dichiarzioni sono espressi in KLire)
      this.oParentObject.w_OLDACC = (this.w_IMPO12 - this.w_IMPO13) * IIF(L_OLDVAL=g_CODLIR, 1000, 1)
      this.oParentObject.w_OLDACC = IIF(this.oParentObject.w_OLDACC<0, 0, this.oParentObject.w_OLDACC)
      this.w_APPO = this.oParentObject.w_OLDACC
      * --- Se il Versamento del Precedente anno e' in altra Valuta converte
      * --- Prevediamo solo LIRE o EURO
      do case
        case this.oParentObject.w_VALIVA=g_CODLIR AND this.w_IMPEUR="S"
          this.w_APPO = cp_ROUND(this.w_APPO*g_CAOEUR, 6)
        case this.oParentObject.w_VALIVA=g_CODEUR AND this.w_IMPEUR<>"S"
          this.w_APPO = cp_ROUND(this.w_APPO/g_CAOEUR, 6)
      endcase
      this.oParentObject.w_ACCIVA = cp_ROUND(this.w_APPO * this.oParentObject.w_PEAIVA / 100, 6)
      * --- Arrotonda al centesimo di EURO o alle 1000 Lire
      this.oParentObject.w_ACCIVA = IIF(g_PERVAL=g_CODLIR, INTROUND(this.oParentObject.w_ACCIVA, 1000), cp_ROUND(this.oParentObject.w_ACCIVA,2))
      if this.oParentObject.w_METODO="S"
        do case
          case this.oParentObject.w_ACCIVA=0
            ah_ErrorMsg("Nessun importo versato nell'ultimo periodo dell'anno precedente",,"")
            this.w_OK = .F.
          case this.oParentObject.w_ACCIVA<this.oParentObject.w_MACIVA
            ah_ErrorMsg("Acconto da versare (%1) inferiore al minimo da versare",,"",ALLTRIM(STR(this.oParentObject.w_ACCIVA,18,2)))
            this.oParentObject.w_ACCIVA = 0
            this.w_OK = .F.
        endcase
      endif
    else
      if this.oParentObject.w_METODO="S"
        ah_ErrorMsg("L'acconto non � calcolabile perch� non � presente%0l'importo nell'ultimo periodo dell'anno precedente.",,"")
        this.w_OK = .F.
      endif
    endif
    if this.pTipo<>"C" AND this.w_OK
      this.w_ACCSTO = this.oParentObject.w_ACCIVA
      this.w_ANNO = STR(YEAR(this.oParentObject.w_DATSTA), 4, 0)
      if this.oParentObject.w_METODO="O"
        * --- Lancio il Batch della Liquidazione periodica per calcolo acconto Iva con metodo Operazioni Effettuate
        this.w_F2DEFI = this.w_FLDEFI
        this.w_VPIMPO7A = 0
        this.w_VPIMPO7B = 0
        this.w_VPIMPO7C = 0
        this.w_VIMPOR8 = 0
        this.w_VIMPOR9 = 0
        this.w_VIMPO10 = 0
        this.w_VIMPO11 = 0
        this.w_VIMPO12 = 0
        this.w_VIMPO13 = 0
        this.w_VIMPO15 = 0
        this.w_VIMPO16 = 0
        this.w_ACCONTO = 0
        this.w_STOPELA = .T.
        do GSCG_BDI with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_STOPELA
          * --- Risetto la var. OK a False: se nella maschera premo ok viene risettata a .T.
          this.w_OK = .F.
          * --- Leggo eventuale Credito/debito periodo precedente...
          this.w_ANNO1 = STR(YEAR(this.oParentObject.w_DATSTA), 4, 0)
          this.w_NUMPER1 = IIF(g_TIPDEN="M", 11, 3)
          * --- Read from IVA_PERI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.IVA_PERI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VPIMPO12,VPCRERIM,VPCREUTI"+;
              " from "+i_cTable+" IVA_PERI where ";
                  +"VP__ANNO = "+cp_ToStrODBC(this.w_ANNO1);
                  +" and VPPERIOD = "+cp_ToStrODBC(this.w_NUMPER1);
                  +" and VPKEYATT = "+cp_ToStrODBC(this.w_KEYATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VPIMPO12,VPCRERIM,VPCREUTI;
              from (i_cTable) where;
                  VP__ANNO = this.w_ANNO1;
                  and VPPERIOD = this.w_NUMPER1;
                  and VPKEYATT = this.w_KEYATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VIMPO10 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
            this.w_PRCRERIM = NVL(cp_ToDate(_read_.VPCRERIM),cp_NullValue(_read_.VPCRERIM))
            this.w_PRCREUTI = NVL(cp_ToDate(_read_.VPCREUTI),cp_NullValue(_read_.VPCREUTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from DAT_IVAN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IAMINIVA,IAMINIVE"+;
              " from "+i_cTable+" DAT_IVAN where ";
                  +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                  +" and IA__ANNO = "+cp_ToStrODBC(this.w_ANNO1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IAMINIVA,IAMINIVE;
              from (i_cTable) where;
                  IACODAZI = this.w_CODAZI;
                  and IA__ANNO = this.w_ANNO1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MINIVA = NVL(cp_ToDate(_read_.IAMINIVA),cp_NullValue(_read_.IAMINIVA))
            this.w_MINIVE = NVL(cp_ToDate(_read_.IAMINIVE),cp_NullValue(_read_.IAMINIVE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MINVER = IIF(g_PERVAL=g_CODLIR, this.w_MINIVA, this.w_MINIVE)
          if this.w_VIMPO10>0
            * --- Precedente dichiarazione a Debito (VP21 importo positivo) ; se Minore (o uguale) del versamento Minimo riporta il debito
            this.w_VIMPO10 = IIF(this.w_VIMPO10 <= this.w_MINVER, this.w_VIMPO10 , 0)
          else
            * --- Altrimenti se Precedente dichiarazione IVA a Credito (VP17 importo negativo) ; riporta il credito diminuito dei Rimborsi
            if this.w_VIMPO10<0
              * --- Attenzione, per i crediti, PRIMPO12 e' di segno negativo mentre CRERIM e CREUTI e' sempre in valore assoluto
              this.w_VIMPO10 = this.w_VIMPO10 + (this.w_PRCRERIM + this.w_PRCREUTI)
            endif
          endif
          * --- Lancio maschera di Riepilogo IVA per Acconto
          do GSCG_SA2 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Cancello la tabella temporanea che contiene i dati per il calcolo dell'Acconto
          * --- Drop temporary table PRI_TEMP
          i_nIdx=cp_GetTableDefIdx('PRI_TEMP')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('PRI_TEMP')
          endif
          * --- Chiudo il cursore LIQIVA creato nel GSCG_BDI
          if USED("LIQIVA") 
            SELECT LIQIVA
            USE
          endif
          if this.w_STORICO="S" AND this.w_OK
            this.w_OKELAB = "T"
            if this.w_APPO1<>0
              do case
                case this.oParentObject.w_ACCIVA=0
                  ah_ErrorMsg("Nessun importo versato nell'ultimo periodo dell'anno precedente",,"")
                  i_retcode = 'stop'
                  return
                case this.oParentObject.w_ACCIVA<this.oParentObject.w_MACIVA
                  ah_ErrorMsg("Acconto da versare (%1) inferiore al minimo da versare",,"",ALLTRIM(STR(this.oParentObject.w_ACCIVA,18,2)))
                  this.oParentObject.w_ACCIVA = 0
                  i_retcode = 'stop'
                  return
              endcase
            else
              ah_ErrorMsg("L'acconto non � calcolabile perch� non � presente%0l'importo nell'ultimo periodo dell'anno precedente.",,"")
              i_retcode = 'stop'
              return
            endif
            L_ABI=this.w_RCODABI
            L_CAB=this.w_RCODCAB
            L_DESBAN=this.w_RDESBAN
            L_DESABI=this.w_RDESABI
            L_INDIRI=this.w_RINDIRI
            L_CAP=this.w_RCAP
            L_DESFIL=this.w_RDESFIL
            * --- Utilizzati nella Stampa
            L_ANNO = STR(YEAR(this.oParentObject.w_DATSTA), 4, 0)
            L_PEAIVA = this.oParentObject.w_PEAIVA
            L_OLDACC = this.oParentObject.w_OLDACC
            L_NEWACC = this.oParentObject.w_ACCIVA
            L_VALIVA = this.oParentObject.w_VALIVA
            L_DECTOT = this.oParentObject.w_DECTOT
            L_OLDVAL = IIF(this.w_IMPEUR="S", g_CODEUR, g_CODLIR)
            L_METODO="S"
            * --- Lancio la query che calcola imposta dovuta nell'ultimo periodo dell'anno precedente:
            CREATE CURSOR __TMP__ (CODICE C(5))
            INSERT INTO __TMP__ (CODICE) VALUES ("xxxxx")
            L_OLDDEC = this.oParentObject.w_DECTOT
            * --- Legge Decimali Valuta Dich.Precedente (se diversa)
            if L_OLDVAL<>L_VALIVA
              this.w_APPO = L_OLDVAL
              * --- Read from VALUTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "VADECTOT"+;
                  " from "+i_cTable+" VALUTE where ";
                      +"VACODVAL = "+cp_ToStrODBC(this.w_APPO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  VADECTOT;
                  from (i_cTable) where;
                      VACODVAL = this.w_APPO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                L_OLDDEC = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Lancio il report di stampa
            SELECT __TMP__
            Cp_ChPrn("QUERY\GSCG_SAI.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.w_PRPARI
            if USED("__TMP__")
              SELECT __TMP__
              USE
            endif
          endif
          if this.w_OKELAB="T"
            * --- Aggiorno i Parametri IVA del Versamento
            this.w_PAGINE = IIF(this.w_PAGINE=0,this.w_PRPARI,this.w_PAGINE)
            if this.w_ACCONTO>0
              if ah_YesNo("Aggiorno i parametri IVA relativi al versamento acconto?")
                * --- Write into DAT_IVAN
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DAT_IVAN_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IAACCIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ACCONTO),'DAT_IVAN','IAACCIVA');
                  +",IADATVER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATVER),'DAT_IVAN','IADATVER');
                  +",IACODBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODBAN),'DAT_IVAN','IACODBAN');
                  +",IADESBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESBAN),'DAT_IVAN','IADESBAN');
                      +i_ccchkf ;
                  +" where ";
                      +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                      +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNRIF);
                         )
                else
                  update (i_cTable) set;
                      IAACCIVA = this.w_ACCONTO;
                      ,IADATVER = this.oParentObject.w_DATVER;
                      ,IACODBAN = this.oParentObject.w_CODBAN;
                      ,IADESBAN = this.oParentObject.w_DESBAN;
                      &i_ccchkf. ;
                   where;
                      IACODAZI = this.w_CODAZI;
                      and IA__ANNO = this.oParentObject.w_ANNRIF;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
          * --- Aggiornamento ultima pagina nel registro principale solo se attiva intestazione
          if this.w_INTLIG="S"
            if ah_YesNo("Aggiorno il progressivo di pagina del registro corrispondente?")
              this.oParentObject.w_PRPARI2 = this.w_PAGINE
              * --- Write into ATTIDETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ATTIDETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI2),'ATTIDETT','ATPRPARI');
                    +i_ccchkf ;
                +" where ";
                    +"ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                    +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
                    +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
                       )
              else
                update (i_cTable) set;
                    ATPRPARI = this.oParentObject.w_PRPARI2;
                    &i_ccchkf. ;
                 where;
                    ATCODATT = this.w_CODATT;
                    and ATNUMREG = this.oParentObject.w_NUMREG;
                    and ATTIPREG = this.oParentObject.w_TIPREG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      else
        if this.oParentObject.w_METODO="A"
          * --- Se utilizzato altro metodo aggiorno i parametri con il valore impostato sulla maschera.
          this.oParentObject.w_ACCIVA = this.w_ACCIVA2
        endif
        L_ABI=this.w_RCODABI
        L_CAB=this.w_RCODCAB
        L_DESBAN=this.w_RDESBAN
        L_DESABI=this.w_RDESABI
        L_INDIRI=this.w_RINDIRI
        L_CAP=this.w_RCAP
        L_DESFIL=this.w_RDESFIL
        * --- Utilizzati nella Stampa
        L_ANNO = STR(YEAR(this.oParentObject.w_DATSTA), 4, 0)
        L_PEAIVA = this.oParentObject.w_PEAIVA
        L_OLDACC = this.oParentObject.w_OLDACC
        L_NEWACC = this.oParentObject.w_ACCIVA
        L_VALIVA = this.oParentObject.w_VALIVA
        L_DECTOT = this.oParentObject.w_DECTOT
        L_OLDVAL = IIF(this.w_IMPEUR="S", g_CODEUR,IIF(EMPTY(this.w_IMPEUR),"",g_CODLIR))
        * --- Lancio la query che calcola imposta dovuta nell'ultimo periodo dell'anno precedente:
        CREATE CURSOR __TMP__ (CODICE C(5))
        INSERT INTO __TMP__ (CODICE) VALUES ("xxxxx")
        L_OLDDEC = this.oParentObject.w_DECTOT 
 L_NOSPLIT=this.w_NOSPLIT
        * --- Legge Decimali Valuta Dich.Precedente (se diversa)
        if L_OLDVAL<>L_VALIVA
          this.w_APPO = L_OLDVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_APPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_APPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            L_OLDDEC = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Lancio il report di stampa
        SELECT __TMP__
        Cp_ChPrn("QUERY\GSCG_SAI.FRX", " ", this)
        this.w_PAGINE = L_PAGINE+this.w_PRPARI
        if USED("__TMP__")
          SELECT __TMP__
          USE
        endif
        * --- Aggiorno i Parametri IVA del Versamento
        this.w_PAGINE = IIF(this.w_PAGINE=0,this.w_PRPARI,this.w_PAGINE)
        if ah_YesNo("Aggiorno i parametri IVA relativi al versamento acconto?")
          * --- Write into DAT_IVAN
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DAT_IVAN_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IAACCIVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ACCIVA),'DAT_IVAN','IAACCIVA');
            +",IADATVER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATVER),'DAT_IVAN','IADATVER');
            +",IACODBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODBAN),'DAT_IVAN','IACODBAN');
            +",IADESBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESBAN),'DAT_IVAN','IADESBAN');
                +i_ccchkf ;
            +" where ";
                +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNRIF);
                   )
          else
            update (i_cTable) set;
                IAACCIVA = this.oParentObject.w_ACCIVA;
                ,IADATVER = this.oParentObject.w_DATVER;
                ,IACODBAN = this.oParentObject.w_CODBAN;
                ,IADESBAN = this.oParentObject.w_DESBAN;
                &i_ccchkf. ;
             where;
                IACODAZI = this.w_CODAZI;
                and IA__ANNO = this.oParentObject.w_ANNRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Aggiornamento ultima pagina nel registro principale solo se attiva intestazione
        if this.w_INTLIG="S"
          if ah_YesNo("Aggiorno il progressivo di pagina del registro corrispondente?")
            this.oParentObject.w_PRPARI2 = this.w_PAGINE
            * --- Write into ATTIDETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI2),'ATTIDETT','ATPRPARI');
                  +i_ccchkf ;
              +" where ";
                  +"ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                  +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
                  +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
                     )
            else
              update (i_cTable) set;
                  ATPRPARI = this.oParentObject.w_PRPARI2;
                  &i_ccchkf. ;
               where;
                  ATCODATT = this.w_CODATT;
                  and ATNUMREG = this.oParentObject.w_NUMREG;
                  and ATTIPREG = this.oParentObject.w_TIPREG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='IVA_PERI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='DAT_IVAN'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ATTIDETT'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='COD_ABI'
    this.cWorkTables[8]='COD_CAB'
    this.cWorkTables[9]='DAT_IVAN'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_GSCG_BAI')
      use in _Curs_GSCG_BAI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
