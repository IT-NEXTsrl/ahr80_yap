* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkm                                                        *
*              Controlli magazzino                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2012-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkm",oParentObject,m.pEXEC)
return(i_retval)

define class tgsar_bkm as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OK = .f.
  w_APPO = space(5)
  w_MESS = space(10)
  w_CODMAG = space(5)
  w_CODUBI = space(20)
  w_DESCRI = space(30)
  w_TEST = .f.
  w_FORMAT = space(20)
  w_READAZI = space(5)
  * --- WorkFile variables
  MAGAZZIN_idx=0
  SALDIART_idx=0
  UBICAZIO_idx=0
  AZIENDA_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se il Magazzino ha dei Riferimenti (da GSAR_AMA)
    * --- Parametro
    *     A: evento Update end
    *     B: evento Record Updated
    *     C: Delete start
    *     D: w_MGFLUBIC Changed
    do case
      case this.pEXEC="A"
        this.w_OK = .T.
        this.w_MESS = "Impossibile raggruppare il magazzino"
        if NOT EMPTY(NVL(this.oParentObject.w_MGMAGRAG," "))
          * --- Se il Magazzino e' Raggruppato, verifica che a sua volta non lo sia
          this.w_APPO = ""
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGMAGRAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGMAGRAG = "+cp_ToStrODBC(this.oParentObject.w_MGCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGMAGRAG;
              from (i_cTable) where;
                  MGMAGRAG = this.oParentObject.w_MGCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0 AND this.w_APPO=this.oParentObject.w_MGCODMAG
            this.w_OK = .F.
          endif
        endif
        if this.w_OK=.T. AND NOT EMPTY(this.oParentObject.w_MGCODMAG) AND this.oParentObject.w_ODISMAG<>this.oParentObject.w_MGDISMAG AND g_PROD="S" AND this.oParentObject.cFunction="Edit"
          * --- Verifica Esistenza ODP/ODL Collegati al Magazzino (se si il Flag Nettifica non puo' essere modificato)
          ah_Msg("Verifica esistenza ODP/ODL collegati al magazzino...",.T.)
          vq_exec("..\COLA\EXE\QUERY\GSCO_BKM.VQR",this,"VERIFICA")
          if USED("VERIFICA")
            this.w_MESS = "Esistono ODL/ODP associati al magazzino; impossibile modificare il check nettificabile"
            SELECT VERIFICA
            GO TOP
            this.w_OK = IIF(NVL(TOTALE,0)=0, .T., .F.)
            USE
          endif
          WAIT CLEAR
        endif
        if this.w_OK=.F.
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_MsgFormat(this.w_MESS)
        endif
      case this.pEXEC="B"
        * --- Aggiornamento Documenti \Movimenti nel caso di cambiamento check ubicazioni
        if this.oParentObject.w_MGFLUBIC="S" AND g_PERUBI="S" AND (this.oParentObject.w_OLDFLUB<>this.oParentObject.w_MGFLUBIC)
          this.w_TEST = .F.
          this.w_CODMAG = this.oParentObject.w_MGCODMAG
          this.w_READAZI = i_CODAZI
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZMASUBI"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(this.w_READAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZMASUBI;
              from (i_cTable) where;
                  AZCODAZI = this.w_READAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FORMAT = NVL(cp_ToDate(_read_.AZMASUBI),cp_NullValue(_read_.AZMASUBI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(this.w_FORMAT)
            this.w_CODUBI = this.oParentObject.w_MGCODMAG
          endif
          this.w_DESCRI = this.oParentObject.w_MGDESMAG
          do GSMD_KUB with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_TEST=.F.
            * --- Ripristino valore originario
            this.oParentObject.w_MGFLUBIC = this.oParentObject.w_OLDFLUB
            * --- Write into MAGAZZIN
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MGFLUBIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDFLUB),'MAGAZZIN','MGFLUBIC');
                  +i_ccchkf ;
              +" where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MGCODMAG);
                     )
            else
              update (i_cTable) set;
                  MGFLUBIC = this.oParentObject.w_OLDFLUB;
                  &i_ccchkf. ;
               where;
                  MGCODMAG = this.oParentObject.w_MGCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      case this.pEXEC="D"
        if this.oParentObject.w_MGFLUBIC<>"S"
          * --- Select from gsarubkm
          do vq_exec with 'gsarubkm',this,'_Curs_gsarubkm','',.f.,.t.
          if used('_Curs_gsarubkm')
            select _Curs_gsarubkm
            locate for 1=1
            do while not(eof())
            if Nvl( _Curs_gsarubkm.TEST, 0 )>0
              Ah_ErrorMsg("Impossibile disattivare: sono presenti movimenti sulle ubicazioni del magazzino.")
              this.oParentObject.w_MGFLUBIC = this.oParentObject.w_OLDFLUB
              exit
            endif
              select _Curs_gsarubkm
              continue
            enddo
            use
          endif
          if this.oParentObject.w_MGFLUBIC <> this.oParentObject.w_OLDFLUB
            * --- Select from UBICAZIO
            i_nConn=i_TableProp[this.UBICAZIO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UBICAZIO_idx,2],.t.,this.UBICAZIO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select UBCODICE,UBCODMAG  from "+i_cTable+" UBICAZIO ";
                  +" where UBCODMAG="+cp_ToStrODBC(this.oParentObject.w_MGCODMAG)+"";
                   ,"_Curs_UBICAZIO")
            else
              select UBCODICE,UBCODMAG from (i_cTable);
               where UBCODMAG=this.oParentObject.w_MGCODMAG;
                into cursor _Curs_UBICAZIO
            endif
            if used('_Curs_UBICAZIO')
              select _Curs_UBICAZIO
              locate for 1=1
              do while not(eof())
              if !Ah_Yesno("Attenzione: esistono ubicazioni collegate al magazzino. %0Si desidera disattivare ugualmente?")
                this.oParentObject.w_MGFLUBIC = this.oParentObject.w_OLDFLUB
              endif
              exit
                select _Curs_UBICAZIO
                continue
              enddo
              use
            endif
          endif
        endif
      otherwise
        this.w_MESS = "Magazzino definito come preferenziale su alcuni articoli%0Impossibile eliminare."
        * --- Select from gsar_bkm
        do vq_exec with 'gsar_bkm',this,'_Curs_gsar_bkm','',.f.,.t.
        if used('_Curs_gsar_bkm')
          select _Curs_gsar_bkm
          locate for 1=1
          do while not(eof())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_MsgFormat(this.w_MESS)
            select _Curs_gsar_bkm
            continue
          enddo
          use
        endif
        if g_MADV="S"
          this.w_MESS = "Magazzino usato nell' anagrafica saldi/lotti%0Impossibile eliminare."
          * --- Select from gsar1bks
          do vq_exec with 'gsar1bks',this,'_Curs_gsar1bks','',.f.,.t.
          if used('_Curs_gsar1bks')
            select _Curs_gsar1bks
            locate for 1=1
            do while not(eof())
            if CONTA>0
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=ah_MsgFormat(this.w_MESS)
              i_retcode = 'stop'
              return
            endif
              select _Curs_gsar1bks
              continue
            enddo
            use
          endif
        endif
        * --- Select from MAGAZZIN
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT (*) as numero  from "+i_cTable+" MAGAZZIN ";
              +" where MGMAGRAG="+cp_ToStrODBC(this.oParentObject.w_MGCODMAG)+"";
               ,"_Curs_MAGAZZIN")
        else
          select COUNT (*) as numero from (i_cTable);
           where MGMAGRAG=this.oParentObject.w_MGCODMAG;
            into cursor _Curs_MAGAZZIN
        endif
        if used('_Curs_MAGAZZIN')
          select _Curs_MAGAZZIN
          locate for 1=1
          do while not(eof())
          if numero>0
            this.w_MESS = "Il magazzino � usato come magazzino di raggruppamento per altri magazzini.%0Impossibile eliminare."
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
            select _Curs_MAGAZZIN
            continue
          enddo
          use
        endif
        * --- Select from TIP_DOCU
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT (*) as numero  from "+i_cTable+" TIP_DOCU ";
              +" where TDCODMAG="+cp_ToStrODBC(this.oParentObject.w_MGCODMAG)+" OR TDCODMAT="+cp_ToStrODBC(this.oParentObject.w_MGCODMAG)+"";
               ,"_Curs_TIP_DOCU")
        else
          select COUNT (*) as numero from (i_cTable);
           where TDCODMAG=this.oParentObject.w_MGCODMAG OR TDCODMAT=this.oParentObject.w_MGCODMAG;
            into cursor _Curs_TIP_DOCU
        endif
        if used('_Curs_TIP_DOCU')
          select _Curs_TIP_DOCU
          locate for 1=1
          do while not(eof())
          if numero>0
            this.w_MESS = "Il magazzino � usato come magazzino di una causale documento.%0Impossibile eliminare."
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat(this.w_MESS)
          endif
          i_retcode = 'stop'
          return
            select _Curs_TIP_DOCU
            continue
          enddo
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='UBICAZIO'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='TIP_DOCU'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gsarubkm')
      use in _Curs_gsarubkm
    endif
    if used('_Curs_UBICAZIO')
      use in _Curs_UBICAZIO
    endif
    if used('_Curs_gsar_bkm')
      use in _Curs_gsar_bkm
    endif
    if used('_Curs_gsar1bks')
      use in _Curs_gsar1bks
    endif
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
