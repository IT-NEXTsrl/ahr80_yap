* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bup                                                        *
*              Accorpamento partite                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-10                                                      *
* Last revis.: 2000-06-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bup",oParentObject)
return(i_retval)

define class tgste_bup as StdBatch
  * --- Local variables
  w_OK = .f.
  w_ZOOM = space(10)
  w_TTIP = space(1)
  w_TCON = space(15)
  w_TVAL = space(3)
  w_NPAR = space(31)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Partite da Accorpare (da GSTE_KUP)
    this.w_ZOOM = this.oParentObject.oParentObject.w_ZoomScad
    if EMPTY(this.oParentObject.w_NEWPAR)
      ah_ErrorMsg("Indicare la scadenza da assegnare",,"")
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    NC = this.w_ZOOM.cCursor
    * --- Seleziona i Record col Check raggruppati per Numero Partita
    SELECT NUMPAR, MAX(DATSCA) AS DATSCA, TIPCON, CODCON, CODVAL FROM &NC ;
    WHERE XCHK=1 AND NOT EMPTY(NVL(NUMPAR,"")) AND NOT EMPTY(NVL(CODCON,"")) AND NOT EMPTY(NVL(CODVAL,"")) ;
    INTO CURSOR SCASEL NOFILTER ;
    GROUP BY NUMPAR, TIPCON, CODCON, CODVAL
    * --- Se Controlli OK
    this.w_OK = .F.
    * --- Try
    local bErr_041B0E98
    bErr_041B0E98=bTrsErr
    this.Try_041B0E98()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore durante l'aggiornamento; operazione abbandonata",,"")
    endif
    bTrsErr=bTrsErr or bErr_041B0E98
    * --- End
    if this.w_OK=.T.
      ah_ErrorMsg("Assegnazione partite completata",,"")
    else
      ah_ErrorMsg("Non ci sono partite da assegnare",,"")
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_041B0E98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT SCASEL
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR NOT EMPTY(NUMPAR)
    this.w_NPAR = NUMPAR
    this.w_TTIP = NVL(TIPCON," ")
    this.w_TCON = CODCON
    this.w_TVAL = CODVAL
    ah_Msg("Aggiornamento partita: %1",.T.,.F.,.F., this.w_NPAR)
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWPAR),'PAR_TITE','PTNUMPAR');
          +i_ccchkf ;
      +" where ";
          +"PTNUMPAR = "+cp_ToStrODBC(this.w_NPAR);
          +" and PTTIPCON = "+cp_ToStrODBC(this.w_TTIP);
          +" and PTCODCON = "+cp_ToStrODBC(this.w_TCON);
          +" and PTCODVAL = "+cp_ToStrODBC(this.w_TVAL);
             )
    else
      update (i_cTable) set;
          PTNUMPAR = this.oParentObject.w_NEWPAR;
          &i_ccchkf. ;
       where;
          PTNUMPAR = this.w_NPAR;
          and PTTIPCON = this.w_TTIP;
          and PTCODCON = this.w_TCON;
          and PTCODVAL = this.w_TVAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_OK = .T.
    SELECT SCASEL
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiudo la maschera che lancia il batch
    if USED("SCASEL")
      SELECT SCASEL
      USE
    endif
    * --- Se chiudo la maschera prima di rieseguire la Query mi da errore
    * --- Rieseguo la Query
     this.oParentObject.oParentObject.NotifyEvent("Riesegui")
    * --- chiudo la maschera di Riassegnazione
     this.oParentObject.ecpQuit()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
