* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bdx                                                        *
*              Driver per foglio Excel                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_86]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-13                                                      *
* Last revis.: 2017-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bdx",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bdx as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  nConn = 0
  XlsDir = space(254)
  ErrIns = .f.
  w_StrdaIns = space(0)
  W_numrip = 0
  err = 0
  w_CURNAME = space(8)
  w_MESS = space(250)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per foglio elettronico.
    *     ========================================================
    *     Utilizzare questo batch come driver per caricare dati da foglio elettronico
    *     tramite l'INSERIMENTO DATI (GSAR_BPG=>GSAR_MPG).
    *     
    *     Per utilizzare il driver � neccessario avere installato il driver Odbc Microsoft Excel Driver (*.xls)
    *     E' possibile verificare l'installazione dello stesso da Pannello di controllo --> Strumenti di amministrazione --> Origine Dati (ODBC)
    * --- PREPARAZIONE DEL FOGLIO EXCEL
    *     
    *     Perch� il driver possa funzionare la struttura del foglio Excel deve essere piuttosto rigida.
    *     Per problemi di gestione degli handle dei processi, si � scelto di utilizzare come sorgente dati
    *     del foglio, solamente la pagina nominata "Foglio1". 
    *     
    *     Nei campi che devono contenere STRINGHE, prima di riempirli con i dati, selezionare come "Formato 
    *     Celle", la categoria "Testo"
    *     
    *     Se all'interno del foglio excel, nessuna pagina � nominata in questo modo,
    *     la procedura restituir� errore.
    *     La pagina cos� definita deve contenere sulla riga 1, i nomi dei campi che si vuole partecipino 
    *     all'estrazione dati.
    *     Dalla riga 2 in poi, � possibile definire i valori dei campi, senza che siano lasciate righe vuote.
    * --- La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL,SERRIF C(10) NULL )
    *     
    *     All'interno del batch vi � una chiamata per aprire una maschera di selezione 
    *     per caricare un folgio lettronico (*xls) se questo non � stato precedentemente settato
    *     Se il secondo parametro � gi� stato valorizzato questa non apparir�.
    *     Il secondo parametro rappresenta appunto il foglio elettronico (con path relativo o assoluto)
    *     dal quale la procedura prelever� i dati. (Da specificare, se fisso, nell'anagrafica
    *     dispositivi nella casella parametro)
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    *     In questo driver  non sono utilizzati, MA E' NECESSARIO CHE SIANO PRESENTI PER EVITARE SPIACEVOLI MALFUNZIONAMENTI!
    *     
    * --- Conterr� il valore della connessione ODBC
    * --- Conterr� il path assoluto della directrory in cui risiede il file *.xls
    * --- Messaggistica ed errori
    this.W_numrip = 60
    * --- E' necessario inizializzare le variabili all'interno del foglio elettronico per evitare
    *     che la insert nel cursore pCursore dia errore.
     
 L_CODICE=" " 
 L_UNIMIS=" " 
 L_QTAMOV=0 
 L_PREZZO=0 
 L_LOTTO=" " 
 L_CODMAG=" " 
 L_UBICAZIONE=" " 
 L_MATRICOLA=" " 
 L_SERRIF=" "
    * --- Inizio Elaborazione...
    * --- Se pTXTFile non � valorizzata chiedo un foglio elettronico
    if EMPTY (this.pTXTFile)
      this.pTXTFile = getfile("xls",ah_Msgformat("Nome file:"),ah_Msgformat("Apri"),0,ah_Msgformat("Apri"))
      if empty(this.pTXTFile)
        i_retcode = 'stop'
        return
      endif
      if LEN( ALLTRIM( this.pTXTFile ) ) > 76
        ah_ErrorMsg("Il percorso selezionato � troppo lungo. Occorre spostare il file",,"", this.pTXTFile)
        i_retcode = 'stop'
        return
      endif
    else
      if !FILE(this.pTXTFile)
        ah_ErrorMsg("Il file %1 non esiste",,"", this.pTXTFile)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Recupero il path della directory che contiene il foglio elettronico.
    *     Questa informazione � un parametro necessario per stabilire la connessione ODBC
    this.XlsDir = SUBSTR(this.pTXTFile,1,RAT(CHR(92),this.pTXTFile)-1)
    AddMsgNL("File selezionato: %1",this.oparentobject, ALLTRIM(SUBSTR(this.pTXTFile,RAT(CHR(92),this.pTXTFile))) )
    AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("Fase 1: connessione alla pagina del foglio di calcolo...%0%1",this.oparentobject, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    * --- Creo la Connessione al Foglio Excel...
    if TYPE ("g_EXCELSTRINGCONN")="C" AND not empty (g_EXCELSTRINGCONN)
      this.nConn = SQLSTRINGCONNECT( STRTRAN ( STRTRAN(g_EXCELSTRINGCONN,"<NOMEFILE>", this.pTXTFile ), "<NOMEDIR>", this.XlsDir ) )
    else
      this.nConn = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.pTXTFile+";DefaultDir="+this.XlsDir+";DriverId=790;FIL=excel 8.0;MaxBufferSize=2048;PageTimeout=5;")
    endif
    * --- Eseguo la query...
    *     Il riferimento alla pagina del foglio Excel avviene SEMPRE e SOLO per NOME...
    *     A causa di problemi con le gestione degli handle dei processi aperti, la 
    *     lettura dei dati dal foglio Excel NECESSITA chela pagina sorgente sia nominata
    *     come "Foglio1"
    *     Cambiando il nome alla pagina di riferimento, la connessione restituir� un errore.
    this.w_CURNAME = SYS(2015)
    this.err = SQLEXEC(this.nConn, "select * from [Foglio1$]" , this.w_CURNAME )
    if this.err = -1
      ah_ErrorMsg("%1",,"", MESSAGE() )
      i_retcode = 'stop'
      return
    endif
    * --- Chiusura della connessione al foglio Excel
    SQLCANCEL(this.nConn)
    SQLDISCONNECT(this.nConn)
    * --- I tipi restituiti da una query su foglio Excel, tranne che per i numerici  e 
    *     le date, sono tutti Memo!
    *     Per evitare problemi di inserzione, memorizziamo ogni campo in una variabile
    *     e inseriamo nel cursore pCursore queste variabiali.
    AddMsgNL("Fase 2: verifica file...%0%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    if reccount ( this.w_CURNAME ) > 0
      AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
      AddMsgNL("Fase 3: inserimento informazioni...%0%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
       
 select ( this.w_CURNAME ) 
 go top 
 scan
      if type("CODICE")<>"U" and type("CODICE")$"MC"
        L_CODICE=left(Nvl(CODICE," "),20)
        if type("L_CODICE")="N"
          L_CODICE=alltrim(str(L_CODICE))
        endif
      endif
      if type("UNIMIS")<>"U" and type("UNIMIS")$"MC"
        L_UNIMIS=left(Nvl(UNIMIS," "),3)
      endif
      if type("QTAMOV")<>"U" and type("QTAMOV") = "N"
        L_QTAMOV=Nvl(QTAMOV,0)
      endif
      if type("PREZZO")<>"U" and type("PREZZO")="N"
        L_PREZZO=Nvl(PREZZO,0)
      endif
      if type("LOTTO")<>"U" and type("LOTTO")$"MC"
        L_LOTTO=left(Nvl(LOTTO," "),20)
      endif
      if type("CODMAG")<>"U" and type("CODMAG")$"MC"
        L_CODMAG=left(Nvl(CODMAG," "),5)
      endif
      if type("UBICAZIONE")<>"U" and type("UBICAZIONE")$"MC"
        L_UBICAZIONE=left(Nvl(UBICAZIONE," "),20)
      endif
      if type("MATRICOLA")<>"U" and type("MATRICOLA")$"MC"
        L_MATRICOLA=left(Nvl(MATRICOLA," "),40)
      endif
      if type("SERRIF")<>"U" and type("SERRIF")$"MC"
        L_SERRIF=left(Nvl(SERRIF," "),10)
      endif
      if Type("pObjMsg")="O"
        this.w_MESS = Ah_MsgFormat("Inserimento record")
        if AT(this.w_MESS,this.pObjMsg.w_Msg)>0
          this.w_StrDaIns = LEFT(this.pObjMsg.w_Msg,AT(this.w_MESS,this.pObjMsg.w_Msg)-1)
          this.pObjMsg.w_MSG=""
        else
          this.w_StrDaIns = ""
        endif
      endif
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Inserimento record")
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("%0Codice ricerca: %1",L_CODICE)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("-u.mis.: %1%0",L_UNIMIS)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Quantit�: %1", str(L_QTAMOV,12,7) )
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("-prezzo: %1%0", str(L_PREZZO,20,4) )
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Lotto: %1%0",L_LOTTO)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Magazzino: %1%0", L_CODMAG)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Ubicazione: %1%0", L_UBICAZIONE)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Matricola: %1%0", L_MATRICOLA)
      this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Rif origine: %1%0", L_SERRIF)
      AddMsgNL("%1", this.oparentobject, this.w_StrDaIns)
      ON ERROR this.ErrIns=.T.
       
 INSERT INTO ( this.pCursore ) (CODICE,UNIMIS,QTAMOV,PREZZO,LOTTO,CODMAG,UBICAZIONE,MATRICOLA,SERRIF) ; 
 VALUES (L_CODICE,L_UNIMIS,L_QTAMOV,L_PREZZO,L_LOTTO,L_CODMAG,L_UBICAZIONE,L_MATRICOLA,L_SERRIF)
      if this.ErrIns
        ah_ErrorMsg("Errore inserimento dati cursore %1",,"", MESSAGE())
      endif
      endscan
      if used ( this.w_CURNAME )
         
 select ( this.w_CURNAME ) 
 use
      endif
    else
      if used ( this.w_CURNAME )
         
 select ( this.w_CURNAME ) 
 use
      endif
      AddMsgNL("***Riscontrati errori nel file: procedura interrotta ***",this.oparentobject)
      ah_ErrorMsg("Il foglio Excel selezionato non contiene dati",,"")
      i_retcode = 'stop'
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
