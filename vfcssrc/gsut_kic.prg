* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kic                                                        *
*              Installazione componenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-02                                                      *
* Last revis.: 2016-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kic
*--- Maschera utilizzabile solo se programma lanciato come amministratore
If !cp_IsAdminWin()
   If ah_YesNo(MSG_RESTART_AS_ADMIN_QP)
     AdHocRunAs()
   Endif
   return
Endif
* --- Fine Area Manuale
return(createobject("tgsut_kic",oParentObject))

* --- Class definition
define class tgsut_kic as StdForm
  Top    = 33
  Left   = 73

  * --- Standard Properties
  Width  = 389
  Height = 173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-31"
  HelpContextID=266945385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kic"
  cComment = "Installazione componenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AGENDA = space(1)
  o_AGENDA = space(1)
  w_PUBLISH = space(1)
  o_PUBLISH = space(1)
  w_BCSENDTO = space(1)
  w_BCPDF417 = space(1)
  w_LEGGIPEC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kicPag1","gsut_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAGENDA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BIC with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AGENDA=space(1)
      .w_PUBLISH=space(1)
      .w_BCSENDTO=space(1)
      .w_BCPDF417=space(1)
      .w_LEGGIPEC=space(1)
        .w_AGENDA = IIF(g_agen='S', 'S', ' ')
        .w_PUBLISH = IIF(g_IRDR='S','S', ' ')
        .w_BCSENDTO = 'S'
        .w_BCPDF417 = 'S'
        .w_LEGGIPEC = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAGENDA_1_1.enabled = this.oPgFrm.Page1.oPag.oAGENDA_1_1.mCond()
    this.oPgFrm.Page1.oPag.oPUBLISH_1_2.enabled = this.oPgFrm.Page1.oPag.oPUBLISH_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBCPDF417_1_4.visible=!this.oPgFrm.Page1.oPag.oBCPDF417_1_4.mHide()
    this.oPgFrm.Page1.oPag.oLEGGIPEC_1_7.visible=!this.oPgFrm.Page1.oPag.oLEGGIPEC_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAGENDA_1_1.RadioValue()==this.w_AGENDA)
      this.oPgFrm.Page1.oPag.oAGENDA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBLISH_1_2.RadioValue()==this.w_PUBLISH)
      this.oPgFrm.Page1.oPag.oPUBLISH_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBCSENDTO_1_3.RadioValue()==this.w_BCSENDTO)
      this.oPgFrm.Page1.oPag.oBCSENDTO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBCPDF417_1_4.RadioValue()==this.w_BCPDF417)
      this.oPgFrm.Page1.oPag.oBCPDF417_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLEGGIPEC_1_7.RadioValue()==this.w_LEGGIPEC)
      this.oPgFrm.Page1.oPag.oLEGGIPEC_1_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AGENDA = this.w_AGENDA
    this.o_PUBLISH = this.w_PUBLISH
    return

enddefine

* --- Define pages as container
define class tgsut_kicPag1 as StdContainer
  Width  = 385
  height = 173
  stdWidth  = 385
  stdheight = 173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAGENDA_1_1 as StdCheck with uid="VBYBBFWFZQ",rtseq=1,rtrep=.f.,left=13, top=13, caption="Componenti per visualizzazione agenda",;
    ToolTipText = "Se attivo: installa componenti necessari per visualizzazione agenda",;
    HelpContextID = 173451258,;
    cFormVar="w_AGENDA", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oAGENDA_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGENDA_1_1.GetRadio()
    this.Parent.oContained.w_AGENDA = this.RadioValue()
    return .t.
  endfunc

  func oAGENDA_1_1.SetRadio()
    this.Parent.oContained.w_AGENDA=trim(this.Parent.oContained.w_AGENDA)
    this.value = ;
      iif(this.Parent.oContained.w_AGENDA=='S',1,;
      0)
  endfunc

  func oAGENDA_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_agen='S')
    endwith
   endif
  endfunc

  add object oPUBLISH_1_2 as StdCheck with uid="DDDQSPQVCU",rtseq=2,rtrep=.f.,left=13, top=36, caption="Componenti per infopublisher",;
    ToolTipText = "Se attivo: installa componenti necessari per infopublisher",;
    HelpContextID = 133641974,;
    cFormVar="w_PUBLISH", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oPUBLISH_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPUBLISH_1_2.GetRadio()
    this.Parent.oContained.w_PUBLISH = this.RadioValue()
    return .t.
  endfunc

  func oPUBLISH_1_2.SetRadio()
    this.Parent.oContained.w_PUBLISH=trim(this.Parent.oContained.w_PUBLISH)
    this.value = ;
      iif(this.Parent.oContained.w_PUBLISH=='S',1,;
      0)
  endfunc

  func oPUBLISH_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IRDR='S')
    endwith
   endif
  endfunc

  add object oBCSENDTO_1_3 as StdCheck with uid="CCJNXJQMLU",rtseq=3,rtrep=.f.,left=13, top=59, caption="Componenti per invia a...",;
    ToolTipText = "Se attivo: installa componenti necessari per l' invia a...",;
    HelpContextID = 155268197,;
    cFormVar="w_BCSENDTO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oBCSENDTO_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oBCSENDTO_1_3.GetRadio()
    this.Parent.oContained.w_BCSENDTO = this.RadioValue()
    return .t.
  endfunc

  func oBCSENDTO_1_3.SetRadio()
    this.Parent.oContained.w_BCSENDTO=trim(this.Parent.oContained.w_BCSENDTO)
    this.value = ;
      iif(this.Parent.oContained.w_BCSENDTO=='S',1,;
      0)
  endfunc

  add object oBCPDF417_1_4 as StdCheck with uid="FQINJYZQDT",rtseq=4,rtrep=.f.,left=13, top=82, caption="Componenti per barcode PDF417",;
    ToolTipText = "Se attivo: installa componenti necessari per il Barcode PDF417",;
    HelpContextID = 121633715,;
    cFormVar="w_BCPDF417", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oBCPDF417_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oBCPDF417_1_4.GetRadio()
    this.Parent.oContained.w_BCPDF417 = this.RadioValue()
    return .t.
  endfunc

  func oBCPDF417_1_4.SetRadio()
    this.Parent.oContained.w_BCPDF417=trim(this.Parent.oContained.w_BCPDF417)
    this.value = ;
      iif(this.Parent.oContained.w_BCPDF417=='S',1,;
      0)
  endfunc

  func oBCPDF417_1_4.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc


  add object oBtn_1_5 as StdButton with uid="MBXAIGGBZE",left=277, top=125, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per installare i componenti";
    , HelpContextID = 266916634;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="ISDICPRYHO",left=327, top=125, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 259627962;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLEGGIPEC_1_7 as StdCheck with uid="YLPYLYHYMA",rtseq=5,rtrep=.f.,left=13, top=105, caption="Leggi PEC client",;
    ToolTipText = "Se attivo: installa componenti necessari per lettura PEC da client",;
    HelpContextID = 82999033,;
    cFormVar="w_LEGGIPEC", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oLEGGIPEC_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oLEGGIPEC_1_7.GetRadio()
    this.Parent.oContained.w_LEGGIPEC = this.RadioValue()
    return .t.
  endfunc

  func oLEGGIPEC_1_7.SetRadio()
    this.Parent.oContained.w_LEGGIPEC=trim(this.Parent.oContained.w_LEGGIPEC)
    this.value = ;
      iif(this.Parent.oContained.w_LEGGIPEC=='S',1,;
      0)
  endfunc

  func oLEGGIPEC_1_7.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
