* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bst                                                        *
*              Calcolo importo totalizzatore                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-07                                                      *
* Last revis.: 2006-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bst",oParentObject)
return(i_retval)

define class tgscg_bst as StdBatch
  * --- Local variables
  w_LPEIINT = 0
  w_LPERINT = 0
  w_LCODICE = space(10)
  w_LTIPTOT = space(1)
  w_LFORMUL = space(30)
  w_LTIPORI = space(1)
  w_L__ANNO = space(4)
  w_LMESREI = 0
  w_LMESREF = 0
  w_LGIOREI = 0
  w_LGIOREF = 0
  w_LFLPROV = space(1)
  w_LCAUCON = space(5)
  w_LMESDOI = 0
  w_LMESDOF = 0
  w_LGIODOI = 0
  w_LGIODOF = 0
  w_LALFDOC = space(10)
  w_LNUMDOI = 0
  w_LNUMDOF = 0
  w_LALFPRO = space(10)
  w_LNUMPRI = 0
  w_LNUMPRF = 0
  w_LFLPERF = space(1)
  w_LFLTIPA = space(1)
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_LCODZON = space(3)
  w_LCODMAS = space(15)
  w_LTIPREG = space(1)
  w_LNUMREI = 0
  w_LNUMREF = 0
  w_LMESCOI = 0
  w_LMESCOF = 0
  w_GLIOCOI = 0
  w_LGIOCOF = 0
  w_LPERIVA = 0
  w_LFLESAZ = space(1)
  w_LPROIVA = space(1)
  w_LTIPPLA = space(1)
  w_LPLAIVA = space(1)
  w_LMACIVA = space(1)
  w_LIMPEXP = space(1)
  w_LBOLIVA = space(1)
  w_LPERCOM = 0
  w_LTIPAGR = space(1)
  w_LREVCHA = space(1)
  w_LFLVAFF = space(1)
  w_LACQINT = space(1)
  w_LDATANA = 0
  w_LDATANP = 0
  w_LTIPBEN = space(1)
  w_LCODINT = space(5)
  w_LPLAIVA = space(1)
  w_LMACIVA = space(1)
  w_LIMPEXP = space(1)
  w_LBOLIVA = space(1)
  w_LPERCOM = 0
  w_LTIPAGR = space(1)
  w_LREVCHA = space(1)
  w_LFLVAFF = space(1)
  w_LACQINT = space(1)
  w_LDATANP = 0
  w_LTIPBEN = space(1)
  w_LCODINT = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCG_ATI totalizzatori iva esegue calcolo importo
    * --- Variabili Archivio Voci IVA
    this.w_LPROIVA = iif(this.oParentObject.w_TIPROIVA="F"," ",this.oParentObject.w_TIPROIVA)
    this.w_LTIPPLA = iif(this.oParentObject.w_TITIPPLA="F"," ",this.oParentObject.w_TITIPPLA)
    this.w_LTIPAGR = iif(this.oParentObject.w_TITIPAGR="F"," ",this.oParentObject.w_TITIPAGR)
    this.w_LFLVAFF = iif(this.oParentObject.w_TIFLVAFF="F"," ",this.oParentObject.w_TIFLVAFF)
    this.w_LACQINT = iif(iw_TIACQINT="F"," ",this.oParentObject.w_TIACQINT)
    this.w_LDATANA = IIF(this.oParentObject.w_TIDATANA=4,0,this.oParentObject.w_TIDATANA-1)
    if this.oParentObject.w_TITIPTOT="S"
      do case
        case this.oParentObject.w_TITIPORI="P"
          * --- Select from GSCG_BST
          do vq_exec with 'GSCG_BST',this,'_Curs_GSCG_BST','',.f.,.t.
          if used('_Curs_GSCG_BST')
            select _Curs_GSCG_BST
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_TIIMPTOT = Nvl(_Curs_GSCG_BST.TOTALE,0)
              select _Curs_GSCG_BST
              continue
            enddo
            use
          endif
        case this.oParentObject.w_TITIPORI="S"
        case this.oParentObject.w_TITIPORI="M"
      endcase
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSCG_BST')
      use in _Curs_GSCG_BST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
