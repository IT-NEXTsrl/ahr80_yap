* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bic                                                        *
*              Inserimento cambio giornaliero                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-24                                                      *
* Last revis.: 2011-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODVAL,w_CAOVAL,w_DATCAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bic",oParentObject,m.w_CODVAL,m.w_CAOVAL,m.w_DATCAM)
return(i_retval)

define class tgsar_bic as StdBatch
  * --- Local variables
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_DATCAM = ctod("  /  /  ")
  w_OBJ = .NULL.
  w_CGCAMBIO = 0
  * --- WorkFile variables
  CAM_BI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento automatico cambio giornaliero da documenti
    * --- Verifico i diritti di accesso sulla gestioe dei cambi giornalieri
    this.w_OBJ=createobject("CUSTOM")
    this.w_OBJ.addproperty("bSec1")     
    this.w_OBJ.addproperty("bSec2")     
    this.w_OBJ.addproperty("bSec3")     
    this.w_OBJ.addproperty("bSec4")     
    this.w_OBJ.addproperty("bFox26")     
    this.w_OBJ.addproperty("cPrg")     
    this.w_OBJ.cPrg = "gsar_mcg"
    DO cp_GetSecurity WITH this.w_OBJ, this.w_OBJ.cprg
    if this.w_OBJ.bSec1 and this.w_OBJ.bSec2
      * --- abilitato all'accesso e all'inserimento
      * --- Read from CAM_BI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_BI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2],.t.,this.CAM_BI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CGCAMBIO"+;
          " from "+i_cTable+" CAM_BI where ";
              +"CGDATCAM = "+cp_ToStrODBC(this.w_DATCAM);
              +" and CGCODVAL = "+cp_ToStrODBC(this.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CGCAMBIO;
          from (i_cTable) where;
              CGDATCAM = this.w_DATCAM;
              and CGCODVAL = this.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CGCAMBIO = NVL(cp_ToDate(_read_.CGCAMBIO),cp_NullValue(_read_.CGCAMBIO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_CGCAMBIO=0 and ah_yesno("Si desidera memorizzare il cambio giornaliero?")
        * --- Insert into CAM_BI
        i_nConn=i_TableProp[this.CAM_BI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAM_BI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CGDATCAM"+",CGCODVAL"+",CGCAMBIO"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DATCAM),'CAM_BI','CGDATCAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'CAM_BI','CGCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'CAM_BI','CGCAMBIO');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CGDATCAM',this.w_DATCAM,'CGCODVAL',this.w_CODVAL,'CGCAMBIO',this.w_CAOVAL)
          insert into (i_cTable) (CGDATCAM,CGCODVAL,CGCAMBIO &i_ccchkf. );
             values (;
               this.w_DATCAM;
               ,this.w_CODVAL;
               ,this.w_CAOVAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_CODVAL,w_CAOVAL,w_DATCAM)
    this.w_CODVAL=w_CODVAL
    this.w_CAOVAL=w_CAOVAL
    this.w_DATCAM=w_DATCAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAM_BI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODVAL,w_CAOVAL,w_DATCAM"
endproc
