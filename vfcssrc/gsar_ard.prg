* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ard                                                        *
*              Rapporti                                                        *
*                                                                              *
*      Author: ZUCCHETTI SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-22                                                      *
* Last revis.: 2009-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ard")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ard")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ard")
  return

* --- Class definition
define class tgsar_ard as StdPCForm
  Width  = 271
  Height = 143
  Top    = 10
  Left   = 20
  cComment = "Rapporti"
  cPrg = "gsar_ard"
  HelpContextID=126449513
  add object cnt as tcgsar_ard
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ard as PCContext
  w_DRCODANA = space(15)
  w_DRCODAZI = space(5)
  w_DRCODSOC = space(15)
  w_DRCODRAP = 0
  w_DRDATOBSO = space(8)
  proc Save(oFrom)
    this.w_DRCODANA = oFrom.w_DRCODANA
    this.w_DRCODAZI = oFrom.w_DRCODAZI
    this.w_DRCODSOC = oFrom.w_DRCODSOC
    this.w_DRCODRAP = oFrom.w_DRCODRAP
    this.w_DRDATOBSO = oFrom.w_DRDATOBSO
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DRCODANA = this.w_DRCODANA
    oTo.w_DRCODAZI = this.w_DRCODAZI
    oTo.w_DRCODSOC = this.w_DRCODSOC
    oTo.w_DRCODRAP = this.w_DRCODRAP
    oTo.w_DRDATOBSO = this.w_DRDATOBSO
    PCContext::Load(oTo)
enddefine

define class tcgsar_ard as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 271
  Height = 143
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-29"
  HelpContextID=126449513
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  RAPP_ANA_IDX = 0
  cFile = "RAPP_ANA"
  cKeySelect = "DRCODANA,DRCODAZI,DRCODSOC,DRCODRAP"
  cKeyWhere  = "DRCODANA=this.w_DRCODANA and DRCODAZI=this.w_DRCODAZI and DRCODSOC=this.w_DRCODSOC and DRCODRAP=this.w_DRCODRAP"
  cKeyWhereODBC = '"DRCODANA="+cp_ToStrODBC(this.w_DRCODANA)';
      +'+" and DRCODAZI="+cp_ToStrODBC(this.w_DRCODAZI)';
      +'+" and DRCODSOC="+cp_ToStrODBC(this.w_DRCODSOC)';
      +'+" and DRCODRAP="+cp_ToStrODBC(this.w_DRCODRAP)';

  cKeyWhereODBCqualified = '"RAPP_ANA.DRCODANA="+cp_ToStrODBC(this.w_DRCODANA)';
      +'+" and RAPP_ANA.DRCODAZI="+cp_ToStrODBC(this.w_DRCODAZI)';
      +'+" and RAPP_ANA.DRCODSOC="+cp_ToStrODBC(this.w_DRCODSOC)';
      +'+" and RAPP_ANA.DRCODRAP="+cp_ToStrODBC(this.w_DRCODRAP)';

  cPrg = "gsar_ard"
  cComment = "Rapporti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DRCODANA = space(15)
  w_DRCODAZI = space(5)
  w_DRCODSOC = space(15)
  w_DRCODRAP = 0
  w_DRDATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ardPag1","gsar_ard",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Rapporti")
      .Pages(1).HelpContextID = 31321983
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='RAPP_ANA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RAPP_ANA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RAPP_ANA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ard'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RAPP_ANA where DRCODANA=KeySet.DRCODANA
    *                            and DRCODAZI=KeySet.DRCODAZI
    *                            and DRCODSOC=KeySet.DRCODSOC
    *                            and DRCODRAP=KeySet.DRCODRAP
    *
    i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAPP_ANA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RAPP_ANA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RAPP_ANA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RAPP_ANA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DRCODANA',this.w_DRCODANA  ,'DRCODAZI',this.w_DRCODAZI  ,'DRCODSOC',this.w_DRCODSOC  ,'DRCODRAP',this.w_DRCODRAP  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DRCODANA = NVL(DRCODANA,space(15))
        .w_DRCODAZI = NVL(DRCODAZI,space(5))
        .w_DRCODSOC = NVL(DRCODSOC,space(15))
        .w_DRCODRAP = NVL(DRCODRAP,0)
        .w_DRDATOBSO = NVL(cp_ToDate(DRDATOBSO),ctod("  /  /  "))
        cp_LoadRecExtFlds(this,'RAPP_ANA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DRCODANA = space(15)
      .w_DRCODAZI = space(5)
      .w_DRCODSOC = space(15)
      .w_DRCODRAP = 0
      .w_DRDATOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_DRCODANA = This.oParentObject .w_CODANA
        .w_DRCODAZI = i_codazi
        .w_DRCODSOC = This.oParentObject .w_CODSOC
        .w_DRCODRAP = This.oParentObject .w_CODRAP
        .w_DRDATOBSO = This.oParentObject .w_RAPOBSO
      endif
    endwith
    cp_BlankRecExtFlds(this,'RAPP_ANA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
    endwith
    cp_SetEnabledExtFlds(this,'RAPP_ANA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODANA,"DRCODANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODAZI,"DRCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODSOC,"DRCODSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODRAP,"DRCODRAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRDATOBSO,"DRDATOBSO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAPP_ANA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RAPP_ANA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RAPP_ANA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RAPP_ANA')
        i_extval=cp_InsertValODBCExtFlds(this,'RAPP_ANA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DRCODANA,DRCODAZI,DRCODSOC,DRCODRAP,DRDATOBSO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DRCODANA)+;
                  ","+cp_ToStrODBC(this.w_DRCODAZI)+;
                  ","+cp_ToStrODBC(this.w_DRCODSOC)+;
                  ","+cp_ToStrODBC(this.w_DRCODRAP)+;
                  ","+cp_ToStrODBC(this.w_DRDATOBSO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RAPP_ANA')
        i_extval=cp_InsertValVFPExtFlds(this,'RAPP_ANA')
        cp_CheckDeletedKey(i_cTable,0,'DRCODANA',this.w_DRCODANA,'DRCODAZI',this.w_DRCODAZI,'DRCODSOC',this.w_DRCODSOC,'DRCODRAP',this.w_DRCODRAP)
        INSERT INTO (i_cTable);
              (DRCODANA,DRCODAZI,DRCODSOC,DRCODRAP,DRDATOBSO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DRCODANA;
                  ,this.w_DRCODAZI;
                  ,this.w_DRCODSOC;
                  ,this.w_DRCODRAP;
                  ,this.w_DRDATOBSO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAPP_ANA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RAPP_ANA_IDX,i_nConn)
      *
      * update RAPP_ANA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RAPP_ANA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DRDATOBSO="+cp_ToStrODBC(this.w_DRDATOBSO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RAPP_ANA')
        i_cWhere = cp_PKFox(i_cTable  ,'DRCODANA',this.w_DRCODANA  ,'DRCODAZI',this.w_DRCODAZI  ,'DRCODSOC',this.w_DRCODSOC  ,'DRCODRAP',this.w_DRCODRAP  )
        UPDATE (i_cTable) SET;
              DRDATOBSO=this.w_DRDATOBSO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAPP_ANA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RAPP_ANA_IDX,i_nConn)
      *
      * delete RAPP_ANA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DRCODANA',this.w_DRCODANA  ,'DRCODAZI',this.w_DRCODAZI  ,'DRCODSOC',this.w_DRCODSOC  ,'DRCODRAP',this.w_DRCODRAP  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RAPP_ANA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAPP_ANA_IDX,2])
    if i_bUpd
      with this
            .w_DRCODANA = This.oParentObject .w_CODANA
        .DoRTCalc(2,2,.t.)
            .w_DRCODSOC = This.oParentObject .w_CODSOC
            .w_DRCODRAP = This.oParentObject .w_CODRAP
            .w_DRDATOBSO = This.oParentObject .w_RAPOBSO
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'RAPP_ANA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ardPag1 as StdContainer
  Width  = 267
  height = 143
  stdWidth  = 267
  stdheight = 143
  resizeXpos=119
  resizeYpos=46
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oStr_1_6 as StdString with uid="CVUVHQVPNE",Visible=.t., Left=6, Top=22,;
    Alignment=1, Width=117, Height=18,;
    Caption="Rapporti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="BCZFBFPSIW",Visible=.t., Left=137, Top=22,;
    Alignment=0, Width=117, Height=18,;
    Caption="Cliente"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MMQEYPOFLM",Visible=.t., Left=137, Top=43,;
    Alignment=0, Width=117, Height=18,;
    Caption="Fornitore"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HPCYVDCUMU",Visible=.t., Left=137, Top=64,;
    Alignment=0, Width=117, Height=18,;
    Caption="Agente"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MOJJICWDON",Visible=.t., Left=137, Top=106,;
    Alignment=0, Width=117, Height=18,;
    Caption="Risorsa umana"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ZCWFMWDGYF",Visible=.t., Left=137, Top=85,;
    Alignment=0, Width=117, Height=18,;
    Caption="Vettore"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ard','RAPP_ANA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DRCODANA=RAPP_ANA.DRCODANA";
  +" and "+i_cAliasName2+".DRCODAZI=RAPP_ANA.DRCODAZI";
  +" and "+i_cAliasName2+".DRCODSOC=RAPP_ANA.DRCODSOC";
  +" and "+i_cAliasName2+".DRCODRAP=RAPP_ANA.DRCODRAP";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
