* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bib                                                        *
*              Calcolo formula                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [39] [VRS_61]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2009-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bib",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bib as StdBatch
  * --- Local variables
  pOper = space(5)
  w_OCCUR = 0
  w_LUNGVAR = 0
  w_INDOCC = 0
  w_PADRE = .NULL.
  w_GSCG_MTI = .NULL.
  w_GSCG_KCF = .NULL.
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_OCCUR2 = 0
  w_VALORE = 0
  Func = space(0)
  CODTOT = space(10)
  CODMIS = space(15)
  INDICE = 0
  w_STRINGA = space(50)
  w_TIPTOT = space(1)
  * --- WorkFile variables
  TOT_MAST_idx=0
  TOT_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato per il calcolo degli indici.(GSCG_KCF calcola formula)
    do case
      case alltrim(this.pOper)="CE"
        this.w_OCCUR = 0
        DIMENSION OCCURV(6)
        OCCURV(1) = RATC("+",this.oParentObject.w_TIFORMUL)
        OCCURV(2) = RATC("-",this.oParentObject.w_TIFORMUL)
        OCCURV(3) = RATC("*",this.oParentObject.w_TIFORMUL)
        OCCURV(4) = RATC("/",this.oParentObject.w_TIFORMUL)
        OCCURV(5) = RATC("(",this.oParentObject.w_TIFORMUL)
        OCCURV(6) = RATC(")",this.oParentObject.w_TIFORMUL)
        this.w_LUNGVAR = LEN(ALLTRIM(this.oParentObject.w_TIFORMUL))
        FOR this.w_INDOCC=1 TO 6
        this.w_OCCUR = IIF(this.w_OCCUR < OCCURV(this.w_INDOCC), IIF(this.w_LUNGVAR=OCCURV(this.w_INDOCC),OCCURV(this.w_INDOCC)-1,OCCURV(this.w_INDOCC)), this.w_OCCUR)
        ENDFOR
        this.oParentObject.w_TIFORMUL = SUBSTR(ALLTRIM(this.oParentObject.w_TIFORMUL),1,this.w_OCCUR)
      case INLIST(alltrim(this.pOper),"+","-","*","/","(",".",")")
        this.oParentObject.w_TIFORMUL = alltrim(this.oParentObject.w_TIFORMUL)+alltrim(this.pOper)
      case INLIST(alltrim(this.pOper),"0","1","2","3","4","5","6","7","8","9")
        this.oParentObject.w_TIFORMUL = alltrim(this.oParentObject.w_TIFORMUL)+alltrim(this.pOper)
      case alltrim(this.pOper)="CZ"
        this.oParentObject.w_TIFORMUL = ""
      case alltrim(this.pOper)="VE"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case alltrim(this.pOper)="ST"
        * --- Utilizzato in GSCG_KST per selezionare totalizzatori
        this.w_PADRE = this.oParentobject
        this.w_GSCG_KCF = this.oParentobject.oParentobject
        this.w_GSCG_MTI = this.oParentobject.oParentobject.oparentobject
        this.w_GSCG_KCF.w_TIFORMUL = Alltrim(this.w_GSCG_KCF.w_TIFORMUL) +"{"+alltrim(this.oParentObject.w_CODTOT)+"."+alltrim(this.oParentObject.w_CODMIS) + "}"
        this.w_GSCG_MTI.SetUpdateRow()     
        * --- Chiudo la Maschera - utilizzato in GSBI_KIB
        this.w_PADRE.ecpsave()     
        i_retcode = 'stop'
        return
      case alltrim(this.pOper)="CF"
        * --- Utilizzato in GSCG_KCF per selezionare totalizzatori
        this.w_GSCG_MTI = this.oParentobject.oParentobject
        this.w_GSCG_MTI.SetUpdateRow()     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della formula integrata nell'indice di bilancio
    Messaggio = "Corretta"
    * --- Ricerco il segno '{' il quale indica che ho inserito una costante
    this.TmpSave = ALLTRIM(this.oParentObject.w_TIFORMUL)
    this.w_OCCUR = RATC("{",this.TmpSave)
    this.w_OCCUR1 = RATC("}",this.TmpSave)
    do while this.w_OCCUR <> 0
      this.w_STRINGA = SUBSTR(this.TmpSave,this.w_occur+1,this.w_occur1-this.w_occur-1)
      this.w_OCCUR2 = RATC(".",this.w_STRINGA)
      this.CODTOT = ALLTRIM(SUBSTR(this.w_STRINGA,1,this.w_OCCUR2-1))
      this.CODMIS = ALLTRIM(SUBSTR(this.w_STRINGA,this.w_OCCUR2+1,LEN(this.w_STRINGA)))
      * --- Ciclo sulla stringa per verificarne la correttezza
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      this.TmpSave1 = this.TmpSave1 + "10"
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      * --- Verifico la correttezza del Totalizzatore
      * --- Read from TOT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TOT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2],.t.,this.TOT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TITIPTOT"+;
          " from "+i_cTable+" TOT_MAST where ";
              +"TICODICE = "+cp_ToStrODBC(this.CODTOT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TITIPTOT;
          from (i_cTable) where;
              TICODICE = this.CODTOT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPTOT = NVL(cp_ToDate(_read_.TITIPTOT),cp_NullValue(_read_.TITIPTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows = 0
        Messaggio = "Errata"
        * --- Sostituisco il totalizzatore errato  mettendolo tra ##
        this.TmpSave1 = SUBSTR(this.oParentObject.w_TIFORMUL,1,this.w_OCCUR)
        this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODTOT) + "#"
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_TIFORMUL,this.w_OCCUR2))
        this.oParentObject.w_TIFORMUL = ALLTRIM(this.TmpSave1)
        this.oParentObject.w_ERRMSG = "T"
        EXIT
      else
        if this.w_TIPTOT$ "C-M"
          * --- Totalizzatore composto
          * --- Read from TOT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TOT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" TOT_DETT where ";
                  +"TICODICE = "+cp_ToStrODBC(this.CODTOT);
                  +" and TICODCOM = "+cp_ToStrODBC(this.CODMIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  TICODICE = this.CODTOT;
                  and TICODCOM = this.CODMIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              Messaggio = "Errata"
              * --- Sostituisco lamisura errata  mettendola tra ##
              this.TmpSave1 = SUBSTR(this.oParentObject.w_TIFORMUL,1,this.w_OCCUR2)
              this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODMIS) + "#"
              this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_TIFORMUL,this.w_OCCUR1))
              this.oParentObject.w_TIFORMUL = ALLTRIM(this.TmpSave1)
              this.oParentObject.w_ERRMSG = "M"
              EXIT
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Totalizzatore semplice
          * --- Read from TOT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TOT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" TOT_DETT where ";
                  +"TICODICE = "+cp_ToStrODBC(this.CODTOT);
                  +" and TICODMIS = "+cp_ToStrODBC(this.CODMIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  TICODICE = this.CODTOT;
                  and TICODMIS = this.CODMIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              Messaggio = "Errata"
              * --- Sostituisco la misura errata  mettendola tra ##
              this.TmpSave1 = SUBSTR(this.oParentObject.w_TIFORMUL,1,this.w_OCCUR2)
              this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODMIS) + "#"
              this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_TIFORMUL,this.w_OCCUR1))
              this.oParentObject.w_TIFORMUL = ALLTRIM(this.TmpSave1)
              this.oParentObject.w_ERRMSG = "M"
              EXIT
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("{",this.TmpSave)
      this.w_OCCUR1 = RATC("}",this.TmpSave)
    enddo
    if Messaggio = "Corretta"
      this.Func = this.oParentObject.w_TIFORMUL
      this.FuncCalc = ALLTRIM(this.TmpSave)
      ON ERROR Messaggio = "Errata"
      TmpVal = this.FuncCalc
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      ON ERROR
      if Messaggio="Errata"
        this.oParentObject.w_ERRMSG = "N"
      else
        this.oParentObject.w_ERRMSG = "S"
      endif
    endif
    WAIT CLEAR
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TOT_MAST'
    this.cWorkTables[2]='TOT_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
