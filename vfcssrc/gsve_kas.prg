* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kas                                                        *
*              Cruscotto analisi scostamento prezzo                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-12                                                      *
* Last revis.: 2012-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kas",oParentObject))

* --- Class definition
define class tgsve_kas as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 789
  Height = 559+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-27"
  HelpContextID=132788841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=97

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  BUSIUNIT_IDX = 0
  CAN_TIER_IDX = 0
  MAGAZZIN_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsve_kas"
  cComment = "Cruscotto analisi scostamento prezzo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ID__GEST = space(10)
  w_MVFLVEAC = space(1)
  o_MVFLVEAC = space(1)
  w_MVTIPDOC = space(5)
  o_MVTIPDOC = space(5)
  w_ANTIPINI = space(1)
  w_TDDESDOC = space(35)
  w_TDFLVEAC = space(1)
  w_TDFLINTE = space(1)
  w_TDCHKUCA = space(1)
  w_MVCONINI = space(15)
  w_ANDESINI = space(60)
  w_MVCONFIN = space(15)
  w_ANDESFIN = space(60)
  w_MVNREINI = 0
  w_MVREGINI = ctod('  /  /  ')
  w_MVNREFIN = 0
  w_MVREGFIN = ctod('  /  /  ')
  w_MVNDOINI = 0
  w_MVALFINI = space(2)
  w_MVDATINI = ctod('  /  /  ')
  w_MVNDOFIN = 0
  w_MVALFFIN = space(2)
  w_MVDATFIN = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_MVFLPROV = space(1)
  w_MVSERATT = space(10)
  w_MVCRIINI = space(41)
  o_MVCRIINI = space(41)
  w_CADESINI = space(40)
  w_MVCRIFIN = space(41)
  o_MVCRIFIN = space(41)
  w_MVCARINI = space(20)
  w_ARDESINI = space(40)
  w_CADESFIN = space(40)
  w_MVCARFIN = space(20)
  w_ARDESFIN = space(40)
  w_MVBUNINI = space(3)
  w_BUDESINI = space(40)
  w_MVBUNFIN = space(3)
  w_BUDESFIN = space(40)
  w_MVCOMINI = space(15)
  w_CNCANINI = space(100)
  w_MVCOMFIN = space(15)
  w_CNCANFIN = space(100)
  w_MVCODMAG = space(5)
  w_FLTYPEAN = space(1)
  w_FLFRSTRU = .F.
  w_ANTIPFIN = space(1)
  w_MGDESMAG = space(30)
  w_VADESVAL = space(35)
  w_FLTYPROW = space(1)
  o_FLTYPROW = space(1)
  w_ANTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_MVCLADOC = space(2)
  o_MVCLADOC = space(2)
  w_TDCATDOC = space(2)
  w_MVROWATT = 0
  w_ATFLVEAC = space(1)
  w_ATCLADOC = space(2)
  w_ATROWORD = 0
  w_ATCODICE = space(41)
  w_ATUNIMIS = space(41)
  w_ATPRZUCA = 0
  w_ATDATUCA = ctod('  /  /  ')
  w_ATLISCHK = space(5)
  w_ATPREZZO = 0
  w_ATQTAMOV = 0
  w_ATIMPNAZ = 0
  w_ATVALRIG = 0
  w_ATPRZCHK = 0
  w_ATIMPPRO = 0
  w_ATPERPRO = 0
  w_ATSCONT1 = 0
  w_ATSCONT2 = 0
  w_ATSCONT3 = 0
  w_ATSCONT4 = 0
  w_ATFLCPRO = space(1)
  w_ATCODLIS = space(5)
  w_ATCHKUCA = space(1)
  w_ATSCOLIS = space(5)
  w_ATPROLIS = space(5)
  w_ATPROSCO = space(5)
  w_ATCODVAL = space(5)
  w_ATQTAUM1 = 0
  w_ATCODMAG = space(5)
  w_ATTIPCON = space(1)
  w_ATCODCON = space(15)
  w_ATCAOVAL = 0
  w_ATDATDOC = ctod('  /  /  ')
  w_ATIMPSCO = 0
  w_ATSCOCL1 = 0
  w_ATSCOCL2 = 0
  w_FLCHKUCA = space(1)
  w_FLCHKLST = space(1)
  w_FLCHKLFA = space(1)
  w_TDRICNOM = space(1)
  w_CATEGO = space(2)
  w_CICLO = space(1)
  w_FLDATRIF = ctod('  /  /  ')
  w_FLPERMAR = 0
  w_ZOOMMAST = .NULL.
  w_ZOOMDETT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kasPag1","gsve_kas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsve_kasPag2","gsve_kas",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Documenti analizzati")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVTIPDOC_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMMAST = this.oPgFrm.Pages(2).oPag.ZOOMMAST
    this.w_ZOOMDETT = this.oPgFrm.Pages(2).oPag.ZOOMDETT
    DoDefault()
    proc Destroy()
      this.w_ZOOMMAST = .NULL.
      this.w_ZOOMDETT = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='VALUTE'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ID__GEST=space(10)
      .w_MVFLVEAC=space(1)
      .w_MVTIPDOC=space(5)
      .w_ANTIPINI=space(1)
      .w_TDDESDOC=space(35)
      .w_TDFLVEAC=space(1)
      .w_TDFLINTE=space(1)
      .w_TDCHKUCA=space(1)
      .w_MVCONINI=space(15)
      .w_ANDESINI=space(60)
      .w_MVCONFIN=space(15)
      .w_ANDESFIN=space(60)
      .w_MVNREINI=0
      .w_MVREGINI=ctod("  /  /  ")
      .w_MVNREFIN=0
      .w_MVREGFIN=ctod("  /  /  ")
      .w_MVNDOINI=0
      .w_MVALFINI=space(2)
      .w_MVDATINI=ctod("  /  /  ")
      .w_MVNDOFIN=0
      .w_MVALFFIN=space(2)
      .w_MVDATFIN=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_MVFLPROV=space(1)
      .w_MVSERATT=space(10)
      .w_MVCRIINI=space(41)
      .w_CADESINI=space(40)
      .w_MVCRIFIN=space(41)
      .w_MVCARINI=space(20)
      .w_ARDESINI=space(40)
      .w_CADESFIN=space(40)
      .w_MVCARFIN=space(20)
      .w_ARDESFIN=space(40)
      .w_MVBUNINI=space(3)
      .w_BUDESINI=space(40)
      .w_MVBUNFIN=space(3)
      .w_BUDESFIN=space(40)
      .w_MVCOMINI=space(15)
      .w_CNCANINI=space(100)
      .w_MVCOMFIN=space(15)
      .w_CNCANFIN=space(100)
      .w_MVCODMAG=space(5)
      .w_FLTYPEAN=space(1)
      .w_FLFRSTRU=.f.
      .w_ANTIPFIN=space(1)
      .w_MGDESMAG=space(30)
      .w_VADESVAL=space(35)
      .w_FLTYPROW=space(1)
      .w_ANTIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_MVCLADOC=space(2)
      .w_TDCATDOC=space(2)
      .w_MVROWATT=0
      .w_ATFLVEAC=space(1)
      .w_ATCLADOC=space(2)
      .w_ATROWORD=0
      .w_ATCODICE=space(41)
      .w_ATUNIMIS=space(41)
      .w_ATPRZUCA=0
      .w_ATDATUCA=ctod("  /  /  ")
      .w_ATLISCHK=space(5)
      .w_ATPREZZO=0
      .w_ATQTAMOV=0
      .w_ATIMPNAZ=0
      .w_ATVALRIG=0
      .w_ATPRZCHK=0
      .w_ATIMPPRO=0
      .w_ATPERPRO=0
      .w_ATSCONT1=0
      .w_ATSCONT2=0
      .w_ATSCONT3=0
      .w_ATSCONT4=0
      .w_ATFLCPRO=space(1)
      .w_ATCODLIS=space(5)
      .w_ATCHKUCA=space(1)
      .w_ATSCOLIS=space(5)
      .w_ATPROLIS=space(5)
      .w_ATPROSCO=space(5)
      .w_ATCODVAL=space(5)
      .w_ATQTAUM1=0
      .w_ATCODMAG=space(5)
      .w_ATTIPCON=space(1)
      .w_ATCODCON=space(15)
      .w_ATCAOVAL=0
      .w_ATDATDOC=ctod("  /  /  ")
      .w_ATIMPSCO=0
      .w_ATSCOCL1=0
      .w_ATSCOCL2=0
      .w_FLCHKUCA=space(1)
      .w_FLCHKLST=space(1)
      .w_FLCHKLFA=space(1)
      .w_TDRICNOM=space(1)
      .w_CATEGO=space(2)
      .w_CICLO=space(1)
      .w_FLDATRIF=ctod("  /  /  ")
      .w_FLPERMAR=0
        .w_CODAZI = i_codazi
        .w_ID__GEST = SYS(2015)
        .w_MVFLVEAC = 'V'
        .w_MVTIPDOC = IIF((.w_TDFLVEAC=.w_MVFLVEAC OR .w_MVFLVEAC='E') AND (.w_TDCATDOC=.w_MVCLADOC OR .w_MVCLADOC='XX'), .w_MVTIPDOC, '')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MVTIPDOC))
          .link_1_5('Full')
        endif
          .DoRTCalc(5,9,.f.)
        .w_MVCONINI = ''
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MVCONINI))
          .link_1_13('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_MVCONFIN = ''
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MVCONFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(13,24,.f.)
        if not(empty(.w_MVCODVAL))
          .link_1_37('Full')
        endif
        .w_MVFLPROV = 'N'
      .oPgFrm.Page2.oPag.ZOOMMAST.Calculate()
        .w_MVSERATT = .w_ZOOMMAST.Getvar("MVSERIAL")
      .oPgFrm.Page2.oPag.ZOOMDETT.Calculate(.w_MVSERATT)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_MVCRIINI))
          .link_1_43('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_MVCRIFIN))
          .link_1_46('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_MVCARINI))
          .link_1_47('Full')
        endif
        .DoRTCalc(31,33,.f.)
        if not(empty(.w_MVCARFIN))
          .link_1_52('Full')
        endif
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_MVBUNINI))
          .link_1_55('Full')
        endif
        .DoRTCalc(36,39,.f.)
        if not(empty(.w_MVCOMINI))
          .link_1_60('Full')
        endif
        .DoRTCalc(40,43,.f.)
        if not(empty(.w_MVCODMAG))
          .link_1_66('Full')
        endif
        .w_FLTYPEAN = 'D'
        .w_FLFRSTRU = .T.
          .DoRTCalc(46,48,.f.)
        .w_FLTYPROW = 'T'
        .w_ANTIPCON = ICASE( !EMPTY(.w_ANTIPINI), .w_ANTIPINI, !EMPTY(.w_ANTIPFIN), .w_ANTIPFIN, '')
        .w_OBTEST = i_Datsys
        .w_MVCLADOC = 'XX'
          .DoRTCalc(53,53,.f.)
        .w_MVROWATT = .w_ZOOMDETT.Getvar("CPROWNUM")
        .w_ATFLVEAC = .w_ZOOMMAST.Getvar("MVFLVEAC")
        .w_ATCLADOC = .w_ZOOMMAST.Getvar("MVCLADOC")
        .w_ATROWORD = .w_ZOOMDETT.Getvar("CPROWORD")
        .w_ATCODICE = .w_ZOOMDETT.Getvar("MVCODICE")
        .w_ATUNIMIS = .w_ZOOMDETT.Getvar("MVUNIMIS")
        .w_ATPRZUCA = .w_ZOOMDETT.Getvar("PREZ_UCA")
        .w_ATDATUCA = .w_ZOOMDETT.Getvar("DATE_UCA")
        .w_ATLISCHK = .w_ZOOMDETT.Getvar("LIST_CHK")
        .w_ATPREZZO = .w_ZOOMDETT.Getvar("MVPREZZO")
        .w_ATQTAMOV = .w_ZOOMDETT.Getvar("MVQTAMOV")
        .w_ATIMPNAZ = .w_ZOOMDETT.Getvar("MVIMPNAZ")
        .w_ATVALRIG = .w_ZOOMDETT.Getvar("MVVALRIG") / .w_ATQTAMOV
        .w_ATPRZCHK = .w_ZOOMDETT.Getvar("PRZCHECK")
        .w_ATIMPPRO = .w_ZOOMDETT.Getvar("MVIMPPRO")
        .w_ATPERPRO = .w_ZOOMDETT.Getvar("MVPERPRO")
        .w_ATSCONT1 = .w_ZOOMDETT.Getvar("MVSCONT1")
        .w_ATSCONT2 = .w_ZOOMDETT.Getvar("MVSCONT2")
        .w_ATSCONT3 = .w_ZOOMDETT.Getvar("MVSCONT3")
        .w_ATSCONT4 = .w_ZOOMDETT.Getvar("MVSCONT4")
        .w_ATFLCPRO = IIF(.w_ATIMPPRO<>0 OR .w_ATPERPRO<>0, 'S', 'N')
        .w_ATCODLIS = .w_ZOOMDETT.Getvar("MVCODLIS")
        .w_ATCHKUCA = .w_ZOOMDETT.Getvar("ORCHKUCA")
        .w_ATSCOLIS = .w_ZOOMDETT.Getvar("MVSCOLIS")
        .w_ATPROLIS = .w_ZOOMDETT.Getvar("MVPROLIS")
        .w_ATPROSCO = .w_ZOOMDETT.Getvar("MVPROSCO")
        .w_ATCODVAL = .w_ZOOMDETT.Getvar("MVCODVAL")
        .w_ATQTAUM1 = .w_ZOOMDETT.Getvar("MVQTAUM1")
        .w_ATCODMAG = .w_ZOOMDETT.Getvar("MVCODMAG")
        .w_ATTIPCON = .w_ZOOMMAST.Getvar("MVTIPCON")
        .w_ATCODCON = .w_ZOOMMAST.Getvar("MVCODCON")
        .w_ATCAOVAL = .w_ZOOMDETT.Getvar("MVCAOVAL")
        .w_ATDATDOC = .w_ZOOMMAST.Getvar("MVDATDOC")
        .w_ATIMPSCO = .w_ZOOMDETT.Getvar("MVIMPSCO")
        .w_ATSCOCL1 = .w_ZOOMDETT.Getvar("MVSCOCL1")
        .w_ATSCOCL2 = .w_ZOOMDETT.Getvar("MVSCOCL2")
        .w_FLCHKUCA = 'S'
        .w_FLCHKLST = 'S'
        .w_FLCHKLFA = 'S'
          .DoRTCalc(93,93,.f.)
        .w_CATEGO = IIF(.w_MVCLADOC='XX', '', .w_MVCLADOC)
        .w_CICLO = .w_MVFLVEAC
        .w_FLDATRIF = i_DatSys
        .w_FLPERMAR = IIF( .w_FLTYPROW='T', 0, .w_FLPERMAR )
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_1.enabled = this.oPgFrm.Page2.oPag.oBtn_2_1.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_MVFLVEAC<>.w_MVFLVEAC.or. .o_MVCLADOC<>.w_MVCLADOC
            .w_MVTIPDOC = IIF((.w_TDFLVEAC=.w_MVFLVEAC OR .w_MVFLVEAC='E') AND (.w_TDCATDOC=.w_MVCLADOC OR .w_MVCLADOC='XX'), .w_MVTIPDOC, '')
          .link_1_5('Full')
        endif
        .DoRTCalc(5,9,.t.)
        if .o_MVFLVEAC<>.w_MVFLVEAC.or. .o_MVTIPDOC<>.w_MVTIPDOC
            .w_MVCONINI = ''
          .link_1_13('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_MVFLVEAC<>.w_MVFLVEAC.or. .o_MVTIPDOC<>.w_MVTIPDOC
            .w_MVCONFIN = ''
          .link_1_15('Full')
        endif
        .oPgFrm.Page2.oPag.ZOOMMAST.Calculate()
        .DoRTCalc(13,25,.t.)
            .w_MVSERATT = .w_ZOOMMAST.Getvar("MVSERIAL")
        .oPgFrm.Page2.oPag.ZOOMDETT.Calculate(.w_MVSERATT)
        if .o_MVCRIINI<>.w_MVCRIINI.or. .o_MVCRIFIN<>.w_MVCRIFIN
          .Calculate_FQAMRJXPFH()
        endif
        .DoRTCalc(27,49,.t.)
            .w_ANTIPCON = ICASE( !EMPTY(.w_ANTIPINI), .w_ANTIPINI, !EMPTY(.w_ANTIPFIN), .w_ANTIPFIN, '')
        .DoRTCalc(51,53,.t.)
            .w_MVROWATT = .w_ZOOMDETT.Getvar("CPROWNUM")
            .w_ATFLVEAC = .w_ZOOMMAST.Getvar("MVFLVEAC")
            .w_ATCLADOC = .w_ZOOMMAST.Getvar("MVCLADOC")
            .w_ATROWORD = .w_ZOOMDETT.Getvar("CPROWORD")
            .w_ATCODICE = .w_ZOOMDETT.Getvar("MVCODICE")
            .w_ATUNIMIS = .w_ZOOMDETT.Getvar("MVUNIMIS")
            .w_ATPRZUCA = .w_ZOOMDETT.Getvar("PREZ_UCA")
            .w_ATDATUCA = .w_ZOOMDETT.Getvar("DATE_UCA")
            .w_ATLISCHK = .w_ZOOMDETT.Getvar("LIST_CHK")
            .w_ATPREZZO = .w_ZOOMDETT.Getvar("MVPREZZO")
            .w_ATQTAMOV = .w_ZOOMDETT.Getvar("MVQTAMOV")
            .w_ATIMPNAZ = .w_ZOOMDETT.Getvar("MVIMPNAZ")
            .w_ATVALRIG = .w_ZOOMDETT.Getvar("MVVALRIG") / .w_ATQTAMOV
            .w_ATPRZCHK = .w_ZOOMDETT.Getvar("PRZCHECK")
            .w_ATIMPPRO = .w_ZOOMDETT.Getvar("MVIMPPRO")
            .w_ATPERPRO = .w_ZOOMDETT.Getvar("MVPERPRO")
            .w_ATSCONT1 = .w_ZOOMDETT.Getvar("MVSCONT1")
            .w_ATSCONT2 = .w_ZOOMDETT.Getvar("MVSCONT2")
            .w_ATSCONT3 = .w_ZOOMDETT.Getvar("MVSCONT3")
            .w_ATSCONT4 = .w_ZOOMDETT.Getvar("MVSCONT4")
            .w_ATFLCPRO = IIF(.w_ATIMPPRO<>0 OR .w_ATPERPRO<>0, 'S', 'N')
            .w_ATCODLIS = .w_ZOOMDETT.Getvar("MVCODLIS")
            .w_ATCHKUCA = .w_ZOOMDETT.Getvar("ORCHKUCA")
            .w_ATSCOLIS = .w_ZOOMDETT.Getvar("MVSCOLIS")
            .w_ATPROLIS = .w_ZOOMDETT.Getvar("MVPROLIS")
            .w_ATPROSCO = .w_ZOOMDETT.Getvar("MVPROSCO")
            .w_ATCODVAL = .w_ZOOMDETT.Getvar("MVCODVAL")
            .w_ATQTAUM1 = .w_ZOOMDETT.Getvar("MVQTAUM1")
            .w_ATCODMAG = .w_ZOOMDETT.Getvar("MVCODMAG")
            .w_ATTIPCON = .w_ZOOMMAST.Getvar("MVTIPCON")
            .w_ATCODCON = .w_ZOOMMAST.Getvar("MVCODCON")
            .w_ATCAOVAL = .w_ZOOMDETT.Getvar("MVCAOVAL")
            .w_ATDATDOC = .w_ZOOMMAST.Getvar("MVDATDOC")
            .w_ATIMPSCO = .w_ZOOMDETT.Getvar("MVIMPSCO")
            .w_ATSCOCL1 = .w_ZOOMDETT.Getvar("MVSCOCL1")
            .w_ATSCOCL2 = .w_ZOOMDETT.Getvar("MVSCOCL2")
        .DoRTCalc(90,93,.t.)
            .w_CATEGO = IIF(.w_MVCLADOC='XX', '', .w_MVCLADOC)
            .w_CICLO = .w_MVFLVEAC
        .DoRTCalc(96,96,.t.)
        if .o_FLTYPROW<>.w_FLTYPROW
            .w_FLPERMAR = IIF( .w_FLTYPROW='T', 0, .w_FLPERMAR )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMMAST.Calculate()
        .oPgFrm.Page2.oPag.ZOOMDETT.Calculate(.w_MVSERATT)
    endwith
  return

  proc Calculate_ZBSFAAYGXI()
    with this
          * --- Avvia la ricerca dei documenti
          gsve_bka(this;
              ,'RICERCA';
             )
    endwith
  endproc
  proc Calculate_LITOQELGHH()
    with this
          * --- Cancella i dati dalla tabella temporanea
          gsve_bka(this;
              ,'CHIUSURA';
             )
    endwith
  endproc
  proc Calculate_FQAMRJXPFH()
    with this
          * --- Carica dati articoli su cambio codice di ricerca
          .w_MVCARINI = .w_MVCARINI
          .link_1_47('Full')
          .w_MVCARFIN = .w_MVCARFIN
          .link_1_52('Full')
    endwith
  endproc
  proc Calculate_SNGAFWCWUL()
    with this
          * --- Apertura documento selezionato
          gsve_bka(this;
              ,"APRIMAST";
             )
    endwith
  endproc
  proc Calculate_CZAICZDDUL()
    with this
          * --- Apre maschera dettaglio riga
          gsve_kiu(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVCONINI_1_13.enabled = this.oPgFrm.Page1.oPag.oMVCONINI_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMVCONFIN_1_15.enabled = this.oPgFrm.Page1.oPag.oMVCONFIN_1_15.mCond()
    this.oPgFrm.Page1.oPag.oMVCRIINI_1_43.enabled = this.oPgFrm.Page1.oPag.oMVCRIINI_1_43.mCond()
    this.oPgFrm.Page1.oPag.oMVCRIFIN_1_46.enabled = this.oPgFrm.Page1.oPag.oMVCRIFIN_1_46.mCond()
    this.oPgFrm.Page1.oPag.oMVCARINI_1_47.enabled = this.oPgFrm.Page1.oPag.oMVCARINI_1_47.mCond()
    this.oPgFrm.Page1.oPag.oMVCARFIN_1_52.enabled = this.oPgFrm.Page1.oPag.oMVCARFIN_1_52.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_6.visible=!this.oPgFrm.Page2.oPag.oStr_2_6.mHide()
    this.oPgFrm.Page1.oPag.oFLDATRIF_1_90.visible=!this.oPgFrm.Page1.oPag.oFLDATRIF_1_90.mHide()
    this.oPgFrm.Page1.oPag.oFLPERMAR_1_91.visible=!this.oPgFrm.Page1.oPag.oFLPERMAR_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMMAST.Event(cEvent)
      .oPgFrm.Page2.oPag.ZOOMDETT.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_ZBSFAAYGXI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LITOQELGHH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAST selected")
          .Calculate_SNGAFWCWUL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMDETT selected")
          .Calculate_CZAICZDDUL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPDOC
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MVTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_MVTIPDOC)+"%");

            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMVTIPDOC_1_5'),i_cWhere,'',"Causali documento",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCHKUCA,TDCATDOC,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDCHKUCA = NVL(_Link_.TDCHKUCA,space(1))
      this.w_TDCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDRICNOM = NVL(_Link_.TDRICNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC = space(5)
      endif
      this.w_TDDESDOC = space(35)
      this.w_TDFLVEAC = space(1)
      this.w_TDFLINTE = space(1)
      this.w_TDCHKUCA = space(1)
      this.w_TDCATDOC = space(2)
      this.w_TDRICNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TDFLVEAC=.w_MVFLVEAC OR .w_MVFLVEAC='E') AND (.w_TDCATDOC=.w_MVCLADOC OR .w_MVCLADOC='XX')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MVTIPDOC = space(5)
        this.w_TDDESDOC = space(35)
        this.w_TDFLVEAC = space(1)
        this.w_TDFLINTE = space(1)
        this.w_TDCHKUCA = space(1)
        this.w_TDCATDOC = space(2)
        this.w_TDRICNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONINI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCONINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_MVCONINI))
          select ANCODICE,ANDESCRI,ANTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MVCONINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MVCONINI)+"%");

            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCONINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANCODICE',cp_AbsName(oSource.parent,'oMVCONINI_1_13'),i_cWhere,'',"Intestatari",'GSVE_KAS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCONINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_MVCONINI)
            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONINI = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESINI = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANTIPINI = NVL(_Link_.ANTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONINI = space(15)
      endif
      this.w_ANDESINI = space(60)
      this.w_ANTIPINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MVCONINI<=.w_MVCONFIN OR EMPTY(.w_MVCONFIN) ) AND (EMPTY(.w_TDFLINTE) OR .w_ANTIPINI=.w_TDFLINTE) AND (EMPTY(.w_ANTIPFIN) OR .w_ANTIPINI=.w_ANTIPFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario iniziale inesistente, maggiore del codice intestatario finale, incongruente con la causale selezionata o con l'intestatario finale")
        endif
        this.w_MVCONINI = space(15)
        this.w_ANDESINI = space(60)
        this.w_ANTIPINI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCONFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_MVCONFIN))
          select ANCODICE,ANDESCRI,ANTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MVCONFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MVCONFIN)+"%");

            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCONFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANCODICE',cp_AbsName(oSource.parent,'oMVCONFIN_1_15'),i_cWhere,'',"Intestatari",'GSVE_KAS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCONFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_MVCONFIN)
            select ANCODICE,ANDESCRI,ANTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESFIN = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANTIPFIN = NVL(_Link_.ANTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONFIN = space(15)
      endif
      this.w_ANDESFIN = space(60)
      this.w_ANTIPFIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MVCONINI<=.w_MVCONFIN OR EMPTY(.w_MVCONINI)) AND (EMPTY(.w_TDFLINTE) OR .w_ANTIPFIN=.w_TDFLINTE) AND (EMPTY(.w_ANTIPINI) OR .w_ANTIPINI=.w_ANTIPFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario finale inesistente, minore del codice intestatario iniziale, incongruente con la causale selezionata o con l'intestatario iniziale")
        endif
        this.w_MVCONFIN = space(15)
        this.w_ANDESFIN = space(60)
        this.w_ANTIPFIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODVAL
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MVCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MVCODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMVCODVAL_1_37'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MVCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MVCODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_VADESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODVAL = space(3)
      endif
      this.w_VADESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCRIINI
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCRIINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MVCRIINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MVCRIINI))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCRIINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MVCRIINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MVCRIINI)+"%");

            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCRIINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMVCRIINI_1_43'),i_cWhere,'',"Codici di ricerca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCRIINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MVCRIINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MVCRIINI)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCRIINI = NVL(_Link_.CACODICE,space(41))
      this.w_CADESINI = NVL(_Link_.CADESART,space(40))
      this.w_MVCARINI = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MVCRIINI = space(41)
      endif
      this.w_CADESINI = space(40)
      this.w_MVCARINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVCRIINI<=.w_MVCRIFIN OR EMPTY(.w_MVCRIFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice di ricerca iniziale deve essere minore o uguale del codice di ricerca finale")
        endif
        this.w_MVCRIINI = space(41)
        this.w_CADESINI = space(40)
        this.w_MVCARINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCRIINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCRIFIN
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCRIFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MVCRIFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MVCRIFIN))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCRIFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MVCRIFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MVCRIFIN)+"%");

            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCRIFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMVCRIFIN_1_46'),i_cWhere,'',"Codici di ricerca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCRIFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MVCRIFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MVCRIFIN)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCRIFIN = NVL(_Link_.CACODICE,space(41))
      this.w_CADESFIN = NVL(_Link_.CADESART,space(40))
      this.w_MVCARFIN = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MVCRIFIN = space(41)
      endif
      this.w_CADESFIN = space(40)
      this.w_MVCARFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVCRIINI<=.w_MVCRIFIN OR EMPTY(.w_MVCRIINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice di ricerca finale deve essere maggiore o uguale del codice di ricerca iniziale")
        endif
        this.w_MVCRIFIN = space(41)
        this.w_CADESFIN = space(40)
        this.w_MVCARFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCRIFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCARINI
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_MVCARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_MVCARINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCARINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_MVCARINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_MVCARINI)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCARINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oMVCARINI_1_47'),i_cWhere,'',"Articoli/Servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCARINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCARINI = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESINI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVCARINI = space(20)
      endif
      this.w_ARDESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVCARINI<=.w_MVCARFIN OR EMPTY(.w_MVCARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice articolo iniziale deve essre minore o uguale del codice articolo finale")
        endif
        this.w_MVCARINI = space(20)
        this.w_ARDESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCARFIN
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_MVCARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_MVCARFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCARFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_MVCARFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_MVCARFIN)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCARFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oMVCARFIN_1_52'),i_cWhere,'',"Articoli/Servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCARFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCARFIN = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVCARFIN = space(20)
      endif
      this.w_ARDESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVCARINI<=.w_MVCARFIN OR EMPTY(.w_MVCARINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice articolo finale deve essre maggiore o uguale del codice articolo iniziale")
        endif
        this.w_MVCARFIN = space(20)
        this.w_ARDESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVBUNINI
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVBUNINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_MVBUNINI)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_MVBUNINI))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVBUNINI)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStrODBC(trim(this.w_MVBUNINI)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BUDESCRI like "+cp_ToStr(trim(this.w_MVBUNINI)+"%");
                   +" and BUCODAZI="+cp_ToStr(this.w_CODAZI);

            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVBUNINI) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oMVBUNINI_1_55'),i_cWhere,'',"Business unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La business unit iniziale deve essere minore o uguale della business unit finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVBUNINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_MVBUNINI);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_MVBUNINI)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVBUNINI = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESINI = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVBUNINI = space(3)
      endif
      this.w_BUDESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVBUNINI <= .w_MVBUNFIN OR EMPTY(.w_MVBUNFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La business unit iniziale deve essere minore o uguale della business unit finale")
        endif
        this.w_MVBUNINI = space(3)
        this.w_BUDESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVBUNINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCOMINI
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MVCOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MVCOMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCOMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_MVCOMINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_MVCOMINI)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCOMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMVCOMINI_1_60'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MVCOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MVCOMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCOMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_CNCANINI = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_MVCOMINI = space(15)
      endif
      this.w_CNCANINI = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAG
  func Link_1_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAG_1_66'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAG = space(5)
      endif
      this.w_MGDESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOC_1_5.value==this.w_MVTIPDOC)
      this.oPgFrm.Page1.oPag.oMVTIPDOC_1_5.value=this.w_MVTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_7.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_7.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONINI_1_13.value==this.w_MVCONINI)
      this.oPgFrm.Page1.oPag.oMVCONINI_1_13.value=this.w_MVCONINI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESINI_1_14.value==this.w_ANDESINI)
      this.oPgFrm.Page1.oPag.oANDESINI_1_14.value=this.w_ANDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONFIN_1_15.value==this.w_MVCONFIN)
      this.oPgFrm.Page1.oPag.oMVCONFIN_1_15.value=this.w_MVCONFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESFIN_1_16.value==this.w_ANDESFIN)
      this.oPgFrm.Page1.oPag.oANDESFIN_1_16.value=this.w_ANDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNREINI_1_18.value==this.w_MVNREINI)
      this.oPgFrm.Page1.oPag.oMVNREINI_1_18.value=this.w_MVNREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVREGINI_1_20.value==this.w_MVREGINI)
      this.oPgFrm.Page1.oPag.oMVREGINI_1_20.value=this.w_MVREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNREFIN_1_22.value==this.w_MVNREFIN)
      this.oPgFrm.Page1.oPag.oMVNREFIN_1_22.value=this.w_MVNREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVREGFIN_1_24.value==this.w_MVREGFIN)
      this.oPgFrm.Page1.oPag.oMVREGFIN_1_24.value=this.w_MVREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNDOINI_1_26.value==this.w_MVNDOINI)
      this.oPgFrm.Page1.oPag.oMVNDOINI_1_26.value=this.w_MVNDOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFINI_1_28.value==this.w_MVALFINI)
      this.oPgFrm.Page1.oPag.oMVALFINI_1_28.value=this.w_MVALFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATINI_1_30.value==this.w_MVDATINI)
      this.oPgFrm.Page1.oPag.oMVDATINI_1_30.value=this.w_MVDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNDOFIN_1_32.value==this.w_MVNDOFIN)
      this.oPgFrm.Page1.oPag.oMVNDOFIN_1_32.value=this.w_MVNDOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFFIN_1_34.value==this.w_MVALFFIN)
      this.oPgFrm.Page1.oPag.oMVALFFIN_1_34.value=this.w_MVALFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATFIN_1_36.value==this.w_MVDATFIN)
      this.oPgFrm.Page1.oPag.oMVDATFIN_1_36.value=this.w_MVDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODVAL_1_37.value==this.w_MVCODVAL)
      this.oPgFrm.Page1.oPag.oMVCODVAL_1_37.value=this.w_MVCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLPROV_1_39.RadioValue()==this.w_MVFLPROV)
      this.oPgFrm.Page1.oPag.oMVFLPROV_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCRIINI_1_43.value==this.w_MVCRIINI)
      this.oPgFrm.Page1.oPag.oMVCRIINI_1_43.value=this.w_MVCRIINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESINI_1_44.value==this.w_CADESINI)
      this.oPgFrm.Page1.oPag.oCADESINI_1_44.value=this.w_CADESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCRIFIN_1_46.value==this.w_MVCRIFIN)
      this.oPgFrm.Page1.oPag.oMVCRIFIN_1_46.value=this.w_MVCRIFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCARINI_1_47.value==this.w_MVCARINI)
      this.oPgFrm.Page1.oPag.oMVCARINI_1_47.value=this.w_MVCARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESINI_1_48.value==this.w_ARDESINI)
      this.oPgFrm.Page1.oPag.oARDESINI_1_48.value=this.w_ARDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESFIN_1_50.value==this.w_CADESFIN)
      this.oPgFrm.Page1.oPag.oCADESFIN_1_50.value=this.w_CADESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCARFIN_1_52.value==this.w_MVCARFIN)
      this.oPgFrm.Page1.oPag.oMVCARFIN_1_52.value=this.w_MVCARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESFIN_1_53.value==this.w_ARDESFIN)
      this.oPgFrm.Page1.oPag.oARDESFIN_1_53.value=this.w_ARDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVBUNINI_1_55.value==this.w_MVBUNINI)
      this.oPgFrm.Page1.oPag.oMVBUNINI_1_55.value=this.w_MVBUNINI
    endif
    if not(this.oPgFrm.Page1.oPag.oBUDESINI_1_56.value==this.w_BUDESINI)
      this.oPgFrm.Page1.oPag.oBUDESINI_1_56.value=this.w_BUDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCOMINI_1_60.value==this.w_MVCOMINI)
      this.oPgFrm.Page1.oPag.oMVCOMINI_1_60.value=this.w_MVCOMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCNCANINI_1_61.value==this.w_CNCANINI)
      this.oPgFrm.Page1.oPag.oCNCANINI_1_61.value=this.w_CNCANINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODMAG_1_66.value==this.w_MVCODMAG)
      this.oPgFrm.Page1.oPag.oMVCODMAG_1_66.value=this.w_MVCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTYPEAN_1_67.RadioValue()==this.w_FLTYPEAN)
      this.oPgFrm.Page1.oPag.oFLTYPEAN_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG_1_71.value==this.w_MGDESMAG)
      this.oPgFrm.Page1.oPag.oMGDESMAG_1_71.value=this.w_MGDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oVADESVAL_1_73.value==this.w_VADESVAL)
      this.oPgFrm.Page1.oPag.oVADESVAL_1_73.value=this.w_VADESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTYPROW_1_76.RadioValue()==this.w_FLTYPROW)
      this.oPgFrm.Page1.oPag.oFLTYPROW_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCLADOC_1_80.RadioValue()==this.w_MVCLADOC)
      this.oPgFrm.Page1.oPag.oMVCLADOC_1_80.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKUCA_1_83.RadioValue()==this.w_FLCHKUCA)
      this.oPgFrm.Page1.oPag.oFLCHKUCA_1_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLST_1_84.RadioValue()==this.w_FLCHKLST)
      this.oPgFrm.Page1.oPag.oFLCHKLST_1_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLFA_1_85.RadioValue()==this.w_FLCHKLFA)
      this.oPgFrm.Page1.oPag.oFLCHKLFA_1_85.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATRIF_1_90.value==this.w_FLDATRIF)
      this.oPgFrm.Page1.oPag.oFLDATRIF_1_90.value=this.w_FLDATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPERMAR_1_91.value==this.w_FLPERMAR)
      this.oPgFrm.Page1.oPag.oFLPERMAR_1_91.value=this.w_FLPERMAR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_TDFLVEAC=.w_MVFLVEAC OR .w_MVFLVEAC='E') AND (.w_TDCATDOC=.w_MVCLADOC OR .w_MVCLADOC='XX'))  and not(empty(.w_MVTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPDOC_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   not((.w_MVCONINI<=.w_MVCONFIN OR EMPTY(.w_MVCONFIN) ) AND (EMPTY(.w_TDFLINTE) OR .w_ANTIPINI=.w_TDFLINTE) AND (EMPTY(.w_ANTIPFIN) OR .w_ANTIPINI=.w_ANTIPFIN))  and ((EMPTY(.w_TDFLINTE) OR .w_TDFLINTE $ 'C-F' ) AND .w_TDRICNOM<>'A')  and not(empty(.w_MVCONINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario iniziale inesistente, maggiore del codice intestatario finale, incongruente con la causale selezionata o con l'intestatario finale")
          case   not((.w_MVCONINI<=.w_MVCONFIN OR EMPTY(.w_MVCONINI)) AND (EMPTY(.w_TDFLINTE) OR .w_ANTIPFIN=.w_TDFLINTE) AND (EMPTY(.w_ANTIPINI) OR .w_ANTIPINI=.w_ANTIPFIN))  and ((EMPTY(.w_TDFLINTE) OR .w_TDFLINTE $ 'C-F' ) AND .w_TDRICNOM<>'A')  and not(empty(.w_MVCONFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario finale inesistente, minore del codice intestatario iniziale, incongruente con la causale selezionata o con l'intestatario iniziale")
          case   not(.w_MVNREINI <= .w_MVNREFIN OR EMPTY(.w_MVNREFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNREINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero registrazione iniziale deve essere minore o uguale al numero registrazione finale")
          case   not(.w_MVREGINI<=.w_MVREGFIN OR EMPTY(.w_MVREGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVREGINI_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di registrazione iniziale deve essere minore o uguale della data di registrazione finale")
          case   not(.w_MVNREINI <= .w_MVNREFIN OR EMPTY(.w_MVNREINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNREFIN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero registrazione finale deve essere maggiore o uguale del numero registrazione iniziale")
          case   not(.w_MVREGINI<=.w_MVREGFIN OR EMPTY(.w_MVREGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVREGFIN_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di registrazione finale deve essere maggiore o uguale della data di registrazione iniziale")
          case   not(.w_MVNDOINI <= .w_MVNDOFIN OR EMPTY(.w_MVNDOFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNDOINI_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale deve essere minore o uguale al numero documento finale")
          case   not(.w_MVALFINI<=.w_MVALFFIN OR EMPTY(.w_MVALFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVALFINI_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'alfa iniziale deve essere minore o uguale dell'alfa finale")
          case   not(.w_MVDATINI<=.w_MVDATFIN OR EMPTY(.w_MVDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATINI_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento iniziale deve essere minore o uguale della data documento finale")
          case   not(.w_MVNDOINI <= .w_MVNDOFIN OR EMPTY(.w_MVNDOINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNDOFIN_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento finale deve essere maggiore o uguale del numero documento iniziale")
          case   not(.w_MVALFINI<=.w_MVALFFIN OR EMPTY(.w_MVALFINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVALFFIN_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'alfa finale deve essere maggiore o uguale dell'alfa iniziale")
          case   not(.w_MVDATINI<=.w_MVDATFIN OR EMPTY(.w_MVDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATFIN_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento finale deve essere maggiore o uguale della data documento iniziale")
          case   not(.w_MVCRIINI<=.w_MVCRIFIN OR EMPTY(.w_MVCRIFIN))  and (!EMPTY(.w_MVCRIINI) OR !EMPTY(.w_MVCRIFIN) OR (EMPTY(.w_MVCARINI) AND EMPTY(.w_MVCARFIN)))  and not(empty(.w_MVCRIINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCRIINI_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice di ricerca iniziale deve essere minore o uguale del codice di ricerca finale")
          case   not(.w_MVCRIINI<=.w_MVCRIFIN OR EMPTY(.w_MVCRIINI))  and (!EMPTY(.w_MVCRIINI) OR !EMPTY(.w_MVCRIFIN) OR (EMPTY(.w_MVCARINI) AND EMPTY(.w_MVCARFIN) ))  and not(empty(.w_MVCRIFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCRIFIN_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice di ricerca finale deve essere maggiore o uguale del codice di ricerca iniziale")
          case   not(.w_MVCARINI<=.w_MVCARFIN OR EMPTY(.w_MVCARFIN))  and (EMPTY(.w_MVCRIINI) AND EMPTY(.w_MVCRIFIN))  and not(empty(.w_MVCARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCARINI_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice articolo iniziale deve essre minore o uguale del codice articolo finale")
          case   not(.w_MVCARINI<=.w_MVCARFIN OR EMPTY(.w_MVCARINI))  and (EMPTY(.w_MVCRIINI) AND EMPTY(.w_MVCRIFIN))  and not(empty(.w_MVCARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCARFIN_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice articolo finale deve essre maggiore o uguale del codice articolo iniziale")
          case   not(.w_MVBUNINI <= .w_MVBUNFIN OR EMPTY(.w_MVBUNFIN))  and not(empty(.w_MVBUNINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVBUNINI_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La business unit iniziale deve essere minore o uguale della business unit finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVFLVEAC = this.w_MVFLVEAC
    this.o_MVTIPDOC = this.w_MVTIPDOC
    this.o_MVCRIINI = this.w_MVCRIINI
    this.o_MVCRIFIN = this.w_MVCRIFIN
    this.o_FLTYPROW = this.w_FLTYPROW
    this.o_MVCLADOC = this.w_MVCLADOC
    return

enddefine

* --- Define pages as container
define class tgsve_kasPag1 as StdContainer
  Width  = 785
  height = 559
  stdWidth  = 785
  stdheight = 559
  resizeXpos=482
  resizeYpos=456
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVTIPDOC_1_5 as StdField with uid="IBUWWNBEXU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC", cQueryName = "MVTIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Permette di impostare il tipo documento da analizzare",;
    HelpContextID = 245077751,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC"

  func oMVTIPDOC_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPDOC_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPDOC_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMVTIPDOC_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oTDDESDOC_1_7 as StdField with uid="FXMKDGTZQJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242264199,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=213, Top=37, InputMask=replicate('X',35)

  add object oMVCONINI_1_13 as StdField with uid="ODGEYXQTVF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MVCONINI", cQueryName = "MVCONINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario iniziale inesistente, maggiore del codice intestatario finale, incongruente con la causale selezionata o con l'intestatario finale",;
    ToolTipText = "Intestatario d'inizio selezione",;
    HelpContextID = 162965233,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=149, Top=61, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANCODICE", oKey_1_2="this.w_MVCONINI"

  func oMVCONINI_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((EMPTY(.w_TDFLINTE) OR .w_TDFLINTE $ 'C-F' ) AND .w_TDRICNOM<>'A')
    endwith
   endif
  endfunc

  func oMVCONINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONINI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONINI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CONTI','*','ANCODICE',cp_AbsName(this.parent,'oMVCONINI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Intestatari",'GSVE_KAS.CONTI_VZM',this.parent.oContained
  endproc

  add object oANDESINI_1_14 as StdField with uid="UJXILVRCZS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANDESINI", cQueryName = "ANDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 158375857,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=297, Top=61, InputMask=replicate('X',60)

  add object oMVCONFIN_1_15 as StdField with uid="AHLVILJCPX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MVCONFIN", cQueryName = "MVCONFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario finale inesistente, minore del codice intestatario iniziale, incongruente con la causale selezionata o con l'intestatario iniziale",;
    ToolTipText = "Intestatario di fine selezione",;
    HelpContextID = 55138580,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=149, Top=87, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANCODICE", oKey_1_2="this.w_MVCONFIN"

  func oMVCONFIN_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((EMPTY(.w_TDFLINTE) OR .w_TDFLINTE $ 'C-F' ) AND .w_TDRICNOM<>'A')
    endwith
   endif
  endfunc

  func oMVCONFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CONTI','*','ANCODICE',cp_AbsName(this.parent,'oMVCONFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Intestatari",'GSVE_KAS.CONTI_VZM',this.parent.oContained
  endproc

  add object oANDESFIN_1_16 as StdField with uid="PMFDLMVGXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ANDESFIN", cQueryName = "ANDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59727956,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=297, Top=87, InputMask=replicate('X',60)

  add object oMVNREINI_1_18 as StdField with uid="MRXYGQYKYA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MVNREINI", cQueryName = "MVNREINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero registrazione iniziale deve essere minore o uguale al numero registrazione finale",;
    ToolTipText = "Numero registrazione d'inizio selezione",;
    HelpContextID = 172160753,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=113

  func oMVNREINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNREINI <= .w_MVNREFIN OR EMPTY(.w_MVNREFIN))
    endwith
    return bRes
  endfunc

  add object oMVREGINI_1_20 as StdField with uid="AQQWKHQCED",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MVREGINI", cQueryName = "MVREGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di registrazione iniziale deve essere minore o uguale della data di registrazione finale",;
    ToolTipText = "Data registrazione d'inizio selezione",;
    HelpContextID = 170899185,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=397, Top=113

  func oMVREGINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVREGINI<=.w_MVREGFIN OR EMPTY(.w_MVREGFIN))
    endwith
    return bRes
  endfunc

  add object oMVNREFIN_1_22 as StdField with uid="ZHLTEPQQII",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MVNREFIN", cQueryName = "MVNREFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero registrazione finale deve essere maggiore o uguale del numero registrazione iniziale",;
    ToolTipText = "Numero registrazione di fine selezione",;
    HelpContextID = 45943060,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=137

  func oMVNREFIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNREINI <= .w_MVNREFIN OR EMPTY(.w_MVNREINI))
    endwith
    return bRes
  endfunc

  add object oMVREGFIN_1_24 as StdField with uid="AEMAVSJYQU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MVREGFIN", cQueryName = "MVREGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di registrazione finale deve essere maggiore o uguale della data di registrazione iniziale",;
    ToolTipText = "Data registrazione di fine selezione",;
    HelpContextID = 47204628,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=397, Top=137

  func oMVREGFIN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVREGINI<=.w_MVREGFIN OR EMPTY(.w_MVREGINI))
    endwith
    return bRes
  endfunc

  add object oMVNDOINI_1_26 as StdField with uid="KOJKASFNMQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MVNDOINI", cQueryName = "MVNDOINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale deve essere minore o uguale al numero documento finale",;
    ToolTipText = "Numero documento d'inizio selezione",;
    HelpContextID = 162592497,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=161

  func oMVNDOINI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNDOINI <= .w_MVNDOFIN OR EMPTY(.w_MVNDOFIN))
    endwith
    return bRes
  endfunc

  add object oMVALFINI_1_28 as StdField with uid="SNAAEZZLXJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVALFINI", cQueryName = "MVALFINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "L'alfa iniziale deve essere minore o uguale dell'alfa finale",;
    ToolTipText = "Alfa documento d'inizio selezione",;
    HelpContextID = 171558641,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=239, Top=161, InputMask=replicate('X',2)

  func oMVALFINI_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVALFINI<=.w_MVALFFIN OR EMPTY(.w_MVALFFIN))
    endwith
    return bRes
  endfunc

  add object oMVDATINI_1_30 as StdField with uid="RGJTMGLFNZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVDATINI", cQueryName = "MVDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento iniziale deve essere minore o uguale della data documento finale",;
    ToolTipText = "Data documento d'inizio selezione",;
    HelpContextID = 157587185,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=397, Top=161

  func oMVDATINI_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVDATINI<=.w_MVDATFIN OR EMPTY(.w_MVDATFIN))
    endwith
    return bRes
  endfunc

  add object oMVNDOFIN_1_32 as StdField with uid="AOOUDIBUTU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MVNDOFIN", cQueryName = "MVNDOFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento finale deve essere maggiore o uguale del numero documento iniziale",;
    HelpContextID = 55511316,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=185

  func oMVNDOFIN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNDOINI <= .w_MVNDOFIN OR EMPTY(.w_MVNDOINI))
    endwith
    return bRes
  endfunc

  add object oMVALFFIN_1_34 as StdField with uid="WUMEXENHTG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVALFFIN", cQueryName = "MVALFFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "L'alfa finale deve essere maggiore o uguale dell'alfa iniziale",;
    ToolTipText = "Alfa documento di fine selezione",;
    HelpContextID = 46545172,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=239, Top=185, InputMask=replicate('X',2)

  func oMVALFFIN_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVALFINI<=.w_MVALFFIN OR EMPTY(.w_MVALFINI))
    endwith
    return bRes
  endfunc

  add object oMVDATFIN_1_36 as StdField with uid="VVBRWOCSJR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVDATFIN", cQueryName = "MVDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento finale deve essere maggiore o uguale della data documento iniziale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 60516628,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=397, Top=185

  func oMVDATFIN_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVDATINI<=.w_MVDATFIN OR EMPTY(.w_MVDATINI))
    endwith
    return bRes
  endfunc

  add object oMVCODVAL_1_37 as StdField with uid="XMLIUPNRWF",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVCODVAL", cQueryName = "MVCODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Permette di filtrare i documenti nella valuta specificata",;
    HelpContextID = 44652818,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=149, Top=209, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_MVCODVAL"

  func oMVCODVAL_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODVAL_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODVAL_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMVCODVAL_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc


  add object oMVFLPROV_1_39 as StdCombo with uid="FWYBUKPVMW",rtseq=25,rtrep=.f.,left=606,top=11,width=124,height=22;
    , ToolTipText = "Permette di filtrare l'analisi sui documenti provvisori, confermati o entrambi gli stati";
    , HelpContextID = 10057444;
    , cFormVar="w_MVFLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMVFLPROV_1_39.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oMVFLPROV_1_39.GetRadio()
    this.Parent.oContained.w_MVFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oMVFLPROV_1_39.SetRadio()
    this.Parent.oContained.w_MVFLPROV=trim(this.Parent.oContained.w_MVFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLPROV=='N',1,;
      iif(this.Parent.oContained.w_MVFLPROV=='S',2,;
      iif(this.Parent.oContained.w_MVFLPROV=='E',3,;
      0)))
  endfunc


  add object oBtn_1_40 as StdButton with uid="ZMIZZNTUDJ",left=734, top=8, width=48,height=45,;
    CpPicture="BMP\requery.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la ricerca dei documenti corrispondenti ai criteri impostati";
    , HelpContextID = 44133398;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_41 as StdButton with uid="JWIBHYOYEX",left=734, top=512, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 125471418;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVCRIINI_1_43 as StdField with uid="KGGYFBWKIF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVCRIINI", cQueryName = "MVCRIINI",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice di ricerca iniziale deve essere minore o uguale del codice di ricerca finale",;
    ToolTipText = "Codice di ricerca d'inizio selezione",;
    HelpContextID = 168011505,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=149, Top=233, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MVCRIINI"

  func oMVCRIINI_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_MVCRIINI) OR !EMPTY(.w_MVCRIFIN) OR (EMPTY(.w_MVCARINI) AND EMPTY(.w_MVCARFIN)))
    endwith
   endif
  endfunc

  func oMVCRIINI_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCRIINI_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCRIINI_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMVCRIINI_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'',this.parent.oContained
  endproc

  add object oCADESINI_1_44 as StdField with uid="BZAZOVETJM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CADESINI", cQueryName = "CADESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158379153,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=437, Top=233, InputMask=replicate('X',40)

  add object oMVCRIFIN_1_46 as StdField with uid="RLBQZLXXBY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MVCRIFIN", cQueryName = "MVCRIFIN",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice di ricerca finale deve essere maggiore o uguale del codice di ricerca iniziale",;
    ToolTipText = "Codice di ricerca di fine selezione",;
    HelpContextID = 50092308,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=149, Top=257, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MVCRIFIN"

  func oMVCRIFIN_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_MVCRIINI) OR !EMPTY(.w_MVCRIFIN) OR (EMPTY(.w_MVCARINI) AND EMPTY(.w_MVCARFIN) ))
    endwith
   endif
  endfunc

  func oMVCRIFIN_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCRIFIN_1_46.ecpDrop(oSource)
    this.Parent.oContained.link_1_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCRIFIN_1_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMVCRIFIN_1_46'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'',this.parent.oContained
  endproc

  add object oMVCARINI_1_47 as StdField with uid="UYCTMQCIMF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MVCARINI", cQueryName = "MVCARINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice articolo iniziale deve essre minore o uguale del codice articolo finale",;
    ToolTipText = "Codice articolo d'inizio selezione",;
    HelpContextID = 159688433,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=149, Top=282, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MVCARINI"

  func oMVCARINI_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MVCRIINI) AND EMPTY(.w_MVCRIFIN))
    endwith
   endif
  endfunc

  func oMVCARINI_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCARINI_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCARINI_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oMVCARINI_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/Servizi",'',this.parent.oContained
  endproc

  add object oARDESINI_1_48 as StdField with uid="LMZEDPABIN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ARDESINI", cQueryName = "ARDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158374833,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=327, Top=282, InputMask=replicate('X',40)

  add object oCADESFIN_1_50 as StdField with uid="DZZPGAETLA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CADESFIN", cQueryName = "CADESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59724660,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=437, Top=257, InputMask=replicate('X',40)

  add object oMVCARFIN_1_52 as StdField with uid="EKXZXSTHYX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MVCARFIN", cQueryName = "MVCARFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice articolo finale deve essre maggiore o uguale del codice articolo iniziale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 58415380,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=149, Top=307, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MVCARFIN"

  func oMVCARFIN_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MVCRIINI) AND EMPTY(.w_MVCRIFIN))
    endwith
   endif
  endfunc

  func oMVCARFIN_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCARFIN_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCARFIN_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oMVCARFIN_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/Servizi",'',this.parent.oContained
  endproc

  add object oARDESFIN_1_53 as StdField with uid="CUYXJHETXN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ARDESFIN", cQueryName = "ARDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59728980,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=327, Top=307, InputMask=replicate('X',40)

  add object oMVBUNINI_1_55 as StdField with uid="HFMFWHYLWK",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MVBUNINI", cQueryName = "MVBUNINI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "La business unit iniziale deve essere minore o uguale della business unit finale",;
    ToolTipText = "Permette di filtrare le righe con la business unit impostata",;
    HelpContextID = 162576113,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=332, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_MVBUNINI"

  func oMVBUNINI_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVBUNINI_1_55.ecpDrop(oSource)
    this.Parent.oContained.link_1_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVBUNINI_1_55.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oMVBUNINI_1_55'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Business unit",'',this.parent.oContained
  endproc

  add object oBUDESINI_1_56 as StdField with uid="CTFPTZRIQO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_BUDESINI", cQueryName = "BUDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158374049,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=199, Top=332, InputMask=replicate('X',40)

  add object oMVCOMINI_1_60 as StdField with uid="YOCQZGUTTU",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MVCOMINI", cQueryName = "MVCOMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Permette di filtrare le righe con la commessa impostata",;
    HelpContextID = 164013809,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=149, Top=356, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MVCOMINI"

  func oMVCOMINI_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCOMINI_1_60.ecpDrop(oSource)
    this.Parent.oContained.link_1_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCOMINI_1_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMVCOMINI_1_60'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCNCANINI_1_61 as StdField with uid="AGNOZLDMSI",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CNCANINI", cQueryName = "CNCANINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 163884945,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=282, Top=356, InputMask=replicate('X',100)

  add object oMVCODMAG_1_66 as StdField with uid="VDJUPCTRIW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_MVCODMAG", cQueryName = "MVCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino su cui filtrare i documenti",;
    HelpContextID = 106342131,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=380, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAG"

  func oMVCODMAG_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_66('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAG_1_66.ecpDrop(oSource)
    this.Parent.oContained.link_1_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAG_1_66.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAG_1_66'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc


  add object oFLTYPEAN_1_67 as StdCombo with uid="EDPOFYIGDM",rtseq=44,rtrep=.f.,left=149,top=404,width=171,height=22;
    , ToolTipText = "Esegue l'analisi dei prezzi alla data del documento, alla data specificata o alla data standard in base al tipo di controllo impostato su ciascun articolo";
    , HelpContextID = 227254620;
    , cFormVar="w_FLTYPEAN",RowSource=""+"Data documento,"+"Data specifica,"+"Data standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTYPEAN_1_67.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLTYPEAN_1_67.GetRadio()
    this.Parent.oContained.w_FLTYPEAN = this.RadioValue()
    return .t.
  endfunc

  func oFLTYPEAN_1_67.SetRadio()
    this.Parent.oContained.w_FLTYPEAN=trim(this.Parent.oContained.w_FLTYPEAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLTYPEAN=='D',1,;
      iif(this.Parent.oContained.w_FLTYPEAN=='O',2,;
      iif(this.Parent.oContained.w_FLTYPEAN=='S',3,;
      0)))
  endfunc

  add object oMGDESMAG_1_71 as StdField with uid="XHPFHOWWJZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MGDESMAG", cQueryName = "MGDESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 91268595,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=213, Top=380, InputMask=replicate('X',30)

  add object oVADESVAL_1_73 as StdField with uid="QSGFQDFFHZ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_VADESVAL", cQueryName = "VADESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59724962,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=205, Top=209, InputMask=replicate('X',35)


  add object oFLTYPROW_1_76 as StdCombo with uid="JVIYLQKXWL",rtseq=49,rtrep=.f.,left=149,top=431,width=171,height=22;
    , ToolTipText = "Permette di scegliere se visualizzare le sole righe difformi in base alla percentuale indicata o tutte le righe con colorazione delle sole righe con margine inferiore o uguale allo zero";
    , HelpContextID = 9150803;
    , cFormVar="w_FLTYPROW",RowSource=""+"Tutte,"+"Solo margine minore del,"+"Solo margine maggiore del", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTYPROW_1_76.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'I',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLTYPROW_1_76.GetRadio()
    this.Parent.oContained.w_FLTYPROW = this.RadioValue()
    return .t.
  endfunc

  func oFLTYPROW_1_76.SetRadio()
    this.Parent.oContained.w_FLTYPROW=trim(this.Parent.oContained.w_FLTYPROW)
    this.value = ;
      iif(this.Parent.oContained.w_FLTYPROW=='T',1,;
      iif(this.Parent.oContained.w_FLTYPROW=='I',2,;
      iif(this.Parent.oContained.w_FLTYPROW=='S',3,;
      0)))
  endfunc


  add object oMVCLADOC_1_80 as StdCombo with uid="DRGEWOFEOF",rtseq=52,rtrep=.f.,left=149,top=11,width=145,height=22;
    , ToolTipText = "Permette di selezionare la categoria documento da analizzare";
    , HelpContextID = 260679415;
    , cFormVar="w_MVCLADOC",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMVCLADOC_1_80.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'OR',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'OP',;
    space(2)))))))))
  endfunc
  func oMVCLADOC_1_80.GetRadio()
    this.Parent.oContained.w_MVCLADOC = this.RadioValue()
    return .t.
  endfunc

  func oMVCLADOC_1_80.SetRadio()
    this.Parent.oContained.w_MVCLADOC=trim(this.Parent.oContained.w_MVCLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_MVCLADOC=='XX',1,;
      iif(this.Parent.oContained.w_MVCLADOC=='DI',2,;
      iif(this.Parent.oContained.w_MVCLADOC=='OR',3,;
      iif(this.Parent.oContained.w_MVCLADOC=='DT',4,;
      iif(this.Parent.oContained.w_MVCLADOC=='FA',5,;
      iif(this.Parent.oContained.w_MVCLADOC=='NC',6,;
      iif(this.Parent.oContained.w_MVCLADOC=='OP',7,;
      0)))))))
  endfunc

  add object oFLCHKUCA_1_83 as StdCheck with uid="LHWTHFBLWP",rtseq=90,rtrep=.f.,left=192, top=479, caption="U.C.A.",;
    ToolTipText = "Se attivo: analizza le righe dei documenti di articoli con controllo impostato su ultimo costo d'acquisto",;
    HelpContextID = 34754199,;
    cFormVar="w_FLCHKUCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKUCA_1_83.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCHKUCA_1_83.GetRadio()
    this.Parent.oContained.w_FLCHKUCA = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKUCA_1_83.SetRadio()
    this.Parent.oContained.w_FLCHKUCA=trim(this.Parent.oContained.w_FLCHKUCA)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKUCA=='S',1,;
      0)
  endfunc

  add object oFLCHKLST_1_84 as StdCheck with uid="QKKDZJKDWC",rtseq=91,rtrep=.f.,left=192, top=504, caption="Listino statistico",;
    ToolTipText = "Se attivo: analizza le righe dei documenti di articoli con controllo impostato su listino statistico",;
    HelpContextID = 152194730,;
    cFormVar="w_FLCHKLST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLST_1_84.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCHKLST_1_84.GetRadio()
    this.Parent.oContained.w_FLCHKLST = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLST_1_84.SetRadio()
    this.Parent.oContained.w_FLCHKLST=trim(this.Parent.oContained.w_FLCHKLST)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLST=='S',1,;
      0)
  endfunc

  add object oFLCHKLFA_1_85 as StdCheck with uid="ZCIIEOKYRJ",rtseq=92,rtrep=.f.,left=192, top=529, caption="Listino fornitore abituale",;
    ToolTipText = "Se attivo: analizza le righe dei documenti di articoli con controllo impostato su listino fornitore abituale",;
    HelpContextID = 152194711,;
    cFormVar="w_FLCHKLFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLFA_1_85.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCHKLFA_1_85.GetRadio()
    this.Parent.oContained.w_FLCHKLFA = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLFA_1_85.SetRadio()
    this.Parent.oContained.w_FLCHKLFA=trim(this.Parent.oContained.w_FLCHKLFA)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLFA=='S',1,;
      0)
  endfunc

  add object oFLDATRIF_1_90 as StdField with uid="WQCYCSKMQJ",rtseq=96,rtrep=.f.,;
    cFormVar = "w_FLDATRIF", cQueryName = "FLDATRIF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di controllo ",;
    HelpContextID = 261840540,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=330, Top=404

  func oFLDATRIF_1_90.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEAN<>'O')
    endwith
  endfunc

  add object oFLPERMAR_1_91 as StdField with uid="QWQFBMQXLG",rtseq=97,rtrep=.f.,;
    cFormVar = "w_FLPERMAR", cQueryName = "FLPERMAR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di controllo",;
    HelpContextID = 92266840,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=330, Top=431

  func oFLPERMAR_1_91.mHide()
    with this.Parent.oContained
      return (.w_FLTYPROW = 'T')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="QVDZSRHBLS",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=140, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BBEZULXQGW",Visible=.t., Left=7, Top=62,;
    Alignment=1, Width=137, Height=18,;
    Caption="Da intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="TKFRFLMFMG",Visible=.t., Left=7, Top=88,;
    Alignment=1, Width=137, Height=18,;
    Caption="A intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YWHFJPYFGY",Visible=.t., Left=5, Top=114,;
    Alignment=1, Width=139, Height=18,;
    Caption="Da numero reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GMCTBOXLPR",Visible=.t., Left=276, Top=114,;
    Alignment=1, Width=117, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CKDVMKIAAC",Visible=.t., Left=5, Top=138,;
    Alignment=1, Width=139, Height=18,;
    Caption="A numero reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QLQQGYQVDC",Visible=.t., Left=276, Top=138,;
    Alignment=1, Width=117, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RBAXFOSJZM",Visible=.t., Left=5, Top=162,;
    Alignment=1, Width=139, Height=18,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="PYVEZFPUJT",Visible=.t., Left=226, Top=163,;
    Alignment=2, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KBZHQWVALA",Visible=.t., Left=276, Top=162,;
    Alignment=1, Width=117, Height=18,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ACQXLWXNAD",Visible=.t., Left=5, Top=186,;
    Alignment=1, Width=139, Height=18,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IGIXLSAWPG",Visible=.t., Left=226, Top=187,;
    Alignment=2, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GWMPBESBRG",Visible=.t., Left=276, Top=186,;
    Alignment=1, Width=117, Height=18,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GDCGRIRWFW",Visible=.t., Left=462, Top=13,;
    Alignment=1, Width=139, Height=18,;
    Caption="Stato documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="KZQTAVSGAD",Visible=.t., Left=5, Top=235,;
    Alignment=1, Width=139, Height=18,;
    Caption="Da codice di ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="OUHUHMCNHI",Visible=.t., Left=5, Top=284,;
    Alignment=1, Width=139, Height=18,;
    Caption="Da cod. articolo/servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KOQIOZXBNF",Visible=.t., Left=5, Top=259,;
    Alignment=1, Width=139, Height=18,;
    Caption="A codice di ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="GUIDAPYIRR",Visible=.t., Left=5, Top=309,;
    Alignment=1, Width=139, Height=18,;
    Caption="A cod. articolo/servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="TXGPMSLEYU",Visible=.t., Left=5, Top=333,;
    Alignment=1, Width=139, Height=18,;
    Caption="Business unit:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="RQFJPONGBF",Visible=.t., Left=5, Top=357,;
    Alignment=1, Width=139, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="TVZDDQGTKI",Visible=.t., Left=5, Top=406,;
    Alignment=1, Width=139, Height=18,;
    Caption="Analisi prezzi a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="PWKFPZRJUU",Visible=.t., Left=5, Top=382,;
    Alignment=1, Width=139, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="DXMQNVTIFM",Visible=.t., Left=5, Top=211,;
    Alignment=1, Width=139, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="AAHDXTYGTE",Visible=.t., Left=5, Top=434,;
    Alignment=1, Width=139, Height=18,;
    Caption="Visualizza righe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="MBEYBTVOLX",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=140, Height=18,;
    Caption="Categoria documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="FTYGNOMLLR",Visible=.t., Left=17, Top=459,;
    Alignment=1, Width=256, Height=18,;
    Caption="Analizza articoli con controllo su"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="NEINYTZLTW",Visible=.t., Left=396, Top=433,;
    Alignment=0, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (.w_FLTYPROW = 'T')
    endwith
  endfunc
enddefine
define class tgsve_kasPag2 as StdContainer
  Width  = 785
  height = 559
  stdWidth  = 785
  stdheight = 559
  resizeXpos=462
  resizeYpos=388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_1 as StdButton with uid="RCGITVSKZF",left=734, top=8, width=48,height=45,;
    CpPicture="BMP\requery.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per avviare la ricerca dei documenti corrispondenti ai criteri impostati";
    , HelpContextID = 44133398;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_1.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMMAST as cp_zoombox with uid="KDUKRVLEYW",left=6, top=54, width=776,height=204,;
    caption='ZOOMMAST',;
   bGlobalFont=.t.,;
    cZoomFile="GSVEMKAS",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="DOC_MAST",DisableSelMenu=.t.,bRetriveAllRows=.f.,bNoZoomGridShape=.f.,;
    cEvent = "AggZoom",;
    nPag=2;
    , HelpContextID = 238555882


  add object ZOOMDETT as cp_zoombox with uid="JBDFZJSODN",left=6, top=260, width=776,height=242,;
    caption='ZOOMDETT',;
   bGlobalFont=.t.,;
    cZoomFile="GSVEDKAS",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",cTable="DOC_DETT",DisableSelMenu=.t.,bRetriveAllRows=.f.,bNoZoomGridShape=.f.,;
    cEvent = "AggZoom",;
    nPag=2;
    , HelpContextID = 27792106


  add object oBtn_2_5 as StdButton with uid="XEHQUIVSCE",left=734, top=512, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 125471418;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_13 as StdButton with uid="RYSBPXMQQB",left=681, top=512, width=48,height=45,;
    CpPicture="BMP\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Apre il documento evidenziando la riga selezionata";
    , HelpContextID = 58300291;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSVE_BKA(this.Parent.oContained,"APRIRIGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_MVSERATT))
      endwith
    endif
  endfunc

  add object oStr_2_6 as StdString with uid="CXONKRWFKM",Visible=.t., Left=8, Top=513,;
    Alignment=0, Width=346, Height=18,;
    Caption="Le righe di colore rosso hanno il prezzo difforme"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_6.mHide()
    with this.Parent.oContained
      return (.w_FLTYPROW<>'T')
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="QDSRESDFGU",Visible=.t., Left=8, Top=531,;
    Alignment=0, Width=423, Height=18,;
    Caption="Sfondo azzurro righe con prezzo difforme e provvigioni applicate"    , ForeColor = 0, BackColor = RGB(120,200,255), BackStyle=1;
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kas','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
