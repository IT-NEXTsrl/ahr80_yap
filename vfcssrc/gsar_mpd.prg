* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mpd                                                        *
*              Punti di accesso per utente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-29                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mpd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mpd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mpd")
  return

* --- Class definition
define class tgsar_mpd as StdPCForm
  Width  = 631
  Height = 483
  Top    = 42
  Left   = 51
  cComment = "Punti di accesso per utente"
  cPrg = "gsar_mpd"
  HelpContextID=147421033
  add object cnt as tcgsar_mpd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mpd as PCContext
  w_PDCODPER = space(5)
  w_DPNOME = space(50)
  w_DPCOGNOM = space(50)
  w_PDFLPREF = space(1)
  w_PDCODPDA = space(5)
  w_PRDESCRI = space(80)
  w_PRINDIRI = space(254)
  w_Attivo = 0
  w_PDUFFREG = space(15)
  w_UFLOCALI = space(50)
  w_UF__NOME = space(80)
  w_UFPRORIF = space(2)
  w_FiltDistretto                = space(4)
  w_TIPUFF = space(1)
  w_DesUff = space(150)
  w_NomeUte = space(150)
  w_Totale = 0
  w_FiltLocalita = space(4)
  w_RowStatus = space(1)
  proc Save(i_oFrom)
    this.w_PDCODPER = i_oFrom.w_PDCODPER
    this.w_DPNOME = i_oFrom.w_DPNOME
    this.w_DPCOGNOM = i_oFrom.w_DPCOGNOM
    this.w_PDFLPREF = i_oFrom.w_PDFLPREF
    this.w_PDCODPDA = i_oFrom.w_PDCODPDA
    this.w_PRDESCRI = i_oFrom.w_PRDESCRI
    this.w_PRINDIRI = i_oFrom.w_PRINDIRI
    this.w_Attivo = i_oFrom.w_Attivo
    this.w_PDUFFREG = i_oFrom.w_PDUFFREG
    this.w_UFLOCALI = i_oFrom.w_UFLOCALI
    this.w_UF__NOME = i_oFrom.w_UF__NOME
    this.w_UFPRORIF = i_oFrom.w_UFPRORIF
    this.w_FiltDistretto                = i_oFrom.w_FiltDistretto               
    this.w_TIPUFF = i_oFrom.w_TIPUFF
    this.w_DesUff = i_oFrom.w_DesUff
    this.w_NomeUte = i_oFrom.w_NomeUte
    this.w_Totale = i_oFrom.w_Totale
    this.w_FiltLocalita = i_oFrom.w_FiltLocalita
    this.w_RowStatus = i_oFrom.w_RowStatus
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PDCODPER = this.w_PDCODPER
    i_oTo.w_DPNOME = this.w_DPNOME
    i_oTo.w_DPCOGNOM = this.w_DPCOGNOM
    i_oTo.w_PDFLPREF = this.w_PDFLPREF
    i_oTo.w_PDCODPDA = this.w_PDCODPDA
    i_oTo.w_PRDESCRI = this.w_PRDESCRI
    i_oTo.w_PRINDIRI = this.w_PRINDIRI
    i_oTo.w_Attivo = this.w_Attivo
    i_oTo.w_PDUFFREG = this.w_PDUFFREG
    i_oTo.w_UFLOCALI = this.w_UFLOCALI
    i_oTo.w_UF__NOME = this.w_UF__NOME
    i_oTo.w_UFPRORIF = this.w_UFPRORIF
    i_oTo.w_FiltDistretto                = this.w_FiltDistretto               
    i_oTo.w_TIPUFF = this.w_TIPUFF
    i_oTo.w_DesUff = this.w_DesUff
    i_oTo.w_NomeUte = this.w_NomeUte
    i_oTo.w_Totale = this.w_Totale
    i_oTo.w_FiltLocalita = this.w_FiltLocalita
    i_oTo.w_RowStatus = this.w_RowStatus
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mpd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 631
  Height = 483
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-02"
  HelpContextID=147421033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  UTE_PDAM_IDX = 0
  PUN_ACCE_IDX = 0
  UFF_GIUD_IDX = 0
  cFile = "UTE_PDAM"
  cKeySelect = "PDCODPER"
  cKeyWhere  = "PDCODPER=this.w_PDCODPER"
  cKeyDetail  = "PDCODPER=this.w_PDCODPER and PDCODPDA=this.w_PDCODPDA"
  cKeyWhereODBC = '"PDCODPER="+cp_ToStrODBC(this.w_PDCODPER)';

  cKeyDetailWhereODBC = '"PDCODPER="+cp_ToStrODBC(this.w_PDCODPER)';
      +'+" and PDCODPDA="+cp_ToStrODBC(this.w_PDCODPDA)';

  cKeyWhereODBCqualified = '"UTE_PDAM.PDCODPER="+cp_ToStrODBC(this.w_PDCODPER)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mpd"
  cComment = "Punti di accesso per utente"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PDCODPER = space(5)
  w_DPNOME = space(50)
  w_DPCOGNOM = space(50)
  w_PDFLPREF = space(1)
  w_PDCODPDA = space(5)
  w_PRDESCRI = space(80)
  w_PRINDIRI = space(254)
  w_Attivo = 0
  w_PDUFFREG = space(15)
  w_UFLOCALI = space(50)
  w_UF__NOME = space(80)
  w_UFPRORIF = space(2)
  w_FiltDistretto                = space(4)
  w_TIPUFF = space(1)
  w_DesUff = space(150)
  w_NomeUte = space(150)
  w_Totale = 0
  w_FiltLocalita = space(4)
  w_RowStatus = space(1)

  * --- Children pointers
  GSAR_MUG = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAR_MUG additive
    with this
      .Pages(1).addobject("oPag","tgsar_mpdPag1","gsar_mpd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAR_MUG
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PUN_ACCE'
    this.cWorkTables[2]='UFF_GIUD'
    this.cWorkTables[3]='UTE_PDAM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UTE_PDAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UTE_PDAM_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MUG = CREATEOBJECT('stdDynamicChild',this,'GSAR_MUG',this.oPgFrm.Page1.oPag.oLinkPC_2_7)
    this.GSAR_MUG.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsar_mpd'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSAR_MUG)
      this.GSAR_MUG.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSAR_MUG.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSAR_MUG.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MUG)
      this.GSAR_MUG.DestroyChildrenChain()
      this.GSAR_MUG=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_7')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MUG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MUG.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MUG.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MUG.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PDCODPER,"UFCODPER";
             ,.w_PDCODPDA,"UFCODPDA";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from UTE_PDAM where PDCODPER=KeySet.PDCODPER
    *                            and PDCODPDA=KeySet.PDCODPDA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2],this.bLoadRecFilter,this.UTE_PDAM_IDX,"gsar_mpd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UTE_PDAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UTE_PDAM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UTE_PDAM '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PDCODPER',this.w_PDCODPER  )
      select * from (i_cTable) UTE_PDAM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DPNOME = space(50)
        .w_DPCOGNOM = space(50)
        .w_FiltDistretto                = space(4)
        .w_TIPUFF = space(1)
        .w_Totale = 0
        .w_FiltLocalita = space(4)
        .w_PDCODPER = NVL(PDCODPER,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_NomeUte = ALLTRIM(.w_DPCOGNOM)+' '+ALLTRIM(.w_DPNOME)
        cp_LoadRecExtFlds(this,'UTE_PDAM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_Totale = 0
      scan
        with this
          .w_PRDESCRI = space(80)
          .w_PRINDIRI = space(254)
          .w_UFLOCALI = space(50)
          .w_UF__NOME = space(80)
          .w_UFPRORIF = space(2)
          .w_PDFLPREF = NVL(PDFLPREF,space(1))
          .w_PDCODPDA = NVL(PDCODPDA,space(5))
          if link_2_2_joined
            this.w_PDCODPDA = NVL(PRCODICE202,NVL(this.w_PDCODPDA,space(5)))
            this.w_PRDESCRI = NVL(PRDESCRI202,space(80))
            this.w_PRINDIRI = NVL(PRINDIRI202,space(254))
          else
          .link_2_2('Load')
          endif
        .w_Attivo = IIF(.w_PDFLPREF='S',1,0)
          .w_PDUFFREG = NVL(PDUFFREG,space(15))
          if link_2_6_joined
            this.w_PDUFFREG = NVL(UFCODICE206,NVL(this.w_PDUFFREG,space(15)))
            this.w_UF__NOME = NVL(UF__NOME206,space(80))
            this.w_UFLOCALI = NVL(UFLOCALI206,space(50))
            this.w_UFPRORIF = NVL(UFPRORIF206,space(2))
          else
          .link_2_6('Load')
          endif
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
        .w_RowStatus = this.RowStatus()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_Totale = .w_Totale+.w_ATTIVO
          replace PDCODPDA with .w_PDCODPDA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_NomeUte = ALLTRIM(.w_DPCOGNOM)+' '+ALLTRIM(.w_DPNOME)
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PDCODPER=space(5)
      .w_DPNOME=space(50)
      .w_DPCOGNOM=space(50)
      .w_PDFLPREF=space(1)
      .w_PDCODPDA=space(5)
      .w_PRDESCRI=space(80)
      .w_PRINDIRI=space(254)
      .w_Attivo=0
      .w_PDUFFREG=space(15)
      .w_UFLOCALI=space(50)
      .w_UF__NOME=space(80)
      .w_UFPRORIF=space(2)
      .w_FiltDistretto               =space(4)
      .w_TIPUFF=space(1)
      .w_DesUff=space(150)
      .w_NomeUte=space(150)
      .w_Totale=0
      .w_FiltLocalita=space(4)
      .w_RowStatus=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,3,.f.)
        .w_PDFLPREF = 'N'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PDCODPDA))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,7,.f.)
        .w_Attivo = IIF(.w_PDFLPREF='S',1,0)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PDUFFREG))
         .link_2_6('Full')
        endif
        .DoRTCalc(10,14,.f.)
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
        .w_NomeUte = ALLTRIM(.w_DPCOGNOM)+' '+ALLTRIM(.w_DPNOME)
        .DoRTCalc(17,18,.f.)
        .w_RowStatus = this.RowStatus()
      endif
    endwith
    cp_BlankRecExtFlds(this,'UTE_PDAM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPDUFFREG_2_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSAR_MUG.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'UTE_PDAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MUG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODPER,"PDCODPER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PDFLPREF N(3);
      ,t_PDCODPDA C(5);
      ,t_PRDESCRI C(80);
      ,t_PRINDIRI C(254);
      ,t_PDUFFREG C(15);
      ,t_DesUff C(150);
      ,PDCODPDA C(5);
      ,t_Attivo N(10);
      ,t_UFLOCALI C(50);
      ,t_UF__NOME C(80);
      ,t_UFPRORIF C(2);
      ,t_RowStatus C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mpdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.controlsource=this.cTrsName+'.t_PDFLPREF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPDCODPDA_2_2.controlsource=this.cTrsName+'.t_PDCODPDA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESCRI_2_3.controlsource=this.cTrsName+'.t_PRDESCRI'
    this.oPgFRm.Page1.oPag.oPRINDIRI_2_4.controlsource=this.cTrsName+'.t_PRINDIRI'
    this.oPgFRm.Page1.oPag.oPDUFFREG_2_6.controlsource=this.cTrsName+'.t_PDUFFREG'
    this.oPgFRm.Page1.oPag.oDesUff_2_14.controlsource=this.cTrsName+'.t_DesUff'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(73)
    this.AddVLine(556)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2])
      *
      * insert into UTE_PDAM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UTE_PDAM')
        i_extval=cp_InsertValODBCExtFlds(this,'UTE_PDAM')
        i_cFldBody=" "+;
                  "(PDCODPER,PDFLPREF,PDCODPDA,PDUFFREG,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PDCODPER)+","+cp_ToStrODBC(this.w_PDFLPREF)+","+cp_ToStrODBCNull(this.w_PDCODPDA)+","+cp_ToStrODBCNull(this.w_PDUFFREG)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UTE_PDAM')
        i_extval=cp_InsertValVFPExtFlds(this,'UTE_PDAM')
        cp_CheckDeletedKey(i_cTable,0,'PDCODPER',this.w_PDCODPER,'PDCODPDA',this.w_PDCODPDA)
        INSERT INTO (i_cTable) (;
                   PDCODPER;
                  ,PDFLPREF;
                  ,PDCODPDA;
                  ,PDUFFREG;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PDCODPER;
                  ,this.w_PDFLPREF;
                  ,this.w_PDCODPDA;
                  ,this.w_PDUFFREG;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_PDAM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PDCODPDA="+cp_ToStrODBC(&i_TN.->PDCODPDA)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_PDAM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PDCODPDA=&i_TN.->PDCODPDA;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSAR_MUG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PDCODPER,"UFCODPER";
                     ,this.w_PDCODPDA,"UFCODPDA";
                     )
              this.GSAR_MUG.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PDCODPDA="+cp_ToStrODBC(&i_TN.->PDCODPDA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PDCODPDA=&i_TN.->PDCODPDA;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PDCODPDA with this.w_PDCODPDA
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update UTE_PDAM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_PDAM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PDFLPREF="+cp_ToStrODBC(this.w_PDFLPREF)+;
                     ",PDUFFREG="+cp_ToStrODBCNull(this.w_PDUFFREG)+;
                     ",PDCODPDA="+cp_ToStrODBC(this.w_PDCODPDA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PDCODPDA="+cp_ToStrODBC(PDCODPDA)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_PDAM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PDFLPREF=this.w_PDFLPREF;
                     ,PDUFFREG=this.w_PDUFFREG;
                     ,PDCODPDA=this.w_PDCODPDA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PDCODPDA=&i_TN.->PDCODPDA;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSAR_MUG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PDCODPER,"UFCODPER";
               ,this.w_PDCODPDA,"UFCODPDA";
               )
          this.GSAR_MUG.mReplace()
          this.GSAR_MUG.bSaveContext=.f.
        endif
      endscan
     this.GSAR_MUG.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAR_MUG : Deleting
        this.GSAR_MUG.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PDCODPER,"UFCODPER";
               ,this.w_PDCODPDA,"UFCODPDA";
               )
        this.GSAR_MUG.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete UTE_PDAM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PDCODPDA="+cp_ToStrODBC(&i_TN.->PDCODPDA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PDCODPDA=&i_TN.->PDCODPDA;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UTE_PDAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAM_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,7,.t.)
          .w_Totale = .w_Totale-.w_attivo
          .w_Attivo = IIF(.w_PDFLPREF='S',1,0)
          .w_Totale = .w_Totale+.w_attivo
        .DoRTCalc(9,14,.t.)
          .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
          .w_NomeUte = ALLTRIM(.w_DPCOGNOM)+' '+ALLTRIM(.w_DPNOME)
        .DoRTCalc(17,18,.t.)
          .w_RowStatus = this.RowStatus()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_Attivo with this.w_Attivo
      replace t_UFLOCALI with this.w_UFLOCALI
      replace t_UF__NOME with this.w_UF__NOME
      replace t_UFPRORIF with this.w_UFPRORIF
      replace t_RowStatus with this.w_RowStatus
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPDCODPDA_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPDCODPDA_2_2.mCond()
    this.GSAR_MUG.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PDCODPDA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUN_ACCE_IDX,3]
    i_lTable = "PUN_ACCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2], .t., this.PUN_ACCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODPDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAL_APR',True,'PUN_ACCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODICE like "+cp_ToStrODBC(trim(this.w_PDCODPDA)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODICE',trim(this.w_PDCODPDA))
          select PRCODICE,PRDESCRI,PRINDIRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODPDA)==trim(_Link_.PRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStrODBC(trim(this.w_PDCODPDA)+"%");

            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStr(trim(this.w_PDCODPDA)+"%");

            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODPDA) and !this.bDontReportError
            deferred_cp_zoom('PUN_ACCE','*','PRCODICE',cp_AbsName(oSource.parent,'oPDCODPDA_2_2'),i_cWhere,'GSAL_APR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',oSource.xKey(1))
            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODPDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODICE,PRDESCRI,PRINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODICE="+cp_ToStrODBC(this.w_PDCODPDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODICE',this.w_PDCODPDA)
            select PRCODICE,PRDESCRI,PRINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODPDA = NVL(_Link_.PRCODICE,space(5))
      this.w_PRDESCRI = NVL(_Link_.PRDESCRI,space(80))
      this.w_PRINDIRI = NVL(_Link_.PRINDIRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODPDA = space(5)
      endif
      this.w_PRDESCRI = space(80)
      this.w_PRINDIRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])+'\'+cp_ToStr(_Link_.PRCODICE,1)
      cp_ShowWarn(i_cKey,this.PUN_ACCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODPDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PUN_ACCE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PUN_ACCE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.PRCODICE as PRCODICE202"+ ",link_2_2.PRDESCRI as PRDESCRI202"+ ",link_2_2.PRINDIRI as PRINDIRI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on UTE_PDAM.PDCODPDA=link_2_2.PRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and UTE_PDAM.PDCODPDA=link_2_2.PRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDUFFREG
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UFF_GIUD_IDX,3]
    i_lTable = "UFF_GIUD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2], .t., this.UFF_GIUD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDUFFREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UFF_GIUD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_PDUFFREG)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_PDUFFREG))
          select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDUFFREG)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UF__NOME like "+cp_ToStrODBC(trim(this.w_PDUFFREG)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UF__NOME like "+cp_ToStr(trim(this.w_PDUFFREG)+"%");

            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UFLOCALI like "+cp_ToStrODBC(trim(this.w_PDUFFREG)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UFLOCALI like "+cp_ToStr(trim(this.w_PDUFFREG)+"%");

            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDUFFREG) and !this.bDontReportError
            deferred_cp_zoom('UFF_GIUD','*','UFCODICE',cp_AbsName(oSource.parent,'oPDUFFREG_2_6'),i_cWhere,'',"Codici uffici giudiziari",'GSAL_KWS.UFF_GIUD_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDUFFREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_PDUFFREG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_PDUFFREG)
            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDUFFREG = NVL(_Link_.UFCODICE,space(15))
      this.w_UF__NOME = NVL(_Link_.UF__NOME,space(80))
      this.w_UFLOCALI = NVL(_Link_.UFLOCALI,space(50))
      this.w_UFPRORIF = NVL(_Link_.UFPRORIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PDUFFREG = space(15)
      endif
      this.w_UF__NOME = space(80)
      this.w_UFLOCALI = space(50)
      this.w_UFPRORIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDUFFREG) OR !EMPTY(.w_UFPRORIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un ufficio che abbia un gestore associato")
        endif
        this.w_PDUFFREG = space(15)
        this.w_UF__NOME = space(80)
        this.w_UFLOCALI = space(50)
        this.w_UFPRORIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.UFF_GIUD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDUFFREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UFF_GIUD_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.UFCODICE as UFCODICE206"+ ",link_2_6.UF__NOME as UF__NOME206"+ ",link_2_6.UFLOCALI as UFLOCALI206"+ ",link_2_6.UFPRORIF as UFPRORIF206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on UTE_PDAM.PDUFFREG=link_2_6.UFCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and UTE_PDAM.PDUFFREG=link_2_6.UFCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPRINDIRI_2_4.value==this.w_PRINDIRI)
      this.oPgFrm.Page1.oPag.oPRINDIRI_2_4.value=this.w_PRINDIRI
      replace t_PRINDIRI with this.oPgFrm.Page1.oPag.oPRINDIRI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPDUFFREG_2_6.value==this.w_PDUFFREG)
      this.oPgFrm.Page1.oPag.oPDUFFREG_2_6.value=this.w_PDUFFREG
      replace t_PDUFFREG with this.oPgFrm.Page1.oPag.oPDUFFREG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDesUff_2_14.value==this.w_DesUff)
      this.oPgFrm.Page1.oPag.oDesUff_2_14.value=this.w_DesUff
      replace t_DesUff with this.oPgFrm.Page1.oPag.oDesUff_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNomeUte_1_5.value==this.w_NomeUte)
      this.oPgFrm.Page1.oPag.oNomeUte_1_5.value=this.w_NomeUte
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.RadioValue()==this.w_PDFLPREF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.SetRadio()
      replace t_PDFLPREF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDCODPDA_2_2.value==this.w_PDCODPDA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDCODPDA_2_2.value=this.w_PDCODPDA
      replace t_PDCODPDA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDCODPDA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESCRI_2_3.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESCRI_2_3.value=this.w_PRDESCRI
      replace t_PRDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESCRI_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'UTE_PDAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_Totale<=1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Attenzione! Selezionare un solo punto di accesso come preferenziale"))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_PDUFFREG) OR !EMPTY(.w_UFPRORIF)) and not(empty(.w_PDUFFREG)) and (not(Empty(.w_PDCODPDA)) AND not(Empty(.w_PDUFFREG)))
          .oNewFocus=.oPgFrm.Page1.oPag.oPDUFFREG_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un ufficio che abbia un gestore associato")
      endcase
      i_bRes = i_bRes .and. .GSAR_MUG.CheckForm()
      if not(Empty(.w_PDCODPDA)) AND not(Empty(.w_PDUFFREG))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSAR_MUG : Depends On
    this.GSAR_MUG.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PDCODPDA)) AND not(Empty(t_PDUFFREG)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PDFLPREF=space(1)
      .w_PDCODPDA=space(5)
      .w_PRDESCRI=space(80)
      .w_PRINDIRI=space(254)
      .w_Attivo=0
      .w_PDUFFREG=space(15)
      .w_UFLOCALI=space(50)
      .w_UF__NOME=space(80)
      .w_UFPRORIF=space(2)
      .w_DesUff=space(150)
      .w_RowStatus=space(1)
      .DoRTCalc(1,3,.f.)
        .w_PDFLPREF = 'N'
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_PDCODPDA))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,7,.f.)
        .w_Attivo = IIF(.w_PDFLPREF='S',1,0)
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_PDUFFREG))
        .link_2_6('Full')
      endif
      .DoRTCalc(10,14,.f.)
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
      .DoRTCalc(16,18,.f.)
        .w_RowStatus = this.RowStatus()
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PDFLPREF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.RadioValue(.t.)
    this.w_PDCODPDA = t_PDCODPDA
    this.w_PRDESCRI = t_PRDESCRI
    this.w_PRINDIRI = t_PRINDIRI
    this.w_Attivo = t_Attivo
    this.w_PDUFFREG = t_PDUFFREG
    this.w_UFLOCALI = t_UFLOCALI
    this.w_UF__NOME = t_UF__NOME
    this.w_UFPRORIF = t_UFPRORIF
    this.w_DesUff = t_DesUff
    this.w_RowStatus = t_RowStatus
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PDFLPREF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPDFLPREF_2_1.ToRadio()
    replace t_PDCODPDA with this.w_PDCODPDA
    replace t_PRDESCRI with this.w_PRDESCRI
    replace t_PRINDIRI with this.w_PRINDIRI
    replace t_Attivo with this.w_Attivo
    replace t_PDUFFREG with this.w_PDUFFREG
    replace t_UFLOCALI with this.w_UFLOCALI
    replace t_UF__NOME with this.w_UF__NOME
    replace t_UFPRORIF with this.w_UFPRORIF
    replace t_DesUff with this.w_DesUff
    replace t_RowStatus with this.w_RowStatus
    if i_srv='A'
      replace PDCODPDA with this.w_PDCODPDA
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_Totale = .w_Totale-.w_attivo
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mpdPag1 as StdContainer
  Width  = 627
  height = 483
  stdWidth  = 627
  stdheight = 483
  resizeXpos=411
  resizeYpos=130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=22, top=33, width=517,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="PDCODPDA",Label1=" ",Field2="PRDESCRI",Label2="Punto di accesso",Field3="PDFLPREF",Label3="Pref.",Field4="",Label4="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168669818

  add object oNomeUte_1_5 as StdField with uid="ZDVBVZFNAJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_NomeUte", cQueryName = "NomeUte",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    HelpContextID = 252523306,;
   bGlobalFont=.t.,;
    Height=21, Width=587, Left=22, Top=10, InputMask=replicate('X',150)
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mug",lower(this.oContained.GSAR_MUG.class))=0
        this.oContained.GSAR_MUG.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=12,top=54,;
    width=581+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=13,top=55,width=580+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='PUN_ACCE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPRINDIRI_2_4.Refresh()
      this.Parent.oPDUFFREG_2_6.Refresh()
      this.Parent.oDesUff_2_14.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='PUN_ACCE'
        oDropInto=this.oBodyCol.oRow.oPDCODPDA_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPRINDIRI_2_4 as StdTrsField with uid="RSMADODDTC",rtseq=7,rtrep=.t.,;
    cFormVar="w_PRINDIRI",value=space(254),enabled=.f.,;
    HelpContextID = 80310335,;
    cTotal="", bFixedPos=.t., cQueryName = "PRINDIRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=587, Left=22, Top=181, InputMask=replicate('X',254)

  add object oPDUFFREG_2_6 as StdTrsField with uid="LRHPVGMDNF",rtseq=9,rtrep=.t.,;
    cFormVar="w_PDUFFREG",value=space(15),;
    ToolTipText = "Ufficio Giudiziario di destinazione della richiesta, ossia Ufficio Giudiziario che fornisce le informazioni dei Fascicoli",;
    HelpContextID = 232923709,;
    cTotal="", bFixedPos=.t., cQueryName = "PDUFFREG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un ufficio che abbia un gestore associato",;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=22, Top=234, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="UFF_GIUD", oKey_1_1="UFCODICE", oKey_1_2="this.w_PDUFFREG"

  func oPDUFFREG_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDUFFREG_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPDUFFREG_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UFF_GIUD','*','UFCODICE',cp_AbsName(this.parent,'oPDUFFREG_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici uffici giudiziari",'GSAL_KWS.UFF_GIUD_VZM',this.parent.oContained
  endproc

  add object oLinkPC_2_7 as stdDynamicChildContainer with uid="TKOJMPYSJS",bOnScreen=.t.,width=609,height=198,;
   left=10, top=269;


  func oLinkPC_2_7.mCond()
    with this.Parent.oContained
      return (!EMPTY(.w_PDCODPDA))
    endwith
  endfunc

  add object oDesUff_2_14 as StdTrsField with uid="PKOLWCHMBE",rtseq=15,rtrep=.t.,;
    cFormVar="w_DesUff",value=space(150),enabled=.f.,;
    HelpContextID = 66265654,;
    cTotal="", bFixedPos=.t., cQueryName = "DesUff",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=113, Top=234, InputMask=replicate('X',150)

  add object oStr_2_8 as StdString with uid="IJYEVFFZZH",Visible=.t., Left=22, Top=211,;
    Alignment=0, Width=264, Height=18,;
    Caption="Ufficio Giudiziario di destinazione della richiesta:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mpdBodyRow as CPBodyRowCnt
  Width=571
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPDFLPREF_2_1 as StdTrsCheck with uid="CDPTDITNOS",rtrep=.t.,;
    cFormVar="w_PDFLPREF",  caption="",;
    ToolTipText = "Se attivo, il PDA selezionato sar� utilizzato come preferenziale",;
    HelpContextID = 243741244,;
    Left=538, Top=0, Width=28,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oPDFLPREF_2_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PDFLPREF,&i_cF..t_PDFLPREF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oPDFLPREF_2_1.GetRadio()
    this.Parent.oContained.w_PDFLPREF = this.RadioValue()
    return .t.
  endfunc

  func oPDFLPREF_2_1.ToRadio()
    this.Parent.oContained.w_PDFLPREF=trim(this.Parent.oContained.w_PDFLPREF)
    return(;
      iif(this.Parent.oContained.w_PDFLPREF=='S',1,;
      0))
  endfunc

  func oPDFLPREF_2_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPDCODPDA_2_2 as StdTrsField with uid="YORMZGZBIS",rtseq=5,rtrep=.t.,;
    cFormVar="w_PDCODPDA",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice punto di accesso",;
    HelpContextID = 197788215,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PUN_ACCE", cZoomOnZoom="GSAL_APR", oKey_1_1="PRCODICE", oKey_1_2="this.w_PDCODPDA"

  func oPDCODPDA_2_2.mCond()
    with this.Parent.oContained
      return (.w_RowStatus='A')
    endwith
  endfunc

  func oPDCODPDA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODPDA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPDCODPDA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPDCODPDA_2_2.readonly and this.parent.oPDCODPDA_2_2.isprimarykey)
    do cp_zoom with 'PUN_ACCE','*','PRCODICE',cp_AbsName(this.parent,'oPDCODPDA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAL_APR',"",'',this.parent.oContained
   endif
  endproc
  proc oPDCODPDA_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAL_APR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRCODICE=this.parent.oContained.w_PDCODPDA
    i_obj.ecpSave()
  endproc

  add object oPRDESCRI_2_3 as StdTrsField with uid="JXJLCJHXKM",rtseq=6,rtrep=.t.,;
    cFormVar="w_PRDESCRI",value=space(80),enabled=.f.,;
    HelpContextID = 263200831,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=475, Left=52, Top=0, InputMask=replicate('X',80)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPDFLPREF_2_1.When()
    return(.t.)
  proc oPDFLPREF_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPDFLPREF_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mpd','UTE_PDAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PDCODPER=UTE_PDAM.PDCODPER";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
