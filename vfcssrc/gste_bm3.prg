* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bm3                                                        *
*              Divisione scadenze                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-28                                                      *
* Last revis.: 2011-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bm3",oParentObject)
return(i_retval)

define class tgste_bm3 as StdBatch
  * --- Local variables
  w_SEGNO = space(1)
  w_PTNUMPAR = space(31)
  w_PTFLSOSP = space(1)
  w_PTTIPCON = space(1)
  w_PTBANAPP = space(10)
  w_PTCODCON = space(15)
  w_PTBANNOS = space(15)
  w_PT_SEGNO = space(1)
  w_PTFLRAGG = space(1)
  w_PTCODVAL = space(3)
  w_PTFLRITE = 0
  w_PTCAOVAL = 0
  w_PTTOTABB = 0
  w_PTCAOAPE = 0
  w_PTFLCRSA = space(1)
  w_PTDATAPE = ctod("  /  /  ")
  w_PTFLIMPE = space(1)
  w_PTNUMDOC = 0
  w_PTIMPDOC = 0
  w_PTALFDOC = space(10)
  w_PTFLINDI = space(1)
  w_PTDATDOC = ctod("  /  /  ")
  w_PTNUMEFF = 0
  w_PTMODPAG = space(10)
  w_PTNUMDIS = space(10)
  w_PTCODAGE = space(5)
  w_PTNUMPRO = 0
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_ORTOTIMP = 0
  w_PTDESRIG = space(50)
  w_PTFLVADB = space(1)
  w_PTDATINT = ctod("  /  /  ")
  w_PTNUMCOR = space(25)
  RA = space(10)
  w_INIT = .f.
  w_APPO1 = ctod("  /  /  ")
  w_APPO2 = 0
  w_NEWNUM = 0
  w_PTDATREG = ctod("  /  /  ")
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Divide Scadenze (da GSTE_KM3)
    * --- RA[] - Vettore Scadenze : 1^Indice N. Riga Scadenza
    * --- 2^Indice : 1 =Data Scadenza
    * --- 2 =Importo Partita
    DIMENSION RA[5, 2]
    * --- Inizializza Variabili Globali/Locali
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Controlli Preliminari
    if this.oParentObject.w_PTTOTIMP=0
      ah_ErrorMsg("Importo partita inesistente",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_IMPO1+this.oParentObject.w_IMPO2+this.oParentObject.w_IMPO3+this.oParentObject.w_IMPO4+this.oParentObject.w_IMPO5<>this.oParentObject.w_PTTOTIMP
      ah_ErrorMsg("Verificare importi; totali incongruenti con la partita da dividere",,"")
      i_retcode = 'stop'
      return
    endif
    FOR M=1 to 5
    if (EMPTY(RA[M,1]) AND RA[M,2]<>0) OR (RA[M,2]=0 AND NOT EMPTY(RA[M,1]))
      ah_ErrorMsg("Verificare riga %1; data scadenza o importo inesistente",,"", ALLTRIM(STR(M)) )
      i_retcode = 'stop'
      return
    endif
    ENDFOR
    * --- Legge la Partita Origine
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PTSERIAL = this.oParentObject.w_PTSERIAL;
            and PTROWORD = this.oParentObject.w_PTROWORD;
            and CPROWNUM = this.oParentObject.w_CPROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
      this.oParentObject.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
      this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
      this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
      this.w_PT_SEGNO = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
      this.w_ORTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
      this.w_PTCODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
      this.w_PTCAOVAL = NVL(cp_ToDate(_read_.PTCAOVAL),cp_NullValue(_read_.PTCAOVAL))
      this.w_PTCAOAPE = NVL(cp_ToDate(_read_.PTCAOAPE),cp_NullValue(_read_.PTCAOAPE))
      this.w_PTDATAPE = NVL(cp_ToDate(_read_.PTDATAPE),cp_NullValue(_read_.PTDATAPE))
      this.w_PTNUMDOC = NVL(cp_ToDate(_read_.PTNUMDOC),cp_NullValue(_read_.PTNUMDOC))
      this.w_PTALFDOC = NVL(cp_ToDate(_read_.PTALFDOC),cp_NullValue(_read_.PTALFDOC))
      this.w_PTDATDOC = NVL(cp_ToDate(_read_.PTDATDOC),cp_NullValue(_read_.PTDATDOC))
      this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
      this.w_PTFLSOSP = NVL(cp_ToDate(_read_.PTFLSOSP),cp_NullValue(_read_.PTFLSOSP))
      this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
      this.w_PTNUMEFF = NVL(cp_ToDate(_read_.PTNUMEFF),cp_NullValue(_read_.PTNUMEFF))
      this.w_PTFLINDI = NVL(cp_ToDate(_read_.PTFLINDI),cp_NullValue(_read_.PTFLINDI))
      this.w_PTBANNOS = NVL(cp_ToDate(_read_.PTBANNOS),cp_NullValue(_read_.PTBANNOS))
      this.w_PTFLRAGG = NVL(cp_ToDate(_read_.PTFLRAGG),cp_NullValue(_read_.PTFLRAGG))
      this.w_PTNUMDIS = NVL(cp_ToDate(_read_.PTNUMDIS),cp_NullValue(_read_.PTNUMDIS))
      this.w_PTIMPDOC = NVL(cp_ToDate(_read_.PTIMPDOC),cp_NullValue(_read_.PTIMPDOC))
      this.w_PTFLRITE = NVL(cp_ToDate(_read_.PTFLRITE),cp_NullValue(_read_.PTFLRITE))
      this.w_PTTOTABB = NVL(cp_ToDate(_read_.PTTOTABB),cp_NullValue(_read_.PTTOTABB))
      this.w_PTFLCRSA = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
      this.w_PTFLIMPE = NVL(cp_ToDate(_read_.PTFLIMPE),cp_NullValue(_read_.PTFLIMPE))
      this.w_PTDESRIG = NVL(cp_ToDate(_read_.PTDESRIG),cp_NullValue(_read_.PTDESRIG))
      this.w_PTCODAGE = NVL(cp_ToDate(_read_.PTCODAGE),cp_NullValue(_read_.PTCODAGE))
      this.w_PTNUMPRO = NVL(cp_ToDate(_read_.PTNUMPRO),cp_NullValue(_read_.PTNUMPRO))
      this.w_OSERIAL = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
      this.w_OROWORD = NVL(cp_ToDate(_read_.PTORDRIF),cp_NullValue(_read_.PTORDRIF))
      this.w_OROWNUM = NVL(cp_ToDate(_read_.PTNUMRIF),cp_NullValue(_read_.PTNUMRIF))
      w_PTFLVABD = NVL(cp_ToDate(_read_.PTFLVABD),cp_NullValue(_read_.PTFLVABD))
      this.w_PTDATINT = NVL(cp_ToDate(_read_.PTDATINT),cp_NullValue(_read_.PTDATINT))
      this.w_PTDATREG = NVL(cp_ToDate(_read_.PTDATREG),cp_NullValue(_read_.PTDATREG))
      this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PTTOTABB<>0
      ah_ErrorMsg("Partita con abbuono, non divisibile",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_INIT = .T.
    * --- Try
    local bErr_038326F8
    bErr_038326F8=bTrsErr
    this.Try_038326F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore durante l'aggiornamento; operazione abbandonata",,"")
    endif
    bTrsErr=bTrsErr or bErr_038326F8
    * --- End
    * --- Rieseguo la Query
     this.oParentObject.oParentObject.oParentObject.NotifyEvent("Riesegui")
    * --- chiudo la maschera di Manutenzione
     this.oParentObject.ecpQuit()
  endproc
  proc Try_038326F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Cicla sulle Righe Valide
    FOR M=1 to 5
    if RA[M,2]<>0 AND NOT EMPTY(RA[M,1])
      this.w_APPO1 = RA[M,1]
      this.w_APPO2 = RA[M,2]
      if this.w_INIT=.T.
        * --- Aggiorna Partita Originaria (solo la prima volta)
        if this.oParentObject.w_FLSALD1="R"
          this.w_APPO1 = IIF(this.w_ORTOTIMP<>this.oParentObject.w_PTTOTIMP,this.oParentObject.w_PTDATSCA,RA[M,1])
          this.w_APPO2 = IIF(this.w_ORTOTIMP<>this.oParentObject.w_PTTOTIMP,this.w_ORTOTIMP-this.oParentObject.w_PTTOTIMP,RA[M,2])
        endif
        if this.w_APPO2<0
          this.w_SEGNO = IIF(this.w_PT_SEGNO="A","D","A")
          this.w_APPO2 = ABS(this.w_APPO2)
        else
          this.w_SEGNO = this.w_PT_SEGNO
        endif
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTDATSCA ="+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'PAR_TITE','PTDATSCA');
          +",PTTOTIMP ="+cp_NullLink(cp_ToStrODBC(this.w_APPO2),'PAR_TITE','PTTOTIMP');
          +",PT_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'PAR_TITE','PT_SEGNO');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              PTDATSCA = this.w_APPO1;
              ,PTTOTIMP = this.w_APPO2;
              ,PT_SEGNO = this.w_SEGNO;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.oParentObject.w_PTSERIAL;
              and PTROWORD = this.oParentObject.w_PTROWORD;
              and CPROWNUM = this.oParentObject.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Calcola Primo Num.Riga Disponibile per eventuali inserimenti future scadenze
        this.w_NEWNUM = 0
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" PAR_TITE ";
              +" where PTSERIAL="+cp_ToStrODBC(this.oParentObject.w_PTSERIAL)+" AND PTROWORD="+cp_ToStrODBC(this.oParentObject.w_PTROWORD)+"";
               ,"_Curs_PAR_TITE")
        else
          select CPROWNUM from (i_cTable);
           where PTSERIAL=this.oParentObject.w_PTSERIAL AND PTROWORD=this.oParentObject.w_PTROWORD;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_NEWNUM = IIF(this.w_NEWNUM < CPROWNUM, CPROWNUM, this.w_NEWNUM)
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
        this.w_INIT = .F.
        if this.oParentObject.w_FLSALD1="R"
          if this.w_ORTOTIMP<>this.oParentObject.w_PTTOTIMP
            * --- Carica Prima Partita
            this.w_APPO1 = RA[M,1]
            this.w_APPO2 = RA[M,2]
            this.w_NEWNUM = this.w_NEWNUM + 1
            if this.w_APPO2<0
              this.w_SEGNO = IIF(this.w_PT_SEGNO="A","D","A")
              this.w_APPO2 = ABS(this.w_APPO2)
            else
              this.w_SEGNO = this.w_PT_SEGNO
            endif
            * --- Insert into PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMEFF"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTNUMDIS"+",PTIMPDOC"+",PTFLRITE"+",PTFLCRSA"+",PTFLIMPE"+",PTDESRIG"+",PTNUMPRO"+",PTCODAGE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLVABD"+",PTDATINT"+",PTDATREG"+",PTNUMCOR"+",PT_SEGNO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTSERIAL),'PAR_TITE','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTROWORD),'PAR_TITE','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NEWNUM),'PAR_TITE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'PAR_TITE','PTDATSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPO2),'PAR_TITE','PTTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLINDI),'PAR_TITE','PTFLINDI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRITE),'PAR_TITE','PTFLRITE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPRO),'PAR_TITE','PTNUMPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(w_PTFLVABD),'PAR_TITE','PTFLVABD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATINT),'PAR_TITE','PTDATINT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATREG),'PAR_TITE','PTDATREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'PAR_TITE','PT_SEGNO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PTSERIAL,'PTROWORD',this.oParentObject.w_PTROWORD,'CPROWNUM',this.w_NEWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_APPO1,'PTTIPCON',this.w_PTTIPCON,'PTCODCON',this.w_PTCODCON,'PTTOTIMP',this.w_APPO2,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTCAOAPE',this.w_PTCAOAPE,'PTDATAPE',this.w_PTDATAPE)
              insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMEFF,PTFLINDI,PTBANNOS,PTFLRAGG,PTNUMDIS,PTIMPDOC,PTFLRITE,PTFLCRSA,PTFLIMPE,PTDESRIG,PTNUMPRO,PTCODAGE,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,PTDATINT,PTDATREG,PTNUMCOR,PT_SEGNO &i_ccchkf. );
                 values (;
                   this.oParentObject.w_PTSERIAL;
                   ,this.oParentObject.w_PTROWORD;
                   ,this.w_NEWNUM;
                   ,this.w_PTNUMPAR;
                   ,this.w_APPO1;
                   ,this.w_PTTIPCON;
                   ,this.w_PTCODCON;
                   ,this.w_APPO2;
                   ,this.w_PTCODVAL;
                   ,this.w_PTCAOVAL;
                   ,this.w_PTCAOAPE;
                   ,this.w_PTDATAPE;
                   ,this.w_PTNUMDOC;
                   ,this.w_PTALFDOC;
                   ,this.w_PTDATDOC;
                   ,this.w_PTMODPAG;
                   ,this.w_PTFLSOSP;
                   ,this.w_PTBANAPP;
                   ,this.w_PTNUMEFF;
                   ,this.w_PTFLINDI;
                   ,this.w_PTBANNOS;
                   ,this.w_PTFLRAGG;
                   ,this.w_PTNUMDIS;
                   ,this.w_PTIMPDOC;
                   ,this.w_PTFLRITE;
                   ,this.w_PTFLCRSA;
                   ,this.w_PTFLIMPE;
                   ,this.w_PTDESRIG;
                   ,this.w_PTNUMPRO;
                   ,this.w_PTCODAGE;
                   ,this.w_OSERIAL;
                   ,this.w_OROWORD;
                   ,this.w_OROWNUM;
                   ,w_PTFLVABD;
                   ,this.w_PTDATINT;
                   ,this.w_PTDATREG;
                   ,this.w_PTNUMCOR;
                   ,this.w_SEGNO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      else
        * --- Carica Nuova Partita
        this.w_NEWNUM = this.w_NEWNUM + 1
        if this.w_APPO2<0
          this.w_SEGNO = IIF(this.w_PT_SEGNO="A","D","A")
          this.w_APPO2 = ABS(this.w_APPO2)
        else
          this.w_SEGNO = this.w_PT_SEGNO
        endif
        * --- Insert into PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMEFF"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTNUMDIS"+",PTIMPDOC"+",PTFLRITE"+",PTFLCRSA"+",PTFLIMPE"+",PTDESRIG"+",PTNUMPRO"+",PTCODAGE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLVABD"+",PTDATINT"+",PTDATREG"+",PTNUMCOR"+",PT_SEGNO"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTSERIAL),'PAR_TITE','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTROWORD),'PAR_TITE','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NEWNUM),'PAR_TITE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'PAR_TITE','PTDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_APPO2),'PAR_TITE','PTTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLINDI),'PAR_TITE','PTFLINDI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRITE),'PAR_TITE','PTFLRITE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPRO),'PAR_TITE','PTNUMPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(w_PTFLVABD),'PAR_TITE','PTFLVABD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATINT),'PAR_TITE','PTDATINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATREG),'PAR_TITE','PTDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'PAR_TITE','PT_SEGNO');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.oParentObject.w_PTSERIAL,'PTROWORD',this.oParentObject.w_PTROWORD,'CPROWNUM',this.w_NEWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_APPO1,'PTTIPCON',this.w_PTTIPCON,'PTCODCON',this.w_PTCODCON,'PTTOTIMP',this.w_APPO2,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTCAOAPE',this.w_PTCAOAPE,'PTDATAPE',this.w_PTDATAPE)
          insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMEFF,PTFLINDI,PTBANNOS,PTFLRAGG,PTNUMDIS,PTIMPDOC,PTFLRITE,PTFLCRSA,PTFLIMPE,PTDESRIG,PTNUMPRO,PTCODAGE,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,PTDATINT,PTDATREG,PTNUMCOR,PT_SEGNO &i_ccchkf. );
             values (;
               this.oParentObject.w_PTSERIAL;
               ,this.oParentObject.w_PTROWORD;
               ,this.w_NEWNUM;
               ,this.w_PTNUMPAR;
               ,this.w_APPO1;
               ,this.w_PTTIPCON;
               ,this.w_PTCODCON;
               ,this.w_APPO2;
               ,this.w_PTCODVAL;
               ,this.w_PTCAOVAL;
               ,this.w_PTCAOAPE;
               ,this.w_PTDATAPE;
               ,this.w_PTNUMDOC;
               ,this.w_PTALFDOC;
               ,this.w_PTDATDOC;
               ,this.w_PTMODPAG;
               ,this.w_PTFLSOSP;
               ,this.w_PTBANAPP;
               ,this.w_PTNUMEFF;
               ,this.w_PTFLINDI;
               ,this.w_PTBANNOS;
               ,this.w_PTFLRAGG;
               ,this.w_PTNUMDIS;
               ,this.w_PTIMPDOC;
               ,this.w_PTFLRITE;
               ,this.w_PTFLCRSA;
               ,this.w_PTFLIMPE;
               ,this.w_PTDESRIG;
               ,this.w_PTNUMPRO;
               ,this.w_PTCODAGE;
               ,this.w_OSERIAL;
               ,this.w_OROWORD;
               ,this.w_OROWNUM;
               ,w_PTFLVABD;
               ,this.w_PTDATINT;
               ,this.w_PTDATREG;
               ,this.w_PTNUMCOR;
               ,this.w_SEGNO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    ENDFOR
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Dati Partita
    * --- Inizializza Variabili
    * --- Assegna ai Vettori
    FOR M=1 to 5
    N = ALLTRIM(STR(M))
    RA[M, 1] = this.oParentObject.w_DATA&N
    RA[M, 2] = this.oParentObject.w_IMPO&N
    ENDFOR
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
