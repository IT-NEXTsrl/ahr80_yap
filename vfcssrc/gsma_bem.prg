* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bem                                                        *
*              Cancellazione dati rilevati e movimenti di magazzino            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_58]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-08-07                                                      *
* Last revis.: 2010-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bem",oParentObject)
return(i_retval)

define class tgsma_bem as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_SCELTA = 0
  w_MESS = space(10)
  w_DRSERIAL = space(10)
  w_PROG = .NULL.
  w_PRIMOGIRO = .f.
  w_SERRET = space(10)
  w_MOVICANC = 0
  * --- WorkFile variables
  RILEVAZI_idx=0
  MVM_MAST_idx=0
  MVM_DETT_idx=0
  TMPMOVIMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di Cancellazione Dati Rilevati
    if EMPTY(this.oParentObject.w_CODRIL)
      ah_ErrorMsg("Codice rilevazione obbligatorio")
      i_retcode = 'stop'
      return
    endif
    if g_APPLICATION = "ADHOC REVOLUTION" And Not Empty(CHKCONS("M",this.oParentObject.w_DATRIL,"B","N"))
      this.w_MESS = "Impossibile eliminare movimenti di magazzino di rettifica%0%1"
      ah_ErrorMsg(this.w_MESS,"!","",CHKCONS("M",this.oParentObject.w_DATRIL,"B","N") )
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_FLRIM_DR="S"
      this.w_SCELTA = ah_Yesno("Questa operazione eliminerÓ i dati rilevati selezionati che hanno generato movimento di magazzino e i relativi movimenti%0Si desidera proseguire?")
    else
      this.w_SCELTA = ah_Yesno("Questa operazione eliminerÓ i movimenti di magazzino generati dal dato rilevato selezionato%0Si desidera proseguire?")
    endif
    do case
      case !(this.w_SCELTA)
        ah_ErrorMsg("Eliminazione dati rilevati e movimenti di magazzino interrotta come richiesto")
        i_retcode = 'stop'
        return
      case this.w_SCELTA
        ah_Msg("Eliminazione movimenti di magazzino...",.T.,.T.)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Try
        local bErr_047C3A30
        bErr_047C3A30=bTrsErr
        this.Try_047C3A30()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Eliminazione dati rilevati e movimenti di magazzino fallita")
        endif
        bTrsErr=bTrsErr or bErr_047C3A30
        * --- End
    endcase
  endproc
  proc Try_047C3A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_FLRIM_DR="S"
      ah_Msg("Eliminazione dati rilevati...",.T.,.T.)
      * --- Delete from RILEVAZI
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DRSERIAL = "+i_cQueryTable+".DRSERIAL";
      
        do vq_exec with 'QUERY\GSMA_EM2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      WAIT CLEAR
      if i_ROWS = 0
        ah_ErrorMsg("L'eliminazione dati rilevati e movimenti di magazzino non ha eseguito operazioni")
      else
        ah_ErrorMsg("Eliminazione dati rilevati e movimenti di magazzino completata")
      endif
    else
      ah_Msg("Aggiornamento dati rilevati...",.T.,.T.)
      this.w_DRSERIAL = SPACE( 10 )
      * --- Select from QUERY\GSMA_EM2
      do vq_exec with 'QUERY\GSMA_EM2',this,'_Curs_QUERY_GSMA_EM2','',.f.,.t.
      if used('_Curs_QUERY_GSMA_EM2')
        select _Curs_QUERY_GSMA_EM2
        locate for 1=1
        do while not(eof())
        this.w_DRSERIAL = DRSERIAL
        * --- Write into RILEVAZI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RILEVAZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DRCONFER ="+cp_NullLink(cp_ToStrODBC(" " ),'RILEVAZI','DRCONFER');
          +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRSERMOV');
              +i_ccchkf ;
          +" where ";
              +"DRCODRIL = "+cp_ToStrODBC(this.oParentObject.w_CODRIL);
              +" and DRCONFER = "+cp_ToStrODBC("S");
              +" and DRSERIAL = "+cp_ToStrODBC(this.w_DRSERIAL);
                 )
        else
          update (i_cTable) set;
              DRCONFER = " " ;
              ,DRSERMOV = " ";
              &i_ccchkf. ;
           where;
              DRCODRIL = this.oParentObject.w_CODRIL;
              and DRCONFER = "S";
              and DRSERIAL = this.w_DRSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_QUERY_GSMA_EM2
          continue
        enddo
        use
      endif
      WAIT CLEAR
      if EMPTY( this.w_DRSERIAL )
        ah_ErrorMsg("L'eliminazione movimenti di magazzino e l'aggiornamento dati rilevati non hanno eseguito operazioni")
      else
        ah_ErrorMsg("Eliminazione movimenti di magazzino ed aggiornamento dati rilevati completata")
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_CODRIL = SPACE(10)
    this.oParentObject.w_DATRIL = cp_CharToDate("  -  -  ")
    this.oParentObject.w_DESRIL = SPACE(40)
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRIMOGIRO = .T.
    this.w_PROG = .NULL.
    * --- Create temporary table TMPMOVIMAST
    i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.RILEVAZI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"DRSERIAL "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPMOVIMAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from GSMA_EMM
    do vq_exec with 'GSMA_EMM',this,'_Curs_GSMA_EMM','',.f.,.t.
    if used('_Curs_GSMA_EMM')
      select _Curs_GSMA_EMM
      locate for 1=1
      do while not(eof())
      if this.w_PRIMOGIRO
        this.w_PROG = GSMA_MVM()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PRIMOGIRO = .F.
      endif
      this.w_SERIAL = _Curs_GSMA_EMM.MMSERIAL
      this.w_PROG.w_MMSERIAL = this.w_SERIAL
      * --- creo il curosre delle solo chiavi
      this.w_PROG.QueryKeySet("MMSERIAL='"+this.w_SERIAL+ "'","")     
      * --- mi metto in interrogazione
      this.w_PROG.LoadRecWarn()     
      * --- Gestisce la cancellazione se lanciato da routine
      this.w_PROG.w_DELCARPRO = This.OparentObject
      * --- Verifico se posso eliminare la registrazione
      if this.w_PROG.HAsCpEvents("EcpDelete")
        this.w_PROG.ECPDELETE()     
      endif
      * --- Insert into TMPMOVIMAST
      i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA_EM1",this.TMPMOVIMAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSMA_EMM
        continue
      enddo
      use
    endif
    if NOT ISNULL( this.w_PROG )
      this.w_PROG.Ecpquit()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='RILEVAZI'
    this.cWorkTables[2]='MVM_MAST'
    this.cWorkTables[3]='MVM_DETT'
    this.cWorkTables[4]='*TMPMOVIMAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_QUERY_GSMA_EM2')
      use in _Curs_QUERY_GSMA_EM2
    endif
    if used('_Curs_GSMA_EMM')
      use in _Curs_GSMA_EMM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
