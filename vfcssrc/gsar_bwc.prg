* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bwc                                                        *
*              Gestione wizard nuovo nominativo                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-10                                                      *
* Last revis.: 2015-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bwc",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bwc as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_ATTPOS = 0
  w_CODGEN = space(10)
  w_OBJCTRL = .NULL.
  w_OKNOM = .f.
  w_OBJ = .NULL.
  w_ERRORE = .f.
  * --- WorkFile variables
  CONF_INT_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="INITFIELDS"
        if g_OFNUME="S"
          this.oParentObject.w_NOCODICE = SPACE(15)
          i_nConn = i_TableProp[this.CONF_INT_IDX,3]
          cp_AskTableProg(this.oParentObject,i_nConn,"PRNUNMOF","i_codazi,w_NOCODICE")
          if LEN(ALLTRIM(p_NOM))<>0
            this.oParentObject.w_NOCODICE = RIGHT(this.oParentObject.w_NOCODICE, LEN(ALLTRIM(p_NOM)))
          endif
        endif
        this.w_OBJCTRL = this.oParentObject.GetCtrl("w_NODESCRI")
        this.w_OBJCTRL.SetFocus()     
      case this.pOPER=="CHECK_FORM"
        if g_OFNUME="S"
          this.w_ATTPOS = 1
          do while this.w_ATTPOS<=LEN(this.oParentObject.w_NOCODICE)
            this.oParentObject.w_NOCODICE = IIF(SUBSTR(this.oParentObject.w_NOCODICE, this.w_ATTPOS, 1)$"0123456789", this.oParentObject.w_NOCODICE, 1)
            this.w_ATTPOS = this.w_ATTPOS + 1
          enddo
          * --- Try
          local bErr_02C90E78
          bErr_02C90E78=bTrsErr
          this.Try_02C90E78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat("Codice utentegi� utilizzato")
            i_retcode = 'stop'
            i_retval = ah_MsgFormat("Codice utente gi� utilizzato")
            return
          endif
          bTrsErr=bTrsErr or bErr_02C90E78
          * --- End
        endif
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_NOCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                NOCODICE = this.oParentObject.w_NOCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          i_retcode = 'stop'
          i_retval = ah_MsgFormat("Codice utente gi� utilizzato")
          return
        endif
        i_retcode = 'stop'
        i_retval = ""
        return
      case this.pOPER=="SAVE"
        this.w_OKNOM = .t.
        * --- Try
        local bErr_02C9BF98
        bErr_02C9BF98=bTrsErr
        this.Try_02C9BF98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_OKNOM = .f.
          ah_ErrorMsg("Errore inserimento nominativo%0%1",,, MESSAGE())
        endif
        bTrsErr=bTrsErr or bErr_02C9BF98
        * --- End
        if this.w_OKNOM 
          this.w_OBJ = This.oparentobject.oparentobject
          this.oParentObject.w_OGG_FIELD.Value = this.oParentObject.w_NOCODICE
          w_ErrorHandler = on("ERROR")
          on error this.w_ERRORE = .T.
          macro="This.oparentobject.w_OGGETTO." + Alltrim(this.oParentObject.w_OGG_FIELD.cFormvar) + " = '" + this.oParentObject.w_NOCODICE + "'" 
 &macro
          on error &w_ErrorHandler
          this.oParentObject.w_OGG_FIELD.Check()     
          this.w_OBJ.mcalc(.t.)     
          this.w_OBJ = Null
          this.oParentObject.w_OGG_FIELD = .Null.
        endif
      case this.pOPER=="NEXT" OR this.pOPER=="PREV"
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + IIF(this.pOPER=="NEXT", 1, -1)
        this.oParentObject.oPGFRM.ActivePage=this.oParentObject.w_CUR_STEP
      case this.pOPER=="OPEN"
        DO gsar_kwc with this.oparentobject
    endcase
  endproc
  proc Try_02C90E78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    cp_NextTableProg(this.oParentObject,i_nConn,"PRNUNMOF","i_codazi,w_NOCODICE")
    if LEN(ALLTRIM(p_NOM))<>0
      this.oParentObject.w_NOCODICE = RIGHT(this.oParentObject.w_NOCODICE, LEN(ALLTRIM(p_NOM)))
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_02C9BF98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Inserimento nuovo nominativo per poi utilizzare il batch standard 
    *     di conversione nominativo/cliente
    * --- Insert into OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"NOCODICE"+",NOTIPPER"+",NOCOGNOM"+",NO__NOME"+",NO_SESSO"+",NODATNAS"+",NOLOCNAS"+",NOPRONAS"+",NODESCRI"+",NOINDIRI"+",NOINDI_2"+",NO___CAP"+",NOLOCALI"+",NOPROVIN"+",NONAZION"+",NOPARIVA"+",NOCODFIS"+",NOTELEFO"+",NOTELFAX"+",NONUMCEL"+",NOINDWEB"+",NO_EMAIL"+",UTCC"+",UTDC"+",NO__NOTE"+",NO_SKYPE"+",NOTIPNOM"+",NOCODLIN"+",NOSOGGET"+",NONATGIU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOCODICE),'OFF_NOMI','NOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTIPPER),'OFF_NOMI','NOTIPPER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOCOGNOM),'OFF_NOMI','NOCOGNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO__NOME),'OFF_NOMI','NO__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO_SESSO),'OFF_NOMI','NO_SESSO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NODATNAS),'OFF_NOMI','NODATNAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOLOCNAS),'OFF_NOMI','NOLOCNAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOPRONAS),'OFF_NOMI','NOPRONAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NODESCRI),'OFF_NOMI','NODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOINDIRI),'OFF_NOMI','NOINDIRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOINDI_2),'OFF_NOMI','NOINDI_2');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO___CAP),'OFF_NOMI','NO___CAP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOLOCALI),'OFF_NOMI','NOLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOPROVIN),'OFF_NOMI','NOPROVIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NONAZION),'OFF_NOMI','NONAZION');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOPARIVA),'OFF_NOMI','NOPARIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOCODFIS),'OFF_NOMI','NOCODFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTELEFO),'OFF_NOMI','NOTELEFO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTELFAX),'OFF_NOMI','NOTELFAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NONUMCEL),'OFF_NOMI','NONUMCEL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOINDWEB),'OFF_NOMI','NOINDWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO_EMAIL),'OFF_NOMI','NO_EMAIL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_NOMI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'OFF_NOMI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO__NOTE),'OFF_NOMI','NO__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NO_SKYPE),'OFF_NOMI','NO_SKYPE');
      +","+cp_NullLink(cp_ToStrODBC("T"),'OFF_NOMI','NOTIPNOM');
      +","+cp_NullLink(cp_ToStrODBC(g_CODLIN),'OFF_NOMI','NOCODLIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOSOGGET),'OFF_NOMI','NOSOGGET');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NONATGIU),'OFF_NOMI','NONATGIU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'NOCODICE',this.oParentObject.w_NOCODICE,'NOTIPPER',this.oParentObject.w_NOTIPPER,'NOCOGNOM',this.oParentObject.w_NOCOGNOM,'NO__NOME',this.oParentObject.w_NO__NOME,'NO_SESSO',this.oParentObject.w_NO_SESSO,'NODATNAS',this.oParentObject.w_NODATNAS,'NOLOCNAS',this.oParentObject.w_NOLOCNAS,'NOPRONAS',this.oParentObject.w_NOPRONAS,'NODESCRI',this.oParentObject.w_NODESCRI,'NOINDIRI',this.oParentObject.w_NOINDIRI,'NOINDI_2',this.oParentObject.w_NOINDI_2,'NO___CAP',this.oParentObject.w_NO___CAP)
      insert into (i_cTable) (NOCODICE,NOTIPPER,NOCOGNOM,NO__NOME,NO_SESSO,NODATNAS,NOLOCNAS,NOPRONAS,NODESCRI,NOINDIRI,NOINDI_2,NO___CAP,NOLOCALI,NOPROVIN,NONAZION,NOPARIVA,NOCODFIS,NOTELEFO,NOTELFAX,NONUMCEL,NOINDWEB,NO_EMAIL,UTCC,UTDC,NO__NOTE,NO_SKYPE,NOTIPNOM,NOCODLIN,NOSOGGET,NONATGIU &i_ccchkf. );
         values (;
           this.oParentObject.w_NOCODICE;
           ,this.oParentObject.w_NOTIPPER;
           ,this.oParentObject.w_NOCOGNOM;
           ,this.oParentObject.w_NO__NOME;
           ,this.oParentObject.w_NO_SESSO;
           ,this.oParentObject.w_NODATNAS;
           ,this.oParentObject.w_NOLOCNAS;
           ,this.oParentObject.w_NOPRONAS;
           ,this.oParentObject.w_NODESCRI;
           ,this.oParentObject.w_NOINDIRI;
           ,this.oParentObject.w_NOINDI_2;
           ,this.oParentObject.w_NO___CAP;
           ,this.oParentObject.w_NOLOCALI;
           ,this.oParentObject.w_NOPROVIN;
           ,this.oParentObject.w_NONAZION;
           ,this.oParentObject.w_NOPARIVA;
           ,this.oParentObject.w_NOCODFIS;
           ,this.oParentObject.w_NOTELEFO;
           ,this.oParentObject.w_NOTELFAX;
           ,this.oParentObject.w_NONUMCEL;
           ,this.oParentObject.w_NOINDWEB;
           ,this.oParentObject.w_NO_EMAIL;
           ,i_CODUTE;
           ,SetInfoDate(g_CALUTD);
           ,this.oParentObject.w_NO__NOTE;
           ,this.oParentObject.w_NO_SKYPE;
           ,"T";
           ,g_CODLIN;
           ,this.oParentObject.w_NOSOGGET;
           ,this.oParentObject.w_NONATGIU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONF_INT'
    this.cWorkTables[2]='OFF_NOMI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
