* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bli                                                        *
*              Batch dichiarazione di intento                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-15                                                      *
* Last revis.: 2017-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bli",oParentObject,m.pTIPO)
return(i_retval)

define class tgscg_bli as StdBatch
  * --- Local variables
  pTIPO = 0
  w_DATA = ctod("  /  /  ")
  w_PERSO = space(1)
  w_MESS = space(50)
  w_PARIVA = space(12)
  w_CODATT = space(5)
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_PAGINE = 0
  w_ERRORE = .f.
  w_CODAZI = space(5)
  w_NRPAGINT = 0
  w_NRPREFIS = space(20)
  w_PRPAG = 0
  w_DISER = space(10)
  w_STATO03 = space(1)
  w_NomeCursore = space(10)
  w_DICINTE_Temp = space(10)
  w_SERIALE_DICH = space(10)
  w_TipoGest = space(3)
  w_STAMPATE = space(10)
  w_CONFER = space(1)
  w_CAZI = space(5)
  w_CESE = space(4)
  w_S2NPARI = 0
  w_SERIALE = space(10)
  w_DATBLO = ctod("  /  /  ")
  w_DATDIC = ctod("  /  /  ")
  w_DOCSER = space(10)
  w_OBJCTRL = .NULL.
  w_GSCG_ADI = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  ESERCIZI_idx=0
  DIC_INTE_idx=0
  VALUTE_idx=0
  ATTIDETT_idx=0
  DOC_MAST_idx=0
  TITOLARI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per il controllo univocit� protocollo nelle lettere di intento ai fornitori
    *     Lanciato dalla anagrafica GSCG_ADI alla modifica del campo 'n. protocollo' 
    * --- Lostesso batch viene anche utilizzato per tutte le altre maschere riguardanti dichiarazioni di intento:
    *     GSCG_SIB(stampa brogliaccio), GSCG_SIR(stampa registri), GSCG_SLI(stampa dichiarazioni)
    * --- pTIPO
    *     0: Controlli alla CheckForm
    *     2: Stampa Dichiarazioni d'Intento da menu'
    *     3: Esegue Stampa Registri
    *     4: Valorizza ultima Pagina (Stampa registri)
    *     5: Controlli alla HasEvent
    *     6: Check univocit� numero documento
    this.w_DISER = ""
    this.w_STATO03 = " "
    this.w_CODAZI = I_CODAZI
    this.w_GSCG_ADI = this.oParentObject
    do case
      case this.pTIPO=0
        * --- Controlli INSERT, UPDATE
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO=2 OR this.pTipo=9
        * --- Stampa Dichiarazioni d'Intento da menu'
        * --- Test persona fisica
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZPERAZI,AZPIVAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZPERAZI,AZPIVAZI;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERSO = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
          this.w_PARIVA = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
         
 L_COGNOME="" 
 L_NOME="" 
 L_DATNAS=cp_CharToDate("  -  -  ") 
 L_SESSO="" 
 L_LUONAS="" 
 L_PRONAS="" 
 L_PERSO="N"
        if this.w_PERSO="S"
          * --- Nella dichiarazione di intento stampo solo i dati del titolare
          * --- Select from TITOLARI
          i_nConn=i_TableProp[this.TITOLARI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2],.t.,this.TITOLARI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" TITOLARI ";
                +" where TTCODAZI = "+cp_ToStrODBC(this.w_CODAZI)+" ";
                +" order by CPROWNUM Asc";
                 ,"_Curs_TITOLARI")
          else
            select * from (i_cTable);
             where TTCODAZI = this.w_CODAZI ;
             order by CPROWNUM Asc;
              into cursor _Curs_TITOLARI
          endif
          if used('_Curs_TITOLARI')
            select _Curs_TITOLARI
            locate for 1=1
            do while not(eof())
            * --- Recupero il titolare con il CPROWNUM pi� basso
             
 L_COGNOME=NVL(_Curs_TITOLARI.TTCOGTIT,"") 
 L_NOME=NVL(_Curs_TITOLARI.TTNOMTIT,"") 
 L_DATNAS=Cp_todate(_Curs_TITOLARI.TTDATNAS) 
 L_SESSO=NVL(_Curs_TITOLARI.TT_SESSO,"") 
 L_LUONAS=NVL(_Curs_TITOLARI.TTLUONAS,"") 
 L_PRONAS=NVL(_Curs_TITOLARI.TTPRONAS,"") 
 L_PERSO=this.w_PERSO 
 Exit
              select _Curs_TITOLARI
              continue
            enddo
            use
          endif
          * --- Non ho titolari definiti in azienda..
          if L_PERSO="N"
            ah_ErrorMsg("Attenzione: dati del titolare non inseriti")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Stampa
        L_PARIVA=this.w_PARIVA
        * --- Imposto l'indirizzo email e numero di Fax se ho selezionato un unico fornitore
        if ! EMPTY(this.oParentObject.w_CODICE) AND this.oParentObject.w_CODICE=this.oParentObject.w_CODICE1 AND this.oParentObject.w_CLIFOR = "F"
          * --- Assegno le variabili per la selezione dei numeri di Fax
          i_EMAIL=this.oParentObject.w_EMAIL
          i_EMAIL_PEC=this.oParentObject.w_EMPEC
          i_FAXNO = this.oParentObject.w_TELFAX
          i_CLIFORDES = this.oParentObject.w_CLIFOR
          i_CODDES = this.oParentObject.w_CODICE
          i_TIPDES="CO"
          i_DEST=this.oParentObject.w_DESCRI
        else
          this.oParentObject.w_EMAIL = ""
          this.oParentObject.w_EMPEC = ""
        endif
        this.w_Stampate = .F.
        * --- Stampa di prova o ristampa
        this.w_NomeCursore = "DICINTE"
        if this.pTipo=9
          this.w_DICINTE_Temp = ADDBS(tempadhoc())+"DICINTE-Temp\"
          Vq_Exec("GSCG_SDE", this, this.w_NomeCursore)
          if RECCOUNT (this.w_NomeCursore)>0
            * --- Se non esiste crea la cartella per i file temporanei
            if NOT DIRECTORY (this.w_DICINTE_Temp)
              MD (this.w_DICINTE_Temp)
            endif
            * --- --Lancia Batch di creazione immagini 
            this.w_TipoGest = "Dicinte"
            GSAR_BDI (this,this.w_NomeCursore,this.w_DICINTE_Temp,this.w_TipoGest )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            Select * from (this.w_NomeCursore) order by dinumdoc into cursor __TMP__
            CP_CHPRN("QUERY\GSCG_SDE.FRX","",this.oParentObject)
            if this.oParentObject.w_TIPOSTAM="D"
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            ah_errormsg("Non esistono dichiarazioni da stampare")
          endif
        else
          Vq_Exec("GSCG_QIF", this, this.w_NomeCursore)
          if RECCOUNT (this.w_NomeCursore)>0
            Select * from (this.w_NomeCursore) order by dinumdoc into cursor __TMP__
            CP_CHPRN("QUERY\GSCG_QIN.FRX","",this.oParentObject)
            if this.oParentObject.w_TIPOSTAM="D"
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            ah_errormsg("Non esistono dichiarazioni da stampare")
          endif
        endif
        * --- Distrugge la cartella dei file temporanei
        if DIRECTORY (this.w_DICINTE_Temp) AND this.pTipo=9
          DELETE file ( this.w_DICINTE_Temp +"*.*")
          RD (this.w_DICINTE_Temp)
        endif
        use in Select (this.w_NomeCursore)
        if USED ("__TMP__")
          SELECT __TMP__ 
 USE
        endif
        * --- Ripulisco i valori delle variabili usate per indirizzo email e numero fax
        i_CLIFORDES =" "
        i_CODDES = " "
        i_TIPDES=" "
        i_EMAIL = " "
        i_EMAIL_PEC = " "
        i_FAXNO=" "
        i_DEST=" "
      case this.pTIPO=3
        * --- Stampa Registro
        * --- Controllo intervallo date
        if this.oParentObject.w_IN>this.oParentObject.w_DATFIN
          this.w_MESS = "Data iniziale maggiore di quella finale"
          ah_errormsg(this.w_MESS)
          i_retcode = 'stop'
          return
        endif
        * --- Controllo Data iniziale Successiva Ultima Stampa - oppure data vuota ultima stampa
        if (NOT this.oParentObject.w_IN=(this.oParentObject.w_VISUDATA+1)) AND (NOT empty(this.oParentObject.w_visudata)) .and. this.oParentObject.w_TipoStam<>"R"
          this.w_MESS = "Data iniziale non consecutiva all'ultima stampa%0Nel caso di stampa definitiva potrebbero essere ignorate alcune dichiarazioni%0Confermi?"
          if NOT AH_YESNO(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
        endif
        if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
          GSCG_BLU (this,"I")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Il blocco dell'anagrafica, come per la prima Nota non viene previsto per questa stampa
        * --- Lancio Stampa
        PUBLIC STPAG
        * --- PER NUMERAZIONE PAGINE IN TESTATA
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCATAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCATAZI;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          this.w_CODATT = NVL(cp_ToDate(_read_.AZCATAZI),cp_NullValue(_read_.AZCATAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_PAGINE=0
        L_PRPARI=this.oParentObject.w_PRPARI
        L_CODESE=this.oParentObject.w_CODESE
        L_INTLIG=this.oParentObject.w_INTLIG
        L_PREFIS=this.oParentObject.w_PREFIS
        L_INDAZI=this.w_INDAZI
        L_LOCAZI=this.w_LOCAZI
        L_CAPAZI=this.w_CAPAZI
        L_PROAZI=this.w_PROAZI
        L_COFAZI=this.w_COFAZI
        L_PIVAZI=this.w_PIVAZI
        L_STAMPATO=0
        STPAG=0
        vq_exec("gscg_sir.vqr",this,"Temp")
        if Reccount("Temp")>0
          vx_exec("gscg_sir.vqr, query\gscg_sir.FRX",this)
          * --- Richiesta conferma aggiornamento
          this.w_PAGINE = STPAG+this.oParentObject.w_PRPARI
          STPAG=0
          this.w_CAZI = this.w_CODAZI
          this.w_CESE = this.oParentObject.w_CODESE
          if this.oParentObject.w_TipoStam="B" AND L_STAMPATO=1
            * --- Se stampa su Bollato chiedo conferma
            this.w_CONFER = "N"
            this.w_DATA = this.oParentObject.w_DATFIN
            * --- Lancio la maschera di Conferma
            this.w_S2NPARI = this.w_PAGINE
            do GSCG_S2N with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_CONFER="S"
              * --- Try
              local bErr_04DC6218
              bErr_04DC6218=bTrsErr
              this.Try_04DC6218()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                ah_errormsg("Non � possibile aggiornare le dichiarazioni d'intento")
              endif
              bTrsErr=bTrsErr or bErr_04DC6218
              * --- End
            endif
          endif
        else
          this.w_MESS = "Per il periodo impostato non risultano dichiarazioni da stampare!"
          ah_errormsg(this.w_MESS)
        endif
        if Used("Temp")
           
 Select Temp 
 Use
        endif
      case this.pTIPO=4
        * --- Determino Numerazione Pagina Lettere D'intento
        if this.oParentObject.w_TIPINT="V"
          * --- Select from ATTIDETT
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
                +" where ATFLREGI='S' and ATCODATT="+cp_ToStrODBC(this.oParentObject.w_ATTPRI)+"";
                 ,"_Curs_ATTIDETT")
          else
            select * from (i_cTable);
             where ATFLREGI="S" and ATCODATT=this.oParentObject.w_ATTPRI;
              into cursor _Curs_ATTIDETT
          endif
          if used('_Curs_ATTIDETT')
            select _Curs_ATTIDETT
            locate for 1=1
            do while not(eof())
            this.oParentObject.w_PREFIS = NVL(_Curs_ATTIDETT.ATPREFIS, SPACE(20))
            this.oParentObject.w_PRPARI = NVL(_Curs_ATTIDETT.ATPRPARI, 0)
            this.oParentObject.w_INTLIG = NVL(_Curs_ATTIDETT.ATSTAINT, SPACE(1))
              select _Curs_ATTIDETT
              continue
            enddo
            use
          endif
        else
          if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
            GSCG_BLU (this,"R")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_PREFIS = this.w_NRPREFIS
            this.oParentObject.w_PRPARI = this.w_NRPAGINT
          else
            * --- Read from AZIENDA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AZIENDA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AZPREFIS"+;
                " from "+i_cTable+" AZIENDA where ";
                    +"AZCODAZI = "+cp_ToStrODBC(this.w_codazi);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AZPREFIS;
                from (i_cTable) where;
                    AZCODAZI = this.w_codazi;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_PREFIS = NVL(cp_ToDate(_read_.AZPREFIS),cp_NullValue(_read_.AZPREFIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_PRPARI = this.oParentObject.w_PAGINT
          endif
          this.oParentObject.w_INTLIG = "S"
        endif
      case this.pTIPO=5
        * --- Controlli che impediscono la variazione o la cancellazione
        this.bUpdateParentObject = .F.
        this.oParentObject.w_HASEVENT = .t.
        do case
          case UPPER(this.oParentObject.w_HASEVCOP)="ECPEDIT"
            do case
              case this.oParentObject.w_DIFLSTAM = "R"
                * --- Dichiarazione stampata sul registro
                this.w_MESS = "Dichiarazione stampata sul registro. Confermi variazione?"
                this.oParentObject.w_HASEVENT = ah_YesNo( this.w_MESS )
              case this.oParentObject.w_DIFLSTAM = "S"
                * --- Dichiarazione stampata in definitiva
                this.w_MESS = "Dichiarazione gia' stampata. Confermi variazione ? "
                if .not. AH_YESNO(this.w_MESS)
                  this.oParentObject.w_HASEVENT = .f.
                else
                  this.oParentObject.w_DIFLSTAM = "N"
                endif
            endcase
          case UPPER(this.oParentObject.w_HASEVCOP)="ECPDELETE"
            * --- CANCELLAZIONE:
            * --- ===============
            * --- Se stampata in definitiva chiede conferma.
            * --- Se stampata sul registro impedisce l'operazione.
            * --- ===============
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESSTAINT"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
                    +" and ESCODESE = "+cp_ToStrODBC(g_codese);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESSTAINT;
                from (i_cTable) where;
                    ESCODAZI = i_codazi;
                    and ESCODESE = g_codese;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DATBLO = NVL(cp_ToDate(_read_.ESSTAINT),cp_NullValue(_read_.ESSTAINT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_DATDIC = this.oParentObject.w_ODATDIC
            if this.oParentObject.w_DIFLSTAM = "R"
              * --- Stampata sul registro
              this.w_MESS = "Dichiarazione stampata sul registro. Cancellazione non consentita"
              ah_errormsg(this.w_MESS)
              this.oParentObject.w_HASEVENT = .f.
            endif
            * --- Verifica se c � la comunicazione
            if this.oParentObject.w_DITIPCON="F" AND this.oParentObject.w_DITIPIVA="S" AND Not empty (this.oparentobject.w_SERIALE)
              * --- Stampata in definitiva
              this.w_MESS = "Dichiarazione comunicata. Confermi la cancellazione?"
              this.oParentObject.w_HASEVENT = Ah_YesNo( this.w_MESS )
              if not this.oParentObject.w_HASEVENT
                i_retcode = 'stop'
                return
              endif
            endif
            if this.oParentObject.w_DIFLSTAM = "S"
              * --- Stampata in definitiva
              this.w_MESS = "Dichiarazione gi� stampata. Confermi la cancellazione?"
              this.oParentObject.w_HASEVENT = ah_YesNo( this.w_MESS )
            endif
            * --- Impedisco la cancellazione di una dichiarazione utilizzata nei documenti
            this.w_DOCSER = ""
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVSERIAL"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVRIFDIC = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVSERIAL;
                from (i_cTable) where;
                    MVRIFDIC = this.oParentObject.w_DISERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DOCSER = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if !Empty( this.w_DOCSER )
              this.w_MESS = "Dichiarazione utilizzata. Cancellazione non consentita"
              ah_errormsg(this.w_MESS)
              this.oParentObject.w_HASEVENT = .f.
            else
              * --- Controllo se la dichiarazione di intento che si intende cancellare sia stata
              *     collegata ad un'altra dichiarazione e non sia stata ancora movimentata
              *     sui documenti
              * --- Select from DICDINTE5
              do vq_exec with 'DICDINTE5',this,'_Curs_DICDINTE5','',.f.,.t.
              if used('_Curs_DICDINTE5')
                select _Curs_DICDINTE5
                locate for 1=1
                do while not(eof())
                this.w_MESS = "Dichiarazione utilizzata. Cancellazione non consentita"
                ah_errormsg(this.w_MESS)
                this.oParentObject.w_HASEVENT = .f.
                exit
                  select _Curs_DICDINTE5
                  continue
                enddo
                use
              endif
            endif
        endcase
      case this.pTIPO=6
        this.w_SERIALE = IIF( this.w_GSCG_ADI.cFunction="Load", Repl("X" ,15) , this.oParentObject.w_DISERIAL )
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO=1
        * --- Stampa della dichiarazione alla fine del caricamento o della variazione
        if this.oParentObject.w_DITIPCON="F" .and. this.oParentObject.w_DIFLSTAM="N" AND this.oParentObject.w_DITIPIVA<>"S"
          * --- Emessa a fornitore
          this.w_MESS = "Vuoi stampare la dichiarazione di intento?"
          if NOT AH_YESNO(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
          * --- Test persona fisica
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZPERAZI,AZPIVAZI"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZPERAZI,AZPIVAZI;
              from (i_cTable) where;
                  AZCODAZI = this.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERSO = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
            this.w_PARIVA = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ERRORE = .T.
          if this.w_PERSO="S"
            * --- Nella dichiarazione di intento stampo solo i dati del titolare
            * --- Lancio la query Titolari per avere i dati del titolare da stampare sulla dichiarazione.
            vq_exec("query\titolari",this,"titolari")
            if Reccount("Titolari")>0
              Select Titolari
              * --- Prendo i dati solo del primo record
              Go Top
              L_COGNOME=NVL(TITOLARI.TTCOGTIT,"")
              L_NOME=NVL(TITOLARI.TTNOMTIT,"")
              L_DATNAS=Cp_todate(TITOLARI.TTDATNAS)
              L_SESSO=NVL(TITOLARI.TT_SESSO,"")
              L_LUONAS=NVL(TITOLARI.TTLUONAS,"")
              L_PRONAS=NVL(TITOLARI.TTPRONAS,"")
              L_PERSO="S"
            else
              ah_ErrorMsg("Attenzione: dati del titolare non inseriti")
              this.w_ERRORE = .F.
            endif
          else
            * --- Nella dichiarazione di intento stampo i dati dell'azienda e del titolare
            * --- Lancio la query Titolari per avere i dati del titolare da stampare sulla dichiarazione.
            L_COGNOME=""
            L_NOME=""
            L_DATNAS=cp_CharToDate("  -  -  ")
            L_SESSO=""
            L_LUONAS=""
            L_PRONAS=""
            L_PERSO="N"
          endif
          * --- Lancio la stampa della Dichiarazione appena salvata
          L_PARIVA=this.w_PARIVA
          if this.w_ERRORE=.T.
            i_FAXNO = this.oParentObject.w_TELFAX
            i_EMAIL = this.oParentObject.w_EMAIL
            i_EMAIL_PEC = this.oParentObject.w_EMPEC
            i_DEST=this.oParentObject.w_DESCON
            i_CLIFORDES =this.oParentObject.w_DICODICE
            vx_exec("query\gscg_qin.vqr",this)
            i_FAXNO = " "
            i_EMAIL = " "
            i_EMAIL_PEC = " "
            i_DEST=" "
            i_CLIFORDES =" "
            * --- Richiesta conferma aggiornamento
            this.w_MESS = "Confermi l'aggiornamento della dichiarazione come stampata?"
            if AH_YESNO(this.w_MESS)
              * --- Write into DIC_INTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_INTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DIFLSTAM ="+cp_NullLink(cp_ToStrODBC("S"),'DIC_INTE','DIFLSTAM');
                    +i_ccchkf ;
                +" where ";
                    +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
                       )
              else
                update (i_cTable) set;
                    DIFLSTAM = "S";
                    &i_ccchkf. ;
                 where;
                    DISERIAL = this.oParentObject.w_DISERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            i_retcode = 'stop'
            return
          endif
        endif
    endcase
  endproc
  proc Try_04DC6218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scrittura data negli esercizi
    * --- Write into ESERCIZI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ESERCIZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESSTAINT ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'ESERCIZI','ESSTAINT');
          +i_ccchkf ;
      +" where ";
          +"ESCODAZI = "+cp_ToStrODBC(this.w_CAZI);
          +" and ESCODESE = "+cp_ToStrODBC(this.w_CESE);
             )
    else
      update (i_cTable) set;
          ESSTAINT = this.w_DATA;
          &i_ccchkf. ;
       where;
          ESCODAZI = this.w_CAZI;
          and ESCODESE = this.w_CESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiornamento dichiarazioni d'intento
    * --- Select from GSCG_SIR
    do vq_exec with 'GSCG_SIR',this,'_Curs_GSCG_SIR','',.f.,.t.
    if used('_Curs_GSCG_SIR')
      select _Curs_GSCG_SIR
      locate for 1=1
      do while not(eof())
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIFLSTAM ="+cp_NullLink(cp_ToStrODBC("R"),'DIC_INTE','DIFLSTAM');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(_Curs_gscg_sir.DISERIAL);
               )
      else
        update (i_cTable) set;
            DIFLSTAM = "R";
            &i_ccchkf. ;
         where;
            DISERIAL = _Curs_gscg_sir.DISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSCG_SIR
        continue
      enddo
      use
    endif
    this.oParentObject.w_PRPARI = this.w_PAGINE
    if this.oParentObject.w_TIPINT="V"
      * --- Se numerazione non separata
      *     inserisco il progressivo pagina nelle Attivit� su registro Vendita Principale
      if this.oParentObject.w_INTLIG="S"
        * --- Aggiorno il progressivo pagine solo se Attivo il check intestazione
        * --- Write into ATTIDETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI),'ATTIDETT','ATPRPARI');
              +i_ccchkf ;
          +" where ";
              +"ATFLREGI = "+cp_ToStrODBC("S");
              +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                 )
        else
          update (i_cTable) set;
              ATPRPARI = this.oParentObject.w_PRPARI;
              &i_ccchkf. ;
           where;
              ATFLREGI = "S";
              and ATCODATT = this.w_CODATT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      * --- Aggiorno Progressivo Specifico delle lettere d'intento nei Parametri Iva
      if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
        this.w_PRPAG = this.oParentObject.w_PRPARI
        if this.oParentObject.w_INTLIG <>"N"
          GSCG_BLU (this,"W")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZPAGINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI),'AZIENDA','AZPAGINT');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZPAGINT = this.oParentObject.w_PRPARI;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla checkform fuori transazione
    do case
      case Not CHKLINTE(this.oParentObject.w_DIDATINI,this.oParentObject.w_DIDATFIN,this.oParentObject.w_DIDATDIC,.F.) and this.oParentObject.w_DITIPOPE="D"
         
 L_A=CHKLINTE(this.oParentObject.w_DIDATINI,this.oParentObject.w_DIDATFIN,this.oParentObject.w_DIDATDIC,.T.)
        if YEAR(this.oParentObject.w_DIDATDIC)<>YEAR(this.oParentObject.w_DIDATFIN)
          this.w_OBJCTRL = this.w_GSCG_ADI.GetCtrl( "w_DIDATFIN" )
        else
          this.w_OBJCTRL = this.w_GSCG_ADI.GetCtrl( "w_DIDATINI" )
        endif
        this.w_OBJCTRL.Setfocus()     
      case ((this.oParentObject.w_DITIPOPE="I" And this.oParentObject.w_DIFLSTAM<>"R" ) OR this.oParentObject.w_DITIPOPE="O") AND this.oParentObject.w_DIIMPDIC=0
        this.w_MESS = "Scrivere un importo diverso da zero"
        ah_errormsg(this.w_MESS)
        this.w_OBJCTRL = this.w_GSCG_ADI.GetCtrl( "w_DIIMPDIC" )
        this.oParentObject.w_RESCHK = -1
        this.w_OBJCTRL.Setfocus()     
        i_retcode = 'stop'
        return
    endcase
    * --- Verifica inserimento Cliente oppure fornitore/dogana
    if (this.oParentObject.w_DITIPCON="C" AND EMPTY(this.oParentObject.w_DICODICE)) OR (this.oParentObject.w_DITIPCON="F" AND EMPTY(this.oParentObject.w_DICODICE) AND EMPTY(this.oParentObject.w_DIDOGANA))
      this.w_MESS = "Specificare il cliente o il fornitore (dogana)"
      ah_errormsg(this.w_MESS)
      this.oParentObject.w_RESCHK = -1
      i_retcode = 'stop'
      return
    endif
    * --- Legge la data dell'ultima stampa del registro
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESSTAINT"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and ESCODESE = "+cp_ToStrODBC(g_codese);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESSTAINT;
        from (i_cTable) where;
            ESCODAZI = i_codazi;
            and ESCODESE = g_codese;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.ESSTAINT),cp_NullValue(_read_.ESSTAINT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Imposta la data del documento, se in caricamento quanto impostato 
    *     attualmente se in modifica il valore originale sul database...
    if this.w_GSCG_ADI.cFunction="Load"
      this.w_DATDIC = this.oParentObject.w_DIDATDIC
    else
      this.w_DATDIC = this.oParentObject.w_ODATDIC
    endif
    * --- Controllo data ultima stampa registro
    * --- La dichiarazione che si sta cancellando modificando o inserendo ha la data inferiore all'ultima stampata
    if this.w_DATDIC<this.w_DATBLO and this.oParentObject.w_DITIPIVA="S"
      this.w_MESS = "La dichiarazione ha data inferiore all'ultima stampa dei registri%0Confermi?"
      if NOT AH_YESNO(this.w_MESS)
        this.oParentObject.w_RESCHK = -1
        i_retcode = 'stop'
        return
      endif
    endif
    * --- INSERIMENTO O VARIAZIONE:
    this.w_SERIALE = IIF( this.w_GSCG_ADI.cFunction="Load", Repl("X" ,15) , this.oParentObject.w_DISERIAL )
    if this.oParentObject.w_DITIPCON="F"
      * --- Controllo che la data sia maggiore di tutte le emissioni con date inferiori
      * --- Select from DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) AS CONTA  from "+i_cTable+" DIC_INTE ";
            +" where DIDATDIC<"+cp_ToStrODBC(this.oParentObject.w_DIDATDIC)+" AND   DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_DI__ANNO)+" AND   DITIPCON='F' AND DITIPIVA="+cp_ToStrODBC(this.oParentObject.w_DITIPIVA)+" AND   DISERDIC="+cp_ToStrODBC(this.oParentObject.w_DISERDIC)+" AND   "+cp_ToStrODBC(this.oParentObject.w_DINUMDIC)+"<=DINUMDIC";
             ,"_Curs_DIC_INTE")
      else
        select Count(*) AS CONTA from (i_cTable);
         where DIDATDIC<this.oParentObject.w_DIDATDIC AND   DI__ANNO=this.oParentObject.w_DI__ANNO AND   DITIPCON="F" AND DITIPIVA=this.oParentObject.w_DITIPIVA AND   DISERDIC=this.oParentObject.w_DISERDIC AND   this.oParentObject.w_DINUMDIC<=DINUMDIC;
          into cursor _Curs_DIC_INTE
      endif
      if used('_Curs_DIC_INTE')
        select _Curs_DIC_INTE
        locate for 1=1
        do while not(eof())
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_DIC_INTE
          continue
        enddo
        use
      endif
      * --- Controllo che il numero sia minore di tutti quelli con la data maggiore
      * --- Select from DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) AS Conta  from "+i_cTable+" DIC_INTE ";
            +" where DIDATDIC>"+cp_ToStrODBC(this.oParentObject.w_DIDATDIC)+" AND   DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_DI__ANNO)+" AND   DITIPCON='F' AND   DITIPIVA='S' AND   DISERDIC="+cp_ToStrODBC(this.oParentObject.w_DISERDIC)+" and   "+cp_ToStrODBC(this.oParentObject.w_DINUMDIC)+">=DINUMDIC";
             ,"_Curs_DIC_INTE")
      else
        select Count(*) AS Conta from (i_cTable);
         where DIDATDIC>this.oParentObject.w_DIDATDIC AND   DI__ANNO=this.oParentObject.w_DI__ANNO AND   DITIPCON="F" AND   DITIPIVA="S" AND   DISERDIC=this.oParentObject.w_DISERDIC and   this.oParentObject.w_DINUMDIC>=DINUMDIC;
          into cursor _Curs_DIC_INTE
      endif
      if used('_Curs_DIC_INTE')
        select _Curs_DIC_INTE
        locate for 1=1
        do while not(eof())
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_DIC_INTE
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_RESCHK = 0
      * --- Verifica univocita' documento - ElimIno dai candidati il documento corrente
      *     se sono in modifica, in caricamento il seriale non � ancora definitivo 
      *     (sono fuori transazione)
      * --- Select from DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DIC_INTE ";
            +" where DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_DI__ANNO)+" AND   DITIPCON="+cp_ToStrODBC(this.oParentObject.w_DITIPCON)+" AND   DIDATDIC="+cp_ToStrODBC(this.oParentObject.w_DIDATDIC)+" AND   DITIPIVA="+cp_ToStrODBC(this.oParentObject.w_DITIPIVA)+" AND   DISERDIC="+cp_ToStrODBC(this.oParentObject.w_DISERDIC)+" AND   DINUMDIC="+cp_ToStrODBC(this.oParentObject.w_DINUMDIC)+" AND   DISERIAL<>"+cp_ToStrODBC(this.w_SERIALE)+"";
             ,"_Curs_DIC_INTE")
      else
        select Count(*) As Conta from (i_cTable);
         where DI__ANNO=this.oParentObject.w_DI__ANNO AND   DITIPCON=this.oParentObject.w_DITIPCON AND   DIDATDIC=this.oParentObject.w_DIDATDIC AND   DITIPIVA=this.oParentObject.w_DITIPIVA AND   DISERDIC=this.oParentObject.w_DISERDIC AND   DINUMDIC=this.oParentObject.w_DINUMDIC AND   DISERIAL<>this.w_SERIALE;
          into cursor _Curs_DIC_INTE
      endif
      if used('_Curs_DIC_INTE')
        select _Curs_DIC_INTE
        locate for 1=1
        do while not(eof())
        if Nvl( _Curs_DIC_INTE.CONTA ,0 )>0
          this.w_MESS = "Lettera d'intento gi� inserita"
          ah_errormsg(this.w_MESS)
          this.oParentObject.w_RESCHK = -1
        endif
          select _Curs_DIC_INTE
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_RESCHK = 0
      * --- Controllo di univocit� del protocollo (Progressivo per anno)
      if (this.oParentObject.w_DITIPCON="F" And this.w_GSCG_ADI.cFunction="Load") Or this.oParentObject.w_DITIPCON="C"
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DIC_INTE ";
              +" where DITIPCON="+cp_ToStrODBC(this.oParentObject.w_DITIPCON)+" AND   DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_DI__ANNO)+" AND   DINUMDIC="+cp_ToStrODBC(this.oParentObject.w_DINUMDIC)+" AND   DISERDIC="+cp_ToStrODBC(this.oParentObject.w_DISERDIC)+" AND   DISERIAL<>"+cp_ToStrODBC(this.w_SERIALE)+"";
               ,"_Curs_DIC_INTE")
        else
          select Count(*) As Conta from (i_cTable);
           where DITIPCON=this.oParentObject.w_DITIPCON AND   DI__ANNO=this.oParentObject.w_DI__ANNO AND   DINUMDIC=this.oParentObject.w_DINUMDIC AND   DISERDIC=this.oParentObject.w_DISERDIC AND   DISERIAL<>this.w_SERIALE;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          if Nvl( _Curs_DIC_INTE.CONTA ,0 )>0
            this.w_MESS = "Numero protocollo gi� esistente%0Confermi?"
            if NOT AH_YESNO(this.w_MESS)
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
          endif
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
      endif
      if this.oParentObject.w_DITIPCON="F" And this.oParentObject.w_DITIPOPE="D"
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DIC_INTE ";
              +" where DITIPOPE="+cp_ToStrODBC(this.oParentObject.w_DITIPOPE)+" AND   DITIPCON="+cp_ToStrODBC(this.oParentObject.w_DITIPCON)+" AND   DIDATINI="+cp_ToStrODBC(this.oParentObject.w_DIDATINI)+" AND   DIDATFIN="+cp_ToStrODBC(this.oParentObject.w_DIDATFIN)+" AND   DICODICE="+cp_ToStrODBC(this.oParentObject.w_DICODICE)+" AND   DISERIAL<>"+cp_ToStrODBC(this.w_SERIALE)+"";
               ,"_Curs_DIC_INTE")
        else
          select Count(*) As Conta from (i_cTable);
           where DITIPOPE=this.oParentObject.w_DITIPOPE AND   DITIPCON=this.oParentObject.w_DITIPCON AND   DIDATINI=this.oParentObject.w_DIDATINI AND   DIDATFIN=this.oParentObject.w_DIDATFIN AND   DICODICE=this.oParentObject.w_DICODICE AND   DISERIAL<>this.w_SERIALE;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          if Nvl( _Curs_DIC_INTE.CONTA ,0 )>0
            this.w_MESS = 'Esiste una dichiarazione di intento associata allo stesso fornitore%0con le stesse date di inizio/fine validit�%0e come tipo operazione "Definizione periodo"'
            ah_errormsg(this.w_MESS)
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
      endif
    endif
    * --- Se superati i controlli bloccanti si effettua controllo che numero documento inserito, non sia gi� stato usato
    *     in una lettera di intento precedente. Per lo stesso fornitore
    if this.oParentObject.w_RESCHK = 0
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Nvl( _Curs_DIC_INTE.CONTA ,0 )>0
      this.w_MESS = "Il numero della lettera d'intento � fuori sequenza%0Confermi?"
      if NOT AH_YESNO(this.w_MESS)
        this.oParentObject.w_RESCHK = -1
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che il numero documento inserito, non sia gi� stato usato
    *     in una lettera di intento precedente. Per lo stesso fornitore o cliente
    * --- Select from DIC_INTE
    i_nConn=i_TableProp[this.DIC_INTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DINUMDOC,DISERDOC  from "+i_cTable+" DIC_INTE ";
          +" where DISERIAL<>"+cp_ToStrODBC(this.w_SERIALE)+" AND   DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_DI__ANNO)+" AND   DITIPCON="+cp_ToStrODBC(this.oParentObject.w_DITIPCON)+" AND   DICODICE="+cp_ToStrODBC(this.oParentObject.w_DICODICE)+" AND   DINUMDOC="+cp_ToStrODBC(this.oParentObject.w_DINUMDOC)+" AND   DISERDOC="+cp_ToStrODBC(this.oParentObject.w_DISERDOC)+"";
           ,"_Curs_DIC_INTE")
    else
      select DINUMDOC,DISERDOC from (i_cTable);
       where DISERIAL<>this.w_SERIALE AND   DI__ANNO=this.oParentObject.w_DI__ANNO AND   DITIPCON=this.oParentObject.w_DITIPCON AND   DICODICE=this.oParentObject.w_DICODICE AND   DINUMDOC=this.oParentObject.w_DINUMDOC AND   DISERDOC=this.oParentObject.w_DISERDOC;
        into cursor _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      select _Curs_DIC_INTE
      locate for 1=1
      do while not(eof())
      this.w_MESS = "Lettera d'intento con numero documento %1/%2 gi� inserita"
      ah_errormsg(this.w_MESS,48,,ALLTRIM(STR(this.oParentObject.w_DINUMDOC)),ALLTRIM(this.oParentObject.w_DISERDOC))
      i_retcode = 'stop'
      return
        select _Curs_DIC_INTE
        continue
      enddo
      use
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Ah_Yesno("Confermi l'aggiornamento delle dichiarazioni?")
      * --- Try
      local bErr_04D88080
      bErr_04D88080=bTrsErr
      this.Try_04D88080()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if i_ErrMsg <> "<No message>"
          ah_errormsg("Non � possibile aggiornare le dichiarazioni d'intento")
        endif
      endif
      bTrsErr=bTrsErr or bErr_04D88080
      * --- End
    endif
  endproc
  proc Try_04D88080()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT (this.w_NomeCursore) 
 GO TOP
    do while NOT EOF()
      this.w_SERIALE_DICH = DISERIAL
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIFLSTAM ="+cp_NullLink(cp_ToStrODBC("S"),'DIC_INTE','DIFLSTAM');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_SERIALE_DICH);
               )
      else
        update (i_cTable) set;
            DIFLSTAM = "S";
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_SERIALE_DICH;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT (this.w_NomeCursore) 
 SKIP 1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='DIC_INTE'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='ATTIDETT'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='TITOLARI'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_TITOLARI')
      use in _Curs_TITOLARI
    endif
    if used('_Curs_GSCG_SIR')
      use in _Curs_GSCG_SIR
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_DICDINTE5')
      use in _Curs_DICDINTE5
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
