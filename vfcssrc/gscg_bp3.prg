* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bp3                                                        *
*              Carica automatismi contabili                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2014-02-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bp3",oParentObject)
return(i_retval)

define class tgscg_bp3 as StdBatch
  * --- Local variables
  w_NUMERO = 0
  w_PADRE = .NULL.
  w_TIPCOP = space(1)
  w_APPO = space(5)
  w_TESTIND = .f.
  w_ALMRIG = .f.
  w_OBJECT = .NULL.
  w_DATOBSO = ctod("  /  /  ")
  RI = space(10)
  RC = space(10)
  w_CODET = space(15)
  w_INPO = 0
  w_DIFDOC = 0
  w_OKDET = .f.
  w_VALO = 0
  w_TOTDOC = 0
  w_ININD = 0
  w_OREC = 0
  w_DIFDOC = 0
  w_INDET = 0
  w_DATRIF = ctod("  /  /  ")
  w_MESS = space(10)
  w_CFLOMA = space(1)
  w_DAVE = space(1)
  w_OK = .f.
  w_COIND = space(15)
  w_INDICE = 0
  w_INDIC1 = 0
  w_OKIND = .f.
  w_AGG1 = .f.
  w_PERIND = 0
  w_IMPONI = 0
  w_CONTRO = space(15)
  w_OKCON = .f.
  w_TESTCO = .f.
  w_CAURIG = space(5)
  w_DTOBSO1 = ctod("  /  /  ")
  w_TIPREG = space(1)
  w_COUNT = 0
  w_FLACBD = space(1)
  w_LCODCON = space(15)
  w_GSCG_MIV = .NULL.
  w_INIT = .f.
  w_OBJECT = .NULL.
  w_MOCAURIG = space(5)
  w_MOFLDAVE = space(1)
  w_MOTIPCON = space(1)
  w_MOCODCON = space(15)
  w_MOCAUMOV = space(5)
  w_MOCONBAN = space(15)
  w_MOFLPART = space(1)
  w_MOFLAUTO = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  CAUPRI_idx=0
  CAUPRI1_idx=0
  CAU_CONT_idx=0
  SALDICON_idx=0
  COC_MAST_idx=0
  BAN_CONTI_idx=0
  PAG_AMEN_idx=0
  MOD_CONT_idx=0
  CAUIVA1_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gli Importi provenienti dal Documento e dalle Righe IVA nel File Registrazioni Contabili (da GSCG_MPN)
    * --- Variabili per C\C
    * --- Variabili definite ed utilizzate per il modulo ritenute
    this.w_PADRE = this.oParentObject
    this.oParentObject.w_TOTDAR = 0
    this.oParentObject.w_TOTAVE = 0
    * --- Se sono in modifica rileggo la variabile w_CHKAUT
    if this.w_PADRE.cFunction="Edit"
      * --- Variato Input su CliFor - Verifica se esiste un Modello Associato
      * --- w_CHKAUT: 0 =No Automatismi; 1 =Automatismo su Causale; 2 =Automatismo su Modello
      this.w_APPO = "###############"
      * --- Read from MOD_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2],.t.,this.MOD_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCODCON"+;
          " from "+i_cTable+" MOD_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCLF);
              +" and CCCODCON = "+cp_ToStrODBC(this.oParentObject.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCODCON;
          from (i_cTable) where;
              CCCODICE = this.oParentObject.w_PNCODCAU;
              and CCTIPCON = this.oParentObject.w_PNTIPCLF;
              and CCCODCON = this.oParentObject.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.CCCODCON),cp_NullValue(_read_.CCCODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows<>0
        this.oParentObject.w_CHKAUT = 2
      else
        * --- Legge Automatismi legati alla Causale
        * --- w_CHKAUT: 0 =No Automatismi; 1 =Automatismo su Causale
        this.w_APPO = "#####"
        this.oParentObject.w_CHKAUT = 0
        * --- Read from CAUPRI1
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "APCODCAU"+;
            " from "+i_cTable+" CAUPRI1 where ";
                +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            APCODCAU;
            from (i_cTable) where;
                APCODCAU = this.oParentObject.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO = NVL(cp_ToDate(_read_.APCODCAU),cp_NullValue(_read_.APCODCAU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_CHKAUT = IIF(i_rows<>0, 1, 0)
        if this.oParentObject.w_CHKAUT=0 AND this.oParentObject.w_PNTIPREG<>"N"
          this.w_APPO = "#####"
          * --- Read from CAUIVA1
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUIVA1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AICODCAU"+;
              " from "+i_cTable+" CAUIVA1 where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AICODCAU;
              from (i_cTable) where;
                  AICODCAU = this.oParentObject.w_PNCODCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.AICODCAU),cp_NullValue(_read_.AICODCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_CHKAUT = IIF(i_rows<>0, 1, 0)
        endif
      endif
    endif
    if Not ( this.oParentObject.w_GIACAR=" " AND this.oParentObject.w_CHKAUT>0 )
      i_retcode = 'stop'
      return
    else
      * --- Prima di applicare l'automatismo, se sono in modifica, verifico
      *     se esisitono righe provenienti da Cont. indiretta
      * --- Cancello tutte le righe del transitorio
      this.w_ALMRIG = .F.
      this.w_PADRE.FirstRow()     
      this.w_PADRE.SetRow()     
      this.w_PADRE.MarkPos()     
      do while Not this.w_PADRE.Eof_Trs() And Not this.w_TESTIND
        this.w_PADRE.SetRow()     
        if this.w_PADRE.FULLROW()
          this.w_ALMRIG = .T.
        endif
        if NOT EMPTY( this.oParentObject.w_PNRIFDIS ) AND VAL( this.oParentObject.w_PNRIFDIS )<0
          ah_ErrorMsg("Impossibile applicare automatismo su righe di registrazioni da contabilizzazione indiretta",,"")
          this.w_TESTIND = .T.
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos()     
      if this.w_TESTIND or (this.w_ALMRIG AND !AH_YESNO("Si desidera riapplicare l'automatismo contabile?"))
        * --- Esco..
        if this.w_ALMRIG
          this.w_OBJECT = this.w_PADRE.GETBODYCTRL("w_PNTIPCON")
          this.w_OBJECT.setfocus()     
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_MESS = ""
    do case
      case this.oParentObject.w_CHKAUT=1
        * --- Select from CAUPRI1
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select APTIPCON,APCODCON  from "+i_cTable+" CAUPRI1 ";
              +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+"";
               ,"_Curs_CAUPRI1")
        else
          select APTIPCON,APCODCON from (i_cTable);
           where APCODCAU=this.oParentObject.w_PNCODCAU;
            into cursor _Curs_CAUPRI1
        endif
        if used('_Curs_CAUPRI1')
          select _Curs_CAUPRI1
          locate for 1=1
          do while not(eof())
          this.w_MOTIPCON = NVL(_Curs_CAUPRI1.APTIPCON, " ")
          this.w_MOCODCON = NVL(_Curs_CAUPRI1.APCODCON, SPACE(15))
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDTOBSO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MOTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MOCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDTOBSO;
              from (i_cTable) where;
                  ANTIPCON = this.w_MOTIPCON;
                  and ANCODICE = this.w_MOCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_DATOBSO) AND this.w_DATOBSO<=this.oParentObject.w_PNDATREG
            this.w_MESS = "Conto"+" "+ALLTRIM(this.w_MOCODCON)+ " " +"Obsoleto."+chr(13) +"Impossibile applicare il modello!"
            EXIT
          endif
            select _Curs_CAUPRI1
            continue
          enddo
          use
        endif
      case this.oParentObject.w_CHKAUT=2
        * --- Select from CAUPRI
        i_nConn=i_TableProp[this.CAUPRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2],.t.,this.CAUPRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select APTIPCON,APCODCON  from "+i_cTable+" CAUPRI ";
              +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+" AND APTIPCLF="+cp_ToStrODBC(this.oParentObject.w_PNTIPCLF)+" AND APCODCLF="+cp_ToStrODBC(this.oParentObject.w_PNCODCLF)+"";
               ,"_Curs_CAUPRI")
        else
          select APTIPCON,APCODCON from (i_cTable);
           where APCODCAU=this.oParentObject.w_PNCODCAU AND APTIPCLF=this.oParentObject.w_PNTIPCLF AND APCODCLF=this.oParentObject.w_PNCODCLF;
            into cursor _Curs_CAUPRI
        endif
        if used('_Curs_CAUPRI')
          select _Curs_CAUPRI
          locate for 1=1
          do while not(eof())
          this.w_MOTIPCON = NVL(_Curs_CAUPRI.APTIPCON, " ")
          this.w_MOCODCON = NVL(_Curs_CAUPRI.APCODCON, SPACE(15))
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDTOBSO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MOTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MOCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDTOBSO;
              from (i_cTable) where;
                  ANTIPCON = this.w_MOTIPCON;
                  and ANCODICE = this.w_MOCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_DATOBSO) AND this.w_DATOBSO<this.oParentObject.w_PNDATREG
            this.w_MESS = "Conto"+" "+ALLTRIM(this.w_MOCODCON)+ " " +"Obsoleto."+chr(13) +"Impossibile applicare il modello!"
            EXIT
          endif
            select _Curs_CAUPRI
            continue
          enddo
          use
        endif
    endcase
    if NOT EMPTY(this.w_MESS)
      do CP_ERRORMSG WITH this.w_MESS,,"ATTENZIONE"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Vettore contenente i Dati dei Conti e Importi provenienti dalle Righe IVA
    DIMENSION RI[50,4],RC[50,4]
    * --- Indici Vettori
    this.w_INDIC1 = 0
    this.w_INDICE = 0
    this.w_TESTCO = .T.
    * --- Inizializza il Vettore
    FOR I = 1 TO 50
    RI[I, 1] = SPACE(15)
    RI[I, 2] = 0
    RI[I, 3] = " "
    RI[I, 4] = " "
    ENDFOR
    if this.oParentObject.w_DETCON="S"
      do case
        case this.oParentObject.w_CHKAUT=1 
          * --- Select from CAUPRI1
          i_nConn=i_TableProp[this.CAUPRI1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select APTIPCON,APCODCON,APFLPART  from "+i_cTable+" CAUPRI1 ";
                +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+" and APTIPCON='G' and APFLAUTO='T'";
                 ,"_Curs_CAUPRI1")
          else
            select APTIPCON,APCODCON,APFLPART from (i_cTable);
             where APCODCAU=this.oParentObject.w_PNCODCAU and APTIPCON="G" and APFLAUTO="T";
              into cursor _Curs_CAUPRI1
          endif
          if used('_Curs_CAUPRI1')
            select _Curs_CAUPRI1
            locate for 1=1
            do while not(eof())
            this.w_LCODCON = Nvl(_Curs_CAUPRI1.APCODCON,Space(15))
              select _Curs_CAUPRI1
              continue
            enddo
            use
          endif
        case this.oParentObject.w_CHKAUT=2 
          * --- Select from CAUPRI
          i_nConn=i_TableProp[this.CAUPRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2],.t.,this.CAUPRI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select APTIPCON,APCODCON,APFLAUTO  from "+i_cTable+" CAUPRI ";
                +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+" AND APTIPCLF="+cp_ToStrODBC(this.oParentObject.w_PNTIPCLF)+" AND APCODCLF="+cp_ToStrODBC(this.oParentObject.w_PNCODCLF)+" and APTIPCON='G' and APFLAUTO='T'";
                 ,"_Curs_CAUPRI")
          else
            select APTIPCON,APCODCON,APFLAUTO from (i_cTable);
             where APCODCAU=this.oParentObject.w_PNCODCAU AND APTIPCLF=this.oParentObject.w_PNTIPCLF AND APCODCLF=this.oParentObject.w_PNCODCLF and APTIPCON="G" and APFLAUTO="T";
              into cursor _Curs_CAUPRI
          endif
          if used('_Curs_CAUPRI')
            select _Curs_CAUPRI
            locate for 1=1
            do while not(eof())
            this.w_LCODCON = Nvl(_Curs_CAUPRI.APCODCON,Space(15))
              select _Curs_CAUPRI
              continue
            enddo
            use
          endif
      endcase
    endif
    this.w_PADRE = this.oParentObject
    this.w_GSCG_MIV = this.w_PADRE.GSCG_MIV
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
     
 SELECT * FROM ( this.w_GSCG_MIV.cTrsName) WHERE t_IVCODIVA<>SPACE(5) AND (t_IVIMPONI<>0 OR ; 
 t_IVIMPIVA<>0 OR t_IVCFLOMA="S") INTO CURSOR TEMP NOFILTER
    b= WRCURSOR("TEMP")
    SELECT TEMP
    GO TOP
    SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," "))
    this.w_CFLOMA = t_IVCFLOMA
    this.w_DTOBSO1 = NVL(t_DTOBSO,cp_CharToDate("  -  -    "))
    this.w_COIND = NVL(t_IVCODCOI,SPACE(15))
    this.w_CONTRO = IIF((this.w_DTOBSO1>this.oParentObject.w_PNDATREG OR EMPTY(this.w_DTOBSO1)),iif(Empty(NVL(t_IVCONTRO,SPACE(15))),this.w_LCODCON,NVL(t_IVCONTRO,SPACE(15))),SPACE(15))
    if NOT EMPTY(this.w_CONTRO) AND EMPTY(this.w_COIND) AND this.oParentObject.w_DETCON="S" AND NVL(t_IVPERIND,0)<>0
      REPLACE t_IVCODCOI WITH this.w_CONTRO
    endif
    if this.w_CFLOMA="E"
      REPLACE t_IVIMPIVA WITH 0
      REPLACE t_IVIMPONI WITH 0
    endif
    ENDSCAN
    * --- Raggruppo righe del dettaglio iva per calcolare iva indetraibile sul totale
     
 SELECT t_IVCODCON,t_IVCODIVA,t_IVCONTRO,t_IVCODCOI,t_IVPERIND,t_CTIPREG,SUM(t_IVIMPIVA) AS t_IVIMPIVA,SUM(cp_ROUND((t_IVIMPIVA*t_IVPERIND)/100,this.oParentObject.w_DECTOP)) AS t_ININD ; 
 FROM TEMP where Nvl(t_IVIMPIVA,0) <>0 GROUP BY t_IVCODIVA,t_IVCODCON,t_IVCODCOI,t_IVTIPREG INTO CURSOR TEMP1 NOFILTER
     
 SELECT TEMP 
 USE 
 SELECT TEMP1 
 GO TOP
    SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," "))
    this.w_CODET = t_IVCODCON
    this.w_COIND = NVL(t_IVCODCOI,SPACE(15))
    this.w_CONTRO = NVL(t_IVCONTRO,SPACE(15))
    this.w_TIPREG = NVL(t_CTIPREG," ")
    this.w_INPO = NVL( t_IVIMPIVA,0)
    this.w_PERIND = t_IVPERIND
    this.w_ININD = Nvl(t_ININD,0)
    this.w_INDET = IIF(EMPTY(this.w_CODET), 0 , this.w_INPO-this.w_ININD)
    this.w_OKCON = .F.
    * --- Nel caso di dettaglio iva per contropartite il conto  indetraibile potrebbe essere una delle contropartite gi� inserite
    if NOT EMPTY(this.w_COIND) AND this.oParentObject.w_DETCON="S"
      FOR I = 1 TO this.w_INDIC1
      if RC[I, 1]=this.w_COIND AND ( this.oParentObject.w_PNTIPREG=this.w_TIPREG)
        * --- Contropartite
        RC[I, 2] = RC[I, 2] + this.w_ININD
        this.w_OKCON = .T.
      endif
      ENDFOR
    endif
    * --- Cicla sugli Indici del Vettore
    this.w_OKDET = .F.
    this.w_OKIND = .F.
    FOR I = 1 TO this.w_INDICE
    if this.w_CODET<>SPACE(15) AND RI[I, 1]=this.w_CODET AND this.w_TIPREG=RI[I,4]
      * --- Conto IVA Detraibile
      RI[I, 2] = RI[I, 2] + this.w_INDET
      this.w_OKDET = .T.
    endif
    if this.w_COIND<>SPACE(15) AND RI[I, 1]=this.w_COIND AND this.w_TIPREG=RI[I,4]
      * --- Conto IVA Indetraibile
      RI[I, 2] = RI[I, 2] + this.w_ININD
      this.w_OKIND = .T.
    endif
    ENDFOR
    * --- Se non Trovato aggiunge nuovo indice nel Vettore
    if this.w_CODET<>SPACE(15) AND this.w_OKDET=.F.
      * --- Conto IVA Detraibile
      this.w_INDICE = this.w_INDICE + 1
      RI[this.w_INDICE, 1] = this.w_CODET
      RI[this.w_INDICE, 2] = this.w_INDET
      RI[this.w_INDICE, 3] = " "
      RI[this.w_INDICE, 4] = this.w_TIPREG
    endif
    if not empty(this.w_COIND) AND this.w_OKIND=.F. AND this.w_OKCON=.F.
      * --- Conto IVA Indetraibile
      this.w_INDICE = this.w_INDICE + 1
      RI[this.w_INDICE, 1] = this.w_COIND
      RI[this.w_INDICE, 2] = this.w_ININD
      RI[this.w_INDICE, 3] = "I"
      RI[this.w_INDICE, 4] = this.w_TIPREG
    endif
    SELECT TEMP1
    ENDSCAN
    if this.oParentObject.w_PNTIPREG<>"N" AND NOT EMPTY(this.oParentObject.w_PNTIPCLF) AND NOT EMPTY(this.oParentObject.w_PNCODCLF)
      if this.oParentObject.w_PNTOTDOC<>0 
        * --- Esegue il Controllo Cli/For con il Totale Documento in Valuta
        if this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL
          * --- Esegue Conversione 
          this.w_DATRIF = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
          this.w_TOTDOC = cp_ROUND(VAL2MON(this.oParentObject.w_PNTOTDOC, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATRIF, this.oParentObject.w_PNVALNAZ), this.oParentObject.w_DECTOP)
        else
          this.w_TOTDOC = cp_ROUND( this.oParentObject.w_PNTOTDOC, this.oParentObject.w_DECTOP )
        endif
        this.w_DIFDOC = this.w_VALO -this.w_TOTDOC
        if this.w_DIFDOC<>0 AND (this.oParentObject.w_CALDOC<>"I" OR (g_PERPAR="S" AND this.oParentObject.w_FLPART $ "CSA"))
          if this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL
            * --- Congruita' Primanota/Documenti (in Valuta)
            this.w_MESS = "Importo su Cliente/Fornitore Incongruente Con Totale Documento in Valuta"
          else
            * --- Congruita' Primanota/Documenti 
            this.w_MESS = "Importo su Cliente/Fornitore Incongruente Con Totale Documento "
          endif
          this.w_MESS = this.w_MESS + CHR(13) + "(Differenza = "+ALLTRIM(STR(ABS(this.w_DIFDOC), 18, this.oParentObject.w_DECTOP))+")"
          this.w_MESS = this.w_MESS + CHR(13) + CHR(13) + "Inserisco la differenza come Fuori Campo IVA ?"
          this.w_OK = CP_YESNO(this.w_MESS)
          if this.w_OK
            * --- Inserisce Differenza Fuori Campo IVA
            if EMPTY(g_COIDIF)
              this.w_MESS = "Non esiste il Codice IVA per Differenze di Conversione! (Contropartite) "
            else
              * --- Aggiorno Castelletto IVA
              this.w_GSCG_MIV.w_DIFDOC = this.w_DIFDOC
              this.w_GSCG_MIV.NotifyEvent("DiffConv")     
              this.w_VALO = this.w_VALO - this.w_DIFDOC
              if this.oParentObject.w_DETCON="S" 
                * --- Aggiorno Array Contropartite per Riga fuori campo iva inserita se dettaglio iva per contropartita
                if Empty(this.w_CONTRO)
                  this.w_CONTRO = this.w_LCODCON
                endif
                FOR I = 1 TO this.w_INDIC1
                if this.w_CONTRO<>SPACE(15) AND this.w_DIFDOC<>0 AND RC[I, 1]=this.w_CONTRO 
                  * --- Aggiorno Contropartita presente 
                  RC[I, 2] = RC[I, 2] - this.w_DIFDOC
                endif
                ENDFOR
              endif
            endif
          endif
        endif
      endif
    endif
    * --- Tappo al Vettore
    RI[this.w_INDICE+1, 1] = REPL("@", 15)
    RI[this.w_INDICE+1, 2] = this.w_VALO
    RC[this.w_INDIC1+1, 1] = REPL("@", 15)
    * --- Adesso Scorre sul Tmp Registrazioni Contabili
    this.w_INIT = .T.
    * --- Carica Detail Primanota dai Modelli Contabili se:
    * --- Carica Detail Primanota dai Modelli Contabili se:
    *     1) No dettaglio Iva Contropartita  
    *     2) Si dettaglio iva Contropartita e Esisite una riga nel dettaglio iva senza contropartita  
    *     3) Causale Contabile che non gestisce l'iva
    if this.w_PADRE.numrow() =0
      this.w_PADRE.FirstRow()     
      this.w_PADRE.SetRow(1)     
    endif
    this.w_PADRE.MarkPos()     
    if this.oParentObject.w_DETCON<>"S" OR( Not this.w_TESTCO AND this.oParentObject.w_DETCON="S") OR this.oParentObject.w_PNTIPREG="N" OR this.w_COUNT=0
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
     
 SELECT ( this.w_PADRE.cTrsName) 
 Go Top
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Flag Caricati Automatismi
    this.oParentObject.w_GIACAR = " "
    this.w_PADRE.RePos()     
    this.w_OBJECT = this.w_PADRE.GETBODYCTRL("w_PNTIPCON")
    this.w_OBJECT.setfocus()     
    if used("TEMP1")
      select TEMP1
      use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggorna Detail Primanota
    this.w_VALO = 0
    this.w_DAVE = " "
    this.w_AGG1 = .F.
    * --- Riscorre sul Tmp Registrazioni Contabili e Aggiorna gli Automatismi Contabili
    this.oParentObject.w_TOTFLAG = 0
    FOR L_i = 1 TO 50
    if RI[L_i, 1]=REPL("@", 15)
      this.w_VALO = RI[L_i,2]
      EXIT FOR
    endif
    ENDFOR
    this.oParentObject.w_TOTFOR = 0
    FOR L_FASE = 1 TO 3
    SELECT (this.w_PADRE.cTrsName)
    GO TOP
    SCAN
    * --- Legge i Dati del Temporaneo
    this.w_PADRE.WorkFromTrs()     
    do case
      case L_FASE=1 
        * --- Carica Codice Cli/For della Registrazione
        if this.oParentObject.w_PNTIPCLF $ "CF" AND this.oParentObject.w_PNTIPCLF=this.oParentObject.w_PNTIPCON AND (NOT EMPTY(this.oParentObject.w_PNCODCLF)) AND EMPTY(this.oParentObject.w_PNCODCON)
          this.oParentObject.w_PNCODCON = this.oParentObject.w_PNCODCLF
          if NOT EMPTY(this.oParentObject.w_PNCODCON)
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF(this.oParentObject.w_FLINSO="S" AND NOT EMPTY(NVL(this.oParentObject.w_PAGINS,"")), this.oParentObject.w_PAGINS,IIF(this.oParentObject.w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_PAGCLF1)))
            if Not Empty(this.oParentObject.w_PNCODPAG)
              * --- Read from PAG_AMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PADESCRI"+cp_TransInsFldName("PADESCRI")+""+;
                  " from "+i_cTable+" PAG_AMEN where ";
                      +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODPAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PADESCRI,PADESCRI;
                  from (i_cTable) where;
                      PACODICE = this.oParentObject.w_PNCODPAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESCRI = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.oParentObject.w_PNCAURIG = IIF(EMPTY(this.oParentObject.w_PNCAURIG), this.oParentObject.w_PNCODCAU, this.oParentObject.w_PNCAURIG)
            * --- Read from CAU_CONT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAU_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CCDESCRI,CCFLPART"+;
                " from "+i_cTable+" CAU_CONT where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CCDESCRI,CCFLPART;
                from (i_cTable) where;
                    CCCODICE = this.oParentObject.w_PNCAURIG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
              this.oParentObject.w_RIGPAR = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_PARTSN="S", IIF(this.oParentObject.w_PNFLPART $ "CSA" AND this.oParentObject.w_RIGPAR $ "CSA", this.oParentObject.w_PNFLPART, this.oParentObject.w_RIGPAR), "N")
            this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
            * --- Read from SALDICON
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SALDICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN"+;
                " from "+i_cTable+" SALDICON where ";
                    +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                    +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                    +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_PNCODESE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN;
                from (i_cTable) where;
                    SLTIPCON = this.oParentObject.w_PNTIPCON;
                    and SLCODICE = this.oParentObject.w_PNCODCON;
                    and SLCODESE = this.oParentObject.w_PNCODESE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_SLDPRO = NVL(cp_ToDate(_read_.SLDARPRO),cp_NullValue(_read_.SLDARPRO))
              this.oParentObject.w_SLDPER = NVL(cp_ToDate(_read_.SLDARPER),cp_NullValue(_read_.SLDARPER))
              this.oParentObject.w_SLDFIN = NVL(cp_ToDate(_read_.SLDARFIN),cp_NullValue(_read_.SLDARFIN))
              this.oParentObject.w_SLAPRO = NVL(cp_ToDate(_read_.SLAVEPRO),cp_NullValue(_read_.SLAVEPRO))
              this.oParentObject.w_SLAPER = NVL(cp_ToDate(_read_.SLAVEPER),cp_NullValue(_read_.SLAVEPER))
              this.oParentObject.w_SLAFIN = NVL(cp_ToDate(_read_.SLAVEFIN),cp_NullValue(_read_.SLAVEFIN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_SALDO = ABS((this.oParentObject.w_SLDPRO+this.oParentObject.w_SLDPER+this.oParentObject.w_SLDFIN) - (this.oParentObject.w_SLAPRO+this.oParentObject.w_SLAPER+this.oParentObject.w_SLAFIN))
          endif
        endif
        * --- Carica IMPORTI Codice Cli/For della Registrazione
        * --- Verifica Se DARE/AVERE Cliente/Fornitore = 0
        if (this.oParentObject.w_PNTIPCON $ "CF" OR (this.oParentObject.w_PNTIPCON="G" AND this.oParentObject.w_FLAGTOT="A")) AND NOT EMPTY(this.oParentObject.w_PNCODCON) AND this.w_AGG1=.F. 
          this.w_AGG1 = .T.
          this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
          this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
          this.oParentObject.w_PNIMPDAR = IIF(this.oParentObject.w_FLDAVE="D", this.w_VALO, 0)
          this.oParentObject.w_PNIMPAVE = IIF(this.oParentObject.w_FLDAVE="A", this.w_VALO, 0)
          this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
          this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
          this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
          this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
          this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR+ this.oParentObject.w_IMPDAR
          this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE+ this.oParentObject.w_IMPAVE
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_FLASMO = "S"
        endif
        * --- Controllo Ritenute
        if this.oParentObject.w_GESRIT="S" and G_RITE="S" and ((this.oParentObject.w_RITENU$"CS" and this.oParentObject.w_PNTIPCON="F") OR (this.oParentObject.w_FLGRIT = "S" and this.oParentObject.w_PNTIPCON="C")) and this.oParentObject.w_PNCAURIG=this.oParentObject.w_PNCODCAU
          if Not (this.oParentObject.w_DETCON="S" AND this.w_TESTCO AND this.oParentObject.w_PNTIPREG<>"N" AND this.w_COUNT>0 )
            this.oParentObject.w_IMPFOR = IIF(this.oParentObject.w_FLGRIT = "S" ,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,this.oParentObject.w_PNIMPAVE-this.oParentObject.w_PNIMPDAR)
            this.oParentObject.w_TOTFOR = this.oParentObject.w_TOTFOR+this.oParentObject.w_IMPFOR
          endif
        endif
      case L_FASE=2
        if this.oParentObject.w_PNTIPCON="G"
          this.w_DAVE = NVL(t_FLDAVE," ")
          FOR L_i = 1 TO 50
          if RI[L_i, 1]=REPL("@", 15)
            EXIT FOR
          endif
          if RI[L_i,1]=this.oParentObject.w_PNCODCON AND NOT EMPTY(this.oParentObject.w_PNCODCON)
            this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
            this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
            this.oParentObject.w_PNIMPDAR = IIF(this.w_DAVE="D", RI[L_i,2], 0)
            this.oParentObject.w_PNIMPAVE = IIF(this.w_DAVE="A", RI[L_i,2], 0)
            this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
            this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
            this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
            this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
            this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_IMPDAR
            this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_IMPAVE
            this.oParentObject.w_PNIMPIND = IIF(RI[L_i,3]="I", ABS(this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE), 0)
            if RI[L_i,3]<>"I"
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.oParentObject.w_FLASMO = IIF(RI[L_i,3]="I"," ","S")
            * --- Carica il Temporaneo dei Dati
            this.w_PADRE.TrsFromWork()     
            * --- Annulla il Vettore
            RI[L_i, 1]= SPACE(15)
            EXIT FOR
          endif
          ENDFOR
        endif
      otherwise
        * --- Automatismi
        if this.oParentObject.w_FLAGTOT $ "TPDIA" AND this.oParentObject.w_FLASMO<>"S"
          * --- Totalizza
          this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
          this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
          this.oParentObject.w_PNIMPDAR = IIF(this.oParentObject.w_FLDAVE="D", this.oParentObject.w_TOTFLAG, 0)
          this.oParentObject.w_PNIMPAVE = IIF(this.oParentObject.w_FLDAVE="A", this.oParentObject.w_TOTFLAG, 0)
          this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
          this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
          this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
          this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
          this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_IMPDAR
          this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_IMPAVE
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    * --- Carica il Temporaneo dei Dati
    this.w_PADRE.TrsFromWork()     
    * --- Flag Notifica Riga Variata
    if i_SRV<>"A"
      replace i_SRV with "U"
    endif
    ENDSCAN
    if L_FASE=1 AND this.oParentObject.w_DETCON="S" AND this.w_TESTCO=.T. AND this.oParentObject.w_PNTIPREG<>"N" AND this.w_COUNT>0 
      * --- Carica riga cliente\fornitore nel caso di dettaglio iva per contropartite senza applicazione  del modello 
      if this.w_INIT 
        this.oParentObject.w_TOTDAR = 0
        this.oParentObject.w_TOTAVE = 0
        this.oParentObject.w_IMPDAR = 0
        this.oParentObject.w_IMPAVE = 0
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Nuova Riga del Temporaneo
      this.w_PADRE.InitRow()     
      if EMPTY(this.oParentObject.w_PNCODCLF)
        * --- Nel caso di Registrazioni senza intestatario ricerco eventuale riga del conto nell'Automatismo contabile
        * --- Read from CAUPRI1
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "APCODCON,APTIPCON"+;
            " from "+i_cTable+" CAUPRI1 where ";
                +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
                +" and APFLAUTO = "+cp_ToStrODBC("A");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            APCODCON,APTIPCON;
            from (i_cTable) where;
                APCODCAU = this.oParentObject.w_PNCODCAU;
                and APFLAUTO = "A";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PNCODCON = NVL(cp_ToDate(_read_.APCODCON),cp_NullValue(_read_.APCODCON))
          this.oParentObject.w_PNTIPCON = NVL(cp_ToDate(_read_.APTIPCON),cp_NullValue(_read_.APTIPCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_PNTIPCON = IIF(NOT EMPTY(this.oParentObject.w_PNTIPCON), this.oParentObject.w_PNTIPCON,"G")
      else
        this.oParentObject.w_PNTIPCON = this.oParentObject.w_PNTIPCLF
        this.oParentObject.w_PNCODCON = this.oParentObject.w_PNCODCLF
      endif
      do case
        case (this.oParentObject.w_PNTIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.oParentObject.w_PNTIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "NU-NC-NE") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "F")
          this.oParentObject.w_FLDAVE = "A"
        case (this.oParentObject.w_PNTIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.oParentObject.w_PNTIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU") OR (this.oParentObject.w_PNTIPREG $"CE") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "C")
          this.oParentObject.w_FLDAVE = "D"
      endcase
      this.oParentObject.w_FLAGTOT = IIF(NOT EMPTY(this.oParentObject.w_PNCODCLF),"+","A")
      this.oParentObject.w_SEZB = " "
      this.oParentObject.w_PNCAURIG = this.oParentObject.w_PNCODCAU
      if NOT EMPTY(this.oParentObject.w_PNCODCON)
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_PNCAURIG = IIF(EMPTY(this.oParentObject.w_PNCAURIG), this.oParentObject.w_PNCODCAU, this.oParentObject.w_PNCAURIG)
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESCRI,CCFLPART"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESCRI,CCFLPART;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_PNCAURIG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
          this.oParentObject.w_RIGPAR = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_PARTSN="S", this.oParentObject.w_RIGPAR, "N")
        this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF(this.oParentObject.w_FLINSO="S" AND NOT EMPTY(NVL(this.oParentObject.w_PAGINS,"")), this.oParentObject.w_PAGINS,IIF(this.oParentObject.w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_PAGCLF1)))
        if Not Empty(this.oParentObject.w_PNCODPAG)
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PADESCRI"+cp_TransInsFldName("PADESCRI")+""+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PADESCRI,PADESCRI;
              from (i_cTable) where;
                  PACODICE = this.oParentObject.w_PNCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCRI = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
      endif
      * --- Verifica Se DARE/AVERE Cliente/Fornitore = 0
      if this.w_VALO<>0 
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_PNIMPDAR = IIF(this.oParentObject.w_FLDAVE="D", this.w_VALO, 0)
        this.oParentObject.w_PNIMPAVE = IIF(this.oParentObject.w_FLDAVE="A", this.w_VALO, 0)
        this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
        this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
        this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
        this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR+ this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_IMPAVE
      endif
      * --- Read from SALDICON
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN"+;
          " from "+i_cTable+" SALDICON where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_PNCODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN;
          from (i_cTable) where;
              SLTIPCON = this.oParentObject.w_PNTIPCON;
              and SLCODICE = this.oParentObject.w_PNCODCON;
              and SLCODESE = this.oParentObject.w_PNCODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_SLDPRO = NVL(cp_ToDate(_read_.SLDARPRO),cp_NullValue(_read_.SLDARPRO))
        this.oParentObject.w_SLDPER = NVL(cp_ToDate(_read_.SLDARPER),cp_NullValue(_read_.SLDARPER))
        this.oParentObject.w_SLDFIN = NVL(cp_ToDate(_read_.SLDARFIN),cp_NullValue(_read_.SLDARFIN))
        this.oParentObject.w_SLAPRO = NVL(cp_ToDate(_read_.SLAVEPRO),cp_NullValue(_read_.SLAVEPRO))
        this.oParentObject.w_SLAPER = NVL(cp_ToDate(_read_.SLAVEPER),cp_NullValue(_read_.SLAVEPER))
        this.oParentObject.w_SLAFIN = NVL(cp_ToDate(_read_.SLAVEFIN),cp_NullValue(_read_.SLAVEFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_SALDO = ABS((this.oParentObject.w_SLDPRO+this.oParentObject.w_SLDPER+this.oParentObject.w_SLDFIN) - (this.oParentObject.w_SLAPRO+this.oParentObject.w_SLAPER+this.oParentObject.w_SLAFIN))
      * --- Controllo Ritenute
      if this.oParentObject.w_GESRIT="S" and G_RITE="S" and ((this.oParentObject.w_RITENU$"CS" and this.oParentObject.w_PNTIPCON="F") OR (this.oParentObject.w_FLGRIT = "S" and this.oParentObject.w_PNTIPCON="C")) and this.oParentObject.w_PNCAURIG=this.oParentObject.w_PNCODCAU
        this.oParentObject.w_IMPFOR = IIF(this.oParentObject.w_FLGRIT = "S" ,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,this.oParentObject.w_PNIMPAVE-this.oParentObject.w_PNIMPDAR)
        this.oParentObject.w_TOTFOR = this.oParentObject.w_TOTFOR+this.oParentObject.w_IMPFOR
      endif
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_FLASMO = "S"
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.w_PADRE.TrsFromWork()     
    endif
    if L_FASE=2 
      * --- Al Termine Aggiunge le Righe IVA con CONTO
      FOR L_i = 1 TO 50
      if RI[L_i, 1]=REPL("@", 15)
        EXIT FOR
      endif
      if NOT EMPTY(RI[L_i,1])
        * --- Nuova Riga del Temporaneo
        this.w_PADRE.InitRow()     
        this.oParentObject.w_PNTIPCON = "G"
        this.oParentObject.w_PNCODCON = RI[L_i,1]
        this.w_TIPREG = RI[L_i,4]
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_PNCAURIG = IIF(EMPTY(this.oParentObject.w_PNCAURIG), this.oParentObject.w_PNCODCAU, this.oParentObject.w_PNCAURIG)
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESCRI,CCFLPART"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESCRI,CCFLPART;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_PNCAURIG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
          this.oParentObject.w_RIGPAR = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_PARTSN="S", this.oParentObject.w_RIGPAR, "N")
        this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
        this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF(this.oParentObject.w_FLINSO="S" AND NOT EMPTY(NVL(this.oParentObject.w_PAGINS,"")), this.oParentObject.w_PAGINS,IIF(this.oParentObject.w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_PAGCLF1)))
        if Not Empty(this.oParentObject.w_PNCODPAG)
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PADESCRI"+cp_TransInsFldName("PADESCRI")+""+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PADESCRI,PADESCRI;
              from (i_cTable) where;
                  PACODICE = this.oParentObject.w_PNCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCRI = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.w_SEZB = IIF(this.oParentObject.w_PNTIPCON="G", CALCSEZ(this.oParentObject.w_MASTRO), " ")
        this.oParentObject.w_PNFLSALI = IIF(this.oParentObject.w_FLSALI="S" AND this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_SEZB<>"T", "+", " ")
        * --- Read from SALDICON
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN"+;
            " from "+i_cTable+" SALDICON where ";
                +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_PNCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN;
            from (i_cTable) where;
                SLTIPCON = this.oParentObject.w_PNTIPCON;
                and SLCODICE = this.oParentObject.w_PNCODCON;
                and SLCODESE = this.oParentObject.w_PNCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_SLDPRO = NVL(cp_ToDate(_read_.SLDARPRO),cp_NullValue(_read_.SLDARPRO))
          this.oParentObject.w_SLDPER = NVL(cp_ToDate(_read_.SLDARPER),cp_NullValue(_read_.SLDARPER))
          this.oParentObject.w_SLDFIN = NVL(cp_ToDate(_read_.SLDARFIN),cp_NullValue(_read_.SLDARFIN))
          this.oParentObject.w_SLAPRO = NVL(cp_ToDate(_read_.SLAVEPRO),cp_NullValue(_read_.SLAVEPRO))
          this.oParentObject.w_SLAPER = NVL(cp_ToDate(_read_.SLAVEPER),cp_NullValue(_read_.SLAVEPER))
          this.oParentObject.w_SLAFIN = NVL(cp_ToDate(_read_.SLAVEFIN),cp_NullValue(_read_.SLAVEFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_SALDO = ABS((this.oParentObject.w_SLDPRO+this.oParentObject.w_SLDPER+this.oParentObject.w_SLDFIN) - (this.oParentObject.w_SLAPRO+this.oParentObject.w_SLAPER+this.oParentObject.w_SLAFIN))
        do case
          case (this.w_TIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.w_TIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "F")
            this.oParentObject.w_FLDAVE = "D"
          case (this.w_TIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.w_TIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU") OR (this.w_TIPREG $"CE") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "C")
            this.oParentObject.w_FLDAVE = "A"
        endcase
        this.oParentObject.w_FLAGTOT = IIF(this.oParentObject.w_PNTIPREG<>this.w_TIPREG,"+","-")
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_PNIMPDAR = IIF(this.oParentObject.w_FLDAVE="D", RI[L_i,2], 0)
        this.oParentObject.w_PNIMPAVE = IIF(this.oParentObject.w_FLDAVE="A", RI[L_i,2], 0)
        this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
        this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
        this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
        this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_IMPAVE
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_FLASMO = "S"
        * --- Carica il Temporaneo dei Dati e skippa al record successivo
        this.w_PADRE.TrsFromWork()     
      endif
      ENDFOR
    endif
    if L_FASE=3 AND this.oParentObject.w_DETCON="S" AND this.w_TESTCO=.T. AND this.oParentObject.w_PNTIPREG<>"N" AND this.w_COUNT>0 
      * --- Al Termine Aggiunge le contropartite dal relativo array e applica modello
      FOR L_i = 1 TO 50
      if RC[L_i, 1]=REPL("@", 15)
        EXIT FOR
      endif
      this.w_TIPREG = RC[L_i,3]
      * --- Aggiunge solo contropartite congruenti col registro iva di testata
      if (this.oParentObject.w_PNTIPREG=this.w_TIPREG)
        * --- Nuova Riga del Temporaneo
        this.w_PADRE.InitRow()     
        this.oParentObject.w_PNTIPCON = RC[L_i,4]
        this.oParentObject.w_PNCODCON = RC[L_i,1]
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF(this.oParentObject.w_FLINSO="S" AND NOT EMPTY(NVL(this.oParentObject.w_PAGINS,"")), this.oParentObject.w_PAGINS,IIF(this.oParentObject.w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_PAGCLF1)))
        if Not Empty(this.oParentObject.w_PNCODPAG)
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PADESCRI"+cp_TransInsFldName("PADESCRI")+""+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PADESCRI,PADESCRI;
              from (i_cTable) where;
                  PACODICE = this.oParentObject.w_PNCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCRI = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.w_PNCAURIG = IIF(EMPTY(this.oParentObject.w_PNCAURIG), this.oParentObject.w_PNCODCAU, this.oParentObject.w_PNCAURIG)
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESCRI,CCFLPART"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESCRI,CCFLPART;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_PNCAURIG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
          this.oParentObject.w_RIGPAR = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_PARTSN="S", this.oParentObject.w_RIGPAR, "N")
        this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
        this.oParentObject.w_SEZB = IIF(this.oParentObject.w_PNTIPCON="G", CALCSEZ(this.oParentObject.w_MASTRO), " ")
        this.oParentObject.w_PNFLSALI = IIF(this.oParentObject.w_FLSALI="S" AND this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_SEZB<>"T", "+", " ")
        * --- Read from SALDICON
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN"+;
            " from "+i_cTable+" SALDICON where ";
                +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_PNCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN;
            from (i_cTable) where;
                SLTIPCON = this.oParentObject.w_PNTIPCON;
                and SLCODICE = this.oParentObject.w_PNCODCON;
                and SLCODESE = this.oParentObject.w_PNCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_SLDPRO = NVL(cp_ToDate(_read_.SLDARPRO),cp_NullValue(_read_.SLDARPRO))
          this.oParentObject.w_SLDPER = NVL(cp_ToDate(_read_.SLDARPER),cp_NullValue(_read_.SLDARPER))
          this.oParentObject.w_SLDFIN = NVL(cp_ToDate(_read_.SLDARFIN),cp_NullValue(_read_.SLDARFIN))
          this.oParentObject.w_SLAPRO = NVL(cp_ToDate(_read_.SLAVEPRO),cp_NullValue(_read_.SLAVEPRO))
          this.oParentObject.w_SLAPER = NVL(cp_ToDate(_read_.SLAVEPER),cp_NullValue(_read_.SLAVEPER))
          this.oParentObject.w_SLAFIN = NVL(cp_ToDate(_read_.SLAVEFIN),cp_NullValue(_read_.SLAVEFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_SALDO = ABS((this.oParentObject.w_SLDPRO+this.oParentObject.w_SLDPER+this.oParentObject.w_SLDFIN) - (this.oParentObject.w_SLAPRO+this.oParentObject.w_SLAPER+this.oParentObject.w_SLAFIN))
        do case
          case (this.w_TIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.w_TIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "F")
            this.oParentObject.w_FLDAVE = "D"
          case (this.w_TIPREG="V" AND this.oParentObject.w_PNTIPDOC $ "FA-FE-AU") OR (this.w_TIPREG="A" AND this.oParentObject.w_PNTIPDOC $ "NC-NE-NU") OR (this.w_TIPREG $"CE") OR (this.oParentObject.w_PNTIPDOC = "NO" AND this.oParentObject.w_PNTIPCLF = "C")
            this.oParentObject.w_FLDAVE = "A"
        endcase
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR - this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE - this.oParentObject.w_IMPAVE
        this.oParentObject.w_PNIMPDAR = IIF(this.oParentObject.w_FLDAVE="D", RC[L_i,2], 0)
        this.oParentObject.w_PNIMPAVE = IIF(this.oParentObject.w_FLDAVE="A", RC[L_i,2], 0)
        this.oParentObject.w_SLIMPDAR = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE>=0,this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE,0)
        this.oParentObject.w_SLIMPAVE = IIF(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE<0,ABS(this.oParentObject.w_PNIMPDAR-this.oParentObject.w_PNIMPAVE),0)
        this.oParentObject.w_IMPDAR = this.oParentObject.w_PNIMPDAR
        this.oParentObject.w_IMPAVE = this.oParentObject.w_PNIMPAVE
        this.oParentObject.w_TOTDAR = this.oParentObject.w_TOTDAR + this.oParentObject.w_IMPDAR
        this.oParentObject.w_TOTAVE = this.oParentObject.w_TOTAVE + this.oParentObject.w_IMPAVE
        this.oParentObject.w_FLAGTOT = "D"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_FLASMO = "S"
        * --- Carica il Temporaneo dei Dati e skippa al record successivo
        this.w_PADRE.TrsFromWork()     
      endif
      ENDFOR
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    ENDFOR
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Totalizzatore
    if this.oParentObject.w_FLASMO<>"S" 
      do case
        case this.oParentObject.w_FLAGTOT="T"
          * --- Azzera
          this.oParentObject.w_TOTFLAG = 0
        case this.oParentObject.w_FLAGTOT $ "+I"
          * --- Incrementa
          this.oParentObject.w_TOTFLAG = this.oParentObject.w_TOTFLAG + (this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE)
        case this.oParentObject.w_FLAGTOT $ "-D"
          * --- Decrementa
          this.oParentObject.w_TOTFLAG = this.oParentObject.w_TOTFLAG - (this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE)
        case this.oParentObject.w_FLAGTOT="A"
          * --- Assegna
          this.oParentObject.w_TOTFLAG = this.oParentObject.w_PNIMPDAR+this.oParentObject.w_PNIMPAVE
      endcase
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Applica Automatismi
    do case
      case this.oParentObject.w_CHKAUT=1 
        * --- Select from CAUPRI1
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select APCAURIG,APFLDAVE,APTIPCON,APCODCON,APCAUMOV,APCONBAN,APFLPART,APFLAUTO  from "+i_cTable+" CAUPRI1 ";
              +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+"";
               ,"_Curs_CAUPRI1")
        else
          select APCAURIG,APFLDAVE,APTIPCON,APCODCON,APCAUMOV,APCONBAN,APFLPART,APFLAUTO from (i_cTable);
           where APCODCAU=this.oParentObject.w_PNCODCAU;
            into cursor _Curs_CAUPRI1
        endif
        if used('_Curs_CAUPRI1')
          select _Curs_CAUPRI1
          locate for 1=1
          do while not(eof())
          this.w_MOCAURIG = NVL(_Curs_CAUPRI1.APCAURIG," ")
          this.w_MOFLDAVE = NVL(_Curs_CAUPRI1.APFLDAVE, " ")
          this.w_MOTIPCON = NVL(_Curs_CAUPRI1.APTIPCON, " ")
          this.w_MOCODCON = NVL(_Curs_CAUPRI1.APCODCON, SPACE(15))
          this.w_MOCAUMOV = NVL(_Curs_CAUPRI1.APCAUMOV,SPACE(5))
          this.w_MOCONBAN = NVL(_Curs_CAUPRI1.APCONBAN,SPACE(15))
          this.w_MOFLPART = NVL(_Curs_CAUPRI1.APFLPART,"N")
          this.w_MOFLAUTO = NVL(_Curs_CAUPRI1.APFLAUTO," ")
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_CAUPRI1
            continue
          enddo
          use
        endif
      case this.oParentObject.w_CHKAUT=2 
        * --- Select from CAUPRI
        i_nConn=i_TableProp[this.CAUPRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2],.t.,this.CAUPRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select APCAURIG,APFLDAVE,APTIPCON,APCODCON,APCAUMOV,APCONBAN,APFLPART,APFLAUTO  from "+i_cTable+" CAUPRI ";
              +" where APCODCAU="+cp_ToStrODBC(this.oParentObject.w_PNCODCAU)+" AND APTIPCLF="+cp_ToStrODBC(this.oParentObject.w_PNTIPCLF)+" AND APCODCLF="+cp_ToStrODBC(this.oParentObject.w_PNCODCLF)+"";
               ,"_Curs_CAUPRI")
        else
          select APCAURIG,APFLDAVE,APTIPCON,APCODCON,APCAUMOV,APCONBAN,APFLPART,APFLAUTO from (i_cTable);
           where APCODCAU=this.oParentObject.w_PNCODCAU AND APTIPCLF=this.oParentObject.w_PNTIPCLF AND APCODCLF=this.oParentObject.w_PNCODCLF;
            into cursor _Curs_CAUPRI
        endif
        if used('_Curs_CAUPRI')
          select _Curs_CAUPRI
          locate for 1=1
          do while not(eof())
          this.w_MOCAURIG = NVL(_Curs_CAUPRI.APCAURIG," ")
          this.w_MOFLDAVE = NVL(_Curs_CAUPRI.APFLDAVE, " ")
          this.w_MOTIPCON = NVL(_Curs_CAUPRI.APTIPCON, " ")
          this.w_MOCODCON = NVL(_Curs_CAUPRI.APCODCON, SPACE(15))
          this.w_MOCAUMOV = NVL(_Curs_CAUPRI.APCAUMOV,SPACE(5))
          this.w_MOCONBAN = NVL(_Curs_CAUPRI.APCONBAN,SPACE(15))
          this.w_MOFLPART = NVL(_Curs_CAUPRI.APFLPART,"N")
          this.w_MOFLAUTO = NVL(_Curs_CAUPRI.APFLAUTO," ")
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_CAUPRI
            continue
          enddo
          use
        endif
    endcase
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili da passare alla pagina...
    * --- Applica modello se:
    *     1) No dettaglio iva per contropartite  
    *     2) Si dettaglio iva per contropartite  e Esiste riga del dettaglio iva senza contropartita 
    *     3) Esiste Causale di riga nell'automatismo contabile
    *     4) Causale Causale che non gestrisce l'iva
    if (this.oParentObject.w_DETCON="S" AND NOT EMPTY(this.w_MOCAURIG) and this.w_MOCAURIG<>this.oParentObject.w_PNCODCAU ) OR (Not this.w_TESTCO) OR this.oParentObject.w_DETCON<>"S" OR this.oParentObject.w_PNTIPREG="N" OR this.w_COUNT=0
      * --- Cicla Sugli Automatismi Contabili (Generico)
      if this.w_INIT AND ((this.oParentObject.w_DETCON <>"S" OR (Not this.w_TESTCO AND this.oParentObject.w_DETCON="S")) OR this.oParentObject.w_PNTIPREG="N" OR this.w_COUNT=0)
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Nuova Riga del Temporaneo
      * --- Necessaria per la presenza di righe senza la condizione di riga piena
      this.w_PADRE.InitRow()     
      this.oParentObject.w_FLDAVE = this.w_MOFLDAVE
      this.oParentObject.w_PNTIPCON = this.w_MOTIPCON
      this.oParentObject.w_PNCODCON = this.w_MOCODCON
      if this.oParentObject.w_DETCON="S" AND this.w_TESTCO AND this.oParentObject.w_PNTIPCON $"CF" AND this.w_COUNT>0
        this.oParentObject.w_PNCODCON = IIF(NOT EMPTY(this.oParentObject.w_PNCODCON),this.oParentObject.w_PNCODCON,this.oParentObject.w_PNCODCLF)
      endif
      this.oParentObject.w_PNCAURIG = IIF(EMPTY(NVL(this.w_MOCAURIG," ")), this.oParentObject.w_PNCODCAU, this.w_MOCAURIG)
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDESCRI,CCFLPART"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCAURIG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDESCRI,CCFLPART;
          from (i_cTable) where;
              CCCODICE = this.oParentObject.w_PNCAURIG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CAUDES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
        this.oParentObject.w_RIGPAR = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.oParentObject.w_PNCODCON)
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_AGECLF = this.oParentObject.w_PNCODAGE
        if this.oParentObject.w_TIPSOT="B" AND g_BANC="S" and this.oParentObject.w_FLMOVC="S"
          * --- Se abilitato Modulo Conti Correnti e conto di tipo banca valorizzo relativa causale Movimenti di C\C e il Conto banche
          this.oParentObject.w_CAUMOV = this.w_MOCAUMOV
          this.oParentObject.w_CONBAN = this.w_MOCONBAN
          if Not Empty(this.oParentObject.w_CONBAN)
            * --- Leggo valuta conto corrente 
            * --- Read from COC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.COC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "BACODVAL,BACALFES"+;
                " from "+i_cTable+" COC_MAST where ";
                    +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_CONBAN);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                BACODVAL,BACALFES;
                from (i_cTable) where;
                    BACODBAN = this.oParentObject.w_CONBAN;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_BAVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
              this.oParentObject.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.oParentObject.w_PNCODPAG = IIF(this.oParentObject.w_DATMOR1>=IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC) AND NOT EMPTY(this.oParentObject.w_PAGMOR1), this.oParentObject.w_PAGMOR1, IIF(this.oParentObject.w_FLINSO="S" AND NOT EMPTY(NVL(this.oParentObject.w_PAGINS,"")), this.oParentObject.w_PAGINS,IIF(this.oParentObject.w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_PAGCLF1)))
        if Not Empty(this.oParentObject.w_PNCODPAG)
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PADESCRI"+cp_TransInsFldName("PADESCRI")+""+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PADESCRI,PADESCRI;
              from (i_cTable) where;
                  PACODICE = this.oParentObject.w_PNCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCRI = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_TESTPART<>"N" AND this.oParentObject.w_PARTSN="S", this.w_MOFLPART, "N")
        this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
        * --- Read from SALDICON
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN"+;
            " from "+i_cTable+" SALDICON where ";
                +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
                +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_PNCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLDARPRO,SLDARPER,SLDARFIN,SLAVEPRO,SLAVEPER,SLAVEFIN;
            from (i_cTable) where;
                SLTIPCON = this.oParentObject.w_PNTIPCON;
                and SLCODICE = this.oParentObject.w_PNCODCON;
                and SLCODESE = this.oParentObject.w_PNCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_SLDPRO = NVL(cp_ToDate(_read_.SLDARPRO),cp_NullValue(_read_.SLDARPRO))
          this.oParentObject.w_SLDPER = NVL(cp_ToDate(_read_.SLDARPER),cp_NullValue(_read_.SLDARPER))
          this.oParentObject.w_SLDFIN = NVL(cp_ToDate(_read_.SLDARFIN),cp_NullValue(_read_.SLDARFIN))
          this.oParentObject.w_SLAPRO = NVL(cp_ToDate(_read_.SLAVEPRO),cp_NullValue(_read_.SLAVEPRO))
          this.oParentObject.w_SLAPER = NVL(cp_ToDate(_read_.SLAVEPER),cp_NullValue(_read_.SLAVEPER))
          this.oParentObject.w_SLAFIN = NVL(cp_ToDate(_read_.SLAVEFIN),cp_NullValue(_read_.SLAVEFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_SALDO = ABS((this.oParentObject.w_SLDPRO+this.oParentObject.w_SLDPER+this.oParentObject.w_SLDFIN) - (this.oParentObject.w_SLAPRO+this.oParentObject.w_SLAPER+this.oParentObject.w_SLAFIN))
      else
        this.oParentObject.w_PNFLPART = IIF(this.oParentObject.w_TESTPART<>"N", this.w_MOFLPART , "N")
        this.oParentObject.w_PNFLVABD = IIF(this.oParentObject.w_PNFLPART $ "AC", IIF(this.oParentObject.w_PNTIPCON="C", this.oParentObject.w_FLVEBD, IIF(this.oParentObject.w_PNTIPCON="F", this.w_FLACBD, " ")), " ")
      endif
      this.oParentObject.w_SEZB = IIF(this.oParentObject.w_PNTIPCON="G", CALCSEZ(this.oParentObject.w_MASTRO), " ")
      this.oParentObject.w_PNFLSALI = IIF(this.oParentObject.w_FLSALI="S" AND this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_SEZB<>"T", "+", " ")
      this.oParentObject.w_FLAGTOT = this.w_MOFLAUTO
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.w_PADRE.TrsFromWork()     
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,ANCONSUP,ANCCTAGG,ANTIPSOT,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANDTOBSO,ANCODBAN,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1,ANFLACBD,ANNUMCOR"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,ANCONSUP,ANCCTAGG,ANTIPSOT,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANDTOBSO,ANCODBAN,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1,ANFLACBD,ANNUMCOR;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_PNTIPCON;
            and ANCODICE = this.oParentObject.w_PNCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DESCON = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
      this.oParentObject.w_DESCO2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
      this.oParentObject.w_PAGCLF1 = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
      this.oParentObject.w_PARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
      this.oParentObject.w_MASTRO = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
      this.oParentObject.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
      this.oParentObject.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
      this.oParentObject.w_CLMES1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
      this.oParentObject.w_CLMES2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
      this.oParentObject.w_CLGIO1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
      this.oParentObject.w_CLGIO2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
      this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
      this.oParentObject.w_BANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
      this.oParentObject.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
      this.oParentObject.w_BANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
      this.oParentObject.w_PAGMOR1 = NVL(cp_ToDate(_read_.ANPAGPAR),cp_NullValue(_read_.ANPAGPAR))
      this.oParentObject.w_DATMOR1 = NVL(cp_ToDate(_read_.ANDATMOR),cp_NullValue(_read_.ANDATMOR))
      this.oParentObject.w_AGECLF = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
      this.oParentObject.w_PNCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
      this.w_FLACBD = NVL(cp_ToDate(_read_.ANFLACBD),cp_NullValue(_read_.ANFLACBD))
      this.oParentObject.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Cerco prima c\c di defualt
    if Empty(this.oParentObject.w_NUMCOR)
      * --- Cerco C\C specifico associato alla banca
      * --- Read from BAN_CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCONCOR"+;
          " from "+i_cTable+" BAN_CONTI where ";
              +"CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PNTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.oParentObject.w_PNCODCON);
              +" and CCCODBAN = "+cp_ToStrODBC(this.oParentObject.w_BANAPP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCONCOR;
          from (i_cTable) where;
              CCTIPCON = this.oParentObject.w_PNTIPCON;
              and CCCODCON = this.oParentObject.w_PNCODCON;
              and CCCODBAN = this.oParentObject.w_BANAPP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_NUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.cFunction="Edit"
      this.w_NUMERO = 1000
      * --- Cancello tutte le righe del transitorio
      *     la deleterow con parametro true non esegue la initrow nel caso in cui 
      *     non ho pi� righe nel temporaneo poich� la eseguo direttamente 
      *     alla fine del ciclo While
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs() AND this.w_NUMERO>0
        if this.w_PADRE.Fullrow() or this.w_PADRE.RowStatus()="A"
          this.w_PADRE.DeleteRow(.t.)     
        else
          this.w_PADRE.Set("w_PNFLZERO" , "S", .T. , .T.)     
          this.w_PADRE.Set("w_PNCODCON" , this.oParentObject.o_PNCODCON, .T. , .T.)     
        endif
        this.w_NUMERO = this.w_NUMERO-1
        this.w_PADRE.FirstRow()     
      enddo
    else
      if Type("this.oParentObject.gscg_mca.CNT")="O"
        SELECT (this.w_PADRE.cTrsName)
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs() 
          SELECT (this.w_PADRE.GSCG_MCA.CNT.cTrsName)
          this.w_PADRE.GSCG_MCA.CNT.DeleteRow(.t.)     
          this.w_PADRE.NextRow()     
        enddo
      endif
      SELECT (this.w_PADRE.cTrsName)
      if reccount()<>0
        Zap
      endif
    endif
    this.w_INIT = .F.
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_COUNT = 0
    this.w_VALO = 0
    * --- Inizializzazione array
    FOR I=1 TO 50
    RC[I, 1] = SPACE(15)
    RC[I, 2] = 0
    RC[I, 3] = " "
    RC[I, 4] = " "
    ENDFOR
    this.w_GSCG_MIV.MarkPos()     
     
 SELECT (this.w_GSCG_MIV.cTrsName) 
 Go Top
    SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," "))
    this.w_CFLOMA = t_IVCFLOMA
    this.w_DTOBSO1 = NVL(t_DTOBSO,cp_CharToDate("  -  -    "))
    this.w_CONTRO = IIF((this.w_DTOBSO1>this.oParentObject.w_PNDATREG OR EMPTY(this.w_DTOBSO1)),iif(Empty(NVL(t_IVCONTRO,SPACE(15))),this.w_LCODCON,NVL(t_IVCONTRO,SPACE(15))),SPACE(15))
    this.w_COIND = NVL(t_IVCODCOI,SPACE(15))
    this.w_TIPREG = NVL(t_CTIPREG," ")
    this.w_TIPCOP = NVL(t_IVTIPCOP," ")
    this.w_COUNT = this.w_COUNT+1
    * --- Aggiorna Totalizzatore importo Riga Cli/For (solo se Tipo Registro IVA = a quello di Testata)
    if this.oParentObject.w_PNTIPREG=t_CTIPREG
      this.w_VALO = this.w_VALO + IIF(this.oParentObject.w_CALDOC="I" OR this.w_CFLOMA $ "IE", 0, t_IVIMPONI)
      this.w_VALO = this.w_VALO + IIF(this.oParentObject.w_CALDOC="M" OR this.w_CFLOMA="E" OR (this.oParentObject.w_PNTIPDOC $ "AU-NU" AND Nvl(t_REVCHA," ")="S"), 0, t_IVIMPIVA)
    endif
    * --- Aggiorno Array Dettaglio iva Contropartite
    if this.oParentObject.w_DETCON="S"
      this.w_IMPONI = IIF(this.w_CFLOMA $ "EI", 0, NVL(t_IVIMPONI,0))
      this.w_OKCON = .F.
      FOR I = 1 TO this.w_INDIC1
      if this.w_IMPONI<>0 AND RC[I, 1]=this.w_CONTRO AND ( this.oParentObject.w_PNTIPREG=this.w_TIPREG)
        * --- Aggiorno Contropartita presente 
        RC[I, 2] = RC[I, 2] + this.w_IMPONI
        this.w_OKCON = .T.
      endif
      ENDFOR
      if (this.w_IMPONI<>0 OR this.w_CFLOMA $ "EIS") AND this.w_OKCON=.F.
        * --- Inserimento nuova Contropartita
        this.w_INDIC1 = this.w_INDIC1 + 1
        RC[this.w_INDIC1, 1] = this.w_CONTRO
        RC[this.w_INDIC1, 2] = this.w_IMPONI
        RC[this.w_INDIC1, 3] = this.w_TIPREG
        RC[this.w_INDIC1, 4] = this.w_TIPCOP
      endif
    endif
    ENDSCAN
    this.w_GSCG_MIV.RePos()     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAUPRI'
    this.cWorkTables[3]='CAUPRI1'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='SALDICON'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='BAN_CONTI'
    this.cWorkTables[8]='PAG_AMEN'
    this.cWorkTables[9]='MOD_CONT'
    this.cWorkTables[10]='CAUIVA1'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_CAUPRI1')
      use in _Curs_CAUPRI1
    endif
    if used('_Curs_CAUPRI')
      use in _Curs_CAUPRI
    endif
    if used('_Curs_CAUPRI1')
      use in _Curs_CAUPRI1
    endif
    if used('_Curs_CAUPRI')
      use in _Curs_CAUPRI
    endif
    if used('_Curs_CAUPRI1')
      use in _Curs_CAUPRI1
    endif
    if used('_Curs_CAUPRI')
      use in _Curs_CAUPRI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
