* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bcm                                                        *
*              Controllo flag mov.fiscali                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2000-09-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bcm",oParentObject)
return(i_retval)

define class tgsma_bcm as StdBatch
  * --- Local variables
  w_MESS = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo il valore che assume il campo CMFLAVAL (Valore da Aggiornare)
    * --- Se assume il valore 'Altri Carichi' oppure 'Altri Scarichi' devo controllare
    * --- il valore che il check Movimenti Fiscali assume.
    if this.oParentObject.w_CMFLAVAL $ "CS"
      * --- Se il check movimenti fiscali � settato permetto il salvataggio altrimenti messaggio e richiesta di conferma
      if this.oParentObject.w_CMFLELGM<>"S"
        this.w_MESS = ah_msgformat("Elaborazione interrotta")
        if NOT ah_YesNo("ATTENZIONE:%0Altri carichi e scarichi definiti non fiscali%0Confermi ugualmente?")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
