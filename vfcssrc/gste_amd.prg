* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_amd                                                        *
*              Mandati SDD                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-08-02                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_amd"))

* --- Class definition
define class tgste_amd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 770
  Height = 513+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=210383977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  MAN_DATI_IDX = 0
  CONTI_IDX = 0
  TITOLARI_IDX = 0
  AZIENDA_IDX = 0
  SEDIAZIE_IDX = 0
  NAZIONI_IDX = 0
  cFile = "MAN_DATI"
  cKeySelect = "MACODICE"
  cKeyWhere  = "MACODICE=this.w_MACODICE"
  cKeyWhereODBC = '"MACODICE="+cp_ToStrODBC(this.w_MACODICE)';

  cKeyWhereODBCqualified = '"MAN_DATI.MACODICE="+cp_ToStrODBC(this.w_MACODICE)';

  cPrg = "gste_amd"
  cComment = "Mandati SDD"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- gste_amd
  w_NEW_SERIAL=space(15)
  * --- Fine Area Manuale

  * --- Local Variables
  w_MACODICE = space(15)
  o_MACODICE = space(15)
  w_MADATCRE = ctod('  /  /  ')
  o_MADATCRE = ctod('  /  /  ')
  w_MATIPMAN = space(1)
  o_MATIPMAN = space(1)
  w_MATIPSEQ = space(4)
  w_MARIFMAN = space(80)
  w_MACODCLI = space(15)
  o_MACODCLI = space(15)
  w_MACOIBAN = space(34)
  w_MATIPSDD = space(1)
  w_MADATINV = ctod('  /  /  ')
  w_MADATSOT = ctod('  /  /  ')
  w_MA__NOTE = space(0)
  w_MADATFIN = ctod('  /  /  ')
  w_MANOMFIL = space(254)
  w_DESCLI = space(60)
  w_DESCLI = space(60)
  w_CODCLI = space(15)
  w_MAIDCRED = space(23)
  w_CODICE_MANDATO = space(15)
  w_TESTSEL = space(1)
  w_TIPO = space(1)
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_TIPCLF = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_TTCOGTIT = space(25)
  w_TTNOMTIT = space(25)
  w_COD_AZI = space(5)
  w_AZPERAZI = space(1)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_SEINDIRI = space(35)
  w_SECODDES = space(5)
  w_SE___CAP = space(9)
  w_SEPROVIN = space(2)
  w_SELOCALI = space(30)
  w_NAZ_AZIENDA = space(3)
  w_DESC_AZIENDA = space(35)
  w_MATIPSTO = space(1)
  w_TIPSEQ = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Children pointers
  GSTE_MMD = .NULL.
  w_Scad_Mandati = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gste_amd
  Procedure ecpsave ()
  local tipo
  tipo=this.w_MATIPMAN
  if this.cfunction='Load'
  dodefault()
  IF not btrserr AND tipo='D'
  this.cFunction='Query'
  this.NotifyEvent("Edit")
  this.ecpEdit()
  this.oPgFrm.activepage=2
  this.oPgFrm.click()
  ENDIF
  ELSE
  dodefault()
  ENDIF
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAN_DATI','gste_amd')
    stdPageFrame::Init()
    *set procedure to GSTE_MMD additive
    with this
      .Pages(1).addobject("oPag","tgste_amdPag1","gste_amd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Mandati")
      .Pages(1).HelpContextID = 34532922
      .Pages(2).addobject("oPag","tgste_amdPag2","gste_amd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Scadenze")
      .Pages(2).HelpContextID = 131054453
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSTE_MMD
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_Scad_Mandati = this.oPgFrm.Pages(2).oPag.Scad_Mandati
      DoDefault()
    proc Destroy()
      this.w_Scad_Mandati = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='SEDIAZIE'
    this.cWorkTables[5]='NAZIONI'
    this.cWorkTables[6]='MAN_DATI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAN_DATI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAN_DATI_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MMD = CREATEOBJECT('stdDynamicChild',this,'GSTE_MMD',this.oPgFrm.Page1.oPag.oLinkPC_1_25)
    this.GSTE_MMD.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MMD)
      this.GSTE_MMD.DestroyChildrenChain()
      this.GSTE_MMD=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_25')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MMD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MMD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MMD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MMD.SetKey(;
            .w_MACODICE,"MDCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MMD.ChangeRow(this.cRowID+'      1',1;
             ,.w_MACODICE,"MDCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MMD)
        i_f=.GSTE_MMD.BuildFilter()
        if !(i_f==.GSTE_MMD.cQueryFilter)
          i_fnidx=.GSTE_MMD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MMD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MMD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MMD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MMD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MACODICE = NVL(MACODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MAN_DATI where MACODICE=KeySet.MACODICE
    *
    i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAN_DATI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAN_DATI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MAN_DATI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCLI = space(60)
        .w_DESCLI = space(60)
        .w_TESTSEL = space(1)
        .w_TIPO = 'A'
        .w_CODAZI = i_Codazi
        .w_TIPCLF = 'C'
        .w_DATOBSO = ctod("  /  /  ")
        .w_TTCOGTIT = space(25)
        .w_TTNOMTIT = space(25)
        .w_COD_AZI = i_Codazi
        .w_AZPERAZI = space(1)
        .w_TIPSL = 'SL'
        .w_SEDILEG = i_CODAZI
        .w_SEINDIRI = space(35)
        .w_SECODDES = space(5)
        .w_SE___CAP = space(9)
        .w_SEPROVIN = space(2)
        .w_SELOCALI = space(30)
        .w_NAZ_AZIENDA = g_Codnaz
        .w_DESC_AZIENDA = space(35)
        .w_TIPSEQ = 'RCUR'
        .w_MACODICE = NVL(MACODICE,space(15))
        .w_MADATCRE = NVL(cp_ToDate(MADATCRE),ctod("  /  /  "))
        .w_MATIPMAN = NVL(MATIPMAN,space(1))
        .w_MATIPSEQ = NVL(MATIPSEQ,space(4))
        .w_MARIFMAN = NVL(MARIFMAN,space(80))
        .w_MACODCLI = NVL(MACODCLI,space(15))
          .link_1_6('Load')
        .w_MACOIBAN = NVL(MACOIBAN,space(34))
        .w_MATIPSDD = NVL(MATIPSDD,space(1))
        .w_MADATINV = NVL(cp_ToDate(MADATINV),ctod("  /  /  "))
        .w_MADATSOT = NVL(cp_ToDate(MADATSOT),ctod("  /  /  "))
        .w_MA__NOTE = NVL(MA__NOTE,space(0))
        .w_MADATFIN = NVL(cp_ToDate(MADATFIN),ctod("  /  /  "))
        .w_MANOMFIL = NVL(MANOMFIL,space(254))
        .w_CODCLI = .w_MACODCLI
        .oPgFrm.Page2.oPag.Scad_Mandati.Calculate()
        .w_MAIDCRED = NVL(MAIDCRED,space(23))
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_CODICE_MANDATO = .w_MACODICE
          .link_1_33('Load')
        .w_OBTEST = .w_MADATCRE
          .link_1_42('Load')
          .link_1_45('Load')
          .link_1_51('Load')
        .w_MATIPSTO = NVL(MATIPSTO,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'MAN_DATI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gste_amd
    local ogg
    ogg=this.getctrl ("S\<tampa")
    ogg.enabled=this.w_MATIPMAN<>'C' AND not empty(this.w_OQRY) And this.cFunction<>'Load'  And this.cFunction<>'Edit' 
    this.oPgFrm.Pages(2).Enabled = this.w_MATIPMAN='D'
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MACODICE = space(15)
      .w_MADATCRE = ctod("  /  /  ")
      .w_MATIPMAN = space(1)
      .w_MATIPSEQ = space(4)
      .w_MARIFMAN = space(80)
      .w_MACODCLI = space(15)
      .w_MACOIBAN = space(34)
      .w_MATIPSDD = space(1)
      .w_MADATINV = ctod("  /  /  ")
      .w_MADATSOT = ctod("  /  /  ")
      .w_MA__NOTE = space(0)
      .w_MADATFIN = ctod("  /  /  ")
      .w_MANOMFIL = space(254)
      .w_DESCLI = space(60)
      .w_DESCLI = space(60)
      .w_CODCLI = space(15)
      .w_MAIDCRED = space(23)
      .w_CODICE_MANDATO = space(15)
      .w_TESTSEL = space(1)
      .w_TIPO = space(1)
      .w_CODAZI = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPCLF = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_TTCOGTIT = space(25)
      .w_TTNOMTIT = space(25)
      .w_COD_AZI = space(5)
      .w_AZPERAZI = space(1)
      .w_TIPSL = space(2)
      .w_SEDILEG = space(5)
      .w_SEINDIRI = space(35)
      .w_SECODDES = space(5)
      .w_SE___CAP = space(9)
      .w_SEPROVIN = space(2)
      .w_SELOCALI = space(30)
      .w_NAZ_AZIENDA = space(3)
      .w_DESC_AZIENDA = space(35)
      .w_MATIPSTO = space(1)
      .w_TIPSEQ = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_MADATCRE = i_datsys
        .w_MATIPMAN = 'D'
        .w_MATIPSEQ = 'RCUR'
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_MACODCLI))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_MATIPSDD = 'C'
          .DoRTCalc(9,15,.f.)
        .w_CODCLI = .w_MACODCLI
        .oPgFrm.Page2.oPag.Scad_Mandati.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_CODICE_MANDATO = .w_MACODICE
          .DoRTCalc(19,19,.f.)
        .w_TIPO = 'A'
        .w_CODAZI = i_Codazi
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_CODAZI))
          .link_1_33('Full')
          endif
        .w_OBTEST = .w_MADATCRE
        .w_TIPCLF = 'C'
          .DoRTCalc(24,26,.f.)
        .w_COD_AZI = i_Codazi
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_COD_AZI))
          .link_1_42('Full')
          endif
          .DoRTCalc(28,28,.f.)
        .w_TIPSL = 'SL'
        .w_SEDILEG = i_CODAZI
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_SEDILEG))
          .link_1_45('Full')
          endif
          .DoRTCalc(31,35,.f.)
        .w_NAZ_AZIENDA = g_Codnaz
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_NAZ_AZIENDA))
          .link_1_51('Full')
          endif
          .DoRTCalc(37,37,.f.)
        .w_MATIPSTO = ' '
        .w_TIPSEQ = 'RCUR'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAN_DATI')
    this.DoRTCalc(40,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMADATCRE_1_2.enabled = i_bVal
      .Page1.oPag.oMATIPMAN_1_3.enabled = i_bVal
      .Page1.oPag.oMATIPSEQ_1_4.enabled = i_bVal
      .Page1.oPag.oMARIFMAN_1_5.enabled = i_bVal
      .Page1.oPag.oMACODCLI_1_6.enabled = i_bVal
      .Page1.oPag.oMACOIBAN_1_11.enabled = i_bVal
      .Page1.oPag.oMATIPSDD_1_12.enabled = i_bVal
      .Page1.oPag.oMADATINV_1_13.enabled = i_bVal
      .Page1.oPag.oMADATSOT_1_14.enabled = i_bVal
      .Page1.oPag.oMA__NOTE_1_15.enabled = i_bVal
      .Page1.oPag.oMADATFIN_1_16.enabled = i_bVal
      .Page1.oPag.oMANOMFIL_1_18.enabled = i_bVal
      .Page1.oPag.oMAIDCRED_1_27.enabled = i_bVal
      .Page1.oPag.oMATIPSTO_1_53.enabled = i_bVal
      .Page2.oPag.oTIPSEQ_2_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBtn_1_28.enabled = .Page1.oPag.oBtn_1_28.mCond()
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
      .Page1.oPag.oBtn_1_32.enabled = i_bVal
      .Page2.oPag.oBtn_2_9.enabled = i_bVal
      .Page2.oPag.oBtn_2_10.enabled = i_bVal
      .Page2.oPag.oBtn_2_11.enabled = i_bVal
      .Page2.oPag.oBtn_2_12.enabled = i_bVal
      .Page2.oPag.oBtn_2_13.enabled = i_bVal
      .Page2.oPag.oBtn_2_14.enabled = i_bVal
      .Page2.oPag.Scad_Mandati.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMACODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSTE_MMD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MAN_DATI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gste_amd
    local ogg
    ogg=this.getctrl ("S\<tampa")
    ogg.enabled=this.w_MATIPMAN<>'C' AND not empty(this.w_OQRY) And this.cFunction<>'Load'  And this.cFunction<>'Edit' 
    this.oPgFrm.Pages(2).Enabled = this.w_MATIPMAN='D'
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MMD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODICE,"MACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATCRE,"MADATCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MATIPMAN,"MATIPMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MATIPSEQ,"MATIPSEQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MARIFMAN,"MARIFMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODCLI,"MACODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACOIBAN,"MACOIBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MATIPSDD,"MATIPSDD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATINV,"MADATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATSOT,"MADATSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MA__NOTE,"MA__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATFIN,"MADATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MANOMFIL,"MANOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MAIDCRED,"MAIDCRED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MATIPSTO,"MATIPSTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
    i_lTable = "MAN_DATI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAN_DATI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MAN_DATI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MAN_DATI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAN_DATI')
        i_extval=cp_InsertValODBCExtFlds(this,'MAN_DATI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MACODICE,MADATCRE,MATIPMAN,MATIPSEQ,MARIFMAN"+;
                  ",MACODCLI,MACOIBAN,MATIPSDD,MADATINV,MADATSOT"+;
                  ",MA__NOTE,MADATFIN,MANOMFIL,MAIDCRED,MATIPSTO"+;
                  ",UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MACODICE)+;
                  ","+cp_ToStrODBC(this.w_MADATCRE)+;
                  ","+cp_ToStrODBC(this.w_MATIPMAN)+;
                  ","+cp_ToStrODBC(this.w_MATIPSEQ)+;
                  ","+cp_ToStrODBC(this.w_MARIFMAN)+;
                  ","+cp_ToStrODBCNull(this.w_MACODCLI)+;
                  ","+cp_ToStrODBC(this.w_MACOIBAN)+;
                  ","+cp_ToStrODBC(this.w_MATIPSDD)+;
                  ","+cp_ToStrODBC(this.w_MADATINV)+;
                  ","+cp_ToStrODBC(this.w_MADATSOT)+;
                  ","+cp_ToStrODBC(this.w_MA__NOTE)+;
                  ","+cp_ToStrODBC(this.w_MADATFIN)+;
                  ","+cp_ToStrODBC(this.w_MANOMFIL)+;
                  ","+cp_ToStrODBC(this.w_MAIDCRED)+;
                  ","+cp_ToStrODBC(this.w_MATIPSTO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAN_DATI')
        i_extval=cp_InsertValVFPExtFlds(this,'MAN_DATI')
        cp_CheckDeletedKey(i_cTable,0,'MACODICE',this.w_MACODICE)
        INSERT INTO (i_cTable);
              (MACODICE,MADATCRE,MATIPMAN,MATIPSEQ,MARIFMAN,MACODCLI,MACOIBAN,MATIPSDD,MADATINV,MADATSOT,MA__NOTE,MADATFIN,MANOMFIL,MAIDCRED,MATIPSTO,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MACODICE;
                  ,this.w_MADATCRE;
                  ,this.w_MATIPMAN;
                  ,this.w_MATIPSEQ;
                  ,this.w_MARIFMAN;
                  ,this.w_MACODCLI;
                  ,this.w_MACOIBAN;
                  ,this.w_MATIPSDD;
                  ,this.w_MADATINV;
                  ,this.w_MADATSOT;
                  ,this.w_MA__NOTE;
                  ,this.w_MADATFIN;
                  ,this.w_MANOMFIL;
                  ,this.w_MAIDCRED;
                  ,this.w_MATIPSTO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MAN_DATI_IDX,i_nConn)
      *
      * update MAN_DATI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MAN_DATI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MADATCRE="+cp_ToStrODBC(this.w_MADATCRE)+;
             ",MATIPMAN="+cp_ToStrODBC(this.w_MATIPMAN)+;
             ",MATIPSEQ="+cp_ToStrODBC(this.w_MATIPSEQ)+;
             ",MARIFMAN="+cp_ToStrODBC(this.w_MARIFMAN)+;
             ",MACODCLI="+cp_ToStrODBCNull(this.w_MACODCLI)+;
             ",MACOIBAN="+cp_ToStrODBC(this.w_MACOIBAN)+;
             ",MATIPSDD="+cp_ToStrODBC(this.w_MATIPSDD)+;
             ",MADATINV="+cp_ToStrODBC(this.w_MADATINV)+;
             ",MADATSOT="+cp_ToStrODBC(this.w_MADATSOT)+;
             ",MA__NOTE="+cp_ToStrODBC(this.w_MA__NOTE)+;
             ",MADATFIN="+cp_ToStrODBC(this.w_MADATFIN)+;
             ",MANOMFIL="+cp_ToStrODBC(this.w_MANOMFIL)+;
             ",MAIDCRED="+cp_ToStrODBC(this.w_MAIDCRED)+;
             ",MATIPSTO="+cp_ToStrODBC(this.w_MATIPSTO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MAN_DATI')
        i_cWhere = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
        UPDATE (i_cTable) SET;
              MADATCRE=this.w_MADATCRE;
             ,MATIPMAN=this.w_MATIPMAN;
             ,MATIPSEQ=this.w_MATIPSEQ;
             ,MARIFMAN=this.w_MARIFMAN;
             ,MACODCLI=this.w_MACODCLI;
             ,MACOIBAN=this.w_MACOIBAN;
             ,MATIPSDD=this.w_MATIPSDD;
             ,MADATINV=this.w_MADATINV;
             ,MADATSOT=this.w_MADATSOT;
             ,MA__NOTE=this.w_MA__NOTE;
             ,MADATFIN=this.w_MADATFIN;
             ,MANOMFIL=this.w_MANOMFIL;
             ,MAIDCRED=this.w_MAIDCRED;
             ,MATIPSTO=this.w_MATIPSTO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MMD : Saving
      this.GSTE_MMD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MACODICE,"MDCODICE";
             )
      this.GSTE_MMD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSTE_MMD : Deleting
    this.GSTE_MMD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MACODICE,"MDCODICE";
           )
    this.GSTE_MMD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MAN_DATI_IDX,i_nConn)
      *
      * delete MAN_DATI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAN_DATI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_DATI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_MATIPMAN<>.w_MATIPMAN
            .w_MATIPSEQ = 'RCUR'
        endif
        .DoRTCalc(5,15,.t.)
        if .o_MACODCLI<>.w_MACODCLI
            .w_CODCLI = .w_MACODCLI
        endif
        .oPgFrm.Page2.oPag.Scad_Mandati.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(17,17,.t.)
        if .o_MACODICE<>.w_MACODICE
            .w_CODICE_MANDATO = .w_MACODICE
        endif
        .DoRTCalc(19,20,.t.)
          .link_1_33('Full')
        if .o_MADATCRE<>.w_MADATCRE
            .w_OBTEST = .w_MADATCRE
        endif
        .DoRTCalc(23,26,.t.)
          .link_1_42('Full')
        .DoRTCalc(28,29,.t.)
          .link_1_45('Full')
        .DoRTCalc(31,35,.t.)
          .link_1_51('Full')
        if .o_MATIPMAN<>.w_MATIPMAN
          .Calculate_JYBGTABQNC()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(37,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.Scad_Mandati.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return

  proc Calculate_CKROZOCCYT()
    with this
          * --- Aggiorna variabili Testsel
          GSTE_BME(this;
              ,'SEL_X';
             )
    endwith
  endproc
  proc Calculate_MCXAHETREY()
    with this
          * --- Azzeramento codice IBAN
          .w_MACOIBAN = Space(34)
    endwith
  endproc
  proc Calculate_XGNXSREGYJ()
    with this
          * --- Salvo seriale gste1bmb
          GSTE1BMB(this;
              ,.w_MACODICE;
              ,this.name;
              ,.t.;
             )
    endwith
  endproc
  proc Calculate_LQVMDQOPYP()
    with this
          * --- Ricarico record gste1bmb
          GSTE1BMB(this;
              ,.w_NEW_SERIAL;
              ,this.name;
             )
    endwith
  endproc
  proc Calculate_JYBGTABQNC()
    with this
          * --- Sbianca cliente
          .w_MACODCLI = IIF (  .w_MATIPMAN='C'  ,SPACE(15) ,  .w_MACODCLI   )
          .w_MATIPSDD = IIF (  .w_MATIPMAN='C'  ,'C',  .w_MATIPSDD   )
          .w_MATIPSTO = IIF (  .w_MATIPMAN='C'  ,' ',  .w_MATIPSTO   )
          .w_DESCLI = IIF (  .w_MATIPMAN='C'  ,' ',  .w_DESCLI   )
          .w_MACOIBAN = IIF (  .w_MATIPMAN='C'  ,' ',  .w_MACOIBAN   )
          .w_MAIDCRED = IIF (  .w_MATIPMAN='C'  ,' ',  .w_MAIDCRED   )
          .oPgFrm.Pages(2).Enabled = .w_MATIPMAN='D'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMATIPMAN_1_3.enabled = this.oPgFrm.Page1.oPag.oMATIPMAN_1_3.mCond()
    this.oPgFrm.Page1.oPag.oMACODCLI_1_6.enabled = this.oPgFrm.Page1.oPag.oMACODCLI_1_6.mCond()
    this.oPgFrm.Page1.oPag.oMACOIBAN_1_11.enabled = this.oPgFrm.Page1.oPag.oMACOIBAN_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.enabled = this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.mCond()
    this.oPgFrm.Page1.oPag.oMADATINV_1_13.enabled = this.oPgFrm.Page1.oPag.oMADATINV_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMAIDCRED_1_27.enabled = this.oPgFrm.Page1.oPag.oMAIDCRED_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMATIPSTO_1_53.enabled = this.oPgFrm.Page1.oPag.oMATIPSTO_1_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_MATIPMAN<>'D' or this.cfunction='Load')
    this.oPgFrm.Page1.oPag.oMATIPSEQ_1_4.visible=!this.oPgFrm.Page1.oPag.oMATIPSEQ_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.visible=!this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.mHide()
    this.oPgFrm.Page1.oPag.oMANOMFIL_1_18.visible=!this.oPgFrm.Page1.oPag.oMANOMFIL_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_25.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Scad_Mandati menucheck") or lower(cEvent)==lower("w_scad_mandati after query") or lower(cEvent)==lower("w_Scad_Mandati row checked") or lower(cEvent)==lower("w_Scad_Mandati row unchecked")
          .Calculate_CKROZOCCYT()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.Scad_Mandati.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
        if lower(cEvent)==lower("w_MACODCLI Changed")
          .Calculate_MCXAHETREY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_XGNXSREGYJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_LQVMDQOPYP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MACODCLI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MACODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_MACODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MACODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MACODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MACODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMACODCLI_1_6'),i_cWhere,'GSAR_ACL',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MACODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_MACODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(60))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MACODCLI = space(15)
      endif
      this.w_DESCLI = space(60)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_MACODCLI = space(15)
        this.w_DESCLI = space(60)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI)
            select TTCODAZI,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.TTCODAZI,space(5))
      this.w_TTCOGTIT = NVL(_Link_.TTCOGTIT,space(25))
      this.w_TTNOMTIT = NVL(_Link_.TTNOMTIT,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TTCOGTIT = space(25)
      this.w_TTNOMTIT = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_AZI
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPERAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COD_AZI)
            select AZCODAZI,AZPERAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_AZPERAZI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SECODDES";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SECODDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SELOCALI = NVL(_Link_.SELOCALI,space(30))
      this.w_SEPROVIN = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI = NVL(_Link_.SEINDIRI,space(35))
      this.w_SE___CAP = NVL(_Link_.SE___CAP,space(9))
      this.w_SECODDES = NVL(_Link_.SECODDES,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SELOCALI = space(30)
      this.w_SEPROVIN = space(2)
      this.w_SEINDIRI = space(35)
      this.w_SE___CAP = space(9)
      this.w_SECODDES = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NAZ_AZIENDA
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NAZ_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NAZ_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NAZ_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NAZ_AZIENDA)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NAZ_AZIENDA = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESC_AZIENDA = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NAZ_AZIENDA = space(3)
      endif
      this.w_DESC_AZIENDA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NAZ_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMACODICE_1_1.value==this.w_MACODICE)
      this.oPgFrm.Page1.oPag.oMACODICE_1_1.value=this.w_MACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATCRE_1_2.value==this.w_MADATCRE)
      this.oPgFrm.Page1.oPag.oMADATCRE_1_2.value=this.w_MADATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oMATIPMAN_1_3.RadioValue()==this.w_MATIPMAN)
      this.oPgFrm.Page1.oPag.oMATIPMAN_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMATIPSEQ_1_4.RadioValue()==this.w_MATIPSEQ)
      this.oPgFrm.Page1.oPag.oMATIPSEQ_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMARIFMAN_1_5.value==this.w_MARIFMAN)
      this.oPgFrm.Page1.oPag.oMARIFMAN_1_5.value=this.w_MARIFMAN
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODCLI_1_6.value==this.w_MACODCLI)
      this.oPgFrm.Page1.oPag.oMACODCLI_1_6.value=this.w_MACODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oMACOIBAN_1_11.value==this.w_MACOIBAN)
      this.oPgFrm.Page1.oPag.oMACOIBAN_1_11.value=this.w_MACOIBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.RadioValue()==this.w_MATIPSDD)
      this.oPgFrm.Page1.oPag.oMATIPSDD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATINV_1_13.value==this.w_MADATINV)
      this.oPgFrm.Page1.oPag.oMADATINV_1_13.value=this.w_MADATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATSOT_1_14.value==this.w_MADATSOT)
      this.oPgFrm.Page1.oPag.oMADATSOT_1_14.value=this.w_MADATSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oMA__NOTE_1_15.value==this.w_MA__NOTE)
      this.oPgFrm.Page1.oPag.oMA__NOTE_1_15.value=this.w_MA__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATFIN_1_16.value==this.w_MADATFIN)
      this.oPgFrm.Page1.oPag.oMADATFIN_1_16.value=this.w_MADATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMANOMFIL_1_18.value==this.w_MANOMFIL)
      this.oPgFrm.Page1.oPag.oMANOMFIL_1_18.value=this.w_MANOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_24.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_24.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_2.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_2.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCLI_2_3.value==this.w_CODCLI)
      this.oPgFrm.Page2.oPag.oCODCLI_2_3.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAIDCRED_1_27.value==this.w_MAIDCRED)
      this.oPgFrm.Page1.oPag.oMAIDCRED_1_27.value=this.w_MAIDCRED
    endif
    if not(this.oPgFrm.Page1.oPag.oMATIPSTO_1_53.RadioValue()==this.w_MATIPSTO)
      this.oPgFrm.Page1.oPag.oMATIPSTO_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPSEQ_2_17.RadioValue()==this.w_TIPSEQ)
      this.oPgFrm.Page2.oPag.oTIPSEQ_2_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MAN_DATI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MADATCRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMADATCRE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_MADATCRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MARIFMAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARIFMAN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_MARIFMAN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MACODCLI)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.cFunction='Load' and .w_MATIPMAN<>'C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMACODCLI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MACODCLI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MMD.CheckForm()
      if i_bres
        i_bres=  .GSTE_MMD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MACODICE = this.w_MACODICE
    this.o_MADATCRE = this.w_MADATCRE
    this.o_MATIPMAN = this.w_MATIPMAN
    this.o_MACODCLI = this.w_MACODCLI
    * --- GSTE_MMD : Depends On
    this.GSTE_MMD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgste_amdPag1 as StdContainer
  Width  = 766
  height = 643
  stdWidth  = 766
  stdheight = 643
  resizeXpos=383
  resizeYpos=172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMACODICE_1_1 as StdField with uid="MKZLLPYSRW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MACODICE", cQueryName = "MACODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice mandato",;
    HelpContextID = 17383947,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=165, Top=18, InputMask=replicate('X',15)

  add object oMADATCRE_1_2 as StdField with uid="JNXPAMSMEW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MADATCRE", cQueryName = "MADATCRE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di caricamento del mandato",;
    HelpContextID = 201019915,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=518, Top=18


  add object oMATIPMAN_1_3 as StdCombo with uid="KEPWTNKKFM",rtseq=3,rtrep=.f.,left=166,top=48,width=202,height=21;
    , ToolTipText = "Tipologia mandato";
    , HelpContextID = 96752148;
    , cFormVar="w_MATIPMAN",RowSource=""+"Generico,"+"Intestato,"+"Intestato con dettaglio scadenze", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oMATIPMAN_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'G',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oMATIPMAN_1_3.GetRadio()
    this.Parent.oContained.w_MATIPMAN = this.RadioValue()
    return .t.
  endfunc

  func oMATIPMAN_1_3.SetRadio()
    this.Parent.oContained.w_MATIPMAN=trim(this.Parent.oContained.w_MATIPMAN)
    this.value = ;
      iif(this.Parent.oContained.w_MATIPMAN=='C',1,;
      iif(this.Parent.oContained.w_MATIPMAN=='G',2,;
      iif(this.Parent.oContained.w_MATIPMAN=='D',3,;
      0)))
  endfunc

  func oMATIPMAN_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oMATIPSEQ_1_4 as StdCombo with uid="MRITNWOGSM",rtseq=4,rtrep=.f.,left=518,top=48,width=132,height=21;
    , ToolTipText = "Tipo sequenza";
    , HelpContextID = 197415447;
    , cFormVar="w_MATIPSEQ",RowSource=""+"First,"+"Recurrent,"+"Last,"+"One-off", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oMATIPSEQ_1_4.RadioValue()
    return(iif(this.value =1,'FRST',;
    iif(this.value =2,'RCUR',;
    iif(this.value =3,'FNAL',;
    iif(this.value =4,'OOFF',;
    space(4))))))
  endfunc
  func oMATIPSEQ_1_4.GetRadio()
    this.Parent.oContained.w_MATIPSEQ = this.RadioValue()
    return .t.
  endfunc

  func oMATIPSEQ_1_4.SetRadio()
    this.Parent.oContained.w_MATIPSEQ=trim(this.Parent.oContained.w_MATIPSEQ)
    this.value = ;
      iif(this.Parent.oContained.w_MATIPSEQ=='FRST',1,;
      iif(this.Parent.oContained.w_MATIPSEQ=='RCUR',2,;
      iif(this.Parent.oContained.w_MATIPSEQ=='FNAL',3,;
      iif(this.Parent.oContained.w_MATIPSEQ=='OOFF',4,;
      0))))
  endfunc

  func oMATIPSEQ_1_4.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN='D')
    endwith
  endfunc

  add object oMARIFMAN_1_5 as StdField with uid="BLCDTAAQFU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MARIFMAN", cQueryName = "MARIFMAN",;
    bObbl = .t. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione mandato",;
    HelpContextID = 86258196,;
   bGlobalFont=.t.,;
    Height=21, Width=573, Left=165, Top=76, InputMask=replicate('X',80)

  add object oMACODCLI_1_6 as StdField with uid="DZMINFNKFX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MACODCLI", cQueryName = "MACODCLI",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente associato al mandato",;
    HelpContextID = 83279345,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=165, Top=104, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_MACODCLI"

  func oMACODCLI_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' and .w_MATIPMAN<>'C')
    endwith
   endif
  endfunc

  func oMACODCLI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACODCLI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACODCLI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMACODCLI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"Clienti",'',this.parent.oContained
  endproc
  proc oMACODCLI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_MACODCLI
     i_obj.ecpSave()
  endproc


  add object oBtn_1_7 as StdButton with uid="PZKLYNNGQZ",left=423, top=131, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona codice IBAN";
    , HelpContextID = 210182954;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      do Gste_Smd with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Macodcli) AND .w_MATIPMAN<>'C')
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Macodcli) Or .cFunction='Query')
     endwith
    endif
  endfunc

  add object oMACOIBAN_1_11 as StdField with uid="WUXRKBAHDT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MACOIBAN", cQueryName = "MACOIBAN",;
    bObbl = .f. , nPag = 1, value=space(34), bMultilanguage =  .f.,;
    ToolTipText = "Codice IBAN",;
    HelpContextID = 173621780,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=165, Top=132, InputMask=replicate('X',34)

  func oMACOIBAN_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MATIPMAN<>'C')
    endwith
   endif
  endfunc


  add object oMATIPSDD_1_12 as StdCombo with uid="YNSIVZVOGR",rtseq=8,rtrep=.f.,left=543,top=129,width=69,height=21;
    , ToolTipText = "Tipo SDD";
    , HelpContextID = 197415434;
    , cFormVar="w_MATIPSDD",RowSource=""+"CORE,"+"B2B", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oMATIPSDD_1_12.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'B',;
    space(1))))
  endfunc
  func oMATIPSDD_1_12.GetRadio()
    this.Parent.oContained.w_MATIPSDD = this.RadioValue()
    return .t.
  endfunc

  func oMATIPSDD_1_12.SetRadio()
    this.Parent.oContained.w_MATIPSDD=trim(this.Parent.oContained.w_MATIPSDD)
    this.value = ;
      iif(this.Parent.oContained.w_MATIPSDD=='C',1,;
      iif(this.Parent.oContained.w_MATIPSDD=='B',2,;
      0))
  endfunc

  func oMATIPSDD_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MATIPMAN<>'C')
    endwith
   endif
  endfunc

  func oMATIPSDD_1_12.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN='C')
    endwith
  endfunc

  add object oMADATINV_1_13 as StdField with uid="WHQVOVUQAB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MADATINV", cQueryName = "MADATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'invio a cliente",;
    HelpContextID = 235187684,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=164, Top=362

  func oMADATINV_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MATIPMAN<>'C')
    endwith
   endif
  endfunc

  add object oMADATSOT_1_14 as StdField with uid="NJJKXPPFBG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MADATSOT", cQueryName = "MADATSOT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di sottoscrizione del cliente",;
    HelpContextID = 67415526,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=398, Top=362

  add object oMA__NOTE_1_15 as StdMemo with uid="GEPKYFMANB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MA__NOTE", cQueryName = "MA__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 129696267,;
   bGlobalFont=.t.,;
    Height=165, Width=569, Left=164, Top=189, tabstop=.f.

  add object oMADATFIN_1_16 as StdField with uid="LSPBRKGPGP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MADATFIN", cQueryName = "MADATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit� del mandato",;
    HelpContextID = 17083884,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=636, Top=362

  add object oMANOMFIL_1_18 as StdField with uid="RDNLBXCNBI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MANOMFIL", cQueryName = "MANOMFIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file distinta generato",;
    HelpContextID = 23465454,;
   bGlobalFont=.t.,;
    Height=21, Width=572, Left=165, Top=411, InputMask=replicate('X',254)

  func oMANOMFIL_1_18.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN<>'G' OR .w_MATIPSEQ<>'RCUR')
    endwith
  endfunc

  add object oDESCLI_1_24 as StdField with uid="GOOCNRTXHO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 25052470,;
   bGlobalFont=.t.,;
    Height=21, Width=438, Left=300, Top=104, InputMask=replicate('X',60)


  add object oLinkPC_1_25 as stdDynamicChildContainer with uid="WYKDTAARPI",left=225, top=533, width=270, height=110, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc

  add object oMAIDCRED_1_27 as StdField with uid="YORFNFZQZF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MAIDCRED", cQueryName = "MAIDCRED",;
    bObbl = .f. , nPag = 1, value=space(23), bMultilanguage =  .f.,;
    HelpContextID = 166633994,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=165, Top=160, InputMask=replicate('X',23)

  func oMAIDCRED_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MATIPMAN<>'C')
    endwith
   endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="KSYEGVMHVL",left=355, top=160, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona identificativo del creditore";
    , HelpContextID = 210182954;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      do Gste1Smd with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MATIPMAN<>'C')
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Query')
     endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="KWFWCRWNHO",left=614, top=445, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 148997398;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) And .cFunction<>'Load'  And .cFunction<>'Edit'  AND .w_MATIPMAN<>'C')
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_Macodice))
     endwith
    endif
  endfunc


  add object oObj_1_30 as cp_outputCombo with uid="ZLERXIVPWT",left=225, top=452, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 236049126


  add object oBtn_1_32 as StdButton with uid="AITCBYAXMM",left=668, top=136, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare i dati inseriti";
    , HelpContextID = 63758457;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSTE1BMB(this.Parent.oContained,.w_Macodice,this.name,.f.,.t.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Load' And Not Empty(.w_Macodice) And Not Empty(.w_Macodcli)  and .w_MATIPMAN='D' And Not Empty(.w_Marifman) )
      endwith
    endif
  endfunc


  add object oMATIPSTO_1_53 as StdCombo with uid="SMIRKHWZLF",value=1,rtseq=38,rtrep=.f.,left=543,top=159,width=73,height=21;
    , ToolTipText = "Identifica il flag facolt� storno";
    , HelpContextID = 197415445;
    , cFormVar="w_MATIPSTO",RowSource=""+"Nessuno,"+"1,"+"2,"+"3,"+"4,"+"8,"+"9", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oMATIPSTO_1_53.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    iif(this.value =6,'8',;
    iif(this.value =7,'9',;
    space(1)))))))))
  endfunc
  func oMATIPSTO_1_53.GetRadio()
    this.Parent.oContained.w_MATIPSTO = this.RadioValue()
    return .t.
  endfunc

  func oMATIPSTO_1_53.SetRadio()
    this.Parent.oContained.w_MATIPSTO=trim(this.Parent.oContained.w_MATIPSTO)
    this.value = ;
      iif(this.Parent.oContained.w_MATIPSTO=='',1,;
      iif(this.Parent.oContained.w_MATIPSTO=='1',2,;
      iif(this.Parent.oContained.w_MATIPSTO=='2',3,;
      iif(this.Parent.oContained.w_MATIPSTO=='3',4,;
      iif(this.Parent.oContained.w_MATIPSTO=='4',5,;
      iif(this.Parent.oContained.w_MATIPSTO=='8',6,;
      iif(this.Parent.oContained.w_MATIPSTO=='9',7,;
      0)))))))
  endfunc

  func oMATIPSTO_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MATIPMAN<>'C')
    endwith
   endif
  endfunc

  add object oStr_1_8 as StdString with uid="CJUZKKFZLR",Visible=.t., Left=67, Top=21,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BHNZCOADBF",Visible=.t., Left=434, Top=21,;
    Alignment=1, Width=81, Height=18,;
    Caption="Data mandato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="ITILNRPCME",Visible=.t., Left=55, Top=79,;
    Alignment=1, Width=106, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="GNHXKEWPIF",Visible=.t., Left=73, Top=135,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codice IBAN:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="EUWTHHLVKN",Visible=.t., Left=454, Top=132,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo SDD:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN='C')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="MKXNKYTLUJ",Visible=.t., Left=75, Top=367,;
    Alignment=1, Width=85, Height=18,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="XFOQGGZJTN",Visible=.t., Left=245, Top=367,;
    Alignment=1, Width=146, Height=18,;
    Caption="Data sottoscrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="WRLFNOUZPG",Visible=.t., Left=486, Top=364,;
    Alignment=1, Width=145, Height=18,;
    Caption="Data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LWQTXMAHKB",Visible=.t., Left=101, Top=106,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IYJEHRJENI",Visible=.t., Left=9, Top=164,;
    Alignment=1, Width=152, Height=15,;
    Caption="Identificativo del creditore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ZDLOVMWIXH",Visible=.t., Left=135, Top=455,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="KKUAKSKWEG",Visible=.t., Left=454, Top=162,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo Storno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="HMVSOAPCEH",Visible=.t., Left=107, Top=48,;
    Alignment=1, Width=54, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="GHKCRKBRBS",Visible=.t., Left=396, Top=48,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo sequenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN='D')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="YNZRMMDGNA",Visible=.t., Left=16, Top=413,;
    Alignment=1, Width=145, Height=18,;
    Caption="Nome file distinta:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_MATIPMAN<>'G' OR .w_MATIPSEQ<>'RCUR')
    endwith
  endfunc
enddefine
define class tgste_amdPag2 as StdContainer
  Width  = 766
  height = 643
  stdWidth  = 766
  stdheight = 643
  resizeXpos=339
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCLI_2_2 as StdField with uid="MUKTSLBOUR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 25052470,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=208, Top=13, InputMask=replicate('X',60)

  add object oCODCLI_2_3 as StdField with uid="KIGXGNGYCR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 24993574,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=75, Top=13, InputMask=replicate('X',15)


  add object Scad_Mandati as cp_szoombox with uid="ZIHJRMWNDA",left=7, top=58, width=750,height=398,;
    caption='Scadenze',;
   bGlobalFont=.t.,;
    cTable="PAR_TITE",cZoomFile="GSTE1SMD",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 131054453


  add object oBtn_2_9 as StdButton with uid="GQMUPTTTCY",left=11, top=466, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare le scadenze da eliminare/aggiornare";
    , HelpContextID = 148997398;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSTE_BME(this.Parent.oContained,"SEL_S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit')
      endwith
    endif
  endfunc


  add object oBtn_2_10 as StdButton with uid="ZCYHLDNLVF",left=66, top=466, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare le scadenze da eliminare/aggiornare";
    , HelpContextID = 148997398;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSTE_BME(this.Parent.oContained,"SEL_D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit')
      endwith
    endif
  endfunc


  add object oBtn_2_11 as StdButton with uid="GUQBLQMZBT",left=121, top=466, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione delle scadenze da eliminare/aggiornare";
    , HelpContextID = 148997398;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSTE_BME(this.Parent.oContained,"SEL_I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit')
      endwith
    endif
  endfunc


  add object oBtn_2_12 as StdButton with uid="GBHECXBFMC",left=606, top=466, width=48,height=45,;
    CpPicture="bmp\ABBINA.bmp", caption="", nPag=2;
    , ToolTipText = "Abbina partite / scadenze";
    , HelpContextID = 113924241;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      do GSTE2SMD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' AND Not Empty(.w_Macodice) And .w_Testsel<>'S')
      endwith
    endif
  endfunc


  add object oBtn_2_13 as StdButton with uid="CQRONLTTLT",left=658, top=466, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare il tipo sequenza ";
    , HelpContextID = 148997398;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSTE_BME(this.Parent.oContained,"TIPS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' And .w_Testsel='S')
      endwith
    endif
  endfunc


  add object oBtn_2_14 as StdButton with uid="FDAHYUCEQK",left=709, top=466, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per eliminare le scadenze/partite selezionate ";
    , HelpContextID = 142199878;
    , caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      with this.Parent.oContained
        GSTE_BME(this.Parent.oContained,"CANC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Edit' And .w_Testsel='S')
      endwith
    endif
  endfunc


  add object oTIPSEQ_2_17 as StdCombo with uid="VMVQXQYOKY",rtseq=39,rtrep=.f.,left=362,top=476,width=186,height=21;
    , ToolTipText = "Tipo sequenza";
    , HelpContextID = 152967734;
    , cFormVar="w_TIPSEQ",RowSource=""+"First,"+"Recurrent,"+"Last,"+"One-off", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oTIPSEQ_2_17.RadioValue()
    return(iif(this.value =1,'FRST',;
    iif(this.value =2,'RCUR',;
    iif(this.value =3,'FNAL',;
    iif(this.value =4,'OOFF',;
    space(1))))))
  endfunc
  func oTIPSEQ_2_17.GetRadio()
    this.Parent.oContained.w_TIPSEQ = this.RadioValue()
    return .t.
  endfunc

  func oTIPSEQ_2_17.SetRadio()
    this.Parent.oContained.w_TIPSEQ=trim(this.Parent.oContained.w_TIPSEQ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSEQ=='FRST',1,;
      iif(this.Parent.oContained.w_TIPSEQ=='RCUR',2,;
      iif(this.Parent.oContained.w_TIPSEQ=='FNAL',3,;
      iif(this.Parent.oContained.w_TIPSEQ=='OOFF',4,;
      0))))
  endfunc

  add object oStr_2_1 as StdString with uid="LIYWMTIGDO",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="PAYNPMFJWC",Visible=.t., Left=77, Top=37,;
    Alignment=0, Width=275, Height=17,;
    Caption="Scadenze con iban uguale a quello definito sul mandato"    , BackStyle=1, BackColor=RGB(0,255,128);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_16 as StdString with uid="QBGFQMSXUU",Visible=.t., Left=378, Top=37,;
    Alignment=0, Width=214, Height=17,;
    Caption="Scadenze gi� presenti in altri mandati"    , BackStyle=1, BackColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_18 as StdString with uid="HRCKSJXCZE",Visible=.t., Left=238, Top=478,;
    Alignment=1, Width=119, Height=18,;
    Caption="Tipo sequenza:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_amd','MAN_DATI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MACODICE=MAN_DATI.MACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
