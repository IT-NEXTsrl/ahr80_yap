* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_atl                                                        *
*              Tabella calendari                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-25                                                      *
* Last revis.: 2012-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_atl"))

* --- Class definition
define class tgsar_atl as StdForm
  Top    = 45
  Left   = 76

  * --- Standard Properties
  Width  = 400
  Height = 309+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-24"
  HelpContextID=175540375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  TAB_CALE_IDX = 0
  cFile = "TAB_CALE"
  cKeySelect = "TCCODICE"
  cKeyWhere  = "TCCODICE=this.w_TCCODICE"
  cKeyWhereODBC = '"TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cKeyWhereODBCqualified = '"TAB_CALE.TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cPrg = "gsar_atl"
  cComment = "Tabella calendari"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TCCODICE = space(5)
  w_TCDESCRI = space(40)
  w_TCFLDEFA = space(1)
  w_TCDA1STD = space(5)
  o_TCDA1STD = space(5)
  w_TCAL1STD = space(5)
  o_TCAL1STD = space(5)
  w_TCDA2STD = space(5)
  o_TCDA2STD = space(5)
  w_TCAL2STD = space(5)
  o_TCAL2STD = space(5)
  w_TCNUMORE = 0
  w_TC__DAY2 = space(1)
  w_TC__DAY3 = space(1)
  w_TC__DAY4 = space(1)
  w_TC__DAY5 = space(1)
  w_TC__DAY6 = space(1)
  w_TC__DAY7 = space(1)
  w_TC__DAY1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_CALE','gsar_atl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_atlPag1","gsar_atl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Calendario")
      .Pages(1).HelpContextID = 171760600
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TAB_CALE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_CALE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_CALE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TCCODICE = NVL(TCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_CALE where TCCODICE=KeySet.TCCODICE
    *
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_CALE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_CALE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_CALE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TCCODICE = NVL(TCCODICE,space(5))
        .w_TCDESCRI = NVL(TCDESCRI,space(40))
        .w_TCFLDEFA = NVL(TCFLDEFA,space(1))
        .w_TCDA1STD = NVL(TCDA1STD,space(5))
        .w_TCAL1STD = NVL(TCAL1STD,space(5))
        .w_TCDA2STD = NVL(TCDA2STD,space(5))
        .w_TCAL2STD = NVL(TCAL2STD,space(5))
        .w_TCNUMORE = NVL(TCNUMORE,0)
        .w_TC__DAY2 = NVL(TC__DAY2,space(1))
        .w_TC__DAY3 = NVL(TC__DAY3,space(1))
        .w_TC__DAY4 = NVL(TC__DAY4,space(1))
        .w_TC__DAY5 = NVL(TC__DAY5,space(1))
        .w_TC__DAY6 = NVL(TC__DAY6,space(1))
        .w_TC__DAY7 = NVL(TC__DAY7,space(1))
        .w_TC__DAY1 = NVL(TC__DAY1,space(1))
        cp_LoadRecExtFlds(this,'TAB_CALE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TCCODICE = space(5)
      .w_TCDESCRI = space(40)
      .w_TCFLDEFA = space(1)
      .w_TCDA1STD = space(5)
      .w_TCAL1STD = space(5)
      .w_TCDA2STD = space(5)
      .w_TCAL2STD = space(5)
      .w_TCNUMORE = 0
      .w_TC__DAY2 = space(1)
      .w_TC__DAY3 = space(1)
      .w_TC__DAY4 = space(1)
      .w_TC__DAY5 = space(1)
      .w_TC__DAY6 = space(1)
      .w_TC__DAY7 = space(1)
      .w_TC__DAY1 = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_TCFLDEFA = 'N'
        .w_TCDA1STD = IIF(EMPTY(.w_TCDA1STD), "09:00", formattime(.w_TCDA1STD, ":"))
        .w_TCAL1STD = IIF(EMPTY(.w_TCAL1STD), "13:00", formattime(.w_TCAL1STD, ":"))
        .w_TCDA2STD = formattime(.w_TCDA2STD, ":")
        .w_TCAL2STD = IIF(EMPTY(.w_TCDA2STD),"", formattime(.w_TCAL2STD, ":" ) )
          .DoRTCalc(8,8,.f.)
        .w_TC__DAY2 = "S"
        .w_TC__DAY3 = "S"
        .w_TC__DAY4 = "S"
        .w_TC__DAY5 = "S"
        .w_TC__DAY6 = "S"
        .w_TC__DAY7 = "N"
        .w_TC__DAY1 = "N"
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_CALE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTCFLDEFA_1_5.enabled = i_bVal
      .Page1.oPag.oTCDA1STD_1_7.enabled = i_bVal
      .Page1.oPag.oTCAL1STD_1_9.enabled = i_bVal
      .Page1.oPag.oTCDA2STD_1_11.enabled = i_bVal
      .Page1.oPag.oTCAL2STD_1_13.enabled = i_bVal
      .Page1.oPag.oTCNUMORE_1_14.enabled = i_bVal
      .Page1.oPag.oTC__DAY2_1_16.enabled = i_bVal
      .Page1.oPag.oTC__DAY3_1_17.enabled = i_bVal
      .Page1.oPag.oTC__DAY4_1_18.enabled = i_bVal
      .Page1.oPag.oTC__DAY5_1_19.enabled = i_bVal
      .Page1.oPag.oTC__DAY6_1_20.enabled = i_bVal
      .Page1.oPag.oTC__DAY7_1_21.enabled = i_bVal
      .Page1.oPag.oTC__DAY1_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTCCODICE_1_1.enabled = .t.
        .Page1.oPag.oTCDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_CALE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCCODICE,"TCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDESCRI,"TCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCFLDEFA,"TCFLDEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDA1STD,"TCDA1STD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCAL1STD,"TCAL1STD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDA2STD,"TCDA2STD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCAL2STD,"TCAL2STD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCNUMORE,"TCNUMORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY2,"TC__DAY2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY3,"TC__DAY3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY4,"TC__DAY4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY5,"TC__DAY5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY6,"TC__DAY6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY7,"TC__DAY7",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__DAY1,"TC__DAY1",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    i_lTable = "TAB_CALE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_CALE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_CALE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_CALE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_CALE')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_CALE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TCCODICE,TCDESCRI,TCFLDEFA,TCDA1STD,TCAL1STD"+;
                  ",TCDA2STD,TCAL2STD,TCNUMORE,TC__DAY2,TC__DAY3"+;
                  ",TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TC__DAY1 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TCCODICE)+;
                  ","+cp_ToStrODBC(this.w_TCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TCFLDEFA)+;
                  ","+cp_ToStrODBC(this.w_TCDA1STD)+;
                  ","+cp_ToStrODBC(this.w_TCAL1STD)+;
                  ","+cp_ToStrODBC(this.w_TCDA2STD)+;
                  ","+cp_ToStrODBC(this.w_TCAL2STD)+;
                  ","+cp_ToStrODBC(this.w_TCNUMORE)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY2)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY3)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY4)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY5)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY6)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY7)+;
                  ","+cp_ToStrODBC(this.w_TC__DAY1)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_CALE')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_CALE')
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE)
        INSERT INTO (i_cTable);
              (TCCODICE,TCDESCRI,TCFLDEFA,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TCNUMORE,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TC__DAY1  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TCCODICE;
                  ,this.w_TCDESCRI;
                  ,this.w_TCFLDEFA;
                  ,this.w_TCDA1STD;
                  ,this.w_TCAL1STD;
                  ,this.w_TCDA2STD;
                  ,this.w_TCAL2STD;
                  ,this.w_TCNUMORE;
                  ,this.w_TC__DAY2;
                  ,this.w_TC__DAY3;
                  ,this.w_TC__DAY4;
                  ,this.w_TC__DAY5;
                  ,this.w_TC__DAY6;
                  ,this.w_TC__DAY7;
                  ,this.w_TC__DAY1;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_CALE_IDX,i_nConn)
      *
      * update TAB_CALE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_CALE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TCDESCRI="+cp_ToStrODBC(this.w_TCDESCRI)+;
             ",TCFLDEFA="+cp_ToStrODBC(this.w_TCFLDEFA)+;
             ",TCDA1STD="+cp_ToStrODBC(this.w_TCDA1STD)+;
             ",TCAL1STD="+cp_ToStrODBC(this.w_TCAL1STD)+;
             ",TCDA2STD="+cp_ToStrODBC(this.w_TCDA2STD)+;
             ",TCAL2STD="+cp_ToStrODBC(this.w_TCAL2STD)+;
             ",TCNUMORE="+cp_ToStrODBC(this.w_TCNUMORE)+;
             ",TC__DAY2="+cp_ToStrODBC(this.w_TC__DAY2)+;
             ",TC__DAY3="+cp_ToStrODBC(this.w_TC__DAY3)+;
             ",TC__DAY4="+cp_ToStrODBC(this.w_TC__DAY4)+;
             ",TC__DAY5="+cp_ToStrODBC(this.w_TC__DAY5)+;
             ",TC__DAY6="+cp_ToStrODBC(this.w_TC__DAY6)+;
             ",TC__DAY7="+cp_ToStrODBC(this.w_TC__DAY7)+;
             ",TC__DAY1="+cp_ToStrODBC(this.w_TC__DAY1)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_CALE')
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        UPDATE (i_cTable) SET;
              TCDESCRI=this.w_TCDESCRI;
             ,TCFLDEFA=this.w_TCFLDEFA;
             ,TCDA1STD=this.w_TCDA1STD;
             ,TCAL1STD=this.w_TCAL1STD;
             ,TCDA2STD=this.w_TCDA2STD;
             ,TCAL2STD=this.w_TCAL2STD;
             ,TCNUMORE=this.w_TCNUMORE;
             ,TC__DAY2=this.w_TC__DAY2;
             ,TC__DAY3=this.w_TC__DAY3;
             ,TC__DAY4=this.w_TC__DAY4;
             ,TC__DAY5=this.w_TC__DAY5;
             ,TC__DAY6=this.w_TC__DAY6;
             ,TC__DAY7=this.w_TC__DAY7;
             ,TC__DAY1=this.w_TC__DAY1;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_CALE_IDX,i_nConn)
      *
      * delete TAB_CALE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TCDA1STD<>.w_TCDA1STD
            .w_TCDA1STD = IIF(EMPTY(.w_TCDA1STD), "09:00", formattime(.w_TCDA1STD, ":"))
        endif
        if .o_TCAL1STD<>.w_TCAL1STD.or. .o_TCDA1STD<>.w_TCDA1STD
            .w_TCAL1STD = IIF(EMPTY(.w_TCAL1STD), "13:00", formattime(.w_TCAL1STD, ":"))
        endif
        if .o_TCDA2STD<>.w_TCDA2STD.or. .o_TCDA1STD<>.w_TCDA1STD.or. .o_TCAL1STD<>.w_TCAL1STD
            .w_TCDA2STD = formattime(.w_TCDA2STD, ":")
        endif
        if .o_TCDA1STD<>.w_TCDA1STD.or. .o_TCAL1STD<>.w_TCAL1STD.or. .o_TCDA2STD<>.w_TCDA2STD.or. .o_TCAL2STD<>.w_TCAL2STD
            .w_TCAL2STD = IIF(EMPTY(.w_TCDA2STD),"", formattime(.w_TCAL2STD, ":" ) )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_HGGDJHDCWP()
    with this
          * --- VERIFICA CALENDARI PREDEFINITI
          gsar_btl(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTCAL1STD_1_9.enabled = this.oPgFrm.Page1.oPag.oTCAL1STD_1_9.mCond()
    this.oPgFrm.Page1.oPag.oTCDA2STD_1_11.enabled = this.oPgFrm.Page1.oPag.oTCDA2STD_1_11.mCond()
    this.oPgFrm.Page1.oPag.oTCAL2STD_1_13.enabled = this.oPgFrm.Page1.oPag.oTCAL2STD_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_HGGDJHDCWP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value==this.w_TCCODICE)
      this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value=this.w_TCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_3.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_3.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTCFLDEFA_1_5.RadioValue()==this.w_TCFLDEFA)
      this.oPgFrm.Page1.oPag.oTCFLDEFA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDA1STD_1_7.value==this.w_TCDA1STD)
      this.oPgFrm.Page1.oPag.oTCDA1STD_1_7.value=this.w_TCDA1STD
    endif
    if not(this.oPgFrm.Page1.oPag.oTCAL1STD_1_9.value==this.w_TCAL1STD)
      this.oPgFrm.Page1.oPag.oTCAL1STD_1_9.value=this.w_TCAL1STD
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDA2STD_1_11.value==this.w_TCDA2STD)
      this.oPgFrm.Page1.oPag.oTCDA2STD_1_11.value=this.w_TCDA2STD
    endif
    if not(this.oPgFrm.Page1.oPag.oTCAL2STD_1_13.value==this.w_TCAL2STD)
      this.oPgFrm.Page1.oPag.oTCAL2STD_1_13.value=this.w_TCAL2STD
    endif
    if not(this.oPgFrm.Page1.oPag.oTCNUMORE_1_14.value==this.w_TCNUMORE)
      this.oPgFrm.Page1.oPag.oTCNUMORE_1_14.value=this.w_TCNUMORE
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY2_1_16.RadioValue()==this.w_TC__DAY2)
      this.oPgFrm.Page1.oPag.oTC__DAY2_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY3_1_17.RadioValue()==this.w_TC__DAY3)
      this.oPgFrm.Page1.oPag.oTC__DAY3_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY4_1_18.RadioValue()==this.w_TC__DAY4)
      this.oPgFrm.Page1.oPag.oTC__DAY4_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY5_1_19.RadioValue()==this.w_TC__DAY5)
      this.oPgFrm.Page1.oPag.oTC__DAY5_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY6_1_20.RadioValue()==this.w_TC__DAY6)
      this.oPgFrm.Page1.oPag.oTC__DAY6_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY7_1_21.RadioValue()==this.w_TC__DAY7)
      this.oPgFrm.Page1.oPag.oTC__DAY7_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__DAY1_1_22.RadioValue()==this.w_TC__DAY1)
      this.oPgFrm.Page1.oPag.oTC__DAY1_1_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TAB_CALE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_TCDA1STD)) or not(chktimewnd(1, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCDA1STD_1_7.SetFocus()
            i_bnoObbl = !empty(.w_TCDA1STD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido oppure orario iniziale maggiore dell'orario finale")
          case   ((empty(.w_TCAL1STD)) or not(chktimewnd(2, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD)))  and (!EMPTY(.w_TCDA1STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCAL1STD_1_9.SetFocus()
            i_bnoObbl = !empty(.w_TCAL1STD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido o orario finale minore dell'orario iniziale")
          case   not(chktimewnd(3, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))  and (!EMPTY(.w_TCDA1STD) AND !EMPTY(.w_TCAL1STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCDA2STD_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))  and (!EMPTY(.w_TCDA2STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCAL2STD_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido o orario finale minore dell'orario iniziale")
          case   not(.w_TCNUMORE>=0 AND .w_TCNUMORE<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCNUMORE_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TCDA1STD = this.w_TCDA1STD
    this.o_TCAL1STD = this.w_TCAL1STD
    this.o_TCDA2STD = this.w_TCDA2STD
    this.o_TCAL2STD = this.w_TCAL2STD
    return

enddefine

* --- Define pages as container
define class tgsar_atlPag1 as StdContainer
  Width  = 396
  height = 309
  stdWidth  = 396
  stdheight = 309
  resizeXpos=373
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTCCODICE_1_1 as StdField with uid="VAVPCCOWZI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TCCODICE", cQueryName = "TCCODICE",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice calendario",;
    HelpContextID = 133561989,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=94, Top=12, InputMask=replicate('X',5)

  add object oTCDESCRI_1_3 as StdField with uid="YJZTPJNMAB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione calendari",;
    HelpContextID = 219147905,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=94, Top=36, InputMask=replicate('X',40)

  add object oTCFLDEFA_1_5 as StdCheck with uid="HWGUXIJTQU",rtseq=3,rtrep=.f.,left=97, top=64, caption="Predefinito",;
    ToolTipText = "Se attivo imposta il calendario come calendario aziendale predefinito",;
    HelpContextID = 200855177,;
    cFormVar="w_TCFLDEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTCFLDEFA_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTCFLDEFA_1_5.GetRadio()
    this.Parent.oContained.w_TCFLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oTCFLDEFA_1_5.SetRadio()
    this.Parent.oContained.w_TCFLDEFA=trim(this.Parent.oContained.w_TCFLDEFA)
    this.value = ;
      iif(this.Parent.oContained.w_TCFLDEFA=='S',1,;
      0)
  endfunc

  add object oTCDA1STD_1_7 as StdField with uid="ZKCMQFZCCW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TCDA1STD", cQueryName = "TCDA1STD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido oppure orario iniziale maggiore dell'orario finale",;
    HelpContextID = 255061638,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=156, Top=109, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oTCDA1STD_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))
    endwith
    return bRes
  endfunc

  add object oTCAL1STD_1_9 as StdField with uid="CSJZUOHIGY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TCAL1STD", cQueryName = "TCAL1STD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido o orario finale minore dell'orario iniziale",;
    HelpContextID = 254353030,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=272, Top=110, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oTCAL1STD_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_TCDA1STD))
    endwith
   endif
  endfunc

  func oTCAL1STD_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))
    endwith
    return bRes
  endfunc

  add object oTCDA2STD_1_11 as StdField with uid="TCYZULWKLH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TCDA2STD", cQueryName = "TCDA2STD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    HelpContextID = 254013062,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=156, Top=136, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oTCDA2STD_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_TCDA1STD) AND !EMPTY(.w_TCAL1STD))
    endwith
   endif
  endfunc

  func oTCDA2STD_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))
    endwith
    return bRes
  endfunc

  add object oTCAL2STD_1_13 as StdField with uid="OVMGYCBFGY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TCAL2STD", cQueryName = "TCAL2STD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido o orario finale minore dell'orario iniziale",;
    HelpContextID = 253304454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=272, Top=136, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oTCAL2STD_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_TCDA2STD))
    endwith
   endif
  endfunc

  func oTCAL2STD_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_TCDA1STD, .w_TCAL1STD, .w_TCDA2STD, .w_TCAL2STD))
    endwith
    return bRes
  endfunc

  add object oTCNUMORE_1_14 as StdField with uid="SUZLUIHMWP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TCNUMORE", cQueryName = "TCNUMORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative ( 0 =calcolate in base agli intervalli orari)",;
    HelpContextID = 23023237,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=272, Top=162, cSayPict='"99.99"', cGetPict='"99.99"'

  func oTCNUMORE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TCNUMORE>=0 AND .w_TCNUMORE<=24)
    endwith
    return bRes
  endfunc

  add object oTC__DAY2_1_16 as StdCheck with uid="VYAUECLAEC",rtseq=9,rtrep=.f.,left=12, top=218, caption="Lunedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818984,;
    cFormVar="w_TC__DAY2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY2_1_16.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY2_1_16.GetRadio()
    this.Parent.oContained.w_TC__DAY2 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY2_1_16.SetRadio()
    this.Parent.oContained.w_TC__DAY2=trim(this.Parent.oContained.w_TC__DAY2)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY2=="S",1,;
      0)
  endfunc

  add object oTC__DAY3_1_17 as StdCheck with uid="MZSKHNSHCF",rtseq=10,rtrep=.f.,left=143, top=218, caption="Marted�",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818985,;
    cFormVar="w_TC__DAY3", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY3_1_17.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY3_1_17.GetRadio()
    this.Parent.oContained.w_TC__DAY3 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY3_1_17.SetRadio()
    this.Parent.oContained.w_TC__DAY3=trim(this.Parent.oContained.w_TC__DAY3)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY3=="S",1,;
      0)
  endfunc

  add object oTC__DAY4_1_18 as StdCheck with uid="QNOXUZQCID",rtseq=11,rtrep=.f.,left=266, top=218, caption="Mercoled�",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818986,;
    cFormVar="w_TC__DAY4", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY4_1_18.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY4_1_18.GetRadio()
    this.Parent.oContained.w_TC__DAY4 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY4_1_18.SetRadio()
    this.Parent.oContained.w_TC__DAY4=trim(this.Parent.oContained.w_TC__DAY4)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY4=="S",1,;
      0)
  endfunc

  add object oTC__DAY5_1_19 as StdCheck with uid="TAHFFGHUIV",rtseq=12,rtrep=.f.,left=12, top=248, caption="Gioved�",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818987,;
    cFormVar="w_TC__DAY5", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY5_1_19.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY5_1_19.GetRadio()
    this.Parent.oContained.w_TC__DAY5 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY5_1_19.SetRadio()
    this.Parent.oContained.w_TC__DAY5=trim(this.Parent.oContained.w_TC__DAY5)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY5=="S",1,;
      0)
  endfunc

  add object oTC__DAY6_1_20 as StdCheck with uid="XVSPPVUVFA",rtseq=13,rtrep=.f.,left=143, top=248, caption="Venerd�",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818988,;
    cFormVar="w_TC__DAY6", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY6_1_20.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY6_1_20.GetRadio()
    this.Parent.oContained.w_TC__DAY6 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY6_1_20.SetRadio()
    this.Parent.oContained.w_TC__DAY6=trim(this.Parent.oContained.w_TC__DAY6)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY6=="S",1,;
      0)
  endfunc

  add object oTC__DAY7_1_21 as StdCheck with uid="LKKEKIOEFO",rtseq=14,rtrep=.f.,left=266, top=248, caption="Sabato",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818989,;
    cFormVar="w_TC__DAY7", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY7_1_21.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY7_1_21.GetRadio()
    this.Parent.oContained.w_TC__DAY7 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY7_1_21.SetRadio()
    this.Parent.oContained.w_TC__DAY7=trim(this.Parent.oContained.w_TC__DAY7)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY7=="S",1,;
      0)
  endfunc

  add object oTC__DAY1_1_22 as StdCheck with uid="JCIXXYXAQE",rtseq=15,rtrep=.f.,left=12, top=278, caption="Domenica",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 1818983,;
    cFormVar="w_TC__DAY1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTC__DAY1_1_22.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oTC__DAY1_1_22.GetRadio()
    this.Parent.oContained.w_TC__DAY1 = this.RadioValue()
    return .t.
  endfunc

  func oTC__DAY1_1_22.SetRadio()
    this.Parent.oContained.w_TC__DAY1=trim(this.Parent.oContained.w_TC__DAY1)
    this.value = ;
      iif(this.Parent.oContained.w_TC__DAY1=="S",1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="JVRUAGFUEM",Visible=.t., Left=24, Top=9,;
    Alignment=1, Width=64, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="AJMUJKHQAU",Visible=.t., Left=6, Top=33,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MYCSXMYANJ",Visible=.t., Left=97, Top=113,;
    Alignment=1, Width=55, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QEXNNKTSLI",Visible=.t., Left=210, Top=112,;
    Alignment=1, Width=55, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IIKEGZIQUO",Visible=.t., Left=97, Top=140,;
    Alignment=1, Width=55, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="YIOHNFUNRD",Visible=.t., Left=210, Top=140,;
    Alignment=1, Width=55, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MPDOLDOFYN",Visible=.t., Left=89, Top=88,;
    Alignment=0, Width=244, Height=18,;
    Caption="Orario lavorativo predefinito"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WQNZPMSYDL",Visible=.t., Left=8, Top=195,;
    Alignment=0, Width=188, Height=18,;
    Caption="Giornate lavorative predefinite"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YLVLTIIUER",Visible=.t., Left=182, Top=166,;
    Alignment=1, Width=83, Height=18,;
    Caption="Ore lavorative:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="FAUWEDPQJN",left=91, top=105, width=237,height=85

  add object oBox_1_26 as StdBox with uid="FWUFLSYJPU",left=3, top=215, width=388,height=89
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_atl','TAB_CALE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TCCODICE=TAB_CALE.TCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
