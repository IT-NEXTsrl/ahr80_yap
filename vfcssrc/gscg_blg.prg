* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_blg                                                        *
*              Elabora stampa libro giornale                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_128]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-25                                                      *
* Last revis.: 2012-05-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_blg",oParentObject)
return(i_retval)

define class tgscg_blg as StdBatch
  * --- Local variables
  w_DATIN2 = ctod("  /  /  ")
  w_PROLG1 = 0
  w_DARLG1 = 0
  w_AVELG1 = 0
  w_PROLG2 = 0
  w_DARLG2 = 0
  w_AVELG2 = 0
  w_CAOVAL = 0
  w_DECTOT = 0
  w_APPVAL = space(3)
  w_APPCAM = 0
  w_AGGPRO = .f.
  w_DESVAL = space(35)
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_DATBLO = ctod("  /  /  ")
  w_CONFER = space(1)
  w_PROLIB = 0
  w_DATULT = ctod("  /  /  ")
  w_OLDINI = ctod("  /  /  ")
  w_OLDFIN = ctod("  /  /  ")
  w_OLDPRO = space(1)
  w_APPO = space(10)
  w_PAGINE = 0
  w_OLDSERIAL = space(10)
  w_FLSALDA = space(1)
  w_OGGETTO = .NULL.
  w_CODESES = space(5)
  w_CAPFOR = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  ESERCIZI_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  VALUTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Stampa Libro Giornale (da GSCG_SLG)
    * --- Variabile  w_OLDSERIAL per calcolo progressivo con Contabilit� per Cassa
    * --- Se ristampa Cambia i Filtri di Selezione
    this.w_OLDINI = this.oParentObject.w_DATINI
    this.w_OLDFIN = this.oParentObject.w_DATFIN
    this.w_OLDPRO = this.oParentObject.w_CALPRO
    if this.oParentObject.w_TIPSTA="R"
      this.oParentObject.w_DATINI = IIF(EMPTY(this.oParentObject.w_DATINR), this.oParentObject.w_INIESE-1, this.oParentObject.w_DATINR - 1)
      this.oParentObject.w_DATFIN = this.oParentObject.w_DATFIR
      this.oParentObject.w_CALPRO = "S"
    endif
    * --- Data iniziale di Ricerca Registrazioni da Stampare
    this.w_DATIN2 = IIF(EMPTY(this.oParentObject.w_DATINI), this.oParentObject.w_INIESE, this.oParentObject.w_DATINI + 1)
    * --- Se .T. Aggiorna i Progressivi L.G.
    this.w_AGGPRO = IIF(this.oParentObject.w_TIPSTA="D", .T., .F.)
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    if this.w_DATIN2>this.oParentObject.w_DATFIN
      ah_ErrorMsg("Data iniziale maggiore di quella finale",,"")
      i_retcode = 'stop'
      return
    endif
    if (this.w_DATIN2<this.oParentObject.w_INIESE OR this.oParentObject.w_DATFIN>this.oParentObject.w_FINESE)
      ah_ErrorMsg("L'intervallo delle date non � contenuto all'interno dell'esercizio attuale.%0Selezionare l'esercizio di competenza corretto per effettuare la stampa con le selezioni impostate",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_DATINI<>this.oParentObject.w_STALIG AND this.oParentObject.w_CALPRO<>"S" AND NOT (EMPTY(this.oParentObject.w_STALIG) AND this.w_DATIN2=this.oParentObject.w_INIESE) AND NOT (this.oParentObject.w_CONCAS="S" AND this.oParentObject.w_TIPSTA="P" AND ISALT())
      ah_ErrorMsg("Data di inizio elaborazione non consecutiva all'ultima stampa.%0Per forzare la data iniziale � necessario eseguire anche il ricalcolo dei progressivi.",,"")
      i_retcode = 'stop'
      return
    endif
    if YEAR(this.w_DATIN2)<>YEAR(this.oParentObject.w_DATFIN)
      ah_ErrorMsg("La data di fine selezione � al di fuori dell'anno solare della data di inizio selezione.%0Come fine selezione impostare una data all'interno dello stesso anno solare.",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Verifica presenza di registrazioni Provvisorie
    vq_exec("query\gscg2qlg.vqr",this,"__TMP__")
    if NVL(__TMP__.conta,0)>0
      if NOT ah_YesNo("Esistono per il periodo selezionato delle registrazioni provvisorie che verranno ignorate.%0Proseguire con l'elaborazione?")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- LETTURA VARIABILI NECESSARIE ANCHE PER LA NUMERAZIONE PAGINE
    if this.oParentObject.w_TIPSTA="D" OR this.oParentObject.w_INTLIG="S"
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZDATBLO"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZDATBLO;
          from (i_cTable) where;
              AZCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
        this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
        this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
        this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.oParentObject.w_TIPSTA="D"
      * --- tenta blocco prima nota
      * --- Try
      local bErr_03CED240
      bErr_03CED240=bTrsErr
      this.Try_03CED240()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if this.oParentObject.w_CONCAS=" "
          ah_ErrorMsg("Non � possibile stampare il giornale su bollati in questo momento",,"")
        else
          ah_ErrorMsg("Non � possibile stampare il registro cronologico su bollati in questo momento",,"")
        endif
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_03CED240
      * --- End
    endif
    * --- Il libro Giornale viene stampato nella Moneta di Conto dell'esercizio scelto
    * --- Stampa nel formato dell'esercizio scelto, traduce gli importi - Aggiunta una voce di differenza cambi
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADESVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADESVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESVAL = NVL(cp_ToDate(_read_.VADESVAL),cp_NullValue(_read_.VADESVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAOVAL = GETCAM(g_PERVAL,I_DATSYS)
    * --- Progressivi Iniziali di Stampa
    this.w_PROLG1 = this.oParentObject.w_PROLIG
    this.w_DARLG1 = this.oParentObject.w_DARLIG
    this.w_AVELG1 = this.oParentObject.w_AVELIG
    if this.oParentObject.w_CALPRO="S"
      * --- Ricalcola i Progressivi Libro Giornale fino alla data di Inizio Selezione
      this.w_PROLG1 = 0
      this.w_DARLG1 = 0
      this.w_AVELG1 = 0
      vq_exec("query\gscg1qlg.vqr",this,"__TMP__")
      if USED("__TMP__")
        SELECT __TMP__
        GO TOP
        SCAN FOR NOT EMPTY(NVL(VALNAZ,"")) AND NVL(TOTREC,0)>0
        this.w_PROLG1 = this.w_PROLG1 + TOTREC
        if VALNAZ=this.oParentObject.w_VALUTA
          this.w_DARLG1 = this.w_DARLG1 + NVL(IMPDAR,0)
          this.w_AVELG1 = this.w_AVELG1 + NVL(IMPAVE,0)
        else
          * --- S altra Valuta Converte nella Moneta di Conto dell'esercizio
          this.w_APPVAL = VALNAZ
          this.w_APPCAM = 0
          this.w_APPCAM = GETCAM(this.w_APPVAL, i_DATSYS)
          SELECT __TMP__
          if this.w_APPCAM<>0
            this.w_DARLG1 = this.w_DARLG1 + VAL2MON(NVL(IMPDAR,0),this.w_APPCAM, 1,i_DATSYS,g_PERVAL,this.w_DECTOT)
            this.w_AVELG1 = this.w_AVELG1 + VAL2MON(NVL(IMPAVE,0),this.w_APPCAM, 1,i_DATSYS,g_PERVAL,this.w_DECTOT)
          endif
        endif
        ENDSCAN
        if this.oParentObject.w_CONCAS="S"
          * --- Progressivo con Contabilit� per Cassa:
          *     invece che contare le righe di primanota vengono contate le "testate" di primanota
          vq_exec("query\gscg5qlg.vqr",this,"TESPNOTA")
          if USED("TESPNOTA")
            SELECT TESPNOTA
            this.w_PROLG1 = TOTREC
            USE
          endif
        endif
      endif
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    * --- Lancia la Query di Elaborazione
    this.w_PROLG2 = 0
    this.w_DARLG2 = 0
    this.w_AVELG2 = 0
    this.w_OLDSERIAL = "AAAAAAAAAA"
    * --- ordinate per PNDATREG + PNSERIAL + CPROWORD 
    if this.oParentObject.w_CONCAS=" "
      vq_exec("query\gscg_qlg.vqr",this,"__TMP__")
    else
      * --- Contabilit� per cassa
      this.w_OGGETTO = This
      this.w_FLSALDA = "S"
      * --- Filtro anche i saldi per esercizio di competenza
      this.w_CODESES = this.oParentObject.w_CODESE
      vq_exec("gscg3qlg.vqr",this,"__TMP__")
      * --- Gestione resto riche contropartite di cassa
      Wrcursor("__TMP__")
       
 Select Max(RowFat) as RowFat,Pnserial,Cprownum,Serfat,Max(Saldato)-Sum(Saldatoa) as Diffa,0 as Diffd from __TMP__ ; 
 Where Saldatoa<>0 and tiprec="B" Group by Pnserial,Cprownum,Serfat Having Diffa <>0 ; 
 Union all; 
 Select Max(RowFat) as RowFat,Pnserial,Cprownum,Serfat,0 as Diffa,Max(Saldato)-Sum(Saldatod) as Diffd from __TMP__ ; 
 Where Saldatod<>0 and tiprec="B" Group by Pnserial,Cprownum,Serfat Having Diffd <>0; 
 Into cursor Temp
       
 Update __TMP__ set saldatoa=saldatoa+diffa,saldatod=saldatod+diffd from __TMP__; 
 Inne Join Temp ON (__Tmp__.Pnserial=Temp.Pnserial and __Tmp__.Cprownum=Temp.Cprownum and __Tmp__.Serfat=Temp.Serfat and __Tmp__.Rowfat=Temp.Rowfat )
      Use in Temp
    endif
    if USED("__TMP__")
      * --- Verifica Registrazioni "SQUADRATE"
      SELECT PNSERIAL, SUM(NVL(PNIMPDAR,0)-NVL(PNIMPAVE,0)) AS IMPQUA FROM __TMP__ where TIPREC="A" ;
      INTO CURSOR APPOQUAD GROUP BY 1 HAVING IMPQUA<>0
      if USED("APPOQUAD")
        SELECT APPOQUAD
        if RECCOUNT()>0
          GO TOP
          SCAN FOR NOT EMPTY(NVL(PNSERIAL,"")) AND IMPQUA<>0
          this.w_APPO = PNSERIAL
          SELECT __TMP__
          GO TOP
          UPDATE __TMP__ SET SQUAD="S" WHERE NVL(PNSERIAL,"  ")=this.w_APPO
          SELECT APPOQUAD
          ENDSCAN 
        endif
        SELECT APPOQUAD
        USE
      endif
      SELECT __TMP__
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PNVALNAZ,"")) 
      if this.oParentObject.w_CONCAS=" "
        this.w_PROLG2 = this.w_PROLG2 + 1
      else
        * --- Progressivo con Contabilit� per Cassa:
        *     invece che contare le righe di primanota vengono contate le "testate" di primanota
        if this.w_OLDSERIAL<>PNSERIAL
          this.w_PROLG2 = this.w_PROLG2 + 1
          this.w_OLDSERIAL = PNSERIAL
        endif
      endif
      if PNVALNAZ=this.oParentObject.w_VALUTA
        this.w_DARLG2 = this.w_DARLG2 + NVL(PNIMPDAR,0)
        this.w_AVELG2 = this.w_AVELG2 + NVL(PNIMPAVE,0)
      else
        * --- Se altra Valuta Converte nella Moneta di Conto dell'esercizio
        this.w_APPVAL = PNVALNAZ
        this.w_APPCAM = 0
        if this.w_APPVAL<>PNVALNAZ
          this.w_APPCAM = GETCAM(PNVALNAZ,i_DATSYS)
        endif
        if this.w_APPCAM<>0
          this.w_DARLG2 = this.w_DARLG2 + VAL2MON(NVL(PNIMPDAR,0),this.w_APPCAM, 1,i_DATSYS,g_PERVAL,this.w_DECTOT)
          this.w_AVELG2 = this.w_AVELG2 + VAL2MON(NVL(PNIMPAVE,0) ,this.w_APPCAM, 1,i_DATSYS,g_PERVAL,this.w_DECTOT)
        endif
      endif
      ENDSCAN
    endif
    * --- Per numerazione pagine
     
 PUBLIC STPAG 
 STPAG = 0 
 L_PAGINE=0
    * --- Se .t. inizia la Stampa
    if NOT USED("__TMP__") OR this.w_PROLG2=0
      if this.oParentObject.w_CALPRO="S" AND this.w_AGGPRO=.T.
        if this.oParentObject.w_CONCAS=" "
          this.w_AGGPRO = ah_YesNo("Per l'intervallo selezionato non ci sono dati da stampare.%0Aggiorno comunque i progressivi libro giornale?")
        else
          this.w_AGGPRO = ah_YesNo("Per l'intervallo selezionato non ci sono dati da stampare.%0Aggiorno comunque i progressivi registro cronologico?")
        endif
      else
        ah_ErrorMsg("Per l'intervallo selezionato non ci sono dati da stampare",,"")
        this.w_AGGPRO = .F.
      endif
    else
      * --- Variabili da passare al report
      L_TIPSTA=this.oParentObject.w_TIPSTA
      L_INDAZI=this.w_INDAZI
      L_LOCAZI=this.w_LOCAZI
      L_CAPAZI=this.w_CAPAZI
      L_PROAZI=this.w_PROAZI
      L_COFAZI=this.w_COFAZI
      L_PIVAZI=this.w_PIVAZI
      L_PROLG1=this.w_PROLG1
      L_DARLG1=this.w_DARLG1
      L_AVELG1=this.w_AVELG1
      L_VALUTA=this.oParentObject.w_VALUTA
      L_DESVAL=this.w_DESVAL
      L_CAOVAL=this.w_CAOVAL
      L_DECTOT=this.w_DECTOT
      L_FORMAT=v_PV[20*( this.w_DECTOT+2 )]
      L_DATIN2=this.w_DATIN2
      L_TOTDAY=this.oParentObject.w_TOTDAY
      L_SALPRO=this.oParentObject.w_SALPRO
      * --- PER NUMERAZIONE PAGINE IN TESTATA
      L_PAGINE=0
      L_PRPALG=this.oParentObject.w_PRPALG
      L_CODESE=this.oParentObject.w_CODESE
      L_INTLIG=this.oParentObject.w_INTLIG
      L_PREFIS=this.oParentObject.w_PREFIS
      * --- Contabilit� per cassa e stampa totali cronologico
      L_FLTCRO=this.oParentObject.w_FLTCRO
      if this.oParentObject.w_FLTEST<>"S"
        * --- Inserisco riga vuota con PNSERIAL = ZZZZZZZZZZ per problema salto pagina report grafico.
        if Not Isalt()
          SELECT __TMP__
          APPEND BLANK
          REPLACE PNSERIAL WITH "ZZZZZZZZZZ"
        endif
        if this.oParentObject.w_CONCAS="S" AND this.oParentObject.w_FLTCRO="S"
          SELECT * FROM __TMP__ INTO CURSOR PRINOT
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          * --- Contabilit� per cassa e stampa totali cronologico
          vq_exec("query\gscg4qlg.vqr",this,"TOTALI")
          SELECT * FROM PRINOT UNION ALL SELECT * FROM TOTALI INTO CURSOR __TMP__
          if USED("PRINOT")
            SELECT PRINOT
            USE
          endif
          if USED("TOTALI")
            SELECT TOTALI
            USE
          endif
        endif
      endif
      SELECT __TMP__
      GO TOP
      * --- lancio il report a seconda se solo testo o grafica
      if this.oParentObject.w_FLTEST="S"
        STPAG = this.oParentObject.w_PRPALG
        * --- Selezione orientamento
        if this.oParentObject.w_STAREG="V"
          CP_CHPRN("LIBGIO_V.FXP", "S", this)
        else
          CP_CHPRN("LIBGIO_O.FXP", "S", this)
        endif
      else
        STPAG = 0
        if this.oParentObject.w_CONCAS=" "
          CP_CHPRN("query\gscg_qlg.frx", " ", this)
        else
          CP_CHPRN("query\gscg3qlg.frx", " ", this)
        endif
      endif
    endif
    * --- Aggiorna Progressivi Libro Giornale
    if this.w_AGGPRO=.T.
      this.w_CONFER = "N"
      this.w_PROLIB = this.w_PROLG1+this.w_PROLG2
      this.w_PAGINE = IIF(this.oParentObject.w_FLTEST="S",STPAG,STPAG+this.oParentObject.w_PRPALG)
      this.w_DATULT = this.oParentObject.w_DATFIN
      * --- Variabile per cambia caption form
      this.w_CAPFOR = this.oParentObject.w_CONCAS
      do gscg_s2l with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONFER="S"
        * --- Riassegna Variabili per la Maschera
        this.oParentObject.w_STALIG = this.oParentObject.w_DATFIN
        this.oParentObject.w_PRPALG = IIF(this.oParentObject.w_FLTEST="S",STPAG,STPAG+this.oParentObject.w_PRPALG)
        this.oParentObject.w_PROLIG = this.w_PROLG1+this.w_PROLG2
        this.oParentObject.w_DARLIG = this.w_DARLG1+this.w_DARLG2
        this.oParentObject.w_AVELIG = this.w_AVELG1+this.w_AVELG2
        this.oParentObject.w_DATINI = this.oParentObject.w_STALIG
        this.oParentObject.w_DATFIN = MESESKIP(IIF(EMPTY(this.oParentObject.w_DATINI), this.oParentObject.w_INIESE, this.oParentObject.w_DATINI+1),0,"F")
        * --- Ultima Stampa
        * --- Try
        local bErr_03B16168
        bErr_03B16168=bTrsErr
        this.Try_03B16168()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile aggiornare ultima data di stampa; verificare tabella dati azienda",,"")
        endif
        bTrsErr=bTrsErr or bErr_03B16168
        * --- End
        * --- Progressivo N.Pag. solo se attivo check intestazione Libro Giornale in Azienda
        if this.oParentObject.w_INTLIG="S"
          * --- Try
          local bErr_0402B220
          bErr_0402B220=bTrsErr
          this.Try_0402B220()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            ah_ErrorMsg("Impossibile aggiornare progressivo pagina; verificare tabella dati azienda",,"")
          endif
          bTrsErr=bTrsErr or bErr_0402B220
          * --- End
        endif
        * --- Progressivi N.Riga, Dare/Avere Totali
        * --- Try
        local bErr_03BA7608
        bErr_03BA7608=bTrsErr
        this.Try_03BA7608()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_CONCAS=" "
            ah_ErrorMsg("Impossibile aggiornare i progressivi libro giornale; verificare tabella esercizi",,"")
          else
            ah_ErrorMsg("Impossibile aggiornare i progressivi registro cronologico; verificare tabella esercizi",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_03BA7608
        * --- End
      endif
    endif
    if this.oParentObject.w_TIPSTA="D"
      * --- Toglie <Blocco> per Primanota
      * --- Try
      local bErr_03D4DF00
      bErr_03D4DF00=bTrsErr
      this.Try_03D4DF00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile rimuovere blocco della prima nota; verificare tabella dati azienda",,"")
      endif
      bTrsErr=bTrsErr or bErr_03D4DF00
      * --- End
    endif
    if this.oParentObject.w_TIPSTA="R"
      * --- Se ristampa Ripristina i Filtri di Selezione
      this.oParentObject.w_DATINI = this.w_OLDINI
      this.oParentObject.w_DATFIN = this.w_OLDFIN
      this.oParentObject.w_CALPRO = this.w_OLDPRO
    endif
    this.w_OGGETTO = Null
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc
  proc Try_03CED240()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if EMPTY(CP_TODATE(this.w_DATBLO))
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.oParentObject.w_DATFIN;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.oParentObject.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN ALTRO STA STAMPANDO IL GIORNALE - controllo concorrenza
      if this.oParentObject.w_CONCAS=" "
        ah_ErrorMsg("Stampa giornale gi� in corso da parte di un altro utente",,"")
      else
        ah_ErrorMsg("Stampa registro cronologico gi� in corso da parte di un altro utente",,"")
      endif
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03B16168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZSTALIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STALIG),'AZIENDA','AZSTALIG');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZSTALIG = this.oParentObject.w_STALIG;
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
     
 g_STALIG=this.oParentObject.w_STALIG
    return
  proc Try_0402B220()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPRPALG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPALG),'AZIENDA','AZPRPALG');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZPRPALG = this.oParentObject.w_PRPALG;
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03BA7608()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ESERCIZI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ESERCIZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESAVELIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AVELIG),'ESERCIZI','ESAVELIG');
      +",ESPROLIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROLIG),'ESERCIZI','ESPROLIG');
      +",ESDARLIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DARLIG),'ESERCIZI','ESDARLIG');
          +i_ccchkf ;
      +" where ";
          +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
          +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             )
    else
      update (i_cTable) set;
          ESAVELIG = this.oParentObject.w_AVELIG;
          ,ESPROLIG = this.oParentObject.w_PROLIG;
          ,ESDARLIG = this.oParentObject.w_DARLIG;
          &i_ccchkf. ;
       where;
          ESCODAZI = this.oParentObject.w_CODAZI;
          and ESCODESE = this.oParentObject.w_CODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03D4DF00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='PNT_MAST'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
