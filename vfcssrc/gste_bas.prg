* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bas                                                        *
*              Elab. analisi scostamento                                       *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-22                                                      *
* Last revis.: 2016-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bas",oParentObject,m.pPar)
return(i_retval)

define class tgste_bas as StdBatch
  * --- Local variables
  pPar = .f.
  w_DATAELA = ctod("  /  /  ")
  w_PTSERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_PTDATRAG = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCAU = space(5)
  w_VALFIA = space(1)
  w_PERVAA = space(10)
  w_FLCONT = space(1)
  w_GIOVAA = 0
  w_NUMCOA = space(15)
  w_NUMCOC = space(15)
  w_CODCON = space(15)
  w_EXT = space(4)
  w_PAGRB = space(1)
  w_PAGBO = space(1)
  w_PAGRD = space(1)
  w_PAGRI = space(1)
  w_PAGMA = space(1)
  w_PAGCA = space(1)
  w_PAGRA = space(1)
  w_PAGSCA = space(1)
  w_SCAINI = ctod("  /  /  ")
  w_SCAFIN = ctod("  /  /  ")
  w_SCACLI = space(0)
  w_SCAFOR = space(0)
  w_SCACON = space(0)
  W_consel1 = space(15)
  W_consel2 = space(15)
  w_AGENTE = space(15)
  w_FSALD = space(1)
  w_FILTDATA = space(1)
  w_DDOFIN = space(0)
  w_VALUTA = space(3)
  * --- WorkFile variables
  ZOOMPART_idx=0
  PAR_TITE_idx=0
  ZOOMPART1_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Inizializza variabile w_ESPOS dei dati azienda
    if this.pPar
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZFLSCDI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZFLSCDI;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_ESPOS = NVL(cp_ToDate(_read_.AZFLSCDI),cp_NullValue(_read_.AZFLSCDI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i_retcode = 'stop'
      return
    endif
    ah_msg( "Elaborazione partite" )
    this.w_DATAELA = this.oparentobject.w_DATAELA
    * --- Elaboro i dati in base ai filtri impostati sulla maschera.
    *     Se tipologia Agente (Tipost='A' lancio la query GSTE8ASAS mentre in tutti
    *     gli altri casi lancio la query GSTE8SAS.
    *     La prima query prende in considerazione tutte le partite che risultano
    *     non ancora saldate ma che hanno data scadenza minore della data di elaborazione.
    *     La query GSTE10SAS prende invece tutte le partite saldate.
    this.w_PAGRB = this.oparentobject.w_PAGRB
    this.w_PAGBO = this.oparentobject.w_PAGBO
    this.w_PAGRD = this.oparentobject.w_PAGRD
    this.w_PAGRI = this.oparentobject.w_PAGRI
    this.w_PAGMA = this.oparentobject.w_PAGMA
    this.w_PAGCA = this.oparentobject.w_PAGCA
    this.w_PAGRA = this.oparentobject.w_PAGRA
    this.w_PAGSCA = this.oparentobject.w_PAGSCA
    this.w_SCAINI = this.oparentobject.w_SCAINI
    this.w_SCAFIN = this.oparentobject.w_SCAFIN
    this.w_SCACLI = "X"
    this.w_SCAFOR = "X"
    this.w_SCACON = "X"
    if this.oParentObject.w_TIPOST="C"
      this.w_SCACON = "C"
    endif
    if this.oParentObject.w_TIPOST="F"
      this.w_SCACON = "F"
    endif
    if this.oParentObject.w_TIPOST<>"F" AND this.oParentObject.w_TIPOST<>"C"
      this.w_SCACON = "G"
      this.w_SCAFOR = "F"
      this.w_SCACLI = "C"
    endif
    this.w_FSALD = "T"
    this.w_FILTDATA = "S"
    this.w_DDOFIN = CTOD("  -  -  ")
    if this.oParentObject.w_TIPOST="F" OR this.oParentObject.w_TIPOST="C"
      this.W_consel1 = this.oParentObject.w_CODINI
      this.W_consel2 = this.oParentObject.w_CODFIN
    else
      this.W_consel1 = ""
      this.W_consel2 = ""
    endif
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Create temporary table ZOOMPART
      i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.PAR_TITE_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
            +" where 1=2";
            )
      this.ZOOMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      GSTE_BVS (this,this)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_VALUTA = this.oparentobject.w_VALUTA 
      * --- Elimino le partite che hanno generato un insoluto.
      if this.oParentObject.w_STAMPA="S"
        * --- Delete from ZOOMPART
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".PTORIGSA = "+i_cQueryTable+".PTORIGSA";
        
          do vq_exec with 'GSTE6SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from ZOOMPART
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".COSERIAL = "+i_cQueryTable+".COSERIAL";
        
          do vq_exec with 'GSTE6SAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from ZOOMPART
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".PTORIGSA = "+i_cQueryTable+".PTORIGSA";
        
          do vq_exec with 'GSTE6ASAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Cancella partite riaperte con distinta di richiamo forzato 
        * --- Write into ZOOMPART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLORIG ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'ZOOMPART','FLORIG');
              +i_ccchkf ;
                 )
        else
          update (i_cTable) set;
              FLORIG = SPACE(1);
              &i_ccchkf. ;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Marco le partite riaperte da distinta di richiamo forzato
        * --- Write into ZOOMPART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZOOMPART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG"
          do vq_exec with 'GSTERSAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ZOOMPART.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FLORIG = _t2.FLORIG";
              +i_ccchkf;
              +" from "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ZOOMPART.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART, "+i_cQueryTable+" _t2 set ";
              +"ZOOMPART.FLORIG = _t2.FLORIG";
              +Iif(Empty(i_ccchkf),"",",ZOOMPART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ZOOMPART.NUMPAR = t2.NUMPAR";
                  +" and "+"ZOOMPART.DATSCA = t2.DATSCA";
                  +" and "+"ZOOMPART.TIPCON = t2.TIPCON";
                  +" and "+"ZOOMPART.CODCON = t2.CODCON";
                  +" and "+"ZOOMPART.CODVAL = t2.CODVAL";
                  +" and "+"ZOOMPART.MODPAG = t2.MODPAG";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set (";
              +"FLORIG";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.FLORIG";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ZOOMPART.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART set ";
              +"FLORIG = _t2.FLORIG";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
                  +" and "+i_cTable+".MODPAG = "+i_cQueryTable+".MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FLORIG = (select FLORIG from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Cancello partite che hanno la stessa chiave logica delle partite finite in distinta di richiamo forzata
        if g_REBA="S"
          * --- Delete from ZOOMPART
          i_nConn=i_TableProp[this.ZOOMPART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
                  +" and "+i_cTable+".MODPAG = "+i_cQueryTable+".MODPAG";
          
            do vq_exec with 'GSTESSAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    else
      this.w_VALUTA = this.oparentobject.w_VALUTA 
    endif
    if this.oParentObject.w_Tipost="A"
      if this.oParentObject.w_STAMPA="S"
        * --- Create temporary table ZOOMPART1
        i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_LCJTFIQNXL[2]
        indexes_LCJTFIQNXL[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
        indexes_LCJTFIQNXL[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
        vq_exec('GSTE8ASAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_LCJTFIQNXL,.f.)
        this.ZOOMPART1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table ZOOMPART1
        i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_KOUUMJHMQI[2]
        indexes_KOUUMJHMQI[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
        indexes_KOUUMJHMQI[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
        vq_exec('GSTE8ASAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_KOUUMJHMQI,.f.)
        this.ZOOMPART1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Riporto il codice Agente sulla partita di chiusura.L'agente viene letto sulla
      *     partita di apertura
      if g_APPLICATION="ad hoc ENTERPRISE"
        * --- Write into ZOOMPART1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG"
          do vq_exec with 'gste3sas',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART1_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART1.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODAGE = _t2.CODAGE";
              +i_ccchkf;
              +" from "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART1.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 set ";
              +"ZOOMPART1.CODAGE = _t2.CODAGE";
              +Iif(Empty(i_ccchkf),"",",ZOOMPART1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ZOOMPART1.NUMPAR = t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = t2.CODVAL";
                  +" and "+"ZOOMPART1.MODPAG = t2.MODPAG";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set (";
              +"CODAGE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.CODAGE";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                  +" and "+"ZOOMPART1.MODPAG = _t2.MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set ";
              +"CODAGE = _t2.CODAGE";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
                  +" and "+i_cTable+".MODPAG = "+i_cQueryTable+".MODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODAGE = (select CODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino i record in base ai filtri sull'agente specificati nella maschera di stampa.
        * --- Delete from ZOOMPART1
        i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
                +" and "+i_cTable+".MODPAG = "+i_cQueryTable+".MODPAG";
        
          do vq_exec with 'GSTE4SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Write into ZOOMPART1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="NUMPAR,DATSCA,TIPCON,CODCON,CODVAL"
          do vq_exec with 'gste3sas',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART1_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODAGE = _t2.CODAGE";
              +i_ccchkf;
              +" from "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 set ";
              +"ZOOMPART1.CODAGE = _t2.CODAGE";
              +Iif(Empty(i_ccchkf),"",",ZOOMPART1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ZOOMPART1.NUMPAR = t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = t2.CODVAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set (";
              +"CODAGE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.CODAGE";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ZOOMPART1.NUMPAR = _t2.NUMPAR";
                  +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                  +" and "+"ZOOMPART1.TIPCON = _t2.TIPCON";
                  +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                  +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set ";
              +"CODAGE = _t2.CODAGE";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                  +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODAGE = (select CODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino i record in base ai filtri sull'agente specificati nella maschera di stampa.
        * --- Delete from ZOOMPART1
        i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
        
          do vq_exec with 'GSTE4SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if NOT EMPTY (this.oParentObject.w_CODINIAG) OR NOT EMPTY (this.oParentObject.w_CODFINAG)
        * --- Delete from ZOOMPART1
        i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CODAGE = "+cp_ToStrODBC("Agente non presente");
                 )
        else
          delete from (i_cTable) where;
                CODAGE = "Agente non presente";

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    else
      if this.oParentObject.w_STAMPA="S"
        * --- Create temporary table ZOOMPART1
        i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_NIZURERPHN[2]
        indexes_NIZURERPHN[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
        indexes_NIZURERPHN[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
        vq_exec('GSTE8SAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_NIZURERPHN,.f.)
        this.ZOOMPART1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        if g_APPLICATION<>"ad hoc ENTERPRISE"
          if g_SOLL<> "S"
            * --- Create temporary table ZOOMPART1
            i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            declare indexes_ZJQEJXTSMT[2]
            indexes_ZJQEJXTSMT[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
            indexes_ZJQEJXTSMT[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
            vq_exec('GSTE8SASI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ZJQEJXTSMT,.f.)
            this.ZOOMPART1_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            * --- Create temporary table ZOOMPART1
            i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            declare indexes_AUCGHEWEJS[2]
            indexes_AUCGHEWEJS[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
            indexes_AUCGHEWEJS[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
            vq_exec('GSTE8SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_AUCGHEWEJS,.f.)
            this.ZOOMPART1_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
        else
          * --- Create temporary table ZOOMPART1
          i_nIdx=cp_AddTableDef('ZOOMPART1') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_BLXEVKIZQU[2]
          indexes_BLXEVKIZQU[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG'
          indexes_BLXEVKIZQU[2]='PTSERIAL,PTROWORD,PTORIGSA,CPROWNUM'
          vq_exec('GSTE8SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_BLXEVKIZQU,.f.)
          this.ZOOMPART1_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      endif
    endif
    * --- Elimino le partite che hanno generato un insoluto.
    if g_APPLICATION<>"ad hoc ENTERPRISE"
      * --- Delete from ZOOMPART1
      i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with 'GSTE6SAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Elimino saldi orfani
      * --- Delete from ZOOMPART1
      i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with 'GSTE6SASD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Aggiorna la data di chiusura delle partite in distinte contabilizzate che erano state inserite anche in contabilizzazione indiretta effetti
      * --- Write into ZOOMPART1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ZOOMPART1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART1_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="TIPCON,CODCON,NUMPAR,DATSCA,CODVAL,FLCRSA,PNRIFDIS"
        do vq_exec with 'GSTE1BAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART1_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ZOOMPART1.TIPCON = _t2.TIPCON";
                +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                +" and "+"ZOOMPART1.NUMPAR = _t2.NUMPAR";
                +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                +" and "+"ZOOMPART1.FLCRSA = _t2.FLCRSA";
                +" and "+"ZOOMPART1.PNRIFDIS = _t2.PNRIFDIS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTDATDOC = _t2.PTDATDOC ";
            +i_ccchkf;
            +" from "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ZOOMPART1.TIPCON = _t2.TIPCON";
                +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                +" and "+"ZOOMPART1.NUMPAR = _t2.NUMPAR";
                +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                +" and "+"ZOOMPART1.FLCRSA = _t2.FLCRSA";
                +" and "+"ZOOMPART1.PNRIFDIS = _t2.PNRIFDIS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1, "+i_cQueryTable+" _t2 set ";
            +"ZOOMPART1.PTDATDOC = _t2.PTDATDOC ";
            +Iif(Empty(i_ccchkf),"",",ZOOMPART1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ZOOMPART1.TIPCON = t2.TIPCON";
                +" and "+"ZOOMPART1.CODCON = t2.CODCON";
                +" and "+"ZOOMPART1.NUMPAR = t2.NUMPAR";
                +" and "+"ZOOMPART1.DATSCA = t2.DATSCA";
                +" and "+"ZOOMPART1.CODVAL = t2.CODVAL";
                +" and "+"ZOOMPART1.FLCRSA = t2.FLCRSA";
                +" and "+"ZOOMPART1.PNRIFDIS = t2.PNRIFDIS";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set (";
            +"PTDATDOC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.PTDATDOC ";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ZOOMPART1.TIPCON = _t2.TIPCON";
                +" and "+"ZOOMPART1.CODCON = _t2.CODCON";
                +" and "+"ZOOMPART1.NUMPAR = _t2.NUMPAR";
                +" and "+"ZOOMPART1.DATSCA = _t2.DATSCA";
                +" and "+"ZOOMPART1.CODVAL = _t2.CODVAL";
                +" and "+"ZOOMPART1.FLCRSA = _t2.FLCRSA";
                +" and "+"ZOOMPART1.PNRIFDIS = _t2.PNRIFDIS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ZOOMPART1 set ";
            +"PTDATDOC = _t2.PTDATDOC ";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
                +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
                +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
                +" and "+i_cTable+".FLCRSA = "+i_cQueryTable+".FLCRSA";
                +" and "+i_cTable+".PNRIFDIS = "+i_cQueryTable+".PNRIFDIS";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTDATDOC = (select PTDATDOC  from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Passo il risultato del cursore lato server ad un cursore intermedio per effettuare
    *     i vari calcoli.
    vq_exec("query\GSTE7SAS.VQR",this,"PARTCHI1")
    if EMPTY (this.oParentObject.w_DETTRIG) 
      select PARTCHI1 
 Go TOp 
 delete FROM PARTCHI1 where NOT EMPTY (PTRAGG ) AND NVL (PTFLRAGG," ") <>"S" 
 select PARTCHI1 
 Go TOp
    endif
    * --- LE partite aperte vengono raggruppate e visualizzate per il saldo, mentre le partite di saldo vengono riportate in stampa per il loro valore originale
    * --- Estrae e raggruppa le partite ancora aperte
    select TIPCON, CODCON, MAX (FLCRSA) AS FLCRSA , DATSCA, NUMPAR, CODVAL, MAX( PTDATDOC ) AS PTDATDOC , MODPAG ,; 
 abs (SALDO) AS TOTIMP , MAX ( SALDO ) AS SALDO , MAX(ANCATCOM) AS ANCATCOM, MAX (FLDAVE) AS FLDAVE, MAX(TIPO) AS TIPO,; 
 MAX(DESCON) AS DESCON , MAX(TOTALECREA) AS TOTALECREA , MAX(PTDATRAG) AS PTDATRAG, MAX(ANCODZON) AS ANCODZON , MAX(PTCAOVAL) AS PTCAOVAL, MAX(FLCRSACREA) as FLCRSACREA,; 
 MAX(CODAGE) AS CODAGE, MAX(FLSEGNO) AS FLSEGNO , MAX(VASIMVAL) AS VASIMVAL,MAX(PTCAOAPE) AS PTCAOAPE , Min(CP_TODATE(PTDATORIG)) as PTDATORIG , MAX( PTRAGG) AS PTRAGG , MAX( PTFLRAGG) AS PTFLRAGG , MAX(COSERIAL) as COSERIAL, max (NVL (CCTIPDOC,"NC")) AS CCTIPDOC , min (PNRIFDIS) AS PNRIFDIS from PARTCHI1 where FLCRSA<>"S" GROUP BY TIPCON, CODCON,; 
 DATSCA, NUMPAR, CODVAL, MODPAG into cursor PARCREA
    * --- Estrae le partite di saldo 
    select TIPCON, CODCON, FLCRSA, DATSCA, NUMPAR, CODVAL, PTDATDOC , MODPAG ,; 
 SUM( iif (FLDAVE ="D",1,-1) * TOTIMP) AS TOTIMP ,SALDO , ANCATCOM, FLDAVE, TIPO,; 
 DESCON , TOTALECREA , PTDATRAG, ANCODZON , PTCAOVAL,FLCRSACREA as FLCRSACREA,; 
 CODAGE, FLSEGNO , VASIMVAL,PTCAOAPE , CP_TODATE( PTDATORIG ) , MAX( PTRAGG) AS PTRAGG , MAX( PTFLRAGG) AS PTFLRAGG, MAX(COSERIAL) as COSERIAL, NVL(CCTIPDOC,"NC") AS CCTIPDOC , max(PNRIFDIS) AS PNRIFDIS from PARTCHI1 where FLCRSA="S" GROUP BY TIPCON, CODCON,; 
 FLCRSA, DATSCA, NUMPAR, CODVAL, PTDATDOC , MODPAG,SALDO into cursor PARCREA1
    if USED("PARTCHI1")
      USE IN PARTCHI1
    endif
    WRCURSOR( "PARCREA" )
    WRCURSOR( "PARCREA1" )
    UPDATE PARCREA1 SET FLSEGNO="D" , FLDAVE="D" where SALDO=0 AND TOTIMP>0 
 UPDATE PARCREA1 SET FLSEGNO="A" , FLDAVE="A" where SALDO=0 AND TOTIMP<0 
 
 UPDATE PARCREA SET FLDAVE="D" where SALDO>0 
 UPDATE PARCREA SET FLDAVE="A" where SALDO<0
    UPDATE PARCREA SET FLSEGNO="D" where SALDO>0 
 UPDATE PARCREA SET FLSEGNO="A" where SALDO<0
    * --- Il risultato delle precedenti elaborazioni � il cursore contenente 1 sola tupla per ogni partita aperta e 1 tupla per ogni partita di saldo
    select * FROM PARCREA UNION SELECT * FROM PARCREA1 into cursor PARTCHI
    if USED("PARCREA")
      USE IN PARCREA
    endif
    if USED("PARCREA1")
      USE IN PARCREA1
    endif
    * --- Drop temporary table ZOOMPART1
    i_nIdx=cp_GetTableDefIdx('ZOOMPART1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZOOMPART1')
    endif
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Drop temporary table ZOOMPART
      i_nIdx=cp_GetTableDefIdx('ZOOMPART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('ZOOMPART')
      endif
    endif
    if USED("PARTCHI")
      * --- Per avere l'aggiornamento del cursore
      SELECT PARTCHI
      =WRCURSOR("PARTCHI")
      ah_msg( "Elaborazione dati di stampa" )
      * --- Elaboro cursore di stampa 
      SELECT TIPCON,CODCON,DESCON,IIF(EMPTY(CP_TODATE(PTDATRAG)),CP_TODATE(DATSCA),CP_TODATE(PTDATRAG)) AS DATSCA, ; 
 NUMPAR,FLDAVE,IIF(CODVAL<>this.w_VALUTA, ; 
 VAL2VAL(TOTIMP, PTCAOVAL, CP_TODATE(PTDATDOC), this.w_DATAELA, this.oParentObject.w_CAMBIO, this.oParentObject.w_Decimali), ; 
 TOTIMP) * 1.00000000000000 AS TOTIMP,CP_TODATE(PTDATDOC) AS PTDATDOC, ; 
 CODVAL AS CODVAL,CODAGE AS CODAGE,ANCODZON AS ANCODZON,ANCATCOM AS ANCATCOM, FLCRSACREA as FLCRSACREA , ; 
 IIF(TIPO="A",(this.w_DATAELA-IIF(EMPTY(CP_TODATE(PTDATRAG)),CP_TODATE(DATSCA),CP_TODATE(PTDATRAG))),(CP_TODATE(PTDATDOC); 
 -IIF(EMPTY(CP_TODATE(PTDATRAG)),CP_TODATE(DATSCA),CP_TODATE(PTDATRAG)))) AS SCOST,TIPO AS TIPO, FLCRSA , TOTALECREA , FLSEGNO,VASIMVAL,PTCAOAPE , PTDATORIG, PTRAGG , PTFLRAGG , COSERIAL , CCTIPDOC AS CCTIPDOC ,PNRIFDIS AS PNRIFDIS ; 
 FROM PARTCHI INTO CURSOR PARTCHI2 
      =WRCURSOR("PARTCHI2")
      * --- Se si sta analizzando gli scostamenti dei Clienti  e  il Flag w_TIPOANALISI1 della maschera GSTE_SAS � disattivato allora si scartano  i clienti che hanno saldo in AVERE 
      *     Se si sta analizzando gli scostamenti dei Fornitori  e il Flag w_TIPOANALISI1 della maschera GSTE_SAS � disattivato allora si scartano  i fornitori che hanno saldo in DARE
      if this.oParentObject.w_TIPOANALISI1 = "S"
        * --- Per i Clienti
        SELECT TIPCON,CODCON, SUM (abs (TOTIMP) * IIF(FLSEGNO="D",1,-1) ) AS TOTALE FROM PARTCHI2 WHERE (FLCRSA<>"S") GROUP by TIPCON, CODCON HAVING ( (TOTALE >0 AND TIPCON="F") OR (TOTALE<0 AND TIPCON="C") ) into cursor ELIMINATI
        SELECT ELIMINATI 
 Go top 
 SCAN
        this.w_TIPCON = TIPCON
        this.w_CODCON = CODCON
        DELETE FROM PARTCHI2 WHERE (TIPCON=this.w_TIPCON AND CODCON=this.w_CODCON) 
 ENDSCAN
        if USED("ELIMINATI")
          USE IN ELIMINATI
        endif
      endif
      USE IN PARTCHI
      * --- Cambio il segno all'importo della partita per le note di credito
      Select PARTCHI2 
 Go Top
      =WRCURSOR("PARTCHI2")
      SCAN
      if FLCRSA="C" OR FLCRSA="A"
        Replace TOTIMP With iif((TIPCON="C" and FLDAVE="A") or (TIPCON="F" and FLDAVE="D"), abs (TOTIMP) *(-1),abs (TOTIMP))
        Replace TOTALECREA With iif((TIPCON="C" and FLDAVE="A") or (TIPCON="F" and FLDAVE="D"), ABS(TOTALECREA) *(-1),ABS(TOTALECREA))
      else
        Replace TOTIMP With iif((TIPCON="C" and FLDAVE="A") or (TIPCON="F" and FLDAVE="D"),abs(TOTIMP),abs(TOTIMP)*(-1))
        Replace TOTALECREA With iif((TIPCON="C" and FLDAVE="A") or (TIPCON="F" and FLDAVE="D"), ABS(TOTALECREA) ,ABS(TOTALECREA) *(-1) )
      endif
      ENDSCAN
      L_DECIMI=this.oParentObject.w_DECIMALI
      L_SIMVAL=this.oParentObject.w_SIMVAL
      L_VALUTA=this.w_VALUTA
      L_SCAINI=this.w_SCAINI
      L_SCAFIN=this.w_SCAFIN
      L_TIPOST=this.oParentObject.w_TIPOST
      L_PERCEN=this.oParentObject.w_PERCEN
      L_DATAELA=this.w_DATAELA
      L_TIPOANALISI=this.oParentObject.w_TIPOANALISI1
      L_DETTRIG=this.oParentObject.w_DETTRIG
      L_PAGANT=this.oParentObject.w_PAGANT
      L_PAGSCA=this.w_PAGSCA
      if this.oParentObject.w_EXCEL="S"
        if g_OFFICE="M"
          this.w_EXT = ".XLT"
        else
          this.w_EXT = ".STC"
        endif
      endif
      do case
        case this.oParentObject.w_TIPOST="C" OR this.oParentObject.w_TIPOST="F"
          L_CODINI=this.oParentObject.w_CODINI
          L_CODFIN=this.oParentObject.w_CODFIN
          if this.oParentObject.w_Excel="N"
            * --- Lancio il report grafico
            * --- Per la stampa scostamento (w_STAMPA <>'C') si visualizzano solo le partite con SCOST>0
            *     Per la stampa durata crediti (w_STAMPA='C') si visualizzano tute le partite che hanno Scostamento (SCOST<>0) , dilazione effettiva (DILEFF<>0) o dilazione concessa (DILCON<>0)
            SELECT *, IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as PAGAMENTO, cp_ROUND(TOTIMP*IIF ( NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S", 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO, DATSCA - PTDATORIG as DILCON, IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF FROM PARTCHI2 ; 
 INTO CURSOR __TMP__ WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY CODCON, PTRAGG desc , PTFLRAGG desc , DATSCA , NUMPAR,PAGAMENTO
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE_SAS.FRX","",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE4SAS.FRX","",this.oParentObject)
            endif
          else
            * --- I campi Perint e Datelabo sono stati creati per poter essere utilizzati all'interno
            *     della stampa di excel GSTE_SAS.XLT.
            *     Se viene modificato il report ricordarsi di modificare anche il foglio excel
            SELECT TIPCON AS TIPCON,CODCON AS CODCON,DESCON AS DESCON,DATSCA AS DATSCA,NUMPAR AS NUMPAR,FLDAVE AS FLDAVE,TOTALECREA as TOTALECREA ,TOTIMP AS TOTIMP,; 
 PTDATDOC AS PTDATDOC,SCOST AS SCOST,TIPO AS TIPO, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S" OR (TOTIMP * SCOST<0 AND FLCRSA<>"A" AND CCTIPDOC <>"NC" AND CCTIPDOC<>"NE" AND CCTIPDOC<>"NU" ) , 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO,; 
 cp_ROUND( ABS(TOTIMP)*SCOST,this.oParentObject.w_DECIMALI) AS SCOSTA,this.oParentObject.w_PERCEN as PERINT,this.w_DATAELA AS DATELABO,this.oParentObject.w_SIMVAL AS SIMVAL,"A" as ORDINE, VASIMVAL AS VASIMVAL , DATSCA - PTDATORIG as DILCON , IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF , PTDATORIG as PTDATORIG , IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as DATPAG , IIF (FLCRSA = "S" , " " , IIF ( PTDATDOC>L_DATAELA," ","*") ) as PAGATA , FLCRSA as FLCRSA ,CCTIPDOC ; 
 FROM PARTCHI2 INTO CURSOR EXCEL WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY CODCON,DATSCA
            * --- Creo un nuovo cursore per contenere i totali.Effettuo questa operazione
            *     perch� i totali non posso calcolarli all'interno del foglio di excel 
            SELECT TIPCON AS TIPCON,CODCON AS CODCON,MAX(DESCON) AS DESCON,MAX(DATSCA) AS DATSCA,; 
 MAX(NUMPAR) AS NUMPAR,MAX(FLDAVE) AS FLDAVE,SUM(cp_ROUND(TOTALECREA,this.oParentObject.w_DECIMALI)) as TOTALECREA , SUM(cp_ROUND(TOTIMP,this.oParentObject.w_DECIMALI)) AS TOTIMP,SUM(cp_ROUND(ABS(TOTIMP),this.oParentObject.w_DECIMALI)) AS TOTIMPABS,MAX(PTDATDOC) AS PTDATDOC,; 
 SUM(SCOSTA) AS SCOST,MAX(TIPO) AS TIPO, SUM(cp_ROUND(IIF (COSTO>0 OR FLCRSA="A" OR CCTIPDOC ="NC" OR CCTIPDOC="NE" OR CCTIPDOC="NU" ,COSTO,0),this.oParentObject.w_DECIMALI)) AS COSTO,MAX(SCOSTA) AS SCOSTA,MAX(PERINT) AS PERINT,; 
 MAX(DATELABO) AS DATELABO,MAX(SIMVAL) AS SIMVAL,"Z" as ORDINE, MAX(VASIMVAL) AS VASIMVAL, SUM ( DILCON ) as DILCON , SUM (DILEFF) as DILEFF , MAX (PTDATORIG) as PTDATORIG, " " as FLCRSA ,CCTIPDOC ; 
 FROM EXCEL INTO CURSOR TOTALI ORDER BY ORDINE,CODCON GROUP BY TIPCON,CODCON
            * --- Calcolo lo Scostamento Medio
            SELECT TIPCON AS TIPCON,CODCON AS CODCON,LEFT("TOTALE"+SPACE(40),40) AS DESCON,DATE(1900,1,1) AS DATSCA,; 
 SPACE(LEN(NUMPAR)) AS NUMPAR," " AS FLDAVE,0 as TOTALECREA , TOTIMP AS TOTIMP,cp_CharToDate("  -  -  ") AS PTDATDOC,; 
 IVAROUND((SCOST/TOTIMPABS),0,1,g_Codlir) as SCOST," " AS TIPO, COSTO AS COSTO,; 
 SCOSTA AS SCOSTA,PERINT AS PERINT,cp_CharToDate("  -  -  ") AS DATELABO,SIMVAL AS SIMVAL,"Z" as ORDINE, VASIMVAL AS VASIMVAL, DILCON , DILEFF , cp_CharToDate("  -  -  ") as PTDATORIG , cp_CharToDate("  -  -  ") as DATPAG , " " as PAGATA, " " as FLCRSA ,CCTIPDOC ; 
 FROM TOTALI INTO CURSOR TOT_GEN ORDER BY ORDINE,TIPCON,CODCON
            * --- Metto in union il cursore Excel con il cursore Totali
            SELECT * FROM EXCEL ;
            UNION ALL;
            SELECT * FROM TOT_GEN ;
            ORDER BY 1,2 , ORDINE, NUMPAR,DATPAG INTO CURSOR __TMP__ NOFILTER
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE_XAS"+this.w_EXT,"",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE4XAS"+this.w_EXT,"",this.oParentObject)
            endif
          endif
        case this.oParentObject.w_TIPOST="A"
          L_CODINI=this.oParentObject.w_CODINIAG
          L_CODFIN=this.oParentObject.w_CODFINAG
          L_DESINI=this.oparentobject.w_DESCRI3
          L_DESFIN=this.oparentobject.w_DESCRI4
          if this.oParentObject.w_EXCEL="N"
            * --- Per la stampa scostamento (w_STAMPA <>'C') si visualizzano solo le partite con SCOST>0
            *     Per la stampa durata crediti (w_STAMPA='C') si visualizzano tute le partite che hanno Scostamento (SCOST<>0) , dilazione effettiva (DILEFF<>0) o dilazione concessa (DILCON<>0)
            SELECT *, IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as DATPAG , cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S", 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO, DATSCA - PTDATORIG as DILCON, IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF, IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as PAGAMENTO FROM PARTCHI2 ; 
 INTO CURSOR __TMP__ WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY CODAGE,DATSCA, NUMPAR,PAGAMENTO 
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE1SAS.FRX","",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE5SAS.FRX","",this.oParentObject)
            endif
          else
            * --- I campi Perint e Datelabo sono stati creati per poter essere utilizzati all'interno
            *     della stampa di excel GSTE1SAS.XLT.
            *     Se viene modificato il report ricordarsi di modificare anche il foglio excel
            SELECT CODAGE AS CODAGE,TIPCON AS TIPCON,CODCON AS CODCON,DATSCA AS DATSCA,NUMPAR AS NUMPAR,FLDAVE AS FLDAVE, TOTALECREA as TOTALECREA , TOTIMP AS TOTIMP,; 
 PTDATDOC AS PTDATDOC,SCOST AS SCOST,TIPO AS TIPO, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S" OR (TOTIMP * SCOST<0 AND FLCRSA<>"A" AND CCTIPDOC <>"NC" AND CCTIPDOC<>"NE" AND CCTIPDOC<>"NU" ) , 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO,; 
 cp_ROUND(ABS(TOTIMP)*SCOST,this.oParentObject.w_DECIMALI) AS SCOSTA,this.oParentObject.w_PERCEN as PERINT,this.w_DATAELA AS DATELABO,this.oParentObject.w_SIMVAL AS SIMVAL,"A" as ORDINE , DATSCA - PTDATORIG as DILCON , IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF , PTDATORIG as PTDATORIG , IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as DATPAG , IIF (FLCRSA = "S" , " " , IIF ( PTDATDOC>L_DATAELA," ","*") ) as PAGATA,FLCRSA as FLCRSA , CCTIPDOC ; 
 FROM PARTCHI2 INTO CURSOR EXCEL WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY CODAGE,DATSCA
            * --- Creo un nuovo cursore per contenere i totali.Effettuo questa operazione
            *     perch� i totali non posso calcolarli all'interno del foglio di excel 
            SELECT CODAGE AS CODAGE,MAX(TIPCON) AS TIPCON,MAX(CODCON) AS CODCON,MAX(DATSCA) AS DATSCA,; 
 MAX(NUMPAR) AS NUMPAR,MAX(FLDAVE) AS FLDAVE, SUM(cp_ROUND(TOTALECREA,this.oParentObject.w_DECIMALI)) as TOTALECREA ,SUM(cp_ROUND(TOTIMP,this.oParentObject.w_DECIMALI)) AS TOTIMP,SUM(cp_ROUND(ABS(TOTIMP),this.oParentObject.w_DECIMALI)) AS TOTIMPABS,MAX(PTDATDOC) AS PTDATDOC,; 
 SUM(SCOSTA) AS SCOST,MAX(TIPO) AS TIPO, SUM(cp_ROUND(IIF (COSTO>0 OR FLCRSA="A" OR CCTIPDOC ="NC" OR CCTIPDOC="NE" OR CCTIPDOC="NU",COSTO,0),this.oParentObject.w_DECIMALI)) AS COSTO,MAX(SCOSTA) AS SCOSTA,MAX(PERINT) AS PERINT,; 
 MAX(DATELABO) AS DATELABO,MAX(SIMVAL) AS SIMVAL,"Z" as ORDINE , SUM ( DILCON ) as DILCON , SUM (DILEFF) as DILEFF , MAX (PTDATORIG) as PTDATORIG , MAX (DATPAG) as DATPAG, " " as PAGATA, " " as FLCRSA , CCTIPDOC ; 
 FROM EXCEL INTO CURSOR TOTALI ORDER BY ORDINE,CODAGE GROUP BY CODAGE
            * --- Calcolo lo Scostamento Medio
            SELECT CODAGE AS CODAGE," " AS TIPCON,SPACE(LEN(CODCON)) AS CODCON,DATE(1900,1,1) AS DATSCA,; 
 LEFT("TOTALE"+SPACE(14),14) AS NUMPAR," " AS FLDAVE,0 as TOTALECREA ,TOTIMP AS TOTIMP,cp_CharToDate("  -  -  ") AS PTDATDOC,; 
 IVAROUND((SCOST/TOTIMPABS),0,1,g_Codlir) as SCOST," " AS TIPO, COSTO AS COSTO,; 
 SCOSTA AS SCOSTA,PERINT AS PERINT,cp_CharToDate("  -  -  ") AS DATELABO,SIMVAL AS SIMVAL,"Z" as ORDINE , DILCON , DILEFF , cp_CharToDate("  -  -  ") as PTDATORIG , cp_CharToDate("  -  -  ") as DATPAG , " " as PAGATA , " " as FLCRSA , CCTIPDOC ; 
 FROM TOTALI INTO CURSOR TOT_GEN ORDER BY ORDINE,CODAGE
            * --- Metto in union il cursore Excel con il cursore Totali
            SELECT * FROM EXCEL ;
            UNION ALL;
            SELECT * FROM TOT_GEN ;
            ORDER BY 1, ORDINE,NUMPAR,DATPAG INTO CURSOR __TMP__ NOFILTER
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE1XAS"+this.w_EXT,"",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE5XAS"+this.w_EXT,"",this.oParentObject)
            endif
          endif
        case this.oParentObject.w_TIPOST="M"
          L_CODINI=this.oParentObject.w_CODINICT
          L_CODFIN=this.oParentObject.w_CODFINCT
          if this.oParentObject.w_EXCEL="N"
            * --- Per la stampa scostamento (w_STAMPA <>'C') si visualizzano solo le partite con SCOST>0
            *     Per la stampa durata crediti (w_STAMPA='C') si visualizzano tute le partite che hanno Scostamento (SCOST<>0) , dilazione effettiva (DILEFF<>0) o dilazione concessa (DILCON<>0)
            SELECT *, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S", 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO, DATSCA - PTDATORIG as DILCON, IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF, IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as PAGAMENTO FROM PARTCHI2 ; 
 INTO CURSOR __TMP__ WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY ANCATCOM,TIPCON,CODCON,DATSCA, NUMPAR,PAGAMENTO
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE2SAS.FRX","",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE6SAS.FRX","",this.oParentObject)
            endif
          else
            * --- I campi Perint e Datelabo sono stati creati per poter essere utilizzati all'interno
            *     della stampa di excel GSTE2SAS.XLT.
            *     Se viene modificato il report ricordarsi di modificare anche il foglio excel
            SELECT ANCATCOM AS ANCATCOM,TIPCON AS TIPCON,CODCON AS CODCON,DATSCA AS DATSCA,NUMPAR AS NUMPAR,FLDAVE AS FLDAVE, TOTALECREA as TOTALECREA ,TOTIMP AS TOTIMP,; 
 PTDATDOC AS PTDATDOC,SCOST AS SCOST,TIPO AS TIPO, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S" OR (TOTIMP * SCOST<0 AND FLCRSA<>"A" AND CCTIPDOC <>"NC" AND CCTIPDOC<>"NE" AND CCTIPDOC<>"NU"), 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO,; 
 cp_ROUND(ABS(TOTIMP)*SCOST,this.oParentObject.w_DECIMALI) AS SCOSTA,this.oParentObject.w_PERCEN as PERINT,this.w_DATAELA AS DATELABO,this.oParentObject.w_SIMVAL AS SIMVAL,"A" as ORDINE , DATSCA - PTDATORIG as DILCON , IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF , PTDATORIG as PTDATORIG , IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as DATPAG , IIF (FLCRSA = "S" , " " , IIF ( PTDATDOC>L_DATAELA," ","*") ) as PAGATA ,FLCRSA as FLCRSA ,CCTIPDOC ; 
 FROM PARTCHI2 INTO CURSOR EXCEL WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY ANCATCOM,TIPCON,CODCON,DATSCA
            * --- Creo un nuovo cursore per contenere i totali.Effettuo questa operazione
            *     perch� i totali non posso calcolarli all'interno del foglio di excel 
            SELECT ANCATCOM AS ANCATCOM,TIPCON AS TIPCON,MAX(CODCON) AS CODCON,MAX(DATSCA) AS DATSCA,; 
 MAX(NUMPAR) AS NUMPAR,MAX(FLDAVE) AS FLDAVE, SUM(cp_ROUND(TOTALECREA,this.oParentObject.w_DECIMALI)) as TOTALECREA , SUM(cp_ROUND(TOTIMP,this.oParentObject.w_DECIMALI)) AS TOTIMP,SUM(cp_ROUND(ABS(TOTIMP),this.oParentObject.w_DECIMALI)) AS TOTIMPABS,MAX(PTDATDOC) AS PTDATDOC,; 
 SUM(SCOSTA) AS SCOST,MAX(TIPO) AS TIPO, SUM(cp_ROUND(IIF (COSTO>0 OR FLCRSA="A" OR CCTIPDOC ="NC" OR CCTIPDOC="NE" OR CCTIPDOC="NU" ,COSTO,0),this.oParentObject.w_DECIMALI)) AS COSTO,MAX(SCOSTA) AS SCOSTA,MAX(PERINT) AS PERINT,; 
 MAX(DATELABO) AS DATELABO,MAX(SIMVAL) AS SIMVAL,"Z" as ORDINE,SUM ( DILCON ) as DILCON , SUM (DILEFF) as DILEFF , MAX (PTDATORIG) as PTDATORIG , MAX (DATPAG) as DATPAG, " " as PAGATA , " " as FLCRSA,CCTIPDOC ; 
 FROM EXCEL INTO CURSOR TOTALI ORDER BY ANCATCOM,TIPCON,CODCON,ORDINE GROUP BY TIPCON,ANCATCOM
            * --- Calcolo lo Scostamento Medio
            SELECT ANCATCOM AS ANCATCOM,TIPCON AS TIPCON,CODCON AS CODCON,DATE(1900,1,1) AS DATSCA,; 
 LEFT("TOTALE"+SPACE(14),14) AS NUMPAR," " AS FLDAVE, 0 as TOTALECREA , TOTIMP AS TOTIMP,cp_CharToDate("  -  -  ") AS PTDATDOC,; 
 IVAROUND((SCOST/TOTIMPABS),0,1,g_Codlir) as SCOST," " AS TIPO, COSTO AS COSTO,; 
 SCOSTA AS SCOSTA,PERINT AS PERINT,cp_CharToDate("  -  -  ") AS DATELABO,SIMVAL AS SIMVAL,"Z" as ORDINE , DILCON , DILEFF , cp_CharToDate("  -  -  ") as PTDATORIG , cp_CharToDate("  -  -  ") as DATPAG , " " as PAGATA , " " as FLCRSA ,CCTIPDOC ; 
 FROM TOTALI INTO CURSOR TOT_GEN ORDER BY ANCATCOM,TIPCON,CODCON,ORDINE
            * --- Metto in union il cursore Excel con il cursore Totali
            SELECT * FROM EXCEL ;
            UNION ALL;
            SELECT * FROM TOT_GEN ;
            ORDER BY 1,2,3,16, NUMPAR,DATPAG INTO CURSOR __TMP__ NOFILTER
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE2XAS"+this.w_EXT,"",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE6XAS"+this.w_EXT,"",this.oParentObject)
            endif
          endif
        case this.oParentObject.w_TIPOST="Z"
          L_CODINI=this.oParentObject.w_CODINIZO
          L_CODFIN=this.oParentObject.w_CODFINZO
          if this.oParentObject.w_EXCEL="N"
            * --- Per la stampa scostamento (w_STAMPA <>'C') si visualizzano solo le partite con SCOST>0
            *     Per la stampa durata crediti (w_STAMPA='C') si visualizzano tute le partite che hanno Scostamento (SCOST<>0) , dilazione effettiva (DILEFF<>0) o dilazione concessa (DILCON<>0)
            SELECT *, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S", 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO, DATSCA - PTDATORIG as DILCON, IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF , IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as PAGAMENTO FROM PARTCHI2 ; 
 INTO CURSOR __TMP__ WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY ANCODZON,TIPCON,CODCON,DATSCA, NUMPAR,PAGAMENTO
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE3SAS.FRX","",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE7SAS.FRX","",this.oParentObject)
            endif
          else
            * --- I campi Perint e Datelabo sono stati creati per poter essere utilizzati all'interno
            *     della stampa di excel GSTE3SAS.XLT.
            *     Se viene modificato il report ricordarsi di modificare anche il foglio excel
            SELECT ANCODZON AS ANCODZON,TIPCON AS TIPCON,CODCON AS CODCON,DATSCA AS DATSCA,NUMPAR AS NUMPAR,FLDAVE AS FLDAVE, TOTALECREA as TOTALECREA , TOTIMP AS TOTIMP,; 
 PTDATDOC AS PTDATDOC,SCOST AS SCOST,TIPO AS TIPO, cp_ROUND(TOTIMP*IIF (NVL (FLCRSA, "  ") ="A" AND this.oParentObject.w_PAGANT<>"S" OR (TOTIMP * SCOST<0 AND FLCRSA<>"A" AND CCTIPDOC <>"NC" AND CCTIPDOC<>"NE" AND CCTIPDOC<>"NU") , 0 , SCOST )*this.oParentObject.w_PERCEN/36500,this.oParentObject.w_Decimali) AS COSTO,; 
 cp_ROUND(ABS(TOTIMP)*SCOST,this.oParentObject.w_DECIMALI) AS SCOSTA,this.oParentObject.w_PERCEN as PERINT,this.w_DATAELA AS DATELABO,this.oParentObject.w_SIMVAL AS SIMVAL,"A" as ORDINE,DATSCA - PTDATORIG as DILCON , IIF (FLCRSA = "S" , PTDATDOC - PTDATORIG , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) - PTDATORIG) as DILEFF , PTDATORIG as PTDATORIG , IIF (FLCRSA = "S" , PTDATDOC , IIF ( PTDATDOC>L_DATAELA,PTDATDOC,L_DATAELA) ) as DATPAG , IIF (FLCRSA = "S" , " " , IIF ( PTDATDOC>L_DATAELA," ","*") ) as PAGATA ,FLCRSA as FLCRSA , CCTIPDOC; 
 FROM PARTCHI2 INTO CURSOR EXCEL WHERE (SCOST>0 OR this.oParentObject.w_STAMPA="C" ) ORDER BY ANCODZON,TIPCON,CODCON,DATSCA
            * --- Creo un nuovo cursore per contenere i totali.Effettuo questa operazione
            *     perch� i totali non posso calcolarli all'interno del foglio di excel 
            SELECT ANCODZON AS ANCODZON,TIPCON AS TIPCON,MAX(CODCON) AS CODCON,MAX(DATSCA) AS DATSCA,; 
 MAX(NUMPAR) AS NUMPAR,MAX(FLDAVE) AS FLDAVE, SUM(cp_ROUND(TOTALECREA,this.oParentObject.w_DECIMALI)) as TOTALECREA , SUM(cp_ROUND(TOTIMP,this.oParentObject.w_DECIMALI)) AS TOTIMP,SUM(cp_ROUND(ABS(TOTIMP),this.oParentObject.w_DECIMALI)) AS TOTIMPABS,MAX(PTDATDOC) AS PTDATDOC,; 
 SUM(SCOSTA) AS SCOST,MAX(TIPO) AS TIPO, SUM(cp_ROUND(IIF (COSTO>0 OR FLCRSA="A" OR CCTIPDOC ="NC" OR CCTIPDOC="NE" OR CCTIPDOC="NU",COSTO,0),this.oParentObject.w_DECIMALI)) AS COSTO,MAX(SCOSTA) AS SCOSTA,MAX(PERINT) AS PERINT,; 
 MAX(DATELABO) AS DATELABO,MAX(SIMVAL) AS SIMVAL,"Z" as ORDINE,SUM ( DILCON ) as DILCON , SUM (DILEFF) as DILEFF , MAX (PTDATORIG) as PTDATORIG , MAX (DATPAG) as DATPAG, " " as PAGATA , " " as FLCRSA , CCTIPDOC; 
 FROM EXCEL INTO CURSOR TOTALI ORDER BY ANCODZON,TIPCON,CODCON,ORDINE GROUP BY TIPCON,ANCODZON
            * --- Calcolo lo Scostamento Medio
            SELECT ANCODZON AS ANCODZON,TIPCON AS TIPCON,CODCON AS CODCON,DATE(1900,1,1) AS DATSCA,; 
 LEFT("TOTALE"+SPACE(14),14) AS NUMPAR," " AS FLDAVE, 0 as TOTALECREA , TOTIMP AS TOTIMP,cp_CharToDate("  -  -  ") AS PTDATDOC,; 
 IVAROUND((SCOST/TOTIMPABS),0,1,g_Codlir) as SCOST," " AS TIPO, COSTO AS COSTO,; 
 SCOSTA AS SCOSTA,PERINT AS PERINT,cp_CharToDate("  -  -  ") AS DATELABO,SIMVAL AS SIMVAL,"Z" as ORDINE , DILCON , DILEFF , cp_CharToDate("  -  -  ") as PTDATORIG , cp_CharToDate("  -  -  ") as DATPAG , " " as PAGATA , " " as FLCRSA , CCTIPDOC; 
 FROM TOTALI INTO CURSOR TOT_GEN ORDER BY ANCODZON,TIPCON,CODCON,ORDINE
            * --- Metto in union il cursore Excel con il cursore Totali
            SELECT * FROM EXCEL ;
            UNION ALL;
            SELECT * FROM TOT_GEN ;
            ORDER BY 1,2,3,16, NUMPAR,DATPAG INTO CURSOR __TMP__ NOFILTER
            if this.oParentObject.w_STAMPA="S"
              CP_CHPRN("QUERY\GSTE3XAS"+this.w_EXT,"",this.oParentObject)
            else
              CP_CHPRN("QUERY\GSTE7XAS"+this.w_EXT,"",this.oParentObject)
            endif
          endif
      endcase
      if USED("PARTCHI2")
        USE IN PARTCHI2
      endif
      if USED("EXCEL")
        USE IN EXCEL
      endif
      if USED("TOTCLI")
        USE IN TOTCLI
      endif
      if USED("TOT_GEN")
        USE IN TOT_GEN
      endif
      if USED("TOTALI")
        USE IN TOTALI
      endif
      if USED("PARCREA")
        USE IN PARCREA
      endif
      if USED("__TMP__")
        USE IN __TMP__
      endif
    endif
  endproc


  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*ZOOMPART'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='*ZOOMPART1'
    this.cWorkTables[4]='AZIENDA'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
