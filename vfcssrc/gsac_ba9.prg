* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_ba9                                                        *
*              Proposte d'acquisto                                             *
*                                                                              *
*      Author: ZUCCHETTI TAM SRL                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_59]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-18                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pScelta
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_ba9",oParentObject,m.pScelta)
return(i_retval)

define class tgsac_ba9 as StdBatch
  * --- Local variables
  pScelta = space(10)
  w_PROG = .NULL.
  w_MVFLVEAC = space(1)
  Trovato = .f.
  w_DTOBSO = ctod("  /  /  ")
  w_MVCLADOC = space(2)
  w_POS1 = 0
  w_DTINVA = ctod("  /  /  ")
  cTMP = space(10)
  w_POS2 = 0
  w_QTAMIN = 0
  w_LOTRIO = 0
  w_GIOAPP = 0
  TmpN = 0
  w_QTANEW = 0
  TmpC = space(100)
  w_CONTRA = space(15)
  w_ALL = .f.
  DataDiBase = ctod("  /  /  ")
  w_OrderBy = space(10)
  DatRisul = ctod("  /  /  ")
  TipoCalc = space(1)
  w_PERFIND = .f.
  * --- WorkFile variables
  DOC_MAST_idx=0
  KEY_ARTI_idx=0
  PDA_TPER_idx=0
  PAR_RIOR_idx=0
  CON_TRAD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Proposte d'Acquisto (da GSAC_APD)
    do case
      case this.pScelta=="D"
        * --- carico il documento
        gsar_bzm(this,this.oParentObject.w_PDSERRIF,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pScelta="load"
        this.oParentObject.w_OLDSTATUS = this.oParentObject.w_PDSTATUS
      case this.pScelta="ins"
        if this.oParentObject.w_PDSTATUS="S" or empty(this.oParentObject.w_PDSTATUS)
          this.oParentObject.w_PDSTATUS = "D"
        endif
      case this.pScelta="upd"
        this.cTMP = " "
        if this.oParentObject.w_OLDSTATUS="O"
          this.cTMP = ah_Msgformat("Questa PDA ha gi� generato un ordine, impossibile modificarla")
        else
          if empty(this.oParentObject.w_PDCODMAG)
            this.cTMP = ah_Msgformat("Codice magazzino mancante")
          endif
          if empty(this.oParentObject.w_PDCODCON)
            this.cTMP = ah_Msgformat("Codice fornitore mancante")
          endif
        endif
        if not empty(this.cTMP)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.cTMP
        endif
      case this.pScelta="status"
        this.w_POS1 = at(this.oParentObject.w_PDSTATUS,"SDO")
        this.w_POS2 = at(this.oParentObject.w_OLDSTATUS,"SDO")
        if this.w_POS1<this.w_POS2 or (this.oParentObject.w_PDSTATUS="O" and this.oParentObject.w_OLDSTATUS<>"O")
          ah_ErrorMsg("Errore: impossibile impostare questo stato","!","")
          this.oParentObject.w_PDSTATUS = this.oParentObject.w_OLDSTATUS
        endif
      case this.pScelta="codcon"
        * --- Cerca un codice di ricerca associato al fornitore
        this.Trovato = .F.
        * --- Select from KEY_ARTI
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" KEY_ARTI ";
              +" where CACODART="+cp_ToStrODBC(this.oParentObject.w_PDCODART)+" and CACODCON="+cp_ToStrODBC(this.oParentObject.w_PDCODCON)+"";
               ,"_Curs_KEY_ARTI")
        else
          select * from (i_cTable);
           where CACODART=this.oParentObject.w_PDCODART and CACODCON=this.oParentObject.w_PDCODCON;
            into cursor _Curs_KEY_ARTI
        endif
        if used('_Curs_KEY_ARTI')
          select _Curs_KEY_ARTI
          locate for 1=1
          do while not(eof())
          this.w_DTINVA = nvl(cp_ToDate(_Curs_KEY_ARTI.CADTINVA),i_DATSYS)
          this.w_DTOBSO = nvl(cp_ToDate(_Curs_KEY_ARTI.CADTOBSO),i_DATSYS)
          if this.w_DTINVA<=i_DATSYS and this.w_DTOBSO<=i_DATSYS
            this.oParentObject.w_PDCODICE = _Curs_KEY_ARTI.CACODICE
            this.oParentObject.w_PDUMORDI = _Curs_KEY_ARTI.CAUNIMIS
            this.oParentObject.w_UNMIS3 = _Curs_KEY_ARTI.CAUNIMIS
            this.oParentObject.w_MOLTI3 = _Curs_KEY_ARTI.CAMOLTIP
            this.oParentObject.w_OPERA3 = _Curs_KEY_ARTI.CAOPERAT
            this.oParentObject.w_COCONA = _Curs_KEY_ARTI.CACODCON
            this.oParentObject.w_PDQTAORD = iif(_Curs_KEY_ARTI.CAOPERAT="*", this.oParentObject.w_PDQTAUM1*_Curs_KEY_ARTI.CAMOLTIP, this.oParentObject.w_PDQTAUM1/_Curs_KEY_ARTI.CAMOLTIP)
            this.Trovato = .T.
          endif
            select _Curs_KEY_ARTI
            continue
          enddo
          use
        endif
        if not this.Trovato and EMPTY(this.oParentObject.w_PDCODCON)
          this.oParentObject.w_PDCODICE = this.oParentObject.w_PDCODART
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAMOLTIP,CAOPERAT"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_PDCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAMOLTIP,CAOPERAT;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_PDCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PDUMORDI = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.oParentObject.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.oParentObject.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            this.oParentObject.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_PDQTAORD = this.oParentObject.w_PDQTAUM1
          this.oParentObject.w_COCONA = SPACE(15)
        endif
        this.oParentObject.w_PDCONTRA = ""
        this.w_QTANEW = 0
        * --- Select from GSACZAPD
        do vq_exec with 'GSACZAPD',this,'_Curs_GSACZAPD','',.f.,.t.
        if used('_Curs_GSACZAPD')
          select _Curs_GSACZAPD
          locate for 1=1
          do while not(eof())
          this.w_CONTRA = nvl(_Curs_GSACZAPD.conumero,"")
          this.w_QTAMIN = nvl(_Curs_GSACZAPD.coqtamin,0)
          this.w_LOTRIO = nvl(_Curs_GSACZAPD.colotmul,0)
          this.w_GIOAPP = nvl(_Curs_GSACZAPD.cogioapp,0)
            select _Curs_GSACZAPD
            continue
          enddo
          use
        endif
        * --- Se non trovo un contratto valido leggo i dati dall'articolo
        if EMPTY(this.w_CONTRA)
          * --- Read from PAR_RIOR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PRQTAMIN,PRLOTRIO,PRGIOAPP"+;
              " from "+i_cTable+" PAR_RIOR where ";
                  +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_PDCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PRQTAMIN,PRLOTRIO,PRGIOAPP;
              from (i_cTable) where;
                  PRCODART = this.oParentObject.w_PDCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_QTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
            this.w_LOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
            this.oParentObject.w_GIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.oParentObject.w_PDCRIFOR = "M"
          this.oParentObject.w_PDCONTRA = this.w_CONTRA
          =setValueLinked("M",this.oParentObject,"w_PDCONTRA",this.w_CONTRA)
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.mCalc(.T.)
      case this.pScelta $ "DATAFINVAR-DATAINIVAR"
        * --- Variabile per la gestione dei giorni festivi
        if NOT EMPTY(this.oParentObject.w_PPCALSTA)
          this.w_ALL = .F.
          SET_NEAR = Set("NEAR")
          SET NEAR ON
          if this.pScelta="DATAINIVAR"
            * --- Avanti
            this.TipoCalc = "A"
            this.DataDiBase = this.oParentObject.w_PDDATORD
            this.w_OrderBy = "CAL_AZIE.CAGIORNO ASC"
          else
            this.TipoCalc = "I"
            this.DataDiBase = this.oParentObject.w_PDDATEVA
            this.w_OrderBy = "CAL_AZIE.CAGIORNO DESC"
          endif
          if Not Empty(this.DataDiBase)
            VQ_EXEC("QUERY\GSAC6BFB", this, "Calendario")
            select cagiorno, tpperass, dtos(cagiorno) as datidx, canumore>0 as giolav from Calendario into cursor Calend nofilter 
 index on datidx to datidx for giolav 
 use in Calendario
            Select Calend 
 reccount() 
 go top
            lOnError = On("Error") 
 On error w_Err=TRUE
            if this.pScelta="DATAINIVAR"
              Select Calend 
 go top
              skip +ceiling(this.oParentObject.w_GIOAPP)
              if eof()
                * --- Data posteriore all'orizzonte di pianificazione
                ah_ErrorMsg("Raggiunto limite superiore dell'orizzonte di pianificazione","!","")
              endif
            else
              Select Calend 
 go bottom
              skip -ceiling(this.oParentObject.w_GIOAPP)
              if bof()
                * --- Data anteriore all'orizzonte di pianificazione
                ah_ErrorMsg("Raggiunto limite inferiore dell'orizzonte di pianificazione" ,"!","")
              endif
            endif
            this.DatRisul = CP_TODATE(Calend.cagiorno)
            if this.pScelta="DATAINIVAR"
              if Empty(this.DatRisul)
                this.DatRisul = this.oParentObject.w_PDDATORD
              endif
              this.oParentObject.w_PDDATEVA = this.DatRisul
            else
              if Empty(this.DatRisul)
                this.DatRisul = this.oParentObject.w_PDDATEVA
              endif
              this.oParentObject.w_PDDATORD = this.DatRisul
            endif
            On error &lOnError 
 Select Calend
            SET NEAR &SET_NEAR
            * --- Chiude Cursore
            Use IN select("Calend")
          endif
        else
          if this.pScelta="DATAINIVAR"
            this.oParentObject.w_PDDATEVA = this.oParentObject.w_PDDATORD+this.oParentObject.w_GIOAPP
          else
            this.oParentObject.w_PDDATORD = this.oParentObject.w_PDDATEVA-this.oParentObject.w_GIOAPP
          endif
        endif
        * --- Legge periodo relativo alla data di evasione
        * --- Select from PDA_TPER
        i_nConn=i_TableProp[this.PDA_TPER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2],.t.,this.PDA_TPER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select TPPERASS  from "+i_cTable+" PDA_TPER ";
              +" where TPDATINI<="+cp_ToStrODBC(this.oParentObject.w_PDDATEVA)+" and "+cp_ToStrODBC(this.oParentObject.w_PDDATEVA)+"<=TPDATFIN";
               ,"_Curs_PDA_TPER")
        else
          select TPPERASS from (i_cTable);
           where TPDATINI<=this.oParentObject.w_PDDATEVA and this.oParentObject.w_PDDATEVA<=TPDATFIN;
            into cursor _Curs_PDA_TPER
        endif
        if used('_Curs_PDA_TPER')
          select _Curs_PDA_TPER
          locate for 1=1
          do while not(eof())
          this.w_PERFIND = .T.
          this.oParentObject.w_PDPERASS = _Curs_PDA_TPER.TPPERASS
            select _Curs_PDA_TPER
            continue
          enddo
          use
        endif
        if !this.w_PERFIND
          this.oParentObject.w_PDPERASS = SPACE(3)
        endif
        if empty(this.oParentObject.w_PDPERASS)
          ah_ErrorMsg("Attenzione: data non compresa nell'orizzonte di pianificazione","!","")
        endif
      case (this.pScelta="qtaord" or this.pScelta="umordi") and not empty(this.oParentObject.w_PDUMORDI)
        if this.pScelta="qtaord"
          this.oParentObject.w_PDQTAUM1 = CALQTAADV(this.oParentObject.w_PDQTAORD,this.oParentObject.w_PDUMORDI, this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_FLFRAZ, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3,, , this.oparentobject, "PDQTAORD", .T.)
          * --- Vincolo quantit� minima (Gestione a fabbisogno)
          if this.oParentObject.w_PDQTAUM1<this.w_QTAMIN and this.oParentObject.w_TIPGES="F"
            this.oParentObject.w_PDQTAUM1 = this.w_QTAMIN
          endif
        endif
        * --- Riassegna la quantit� da ordinare
        if this.oParentObject.w_PDUMORDI=this.oParentObject.w_PDUNIMIS
          * --- UM principale
          this.oParentObject.w_PDQTAORD = this.oParentObject.w_PDQTAUM1
        else
          if this.oParentObject.w_PDUMORDI=this.oParentObject.w_UNMIS2
            * --- UM secondaria
            this.oParentObject.w_PDQTAORD = iif(this.oParentObject.w_OPERAT="/", this.oParentObject.w_PDQTAUM1/this.oParentObject.w_MOLTIP, this.oParentObject.w_PDQTAUM1*this.oParentObject.w_MOLTIP)
          else
            * --- UM associata al codice di ricerca
            this.oParentObject.w_PDQTAORD = iif(this.oParentObject.w_OPERA3="/", this.oParentObject.w_PDQTAUM1/this.oParentObject.w_MOLTI3, this.oParentObject.w_PDQTAUM1*this.oParentObject.w_MOLTI3)
          endif
        endif
      case this.pScelta="contra"
        * --- Read from CON_TRAD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2],.t.,this.CON_TRAD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COQTAMIN,COLOTMUL,COGIOAPP"+;
            " from "+i_cTable+" CON_TRAD where ";
                +"CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_PDCONTRA);
                +" and COCODART = "+cp_ToStrODBC(this.oParentObject.w_PDCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COQTAMIN,COLOTMUL,COGIOAPP;
            from (i_cTable) where;
                CONUMERO = this.oParentObject.w_PDCONTRA;
                and COCODART = this.oParentObject.w_PDCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_QTAMIN = NVL(cp_ToDate(_read_.COQTAMIN),cp_NullValue(_read_.COQTAMIN))
          this.w_LOTRIO = NVL(cp_ToDate(_read_.COLOTMUL),cp_NullValue(_read_.COLOTMUL))
          this.oParentObject.w_GIOAPP = NVL(cp_ToDate(_read_.COGIOAPP),cp_NullValue(_read_.COGIOAPP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do vq_exec with "query\Gsac_bcb",this,"Periodi"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_QTANEW = this.oParentObject.w_PDQTAUM1
    if this.oParentObject.w_TIPGES="F"
      * --- Vincolo quantit� minima
      if this.w_QTANEW < this.w_QTAMIN
        this.w_QTANEW = this.w_QTAMIN
      endif
      * --- Vincolo lotto di riordino
      if this.w_LOTRIO > 0
        this.TmpN = this.w_QTANEW/this.w_LOTRIO
        if this.TmpN<>ceiling(this.TmpN)
          this.w_QTANEW = this.w_LOTRIO * ceiling(this.TmpN)
        endif
      endif
    endif
    this.oParentObject.w_PDQTAORD = this.oParentObject.w_PDQTAUM1
    if this.oParentObject.w_TIPGES="S"
      * --- Gestione a scorta
      this.oParentObject.w_PDDATORD = i_DATSYS
      this.oParentObject.w_PDDATEVA = this.oParentObject.w_PDDATORD + this.oParentObject.w_GIOAPP
    else
      * --- Gestione a fabbisogno
      this.oParentObject.w_PDDATORD = this.oParentObject.w_PDDATEVA - this.oParentObject.w_GIOAPP
    endif
    if not empty(this.oParentObject.w_PDDATEVA)
      Select Periodi
      Locate for tpdatini<=this.oParentObject.w_PDDATEVA and this.oParentObject.w_PDDATEVA<=tpdatfin
      if found()
        this.oParentObject.w_PDPERASS = tpperass
      else
        this.oParentObject.w_PDPERASS = "   "
      endif
    endif
    if this.w_QTANEW<>this.oParentObject.w_PDQTAUM1
      if ah_YesNo("Adeguare quantit� ordine a politica di lottizzazione? %0%0Quantit� originaria: %1 %0Quantit� calcolata: %2",,this.oParentObject.w_PDUMORDI+tran(this.oParentObject.w_PDQTAORD,v_PQ(12) ),tran(this.w_QTANEW,v_PQ(12) ))
        this.oParentObject.w_PDQTAUM1 = this.w_QTANEW
        if EMPTY(this.oParentObject.w_PDUMORDI)
          this.oParentObject.w_PDUMORDI = this.oParentObject.w_PDUNIMIS
        endif
        if this.oParentObject.w_PDUMORDI=this.oParentObject.w_PDUNIMIS
          * --- UM principale
          this.oParentObject.w_PDQTAORD = this.oParentObject.w_PDQTAUM1
        else
          if this.oParentObject.w_PDUMORDI=this.oParentObject.w_UNMIS2
            * --- UM secondaria
            this.oParentObject.w_PDQTAORD = iif(this.oParentObject.w_OPERAT="/", this.oParentObject.w_PDQTAUM1/this.oParentObject.w_MOLTIP, this.oParentObject.w_PDQTAUM1*this.oParentObject.w_MOLTIP)
          else
            * --- UM associata al codice di ricerca
            this.oParentObject.w_PDQTAORD = iif(this.oParentObject.w_OPERA3="/", this.oParentObject.w_PDQTAUM1/this.oParentObject.w_MOLTI3, this.oParentObject.w_PDQTAUM1*this.oParentObject.w_MOLTI3)
          endif
        endif
      endif
    endif
    USE IN SELECT("Periodi")
  endproc


  proc Init(oParentObject,pScelta)
    this.pScelta=pScelta
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='PDA_TPER'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='CON_TRAD'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    if used('_Curs_GSACZAPD')
      use in _Curs_GSACZAPD
    endif
    if used('_Curs_PDA_TPER')
      use in _Curs_PDA_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pScelta"
endproc
