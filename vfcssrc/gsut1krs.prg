* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1krs                                                        *
*              Configurazioni RSS Viewer                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-26                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut1krs",oParentObject))

* --- Class definition
define class tgsut1krs as StdForm
  Top    = 60
  Left   = 87

  * --- Standard Properties
  Width  = 507
  Height = 339+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-30"
  HelpContextID=118965097
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  RSS_FEED_IDX = 0
  cPrg = "gsut1krs"
  cComment = "Configurazioni RSS Viewer"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODGRP = 0
  o_CODGRP = 0
  w_CODUTE = 0
  o_CODUTE = 0
  w_UTEGRP = space(1)
  o_UTEGRP = space(1)
  w_CODICE = 0
  o_CODICE = 0
  w_DESUTE = space(50)
  w_DESGRP = space(50)
  w_RSSFEED = space(15)
  w_RDFLGACT = space(1)
  w_CODZOOM = 0
  w_UTEGRPZOOM = space(1)
  w_RSDESCRI = space(254)
  w_SELEZI = space(1)
  o_SELEZI = space(1)

  * --- Children pointers
  GSUT_MRS = .NULL.
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSUT_MRS additive
    with this
      .Pages(1).addobject("oPag","tgsut1krsPag1","gsut1krs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("RSS Feed")
      .Pages(2).addobject("oPag","tgsut1krsPag2","gsut1krs",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ricerca")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODGRP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSUT_MRS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(2).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='RSS_FEED'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_MRS = CREATEOBJECT('stdDynamicChild',this,'GSUT_MRS',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSUT_MRS.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MRS)
      this.GSUT_MRS.DestroyChildrenChain()
      this.GSUT_MRS=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MRS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MRS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MRS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MRS.SetKey(;
            .w_CODICE,"RDCODICE";
            ,.w_UTEGRP,"RDUTEGRP";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MRS.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODICE,"RDCODICE";
             ,.w_UTEGRP,"RDUTEGRP";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MRS)
        i_f=.GSUT_MRS.BuildFilter()
        if !(i_f==.GSUT_MRS.cQueryFilter)
          i_fnidx=.GSUT_MRS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MRS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MRS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MRS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MRS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_MRS(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_MRS.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_MRS(i_ask)
    if this.GSUT_MRS.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (Configurazioni RSS Viewer)'))
      cp_BeginTrs()
      this.GSUT_MRS.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODGRP=0
      .w_CODUTE=0
      .w_UTEGRP=space(1)
      .w_CODICE=0
      .w_DESUTE=space(50)
      .w_DESGRP=space(50)
      .w_RSSFEED=space(15)
      .w_RDFLGACT=space(1)
      .w_CODZOOM=0
      .w_UTEGRPZOOM=space(1)
      .w_RSDESCRI=space(254)
      .w_SELEZI=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODGRP))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODUTE))
          .link_1_2('Full')
        endif
        .w_UTEGRP = 'U'
        .w_CODICE = IIF(.w_UTEGRP= 'U', .w_CODUTE, .w_CODGRP)
      .GSUT_MRS.NewDocument()
      .GSUT_MRS.ChangeRow('1',1,.w_CODICE,"RDCODICE",.w_UTEGRP,"RDUTEGRP")
      if not(.GSUT_MRS.bLoaded)
        .GSUT_MRS.SetKey(.w_CODICE,"RDCODICE",.w_UTEGRP,"RDUTEGRP")
      endif
        .DoRTCalc(5,7,.f.)
        if not(empty(.w_RSSFEED))
          .link_2_1('Full')
        endif
        .w_RDFLGACT = ' '
        .w_CODZOOM = .w_ZOOM.GetVar("RDCODICE")
        .w_UTEGRPZOOM = .w_ZOOM.GetVar("UTEGRP")
      .oPgFrm.Page2.oPag.ZOOM.Calculate()
          .DoRTCalc(11,11,.f.)
        .w_SELEZI = "D"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_MRS.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MRS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_UTEGRP<>.w_UTEGRP
          .link_1_1('Full')
        endif
        if .o_UTEGRP<>.w_UTEGRP
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_CODGRP<>.w_CODGRP.or. .o_CODUTE<>.w_CODUTE.or. .o_UTEGRP<>.w_UTEGRP
            .w_CODICE = IIF(.w_UTEGRP= 'U', .w_CODUTE, .w_CODGRP)
        endif
        .DoRTCalc(5,8,.t.)
            .w_CODZOOM = .w_ZOOM.GetVar("RDCODICE")
            .w_UTEGRPZOOM = .w_ZOOM.GetVar("UTEGRP")
        .oPgFrm.Page2.oPag.ZOOM.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_PDEGLSQSEZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CODICE<>.o_CODICE or .w_UTEGRP<>.o_UTEGRP
          .Save_GSUT_MRS(.t.)
          .GSUT_MRS.NewDocument()
          .GSUT_MRS.ChangeRow('1',1,.w_CODICE,"RDCODICE",.w_UTEGRP,"RDUTEGRP")
          if not(.GSUT_MRS.bLoaded)
            .GSUT_MRS.SetKey(.w_CODICE,"RDCODICE",.w_UTEGRP,"RDUTEGRP")
          endif
        endif
      endwith
      this.DoRTCalc(11,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOM.Calculate()
    endwith
  return

  proc Calculate_WXKEIURWUB()
    with this
          * --- Zoom Selected
          SelectRSSFeed(this;
             )
    endwith
  endproc
  proc Calculate_PDEGLSQSEZ()
    with this
          * --- Seleziona/Deseleziona
          SelFeed(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODGRP_1_1.enabled = this.oPgFrm.Page1.oPag.oCODGRP_1_1.mCond()
    this.oPgFrm.Page1.oPag.oCODUTE_1_2.enabled = this.oPgFrm.Page1.oPag.oCODUTE_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODGRP_1_1.visible=!this.oPgFrm.Page1.oPag.oCODGRP_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCODUTE_1_2.visible=!this.oPgFrm.Page1.oPag.oCODUTE_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_7.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESGRP_1_8.visible=!this.oPgFrm.Page1.oPag.oDESGRP_1_8.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOM selected")
          .Calculate_WXKEIURWUB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oCODGRP_1_1'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRP = NVL(_Link_.code,0)
      this.w_DESGRP = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRP = 0
      endif
      this.w_DESGRP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCODUTE_1_2'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESUTE = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RSSFEED
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_lTable = "RSS_FEED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2], .t., this.RSS_FEED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RSSFEED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_ARS',True,'RSS_FEED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RFRSSNAM like "+cp_ToStrODBC(trim(this.w_RSSFEED)+"%");

          i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RFRSSNAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RFRSSNAM',trim(this.w_RSSFEED))
          select RFRSSNAM,RFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RFRSSNAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RSSFEED)==trim(_Link_.RFRSSNAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RSSFEED) and !this.bDontReportError
            deferred_cp_zoom('RSS_FEED','*','RFRSSNAM',cp_AbsName(oSource.parent,'oRSSFEED_2_1'),i_cWhere,'GSUT_ARS',"RSS Feed",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RFRSSNAM="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFRSSNAM',oSource.xKey(1))
            select RFRSSNAM,RFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RSSFEED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RFRSSNAM="+cp_ToStrODBC(this.w_RSSFEED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFRSSNAM',this.w_RSSFEED)
            select RFRSSNAM,RFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RSSFEED = NVL(_Link_.RFRSSNAM,space(15))
      this.w_RSDESCRI = NVL(_Link_.RFDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_RSSFEED = space(15)
      endif
      this.w_RSDESCRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])+'\'+cp_ToStr(_Link_.RFRSSNAM,1)
      cp_ShowWarn(i_cKey,this.RSS_FEED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RSSFEED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODGRP_1_1.value==this.w_CODGRP)
      this.oPgFrm.Page1.oPag.oCODGRP_1_1.value=this.w_CODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_2.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_2.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEGRP_1_3.RadioValue()==this.w_UTEGRP)
      this.oPgFrm.Page1.oPag.oUTEGRP_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_7.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_7.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_1_8.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_1_8.value=this.w_DESGRP
    endif
    if not(this.oPgFrm.Page2.oPag.oRSSFEED_2_1.value==this.w_RSSFEED)
      this.oPgFrm.Page2.oPag.oRSSFEED_2_1.value=this.w_RSSFEED
    endif
    if not(this.oPgFrm.Page2.oPag.oRDFLGACT_2_2.RadioValue()==this.w_RDFLGACT)
      this.oPgFrm.Page2.oPag.oRDFLGACT_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRSDESCRI_2_8.value==this.w_RSDESCRI)
      this.oPgFrm.Page2.oPag.oRSDESCRI_2_8.value=this.w_RSDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSUT_MRS.CheckForm()
      if i_bres
        i_bres=  .GSUT_MRS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODGRP = this.w_CODGRP
    this.o_CODUTE = this.w_CODUTE
    this.o_UTEGRP = this.w_UTEGRP
    this.o_CODICE = this.w_CODICE
    this.o_SELEZI = this.w_SELEZI
    * --- GSUT_MRS : Depends On
    this.GSUT_MRS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut1krsPag1 as StdContainer
  Width  = 503
  height = 345
  stdWidth  = 503
  stdheight = 345
  resizeXpos=345
  resizeYpos=183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODGRP_1_1 as StdField with uid="KHURRUNHGW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODGRP", cQueryName = "CODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 28028890,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=64, Top=10, bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_CODGRP"

  func oCODGRP_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTEGRP = 'G')
    endwith
   endif
  endfunc

  func oCODGRP_1_1.mHide()
    with this.Parent.oContained
      return (.w_UTEGRP <> 'G')
    endwith
  endfunc

  func oCODGRP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oCODGRP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oCODUTE_1_2 as StdField with uid="LOHBBTPZLN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 209563610,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=64, Top=10, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_UTEGRP = 'U')
    endwith
   endif
  endfunc

  func oCODUTE_1_2.mHide()
    with this.Parent.oContained
      return (.w_UTEGRP <> 'U')
    endwith
  endfunc

  func oCODUTE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCODUTE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object oUTEGRP_1_3 as StdCombo with uid="RXACQWBTMD",rtseq=3,rtrep=.f.,left=142,top=39,width=178,height=21;
    , HelpContextID = 28023226;
    , cFormVar="w_UTEGRP",RowSource=""+"Utente,"+"Gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUTEGRP_1_3.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oUTEGRP_1_3.GetRadio()
    this.Parent.oContained.w_UTEGRP = this.RadioValue()
    return .t.
  endfunc

  func oUTEGRP_1_3.SetRadio()
    this.Parent.oContained.w_UTEGRP=trim(this.Parent.oContained.w_UTEGRP)
    this.value = ;
      iif(this.Parent.oContained.w_UTEGRP=='U',1,;
      iif(this.Parent.oContained.w_UTEGRP=='G',2,;
      0))
  endfunc


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="SYIGTZKFMV",left=3, top=65, width=497, height=227, bOnScreen=.t.;


  add object oDESUTE_1_7 as StdField with uid="HLGWIOZHJY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 209504714,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=141, Top=10, InputMask=replicate('X',50)

  func oDESUTE_1_7.mHide()
    with this.Parent.oContained
      return (.w_UTEGRP = 'G')
    endwith
  endfunc

  add object oDESGRP_1_8 as StdField with uid="KHVZHTFGMJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 27969994,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=141, Top=10, InputMask=replicate('X',50)

  func oDESGRP_1_8.mHide()
    with this.Parent.oContained
      return (.w_UTEGRP = 'U')
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="APIWEFNJEL",left=401, top=299, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 118965002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="ZJIUAZYUNY",left=452, top=299, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 118965002;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="NXJIYZDGNA",Visible=.t., Left=21, Top=12,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="AYAVPLOOVL",Visible=.t., Left=12, Top=39,;
    Alignment=1, Width=128, Height=17,;
    Caption="Utente/Gruppo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut1krsPag2 as StdContainer
  Width  = 503
  height = 345
  stdWidth  = 503
  stdheight = 345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRSSFEED_2_1 as StdField with uid="AIROXYJCSD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RSSFEED", cQueryName = "RSSFEED",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 42222870,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=54, Top=20, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="RSS_FEED", cZoomOnZoom="GSUT_ARS", oKey_1_1="RFRSSNAM", oKey_1_2="this.w_RSSFEED"

  func oRSSFEED_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRSSFEED_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRSSFEED_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RSS_FEED','*','RFRSSNAM',cp_AbsName(this.parent,'oRSSFEED_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_ARS',"RSS Feed",'',this.parent.oContained
  endproc
  proc oRSSFEED_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSUT_ARS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RFRSSNAM=this.parent.oContained.w_RSSFEED
     i_obj.ecpSave()
  endproc


  add object oRDFLGACT_2_2 as StdCombo with uid="KNLVNUXPAG",value=1,rtseq=8,rtrep=.f.,left=308,top=44,width=122,height=21;
    , HelpContextID = 245982826;
    , cFormVar="w_RDFLGACT",RowSource=""+"Tutti,"+"Si,"+"No,"+"No tassativo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRDFLGACT_2_2.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oRDFLGACT_2_2.GetRadio()
    this.Parent.oContained.w_RDFLGACT = this.RadioValue()
    return .t.
  endfunc

  func oRDFLGACT_2_2.SetRadio()
    this.Parent.oContained.w_RDFLGACT=trim(this.Parent.oContained.w_RDFLGACT)
    this.value = ;
      iif(this.Parent.oContained.w_RDFLGACT=='',1,;
      iif(this.Parent.oContained.w_RDFLGACT=='S',2,;
      iif(this.Parent.oContained.w_RDFLGACT=='N',3,;
      iif(this.Parent.oContained.w_RDFLGACT=='X',4,;
      0))))
  endfunc


  add object oBtn_2_3 as StdButton with uid="QYBPKEREZO",left=453, top=20, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 57957142;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_szoombox with uid="PGTJROFLYR",left=4, top=71, width=503,height=226,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cTable="RSS_DESK",cZoomOnZoom="",bRetriveAllRows=.f.,cZoomFile="GSUT1KRS",bOptions=.f.,;
    cEvent = "Init,Ricerca",;
    nPag=2;
    , HelpContextID = 59032550

  add object oRSDESCRI_2_8 as StdField with uid="AJTYPTUSJV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RSDESCRI", cQueryName = "RSDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 23221599,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=175, Top=20, InputMask=replicate('X',254)


  add object oBtn_2_11 as StdButton with uid="EOPYGOJECB",left=453, top=300, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eliminare l'associazione";
    , HelpContextID = 233618758;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSUT_BRS(this.Parent.oContained,"L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_12 as StdRadio with uid="BUGXSLLSWC",rtseq=12,rtrep=.f.,left=7, top=304, width=128,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 137181402
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 137181402
      this.Buttons(2).Top=15
      this.SetAll("Width",126)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_12.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oStr_2_7 as StdString with uid="QZZWMQCMUU",Visible=.t., Left=2, Top=23,;
    Alignment=1, Width=50, Height=18,;
    Caption="Feed:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="LTJJPXZGCT",Visible=.t., Left=205, Top=45,;
    Alignment=1, Width=101, Height=18,;
    Caption="VisibilitÓ:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1krs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut1krs
Proc SelectRSSFeed(pParent)
   pParent.w_UTEGRP=pParent.w_UTEGRPZOOM
   pParent.w_CODICE=pParent.w_CODZOOM
   If pParent.w_UTEGRP='G'
     pParent.w_CODGRP=pParent.w_CODZOOM
   Else
     pParent.w_CODUTE=pParent.w_CODZOOM
   Endif
   pParent.mCalc(.t.)
   pParent.oPgfrm.Activepage=1
EndProc

Proc SelFeed(pParent)
   UPDATE (pParent.w_ZOOM.cCursor) SET xChk=IIF(pParent.w_SELEZI='S', 1, 0)
EndProc
* --- Fine Area Manuale
