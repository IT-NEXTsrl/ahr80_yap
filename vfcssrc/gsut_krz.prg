* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krz                                                        *
*              Controllo zoom visuali                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_32]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-06                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_krz
* Occorre essere amministratore per svolgere il back Up
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
* Verifica l'eventuale presenza di maschere aperte
Local L_Msg
L_Msg=openForm(.t.)
IF Not Empty(L_Msg)
   ah_ErrorMsg("Chiudere tutte le maschere; maschera %1 aperta",'!', '', L_MSG)
   return
Endif
* --- Fine Area Manuale
return(createobject("tgsut_krz",oParentObject))

* --- Class definition
define class tgsut_krz as StdForm
  Top    = 28
  Left   = 37

  * --- Standard Properties
  Width  = 508
  Height = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-02"
  HelpContextID=152485015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_krz"
  cComment = "Controllo zoom visuali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATH = space(254)
  w_CHECKTABLE = .F.
  w_CHECKKEY = .F.
  w_CHECKNOKEY = .F.
  w_REPAIR = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krzPag1","gsut_krz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BRZ with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATH=space(254)
      .w_CHECKTABLE=.f.
      .w_CHECKKEY=.f.
      .w_CHECKNOKEY=.f.
      .w_REPAIR=.f.
        .w_PATH = alltrim(sys(5)+sys(2003))+'\STD\'
        .w_CHECKTABLE = .T.
        .w_CHECKKEY = .T.
        .w_CHECKNOKEY = .T.
        .w_REPAIR = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_1.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_1.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oCHECKTABLE_1_3.RadioValue()==this.w_CHECKTABLE)
      this.oPgFrm.Page1.oPag.oCHECKTABLE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHECKKEY_1_4.RadioValue()==this.w_CHECKKEY)
      this.oPgFrm.Page1.oPag.oCHECKKEY_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHECKNOKEY_1_5.RadioValue()==this.w_CHECKNOKEY)
      this.oPgFrm.Page1.oPag.oCHECKNOKEY_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREPAIR_1_6.RadioValue()==this.w_REPAIR)
      this.oPgFrm.Page1.oPag.oREPAIR_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(DIRECTORY(FULLPATH(ALLTRIM( .w_PATH ))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATH_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso indicato non � valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_krzPag1 as StdContainer
  Width  = 504
  height = 158
  stdWidth  = 504
  stdheight = 158
  resizeXpos=138
  resizeYpos=45
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATH_1_1 as StdField with uid="YQRRLJERIX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso indicato non � valido",;
    ToolTipText = "Directory da analizzare",;
    HelpContextID = 157565686,;
   bGlobalFont=.t.,;
    Height=21, Width=407, Left=66, Top=18, InputMask=replicate('X',254)

  func oPATH_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(FULLPATH(ALLTRIM( .w_PATH ))))
    endwith
    return bRes
  endfunc


  add object oBtn_1_2 as StdButton with uid="OYYRNGLHWC",left=478, top=18, width=20,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona directory da analizzare";
    , HelpContextID = 152686038;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      with this.Parent.oContained
        .w_PATH=Cp_getdir()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCHECKTABLE_1_3 as StdCheck with uid="FXFWWJSUYK",rtseq=2,rtrep=.f.,left=19, top=74, caption="Tabella assente",;
    ToolTipText = "Controlla la presenza della tabella",;
    HelpContextID = 233921752,;
    cFormVar="w_CHECKTABLE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHECKTABLE_1_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHECKTABLE_1_3.GetRadio()
    this.Parent.oContained.w_CHECKTABLE = this.RadioValue()
    return .t.
  endfunc

  func oCHECKTABLE_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHECKTABLE==.T.,1,;
      0)
  endfunc

  add object oCHECKKEY_1_4 as StdCheck with uid="OYTBRWUEEF",rtseq=3,rtrep=.f.,left=179, top=74, caption="Chiave errata",;
    ToolTipText = "Controlla eventuali chiavi errate",;
    HelpContextID = 116500097,;
    cFormVar="w_CHECKKEY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHECKKEY_1_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHECKKEY_1_4.GetRadio()
    this.Parent.oContained.w_CHECKKEY = this.RadioValue()
    return .t.
  endfunc

  func oCHECKKEY_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHECKKEY==.T.,1,;
      0)
  endfunc

  add object oCHECKNOKEY_1_5 as StdCheck with uid="PLOOTWNMZZ",rtseq=4,rtrep=.f.,left=324, top=74, caption="Chiave assente",;
    ToolTipText = "Controlla assenza chiavi",;
    HelpContextID = 202290881,;
    cFormVar="w_CHECKNOKEY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHECKNOKEY_1_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHECKNOKEY_1_5.GetRadio()
    this.Parent.oContained.w_CHECKNOKEY = this.RadioValue()
    return .t.
  endfunc

  func oCHECKNOKEY_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHECKNOKEY==.T.,1,;
      0)
  endfunc

  add object oREPAIR_1_6 as StdCheck with uid="FHURZJVXYS",rtseq=5,rtrep=.f.,left=24, top=111, caption="Ripara",;
    ToolTipText = "Ripara gli zoom errati",;
    HelpContextID = 267192086,;
    cFormVar="w_REPAIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oREPAIR_1_6.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oREPAIR_1_6.GetRadio()
    this.Parent.oContained.w_REPAIR = this.RadioValue()
    return .t.
  endfunc

  func oREPAIR_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_REPAIR==.T.,1,;
      0)
  endfunc


  add object oBtn_1_7 as StdButton with uid="AVEBAEPVRV",left=398, top=109, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per analizzare gli zoom";
    , HelpContextID = 152505574;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSUT_BRZ with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="YQNCWGTNXV",left=450, top=109, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 61665530;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="BBQJEEQAEK",Visible=.t., Left=10, Top=18,;
    Alignment=1, Width=53, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="INSOLIQNXG",Visible=.t., Left=10, Top=50,;
    Alignment=0, Width=135, Height=18,;
    Caption="Parametri di controllo"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="DLRCNIUKZJ",left=9, top=66, width=489,height=37
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krz','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
