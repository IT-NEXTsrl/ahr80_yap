* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bdf                                                        *
*              Calcola data fatturazione                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-14                                                      *
* Last revis.: 2006-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoMsg
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bdf",oParentObject,m.pNoMsg)
return(i_retval)

define class tgsor_bdf as StdBatch
  * --- Local variables
  pNoMsg = .f.
  w_DATEVA = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola data prossima fatturazione per ordini aperti
    this.w_DATEVA = NextTime(this.oParentObject.w_MVMC_PER, MAX(this.oParentObject.w_MVDATOAI, i_DATSYS))
    if this.pNoMsg Or (Not Empty(this.oParentObject.w_MVDATEVA) And this.oParentObject.w_MVDATEVA <> this.w_DATEVA And ah_YesNo("Aggiorno la data di prossima fatturazione?"))
      i_retcode = 'stop'
      i_retval = this.w_DATEVA
      return
    else
      i_retcode = 'stop'
      i_retval = this.oParentObject.w_MVDATEVA
      return
    endif
  endproc


  proc Init(oParentObject,pNoMsg)
    this.pNoMsg=pNoMsg
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoMsg"
endproc
