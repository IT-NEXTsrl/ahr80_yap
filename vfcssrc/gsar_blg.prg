* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_blg                                                        *
*              Manutenzione tabella cplangs                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-05                                                      *
* Last revis.: 2008-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_blg",oParentObject)
return(i_retval)

define class tgsar_blg as StdBatch
  * --- Local variables
  w_DESCRI = space(24)
  * --- WorkFile variables
  cplangs_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per la manutenzione della tabella di sistema cplangs che dettaglia le lingue su cui effettuare traduzione del dato
    this.w_DESCRI = LEFT(this.oParentObject.w_LUDESCRI, 24)
    if this.oParentObject.w_LUABTRDA="S"
      * --- Try
      local bErr_03A13930
      bErr_03A13930=bTrsErr
      this.Try_03A13930()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_03619C08
        bErr_03619C08=bTrsErr
        this.Try_03619C08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03619C08
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_03A13930
      * --- End
    else
      * --- Try
      local bErr_035EB5C8
      bErr_035EB5C8=bTrsErr
      this.Try_035EB5C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035EB5C8
      * --- End
    endif
  endproc
  proc Try_03A13930()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into cplangs
    i_nConn=i_TableProp[this.cplangs_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cplangs_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cplangs_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"code"+",name"+",datalanguage"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LUCODISO),'cplangs','code');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'cplangs','name');
      +","+cp_NullLink(cp_ToStrODBC("S"),'cplangs','datalanguage');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'code',this.oParentObject.w_LUCODISO,'name',this.w_DESCRI,'datalanguage',"S")
      insert into (i_cTable) (code,name,datalanguage &i_ccchkf. );
         values (;
           this.oParentObject.w_LUCODISO;
           ,this.w_DESCRI;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03619C08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into cplangs
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.cplangs_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cplangs_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.cplangs_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"datalanguage ="+cp_NullLink(cp_ToStrODBC("S"),'cplangs','datalanguage');
          +i_ccchkf ;
      +" where ";
          +"code = "+cp_ToStrODBC(this.oParentObject.w_LUCODISO);
             )
    else
      update (i_cTable) set;
          datalanguage = "S";
          &i_ccchkf. ;
       where;
          code = this.oParentObject.w_LUCODISO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_035EB5C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from cplangs
    i_nConn=i_TableProp[this.cplangs_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cplangs_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"code = "+cp_ToStrODBC(this.oParentObject.w_LUCODISO);
             )
    else
      delete from (i_cTable) where;
            code = this.oParentObject.w_LUCODISO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='cplangs'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
