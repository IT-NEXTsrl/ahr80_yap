* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bbe                                                        *
*              Stampa brogliaccio effetti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_276]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-20                                                      *
* Last revis.: 2012-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bbe",oParentObject)
return(i_retval)

define class tgste_bbe as StdBatch
  * --- Local variables
  w_OKUNION = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Brogliaccio Effetti (Dalla Maschera GSVE_SBE)
    * --- Costruisce il temporaneo con gli effetti da presentare (Par_Ape)
    do case
      case this.oParentObject.w_EFPRES="S" 
        * --- Costrusice il temporaneo Da presentare 
        *     Nel caso di selezione da presentare non ha senso l'ulteriore
        *     selezione da contabilizzare poiche insiemi non intersecanti
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_EFPRES="N" OR Not Empty(this.oParentObject.w_CODBAN) OR this.oParentObject.w_DINUMERO<>0 OR NOT EMPTY(this.oParentObject.w_DATDIS)
        * --- Se valorizzata una selezione specifica delle presentate non eseguo la Pag2
        *     per determinare le partite da presentare
        vq_exec("query\GSTE3BBE.VQR",this,"SELEPART")
      case this.oParentObject.w_EFPRES="T"
        this.w_OKUNION = .F.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if reccount("PAR_APE")=0
          * --- Eseguo solo le query per le presentate
          vq_exec("query\GSTE3BBE.VQR",this,"SELEPART")
        else
          vq_exec("query\GSTE3BBE.VQR",this,"PAR_CHI")
          if reccount("PAR_CHI")<>0
            this.w_OKUNION = .T.
          endif
        endif
        if this.w_OKUNION
          * --- Costruisco il risultato finale unendo il risultato dei due cursori se entrambi 
          *     contengono dei dati
           
 SELECT DATSCA, NUMPAR, FLDAVE , TOTIMP , SIMVAL , MODPAG , TIPCON, CODCON , BANAPP, PTSERIAL , ; 
 PTROWORD, CPROWNUM , VIDAVE, DINUMERO, DIDATDIS, DIFLDEFI, DIRIFCON, VADECTOT, VADECUNI, ; 
 DINUMDIS, ORIGINE, PTNUMEFF, CODVAL , BANNOS , PTFLSOSP, PTDATDOC, CAOVAL , PTFLINDI,SALDO, ; 
 PTALFDOC,DIBANRIF,RIFSOL,FLINSO,FLIMPE,IMPAVE,IMPDAR,BADESBAN,ANDESCRI FROM SELEPART WHERE NOT DELETED() UNION ; 
 SELECT DATSCA, NUMPAR, FLDAVE , TOTIMP , SIMVAL , MODPAG , TIPCON, CODCON , BANAPP, PTSERIAL , ; 
 PTROWORD, CPROWNUM , VIDAVE, DINUMERO, DIDATDIS, DIFLDEFI, DIRIFCON, VADECTOT, VADECUNI, ; 
 DINUMDIS, ORIGINE, PTNUMEFF, CODVAL , BANNOS , PTFLSOSP, PTDATDOC, CAOVAL , PTFLINDI,SALDO, ; 
 PTALFDOC,DIBANRIF,RIFSOL,FLINSO,FLIMPE,IMPAVE,IMPDAR,BADESBAN,ANDESCRI FROM PAR_CHI WHERE NOT DELETED() ; 
 INTO CURSOR SelePart NoFilter
        endif
        if Used("PAR_APE")
           
 Select PAR_APE 
 Use 
        endif
        if Used("PAR_CHI")
           
 Select PAR_CHI 
 Use 
        endif
      otherwise
        ah_ErrorMsg("Non esistono dati per le selezioni effettuate",,"")
    endcase
    if USED("SelePart")
      if RECCOUNT("SelePart")>0
        * --- Costruisce clausola di ordinamento in base al Radio sulla maschera
        *     S= banca di presentazione
        *     N = data scadenza
        *     R = Ragione Sociale
        *     C = Cliente/Fornitore
        do case
          case this.oParentObject.w_ORDINA="S"
            * --- Ordino per
            *     VASIMVAL - Simbolo valuta
            *     PTMODPAG - Pagamento
            *     DIBANRIF - Codice banca di presentazione
            *     PTDATSCA - data scadenza
            *     PTNUMEFF - Numero Effetto (Discendente)
            *     ORIGINE - Origine scadenza (Scadenze diverse, Prima Nota..)
            *     DIFLDEFI - Distinta stampata o meno in definitiva
            L_Ordine="VASIMVAL,PTMODPAG,DIBANRIF,PTDATSCA,PTNUMEFF,ORIGINE,DIFLDEFI,PTSERIAL"
          case this.oParentObject.w_ORDINA="N"
            * --- Ordino per
            *     VASIMVAL - Simbolo valuta
            *     PTMODPAG - Pagamento
            *     PTDATSCA - data scadenza
            *     PTNUMEFF - Numero Effetto (Discendente)
            *     ORIGINE - Origine scadenza (Scadenze diverse, Prima Nota..)
            *     DIFLDEFI - Distinta stampata o meno in definitiva
            L_Ordine="VASIMVAL,PTMODPAG,PTDATSCA,PTNUMEFF,ORIGINE,DIFLDEFI,PTSERIAL"
          case this.oParentObject.w_ORDINA="R"
            * --- Ordino per
            *     VASIMVAL - Simbolo valuta
            *     PTMODPAG - Pagamento
            *     TIPCON - Tipo
            *     ANDESCRI - Ragione Sociale
            *     PTDATSCA - data scadenza
            *     PTNUMEFF - Numero Effetto (Discendente)
            *     ORIGINE - Origine scadenza (Scadenze diverse, Prima Nota..)
            *     DIFLDEFI - Distinta stampata o meno in definitiva
            L_Ordine="VASIMVAL,PTMODPAG,TIPCON,ANDESCRI,PTDATSCA,PTNUMEFF,ORIGINE,DIFLDEFI,PTSERIAL"
          case this.oParentObject.w_ORDINA="C"
            * --- Ordino per
            *     VASIMVAL - Simbolo valuta
            *     PTMODPAG - Pagamento
            *     TIPCON - Tipo
            *     PTCODCON - Codice Cliente/Fornitore
            *     PTDATSCA - data scadenza
            *     PTNUMEFF - Numero Effetto (Discendente)
            *     ORIGINE - Origine scadenza (Scadenze diverse, Prima Nota..)
            *     DIFLDEFI - Distinta stampata o meno in definitiva
            L_Ordine="VASIMVAL,PTMODPAG,TIPCON,PTCODCON,PTDATSCA,PTNUMEFF,ORIGINE,DIFLDEFI,PTSERIAL"
        endcase
        * --- Elabora i Cursori e ritorna il Temporaneo per la Stampa
         
 SELECT CP_TODATE(DATSCA) AS PTDATSCA, NUMPAR AS PTNUMPAR, FLDAVE AS FLDAVE, TOTIMP AS TOTIMP, ; 
 SIMVAL AS VASIMVAL, MODPAG AS PTMODPAG, TIPCON, CODCON AS PTCODCON, BANAPP AS PTBANAPP, PTSERIAL AS PTSERIAL, ; 
 PTROWORD AS PTROWORD, CPROWNUM AS CPROWNUM, VIDAVE, DINUMERO, DIDATDIS, DIFLDEFI, DIRIFCON, VADECTOT, ; 
 VADECUNI, DINUMDIS, ORIGINE, PTNUMEFF, CODVAL AS VACDOVAL, BANNOS AS PTBANNOS, PTFLSOSP, PTDATDOC, CAOVAL AS PTCAOVAL, PTFLINDI,SALDO, ; 
 PTALFDOC,DIBANRIF,RIFSOL,FLINSO,FLIMPE,IMPAVE,IMPDAR,BADESBAN FROM SELEPART WHERE NOT DELETED() ; 
 ORDER BY &L_Ordine ; 
 INTO CURSOR __Tmp__ NoFilter
         
 Select SelePart 
 Use in SelePart
        * --- Passo le variabili al report
        L_CODCON=this.oParentObject.w_CODCON
        L_ORDINA=this.oParentObject.w_ORDINA
        L_SCAINI=this.oParentObject.w_SCAINI
        L_SCAFIN=this.oParentObject.w_SCAFIN
        L_DOCINI=this.oParentObject.w_DOCINI
        L_DOCFIN=this.oParentObject.w_DOCFIN
        L_REGINI=this.oParentObject.w_REGINI
        L_REGFIN=this.oParentObject.w_REGFIN
        L_NDOINI=this.oParentObject.w_NDOINI
        L_NDOFIN=this.oParentObject.w_NDOFIN
        L_ADOINI=this.oParentObject.w_ADOINI
        L_ADOFIN=this.oParentObject.w_ADOFIN
        L_FLDEFI=this.oParentObject.w_EFPRES
        L_CONTAB=this.oParentObject.w_EFCONT
        L_CODBAN=this.oParentObject.w_CODBAN
        L_PAGRI=this.oParentObject.w_PAGRI
        L_PAGBO=this.oParentObject.w_PAGBO
        L_PAGRB=this.oParentObject.w_PAGRB
        L_PAGRD=this.oParentObject.w_PAGRD
        L_PAGCA=this.oParentObject.w_PAGCA
        L_PAGMA=this.oParentObject.w_PAGMA
        L_SOSP=this.oParentObject.w_FLSOSP
        L_FLRAGG=this.oParentObject.w_FLRAGG
        * --- Lancio il report
        CP_CHPRN( ALLTRIM("QUERY\GSTE_SBE.FRX"), " ", this)
      else
        ah_ErrorMsg("Non esistono dati per le selezioni effettuate",,"")
      endif
    else
      ah_ErrorMsg("Non esistono dati per le selezioni effettuate",,"")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determino le partite da presentare (come da gestione distinte)
    *     (Da Presentare)
    GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.oParentObject.w_TIPCON , this.oParentObject.w_CODCON , "", this.oParentObject.w_VALUTA , "", this.oParentObject.w_PAGRB,this.oParentObject.w_PAGBO,this.oParentObject.w_PAGRD,this.oParentObject.w_PAGRI,this.oParentObject.w_PAGMA,this.oParentObject.w_PAGCA, "BE","N", cp_CharToDate("  -  -  ") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("query\GSTE_BBE.VQR",this,"PAR_APE")
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    if RECCOUNT("PAR_APE")>0
      Cur = WrCursor("PAR_APE")
      * --- Nel caso di Partite al Dopo Incasso devo eliminare la partita con PTFLIMPE='DI' qualor
      *     esista una partita di Saldo che chiude effettivamente la Partita in Primanota (esclusa una
      *     eventuale chiusura da indiretta effetti)
       
 Select DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(14),14)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) As CHIAVE; 
 From Par_Ape Where FLCRSA="S" And EMPTY(Nvl(Rifdis,SPACE(10))) AND NVL(FLIMPE,"  ")<>"DI"; 
 Into Cursor NOSALD NoFilter Order By CHIAVE
      if RECCOUNT("NOSALD") <>0
         
 Delete From PAR_APE Where Nvl(FLIMPE,"  ")="DI" and DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(14),14)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) ; 
 In (Select CHIAVE From NOSALD) 
      endif
      Use in NOSALD
      * --- Passo al batch le sole partite con pi� di una scadenza
      GSTE_BCP(this,"B","PAR_APE"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Applico sul risultato i filtri che non potevo fare prima (relativi alla parte aperta)
      *     Se Opzione Contabilizzate devo selezionare solo le Indirette Effetti
      VQ_EXEC("..\EXE\QUERY\GSTE1BBE",THIS,"TMPBANC")
       
 Select PAR_APE.*, BADESBAN FROM Par_Ape LEFT OUTER JOIN TMPBANC ON TMPBANC.BACODBAN=PAR_APE.BANAPP; 
 Where ; 
 ( ( ( TIPPAG IN (this.oParentObject.w_PAGRD , this.oParentObject.w_PAGBO , this.oParentObject.w_PAGRB , this.oParentObject.w_PAGCA , this.oParentObject.w_PAGRI , this.oParentObject.w_PAGMA ) And Not Empty(Nvl(TIPPAG,"")) ) ) ; 
 And ( Empty( this.oParentObject.w_NDOINI ) Or NVL(PTNUMDOC,0) >= this.oParentObject.w_NDOINI ) And ( Empty( this.oParentObject.w_NDOFIN ) Or Nvl(PTNUMDOC,0)<= this.oParentObject.w_NDOFIN ); 
 And ( Empty( this.oParentObject.w_ADOINI ) Or NVL(PTALFDOC, Space(10))>= this.oParentObject.w_ADOINI ) And ( Empty( this.oParentObject.w_ADOFIN ) Or NVL(PTALFDOC,Space(10)) <= this.oParentObject.w_ADOFIN ); 
 And ( Empty( this.oParentObject.w_CODBAN ) Or NVL(BANNOS,SPACE(10)) = this.oParentObject.w_CODBAN ) ) And ((this.oParentObject.w_EFCONT="S" And PTROWORD=-3) Or ; 
 (this.oParentObject.w_EFCONT="N" And PTROWORD<>-3) Or this.oParentObject.w_EFCONT="T") and ((Not Empty(Nvl(PTFLSOSP," ")) And this.oParentObject.w_FLSOSP="S") Or ; 
 (Empty(Nvl(PTFLSOSP," ")) And this.oParentObject.w_FLSOSP="N") Or this.oParentObject.w_FLSOSP="T" ) AND NVL(TOTIMP,0)<>0 Into Cursor SelePart NoFilter
      Use in Select("TMPBANC")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
