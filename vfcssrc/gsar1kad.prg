* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1kad                                                        *
*              Altri dati                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-04-23                                                      *
* Last revis.: 2014-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar1kad",oParentObject))

* --- Class definition
define class tgsar1kad as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 653
  Height = 189
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-23"
  HelpContextID=135755625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar1kad"
  cComment = "Altri dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ANDESCR3 = space(60)
  w_ANDESCR4 = space(60)
  w_ANINDIR3 = space(35)
  w_ANINDIR4 = space(35)
  w_ANLOCAL2 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar1kadPag1","gsar1kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANDESCR3_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANDESCR3=space(60)
      .w_ANDESCR4=space(60)
      .w_ANINDIR3=space(35)
      .w_ANINDIR4=space(35)
      .w_ANLOCAL2=space(30)
      .w_ANDESCR3=oParentObject.w_ANDESCR3
      .w_ANDESCR4=oParentObject.w_ANDESCR4
      .w_ANINDIR3=oParentObject.w_ANINDIR3
      .w_ANINDIR4=oParentObject.w_ANINDIR4
      .w_ANLOCAL2=oParentObject.w_ANLOCAL2
    endwith
    this.DoRTCalc(1,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oANDESCR3_1_1.enabled = i_bVal
      .Page1.oPag.oANDESCR4_1_2.enabled = i_bVal
      .Page1.oPag.oANINDIR3_1_3.enabled = i_bVal
      .Page1.oPag.oANINDIR4_1_4.enabled = i_bVal
      .Page1.oPag.oANLOCAL2_1_5.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ANDESCR3=.w_ANDESCR3
      .oParentObject.w_ANDESCR4=.w_ANDESCR4
      .oParentObject.w_ANINDIR3=.w_ANINDIR3
      .oParentObject.w_ANINDIR4=.w_ANINDIR4
      .oParentObject.w_ANLOCAL2=.w_ANLOCAL2
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANDESCR3_1_1.value==this.w_ANDESCR3)
      this.oPgFrm.Page1.oPag.oANDESCR3_1_1.value=this.w_ANDESCR3
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCR4_1_2.value==this.w_ANDESCR4)
      this.oPgFrm.Page1.oPag.oANDESCR4_1_2.value=this.w_ANDESCR4
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIR3_1_3.value==this.w_ANINDIR3)
      this.oPgFrm.Page1.oPag.oANINDIR3_1_3.value=this.w_ANINDIR3
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIR4_1_4.value==this.w_ANINDIR4)
      this.oPgFrm.Page1.oPag.oANINDIR4_1_4.value=this.w_ANINDIR4
    endif
    if not(this.oPgFrm.Page1.oPag.oANLOCAL2_1_5.value==this.w_ANLOCAL2)
      this.oPgFrm.Page1.oPag.oANLOCAL2_1_5.value=this.w_ANLOCAL2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar1kadPag1 as StdContainer
  Width  = 649
  height = 189
  stdWidth  = 649
  stdheight = 189
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANDESCR3_1_1 as StdField with uid="YZPPPFMFAY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANDESCR3", cQueryName = "ANDESCR3",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 6429497,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=206, Top=18, InputMask=replicate('X',60)

  add object oANDESCR4_1_2 as StdField with uid="LKNQDXNMPG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANDESCR4", cQueryName = "ANDESCR4",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 6429498,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=206, Top=44, InputMask=replicate('X',60)

  add object oANINDIR3_1_3 as StdField with uid="PPDRCVZGSB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ANINDIR3", cQueryName = "ANINDIR3",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi indirizzo",;
    HelpContextID = 91974457,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=206, Top=79, InputMask=replicate('X',35)

  add object oANINDIR4_1_4 as StdField with uid="GTJTRRUSDE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANINDIR4", cQueryName = "ANINDIR4",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi indirizzo",;
    HelpContextID = 91974458,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=206, Top=105, InputMask=replicate('X',35)

  add object oANLOCAL2_1_5 as StdField with uid="OYFEFIZUGX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANLOCAL2", cQueryName = "ANLOCAL2",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi localit�",;
    HelpContextID = 43214024,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=206, Top=140, InputMask=replicate('X',30)

  add object oStr_1_6 as StdString with uid="YKZMZDZAZK",Visible=.t., Left=158, Top=79,;
    Alignment=1, Width=47, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="VAWUJRAWEA",Visible=.t., Left=14, Top=17,;
    Alignment=1, Width=191, Height=18,;
    Caption="Dati aggiuntivi alla ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YMGDPDILCD",Visible=.t., Left=159, Top=139,;
    Alignment=1, Width=46, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar1kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
