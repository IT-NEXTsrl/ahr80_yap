* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_krs                                                        *
*              Ricostruzione saldi magazzino                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2016-08-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_krs",oParentObject))

* --- Class definition
define class tgsma_krs as StdForm
  Top    = 8
  Left   = 126

  * --- Standard Properties
  Width  = 592
  Height = 539
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-08-02"
  HelpContextID=116030313
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  FILDTREG_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsma_krs"
  cComment = "Ricostruzione saldi magazzino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLCAU = space(1)
  w_AGGLOTUBI = space(1)
  o_AGGLOTUBI = space(1)
  w_AGGCOMME = space(1)
  o_AGGCOMME = space(1)
  w_FLCP = space(1)
  w_AGCLOTUBI = space(1)
  w_GGINTERV = 0
  o_GGINTERV = 0
  w_NUMDOC = 0
  w_Msg = space(0)
  w_DATAFINE = ctod('  /  /  ')
  w_DATAINIZ = ctod('  /  /  ')
  w_INITIME = space(10)
  w_ENDTIME = space(10)
  w_FLDELSLD = space(1)
  w_CODARTIN = space(20)
  w_DESINI = space(40)
  w_CODARTFI = space(20)
  w_DESFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DATOBS1 = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- gsma_krs
  OldNumGG = 0
  OldNumDoc = 0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_krsPag1","gsma_krs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLCAU_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='FILDTREG'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSMA_BRS with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLCAU=space(1)
      .w_AGGLOTUBI=space(1)
      .w_AGGCOMME=space(1)
      .w_FLCP=space(1)
      .w_AGCLOTUBI=space(1)
      .w_GGINTERV=0
      .w_NUMDOC=0
      .w_Msg=space(0)
      .w_DATAFINE=ctod("  /  /  ")
      .w_DATAINIZ=ctod("  /  /  ")
      .w_INITIME=space(10)
      .w_ENDTIME=space(10)
      .w_FLDELSLD=space(1)
      .w_CODARTIN=space(20)
      .w_DESINI=space(40)
      .w_CODARTFI=space(20)
      .w_DESFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATOBS1=ctod("  /  /  ")
        .w_FLCAU = ' '
        .w_AGGLOTUBI = 'N'
        .w_AGGCOMME = 'N'
        .w_FLCP = 'S'
        .w_AGCLOTUBI = IIF(.w_AGGCOMME='S' and .w_AGGLOTUBI='S' and (g_PERLOT='S' Or g_PERUBI='S') , 'S' , IIF(.w_AGGCOMME<>'S' or .w_AGGLOTUBI<>'S', 'N', .w_AGCLOTUBI))
        .w_GGINTERV = LOOKTAB('FILDTREG','FLGIOINT','FLCODAZI',i_CODAZI)
        .w_NUMDOC = LOOKTAB('FILDTREG','FLNUMDOC','FLCODAZI',i_CODAZI)
          .DoRTCalc(8,8,.f.)
        .w_DATAFINE = IIF(.w_GGINTERV<>0,i_DATSYS,CTOD('  -  -    '))
        .w_DATAINIZ = iif(.w_GGINTERV<>0,i_DATSYS - .w_GGINTERV,ctod('  -  -    '))
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_msgformat("Questa funzione ricostruisce, leggendo tutti i movimenti di %0magazzino e documenti presenti in archivio, i saldi articoli."))
          .DoRTCalc(11,12,.f.)
        .w_FLDELSLD = ' '
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODARTIN))
          .link_1_22('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_CODARTFI))
          .link_1_24('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.DoRTCalc(19,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_krs
    this.OldNumGG = this.w_GGINTERV
    this.OldNumDoc = this.w_NUMDOC
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_AGGCOMME<>.w_AGGCOMME.or. .o_AGGLOTUBI<>.w_AGGLOTUBI
            .w_AGCLOTUBI = IIF(.w_AGGCOMME='S' and .w_AGGLOTUBI='S' and (g_PERLOT='S' Or g_PERUBI='S') , 'S' , IIF(.w_AGGCOMME<>'S' or .w_AGGLOTUBI<>'S', 'N', .w_AGCLOTUBI))
        endif
        .DoRTCalc(6,9,.t.)
        if .o_GGINTERV<>.w_GGINTERV
            .w_DATAINIZ = iif(.w_GGINTERV<>0,i_DATSYS - .w_GGINTERV,ctod('  -  -    '))
        endif
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_msgformat("Questa funzione ricostruisce, leggendo tutti i movimenti di %0magazzino e documenti presenti in archivio, i saldi articoli."))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(AH_msgformat("Questa funzione ricostruisce, leggendo tutti i movimenti di %0magazzino e documenti presenti in archivio, i saldi articoli."))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAGCLOTUBI_1_6.enabled = this.oPgFrm.Page1.oPag.oAGCLOTUBI_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAGGLOTUBI_1_2.visible=!this.oPgFrm.Page1.oPag.oAGGLOTUBI_1_2.mHide()
    this.oPgFrm.Page1.oPag.oFLCP_1_5.visible=!this.oPgFrm.Page1.oPag.oFLCP_1_5.mHide()
    this.oPgFrm.Page1.oPag.oGGINTERV_1_7.visible=!this.oPgFrm.Page1.oPag.oGGINTERV_1_7.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_8.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oINITIME_1_17.visible=!this.oPgFrm.Page1.oPag.oINITIME_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oENDTIME_1_19.visible=!this.oPgFrm.Page1.oPag.oENDTIME_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODARTIN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTIN_1_22'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODARTFI)) OR  (UPPER(.w_CODARTIN)<= UPPER(.w_CODARTFI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_CODARTIN = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTFI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTFI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTFI_1_24'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTFI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBS1 = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_CODARTIN) <= UPPER(.w_CODARTFI)) or (empty(.w_CODARTFI))) and (EMPTY(.w_DATOBS1) OR .w_DATOBS1>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_CODARTFI = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLCAU_1_1.RadioValue()==this.w_FLCAU)
      this.oPgFrm.Page1.oPag.oFLCAU_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGLOTUBI_1_2.RadioValue()==this.w_AGGLOTUBI)
      this.oPgFrm.Page1.oPag.oAGGLOTUBI_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGCOMME_1_3.RadioValue()==this.w_AGGCOMME)
      this.oPgFrm.Page1.oPag.oAGGCOMME_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCP_1_5.RadioValue()==this.w_FLCP)
      this.oPgFrm.Page1.oPag.oFLCP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCLOTUBI_1_6.RadioValue()==this.w_AGCLOTUBI)
      this.oPgFrm.Page1.oPag.oAGCLOTUBI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGGINTERV_1_7.value==this.w_GGINTERV)
      this.oPgFrm.Page1.oPag.oGGINTERV_1_7.value=this.w_GGINTERV
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_8.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_8.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_9.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_9.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oINITIME_1_17.value==this.w_INITIME)
      this.oPgFrm.Page1.oPag.oINITIME_1_17.value=this.w_INITIME
    endif
    if not(this.oPgFrm.Page1.oPag.oENDTIME_1_19.value==this.w_ENDTIME)
      this.oPgFrm.Page1.oPag.oENDTIME_1_19.value=this.w_ENDTIME
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDELSLD_1_21.RadioValue()==this.w_FLDELSLD)
      this.oPgFrm.Page1.oPag.oFLDELSLD_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTIN_1_22.value==this.w_CODARTIN)
      this.oPgFrm.Page1.oPag.oCODARTIN_1_22.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_23.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_23.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTFI_1_24.value==this.w_CODARTFI)
      this.oPgFrm.Page1.oPag.oCODARTFI_1_24.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_25.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_25.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_CODARTFI)) OR  (UPPER(.w_CODARTIN)<= UPPER(.w_CODARTFI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODARTIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTIN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_CODARTIN) <= UPPER(.w_CODARTFI)) or (empty(.w_CODARTFI))) and (EMPTY(.w_DATOBS1) OR .w_DATOBS1>.w_OBTEST))  and not(empty(.w_CODARTFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTFI_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AGGLOTUBI = this.w_AGGLOTUBI
    this.o_AGGCOMME = this.w_AGGCOMME
    this.o_GGINTERV = this.w_GGINTERV
    return

enddefine

* --- Define pages as container
define class tgsma_krsPag1 as StdContainer
  Width  = 588
  height = 539
  stdWidth  = 588
  stdheight = 539
  resizeXpos=220
  resizeYpos=273
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLCAU_1_1 as StdCheck with uid="QNDILALIIQ",rtseq=1,rtrep=.f.,left=24, top=59, caption="Verifica flag saldi",;
    ToolTipText = "Se Attivo: Verifica e corregge i Flag di Aggiornamento Magazzino dalla causale di Magazzino",;
    HelpContextID = 22346410,;
    cFormVar="w_FLCAU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCAU_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLCAU_1_1.GetRadio()
    this.Parent.oContained.w_FLCAU = this.RadioValue()
    return .t.
  endfunc

  func oFLCAU_1_1.SetRadio()
    this.Parent.oContained.w_FLCAU=trim(this.Parent.oContained.w_FLCAU)
    this.value = ;
      iif(this.Parent.oContained.w_FLCAU=='S',1,;
      0)
  endfunc

  add object oAGGLOTUBI_1_2 as StdCheck with uid="SNMOSNXKLX",rtseq=2,rtrep=.f.,left=24, top=81, caption="Aggiorna saldi lotti/ubicazioni",;
    ToolTipText = "Se attivo aggiorna i saldi lotti/ubicazioni",;
    HelpContextID = 39208152,;
    cFormVar="w_AGGLOTUBI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGLOTUBI_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGLOTUBI_1_2.GetRadio()
    this.Parent.oContained.w_AGGLOTUBI = this.RadioValue()
    return .t.
  endfunc

  func oAGGLOTUBI_1_2.SetRadio()
    this.Parent.oContained.w_AGGLOTUBI=trim(this.Parent.oContained.w_AGGLOTUBI)
    this.value = ;
      iif(this.Parent.oContained.w_AGGLOTUBI=='S',1,;
      0)
  endfunc

  func oAGGLOTUBI_1_2.mHide()
    with this.Parent.oContained
      return (g_PERUBI <> 'S' And g_PERLOT <>'S')
    endwith
  endfunc

  add object oAGGCOMME_1_3 as StdCheck with uid="KIRNGQYLHY",rtseq=3,rtrep=.f.,left=24, top=103, caption="Aggiorna saldi commessa",;
    ToolTipText = "Se attivo aggiorna i saldi commessa",;
    HelpContextID = 78823349,;
    cFormVar="w_AGGCOMME", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGCOMME_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGCOMME_1_3.GetRadio()
    this.Parent.oContained.w_AGGCOMME = this.RadioValue()
    return .t.
  endfunc

  func oAGGCOMME_1_3.SetRadio()
    this.Parent.oContained.w_AGGCOMME=trim(this.Parent.oContained.w_AGGCOMME)
    this.value = ;
      iif(this.Parent.oContained.w_AGGCOMME=='S',1,;
      0)
  endfunc

  add object oFLCP_1_5 as StdCheck with uid="SGXBAXSXRQ",rtseq=4,rtrep=.f.,left=24, top=125, caption="Aggiorna U.costo/prz",;
    ToolTipText = "Se attivo: aggiorna anche i dati relativi all'ultimo costo di acquisto e all'ultimo prezzo di vendita",;
    HelpContextID = 110492330,;
    cFormVar="w_FLCP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCP_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLCP_1_5.GetRadio()
    this.Parent.oContained.w_FLCP = this.RadioValue()
    return .t.
  endfunc

  func oFLCP_1_5.SetRadio()
    this.Parent.oContained.w_FLCP=trim(this.Parent.oContained.w_FLCP)
    this.value = ;
      iif(this.Parent.oContained.w_FLCP=='S',1,;
      0)
  endfunc

  func oFLCP_1_5.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oAGCLOTUBI_1_6 as StdCheck with uid="XPCODCUHNN",rtseq=5,rtrep=.f.,left=24, top=147, caption="Aggiorna saldi commessa/lotti/ubicazioni",;
    ToolTipText = "Se attivo aggiorna i saldi commessa/lotti/ubicazion",;
    HelpContextID = 39191768,;
    cFormVar="w_AGCLOTUBI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCLOTUBI_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCLOTUBI_1_6.GetRadio()
    this.Parent.oContained.w_AGCLOTUBI = this.RadioValue()
    return .t.
  endfunc

  func oAGCLOTUBI_1_6.SetRadio()
    this.Parent.oContained.w_AGCLOTUBI=trim(this.Parent.oContained.w_AGCLOTUBI)
    this.value = ;
      iif(this.Parent.oContained.w_AGCLOTUBI=='S',1,;
      0)
  endfunc

  func oAGCLOTUBI_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGCOMME='S' and  .w_AGGLOTUBI='S')
    endwith
   endif
  endfunc

  add object oGGINTERV_1_7 as StdField with uid="OAPMWSJMLK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GGINTERV", cQueryName = "GGINTERV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo di date documenti da considerare nella ricostruzione saldi per aggiornare l'ultimo costo  di acquisto/vendita",;
    HelpContextID = 61366460,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=533, Top=73, cSayPict="'999'", cGetPict="'999'"

  func oGGINTERV_1_7.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oNUMDOC_1_8 as StdField with uid="EFFDRRHHBK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo di  numeri documenti da considerare nella ricostruzione saldi per aggiornare l'ultimo costo  di acquisto/vendita",;
    HelpContextID = 246501674,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=533, Top=99, cSayPict="'999'", cGetPict="'999'"

  func oNUMDOC_1_8.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oMsg_1_9 as StdMemo with uid="ATBBCHAJSX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 115577658,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=234, Width=555, Left=20, Top=247, tabstop = .f., readonly = .t.


  add object oBtn_1_10 as StdButton with uid="NZCFVFIQLY",left=470, top=486, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'aggiornamento dei saldi di magazzino";
    , HelpContextID = 164842566;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        do GSMA_BRS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="VSNKBXIUPH",left=523, top=486, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108712890;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oINITIME_1_17 as StdField with uid="DGJQWNXEOK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_INITIME", cQueryName = "INITIME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 184444806,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=81, Top=485, InputMask=replicate('X',10)

  func oINITIME_1_17.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oENDTIME_1_19 as StdField with uid="ZUZZQGUCKO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ENDTIME", cQueryName = "ENDTIME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 184424262,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=81, Top=508, InputMask=replicate('X',10)

  func oENDTIME_1_19.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc


  add object oObj_1_20 as cp_calclbl with uid="BROTJPJHQP",left=11, top=22, width=565,height=37,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 61967334

  add object oFLDELSLD_1_21 as StdCheck with uid="KCACZZHYMJ",rtseq=13,rtrep=.f.,left=24, top=169, caption="Pulisci tabella saldi",;
    ToolTipText = "Se Attivo: svuota le tabelle saldi prima della ricostruzione",;
    HelpContextID = 249621094,;
    cFormVar="w_FLDELSLD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDELSLD_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDELSLD_1_21.GetRadio()
    this.Parent.oContained.w_FLDELSLD = this.RadioValue()
    return .t.
  endfunc

  func oFLDELSLD_1_21.SetRadio()
    this.Parent.oContained.w_FLDELSLD=trim(this.Parent.oContained.w_FLDELSLD)
    this.value = ;
      iif(this.Parent.oContained.w_FLDELSLD=='S',1,;
      0)
  endfunc

  add object oCODARTIN_1_22 as StdField with uid="TFOXAFTDJV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 226813836,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=116, Top=195, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTIN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODARTIN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARTIN
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_23 as StdField with uid="CHMQSNFQMU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 146538954,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=283, Top=195, InputMask=replicate('X',40)

  add object oCODARTFI_1_24 as StdField with uid="PFSBOHPKDW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 41621615,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=116, Top=219, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTFI_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODARTFI_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARTFI
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_25 as StdField with uid="TPNLPGVTQY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68092362,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=283, Top=219, InputMask=replicate('X',40)

  add object oStr_1_4 as StdString with uid="ZIPQSGHIVF",Visible=.t., Left=11, Top=4,;
    Alignment=0, Width=324, Height=15,;
    Caption="ATTENZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="NCSUACCHYK",Visible=.t., Left=422, Top=77,;
    Alignment=1, Width=108, Height=18,;
    Caption="Giorni intervallo:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="VJPUBJMZHM",Visible=.t., Left=469, Top=103,;
    Alignment=1, Width=61, Height=18,;
    Caption="Num. doc:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="UURLTSQNTO",Visible=.t., Left=10, Top=488,;
    Alignment=1, Width=68, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="BRMGQORKFH",Visible=.t., Left=24, Top=511,;
    Alignment=1, Width=54, Height=18,;
    Caption="Ora fine:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" or .w_FLCP#'S')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="FNODJPRENP",Visible=.t., Left=8, Top=195,;
    Alignment=1, Width=106, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="OXEGJSQCOM",Visible=.t., Left=8, Top=219,;
    Alignment=1, Width=106, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_krs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
