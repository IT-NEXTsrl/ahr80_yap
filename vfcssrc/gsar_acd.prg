* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_acd                                                        *
*              Modello comunicazione annuale dati IVA 2003/2004                *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_299]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-30                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_acd
L_descri5='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore o inabilitato o interdetto'
L_descri6='2) Socio amministratore'
L_descri7='3) Curatore fallimentare'
L_descri8='4) Commissario liquidatore'
L_descri9='5) Commissario giudiziale'
L_descri10='6) Rappr. fiscale di soggetto non residente'
L_descri11='7) Erede del contribuente'
L_descri12='8) Liquidatore'
L_descri13='9) Societ� beneficiaria o incorporante'
L_descri3='9) Soggetti per operazioni straordinarie'
L_comb1='Persona fisica'
L_comb2='Altri Soggetti'
L_combo1='01: Soggetti che utilizzano il canale Internet'
L_combo2a='02: Soggetti che utilizzano il canale Entratel'
L_combo2='03: C.A.F. dipendenti e pensionati'
L_combo3='05: C.A.F: imprese'
L_combo4='09: Art 3 comma 2'
L_combo5='10: Altri intermediari'
* --- Fine Area Manuale
return(createobject("tgsar_acd"))

* --- Class definition
define class tgsar_acd as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 698
  Height = 315+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=109672297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=72

  * --- Constant Properties
  MOD_COAN_IDX = 0
  AZIENDA_IDX = 0
  ATTIDETT_IDX = 0
  ESERCIZI_IDX = 0
  TITOLARI_IDX = 0
  cFile = "MOD_COAN"
  cKeySelect = "AIANNIMP,AIDATINV"
  cKeyWhere  = "AIANNIMP=this.w_AIANNIMP and AIDATINV=this.w_AIDATINV"
  cKeyWhereODBC = '"AIANNIMP="+cp_ToStrODBC(this.w_AIANNIMP)';
      +'+" and AIDATINV="+cp_ToStrODBC(this.w_AIDATINV,"D")';

  cKeyWhereODBCqualified = '"MOD_COAN.AIANNIMP="+cp_ToStrODBC(this.w_AIANNIMP)';
      +'+" and MOD_COAN.AIDATINV="+cp_ToStrODBC(this.w_AIDATINV,"D")';

  cPrg = "gsar_acd"
  cComment = "Modello comunicazione annuale dati IVA 2003/2004"
  icon = "anag.ico"
  cAutoZoom = 'GSAR_2004'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_AZCODCAR = space(2)
  w_AZPIVAZI = space(12)
  w_AZIVACOF = space(16)
  w_AZPERAZI = space(1)
  w_AZCODFIS = space(16)
  w_AZCODNAZ = space(3)
  w_AIANNIMP = 0
  w_AIDATINV = ctod('  /  /  ')
  w_CODFIS = space(16)
  w_AIPARIVA = space(11)
  w_AICODATT = space(5)
  w_AITIPFOR = space(2)
  o_AITIPFOR = space(2)
  w_AICONSEP = space(1)
  w_RAGSOC = space(40)
  w_AISOCGRU = space(1)
  w_AIEVEECC = space(1)
  w_EDITADICH = space(1)
  o_EDITADICH = space(1)
  w_AICODFIS = space(16)
  o_AICODFIS = space(16)
  w_AICODCAR = space(1)
  w_AICODIVA = space(11)
  w_AIMANCOR = space(1)
  w_AIINOLTRO = space(1)
  w_NUMREG = 0
  w_CODAZI1 = space(5)
  w_COGTIT = space(25)
  o_COGTIT = space(25)
  w_NOMTIT = space(25)
  o_NOMTIT = space(25)
  w_AITOTATT = 0
  w_AITA_NIM = 0
  w_AITA_ESE = 0
  w_AITA_CIN = 0
  w_AITOTPAS = 0
  w_AITP_NIM = 0
  w_AITP_ESE = 0
  w_AITP_CIN = 0
  w_AIIMPORO = 0
  w_AIIVAORO = 0
  w_AIIVAESI = 0
  o_AIIVAESI = 0
  w_AIIVADET = 0
  o_AIIVADET = 0
  w_AIIVADOV = 0
  w_AIIVACRE = 0
  w_AIFIRMAC = space(1)
  w_AICOFINT = space(16)
  o_AICOFINT = space(16)
  w_AIALBCAF = space(5)
  w_AIINVCON = space(1)
  o_AIINVCON = space(1)
  w_AIINVSOG = space(1)
  o_AIINVSOG = space(1)
  w_AIDATIMP = ctod('  /  /  ')
  w_AIFIRMA = space(1)
  w_AIPERAZI = space(1)
  o_AIPERAZI = space(1)
  w_AICOGNOM = space(24)
  w_AINOME = space(20)
  w_AISESSO = space(1)
  w_AICOMUNE = space(40)
  w_AIDATNAS = ctod('  /  /  ')
  w_AISIGLA = space(2)
  w_AIRESCOM = space(40)
  w_AIRESSIG = space(2)
  w_AIINDIRI = space(35)
  w_AI___CAP = space(8)
  w_AIDENOMI = space(60)
  w_AISECOMU = space(40)
  w_AISESIGL = space(2)
  w_AISEIND2 = space(35)
  w_AISE_CAP = space(8)
  w_AISERCOM = space(40)
  w_AISERSIG = space(2)
  w_AISERIND = space(35)
  w_AISERCAP = space(8)
  w_NOMEFILEFIS = space(20)
  w_NOMEFILE = space(200)
  w_GSAR_BCD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_COAN','gsar_acd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_acdPag1","gsar_acd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sez. I")
      .Pages(1).HelpContextID = 190016730
      .Pages(2).addobject("oPag","tgsar_acdPag2","gsar_acd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Sez. II")
      .Pages(2).HelpContextID = 190016730
      .Pages(3).addobject("oPag","tgsar_acdPag3","gsar_acd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Sez. III")
      .Pages(3).HelpContextID = 190016657
      .Pages(4).addobject("oPag","tgsar_acdPag4","gsar_acd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Inol.telem.")
      .Pages(4).HelpContextID = 13451074
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAIANNIMP_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_GSAR_BCD = this.oPgFrm.Pages(1).oPag.GSAR_BCD
      DoDefault()
    proc Destroy()
      this.w_GSAR_BCD = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ATTIDETT'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='TITOLARI'
    this.cWorkTables[5]='MOD_COAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_COAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_COAN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AIANNIMP = NVL(AIANNIMP,0)
      .w_AIDATINV = NVL(AIDATINV,ctod("  /  /  "))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsar_acd
    this.w_AIDATINV=iif(empty(this.w_AIDATINV), i_datsys, this.w_AIDATINV)
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_COAN where AIANNIMP=KeySet.AIANNIMP
    *                            and AIDATINV=KeySet.AIDATINV
    *
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_COAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_COAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_COAN '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CODESE = g_CODESE
        .w_INIESE = ctod("  /  /  ")
        .w_AZCODCAR = space(2)
        .w_AZPIVAZI = space(12)
        .w_AZIVACOF = space(16)
        .w_AZPERAZI = space(1)
        .w_AZCODFIS = space(16)
        .w_AZCODNAZ = space(3)
        .w_RAGSOC = g_RAGAZI
        .w_NUMREG = 0
        .w_CODAZI1 = i_CODAZI
        .w_NOMEFILEFIS = 'COMANNIVA'
          .link_1_1('Load')
          .link_1_2('Load')
        .w_AIANNIMP = NVL(AIANNIMP,0)
        .w_AIDATINV = NVL(cp_ToDate(AIDATINV),ctod("  /  /  "))
        .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        .w_AIPARIVA = NVL(AIPARIVA,space(11))
        .w_AICODATT = NVL(AICODATT,space(5))
        .w_AITIPFOR = NVL(AITIPFOR,space(2))
        .w_AICONSEP = NVL(AICONSEP,space(1))
        .oPgFrm.Page1.oPag.GSAR_BCD.Calculate()
        .w_AISOCGRU = NVL(AISOCGRU,space(1))
        .w_AIEVEECC = NVL(AIEVEECC,space(1))
        .w_EDITADICH = NVL(EDITADICH,space(1))
        .w_AICODFIS = NVL(AICODFIS,space(16))
        .w_AICODCAR = NVL(AICODCAR,space(1))
        .w_AICODIVA = NVL(AICODIVA,space(11))
        .w_AIMANCOR = NVL(AIMANCOR,space(1))
        .w_AIINOLTRO = NVL(AIINOLTRO,space(1))
          .link_1_46('Load')
        .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
        .w_AITOTATT = NVL(AITOTATT,0)
        .w_AITA_NIM = NVL(AITA_NIM,0)
        .w_AITA_ESE = NVL(AITA_ESE,0)
        .w_AITA_CIN = NVL(AITA_CIN,0)
        .w_AITOTPAS = NVL(AITOTPAS,0)
        .w_AITP_NIM = NVL(AITP_NIM,0)
        .w_AITP_ESE = NVL(AITP_ESE,0)
        .w_AITP_CIN = NVL(AITP_CIN,0)
        .w_AIIMPORO = NVL(AIIMPORO,0)
        .w_AIIVAORO = NVL(AIIVAORO,0)
        .w_AIIVAESI = NVL(AIIVAESI,0)
        .w_AIIVADET = NVL(AIIVADET,0)
        .w_AIIVADOV = NVL(AIIVADOV,0)
        .w_AIIVACRE = NVL(AIIVACRE,0)
        .w_AIFIRMAC = NVL(AIFIRMAC,space(1))
        .w_AICOFINT = NVL(AICOFINT,space(16))
        .w_AIALBCAF = NVL(AIALBCAF,space(5))
        .w_AIINVCON = NVL(AIINVCON,space(1))
        .w_AIINVSOG = NVL(AIINVSOG,space(1))
        .w_AIDATIMP = NVL(cp_ToDate(AIDATIMP),ctod("  /  /  "))
        .w_AIFIRMA = NVL(AIFIRMA,space(1))
        .w_AIPERAZI = NVL(AIPERAZI,space(1))
        .oPgFrm.Page4.oPag.oObj_4_3.Calculate()
        .w_AICOGNOM = NVL(AICOGNOM,space(24))
        .w_AINOME = NVL(AINOME,space(20))
        .w_AISESSO = NVL(AISESSO,space(1))
        .w_AICOMUNE = NVL(AICOMUNE,space(40))
        .w_AIDATNAS = NVL(cp_ToDate(AIDATNAS),ctod("  /  /  "))
        .w_AISIGLA = NVL(AISIGLA,space(2))
        .w_AIRESCOM = NVL(AIRESCOM,space(40))
        .w_AIRESSIG = NVL(AIRESSIG,space(2))
        .w_AIINDIRI = NVL(AIINDIRI,space(35))
        .w_AI___CAP = NVL(AI___CAP,space(8))
        .w_AIDENOMI = NVL(AIDENOMI,space(60))
        .w_AISECOMU = NVL(AISECOMU,space(40))
        .w_AISESIGL = NVL(AISESIGL,space(2))
        .w_AISEIND2 = NVL(AISEIND2,space(35))
        .w_AISE_CAP = NVL(AISE_CAP,space(8))
        .w_AISERCOM = NVL(AISERCOM,space(40))
        .w_AISERSIG = NVL(AISERSIG,space(2))
        .w_AISERIND = NVL(AISERIND,space(35))
        .w_AISERCAP = NVL(AISERCAP,space(8))
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .w_NOMEFILE = NVL(NOMEFILE,space(200))
        cp_LoadRecExtFlds(this,'MOD_COAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_CODESE = space(4)
      .w_INIESE = ctod("  /  /  ")
      .w_AZCODCAR = space(2)
      .w_AZPIVAZI = space(12)
      .w_AZIVACOF = space(16)
      .w_AZPERAZI = space(1)
      .w_AZCODFIS = space(16)
      .w_AZCODNAZ = space(3)
      .w_AIANNIMP = 0
      .w_AIDATINV = ctod("  /  /  ")
      .w_CODFIS = space(16)
      .w_AIPARIVA = space(11)
      .w_AICODATT = space(5)
      .w_AITIPFOR = space(2)
      .w_AICONSEP = space(1)
      .w_RAGSOC = space(40)
      .w_AISOCGRU = space(1)
      .w_AIEVEECC = space(1)
      .w_EDITADICH = space(1)
      .w_AICODFIS = space(16)
      .w_AICODCAR = space(1)
      .w_AICODIVA = space(11)
      .w_AIMANCOR = space(1)
      .w_AIINOLTRO = space(1)
      .w_NUMREG = 0
      .w_CODAZI1 = space(5)
      .w_COGTIT = space(25)
      .w_NOMTIT = space(25)
      .w_AITOTATT = 0
      .w_AITA_NIM = 0
      .w_AITA_ESE = 0
      .w_AITA_CIN = 0
      .w_AITOTPAS = 0
      .w_AITP_NIM = 0
      .w_AITP_ESE = 0
      .w_AITP_CIN = 0
      .w_AIIMPORO = 0
      .w_AIIVAORO = 0
      .w_AIIVAESI = 0
      .w_AIIVADET = 0
      .w_AIIVADOV = 0
      .w_AIIVACRE = 0
      .w_AIFIRMAC = space(1)
      .w_AICOFINT = space(16)
      .w_AIALBCAF = space(5)
      .w_AIINVCON = space(1)
      .w_AIINVSOG = space(1)
      .w_AIDATIMP = ctod("  /  /  ")
      .w_AIFIRMA = space(1)
      .w_AIPERAZI = space(1)
      .w_AICOGNOM = space(24)
      .w_AINOME = space(20)
      .w_AISESSO = space(1)
      .w_AICOMUNE = space(40)
      .w_AIDATNAS = ctod("  /  /  ")
      .w_AISIGLA = space(2)
      .w_AIRESCOM = space(40)
      .w_AIRESSIG = space(2)
      .w_AIINDIRI = space(35)
      .w_AI___CAP = space(8)
      .w_AIDENOMI = space(60)
      .w_AISECOMU = space(40)
      .w_AISESIGL = space(2)
      .w_AISEIND2 = space(35)
      .w_AISE_CAP = space(8)
      .w_AISERCOM = space(40)
      .w_AISERSIG = space(2)
      .w_AISERIND = space(35)
      .w_AISERCAP = space(8)
      .w_NOMEFILEFIS = space(20)
      .w_NOMEFILE = space(200)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CODESE))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,9,.f.)
        .w_AIANNIMP = IIF(.cFunction='Load',YEAR(.w_INIESE)-1,.w_AIANNIMP)
        .w_AIDATINV = IIF(.cFunction='Load',i_DATSYS,.w_AIDATINV)
        .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        .w_AIPARIVA = left(.w_AZPIVAZI,11)
        .w_AICODATT = IIF(EMPTY(.w_AICODATT),IIF(g_ATTIVI='S', space(5), g_CATAZI),.w_AICODATT)
        .w_AITIPFOR = '01'
        .w_AICONSEP = '0'
        .w_RAGSOC = g_RAGAZI
        .oPgFrm.Page1.oPag.GSAR_BCD.Calculate()
          .DoRTCalc(18,18,.f.)
        .w_AIEVEECC = '0'
        .w_EDITADICH = IIF(.w_AZPERAZI='S', 'N', 'S')
        .w_AICODFIS = IIF(.w_EDITADICH='S' and .cFunction<>'Query',.w_AZIVACOF,space(16))
        .w_AICODCAR = .w_AZCODCAR
        .w_AICODIVA = space(11)
        .w_AIMANCOR = '0'
          .DoRTCalc(25,26,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_46('Full')
          endif
        .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
          .DoRTCalc(30,41,.f.)
        .w_AIIVADOV = IIF(.w_AIIVAESI>=.w_AIIVADET,.w_AIIVAESI-.w_AIIVADET,0)
        .w_AIIVACRE = IIF(.w_AIIVAESI<.w_AIIVADET,.w_AIIVADET-.w_AIIVAESI,0)
        .w_AIFIRMAC = '0'
        .w_AICOFINT = iif(.w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02',.w_AICOFINT,space(16))
        .w_AIALBCAF = IIF(empty(.w_AICOFINT),space(5),.w_AIALBCAF)
        .w_AIINVCON = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVSOG='0','1','0')
        .w_AIINVSOG = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVCON='0','1','0')
        .w_AIDATIMP = IIF(empty(.w_AICOFINT),cp_CharToDate('  -  -    '),.w_AIDATIMP)
        .w_AIFIRMA = IIF(NOT EMPTY(.w_AICOFINT),.w_AIFIRMA,'0')
        .w_AIPERAZI = 'S'
        .oPgFrm.Page4.oPag.oObj_4_3.Calculate()
        .w_AICOGNOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOGNOM,' ')
        .w_AINOME = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AINOME,' ')
        .w_AISESSO = 'M'
        .w_AICOMUNE = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOMUNE,' ')
        .w_AIDATNAS = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIDATNAS,cp_CharToDate('  -  -  '))
        .w_AISIGLA = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AISIGLA,' ')
        .w_AIRESCOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESCOM,' ')
        .w_AIRESSIG = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESSIG,' ')
        .w_AIINDIRI = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIINDIRI,' ')
        .w_AI___CAP = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AI___CAP,' ')
        .w_AIDENOMI = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AIDENOMI,' ')
        .w_AISECOMU = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISECOMU,' ')
        .w_AISESIGL = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISESIGL,' ')
        .w_AISEIND2 = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISEIND2,' ')
        .w_AISE_CAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISE_CAP,' ')
        .w_AISERCOM = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCOM,' ')
        .w_AISERSIG = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERSIG,' ')
        .w_AISERIND = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERIND,' ')
        .w_AISERCAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCAP,' ')
        .w_NOMEFILEFIS = 'COMANNIVA'
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .w_NOMEFILE = left(sys(5)+sys(2003)+'\'+.w_NOMEFILEFIS+space(200),200)
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_COAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAIANNIMP_1_10.enabled = i_bVal
      .Page1.oPag.oAIDATINV_1_11.enabled = i_bVal
      .Page1.oPag.oCODFIS_1_12.enabled = i_bVal
      .Page1.oPag.oAIPARIVA_1_13.enabled = i_bVal
      .Page1.oPag.oAICODATT_1_14.enabled = i_bVal
      .Page1.oPag.oAITIPFOR_1_15.enabled = i_bVal
      .Page1.oPag.oAICONSEP_1_16.enabled = i_bVal
      .Page1.oPag.oAISOCGRU_1_29.enabled = i_bVal
      .Page1.oPag.oAIEVEECC_1_30.enabled = i_bVal
      .Page1.oPag.oEDITADICH_1_32.enabled = i_bVal
      .Page1.oPag.oAICODFIS_1_33.enabled = i_bVal
      .Page1.oPag.oAICODCAR_1_36.enabled = i_bVal
      .Page1.oPag.oAICODIVA_1_38.enabled = i_bVal
      .Page1.oPag.oAIMANCOR_1_39.enabled = i_bVal
      .Page2.oPag.oAITOTATT_2_9.enabled = i_bVal
      .Page2.oPag.oAITA_NIM_2_12.enabled = i_bVal
      .Page2.oPag.oAITA_ESE_2_14.enabled = i_bVal
      .Page2.oPag.oAITA_CIN_2_16.enabled = i_bVal
      .Page2.oPag.oAITOTPAS_2_27.enabled = i_bVal
      .Page2.oPag.oAITP_NIM_2_30.enabled = i_bVal
      .Page2.oPag.oAITP_ESE_2_32.enabled = i_bVal
      .Page2.oPag.oAITP_CIN_2_34.enabled = i_bVal
      .Page2.oPag.oAIIMPORO_2_36.enabled = i_bVal
      .Page2.oPag.oAIIVAORO_2_37.enabled = i_bVal
      .Page3.oPag.oAIIVAESI_3_8.enabled = i_bVal
      .Page3.oPag.oAIIVADET_3_12.enabled = i_bVal
      .Page3.oPag.oAIFIRMAC_3_19.enabled = i_bVal
      .Page3.oPag.oAICOFINT_3_21.enabled = i_bVal
      .Page3.oPag.oAIALBCAF_3_25.enabled = i_bVal
      .Page3.oPag.oAIINVCON_3_30.enabled = i_bVal
      .Page3.oPag.oAIINVSOG_3_33.enabled = i_bVal
      .Page3.oPag.oAIDATIMP_3_36.enabled = i_bVal
      .Page3.oPag.oAIFIRMA_3_40.enabled = i_bVal
      .Page4.oPag.oAIPERAZI_4_2.enabled = i_bVal
      .Page4.oPag.oAICOGNOM_4_13.enabled = i_bVal
      .Page4.oPag.oAINOME_4_14.enabled = i_bVal
      .Page4.oPag.oAISESSO_4_15.enabled = i_bVal
      .Page4.oPag.oAICOMUNE_4_16.enabled = i_bVal
      .Page4.oPag.oAIDATNAS_4_17.enabled = i_bVal
      .Page4.oPag.oAISIGLA_4_18.enabled = i_bVal
      .Page4.oPag.oAIRESCOM_4_19.enabled = i_bVal
      .Page4.oPag.oAIRESSIG_4_20.enabled = i_bVal
      .Page4.oPag.oAIINDIRI_4_21.enabled = i_bVal
      .Page4.oPag.oAI___CAP_4_22.enabled = i_bVal
      .Page4.oPag.oAIDENOMI_4_32.enabled = i_bVal
      .Page4.oPag.oAISECOMU_4_33.enabled = i_bVal
      .Page4.oPag.oAISESIGL_4_34.enabled = i_bVal
      .Page4.oPag.oAISEIND2_4_35.enabled = i_bVal
      .Page4.oPag.oAISE_CAP_4_36.enabled = i_bVal
      .Page4.oPag.oAISERCOM_4_37.enabled = i_bVal
      .Page4.oPag.oAISERSIG_4_38.enabled = i_bVal
      .Page4.oPag.oAISERIND_4_39.enabled = i_bVal
      .Page4.oPag.oAISERCAP_4_40.enabled = i_bVal
      .Page4.oPag.oNOMEFILE_4_48.enabled = i_bVal
      .Page2.oPag.oBtn_2_38.enabled = i_bVal
      .Page4.oPag.oBtn_4_44.enabled = i_bVal
      .Page4.oPag.oBtn_4_46.enabled = i_bVal
      .Page4.oPag.oBtn_4_47.enabled = i_bVal
      .Page1.oPag.GSAR_BCD.enabled = i_bVal
      .Page4.oPag.oObj_4_3.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAIANNIMP_1_10.enabled = .f.
        .Page1.oPag.oAIDATINV_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAIANNIMP_1_10.enabled = .t.
        .Page1.oPag.oAIDATINV_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_COAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIANNIMP,"AIANNIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATINV,"AIDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPARIVA,"AIPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODATT,"AICODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPFOR,"AITIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICONSEP,"AICONSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISOCGRU,"AISOCGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIEVEECC,"AIEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDITADICH,"EDITADICH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODFIS,"AICODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAR,"AICODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODIVA,"AICODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIMANCOR,"AIMANCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINOLTRO,"AIINOLTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITOTATT,"AITOTATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_NIM,"AITA_NIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_ESE,"AITA_ESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_CIN,"AITA_CIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITOTPAS,"AITOTPAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_NIM,"AITP_NIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_ESE,"AITP_ESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_CIN,"AITP_CIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIMPORO,"AIIMPORO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVAORO,"AIIVAORO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVAESI,"AIIVAESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVADET,"AIIVADET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVADOV,"AIIVADOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVACRE,"AIIVACRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRMAC,"AIFIRMAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOFINT,"AICOFINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIALBCAF,"AIALBCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINVCON,"AIINVCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINVSOG,"AIINVSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATIMP,"AIDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRMA,"AIFIRMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPERAZI,"AIPERAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOGNOM,"AICOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINOME,"AINOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISESSO,"AISESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOMUNE,"AICOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATNAS,"AIDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISIGLA,"AISIGLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRESCOM,"AIRESCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRESSIG,"AIRESSIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINDIRI,"AIINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI___CAP,"AI___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDENOMI,"AIDENOMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISECOMU,"AISECOMU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISESIGL,"AISESIGL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISEIND2,"AISEIND2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISE_CAP,"AISE_CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERCOM,"AISERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERSIG,"AISERSIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERIND,"AISERIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERCAP,"AISERCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOMEFILE,"NOMEFILE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    i_lTable = "MOD_COAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_COAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsar_acd
    this.bUpdated = .t.
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_COAN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_COAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_COAN')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_COAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AIANNIMP,AIDATINV,AIPARIVA,AICODATT,AITIPFOR"+;
                  ",AICONSEP,AISOCGRU,AIEVEECC,EDITADICH,AICODFIS"+;
                  ",AICODCAR,AICODIVA,AIMANCOR,AIINOLTRO,AITOTATT"+;
                  ",AITA_NIM,AITA_ESE,AITA_CIN,AITOTPAS,AITP_NIM"+;
                  ",AITP_ESE,AITP_CIN,AIIMPORO,AIIVAORO,AIIVAESI"+;
                  ",AIIVADET,AIIVADOV,AIIVACRE,AIFIRMAC,AICOFINT"+;
                  ",AIALBCAF,AIINVCON,AIINVSOG,AIDATIMP,AIFIRMA"+;
                  ",AIPERAZI,AICOGNOM,AINOME,AISESSO,AICOMUNE"+;
                  ",AIDATNAS,AISIGLA,AIRESCOM,AIRESSIG,AIINDIRI"+;
                  ",AI___CAP,AIDENOMI,AISECOMU,AISESIGL,AISEIND2"+;
                  ",AISE_CAP,AISERCOM,AISERSIG,AISERIND,AISERCAP"+;
                  ",NOMEFILE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AIANNIMP)+;
                  ","+cp_ToStrODBC(this.w_AIDATINV)+;
                  ","+cp_ToStrODBC(this.w_AIPARIVA)+;
                  ","+cp_ToStrODBC(this.w_AICODATT)+;
                  ","+cp_ToStrODBC(this.w_AITIPFOR)+;
                  ","+cp_ToStrODBC(this.w_AICONSEP)+;
                  ","+cp_ToStrODBC(this.w_AISOCGRU)+;
                  ","+cp_ToStrODBC(this.w_AIEVEECC)+;
                  ","+cp_ToStrODBC(this.w_EDITADICH)+;
                  ","+cp_ToStrODBC(this.w_AICODFIS)+;
                  ","+cp_ToStrODBC(this.w_AICODCAR)+;
                  ","+cp_ToStrODBC(this.w_AICODIVA)+;
                  ","+cp_ToStrODBC(this.w_AIMANCOR)+;
                  ","+cp_ToStrODBC(this.w_AIINOLTRO)+;
                  ","+cp_ToStrODBC(this.w_AITOTATT)+;
                  ","+cp_ToStrODBC(this.w_AITA_NIM)+;
                  ","+cp_ToStrODBC(this.w_AITA_ESE)+;
                  ","+cp_ToStrODBC(this.w_AITA_CIN)+;
                  ","+cp_ToStrODBC(this.w_AITOTPAS)+;
                  ","+cp_ToStrODBC(this.w_AITP_NIM)+;
                  ","+cp_ToStrODBC(this.w_AITP_ESE)+;
                  ","+cp_ToStrODBC(this.w_AITP_CIN)+;
                  ","+cp_ToStrODBC(this.w_AIIMPORO)+;
                  ","+cp_ToStrODBC(this.w_AIIVAORO)+;
                  ","+cp_ToStrODBC(this.w_AIIVAESI)+;
                  ","+cp_ToStrODBC(this.w_AIIVADET)+;
                  ","+cp_ToStrODBC(this.w_AIIVADOV)+;
                  ","+cp_ToStrODBC(this.w_AIIVACRE)+;
                  ","+cp_ToStrODBC(this.w_AIFIRMAC)+;
                  ","+cp_ToStrODBC(this.w_AICOFINT)+;
                  ","+cp_ToStrODBC(this.w_AIALBCAF)+;
                  ","+cp_ToStrODBC(this.w_AIINVCON)+;
                  ","+cp_ToStrODBC(this.w_AIINVSOG)+;
                  ","+cp_ToStrODBC(this.w_AIDATIMP)+;
                  ","+cp_ToStrODBC(this.w_AIFIRMA)+;
                  ","+cp_ToStrODBC(this.w_AIPERAZI)+;
                  ","+cp_ToStrODBC(this.w_AICOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AINOME)+;
                  ","+cp_ToStrODBC(this.w_AISESSO)+;
                  ","+cp_ToStrODBC(this.w_AICOMUNE)+;
                  ","+cp_ToStrODBC(this.w_AIDATNAS)+;
                  ","+cp_ToStrODBC(this.w_AISIGLA)+;
                  ","+cp_ToStrODBC(this.w_AIRESCOM)+;
                  ","+cp_ToStrODBC(this.w_AIRESSIG)+;
                  ","+cp_ToStrODBC(this.w_AIINDIRI)+;
                  ","+cp_ToStrODBC(this.w_AI___CAP)+;
                  ","+cp_ToStrODBC(this.w_AIDENOMI)+;
                  ","+cp_ToStrODBC(this.w_AISECOMU)+;
                  ","+cp_ToStrODBC(this.w_AISESIGL)+;
                  ","+cp_ToStrODBC(this.w_AISEIND2)+;
                  ","+cp_ToStrODBC(this.w_AISE_CAP)+;
                  ","+cp_ToStrODBC(this.w_AISERCOM)+;
                  ","+cp_ToStrODBC(this.w_AISERSIG)+;
                  ","+cp_ToStrODBC(this.w_AISERIND)+;
                  ","+cp_ToStrODBC(this.w_AISERCAP)+;
                  ","+cp_ToStrODBC(this.w_NOMEFILE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_COAN')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_COAN')
        cp_CheckDeletedKey(i_cTable,0,'AIANNIMP',this.w_AIANNIMP,'AIDATINV',this.w_AIDATINV)
        INSERT INTO (i_cTable);
              (AIANNIMP,AIDATINV,AIPARIVA,AICODATT,AITIPFOR,AICONSEP,AISOCGRU,AIEVEECC,EDITADICH,AICODFIS,AICODCAR,AICODIVA,AIMANCOR,AIINOLTRO,AITOTATT,AITA_NIM,AITA_ESE,AITA_CIN,AITOTPAS,AITP_NIM,AITP_ESE,AITP_CIN,AIIMPORO,AIIVAORO,AIIVAESI,AIIVADET,AIIVADOV,AIIVACRE,AIFIRMAC,AICOFINT,AIALBCAF,AIINVCON,AIINVSOG,AIDATIMP,AIFIRMA,AIPERAZI,AICOGNOM,AINOME,AISESSO,AICOMUNE,AIDATNAS,AISIGLA,AIRESCOM,AIRESSIG,AIINDIRI,AI___CAP,AIDENOMI,AISECOMU,AISESIGL,AISEIND2,AISE_CAP,AISERCOM,AISERSIG,AISERIND,AISERCAP,NOMEFILE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AIANNIMP;
                  ,this.w_AIDATINV;
                  ,this.w_AIPARIVA;
                  ,this.w_AICODATT;
                  ,this.w_AITIPFOR;
                  ,this.w_AICONSEP;
                  ,this.w_AISOCGRU;
                  ,this.w_AIEVEECC;
                  ,this.w_EDITADICH;
                  ,this.w_AICODFIS;
                  ,this.w_AICODCAR;
                  ,this.w_AICODIVA;
                  ,this.w_AIMANCOR;
                  ,this.w_AIINOLTRO;
                  ,this.w_AITOTATT;
                  ,this.w_AITA_NIM;
                  ,this.w_AITA_ESE;
                  ,this.w_AITA_CIN;
                  ,this.w_AITOTPAS;
                  ,this.w_AITP_NIM;
                  ,this.w_AITP_ESE;
                  ,this.w_AITP_CIN;
                  ,this.w_AIIMPORO;
                  ,this.w_AIIVAORO;
                  ,this.w_AIIVAESI;
                  ,this.w_AIIVADET;
                  ,this.w_AIIVADOV;
                  ,this.w_AIIVACRE;
                  ,this.w_AIFIRMAC;
                  ,this.w_AICOFINT;
                  ,this.w_AIALBCAF;
                  ,this.w_AIINVCON;
                  ,this.w_AIINVSOG;
                  ,this.w_AIDATIMP;
                  ,this.w_AIFIRMA;
                  ,this.w_AIPERAZI;
                  ,this.w_AICOGNOM;
                  ,this.w_AINOME;
                  ,this.w_AISESSO;
                  ,this.w_AICOMUNE;
                  ,this.w_AIDATNAS;
                  ,this.w_AISIGLA;
                  ,this.w_AIRESCOM;
                  ,this.w_AIRESSIG;
                  ,this.w_AIINDIRI;
                  ,this.w_AI___CAP;
                  ,this.w_AIDENOMI;
                  ,this.w_AISECOMU;
                  ,this.w_AISESIGL;
                  ,this.w_AISEIND2;
                  ,this.w_AISE_CAP;
                  ,this.w_AISERCOM;
                  ,this.w_AISERSIG;
                  ,this.w_AISERIND;
                  ,this.w_AISERCAP;
                  ,this.w_NOMEFILE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_COAN_IDX,i_nConn)
      *
      * update MOD_COAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_COAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AIPARIVA="+cp_ToStrODBC(this.w_AIPARIVA)+;
             ",AICODATT="+cp_ToStrODBC(this.w_AICODATT)+;
             ",AITIPFOR="+cp_ToStrODBC(this.w_AITIPFOR)+;
             ",AICONSEP="+cp_ToStrODBC(this.w_AICONSEP)+;
             ",AISOCGRU="+cp_ToStrODBC(this.w_AISOCGRU)+;
             ",AIEVEECC="+cp_ToStrODBC(this.w_AIEVEECC)+;
             ",EDITADICH="+cp_ToStrODBC(this.w_EDITADICH)+;
             ",AICODFIS="+cp_ToStrODBC(this.w_AICODFIS)+;
             ",AICODCAR="+cp_ToStrODBC(this.w_AICODCAR)+;
             ",AICODIVA="+cp_ToStrODBC(this.w_AICODIVA)+;
             ",AIMANCOR="+cp_ToStrODBC(this.w_AIMANCOR)+;
             ",AIINOLTRO="+cp_ToStrODBC(this.w_AIINOLTRO)+;
             ",AITOTATT="+cp_ToStrODBC(this.w_AITOTATT)+;
             ",AITA_NIM="+cp_ToStrODBC(this.w_AITA_NIM)+;
             ",AITA_ESE="+cp_ToStrODBC(this.w_AITA_ESE)+;
             ",AITA_CIN="+cp_ToStrODBC(this.w_AITA_CIN)+;
             ",AITOTPAS="+cp_ToStrODBC(this.w_AITOTPAS)+;
             ",AITP_NIM="+cp_ToStrODBC(this.w_AITP_NIM)+;
             ",AITP_ESE="+cp_ToStrODBC(this.w_AITP_ESE)+;
             ",AITP_CIN="+cp_ToStrODBC(this.w_AITP_CIN)+;
             ",AIIMPORO="+cp_ToStrODBC(this.w_AIIMPORO)+;
             ",AIIVAORO="+cp_ToStrODBC(this.w_AIIVAORO)+;
             ",AIIVAESI="+cp_ToStrODBC(this.w_AIIVAESI)+;
             ",AIIVADET="+cp_ToStrODBC(this.w_AIIVADET)+;
             ",AIIVADOV="+cp_ToStrODBC(this.w_AIIVADOV)+;
             ",AIIVACRE="+cp_ToStrODBC(this.w_AIIVACRE)+;
             ",AIFIRMAC="+cp_ToStrODBC(this.w_AIFIRMAC)+;
             ",AICOFINT="+cp_ToStrODBC(this.w_AICOFINT)+;
             ",AIALBCAF="+cp_ToStrODBC(this.w_AIALBCAF)+;
             ",AIINVCON="+cp_ToStrODBC(this.w_AIINVCON)+;
             ",AIINVSOG="+cp_ToStrODBC(this.w_AIINVSOG)+;
             ",AIDATIMP="+cp_ToStrODBC(this.w_AIDATIMP)+;
             ",AIFIRMA="+cp_ToStrODBC(this.w_AIFIRMA)+;
             ",AIPERAZI="+cp_ToStrODBC(this.w_AIPERAZI)+;
             ",AICOGNOM="+cp_ToStrODBC(this.w_AICOGNOM)+;
             ",AINOME="+cp_ToStrODBC(this.w_AINOME)+;
             ",AISESSO="+cp_ToStrODBC(this.w_AISESSO)+;
             ",AICOMUNE="+cp_ToStrODBC(this.w_AICOMUNE)+;
             ",AIDATNAS="+cp_ToStrODBC(this.w_AIDATNAS)+;
             ",AISIGLA="+cp_ToStrODBC(this.w_AISIGLA)+;
             ",AIRESCOM="+cp_ToStrODBC(this.w_AIRESCOM)+;
             ",AIRESSIG="+cp_ToStrODBC(this.w_AIRESSIG)+;
             ",AIINDIRI="+cp_ToStrODBC(this.w_AIINDIRI)+;
             ",AI___CAP="+cp_ToStrODBC(this.w_AI___CAP)+;
             ",AIDENOMI="+cp_ToStrODBC(this.w_AIDENOMI)+;
             ",AISECOMU="+cp_ToStrODBC(this.w_AISECOMU)+;
             ",AISESIGL="+cp_ToStrODBC(this.w_AISESIGL)+;
             ",AISEIND2="+cp_ToStrODBC(this.w_AISEIND2)+;
             ",AISE_CAP="+cp_ToStrODBC(this.w_AISE_CAP)+;
             ",AISERCOM="+cp_ToStrODBC(this.w_AISERCOM)+;
             ",AISERSIG="+cp_ToStrODBC(this.w_AISERSIG)+;
             ",AISERIND="+cp_ToStrODBC(this.w_AISERIND)+;
             ",AISERCAP="+cp_ToStrODBC(this.w_AISERCAP)+;
             ",NOMEFILE="+cp_ToStrODBC(this.w_NOMEFILE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_COAN')
        i_cWhere = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
        UPDATE (i_cTable) SET;
              AIPARIVA=this.w_AIPARIVA;
             ,AICODATT=this.w_AICODATT;
             ,AITIPFOR=this.w_AITIPFOR;
             ,AICONSEP=this.w_AICONSEP;
             ,AISOCGRU=this.w_AISOCGRU;
             ,AIEVEECC=this.w_AIEVEECC;
             ,EDITADICH=this.w_EDITADICH;
             ,AICODFIS=this.w_AICODFIS;
             ,AICODCAR=this.w_AICODCAR;
             ,AICODIVA=this.w_AICODIVA;
             ,AIMANCOR=this.w_AIMANCOR;
             ,AIINOLTRO=this.w_AIINOLTRO;
             ,AITOTATT=this.w_AITOTATT;
             ,AITA_NIM=this.w_AITA_NIM;
             ,AITA_ESE=this.w_AITA_ESE;
             ,AITA_CIN=this.w_AITA_CIN;
             ,AITOTPAS=this.w_AITOTPAS;
             ,AITP_NIM=this.w_AITP_NIM;
             ,AITP_ESE=this.w_AITP_ESE;
             ,AITP_CIN=this.w_AITP_CIN;
             ,AIIMPORO=this.w_AIIMPORO;
             ,AIIVAORO=this.w_AIIVAORO;
             ,AIIVAESI=this.w_AIIVAESI;
             ,AIIVADET=this.w_AIIVADET;
             ,AIIVADOV=this.w_AIIVADOV;
             ,AIIVACRE=this.w_AIIVACRE;
             ,AIFIRMAC=this.w_AIFIRMAC;
             ,AICOFINT=this.w_AICOFINT;
             ,AIALBCAF=this.w_AIALBCAF;
             ,AIINVCON=this.w_AIINVCON;
             ,AIINVSOG=this.w_AIINVSOG;
             ,AIDATIMP=this.w_AIDATIMP;
             ,AIFIRMA=this.w_AIFIRMA;
             ,AIPERAZI=this.w_AIPERAZI;
             ,AICOGNOM=this.w_AICOGNOM;
             ,AINOME=this.w_AINOME;
             ,AISESSO=this.w_AISESSO;
             ,AICOMUNE=this.w_AICOMUNE;
             ,AIDATNAS=this.w_AIDATNAS;
             ,AISIGLA=this.w_AISIGLA;
             ,AIRESCOM=this.w_AIRESCOM;
             ,AIRESSIG=this.w_AIRESSIG;
             ,AIINDIRI=this.w_AIINDIRI;
             ,AI___CAP=this.w_AI___CAP;
             ,AIDENOMI=this.w_AIDENOMI;
             ,AISECOMU=this.w_AISECOMU;
             ,AISESIGL=this.w_AISESIGL;
             ,AISEIND2=this.w_AISEIND2;
             ,AISE_CAP=this.w_AISE_CAP;
             ,AISERCOM=this.w_AISERCOM;
             ,AISERSIG=this.w_AISERSIG;
             ,AISERIND=this.w_AISERIND;
             ,AISERCAP=this.w_AISERCAP;
             ,NOMEFILE=this.w_NOMEFILE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_COAN_IDX,i_nConn)
      *
      * delete MOD_COAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,11,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        endif
        .oPgFrm.Page1.oPag.GSAR_BCD.Calculate()
        .DoRTCalc(13,20,.t.)
        if .o_EDITADICH<>.w_EDITADICH
            .w_AICODFIS = IIF(.w_EDITADICH='S' and .cFunction<>'Query',.w_AZIVACOF,space(16))
        endif
        .DoRTCalc(22,22,.t.)
        if .o_EDITADICH<>.w_EDITADICH
            .w_AICODIVA = space(11)
        endif
        .DoRTCalc(24,26,.t.)
          .link_1_46('Full')
        if .o_COGTIT<>.w_COGTIT
            .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        endif
        if .o_NOMTIT<>.w_NOMTIT
            .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
        endif
        .DoRTCalc(30,41,.t.)
        if .o_AIIVAESI<>.w_AIIVAESI.or. .o_AIIVADET<>.w_AIIVADET
            .w_AIIVADOV = IIF(.w_AIIVAESI>=.w_AIIVADET,.w_AIIVAESI-.w_AIIVADET,0)
        endif
        if .o_AIIVAESI<>.w_AIIVAESI.or. .o_AIIVADET<>.w_AIIVADET
            .w_AIIVACRE = IIF(.w_AIIVAESI<.w_AIIVADET,.w_AIIVADET-.w_AIIVAESI,0)
        endif
        .DoRTCalc(44,44,.t.)
        if .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOFINT = iif(.w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02',.w_AICOFINT,space(16))
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIALBCAF = IIF(empty(.w_AICOFINT),space(5),.w_AIALBCAF)
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AIINVSOG<>.w_AIINVSOG.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINVCON = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVSOG='0','1','0')
        endif
        if .o_AIINVCON<>.w_AIINVCON.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINVSOG = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVCON='0','1','0')
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDATIMP = IIF(empty(.w_AICOFINT),cp_CharToDate('  -  -    '),.w_AIDATIMP)
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIFIRMA = IIF(NOT EMPTY(.w_AICOFINT),.w_AIFIRMA,'0')
        endif
        .oPgFrm.Page4.oPag.oObj_4_3.Calculate()
        .DoRTCalc(51,51,.t.)
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOGNOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOGNOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AINOME = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AINOME,' ')
        endif
        .DoRTCalc(54,54,.t.)
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOMUNE = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOMUNE,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDATNAS = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIDATNAS,cp_CharToDate('  -  -  '))
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISIGLA = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AISIGLA,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIRESCOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESCOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIRESSIG = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESSIG,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINDIRI = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIINDIRI,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AI___CAP = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AI___CAP,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDENOMI = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AIDENOMI,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISECOMU = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISECOMU,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT
            .w_AISESIGL = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISESIGL,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AITIPFOR<>.w_AITIPFOR.or. .o_AICOFINT<>.w_AICOFINT
            .w_AISEIND2 = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISEIND2,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISE_CAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISE_CAP,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERCOM = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICODFIS<>.w_AICODFIS.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERSIG = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERSIG,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERIND = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERIND,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERCAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCAP,' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        * --- Area Manuale = Calculate
        * --- gsar_acd
        if .o_EDITADICH<>.w_EDITADICH
              .o_EDITADICH=.w_EDITADICH
        endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(71,72,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.GSAR_BCD.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODFIS_1_12.enabled = this.oPgFrm.Page1.oPag.oCODFIS_1_12.mCond()
    this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.enabled = this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.enabled = this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.mCond()
    this.oPgFrm.Page1.oPag.oAICONSEP_1_16.enabled = this.oPgFrm.Page1.oPag.oAICONSEP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.enabled = this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.mCond()
    this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.enabled = this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oAICODFIS_1_33.enabled = this.oPgFrm.Page1.oPag.oAICODFIS_1_33.mCond()
    this.oPgFrm.Page1.oPag.oAICODCAR_1_36.enabled = this.oPgFrm.Page1.oPag.oAICODCAR_1_36.mCond()
    this.oPgFrm.Page1.oPag.oAICODIVA_1_38.enabled = this.oPgFrm.Page1.oPag.oAICODIVA_1_38.mCond()
    this.oPgFrm.Page1.oPag.oAIMANCOR_1_39.enabled = this.oPgFrm.Page1.oPag.oAIMANCOR_1_39.mCond()
    this.oPgFrm.Page2.oPag.oAITOTATT_2_9.enabled = this.oPgFrm.Page2.oPag.oAITOTATT_2_9.mCond()
    this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.enabled = this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.mCond()
    this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.enabled = this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.mCond()
    this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.enabled = this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.mCond()
    this.oPgFrm.Page2.oPag.oAITOTPAS_2_27.enabled = this.oPgFrm.Page2.oPag.oAITOTPAS_2_27.mCond()
    this.oPgFrm.Page2.oPag.oAITP_NIM_2_30.enabled = this.oPgFrm.Page2.oPag.oAITP_NIM_2_30.mCond()
    this.oPgFrm.Page2.oPag.oAITP_ESE_2_32.enabled = this.oPgFrm.Page2.oPag.oAITP_ESE_2_32.mCond()
    this.oPgFrm.Page2.oPag.oAITP_CIN_2_34.enabled = this.oPgFrm.Page2.oPag.oAITP_CIN_2_34.mCond()
    this.oPgFrm.Page2.oPag.oAIIMPORO_2_36.enabled = this.oPgFrm.Page2.oPag.oAIIMPORO_2_36.mCond()
    this.oPgFrm.Page2.oPag.oAIIVAORO_2_37.enabled = this.oPgFrm.Page2.oPag.oAIIVAORO_2_37.mCond()
    this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.enabled = this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.mCond()
    this.oPgFrm.Page3.oPag.oAIIVADET_3_12.enabled = this.oPgFrm.Page3.oPag.oAIIVADET_3_12.mCond()
    this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.enabled = this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.mCond()
    this.oPgFrm.Page3.oPag.oAICOFINT_3_21.enabled = this.oPgFrm.Page3.oPag.oAICOFINT_3_21.mCond()
    this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.enabled = this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.mCond()
    this.oPgFrm.Page3.oPag.oAIINVCON_3_30.enabled = this.oPgFrm.Page3.oPag.oAIINVCON_3_30.mCond()
    this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.enabled = this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.mCond()
    this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.enabled = this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.mCond()
    this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.enabled = this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.mCond()
    this.oPgFrm.Page4.oPag.oAIPERAZI_4_2.enabled = this.oPgFrm.Page4.oPag.oAIPERAZI_4_2.mCond()
    this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.enabled = this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.mCond()
    this.oPgFrm.Page4.oPag.oAINOME_4_14.enabled = this.oPgFrm.Page4.oPag.oAINOME_4_14.mCond()
    this.oPgFrm.Page4.oPag.oAISESSO_4_15.enabled = this.oPgFrm.Page4.oPag.oAISESSO_4_15.mCond()
    this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.enabled = this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.mCond()
    this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.enabled = this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.mCond()
    this.oPgFrm.Page4.oPag.oAISIGLA_4_18.enabled = this.oPgFrm.Page4.oPag.oAISIGLA_4_18.mCond()
    this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.enabled = this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.mCond()
    this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.enabled = this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.mCond()
    this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.enabled = this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.mCond()
    this.oPgFrm.Page4.oPag.oAI___CAP_4_22.enabled = this.oPgFrm.Page4.oPag.oAI___CAP_4_22.mCond()
    this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.enabled = this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.mCond()
    this.oPgFrm.Page4.oPag.oAISECOMU_4_33.enabled = this.oPgFrm.Page4.oPag.oAISECOMU_4_33.mCond()
    this.oPgFrm.Page4.oPag.oAISESIGL_4_34.enabled = this.oPgFrm.Page4.oPag.oAISESIGL_4_34.mCond()
    this.oPgFrm.Page4.oPag.oAISEIND2_4_35.enabled = this.oPgFrm.Page4.oPag.oAISEIND2_4_35.mCond()
    this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.enabled = this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.mCond()
    this.oPgFrm.Page4.oPag.oAISERCOM_4_37.enabled = this.oPgFrm.Page4.oPag.oAISERCOM_4_37.mCond()
    this.oPgFrm.Page4.oPag.oAISERSIG_4_38.enabled = this.oPgFrm.Page4.oPag.oAISERSIG_4_38.mCond()
    this.oPgFrm.Page4.oPag.oAISERIND_4_39.enabled = this.oPgFrm.Page4.oPag.oAISERIND_4_39.mCond()
    this.oPgFrm.Page4.oPag.oAISERCAP_4_40.enabled = this.oPgFrm.Page4.oPag.oAISERCAP_4_40.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_38.enabled = this.oPgFrm.Page2.oPag.oBtn_2_38.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_46.enabled = this.oPgFrm.Page4.oPag.oBtn_4_46.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_47.enabled = this.oPgFrm.Page4.oPag.oBtn_4_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRAGSOC_1_21.visible=!this.oPgFrm.Page1.oPag.oRAGSOC_1_21.mHide()
    this.oPgFrm.Page1.oPag.oEDITADICH_1_32.visible=!this.oPgFrm.Page1.oPag.oEDITADICH_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCOGTIT_1_47.visible=!this.oPgFrm.Page1.oPag.oCOGTIT_1_47.mHide()
    this.oPgFrm.Page1.oPag.oNOMTIT_1_48.visible=!this.oPgFrm.Page1.oPag.oNOMTIT_1_48.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_1.visible=!this.oPgFrm.Page4.oPag.oStr_4_1.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_4.visible=!this.oPgFrm.Page4.oPag.oStr_4_4.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_5.visible=!this.oPgFrm.Page4.oPag.oStr_4_5.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_6.visible=!this.oPgFrm.Page4.oPag.oStr_4_6.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_7.visible=!this.oPgFrm.Page4.oPag.oStr_4_7.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_8.visible=!this.oPgFrm.Page4.oPag.oStr_4_8.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_9.visible=!this.oPgFrm.Page4.oPag.oStr_4_9.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_10.visible=!this.oPgFrm.Page4.oPag.oStr_4_10.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_11.visible=!this.oPgFrm.Page4.oPag.oStr_4_11.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_12.visible=!this.oPgFrm.Page4.oPag.oStr_4_12.mHide()
    this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.visible=!this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.mHide()
    this.oPgFrm.Page4.oPag.oAINOME_4_14.visible=!this.oPgFrm.Page4.oPag.oAINOME_4_14.mHide()
    this.oPgFrm.Page4.oPag.oAISESSO_4_15.visible=!this.oPgFrm.Page4.oPag.oAISESSO_4_15.mHide()
    this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.visible=!this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.mHide()
    this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.visible=!this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.mHide()
    this.oPgFrm.Page4.oPag.oAISIGLA_4_18.visible=!this.oPgFrm.Page4.oPag.oAISIGLA_4_18.mHide()
    this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.visible=!this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.mHide()
    this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.visible=!this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.mHide()
    this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.visible=!this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.mHide()
    this.oPgFrm.Page4.oPag.oAI___CAP_4_22.visible=!this.oPgFrm.Page4.oPag.oAI___CAP_4_22.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_23.visible=!this.oPgFrm.Page4.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_24.visible=!this.oPgFrm.Page4.oPag.oStr_4_24.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_25.visible=!this.oPgFrm.Page4.oPag.oStr_4_25.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_26.visible=!this.oPgFrm.Page4.oPag.oStr_4_26.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_27.visible=!this.oPgFrm.Page4.oPag.oStr_4_27.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_28.visible=!this.oPgFrm.Page4.oPag.oStr_4_28.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_29.visible=!this.oPgFrm.Page4.oPag.oStr_4_29.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_30.visible=!this.oPgFrm.Page4.oPag.oStr_4_30.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_31.visible=!this.oPgFrm.Page4.oPag.oStr_4_31.mHide()
    this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.visible=!this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.mHide()
    this.oPgFrm.Page4.oPag.oAISECOMU_4_33.visible=!this.oPgFrm.Page4.oPag.oAISECOMU_4_33.mHide()
    this.oPgFrm.Page4.oPag.oAISESIGL_4_34.visible=!this.oPgFrm.Page4.oPag.oAISESIGL_4_34.mHide()
    this.oPgFrm.Page4.oPag.oAISEIND2_4_35.visible=!this.oPgFrm.Page4.oPag.oAISEIND2_4_35.mHide()
    this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.visible=!this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.mHide()
    this.oPgFrm.Page4.oPag.oAISERCOM_4_37.visible=!this.oPgFrm.Page4.oPag.oAISERCOM_4_37.mHide()
    this.oPgFrm.Page4.oPag.oAISERSIG_4_38.visible=!this.oPgFrm.Page4.oPag.oAISERSIG_4_38.mHide()
    this.oPgFrm.Page4.oPag.oAISERIND_4_39.visible=!this.oPgFrm.Page4.oPag.oAISERIND_4_39.mHide()
    this.oPgFrm.Page4.oPag.oAISERCAP_4_40.visible=!this.oPgFrm.Page4.oPag.oAISERCAP_4_40.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.GSAR_BCD.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZIVACAR,AZPIVAZI,AZCOFAZI,AZPERAZI,AZIVACOF,AZCODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZIVACAR,AZPIVAZI,AZCOFAZI,AZPERAZI,AZIVACOF,AZCODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZIVACOF = NVL(_Link_.AZIVACOF,space(16))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCODCAR = space(2)
      this.w_AZPIVAZI = space(12)
      this.w_AZCODFIS = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZIVACOF = space(16)
      this.w_AZCODNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI1)
            select TTCODAZI,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT = NVL(_Link_.TTNOMTIT,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_COGTIT = space(25)
      this.w_NOMTIT = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAIANNIMP_1_10.value==this.w_AIANNIMP)
      this.oPgFrm.Page1.oPag.oAIANNIMP_1_10.value=this.w_AIANNIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATINV_1_11.value==this.w_AIDATINV)
      this.oPgFrm.Page1.oPag.oAIDATINV_1_11.value=this.w_AIDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIS_1_12.value==this.w_CODFIS)
      this.oPgFrm.Page1.oPag.oCODFIS_1_12.value=this.w_CODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.value==this.w_AIPARIVA)
      this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.value=this.w_AIPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODATT_1_14.value==this.w_AICODATT)
      this.oPgFrm.Page1.oPag.oAICODATT_1_14.value=this.w_AICODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.RadioValue()==this.w_AITIPFOR)
      this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICONSEP_1_16.RadioValue()==this.w_AICONSEP)
      this.oPgFrm.Page1.oPag.oAICONSEP_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_21.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_21.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.RadioValue()==this.w_AISOCGRU)
      this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.RadioValue()==this.w_AIEVEECC)
      this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEDITADICH_1_32.RadioValue()==this.w_EDITADICH)
      this.oPgFrm.Page1.oPag.oEDITADICH_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value==this.w_AICODFIS)
      this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value=this.w_AICODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODCAR_1_36.RadioValue()==this.w_AICODCAR)
      this.oPgFrm.Page1.oPag.oAICODCAR_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODIVA_1_38.value==this.w_AICODIVA)
      this.oPgFrm.Page1.oPag.oAICODIVA_1_38.value=this.w_AICODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAIMANCOR_1_39.RadioValue()==this.w_AIMANCOR)
      this.oPgFrm.Page1.oPag.oAIMANCOR_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGTIT_1_47.value==this.w_COGTIT)
      this.oPgFrm.Page1.oPag.oCOGTIT_1_47.value=this.w_COGTIT
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMTIT_1_48.value==this.w_NOMTIT)
      this.oPgFrm.Page1.oPag.oNOMTIT_1_48.value=this.w_NOMTIT
    endif
    if not(this.oPgFrm.Page2.oPag.oAITOTATT_2_9.value==this.w_AITOTATT)
      this.oPgFrm.Page2.oPag.oAITOTATT_2_9.value=this.w_AITOTATT
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.value==this.w_AITA_NIM)
      this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.value=this.w_AITA_NIM
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.value==this.w_AITA_ESE)
      this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.value=this.w_AITA_ESE
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.value==this.w_AITA_CIN)
      this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.value=this.w_AITA_CIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAITOTPAS_2_27.value==this.w_AITOTPAS)
      this.oPgFrm.Page2.oPag.oAITOTPAS_2_27.value=this.w_AITOTPAS
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_NIM_2_30.value==this.w_AITP_NIM)
      this.oPgFrm.Page2.oPag.oAITP_NIM_2_30.value=this.w_AITP_NIM
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_ESE_2_32.value==this.w_AITP_ESE)
      this.oPgFrm.Page2.oPag.oAITP_ESE_2_32.value=this.w_AITP_ESE
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_CIN_2_34.value==this.w_AITP_CIN)
      this.oPgFrm.Page2.oPag.oAITP_CIN_2_34.value=this.w_AITP_CIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIMPORO_2_36.value==this.w_AIIMPORO)
      this.oPgFrm.Page2.oPag.oAIIMPORO_2_36.value=this.w_AIIMPORO
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIVAORO_2_37.value==this.w_AIIVAORO)
      this.oPgFrm.Page2.oPag.oAIIVAORO_2_37.value=this.w_AIIVAORO
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.value==this.w_AIIVAESI)
      this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.value=this.w_AIIVAESI
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVADET_3_12.value==this.w_AIIVADET)
      this.oPgFrm.Page3.oPag.oAIIVADET_3_12.value=this.w_AIIVADET
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVADOV_3_16.value==this.w_AIIVADOV)
      this.oPgFrm.Page3.oPag.oAIIVADOV_3_16.value=this.w_AIIVADOV
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVACRE_3_18.value==this.w_AIIVACRE)
      this.oPgFrm.Page3.oPag.oAIIVACRE_3_18.value=this.w_AIIVACRE
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.RadioValue()==this.w_AIFIRMAC)
      this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAICOFINT_3_21.value==this.w_AICOFINT)
      this.oPgFrm.Page3.oPag.oAICOFINT_3_21.value=this.w_AICOFINT
    endif
    if not(this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.value==this.w_AIALBCAF)
      this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.value=this.w_AIALBCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oAIINVCON_3_30.RadioValue()==this.w_AIINVCON)
      this.oPgFrm.Page3.oPag.oAIINVCON_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.RadioValue()==this.w_AIINVSOG)
      this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.value==this.w_AIDATIMP)
      this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.value=this.w_AIDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.RadioValue()==this.w_AIFIRMA)
      this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAIPERAZI_4_2.RadioValue()==this.w_AIPERAZI)
      this.oPgFrm.Page4.oPag.oAIPERAZI_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.value==this.w_AICOGNOM)
      this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.value=this.w_AICOGNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAINOME_4_14.value==this.w_AINOME)
      this.oPgFrm.Page4.oPag.oAINOME_4_14.value=this.w_AINOME
    endif
    if not(this.oPgFrm.Page4.oPag.oAISESSO_4_15.RadioValue()==this.w_AISESSO)
      this.oPgFrm.Page4.oPag.oAISESSO_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.value==this.w_AICOMUNE)
      this.oPgFrm.Page4.oPag.oAICOMUNE_4_16.value=this.w_AICOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.value==this.w_AIDATNAS)
      this.oPgFrm.Page4.oPag.oAIDATNAS_4_17.value=this.w_AIDATNAS
    endif
    if not(this.oPgFrm.Page4.oPag.oAISIGLA_4_18.value==this.w_AISIGLA)
      this.oPgFrm.Page4.oPag.oAISIGLA_4_18.value=this.w_AISIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.value==this.w_AIRESCOM)
      this.oPgFrm.Page4.oPag.oAIRESCOM_4_19.value=this.w_AIRESCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.value==this.w_AIRESSIG)
      this.oPgFrm.Page4.oPag.oAIRESSIG_4_20.value=this.w_AIRESSIG
    endif
    if not(this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.value==this.w_AIINDIRI)
      this.oPgFrm.Page4.oPag.oAIINDIRI_4_21.value=this.w_AIINDIRI
    endif
    if not(this.oPgFrm.Page4.oPag.oAI___CAP_4_22.value==this.w_AI___CAP)
      this.oPgFrm.Page4.oPag.oAI___CAP_4_22.value=this.w_AI___CAP
    endif
    if not(this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.value==this.w_AIDENOMI)
      this.oPgFrm.Page4.oPag.oAIDENOMI_4_32.value=this.w_AIDENOMI
    endif
    if not(this.oPgFrm.Page4.oPag.oAISECOMU_4_33.value==this.w_AISECOMU)
      this.oPgFrm.Page4.oPag.oAISECOMU_4_33.value=this.w_AISECOMU
    endif
    if not(this.oPgFrm.Page4.oPag.oAISESIGL_4_34.value==this.w_AISESIGL)
      this.oPgFrm.Page4.oPag.oAISESIGL_4_34.value=this.w_AISESIGL
    endif
    if not(this.oPgFrm.Page4.oPag.oAISEIND2_4_35.value==this.w_AISEIND2)
      this.oPgFrm.Page4.oPag.oAISEIND2_4_35.value=this.w_AISEIND2
    endif
    if not(this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.value==this.w_AISE_CAP)
      this.oPgFrm.Page4.oPag.oAISE_CAP_4_36.value=this.w_AISE_CAP
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERCOM_4_37.value==this.w_AISERCOM)
      this.oPgFrm.Page4.oPag.oAISERCOM_4_37.value=this.w_AISERCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERSIG_4_38.value==this.w_AISERSIG)
      this.oPgFrm.Page4.oPag.oAISERSIG_4_38.value=this.w_AISERSIG
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERIND_4_39.value==this.w_AISERIND)
      this.oPgFrm.Page4.oPag.oAISERIND_4_39.value=this.w_AISERIND
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERCAP_4_40.value==this.w_AISERCAP)
      this.oPgFrm.Page4.oPag.oAISERCAP_4_40.value=this.w_AISERCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oNOMEFILE_4_48.value==this.w_NOMEFILE)
      this.oPgFrm.Page4.oPag.oNOMEFILE_4_48.value=this.w_NOMEFILE
    endif
    cp_SetControlsValueExtFlds(this,'MOD_COAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AIANNIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIANNIMP_1_10.SetFocus()
            i_bnoObbl = !empty(.w_AIANNIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(YEAR (.w_AIDATINV)<=2004)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATINV_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("questa maschera gestisce le dichiarazioni IVA relative ad anni antecedenti il 2004")
          case   not(CHKCFP(.w_CODFIS,"CF"))  and (EMPTY(.w_CODFIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIS_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AIPARIVA,"PI","","", .w_AZCODNAZ))  and (.w_AIINOLTRO<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPARIVA_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AICODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODATT_1_14.SetFocus()
            i_bnoObbl = !empty(.w_AICODATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AICODFIS)) or not(CHKCFP(.w_AICODFIS, "CF")))  and (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODFIS_1_33.SetFocus()
            i_bnoObbl = !empty(.w_AICODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AICODIVA,"PI"))  and (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODIVA_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AICOFINT, "CF"))  and (.w_AIINOLTRO<>'S' AND .w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAICOFINT_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_AITIPFOR = this.w_AITIPFOR
    this.o_EDITADICH = this.w_EDITADICH
    this.o_AICODFIS = this.w_AICODFIS
    this.o_COGTIT = this.w_COGTIT
    this.o_NOMTIT = this.w_NOMTIT
    this.o_AIIVAESI = this.w_AIIVAESI
    this.o_AIIVADET = this.w_AIIVADET
    this.o_AICOFINT = this.w_AICOFINT
    this.o_AIINVCON = this.w_AIINVCON
    this.o_AIINVSOG = this.w_AIINVSOG
    this.o_AIPERAZI = this.w_AIPERAZI
    return

enddefine

* --- Define pages as container
define class tgsar_acdPag1 as StdContainer
  Width  = 694
  height = 315
  stdWidth  = 694
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAIANNIMP_1_10 as StdField with uid="AAATTTHPCX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AIANNIMP", cQueryName = "AIANNIMP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di imposta",;
    HelpContextID = 139925930,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=208, Top=54, cSayPict='"9999"', cGetPict='"9999"'

  add object oAIDATINV_1_11 as StdField with uid="XIPFKJYRPB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AIDATINV", cQueryName = "AIANNIMP,AIDATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "questa maschera gestisce le dichiarazioni IVA relative ad anni antecedenti il 2004",;
    ToolTipText = "Data invio (per determinare chiave primaria)",;
    HelpContextID = 134474148,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=473, Top=54

  func oAIDATINV_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (YEAR (.w_AIDATINV)<=2004)
    endwith
    return bRes
  endfunc

  add object oCODFIS_1_12 as StdField with uid="OCYCKRBZUY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODFIS", cQueryName = "CODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 22092838,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=473, Top=22, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oCODFIS_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODFIS))
    endwith
   endif
  endfunc

  func oCODFIS_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_CODFIS,"CF"))
    endwith
    return bRes
  endfunc

  add object oAIPARIVA_1_13 as StdField with uid="UYFPYTHNVF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AIPARIVA", cQueryName = "AIPARIVA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA contribuente",;
    HelpContextID = 131913287,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=194, Top=106, cSayPict="'99999999999'", cGetPict="'99999999999'", InputMask=replicate('X',11)

  func oAIPARIVA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIPARIVA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AIPARIVA,"PI","","", .w_AZCODNAZ))
    endwith
    return bRes
  endfunc

  add object oAICODATT_1_14 as StdField with uid="DQEKQXPJDA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AICODATT", cQueryName = "AICODATT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 252315226,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=106, InputMask=replicate('X',5), bHasZoom = .t. 

  proc oAICODATT_1_14.mZoom
    vx_exec("gsar_acd.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oAITIPFOR_1_15 as StdCombo with uid="XVVXXOWQAR",rtseq=15,rtrep=.f.,left=194,top=130,width=244,height=21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 188410280;
    , cFormVar="w_AITIPFOR",RowSource=""+""+L_combo1+","+""+L_combo2a+","+""+L_combo2+","+""+L_combo3+","+""+L_combo4+","+""+L_combo5+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAITIPFOR_1_15.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'02',;
    iif(this.value =3,'03',;
    iif(this.value =4,'05',;
    iif(this.value =5,'09',;
    iif(this.value =6,'10',;
    space(2))))))))
  endfunc
  func oAITIPFOR_1_15.GetRadio()
    this.Parent.oContained.w_AITIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oAITIPFOR_1_15.SetRadio()
    this.Parent.oContained.w_AITIPFOR=trim(this.Parent.oContained.w_AITIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AITIPFOR=='01',1,;
      iif(this.Parent.oContained.w_AITIPFOR=='02',2,;
      iif(this.Parent.oContained.w_AITIPFOR=='03',3,;
      iif(this.Parent.oContained.w_AITIPFOR=='05',4,;
      iif(this.Parent.oContained.w_AITIPFOR=='09',5,;
      iif(this.Parent.oContained.w_AITIPFOR=='10',6,;
      0))))))
  endfunc

  func oAITIPFOR_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAICONSEP_1_16 as StdCheck with uid="UTQYYBJQWW",rtseq=16,rtrep=.f.,left=106, top=158, caption="Contabilit� separate",;
    ToolTipText = "Contabilit� separate",;
    HelpContextID = 27919958,;
    cFormVar="w_AICONSEP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAICONSEP_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAICONSEP_1_16.GetRadio()
    this.Parent.oContained.w_AICONSEP = this.RadioValue()
    return .t.
  endfunc

  func oAICONSEP_1_16.SetRadio()
    this.Parent.oContained.w_AICONSEP=trim(this.Parent.oContained.w_AICONSEP)
    this.value = ;
      iif(this.Parent.oContained.w_AICONSEP=='1',1,;
      0)
  endfunc

  func oAICONSEP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oRAGSOC_1_21 as StdField with uid="ADZVIXCOEV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'azienda",;
    HelpContextID = 239190250,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=15, Top=22, InputMask=replicate('X',40)

  func oRAGSOC_1_21.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc


  add object GSAR_BCD as cp_runprogram with uid="MORMIQQDYR",left=5, top=365, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BCD("INIT")',;
    cEvent = "w_AIANNIMP Changed,New record",;
    nPag=1;
    , HelpContextID = 68325350

  add object oAISOCGRU_1_29 as StdCheck with uid="YNLWMJGXMF",rtseq=18,rtrep=.f.,left=305, top=158, caption="Soc.ader.gruppo IVA",;
    ToolTipText = "Soc. aderente ad un gruppo IVA",;
    HelpContextID = 83560027,;
    cFormVar="w_AISOCGRU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAISOCGRU_1_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAISOCGRU_1_29.GetRadio()
    this.Parent.oContained.w_AISOCGRU = this.RadioValue()
    return .t.
  endfunc

  func oAISOCGRU_1_29.SetRadio()
    this.Parent.oContained.w_AISOCGRU=trim(this.Parent.oContained.w_AISOCGRU)
    this.value = ;
      iif(this.Parent.oContained.w_AISOCGRU=='1',1,;
      0)
  endfunc

  func oAISOCGRU_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIEVEECC_1_30 as StdCheck with uid="YTJCWZOSFK",rtseq=19,rtrep=.f.,left=473, top=159, caption="Eventi eccezionali",;
    ToolTipText = "Eventi eccezionali",;
    HelpContextID = 52504137,;
    cFormVar="w_AIEVEECC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIEVEECC_1_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIEVEECC_1_30.GetRadio()
    this.Parent.oContained.w_AIEVEECC = this.RadioValue()
    return .t.
  endfunc

  func oAIEVEECC_1_30.SetRadio()
    this.Parent.oContained.w_AIEVEECC=trim(this.Parent.oContained.w_AIEVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_AIEVEECC=='1',1,;
      0)
  endfunc

  func oAIEVEECC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oEDITADICH_1_32 as StdCheck with uid="XMGDLNIPAU",rtseq=20,rtrep=.f.,left=473, top=193, caption="Modifica dichiarante",;
    ToolTipText = "Rende editabili i campi del dichiarante",;
    HelpContextID = 237017591,;
    cFormVar="w_EDITADICH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEDITADICH_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEDITADICH_1_32.GetRadio()
    this.Parent.oContained.w_EDITADICH = this.RadioValue()
    return .t.
  endfunc

  func oEDITADICH_1_32.SetRadio()
    this.Parent.oContained.w_EDITADICH=trim(this.Parent.oContained.w_EDITADICH)
    this.value = ;
      iif(this.Parent.oContained.w_EDITADICH=='S',1,;
      0)
  endfunc

  func oEDITADICH_1_32.mHide()
    with this.Parent.oContained
      return (.w_AIINOLTRO='S' OR NVL(.w_AZPERAZI,' ')<>'S')
    endwith
  endfunc

  add object oAICODFIS_1_33 as StdField with uid="UTTHWNQBDX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_AICODFIS", cQueryName = "AICODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dichiarante",;
    HelpContextID = 200669607,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=194, Top=214, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oAICODFIS_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  func oAICODFIS_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AICODFIS, "CF"))
    endwith
    return bRes
  endfunc


  add object oAICODCAR_1_36 as StdCombo with uid="XMRZKEIEDU",rtseq=22,rtrep=.f.,left=398,top=214,width=255,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 17434200;
    , cFormVar="w_AICODCAR",RowSource=""+""+l_descri5+","+""+l_descri2+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri3+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAICODCAR_1_36.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'4',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    iif(this.value =6,'7',;
    iif(this.value =7,'8',;
    iif(this.value =8,'9',;
    space(1))))))))))
  endfunc
  func oAICODCAR_1_36.GetRadio()
    this.Parent.oContained.w_AICODCAR = this.RadioValue()
    return .t.
  endfunc

  func oAICODCAR_1_36.SetRadio()
    this.Parent.oContained.w_AICODCAR=trim(this.Parent.oContained.w_AICODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_AICODCAR=='1',1,;
      iif(this.Parent.oContained.w_AICODCAR=='2',2,;
      iif(this.Parent.oContained.w_AICODCAR=='4',3,;
      iif(this.Parent.oContained.w_AICODCAR=='5',4,;
      iif(this.Parent.oContained.w_AICODCAR=='6',5,;
      iif(this.Parent.oContained.w_AICODCAR=='7',6,;
      iif(this.Parent.oContained.w_AICODCAR=='8',7,;
      iif(this.Parent.oContained.w_AICODCAR=='9',8,;
      0))))))))
  endfunc

  func oAICODCAR_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  add object oAICODIVA_1_38 as StdField with uid="IUYCHBLMBQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AICODIVA", cQueryName = "AICODIVA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� dichiarante",;
    HelpContextID = 118097479,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=305, Top=242, cSayPict="'99999999999'", cGetPict="'99999999999'", InputMask=replicate('X',11)

  func oAICODIVA_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  func oAICODIVA_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AICODIVA,"PI"))
    endwith
    return bRes
  endfunc

  add object oAIMANCOR_1_39 as StdCheck with uid="ENQCKAFMQO",rtseq=24,rtrep=.f.,left=106, top=296, caption="Flag conferma",;
    ToolTipText = "Mancata corrispondenza dei dati da trasmettere",;
    HelpContextID = 241392040,;
    cFormVar="w_AIMANCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIMANCOR_1_39.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIMANCOR_1_39.GetRadio()
    this.Parent.oContained.w_AIMANCOR = this.RadioValue()
    return .t.
  endfunc

  func oAIMANCOR_1_39.SetRadio()
    this.Parent.oContained.w_AIMANCOR=trim(this.Parent.oContained.w_AIMANCOR)
    this.value = ;
      iif(this.Parent.oContained.w_AIMANCOR=='1',1,;
      0)
  endfunc

  func oAIMANCOR_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oCOGTIT_1_47 as StdField with uid="HHYPETRQFJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_COGTIT", cQueryName = "COGTIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Cognome titolare",;
    HelpContextID = 39799846,;
   bGlobalFont=.t.,;
    Height=21, Width=184, Left=15, Top=22, InputMask=replicate('X',25)

  func oCOGTIT_1_47.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oNOMTIT_1_48 as StdField with uid="CGDZTRJWBA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NOMTIT", cQueryName = "NOMTIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome titolare",;
    HelpContextID = 39824598,;
   bGlobalFont=.t.,;
    Height=21, Width=184, Left=202, Top=22, InputMask=replicate('X',25)

  func oNOMTIT_1_48.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc


  add object oObj_1_49 as cp_runprogram with uid="VLUPQYCMCX",left=249, top=365, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BCD('TIPF')",;
    cEvent = "w_AITIPFOR Changed",;
    nPag=1;
    , HelpContextID = 68325350

  add object oStr_1_17 as StdString with uid="YUSQZCSSTK",Visible=.t., Left=107, Top=54,;
    Alignment=1, Width=94, Height=15,;
    Caption="Anno di imposta:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="NEDYJYUGVP",Visible=.t., Left=120, Top=106,;
    Alignment=1, Width=72, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PMAPUHRRHC",Visible=.t., Left=15, Top=5,;
    Alignment=0, Width=398, Height=15,;
    Caption="Denominazione, ragione sociale ovvero cognome e nome"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="ELERXBUKNP",Visible=.t., Left=473, Top=5,;
    Alignment=0, Width=94, Height=15,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="NZEQUPJJMK",Visible=.t., Left=6, Top=51,;
    Alignment=0, Width=54, Height=15,;
    Caption="Sez. I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="HYAKLYMUYU",Visible=.t., Left=6, Top=68,;
    Alignment=0, Width=92, Height=15,;
    Caption="DATI GENERALI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="QNZZRJPHPX",Visible=.t., Left=107, Top=85,;
    Alignment=0, Width=95, Height=15,;
    Caption="- Contribuente -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="MGQUKYVRSZ",Visible=.t., Left=373, Top=106,;
    Alignment=1, Width=96, Height=15,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NUAGVZCWFX",Visible=.t., Left=106, Top=214,;
    Alignment=1, Width=85, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BJTHQXWZOF",Visible=.t., Left=107, Top=193,;
    Alignment=0, Width=340, Height=15,;
    Caption="- Dichiarante (compilare se diverso dal contribuente) -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="JYPBYVRODM",Visible=.t., Left=51, Top=242,;
    Alignment=1, Width=250, Height=15,;
    Caption="Codice fiscale societ� dichiarante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QRAORRPHCG",Visible=.t., Left=349, Top=214,;
    Alignment=1, Width=45, Height=15,;
    Caption="Carica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="WZPWBBFOGR",Visible=.t., Left=107, Top=279,;
    Alignment=1, Width=425, Height=15,;
    Caption="- Mancata corrispond. dei dati da trasmettere con quelli della dichiarazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="PBZPKGZNDI",Visible=.t., Left=104, Top=132,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="ISFFISILZU",Visible=.t., Left=372, Top=54,;
    Alignment=1, Width=97, Height=15,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="NNINIBIMDD",left=6, top=48, width=611,height=0

  add object oBox_1_25 as StdBox with uid="AGTVLHADQF",left=107, top=84, width=510,height=1

  add object oBox_1_41 as StdBox with uid="YBAISFLTRY",left=107, top=276, width=510,height=1
enddefine
define class tgsar_acdPag2 as StdContainer
  Width  = 694
  height = 315
  stdWidth  = 694
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAITOTATT_2_9 as StdField with uid="FXKWZJLOAC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_AITOTATT", cQueryName = "AITOTATT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni attive (al netto dell'IVA)",;
    HelpContextID = 726618,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=364, Top=28, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITOTATT_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_NIM_2_12 as StdField with uid="EVQQKSMDPH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AITA_NIM", cQueryName = "AITA_NIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "operazioni non imponibili",;
    HelpContextID = 38988205,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=55, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_NIM_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_ESE_2_14 as StdField with uid="UEUKYODYCB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_AITA_ESE", cQueryName = "AITA_ESE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "operazioni esenti",;
    HelpContextID = 78452299,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=74, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_ESE_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_CIN_2_16 as StdField with uid="GSRCBTEZQZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AITA_CIN", cQueryName = "AITA_CIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "cessioni intracomunitarie di beni",;
    HelpContextID = 223537580,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=93, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_CIN_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITOTPAS_2_27 as StdField with uid="ZJWEWPUSTU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_AITOTPAS", cQueryName = "AITOTPAS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni passive (al netto dell'IVA)",;
    HelpContextID = 252384857,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=364, Top=141, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITOTPAS_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_NIM_2_30 as StdField with uid="DUSIPJRUKF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_AITP_NIM", cQueryName = "AITP_NIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti non imponibili",;
    HelpContextID = 38005165,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=168, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_NIM_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_ESE_2_32 as StdField with uid="RFNDDYZTRE",rtseq=36,rtrep=.f.,;
    cFormVar = "w_AITP_ESE", cQueryName = "AITP_ESE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti esenti",;
    HelpContextID = 79435339,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=187, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_ESE_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_CIN_2_34 as StdField with uid="URVVIEBZZE",rtseq=37,rtrep=.f.,;
    cFormVar = "w_AITP_CIN", cQueryName = "AITP_CIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti intracomunitari di beni",;
    HelpContextID = 222554540,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=206, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_CIN_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIMPORO_2_36 as StdField with uid="ZCEISVAEWM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AIIMPORO", cQueryName = "AIIMPORO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importazione oro e argento",;
    HelpContextID = 231237205,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=342, Top=267, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIMPORO_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVAORO_2_37 as StdField with uid="UTOPEPFQCK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_AIIVAORO", cQueryName = "AIIVAORO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta importazione oro e argento",;
    HelpContextID = 216098389,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=553, Top=267, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVAORO_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc


  add object oBtn_2_38 as StdButton with uid="MISXTYVFNG",left=7, top=266, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=2;
    , ToolTipText = "Conferma";
    , HelpContextID = 215002662;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_38.Click()
      with this.Parent.oContained
        GSAR_BCD(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP) and not empty(.w_NOMEFILE) and .w_AIINOLTRO<>'S')
      endwith
    endif
  endfunc

  add object oStr_2_2 as StdString with uid="TXSCNQZRPO",Visible=.t., Left=7, Top=9,;
    Alignment=0, Width=57, Height=15,;
    Caption="Sez. II"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_3 as StdString with uid="AAXPWXMPHW",Visible=.t., Left=7, Top=26,;
    Alignment=0, Width=114, Height=15,;
    Caption="DATI RELATIVI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="IADGATZQAJ",Visible=.t., Left=183, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="- OPERAZIONI ATTIVE -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="TLZBPAXWTX",Visible=.t., Left=126, Top=28,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD1"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_6 as StdString with uid="VBWOMBQDID",Visible=.t., Left=7, Top=41,;
    Alignment=0, Width=135, Height=15,;
    Caption="ALLE OPERAZIONI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="NHBLHRHQKX",Visible=.t., Left=7, Top=55,;
    Alignment=0, Width=77, Height=15,;
    Caption="EFFETTUATE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="DUDRPXQVWR",Visible=.t., Left=163, Top=28,;
    Alignment=1, Width=198, Height=15,;
    Caption="Totale operazioni attive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="WCQAYFMUMV",Visible=.t., Left=377, Top=55,;
    Alignment=1, Width=173, Height=15,;
    Caption="operazioni non imponibili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="YMBGPTMEUX",Visible=.t., Left=377, Top=74,;
    Alignment=1, Width=173, Height=15,;
    Caption="operazioni esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="JYQFNGVJJN",Visible=.t., Left=336, Top=93,;
    Alignment=1, Width=214, Height=15,;
    Caption="cessioni intracomunitarie di beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="YZGADIHYKQ",Visible=.t., Left=341, Top=55,;
    Alignment=1, Width=50, Height=15,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="PFTFXPHCJU",Visible=.t., Left=168, Top=123,;
    Alignment=0, Width=465, Height=15,;
    Caption="- OPERAZIONI PASSIVE -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_20 as StdString with uid="KTZXHWLCTE",Visible=.t., Left=126, Top=267,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD3"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="TOEXMFMTNP",Visible=.t., Left=210, Top=267,;
    Alignment=1, Width=128, Height=15,;
    Caption="Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="FFOFAKHHTH",Visible=.t., Left=168, Top=244,;
    Alignment=0, Width=465, Height=15,;
    Caption="- IMPORTAZIONI DI ORO E ARGENTO SENZA PAGAMENTO DELL'IVA IN DOGANA -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="MCYLSKEZOU",Visible=.t., Left=493, Top=267,;
    Alignment=1, Width=57, Height=15,;
    Caption="Imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="LPAYTKJWJM",Visible=.t., Left=126, Top=141,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD2"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_26 as StdString with uid="THATHJKVPC",Visible=.t., Left=156, Top=141,;
    Alignment=1, Width=210, Height=15,;
    Caption="Totale operazioni passive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="WDVWPKYLSW",Visible=.t., Left=377, Top=168,;
    Alignment=1, Width=173, Height=15,;
    Caption="acquisti non imponibili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="VQKCUVQPOW",Visible=.t., Left=377, Top=187,;
    Alignment=1, Width=173, Height=15,;
    Caption="acquisti esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="TGWPQCBTHJ",Visible=.t., Left=339, Top=206,;
    Alignment=1, Width=211, Height=15,;
    Caption="acquisti intracomunitari di beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="ZSJMPJBSUC",Visible=.t., Left=341, Top=168,;
    Alignment=1, Width=50, Height=15,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oBox_2_1 as StdBox with uid="VBPHDMPQWM",left=7, top=6, width=684,height=1

  add object oBox_2_10 as StdBox with uid="YJUYHQOJVK",left=364, top=51, width=323,height=1

  add object oBox_2_18 as StdBox with uid="OGGQMNZBBB",left=168, top=118, width=523,height=1

  add object oBox_2_22 as StdBox with uid="AYYPAJTVHB",left=168, top=239, width=521,height=1

  add object oBox_2_28 as StdBox with uid="SGQXXLXJYC",left=364, top=164, width=323,height=1
enddefine
define class tgsar_acdPag3 as StdContainer
  Width  = 694
  height = 315
  stdWidth  = 694
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAIIVAESI_3_8 as StdField with uid="YSQHCNNRFG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_AIIVAESI", cQueryName = "AIIVAESI",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA esigibile",;
    HelpContextID = 48326223,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=316, Top=12, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVAESI_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVADET_3_12 as StdField with uid="VLWRQVFKQB",rtseq=41,rtrep=.f.,;
    cFormVar = "w_AIIVADET", cQueryName = "AIIVADET",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA detratta",;
    HelpContextID = 31549018,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=549, Top=47, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVADET_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVADOV_3_16 as StdField with uid="CIUONFALZD",rtseq=42,rtrep=.f.,;
    cFormVar = "w_AIIVADOV", cQueryName = "AIIVADOV",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA dovuta",;
    HelpContextID = 236886436,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=317, Top=81, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"', tabstop=.f.

  add object oAIIVACRE_3_18 as StdField with uid="UCFFQVBDBC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AIIVACRE", cQueryName = "AIIVACRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a credito",;
    HelpContextID = 14771787,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=549, Top=81, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"', tabstop=.f.

  add object oAIFIRMAC_3_19 as StdCheck with uid="EBXIVLKYQK",rtseq=44,rtrep=.f.,left=174, top=122, caption="Firma",;
    ToolTipText = "Firma comunicazione presente se il flag � valorizzato",;
    HelpContextID = 199505481,;
    cFormVar="w_AIFIRMAC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIFIRMAC_3_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRMAC_3_19.GetRadio()
    this.Parent.oContained.w_AIFIRMAC = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRMAC_3_19.SetRadio()
    this.Parent.oContained.w_AIFIRMAC=trim(this.Parent.oContained.w_AIFIRMAC)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRMAC=='1',1,;
      0)
  endfunc

  func oAIFIRMAC_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAICOFINT_3_21 as StdField with uid="CNXKMRFHEG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_AICOFINT", cQueryName = "AICOFINT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario",;
    HelpContextID = 148240806,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=361, Top=166, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oAICOFINT_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' AND .w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02')
    endwith
   endif
  endfunc

  func oAICOFINT_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AICOFINT, "CF"))
    endwith
    return bRes
  endfunc

  add object oAIALBCAF_3_25 as StdField with uid="EVJZTMALPD",rtseq=46,rtrep=.f.,;
    cFormVar = "w_AIALBCAF", cQueryName = "AIALBCAF",nZero=5,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo dei C.A.F.",;
    HelpContextID = 15132236,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=635, Top=166, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oAIALBCAF_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIINVCON_3_30 as StdCheck with uid="PWNQJWDNCW",rtseq=47,rtrep=.f.,left=525, top=210, caption="",;
    ToolTipText = "Impegno a presentare la comun. predisposta dal contribuente",;
    HelpContextID = 232167852,;
    cFormVar="w_AIINVCON", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIINVCON_3_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIINVCON_3_30.GetRadio()
    this.Parent.oContained.w_AIINVCON = this.RadioValue()
    return .t.
  endfunc

  func oAIINVCON_3_30.SetRadio()
    this.Parent.oContained.w_AIINVCON=trim(this.Parent.oContained.w_AIINVCON)
    this.value = ;
      iif(this.Parent.oContained.w_AIINVCON=='1',1,;
      0)
  endfunc

  func oAIINVCON_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIINVSOG_3_33 as StdCheck with uid="CUENWICIZG",rtseq=48,rtrep=.f.,left=525, top=255, caption="",;
    ToolTipText = "Impegno a presentare la comun. del contribuente predisposta dal soggetto",;
    HelpContextID = 232167859,;
    cFormVar="w_AIINVSOG", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIINVSOG_3_33.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIINVSOG_3_33.GetRadio()
    this.Parent.oContained.w_AIINVSOG = this.RadioValue()
    return .t.
  endfunc

  func oAIINVSOG_3_33.SetRadio()
    this.Parent.oContained.w_AIINVSOG=trim(this.Parent.oContained.w_AIINVSOG)
    this.value = ;
      iif(this.Parent.oContained.w_AIINVSOG=='1',1,;
      0)
  endfunc

  func oAIINVSOG_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIDATIMP_3_36 as StdField with uid="ZRWZBFZIHN",rtseq=49,rtrep=.f.,;
    cFormVar = "w_AIDATIMP", cQueryName = "AIDATIMP",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 134474154,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=281, Top=292

  func oAIDATIMP_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIFIRMA_3_40 as StdCheck with uid="PDPPGRMLBD",rtseq=50,rtrep=.f.,left=426, top=291, caption="Firma",;
    ToolTipText = "Firma intermediario presente se il flag � valorizzato",;
    HelpContextID = 199505414,;
    cFormVar="w_AIFIRMA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIFIRMA_3_40.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRMA_3_40.GetRadio()
    this.Parent.oContained.w_AIFIRMA = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRMA_3_40.SetRadio()
    this.Parent.oContained.w_AIFIRMA=trim(this.Parent.oContained.w_AIFIRMA)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRMA=='1',1,;
      0)
  endfunc

  func oAIFIRMA_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oStr_3_2 as StdString with uid="RMJZWJTQOV",Visible=.t., Left=7, Top=9,;
    Alignment=0, Width=60, Height=15,;
    Caption="Sez. III"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_3 as StdString with uid="EBDAYAJJFI",Visible=.t., Left=7, Top=22,;
    Alignment=0, Width=100, Height=15,;
    Caption="DETERMINAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_4 as StdString with uid="OQACNORRGQ",Visible=.t., Left=174, Top=11,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD4"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_5 as StdString with uid="BMIIHVZGBP",Visible=.t., Left=7, Top=37,;
    Alignment=0, Width=153, Height=15,;
    Caption="DELL'IVA DOVUTA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_6 as StdString with uid="FDDPEDHAOR",Visible=.t., Left=7, Top=51,;
    Alignment=0, Width=91, Height=15,;
    Caption="O A CREDITO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="WRYJUBGGWS",Visible=.t., Left=235, Top=12,;
    Alignment=1, Width=76, Height=15,;
    Caption="IVA esigibile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="SMYQITFPKR",Visible=.t., Left=174, Top=46,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD5"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_11 as StdString with uid="IVTORGEFKB",Visible=.t., Left=206, Top=47,;
    Alignment=1, Width=105, Height=15,;
    Caption="IVA detratta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="SNFDTCEZAK",Visible=.t., Left=175, Top=81,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD6"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_15 as StdString with uid="CXEYBTUDBS",Visible=.t., Left=209, Top=81,;
    Alignment=1, Width=103, Height=15,;
    Caption="IVA dovuta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="XSVEBBPESF",Visible=.t., Left=460, Top=81,;
    Alignment=1, Width=85, Height=15,;
    Caption="o a credito:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="EDYHSBFJMD",Visible=.t., Left=174, Top=166,;
    Alignment=1, Width=183, Height=15,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="ODJPTQPLQI",Visible=.t., Left=7, Top=162,;
    Alignment=0, Width=114, Height=15,;
    Caption="IMPEGNO ALLA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_24 as StdString with uid="TTJFHXMJXO",Visible=.t., Left=510, Top=166,;
    Alignment=1, Width=121, Height=15,;
    Caption="Num. iscr. C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="LXOQKWZARZ",Visible=.t., Left=7, Top=177,;
    Alignment=0, Width=95, Height=15,;
    Caption="PRESENTAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_27 as StdString with uid="SYFBNJLGZH",Visible=.t., Left=7, Top=192,;
    Alignment=0, Width=91, Height=15,;
    Caption="TELEMATICA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="BLSZTJGKCM",Visible=.t., Left=195, Top=210,;
    Alignment=1, Width=313, Height=15,;
    Caption="predisposta dal contribuente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="TAUYYJEDXQ",Visible=.t., Left=175, Top=240,;
    Alignment=0, Width=316, Height=15,;
    Caption="Impegno a presentare in via telematica la comunicazione"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="REKYKFYKFY",Visible=.t., Left=186, Top=255,;
    Alignment=1, Width=322, Height=15,;
    Caption="del contribuente predisposta dal soggetto che la trasmette:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="UEIMGJSZCI",Visible=.t., Left=176, Top=194,;
    Alignment=0, Width=316, Height=15,;
    Caption="Impegno a presentare in via telematica la comunicazione"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="FGGRWLAFDY",Visible=.t., Left=141, Top=292,;
    Alignment=1, Width=137, Height=15,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="BURNFGFAQT",Visible=.t., Left=8, Top=211,;
    Alignment=0, Width=71, Height=15,;
    Caption="(Rec. tipo B)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="OHWMHBZSCY",Visible=.t., Left=7, Top=118,;
    Alignment=0, Width=114, Height=15,;
    Caption="FIRMA DELLA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_43 as StdString with uid="BLKSXVVDQW",Visible=.t., Left=7, Top=133,;
    Alignment=0, Width=101, Height=15,;
    Caption="COMUNICAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_1 as StdBox with uid="CUUCPPGSJO",left=7, top=6, width=685,height=1

  add object oBox_3_9 as StdBox with uid="DVFJPANOYV",left=174, top=35, width=514,height=1

  add object oBox_3_13 as StdBox with uid="AMDVBONWYM",left=174, top=71, width=514,height=1

  add object oBox_3_22 as StdBox with uid="UCXOJHREQX",left=7, top=111, width=685,height=0

  add object oBox_3_29 as StdBox with uid="KTKRBGSORJ",left=174, top=190, width=514,height=1

  add object oBox_3_32 as StdBox with uid="XDFEQFIDGW",left=173, top=232, width=514,height=1

  add object oBox_3_38 as StdBox with uid="ERBOLHGCXV",left=171, top=283, width=514,height=1

  add object oBox_3_41 as StdBox with uid="QFSRKXIJKT",left=7, top=152, width=685,height=0
enddefine
define class tgsar_acdPag4 as StdContainer
  Width  = 694
  height = 315
  stdWidth  = 694
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAIPERAZI_4_2 as StdCombo with uid="ROSXVJMFVI",rtseq=51,rtrep=.f.,left=7,top=9,width=136,height=21;
    , ToolTipText = "Test se persona fisica";
    , HelpContextID = 2042289;
    , cFormVar="w_AIPERAZI",RowSource=""+""+L_comb1+","+""+L_comb2+"", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAIPERAZI_4_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oAIPERAZI_4_2.GetRadio()
    this.Parent.oContained.w_AIPERAZI = this.RadioValue()
    return .t.
  endfunc

  func oAIPERAZI_4_2.SetRadio()
    this.Parent.oContained.w_AIPERAZI=trim(this.Parent.oContained.w_AIPERAZI)
    this.value = ;
      iif(this.Parent.oContained.w_AIPERAZI=='S',1,;
      iif(this.Parent.oContained.w_AIPERAZI=='N',2,;
      0))
  endfunc

  func oAIPERAZI_4_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc


  add object oObj_4_3 as cp_runprogram with uid="KASFICCGHH",left=5, top=329, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BCD('PERF')",;
    cEvent = "w_AIPERAZI Changed",;
    nPag=4;
    , HelpContextID = 68325350

  add object oAICOGNOM_4_13 as StdField with uid="JDNRKIZSLG",rtseq=52,rtrep=.f.,;
    cFormVar = "w_AICOGNOM", cQueryName = "AICOGNOM",;
    bObbl = .f. , nPag = 4, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 63306157,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=98, Top=45, InputMask=replicate('X',24)

  func oAICOGNOM_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAICOGNOM_4_13.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAINOME_4_14 as StdField with uid="QTVLSFBHQY",rtseq=53,rtrep=.f.,;
    cFormVar = "w_AINOME", cQueryName = "AINOME",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 207964666,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=324, Top=45, InputMask=replicate('X',20)

  func oAINOME_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAINOME_4_14.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc


  add object oAISESSO_4_15 as StdCombo with uid="XZOVQUMAWB",rtseq=54,rtrep=.f.,left=527,top=42,width=92,height=21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 235862522;
    , cFormVar="w_AISESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAISESSO_4_15.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    '')))
  endfunc
  func oAISESSO_4_15.GetRadio()
    this.Parent.oContained.w_AISESSO = this.RadioValue()
    return .t.
  endfunc

  func oAISESSO_4_15.SetRadio()
    this.Parent.oContained.w_AISESSO=trim(this.Parent.oContained.w_AISESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AISESSO=='M',1,;
      iif(this.Parent.oContained.w_AISESSO=='F',2,;
      0))
  endfunc

  func oAISESSO_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISESSO_4_15.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAICOMUNE_4_16 as StdField with uid="XDHWWDJPBR",rtseq=55,rtrep=.f.,;
    cFormVar = "w_AICOMUNE", cQueryName = "AICOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 208009653,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=144, Top=75, InputMask=replicate('X',40)

  func oAICOMUNE_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAICOMUNE_4_16.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIDATNAS_4_17 as StdField with uid="YYRQQYMXRH",rtseq=56,rtrep=.f.,;
    cFormVar = "w_AIDATNAS", cQueryName = "AIDATNAS",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 217847385,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=144, Top=104

  func oAIDATNAS_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIDATNAS_4_17.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAISIGLA_4_18 as StdField with uid="LCTQLXNIKY",rtseq=57,rtrep=.f.,;
    cFormVar = "w_AISIGLA", cQueryName = "AISIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 171247110,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=394, Top=104, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAISIGLA_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISIGLA_4_18.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIRESCOM_4_19 as StdField with uid="AZCCQBHYHJ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AIRESCOM", cQueryName = "AIRESCOM",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 235866541,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=270, Top=134, InputMask=replicate('X',40)

  func oAIRESCOM_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIRESCOM_4_19.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIRESSIG_4_20 as StdField with uid="UDWAUBRUGW",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AIRESSIG", cQueryName = "AIRESSIG",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 235866547,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=186, Top=164, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAIRESSIG_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIRESSIG_4_20.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIINDIRI_4_21 as StdField with uid="QDNHPLRXOY",rtseq=60,rtrep=.f.,;
    cFormVar = "w_AIINDIRI", cQueryName = "AIINDIRI",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 118056527,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=270, Top=164, InputMask=replicate('X',35)

  func oAIINDIRI_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIINDIRI_4_21.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAI___CAP_4_22 as StdField with uid="UKNJUVWKDY",rtseq=61,rtrep=.f.,;
    cFormVar = "w_AI___CAP", cQueryName = "AI___CAP",;
    bObbl = .f. , nPag = 4, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 46909014,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=575, Top=162, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oAI___CAP_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAI___CAP_4_22.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIDENOMI_4_32 as StdField with uid="UJKRUDNGLA",rtseq=62,rtrep=.f.,;
    cFormVar = "w_AIDENOMI", cQueryName = "AIDENOMI",;
    bObbl = .f. , nPag = 4, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 39840177,;
   bGlobalFont=.t.,;
    Height=21, Width=436, Left=123, Top=56, InputMask=replicate('X',60)

  func oAIDENOMI_4_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S'and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIDENOMI_4_32.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISECOMU_4_33 as StdField with uid="BKBJRKVTWJ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AISECOMU", cQueryName = "AISECOMU",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 51313061,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=188, Top=89, InputMask=replicate('X',40)

  func oAISECOMU_4_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISECOMU_4_33.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISESIGL_4_34 as StdField with uid="VSOLXNAMDS",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AISESIGL", cQueryName = "AISESIGL",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 133236306,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=596, Top=89, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAISESIGL_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISESIGL_4_34.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISEIND2_4_35 as StdField with uid="MDTNMVBSXW",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AISEIND2", cQueryName = "AISEIND2",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 206636600,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=188, Top=120, InputMask=replicate('X',35)

  func oAISEIND2_4_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISEIND2_4_35.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISE_CAP_4_36 as StdField with uid="EAHXVXRRTY",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AISE_CAP", cQueryName = "AISE_CAP",;
    bObbl = .f. , nPag = 4, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 45155926,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=575, Top=120, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oAISE_CAP_4_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISE_CAP_4_36.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISERCOM_4_37 as StdField with uid="JMDZCBXAPQ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_AISERCOM", cQueryName = "AISERCOM",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 236911021,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=188, Top=152, InputMask=replicate('X',40)

  func oAISERCOM_4_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERCOM_4_37.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISERSIG_4_38 as StdField with uid="UVTZWHBNBI",rtseq=68,rtrep=.f.,;
    cFormVar = "w_AISERSIG", cQueryName = "AISERSIG",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore:",;
    HelpContextID = 236911027,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=596, Top=152, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAISERSIG_4_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERSIG_4_38.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISERIND_4_39 as StdField with uid="ILQRWWIPUH",rtseq=69,rtrep=.f.,;
    cFormVar = "w_AISERIND", cQueryName = "AISERIND",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 136247734,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=188, Top=182, InputMask=replicate('X',35)

  func oAISERIND_4_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERIND_4_39.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISERCAP_4_40 as StdField with uid="LEBJRKCUXZ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_AISERCAP", cQueryName = "AISERCAP",;
    bObbl = .f. , nPag = 4, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 31524438,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=575, Top=182, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  func oAISERCAP_4_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERCAP_4_40.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc


  add object oBtn_4_44 as StdButton with uid="TQEMIMWBDV",left=456, top=293, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file trasmiva.";
    , HelpContextID = 109471274;
  , bGlobalFont=.t.

    proc oBtn_4_44.Click()
      with this.Parent.oContained
        GSAR_BCD(this.Parent.oContained,"FILE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_46 as StdButton with uid="AYLATODQCL",left=536, top=268, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Conferma";
    , HelpContextID = 33739366;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_46.Click()
      with this.Parent.oContained
        GSAR_BCD(this.Parent.oContained,"GENE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP) and not empty(.w_NOMEFILE))
      endwith
    endif
  endfunc


  add object oBtn_4_47 as StdButton with uid="JPJRCCQWDZ",left=590, top=268, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 32117286;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_47.Click()
      with this.Parent.oContained
        GSAR_BCD(this.Parent.oContained,"STAM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP))
      endwith
    endif
  endfunc

  add object oNOMEFILE_4_48 as StdField with uid="OKVJAIJGZX",rtseq=72,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome del percorso e del file per la creazione memorizzazione dati dell'IVA",;
    HelpContextID = 148853477,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=145, Top=292, InputMask=replicate('X',200)

  add object oStr_4_1 as StdString with uid="NFCEYUFYCL",Visible=.t., Left=36, Top=47,;
    Alignment=1, Width=58, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_4_1.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_4 as StdString with uid="QZIQTOUJSH",Visible=.t., Left=283, Top=47,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_4_4.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_5 as StdString with uid="RCOARSDNRS",Visible=.t., Left=484, Top=47,;
    Alignment=1, Width=39, Height=15,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_4_5.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_6 as StdString with uid="YSDSMJPCUB",Visible=.t., Left=219, Top=106,;
    Alignment=1, Width=170, Height=15,;
    Caption="Sigla provincia di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_6.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_7 as StdString with uid="GKJCARIUFY",Visible=.t., Left=27, Top=77,;
    Alignment=1, Width=114, Height=15,;
    Caption="Comune di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_7.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_8 as StdString with uid="PHEDRNQNJC",Visible=.t., Left=34, Top=136,;
    Alignment=1, Width=232, Height=15,;
    Caption="Comune di residenza o domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_8.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_9 as StdString with uid="XILHRWUEWW",Visible=.t., Left=209, Top=166,;
    Alignment=1, Width=57, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  func oStr_4_9.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_10 as StdString with uid="DUKZPDUBKV",Visible=.t., Left=11, Top=166,;
    Alignment=1, Width=176, Height=15,;
    Caption="Sigla provincia di residenza:"  ;
  , bGlobalFont=.t.

  func oStr_4_10.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_11 as StdString with uid="HDAULOJTTR",Visible=.t., Left=536, Top=164,;
    Alignment=1, Width=39, Height=15,;
    Caption="C.A.P.:"  ;
  , bGlobalFont=.t.

  func oStr_4_11.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_12 as StdString with uid="EAAJCSCSZQ",Visible=.t., Left=27, Top=106,;
    Alignment=1, Width=115, Height=15,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_12.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_23 as StdString with uid="HBBEQDXSVV",Visible=.t., Left=30, Top=58,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_24 as StdString with uid="KORLGBTOUD",Visible=.t., Left=36, Top=91,;
    Alignment=1, Width=149, Height=15,;
    Caption="Comune della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_24.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_25 as StdString with uid="HACQCYBOIB",Visible=.t., Left=476, Top=91,;
    Alignment=1, Width=113, Height=15,;
    Caption="Sigla provincia:"  ;
  , bGlobalFont=.t.

  func oStr_4_25.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_26 as StdString with uid="AMEXRKXOGO",Visible=.t., Left=40, Top=122,;
    Alignment=1, Width=145, Height=15,;
    Caption="Indirizzo della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_26.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_27 as StdString with uid="ODWVYESOUX",Visible=.t., Left=443, Top=122,;
    Alignment=1, Width=125, Height=15,;
    Caption="C.A.P. sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_27.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_28 as StdString with uid="YTPGEBOZJU",Visible=.t., Left=24, Top=154,;
    Alignment=1, Width=159, Height=15,;
    Caption="Comune di domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_28.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_29 as StdString with uid="XEGQVMBKOF",Visible=.t., Left=477, Top=154,;
    Alignment=1, Width=113, Height=15,;
    Caption="Sigla provincia:"  ;
  , bGlobalFont=.t.

  func oStr_4_29.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_30 as StdString with uid="KZTYYYGGXI",Visible=.t., Left=26, Top=184,;
    Alignment=1, Width=159, Height=15,;
    Caption="Indirizzo di domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_30.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_31 as StdString with uid="KJHEMCJKTM",Visible=.t., Left=450, Top=184,;
    Alignment=1, Width=122, Height=15,;
    Caption="C.A.P. domic.fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_31.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_45 as StdString with uid="SORSEKCKVG",Visible=.t., Left=75, Top=294,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oBox_4_41 as StdBox with uid="NLQRMJDLUL",left=7, top=36, width=622,height=0

  add object oBox_4_42 as StdBox with uid="MVNLUETLMF",left=11, top=36, width=577,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_acd','MOD_COAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AIANNIMP=MOD_COAN.AIANNIMP";
  +" and "+i_cAliasName2+".AIDATINV=MOD_COAN.AIDATINV";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
