* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bto                                                        *
*              Controlli accise                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-04                                                      *
* Last revis.: 2006-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Pparam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bto",oParentObject,m.Pparam)
return(i_retval)

define class tgscg_bto as StdBatch
  * --- Local variables
  Pparam = space(1)
  w_TOT = 0
  w_PADRE = .NULL.
  w_MESSAGE = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TOT = 0
    if this.Pparam="S"
      this.w_PADRE = this.oparentobject.GSCG_MAT
      this.w_PADRE.Markpos()     
      this.w_PADRE.FirstRow()     
      do while NOT this.w_PADRE.Eof_Trs() AND this.w_PADRE.fullrow()
        this.w_PADRE.SetRow()     
        if EMPTY( this.w_PADRE.w_AFTRIBUT)
          this.w_MESSAGE = ah_msgformat("Inserire un codice tributo accise")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESSAGE
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.w_PADRE.w_AF__ANNO)
          this.w_MESSAGE = ah_msgformat ("Inserire l' anno accise")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESSAGE
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.w_PADRE.w_AF__MESE) OR val ( this.w_PADRE.w_AF__MESE)>12 OR val( this.w_PADRE.w_AF__MESE)<1
          this.w_MESSAGE = ah_msgformat ("Inserire un mese valido per le accise")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESSAGE
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.w_PADRE.w_AFCODPRO)
          this.w_MESSAGE = ah_msgformat ("Inserire un codice provincia accise")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESSAGE
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.w_PADRE.w_AFCODICE)
          this.w_MESSAGE = ah_msgformat ("Inserire un codice identificativo accise")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESSAGE
          i_retcode = 'stop'
          return
        endif
        this.w_PADRE.Nextrow()     
      enddo
      this.w_PADRE.Repos()     
      this.w_PADRE.SetRow()     
    else
      this.w_PADRE = this.oparentobject
      this.w_PADRE.Markpos()     
      this.w_PADRE.FirstRow()     
      do while NOT this.w_PADRE.Eof_Trs() AND this.w_PADRE.fullrow()
        this.w_PADRE.SetRow()     
        this.w_TOT = this.w_TOT + this.oParentObject.w_AFIMPORT
        this.w_PADRE.Nextrow()     
      enddo
      this.w_PADRE.Repos()     
      this.w_PADRE.SetRow()     
      this.oparentobject.oparentobject.w_MFSALAEN=this.w_TOT 
 this.oparentobject.oparentobject.refresh() 
 this.oparentobject.oparentobject.mcalc(.T.)
    endif
  endproc


  proc Init(oParentObject,Pparam)
    this.Pparam=Pparam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Pparam"
endproc
