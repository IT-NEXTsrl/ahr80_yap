* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bec                                                        *
*              Eliminazione calendari                                          *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-26                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bec",oParentObject,m.Operazione)
return(i_retval)

define class tgsar_bec as StdBatch
  * --- Local variables
  Operazione = space(2)
  PunPAD = .NULL.
  ZOOMInPAD = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_CALSEL = space(5)
  w_ANNSEL = 0
  * --- WorkFile variables
  CAL_AZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Operazione
    * --- Puntatore al padre
    this.PunPAD = this.oParentObject
    * --- Puntatore a zoom dentro Maschera Padre
    this.ZOOMInPAD = this.oParentObject.w_ElenZoom
    this.NC = this.ZoomInPAD.cCursor
    * --- Variabili locali
    do case
      case this.Operazione="IN"
        * --- Esegue Interrograzione
        this.PunPAD.NotifyEvent("Rinfresca")     
      case this.Operazione="OK"
        if used(this.NC)
          select count(*) as Numero from (this.NC) where xChk=1 into cursor temp
          go top
          this.w_nRecSel = temp.Numero
          use
          if this.w_nRecSel>0
            ah_msg("Eliminazione in corso...")
            * --- Legge cursore di selezione ...
            select (this.NC)
            go top
            SCAN for xChk=1
            * --- Try
            local bErr_038C8F10
            bErr_038C8F10=bTrsErr
            this.Try_038C8F10()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Impossibile cancellare il calendario %1",16,"",this.w_CALSEL)
            endif
            bTrsErr=bTrsErr or bErr_038C8F10
            * --- End
            endscan
            * --- Riesegue Interrograzione
            this.PunPAD.NotifyEvent("Rinfresca")     
            wait clear
          else
            ah_ErrorMsg("Non sono stati selezionati calendari da cancellare",48)
          endif
        endif
      case this.Operazione="SE" OR this.Operazione="DE" OR this.Operazione="IS"
        UPDATE (this.NC) SET XCHK=ICASE(this.Operazione="SE", 1, this.Operazione="DE", 0, IIF(XCHK=1,0,1))
    endcase
  endproc
  proc Try_038C8F10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_CALSEL = CACODCAL
    this.w_ANNSEL = CA__ANNO
    * --- Select from CAL_AZIE
    i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAL_AZIE ";
          +" where CACODCAL="+cp_ToStrODBC(this.w_CALSEL)+"";
           ,"_Curs_CAL_AZIE")
    else
      select * from (i_cTable);
       where CACODCAL=this.w_CALSEL;
        into cursor _Curs_CAL_AZIE
    endif
    if used('_Curs_CAL_AZIE')
      select _Curs_CAL_AZIE
      locate for 1=1
      do while not(eof())
      if year(cp_todate(_Curs_CAL_AZIE.CAGIORNO)) = this.w_ANNSEL
        * --- Delete from CAL_AZIE
        i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CACODCAL = "+cp_ToStrODBC(_Curs_CAL_AZIE.CACODCAL);
                +" and CAGIORNO = "+cp_ToStrODBC(_Curs_CAL_AZIE.CAGIORNO);
                 )
        else
          delete from (i_cTable) where;
                CACODCAL = _Curs_CAL_AZIE.CACODCAL;
                and CAGIORNO = _Curs_CAL_AZIE.CAGIORNO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
        select _Curs_CAL_AZIE
        continue
      enddo
      use
    endif
    * --- Riseleziona Cursore Zoom
    select (this.NC)
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,Operazione)
    this.Operazione=Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAL_AZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAL_AZIE')
      use in _Curs_CAL_AZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Operazione"
endproc
