* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bac                                                        *
*              STAMPA CONTRIBUTI APPLICATI/SUBITI                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-11                                                      *
* Last revis.: 2008-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bac",oParentObject)
return(i_retval)

define class tgsar1bac as StdBatch
  * --- Local variables
  w_PUNPAD = .NULL.
  w_NELABCONTI = 0
  w_NELABARTICOLI = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  ARTICOLTMP_idx=0
  CONTITMP_idx=0
  OUTPUTMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PUNPAD = this.oParentObject.GSVE1MDA
    this.w_NELABCONTI = 0
    this.w_NELABARTICOLI = 0
    if UPPER( G_APPLICATION ) = "AD HOC ENTERPRISE" AND VARTYPE( this.w_PUNPAD.cTrsName ) <> "U"
      * --- Attributi di selezione
      select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) AND NOT DELETED() into Cursor Elab 
      this.w_NELABARTICOLI = RECCOUNT("Elab")
      * --- Attributi di selezione
      select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) AND NOT DELETED() into Cursor Elab 
      this.w_NELABCONTI = RECCOUNT("Elab")
      if USED( "ELAB" )
        SELECT ELAB
        USE
      endif
    endif
    if UPPER( G_APPLICATION ) = "AD HOC ENTERPRISE" AND ( this.w_NELABCONTI > 0 OR this.w_NELABARTICOLI > 0 )
      * --- Se � stata richiesta una selezione sugli attributi dei clienti/fornitori
      if this.w_NELABCONTI > 0
        GSAR2BAC(this, "CUCL", this.w_PUNPAD, this.oParentObject.w_PUNTUALE , this.oParentObject.w_TIPSELE , this.oParentObject.w_CICLO )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        CURTOTAB( "__TMP__" , "CONTITMP" )
        if USED( "__TMP__" )
          SELECT __TMP__
          USE
        endif
      else
        * --- Create temporary table CONTITMP
        i_nIdx=cp_AddTableDef('CONTITMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.CONTI_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"ANTIPCON, ANCODICE "," from "+i_cTable;
              +" where ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_CLIFOR)+"";
              )
        this.CONTITMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Se � stata richiesta una selezione sugli attributi degli articoli
      if this.w_NELABARTICOLI > 0
        GSAR2BAC(this, "CUAR", this.w_PUNPAD, this.oParentObject.w_PUNTUALE , this.oParentObject.w_TIPSELE , this.oParentObject.w_CICLO )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        CURTOTAB( "__TMP__" , "ARTICOLTMP" )
        if USED( "__TMP__" )
          SELECT __TMP__
          USE
        endif
      else
        * --- Create temporary table ARTICOLTMP
        i_nIdx=cp_AddTableDef('ARTICOLTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.ART_ICOL_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"ARCODART "," from "+i_cTable;
              +" where ARTIPART = 'FM'";
              )
        this.ARTICOLTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSAR9SAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSAR0SAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    VX_EXEC( this.oParentObject.w_OQRY +","+ this.oParentObject.w_OREP , THIS.OPARENTOBJECT )
    if USED( "__TMP__" )
      SELECT __TMP__
      USE
    endif
    * --- Drop temporary table ARTICOLTMP
    i_nIdx=cp_GetTableDefIdx('ARTICOLTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ARTICOLTMP')
    endif
    * --- Drop temporary table CONTITMP
    i_nIdx=cp_GetTableDefIdx('CONTITMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('CONTITMP')
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='*ARTICOLTMP'
    this.cWorkTables[4]='*CONTITMP'
    this.cWorkTables[5]='*OUTPUTMP'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
