* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kdp                                                        *
*              Dettaglio riferimenti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-22                                                      *
* Last revis.: 2008-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kdp",oParentObject))

* --- Class definition
define class tgsar_kdp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 406
  Height = 72
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-01-22"
  HelpContextID=186026135
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  PAG_AMEN_IDX = 0
  cPrg = "gsar_kdp"
  cComment = "Dettaglio riferimenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMDIC = 0
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_SERDIC = space(3)
  w_DDRFDICH = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kdpPag1","gsar_kdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAG_AMEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMDIC=0
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_SERDIC=space(3)
      .w_DDRFDICH=space(15)
      .w_NUMDIC=oParentObject.w_NUMDIC
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DATDIC=oParentObject.w_DATDIC
      .w_SERDIC=oParentObject.w_SERDIC
      .w_DDRFDICH=oParentObject.w_DDRFDICH
    endwith
    this.DoRTCalc(1,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_NUMDIC=.w_NUMDIC
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DATDIC=.w_DATDIC
      .oParentObject.w_SERDIC=.w_SERDIC
      .oParentObject.w_DDRFDICH=.w_DDRFDICH
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DDBLIGHKTT()
    with this
          * --- Eliminazione valori dichiarazione di intento
          .w_DDRFDICH = SPACE(10)
          .w_NUMDIC = 0
          .w_SERDIC = space(3)
          .w_ANNDIC = space(4)
          .w_DATDIC = cp_CharToDate('  /  /    ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Elimina")
          .Calculate_DDBLIGHKTT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_1_5.value==this.w_NUMDIC)
      this.oPgFrm.Page1.oPag.oNUMDIC_1_5.value=this.w_NUMDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIC_1_6.value==this.w_ANNDIC)
      this.oPgFrm.Page1.oPag.oANNDIC_1_6.value=this.w_ANNDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_1_7.value==this.w_DATDIC)
      this.oPgFrm.Page1.oPag.oDATDIC_1_7.value=this.w_DATDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDIC_1_8.value==this.w_SERDIC)
      this.oPgFrm.Page1.oPag.oSERDIC_1_8.value=this.w_SERDIC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kdpPag1 as StdContainer
  Width  = 402
  height = 72
  stdWidth  = 402
  stdheight = 72
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="PKKSUWHQWC",left=291, top=29, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la dichiarazione di intento";
    , HelpContextID = 186227158;
  , bGlobalFont=.t.

    proc oBtn_1_1.Click()
      do GSAR_KDI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (upper(.oParentObject.cfunction)$'LOAD-EDIT' and oParentObject .w_DDTIPRIF$'CO/FA' and Not Empty(oParentObject .w_DDCODDES))
      endwith
    endif
  endfunc

  add object oNUMDIC_1_5 as StdField with uid="LZSZMXBJYG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMDIC", cQueryName = "NUMDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della eventuale dichiarazione di esenzione",;
    HelpContextID = 219172138,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=8, Top=29

  add object oANNDIC_1_6 as StdField with uid="WDPJRHWULM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNDIC", cQueryName = "ANNDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della eventuale dichiarazione di esenzione",;
    HelpContextID = 219170042,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=139, Top=29, InputMask=replicate('X',4)

  add object oDATDIC_1_7 as StdField with uid="XUSTCCGTDF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATDIC", cQueryName = "DATDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della eventuale dichiarazione di esenzione",;
    HelpContextID = 219148746,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=29

  add object oSERDIC_1_8 as StdField with uid="NXNPEGQYTT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SERDIC", cQueryName = "SERDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 219155674,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=85, Top=29, InputMask=replicate('X',3)


  add object oBtn_1_10 as StdButton with uid="VJARRGRPGZ",left=344, top=7, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per rimuovere il riferimento alla dichiarazione di intento";
    , HelpContextID = 266696378;
    , Tabstop=.f.,Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .notifyevent("Elimina")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (upper(.oParentObject.cfunction)$'LOAD-EDIT' and Not Empty( .w_DDRFDICH ))
      endwith
    endif
  endfunc

  add object oStr_1_2 as StdString with uid="THYYFUEPJR",Visible=.t., Left=71, Top=32,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="PUCRTMLYAO",Visible=.t., Left=8, Top=8,;
    Alignment=0, Width=205, Height=18,;
    Caption="Riferimento dichiarazione di intento"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="MYVZGHLMUE",Visible=.t., Left=188, Top=29,;
    Alignment=1, Width=22, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ADMACVDPOE",Visible=.t., Left=127, Top=32,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
