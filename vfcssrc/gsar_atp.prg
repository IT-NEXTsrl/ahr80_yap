* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_atp                                                        *
*              Contributi accessori                                            *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-26                                                      *
* Last revis.: 2008-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_atp"))

* --- Class definition
define class tgsar_atp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 429
  Height = 125+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-04-03"
  HelpContextID=175540375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  TIPCONTR_IDX = 0
  UNIMIS_IDX = 0
  VALUTE_IDX = 0
  cFile = "TIPCONTR"
  cKeySelect = "TPCODICE"
  cKeyWhere  = "TPCODICE=this.w_TPCODICE"
  cKeyWhereODBC = '"TPCODICE="+cp_ToStrODBC(this.w_TPCODICE)';

  cKeyWhereODBCqualified = '"TIPCONTR.TPCODICE="+cp_ToStrODBC(this.w_TPCODICE)';

  cPrg = "gsar_atp"
  cComment = "Contributi accessori"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TPCODICE = space(15)
  w_USATOINCATEGORIE = space(10)
  w_TPCODVAL = space(3)
  w_TPDESCRI = space(40)
  w_TPAPPPES = space(1)
  o_TPAPPPES = space(1)
  w_TPBASRIF = space(1)
  w_TPUNMIKG = space(3)
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPCONTR','gsar_atp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_atpPag1","gsar_atp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contributi accessori")
      .Pages(1).HelpContextID = 183114882
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='TIPCONTR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPCONTR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPCONTR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TPCODICE = NVL(TPCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPCONTR where TPCODICE=KeySet.TPCODICE
    *
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPCONTR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPCONTR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPCONTR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_USATOINCATEGORIE = !EMPTY(NVL(LOOKTAB("CATMCONT","CGCODICE","CGCONTRI",.w_TPCODICE),""))
        .w_OBTEST = i_DATSYS
        .w_TPCODICE = NVL(TPCODICE,space(15))
        .w_TPCODVAL = NVL(TPCODVAL,space(3))
          * evitabile
          *.link_1_3('Load')
        .w_TPDESCRI = NVL(TPDESCRI,space(40))
        .w_TPAPPPES = NVL(TPAPPPES,space(1))
        .w_TPBASRIF = NVL(TPBASRIF,space(1))
        .w_TPUNMIKG = NVL(TPUNMIKG,space(3))
          * evitabile
          *.link_1_11('Load')
        cp_LoadRecExtFlds(this,'TIPCONTR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TPCODICE = space(15)
      .w_USATOINCATEGORIE = space(10)
      .w_TPCODVAL = space(3)
      .w_TPDESCRI = space(40)
      .w_TPAPPPES = space(1)
      .w_TPBASRIF = space(1)
      .w_TPUNMIKG = space(3)
      .w_OBTEST = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_USATOINCATEGORIE = !EMPTY(NVL(LOOKTAB("CATMCONT","CGCODICE","CGCONTRI",.w_TPCODICE),""))
        .w_TPCODVAL = g_PERVAL
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_TPCODVAL))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_TPAPPPES = 'N'
        .w_TPBASRIF = IIF( .w_TPAPPPES='S' , 'N' , SPACE( 1 ) )
        .w_TPUNMIKG = SPACE( 3 )
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_TPUNMIKG))
          .link_1_11('Full')
          endif
        .w_OBTEST = i_DATSYS
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPCONTR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTPCODVAL_1_3.enabled = i_bVal
      .Page1.oPag.oTPDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oTPAPPPES_1_6.enabled = i_bVal
      .Page1.oPag.oTPBASRIF_1_8.enabled = i_bVal
      .Page1.oPag.oTPUNMIKG_1_11.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTPCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIPCONTR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPCODICE,"TPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPCODVAL,"TPCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPDESCRI,"TPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPAPPPES,"TPAPPPES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPBASRIF,"TPBASRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPUNMIKG,"TPUNMIKG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    i_lTable = "TIPCONTR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPCONTR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPCONTR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPCONTR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPCONTR')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPCONTR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TPCODICE,TPCODVAL,TPDESCRI,TPAPPPES,TPBASRIF"+;
                  ",TPUNMIKG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TPCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_TPCODVAL)+;
                  ","+cp_ToStrODBC(this.w_TPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TPAPPPES)+;
                  ","+cp_ToStrODBC(this.w_TPBASRIF)+;
                  ","+cp_ToStrODBCNull(this.w_TPUNMIKG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPCONTR')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPCONTR')
        cp_CheckDeletedKey(i_cTable,0,'TPCODICE',this.w_TPCODICE)
        INSERT INTO (i_cTable);
              (TPCODICE,TPCODVAL,TPDESCRI,TPAPPPES,TPBASRIF,TPUNMIKG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TPCODICE;
                  ,this.w_TPCODVAL;
                  ,this.w_TPDESCRI;
                  ,this.w_TPAPPPES;
                  ,this.w_TPBASRIF;
                  ,this.w_TPUNMIKG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPCONTR_IDX,i_nConn)
      *
      * update TIPCONTR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPCONTR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TPCODVAL="+cp_ToStrODBCNull(this.w_TPCODVAL)+;
             ",TPDESCRI="+cp_ToStrODBC(this.w_TPDESCRI)+;
             ",TPAPPPES="+cp_ToStrODBC(this.w_TPAPPPES)+;
             ",TPBASRIF="+cp_ToStrODBC(this.w_TPBASRIF)+;
             ",TPUNMIKG="+cp_ToStrODBCNull(this.w_TPUNMIKG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPCONTR')
        i_cWhere = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
        UPDATE (i_cTable) SET;
              TPCODVAL=this.w_TPCODVAL;
             ,TPDESCRI=this.w_TPDESCRI;
             ,TPAPPPES=this.w_TPAPPPES;
             ,TPBASRIF=this.w_TPBASRIF;
             ,TPUNMIKG=this.w_TPUNMIKG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPCONTR_IDX,i_nConn)
      *
      * delete TIPCONTR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_TPAPPPES<>.w_TPAPPPES
            .w_TPBASRIF = IIF( .w_TPAPPPES='S' , 'N' , SPACE( 1 ) )
        endif
        if .o_TPAPPPES<>.w_TPAPPPES
            .w_TPUNMIKG = SPACE( 3 )
          .link_1_11('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTPAPPPES_1_6.enabled = this.oPgFrm.Page1.oPag.oTPAPPPES_1_6.mCond()
    this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.enabled = this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.mCond()
    this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.enabled = this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.visible=!this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.visible=!this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TPCODVAL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_TPCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_TPCODVAL))
          select VACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TPCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TPCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oTPCODVAL_1_3'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_TPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_TPCODVAL)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TPCODVAL = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TPCODVAL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TPUNMIKG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TPUNMIKG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_TPUNMIKG)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_TPUNMIKG))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TPUNMIKG)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TPUNMIKG) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oTPUNMIKG_1_11'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TPUNMIKG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_TPUNMIKG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_TPUNMIKG)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TPUNMIKG = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TPUNMIKG = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TPUNMIKG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTPCODICE_1_1.value==this.w_TPCODICE)
      this.oPgFrm.Page1.oPag.oTPCODICE_1_1.value=this.w_TPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTPCODVAL_1_3.value==this.w_TPCODVAL)
      this.oPgFrm.Page1.oPag.oTPCODVAL_1_3.value=this.w_TPCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTPDESCRI_1_5.value==this.w_TPDESCRI)
      this.oPgFrm.Page1.oPag.oTPDESCRI_1_5.value=this.w_TPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTPAPPPES_1_6.RadioValue()==this.w_TPAPPPES)
      this.oPgFrm.Page1.oPag.oTPAPPPES_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.RadioValue()==this.w_TPBASRIF)
      this.oPgFrm.Page1.oPag.oTPBASRIF_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.value==this.w_TPUNMIKG)
      this.oPgFrm.Page1.oPag.oTPUNMIKG_1_11.value=this.w_TPUNMIKG
    endif
    cp_SetControlsValueExtFlds(this,'TIPCONTR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TPCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TPCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TPCODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPCODVAL_1_3.SetFocus()
            i_bnoObbl = !empty(.w_TPCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TPUNMIKG))  and not(.w_TPAPPPES <> 'S')  and (NOT .w_USATOINCATEGORIE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPUNMIKG_1_11.SetFocus()
            i_bnoObbl = !empty(.w_TPUNMIKG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Controllare unit� di misura")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TPAPPPES = this.w_TPAPPPES
    return

enddefine

* --- Define pages as container
define class tgsar_atpPag1 as StdContainer
  Width  = 425
  height = 125
  stdWidth  = 425
  stdheight = 125
  resizeXpos=181
  resizeYpos=44
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTPCODICE_1_1 as StdField with uid="UZFMLXBABI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TPCODICE", cQueryName = "TPCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice contributo",;
    HelpContextID = 133558661,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=128, Top=9, InputMask=replicate('X',15)

  add object oTPCODVAL_1_3 as StdField with uid="PTYNNOUIDP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TPCODVAL", cQueryName = "TPCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta del contributo",;
    HelpContextID = 183890302,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=366, Top=8, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_TPCODVAL"

  func oTPCODVAL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTPCODVAL_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTPCODVAL_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oTPCODVAL_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oTPCODVAL_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_TPCODVAL
     i_obj.ecpSave()
  endproc

  add object oTPDESCRI_1_5 as StdField with uid="DJQCYNOOCT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TPDESCRI", cQueryName = "TPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contributo",;
    HelpContextID = 219144577,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=128, Top=38, InputMask=replicate('X',40)


  add object oTPAPPPES_1_6 as StdCombo with uid="HERCLWXFCL",rtseq=5,rtrep=.f.,left=128,top=70,width=146,height=21;
    , ToolTipText = "Base di calcolo del contributo sul documento";
    , HelpContextID = 264957577;
    , cFormVar="w_TPAPPPES",RowSource=""+"Su peso,"+"Non a peso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTPAPPPES_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTPAPPPES_1_6.GetRadio()
    this.Parent.oContained.w_TPAPPPES = this.RadioValue()
    return .t.
  endfunc

  func oTPAPPPES_1_6.SetRadio()
    this.Parent.oContained.w_TPAPPPES=trim(this.Parent.oContained.w_TPAPPPES)
    this.value = ;
      iif(this.Parent.oContained.w_TPAPPPES=='S',1,;
      iif(this.Parent.oContained.w_TPAPPPES=='N',2,;
      0))
  endfunc

  func oTPAPPPES_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_USATOINCATEGORIE)
    endwith
   endif
  endfunc


  add object oTPBASRIF_1_8 as StdCombo with uid="QYVBQPXTAV",rtseq=6,rtrep=.f.,left=128,top=100,width=146,height=21;
    , ToolTipText = "Base riferimento (peso netto/peso lordo/tara)";
    , HelpContextID = 32243324;
    , cFormVar="w_TPBASRIF",RowSource=""+"Peso netto,"+"Peso lordo,"+"Tara", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTPBASRIF_1_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'L',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTPBASRIF_1_8.GetRadio()
    this.Parent.oContained.w_TPBASRIF = this.RadioValue()
    return .t.
  endfunc

  func oTPBASRIF_1_8.SetRadio()
    this.Parent.oContained.w_TPBASRIF=trim(this.Parent.oContained.w_TPBASRIF)
    this.value = ;
      iif(this.Parent.oContained.w_TPBASRIF=='N',1,;
      iif(this.Parent.oContained.w_TPBASRIF=='L',2,;
      iif(this.Parent.oContained.w_TPBASRIF=='T',3,;
      0)))
  endfunc

  func oTPBASRIF_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_USATOINCATEGORIE)
    endwith
   endif
  endfunc

  func oTPBASRIF_1_8.mHide()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S')
    endwith
  endfunc

  add object oTPUNMIKG_1_11 as StdField with uid="YQAMNRCXOE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TPUNMIKG", cQueryName = "TPUNMIKG",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Controllare unit� di misura",;
    ToolTipText = "Unit� di misura corrispondente al KG",;
    HelpContextID = 144322173,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=364, Top=99, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_TPUNMIKG"

  func oTPUNMIKG_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_USATOINCATEGORIE)
    endwith
   endif
  endfunc

  func oTPUNMIKG_1_11.mHide()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S')
    endwith
  endfunc

  func oTPUNMIKG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oTPUNMIKG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTPUNMIKG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oTPUNMIKG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oTPUNMIKG_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_TPUNMIKG
     i_obj.ecpSave()
  endproc

  add object oStr_1_4 as StdString with uid="ICKIGZPYMT",Visible=.t., Left=44, Top=9,;
    Alignment=1, Width=83, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="WMSXHDZZCL",Visible=.t., Left=44, Top=38,;
    Alignment=1, Width=83, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XGAFEPMLGW",Visible=.t., Left=4, Top=104,;
    Alignment=1, Width=121, Height=17,;
    Caption="Base riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="VXTHJDEHPU",Visible=.t., Left=281, Top=102,;
    Alignment=1, Width=83, Height=18,;
    Caption="U.M. per KG.:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_TPAPPPES <> 'S')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="XNHLFVXWZB",Visible=.t., Left=306, Top=11,;
    Alignment=1, Width=56, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DDFCCFZLLR",Visible=.t., Left=4, Top=71,;
    Alignment=1, Width=121, Height=18,;
    Caption="Applicazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_atp','TIPCONTR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TPCODICE=TIPCONTR.TPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
