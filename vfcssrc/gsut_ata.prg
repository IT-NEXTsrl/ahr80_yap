* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ata                                                        *
*              Stampanti solo testo                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_116]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-01                                                      *
* Last revis.: 2008-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ata"))

* --- Class definition
define class tgsut_ata as StdForm
  Top    = 13
  Left   = 14

  * --- Standard Properties
  Width  = 742
  Height = 347+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-01"
  HelpContextID=92881769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  TAB_STA_IDX = 0
  cFile = "TAB_STA"
  cKeySelect = "TSDRIVER"
  cKeyWhere  = "TSDRIVER=this.w_TSDRIVER"
  cKeyWhereODBC = '"TSDRIVER="+cp_ToStrODBC(this.w_TSDRIVER)';

  cKeyWhereODBCqualified = '"TAB_STA.TSDRIVER="+cp_ToStrODBC(this.w_TSDRIVER)';

  cPrg = "gsut_ata"
  cComment = "Stampanti solo testo"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFUNC = space(10)
  w_TSDRIVER = space(200)
  w_TSROWPAG = 0
  w_TSROWOK = 0
  w_TSINIZIA = space(50)
  w_TSRESET = space(50)
  w_TS10CPI = space(25)
  w_TS15CPI = space(25)
  w_TS12CPI = space(25)
  w_TSFORPAG = space(25)
  w_TSSTCOMP = space(25)
  w_TSRTCOMP = space(25)
  w_TSSTBOLD = space(25)
  w_TSFIBOLD = space(25)
  w_TSSTDOUB = space(25)
  w_TSFIDOUB = space(25)
  w_TSSTITAL = space(25)
  w_TSFIITAL = space(25)
  w_TSSTUNDE = space(25)
  w_TSFIUNDE = space(25)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_STA','gsut_ata')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ataPag1","gsut_ata",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Set di caratteri")
      .Pages(1).HelpContextID = 157749246
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTSDRIVER_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TAB_STA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_STA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_STA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TSDRIVER = NVL(TSDRIVER,space(200))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_STA where TSDRIVER=KeySet.TSDRIVER
    *
    i_nConn = i_TableProp[this.TAB_STA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_STA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_STA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_STA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TSDRIVER',this.w_TSDRIVER  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CFUNC = this.cFunction
        .w_TSDRIVER = NVL(TSDRIVER,space(200))
        .w_TSROWPAG = NVL(TSROWPAG,0)
        .w_TSROWOK = NVL(TSROWOK,0)
        .w_TSINIZIA = NVL(TSINIZIA,space(50))
        .w_TSRESET = NVL(TSRESET,space(50))
        .w_TS10CPI = NVL(TS10CPI,space(25))
        .w_TS15CPI = NVL(TS15CPI,space(25))
        .w_TS12CPI = NVL(TS12CPI,space(25))
        .w_TSFORPAG = NVL(TSFORPAG,space(25))
        .w_TSSTCOMP = NVL(TSSTCOMP,space(25))
        .w_TSRTCOMP = NVL(TSRTCOMP,space(25))
        .w_TSSTBOLD = NVL(TSSTBOLD,space(25))
        .w_TSFIBOLD = NVL(TSFIBOLD,space(25))
        .w_TSSTDOUB = NVL(TSSTDOUB,space(25))
        .w_TSFIDOUB = NVL(TSFIDOUB,space(25))
        .w_TSSTITAL = NVL(TSSTITAL,space(25))
        .w_TSFIITAL = NVL(TSFIITAL,space(25))
        .w_TSSTUNDE = NVL(TSSTUNDE,space(25))
        .w_TSFIUNDE = NVL(TSFIUNDE,space(25))
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        cp_LoadRecExtFlds(this,'TAB_STA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CFUNC = space(10)
      .w_TSDRIVER = space(200)
      .w_TSROWPAG = 0
      .w_TSROWOK = 0
      .w_TSINIZIA = space(50)
      .w_TSRESET = space(50)
      .w_TS10CPI = space(25)
      .w_TS15CPI = space(25)
      .w_TS12CPI = space(25)
      .w_TSFORPAG = space(25)
      .w_TSSTCOMP = space(25)
      .w_TSRTCOMP = space(25)
      .w_TSSTBOLD = space(25)
      .w_TSFIBOLD = space(25)
      .w_TSSTDOUB = space(25)
      .w_TSFIDOUB = space(25)
      .w_TSSTITAL = space(25)
      .w_TSFIITAL = space(25)
      .w_TSSTUNDE = space(25)
      .w_TSFIUNDE = space(25)
      if .cFunction<>"Filter"
        .w_CFUNC = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_STA')
    this.DoRTCalc(2,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTSDRIVER_1_2.enabled = i_bVal
      .Page1.oPag.oTSROWPAG_1_4.enabled = i_bVal
      .Page1.oPag.oTSROWOK_1_5.enabled = i_bVal
      .Page1.oPag.oTSINIZIA_1_6.enabled = i_bVal
      .Page1.oPag.oTSRESET_1_7.enabled = i_bVal
      .Page1.oPag.oTS10CPI_1_8.enabled = i_bVal
      .Page1.oPag.oTS15CPI_1_9.enabled = i_bVal
      .Page1.oPag.oTS12CPI_1_11.enabled = i_bVal
      .Page1.oPag.oTSFORPAG_1_12.enabled = i_bVal
      .Page1.oPag.oTSSTCOMP_1_13.enabled = i_bVal
      .Page1.oPag.oTSRTCOMP_1_14.enabled = i_bVal
      .Page1.oPag.oTSSTBOLD_1_15.enabled = i_bVal
      .Page1.oPag.oTSFIBOLD_1_16.enabled = i_bVal
      .Page1.oPag.oTSSTDOUB_1_17.enabled = i_bVal
      .Page1.oPag.oTSFIDOUB_1_18.enabled = i_bVal
      .Page1.oPag.oTSSTITAL_1_19.enabled = i_bVal
      .Page1.oPag.oTSFIITAL_1_20.enabled = i_bVal
      .Page1.oPag.oTSSTUNDE_1_21.enabled = i_bVal
      .Page1.oPag.oTSFIUNDE_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = .Page1.oPag.oBtn_1_3.mCond()
      .Page1.oPag.oBtn_1_44.enabled = .Page1.oPag.oBtn_1_44.mCond()
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_45.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTSDRIVER_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTSDRIVER_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_STA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_STA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSDRIVER,"TSDRIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSROWPAG,"TSROWPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSROWOK,"TSROWOK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSINIZIA,"TSINIZIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSRESET,"TSRESET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TS10CPI,"TS10CPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TS15CPI,"TS15CPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TS12CPI,"TS12CPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSFORPAG,"TSFORPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSSTCOMP,"TSSTCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSRTCOMP,"TSRTCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSSTBOLD,"TSSTBOLD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSFIBOLD,"TSFIBOLD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSSTDOUB,"TSSTDOUB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSFIDOUB,"TSFIDOUB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSSTITAL,"TSSTITAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSFIITAL,"TSFIITAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSSTUNDE,"TSSTUNDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TSFIUNDE,"TSFIUNDE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_STA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
    i_lTable = "TAB_STA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_STA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_STA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_STA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_STA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_STA')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_STA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TSDRIVER,TSROWPAG,TSROWOK,TSINIZIA,TSRESET"+;
                  ",TS10CPI,TS15CPI,TS12CPI,TSFORPAG,TSSTCOMP"+;
                  ",TSRTCOMP,TSSTBOLD,TSFIBOLD,TSSTDOUB,TSFIDOUB"+;
                  ",TSSTITAL,TSFIITAL,TSSTUNDE,TSFIUNDE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TSDRIVER)+;
                  ","+cp_ToStrODBC(this.w_TSROWPAG)+;
                  ","+cp_ToStrODBC(this.w_TSROWOK)+;
                  ","+cp_ToStrODBC(this.w_TSINIZIA)+;
                  ","+cp_ToStrODBC(this.w_TSRESET)+;
                  ","+cp_ToStrODBC(this.w_TS10CPI)+;
                  ","+cp_ToStrODBC(this.w_TS15CPI)+;
                  ","+cp_ToStrODBC(this.w_TS12CPI)+;
                  ","+cp_ToStrODBC(this.w_TSFORPAG)+;
                  ","+cp_ToStrODBC(this.w_TSSTCOMP)+;
                  ","+cp_ToStrODBC(this.w_TSRTCOMP)+;
                  ","+cp_ToStrODBC(this.w_TSSTBOLD)+;
                  ","+cp_ToStrODBC(this.w_TSFIBOLD)+;
                  ","+cp_ToStrODBC(this.w_TSSTDOUB)+;
                  ","+cp_ToStrODBC(this.w_TSFIDOUB)+;
                  ","+cp_ToStrODBC(this.w_TSSTITAL)+;
                  ","+cp_ToStrODBC(this.w_TSFIITAL)+;
                  ","+cp_ToStrODBC(this.w_TSSTUNDE)+;
                  ","+cp_ToStrODBC(this.w_TSFIUNDE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_STA')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_STA')
        cp_CheckDeletedKey(i_cTable,0,'TSDRIVER',this.w_TSDRIVER)
        INSERT INTO (i_cTable);
              (TSDRIVER,TSROWPAG,TSROWOK,TSINIZIA,TSRESET,TS10CPI,TS15CPI,TS12CPI,TSFORPAG,TSSTCOMP,TSRTCOMP,TSSTBOLD,TSFIBOLD,TSSTDOUB,TSFIDOUB,TSSTITAL,TSFIITAL,TSSTUNDE,TSFIUNDE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TSDRIVER;
                  ,this.w_TSROWPAG;
                  ,this.w_TSROWOK;
                  ,this.w_TSINIZIA;
                  ,this.w_TSRESET;
                  ,this.w_TS10CPI;
                  ,this.w_TS15CPI;
                  ,this.w_TS12CPI;
                  ,this.w_TSFORPAG;
                  ,this.w_TSSTCOMP;
                  ,this.w_TSRTCOMP;
                  ,this.w_TSSTBOLD;
                  ,this.w_TSFIBOLD;
                  ,this.w_TSSTDOUB;
                  ,this.w_TSFIDOUB;
                  ,this.w_TSSTITAL;
                  ,this.w_TSFIITAL;
                  ,this.w_TSSTUNDE;
                  ,this.w_TSFIUNDE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_STA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_STA_IDX,i_nConn)
      *
      * update TAB_STA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_STA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TSROWPAG="+cp_ToStrODBC(this.w_TSROWPAG)+;
             ",TSROWOK="+cp_ToStrODBC(this.w_TSROWOK)+;
             ",TSINIZIA="+cp_ToStrODBC(this.w_TSINIZIA)+;
             ",TSRESET="+cp_ToStrODBC(this.w_TSRESET)+;
             ",TS10CPI="+cp_ToStrODBC(this.w_TS10CPI)+;
             ",TS15CPI="+cp_ToStrODBC(this.w_TS15CPI)+;
             ",TS12CPI="+cp_ToStrODBC(this.w_TS12CPI)+;
             ",TSFORPAG="+cp_ToStrODBC(this.w_TSFORPAG)+;
             ",TSSTCOMP="+cp_ToStrODBC(this.w_TSSTCOMP)+;
             ",TSRTCOMP="+cp_ToStrODBC(this.w_TSRTCOMP)+;
             ",TSSTBOLD="+cp_ToStrODBC(this.w_TSSTBOLD)+;
             ",TSFIBOLD="+cp_ToStrODBC(this.w_TSFIBOLD)+;
             ",TSSTDOUB="+cp_ToStrODBC(this.w_TSSTDOUB)+;
             ",TSFIDOUB="+cp_ToStrODBC(this.w_TSFIDOUB)+;
             ",TSSTITAL="+cp_ToStrODBC(this.w_TSSTITAL)+;
             ",TSFIITAL="+cp_ToStrODBC(this.w_TSFIITAL)+;
             ",TSSTUNDE="+cp_ToStrODBC(this.w_TSSTUNDE)+;
             ",TSFIUNDE="+cp_ToStrODBC(this.w_TSFIUNDE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_STA')
        i_cWhere = cp_PKFox(i_cTable  ,'TSDRIVER',this.w_TSDRIVER  )
        UPDATE (i_cTable) SET;
              TSROWPAG=this.w_TSROWPAG;
             ,TSROWOK=this.w_TSROWOK;
             ,TSINIZIA=this.w_TSINIZIA;
             ,TSRESET=this.w_TSRESET;
             ,TS10CPI=this.w_TS10CPI;
             ,TS15CPI=this.w_TS15CPI;
             ,TS12CPI=this.w_TS12CPI;
             ,TSFORPAG=this.w_TSFORPAG;
             ,TSSTCOMP=this.w_TSSTCOMP;
             ,TSRTCOMP=this.w_TSRTCOMP;
             ,TSSTBOLD=this.w_TSSTBOLD;
             ,TSFIBOLD=this.w_TSFIBOLD;
             ,TSSTDOUB=this.w_TSSTDOUB;
             ,TSFIDOUB=this.w_TSFIDOUB;
             ,TSSTITAL=this.w_TSSTITAL;
             ,TSFIITAL=this.w_TSFIITAL;
             ,TSSTUNDE=this.w_TSSTUNDE;
             ,TSFIUNDE=this.w_TSFIUNDE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_STA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_STA_IDX,i_nConn)
      *
      * delete TAB_STA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TSDRIVER',this.w_TSDRIVER  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_STA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_STA_IDX,2])
    if i_bUpd
      with this
            .w_CFUNC = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.visible=!this.oPgFrm.Page1.oPag.oBtn_1_3.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTSDRIVER_1_2.value==this.w_TSDRIVER)
      this.oPgFrm.Page1.oPag.oTSDRIVER_1_2.value=this.w_TSDRIVER
    endif
    if not(this.oPgFrm.Page1.oPag.oTSROWPAG_1_4.value==this.w_TSROWPAG)
      this.oPgFrm.Page1.oPag.oTSROWPAG_1_4.value=this.w_TSROWPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTSROWOK_1_5.value==this.w_TSROWOK)
      this.oPgFrm.Page1.oPag.oTSROWOK_1_5.value=this.w_TSROWOK
    endif
    if not(this.oPgFrm.Page1.oPag.oTSINIZIA_1_6.value==this.w_TSINIZIA)
      this.oPgFrm.Page1.oPag.oTSINIZIA_1_6.value=this.w_TSINIZIA
    endif
    if not(this.oPgFrm.Page1.oPag.oTSRESET_1_7.value==this.w_TSRESET)
      this.oPgFrm.Page1.oPag.oTSRESET_1_7.value=this.w_TSRESET
    endif
    if not(this.oPgFrm.Page1.oPag.oTS10CPI_1_8.value==this.w_TS10CPI)
      this.oPgFrm.Page1.oPag.oTS10CPI_1_8.value=this.w_TS10CPI
    endif
    if not(this.oPgFrm.Page1.oPag.oTS15CPI_1_9.value==this.w_TS15CPI)
      this.oPgFrm.Page1.oPag.oTS15CPI_1_9.value=this.w_TS15CPI
    endif
    if not(this.oPgFrm.Page1.oPag.oTS12CPI_1_11.value==this.w_TS12CPI)
      this.oPgFrm.Page1.oPag.oTS12CPI_1_11.value=this.w_TS12CPI
    endif
    if not(this.oPgFrm.Page1.oPag.oTSFORPAG_1_12.value==this.w_TSFORPAG)
      this.oPgFrm.Page1.oPag.oTSFORPAG_1_12.value=this.w_TSFORPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTSSTCOMP_1_13.value==this.w_TSSTCOMP)
      this.oPgFrm.Page1.oPag.oTSSTCOMP_1_13.value=this.w_TSSTCOMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTSRTCOMP_1_14.value==this.w_TSRTCOMP)
      this.oPgFrm.Page1.oPag.oTSRTCOMP_1_14.value=this.w_TSRTCOMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTSSTBOLD_1_15.value==this.w_TSSTBOLD)
      this.oPgFrm.Page1.oPag.oTSSTBOLD_1_15.value=this.w_TSSTBOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oTSFIBOLD_1_16.value==this.w_TSFIBOLD)
      this.oPgFrm.Page1.oPag.oTSFIBOLD_1_16.value=this.w_TSFIBOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oTSSTDOUB_1_17.value==this.w_TSSTDOUB)
      this.oPgFrm.Page1.oPag.oTSSTDOUB_1_17.value=this.w_TSSTDOUB
    endif
    if not(this.oPgFrm.Page1.oPag.oTSFIDOUB_1_18.value==this.w_TSFIDOUB)
      this.oPgFrm.Page1.oPag.oTSFIDOUB_1_18.value=this.w_TSFIDOUB
    endif
    if not(this.oPgFrm.Page1.oPag.oTSSTITAL_1_19.value==this.w_TSSTITAL)
      this.oPgFrm.Page1.oPag.oTSSTITAL_1_19.value=this.w_TSSTITAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTSFIITAL_1_20.value==this.w_TSFIITAL)
      this.oPgFrm.Page1.oPag.oTSFIITAL_1_20.value=this.w_TSFIITAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTSSTUNDE_1_21.value==this.w_TSSTUNDE)
      this.oPgFrm.Page1.oPag.oTSSTUNDE_1_21.value=this.w_TSSTUNDE
    endif
    if not(this.oPgFrm.Page1.oPag.oTSFIUNDE_1_22.value==this.w_TSFIUNDE)
      this.oPgFrm.Page1.oPag.oTSFIUNDE_1_22.value=this.w_TSFIUNDE
    endif
    cp_SetControlsValueExtFlds(this,'TAB_STA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TSROWPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTSROWPAG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TSROWPAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TSROWOK))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTSROWOK_1_5.SetFocus()
            i_bnoObbl = !empty(.w_TSROWOK)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_ata
       if EMPTY(.w_TSDRIVER)
         i_bRes = .f.
      	 i_bnoChk = .f.		
      	 i_cErrorMsg = Ah_MsgFormat("Inserire nome stampante")
       endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_ataPag1 as StdContainer
  Width  = 738
  height = 347
  stdWidth  = 738
  stdheight = 347
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTSDRIVER_1_2 as StdField with uid="DAHVGPUCMW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TSDRIVER", cQueryName = "TSDRIVER",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome stampante",;
    HelpContextID = 90002824,;
    FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=22, Width=471, Left=139, Top=17, InputMask=replicate('X',200)


  add object oBtn_1_3 as StdButton with uid="OANZSBSEKT",left=613, top=17, width=24,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona stampante";
    , HelpContextID = 92680746;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        do SelStampa with thisform
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFUNC='Load')
      endwith
    endif
  endfunc

  func oBtn_1_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_CFUNC='Load')
     endwith
    endif
  endfunc

  add object oTSROWPAG_1_4 as StdField with uid="BZNOEOUAJC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TSROWPAG", cQueryName = "TSROWPAG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero righe pagina",;
    HelpContextID = 264555139,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=139, Top=80, cSayPict='"999"', cGetPict='"999"'

  add object oTSROWOK_1_5 as StdField with uid="RCZFFLXCRK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TSROWOK", cQueryName = "TSROWOK",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero righe utili",;
    HelpContextID = 12896970,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=351, Top=80

  add object oTSINIZIA_1_6 as StdField with uid="UTVRMEXOEC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TSINIZIA", cQueryName = "TSINIZIA",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 156870007,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=139, Top=140, InputMask=replicate('X',50)

  add object oTSRESET_1_7 as StdField with uid="VNOMIWFTQW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TSRESET", cQueryName = "TSRESET",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Set reset stampante",;
    HelpContextID = 82916662,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=501, Top=140, InputMask=replicate('X',50)

  add object oTS10CPI_1_8 as StdField with uid="MBDVTJELTE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TS10CPI", cQueryName = "TS10CPI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Set 10 CPI",;
    HelpContextID = 249177398,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=162, InputMask=replicate('X',25)

  add object oTS15CPI_1_9 as StdField with uid="WSVDFELBUK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TS15CPI", cQueryName = "TS15CPI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Set 15 CPI",;
    HelpContextID = 249505078,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=501, Top=162, InputMask=replicate('X',25)

  add object oTS12CPI_1_11 as StdField with uid="CWNAAXVJCM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TS12CPI", cQueryName = "TS12CPI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Set 12 CPI",;
    HelpContextID = 249308470,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=183, InputMask=replicate('X',25)

  add object oTSFORPAG_1_12 as StdField with uid="YWWXPPOFRU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TSFORPAG", cQueryName = "TSFORPAG",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Set formato pagina",;
    HelpContextID = 1411715,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=501, Top=183, InputMask=replicate('X',25)

  add object oTSSTCOMP_1_13 as StdField with uid="QEYCIELWCO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TSSTCOMP", cQueryName = "TSSTCOMP",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Abilita compresso",;
    HelpContextID = 33536634,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=241, InputMask=replicate('X',25)

  add object oTSRTCOMP_1_14 as StdField with uid="ORXZUQQAIQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_TSRTCOMP", cQueryName = "TSRTCOMP",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Disabilita compresso",;
    HelpContextID = 33540730,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=332, Top=241, InputMask=replicate('X',25)

  add object oTSSTBOLD_1_15 as StdField with uid="IKVJZYXQLD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TSSTBOLD", cQueryName = "TSSTBOLD",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Abilita grassetto",;
    HelpContextID = 34585222,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=261, InputMask=replicate('X',25)

  add object oTSFIBOLD_1_16 as StdField with uid="RDSSBXQOIN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_TSFIBOLD", cQueryName = "TSFIBOLD",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Disabilita grassetto",;
    HelpContextID = 35359366,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=332, Top=261, InputMask=replicate('X',25)

  add object oTSSTDOUB_1_17 as StdField with uid="VCKTLFDNXO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TSSTDOUB", cQueryName = "TSSTDOUB",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Abilita doppio",;
    HelpContextID = 235947384,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=280, InputMask=replicate('X',25)

  add object oTSFIDOUB_1_18 as StdField with uid="WPRBKJACCG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TSFIDOUB", cQueryName = "TSFIDOUB",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Disabilita doppio",;
    HelpContextID = 235173240,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=332, Top=280, InputMask=replicate('X',25)

  add object oTSSTITAL_1_19 as StdField with uid="IZLODRQPLQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TSSTITAL", cQueryName = "TSSTITAL",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Abilita italico",;
    HelpContextID = 211794558,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=301, InputMask=replicate('X',25)

  add object oTSFIITAL_1_20 as StdField with uid="YWZCFRRAUX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_TSFIITAL", cQueryName = "TSFIITAL",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Disabilita italico",;
    HelpContextID = 212568702,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=332, Top=301, InputMask=replicate('X',25)

  add object oTSSTUNDE_1_21 as StdField with uid="BPDIOMIFTW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TSSTUNDE", cQueryName = "TSSTUNDE",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Abilita sottolineato",;
    HelpContextID = 236995963,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=139, Top=321, InputMask=replicate('X',25)

  add object oTSFIUNDE_1_22 as StdField with uid="CBMVCIAGLD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TSFIUNDE", cQueryName = "TSFIUNDE",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Disabilita sottolineato",;
    HelpContextID = 236221819,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=332, Top=321, InputMask=replicate('X',25)


  add object oObj_1_43 as cp_runprogram with uid="THPAAANLYQ",left=289, top=366, width=163,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BPR('N')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 85115878


  add object oBtn_1_44 as StdButton with uid="NUKDPPCKNV",left=660, top=7, width=48,height=45,;
    CpPicture="BMP\Carica.bmp", caption="", nPag=1;
    , ToolTipText = "Duplica impostazioni su altra stampante";
    , HelpContextID = 75118134;
    , caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSUT_BPR(this.Parent.oContained,"X")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TSDRIVER) and .w_CFUNC="Query")
      endwith
    endif
  endfunc


  add object oObj_1_45 as cp_runprogram with uid="JZLKNFPEGA",left=293, top=401, width=155,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BPR('S')",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 85115878

  add object oStr_1_10 as StdString with uid="XNBDHRDKQE",Visible=.t., Left=10, Top=162,;
    Alignment=1, Width=126, Height=18,;
    Caption="10 CPI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XGJCDXCNKL",Visible=.t., Left=386, Top=162,;
    Alignment=1, Width=112, Height=18,;
    Caption="15 CPI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PDFRAEUSDP",Visible=.t., Left=10, Top=183,;
    Alignment=1, Width=126, Height=18,;
    Caption="12 CPI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XNOCBEDQEE",Visible=.t., Left=28, Top=241,;
    Alignment=1, Width=108, Height=18,;
    Caption="Compresso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JTCUTPELFJ",Visible=.t., Left=368, Top=183,;
    Alignment=1, Width=130, Height=18,;
    Caption="Set formato pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="VQHOSGYQVR",Visible=.t., Left=220, Top=80,;
    Alignment=1, Width=128, Height=18,;
    Caption="Numero righe utili:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="BSLUZQICQV",Visible=.t., Left=10, Top=17,;
    Alignment=1, Width=126, Height=18,;
    Caption="Nome stampante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QQIIHHTTSK",Visible=.t., Left=28, Top=261,;
    Alignment=1, Width=108, Height=18,;
    Caption="Grassetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="NRVYVBIYLK",Visible=.t., Left=28, Top=280,;
    Alignment=1, Width=108, Height=18,;
    Caption="Doppio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="XHYTUOASNN",Visible=.t., Left=28, Top=301,;
    Alignment=1, Width=108, Height=18,;
    Caption="Italico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="TTYAEIGSET",Visible=.t., Left=28, Top=321,;
    Alignment=1, Width=108, Height=18,;
    Caption="Sottolineato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="EDHJYOGPZY",Visible=.t., Left=376, Top=140,;
    Alignment=1, Width=122, Height=18,;
    Caption="Reset stampante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="QFUUZAKQIO",Visible=.t., Left=10, Top=80,;
    Alignment=1, Width=126, Height=18,;
    Caption="Numero righe pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="PWVIVOJUNL",Visible=.t., Left=10, Top=49,;
    Alignment=0, Width=154, Height=18,;
    Caption="Impostazioni stampante"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="NELZNIPDJO",Visible=.t., Left=144, Top=218,;
    Alignment=0, Width=59, Height=18,;
    Caption="Abilita"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="GKPDZMUMMG",Visible=.t., Left=335, Top=218,;
    Alignment=0, Width=73, Height=18,;
    Caption="Disabilita"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="EPRFAZHARR",Visible=.t., Left=10, Top=111,;
    Alignment=0, Width=154, Height=18,;
    Caption="Caratteri di controllo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="VPWDGDAGQR",Visible=.t., Left=10, Top=140,;
    Alignment=1, Width=126, Height=18,;
    Caption="Inizializza stampante:"  ;
  , bGlobalFont=.t.

  add object oBox_1_33 as StdBox with uid="TJFNYVQGGK",left=8, top=66, width=729,height=2

  add object oBox_1_39 as StdBox with uid="BVAOXGEXBT",left=4, top=129, width=729,height=2

  add object oBox_1_41 as StdBox with uid="BNQFQOWVDR",left=62, top=236, width=469,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ata','TAB_STA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TSDRIVER=TAB_STA.TSDRIVER";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ata
 procedure SelStampa
 parameters oParent
    * --- Scelta Stampante
    local w_SelPrint
    w_SelPrint = Upper(getPrinter())
    if !empty(w_SelPrint)
       oParent.w_TSDRIVER=w_SelPrint
    endif
 Endproc


* --- Fine Area Manuale
