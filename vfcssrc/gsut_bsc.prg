* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsc                                                        *
*              Invio mail patch installate                                     *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_101]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-09                                                      *
* Last revis.: 2003-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsc",oParentObject)
return(i_retval)

define class tgsut_bsc as StdBatch
  * --- Local variables
  w_LstPatch = space(254)
  w_StrXML = space(254)
  w_NomeFile = space(254)
  w_NomeINI = space(254)
  w_NomeXML = space(254)
  w_RelCorr = space(10)
  w_IdxAddOn = 0
  w_AttCoCodRel = space(15)
  w_AttAgg = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if NVL(this.oParentObject.w_FLINVPDF,"N")="S"
      * --- Eseguo la query selezionata
      vq_exec(this.oParentObject.w_OQRY,this,"__TMP__")
      if NOT USED("__TMP__")
        ah_ErrorMsg("Impossibile creare cursore per file PDF","stop","")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      else
        SELECT "__TMP__"
        * --- Imposto il nome del del file PDF e del file INI nella temp dell'utente
        this.w_NomeFile = tempadhoc() + "\" + "Patch.PDF"
        this.w_NomeINI = tempadhoc() + "\" + __TMP__.momatpro+".Inst"
        * --- Genero il file PDF
        RET = PDF_Format(this.w_NomeFile, this.oParentObject.w_ORep)
        if NOT RET
          ah_ErrorMsg("Funzione di generazione file PDF non configurata","stop","")
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Imposto variabili globali per invio mail
    if g_Application="ad hoc ENTERPRISE"
      i_EMAIL="supportoenterprise@zucchetti.it"
    else
      i_EMAIL="supportorevolution@zucchetti.it"
    endif
    i_EMAILTEXT=ah_Msgformat("In allegato la situazione delle patch installate")
    * --- Lancio la funzione di invio mail
    if this.oParentObject.w_FLINVPDF="S"
      if g_INVIO="S"
        EMAIL(this.w_NomeFile + "; " + this.w_NomeXML,"S")
      else
        DIMENSION L_FILE(2)
        L_FILE(1)=this.w_NomeFile
        L_FILE(2)=this.w_NomeXML
        EMAIL(@L_FILE,"S")
      endif
    else
      EMAIL(this.w_NomeXML,"S")
    endif
    * --- Reimposto le variabili globali utilizzate nel batch
    i_EMAIL=" "
    i_EMAILTEXT=" "
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    RET = 0
    RET = FCREATE(this.w_NomeINI)
    if RET>0
      GO TOP
      =FPUTS(RET,ah_Msgformat("[Diagnostica]") )
      =FPUTS(RET, ah_Msgformat("Prodotto = %1", g_Application) )
      =FPUTS(RET, ah_Msgformat("Release = %1", ALLTRIM(__TMP__.monumrel)))
      =FPUTS(RET, ah_Msgformat("Data = %1", DTOC(DATE())) )
      =FPUTS(RET,ah_Msgformat("Matricola = %1", __TMP__.momatpro))
      =FPUTS(RET,ah_Msgformat("Concessionario = %1", __TMP__.moragins) )
      =FPUTS(RET,ah_Msgformat("Utente = %1", __TMP__.moragcli) )
      =FPUTS(RET, "Patch = {")
      * --- Inserisco le patch installate nel file
      SCAN
      if ALLTRIM(__TMP__.ptreleas)=ALLTRIM(__TMP__.monumrel) AND __TMP__.ptnumpat<>0
        * --- Se la patch installata si riferisce alla release attuale allora la inseirsco nella lista
        =FPUTS(RET, ALLTRIM(STR(__TMP__.ptnumpat)))
      endif
      ENDSCAN
      GO TOP
      =FPUTS(RET, "}")
      =FCLOSE(RET)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    vq_exec("QUERY\GSUT0SCO.VQR",this,"Licenza")
    vq_exec("QUERY\GSUT1SCO.VQR",this,"ListaAddOn")
    this.w_RelCorr = Licenza.monumrel
    vq_exec("QUERY\GSUT2SCO.VQR",this,"ListaPatch")
    if USED("Licenza") AND USED("ListaPatch") AND USED("ListaAddOn")
      this.w_NomeXML = tempadhoc() + "\" + Alltrim(Licenza.momatpro)+".XML"
      RET = 0
      RET = FCREATE(this.w_NomeXML)
      if RET>0
        SELECT "Licenza"
        GO TOP
        =FPUTS(RET,'<?xml version="1.0"?>')
        =FPUTS(RET,'<?xml-stylesheet type="text/xsl" href="Ori.xsl"?>')
        =FPUTS(RET,"<Testata>")
        =FPUTS(RET,CHR(9)+"<Cliente>")
        this.w_StrXML = ALLTRIM(Licenza.moragcli)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+"<RagSoc>"+this.w_StrXML+"</RagSoc>")
        this.w_StrXML = ALLTRIM(Licenza.moindcli)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+"<Indirizzo>"+this.w_StrXML+"</Indirizzo>")
        this.w_StrXML = ALLTRIM(Licenza.mopivcli)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+"<PIVA>"+this.w_StrXML+"</PIVA>")
        =FPUTS(RET,CHR(9)+"</Cliente>")
        this.w_StrXML = ALLTRIM(Licenza.momatpro)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<Matricola>"+this.w_StrXML+"</Matricola>")
        if g_Application="ad hoc ENTERPRISE"
          =FPUTS(RET,CHR(9)+"<Prodotto>AHE</Prodotto>")
        else
          =FPUTS(RET,CHR(9)+"<Prodotto>AHR</Prodotto>")
        endif
        this.w_StrXML = ALLTRIM(Licenza.monumrel)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<Rel>"+this.w_StrXML+"</Rel>")
        this.w_StrXML = ALLTRIM(Licenza.motipdbf)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<DBServer>"+this.w_StrXML+"</DBServer>")
        this.w_StrXML = ALLTRIM(STR(Licenza.momaxute))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<PostiLavoro>"+this.w_StrXML+"</PostiLavoro>")
        this.w_StrXML = ALLTRIM(Licenza.moactke1)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<Key>"+this.w_StrXML+"</Key>")
        this.w_StrXML = DTOC(DATE())
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<Data>"+this.w_StrXML+"</Data>")
        =FPUTS(RET,CHR(9)+"<Concessionario>")
        this.w_StrXML = ALLTRIM(Licenza.moragins)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+"<Nome>"+this.w_StrXML+"</Nome>")
        this.w_StrXML = ALLTRIM(Licenza.mopivins)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+"<PIVA>"+this.w_StrXML+"</PIVA>")
        =FPUTS(RET,CHR(9)+"</Concessionario>")
        this.w_StrXML = ALLTRIM(Licenza.molismod)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<Moduli>"+this.w_StrXML+"</Moduli>")
        this.w_StrXML = ALLTRIM(Licenza.moaddatt)
        =FPUTS(RET,CHR(9)+"<AddAttivi>"+this.w_StrXML+"</AddAttivi>")
        this.w_StrXML = ALLTRIM(Licenza.moaddpri)
        =FPUTS(RET,CHR(9)+"<AddPre>"+this.w_StrXML+"</AddPre>")
        this.w_StrXML = ALLTRIM(STR(15-OCCURS("N",Licenza.moaddatt)))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+"<AddPresenti>"+this.w_StrXML+"</AddPresenti>")
        =FPUTS(RET,CHR(9)+"<ListaAddOn>")
        this.w_IdxAddOn = 1
        do while this.w_IdxAddOn<16
          AddAtt="NOT EMPTY(NVL(Licenza.moadd"+REPLICATE("0",3-LEN(ALLTRIM(STR(this.w_IdxAddOn)))) + ALLTRIM(STR(this.w_IdxAddOn))+",' '))"
          =FPUTS(RET,CHR(9)+CHR(9)+'<Item type="Addon">')
          if &AddAtt
            AddAtt="FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+'<Codice>'+ALLTRIM(Licenza.moadd"+REPLICATE("0",3-LEN(ALLTRIM(STR(this.w_IdxAddOn))))+ALLTRIM(STR(this.w_IdxAddOn))+")+'</Codice>')"
            &AddAtt
            SELECT "ListaAddOn"
            GO TOP
            AddAtt="LOCATE FOR adcodice=Licenza.moadd"+REPLICATE("0",3-LEN(ALLTRIM(STR(this.w_IdxAddOn))))+ALLTRIM(STR(this.w_IdxAddOn))
            &AddAtt
            this.w_StrXML = ALLTRIM(ListaAddOn.addescri)
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Descrizione>"+this.w_StrXML+"</Descrizione>")
            SELECT "Licenza"
          else
            =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Codice></Codice>")
            =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Descrizione></Descrizione>")
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+"</Item>")
          this.w_IdxAddOn = this.w_IdxAddOn+1
        enddo
        =FPUTS(RET,CHR(9)+"</ListaAddOn>")
        =FPUTS(RET,CHR(9)+"<Diagnostica>")
        SELECT "ListaPatch"
        GO TOP
        this.w_StrXML = ALLTRIM(NVL(ListaPatch.cocodrel," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_AttCoCodRel = this.w_StrXML
        =FPUTS(RET,CHR(9)+CHR(9)+'<Item type="ListaPatch">')
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<CodRel>"+this.w_AttCoCodRel+"</CodRel>")
        this.w_StrXML = ALLTRIM(NVL(ListaPatch.ptreleas," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Release>"+this.w_StrXML+"</Release>")
        this.w_StrXML = ALLTRIM(NVL(STR(ListaPatch.ptnumpat)," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_AttAgg = this.w_StrXML
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Agg>"+this.w_StrXML+"</Agg>")
        this.w_StrXML = ALLTRIM(NVL(DTOC(ListaPatch.ptdatins)," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<DataInst>"+this.w_StrXML+"</DataInst>")
        this.w_StrXML = ALLTRIM(NVL(ListaPatch.ptdescri," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Descri>"+this.w_StrXML+"</Descri>")
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<condition>")
        SCAN
        do while this.w_AttCoCodRel<>ALLTRIM(NVL(ListaPatch.cocodrel," ")) OR (empty(this.w_AttCoCodrel) and this.w_attAgg<>ALLTRIM(NVL(STR(ListaPatch.ptnumpat)," "))) OR(not empty(this.w_AttCoCodRel) AND empty(ALLTRIM(NVL(ListaPatch.cocodrel," ")) ))
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"</condition>")
          =FPUTS(RET,CHR(9)+CHR(9)+"</Item>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.cocodrel," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_AttCoCodRel = this.w_StrXML
          =FPUTS(RET,CHR(9)+CHR(9)+'<Item type="ListaPatch">')
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<CodRel>"+this.w_AttCoCodRel+"</CodRel>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.ptreleas," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Release>"+this.w_StrXML+"</Release>")
          this.w_StrXML = ALLTRIM(NVL(STR(ListaPatch.ptnumpat)," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_AttAgg = this.w_StrXML
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Agg>"+this.w_StrXML+"</Agg>")
          this.w_StrXML = ALLTRIM(NVL(DTOC(ListaPatch.ptdatins)," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<DataInst>"+this.w_StrXML+"</DataInst>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.ptdescri," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<Descri>"+this.w_StrXML+"</Descri>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"<condition>")
        enddo
        this.w_StrXML = ALLTRIM(NVL(ListaPatch.coordine," "))
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if not empty(this.w_StrXML)
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+'<Item type="Patch">')
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Ord>"+this.w_StrXML+"</Ord>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.conompro," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Prog>"+this.w_StrXML+"</Prog>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.codescon," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Descri>"+this.w_StrXML+"</Descri>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Eseguita>"+IIF(ALLTRIM(NVL(ListaPatch.coesegui,"N"))="S","Si","No")+"</Eseguita>")
          this.w_StrXML = ALLTRIM(NVL(DTOC(ListaPatch.codatese)," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<DataEsec>"+this.w_StrXML+"</DataEsec>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Ute>"+ALLTRIM(NVL(STR(ListaPatch.couteese)," "))+"</Ute>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Abi>"+IIF(ALLTRIM(NVL(ListaPatch.coabilit,"N"))="S","Si","No")+"</Abi>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Rip>"+IIF(ALLTRIM(NVL(ListaPatch.coripeti,"N"))="S","Si","No")+"</Rip>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Obb>"+IIF(ALLTRIM(NVL(ListaPatch.coeseobb,"N"))="S","Si","No")+"</Obb>")
          this.w_StrXML = ALLTRIM(NVL(ListaPatch.coerrore," "))
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+CHR(9)+"<Mess>"+this.w_StrXML+"</Mess>")
          =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+CHR(9)+"</Item>")
        endif
        ENDSCAN
        =FPUTS(RET,CHR(9)+CHR(9)+CHR(9)+"</condition>")
        =FPUTS(RET,CHR(9)+CHR(9)+"</Item>")
        =FPUTS(RET,CHR(9)+"</Diagnostica>")
        =FPUTS(RET,"</Testata>")
        =FCLOSE(RET)
      endif
    else
      ah_ErrorMsg("Impossibile creare cursore informazioni di licenza","stop","")
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("__TMP__")
      SELECT "__TMP__"
      USE
    endif
    if USED("Licenza")
      SELECT "Licenza"
      USE
    endif
    if USED("ListaPatch")
      SELECT "ListaPatch"
      USE
    endif
    if USED("ListaAddOn")
      SELECT "ListaAddOn"
      USE
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_StrXML = STRTRAN(this.w_StrXML,"<","&#60;")
    this.w_StrXML = STRTRAN(this.w_StrXML,">","&#62;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#224;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#232;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#233;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#236;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#242;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#249;")
    this.w_StrXML = STRTRAN(this.w_StrXML,"�","&#176;")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
