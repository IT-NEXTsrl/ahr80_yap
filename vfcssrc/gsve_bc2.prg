* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bc2                                                        *
*              Calcola totali colli e pesi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_234]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2016-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bc2",oParentObject,m.pExec)
return(i_retval)

define class tgsve_bc2 as StdBatch
  * --- Local variables
  pExec = space(1)
  w_UM1 = space(3)
  w_QTA = 0
  w_UM2 = space(3)
  w_NUMCOL = 0
  w_UM3 = space(3)
  w_PESNET = 0
  w_PN = 0
  w_PESLOR = 0
  w_PL = 0
  w_CODART = space(20)
  w_PC = 0
  w_CODICE = space(20)
  w_CC = 0
  w_UNIMIS = space(3)
  w_CODCOL = space(5)
  w_CDCONF = space(3)
  w_MESS = space(200)
  w_NR = 0
  w_ROWNUM = 0
  w_DESVO = space(15)
  w_UMDIM = space(3)
  w_TARA = 0
  w_FLMIX = space(1)
  w_TIPRIG = space(1)
  w_PERTOL = 0
  w_MAXCON = 0
  w_PROCOL = 0
  w_QTACON = 0
  w_DESART = space(40)
  w_COLLO = 0
  w_CONAGG = 0
  w_ARTAGG = 0
  w_APPO = 0
  w_QTACON2 = 0
  w_CONAGG2 = 0
  w_OLDCOL = 0
  w_TARCOL = 0
  w_ERRCOL = .f.
  w_TESTCOL = space(5)
  w_PADRE = .NULL.
  w_OK = 0
  w_PARAM = space(1)
  w_MYSELF = .NULL.
  w_bIsGenDoc = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_COLL_idx=0
  KEY_ARTI_idx=0
  TIP_COLL_idx=0
  TIPICONF_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Totale Colli e pesi Lordo/Netto sul piede Documento (da GSVE_MDV)
    * --- pExec ='D' da documenti , 'V' da generazione documento da POS, 'G' da generatore documentale
    this.w_bIsGenDoc = this.pExec == "G"
    if this.w_bIsGenDoc
      this.w_PARAM = "V"
    else
      this.w_PARAM = this.pExec
    endif
    this.w_PADRE = this.oParentObject
    this.w_MYSELF = this
    this.oParentObject.w_MVQTACOL = 0
    this.oParentObject.w_MVQTAPES = 0
    this.oParentObject.w_MVQTALOR = 0
    this.w_TARCOL = 0
    * --- Creazione cursore di appoggio per calcolo Packing List.
     
 CREATE CURSOR TMPCUR1; 
 (t_CODCONF C(3), t_CODCOL C(5), t_QTACON N(5,0), t_MAXCON N(6,0), ; 
 t_TARA N(9,3), t_FL_MIX C(1), t_PERTOL N(6,2), t_PESNET N(9,3), t_PESLOR N(9,3), t_ROWNUM N(4,0), t_ROWORD N(5,0), ; 
 t_CODICE C(20), t_DESART C(40), t_QTAMOV N(12,3), t_PZCON N(9,3), t_UNIMIS C(3), t_DESVO C(15), t_UMDIM C(3))
    * --- Cicla sulle Righe Documento
    this.w_ERRCOL = .F.
    * --- Dichiaro l'array che conterr� i risultati della funzione calnumco
    DECLARE ARRAYRET(9,1)
    if this.w_PARAM="D"
      this.w_PADRE.MarkPos()     
    endif
    this.w_MYSELF.FirstRow()     
    do while Not this.w_MYSELF.Eof_Trs()
      this.w_MYSELF.SetRow()     
      this.w_TIPRIG = NVL(this.w_MYSELF.Get("w_MVTIPRIG"), " ")
      if this.w_MYSELF.FullRow() And this.w_TIPRIG="R"
        * --- Legge i Dati del Temporaneo
        this.w_QTA = NVL(this.w_MYSELF.Get("w_MVQTAMOV"), 0)
        this.w_NUMCOL = NVL(this.w_MYSELF.Get("w_MVNUMCOL"), 0)
        this.w_PESNET = 0
        this.w_PESLOR = 0
        this.w_UNIMIS = NVL(this.w_MYSELF.Get("w_MVUNIMIS"), "   ")
        this.w_UM1 = NVL( this.w_MYSELF.Get("w_UNMIS1"), "   ")
        this.w_UM2 = NVL( this.w_MYSELF.Get("w_UNMIS2"), "   ")
        this.w_UM3 = NVL( this.w_MYSELF.Get("w_UNMIS3"), "   ")
        this.w_CODART = NVL( this.w_MYSELF.Get("w_MVCODART"), "   ")
        this.w_DESART = NVL( this.w_MYSELF.Get("w_MVDESART"), "   ")
        this.w_CODICE = NVL( this.w_MYSELF.Get("w_MVCODICE"), "   ")
        this.w_CDCONF = NVL( this.w_MYSELF.Get("w_CODCONF"), "   ")
        this.w_CODCOL = NVL( this.w_MYSELF.Get("w_MVCODCOL"), "   ")
        this.w_NR = this.w_MYSELF.Get("w_CPROWORD")
        this.w_ROWNUM = NVL(this.w_MYSELF.Get("CPROWNUM"),0)
        * --- Calcolo il numero di confezioni
        this.w_OK = CalNumCo(this.w_CODICE, this.w_CODART, this.w_QTA, this.w_UNIMIS, this.w_UM1, this.w_UM2, this.w_UM3, this.w_TIPRIG, this.w_NR, this.w_CDCONF, this.w_NUMCOL, @ARRAYRET, this.w_CODCOL)
        do case
          case this.w_OK = 1
            * --- Visualizzo il messaggio di errore
            if this.w_bIsGenDoc
              this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat( "%1", ARRAYRET[8] ))     
            else
              ah_ErrorMsg("%1","stop","", ARRAYRET[8])
            endif
          case this.w_OK = 2
            * --- Visualizzo il messaggio di warning
            if this.w_bIsGenDoc
              this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat( "%1", ARRAYRET[9] ))     
            else
              ah_ErrorMsg("%1","!","", ARRAYRET[9])
            endif
        endcase
        * --- Se w_OK � uguale a 1 si � verificato un errore bloccante
        this.w_ERRCOL = this.w_OK = 1
        if this.w_QTA>0 AND NOT EMPTY(this.w_UNIMIS)
          this.w_NUMCOL = ARRAYRET[1]
          this.w_PN = ARRAYRET[2]
          this.w_PL = ARRAYRET[3]
          this.w_PC = ARRAYRET[4]
          this.w_CC = ARRAYRET[5]
          this.w_DESVO = ARRAYRET[6]
          this.w_UMDIM = ARRAYRET[7]
          * --- Se Forzati sulla riga N.Colli e Peso Netto prende questi
          this.w_PESNET = cp_ROUND(this.w_QTA * this.w_PN, 3)
          this.w_PESLOR = cp_ROUND(this.w_QTA * this.w_PL, 3)
          if NOT this.w_ERRCOL
            * --- Read from TIP_COLL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_COLL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_COLL_idx,2],.t.,this.TIP_COLL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TC__TARA,TCFL_MIX"+;
                " from "+i_cTable+" TIP_COLL where ";
                    +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TC__TARA,TCFL_MIX;
                from (i_cTable) where;
                    TCCODICE = this.w_CODCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TARA = NVL(cp_ToDate(_read_.TC__TARA),cp_NullValue(_read_.TC__TARA))
              this.w_FLMIX = NVL(cp_ToDate(_read_.TCFL_MIX),cp_NullValue(_read_.TCFL_MIX))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_FLMIX="S"
              * --- Read from CON_COLL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CON_COLL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TCPERTOL"+;
                  " from "+i_cTable+" CON_COLL where ";
                      +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                      +" and TCCODCON = "+cp_ToStrODBC(this.w_CDCONF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TCPERTOL;
                  from (i_cTable) where;
                      TCCODICE = this.w_CODCOL;
                      and TCCODCON = this.w_CDCONF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PERTOL = NVL(cp_ToDate(_read_.TCPERTOL),cp_NullValue(_read_.TCPERTOL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.w_MYSELF.Set("w_MVNUMCOL", this.w_NUMCOL)     
            if this.w_TIPRIG="R"
               
 INSERT INTO TMPCUR1; 
 (t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_TARA, t_FL_MIX,; 
 t_PERTOL, t_PESNET, t_PESLOR, t_ROWNUM, t_ROWORD,; 
 t_CODICE, t_DESART, t_QTAMOV, t_PZCON, t_UNIMIS, t_DESVO, t_UMDIM); 
 VALUES; 
 (this.w_CDCONF, this.w_CODCOL, this.w_NUMCOL, this.w_CC, this.w_TARA, this.w_FLMIX,; 
 this.w_PERTOL, this.w_PN, this.w_PL, this.w_ROWNUM, this.w_NR,; 
 this.w_CODICE, this.w_DESART, this.w_QTA, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM)
            endif
          endif
          this.oParentObject.w_MVQTAPES = this.oParentObject.w_MVQTAPES + this.w_PESNET
          this.oParentObject.w_MVQTALOR = this.oParentObject.w_MVQTALOR + this.w_PESLOR
        endif
      endif
      this.w_MYSELF.NextRow()     
    enddo
    this.w_TARCOL = 0
    if NOT this.w_ERRCOL
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    USE IN SELECT("TMPCUR1")
    USE IN SELECT("TMPCUR2")
    USE IN SELECT("APPO")
    if this.w_PARAM="D"
      this.w_PADRE.RePos()     
    endif
    * --- Evito la mCalc perch� ho modificato dei campi che non sono soggetti
    *     a dipendenze
    this.bUpdateParentObject = .f.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisco il secondo cursore di appoggio per calcolo Packing List
    CREATE CURSOR TMPCUR2 (t_PROCOL N(4,0), t_CODCONF C(3), t_CODCOL C(5), t_QTACON N(6,0), t_MAXCON N(6,0),;
    t_PESTAR N(9,3), t_FLCMIX C(1), t_PERTOL N(6,2), t_PESNET N(9,3), t_PESLOR N(9,3), t_NUMRIG N(4,0), t_NUMORD N(5,0),;
    t_CODICE C(20), t_DESART C(40), t_QTAART N(12,3), t_PZCONF N(9,3),t_UNIMIS C(3), t_VOLUME C(15), t_UMVOLU C(3), t_QTACON2 N(10,6))
    SELECT * FROM TMPCUR1 ORDER BY t_CODCONF, t_CODCOL, t_CODICE INTO CURSOR TMPCUR1
    SELECT TMPCUR1
    GO TOP 
    this.w_PROCOL = 0
    * --- Ciclo sul Cursore
    SCAN FOR NOT EMPTY(t_CODCONF) 
    this.w_CDCONF = t_CODCONF
    this.w_CODCOL = t_CODCOL
    this.w_QTACON = NVL(t_QTACON,0)
    this.w_MAXCON = NVL(t_MAXCON,0)
    this.w_QTA = NVL(t_QTAMOV,0)
    this.w_PC = NVL(t_PZCON,0)
    this.w_TARA = NVL(t_TARA,0)
    this.w_FLMIX = NVL(t_FL_MIX,SPACE(1))
    this.w_PERTOL = NVL(t_PERTOL,0)
    this.w_PESNET = NVL(t_PESNET,0)
    this.w_PESLOR = NVL(t_PESLOR,0)
    this.w_NR = t_ROWORD
    this.w_ROWNUM = t_ROWNUM
    this.w_CODART = t_CODICE
    this.w_DESART = NVL(t_DESART,SPACE(40))
    this.w_UNIMIS = NVL(t_UNIMIS,SPACE(3))
    this.w_DESVO = NVL(t_DESVO,SPACE(15))
    this.w_UMDIM = NVL(t_UMDIM,SPACE(3))
    SELECT t_PROCOL, t_CODCOL, t_CODCONF, SUM(t_QTACON) AS QTACON;
    FROM TMPCUR2 INTO CURSOR APPO GROUP BY t_PROCOL, t_CODCOL,t_CODCONF
    SELECT APPO 
    LOCATE FOR t_CODCONF=this.w_CDCONF AND t_CODCOL=this.w_CODCOL AND QTACON<this.w_MAXCON
    if FOUND()
      this.w_COLLO = t_PROCOL
      * --- Calcola quante confezioni possono rientrare nel collo
      this.w_CONAGG = MIN((this.w_MAXCON-QTACON),this.w_QTACON)
      this.w_ARTAGG = this.w_QTA
      if this.w_CONAGG<>this.w_QTACON
        this.w_ARTAGG = INT(this.w_CONAGG*this.w_PC)
      endif
      INSERT INTO TMPCUR2;
      (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
       t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
      t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2) ;
      VALUES;
      (this.w_COLLO,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
      this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
      this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
      cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6) )
      this.w_QTACON = this.w_QTACON-this.w_CONAGG
      this.w_QTA = this.w_QTA-this.w_ARTAGG
    endif
    do while this.w_QTACON>0
      this.w_CONAGG = MIN(this.w_MAXCON,this.w_QTACON)
      this.w_ARTAGG = this.w_QTA
      if this.w_CONAGG<>this.w_QTACON
        this.w_APPO = INT(this.w_CONAGG*this.w_PC)
        this.w_ARTAGG = MIN(this.w_APPO,this.w_QTA)
      endif
      this.w_PROCOL = this.w_PROCOL+1
      this.w_TARCOL = this.w_TARCOL + this.w_TARA
      INSERT INTO TMPCUR2;
      (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
       t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
      t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2) ;
      VALUES;
      (this.w_PROCOL ,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
      this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
      this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
      cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6) )
      this.w_QTACON = this.w_QTACON-this.w_CONAGG
      this.w_QTA = this.w_QTA-this.w_ARTAGG
    enddo
    SELECT TMPCUR1
    ENDSCAN
    * --- Ricerca Eventuli Colli a Composizione Mista non ancora Pieni
    SELECT t_PROCOL AS t_PROCOL, SUM(t_QTACON) AS QTACON, MAX(t_MAXCON) AS MAXCON FROM TMPCUR2 ;
    WHERE t_FLCMIX="S" GROUP BY t_PROCOL HAVING SUM(t_QTACON)<MAXCON ;
    INTO CURSOR APPO
    if used("APPO")
      if RECCOUNT("APPO")>1
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_PROCOL >99999
      if this.w_bIsGenDoc
        this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat( "Superato massimo numero colli per documento. I valori verranno azzerati" ))     
      else
        ah_ErrorMsg("Superato massimo numero colli per documento. I valori verranno azzerati")
      endif
      this.oParentObject.w_MVQTACOL = 0
      this.w_MYSELF.FirstRow()     
      do while Not this.w_MYSELF.Eof_Trs()
        this.w_MYSELF.Set("w_MVNUMCOL", 0)     
        this.w_MYSELF.NextRow()     
      enddo
    else
      this.oParentObject.w_MVQTACOL = this.w_PROCOL
    endif
    this.oParentObject.w_MVQTALOR = this.oParentObject.w_MVQTALOR + this.w_TARCOL
    SELECT TMPCUR2
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo Colli a Cmposizione Mista
    * --- Estraggo in Colli da Rielaborare
    SELECT * FROM TMPCUR2 WHERE t_PROCOL IN (SELECT t_PROCOL FROM APPO) INTO CURSOR TMPCUR1 ;
    ORDER BY t_CODCOL,t_CODCONF, t_CODICE, t_MAXCON
    * --- Li elimino dal cursore dei Rielaborati
    DELETE FROM TMPCUR2 WHERE t_PROCOL IN (SELECT t_PROCOL FROM APPO)
    SELECT TMPCUR2
    GO TOP
    * --- Ciclo sul Cursore da riassegnare
    SELECT TMPCUR1
    GO TOP 
    SCAN FOR NOT EMPTY(t_CODCONF) 
    this.w_CDCONF = t_CODCONF
    this.w_CODCOL = t_CODCOL
    this.w_QTACON = NVL(t_QTACON,0)
    this.w_QTACON2 = NVL(t_QTACON2,0)
    this.w_QTA = NVL(t_QTAART,0)
    this.w_PC = NVL(t_PZCONF,0)
    this.w_TARA = NVL(t_PESTAR,0)
    this.w_FLMIX = NVL(t_FLCMIX,SPACE(1))
    this.w_PERTOL = NVL(t_PERTOL,0)
    this.w_PESNET = NVL(t_PESNET,0)
    this.w_PESLOR = NVL(t_PESLOR,0)
    this.w_NR = t_NUMORD
    this.w_ROWNUM = t_NUMRIG
    this.w_CODART = t_CODICE
    this.w_DESART = NVL(t_DESART,SPACE(40))
    this.w_UNIMIS = NVL(t_UNIMIS,SPACE(3))
    this.w_DESVO = NVL(t_VOLUME,SPACE(15))
    this.w_UMDIM = NVL(t_UMVOLU,SPACE(3))
    this.w_MAXCON = NVL(t_MAXCON,0)
    do while this.w_QTACON>0
      SELECT t_PROCOL, t_CODCOL, t_MAXCON,cp_round(SUM((t_QTACON2*100)/(100-t_PERTOL)),5) AS QTACON2;
      FROM TMPCUR2 WHERE t_FLCMIX="S" INTO CURSOR APPO GROUP BY t_PROCOL, t_CODCOL ORDER BY t_CODCOL,t_CODICE,QTACON2, t_PROCOL
      SELECT APPO 
      LOCATE FOR t_CODCOL=this.w_CODCOL AND INT(QTACON2) < 100 and MIN(INT((this.w_MAXCON * INT(100 - INT(QTACON2))) / 100),this.w_QTACON) <>0
      if FOUND()
        this.w_COLLO = t_PROCOL
        * --- Calcola la percentuale ancora libera del collo
        this.w_CONAGG2 = INT(100 - INT(QTACON2))
        * --- Calcola quante confezioni possono rientrare nel collo
        this.w_CONAGG = MIN(INT((this.w_MAXCON * this.w_CONAGG2) / 100),this.w_QTACON)
        if this.w_CONAGG<=0
          EXIT
        endif
        this.w_ARTAGG = this.w_QTA
        if this.w_CONAGG<>this.w_QTACON
          this.w_ARTAGG = MIN(INT(this.w_CONAGG*this.w_PC),this.w_QTA)
        endif
        INSERT INTO TMPCUR2;
        (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
         t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
        t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2) ;
        VALUES;
        (this.w_COLLO,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
        this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
        this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
        cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6) )
        this.w_QTACON = this.w_QTACON-this.w_CONAGG
        this.w_QTA = this.w_QTA-this.w_ARTAGG
      else
        EXIT
      endif
    enddo
    do while this.w_QTACON>0
      this.w_CONAGG = MIN(this.w_MAXCON,this.w_QTACON)
      if this.w_CONAGG=0
        EXIT
      endif
      this.w_ARTAGG = this.w_QTA
      if this.w_CONAGG<>this.w_QTACON
        this.w_APPO = INT(this.w_CONAGG*this.w_PC)
        this.w_ARTAGG = MIN(this.w_APPO,this.w_QTA)
      endif
      this.w_PROCOL = this.w_PROCOL+1
      INSERT INTO TMPCUR2;
      (t_PROCOL, t_CODCONF, t_CODCOL, t_QTACON, t_MAXCON, t_PESTAR, t_FLCMIX,;
       t_PERTOL, t_PESNET, t_PESLOR, t_NUMRIG, t_NUMORD,;
      t_CODICE, t_DESART, t_QTAART, t_PZCONF, t_UNIMIS, t_VOLUME, t_UMVOLU, t_QTACON2) ;
      VALUES;
      (this.w_PROCOL ,this.w_CDCONF, this.w_CODCOL, this.w_CONAGG, this.w_MAXCON, this.w_TARA, this.w_FLMIX,;
      this.w_PERTOL, this.w_PESNET, this.w_PESLOR, this.w_ROWNUM, this.w_NR,;
      this.w_CODART, this.w_DESART, this.w_ARTAGG, this.w_PC, this.w_UNIMIS, this.w_DESVO, this.w_UMDIM, ;
      cp_ROUND((this.w_CONAGG * 100) / this.w_MAXCON, 6) )
      this.w_QTACON = this.w_QTACON-this.w_CONAGG
      this.w_QTA = this.w_QTA-this.w_ARTAGG
    enddo
    SELECT TMPCUR1
    ENDSCAN
    * --- Rinumero i Colli 
    SELECT t_PROCOL, MAX(t_PESTAR) AS t_PESTAR FROM TMPCUR2 INTO CURSOR APPO GROUP BY t_PROCOL ORDER BY t_PROCOL
    SELECT APPO
    GO TOP
    this.w_PROCOL = 0
    this.w_TARCOL = 0
    SCAN
    this.w_OLDCOL = t_PROCOL
    this.w_PROCOL = this.w_PROCOL + 1
    this.w_TARCOL = this.w_TARCOL + t_PESTAR
    UPDATE TMPCUR2 SET t_PROCOL=this.w_PROCOL, t_QTACON2=-87 WHERE t_PROCOL=this.w_OLDCOL AND t_QTACON2<>-87
    ENDSCAN
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_COLL'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='TIP_COLL'
    this.cWorkTables[5]='TIPICONF'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- gsve_bc2
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PARAM='V'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1='GENEAPP'
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg(cMsgErr,'stop','')
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_PADRE.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PARAM='V'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_PADRE.FirstRow()
   endif
  EndProc
  
  Function Eof_Trs() 
   if This.w_PARAM='V'
    RETURN (EOF('GENEAPP'))
   else
  	 RETURN (this.w_PADRE.Eof_Trs())
   endif
  ENDFUNC
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( 'GENEAPP' )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_PADRE.SetRow( id_Row )
   endif
  
  Endproc
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName='t_'+SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( 'GENEAPP' )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_PADRE.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select 'GENEAPP'
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_PADRE.NextRow()
   Endif
  	
  EndProc
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
   If This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	cName_1='GENEAPP'
  	cCursName=SYS(2015)	
  	* Se ricerca a partire da un certo record aggiungo la condizione..
  	IF TYPE('StartFrom')='N' AND StartFrom<>0
  	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
  	ELSE
  	cCond=' 1=1 '
  	Endif
  		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND  		&Criterio INTO CURSOR &cCursName
  	* leggo il risultato
  	SELECT(cCursName)
  	GO Top
  	nResult=NVL(&cCursName..Riga,-1) 
  	nResult=IIF(nResult=0,-1, nResult)
  	* rimuovo il cursore di appoggio
      if used(cCursName)
        select (cCursName)
        use
      ENDIF    
  	* ripristino la vecchia area
  	SELECT(coldArea)
  	* restituisco il risultato
  	RETURN nResult
    
   Else
    Return(This.w_PADRE.Search(Criterio,StartFrom))
   Endif
  
  EndFunc
  
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PARAM='V'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1='GENEAPP'
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg(cMsgErr,'stop','')
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_PADRE.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PARAM='V'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_PADRE.FirstRow()
   endif
  EndProc
  
  Function Eof_Trs() 
   if This.w_PARAM='V'
    RETURN (EOF('GENEAPP'))
   else
  	 RETURN (this.w_PADRE.Eof_Trs())
   endif
  ENDFUNC
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( 'GENEAPP' )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_PADRE.SetRow( id_Row )
   endif
  
  Endproc
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName='t_'+SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( 'GENEAPP' )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_PADRE.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select 'GENEAPP'
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_PADRE.NextRow()
   Endif
  	
  EndProc
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
   If This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	cName_1='GENEAPP'
  	cCursName=SYS(2015)	
  	* Se ricerca a partire da un certo record aggiungo la condizione..
  	IF TYPE('StartFrom')='N' AND StartFrom<>0
  	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
  	ELSE
  	cCond=' 1=1 '
  	Endif
  		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND  		&Criterio INTO CURSOR &cCursName
  	* leggo il risultato
  	SELECT(cCursName)
  	GO Top
  	nResult=NVL(&cCursName..Riga,-1) 
  	nResult=IIF(nResult=0,-1, nResult)
  	* rimuovo il cursore di appoggio
      if used(cCursName)
        select (cCursName)
        use
      ENDIF    
  	* ripristino la vecchia area
  	SELECT(coldArea)
  	* restituisco il risultato
  	RETURN nResult
    
   Else
    Return(This.w_PADRE.Search(Criterio,StartFrom))
   Endif
  
  EndFunc
  
  
  Procedure SET(cItem,vValue,bNoUpd)
  LOCAL cOldArea,cFldName,bResult
   If This.w_PARAM='V'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cItem,2))='w_' 
  		cFldName='t_'+SUBSTR(cItem,3,LEN(cItem))
  	ELSE
  		cFldName=cItem
  	ENDIF
  
  	* Aggiorno il transitorio normale...
  	SELECT( 'GENEAPP' )
  		
  	* Se cambio allora svolgo la Replace..
  	IF &cFldName<>vValue
  		Replace &cFldName WITH vValue
  	endif
  			
  	* ripristino la vecchia area
  	SELECT(coldArea) 
   Else
    This.w_PADRE.Set(cItem,vValue,bNoUpd)
   Endif
  EndProc
  
  Function FullRow()
   If This.w_PARAM='V'
    Select ( 'GENEAPP' )
    return ( t_MVTIPRIG<>' ' AND t_MVTIPRIG<>'V' AND NOT EMPTY(t_MVCODICE) )
    else
     return ( This.w_PADRE.Fullrow() )
   Endif
  Endfunc
  
  Procedure Done()
   *Azzerro la variabile di comodo, se non lo facessi rimarebbe la classe
   *del batch appesa in memoria, impedendo tra l'altro la compilazione
   *all'interno di ad hoc
   this.w_MYSELF=.Null.
   DoDefault()
  EndpRoc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
