* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bab                                                        *
*              Riapertura di bilancio                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_39]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-06-24                                                      *
* Last revis.: 2010-04-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bab",oParentObject)
return(i_retval)

define class tgscg_bab as StdBatch
  * --- Local variables
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_PNSERIAL = space(10)
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_PNNUMRER = 0
  w_PNCODVAL = space(3)
  w_PNVALNAZ = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPREG = space(2)
  w_PNCOMPET = space(4)
  w_UTCC = 0
  w_PNCAOVAL = 0
  w_PNDESSUP = space(50)
  w_CONSUP = space(15)
  w_OKREG = .f.
  w_INIREG = .f.
  w_UTCV = 0
  w_SALDO = 0
  w_SLDARFIN = 0
  w_PNPRG = space(8)
  w_UTDV = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_PNTIPCON = space(1)
  w_SLTIPCON = space(1)
  w_UTDC = ctod("  /  /  ")
  w_CODAZI = space(4)
  w_PNIMPDAR = 0
  w_SLAVEFIN = 0
  w_SLCODICE = space(15)
  w_PNFLSALF = space(1)
  w_PNCODCON = space(15)
  w_APPO = 0
  w_PNFLSALI = space(1)
  w_PNFLZERO = space(1)
  w_PNIMPAVE = 0
  w_PNDESRIG = space(50)
  w_ESEPRE = space(4)
  w_FINPRE = ctod("  /  /  ")
  w_CPROWORD = 0
  w_PNFLSALD = space(1)
  w_AGGIORNA = .f.
  w_PNCAURIG = space(5)
  w_VALPRE = space(3)
  w_DECIMI = 0
  w_TOTRIG = 0
  w_MESS = space(50)
  w_DIVIDE = 0
  w_TOTARR = 0
  w_SEZBIL = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  ESERCIZI_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  MASTRI_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apertura Conto Economico (da GSCG_KAB)
    * --- Variabili Locali
    this.w_ESEPRE = SPACE(4)
    * --- Cerca l'esercizio Immediatamente Precedente
    this.w_ESEPRE = ""
    this.w_FINPRE = this.oParentObject.w_INIESE-1
    this.w_CODAZI = i_CODAZI
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ  from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE="+cp_ToStrODBC(this.w_FINPRE)+"";
           ,"_Curs_ESERCIZI")
    else
      select ESCODESE,ESINIESE,ESFINESE,ESVALNAZ from (i_cTable);
       where ESCODAZI=this.w_CODAZI AND ESFINESE=this.w_FINPRE;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      this.w_ESEPRE = NVL(_Curs_ESERCIZI.ESCODESE, SPACE(4))
      this.w_VALPRE = NVL(_Curs_ESERCIZI.ESVALNAZ, SPACE(3))
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    if EMPTY(this.w_ESEPRE)
      ah_ErrorMsg("Non esiste un esercizio precedente a quello da aprire",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Controlli Preliminari
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case NOT this.oParentObject.w_DATREG > this.w_STALIG
        ah_ErrorMsg("Data registrazione inferiore o uguale a ultima stampa libro giornale",,"")
        i_retcode = 'stop'
        return
      case Not Empty(CHKCONS("P",this.oParentObject.w_DATREG,"B","N"))
        ah_ErrorMsg("%1 operazione annullata",,"",CHKCONS("P",this.oParentObject.w_DATREG,"B","N"))
        i_retcode = 'stop'
        return
      case this.oParentObject.w_DATREG <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
        ah_ErrorMsg("Prima nota bloccata - semaforo bollati in dati azienda.%0Operazione annullata",,"")
        i_retcode = 'stop'
        return
    endcase
    * --- Controllo se non vi sinao altre registrazioni con la causale scelta per la chiusura
    * --- nell'esercizio - La Solita Query � utilizzata nell'Chiusura Economica di Esercizio
    * --- Select from GSCG_BCE
    do vq_exec with 'GSCG_BCE',this,'_Curs_GSCG_BCE','',.f.,.t.
    if used('_Curs_GSCG_BCE')
      select _Curs_GSCG_BCE
      locate for 1=1
      do while not(eof())
      if NOT EMPTY(NVL(_Curs_GSCG_BCE.PNSERIAL,""))
        ah_ErrorMsg("In prima nota, per l'esecizio da aprire, sono gi� presenti registrazioni di apertura%0� necessario annullarle prima di procedere.",,"")
        i_retcode = 'stop'
        return
      endif
        select _Curs_GSCG_BCE
        continue
      enddo
      use
    endif
    * --- Try
    local bErr_03EE62C0
    bErr_03EE62C0=bTrsErr
    this.Try_03EE62C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile bloccare la prima nota - semaforo bollati in dati azienda.%0Operazione annullata",,"")
    endif
    bTrsErr=bTrsErr or bErr_03EE62C0
    * --- End
    * --- Aggiorna Variabili Primanota
    this.w_PNCODESE = this.oParentObject.w_CODESE
    this.w_PNCOMPET = this.oParentObject.w_CODESE
    this.w_PNDATREG = this.oParentObject.w_DATREG
    this.w_PNDESSUP = this.oParentObject.w_DESSUP
    this.w_PNCODCAU = this.oParentObject.w_CODCAU
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
    endif
    if Empty(this.w_PNDESSUP) and Not Empty(this.w_CCDESSUP) 
      this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    this.w_PNVALNAZ = this.oParentObject.w_PERVAL
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNTIPREG = "N"
    this.w_PNCODVAL = this.oParentObject.w_PERVAL
    this.w_PNFLSALD = "+"
    this.w_PNCAOVAL = 0
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    * --- Testa se effettua registrazioni
    this.w_OKREG = .F.
    this.w_INIREG = .F.
    this.w_TOTRIG = 0
    this.w_TOTARR = 0
    this.w_CPROWORD = 0
    this.w_CPROWNUM = 0
    * --- Inizia la transazione...
    * --- Try
    local bErr_0402E440
    bErr_0402E440=bTrsErr
    this.Try_0402E440()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(I_ERRMSG,,"")
    endif
    bTrsErr=bTrsErr or bErr_0402E440
    * --- End
    * --- Sblocca Primanota
    * --- Try
    local bErr_0380C558
    bErr_0380C558=bTrsErr
    this.Try_0380C558()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile sbloccare la prima nota - semaforo bollati in dati azienda.%0Sbloccarlo manualmente",,"")
    endif
    bTrsErr=bTrsErr or bErr_0380C558
    * --- End
  endproc
  proc Try_03EE62C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Blocca Primanota - da sola si autotransa
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_INIESE),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = this.oParentObject.w_INIESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0402E440()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNCAOVAL = GETCAM(this.oParentObject.w_PERVAL,this.oParentObject.w_DATREG,0)
    * --- Mettere un controllo se 0 deve uscire
    * --- Leggo il tasso della valuta dell'esercizio precedente
    this.w_DIVIDE = 0
    this.w_DIVIDE = GETCAM(this.w_VALPRE, this.oParentObject.w_DATREG)
    * --- Controllo che il conto arrotondamento sia stato inserito nella maschera
    if this.w_VALPRE<>this.oParentObject.w_PERVAL and EMPTY(this.oParentObject.w_CODARR)
      * --- Raise
      i_Error="Inserire un conto arrotondamento - esercizi con valute diverse"
      return
    endif
    * --- Altrimenti leggo dai saldi
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se valute diverse tra esercizi scrivo come penultima riga dell'ultimo doc. l'arrotondamento
    if this.w_VALPRE<>this.oParentObject.w_PERVAL
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Determina Utile/Perdita di Esercizio
    if this.w_OKREG=.F.
      * --- Abbandona la Transazione
      * --- Raise
      i_Error="NESSUN SALDO VALORIZZATO PER L'ESERCIZIO CONSIDERATO"
      return
    else
      * --- Conferma la Transazione
      * --- commit
      cp_EndTrs(.t.)
      ah_Msg("RIAPERTURA DI BILANCIO COMPLETATA",.T.)
    endif
    return
  proc Try_0380C558()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Riga Finale di Chiusura (ATTENZIONE: Conto di Tipo Transitorio, non aggiorna il Saldo Iniziale)
    if this.w_TOTRIG <> 0
      ah_Msg("Scrivo riga chiusura registrazione",.T.)
      this.w_PNTIPCON = this.oParentObject.w_TIPCON
      this.w_PNCODCON = this.oParentObject.w_CODCON
      this.w_PNIMPDAR = IIF(this.w_TOTRIG<0, ABS(this.w_TOTRIG), 0)
      this.w_PNIMPAVE = IIF(this.w_TOTRIG>0, this.w_TOTRIG, 0)
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM)
        insert into (i_cTable) (PNSERIAL,CPROWNUM &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +",PNCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +",PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
        +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
        +",PNFLPART ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PNTIPCON = this.w_PNTIPCON;
            ,PNCODCON = this.w_PNCODCON;
            ,PNIMPDAR = this.w_PNIMPDAR;
            ,PNIMPAVE = this.w_PNIMPAVE;
            ,PNFLSALI = " ";
            ,PNDESRIG = this.w_PNDESRIG;
            ,PNCAURIG = this.w_PNCAURIG;
            ,CPROWORD = this.w_CPROWORD;
            ,PNFLSALD = this.w_PNFLSALD;
            ,PNFLZERO = this.w_PNFLZERO;
            ,PNFLSALF = " ";
            ,PNFLPART = "N";
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Conto Profitti e Perdite
      * --- Try
      local bErr_03EE0DA0
      bErr_03EE0DA0=bTrsErr
      this.Try_03EE0DA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03EE0DA0
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.w_PNTIPCON;
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.oParentObject.w_CODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TOTRIG = 0
      WAIT CLEAR
    endif
  endproc
  proc Try_03EE0DA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo Arrotondamento  - SE DIVERSO DA 0
    if this.w_TOTARR<>0
      this.w_PNTIPCON = this.oParentObject.w_TIPCON
      this.w_PNCODCON = this.oParentObject.w_CODARR
      this.w_PNIMPDAR = IIF(this.w_TOTARR<0, cp_ROUND(ABS(this.w_TOTARR), this.w_DECIMI), 0)
      this.w_PNIMPAVE = IIF(this.w_TOTARR>0, cp_ROUND(this.w_TOTARR, this.w_DECIMI), 0)
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM)
        insert into (i_cTable) (PNSERIAL,CPROWNUM &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +",PNCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +",PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
        +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
        +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
        +",PNFLPART ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PNTIPCON = this.w_PNTIPCON;
            ,PNCODCON = this.w_PNCODCON;
            ,PNIMPDAR = this.w_PNIMPDAR;
            ,PNIMPAVE = this.w_PNIMPAVE;
            ,PNFLSALI = this.w_PNFLSALI;
            ,PNDESRIG = this.w_PNDESRIG;
            ,PNCAURIG = this.w_PNCAURIG;
            ,CPROWORD = this.w_CPROWORD;
            ,PNFLSALD = this.w_PNFLSALD;
            ,PNFLZERO = this.w_PNFLZERO;
            ,PNFLSALF = " ";
            ,PNFLPART = "N";
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Conto Profitti e Perdite
      * --- Try
      local bErr_03849FB8
      bErr_03849FB8=bTrsErr
      this.Try_03849FB8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03849FB8
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
        +",SLAVEINI =SLAVEINI+ "+cp_ToStrODBC(this.w_PNIMPAVE);
        +",SLDARINI =SLDARINI+ "+cp_ToStrODBC(this.w_PNIMPDAR);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            ,SLAVEINI = SLAVEINI + this.w_PNIMPAVE;
            ,SLDARINI = SLDARINI + this.w_PNIMPDAR;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.w_PNTIPCON;
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.oParentObject.w_CODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Nell'ultima riga considero anche l'arrotondamento
      this.w_TOTRIG = this.w_TOTRIG-this.w_TOTARR
    endif
  endproc
  proc Try_03849FB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Registrazione
    * --- Legge Saldi Conti Economici <> 0
    * --- Select from SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SLTIPCON,SLCODICE,SLDARFIN,SLAVEFIN  from "+i_cTable+" SALDICON ";
          +" where SLCODESE="+cp_ToStrODBC(this.w_ESEPRE)+" AND ((SLDARFIN-SLAVEFIN)<>0)";
          +" order by SLTIPCON,SLCODICE";
           ,"_Curs_SALDICON")
    else
      select SLTIPCON,SLCODICE,SLDARFIN,SLAVEFIN from (i_cTable);
       where SLCODESE=this.w_ESEPRE AND ((SLDARFIN-SLAVEFIN)<>0);
       order by SLTIPCON,SLCODICE;
        into cursor _Curs_SALDICON
    endif
    if used('_Curs_SALDICON')
      select _Curs_SALDICON
      locate for 1=1
      do while not(eof())
      this.w_AGGIORNA = .F.
      this.w_PNTIPCON = NVL(_Curs_SALDICON.SLTIPCON, " ")
      this.w_PNCODCON = NVL(_Curs_SALDICON.SLCODICE, SPACE(15))
      this.w_CONSUP = SPACE(15)
      this.w_SEZBIL = " "
      if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PNTIPCON="G"
          * --- Read from MASTRI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MASTRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCSEZBIL"+;
              " from "+i_cTable+" MASTRI where ";
                  +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCSEZBIL;
              from (i_cTable) where;
                  MCCODICE = this.w_CONSUP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        do case
          case this.w_PNTIPCON="C"
            * --- Cliente
            ah_Msg("Elabora cliente: %1",.T.,.F.,.F.,this.w_PNCODCON)
            this.w_AGGIORNA = .T.
          case this.w_PNTIPCON="F"
            * --- Fornitore
            ah_Msg("Elabora fornitore: %1",.T.,.F.,.F.,this.w_PNCODCON)
            this.w_AGGIORNA = .T.
          case this.w_PNTIPCON="G" AND this.w_SEZBIL $ "APO"
            * --- Conto Patrimoniale o Ordine  (SEZBIL A o P o O)
            ah_Msg("Elabora conto: %1",.T.,.F.,.F.,this.w_PNCODCON)
            this.w_AGGIORNA = .T.
        endcase
      endif
      if this.w_AGGIORNA=.T.
        * --- Se i due esercizi hanno valuta diversa applico il cambio - tra monete europee
        if this.w_VALPRE<>this.oParentObject.w_PERVAL
          this.w_APPO = val2mon((NVL(SLDARFIN, 0) - NVL(SLAVEFIN, 0)),this.w_DIVIDE, this.w_PNCAOVAL,this.oParentObject.w_DATREG,this.oParentObject.w_PERVAL,this.w_DECIMI)
          * --- Uguale a TOTRIG ma se pi� di una registrazione non viene azzerato come TOTRIG
          this.w_TOTARR = this.w_TOTARR+this.w_APPO
        else
          this.w_APPO = cp_ROUND(NVL(SLDARFIN, 0) - NVL(SLAVEFIN, 0),this.w_DECIMI)
        endif
        this.w_TOTRIG = this.w_TOTRIG + this.w_APPO
        this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
        this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
        this.w_PNFLSALI = "+"
        this.w_PNDESRIG = this.oParentObject.w_DESSUP
        if Empty(this.w_PNDESRIG) and Not Empty(this.w_CCDESRIG)
          this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
        endif
        this.w_PNCAURIG = this.oParentObject.w_CODCAU
        * --- Scrive Primanota (Anagrafica)
        if this.w_INIREG=.F.
          * --- Calcola PNSERIAL e PNNUMRER
          this.w_PNSERIAL = SPACE(10)
          this.w_PNNUMRER = 0
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
          cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
          * --- Creo la testata
          * --- Insert into PNT_MAST
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PNSERIAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL)
            insert into (i_cTable) (PNSERIAL &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Write into PNT_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PNDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
            +",PNNUMRER ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
            +",PNCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
            +",PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
            +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
            +",PNPRG ="+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
            +",PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
            +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
            +",PNCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
            +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
            +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
            +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
            +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
            +",PNCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
            +",PNCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
            +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
                +i_ccchkf ;
            +" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                PNDATREG = this.w_PNDATREG;
                ,PNNUMRER = this.w_PNNUMRER;
                ,PNCODCAU = this.w_PNCODCAU;
                ,PNTIPREG = this.w_PNTIPREG;
                ,PNCODVAL = this.w_PNCODVAL;
                ,PNPRG = this.w_PNPRG;
                ,PNCAOVAL = this.w_PNCAOVAL;
                ,PNDESSUP = this.w_PNDESSUP;
                ,PNCOMPET = this.w_PNCOMPET;
                ,PNVALNAZ = this.w_PNVALNAZ;
                ,UTCC = this.w_UTCC;
                ,UTCV = this.w_UTCV;
                ,UTDC = this.w_UTDC;
                ,UTDV = this.w_UTDV;
                ,PNCODESE = this.w_PNCODESE;
                ,PNCODUTE = this.w_PNCODUTE;
                ,PNFLPROV = "N";
                &i_ccchkf. ;
             where;
                PNSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_INIREG = .T.
          this.w_OKREG = .T.
        endif
        * --- Scrive Reg.Contabili (Movimentazione)
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
        * --- Insert into PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLSALI"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLZERO"+",PNFLSALF"+",PNFLPART"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLSALI',this.w_PNFLSALI,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNFLSALD',this.w_PNFLSALD,'PNFLZERO',this.w_PNFLZERO)
          insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLSALI,PNDESRIG,PNCAURIG,PNFLSALD,PNFLZERO,PNFLSALF,PNFLPART &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_PNTIPCON;
               ,this.w_PNCODCON;
               ,this.w_PNIMPDAR;
               ,this.w_PNIMPAVE;
               ,this.w_PNFLSALI;
               ,this.w_PNDESRIG;
               ,this.w_PNCAURIG;
               ,this.w_PNFLSALD;
               ,this.w_PNFLZERO;
               ," ";
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Aggiorna Saldo
        * --- Try
        local bErr_03F19700
        bErr_03F19700=bTrsErr
        this.Try_03F19700()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03F19700
        * --- End
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +",SLAVEINI =SLAVEINI+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +",SLDARINI =SLDARINI+ "+cp_ToStrODBC(this.w_PNIMPDAR);
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_PNIMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
              ,SLAVEINI = SLAVEINI + this.w_PNIMPAVE;
              ,SLDARINI = SLDARINI + this.w_PNIMPDAR;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.oParentObject.w_CODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_CPROWORD>1000
        * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Quindi scrive una nuova Registrazione
        this.w_INIREG = .F.
      endif
        select _Curs_SALDICON
        continue
      enddo
      use
    endif
  endproc
  proc Try_03F19700()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='PNT_DETT'
    this.cWorkTables[5]='PNT_MAST'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='MASTRI'
    this.cWorkTables[9]='CAU_CONT'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_GSCG_BCE')
      use in _Curs_GSCG_BCE
    endif
    if used('_Curs_SALDICON')
      use in _Curs_SALDICON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
