* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kle                                                        *
*              Log generazione                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-16                                                      *
* Last revis.: 2012-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kle",oParentObject))

* --- Class definition
define class tgsve_kle as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 763
  Height = 468
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-20"
  HelpContextID=216674921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  DOC_MAST_IDX = 0
  cPrg = "gsve_kle"
  cComment = "Log generazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_GDSERIAL = space(10)
  w_MESSAGG = space(0)
  w_TIPOEL = space(1)
  w_SERIALD = space(10)
  w_SERIALO = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_FLTLOG = space(1)
  o_FLTLOG = space(1)
  w_LOGGEN = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_klePag1","gsve_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLTLOG_1_15
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LOGGEN = this.oPgFrm.Pages(1).oPag.LOGGEN
    DoDefault()
    proc Destroy()
      this.w_LOGGEN = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GDSERIAL=space(10)
      .w_MESSAGG=space(0)
      .w_TIPOEL=space(1)
      .w_SERIALD=space(10)
      .w_SERIALO=space(10)
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_FLTLOG=space(1)
      .w_GDSERIAL=oParentObject.w_GDSERIAL
      .oPgFrm.Page1.oPag.LOGGEN.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_MESSAGG = .w_LOGGEN.GETVAR('LGMESSAG')
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .w_TIPOEL = .w_LOGGEN.GETVAR('LGTIPRIG')
        .w_SERIALD = .w_LOGGEN.GETVAR('LGSERDES')
        .w_SERIALO = .w_LOGGEN.GETVAR('LGSERORI')
        .w_FLVEAC = IIF(EMPTY(NVL(.w_SERIALO, ' ')), .w_LOGGEN.GETVAR('FLVEACDE') , .w_LOGGEN.GETVAR('FLVEACOR'))
        .w_CLADOC = IIF(EMPTY(NVL(.w_SERIALO, ' ')), .w_LOGGEN.GETVAR('CLADOCDE') , .w_LOGGEN.GETVAR('CLADOCOR'))
        .w_FLTLOG = 'X'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_GDSERIAL=.w_GDSERIAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.LOGGEN.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_MESSAGG = .w_LOGGEN.GETVAR('LGMESSAG')
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
            .w_TIPOEL = .w_LOGGEN.GETVAR('LGTIPRIG')
            .w_SERIALD = .w_LOGGEN.GETVAR('LGSERDES')
            .w_SERIALO = .w_LOGGEN.GETVAR('LGSERORI')
            .w_FLVEAC = IIF(EMPTY(NVL(.w_SERIALO, ' ')), .w_LOGGEN.GETVAR('FLVEACDE') , .w_LOGGEN.GETVAR('FLVEACOR'))
            .w_CLADOC = IIF(EMPTY(NVL(.w_SERIALO, ' ')), .w_LOGGEN.GETVAR('CLADOCDE') , .w_LOGGEN.GETVAR('CLADOCOR'))
        if .o_FLTLOG<>.w_FLTLOG
          .Calculate_DJCHVTFOZX()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LOGGEN.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
    endwith
  return

  proc Calculate_DJCHVTFOZX()
    with this
          * --- Riesegui zoom
          EseguiQuery(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.LOGGEN.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMESSAGG_1_4.value==this.w_MESSAGG)
      this.oPgFrm.Page1.oPag.oMESSAGG_1_4.value=this.w_MESSAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTLOG_1_15.RadioValue()==this.w_FLTLOG)
      this.oPgFrm.Page1.oPag.oFLTLOG_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLTLOG = this.w_FLTLOG
    return

enddefine

* --- Define pages as container
define class tgsve_klePag1 as StdContainer
  Width  = 759
  height = 468
  stdWidth  = 759
  stdheight = 468
  resizeXpos=395
  resizeYpos=216
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object LOGGEN as cp_zoombox with uid="DRRSXLQTTH",left=7, top=6, width=744,height=350,;
    caption='LOG',;
   bGlobalFont=.t.,;
    cTable="LOG_GEND",bRetriveAllRows=.t.,cZoomFile="GSVE_KLE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Blank, Requery",;
    nPag=1;
    , ToolTipText = "Elenco delle segnalazioni di elaborazione";
    , HelpContextID = 216362570

  add object oMESSAGG_1_4 as StdMemo with uid="VNFCJPLOBP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MESSAGG", cQueryName = "MESSAGG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Messaggio esteso del log",;
    HelpContextID = 243156934,;
   bGlobalFont=.t.,;
    Height=50, Width=643, Left=108, Top=358


  add object oBtn_1_5 as StdButton with uid="GZOOPOHXGN",left=598, top=412, width=48,height=45,;
    CpPicture="bmp\doc.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento selezionato";
    , HelpContextID = 258046106;
    , TABSTOP = .F., Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        OpenGest('A', "GSVE_MDV('"+.w_FLVEAC+.w_CLADOC+"')",'MVSERIAL',IIF(EMPTY(NVL(.w_SERIALO,' ')), .w_SERIALD, .w_SERIALO))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALO,' ')) OR NOT EMPTY(NVL(.w_SERIALD,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION')
     endwith
    endif
  endfunc


  add object oObj_1_6 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=412, width=486,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 229758182


  add object oBtn_1_7 as StdButton with uid="ZNDDDDZGXK",left=650, top=412, width=48,height=45,;
    CpPicture="BMP\Stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 142706454;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not EMPTY(.w_GDSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="KFLQFEHBDN",left=701, top=412, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 197490246;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oFLTLOG_1_15 as StdCombo with uid="VLLPUSVVVU",rtseq=8,rtrep=.f.,left=108,top=439,width=245,height=21;
    , ToolTipText = "Filtro log";
    , HelpContextID = 257384022;
    , cFormVar="w_FLTLOG",RowSource=""+"Tutti i messaggi,"+"Messaggi di attenzione o errore,"+"Messaggi di attenzione,"+"Messaggi di errore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTLOG_1_15.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'X',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oFLTLOG_1_15.GetRadio()
    this.Parent.oContained.w_FLTLOG = this.RadioValue()
    return .t.
  endfunc

  func oFLTLOG_1_15.SetRadio()
    this.Parent.oContained.w_FLTLOG=trim(this.Parent.oContained.w_FLTLOG)
    this.value = ;
      iif(this.Parent.oContained.w_FLTLOG=='T',1,;
      iif(this.Parent.oContained.w_FLTLOG=='X',2,;
      iif(this.Parent.oContained.w_FLTLOG=='A',3,;
      iif(this.Parent.oContained.w_FLTLOG=='E',4,;
      0))))
  endfunc


  add object oBtn_1_18 as StdButton with uid="BMJSZOJMYV",left=598, top=412, width=48,height=45,;
    CpPicture="bmp\doc.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento selezionato";
    , HelpContextID = 258046106;
    , TABSTOP = .F., Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,EVL(NVL(.w_SERIALO," "), .w_SERIALD),-20)

      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALO,' ')) OR NOT EMPTY(NVL(.w_SERIALD,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION='ad hoc ENTERPRISE')
     endwith
    endif
  endfunc

  add object oStr_1_3 as StdString with uid="GJSPQFRWJU",Visible=.t., Left=40, Top=358,;
    Alignment=1, Width=64, Height=18,;
    Caption="Messaggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BJNVGYJOHJ",Visible=.t., Left=7, Top=412,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="FKURPRKGOV",Visible=.t., Left=48, Top=441,;
    Alignment=1, Width=56, Height=18,;
    Caption="Filtro log:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsve_kle
proc EseguiQuery(obj)
   obj.NotifyEvent('Requery')
endproc
* --- Fine Area Manuale
