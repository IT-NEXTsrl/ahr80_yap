* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_b1c                                                        *
*              Lancia gestione file                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-02                                                      *
* Last revis.: 2002-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Chiave,w_Programma,w_Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_b1c",oParentObject,m.w_Chiave,m.w_Programma,m.w_Operazione)
return(i_retval)

define class tgsma_b1c as StdBatch
  * --- Local variables
  w_Chiave = space(20)
  w_Programma = space(20)
  w_Operazione = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    gsut_bsw(this,this.w_Chiave, this.w_Programma, this.w_Operazione)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do gsma_bsv with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,w_Chiave,w_Programma,w_Operazione)
    this.w_Chiave=w_Chiave
    this.w_Programma=w_Programma
    this.w_Operazione=w_Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Chiave,w_Programma,w_Operazione"
endproc
