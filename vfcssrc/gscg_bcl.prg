* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcl                                                        *
*              Caricamento rapido lettere d'intento                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-03                                                      *
* Last revis.: 2017-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcl",oParentObject,m.pParam)
return(i_retval)

define class tgscg_bcl as StdBatch
  * --- Local variables
  pParam = space(1)
  w_DICODICE = space(15)
  w_DICODCON = space(15)
  w_DITIPIVA = space(1)
  w_DIFLSTAM = space(1)
  w_DINUMDIC = 0
  w_DI__ANNO = space(4)
  w_DIDATDIC = ctod("  /  /  ")
  w_DIDOGANA = space(30)
  w_DIUFFIVA = space(25)
  w_DINUMDOC = 0
  w_DIDATLET = ctod("  /  /  ")
  w_DICODIVA = space(5)
  w_IVADES = space(35)
  w_DIARTESE = space(5)
  w_DIDESOPE = space(0)
  w_DITIPOPE = space(1)
  w_DIIMPDIC = 0
  w_DIIMPUTI = 0
  w_DIDATINI = ctod("  /  /  ")
  w_DIDATFIN = ctod("  /  /  ")
  w_DIUFIVFO = space(25)
  w_CODSEL = space(15)
  w_CONTA = 0
  w_CONTA1 = 0
  w_DISERIAL = space(15)
  w_DISERIAL_AHE = space(15)
  w_PRIMO = space(10)
  w_ULTIMO = space(10)
  w_DITIPCON = space(1)
  w_DITIPIVA = space(1)
  w_DISERDIC = space(3)
  w_DISERDOC = space(3)
  w_DIAUTON2 = space(7)
  * --- WorkFile variables
  VOCIIVA_idx=0
  CONTI_idx=0
  DIC_INTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DICODICE = SPACE(15)
    this.w_DITIPCON = SPACE(1)
    this.w_DICODCON = SPACE(15)
    this.w_DITIPIVA = SPACE(1)
    this.w_DIFLSTAM = SPACE(1)
    this.w_DINUMDIC = 0
    this.w_DI__ANNO = SPACE(4)
    this.w_DIDATDIC = cp_CharToDate("  -  -  ")
    this.w_DIDOGANA = SPACE(30)
    this.w_DIUFFIVA = SPACE(25)
    this.w_DINUMDOC = 0
    this.w_DIDATLET = cp_CharToDate("  -  -  ")
    this.w_DICODIVA = SPACE(5)
    this.w_DIARTESE = SPACE(5)
    this.w_DIDESOPE = SPACE(50)
    this.w_DITIPOPE = SPACE(1)
    this.w_DIIMPDIC = 0
    this.w_DIIMPUTI = 0
    this.w_DIDATINI = cp_CharToDate("  -  -  ")
    this.w_DIDATFIN = cp_CharToDate("  -  -  ")
    this.w_DIUFIVFO = SPACE(25)
    this.w_DISERDIC = SPACE(3)
    this.w_DISERDOC = SPACE(3)
    this.w_DIAUTON2 = SPACE(7)
    this.w_CONTA = 0
    this.w_CONTA1 = 0
    * --- Se pParam='Z', lancio lo zoom e valorizzo le variabili della maschera
    if this.pParam="Z"
      this.oParentObject.w_DOGANA = SPACE(30)
      this.oParentObject.w_CODICE = SPACE(15)
      vx_exec("QUERY\GSCG_KDI.VZM",this)
      this.oParentObject.w_ANNO1 = this.w_DI__ANNO
      this.oParentObject.w_ARTESE = this.w_DIARTESE
      this.oParentObject.w_CODICE = this.w_DICODICE
      this.oParentObject.w_CODIVA = this.w_DICODIVA
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVDESIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVDESIVA;
          from (i_cTable) where;
              IVCODIVA = this.oParentObject.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IVADES = NVL(cp_ToDate(_read_.IVDESIVA),cp_NullValue(_read_.IVDESIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_DESIVA = this.w_IVADES
      this.oParentObject.w_DATA = Cp_todate(this.w_DIDATDIC)
      this.oParentObject.w_DATFIN = cp_CharToDate("  -  -  ")
      this.oParentObject.w_DATINI = CP_TODATE(I_DATSYS)
      this.oParentObject.w_DATLET = this.oParentObject.w_DATDIC
      this.oParentObject.w_DESOPE = nvl (this.w_DIDESOPE," ")
      this.oParentObject.w_DOGANA = this.w_DIDOGANA
      this.oParentObject.w_FLSTAM = "N"
      this.oParentObject.w_IMPDIC = this.w_DIIMPDIC
      this.oParentObject.w_IMPUTI = 0
      this.oParentObject.w_NUMDIC = this.w_DINUMDIC
      this.oParentObject.w_NUMDOC = this.w_DINUMDOC
      this.oParentObject.w_TIPCON = this.w_DITIPCON
      this.oParentObject.w_TIPOCF = this.w_DITIPCON
      this.oParentObject.w_TIPIVA = this.w_DITIPIVA
      this.oParentObject.w_TIPOPE = this.w_DITIPOPE
      this.oParentObject.w_UFFIVA = this.w_DIUFFIVA
      this.oParentObject.w_UFIVFO = this.w_DIUFIVFO
      this.oParentObject.w_SERIAL = this.w_DISERIAL
      this.oParentObject.w_SERDIC = this.w_DISERDIC
      this.oParentObject.w_SERDOC = this.w_DISERDOC
      this.oParentObject.w_AUTON2 = this.w_DIAUTON2
    else
      * --- Alla conferma, vado ad inserire i dati nell'archivio delle lettere d'intento in base alle selezioni impostate.
      if this.oParentObject.w_AGGDOG = "S" And Empty(this.oParentObject.w_CODDOG)
        AH_ERRORMSG("Inserire dogana nella pagina selezioni aggiuntive")
        i_retcode = 'stop'
        return
      endif
      if this.oParentObject.w_TIPOPE $"IO" And this.oParentObject.w_IMPDIC = 0
        AH_ERRORMSG("Inserire importo massimo")
        i_retcode = 'stop'
        return
      endif
      * --- Try
      local bErr_031A4648
      bErr_031A4648=bTrsErr
      this.Try_031A4648()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_errormsg("%1", 48, "",i_ErrMsg)
      endif
      bTrsErr=bTrsErr or bErr_031A4648
      * --- End
      if this.oParentObject.w_AGGDOG="S"
        AH_ERRORMSG("Operazione completata: inserita lettera d'intento numero %1",48,"",this.w_PRIMO)
      else
        if this.w_CONTA=0
          AH_ERRORMSG("Operazione completata: nessuna lettera d'intento inserita",48,"")
        else
          if this.w_CONTA=1
            AH_ERRORMSG("Operazione completata: %1 lettera d'intento inserita numero %2",48,"", ALLTRIM(STR(this.w_CONTA)), this.w_PRIMO)
          else
            AH_ERRORMSG("Operazione completata: %1 lettere d'intento inserite dal numero %2 al numero %3",48,"", ALLTRIM(STR(this.w_CONTA)), this.w_PRIMO,this.w_ULTIMO)
          endif
        endif
      endif
    endif
  endproc
  proc Try_031A4648()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_AGGDOG="S"
      i_Conn=i_TableProp[this.DIC_INTE_IDX, 3]
      this.w_DI__ANNO = this.oParentObject.w_ANNO
      this.w_DITIPCON = this.oParentObject.w_TIPCON
      this.w_DINUMDOC = 0
      this.w_DITIPIVA = this.oParentObject.w_TIPIVA
      this.w_DISERDIC = this.oParentObject.w_SERDIC
      this.w_DISERDOC = this.oParentObject.w_SERDOC
      this.w_DIAUTON2 = ALLTRIM(this.w_DI__ANNO)+ALLTRIM(this.w_DISERDOC)
      cp_NextTableProg(this, i_Conn, "SEDIN", "i_codazi,w_DISERIAL_AHE")
      cp_NextTableProg(this, i_Conn, "PRDIN", "i_codazi,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
      cp_NextTableProg(this, i_Conn, "PRDID", "i_codazi,w_DIAUTON2,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
      * --- Insert into DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DI__ANNO"+",DIARTESE"+",DICODICE"+",DICODIVA"+",DIDATDIC"+",DIDATFIN"+",DIDATINI"+",DIDATLET"+",DIDESOPE"+",DIDOGANA"+",DIFLSTAM"+",DIIMPDIC"+",DIIMPUTI"+",DINUMDIC"+",DINUMDOC"+",DISERIAL"+",DITIPCON"+",DITIPIVA"+",DITIPOPE"+",DIUFFIVA"+",DISERDIC"+",DISERDOC"+",DIAUTON1"+",DIAUTON2"+",DIUFIVFO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'DIC_INTE','DI__ANNO');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARTESE),'DIC_INTE','DIARTESE');
        +","+cp_NullLink(cp_ToStrODBC(Space(15)),'DIC_INTE','DICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIVA),'DIC_INTE','DICODIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDIC),'DIC_INTE','DIDATDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'DIC_INTE','DIDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINI),'DIC_INTE','DIDATINI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLET),'DIC_INTE','DIDATLET');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESOPE),'DIC_INTE','DIDESOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODDOG),'DIC_INTE','DIDOGANA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLSTAM),'DIC_INTE','DIFLSTAM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMPDIC),'DIC_INTE','DIIMPDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMPUTI),'DIC_INTE','DIIMPUTI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDIC),'DIC_INTE','DINUMDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDOC),'DIC_INTE','DINUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL_AHE),'DIC_INTE','DISERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'DIC_INTE','DITIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPIVA),'DIC_INTE','DITIPIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOPE),'DIC_INTE','DITIPOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UFFIVA),'DIC_INTE','DIUFFIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERDIC),'DIC_INTE','DISERDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERDOC),'DIC_INTE','DISERDOC');
        +","+cp_NullLink(cp_ToStrODBC(Space(7)),'DIC_INTE','DIAUTON1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DIAUTON2),'DIC_INTE','DIAUTON2');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UFIVFO),'DIC_INTE','DIUFIVFO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DI__ANNO',this.oParentObject.w_ANNO,'DIARTESE',this.oParentObject.w_ARTESE,'DICODICE',Space(15),'DICODIVA',this.oParentObject.w_CODIVA,'DIDATDIC',this.oParentObject.w_DATDIC,'DIDATFIN',this.oParentObject.w_DATFIN,'DIDATINI',this.oParentObject.w_DATINI,'DIDATLET',this.oParentObject.w_DATLET,'DIDESOPE',this.oParentObject.w_DESOPE,'DIDOGANA',this.oParentObject.w_CODDOG,'DIFLSTAM',this.oParentObject.w_FLSTAM,'DIIMPDIC',this.oParentObject.w_IMPDIC)
        insert into (i_cTable) (DI__ANNO,DIARTESE,DICODICE,DICODIVA,DIDATDIC,DIDATFIN,DIDATINI,DIDATLET,DIDESOPE,DIDOGANA,DIFLSTAM,DIIMPDIC,DIIMPUTI,DINUMDIC,DINUMDOC,DISERIAL,DITIPCON,DITIPIVA,DITIPOPE,DIUFFIVA,DISERDIC,DISERDOC,DIAUTON1,DIAUTON2,DIUFIVFO &i_ccchkf. );
           values (;
             this.oParentObject.w_ANNO;
             ,this.oParentObject.w_ARTESE;
             ,Space(15);
             ,this.oParentObject.w_CODIVA;
             ,this.oParentObject.w_DATDIC;
             ,this.oParentObject.w_DATFIN;
             ,this.oParentObject.w_DATINI;
             ,this.oParentObject.w_DATLET;
             ,this.oParentObject.w_DESOPE;
             ,this.oParentObject.w_CODDOG;
             ,this.oParentObject.w_FLSTAM;
             ,this.oParentObject.w_IMPDIC;
             ,this.oParentObject.w_IMPUTI;
             ,this.w_DINUMDIC;
             ,this.w_DINUMDOC;
             ,this.w_DISERIAL_AHE;
             ,this.oParentObject.w_TIPCON;
             ,this.oParentObject.w_TIPIVA;
             ,this.oParentObject.w_TIPOPE;
             ,this.oParentObject.w_UFFIVA;
             ,this.oParentObject.w_SERDIC;
             ,this.oParentObject.w_SERDOC;
             ,Space(7);
             ,this.w_DIAUTON2;
             ,this.oParentObject.w_UFIVFO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_PRIMO = ALLTRIM(STR(this.w_DINUMDIC))+iif(!EMPTY(nvl(this.w_DISERDIC,"")),"/"+ALLTRIM(this.w_DISERDIC),"")
      this.w_ULTIMO = ALLTRIM(STR(this.w_DINUMDIC))+iif(!EMPTY(nvl(this.w_DISERDIC,"")),"/"+ALLTRIM(this.w_DISERDIC),"")
      this.w_CONTA = this.w_CONTA+1
    else
      Select (this.oParentObject.w_ZOOMFOR.cCursor) 
 Go top
      SCAN FOR XCHK = 1
      this.w_CODSEL = ANCODICE
      this.w_DICODCON = SPACE(15)
      this.w_DINUMDIC = 0
      this.w_DI__ANNO = this.oParentObject.w_ANNO
      i_Conn=i_TableProp[this.DIC_INTE_IDX, 3]
      this.w_DITIPCON = this.oParentObject.w_TIPCON
      this.w_DINUMDOC = 0
      this.w_DITIPIVA = this.oParentObject.w_TIPIVA
      this.w_DISERIAL_AHE = Space(15)
      this.w_DISERDIC = this.oParentObject.w_SERDIC
      this.w_DISERDOC = this.oParentObject.w_SERDOC
      this.w_DIAUTON2 = ALLTRIM(this.w_DI__ANNO)+ALLTRIM(this.w_DISERDOC)
      cp_NextTableProg(this, i_Conn, "SEDIN", "i_codazi,w_DISERIAL_AHE")
      cp_NextTableProg(this, i_Conn, "PRDIN", "i_codazi,w_DI__ANNO,w_DITIPCON,w_DISERDIC,w_DINUMDIC")
      cp_NextTableProg(this, i_Conn, "PRDID", "i_codazi,w_DIAUTON2,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
      * --- Insert into DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DI__ANNO"+",DIARTESE"+",DICODICE"+",DICODIVA"+",DIDATDIC"+",DIDATFIN"+",DIDATINI"+",DIDATLET"+",DIDESOPE"+",DIDOGANA"+",DIFLSTAM"+",DIIMPDIC"+",DIIMPUTI"+",DINUMDIC"+",DINUMDOC"+",DISERIAL"+",DITIPCON"+",DITIPIVA"+",DITIPOPE"+",DIUFFIVA"+",DISERDIC"+",DISERDOC"+",DIAUTON1"+",DIAUTON2"+",DIUFIVFO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'DIC_INTE','DI__ANNO');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARTESE),'DIC_INTE','DIARTESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODSEL),'DIC_INTE','DICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIVA),'DIC_INTE','DICODIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDIC),'DIC_INTE','DIDATDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'DIC_INTE','DIDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINI),'DIC_INTE','DIDATINI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLET),'DIC_INTE','DIDATLET');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESOPE),'DIC_INTE','DIDESOPE');
        +","+cp_NullLink(cp_ToStrODBC(Space(30)),'DIC_INTE','DIDOGANA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLSTAM),'DIC_INTE','DIFLSTAM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMPDIC),'DIC_INTE','DIIMPDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMPUTI),'DIC_INTE','DIIMPUTI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDIC),'DIC_INTE','DINUMDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDOC),'DIC_INTE','DINUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL_AHE),'DIC_INTE','DISERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'DIC_INTE','DITIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPIVA),'DIC_INTE','DITIPIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOPE),'DIC_INTE','DITIPOPE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UFFIVA),'DIC_INTE','DIUFFIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERDIC),'DIC_INTE','DISERDIC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERDOC),'DIC_INTE','DISERDOC');
        +","+cp_NullLink(cp_ToStrODBC(Space(7)),'DIC_INTE','DIAUTON1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DIAUTON2),'DIC_INTE','DIAUTON2');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UFIVFO),'DIC_INTE','DIUFIVFO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DI__ANNO',this.oParentObject.w_ANNO,'DIARTESE',this.oParentObject.w_ARTESE,'DICODICE',this.w_CODSEL,'DICODIVA',this.oParentObject.w_CODIVA,'DIDATDIC',this.oParentObject.w_DATDIC,'DIDATFIN',this.oParentObject.w_DATFIN,'DIDATINI',this.oParentObject.w_DATINI,'DIDATLET',this.oParentObject.w_DATLET,'DIDESOPE',this.oParentObject.w_DESOPE,'DIDOGANA',Space(30),'DIFLSTAM',this.oParentObject.w_FLSTAM,'DIIMPDIC',this.oParentObject.w_IMPDIC)
        insert into (i_cTable) (DI__ANNO,DIARTESE,DICODICE,DICODIVA,DIDATDIC,DIDATFIN,DIDATINI,DIDATLET,DIDESOPE,DIDOGANA,DIFLSTAM,DIIMPDIC,DIIMPUTI,DINUMDIC,DINUMDOC,DISERIAL,DITIPCON,DITIPIVA,DITIPOPE,DIUFFIVA,DISERDIC,DISERDOC,DIAUTON1,DIAUTON2,DIUFIVFO &i_ccchkf. );
           values (;
             this.oParentObject.w_ANNO;
             ,this.oParentObject.w_ARTESE;
             ,this.w_CODSEL;
             ,this.oParentObject.w_CODIVA;
             ,this.oParentObject.w_DATDIC;
             ,this.oParentObject.w_DATFIN;
             ,this.oParentObject.w_DATINI;
             ,this.oParentObject.w_DATLET;
             ,this.oParentObject.w_DESOPE;
             ,Space(30);
             ,this.oParentObject.w_FLSTAM;
             ,this.oParentObject.w_IMPDIC;
             ,this.oParentObject.w_IMPUTI;
             ,this.w_DINUMDIC;
             ,this.w_DINUMDOC;
             ,this.w_DISERIAL_AHE;
             ,this.oParentObject.w_TIPCON;
             ,this.oParentObject.w_TIPIVA;
             ,this.oParentObject.w_TIPOPE;
             ,this.oParentObject.w_UFFIVA;
             ,this.oParentObject.w_SERDIC;
             ,this.oParentObject.w_SERDOC;
             ,Space(7);
             ,this.w_DIAUTON2;
             ,this.oParentObject.w_UFIVFO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_CONTA = this.w_CONTA+1
      if this.w_CONTA=1
        this.w_PRIMO = ALLTRIM(STR(this.w_DINUMDIC))+iif(!EMPTY(nvl(this.w_DISERDIC,"")),"/"+ALLTRIM(this.w_DISERDIC),"")
        this.w_ULTIMO = ALLTRIM(STR(this.w_DINUMDIC))+iif(!EMPTY(nvl(this.w_DISERDIC,"")),"/"+ALLTRIM(this.w_DISERDIC),"")
      else
        this.w_ULTIMO = ALLTRIM(STR(this.w_DINUMDIC))+iif(!EMPTY(nvl(this.w_DISERDIC,"")),"/"+ALLTRIM(this.w_DISERDIC),"")
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DIC_INTE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
