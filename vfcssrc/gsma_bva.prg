* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bva                                                        *
*              Variazione dati articoli/servizi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_206]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-15                                                      *
* Last revis.: 2018-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bva",oParentObject)
return(i_retval)

define class tgsma_bva as StdBatch
  * --- Local variables
  w_TIPCOD = space(2)
  w_CODART = space(20)
  w_CODDIS = space(20)
  w_CODIVA = space(5)
  w_GRUMER = space(5)
  w_CATCON = space(5)
  w_CODFAM = space(5)
  w_CATOMO = space(5)
  w_VOCCEN = space(15)
  w_VOCRIC = space(15)
  w_CODRIC = space(5)
  w_CATSCM = space(5)
  w_CODMAR = space(5)
  w_GRUPRO = space(5)
  w_TIPGES = space(1)
  w_PROPRE = space(1)
  w_ARTIPART = space(2)
  w_CODFOR = space(15)
  w_CONTA = 0
  w_CONFE1 = space(3)
  w_CONFE2 = space(3)
  w_COLLO1 = space(5)
  w_COLLO2 = space(5)
  w_COLLOR = space(5)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_COCOL1 = 0
  w_COCOL2 = 0
  w_PZCON1 = 0
  w_PZCON2 = 0
  w_FLAGG = .f.
  w_AGGIO = 0
  w_MESS = space(20)
  w_COCOL = 0
  w_COSTO = 0
  w_COSOLD = 0
  w_TESCOS = .f.
  w_TEST = .f.
  w_CODVAL = space(3)
  w_CAOGEN = 0
  w_QUANTI = 0
  w_SC1 = 0
  w_SC2 = 0
  w_SC3 = 0
  w_SC4 = 0
  w_ROWNUM = 0
  w_TESCOS = .f.
  w_ARTPOS = space(1)
  w_CODREP = space(3)
  w_FLPECO = space(1)
  w_PUBWEB = space(1)
  w_OLDFLGCAC = space(1)
  w_NEWFLGCAC = space(1)
  w_ARMINVEN = 0
  w_ARCODTIP = space(35)
  w_ARCODVAL = space(35)
  w_ARCODCLF = space(5)
  w_ARFLGESC = space(1)
  w_APPO = space(10)
  w_AGGIOK = 0
  w_AGGIO_FOR = 0
  w_AGGIO_COSTO = 0
  w_TIPOPE = space(10)
  w_UTISER = space(1)
  w_PRESTA = space(1)
  w_SACFLG = space(5)
  w_CACOCOL3 = 0
  w_CATPCON3 = space(3)
  w_CAPZCON3 = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  KEY_ARTI_idx=0
  CON_COLL_idx=0
  LIS_SCAG_idx=0
  INVEDETT_idx=0
  CONTRART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variazione Dati Articoli/Servizi (da GSMA_KVA)
    * --- Selezioni
    * --- Campi da Variare
    if this.oParentObject.w_TIPSEL="S"
      * --- Servizi
      this.oParentObject.w_FAMFIL = " "
      this.oParentObject.w_CATOFIL = " "
      this.oParentObject.w_PROFIL = " "
      this.oParentObject.w_FORFIL = " "
      this.oParentObject.w_MARFIL = " "
      this.oParentObject.w_COLFIL = " "
    endif
    this.w_TESCOS = .T.
    * --- Verifica se selezionato Almeno un Check di selezione
    this.w_TEST = this.oParentObject.w_CLASEL<>"S" AND this.oParentObject.w_SCOSEL<>"S" AND this.oParentObject.w_MARSEL<>"S" AND this.oParentObject.w_CAPSEL<>"S" AND this.oParentObject.w_COLSEL<>"S" AND this.oParentObject.w_VOCSEL<>"S" 
    this.w_TEST = this.oParentObject.w_VORSEL<>"S" AND this.oParentObject.w_FORSEL<>"S" AND this.oParentObject.w_GESSEL<>"S" AND this.oParentObject.w_PROVSEL<>"S" AND this.oParentObject.w_PUBSEL<>"S" AND this.oParentObject.w_TIPASEL<> "S" AND this.w_TEST
    if g_GPOS="S"
      this.w_TEST = this.oParentObject.w_FLGPOS="X" AND this.oParentObject.w_REPSEL<>"S" AND this.oParentObject.w_PECOSEL<>"S" AND this.w_TEST
    else
      this.oParentObject.w_REPFIL = SPACE(3)
    endif
    if G_COAC = "S"
      this.w_TEST = this.oParentObject.w_FLGCAC="X" AND this.w_TEST
    endif
    if this.oParentObject.w_IVASEL<>"S" AND this.oParentObject.w_GRMSEL<>"S" AND this.oParentObject.w_CATSEL<>"S" AND this.oParentObject.w_FAMSEL<>"S" AND this.oParentObject.w_CATOSEL<>"S" AND this.oParentObject.w_COSSEL<>"S" AND this.oParentObject.w_CIMSEL<>"S" And this.w_TEST AND this.oParentObject.w_SACSEL<>"S" and this.oParentObject.w_UTISEL <>"S" and this.oParentObject.w_sQTAMIN<>"S" and this.oParentObject.w_TIPOSEL<>"S" AND this.oParentObject.w_VALOSEL<>"S" AND this.oParentObject.w_CLASSEL<>"S" AND this.oParentObject.w_GESCSEL<>"S"
      ah_ErrorMsg("Selezionare almeno un campo da sostituire")
      i_retcode = 'stop'
      return
    endif
    * --- Verifica se esiste un Codice IVA da sostituire
    if EMPTY(this.oParentObject.w_IVAFIN) AND this.oParentObject.w_IVASEL="S"
      ah_ErrorMsg("Inserire il codice IVA in sostituzione del codice originario")
      i_retcode = 'stop'
      return
    endif
    * --- Verifica se esiste la Cat.Contabile da sostituire
    if EMPTY(this.oParentObject.w_CATFIN) AND this.oParentObject.w_CATSEL="S"
      ah_ErrorMsg("Inserire la cat.contabile in sostituzione della categoria originaria")
      i_retcode = 'stop'
      return
    endif
    this.w_COCOL = 0
    if this.oParentObject.w_COLSEL="S" AND NOT EMPTY(this.oParentObject.w_COLFIN)
      if EMPTY(this.oParentObject.w_CONDEF)
        ah_ErrorMsg("Occorre indicare un tipo confezione per il collo specificato")
        i_retcode = 'stop'
        return
      else
        this.w_APPO = "   "
        * --- Read from CON_COLL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TCCODCON,TCQTACON"+;
            " from "+i_cTable+" CON_COLL where ";
                +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_COLFIN);
                +" and TCCODCON = "+cp_ToStrODBC(this.oParentObject.w_CONDEF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TCCODCON,TCQTACON;
            from (i_cTable) where;
                TCCODICE = this.oParentObject.w_COLFIN;
                and TCCODCON = this.oParentObject.w_CONDEF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO = NVL(cp_ToDate(_read_.TCCODCON),cp_NullValue(_read_.TCCODCON))
          this.w_COCOL = NVL(cp_ToDate(_read_.TCQTACON),cp_NullValue(_read_.TCQTACON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_APPO<>this.oParentObject.w_CONDEF OR EMPTY(this.w_APPO)
          this.w_MESS = "La confezione specificata non � compresa tra quelle previste per il collo da inserire"
          ah_ErrorMsg(this.w_MESS)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.oParentObject.w_COSSEL="S"
      do case
        case this.oParentObject.w_AGGIORN<>"LI" AND this.oParentObject.w_AGGIORN<>"US"
          if EMPTY(this.oParentObject.w_ESERCI)
            ah_ErrorMsg("Inserire codice esercizio")
            i_retcode = 'stop'
            return
          endif
          if EMPTY(this.oParentObject.w_NUMINV)
            ah_ErrorMsg("Inserire inventario di riferimento")
            i_retcode = 'stop'
            return
          endif
          this.w_CODVAL = this.oParentObject.w_VALNAZ
          this.w_CAOGEN = this.oParentObject.w_CAOVAL
        case this.oParentObject.w_AGGIORN = "LI" 
          if EMPTY(this.oParentObject.w_LISTRIF)
            ah_ErrorMsg("Inserire codice listino di riferimento")
            i_retcode = 'stop'
            return
          endif
          * --- Leggo tutti i listini associati agli articoli da modificare, nella SELECT successiva
          *     eseguo una Locate per recuperare le informazioni di rivalorizzazione...
          vq_exec("QUERY\GSMA_QLI.VQR",this,"RIVALO")
          this.w_CODVAL = this.oParentObject.w_VALRIF
          this.w_CAOGEN = IIF(this.oParentObject.w_CAOLIS=0,this.oParentObject.w_CAMBIO,this.oParentObject.w_CAOLIS)
      endcase
    endif
    if this.oParentObject.w_sQTAMIN="S" AND EMPTY(this.oParentObject.w_UMPRI) 
      ah_ErrorMsg("Specificare l'unit� di misura primaria","stop")
      i_retcode = 'stop'
      return
    endif
    * --- Esecuzione Query di Ricerca
    this.w_CONTA = 0
    this.w_FLAGG = .F.
    this.w_AGGIO = 0
    this.w_AGGIOK = 0
    this.w_AGGIO_FOR = 0
    this.w_AGGIO_COSTO = 0
    this.w_COCOL = IIF(this.w_COCOL=0, 1, this.w_COCOL)
    * --- Try
    local bErr_0499F138
    bErr_0499F138=bTrsErr
    this.Try_0499F138()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Elaborazione annullata, errore: %1",,"", i_ErrMsg)
      if used("RIVALO")
        select RIVALO
        use
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_0499F138
    * --- End
    if used("RIVALO")
      select RIVALO
      use
    endif
    if this.w_AGGIO=0
      if this.oParentObject.w_TIPSEL="A"
        ah_ErrorMsg("Per le selezioni impostate, non ci sono articoli da aggiornare")
      else
        ah_ErrorMsg("Per le selezioni impostate, non ci sono servizi da aggiornare")
      endif
    else
      * --- oggetto per mess. incrementali
      this.w_oMESS=createobject("ah_message")
      do case
        case this.w_AGGIO_FOR<>0 AND this.w_AGGIO_COSTO<>0
          this.w_oPART = this.w_oMESS.addmsgpartNL("Numero articoli/servizi aggiornati: %1%0di cui %2 fornitori abituali%0di cui %3 costi standard%0su un totale di %4 articoli/servizi selezionati")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO_FOR)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO_COSTO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTA)))     
        case this.w_AGGIO_FOR<>0
          this.w_oPART = this.w_oMESS.addmsgpartNL("Numero articoli/servizi aggiornati: %1%0di cui %2 fornitori abituali%0su un totale di %3 articoli/servizi selezionati")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO_FOR)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTA)))     
        case this.w_AGGIO_COSTO<>0
          this.w_oPART = this.w_oMESS.addmsgpartNL("Numero articoli/servizi aggiornati: %1%0di cui %2 costi standard%0su un totale di %3 articoli/servizi selezionati")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO_COSTO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTA)))     
        otherwise
          this.w_oPART = this.w_oMESS.addmsgpartNL("Numero articoli/servizi aggiornati: %1%0su un totale di %2 articoli/servizi selezionati")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIO)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTA)))     
      endcase
      if this.oParentObject.w_COLSEL= "S"
        this.w_oPART = this.w_oMESS.addmsgpart("%0Aggiornati inoltre %1 codici di ricerca")
        this.w_oPART.addParam(ALLTRIM(STR(this.w_AGGIOK)))     
      endif
      this.w_oMESS.ah_ErrorMsg()     
    endif
  endproc
  proc Try_0499F138()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from QUERY\GSMA_QVA
    do vq_exec with 'QUERY\GSMA_QVA',this,'_Curs_QUERY_GSMA_QVA','',.f.,.t.
    if used('_Curs_QUERY_GSMA_QVA')
      select _Curs_QUERY_GSMA_QVA
      locate for 1=1
      do while not(eof())
      this.w_CODART = NVL(_Curs_QUERY_GSMA_QVA.ARCODART," ")
      this.w_CODIVA = NVL(_Curs_QUERY_GSMA_QVA.ARCODIVA, SPACE(5))
      this.w_TIPOPE = NVL(_Curs_QUERY_GSMA_QVA.ARTIPOPE, SPACE(10))
      this.w_GRUMER = NVL(_Curs_QUERY_GSMA_QVA.ARGRUMER," ")
      this.w_CATCON = NVL(_Curs_QUERY_GSMA_QVA.ARCATCON," ")
      this.w_CODFAM = NVL(_Curs_QUERY_GSMA_QVA.ARCODFAM," ")
      this.w_CATOMO = NVL(_Curs_QUERY_GSMA_QVA.ARCATOMO," ")
      this.w_CODRIC = NVL(_Curs_QUERY_GSMA_QVA.ARCODRIC," ")
      this.w_CATSCM = NVL(_Curs_QUERY_GSMA_QVA.ARCATSCM," ")
      this.w_CODMAR = NVL(_Curs_QUERY_GSMA_QVA.ARCODMAR," ")
      this.w_GRUPRO = NVL(_Curs_QUERY_GSMA_QVA.ARGRUPRO," ")
      this.w_VOCCEN = NVL(_Curs_QUERY_GSMA_QVA.ARVOCCEN," ")
      this.w_VOCRIC = NVL(_Curs_QUERY_GSMA_QVA.ARVOCRIC," ")
      this.w_CODFOR = NVL(_Curs_QUERY_GSMA_QVA.PRCODFOR," ")
      this.w_TIPGES = NVL(_Curs_QUERY_GSMA_QVA.ARTIPGES," ")
      this.w_PROPRE = NVL(_Curs_QUERY_GSMA_QVA.ARPROPRE," ")
      this.w_ARTIPART = NVL(_Curs_QUERY_GSMA_QVA.ARTIPART,"  ")
      this.w_CONFE1 = NVL(_Curs_QUERY_GSMA_QVA.ARTPCONF, "   ")
      this.w_CONFE2 = NVL(_Curs_QUERY_GSMA_QVA.ARTPCON2, "   ")
      this.w_COLLO1 = NVL(_Curs_QUERY_GSMA_QVA.ARTIPCO1, "   ")
      this.w_COLLO2 = NVL(_Curs_QUERY_GSMA_QVA.ARTIPCO2, "   ")
      this.w_UNMIS1 = NVL(_Curs_QUERY_GSMA_QVA.ARUNMIS1, "   ")
      this.w_UNMIS2 = NVL(_Curs_QUERY_GSMA_QVA.ARUNMIS2, "   ")
      this.w_COCOL1 = NVL(_Curs_QUERY_GSMA_QVA.ARCOCOL1, 0)
      this.w_COCOL2 = NVL(_Curs_QUERY_GSMA_QVA.ARCOCOL2, 0)
      this.w_PZCON1 = NVL(_Curs_QUERY_GSMA_QVA.ARPZCONF, 0)
      this.w_PZCON2 = NVL(_Curs_QUERY_GSMA_QVA.ARPZCON2, 0)
      this.w_COSTO = NVL(_Curs_QUERY_GSMA_QVA.PRCOSSTA,0)
      this.w_COSOLD = NVL(_Curs_QUERY_GSMA_QVA.PRCOSSTA,0)
      this.w_CODDIS = NVL(_Curs_QUERY_GSMA_QVA.ARCODDIS,SPACE(20))
      this.w_ARTPOS = NVL(_Curs_QUERY_GSMA_QVA.ARARTPOS, " ")
      this.w_CODREP = NVL(_Curs_QUERY_GSMA_QVA.ARCODREP, SPACE(3))
      this.w_FLPECO = IIF(EMPTY(NVL(_Curs_QUERY_GSMA_QVA.ARFLPECO, " ")), "P", NVL(_Curs_QUERY_GSMA_QVA.ARFLPECO, " "))
      this.w_PUBWEB = IIF(EMPTY(NVL(_Curs_QUERY_GSMA_QVA.ARPUBWEB, " ")), "N", NVL(_Curs_QUERY_GSMA_QVA.ARPUBWEB, " "))
      this.w_TIPCOD = NVL(_Curs_QUERY_GSMA_QVA.ARTIPART, "  ")
      this.w_OLDFLGCAC = NVL(_Curs_QUERY_GSMA_QVA.ARFLAPCA, " ")
      this.w_PRESTA = NVL(_Curs_QUERY_GSMA_QVA.ARPRESTA, " ")
      this.w_SACFLG = NVL(_Curs_QUERY_GSMA_QVA.ARFLSPAN, "N")
      this.w_UTISER = IIF(EMPTY(NVL(_Curs_QUERY_GSMA_QVA.ARUTISER, " ")), "N", NVL(_Curs_QUERY_GSMA_QVA.ARUTISER, " "))
      this.w_ARCODTIP = NVL(_Curs_QUERY_GSMA_QVA.ARCODTIP,SPACE(35))
      this.w_ARCODVAL = NVL(_Curs_QUERY_GSMA_QVA.ARCODVAL,SPACE(35))
      this.w_ARCODCLF = NVL(_Curs_QUERY_GSMA_QVA.ARCODCLF,SPACE(5))
      this.w_ARFLGESC = NVL(_Curs_QUERY_GSMA_QVA.ARFLGESC,SPACE(1))
      this.w_TEST = .T.
      * --- Sostituzione ART_ICOL
      if NOT EMPTY(this.w_CODART)
        ah_Msg("Elaborazione codice: %1",.T.,.F.,.F., ALLTRIM(this.w_CODART) )
        * --- Contatore Articoli Selezionati
        this.w_CONTA = this.w_CONTA+1
        this.w_FLAGG = .F.
        * --- Pubblica su WEB
        if this.oParentObject.w_PUBSEL="S" AND this.w_PUBWEB=this.oParentObject.w_PUBINI
          this.w_PUBWEB = this.oParentObject.w_PUBFIN
          this.w_FLAGG = .T.
        endif
        * --- Check P.O.S.
        if this.oParentObject.w_FLGPOS $ "SN"
          this.w_ARTPOS = IIF(this.oParentObject.w_FLGPOS="S", "S", " ")
          this.w_FLAGG = .T.
        endif
        * --- Spese anticipazioni
        if IsAlt() AND this.oParentObject.w_SACSEL="S" AND (this.w_SACFLG=this.oParentObject.w_SACINI OR this.oParentObject.w_SACALL="S") AND ! (this.w_PRESTA $ "A-S")
          this.w_SACFLG = this.oParentObject.w_SACFIN
          this.w_FLAGG = .T.
        endif
        if this.oParentObject.w_UTISEL="S" AND this.w_UTISER=this.oParentObject.w_UTIINI
          this.w_UTISER = this.oParentObject.w_UTIFIN
          this.w_FLAGG = .T.
        endif
        if this.w_TIPCOD<>"DE"
          * --- Codici IVA
          if this.oParentObject.w_IVASEL="S" AND (this.w_CODIVA=this.oParentObject.w_IVAINI OR (EMPTY(this.w_CODIVA) AND EMPTY(this.oParentObject.w_IVAINI)) OR this.oParentObject.w_IVAALL="S")
            this.w_CODIVA = this.oParentObject.w_IVAFIN
            this.w_FLAGG = .T.
          endif
          * --- Codice Operazione IVA
          if this.oParentObject.w_CIMSEL="S" AND (this.w_TIPOPE=this.oParentObject.w_CIMINI OR (EMPTY(this.w_TIPOPE) AND EMPTY(this.oParentObject.w_CIMINI)) OR this.oParentObject.w_CIMALL="S")
            this.w_TIPOPE = this.oParentObject.w_CIMFIN
            this.w_FLAGG = .T.
          endif
          * --- Gruppi Merceologici
          if this.oParentObject.w_GRMSEL="S" AND (this.w_GRUMER=this.oParentObject.w_GRMINI OR (EMPTY(this.w_GRUMER) AND EMPTY(this.oParentObject.w_GRMINI)) OR this.oParentObject.w_GRMALL="S")
            this.w_GRUMER = this.oParentObject.w_GRMFIN
            this.w_FLAGG = .T.
          endif
          * --- Categorie Contabili
          if this.oParentObject.w_CATSEL="S" AND (this.w_CATCON=this.oParentObject.w_CATINI OR (EMPTY(this.w_CATCON) AND EMPTY(this.oParentObject.w_CATINI)) OR this.oParentObject.w_CATALL="S")
            this.w_CATCON = this.oParentObject.w_CATFIN
            this.w_FLAGG = .T.
          endif
          * --- Famiglie
          if this.oParentObject.w_FAMSEL="S" AND (this.w_CODFAM=this.oParentObject.w_FAMINI OR (EMPTY(this.w_CODFAM) AND EMPTY(this.oParentObject.w_FAMINI)) OR this.oParentObject.w_FAMALL="S")
            this.w_CODFAM = this.oParentObject.w_FAMFIN
            this.w_FLAGG = .T.
          endif
          * --- Categorie Omogenee
          if this.oParentObject.w_CATOSEL="S" AND (this.w_CATOMO=this.oParentObject.w_CATOINI OR (EMPTY(this.w_CATOMO) AND EMPTY(this.oParentObject.w_CATOINI)) OR this.oParentObject.w_CATOALL="S")
            this.w_CATOMO = this.oParentObject.w_CATOFIN
            this.w_FLAGG = .T.
          endif
          * --- Classi di Ricarico
          if this.oParentObject.w_CLASEL="S" AND (this.w_CODRIC=this.oParentObject.w_CLAINI OR (EMPTY(this.w_CODRIC) AND EMPTY(this.oParentObject.w_CLAINI)) OR this.oParentObject.w_CLAALL="S")
            this.w_CODRIC = this.oParentObject.w_CLAFIN
            this.w_FLAGG = .T.
          endif
          * --- Cat.Sconti/Magg.
          if this.oParentObject.w_SCOSEL="S" AND (this.w_CATSCM=this.oParentObject.w_SCOINI OR (EMPTY(this.w_CATSCM) AND EMPTY(this.oParentObject.w_SCOINI)) OR this.oParentObject.w_SCOALL="S")
            this.w_CATSCM = this.oParentObject.w_SCOFIN
            this.w_FLAGG = .T.
          endif
          * --- Marche
          if this.oParentObject.w_MARSEL="S" AND (this.w_CODMAR=this.oParentObject.w_MARINI OR (EMPTY(this.w_CODMAR) AND EMPTY(this.oParentObject.w_MARINI)) OR this.oParentObject.w_MARALL="S")
            this.w_CODMAR = this.oParentObject.w_MARFIN
            this.w_FLAGG = .T.
          endif
          * --- Categ.Provvigioni
          if this.oParentObject.w_CAPSEL="S" AND (this.w_GRUPRO=this.oParentObject.w_CAPINI OR (EMPTY(this.w_GRUPRO) AND EMPTY(this.oParentObject.w_CAPINI)) OR this.oParentObject.w_CAPALL="S")
            this.w_GRUPRO = this.oParentObject.w_CAPFIN
            this.w_FLAGG = .T.
          endif
          * --- Voci di Costo
          if this.oParentObject.w_VOCSEL="S" AND (this.w_VOCCEN=this.oParentObject.w_VOCINI OR (EMPTY(this.w_VOCCEN) AND EMPTY(this.oParentObject.w_VOCINI)) OR this.oParentObject.w_VOCALL="S")
            this.w_VOCCEN = this.oParentObject.w_VOCFIN
            this.w_FLAGG = .T.
          endif
          * --- Voci di Ricavo
          if this.oParentObject.w_VORSEL="S" AND (this.w_VOCRIC=this.oParentObject.w_VORINI OR (EMPTY(this.w_VOCRIC) AND EMPTY(this.oParentObject.w_VORINI)) OR this.oParentObject.w_VORALL="S")
            this.w_VOCRIC = this.oParentObject.w_VORFIN
            this.w_FLAGG = .T.
          endif
          * --- Gestione
          if this.oParentObject.w_GESSEL="S" AND this.w_TIPGES=this.oParentObject.w_GESINI
            this.w_TIPGES = this.oParentObject.w_GESFIN
            this.w_FLAGG = .T.
          endif
          * --- Provenienza
          if this.oParentObject.w_PROVSEL="S" AND this.w_PROPRE=this.oParentObject.w_PROVINI
            this.w_PROPRE = this.oParentObject.w_PROVFIN
            this.w_FLAGG = .T.
          endif
          * --- Codice tipo
          if this.oParentObject.w_TIPOSEL="S" AND (this.w_ARCODTIP=this.oParentObject.w_TIPOINI OR (EMPTY(this.w_ARCODTIP) AND EMPTY(this.oParentObject.w_TIPOINI)) OR this.oParentObject.w_TIPOALL="S")
            this.w_ARCODTIP = this.oParentObject.w_TIPOFIN
            this.w_FLAGG = .T.
          endif
          * --- Codice valore
          if this.oParentObject.w_VALOSEL="S" AND (this.w_ARCODVAL=this.oParentObject.w_VALOINI OR (EMPTY(this.w_ARCODVAL) AND EMPTY(this.oParentObject.w_VALOINI)) OR this.oParentObject.w_VALOALL="S")
            this.w_ARCODVAL = this.oParentObject.w_VALOFIN
            this.w_FLAGG = .T.
          endif
          * --- Classificazione FE
          if this.oParentObject.w_CLASSEL="S" AND (this.w_ARCODCLF=this.oParentObject.w_CLASINI OR (EMPTY(this.w_ARCODCLF) AND EMPTY(this.oParentObject.w_CLASINI)) OR this.oParentObject.w_CLASALL="S")
            this.w_ARCODCLF = this.oParentObject.w_CLASFIN
            this.w_FLAGG = .T.
          endif
          * --- Articolo esclusivo
          if this.oParentObject.w_GESCSEL="S" AND (this.w_ARFLGESC=this.oParentObject.w_GESCINI OR (EMPTY(this.w_ARFLGESC) AND EMPTY(this.oParentObject.w_GESCINI)) OR this.oParentObject.w_GESCALL="S")
            this.w_ARFLGESC = this.oParentObject.w_GESCFIN
            this.w_FLAGG = .T.
          endif
          * --- Tipologia articolo
          if this.oParentObject.w_TIPASEL="S" AND this.w_ARTIPART=this.oParentObject.w_TIPARTIN
            this.w_ARTIPART = this.oParentObject.w_TIPARTIF
            this.w_FLAGG = .T.
          endif
          * --- Colli
          if this.oParentObject.w_COLSEL="S"
            * --- Collo associato alla 1^UM se si verificano tutte le seguenti condizioni
            *     Se presente Collo di Default e Articolo con Confezione e
            *     L'articolo rispetta l'eventuale filtro sul tipo collo (remove filter on empty param), filtro non svolto sulla query
            *     L'articolo rispetta l'eventuale filtro sulla confezione (remove filter on empty param), filtro non svolto sulla query
            *     L'utente ha selezionato <tutti> oppure l'articolo ha il collo specificato come valore iniziale
            if (NOT EMPTY(this.oParentObject.w_CONDEF) OR NOT EMPTY(this.w_CONFE1)) And (EMPTY(this.oParentObject.w_COLFIL) OR this.w_COLLO1=this.oParentObject.w_COLFIL) And (EMPTY(this.oParentObject.w_CONFIL) OR this.w_CONFE1=this.oParentObject.w_CONFIL) And ( this.w_COLLO1=this.oParentObject.w_COLINI OR (EMPTY(this.w_COLLO1) AND EMPTY(this.oParentObject.w_COLINI)) OR this.oParentObject.w_COLALL="S" )
              this.w_COLLO1 = this.oParentObject.w_COLFIN
              this.w_COCOL1 = IIF((NOT EMPTY(this.oParentObject.w_CONDEF) AND EMPTY(this.w_CONFE1)) OR this.w_COCOL1=0, this.w_COCOL, this.w_COCOL1)
              this.w_CONFE1 = IIF(EMPTY(this.w_CONFE1), this.oParentObject.w_CONDEF, this.w_CONFE1)
              * --- Read from CON_COLL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CON_COLL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TCCODICE"+;
                  " from "+i_cTable+" CON_COLL where ";
                      +"TCCODICE = "+cp_ToStrODBC(this.w_COLLO1);
                      +" and TCCODCON = "+cp_ToStrODBC(this.w_CONFE1);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TCCODICE;
                  from (i_cTable) where;
                      TCCODICE = this.w_COLLO1;
                      and TCCODCON = this.w_CONFE1;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COLLOR = NVL(cp_ToDate(_read_.TCCODICE),cp_NullValue(_read_.TCCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_ROWS=0
                this.w_CONFE1 = this.oParentObject.w_CONDEF
              endif
              this.w_PZCON1 = IIF(this.w_PZCON1=0, 1, this.w_PZCON1)
              this.w_FLAGG = .T.
            endif
            * --- Collo associato alla 2^UM
            *     Se presente Collo di Default e Articolo con Confezione e
            *     L'articolo rispetta l'eventuale filtro sul tipo collo
            *     L'articolo rispetta l'eventuale filtro sulla confezione
            *     L'utente ha selezionato <tutti> oppure l'articolo ha il collo specificato come valore iniziale
            if NOT EMPTY(this.w_UNMIS2) And (NOT EMPTY(this.oParentObject.w_CONDEF) OR NOT EMPTY(this.w_CONFE2)) And (EMPTY(this.oParentObject.w_COLFIL) OR this.w_COLLO2=this.oParentObject.w_COLFIL) And (EMPTY(this.oParentObject.w_CONFIL) OR this.w_CONFE2=this.oParentObject.w_CONFIL) And (this.w_COLLO2=this.oParentObject.w_COLINI OR (EMPTY(this.w_COLLO2) AND EMPTY(this.oParentObject.w_COLINI)) OR this.oParentObject.w_COLALL="S")
              this.w_COLLO2 = this.oParentObject.w_COLFIN
              this.w_COCOL2 = IIF((NOT EMPTY(this.oParentObject.w_CONDEF) AND EMPTY(this.w_CONFE2)) OR this.w_COCOL2=0, this.w_COCOL, this.w_COCOL2)
              this.w_CONFE2 = IIF(EMPTY(this.w_CONFE2), this.oParentObject.w_CONDEF, this.w_CONFE2)
              * --- Read from CON_COLL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CON_COLL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TCCODICE"+;
                  " from "+i_cTable+" CON_COLL where ";
                      +"TCCODICE = "+cp_ToStrODBC(this.w_COLLO2);
                      +" and TCCODCON = "+cp_ToStrODBC(this.w_CONFE2);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TCCODICE;
                  from (i_cTable) where;
                      TCCODICE = this.w_COLLO2;
                      and TCCODCON = this.w_CONFE2;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COLLOR = NVL(cp_ToDate(_read_.TCCODICE),cp_NullValue(_read_.TCCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_ROWS=0
                this.w_CONFE2 = this.oParentObject.w_CONDEF
              endif
              this.w_PZCON2 = IIF(this.w_PZCON2=0, 1, this.w_PZCON2)
              this.w_FLAGG = .T.
            endif
            * --- Ricerca eventuali colli nelle Chiavi di ricerca legate all'articolo
            * --- Select from KEY_ARTI
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CAUNIMIS,CACODICE,CATIPCO3,CATPCON3,CACOCOL3,CAPZCON3  from "+i_cTable+" KEY_ARTI ";
                  +" where CACODART= "+cp_ToStrODBC(this.w_CODART)+"";
                   ,"_Curs_KEY_ARTI")
            else
              select CAUNIMIS,CACODICE,CATIPCO3,CATPCON3,CACOCOL3,CAPZCON3 from (i_cTable);
               where CACODART= this.w_CODART;
                into cursor _Curs_KEY_ARTI
            endif
            if used('_Curs_KEY_ARTI')
              select _Curs_KEY_ARTI
              locate for 1=1
              do while not(eof())
              * --- Filtri su Collo/Confezione non eseguiti sulla SELECT (devo svolgere il remove on empty Parameter a mano..)
              *     Il codice di ricerca deve avere un'unit� di misura...
              if Not Empty(Nvl(_Curs_KEY_ARTI.CAUNIMIS,"")) And ( NOT EMPTY(this.oParentObject.w_CONDEF) OR NOT EMPTY(_Curs_KEY_ARTI.CATPCON3) ) And (EMPTY(this.oParentObject.w_COLFIL) OR _Curs_KEY_ARTI.CATIPCO3=this.oParentObject.w_COLFIL) And (EMPTY(this.oParentObject.w_CONFIL) OR _Curs_KEY_ARTI.CATPCON3=this.oParentObject.w_CONFIL) And ( _Curs_KEY_ARTI.CATIPCO3=this.oParentObject.w_COLINI OR (EMPTY(_Curs_KEY_ARTI.CATIPCO3) AND EMPTY(this.oParentObject.w_COLINI)) OR this.oParentObject.w_COLALL="S" )
                this.w_CACOCOL3 = IIF((NOT EMPTY(this.oParentObject.w_CONDEF) AND EMPTY(_Curs_KEY_ARTI.CATPCON3)) OR _Curs_KEY_ARTI.CACOCOL3=0, this.w_COCOL, _Curs_KEY_ARTI.CACOCOL3)
                this.w_CATPCON3 = IIF(EMPTY(_Curs_KEY_ARTI.CATPCON3), this.oParentObject.w_CONDEF, _Curs_KEY_ARTI.CATPCON3)
                this.w_CAPZCON3 = IIF(_Curs_KEY_ARTI.CAPZCON3=0, 1, _Curs_KEY_ARTI.CAPZCON3)
                this.w_AGGIOK = this.w_AGGIOK + 1
                * --- Write into KEY_ARTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CATIPCO3 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COLFIN),'KEY_ARTI','CATIPCO3');
                  +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CATPCON3),'KEY_ARTI','CATPCON3');
                  +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(this.w_CACOCOL3),'KEY_ARTI','CACOCOL3');
                  +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(this.w_CAPZCON3),'KEY_ARTI','CAPZCON3');
                      +i_ccchkf ;
                  +" where ";
                      +"CACODICE = "+cp_ToStrODBC(_Curs_KEY_ARTI.CACODICE);
                         )
                else
                  update (i_cTable) set;
                      CATIPCO3 = this.oParentObject.w_COLFIN;
                      ,CATPCON3 = this.w_CATPCON3;
                      ,CACOCOL3 = this.w_CACOCOL3;
                      ,CAPZCON3 = this.w_CAPZCON3;
                      &i_ccchkf. ;
                   where;
                      CACODICE = _Curs_KEY_ARTI.CACODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
                select _Curs_KEY_ARTI
                continue
              enddo
              use
            endif
          endif
          * --- Dati P.O.S.
          if g_GPOS="S"
            * --- Reparti
            if this.oParentObject.w_REPSEL="S" AND (this.w_CODREP=this.oParentObject.w_REPINI OR (EMPTY(this.w_CODREP) AND EMPTY(this.oParentObject.w_REPINI)) OR this.oParentObject.w_REPALL="S")
              this.w_CODREP = this.oParentObject.w_REPFIN
              this.w_FLAGG = .T.
            endif
            * --- Test Peso/Collo
            if this.oParentObject.w_PECOSEL="S" AND (this.w_FLPECO=this.oParentObject.w_PECOINI OR this.oParentObject.w_PECOINI="T")
              this.w_FLPECO = this.oParentObject.w_PECOFIN
              this.w_FLAGG = .T.
            endif
          endif
          * --- Flag contributi accessori
          if this.w_TIPCOD $ "PF-SE-MP-PH-MC-MA-IM-FS" AND this.oParentObject.w_FLGCAC <> "X" AND ( this.oParentObject.w_FLGCAC <> this.w_OLDFLGCAC OR this.oParentObject.w_FLGCAC = "S" )
            this.w_NEWFLGCAC = this.oParentObject.w_FLGCAC
            this.w_FLAGG = .T.
            if !EMPTY( this.oParentObject.w_cMCTIPCAT ) AND !EMPTY(this.oParentObject.w_cMCCODCAT)
              if this.oParentObject.w_FLGCAC = "S"
                * --- Aggiorna il dettaglio contributi accessori
                * --- Try
                local bErr_0497D560
                bErr_0497D560=bTrsErr
                this.Try_0497D560()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                  * --- Read from CONTRART
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CONTRART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTRART_idx,2],.t.,this.CONTRART_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "MCCODCAT"+;
                      " from "+i_cTable+" CONTRART where ";
                          +"MCCODART = "+cp_ToStrODBC(this.w_CODART);
                          +" and MCTIPCAT = "+cp_ToStrODBC(this.oParentObject.w_cMCTIPCAT);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      MCCODCAT;
                      from (i_cTable) where;
                          MCCODART = this.w_CODART;
                          and MCTIPCAT = this.oParentObject.w_cMCTIPCAT;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    w_MCCODCAT = NVL(cp_ToDate(_read_.MCCODCAT),cp_NullValue(_read_.MCCODCAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Write into CONTRART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CONTRART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTRART_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTRART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MCCODCAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_cMCCODCAT),'CONTRART','MCCODCAT');
                        +i_ccchkf ;
                    +" where ";
                        +"MCCODART = "+cp_ToStrODBC(this.w_CODART);
                        +" and MCTIPCAT = "+cp_ToStrODBC(this.oParentObject.w_cMCTIPCAT);
                           )
                  else
                    update (i_cTable) set;
                        MCCODCAT = this.oParentObject.w_cMCCODCAT;
                        &i_ccchkf. ;
                     where;
                        MCCODART = this.w_CODART;
                        and MCTIPCAT = this.oParentObject.w_cMCTIPCAT;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                bTrsErr=bTrsErr or bErr_0497D560
                * --- End
              endif
            endif
          else
            this.w_NEWFLGCAC = this.w_OLDFLGCAC
          endif
          if this.oParentObject.w_sQTAMIN="S" AND this.w_ARTIPART<>"FO" AND this.oParentObject.w_cMINVEN>=0
            * --- Qt� minima vendibile
            this.w_ARMINVEN = this.oParentObject.w_cMINVEN
            this.w_FLAGG = .T.
          endif
          * --- Se almeno un dato � cambiato scrivo sull'articolo..
          if this.w_FLAGG
            * --- Write into ART_ICOL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'ART_ICOL','ARCODIVA');
              +",ARGRUMER ="+cp_NullLink(cp_ToStrODBC(this.w_GRUMER),'ART_ICOL','ARGRUMER');
              +",ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_CATCON),'ART_ICOL','ARCATCON');
              +",ARCODFAM ="+cp_NullLink(cp_ToStrODBC(this.w_CODFAM),'ART_ICOL','ARCODFAM');
              +",ARCATOMO ="+cp_NullLink(cp_ToStrODBC(this.w_CATOMO),'ART_ICOL','ARCATOMO');
              +",ARCODRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CODRIC),'ART_ICOL','ARCODRIC');
              +",ARCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_CATSCM),'ART_ICOL','ARCATSCM');
              +",ARCODMAR ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAR),'ART_ICOL','ARCODMAR');
              +",ARGRUPRO ="+cp_NullLink(cp_ToStrODBC(this.w_GRUPRO),'ART_ICOL','ARGRUPRO');
              +",ARVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_VOCCEN),'ART_ICOL','ARVOCCEN');
              +",ARVOCRIC ="+cp_NullLink(cp_ToStrODBC(this.w_VOCRIC),'ART_ICOL','ARVOCRIC');
              +",ARTIPGES ="+cp_NullLink(cp_ToStrODBC(this.w_TIPGES),'ART_ICOL','ARTIPGES');
              +",ARPROPRE ="+cp_NullLink(cp_ToStrODBC(this.w_PROPRE),'ART_ICOL','ARPROPRE');
              +",ARTIPCO1 ="+cp_NullLink(cp_ToStrODBC(this.w_COLLO1),'ART_ICOL','ARTIPCO1');
              +",ARTIPCO2 ="+cp_NullLink(cp_ToStrODBC(this.w_COLLO2),'ART_ICOL','ARTIPCO2');
              +",ARTPCONF ="+cp_NullLink(cp_ToStrODBC(this.w_CONFE1),'ART_ICOL','ARTPCONF');
              +",ARTPCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_CONFE2),'ART_ICOL','ARTPCON2');
              +",ARCOCOL1 ="+cp_NullLink(cp_ToStrODBC(this.w_COCOL1),'ART_ICOL','ARCOCOL1');
              +",ARCOCOL2 ="+cp_NullLink(cp_ToStrODBC(this.w_COCOL2),'ART_ICOL','ARCOCOL2');
              +",ARPZCONF ="+cp_NullLink(cp_ToStrODBC(this.w_PZCON1),'ART_ICOL','ARPZCONF');
              +",ARPZCON2 ="+cp_NullLink(cp_ToStrODBC(this.w_PZCON2),'ART_ICOL','ARPZCON2');
              +",ARCODREP ="+cp_NullLink(cp_ToStrODBC(this.w_CODREP),'ART_ICOL','ARCODREP');
              +",ARFLPECO ="+cp_NullLink(cp_ToStrODBC(this.w_FLPECO),'ART_ICOL','ARFLPECO');
              +",ARARTPOS ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPOS),'ART_ICOL','ARARTPOS');
              +",ARPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_PUBWEB),'ART_ICOL','ARPUBWEB');
              +",ARTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'ART_ICOL','ARTIPOPE');
              +",ARFLAPCA ="+cp_NullLink(cp_ToStrODBC(this.w_NEWFLGCAC),'ART_ICOL','ARFLAPCA');
              +",ARTIPART ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
              +",ARFLSPAN ="+cp_NullLink(cp_ToStrODBC(this.w_SACFLG),'ART_ICOL','ARFLSPAN');
              +",ARUTISER ="+cp_NullLink(cp_ToStrODBC(this.w_UTISER),'ART_ICOL','ARUTISER');
              +",ARMINVEN ="+cp_NullLink(cp_ToStrODBC(this.w_ARMINVEN),'ART_ICOL','ARMINVEN');
              +",ARCODTIP ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODTIP),'ART_ICOL','ARCODTIP');
              +",ARCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODVAL),'ART_ICOL','ARCODVAL');
              +",ARCODCLF ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLF),'ART_ICOL','ARCODCLF');
              +",ARFLGESC ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLGESC),'ART_ICOL','ARFLGESC');
                  +i_ccchkf ;
              +" where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  ARCODIVA = this.w_CODIVA;
                  ,ARGRUMER = this.w_GRUMER;
                  ,ARCATCON = this.w_CATCON;
                  ,ARCODFAM = this.w_CODFAM;
                  ,ARCATOMO = this.w_CATOMO;
                  ,ARCODRIC = this.w_CODRIC;
                  ,ARCATSCM = this.w_CATSCM;
                  ,ARCODMAR = this.w_CODMAR;
                  ,ARGRUPRO = this.w_GRUPRO;
                  ,ARVOCCEN = this.w_VOCCEN;
                  ,ARVOCRIC = this.w_VOCRIC;
                  ,ARTIPGES = this.w_TIPGES;
                  ,ARPROPRE = this.w_PROPRE;
                  ,ARTIPCO1 = this.w_COLLO1;
                  ,ARTIPCO2 = this.w_COLLO2;
                  ,ARTPCONF = this.w_CONFE1;
                  ,ARTPCON2 = this.w_CONFE2;
                  ,ARCOCOL1 = this.w_COCOL1;
                  ,ARCOCOL2 = this.w_COCOL2;
                  ,ARPZCONF = this.w_PZCON1;
                  ,ARPZCON2 = this.w_PZCON2;
                  ,ARCODREP = this.w_CODREP;
                  ,ARFLPECO = this.w_FLPECO;
                  ,ARARTPOS = this.w_ARTPOS;
                  ,ARPUBWEB = this.w_PUBWEB;
                  ,ARTIPOPE = this.w_TIPOPE;
                  ,ARFLAPCA = this.w_NEWFLGCAC;
                  ,ARTIPART = this.w_ARTIPART;
                  ,ARFLSPAN = this.w_SACFLG;
                  ,ARUTISER = this.w_UTISER;
                  ,ARMINVEN = this.w_ARMINVEN;
                  ,ARCODTIP = this.w_ARCODTIP;
                  ,ARCODVAL = this.w_ARCODVAL;
                  ,ARCODCLF = this.w_ARCODCLF;
                  ,ARFLGESC = this.w_ARFLGESC;
                  &i_ccchkf. ;
               where;
                  ARCODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Ultimo Costo Standard
          this.w_TESCOS = .T.
          if this.oParentObject.w_COSSEL="S"
            if this.oParentObject.w_COSALL="Z" 
              * --- Non rivalorizza articoli con costo Standard diverso da zero
              this.w_TESCOS = this.w_COSTO=0
            endif
            if this.w_TESCOS
              if g_EACD="S"
                * --- Se attivo Modulo Magazzino Produzione
                do case
                  case this.oParentObject.w_DISTIN="D"
                    * --- Non rivalorizza articoli senza Distinta Base
                    this.w_TESCOS = not EMPTY(this.w_CODDIS)
                  case this.oParentObject.w_DISTIN="S"
                    * --- Non rivalorizza articoli Con Distinta Base
                    this.w_TESCOS = EMPTY(this.w_CODDIS)
                endcase
              endif
              if this.w_TESCOS
                * --- Se non specifico ultimo costo standard...
                if this.oParentObject.w_AGGIORN<>"US"
                  if this.oParentObject.w_AGGIORN="LI"
                    * --- Costo standard da listini..
                     
 Select Rivalo 
 Go Top 
 Locate For Rivalo.Articolo = this.w_CODART 
                    if FOUND()
                      this.w_ROWNUM = RIVALO.LIROWNUM
                      if this.w_ROWNUM<>0
                        this.w_QUANTI = IIF(RIVALO.LIQUANTI=999999,0,RIVALO.LIQUANTI)
                        * --- Read from LIS_SCAG
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO"+;
                            " from "+i_cTable+" LIS_SCAG where ";
                                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                                +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                                +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO;
                            from (i_cTable) where;
                                LICODART = this.w_CODART;
                                and LIROWNUM = this.w_ROWNUM;
                                and LIQUANTI = this.w_QUANTI;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_SC1 = NVL(cp_ToDate(_read_.LISCONT1),cp_NullValue(_read_.LISCONT1))
                          this.w_SC2 = NVL(cp_ToDate(_read_.LISCONT2),cp_NullValue(_read_.LISCONT2))
                          this.w_SC3 = NVL(cp_ToDate(_read_.LISCONT3),cp_NullValue(_read_.LISCONT3))
                          this.w_SC4 = NVL(cp_ToDate(_read_.LISCONT4),cp_NullValue(_read_.LISCONT4))
                          this.w_COSTO = NVL(cp_ToDate(_read_.LIPREZZO),cp_NullValue(_read_.LIPREZZO))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        this.w_COSTO = cp_ROUND(this.w_COSTO * (1+this.w_SC1/100)*(1+this.w_SC2/100)*(1+this.w_SC3/100)*(1+this.w_SC4/100),5)
                      endif
                    else
                      * --- Se non trovato non eseguo calcolo percentuale e non aggiorno contatore
                      this.w_TESCOS = .F.
                    endif
                  else
                    * --- Leggo dal dettaglio di riferimento
                    *     E' una READ, utilizzo una Select per comodit� di scrittura...
                    * --- Select from INVEDETT
                    i_nConn=i_TableProp[this.INVEDETT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" INVEDETT ";
                          +" where DINUMINV="+cp_ToStrODBC(this.oParentObject.w_NUMINV)+" And DICODESE="+cp_ToStrODBC(this.oParentObject.w_ESERCI)+" And DICODICE="+cp_ToStrODBC(this.w_CODART)+"";
                           ,"_Curs_INVEDETT")
                    else
                      select * from (i_cTable);
                       where DINUMINV=this.oParentObject.w_NUMINV And DICODESE=this.oParentObject.w_ESERCI And DICODICE=this.w_CODART;
                        into cursor _Curs_INVEDETT
                    endif
                    if used('_Curs_INVEDETT')
                      select _Curs_INVEDETT
                      locate for 1=1
                      do while not(eof())
                      * --- Se non trovato non eseguo calcolo percentuale e non aggiorno contatore
                      this.w_TESCOS = .T.
                      do case
                        case this.oParentObject.w_AGGIORN="CA"
                          * --- Costo medio ponderato annuo
                          this.w_COSTO = _Curs_INVEDETT.DICOSMPA
                        case this.oParentObject.w_AGGIORN="CP"
                          * --- Costo medio ponderato periodico
                          this.w_COSTO = _Curs_INVEDETT.DICOSMPP
                        case this.oParentObject.w_AGGIORN="CU"
                          * --- Costo ultimo
                          this.w_COSTO = _Curs_INVEDETT.DICOSULT
                        case this.oParentObject.w_AGGIORN="CS"
                          * --- Costo standard
                          this.w_COSTO = _Curs_INVEDETT.DICOSSTA
                        case this.oParentObject.w_AGGIORN="LC"
                          * --- Lifo Continuo
                          this.w_COSTO = _Curs_INVEDETT.DICOSLCO
                        case this.oParentObject.w_AGGIORN="FC"
                          * --- Fifo continuo
                          this.w_COSTO = _Curs_INVEDETT.DICOSFCO
                        case this.oParentObject.w_AGGIORN="LS"
                          * --- Lifo scatti
                          this.w_COSTO = _Curs_INVEDETT.DICOSLSC
                        case this.oParentObject.w_AGGIORN="PA"
                          * --- Prezzo medio ponderato annuo
                          this.w_COSTO = _Curs_INVEDETT.DIPRZMPA
                        case this.oParentObject.w_AGGIORN="PP"
                          * --- Prezzo medio ponderato periodico
                          this.w_COSTO = _Curs_INVEDETT.DIPRZMPP
                        case this.oParentObject.w_AGGIORN="UP"
                          * --- Ultimo Prezzo
                          this.w_COSTO = _Curs_INVEDETT.DIPRZULT
                      endcase
                        select _Curs_INVEDETT
                        continue
                      enddo
                      use
                    endif
                  endif
                endif
                * --- Aggiorna Costo in finzione della percentuale se indicata
                if this.oParentObject.w_RICPE<>0 AND ( this.w_TESCOS OR this.oParentObject.w_AGGIORN="US")
                  this.w_COSTO = cp_ROUND( this.w_COSTO * ( 1+this.oParentObject.w_RICPE/100 ) , g_PERPUL )
                endif
                * --- Eseguo Conversione
                if this.w_CODVAL<> g_PERVAL AND this.oParentObject.w_AGGIORN<>"US" AND this.w_TESCOS
                  this.w_COSTO = cp_Round( (this.w_COSTO/g_CAOVAL)/this.w_CAOGEN , g_PERPUL )
                endif
              endif
            endif
          endif
          * --- Fornitori Abituali
          if EMPTY ( NVL(_Curs_QUERY_GSMA_QVA.PRCODART," ") )
            * --- Insert into PAR_RIOR
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PRCODART"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODART),'PAR_RIOR','PRCODART');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_CODART)
              insert into (i_cTable) (PRCODART &i_ccchkf. );
                 values (;
                   this.w_CODART;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          if this.oParentObject.w_FORSEL="S" AND (this.w_CODFOR=this.oParentObject.w_FORINI OR (EMPTY(this.w_CODFOR) AND EMPTY(this.oParentObject.w_FORINI)) OR this.oParentObject.w_FORALL="S")
            this.w_FLAGG = .T.
            * --- Write into PAR_RIOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRCODFOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FORFIN),'PAR_RIOR','PRCODFOR');
              +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  PRCODFOR = this.oParentObject.w_FORFIN;
                  ,PRTIPCON = "F";
                  &i_ccchkf. ;
               where;
                  PRCODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_AGGIO_FOR = this.w_AGGIO_FOR + 1
          endif
          if this.oParentObject.w_COSSEL="S" AND this.w_COSOLD<>this.w_COSTO AND (this.w_TESCOS OR this.oParentObject.w_AGGIORN="US")
            this.w_FLAGG = .T.
            * --- Write into PAR_RIOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_COSTO),'PAR_RIOR','PRCOSSTA');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  PRCOSSTA = this.w_COSTO;
                  &i_ccchkf. ;
               where;
                  PRCODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_AGGIO_COSTO = this.w_AGGIO_COSTO + 1
          endif
        else
          if this.w_FLAGG
            * --- Write into ART_ICOL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARARTPOS ="+cp_NullLink(cp_ToStrODBC(this.w_ARTPOS),'ART_ICOL','ARARTPOS');
              +",ARPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_PUBWEB),'ART_ICOL','ARPUBWEB');
                  +i_ccchkf ;
              +" where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  ARARTPOS = this.w_ARTPOS;
                  ,ARPUBWEB = this.w_PUBWEB;
                  &i_ccchkf. ;
               where;
                  ARCODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if this.w_FLAGG
          * --- Contatore Articoli Modificati
          this.w_AGGIO = this.w_AGGIO + 1
          * --- Se rendo l'articolo non pubblicabile, rendo non pubblicabili anche i codici di ricerca
          *     ad esso riferenti.
          if this.w_PUBWEB = "N"
            * --- Write into KEY_ARTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CAPUBWEB ="+cp_NullLink(cp_ToStrODBC("N"),'KEY_ARTI','CAPUBWEB');
                  +i_ccchkf ;
              +" where ";
                  +"CACODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  CAPUBWEB = "N";
                  &i_ccchkf. ;
               where;
                  CACODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.oParentObject.w_TIPOSEL="S" OR this.oParentObject.w_VALOSEL="S" OR this.oParentObject.w_CLASSEL="S" OR this.oParentObject.w_GESCSEL="S"
            * --- Write into KEY_ARTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CACODTIP ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODTIP),'KEY_ARTI','CACODTIP');
              +",CACODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODVAL),'KEY_ARTI','CACODVAL');
              +",CACODCLF ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODCLF),'KEY_ARTI','CACODCLF');
              +",CAFLGESC ="+cp_NullLink(cp_ToStrODBC(this.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
                  +i_ccchkf ;
              +" where ";
                  +"CACODART = "+cp_ToStrODBC(this.w_CODART);
                     )
            else
              update (i_cTable) set;
                  CACODTIP = this.w_ARCODTIP;
                  ,CACODVAL = this.w_ARCODVAL;
                  ,CACODCLF = this.w_ARCODCLF;
                  ,CAFLGESC = this.w_ARFLGESC;
                  &i_ccchkf. ;
               where;
                  CACODART = this.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
        select _Curs_QUERY_GSMA_QVA
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0497D560()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTRART
    i_nConn=i_TableProp[this.CONTRART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTRART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTRART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCCODART"+",MCTIPCAT"+",MCCODCAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODART),'CONTRART','MCCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_cMCTIPCAT),'CONTRART','MCTIPCAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_cMCCODCAT),'CONTRART','MCCODCAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCCODART',this.w_CODART,'MCTIPCAT',this.oParentObject.w_cMCTIPCAT,'MCCODCAT',this.oParentObject.w_cMCCODCAT)
      insert into (i_cTable) (MCCODART,MCTIPCAT,MCCODCAT &i_ccchkf. );
         values (;
           this.w_CODART;
           ,this.oParentObject.w_cMCTIPCAT;
           ,this.oParentObject.w_cMCCODCAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='PAR_RIOR'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='CON_COLL'
    this.cWorkTables[5]='LIS_SCAG'
    this.cWorkTables[6]='INVEDETT'
    this.cWorkTables[7]='CONTRART'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_QUERY_GSMA_QVA')
      use in _Curs_QUERY_GSMA_QVA
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    if used('_Curs_INVEDETT')
      use in _Curs_INVEDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
