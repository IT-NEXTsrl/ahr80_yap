* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bvf                                                        *
*              Batch versamenti FIRR                                           *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_52]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-01-14                                                      *
* Last revis.: 2008-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bvf",oParentObject)
return(i_retval)

define class tgsve_bvf as StdBatch
  * --- Local variables
  w_IMPLIQU = 0
  w_CODAZI = space(5)
  w_FLAGMOPL = space(1)
  w_CAMBIO = 0
  w_IMPMONO1 = 0
  w_IMPMONO2 = 0
  w_IMPLURI1 = 0
  w_IMPLURI2 = 0
  w_PERMONO1 = 0
  w_PERMONO2 = 0
  w_PERMONO3 = 0
  w_IMPMONO3 = 0
  w_PERPLUR1 = 0
  w_PERPLUR2 = 0
  w_PERPLUR3 = 0
  w_IMPLURI3 = 0
  w_IMPOVALU = 0
  w_CAOVAL = 0
  w_APPO2 = space(3)
  w_INIENA = ctod("  /  /  ")
  w_FINENA = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_MESE = 0
  w_INIT = .f.
  * --- WorkFile variables
  PAR_PROV_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gli agenti dalle provvigioni liquidate (da GSVE_MVF)
    if  NOT this.oParentObject.cFunction = "Load"
      ah_ErrorMsg("FUNZIONE DISPONIBILE SOLO IN CARICAMENTO VERSAMENTO FIRR",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Verifico la valuta di stampa
    this.oParentObject.w_VALUTA = IIF(this.oParentObject.w_VFCODVAL=g_PERVAL,g_PERVAL,g_codlir)
    * --- Effettuo una Read sulla tabella Valute per avere i decimali.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODAZI = i_CODAZI
    * --- Legge i vari importi dalle provvigioni liquidate in base alle selezioni
    vq_exec("query\GSVE_SFR.VQR",this,"VER1FIR")
    * --- Converto tutto alla valuta specificata nei versamenti FIRR
    * --- La valuta specificata nei versamenti FIRR � Lire oppure Euro.
    Select Ver1fir
    Go Top
    =WrCursor("Ver1fir")
    Scan for Ver1fir.Plcodval<>this.oParentObject.w_Vfcodval
    this.w_IMPOVALU = Ver1fir.Pltotpro
    this.w_APPO2 = Ver1fir.Plcodval
    this.w_CAOVAL = GETCAM(this.w_APPO2, I_DATSYS)
    if Ver1fir.Plcodval<>g_PERVAL
      if NOT EMPTY(this.w_APPO2)
        Select Ver1fir
        * --- Converte in EURO
        this.w_IMPOVALU = VAL2MON(this.w_IMPOVALU, this.w_CAOVAL,1, I_DATSYS, this.w_APPO2, GETVALUT(this.w_APPO2, "VADECTOT"))
      endif
    endif
    Replace PLTOTPRO with this.w_IMPOVALU
    Endscan
    Select Plcodage,Min(Agdesage) as Agdesage,Sum(Pltotpro) as Pltotpro,Min(Agflescl) as Agflescl , Min(Aginiena) as Aginiena, Min(Agfinena) as Agfinena;
     From Ver1fir group by Plcodage order by Plcodage into cursor Verfir
    this.w_INIT = .T.
    * --- Cicla sul Risultato della Query
    SELECT VERFIR
    GO TOP
    this.oParentObject.w_TOTFIRR = 0
    this.oParentObject.w_IMPFIRR = 0
    SCAN
    * --- Carica Detail Versamenti FIRR
    SELECT (this.oParentObject.cTrsName)
    if this.w_INIT=.T.
      * --- Azzera il Transitorio
      if reccount()<>0
        ZAP
      endif
      this.w_INIT = .F.
    endif
    * --- Nuova Riga del Temporaneo
    this.oParentObject.InitRow()
    SELECT VERFIR
    this.oParentObject.w_VFCODAGE = PLCODAGE
    this.oParentObject.w_VFIMPLIQ = cp_ROUND(PLTOTPRO, this.oParentObject.w_DECTOT)
    this.oParentObject.w_DESAGE = AGDESAGE
    this.oParentObject.w_VFFLORMI = 1
    this.w_IMPLIQU = this.oParentObject.w_VFIMPLIQ
    this.w_FLAGMOPL = AGFLESCL
    this.w_DATINI = cp_CharToDate("01-01-"+alltrim(this.oParentObject.w_VF__ANNO))
    this.w_DATFIN = cp_CharToDate("31-12-"+alltrim(this.oParentObject.w_VF__ANNO))
    this.w_INIENA = IIF(EMPTY(CP_TODATE(AGINIENA)) OR CP_TODATE(AGINIENA)<this.w_DATINI, this.w_DATINI, CP_TODATE(AGINIENA))
    this.w_FINENA = IIF(EMPTY(CP_TODATE(AGFINENA)) OR CP_TODATE(AGFINENA)>this.w_DATFIN, this.w_DATFIN, CP_TODATE(AGFINENA))
    this.w_MESE = (MONTH(this.w_FINENA)-MONTH(this.w_INIENA))+1
    if (this.w_FLAGMOPL)="M"
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPIMPIN1,PPIMPIN2,PPPERFI1,PPPERFI2,PPPERFI3"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPIMPIN1,PPIMPIN2,PPPERFI1,PPPERFI2,PPPERFI3;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPMONO1 = NVL(cp_ToDate(_read_.PPIMPIN1),cp_NullValue(_read_.PPIMPIN1))
        this.w_IMPMONO2 = NVL(cp_ToDate(_read_.PPIMPIN2),cp_NullValue(_read_.PPIMPIN2))
        this.w_PERMONO1 = NVL(cp_ToDate(_read_.PPPERFI1),cp_NullValue(_read_.PPPERFI1))
        this.w_PERMONO2 = NVL(cp_ToDate(_read_.PPPERFI2),cp_NullValue(_read_.PPPERFI2))
        this.w_PERMONO3 = NVL(cp_ToDate(_read_.PPPERFI3),cp_NullValue(_read_.PPPERFI3))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IMPMONO1 = VALCAM(this.w_IMPMONO1,g_PERVAL,this.oParentObject.w_VFCODVAL,this.oParentObject.w_VFDATREG)*this.w_MESE/12
      this.w_IMPMONO2 = VALCAM(this.w_IMPMONO2,g_PERVAL,this.oParentObject.w_VFCODVAL,this.oParentObject.w_VFDATREG)*this.w_MESE/12
      if this.w_IMPLIQU>this.w_IMPMONO1
        if this.w_IMPLIQU>(this.w_IMPMONO2)
          this.w_IMPMONO3 = this.w_IMPLIQU-this.w_IMPMONO2
          this.w_IMPMONO2 = (this.w_IMPMONO2-this.w_IMPMONO1)
        else
          this.w_IMPMONO2 = (this.w_IMPLIQU-this.w_IMPMONO1)
          this.w_IMPMONO3 = 0
        endif
      else
        this.w_IMPMONO1 = this.w_IMPLIQU
        this.w_IMPMONO2 = 0
        this.w_IMPMONO3 = 0
      endif
      this.oParentObject.w_VFIMPFI1 = cp_ROUND (this.w_IMPMONO1,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFIMPFI2 = cp_ROUND (this.w_IMPMONO2,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFIMPFI3 = cp_ROUND (this.w_IMPMONO3,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFPERFI1 = this.w_PERMONO1
      this.oParentObject.w_VFPERFI2 = this.w_PERMONO2
      this.oParentObject.w_VFPERFI3 = this.w_PERMONO3
      this.oParentObject.w_VFCONFI1 = cp_ROUND (this.w_IMPMONO1*this.w_PERMONO1/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI1 = this.oParentObject.w_VFCONFI1
      this.oParentObject.w_VFCONFI2 = cp_ROUND (this.w_IMPMONO2*this.w_PERMONO2/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI2 = this.oParentObject.w_VFCONFI2
      this.oParentObject.w_VFCONFI3 = cp_ROUND (this.w_IMPMONO3*this.w_PERMONO3/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI3 = this.oParentObject.w_VFCONFI3
      this.oParentObject.w_IMPFIRR = cp_ROUND (this.oParentObject.w_VFCONFI1+this.oParentObject.w_VFCONFI2+this.oParentObject.w_VFCONFI3,this.oParentObject.w_DECTOT)
      this.oParentObject.w_TOTFIRR = cp_ROUND(this.oParentObject.w_TOTFIRR+this.oParentObject.w_IMPFIRR,this.oParentObject.w_DECTOT)
      this.oParentObject.w_AGINIENA = CP_TODATE(AGINIENA)
      this.oParentObject.w_AGFINENA = CP_TODATE(AGFINENA)
    else
      this.w_IMPLIQU = PLTOTPRO
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPIMPPL1,PPIMPPL2,PPPERPL1,PPPERPL2,PPPERPL3"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPIMPPL1,PPIMPPL2,PPPERPL1,PPPERPL2,PPPERPL3;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPLURI1 = NVL(cp_ToDate(_read_.PPIMPPL1),cp_NullValue(_read_.PPIMPPL1))
        this.w_IMPLURI2 = NVL(cp_ToDate(_read_.PPIMPPL2),cp_NullValue(_read_.PPIMPPL2))
        this.w_PERPLUR1 = NVL(cp_ToDate(_read_.PPPERPL1),cp_NullValue(_read_.PPPERPL1))
        this.w_PERPLUR2 = NVL(cp_ToDate(_read_.PPPERPL2),cp_NullValue(_read_.PPPERPL2))
        this.w_PERPLUR3 = NVL(cp_ToDate(_read_.PPPERPL3),cp_NullValue(_read_.PPPERPL3))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IMPLURI1 = VALCAM(this.w_IMPLURI1,g_PERVAL,this.oParentObject.w_VFCODVAL,this.oParentObject.w_VFDATREG)*this.w_MESE/12
      this.w_IMPLURI2 = VALCAM(this.w_IMPLURI2,g_PERVAL,this.oParentObject.w_VFCODVAL,this.oParentObject.w_VFDATREG)*this.w_MESE/12
      if this.w_IMPLIQU>this.w_IMPLURI1
        if this.w_IMPLIQU>this.w_IMPLURI2
          this.w_IMPLURI3 = this.w_IMPLIQU-this.w_IMPLURI2
          this.w_IMPLURI2 = (this.w_IMPLURI2-this.w_IMPLURI1)
        else
          this.w_IMPLURI2 = (this.w_IMPLIQU-this.w_IMPLURI1)
          this.w_IMPLURI3 = 0
        endif
      else
        this.w_IMPLURI1 = this.w_IMPLIQU
        this.w_IMPLURI2 = 0
        this.w_IMPLURI3 = 0
      endif
      this.oParentObject.w_VFIMPFI1 = cp_ROUND (this.w_IMPLURI1,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFIMPFI2 = cp_ROUND (this.w_IMPLURI2,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFIMPFI3 = cp_ROUND (this.w_IMPLURI3,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFPERFI1 = this.w_PERPLUR1
      this.oParentObject.w_VFPERFI2 = this.w_PERPLUR2
      this.oParentObject.w_VFPERFI3 = this.w_PERPLUR3
      this.oParentObject.w_VFCONFI1 = cp_ROUND (this.w_IMPLURI1*this.w_PERPLUR1/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI1 = this.oParentObject.w_VFCONFI1
      this.oParentObject.w_VFCONFI2 = cp_ROUND (this.w_IMPLURI2*this.w_PERPLUR2/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI2 = this.oParentObject.w_VFCONFI2
      this.oParentObject.w_VFCONFI3 = cp_ROUND (this.w_IMPLURI3*this.w_PERPLUR3/100,this.oParentObject.w_DECTOT)
      this.oParentObject.w_VFCONFI3 = this.oParentObject.w_VFCONFI3
      this.oParentObject.w_IMPFIRR = cp_ROUND (this.oParentObject.w_VFCONFI1+this.oParentObject.w_VFCONFI2+this.oParentObject.w_VFCONFI3,this.oParentObject.w_DECTOT)
      this.oParentObject.w_TOTFIRR = cp_ROUND(this.oParentObject.w_TOTFIRR+this.oParentObject.w_IMPFIRR,this.oParentObject.w_DECTOT)
      this.oParentObject.w_AGINIENA = CP_TODATE(AGINIENA)
      this.oParentObject.w_AGFINENA = CP_TODATE(AGFINENA)
    endif
    this.w_IMPLURI1 = 0
    this.w_IMPLURI2 = 0
    this.w_IMPMONO2 = 0
    this.w_IMPLIQU = 0
    this.w_IMPLURI3 = 0
    this.w_IMPMONO1 = 0
    this.w_IMPMONO3 = 0
    * --- Carica il Temporaneo Versamenti FIRR dei Dati
    this.oParentObject.TrsFromWork()
    SELECT VERFIR
    * --- Skippa al record successivo
    ENDSCAN
    if this.w_INIT
      SELECT (this.oParentObject.cTrsName)
      * --- Azzera il Transitorio - Nel caso la precedente richiesta lo abbia riempito (anche se l'utente lo ha modificato)
      if reccount()<>0
        ZAP
      endif
      GO TOP
      ah_ErrorMsg("Non ci sono dati da elaborare",,"")
    else
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      With this.oParentObject
      .WorkFromTrs()
      .SaveDependsOn()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=1
      .oPgFrm.Page1.oPag.oBody.nRelRow=1
      EndWith
    endif
    if used("VERFIR")
      select Verfir
      use
    endif
    if used("VER1FIR")
      select Ver1fir
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_PROV'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
