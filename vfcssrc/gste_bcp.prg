* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bcp                                                        *
*              Elaborazione partite aperte                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_204]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-03                                                      *
* Last revis.: 2014-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC,pCURS,pTIPO,pFLBANC,pTipDis,pIndex
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bcp",oParentObject,m.pEXEC,m.pCURS,m.pTIPO,m.pFLBANC,m.pTipDis,m.pIndex)
return(i_retval)

define class tgste_bcp as StdBatch
  * --- Local variables
  pEXEC = space(1)
  pCURS = space(10)
  pTIPO = space(1)
  pFLBANC = space(1)
  pTipDis = space(1)
  pIndex = .f.
  w_KTIPO = space(1)
  w_KPUNT = 0
  w_KPART = space(10)
  w_KSALS = 0
  w_KSALC = 0
  w_SEARCHTYPE = space(1)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_NUMCONTIND = 0
  w_NOKTIPO = space(1)
  w_LSEGNO = space(1)
  w_TmpN = 0
  w_WHO_DEL = 0
  w_SEGNO = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato Nelle Seguenti Gestioni :
    *     1) Accorpamento Scadenze Routine GSTE_BUS
    *     2) Abbina Partite nelle Distinte Routine  GSTE_BAI
    *     3) Manutenzione Partite Routine GSTE_BIS
    *     4) Contabilizzazione Indiretta Effetti Routine GSCG_BI3
    *     5) Stampa Estratto Conto Cliente/Fornitore Routine GSTE_BEC
    *     6) Compilazione Distinte Routine GSTE_BD1
    *     7) Chiusura da Primanota GSCG_BSA
    *     8) Stampa Partite/Scadenze Routine GSTE_BPS
    *     9) Chiusura Partite da Scadenze Diverse Routine GSTE_BP1
    *     10) Man. Contenzioso caso Mancati Pagamenti  Routine GSSO_BI1
    *     11) Piano Contenzioso caso Mancati Pagamenti  Routine GSSO_BPI
    *     12) Stampa Analisi Scaduto con interessi di Mora GSTE_BIM
    *     13) Stampa Brogliaccio Effetti GSTE_BBE
    *     14) Saldaconto sempre attraverso GSCG_BSA
    *     15) Stampa Cash Flow GSTE_BFL
    * --- Parametri
    *     pExec - Tipo di esecuzione
    *     A - GSTE_BUS, GSTE_BIS, GSCG_BI3,GSTE_BEC,GSCG_BSA,GSTE_BPS,GSTE_BIM
    *     B - GSTE_BBE
    *     C - GSTE_BP1
    *     D - GSTE_BAI, GSTE_BD1
    *     S - GSSO_BI1,GSSO_BPI
    *     pCurs - Cursore di partenza contenente tutte le partite da esaminare
    * --- Il cursore passato al batch  deve contenere i campi :
    *     Tipcon: Tipologia Cliente \Fornitore
    *     Datsca: Data Scadenza
    *     Numpar: Numero Partita
    *     Codcon: Codice Cliente\Fornitore
    *     Codval: Codice Valuta
    *     Flindi: Flag Cont. Indiretta Effetti
    *     Totimp: Importo della Partita
    *     Ptserrif: Seriale Partita di Origine
    *     Ptordrif: Provenienza Partita di Origine
    *     Ptnumrif:Numero di Riga Partita di Origine
    *     Ptflragg: Flag Partite Raggruppate
    *     Flcrsa:  Flag Tipologia Partita  C\S\A
    *     Tipdoc: Classe documento prima nota (x riconoscere se da contenzioso)
    *     Segno: Segno della Partita D\A
    *     Impave: Importo partita in Dare
    *     Impdar: Importo partita in Avere
    *     Tippag: Tipologia Pagamento
    *     Caoape: Cambio di Apertura
    *     Caoval: Cambio della Partita
    *     Ptserial: Seriale Partita
    *     Ptroword: Provenienza Partita
    *     Cprownum: Numero di riga Partita
    *     Conta: Numero record con soliti riferimenti (DATSCA,NUMPAR,TIPCON,CODCON,CODVAL)
    *                Se esiste un solo record non eseguo nessun calcolo
    *     Anflragg: Se pExec='D' utilizzato per capire se raggruppare o meno gli effetti
    *                all'interno del batch GSTE_BCR
    *     PtCodRag: Se pExec='D' utilizzato per capire se raggruppare o meno gli effetti
    *                all'interno del batch GSTE_BCR
    * --- Nel caso di distinte, devo sapere come raggruppare le scadenze (su quale banca)
    *     Se non distinta non occorre passare questo parametro
    * --- Nel caso di distinte, devo sapere come raggruppare le scadenze (se Distinta di tipo CA=Cambiale / Tratta non devo raggruppare)
    *     Se non distinta non occorre passare questo parametro
    * --- --indica se creare gli indici per il cursore dei dati
    if RECCOUNT(this.pCurs)>0
      * --- Indicizza per velocizzare l esecuzione del batch GSTE_BCP (i campi indicizzati sono quelli usati nelle Locate del GSAR_BCP )
      if this.pIndex
        SELECT (this.pCurs) 
 INDEX ON DTOS(NVL(DATSCA,{}))+LEFT(ALLTRIM(NUMPAR)+ SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL+STR(NVL(FLINDI,0),3,0) TAG idx1 ADDITIVE 
 INDEX ON deleted() TAG idx2 ADDITIVE 
 INDEX ON FLCRSA TAG idx3 ADDITIVE 
 INDEX ON NVL(PTSERIAL,SPACE(10)) TAG idx4 ADDITIVE 
 INDEX ON NVL(PTROWORD,0) TAG idx5 ADDITIVE 
 INDEX ON NVL(CPROWNUM,0) TAG idx6 ADDITIVE 
      endif
      if this.pEXEC="D"
        this.w_KTIPO = this.pTIPO
      else
        * --- Se non in distinta elimino dal temporaneo le partite a saldo 0
        *     Codice emulo della query QUERY\GSTE0KAI.VQR utilizzata
        *     nelle distinte
        ah_Msg("Elimina eventuali partite chiuse...",.T.)
        * --- Elenco delle partite aperte...
         
 Select DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+STR(FLINDI,1,0) AS CHIAVE, Sum(TOTIMP) As TOTALE; 
 From (this.pCurs) Group By CHIAVE ; 
 Having TOTALE<>0 ; 
 Into Cursor NOSALD1 NoFilter Order By CHIAVE
         
 Delete From (this.pCurs) ; 
 Where DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+STR(FLINDI,1,0) ; 
 Not In (Select CHIAVE From NOSALD1)
        Use in NOSALD1
      endif
       
 Select(this.pCurs) 
 Go Top
      * --- Esamino le scadenze con PTSERRIF valorizzato di saldo o acconto
      this.w_SEARCHTYPE = "N"
      ah_Msg("Esamino riferimenti partite di saldo...",.T.)
      Scan For Not Deleted() And Not Empty(NVL(PTSERRIF,Space(10))) And (FLCRSA $ "S-A") And IIF(Type(this.pCurs+".Conta")="N",Conta>1,.t.)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      EndScan
      this.w_SEARCHTYPE = "S"
      * --- Verifico la eventuale presenza di scadenze saldate
      *     per verificare se possibile eliminarle. Es. Raggruppo una scadenza
      *     di incasso con una di saldo, al termine ho due scadenze di segno (PT_SEGNO)
      *     opposto ed importo uguale non eliminate dalle precedenti elaborazioni
      ah_Msg("Esamino partite di saldo a sezioni opposte...",.T.)
      SCAN FOR NOT DELETED() AND Nvl(TOTIMP,0)<>0 And FLCRSA="S" And IIF(Type(this.pCurs+".Conta")="N",Conta>1,.t.)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
      * --- Cicla sulle Scadenze (Passive per i Clienti o Attive per i Fornitori) no raggruppate.
      *     
      *     A seguito di questo cicler� su quelle raggruppate
      ah_Msg("Esamino partite di creazione non raggruppate...",.T.)
      SCAN FOR NOT DELETED() AND Empty(NVL(PTFLRAGG,0)) AND FLCRSA="C" And IIF(Type(this.pCurs+".Conta")="N",Conta>1,.t.)
      * --- Verifico se la scadenza in esame pu� essere un insoluto
      *     se si la esaminer� dopo
      *     (copro caso di insoluto di una scadenza raggruppata)
      * --- Leggo il tipo reg. di prima nota per determinare se � un insoluto (PNTIPDOC=NO)
      *     L'insoluto � l'unica reg. di prima nota senza IVA che genera scadenze di
      *     tipo creazione (FLCRSA='C')
      if Not ( FLCRSA="C" And PTROWORD>0 And TipDoc="NO" )
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      ENDSCAN
      ah_Msg("Esamino partite di creazione raggruppate...",.T.)
      * --- Cicla sulle Scadenze (Passive per i Clienti o Attive per i Fornitori) raggruppate
      SCAN FOR NOT DELETED() AND PTFLRAGG<>0 AND FLCRSA="C" And IIF(Type(this.pCurs+".Conta")="N",Conta>1,.t.)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
      * --- Eseguo un'ultima SCAN per processare eventuali 
      *     insoluti non ancora processati(Problema insoluto di scadenze raggruppate)
      ah_Msg("Esamino partite da insoluto...",.T.)
      SCAN FOR NOT DELETED() And TIPDOC="NO" And FLCRSA="C" And PTROWORD>0 And IIF(Type(this.pCurs+".Conta")="N",Conta>1,.t.)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
      if this.pExec<>"D"
        if this.pExec="B"
          * --- Metto o zero l'importo di eventuali scadenze incongruenti da Brogliaccio Effetti
           
 REPLACE TOTIMP WITH IIF(((NVL(TOTIMP,0)>0 AND NVL(tipcon," ")="C") OR (NVL(TOTIMP,0)<0 AND NVL(tipcon," ")="F")) OR Nvl(FLCRSA," ")="C",; 
 ABS(TOTIMP) ,0),IMPDAR WITH IIF(SEGNO="D",ABS(TOTIMP),0),IMPAVE WITH IIF(SEGNO="A",ABS(TOTIMP),0) FOR NVL(TOTIMP, 0)<>0
        else
          * --- Riassegno Importi
           
 REPLACE IMPDAR WITH IIF(SEGNO="D",ABS(TOTIMP),0),IMPAVE WITH IIF(SEGNO="A",ABS(TOTIMP),0),; 
 TOTIMP WITH ABS(TOTIMP) FOR NVL(TOTIMP, 0)<>0
        endif
      endif
       
 REPLACE CAOAPE WITH CAOVAL FOR NVL(CAOAPE,0)=0
      if this.pExec="D"
        * --- Verifico la presenza di eventuali scadenze da cont. indiretta
        this.w_NUMCONTIND = 0
        * --- Verifico eventuali scadenze in contenzioso
         
 Select (this.pCurs) 
 Count For TipDoc="NO" And FLCRSA="C" And ptroword>0 To this.w_NUMCONTIND
        if this.w_NUMCONTIND<>0
          * --- Verifico se eventuali scadenze derivanti da contabilizzazione indiretta non
          *     siano gia presenti tra le scadenze in distinta.
          *     
          *     Questo per evitare il caso di contabilizzazione indiretta seguita da un 
          *     contenzioso.
          * --- Recupero tutte le partite derivanti da insouto presenti in distinta (PTFLINDI vuoto perch� ptroword>0)
           
 Select DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) As CHIAVE; 
 From (this.pCurs) Where TipDoc="NO" And FLCRSA="C" And ptroword>0; 
 Into Cursor NOSALD2 NoFilter Order By CHIAVE
          * --- Elimino eventuali scadenze da cont. indiretta (ptflindi pieno) riaperte che hanno stessi estermi 
          *     di partite in insoluto.
          *     Scadenze pre 2.2 riferimenti vuoti quindi elimino tutte le partite da cont. indiretta
          *     con stessi riferimenti di partite in contezioso.
          *     Scadenze post 2.2 utilizzo oltre ai riferimenti anche il fatto che entrambe le
          *     scadenze  (insoluto e contabilizzazione indiretta) puntano alla stessa partita
          *     di creazione
           
 Delete From (this.pCurs) Where DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) ; 
 In (Select CHIAVE From NOSALD2) And Not Empty( Flindi ) 
          Use in NOSALD2
        endif
        * --- Lancio il batch per valutare il raggruppamento degli effetti, se trova effetti negativi
        *     li elimina...
        Select (this.pCurs)
        if lower(this.oparentobject.class)="tgscg_bap"
          * --- Cambio di segno i PTTOTIMP negativi (nella query PTTOTIMP � moltiplicato con
          *     il segno)
           
 Replace TOTIMP With ABS(TOTIMP) For NVL(TOTIMP, 0)<>0
        else
          if this.pTipDis="CA"
            * --- Metto o zero l'importo di eventuali scadenze se distinta di tipo Cambiale / Tratta
             
 REPLACE TOTIMP WITH IIF((NVL(TOTIMP,0)>0 AND this.w_KTIPO="C") OR (NVL(TOTIMP,0)<0 AND this.w_KTIPO="F"),; 
 ABS(TOTIMP) ,0) FOR NVL(TOTIMP, 0)<>0
          else
            * --- Per le scadenze non raggruppabili (Attive e Passive) sbianco eventuali importi negativi
             
 REPLACE TOTIMP WITH IIF((NVL(TOTIMP,0)>0 AND this.w_KTIPO="C") OR (NVL(TOTIMP,0)<0 AND this.w_KTIPO="F"),; 
 ABS(TOTIMP) ,0) FOR NVL(TOTIMP, 0)<>0 AND Nvl(ANFLRAGG," ")<>"T"
            * --- Non devo considerare 
            this.w_LSEGNO = IIF(this.w_KTIPO="C","D","A")
            this.w_NOKTIPO = IIF(this.w_KTIPO="C","F","C")
            * --- Azzero importo partite di tipologia incongruente alla distinta (Attive\Passive)
            *      e di segno opposto
             
 REPLACE TOTIMP WITH 0 FOR this.w_NOKTIPO=TIPCON AND Nvl(SEGNO," ") <>this.w_LSEGNO AND Nvl(ANFLRAGG," ")="T"
            * --- Applico il valore assoluto prima dell'esame delle partite, xch� il GSTE_BCR
            *     moltiplica per il Segno il totale partita sulle scadenze raggruppabili
             
 REPLACE TOTIMP WITH Abs (TOTIMP) FOR NVL(TOTIMP, 0)<>0 And Nvl(ANFLRAGG," ")="T"
            GSTE_BCR (this, "D" , this.pCurs , this.pFLBANC , this.pTIPDIS , this.w_KTIPO )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      * --- Le scadenze prive di pagamento sono escluse solo per la compilazione
      *     manutenzione distinte
       
 DELETE FROM (this.pCurs) ; 
 WHERE NVL(TOTIMP, 0)=0 OR NVL(CAOVAL,0)=0 OR ( EMPTY(NVL(TIPPAG," ")) And this.pExec= "D")
      if this.pIndex
        SELECT (this.pCurs) 
 DELETE TAG ALL OF idx1 
 DELETE TAG ALL OF idx2 
 DELETE TAG ALL OF idx3 
 DELETE TAG ALL OF idx4 
 DELETE TAG ALL OF idx5 
 DELETE TAG ALL OF idx6 
      endif
    else
      * --- Non ci sono partite non saldate, se seleziono solo le saldate, non serve fare questo giro...
       
 Select (this.pCurs) 
 Zap
    endif
    Wait Clear
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca Relative Partite di Saldo
    do while .T.
      * --- Ricerca Partite di Saldo Per Riferimento Partita di Origine
       
 Select(this.pCurs) 
 Go Top
      if this.w_SEARCHTYPE="N"
        * --- Caso Partite di saldo con riferimento a partita di creazione
        LOCATE FOR this.w_PTSERRIF=NVL(PTSERIAL,SPACE(10)) AND this.w_PTORDRIF=NVL(PTROWORD,0) AND this.w_PTNUMRIF= NVL(CPROWNUM,0) And NOT DELETED() AND FLCRSA="C"
      else
        LOCATE FOR DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+; 
 SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL+STR(NVL(FLINDI,0),3,0)=this.w_KPART AND FLCRSA<>"C" And Not Deleted() ; 
 And this.w_SEGNO<>SEGNO
      endif
      * --- Se trovata e non � la scadenza di partenza (caso esame saldate)
      if FOUND() And Recno() <> this.w_KPUNT
        this.w_KSALC = NVL(TOTIMP, 0)
        this.w_TmpN = this.w_KSALS + this.w_KSALC
        * --- Tra le due scadenze in gioco devo decidere quale eliminare, elimino
        *     quella con importo pi� piccolo in valore assoluto!
        *     w_KSALC = TOTIMP scadenza trovata
        *     w_KSALS = TOTIMP scadenza origine della ricerca
        this.w_WHO_DEL = Abs(this.w_KSALS) - Abs ( this.w_KSALC ) 
        do case
          case this.w_WHO_DEL>0
             
 Delete 
 Goto this.w_KPUNT
            Replace TOTIMP WITH this.w_TmpN
            this.w_KSALS = this.w_TmpN
          case this.w_WHO_DEL<0
            Replace TOTIMP WITH this.w_TmpN
             
 Goto this.w_KPUNT 
 Delete 
 Exit
          otherwise
             
 Delete 
 Goto this.w_KPUNT 
 Delete 
 Exit
        endcase
      else
        Exit
      endif
    enddo
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Segno della scadenze di creazione
    this.w_SEGNO = SEGNO
    this.w_KPUNT = RECNO()
    this.w_KPART = DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(31),31)+TIPCON+LEFT(ALLTRIM(CODCON)+SPACE(15),15)+CODVAL+STR(NVL(FLINDI,0),3,0)
    this.w_KSALS = NVL(TOTIMP, 0)
    this.w_PTSERRIF = IIF(NOT EMPTY(NVL(PTSERRIF,SPACE(10))),PTSERRIF,PTSERIAL)
    this.w_PTORDRIF = IIF(NOT EMPTY(NVL(PTORDRIF,0)),PTORDRIF,PTROWORD)
    this.w_PTNUMRIF = IIF(NOT EMPTY(NVL(PTNUMRIF,0)),PTNUMRIF,CPROWNUM)
    SELECT (this.pCurs)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GOTO this.w_KPUNT
  endproc


  proc Init(oParentObject,pEXEC,pCURS,pTIPO,pFLBANC,pTipDis,pIndex)
    this.pEXEC=pEXEC
    this.pCURS=pCURS
    this.pTIPO=pTIPO
    this.pFLBANC=pFLBANC
    this.pTipDis=pTipDis
    this.pIndex=pIndex
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC,pCURS,pTIPO,pFLBANC,pTipDis,pIndex"
endproc
