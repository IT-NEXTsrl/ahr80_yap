* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bsb                                                        *
*              Seleziona conti banche                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-30                                                      *
* Last revis.: 2014-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bsb",oParentObject)
return(i_retval)

define class tgste_bsb as StdBatch
  * --- Local variables
  w_ZOOMFILE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona o meno tutti i Conti Banca
    this.w_ZOOMFILE = this.oParentObject.w_Zoomcf.cCursor
    * --- Aggiorno il cursore
    Update ( this.w_ZoomFile ) Set Xchk = iif(this.oParentObject.w_Selezi="S", 1, 0)
    Select (this.w_ZoomFile) 
 Go Top
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
