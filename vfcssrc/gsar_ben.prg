* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ben                                                        *
*              Entit�-check archivio principale                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-19                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_ben",oParentObject,m.pEXEC)
return(i_retval)

define class tgsar_ben as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_PADRE = .NULL.
  w_MESS = space(254)
  w_MINROW = 0
  w_PRINCROW = 0
  w_OKLINK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifiche alla conferma dell'entit�
    * --- C esegue controlli
    *     V esegue verifica legame
    *     L controllo finale  legame
    do case
      case this.pEXEC="C"
        this.w_PRINCROW = -99999999
        this.oParentObject.w_RESCHK = 0
        this.w_MINROW = 99999999
        this.w_PADRE = THIS.oParentObject
        * --- Verifica che non vi siano piu' tabelle principali e che la tabella principale abbia
        *     il CPROWORD piu' basso di tutti
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow()
            this.w_MINROW = Min ( this.w_MINROW , this.oParentObject.w_CPROWORD )
            if this.oParentObject.w_ENPRINCI="S"
              if this.w_PRINCROW<>-99999999
                this.w_MESS = "Non � possibile definire piu' di un archivio principale."
              else
                this.w_PRINCROW = this.oParentObject.w_CPROWORD
                this.oParentObject.w_TABPRINC = Alltrim( this.w_PADRE.Get( "w_EN_TABLE" ) )
                if Lower(this.w_PADRE.currentEvent)=lower("w_EN_TABLE Changed") Or Lower(this.w_PADRE.currentEvent)=lower("w_ENPRINCI Changed")
                  this.w_PADRE.Set("w_ENAUTONU" , IIF(this.oParentObject.w_ENPRINCI="N","",IIF(this.oParentObject.w_TABPRINC="DOC_MAST","SEDOC",IIF(this.oParentObject.w_TABPRINC="MVM_MAST","SEMVM",IIF(this.oParentObject.w_TABPRINC="COR_RISP","SERPLU","")))))     
                endif
              endif
            endif
            if this.w_PRINCROW>this.w_MINROW And Empty( this.w_MESS )
              this.w_MESS = "L'archivio principale deve avere il numero riga piu' basso"
            endif
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if not Empty( this.w_MESS )
            * --- Il valore sarebbe 'N' ma essendo una combo nel Trs ha valori numerici
            *     'N' = 1 = Collegato
            *     'S' = 2 = Principale
            this.w_PADRE.Set("w_ENPRINCI" , 1)     
            ah_ErrorMsg("%1",,"", this.w_MESS)
            this.oParentObject.w_RESCHK = -1
            exit
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos(.F.)     
      case this.pEXEC="V"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pEXEC="L"
        if this.oParentObject.w_ENTIPENT="V"
          * --- Controlli vendite funzioni avanzate
          this.oParentObject.w_RESCHK = 0
          this.w_PADRE = THIS.oParentObject
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            if this.w_PADRE.FullRow()
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if Not this.w_OKLINK 
              this.oParentObject.w_RESCHK = -1
              Exit
            else
              this.w_PADRE.NextRow()     
            endif
          enddo
          this.w_PADRE.RePos(.F.)     
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OKLINK = .t.
    if (Not Empty(this.oParentObject.w_EN__LINK) OR Not Empty(this.oParentObject.w_ENFILTRO)) AND this.oParentObject.w_ENTIPENT="V"
      if cp_ExistTableDef( this.oParentObject.w_EN_TABLE)
         
 L_FONTE_IDX=Cp_OpenTable( this.oParentObject.w_EN_TABLE) 
 CodTab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 L_nConn = i_TableProp[L_FONTE_IDX,3] 
 cWhere="" 
 cmsql="" 
 condjoin=""
        if Not Empty(this.oParentObject.w_ENTABCOL) 
          * --- Eseguo il filtro dopo aver creato il cursore
          L_FONTE_IDX=Cp_OpenTable( this.oParentObject.w_ENTABCOL) 
 TabColl=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 CondJoin="( "+ ALLtrim(TABCOLL) +" "+ ALLtrim(this.oParentObject.w_ENTABCOL)+ " Inner Join " + Alltrim(CODTAB) +" "+Alltrim(this.oParentObject.w_EN_TABLE)+ " On "+ Alltrim(this.oParentObject.w_EN__LINK) +" ) "
          cmsql="Select " +Alltrim(this.oParentObject.w_EN_TABLE) + ".*" + " from " + CondJoin 
           
 cWhere= " Where 1=0 "
        endif
        if Not Empty(this.oParentObject.w_ENFILTRO)
           
 cWhere="  Where "+ this.oParentObject.w_ENFILTRO
          if Empty(CondJoin)
             
 cmsql="Select " +Alltrim(this.oParentObject.w_EN_TABLE) + ".*" + " from " + this.oParentObject.w_EN_TABLE 
          endif
        endif
        if Not Empty(cmsql) and cp_SQL(L_nConn, cmsql, Alltrim(this.oParentObject.w_EN_TABLE) )>0
          L_Err=.f.
          if Not Empty(cWhere) AND Empty(this.oParentObject.w_ENTABCOL) 
             
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 cSelect=cmsql + " " + Alltrim(cWhere) + " Into cursor " + Alltrim(this.oParentObject.w_EN_TABLE) 
 &cselect
            on error &L_OldError 
          endif
          if L_Err
            ah_ErrorMsg("Attenzione, tabella %1 presenta legame non corretto errore: %0%2",,"", Alltrim(this.oParentObject.w_EN_TABLE),Message())
            this.w_OKLINK = .f.
          else
            if this.pEXEC="V"
              ah_ErrorMsg("Legame corretto","i","")
            endif
          endif
          if Used(Alltrim(this.oParentObject.w_EN_TABLE))
            Select (Alltrim(this.oParentObject.w_EN_TABLE)) 
 Use
          endif
        else
          ah_ErrorMsg("Attenzione, tabella %1 presenta legame incongruente",,"", Alltrim(this.oParentObject.w_EN_TABLE))
          this.w_OKLINK = .f.
        endif
        if Not this.w_OKLINK and this.pEXEC="V" and This.oparentobject.cFunction <> "Query"
          this.oParentObject.w_ENTABCOL = SPACE(30)
          this.oParentObject.w_DESCOLL = SPACE(60)
        endif
        this.oParentObject.o_EN_TABLE = this.oParentObject.w_EN_TABLE
      endif
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
