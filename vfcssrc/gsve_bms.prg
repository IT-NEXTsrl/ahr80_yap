* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bms                                                        *
*              Stampa messaggi                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_3]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2000-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bms",oParentObject)
return(i_retval)

define class tgsve_bms as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Messaggi - questo Batch pu� essere rimosso se aggiunta la possibilit� di utilizzare la
    * --- clausola DISTINCT in una frase SQL con una UNION - In questo caso si rintrodurr� anche �l'Output Utente nella maschera (GSVE_SME
    do case
      case NOT EMPTY(this.oParentObject.w_CODICE) OR NOT EMPTY(this.oParentObject.w_RIFER)
        * --- Prendo tutti i messaggi che hanno un cli/for o solo w_RIFER
        vx_exec("QUERY\GSVE_SMS.VQR,QUERY\GSVE_SMS.FRX",this)
        i_retcode = 'stop'
        return
      case NOT EMPTY(this.oParentObject.w_CATCOM)
        * --- Prendo tutti i messaggi che hanno una categoria contabile
        vx_exec("QUERY\GSVE1SMS.VQR,QUERY\GSVE_SMS.FRX",this)
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CATCOM) AND EMPTY(this.oParentObject.w_CODICE)
        * --- Nessun filtro sui clienti / fornitori ne sulle categorie
        if empty(this.oParentObject.w_RIFER)
          VQ_EXEC("QUERY\GSVE3SMS.VQR",THIS,"TEMP1")
        else
          VQ_EXEC("QUERY\GSVE_SMS.VQR",THIS,"TEMP1")
        endif
        VQ_EXEC("QUERY\GSVE1SMS.VQR",THIS,"TEMP2")
        VQ_EXEC("QUERY\GSVE2SMS.VQR",THIS,"TEMP3")
        VQ_EXEC("QUERY\GSVE4SMS.VQR",THIS,"TEMP4")
        * --- Ordinati per data messaggio
        SELECT * FROM TEMP1 ;
        UNION ;
        SELECT * FROM TEMP2 UNION ;
        SELECT * FROM TEMP3 UNION ;
        SELECT * FROM TEMP4 ORDER BY 2 INTO CURSOR __TMP__ NOFILTER
        * --- Lancio il report
        CP_CHPRN("QUERY\GSVE_SMS.FRX", " ", this)
        if used("TEMP1")
          select TEMP1
          use
        endif
        if used("TEMP2")
          select TEMP2
          use
        endif
        if used("TEMP3")
          select TEMP3
          use
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
