* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcm                                                        *
*              CREAZIONE MODELLO                                               *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-18                                                      *
* Last revis.: 2012-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPATHMOD,pOBJPARAM,pQUERY,pSAVEPATH
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcm",oParentObject,m.pPATHMOD,m.pOBJPARAM,m.pQUERY,m.pSAVEPATH)
return(i_retval)

define class tgsut_bcm as StdBatch
  * --- Local variables
  pPATHMOD = space(254)
  pOBJPARAM = .NULL.
  pQUERY = space(60)
  pSAVEPATH = space(254)
  w_TIPOWP = space(1)
  w_ESTENSIONE = space(3)
  w_SEGNALIBRO = space(20)
  oWord = .NULL.
  oManager = .NULL.
  oDesktop = .NULL.
  oWriter = .NULL.
  oCurs = .NULL.
  oBookmark = .NULL.
  oAnchor = .NULL.
  oTable = .NULL.
  oShape = .NULL.
  oText = .NULL.
  w_oPosizione = .NULL.
  w_MESS = space(200)
  w_COLONNA = 0
  w_NUMCAMPI = 0
  w_PROGRES = 0
  w_SEGNALIBRO_EXISTS = .f.
  w_NUMCOLON = 0
  w_IMMAGINE = .f.
  w_DATO = space(254)
  w_CURSORE = space(20)
  w_PRIMO = .f.
  w_NUMRIG = 0
  oGraphic = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione del modello
    *     
    *     Parametri:
    *     1 - Percorso e nome del modello;.
    *     2 - Oggetto contenente la lista dei parametri con relativi valori;
    *     3 - Nome query da eseguire per estrarre i dati necessari al modello;
    *     4 - Percorso di archiviazione (completo di nome file ed estensione) del documento creato.
    if file(this.pPATHMOD)
      DIMENSION w_CAMPI(100,3), Args(2), noArgs(1)
      NUMERR = 0
      * --- Verifico il tipo di documento da generare (Word o Open Office)
      this.w_ESTENSIONE = JUSTEXT(this.pPATHMOD)
      do case
        case lower(this.w_ESTENSIONE) $ "dot-doc-docx-dotx"
          this.w_TIPOWP = "W"
        case lower(this.w_ESTENSIONE) $ "stw-sxw-odt-ott"
          this.w_TIPOWP = "O"
        otherwise
          * --- Tipologia documento non gestita
          ah_ErrorMsg("Tipo documento non gestito!%0Impossibile proseguire.",16,"")
          i_retcode = 'stop'
          return
      endcase
      * --- Fase 1 - Creazione oggetti per Word processor
      Ah_Msg("Fase 1 - Creazione oggetti per Word Processor")
      * --- Gestioni errori creazione oggetto
      OLDERR = ON("ERROR")
      ON ERROR NUMERR=ERROR()
      if this.w_TIPOWP="W"
        * --- Microsoft Office
        this.oWord = CREATEOBJECT("Word.Application")
      else
        * --- Open Office
        * --- Inizializzo il ServiceManager, il controller desktop e l'array di PropertyValues che utilizzo nell'apertura del Documento...
        this.oManager = createObject("com.sun.star.ServiceManager")
        this.oDesktop = this.oManager.createInstance("com.sun.star.frame.Desktop")
        comarray(this.oDesktop,10)
        && Creo la Struttura di PropertyValues e setto la propriet� per il metodo LoadComponentFromURL 
 Args[1] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 Args[2] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 Args[1].name="Hidden" 
 Args[1].Value=.T. 
 
 && Preparo la struttura Array di PropertyValues per il metodo InsertDocumentFromURL... 
 noArgs[1] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
      endif
      if NUMERR <> 0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Fase 2: Apertura del modello passato come parametro
      Ah_Msg("Fase 2 - Apertura del modello")
      this.pPATHMOD = alltrim(this.pPATHMOD)
      * --- Verifico se il percorso del documento � completo di 'drive'.
      if empty(justdrive(this.pPATHMOD))
        this.pPATHMOD = addbs(sys(5)+sys(2003))+this.pPATHMOD
      endif
      if this.w_TIPOWP="W"
        this.oWord.Documents.Add(this.pPATHMOD)     
      else
        * --- Apro il documento
        this.oWriter = this.oDesktop.LoadComponentFromUrl( "file:///"+StrTran(ALLTRIM(FULLPATH(this.pPATHMOD)),CHR(92),CHR(47)),"_blank", 0, @Args )
        this.oText = this.oWriter.getText()
        this.oCurs = this.oText.createTextCursorByRange(this.oText.getEnd())
      endif
      if NUMERR <> 0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Verifico se il percorso della query � completo (contiene l'unit� disco o di rete)
      if empty(justdrive(this.pQUERY))
        this.pPATHMOD = addbs(sys(5)+sys(2003))+this.pQUERY
      endif
      * --- Fase 3: Sostituzione dei segnalibri con i rispettivi valori presenti  nei dati estratti dalla Query
      Ah_Msg("Fase 3 - Sostituzione segnalibri con i rispettivi valori")
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if NUMERR <> 0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Fase 4: Salvataggio del documento
      Ah_Msg("Fase 4 - Salvo il documento")
      * --- Prima di salvare il documento verifico se in 'pSAVEPATH' ho il percorso completo
      if empty(justdrive(this.pSAVEPATH)) 
        * --- Completo il Path di archiviazione documento
        this.pSAVEPATH = Fullpath(alltrim(this.pSAVEPATH))
      endif
      * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
      if this.w_TIPOWP="W"
        this.oWord.ActiveDocument.SaveAs(Alltrim(this.pSAVEPATH))     
        this.oWord.ActiveDocument.Close()     
      else
         
 Args[1].name="Overwrite" 
 Args[1].value=.T. 
 Args[2].name="FilterName" 
 Args[2].value="MS Word 97" 
 this.oWriter.storeAsURL("file:///"+StrTran(ALLTRIM(this.pSAVEPATH),CHR(92),CHR(47)), @Args)
        * --- Chiudo il documento in modalit� Hidden 
        *     e lo riapro per renderlo visibile
        this.oWriter.close(.T.)     
         
 *Args[1].name="AsTemplate" 
 *Args[1].value=.F. 
 *this.oWriter=this.oDesktop.loadComponentFromURL("file:///"+StrTran(ALLTRIM(this.pSAVEPATH),CHR(92),CHR(47)), "_blank",0, @Args)
      endif
      * --- Gestione errore
      if NUMERR<>0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Cancello il cursore
      if used(this.w_CURSORE)
        select SEZIONI
        use
      endif
      * --- Rilascio gli oggetti creati
      if this.w_TIPOWP="W"
        this.oWord = null
      else
        this.oWriter = null
      endif
    else
      Ah_ERRORMSG("Non esiste il modello: %1%0Nella cartella: %2",48,"",JUSTFNAME(this.pPATHMOD),JUSTPATH(this.pPATHMOD))
    endif
    RELEASE ObjAttribute
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione errori: Fase 1
    if this.w_TIPOWP="W"
      this.w_MESS = "Errore nella generazione Word Processor:%0%1"
    else
      this.w_MESS = "Errore nella generazione SWriter di OpenOffice:%0%1"
    endif
    ah_ErrorMsg(this.w_MESS,,"", MESSAGE())
    * --- Chiudo l'oggetto ed esco dalla generazione del documento
    if this.w_TIPOWP="W"
      this.oWord.ActiveDocument.Close(0,,)     
      this.oWord.Quit()     
    else
      this.oWriter.close(.T.)     
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimpiazzo i segnalibri con i rispettivi valori presenti nel cursore creato dalla query associata al modello
    * --- Assegno alla variabile 'w_NUMCOLON' il valore della costante 'wdEndOfRangeColumnNumber', cio� 17 perch� verr� utilizzata per verificare se il segnalibro si trova all'interno di una tabella.
    this.w_NUMCOLON = 17
    * --- Creo cursore dei dati da inserire nei segnalibri
    vq_exec(this.pQUERY,this.pOBJPARAM,"SEGNALIBRO")
    this.w_CURSORE = "SEGNALIBRO"
    * --- Inserisco la struttura del cursore in un array, per poter applicare la formattazione (eventuale)  ai campi  in base al loro tipo
    Select(this.w_CURSORE)
    this.w_NUMCAMPI = AFIELDS(w_CAMPI)
    * --- Eseguo le sostituzioni con i rispettivi segnalibri
    * --- Scansione Campi Cursore: cancellazione per far rimanere solo quelli Esistenti 
    this.w_PROGRES = 1
    do while this.w_PROGRES <= this.w_NUMCAMPI
      this.w_SEGNALIBRO = w_CAMPI(this.w_PROGRES,1)
      this.w_SEGNALIBRO_EXISTS = .F.
      * --- Cerco nel testo i segnalibri
      if this.w_TIPOWP="W"
        if !this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNALIBRO)
          * --- Segnalibro non trovato
          this.w_SEGNALIBRO_EXISTS = .T.
        endif
      else
        if TYPE("this.oWriter.getBookmarks().getByname(this.w_SEGNALIBRO)")<>"O"
          * --- Segnalibro non trovato
          this.w_SEGNALIBRO_EXISTS = .T.
        endif
      endif
      * --- Diversifcio le operazione da eseguire in base alla presenza  o meno del segnalibro
      if this.w_SEGNALIBRO_EXISTS
        * --- Il segnalibro non � presente nel modello, cancello il relativo campo dall'array
        ADEL(w_CAMPI,this.w_PROGRES)
        this.w_NUMCAMPI = this.w_NUMCAMPI - 1
      else
        * --- Il segnalibro � presente nel modello
        this.w_SEGNALIBRO_EXISTS = .F.
        * --- Verifico se il segnalibro � contenuto in una tabella
        if this.w_TIPOWP="W"
          * --- Restituisce il numero della colonna di tabella contenente il segnalibro
          if this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.Information(this.w_NUMCOLON)=-1
            * --- Il segnalibro non � contenuto in una tabella
            this.w_SEGNALIBRO_EXISTS = .T.
          endif
        else
          this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
          if isNull(this.oAnchor.TextTable)
            * --- Il bookmarks � fuori dalla tabella
            this.w_SEGNALIBRO_EXISTS = .T.
          endif
        endif
        * --- Se il segnalibro esiste ed � fuori da una tabella, inserisco subito il dato al suo interno
        if this.w_SEGNALIBRO_EXISTS
          this.w_IMMAGINE = .F.
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_TIPOWP="W"
            if this.w_Immagine
              * --- Gestione inserimento immagine
              if Not Empty(this.w_DATO) and file(this.w_DATO) AND INLIST(Lower(JUSTEXT(this.w_DATO)),"jpg", "jpeg", "bmp", "tif", "tiff", "png", "gif")
                this.oShape = this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InlineShapes
                this.oShape.AddPicture(this.w_DATO)     
              endif
            else
              * --- Inserimento Dato standard
              this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InsertAfter(this.w_DATO)     
            endif
            * --- Eliminazione segnalibro appena valorizzato
            this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Delete()     
          else
            this.oBookmark = this.oWriter.Bookmarks.getByName(this.w_SEGNALIBRO)
            this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
            comarray(this.oCurs,10)
            if this.w_Immagine
              * --- Gestione inserimento immagine
              if Not Empty(this.w_DATO) and file(this.w_DATO) AND INLIST(JUSTEXT(this.w_DATO),"jpg", "jpeg", "bmp", "tif", "tiff", "png", "gif")
                this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
                this.oGraphic = this.oWriter.createInstance("com.sun.star.text.GraphicObject")
                * --- E' necessario assegnare la struttura ad una variabile array per evitare errori di Office
                 
 Dimension argTemp(1) 
 argTemp[1]=this.oGraphic.Bridge_getStruct("com.sun.star.beans.PropertyValue")
                this.oGraphic.setPropertyValue("GraphicURL","file:///"+strtran(alltrim(this.w_DATO),chr(92),chr(47)))     
                if isNull(this.oAnchor.TextTable)
                  * --- Il bookmark � fuori da una tabella: inserisco immagine
                  this.oWriter.text.insertTextContent(this.oCurs, this.oGraphic, .F.)     
                else
                  * --- Il bookmark � dentro una tabella: inserisco immagine
                  this.oBookmark.Anchor.text.insertTextContent(this.oBookmark.Anchor,this.oGraphic,.F.)     
                endif
              endif
            else
              * --- Inserimento Dato standard
              this.oCurs.setString(this.w_DATO)     
            endif
            * --- Eliminazione segnalibro appena valorizzato
            this.oCurs.collapseToEnd()     
            this.oWriter.Text.removeTextContent(this.oBookmark)     
          endif
          ADEL(w_CAMPI,this.w_PROGRES)
          this.w_NUMCAMPI = this.w_NUMCAMPI - 1
        else
          this.w_PROGRES = this.w_PROGRES + 1
        endif
      endif
    enddo
    if this.w_NUMCAMPI > 0
      this.w_PRIMO = .T.
      this.w_NUMRIG = 2
       
 Select (this.w_CURSORE) 
 Go Top
      SCAN
      this.w_PROGRES = 1
      if !this.w_PRIMO AND (this.w_PROGRES <= this.w_NUMCAMPI)
        * --- Aggiunge una nuova riga alla 1� tabella recuperata nel modello
        if this.w_TIPOWP="W"
          * --- Gestione modello Word
          this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.Tables(1).Rows.Add()     
        else
          * --- ...Il bookmark � in una tabella...
          this.oBookmark = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO)
          this.oAnchor = this.oBookmark.getAnchor()
          this.w_TABLENAME = this.oAnchor.TextTable.getName()
          this.oTable = this.oWriter.getTextTables().getByName(this.w_TABLENAME)
          * --- Aggiunge una riga alla tabella w_TABLENAME
          oArrayCelle=this.oTable.getCellNames()
          this.w_LENGTH = ALEN(oArrayCelle)
          this.w_STRING_N = oArrayCelle[this.w_LENGTH]
          this.w_STRING_N = SUBSTR(this.w_STRING_N,2)
          this.w_STRING_I = VAL(this.w_STRING_N)+1
          this.w_STRING_I = INT(this.w_STRING_I)
          this.oTable.getRows().insertByIndex(this.w_STRING_I,1)     
        endif
      endif
      do while this.w_PROGRES <= this.w_NUMCAMPI
        this.w_Immagine = .F.
        this.w_SEGNALIBRO = w_CAMPI(this.w_PROGRES,1)
        * --- Determinazione del range del segnalibro
        if this.w_TIPOWP="W"
          this.w_oPosizione = this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range
        else
          * --- ...Il bookmark � dentro una cella....
          this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
          this.w_TABLENAME = this.oAnchor.TextTable.getName()
        endif
        if this.w_PRIMO
          * --- Posizione del segnalibro nella tabella
          if this.w_TIPOWP="W"
            this.w_COLONNA = this.w_oPosizione.Information(this.w_NUMCOLON)
          else
            * --- ...Il bookmark � dentro una cella....
            this.w_SUBSTRING = substr(this.oAnchor.Cell.CellName,1,1)
            this.w_COLONNA = ASC(UPPER(this.w_SUBSTRING)) - w_ASCITEST
          endif
          w_CAMPI(this.w_PROGRES,3) = this.w_COLONNA
        else
          this.w_COLONNA = w_CAMPI(this.w_PROGRES,3)
        endif
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_TIPOWP="W"
          * --- Inserisce il dato nella cella
          if this.w_Immagine
            if Not Empty(this.w_DATO) and file(this.w_DATO) AND INLIST(JUSTEXT(this.w_DATO),"jpg", "jpeg", "bmp", "tif", "tiff", "png", "gif")
              this.oShape = this.w_oPosizione.Tables(1).Cell(this.w_NUMRIG,this.w_COLONNA).Range.InlineShapes
              this.oShape.AddPicture(this.w_DATO)     
            endif
          else
            this.w_oPosizione.Tables(1).Cell(this.w_NUMRIG,this.w_COLONNA).Range.InsertAfter(this.w_DATO)     
          endif
        else
          this.w_CELLNAME = UPPER(CHR(this.w_COLONNA+w_ASCITEST))+ALLTRIM(STR(this.w_NUMRIG))
          this.oCurs = this.oWriter.getTextTables().getByName(this.w_TABLENAME).getCellByName(this.w_CELLNAME)
          comarray(this.oCurs,10)
          if this.w_Immagine
            if not Empty (this.w_DATO) and file(this.w_DATO) AND INLIST(JUSTEXT(this.w_DATO),"jpg", "jpeg", "bmp", "tif", "tiff", "png", "gif")
              this.oGraphic = this.oWriter.createInstance("com.sun.star.text.GraphicObject")
              * --- E' necessario assegnare la struttura ad una variabile array per evitare di Office
               
 Dimension argTemp(1) 
 argTemp[1]=this.oGraphic.Bridge_getStruct("com.sun.star.beans.PropertyValue")
              this.oGraphic.setPropertyValue("GraphicURL","file:///"+strtran(alltrim(this.w_DATO),chr(92),chr(47)))     
              * --- Il bookmark � dentro una tabella: inserisco immagine
              this.oCurs.insertTextContent(this.oCurs.createTextCursor(),this.oGraphic,.F.)     
            endif
          else
            this.oCurs.createTextCursor().setString(this.w_DATO)     
          endif
        endif
        this.w_PROGRES = this.w_PROGRES + 1
      enddo
      this.w_PRIMO = .F.
      this.w_NUMRIG = this.w_NUMRIG + 1
      ENDSCAN
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Valori del Cursore
    do case
      case INLIST(w_CAMPI(this.w_PROGRES,2),"D","T")
        this.w_DATO = DTOC(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)),""))
      case INLIST(w_CAMPI(this.w_PROGRES,2),"C","M")
        if "_P" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
          this.w_Immagine = .T.
        endif
        if Not this.w_Immagine And this.w_TIPOWP="O"
          * --- Se non � un'immagine e utilizzo Open Office devo eliminare tutti i CHR(13) dalla stringa
          *     Se � un campo memo infatti visto che sotto windows l'invio inserisce CHR(10)+CHR(13)
          *     in open office da 2 invii
          this.w_DATO = STRTRAN(ALLTRIM(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)),"")),CHR(13),"")
        else
          this.w_DATO = ALLTRIM(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)),""))
        endif
      case INLIST(w_CAMPI(this.w_PROGRES,2),"N","F", "I", "B", "Y")
        * --- Nel caso il contenuto del campo sia Nullo lo sostituisco con ''
        *     Per esempio le Righe descrittive non devono avere Quantit� 0 ma ''
        if Isnull( EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)) )
          this.w_DATO = ""
        else
          do case
            case "_Q" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- Quantit�
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0), v_PQ[15]))
            case "_I" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- Importi
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0), v_PV[20]))
            case "_0" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- Nessun decimale
              this.w_DATO = ALLTRIM(TRAN( cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0) , 0), v_PV[40]))
            case "_1" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- 1 decimale
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0) ,1), v_PV[60]))
            case "_2" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- 2 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL( EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1) ), 0) ,2 ), v_PV[80]))
            case "_3" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- 3 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1) ), 0) , 3), v_PV[100]))
            case "_4" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- 4 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0) ,4), v_PV[120]))
            case "_5" = RIGHT(w_CAMPI(this.w_PROGRES,1) ,2)
              * --- 5 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0) , 5), v_PV[140]))
            otherwise
              * --- Nessun decimale se non specificato nulla
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)), 0), v_PV[40]))
          endcase
        endif
      otherwise
        this.w_DATO = ALLTRIM(STR(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_PROGRES,1)),"")))
    endcase
  endproc


  proc Init(oParentObject,pPATHMOD,pOBJPARAM,pQUERY,pSAVEPATH)
    this.pPATHMOD=pPATHMOD
    this.pOBJPARAM=pOBJPARAM
    this.pQUERY=pQUERY
    this.pSAVEPATH=pSAVEPATH
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPATHMOD,pOBJPARAM,pQUERY,pSAVEPATH"
endproc
