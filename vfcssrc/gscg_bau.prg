* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bau                                                        *
*              Carica automatismi da P.N.                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2014-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bau",oParentObject)
return(i_retval)

define class tgscg_bau as StdBatch
  * --- Local variables
  w_CCTIPCON = space(1)
  w_IVTIPCOP = space(1)
  w_APPO = space(1)
  w_APCODCON = space(15)
  w_IVTIPREG = space(1)
  w_CCCODCON = space(15)
  w_APTIPCON = space(1)
  w_APFLAUTO = space(1)
  w_IVNUMREG = 0
  w_APFLDAVE = space(1)
  w_APCAURIG = space(5)
  w_IVCODCON = space(15)
  w_FLAUC = .f.
  w_IVCODIVA = space(5)
  w_IVTIPCON = space(1)
  w_IVCODCOI = space(15)
  w_IVCONTRO = space(15)
  w_CONDET = space(15)
  w_OREC = 0
  w_NURIGA = 0
  w_APFLPART = space(1)
  w_MESS = space(10)
  w_FLAUTO = space(1)
  * --- WorkFile variables
  MOD_CONT_idx=0
  CAUPRI_idx=0
  CAUIVA_idx=0
  CAUPRI1_idx=0
  CAUIVA1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registra Automatismi Contabili dalla Primanota (da GSCG_MPN)
    * --- Messaggio di Warning nel caso in cui non � stato applicato l'automatismo
    if Not ( this.oParentObject.w_GIACAR=" " AND this.oParentObject.w_CHKAUT>0 )
      this.w_MESS = "ATTENZIONE:"+CHR(13)
      this.w_MESS = this.w_MESS+"il modello contabile verr� creato/aggiornato senza"
      this.w_MESS = this.w_MESS+CHR(13)+"operatori automatismo (+,-,T, ...)"
      this.w_MESS = this.w_MESS+CHR(13)+"Si vuole proseguire?"
      if NOT CP_YesNo(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Richiesta Conferma
    this.w_MESS = "ATTENZIONE:"+CHR(13)
    this.w_MESS = this.w_MESS+"Questa funzione sovrascrive eventuali automatismi preesistenti"
    this.w_MESS = this.w_MESS+CHR(13)+"riferiti alla causale/modello attualmente gestito."
    this.w_MESS = this.w_MESS+CHR(13)+"Si vuole proseguire?"
    if NOT CP_YesNo(this.w_MESS)
      i_retcode = 'stop'
      return
    endif
    * --- Aggiorna CAU_CONT o MOD_CONT
    this.w_CCTIPCON = IIF(this.oParentObject.w_PNTIPCLF $ "CF", this.oParentObject.w_PNTIPCLF, "A")
    this.w_CCCODCON = IIF(this.w_CCTIPCON="A", SPACE(15), this.oParentObject.w_PNCODCLF)
    this.w_FLAUC = .T.
    if NOT EMPTY(this.w_CCCODCON)
      this.w_FLAUC = CP_YesNo("AUTOMATISMO SOLO SU CAUSALE?")
    endif
    if this.w_FLAUC=.F.
      * --- Aggiorna MOD_CONT  (Prima controlla eventuale Modello gia' presente)
      this.w_APPO = "###############"
      * --- Read from MOD_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2],.t.,this.MOD_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCODCON"+;
          " from "+i_cTable+" MOD_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and CCTIPCON = "+cp_ToStrODBC(this.w_CCTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.w_CCCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCODCON;
          from (i_cTable) where;
              CCCODICE = this.oParentObject.w_PNCODCAU;
              and CCTIPCON = this.w_CCTIPCON;
              and CCCODCON = this.w_CCCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.CCCODCON),cp_NullValue(_read_.CCCODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_APPO=this.w_CCCODCON AND i_rows<>0
        if  NOT CP_YesNo("SOVRASCRIVO MODELLO PRECEDENTE?")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Try
    local bErr_00E90518
    bErr_00E90518=bTrsErr
    this.Try_00E90518()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_00E90518
    * --- End
  endproc
  proc Try_00E90518()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_FLAUC=.F.
      * --- Scrive MOD_CONT
      * --- Try
      local bErr_00E90BA8
      bErr_00E90BA8=bTrsErr
      this.Try_00E90BA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_00E90BA8
      * --- End
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_00E90BA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_CONT
    i_nConn=i_TableProp[this.MOD_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCTIPCON"+",CCCODCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODCAU),'MOD_CONT','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPCON),'MOD_CONT','CCTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODCON),'MOD_CONT','CCCODCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.oParentObject.w_PNCODCAU,'CCTIPCON',this.w_CCTIPCON,'CCCODCON',this.w_CCCODCON)
      insert into (i_cTable) (CCCODICE,CCTIPCON,CCCODCON &i_ccchkf. );
         values (;
           this.oParentObject.w_PNCODCAU;
           ,this.w_CCTIPCON;
           ,this.w_CCCODCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrivo Automatismi
    * --- Azzera Preventivamente Dettagli IVA e Primanota
    if this.w_FLAUC=.T.
      * --- Delete from CAUIVA1
      i_nConn=i_TableProp[this.CAUIVA1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
               )
      else
        delete from (i_cTable) where;
              AICODCAU = this.oParentObject.w_PNCODCAU;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CAUPRI1
      i_nConn=i_TableProp[this.CAUPRI1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
               )
      else
        delete from (i_cTable) where;
              APCODCAU = this.oParentObject.w_PNCODCAU;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from CAUIVA
      i_nConn=i_TableProp[this.CAUIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and AITIPCLF = "+cp_ToStrODBC(this.w_CCTIPCON);
              +" and AICODCLF = "+cp_ToStrODBC(this.w_CCCODCON);
               )
      else
        delete from (i_cTable) where;
              AICODCAU = this.oParentObject.w_PNCODCAU;
              and AITIPCLF = this.w_CCTIPCON;
              and AICODCLF = this.w_CCCODCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CAUPRI
      i_nConn=i_TableProp[this.CAUPRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and APTIPCLF = "+cp_ToStrODBC(this.w_CCTIPCON);
              +" and APCODCLF = "+cp_ToStrODBC(this.w_CCCODCON);
               )
      else
        delete from (i_cTable) where;
              APCODCAU = this.oParentObject.w_PNCODCAU;
              and APTIPCLF = this.w_CCTIPCON;
              and APCODCLF = this.w_CCCODCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Scorre sul Tmp Registrazioni Contabili
    this.w_NURIGA = 0
    this.w_CONDET = SPACE(15)
    SELECT (this.oParentObject.cTrsName)
    this.w_OREC = RECNO()
    GO TOP
    SCAN FOR t_PNTIPCON<>" "
    * --- Legge i Dati del Temporaneo
    this.oParentObject.WorkFromTrs()
    this.w_APTIPCON = this.oParentObject.w_PNTIPCON
    if this.w_APTIPCON $ "FC" AND this.w_FLAUC=.T.
      this.w_APCODCON = SPACE(15)
    else
      this.w_APCODCON = this.oParentObject.w_PNCODCON
    endif
    this.w_APFLDAVE = IIF(EMPTY(this.oParentObject.w_FLDAVE), "D", this.oParentObject.w_FLDAVE)
    this.w_APFLAUTO = IIF(EMPTY(this.oParentObject.w_FLAGTOT), "N", this.oParentObject.w_FLAGTOT)
    if this.w_APFLAUTO="D" AND NOT EMPTY(this.w_APCODCON)
      * --- Read from CAUPRI1
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAUPRI1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2],.t.,this.CAUPRI1_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "APFLAUTO"+;
          " from "+i_cTable+" CAUPRI1 where ";
              +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and APTIPCON = "+cp_ToStrODBC(this.w_APTIPCON);
              +" and APCODCON = "+cp_ToStrODBC(this.w_APCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          APFLAUTO;
          from (i_cTable) where;
              APCODCAU = this.oParentObject.w_PNCODCAU;
              and APTIPCON = this.w_APTIPCON;
              and APCODCON = this.w_APCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLAUTO = NVL(cp_ToDate(_read_.APFLAUTO),cp_NullValue(_read_.APFLAUTO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if I_ROWS<>0
        this.w_APFLAUTO = this.w_FLAUTO
      endif
    endif
    this.w_APFLPART = this.oParentObject.w_PNFLPART
    this.w_APCAURIG = IIF(this.oParentObject.w_PNCAURIG=this.oParentObject.w_PNCODCAU, SPACE(5), this.oParentObject.w_PNCAURIG)
    * --- Seleziona Conto IVA da inserire eventualmente nelle righe IVA
    if this.oParentObject.w_TIPSOT="I" AND this.w_APTIPCON="G" AND EMPTY(this.w_CONDET) AND NOT EMPTY(NVL(this.w_APCODCON," "))
      this.w_CONDET = this.w_APCODCON
    endif
    * --- Aggiorna CAUPRI1 o CAUPRI
    if this.w_FLAUC=.T.
      if this.w_APCODCON<>SPACE(15) OR this.w_APTIPCON=this.oParentObject.w_PNTIPCLF OR this.oParentObject.w_PNTIPCLF="N"
        this.w_NURIGA = this.w_NURIGA + 1
        * --- Insert into CAUPRI1
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUPRI1_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"APCODCAU"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODCAU),'CAUPRI1','APCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'CAUPRI1','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'APCODCAU',this.oParentObject.w_PNCODCAU,'CPROWNUM',this.w_NURIGA)
          insert into (i_cTable) (APCODCAU,CPROWNUM &i_ccchkf. );
             values (;
               this.oParentObject.w_PNCODCAU;
               ,this.w_NURIGA;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into CAUPRI1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAUPRI1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUPRI1_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"APTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_APTIPCON),'CAUPRI1','APTIPCON');
          +",APCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_APCODCON),'CAUPRI1','APCODCON');
          +",APFLDAVE ="+cp_NullLink(cp_ToStrODBC(this.w_APFLDAVE),'CAUPRI1','APFLDAVE');
          +",APFLAUTO ="+cp_NullLink(cp_ToStrODBC(this.w_APFLAUTO),'CAUPRI1','APFLAUTO');
          +",APFLPART ="+cp_NullLink(cp_ToStrODBC(this.w_APFLPART),'CAUPRI1','APFLPART');
          +",APCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_APCAURIG),'CAUPRI1','APCAURIG');
              +i_ccchkf ;
          +" where ";
              +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIGA);
                 )
        else
          update (i_cTable) set;
              APTIPCON = this.w_APTIPCON;
              ,APCODCON = this.w_APCODCON;
              ,APFLDAVE = this.w_APFLDAVE;
              ,APFLAUTO = this.w_APFLAUTO;
              ,APFLPART = this.w_APFLPART;
              ,APCAURIG = this.w_APCAURIG;
              &i_ccchkf. ;
           where;
              APCODCAU = this.oParentObject.w_PNCODCAU;
              and CPROWNUM = this.w_NURIGA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      if this.w_APCODCON<>SPACE(15)
        this.w_NURIGA = this.w_NURIGA + 1
        * --- Insert into CAUPRI
        i_nConn=i_TableProp[this.CAUPRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUPRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"APCODCAU"+",APTIPCLF"+",APCODCLF"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODCAU),'CAUPRI','APCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPCON),'CAUPRI','APTIPCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODCON),'CAUPRI','APCODCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'CAUPRI','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'APCODCAU',this.oParentObject.w_PNCODCAU,'APTIPCLF',this.w_CCTIPCON,'APCODCLF',this.w_CCCODCON,'CPROWNUM',this.w_NURIGA)
          insert into (i_cTable) (APCODCAU,APTIPCLF,APCODCLF,CPROWNUM &i_ccchkf. );
             values (;
               this.oParentObject.w_PNCODCAU;
               ,this.w_CCTIPCON;
               ,this.w_CCCODCON;
               ,this.w_NURIGA;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into CAUPRI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAUPRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUPRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"APTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_APTIPCON),'CAUPRI','APTIPCON');
          +",APCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_APCODCON),'CAUPRI','APCODCON');
          +",APFLDAVE ="+cp_NullLink(cp_ToStrODBC(this.w_APFLDAVE),'CAUPRI','APFLDAVE');
          +",APFLAUTO ="+cp_NullLink(cp_ToStrODBC(this.w_APFLAUTO),'CAUPRI','APFLAUTO');
          +",APFLPART ="+cp_NullLink(cp_ToStrODBC(this.w_APFLPART),'CAUPRI','APFLPART');
          +",APCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_APCAURIG),'CAUPRI','APCAURIG');
              +i_ccchkf ;
          +" where ";
              +"APCODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
              +" and APTIPCLF = "+cp_ToStrODBC(this.w_CCTIPCON);
              +" and APCODCLF = "+cp_ToStrODBC(this.w_CCCODCON);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIGA);
                 )
        else
          update (i_cTable) set;
              APTIPCON = this.w_APTIPCON;
              ,APCODCON = this.w_APCODCON;
              ,APFLDAVE = this.w_APFLDAVE;
              ,APFLAUTO = this.w_APFLAUTO;
              ,APFLPART = this.w_APFLPART;
              ,APCAURIG = this.w_APCAURIG;
              &i_ccchkf. ;
           where;
              APCODCAU = this.oParentObject.w_PNCODCAU;
              and APTIPCLF = this.w_CCTIPCON;
              and APCODCLF = this.w_CCCODCON;
              and CPROWNUM = this.w_NURIGA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    ENDSCAN
    if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
      GOTO this.w_OREC
      this.oParentObject.WorkFromTrs()
    endif
    * --- Scorre sul Tmp Registrazioni IVA
    this.w_NURIGA = 0
    SELECT (this.oParentObject.GSCG_MIV.cTrsName)
    this.w_OREC = RECNO()
    GO TOP
    SCAN FOR t_IVCODIVA<>SPACE(5)
    * --- Legge i Dati del Temporaneo
    this.w_IVCODIVA = t_IVCODIVA
    this.w_IVTIPCON = t_IVTIPCON
    this.w_IVCODCON = t_IVCODCON
    this.w_IVCODCOI = t_IVCODCOI
    this.w_IVTIPREG = t_CTIPREG
    this.w_IVNUMREG = t_IVNUMREG
    this.w_IVCONTRO = t_IVCONTRO
    this.w_IVTIPCOP = t_IVTIPCOP
    * --- Se non esiste un conto IVA lo Prende dalle Righe Contabili
    if EMPTY(NVL(this.w_IVCODCON," ")) AND NOT EMPTY(this.w_CONDET)
      this.w_IVTIPCON = "G"
      this.w_IVCODCON = this.w_CONDET
    endif
    * --- Aggiorna CAUIVA1 o CAUIVA
    this.w_NURIGA = this.w_NURIGA + 1
    if this.w_FLAUC=.T.
      * --- Insert into CAUIVA1
      i_nConn=i_TableProp[this.CAUIVA1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUIVA1_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"AICODCAU"+",CPROWNUM"+",AITIPCOP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODCAU),'CAUIVA1','AICODCAU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'CAUIVA1','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'CAUIVA1','AITIPCOP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'AICODCAU',this.oParentObject.w_PNCODCAU,'CPROWNUM',this.w_NURIGA,'AITIPCOP',this.w_IVTIPCOP)
        insert into (i_cTable) (AICODCAU,CPROWNUM,AITIPCOP &i_ccchkf. );
           values (;
             this.oParentObject.w_PNCODCAU;
             ,this.w_NURIGA;
             ,this.w_IVTIPCOP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into CAUIVA1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAUIVA1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUIVA1_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AICODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'CAUIVA1','AICODIVA');
        +",AITIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCON),'CAUIVA1','AITIPCON');
        +",AICONDET ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODCON),'CAUIVA1','AICONDET');
        +",AICONIND ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODCOI),'CAUIVA1','AICONIND');
        +",AITIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'CAUIVA1','AITIPREG');
        +",AINUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_IVNUMREG),'CAUIVA1','AINUMREG');
        +",AICONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'CAUIVA1','AICONTRO');
        +",AITIPCOP ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'CAUIVA1','AITIPCOP');
            +i_ccchkf ;
        +" where ";
            +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIGA);
               )
      else
        update (i_cTable) set;
            AICODIVA = this.w_IVCODIVA;
            ,AITIPCON = this.w_IVTIPCON;
            ,AICONDET = this.w_IVCODCON;
            ,AICONIND = this.w_IVCODCOI;
            ,AITIPREG = this.w_IVTIPREG;
            ,AINUMREG = this.w_IVNUMREG;
            ,AICONTRO = this.w_IVCONTRO;
            ,AITIPCOP = this.w_IVTIPCOP;
            &i_ccchkf. ;
         where;
            AICODCAU = this.oParentObject.w_PNCODCAU;
            and CPROWNUM = this.w_NURIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Insert into CAUIVA
      i_nConn=i_TableProp[this.CAUIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"AICODCAU"+",AITIPCLF"+",AICODCLF"+",CPROWNUM"+",AITIPCOP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNCODCAU),'CAUIVA','AICODCAU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPCON),'CAUIVA','AITIPCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODCON),'CAUIVA','AICODCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'CAUIVA','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'CAUIVA','AITIPCOP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'AICODCAU',this.oParentObject.w_PNCODCAU,'AITIPCLF',this.w_CCTIPCON,'AICODCLF',this.w_CCCODCON,'CPROWNUM',this.w_NURIGA,'AITIPCOP',this.w_IVTIPCOP)
        insert into (i_cTable) (AICODCAU,AITIPCLF,AICODCLF,CPROWNUM,AITIPCOP &i_ccchkf. );
           values (;
             this.oParentObject.w_PNCODCAU;
             ,this.w_CCTIPCON;
             ,this.w_CCCODCON;
             ,this.w_NURIGA;
             ,this.w_IVTIPCOP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into CAUIVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAUIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AICODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'CAUIVA','AICODIVA');
        +",AITIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCON),'CAUIVA','AITIPCON');
        +",AICONDET ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODCON),'CAUIVA','AICONDET');
        +",AICONIND ="+cp_NullLink(cp_ToStrODBC(this.w_IVCODCOI),'CAUIVA','AICONIND');
        +",AITIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'CAUIVA','AITIPREG');
        +",AINUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_IVNUMREG),'CAUIVA','AINUMREG');
        +",AICONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'CAUIVA','AICONTRO');
        +",AITIPCOP ="+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'CAUIVA','AITIPCOP');
            +i_ccchkf ;
        +" where ";
            +"AICODCAU = "+cp_ToStrODBC(this.oParentObject.w_PNCODCAU);
            +" and AITIPCLF = "+cp_ToStrODBC(this.w_CCTIPCON);
            +" and AICODCLF = "+cp_ToStrODBC(this.w_CCCODCON);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIGA);
               )
      else
        update (i_cTable) set;
            AICODIVA = this.w_IVCODIVA;
            ,AITIPCON = this.w_IVTIPCON;
            ,AICONDET = this.w_IVCODCON;
            ,AICONIND = this.w_IVCODCOI;
            ,AITIPREG = this.w_IVTIPREG;
            ,AINUMREG = this.w_IVNUMREG;
            ,AICONTRO = this.w_IVCONTRO;
            ,AITIPCOP = this.w_IVTIPCOP;
            &i_ccchkf. ;
         where;
            AICODCAU = this.oParentObject.w_PNCODCAU;
            and AITIPCLF = this.w_CCTIPCON;
            and AICODCLF = this.w_CCCODCON;
            and CPROWNUM = this.w_NURIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ENDSCAN
    if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
      GOTO this.w_OREC
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MOD_CONT'
    this.cWorkTables[2]='CAUPRI'
    this.cWorkTables[3]='CAUIVA'
    this.cWorkTables[4]='CAUPRI1'
    this.cWorkTables[5]='CAUIVA1'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
