* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao1                                                        *
*              Contropartite/vincoli                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_124]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2015-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ao1")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ao1")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ao1")
  return

* --- Class definition
define class tgsar_ao1 as StdPCForm
  Width  = 749
  Height = 360
  Top    = 60
  Left   = 10
  cComment = "Contropartite/vincoli"
  cPrg = "gsar_ao1"
  HelpContextID=176781161
  add object cnt as tcgsar_ao1
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ao1 as PCContext
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_COCONINC = space(15)
  w_DESINC = space(40)
  w_COCOIINC = space(5)
  w_COCONIMB = space(15)
  w_DESIMB = space(40)
  w_COCOIIMB = space(5)
  w_COCONTRA = space(15)
  w_DESTRA = space(40)
  w_COCOITRA = space(5)
  w_COCONBOL = space(15)
  w_DESBOL = space(40)
  w_COCOIBOL = space(5)
  w_COCONCAU = space(15)
  w_PERIVB = 0
  w_COCOICAU = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_TIPART = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_COCACINC = space(5)
  w_DCACINC = space(35)
  w_COCOCINC = space(15)
  w_DESCINC = space(40)
  w_COARTDES = space(20)
  w_COARTDES = space(20)
  w_TR122 = space(1)
  w_FP2 = space(1)
  w_DESART = space(40)
  w_COCAUVEO = space(5)
  w_CVDES = space(35)
  w_TIPREGA = space(1)
  w_DESCAU = space(40)
  w_PERIVC = 0
  w_COCRITAT = space(15)
  w_DESRIT = space(40)
  w_COCAUSPL = space(5)
  w_COCONSPL = space(15)
  w_DESCAUSPL = space(35)
  w_DESFINASPY = space(40)
  w_TIPREGSPL = space(1)
  w_DATOBSOSPL = space(8)
  w_DATOBSOSPY = space(8)
  w_FLPARTSPY = space(1)
  w_FLPART = space(1)
  w_TIPOENTE = space(1)
  proc Save(oFrom)
    this.w_COCODAZI = oFrom.w_COCODAZI
    this.w_RAGAZI = oFrom.w_RAGAZI
    this.w_COTIPCON = oFrom.w_COTIPCON
    this.w_COCONINC = oFrom.w_COCONINC
    this.w_DESINC = oFrom.w_DESINC
    this.w_COCOIINC = oFrom.w_COCOIINC
    this.w_COCONIMB = oFrom.w_COCONIMB
    this.w_DESIMB = oFrom.w_DESIMB
    this.w_COCOIIMB = oFrom.w_COCOIIMB
    this.w_COCONTRA = oFrom.w_COCONTRA
    this.w_DESTRA = oFrom.w_DESTRA
    this.w_COCOITRA = oFrom.w_COCOITRA
    this.w_COCONBOL = oFrom.w_COCONBOL
    this.w_DESBOL = oFrom.w_DESBOL
    this.w_COCOIBOL = oFrom.w_COCOIBOL
    this.w_COCONCAU = oFrom.w_COCONCAU
    this.w_PERIVB = oFrom.w_PERIVB
    this.w_COCOICAU = oFrom.w_COCOICAU
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_TIPART = oFrom.w_TIPART
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_COCACINC = oFrom.w_COCACINC
    this.w_DCACINC = oFrom.w_DCACINC
    this.w_COCOCINC = oFrom.w_COCOCINC
    this.w_DESCINC = oFrom.w_DESCINC
    this.w_COARTDES = oFrom.w_COARTDES
    this.w_COARTDES = oFrom.w_COARTDES
    this.w_TR122 = oFrom.w_TR122
    this.w_FP2 = oFrom.w_FP2
    this.w_DESART = oFrom.w_DESART
    this.w_COCAUVEO = oFrom.w_COCAUVEO
    this.w_CVDES = oFrom.w_CVDES
    this.w_TIPREGA = oFrom.w_TIPREGA
    this.w_DESCAU = oFrom.w_DESCAU
    this.w_PERIVC = oFrom.w_PERIVC
    this.w_COCRITAT = oFrom.w_COCRITAT
    this.w_DESRIT = oFrom.w_DESRIT
    this.w_COCAUSPL = oFrom.w_COCAUSPL
    this.w_COCONSPL = oFrom.w_COCONSPL
    this.w_DESCAUSPL = oFrom.w_DESCAUSPL
    this.w_DESFINASPY = oFrom.w_DESFINASPY
    this.w_TIPREGSPL = oFrom.w_TIPREGSPL
    this.w_DATOBSOSPL = oFrom.w_DATOBSOSPL
    this.w_DATOBSOSPY = oFrom.w_DATOBSOSPY
    this.w_FLPARTSPY = oFrom.w_FLPARTSPY
    this.w_FLPART = oFrom.w_FLPART
    this.w_TIPOENTE = oFrom.w_TIPOENTE
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_COCODAZI = this.w_COCODAZI
    oTo.w_RAGAZI = this.w_RAGAZI
    oTo.w_COTIPCON = this.w_COTIPCON
    oTo.w_COCONINC = this.w_COCONINC
    oTo.w_DESINC = this.w_DESINC
    oTo.w_COCOIINC = this.w_COCOIINC
    oTo.w_COCONIMB = this.w_COCONIMB
    oTo.w_DESIMB = this.w_DESIMB
    oTo.w_COCOIIMB = this.w_COCOIIMB
    oTo.w_COCONTRA = this.w_COCONTRA
    oTo.w_DESTRA = this.w_DESTRA
    oTo.w_COCOITRA = this.w_COCOITRA
    oTo.w_COCONBOL = this.w_COCONBOL
    oTo.w_DESBOL = this.w_DESBOL
    oTo.w_COCOIBOL = this.w_COCOIBOL
    oTo.w_COCONCAU = this.w_COCONCAU
    oTo.w_PERIVB = this.w_PERIVB
    oTo.w_COCOICAU = this.w_COCOICAU
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_TIPART = this.w_TIPART
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_COCACINC = this.w_COCACINC
    oTo.w_DCACINC = this.w_DCACINC
    oTo.w_COCOCINC = this.w_COCOCINC
    oTo.w_DESCINC = this.w_DESCINC
    oTo.w_COARTDES = this.w_COARTDES
    oTo.w_COARTDES = this.w_COARTDES
    oTo.w_TR122 = this.w_TR122
    oTo.w_FP2 = this.w_FP2
    oTo.w_DESART = this.w_DESART
    oTo.w_COCAUVEO = this.w_COCAUVEO
    oTo.w_CVDES = this.w_CVDES
    oTo.w_TIPREGA = this.w_TIPREGA
    oTo.w_DESCAU = this.w_DESCAU
    oTo.w_PERIVC = this.w_PERIVC
    oTo.w_COCRITAT = this.w_COCRITAT
    oTo.w_DESRIT = this.w_DESRIT
    oTo.w_COCAUSPL = this.w_COCAUSPL
    oTo.w_COCONSPL = this.w_COCONSPL
    oTo.w_DESCAUSPL = this.w_DESCAUSPL
    oTo.w_DESFINASPY = this.w_DESFINASPY
    oTo.w_TIPREGSPL = this.w_TIPREGSPL
    oTo.w_DATOBSOSPL = this.w_DATOBSOSPL
    oTo.w_DATOBSOSPY = this.w_DATOBSOSPY
    oTo.w_FLPARTSPY = this.w_FLPARTSPY
    oTo.w_FLPART = this.w_FLPART
    oTo.w_TIPOENTE = this.w_TIPOENTE
    PCContext::Load(oTo)
enddefine

define class tcgsar_ao1 as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 749
  Height = 360
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-04"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao1"
  cComment = "Contropartite/vincoli"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_COCONINC = space(15)
  w_DESINC = space(40)
  w_COCOIINC = space(5)
  w_COCONIMB = space(15)
  w_DESIMB = space(40)
  w_COCOIIMB = space(5)
  w_COCONTRA = space(15)
  w_DESTRA = space(40)
  w_COCOITRA = space(5)
  w_COCONBOL = space(15)
  w_DESBOL = space(40)
  w_COCOIBOL = space(5)
  w_COCONCAU = space(15)
  w_PERIVB = 0
  w_COCOICAU = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COCACINC = space(5)
  w_DCACINC = space(35)
  w_COCOCINC = space(15)
  w_DESCINC = space(40)
  w_COARTDES = space(20)
  w_COARTDES = space(20)
  w_TR122 = space(1)
  w_FP2 = space(1)
  w_DESART = space(40)
  w_COCAUVEO = space(5)
  w_CVDES = space(35)
  w_TIPREGA = space(1)
  w_DESCAU = space(40)
  w_PERIVC = 0
  w_COCRITAT = space(15)
  w_DESRIT = space(40)
  w_COCAUSPL = space(5)
  o_COCAUSPL = space(5)
  w_COCONSPL = space(15)
  w_DESCAUSPL = space(35)
  w_DESFINASPY = space(40)
  w_TIPREGSPL = space(1)
  w_DATOBSOSPL = ctod('  /  /  ')
  w_DATOBSOSPY = ctod('  /  /  ')
  w_FLPARTSPY = space(1)
  w_FLPART = space(1)
  w_TIPOENTE = space(1)
  w_Imballo = .NULL.
  w_Trasporto = .NULL.
  w_Riferime = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao1Pag1","gsar_ao1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 118333706
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOCONINC_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao1
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_Imballo = this.oPgFrm.Pages(1).oPag.Imballo
      this.w_Trasporto = this.oPgFrm.Pages(1).oPag.Trasporto
      this.w_Riferime = this.oPgFrm.Pages(1).oPag.Riferime
      DoDefault()
    proc Destroy()
      this.w_Imballo = .NULL.
      this.w_Trasporto = .NULL.
      this.w_Riferime = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ao1'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    local link_1_43_joined
    link_1_43_joined=.f.
    local link_1_51_joined
    link_1_51_joined=.f.
    local link_1_58_joined
    link_1_58_joined=.f.
    local link_1_59_joined
    link_1_59_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_43_joined=this.AddJoinedLink_1_43(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_51_joined=this.AddJoinedLink_1_51(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_58_joined=this.AddJoinedLink_1_58(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_59_joined=this.AddJoinedLink_1_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_DESINC = space(40)
        .w_DESIMB = space(40)
        .w_DESTRA = space(40)
        .w_DESBOL = space(40)
        .w_PERIVB = 0
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DCACINC = space(35)
        .w_DESCINC = space(40)
        .w_TR122 = space(1)
        .w_FP2 = space(1)
        .w_DESART = space(40)
        .w_CVDES = space(35)
        .w_TIPREGA = space(1)
        .w_DESCAU = space(40)
        .w_PERIVC = 0
        .w_DESRIT = space(40)
        .w_DESCAUSPL = space(35)
        .w_DESFINASPY = space(40)
        .w_TIPREGSPL = space(1)
        .w_DATOBSOSPL = ctod("  /  /  ")
        .w_DATOBSOSPY = ctod("  /  /  ")
        .w_FLPARTSPY = space(1)
        .w_FLPART = space(1)
        .w_TIPOENTE = ''
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_COCONINC = NVL(COCONINC,space(15))
          if link_1_4_joined
            this.w_COCONINC = NVL(ANCODICE104,NVL(this.w_COCONINC,space(15)))
            this.w_DESINC = NVL(ANDESCRI104,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO104),ctod("  /  /  "))
          else
          .link_1_4('Load')
          endif
        .w_COCOIINC = NVL(COCOIINC,space(5))
          if link_1_6_joined
            this.w_COCOIINC = NVL(IVCODIVA106,NVL(this.w_COCOIINC,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO106),ctod("  /  /  "))
          else
          .link_1_6('Load')
          endif
        .w_COCONIMB = NVL(COCONIMB,space(15))
          if link_1_7_joined
            this.w_COCONIMB = NVL(ANCODICE107,NVL(this.w_COCONIMB,space(15)))
            this.w_DESIMB = NVL(ANDESCRI107,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO107),ctod("  /  /  "))
          else
          .link_1_7('Load')
          endif
        .w_COCOIIMB = NVL(COCOIIMB,space(5))
          if link_1_9_joined
            this.w_COCOIIMB = NVL(IVCODIVA109,NVL(this.w_COCOIIMB,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO109),ctod("  /  /  "))
          else
          .link_1_9('Load')
          endif
        .w_COCONTRA = NVL(COCONTRA,space(15))
          if link_1_10_joined
            this.w_COCONTRA = NVL(ANCODICE110,NVL(this.w_COCONTRA,space(15)))
            this.w_DESTRA = NVL(ANDESCRI110,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO110),ctod("  /  /  "))
          else
          .link_1_10('Load')
          endif
        .w_COCOITRA = NVL(COCOITRA,space(5))
          if link_1_12_joined
            this.w_COCOITRA = NVL(IVCODIVA112,NVL(this.w_COCOITRA,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_COCONBOL = NVL(COCONBOL,space(15))
          if link_1_13_joined
            this.w_COCONBOL = NVL(ANCODICE113,NVL(this.w_COCONBOL,space(15)))
            this.w_DESBOL = NVL(ANDESCRI113,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_COCOIBOL = NVL(COCOIBOL,space(5))
          if link_1_15_joined
            this.w_COCOIBOL = NVL(IVCODIVA115,NVL(this.w_COCOIBOL,space(5)))
            this.w_PERIVB = NVL(IVPERIVA115,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_COCONCAU = NVL(COCONCAU,space(15))
          if link_1_16_joined
            this.w_COCONCAU = NVL(ANCODICE116,NVL(this.w_COCONCAU,space(15)))
            this.w_DESCAU = NVL(ANDESCRI116,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO116),ctod("  /  /  "))
          else
          .link_1_16('Load')
          endif
        .w_COCOICAU = NVL(COCOICAU,space(5))
          if link_1_18_joined
            this.w_COCOICAU = NVL(IVCODIVA118,NVL(this.w_COCOICAU,space(5)))
            this.w_PERIVC = NVL(IVPERIVA118,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO118),ctod("  /  /  "))
          else
          .link_1_18('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_COCACINC = NVL(COCACINC,space(5))
          if link_1_30_joined
            this.w_COCACINC = NVL(CCCODICE130,NVL(this.w_COCACINC,space(5)))
            this.w_DCACINC = NVL(CCDESCRI130,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO130),ctod("  /  /  "))
            this.w_TR122 = NVL(CCTIPREG130,space(1))
            this.w_FP2 = NVL(CCFLPART130,space(1))
          else
          .link_1_30('Load')
          endif
        .w_COCOCINC = NVL(COCOCINC,space(15))
          if link_1_32_joined
            this.w_COCOCINC = NVL(ANCODICE132,NVL(this.w_COCOCINC,space(15)))
            this.w_DESCINC = NVL(ANDESCRI132,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO132),ctod("  /  /  "))
          else
          .link_1_32('Load')
          endif
        .w_COARTDES = NVL(COARTDES,space(20))
          if link_1_35_joined
            this.w_COARTDES = NVL(CACODICE135,NVL(this.w_COARTDES,space(20)))
            this.w_TIPART = NVL(CATIPCON135,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO135),ctod("  /  /  "))
            this.w_DESART = NVL(CADESART135,space(40))
          else
          .link_1_35('Load')
          endif
        .w_COARTDES = NVL(COARTDES,space(20))
          if link_1_36_joined
            this.w_COARTDES = NVL(CACODICE136,NVL(this.w_COARTDES,space(20)))
            this.w_TIPART = NVL(CATIPCON136,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO136),ctod("  /  /  "))
            this.w_DESART = NVL(CADESART136,space(40))
          else
          .link_1_36('Load')
          endif
        .w_COCAUVEO = NVL(COCAUVEO,space(5))
          if link_1_43_joined
            this.w_COCAUVEO = NVL(CCCODICE143,NVL(this.w_COCAUVEO,space(5)))
            this.w_CVDES = NVL(CCDESCRI143,space(35))
            this.w_TIPREGA = NVL(CCTIPREG143,space(1))
          else
          .link_1_43('Load')
          endif
        .w_COCRITAT = NVL(COCRITAT,space(15))
          if link_1_51_joined
            this.w_COCRITAT = NVL(ANCODICE151,NVL(this.w_COCRITAT,space(15)))
            this.w_DESRIT = NVL(ANDESCRI151,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO151),ctod("  /  /  "))
          else
          .link_1_51('Load')
          endif
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Spese generali:"),Ah_MsgFormat("Rivalsa imballo:")))
        .oPgFrm.Page1.oPag.Trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Cassa previdenza:"),Ah_MsgFormat("Rivalsa trasporto:")))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione delle spese generali", "Conto per la contabilizzazione delle spese di imballo"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione della cassa previdenza", "Conto di contabilizzazione rivalsa spese trasporto"))
        .w_COCAUSPL = NVL(COCAUSPL,space(5))
          if link_1_58_joined
            this.w_COCAUSPL = NVL(CCCODICE158,NVL(this.w_COCAUSPL,space(5)))
            this.w_DESCAUSPL = NVL(CCDESCRI158,space(35))
            this.w_TIPREGSPL = NVL(CCTIPREG158,space(1))
            this.w_DATOBSOSPL = NVL(cp_ToDate(CCDTOBSO158),ctod("  /  /  "))
            this.w_FLPART = NVL(CCFLPART158,space(1))
          else
          .link_1_58('Load')
          endif
        .w_COCONSPL = NVL(COCONSPL,space(15))
          if link_1_59_joined
            this.w_COCONSPL = NVL(ANCODICE159,NVL(this.w_COCONSPL,space(15)))
            this.w_DESFINASPY = NVL(ANDESCRI159,space(40))
            this.w_DATOBSOSPY = NVL(cp_ToDate(ANDTOBSO159),ctod("  /  /  "))
            this.w_FLPARTSPY = NVL(ANPARTSN159,space(1))
          else
          .link_1_59('Load')
          endif
        .oPgFrm.Page1.oPag.Riferime.Calculate(IIF(IsAlt(),Ah_MsgFormat("Prestazioni per riferimenti:"),Ah_MsgFormat("Articolo per riferimenti:")))
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_COCONINC = space(15)
      .w_DESINC = space(40)
      .w_COCOIINC = space(5)
      .w_COCONIMB = space(15)
      .w_DESIMB = space(40)
      .w_COCOIIMB = space(5)
      .w_COCONTRA = space(15)
      .w_DESTRA = space(40)
      .w_COCOITRA = space(5)
      .w_COCONBOL = space(15)
      .w_DESBOL = space(40)
      .w_COCOIBOL = space(5)
      .w_COCONCAU = space(15)
      .w_PERIVB = 0
      .w_COCOICAU = space(5)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COCACINC = space(5)
      .w_DCACINC = space(35)
      .w_COCOCINC = space(15)
      .w_DESCINC = space(40)
      .w_COARTDES = space(20)
      .w_COARTDES = space(20)
      .w_TR122 = space(1)
      .w_FP2 = space(1)
      .w_DESART = space(40)
      .w_COCAUVEO = space(5)
      .w_CVDES = space(35)
      .w_TIPREGA = space(1)
      .w_DESCAU = space(40)
      .w_PERIVC = 0
      .w_COCRITAT = space(15)
      .w_DESRIT = space(40)
      .w_COCAUSPL = space(5)
      .w_COCONSPL = space(15)
      .w_DESCAUSPL = space(35)
      .w_DESFINASPY = space(40)
      .w_TIPREGSPL = space(1)
      .w_DATOBSOSPL = ctod("  /  /  ")
      .w_DATOBSOSPY = ctod("  /  /  ")
      .w_FLPARTSPY = space(1)
      .w_FLPART = space(1)
      .w_TIPOENTE = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_COCONINC))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_COCOIINC))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_COCONIMB))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_COCOIIMB))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_COCONTRA))
          .link_1_10('Full')
          endif
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_COCOITRA))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_COCONBOL))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_COCOIBOL))
          .link_1_15('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_COCONCAU))
          .link_1_16('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_COCOICAU))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,23,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(25,26,.f.)
          if not(empty(.w_COCACINC))
          .link_1_30('Full')
          endif
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_COCOCINC))
          .link_1_32('Full')
          endif
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_COARTDES))
          .link_1_35('Full')
          endif
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_COARTDES))
          .link_1_36('Full')
          endif
        .DoRTCalc(32,35,.f.)
          if not(empty(.w_COCAUVEO))
          .link_1_43('Full')
          endif
        .DoRTCalc(36,40,.f.)
          if not(empty(.w_COCRITAT))
          .link_1_51('Full')
          endif
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Spese generali:"),Ah_MsgFormat("Rivalsa imballo:")))
        .oPgFrm.Page1.oPag.Trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Cassa previdenza:"),Ah_MsgFormat("Rivalsa trasporto:")))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione delle spese generali", "Conto per la contabilizzazione delle spese di imballo"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione della cassa previdenza", "Conto di contabilizzazione rivalsa spese trasporto"))
        .DoRTCalc(41,42,.f.)
          if not(empty(.w_COCAUSPL))
          .link_1_58('Full')
          endif
        .w_COCONSPL = Space(15)
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_COCONSPL))
          .link_1_59('Full')
          endif
        .oPgFrm.Page1.oPag.Riferime.Calculate(IIF(IsAlt(),Ah_MsgFormat("Prestazioni per riferimenti:"),Ah_MsgFormat("Articolo per riferimenti:")))
          .DoRTCalc(44,50,.f.)
        .w_TIPOENTE = ''
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOCONINC_1_4.enabled = i_bVal
      .Page1.oPag.oCOCOIINC_1_6.enabled = i_bVal
      .Page1.oPag.oCOCONIMB_1_7.enabled = i_bVal
      .Page1.oPag.oCOCOIIMB_1_9.enabled = i_bVal
      .Page1.oPag.oCOCONTRA_1_10.enabled = i_bVal
      .Page1.oPag.oCOCOITRA_1_12.enabled = i_bVal
      .Page1.oPag.oCOCONBOL_1_13.enabled = i_bVal
      .Page1.oPag.oCOCOIBOL_1_15.enabled = i_bVal
      .Page1.oPag.oCOCONCAU_1_16.enabled = i_bVal
      .Page1.oPag.oCOCOICAU_1_18.enabled = i_bVal
      .Page1.oPag.oCOCACINC_1_30.enabled = i_bVal
      .Page1.oPag.oCOCOCINC_1_32.enabled = i_bVal
      .Page1.oPag.oCOARTDES_1_35.enabled = i_bVal
      .Page1.oPag.oCOARTDES_1_36.enabled = i_bVal
      .Page1.oPag.oCOCAUVEO_1_43.enabled = i_bVal
      .Page1.oPag.oCOCRITAT_1_51.enabled = i_bVal
      .Page1.oPag.oCOCAUSPL_1_58.enabled = i_bVal
      .Page1.oPag.oCOCONSPL_1_59.enabled = i_bVal
      .Page1.oPag.Imballo.enabled = i_bVal
      .Page1.oPag.Trasporto.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.Riferime.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONINC,"COCONINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOIINC,"COCOIINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONIMB,"COCONIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOIIMB,"COCOIIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONTRA,"COCONTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOITRA,"COCOITRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONBOL,"COCONBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOIBOL,"COCOIBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONCAU,"COCONCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOICAU,"COCOICAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCACINC,"COCACINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOCINC,"COCOCINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COARTDES,"COARTDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COARTDES,"COARTDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUVEO,"COCAUVEO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCRITAT,"COCRITAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUSPL,"COCAUSPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONSPL,"COCONSPL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,COCONINC,COCOIINC,COCONIMB"+;
                  ",COCOIIMB,COCONTRA,COCOITRA,COCONBOL,COCOIBOL"+;
                  ",COCONCAU,COCOICAU,UTCC,UTCV,UTDC"+;
                  ",UTDV,COCACINC,COCOCINC,COARTDES,COCAUVEO"+;
                  ",COCRITAT,COCAUSPL,COCONSPL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_COCONINC)+;
                  ","+cp_ToStrODBCNull(this.w_COCOIINC)+;
                  ","+cp_ToStrODBCNull(this.w_COCONIMB)+;
                  ","+cp_ToStrODBCNull(this.w_COCOIIMB)+;
                  ","+cp_ToStrODBCNull(this.w_COCONTRA)+;
                  ","+cp_ToStrODBCNull(this.w_COCOITRA)+;
                  ","+cp_ToStrODBCNull(this.w_COCONBOL)+;
                  ","+cp_ToStrODBCNull(this.w_COCOIBOL)+;
                  ","+cp_ToStrODBCNull(this.w_COCONCAU)+;
                  ","+cp_ToStrODBCNull(this.w_COCOICAU)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COCACINC)+;
                  ","+cp_ToStrODBCNull(this.w_COCOCINC)+;
                  ","+cp_ToStrODBCNull(this.w_COARTDES)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUVEO)+;
                  ","+cp_ToStrODBCNull(this.w_COCRITAT)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUSPL)+;
                  ","+cp_ToStrODBCNull(this.w_COCONSPL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,COCONINC,COCOIINC,COCONIMB,COCOIIMB,COCONTRA,COCOITRA,COCONBOL,COCOIBOL,COCONCAU,COCOICAU,UTCC,UTCV,UTDC,UTDV,COCACINC,COCOCINC,COARTDES,COCAUVEO,COCRITAT,COCAUSPL,COCONSPL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_COCONINC;
                  ,this.w_COCOIINC;
                  ,this.w_COCONIMB;
                  ,this.w_COCOIIMB;
                  ,this.w_COCONTRA;
                  ,this.w_COCOITRA;
                  ,this.w_COCONBOL;
                  ,this.w_COCOIBOL;
                  ,this.w_COCONCAU;
                  ,this.w_COCOICAU;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COCACINC;
                  ,this.w_COCOCINC;
                  ,this.w_COARTDES;
                  ,this.w_COCAUVEO;
                  ,this.w_COCRITAT;
                  ,this.w_COCAUSPL;
                  ,this.w_COCONSPL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",COCONINC="+cp_ToStrODBCNull(this.w_COCONINC)+;
             ",COCOIINC="+cp_ToStrODBCNull(this.w_COCOIINC)+;
             ",COCONIMB="+cp_ToStrODBCNull(this.w_COCONIMB)+;
             ",COCOIIMB="+cp_ToStrODBCNull(this.w_COCOIIMB)+;
             ",COCONTRA="+cp_ToStrODBCNull(this.w_COCONTRA)+;
             ",COCOITRA="+cp_ToStrODBCNull(this.w_COCOITRA)+;
             ",COCONBOL="+cp_ToStrODBCNull(this.w_COCONBOL)+;
             ",COCOIBOL="+cp_ToStrODBCNull(this.w_COCOIBOL)+;
             ",COCONCAU="+cp_ToStrODBCNull(this.w_COCONCAU)+;
             ",COCOICAU="+cp_ToStrODBCNull(this.w_COCOICAU)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COCACINC="+cp_ToStrODBCNull(this.w_COCACINC)+;
             ",COCOCINC="+cp_ToStrODBCNull(this.w_COCOCINC)+;
             ",COARTDES="+cp_ToStrODBCNull(this.w_COARTDES)+;
             ",COCAUVEO="+cp_ToStrODBCNull(this.w_COCAUVEO)+;
             ",COCRITAT="+cp_ToStrODBCNull(this.w_COCRITAT)+;
             ",COCAUSPL="+cp_ToStrODBCNull(this.w_COCAUSPL)+;
             ",COCONSPL="+cp_ToStrODBCNull(this.w_COCONSPL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,COCONINC=this.w_COCONINC;
             ,COCOIINC=this.w_COCOIINC;
             ,COCONIMB=this.w_COCONIMB;
             ,COCOIIMB=this.w_COCOIIMB;
             ,COCONTRA=this.w_COCONTRA;
             ,COCOITRA=this.w_COCOITRA;
             ,COCONBOL=this.w_COCONBOL;
             ,COCOIBOL=this.w_COCOIBOL;
             ,COCONCAU=this.w_COCONCAU;
             ,COCOICAU=this.w_COCOICAU;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COCACINC=this.w_COCACINC;
             ,COCOCINC=this.w_COCOCINC;
             ,COARTDES=this.w_COARTDES;
             ,COCAUVEO=this.w_COCAUVEO;
             ,COCRITAT=this.w_COCRITAT;
             ,COCAUSPL=this.w_COCAUSPL;
             ,COCONSPL=this.w_COCONSPL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Spese generali:"),Ah_MsgFormat("Rivalsa imballo:")))
        .oPgFrm.Page1.oPag.Trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Cassa previdenza:"),Ah_MsgFormat("Rivalsa trasporto:")))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione delle spese generali", "Conto per la contabilizzazione delle spese di imballo"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione della cassa previdenza", "Conto di contabilizzazione rivalsa spese trasporto"))
        .DoRTCalc(4,42,.t.)
        if .o_Cocauspl<>.w_Cocauspl
            .w_COCONSPL = Space(15)
          .link_1_59('Full')
        endif
        .oPgFrm.Page1.oPag.Riferime.Calculate(IIF(IsAlt(),Ah_MsgFormat("Prestazioni per riferimenti:"),Ah_MsgFormat("Articolo per riferimenti:")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(44,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Imballo.Calculate(IIF(IsAlt(),Ah_MsgFormat("Spese generali:"),Ah_MsgFormat("Rivalsa imballo:")))
        .oPgFrm.Page1.oPag.Trasporto.Calculate(IIF(IsAlt(),Ah_MsgFormat("Cassa previdenza:"),Ah_MsgFormat("Rivalsa trasporto:")))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione delle spese generali", "Conto per la contabilizzazione delle spese di imballo"))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate(IIF(IsAlt(), "Conto per la contabilizzazione della cassa previdenza", "Conto di contabilizzazione rivalsa spese trasporto"))
        .oPgFrm.Page1.oPag.Riferime.Calculate(IIF(IsAlt(),Ah_MsgFormat("Prestazioni per riferimenti:"),Ah_MsgFormat("Articolo per riferimenti:")))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOCONINC_1_4.enabled = this.oPgFrm.Page1.oPag.oCOCONINC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.enabled = this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.enabled = this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.enabled = this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.enabled = this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.enabled = this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCOCONSPL_1_59.enabled = this.oPgFrm.Page1.oPag.oCOCONSPL_1_59.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOCONINC_1_4.visible=!this.oPgFrm.Page1.oPag.oCOCONINC_1_4.mHide()
    this.oPgFrm.Page1.oPag.oDESINC_1_5.visible=!this.oPgFrm.Page1.oPag.oDESINC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.visible=!this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCOCOIIMB_1_9.visible=!this.oPgFrm.Page1.oPag.oCOCOIIMB_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCOCOITRA_1_12.visible=!this.oPgFrm.Page1.oPag.oCOCOITRA_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.visible=!this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESBOL_1_14.visible=!this.oPgFrm.Page1.oPag.oDESBOL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.visible=!this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.visible=!this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.visible=!this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCOARTDES_1_35.visible=!this.oPgFrm.Page1.oPag.oCOARTDES_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCOARTDES_1_36.visible=!this.oPgFrm.Page1.oPag.oCOARTDES_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_48.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Imballo.Event(cEvent)
      .oPgFrm.Page1.oPag.Trasporto.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.Riferime.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONINC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONINC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONINC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONINC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONINC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONINC_1_4'),i_cWhere,'GSAR_BZC',"Conti spese incasso",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONINC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONINC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONINC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINC = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONINC = space(15)
      endif
      this.w_DESINC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONINC = space(15)
        this.w_DESINC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"+ ",link_1_4.ANDTOBSO as ANDTOBSO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on CONTROPA.COCONINC=link_1_4.ANCODICE"+" and CONTROPA.COTIPCON=link_1_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and CONTROPA.COCONINC=link_1_4.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOIINC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOIINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOIINC)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOIINC))
          select IVCODIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOIINC)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOIINC) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOIINC_1_6'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOIINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOIINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOIINC)
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOIINC = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOIINC = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOIINC = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOIINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.IVCODIVA as IVCODIVA106"+ ",link_1_6.IVDTOBSO as IVDTOBSO106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on CONTROPA.COCOIINC=link_1_6.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and CONTROPA.COCOIINC=link_1_6.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONIMB
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONIMB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONIMB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONIMB))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONIMB)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONIMB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONIMB)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONIMB) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONIMB_1_7'),i_cWhere,'GSAR_BZC',"Conti spese imballo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONIMB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONIMB);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONIMB)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONIMB = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIMB = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONIMB = space(15)
      endif
      this.w_DESIMB = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONIMB = space(15)
        this.w_DESIMB = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONIMB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.ANCODICE as ANCODICE107"+ ",link_1_7.ANDESCRI as ANDESCRI107"+ ",link_1_7.ANDTOBSO as ANDTOBSO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on CONTROPA.COCONIMB=link_1_7.ANCODICE"+" and CONTROPA.COTIPCON=link_1_7.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and CONTROPA.COCONIMB=link_1_7.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_7.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOIIMB
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOIIMB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOIIMB)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOIIMB))
          select IVCODIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOIIMB)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOIIMB) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOIIMB_1_9'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOIIMB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOIIMB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOIIMB)
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOIIMB = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOIIMB = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOIIMB = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOIIMB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.IVCODIVA as IVCODIVA109"+ ",link_1_9.IVDTOBSO as IVDTOBSO109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on CONTROPA.COCOIIMB=link_1_9.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and CONTROPA.COCOIIMB=link_1_9.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONTRA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONTRA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONTRA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONTRA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONTRA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONTRA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONTRA_1_10'),i_cWhere,'GSAR_BZC',"Conti spese trasporto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONTRA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONTRA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONTRA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESTRA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONTRA = space(15)
      endif
      this.w_DESTRA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONTRA = space(15)
        this.w_DESTRA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.ANCODICE as ANCODICE110"+ ",link_1_10.ANDESCRI as ANDESCRI110"+ ",link_1_10.ANDTOBSO as ANDTOBSO110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CONTROPA.COCONTRA=link_1_10.ANCODICE"+" and CONTROPA.COTIPCON=link_1_10.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CONTROPA.COCONTRA=link_1_10.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_10.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOITRA
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOITRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOITRA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOITRA))
          select IVCODIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOITRA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOITRA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOITRA_1_12'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOITRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOITRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOITRA)
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOITRA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOITRA = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOITRA = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOITRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.IVCODIVA as IVCODIVA112"+ ",link_1_12.IVDTOBSO as IVDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on CONTROPA.COCOITRA=link_1_12.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and CONTROPA.COCOITRA=link_1_12.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONBOL
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONBOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONBOL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONBOL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONBOL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONBOL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONBOL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONBOL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONBOL_1_13'),i_cWhere,'GSAR_BZC',"Conti spese bolli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONBOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONBOL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONBOL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONBOL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESBOL = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONBOL = space(15)
      endif
      this.w_DESBOL = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCONBOL = space(15)
        this.w_DESBOL = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONBOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ANCODICE as ANCODICE113"+ ",link_1_13.ANDESCRI as ANDESCRI113"+ ",link_1_13.ANDTOBSO as ANDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on CONTROPA.COCONBOL=link_1_13.ANCODICE"+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and CONTROPA.COCONBOL=link_1_13.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOIBOL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOIBOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOIBOL)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOIBOL))
          select IVCODIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOIBOL)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOIBOL) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOIBOL_1_15'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR_ZVE.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOIBOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOIBOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOIBOL)
            select IVCODIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOIBOL = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVB = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOIBOL = space(5)
      endif
      this.w_PERIVB = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PERIVB=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOIBOL = space(5)
        this.w_PERIVB = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOIBOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.IVCODIVA as IVCODIVA115"+ ",link_1_15.IVPERIVA as IVPERIVA115"+ ",link_1_15.IVDTOBSO as IVDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CONTROPA.COCOIBOL=link_1_15.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CONTROPA.COCOIBOL=link_1_15.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONCAU
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONCAU)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONCAU))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONCAU)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCONCAU) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONCAU_1_16'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONCAU);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONCAU)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONCAU = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCAU = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCONCAU = space(15)
      endif
      this.w_DESCAU = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"+ ",link_1_16.ANDTOBSO as ANDTOBSO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CONTROPA.COCONCAU=link_1_16.ANCODICE"+" and CONTROPA.COTIPCON=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CONTROPA.COCONCAU=link_1_16.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOICAU
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOICAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOICAU)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOICAU))
          select IVCODIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOICAU)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOICAU) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOICAU_1_18'),i_cWhere,'GSAR_AIV',"Voci IVA",'GSAR_ZVE.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOICAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOICAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOICAU)
            select IVCODIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOICAU = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVC = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOICAU = space(5)
      endif
      this.w_PERIVC = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PERIVC=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOICAU = space(5)
        this.w_PERIVC = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOICAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.IVCODIVA as IVCODIVA118"+ ",link_1_18.IVPERIVA as IVPERIVA118"+ ",link_1_18.IVDTOBSO as IVDTOBSO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CONTROPA.COCOICAU=link_1_18.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CONTROPA.COCOICAU=link_1_18.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCACINC
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCACINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCACINC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCACINC))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCACINC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCACINC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCACINC_1_30'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCACINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCACINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCACINC)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCACINC = NVL(_Link_.CCCODICE,space(5))
      this.w_DCACINC = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TR122 = NVL(_Link_.CCTIPREG,space(1))
      this.w_FP2 = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCACINC = space(5)
      endif
      this.w_DCACINC = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TR122 = space(1)
      this.w_FP2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TR122='N'  AND (.w_FP2<>'N' OR g_PERPAR<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o no partite o obsoleta o di tipo IVA")
        endif
        this.w_COCACINC = space(5)
        this.w_DCACINC = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TR122 = space(1)
        this.w_FP2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCACINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.CCCODICE as CCCODICE130"+ ",link_1_30.CCDESCRI as CCDESCRI130"+ ",link_1_30.CCDTOBSO as CCDTOBSO130"+ ",link_1_30.CCTIPREG as CCTIPREG130"+ ",link_1_30.CCFLPART as CCFLPART130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on CONTROPA.COCACINC=link_1_30.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and CONTROPA.COCACINC=link_1_30.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOCINC
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOCINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCOCINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCOCINC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOCINC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCOCINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCOCINC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCOCINC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCOCINC_1_32'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOCINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCOCINC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCOCINC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOCINC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINC = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOCINC = space(15)
      endif
      this.w_DESCINC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCOCINC = space(15)
        this.w_DESCINC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOCINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.ANCODICE as ANCODICE132"+ ",link_1_32.ANDESCRI as ANDESCRI132"+ ",link_1_32.ANDTOBSO as ANDTOBSO132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on CONTROPA.COCOCINC=link_1_32.ANCODICE"+" and CONTROPA.COTIPCON=link_1_32.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and CONTROPA.COCOCINC=link_1_32.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_32.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COARTDES
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COARTDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COARTDES)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COARTDES))
          select CACODICE,CATIPCON,CADTOBSO,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COARTDES)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COARTDES) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOARTDES_1_35'),i_cWhere,'GSMA_ACA',"Prestazioni descrittive",'INSPRE.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CATIPCON,CADTOBSO,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COARTDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COARTDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COARTDES)
            select CACODICE,CATIPCON,CADTOBSO,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COARTDES = NVL(_Link_.CACODICE,space(20))
      this.w_TIPART = NVL(_Link_.CATIPCON,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COARTDES = space(20)
      endif
      this.w_TIPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_COARTDES) OR .w_TIPART='D') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice prestazione incongruente! (Non descrittiva)")
        endif
        this.w_COARTDES = space(20)
        this.w_TIPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COARTDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.CACODICE as CACODICE135"+ ",link_1_35.CATIPCON as CATIPCON135"+ ",link_1_35.CADTOBSO as CADTOBSO135"+ ",link_1_35.CADESART as CADESART135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on CONTROPA.COARTDES=link_1_35.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and CONTROPA.COARTDES=link_1_35.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COARTDES
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COARTDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COARTDES)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COARTDES))
          select CACODICE,CATIPCON,CADTOBSO,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COARTDES)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COARTDES) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOARTDES_1_36'),i_cWhere,'GSMA_ACA',"Articoli descrittivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CATIPCON,CADTOBSO,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COARTDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CATIPCON,CADTOBSO,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COARTDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COARTDES)
            select CACODICE,CATIPCON,CADTOBSO,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COARTDES = NVL(_Link_.CACODICE,space(20))
      this.w_TIPART = NVL(_Link_.CATIPCON,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COARTDES = space(20)
      endif
      this.w_TIPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_COARTDES) OR .w_TIPART='D') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente! (Non descrittivo)")
        endif
        this.w_COARTDES = space(20)
        this.w_TIPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COARTDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.CACODICE as CACODICE136"+ ",link_1_36.CATIPCON as CATIPCON136"+ ",link_1_36.CADTOBSO as CADTOBSO136"+ ",link_1_36.CADESART as CADESART136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on CONTROPA.COARTDES=link_1_36.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and CONTROPA.COARTDES=link_1_36.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUVEO
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUVEO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUVEO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUVEO))
          select CCCODICE,CCDESCRI,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUVEO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUVEO)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUVEO)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUVEO) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUVEO_1_43'),i_cWhere,'GSCG_ACC',"",'GSAR_AO1.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUVEO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUVEO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUVEO)
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUVEO = NVL(_Link_.CCCODICE,space(5))
      this.w_CVDES = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREGA = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUVEO = space(5)
      endif
      this.w_CVDES = space(35)
      this.w_TIPREGA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREGA='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o con IVA")
        endif
        this.w_COCAUVEO = space(5)
        this.w_CVDES = space(35)
        this.w_TIPREGA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUVEO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_43(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_43.CCCODICE as CCCODICE143"+ ",link_1_43.CCDESCRI as CCDESCRI143"+ ",link_1_43.CCTIPREG as CCTIPREG143"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_43 on CONTROPA.COCAUVEO=link_1_43.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_43"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUVEO=link_1_43.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCRITAT
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCRITAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzc',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCRITAT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCRITAT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCRITAT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCRITAT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCRITAT_1_51'),i_cWhere,'gsar_bzc',"Conti erario C/Ritenute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCRITAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCRITAT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCRITAT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCRITAT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRIT = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCRITAT = space(15)
      endif
      this.w_DESRIT = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCRITAT = space(15)
        this.w_DESRIT = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCRITAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_51(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_51.ANCODICE as ANCODICE151"+ ",link_1_51.ANDESCRI as ANDESCRI151"+ ",link_1_51.ANDTOBSO as ANDTOBSO151"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_51 on CONTROPA.COCRITAT=link_1_51.ANCODICE"+" and CONTROPA.COTIPCON=link_1_51.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_51"
          i_cKey=i_cKey+'+" and CONTROPA.COCRITAT=link_1_51.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_51.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUSPL
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUSPL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUSPL)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUSPL))
          select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUSPL)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCAUSPL) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUSPL_1_58'),i_cWhere,'GSCG_ACC',"CAUSALI CONTABILI",'CAUCOEFASAL.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUSPL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUSPL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUSPL)
            select CCCODICE,CCDESCRI,CCTIPREG,CCDTOBSO,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUSPL = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUSPL = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREGSPL = NVL(_Link_.CCTIPREG,space(1))
      this.w_DATOBSOSPL = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUSPL = space(5)
      endif
      this.w_DESCAUSPL = space(35)
      this.w_TIPREGSPL = space(1)
      this.w_DATOBSOSPL = ctod("  /  /  ")
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOSPL) OR .w_DATOBSOSPL>.w_OBTEST) AND .w_TIPREGSPL='N' and .w_FLPART='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta,di tipo IVA o non salda partite")
        endif
        this.w_COCAUSPL = space(5)
        this.w_DESCAUSPL = space(35)
        this.w_TIPREGSPL = space(1)
        this.w_DATOBSOSPL = ctod("  /  /  ")
        this.w_FLPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUSPL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_58(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_58.CCCODICE as CCCODICE158"+ ",link_1_58.CCDESCRI as CCDESCRI158"+ ",link_1_58.CCTIPREG as CCTIPREG158"+ ",link_1_58.CCDTOBSO as CCDTOBSO158"+ ",link_1_58.CCFLPART as CCFLPART158"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_58 on CONTROPA.COCAUSPL=link_1_58.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_58"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUSPL=link_1_58.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONSPL
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONSPL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONSPL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCONSPL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONSPL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCONSPL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCONSPL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCONSPL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONSPL_1_59'),i_cWhere,'GSAR_API',"CONTI SPLIT PAYMENT",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONSPL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONSPL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCONSPL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONSPL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFINASPY = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSOSPY = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLPARTSPY = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCONSPL = space(15)
      endif
      this.w_DESFINASPY = space(40)
      this.w_DATOBSOSPY = ctod("  /  /  ")
      this.w_FLPARTSPY = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOSPY) OR .w_DATOBSOSPY>.w_OBTEST) AND .w_FLPARTSPY<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COCONSPL = space(15)
        this.w_DESFINASPY = space(40)
        this.w_DATOBSOSPY = ctod("  /  /  ")
        this.w_FLPARTSPY = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONSPL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_59.ANCODICE as ANCODICE159"+ ",link_1_59.ANDESCRI as ANDESCRI159"+ ",link_1_59.ANDTOBSO as ANDTOBSO159"+ ",link_1_59.ANPARTSN as ANPARTSN159"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_59 on CONTROPA.COCONSPL=link_1_59.ANCODICE"+" and CONTROPA.COTIPCON=link_1_59.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_59"
          i_cKey=i_cKey+'+" and CONTROPA.COCONSPL=link_1_59.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_59.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOCONINC_1_4.value==this.w_COCONINC)
      this.oPgFrm.Page1.oPag.oCOCONINC_1_4.value=this.w_COCONINC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINC_1_5.value==this.w_DESINC)
      this.oPgFrm.Page1.oPag.oDESINC_1_5.value=this.w_DESINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.value==this.w_COCOIINC)
      this.oPgFrm.Page1.oPag.oCOCOIINC_1_6.value=this.w_COCOIINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONIMB_1_7.value==this.w_COCONIMB)
      this.oPgFrm.Page1.oPag.oCOCONIMB_1_7.value=this.w_COCONIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMB_1_8.value==this.w_DESIMB)
      this.oPgFrm.Page1.oPag.oDESIMB_1_8.value=this.w_DESIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOIIMB_1_9.value==this.w_COCOIIMB)
      this.oPgFrm.Page1.oPag.oCOCOIIMB_1_9.value=this.w_COCOIIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONTRA_1_10.value==this.w_COCONTRA)
      this.oPgFrm.Page1.oPag.oCOCONTRA_1_10.value=this.w_COCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRA_1_11.value==this.w_DESTRA)
      this.oPgFrm.Page1.oPag.oDESTRA_1_11.value=this.w_DESTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOITRA_1_12.value==this.w_COCOITRA)
      this.oPgFrm.Page1.oPag.oCOCOITRA_1_12.value=this.w_COCOITRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.value==this.w_COCONBOL)
      this.oPgFrm.Page1.oPag.oCOCONBOL_1_13.value=this.w_COCONBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBOL_1_14.value==this.w_DESBOL)
      this.oPgFrm.Page1.oPag.oDESBOL_1_14.value=this.w_DESBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.value==this.w_COCOIBOL)
      this.oPgFrm.Page1.oPag.oCOCOIBOL_1_15.value=this.w_COCOIBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.value==this.w_COCONCAU)
      this.oPgFrm.Page1.oPag.oCOCONCAU_1_16.value=this.w_COCONCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.value==this.w_COCOICAU)
      this.oPgFrm.Page1.oPag.oCOCOICAU_1_18.value=this.w_COCOICAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCACINC_1_30.value==this.w_COCACINC)
      this.oPgFrm.Page1.oPag.oCOCACINC_1_30.value=this.w_COCACINC
    endif
    if not(this.oPgFrm.Page1.oPag.oDCACINC_1_31.value==this.w_DCACINC)
      this.oPgFrm.Page1.oPag.oDCACINC_1_31.value=this.w_DCACINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOCINC_1_32.value==this.w_COCOCINC)
      this.oPgFrm.Page1.oPag.oCOCOCINC_1_32.value=this.w_COCOCINC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINC_1_33.value==this.w_DESCINC)
      this.oPgFrm.Page1.oPag.oDESCINC_1_33.value=this.w_DESCINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOARTDES_1_35.value==this.w_COARTDES)
      this.oPgFrm.Page1.oPag.oCOARTDES_1_35.value=this.w_COARTDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCOARTDES_1_36.value==this.w_COARTDES)
      this.oPgFrm.Page1.oPag.oCOARTDES_1_36.value=this.w_COARTDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_42.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_42.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUVEO_1_43.value==this.w_COCAUVEO)
      this.oPgFrm.Page1.oPag.oCOCAUVEO_1_43.value=this.w_COCAUVEO
    endif
    if not(this.oPgFrm.Page1.oPag.oCVDES_1_44.value==this.w_CVDES)
      this.oPgFrm.Page1.oPag.oCVDES_1_44.value=this.w_CVDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_48.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_48.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCRITAT_1_51.value==this.w_COCRITAT)
      this.oPgFrm.Page1.oPag.oCOCRITAT_1_51.value=this.w_COCRITAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIT_1_52.value==this.w_DESRIT)
      this.oPgFrm.Page1.oPag.oDESRIT_1_52.value=this.w_DESRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUSPL_1_58.value==this.w_COCAUSPL)
      this.oPgFrm.Page1.oPag.oCOCAUSPL_1_58.value=this.w_COCAUSPL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONSPL_1_59.value==this.w_COCONSPL)
      this.oPgFrm.Page1.oPag.oCOCONSPL_1_59.value=this.w_COCONSPL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUSPL_1_62.value==this.w_DESCAUSPL)
      this.oPgFrm.Page1.oPag.oDESCAUSPL_1_62.value=this.w_DESCAUSPL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFINASPY_1_63.value==this.w_DESFINASPY)
      this.oPgFrm.Page1.oPag.oDESFINASPY_1_63.value=this.w_DESFINASPY
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_COCONINC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONINC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_COCOIINC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOIINC_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCONIMB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONIMB_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_COCOIIMB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOIIMB_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONTRA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_COCOITRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOITRA_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_COCONBOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONBOL_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(.w_PERIVB=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_COCOIBOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOIBOL_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(.w_PERIVC=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_COCOICAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOICAU_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   ((empty(.w_COCACINC)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TR122='N'  AND (.w_FP2<>'N' OR g_PERPAR<>'S')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCACINC_1_30.SetFocus()
            i_bnoObbl = !empty(.w_COCACINC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o no partite o obsoleta o di tipo IVA")
          case   ((empty(.w_COCOCINC)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOCINC_1_32.SetFocus()
            i_bnoObbl = !empty(.w_COCOCINC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_COARTDES) OR .w_TIPART='D') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(NOT IsAlt())  and not(empty(.w_COARTDES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOARTDES_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice prestazione incongruente! (Non descrittiva)")
          case   not((EMPTY(.w_COARTDES) OR .w_TIPART='D') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(IsAlt())  and not(empty(.w_COARTDES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOARTDES_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente! (Non descrittivo)")
          case   not(.w_TIPREGA='N')  and not(empty(.w_COCAUVEO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUVEO_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o con IVA")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCRITAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCRITAT_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSOSPL) OR .w_DATOBSOSPL>.w_OBTEST) AND .w_TIPREGSPL='N' and .w_FLPART='S')  and not(empty(.w_COCAUSPL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUSPL_1_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta,di tipo IVA o non salda partite")
          case   ((empty(.w_COCONSPL)) or not((EMPTY(.w_DATOBSOSPY) OR .w_DATOBSOSPY>.w_OBTEST) AND .w_FLPARTSPY<>'S'))  and (Not Empty(.w_COCAUSPL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCONSPL_1_59.SetFocus()
            i_bnoObbl = !empty(.w_COCONSPL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COCAUSPL = this.w_COCAUSPL
    return

enddefine

* --- Define pages as container
define class tgsar_ao1Pag1 as StdContainer
  Width  = 745
  height = 360
  stdWidth  = 745
  stdheight = 360
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCONINC_1_4 as StdField with uid="UWYJBLSHVS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COCONINC", cQueryName = "COCONINC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione rivalsa spese incasso",;
    HelpContextID = 206959511,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=15, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONINC"

  func oCOCONINC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCONINC_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCONINC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONINC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONINC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONINC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti spese incasso",'',this.parent.oContained
  endproc
  proc oCOCONINC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONINC
     i_obj.ecpSave()
  endproc

  add object oDESINC_1_5 as StdField with uid="SZTGOORAYQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESINC", cQueryName = "DESINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39517642,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=15, InputMask=replicate('X',40)

  func oDESINC_1_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCOCOIINC_1_6 as StdField with uid="UUIRPEYKOU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COCOIINC", cQueryName = "COCOIINC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Eventuale codice IVA da applicare alle spese di incasso",;
    HelpContextID = 212202391,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=677, Top=15, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOIINC"

  func oCOCOIINC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCOIINC_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCOIINC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOIINC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOIINC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOIINC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCOCOIINC_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOIINC
     i_obj.ecpSave()
  endproc

  add object oCOCONIMB_1_7 as StdField with uid="ZDFMRBPUDV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COCONIMB", cQueryName = "COCONIMB",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione rivalsa spese imballo",;
    HelpContextID = 206959512,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=43, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONIMB"

  func oCOCONIMB_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONIMB_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONIMB_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONIMB_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti spese imballo",'',this.parent.oContained
  endproc
  proc oCOCONIMB_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONIMB
     i_obj.ecpSave()
  endproc

  add object oDESIMB_1_8 as StdField with uid="XYPYNUAAZC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESIMB", cQueryName = "DESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57343434,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=43, InputMask=replicate('X',40)

  add object oCOCOIIMB_1_9 as StdField with uid="AUJOUUEZCL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COCOIIMB", cQueryName = "COCOIIMB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Eventuale codice IVA da applicare alle spese di imballo",;
    HelpContextID = 212202392,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=677, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOIIMB"

  func oCOCOIIMB_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCOIIMB_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOIIMB_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOIIMB_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOIIMB_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCOCOIIMB_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOIIMB
     i_obj.ecpSave()
  endproc

  add object oCOCONTRA_1_10 as StdField with uid="APXXZTWLBW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COCONTRA", cQueryName = "COCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione rivalsa spese trasporto",;
    HelpContextID = 22410137,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=71, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONTRA"

  func oCOCONTRA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONTRA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONTRA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONTRA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti spese trasporto",'',this.parent.oContained
  endproc
  proc oCOCONTRA_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONTRA
     i_obj.ecpSave()
  endproc

  add object oDESTRA_1_11 as StdField with uid="UGPKPOWDRQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESTRA", cQueryName = "DESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68156874,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=71, InputMask=replicate('X',40)

  add object oCOCOITRA_1_12 as StdField with uid="OHDVEFCSPR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCOITRA", cQueryName = "COCOITRA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Eventuale codice IVA da applicare alle spese di trasporto",;
    HelpContextID = 27653017,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=677, Top=71, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOITRA"

  func oCOCOITRA_1_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCOITRA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOITRA_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOITRA_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOITRA_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCOCOITRA_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOITRA
     i_obj.ecpSave()
  endproc

  add object oCOCONBOL_1_13 as StdField with uid="LCTSAHQWJH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COCONBOL", cQueryName = "COCONBOL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto di contabilizzazione rivalsa spese bolli",;
    HelpContextID = 55964558,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=99, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONBOL"

  func oCOCONBOL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCONBOL_1_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCONBOL_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONBOL_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONBOL_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONBOL_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti spese bolli",'',this.parent.oContained
  endproc
  proc oCOCONBOL_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONBOL
     i_obj.ecpSave()
  endproc

  add object oDESBOL_1_14 as StdField with uid="CZVIZLNHKI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESBOL", cQueryName = "DESBOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156368330,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=99, InputMask=replicate('X',40)

  func oDESBOL_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCOCOIBOL_1_15 as StdField with uid="NWBILEVFQZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_COCOIBOL", cQueryName = "COCOIBOL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA escluso art. 15 da applicare sull'importo dei bolli",;
    HelpContextID = 61207438,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=677, Top=99, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOIBOL"

  func oCOCOIBOL_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCOIBOL_1_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCOIBOL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOIBOL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOIBOL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOIBOL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR_ZVE.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oCOCOIBOL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOIBOL
     i_obj.ecpSave()
  endproc

  add object oCOCONCAU_1_16 as StdField with uid="UWSHLKWSKJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCONCAU", cQueryName = "COCONCAU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita per cauzioni imballi. Utilizzata in alternativa a quella specificata sul cliente/fornitore",;
    HelpContextID = 39187333,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=127, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONCAU"

  func oCOCONCAU_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCONCAU_1_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCONCAU_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONCAU_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONCAU_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONCAU_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'',this.parent.oContained
  endproc
  proc oCOCONCAU_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONCAU
     i_obj.ecpSave()
  endproc

  add object oCOCOICAU_1_18 as StdField with uid="LMFPSTCFEP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_COCOICAU", cQueryName = "COCOICAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA escluso art. 15 da applicare sull'importo delle cauzioni",;
    HelpContextID = 44430213,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=677, Top=127, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOICAU"

  func oCOCOICAU_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCOCOICAU_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOCOICAU_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOICAU_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOICAU_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOICAU_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'GSAR_ZVE.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oCOCOICAU_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOICAU
     i_obj.ecpSave()
  endproc

  add object oCOCACINC_1_30 as StdField with uid="DRZUJWFPYD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COCACINC", cQueryName = "COCACINC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o no partite o obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile per la contabilizzazione degli acconti incassati",;
    HelpContextID = 219411351,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=181, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCACINC"

  func oCOCACINC_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCACINC_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCACINC_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCACINC_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCACINC_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCACINC
     i_obj.ecpSave()
  endproc

  add object oDCACINC_1_31 as StdField with uid="WYIJDSSGYB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DCACINC", cQueryName = "DCACINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 139321398,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=247, Top=156, InputMask=replicate('X',35)

  add object oCOCOCINC_1_32 as StdField with uid="ZOKEFXZHPT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_COCOCINC", cQueryName = "COCOCINC",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto cassa o banca per contabilizzazione acconti incassati",;
    HelpContextID = 218493847,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=182, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCOCINC"

  func oCOCOCINC_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOCINC_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOCINC_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCOCINC_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oCOCOCINC_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCOCINC
     i_obj.ecpSave()
  endproc

  add object oDESCINC_1_33 as StdField with uid="XFJMNQGQIO",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCINC", cQueryName = "DESCINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 139395638,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=182, InputMask=replicate('X',40)

  add object oCOARTDES_1_35 as StdField with uid="LYPIAVJJCJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_COARTDES", cQueryName = "COARTDES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice prestazione incongruente! (Non descrittiva)",;
    ToolTipText = "Codice prestazione descrittiva usata per riferimenti generici (import doc.)",;
    HelpContextID = 252505209,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=181, Top=208, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_COARTDES"

  func oCOARTDES_1_35.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  func oCOARTDES_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOARTDES_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOARTDES_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOARTDES_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Prestazioni descrittive",'INSPRE.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCOARTDES_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_COARTDES
     i_obj.ecpSave()
  endproc

  add object oCOARTDES_1_36 as StdField with uid="DLFMAFHCPZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_COARTDES", cQueryName = "COARTDES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente! (Non descrittivo)",;
    ToolTipText = "Codice articolo di tipo descrittivo usato per riferimenti generici (import doc.)",;
    HelpContextID = 252505209,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=181, Top=208, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_COARTDES"

  func oCOARTDES_1_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCOARTDES_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOARTDES_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOARTDES_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOARTDES_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Articoli descrittivi",'',this.parent.oContained
  endproc
  proc oCOARTDES_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_COARTDES
     i_obj.ecpSave()
  endproc

  add object oDESART_1_42 as StdField with uid="YTUYISKPGM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 19070410,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=342, Top=208, InputMask=replicate('X',40)

  add object oCOCAUVEO_1_43 as StdField with uid="PBXKCBVZWH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_COCAUVEO", cQueryName = "COCAUVEO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o con IVA",;
    ToolTipText = "Causale contabile per contabilizzazione omaggi (conto e contropartita)",;
    HelpContextID = 17566837,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=181, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUVEO"

  func oCOCAUVEO_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUVEO_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUVEO_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUVEO_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"",'GSAR_AO1.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUVEO_1_43.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUVEO
     i_obj.ecpSave()
  endproc

  add object oCVDES_1_44 as StdField with uid="JHENITNZVX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CVDES", cQueryName = "CVDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84925658,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=247, Top=234, InputMask=replicate('X',35)

  add object oDESCAU_1_48 as StdField with uid="CNULRBPAQA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 19987914,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=127, InputMask=replicate('X',40)

  func oDESCAU_1_48.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCOCRITAT_1_51 as StdField with uid="QBZXUECLHM",rtseq=40,rtrep=.f.,;
    cFormVar = "w_COCRITAT", cQueryName = "COCRITAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Contropartita utilizzata nella contabilizzazione per la rilevazione delle ritenute attive",;
    HelpContextID = 27456390,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=181, Top=260, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_bzc", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCRITAT"

  func oCOCRITAT_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCRITAT_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCRITAT_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCRITAT_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzc',"Conti erario C/Ritenute",'',this.parent.oContained
  endproc
  proc oCOCRITAT_1_51.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCRITAT
     i_obj.ecpSave()
  endproc

  add object oDESRIT_1_52 as StdField with uid="MLFSHLQIAI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESRIT", cQueryName = "DESRIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 27393482,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=327, Top=260, InputMask=replicate('X',40)


  add object Imballo as cp_calclbl with uid="QMNDVOPBBA",left=11, top=43, width=168,height=25,;
    caption='Imballo',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,Alignment=1,;
    nPag=1;
    , HelpContextID = 123856250


  add object Trasporto as cp_calclbl with uid="PQYCPYPRBD",left=11, top=71, width=168,height=25,;
    caption='Trasporto',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,Alignment=1,;
    nPag=1;
    , HelpContextID = 200284058


  add object oObj_1_56 as cp_setobjprop with uid="WBUOCTVRDO",left=98, top=373, width=209,height=21,;
    caption='ToolTip di w_COCONIMB',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_COCONIMB",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 33898441


  add object oObj_1_57 as cp_setobjprop with uid="TDFWHHWZQS",left=98, top=394, width=209,height=21,;
    caption='ToolTip di w_COCONTRA',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_COCONTRA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 33222601

  add object oCOCAUSPL_1_58 as StdField with uid="LNNTILMSSQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_COCAUSPL", cQueryName = "COCAUSPL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta,di tipo IVA o non salda partite",;
    ToolTipText = "Causale contabile di riga per giroconto in contabilizzazione fattura split payment",;
    HelpContextID = 32764814,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=181, Top=290, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUSPL"

  func oCOCAUSPL_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUSPL_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUSPL_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUSPL_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"CAUSALI CONTABILI",'CAUCOEFASAL.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUSPL_1_58.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUSPL
     i_obj.ecpSave()
  endproc

  add object oCOCONSPL_1_59 as StdField with uid="JCGMHXNXBJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_COCONSPL", cQueryName = "COCONSPL",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto contabile di storno in contabilizzazione fattura split payment",;
    HelpContextID = 39187342,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=181, Top=320, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONSPL"

  func oCOCONSPL_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_COCAUSPL))
    endwith
   endif
  endfunc

  func oCOCONSPL_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONSPL_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONSPL_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONSPL_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"CONTI SPLIT PAYMENT",'',this.parent.oContained
  endproc
  proc oCOCONSPL_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONSPL
     i_obj.ecpSave()
  endproc

  add object oDESCAUSPL_1_62 as StdField with uid="NEQRCOPNQX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCAUSPL", cQueryName = "DESCAUSPL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248448838,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=247, Top=290, InputMask=replicate('X',35)

  add object oDESFINASPY_1_63 as StdField with uid="AKUGLHVVHE",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESFINASPY", cQueryName = "DESFINASPY",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 128819063,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=320, InputMask=replicate('X',40)


  add object Riferime as cp_calclbl with uid="YVKFEZJWKL",left=11, top=208, width=168,height=25,;
    caption='Riferimenti',;
   bGlobalFont=.t.,;
    fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,fontname="Arial",caption="label text",fontsize=9,Alignment=1,;
    nPag=1;
    , HelpContextID = 167157157

  add object oStr_1_19 as StdString with uid="VYSNUTIEFH",Visible=.t., Left=619, Top=15,;
    Alignment=1, Width=56, Height=15,;
    Caption="Cod.IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="TKOJQUQJBL",Visible=.t., Left=619, Top=43,;
    Alignment=1, Width=56, Height=15,;
    Caption="Cod.IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="URFWKYGZJJ",Visible=.t., Left=619, Top=71,;
    Alignment=1, Width=56, Height=15,;
    Caption="Cod.IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="CBLAITWPGK",Visible=.t., Left=619, Top=99,;
    Alignment=1, Width=56, Height=15,;
    Caption="Cod.IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="FPWGYFIKRY",Visible=.t., Left=7, Top=182,;
    Alignment=1, Width=172, Height=15,;
    Caption="Conto acconti incassati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="RWHOHKMIBF",Visible=.t., Left=7, Top=156,;
    Alignment=1, Width=172, Height=15,;
    Caption="Causale acconti incassati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="INPYZEEPTC",Visible=.t., Left=7, Top=15,;
    Alignment=1, Width=172, Height=15,;
    Caption="Rivalsa incasso:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="HIDQKIQFIH",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=172, Height=15,;
    Caption="Rivalsa bolli:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="HHZXVYGJVA",Visible=.t., Left=7, Top=234,;
    Alignment=1, Width=172, Height=18,;
    Caption="Causale omaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="AFNMHIBMXY",Visible=.t., Left=7, Top=128,;
    Alignment=1, Width=172, Height=18,;
    Caption="Contropartita per cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="QDRQXXHGZB",Visible=.t., Left=619, Top=127,;
    Alignment=1, Width=56, Height=15,;
    Caption="Cod.IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="WSFISQMLUO",Visible=.t., Left=7, Top=260,;
    Alignment=1, Width=172, Height=18,;
    Caption="Ritenute C/erario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="CHBANGIRHT",Visible=.t., Left=22, Top=291,;
    Alignment=1, Width=157, Height=18,;
    Caption="Caus. storno split payment:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="VGAXPHALOU",Visible=.t., Left=23, Top=321,;
    Alignment=1, Width=156, Height=18,;
    Caption="Conto storno split payment:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao1','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
