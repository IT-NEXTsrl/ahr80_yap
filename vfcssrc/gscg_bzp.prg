* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bzp                                                        *
*              Eliminazione primanota                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_73]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-21                                                      *
* Last revis.: 2011-03-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pSerial
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bzp",oParentObject,m.pTipo,m.pSerial)
return(i_retval)

define class tgscg_bzp as StdBatch
  * --- Local variables
  pTipo = space(10)
  pSerial = space(10)
  w_SERIAL = space(10)
  w_PROG = .NULL.
  w_MESS = space(10)
  w_RIFER = .NULL.
  w_PTNUMPAR = space(31)
  w_PTFLSOSP = space(1)
  w_PTSERIAL = space(10)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTBANAPP = space(10)
  w_PTROWORD = 0
  w_PTTIPCON = space(1)
  w_PTBANNOS = space(15)
  w_CPROWNUM = 0
  w_PTCODCON = space(15)
  w_PTFLRAGG = space(1)
  w_PTIMPDOC = 0
  w_PT_SEGNO = space(1)
  w_PTFLRITE = 0
  w_PTNUMEFF = 0
  w_PTTOTIMP = 0
  w_PTTOTABB = 0
  w_PTFLINDI = space(1)
  w_PTCODVAL = space(3)
  w_PTFLCRSA = space(1)
  w_PTCAOVAL = 0
  w_PTFLIMPE = space(1)
  w_PTCAOAPE = 0
  w_PTNUMDIS = space(10)
  w_PTDATAPE = ctod("  /  /  ")
  w_PTMODPAG = space(10)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue il Programma di Primanota dalla Manutenzione Contabilizzazione Indiretta (da GSCG_ACI)
    * --- chiave della Prima Nota
    * --- ELI
    *     Eliminazione con apertura Primanota
    *     ELA
    *     Eliminazione massiva senza aperura primanota
    *     
    if EMPTY(this.pSERIAL) 
      ah_ErrorMsg("Non esiste registrazione contabile associata",,"")
      i_retcode = 'stop'
      return
    endif
    VVP = 20 * g_PERPVL
    * --- Questo oggetto sar� definito come Prima Nota 
    * --- PrimaNota
    this.w_SERIAL = this.pSERIAL
    this.w_PROG = GSCG_MPN()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- inizializzo la chiave della Prima Nota
    this.w_PROG.w_PNSERIAL = this.w_SERIAL
    * --- creo il curosre delle solo chiavi
    this.w_PROG.QueryKeySet("PNSERIAL='"+this.w_SERIAL+ "'","")     
    * --- mi metto in interrogazione
    this.w_PROG.LoadRecWarn()     
    if ! Isahe()
      * --- Questo assegnamento serve per aggiornare lo zoom al record inserted della primanota
      this.w_PROG.w_RIFGSCGACI = This.OparentObject
      this.w_RIFER = This.OparentObject
    endif
    * --- Verifico se posso eliminare la registrazione
    if this.w_PROG.HAsCpEvents("EcpDelete")
      this.w_PROG.ECPDELETE()     
      if this.pTipo="ELI" and .F.
        if Type("This.w_RIFER")="O" and upper(this.w_RIFER.class)="TGSCG_MSC"
          * --- Riattivo Toolbar Nella Gestione di Origine
           
 i_curform = this.oparentobject 
 this.oparentobject.Active() 
 this.oparentobject.Ecpquery()
        endif
      endif
    endif
    if Vartype(this.w_PROG)="O"
      this.w_PROG.ecpQuit()     
    endif
    this.w_PROG = .Null.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili
  endproc


  proc Init(oParentObject,pTipo,pSerial)
    this.pTipo=pTipo
    this.pSerial=pSerial
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pSerial"
endproc
