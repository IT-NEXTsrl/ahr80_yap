* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b01                                                        *
*              Elabora coge ratei/risc. x S.a                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_214]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-12                                                      *
* Last revis.: 2010-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b01",oParentObject)
return(i_retval)

define class tgscg_b01 as StdBatch
  * --- Local variables
  w_IMPTOT = 0
  w_RTOT = 0
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_CODBUN = space(3)
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_RATEO = 0
  w_COD003 = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_RISATT = space(15)
  w_RECODICE = space(15)
  w_DATREG = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_RATATT = space(15)
  w_RISPAS = space(15)
  w_REDESCRI = space(45)
  w_CONSUP = space(15)
  w_RATPAS = space(15)
  w_TIPSEZ = space(1)
  w_SERIAL = space(10)
  w_RAGGSCRI = space(1)
  w_DESCRI = space(50)
  w_PNSERIAL = space(10)
  w_CPROWNUM = 0
  w_TOTPAR = 0
  w_MRTOTIMP = 0
  w_CODCEN = space(15)
  w_VOCCEN = space(15)
  w_CODCOM = space(15)
  w_DATCIN = ctod("  /  /  ")
  w_DATCFI = ctod("  /  /  ")
  w_PARAMETRO = 0
  w_TOTIMP = 0
  w_CONTA = 0
  w_NUMERO = 0
  w_DATAVUOTA = ctod("  /  /  ")
  w_RIFDOC = space(10)
  w_ALMFAT = space(1)
  w_SERRIF = space(10)
  w_RA_INICOM = ctod("  /  /  ")
  w_RA_FINCOM = ctod("  /  /  ")
  * --- WorkFile variables
  CONTI_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Genera Assestamento (da GSCG_BAT)
    *     Utilizzato per la generazione dei Ratei e dei Risconti.
    this.w_FILINI = this.oParentObject.oParentObject.w_ASDATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_ASDATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_CORISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_CORISPAS
    this.w_RATATT = this.oParentObject.oParentObject.w_CORATATT
    this.w_RATPAS = this.oParentObject.oParentObject.w_CORATPAS
    this.w_RAGGSCRI = this.oParentObject.oParentObject.w_RAGGSCRI
    this.w_DATAVUOTA = cp_CharToDate("  -  -  ")
    if USED("EXTCG001")
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), CODBUN C(3), TOTIMP N(18,4), ;
      INICOM D(8), FINCOM D(8), SERIAL C(10), DESCRI C(50),PNSERIAL C(10),CPROWNUM N(5,0),NUMERO N(4))
      * --- Creo un cursore di appoggio per la gestione dell'importo contabile.
      Select Pnserial as Pnserial,Cprownum as Cprownum,Max(Nvl(Analnum,0)) as Analnum,Tipcon as Tipcon,Codice as Codice,Codbun as Codbun,Inicom as Inicom,Fincom as Fincom,; 
 Valnaz as Valnaz,Datreg as Datreg,Max(Datdoc) as Datdoc,Max(Impdar) as Impdar,Max(Impave) as Impave,Max(Serial) as Serial,; 
 Max (Descri) as Descri,Max(Descri1) as Descri1,Max(Pnrifdoc) as Pnrifdoc from EXTCG001 into cursor Appoggio; 
 group by Pnserial,Cprownum,Tipcon,Codice,Codbun,Inicom,Fincom,Valnaz,Datreg order by Pnserial,Tipcon,Codice,Codbun,Inicom,Fincom,Valnaz,Datreg
      SELECT APPOGGIO
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "G" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_CODICE = CODICE
      this.w_PNSERIAL = PNSERIAL
      this.w_CPROWNUM = CPROWNUM
      this.w_NUMERO = Nvl(Analnum,0)
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_CODBUN = NVL(CODBUN, g_CODBUN)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_DATDOC = CP_TODATE(DATDOC)
      this.w_RIFDOC = Nvl(PNRIFDOC,Space(10))
      this.w_RATEO = 0
      this.w_RISCON = 0
      if this.w_RAGGSCRI="S"
        this.w_SERIAL = SPACE(10)
        this.w_DESCRI = SPACE(50)
      else
        this.w_SERIAL = NVL(SERIAL, "")
        * --- Controllo se � stata specificata la data documento sul movimento di origine
        if NOT EMPTY(this.w_DATDOC)
          this.w_DESCRI = ALLTRIM(NVL(DESCRI1,""))+" Del "+DTOC(this.w_DATDOC)
        else
          this.w_DESCRI = ALLTRIM(NVL(DESCRI,""))+" Del "+DTOC(this.w_DATREG)
        endif
      endif
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, I_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, GETCAM(g_PERVAL,I_DATSYS), i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      * --- 2^OPERAZIONE: Se presente una Competenza, calcola l'esatto Importo e eventuali Risconti
      if NOT EMPTY(this.w_INICOM) AND NOT EMPTY(this.w_FINCOM)
        if this.w_FINCOM<this.w_FILINI OR (this.w_INICOM>this.w_FILFIN AND NOT this.oParentObject.w_ONLYRatRis)
          * --- Registrazione Totalmente fuori dal Periodo, Ignora e passa al record successivo
          LOOP
        else
          this.w_GIOCOM = (this.w_FINCOM+1) - this.w_INICOM
          * --- Devo fare il rateo nei suegenti casi:
          *     ===========================
          *     1) Primanota manuale
          *     2) Primanota da contabilizzazione riferita a documento che non evade
          *     3) Primanota che evade, dove tutte le righe che evadono hanno tipologia 
          *         documento per generazione scritture di assestamento a nessuno
          this.w_SERRIF = Space(10)
          this.w_ALMFAT = "X"
          if Not Empty(this.w_RIFDOC)
            * --- Select from sasfa004
            do vq_exec with 'sasfa004',this,'_Curs_sasfa004','',.f.,.t.
            if used('_Curs_sasfa004')
              select _Curs_sasfa004
              locate for 1=1
              do while not(eof())
              this.w_ALMFAT = _Curs_sasfa004.ALMFAT
              this.w_SERRIF = _Curs_sasfa004.MVSERRIF
                select _Curs_sasfa004
                continue
              enddo
              use
            endif
          endif
          if Empty(this.w_RIFDOC) Or this.w_ALMFAT="N" Or Empty(this.w_SERRIF)
            * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
            * --- RATEO
            if (this.w_FINCOM>=this.w_FILINI OR this.w_INICOM<=this.w_FILFIN) AND this.w_GIOCOM<>0 AND this.w_FILFIN<this.w_DATREG
              this.w_GIOPER = MIN(this.w_FILFIN, this.w_FINCOM) - MAX(this.w_FILINI, this.w_INICOM) + 1
              this.w_RATEO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
            endif
          endif
          * --- Registrazione con Competenza che Prosegue oltre il Periodo, Considera la parte successiva come Risconto
          * --- RISCONTO
          if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0 AND this.w_FILFIN>=this.w_DATREG
            this.w_GIOPER = this.w_FINCOM - MAX(this.w_INICOM, this.w_FILFIN)+iif(this.w_INICOM>this.w_FILFIN,1,0)
            this.w_RISCON = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Ricalcola l'Importo di Competenza
          * --- Il risconto viene calcolato se la data di registrazione � inferiore alla data di fine periodo
          * --- Se la data di registrazione � compresa nel periodo di elaborazione il risconto
          * --- non viene tolto dall'importo ma viene fatto il giroconto dopo
          this.w_TOTIMP = this.w_TOTIMP - this.w_RATEO
        endif
      endif
      this.w_INICOM = IIF(this.w_FINCOM>g_INIESE AND this.w_FILFIN=g_FINESE AND this.oParentObject.w_REGOLA="Generazione ratei e risconti",MAX(this.w_INICOM,this.w_FILFIN+1),cp_CharToDate("  -  -    "))
      this.w_FINCOM = IIF(this.w_FINCOM>g_INIESE AND this.w_FILFIN=g_FINESE AND this.oParentObject.w_REGOLA="Generazione ratei e risconti",this.w_FINCOM,cp_CharToDate("  -  -    "))
      if this.oParentObject.w_ONLYRatRis
        * --- Controllo la sezione di Bilancio perch� nel caso di Ratei Risconti prendo solo i costi e ricavi
        if this.w_TIPCON="G"
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCONSUP"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCONSUP;
              from (i_cTable) where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from MASTRI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MASTRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCSEZBIL"+;
              " from "+i_cTable+" MASTRI where ";
                  +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCSEZBIL;
              from (i_cTable) where;
                  MCCODICE = this.w_CONSUP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPSEZ = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_CONSUP = this.w_CODICE
          this.w_TIPSEZ = " "
        endif
        * --- Calcolo la sezione di bilancio di appartenenza del conto o del mastro
        this.w_TIPSEZ = IIF(this.w_TIPCON="G",this.w_TIPSEZ,"")
      endif
      * --- Calcolo il Rateo o Risconto solo se il conto ha sezionale Costo o Ricavo o non proviene dalla regola Ratei/Risconti
      if NOT this.oParentObject.w_ONLYRatRis OR this.w_TIPSEZ="C" OR this.w_TIPSEZ="R"
        * --- Genero un movimento di rateo per la parte scartata...
        if this.w_RATEO<>0
          this.w_APPO = IIF(this.w_RATEO>0, this.w_RATPAS, this.w_RATATT)
          INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, INICOM, FINCOM,SERIAL,DESCRI,PNSERIAL,CPROWNUM,NUMERO) ;
          VALUES ("G", this.w_APPO, this.w_CODBUN, -1*this.w_RATEO, this.w_DATAVUOTA, this.w_DATAVUOTA, this.w_SERIAL, this.w_DESCRI,this.w_PNSERIAL,this.w_CPROWNUM,this.w_NUMERO)
          * --- Riga di Storno
          this.w_RA_INICOM = IIF(this.oParentObject.w_REGOLA="Generazione ratei e risconti",cp_CharToDate("  -  -    "),this.w_INICOM)
          this.w_RA_FINCOM = IIF(this.oParentObject.w_REGOLA="Generazione ratei e risconti",cp_CharToDate("  -  -    "),this.w_FINCOM)
          INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, INICOM, FINCOM,SERIAL,DESCRI,PNSERIAL,CPROWNUM,NUMERO) ;
          VALUES (this.w_TIPCON, this.w_CODICE, this.w_CODBUN, this.w_RATEO, this.w_RA_INICOM, this.w_RA_FINCOM, this.w_SERIAL, this.w_DESCRI,this.w_PNSERIAL,this.w_CPROWNUM,this.w_NUMERO)
        endif
        * --- Se generati Risconti li Inserisce...
        if this.w_RISCON<>0
          this.w_APPO = IIF(this.w_RISCON>0, this.w_RISATT, this.w_RISPAS)
          INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, INICOM, FINCOM,SERIAL,DESCRI,PNSERIAL,CPROWNUM,NUMERO) ;
          VALUES ("G", this.w_APPO, this.w_CODBUN, this.w_RISCON, this.w_DATAVUOTA, this.w_DATAVUOTA, this.w_SERIAL, this.w_DESCRI,this.w_PNSERIAL,this.w_CPROWNUM,this.w_NUMERO)
          * --- Riga di Storno
          INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, INICOM, FINCOM,SERIAL,DESCRI,PNSERIAL,CPROWNUM,NUMERO) ;
          VALUES (this.w_TIPCON, this.w_CODICE, this.w_CODBUN, -1*this.w_RISCON, this.w_INICOM, this.w_FINCOM, this.w_SERIAL, this.w_DESCRI,this.w_PNSERIAL,this.w_CPROWNUM,this.w_NUMERO)
        endif
        SELECT APPOGGIO
      endif
      ENDSCAN
      * --- Dopo avere calcolato il valore dei ratei e risconti per registrazione di primanota
      *     devo calcolare gli importi da inserire in analitica.
      Select EXTCG001.Mrcodvoc as Mrcodvoc,EXTCG001.Mrcodice as Mrcodice,Nvl(EXTCG001.Mrcodcom,space(15)) as Mrcodcom,; 
 EXTCG001.Pnserial as Pnserial,EXTCG001.Cprownum as Cprownum,EXTCG001.Mrparame as Mrparame,; 
 EXTCG001.Analnum as Analnum,Cp_todate(EXTCG001.Mrinicom) as Mrinicom,; 
 Cp_todate(EXTCG001.Mrfincom) as Mrfincom,EXTCG001.Mrtotimp as Mrtotimp,TmpAgg1.Tipcon as Tipcon,TmpAgg1.Codice as Codice,; 
 TmpAgg1.Codbun as Codbun,TmpAgg1.Totimp as Totimp,TmpAgg1.Inicom as Inicom,TmpAgg1.Fincom as Fincom,TmpAgg1.Serial as Serial,; 
 TmpAgg1.Descri as Descri From EXTCG001 Inner Join TmpAgg1 On EXTCG001.Pnserial=TmpAgg1.Pnserial and EXTCG001.Cprownum=TmpAgg1.Cprownum; 
 where EXTCG001.Analnum<>0 order by EXTCG001.Pnserial,EXTCG001.Cprownum,TmpAgg1.Tipcon,TmpAgg1.Codice,TmpAgg1.Codbun,; 
 TmpAgg1.Inicom,TmpAgg1.Fincom into cursor TEMP 
      * --- Verifico se il cursore Temp contiene almeno un record
      Select Temp
      if Reccount()>0
        * --- Costruisco il valore totale dei parametri per riga di analitica
        Select Sum(Mrparame) as Mrpara,Pnserial as Pnserial,Cprownum as Cprownum,Count(Analnum) as Numresto ; 
 from Temp into cursor Parame where Totimp>0 group by Pnserial,Cprownum order by Pnserial,Cprownum 
        * --- Il valore totale dei parametri lo scrivo sul cursore ELABORA
        Select Temp.Mrcodvoc as Mrcodvoc,Temp.Mrcodice as Mrcodice, Temp.Mrcodcom as Mrcodcom,; 
 Temp.Pnserial as Pnserial,Temp.Cprownum as Cprownum,Temp.Mrparame as Mrparame,Temp.Analnum as Analnum,; 
 Temp.Mrinicom as Mrinicom,Temp.Mrfincom as Mrfincom,Temp.Mrtotimp as Mrtotimp,Temp.Tipcon as Tipcon,Temp.Codice as Codice,; 
 Temp.Codbun as Codbun,Temp.Totimp as Totimp,Temp.Inicom as Inicom,Temp.Fincom as Fincom,Temp.Serial as Serial,; 
 Temp.Descri as Descri,Parame.Mrpara as Mrpara,Parame.Numresto; 
 From Temp Inner Join Parame On Temp.Pnserial=Parame.Pnserial and Temp.Cprownum=Parame.Cprownum; 
 order by Temp.Pnserial,Temp.Cprownum,Temp.Tipcon,Temp.Codice,Temp.Codbun,; 
 Temp.Inicom,Temp.Fincom into cursor ELABORA 
        Select Elabora
        =Wrcursor("ELABORA")
        GO TOP
        SCAN FOR MRCODVOC<>SPACE(15) AND MRCODICE<>SPACE(15) 
        this.w_CONTA = this.w_CONTA+1
        this.w_IMPTOT = Elabora.Totimp
        this.w_TOTPAR = Elabora.Mrpara
        this.w_MRTOTIMP = cp_ROUND((Elabora.Totimp * Elabora.MRPARAME) / this.w_TOTPAR, g_PERPVL)
        this.w_RTOT = this.w_RTOT+this.w_MRTOTIMP
        if this.w_CONTA<>Elabora.Numresto
          Replace Mrtotimp with this.w_Mrtotimp
        else
          if this.w_RTOT<>this.w_IMPTOT
            this.w_MRTOTIMP = this.w_MRTOTIMP + (this.w_IMPTOT - this.w_RTOT)
          endif
          Replace Mrtotimp with this.w_Mrtotimp
          this.w_CONTA = 0
          this.w_RTOT = 0
        endif
        Endscan
        * --- Elimino dal cursore Appoggio le righe che hanno valorizzato il campo
        *     Analnum.Effettuo questa operazione per aggiungere al cursore Elabora le
        *     righe di Appoggio che non hanno valorizzato l'analitica
        Select Space(15) as Mrcodvoc,Space(15) as Mrcodice,Space(15) as Mrcodcom,Pnserial as Pnserial,Cprownum as Cprownum,; 
 999.9999-999.9999 as Mrparame,Numero as Analnum,cp_CharToDate("  -  -  ") as Mrinicom,cp_CharToDate("  -  -  ") as Mrfincom,(Totimp*0) as Mrtotimp,; 
 Tipcon as Tipcon,Codice as Codice,Codbun as Codbun,Totimp as Totimp,Inicom as Inicom,Fincom as Fincom,Serial as Serial,; 
 Descri as Descri from TmpAgg1 where Numero=0 into cursor Noana
        Select Noana
        if Reccount()>0
          * --- Creo un unico cursore.
          Select Mrcodvoc,Mrcodice,Mrcodcom,Pnserial,Cprownum,Mrparame,Analnum,Mrinicom,Mrfincom,; 
 Mrtotimp,Tipcon,Codice,Codbun,Totimp,Inicom,Fincom,Serial,Descri from Noana ;
          Union All ;
          Select Mrcodvoc,Mrcodice,Mrcodcom,Pnserial,Cprownum,Mrparame,Analnum,Mrinicom,Mrfincom,; 
 Mrtotimp,Tipcon,Codice,Codbun,Totimp,Inicom,Fincom,Serial,Descri from Elabora ;
          order by 4,11,12,13,15,16,5 into cursor Finale nofilter
        else
          Select * from Elabora into cursor Finale order by 4,11,12,13,15,16,5
        endif
      else
        Select Space(15) as Mrcodvoc,Space(15) as Mrcodice,Space(15) as Mrcodcom,Pnserial as Pnserial,Cprownum as Cprownum,; 
 999.9999-999.9999 as Mrparame,Numero as Numero,cp_CharToDate("  -  -  ") as Mrinicom,cp_CharToDate("  -  -  ") as Mrfincom,0 as Mrtotimp,; 
 Tipcon as Tipcon,Codice as Codice,Codbun as Codbun,Totimp as Totimp,Inicom as Inicom,Fincom as Fincom,Serial as Serial,; 
 Descri as Descri From TmpAgg1 where Numero=0 into cursor Noana
        Select * from Noana into cursor Finale order by 4,11,12,13,15,16,5
      endif
      * --- Chiusura cursori
      if USED("TmpAgg1")
        * --- Azzera temporaneo
        SELECT TmpAgg1
        USE
      endif
      if used("EXTCG001")
        select EXTCG001
        use
      endif
      if used("Appoggio")
        select Appoggio
        use
      endif
      if used("Temp")
        select Temp
        use
      endif
      if used("Parame")
        select Parame
        use
      endif
      if used("Elabora")
        select Elabora
        use
      endif
      if used("Noana")
        select Noana
        use
      endif
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("Finale")
        SELECT Finale
        if RECCOUNT()>0
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT FINALE
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTIMP,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_CODBUN = NVL(CODBUN, g_CODBUN)
          this.w_TIPCON = TIPCON
          this.w_COD001 = IIF(this.w_TIPCON="M", CODICE, SPACE(15))
          this.w_COD002 = IIF(this.w_TIPCON="G", CODICE, SPACE(15))
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTIMP>0, TOTIMP, 0)
          this.w_IMPAVE = IIF(TOTIMP<0, ABS(TOTIMP), 0)
          this.w_INICOM = CP_TODATE(INICOM)
          this.w_FINCOM = CP_TODATE(FINCOM)
          this.w_SERIAL = SERIAL
          this.w_DESCRI = NVL(DESCRI,"")
          this.w_CODCEN = MRCODICE
          this.w_VOCCEN = MRCODVOC
          this.w_CODCOM = MRCODCOM
          this.w_DATCIN = MRINICOM
          this.w_DATCFI = MRFINCOM
          this.w_PARAMETRO = MRPARAME
          this.w_PNSERIAL = PNSERIAL
          this.w_CPROWNUM = CPROWNUM
          this.w_TOTIMP = MRTOTIMP
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE, INICOM, FINCOM,SERIAL,DESCRI2,PNSERIAL,CPROWNUM, ;
          CODCEN, VOCCEN , CODCOM , DATCIN , DATCFI ,PARAMETRO,TOTIMP);
          VALUES (this.w_RECODICE, this.w_CODBUN, this.w_REDESCRI, this.w_TIPCON, this.w_COD001, this.w_COD002, ;
          this.w_COD003, this.w_IMPDAR, this.w_IMPAVE, this.w_INICOM, this.w_FINCOM,this.w_SERIAL,this.w_DESCRI,this.w_PNSERIAL,this.w_CPROWNUM,;
          this.w_CODCEN,this.w_VOCCEN,this.w_CODCOM, this.w_DATCIN, this.w_DATCFI,this.w_PARAMETRO,this.w_TOTIMP)
          SELECT FINALE
          ENDSCAN
        endif
        SELECT FINALE
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_sasfa004')
      use in _Curs_sasfa004
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
