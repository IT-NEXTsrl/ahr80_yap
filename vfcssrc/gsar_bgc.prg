* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgc                                                        *
*              Generazione calendario                                          *
*                                                                              *
*      Author: TAM Software SRL                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-26                                                      *
* Last revis.: 2012-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgc",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bgc as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_YEAR = space(4)
  w_CHKNUM = .f.
  w_DATATT = ctod("  /  /  ")
  w_DESCRGIO = space(35)
  w_OREATT = 0
  w_FLWORKDAY = space(1)
  w_FLCLOSEDAY = space(1)
  w_PROGDAY = 0
  w_DA1ATT = space(5)
  w_AL1ATT = space(5)
  w_DA2ATT = space(5)
  w_AL2ATT = space(5)
  w_COD_ORIG = space(3)
  w_ISWORKDAY = .f.
  w_IDCHIUPE = 0
  w_OBJCTRL = .NULL.
  w_FLSOSP = space(1)
  w_DATSO1 = ctod("  /  /  ")
  w_DATSO2 = ctod("  /  /  ")
  w_CODFES = space(3)
  w_DESFES = space(35)
  * --- WorkFile variables
  FES_DETT_idx=0
  CAL_AZIE_idx=0
  TAB_CALE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione calendari (da GSAR_KGC)
    do case
      case this.pOPER="GENER"
        this.w_YEAR = STR( this.oParentObject.w_ANNO ,4,0)
        if empty(this.oParentObject.w_CODCAL)
          ah_ErrorMsg("Codice calendario non specificato",48)
          i_retcode = 'stop'
          return
        endif
        if empty(this.oParentObject.w_DATINI)
          ah_ErrorMsg("Data iniziale di generazione calendario non specificata",48)
          i_retcode = 'stop'
          return
        endif
        if empty(this.oParentObject.w_DATFIN)
          ah_ErrorMsg("Data finale di generazione calendario non specificata",48)
          i_retcode = 'stop'
          return
        endif
        if YEAR(this.oParentObject.w_DATINI)=YEAR(this.oParentObject.w_DATFIN) AND EMPTY(this.oParentObject.w_COD_FEST) AND !ah_YesNo("Calendario festivit� non specificato, non verranno caricati i giorni festivi predefiniti. Continuare comunque?")
          ah_ErrorMsg("Operazione sospesa come richiesto",64)
          i_retcode = 'stop'
          return
        endif
        if !EMPTY(this.oParentObject.w_COD_FEST)
          this.w_CHKNUM = .F.
          * --- Select from FES_DETT
          i_nConn=i_TableProp[this.FES_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2],.t.,this.FES_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" FES_DETT ";
                +" where FECODICE="+cp_ToStrODBC(this.oParentObject.w_COD_FEST)+" AND FEDATFES>="+cp_ToStrODBC(this.oParentObject.w_DATINI)+" AND FEDATFES<="+cp_ToStrODBC(this.oParentObject.w_DATFIN)+"";
                 ,"_Curs_FES_DETT")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where FECODICE=this.oParentObject.w_COD_FEST AND FEDATFES>=this.oParentObject.w_DATINI AND FEDATFES<=this.oParentObject.w_DATFIN;
              into cursor _Curs_FES_DETT
          endif
          if used('_Curs_FES_DETT')
            select _Curs_FES_DETT
            locate for 1=1
            do while not(eof())
            this.w_CHKNUM = NVL( _Curs_FES_DETT.CONTA , 0) >0
              select _Curs_FES_DETT
              continue
            enddo
            use
          endif
          if !this.w_CHKNUM AND !ah_YesNo("Il calendario festivit� specificato non ha festivit� impostate per l'anno solare specificato Continuare comunque?")
            ah_ErrorMsg("Operazione sospesa come richiesto",64)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Read from CAL_AZIE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CAL_AZIE where ";
                +"CACODCAL = "+cp_ToStrODBC(this.oParentObject.w_CODCAL);
                +" and CAGIORNO = "+cp_ToStrODBC(this.oParentObject.w_DATINI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CACODCAL = this.oParentObject.w_CODCAL;
                and CAGIORNO = this.oParentObject.w_DATINI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          if IsAHE()
            this.w_CHKNUM = ah_YesNo("Il calendario %1 � gi� presente, sovrascriverlo?", , ALLTRIM(this.oParentObject.w_CODCAL) )
          else
            this.w_CHKNUM = ah_YesNo("Il calendario per l'anno solare specificato � gi� presente, sovrascriverlo?" )
          endif
          if this.w_CHKNUM
            * --- Delete from CAL_AZIE
            i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CACODCAL = "+cp_ToStrODBC(this.oParentObject.w_CODCAL);
                    +" and CAGIORNO >= "+cp_ToStrODBC(this.oParentObject.w_DATINI);
                    +" and CAGIORNO <= "+cp_ToStrODBC(this.oParentObject.w_DATFIN);
                     )
            else
              delete from (i_cTable) where;
                    CACODCAL = this.oParentObject.w_CODCAL;
                    and CAGIORNO >= this.oParentObject.w_DATINI;
                    and CAGIORNO <= this.oParentObject.w_DATFIN;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            ah_ErrorMsg("Operazione sospesa come richiesto",64)
            i_retcode = 'stop'
            return
          endif
        endif
        local L_MACRO
        this.w_DATATT = this.oParentObject.w_DATINI
        * --- Se selezionato un solo anno per la generazione del calendario
        if YEAR(this.oParentObject.w_DATINI)=YEAR(this.oParentObject.w_DATFIN)
          if EMPTY(this.oParentObject.w_COD_FEST)
            * --- Nessun calendario festivo selezionato
            this.w_COD_ORIG = "@@@"
          else
            this.w_COD_ORIG = this.oParentObject.w_COD_FEST
          endif
          vq_exec("GSAR_KDF.VQR", this , "Cal_Fest")
        else
          * --- Se selezionati pi� anni per la generazione del calendario
          *     vengono considerati i relativi calendari festivit� predefiniti.
          vq_exec("GSAR2KDF.VQR", this , "Cal_Fest")
        endif
        * --- Try
        local bErr_035DCEB8
        bErr_035DCEB8=bTrsErr
        this.Try_035DCEB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Attenzione! Generazione calendario non completata. Verificare la presenza di tutte le festivit� predefinite necessarie!",48)
        endif
        bTrsErr=bTrsErr or bErr_035DCEB8
        * --- End
      case this.pOPER=="UPDHM"
        * --- Verifico che l'orario standard variato non sia vuoto
        if Ah_YesNo("Aggiornare gli orari dei giorni selezionati con gli orari predefiniti?")
          this.w_PROGDAY = 0
          do while this.w_PROGDAY<7
            this.w_PROGDAY = this.w_PROGDAY + 1
            * --- Verifico se il flag del giorno � attivo
            L_MACRO = "this.oParentObject.w_DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))+" = 'S'"
            if &L_MACRO
              this.w_OBJCTRL = this.oParentObject.GetCtrl( "w_DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0)) )
              * --- Anche se � di tipo carattere � un check quindi occorre passare 0 e 1 come valori
              this.w_OBJCTRL.Value = 0
              * --- Considero il control sempre modificato per gestire i valori da proporre
              *     (se imposto un valore uguale a quello da proporre la procedura 
              *     non fa scattare vari eventi..)
              this.w_OBJCTRL.bUpd = .t.
              * --- Valid sul campo, esegue Check, NotifyEvent Changed, mCalc e SaveDependsOn
              this.w_OBJCTRL.Valid()     
              this.w_OBJCTRL.Value = 1
              * --- Considero il control sempre modificato per gestire i valori da proporre
              *     (se imposto un valore uguale a quello da proporre la procedura 
              *     non fa scattare vari eventi..)
              this.w_OBJCTRL.bUpd = .t.
              * --- Valid sul campo, esegue Check, NotifyEvent Changed, mCalc e SaveDependsOn
              this.w_OBJCTRL.Valid()     
            endif
          enddo
        endif
      case this.pOPER=="FESPRED"
        * --- Viene selezionato il calendario festivit� predefinito relativo all'anno solare proposto inizialmente in maschera
        * --- Select from GSAR1KDF
        do vq_exec with 'GSAR1KDF',this,'_Curs_GSAR1KDF','',.f.,.t.
        if used('_Curs_GSAR1KDF')
          select _Curs_GSAR1KDF
          locate for 1=1
          do while not(eof())
          this.w_CODFES = _Curs_GSAR1KDF.FECODICE
          this.w_DESFES = _Curs_GSAR1KDF.FEDESCRI
            select _Curs_GSAR1KDF
            continue
          enddo
          use
        endif
        this.oParentObject.w_COD_FEST = this.w_CODFES
        this.oParentObject.w_FEDESCRI = this.w_DESFES
    endcase
  endproc
  proc Try_035DCEB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do while this.w_DATATT<=this.oParentObject.w_DATFIN
      ah_msg("Generazione calendario giorno %1 in corso...",.t.,.f.,.f.,dtoc(this.w_DATATT))
      this.w_PROGDAY = DOW(this.w_DATATT)
      this.w_DESCRGIO = g_GIORNO[ this.w_PROGDAY ]
      this.w_OREATT = 0
      this.w_FLWORKDAY = "N"
      this.w_FLCLOSEDAY = "N"
      this.w_DA1ATT = ""
      this.w_AL1ATT = ""
      this.w_DA2ATT = ""
      this.w_AL2ATT = ""
      * --- Verifico se la data cade in un giorno festivo (calendario festivit� / chiusure periodiche)
      this.w_ISWORKDAY = .T.
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ISWORKDAY
        * --- Verifico se il flag del giorno � attivo
        L_MACRO = "this.oParentObject.w_DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))+" = 'S'"
        if &L_MACRO
          * --- Assegno il numero di ore lavorative impostate
          L_MACRO = "this.oParentObject.w_OREDAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))
          this.w_OREATT = &L_MACRO
          this.w_FLWORKDAY = "S"
          * --- Assegno le varie ore inizio/fine impostate per il giorno attuale
          L_MACRO = "this.oParentObject.w_DA1DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))
          this.w_DA1ATT = &L_MACRO
          L_MACRO = "this.oParentObject.w_AL1DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))
          this.w_AL1ATT = &L_MACRO
          L_MACRO = "this.oParentObject.w_DA2DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))
          this.w_DA2ATT = &L_MACRO
          L_MACRO = "this.oParentObject.w_AL2DAY"+ALLTRIM(STR(this.w_PROGDAY,1,0))
          this.w_AL2ATT = &L_MACRO
        endif
      endif
      this.w_DATSO1 = Cp_CharToDate(this.oParentObject.w_GIOSO1+"-"+this.oParentObject.w_MESO1+"-"+str(year(this.w_DATATT),4,0))
      this.w_DATSO2 = Cp_CharToDate(this.oParentObject.w_GIOSO2+"-"+this.oParentObject.w_MESO2+"-"+str(year(this.w_DATATT),4,0))
      this.w_FLSOSP = iif(this.w_DATATT>=this.w_DATSO1 AND this.w_DATATT<=this.w_DATSO2 AND Isalt(),"S","N")
      * --- Insert into CAL_AZIE
      i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAL_AZIE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CACODCAL"+",CAGIORNO"+",CADESGIO"+",CANUMORE"+",CAGIOLAV"+",CAORAIN1"+",CAORAFI1"+",CAORAIN2"+",CAORAFI2"+",CAFLCHIU"+",CAFLSOSP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCAL),'CAL_AZIE','CACODCAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATATT),'CAL_AZIE','CAGIORNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRGIO),'CAL_AZIE','CADESGIO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OREATT),'CAL_AZIE','CANUMORE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLWORKDAY),'CAL_AZIE','CAGIOLAV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DA1ATT),'CAL_AZIE','CAORAIN1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AL1ATT),'CAL_AZIE','CAORAFI1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DA2ATT),'CAL_AZIE','CAORAIN2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AL2ATT),'CAL_AZIE','CAORAFI2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLCLOSEDAY),'CAL_AZIE','CAFLCHIU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'CAL_AZIE','CAFLSOSP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CACODCAL',this.oParentObject.w_CODCAL,'CAGIORNO',this.w_DATATT,'CADESGIO',this.w_DESCRGIO,'CANUMORE',this.w_OREATT,'CAGIOLAV',this.w_FLWORKDAY,'CAORAIN1',this.w_DA1ATT,'CAORAFI1',this.w_AL1ATT,'CAORAIN2',this.w_DA2ATT,'CAORAFI2',this.w_AL2ATT,'CAFLCHIU',this.w_FLCLOSEDAY,'CAFLSOSP',this.w_FLSOSP)
        insert into (i_cTable) (CACODCAL,CAGIORNO,CADESGIO,CANUMORE,CAGIOLAV,CAORAIN1,CAORAFI1,CAORAIN2,CAORAFI2,CAFLCHIU,CAFLSOSP &i_ccchkf. );
           values (;
             this.oParentObject.w_CODCAL;
             ,this.w_DATATT;
             ,this.w_DESCRGIO;
             ,this.w_OREATT;
             ,this.w_FLWORKDAY;
             ,this.w_DA1ATT;
             ,this.w_AL1ATT;
             ,this.w_DA2ATT;
             ,this.w_AL2ATT;
             ,this.w_FLCLOSEDAY;
             ,this.w_FLSOSP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_DATATT = this.w_DATATT + 1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    USE IN SELECT("Cal_Fest")
    * --- Messaggio di chiusura
    wait CLEAR
    ah_ErrorMsg("Generazione calendario completata con successo",48)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico se � una chiusura periodica
    Local L_MACRO
    this.w_IDCHIUPE = 1
    do while this.w_IDCHIUPE<=7
      L_MACRO = "this.w_DATATT>=this.oParentObject.w_INPER" + ALLTRIM(STR(this.w_IDCHIUPE,1,0)) + " AND this.w_DATATT<=this.oParentObject.w_FIPER"+ ALLTRIM(STR(this.w_IDCHIUPE,1,0))
      if &L_MACRO
        this.w_DESCRGIO = ALLTRIM(this.oParentObject.w_DESCHIPE)
        this.w_ISWORKDAY = .F.
        this.w_FLCLOSEDAY = "S"
        exit
      endif
      this.w_IDCHIUPE = this.w_IDCHIUPE + 1
    enddo
    * --- Verifico se � una festivit� a calendario
    SELECT"Cal_Fest"
    GO TOP
    LOCATE FOR FEDATFES=this.w_DATATT
    if FOUND()
      this.w_DESCRGIO = ALLTRIM(Cal_Fest.FeDesFes)
      this.w_ISWORKDAY = .F.
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='FES_DETT'
    this.cWorkTables[2]='CAL_AZIE'
    this.cWorkTables[3]='TAB_CALE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_FES_DETT')
      use in _Curs_FES_DETT
    endif
    if used('_Curs_GSAR1KDF')
      use in _Curs_GSAR1KDF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
