* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1beo                                                        *
*              Preparazione filtro su XDC Fields                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1071]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-24                                                      *
* Last revis.: 2012-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1beo",oParentObject,m.pAzione)
return(i_retval)

define class tgsar1beo as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_OFNOME = space(40)
  w_OFCODCON1 = space(5)
  w_OFCODCON3 = space(5)
  w_OFDESNOM1 = space(40)
  w_OFDESNOM3 = space(40)
  w_OFCODNOM1 = space(20)
  w_OFCODNOM3 = space(20)
  w_OFRAGGR = space(15)
  w_OFCODCON = space(5)
  w_OFCODCON2 = space(5)
  w_OFDESNOM = space(40)
  w_OFDESNOM2 = space(40)
  w_OFCODNOM = space(20)
  w_OFCODNOM2 = space(20)
  w_OFSOLOBSO = space(1)
  w_OFTIPO = space(1)
  w_OFLOCALI = space(30)
  w_OFPROV = space(2)
  w_OFGRUNOM = space(5)
  w_OFORIGI = space(5)
  w_OFOPERAT = space(4)
  w_OFZONA = space(3)
  w_OFAGENTE = space(5)
  w_OFRUOLO = space(5)
  w_OFDATTEST = ctod("  /  /  ")
  w_NODATNS = space(1)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case ALLTRIM(this.pAzione)=="ELENCO"
        * --- Utilizzato in GSAR_MOU: analizza il cursore della query di sincronizzazione contatti GSAR1BEO.VQR ed inserisce l'elenco dei campi in una variabile
        * --- Entrambe le variabili sono vuote, quindi il cursore ritornato � vuoto
        * --- Import
        * --- Export
        * --- Dobbiamo gestire questa variabile per evitare errori, visto che non abbiamo alcun valore nei filtri.
        *     Nelle query GSAR1BEO e GSAR2BEO la variabile NON ha la remove.
        this.w_OFTIPO = "X"
        vq_exec("QUERY\GSAR1BEO.VQR",this,"__APP__")
        AFIELDS(Arr_Campi)
        FOR CntCampi=1 To ALEN(Arr_Campi, 1)
        this.oParentObject.w_ListaCampi = this.oParentObject.w_ListaCampi+Arr_Campi(CntCampi,1)+";"
        ENDFOR
        USE IN SELECT("__APP__")
      case ALLTRIM(this.pAzione)=="INIT"
        * --- All'apertura visualizza il record presente nella tabella
        this.w_OBJECT = This.oparentobject
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_MCCODAZI = this.oParentObject.w_CodiceAziendaDefault
        this.w_OBJECT.ECPSAVE()     
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
