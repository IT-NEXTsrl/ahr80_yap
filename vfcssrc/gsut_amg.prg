* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_amg                                                        *
*              Modelli gadget                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-16                                                      *
* Last revis.: 2014-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_amg"))

* --- Class definition
define class tgsut_amg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 310
  Height = 250+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-22"
  HelpContextID=210322281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  MOD_GADG_IDX = 0
  cFile = "MOD_GADG"
  cKeySelect = "MGSRCPRG"
  cKeyWhere  = "MGSRCPRG=this.w_MGSRCPRG"
  cKeyWhereODBC = '"MGSRCPRG="+cp_ToStrODBC(this.w_MGSRCPRG)';

  cKeyWhereODBCqualified = '"MOD_GADG.MGSRCPRG="+cp_ToStrODBC(this.w_MGSRCPRG)';

  cPrg = "gsut_amg"
  cComment = "Modelli gadget"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MGSRCPRG = space(50)
  w_MGDESCRI = space(254)
  w_MGPREIMG = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_GADG','gsut_amg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_amgPag1","gsut_amg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modello")
      .Pages(1).HelpContextID = 111308998
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMGSRCPRG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='MOD_GADG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_GADG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_GADG_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MGSRCPRG = NVL(MGSRCPRG,space(50))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_GADG where MGSRCPRG=KeySet.MGSRCPRG
    *
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_GADG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_GADG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_GADG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MGSRCPRG',this.w_MGSRCPRG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MGSRCPRG = NVL(MGSRCPRG,space(50))
        .w_MGDESCRI = NVL(MGDESCRI,space(254))
        .w_MGPREIMG = NVL(MGPREIMG,space(254))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(.w_MGPREIMG)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        cp_LoadRecExtFlds(this,'MOD_GADG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MGSRCPRG = space(50)
      .w_MGDESCRI = space(254)
      .w_MGPREIMG = space(254)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(.w_MGPREIMG)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_GADG')
    this.DoRTCalc(1,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMGSRCPRG_1_1.enabled = i_bVal
      .Page1.oPag.oMGDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oMGPREIMG_1_5.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMGSRCPRG_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMGSRCPRG_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_GADG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGSRCPRG,"MGSRCPRG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGDESCRI,"MGDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGPREIMG,"MGPREIMG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    i_lTable = "MOD_GADG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_GADG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_GADG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_GADG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_GADG')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_GADG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MGSRCPRG,MGDESCRI,MGPREIMG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MGSRCPRG)+;
                  ","+cp_ToStrODBC(this.w_MGDESCRI)+;
                  ","+cp_ToStrODBC(this.w_MGPREIMG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_GADG')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_GADG')
        cp_CheckDeletedKey(i_cTable,0,'MGSRCPRG',this.w_MGSRCPRG)
        INSERT INTO (i_cTable);
              (MGSRCPRG,MGDESCRI,MGPREIMG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MGSRCPRG;
                  ,this.w_MGDESCRI;
                  ,this.w_MGPREIMG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_GADG_IDX,i_nConn)
      *
      * update MOD_GADG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_GADG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MGDESCRI="+cp_ToStrODBC(this.w_MGDESCRI)+;
             ",MGPREIMG="+cp_ToStrODBC(this.w_MGPREIMG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_GADG')
        i_cWhere = cp_PKFox(i_cTable  ,'MGSRCPRG',this.w_MGSRCPRG  )
        UPDATE (i_cTable) SET;
              MGDESCRI=this.w_MGDESCRI;
             ,MGPREIMG=this.w_MGPREIMG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_GADG_IDX,i_nConn)
      *
      * delete MOD_GADG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MGSRCPRG',this.w_MGSRCPRG  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(.w_MGPREIMG)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(.w_MGPREIMG)
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMGSRCPRG_1_1.value==this.w_MGSRCPRG)
      this.oPgFrm.Page1.oPag.oMGSRCPRG_1_1.value=this.w_MGSRCPRG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESCRI_1_3.value==this.w_MGDESCRI)
      this.oPgFrm.Page1.oPag.oMGDESCRI_1_3.value=this.w_MGDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMGPREIMG_1_5.value==this.w_MGPREIMG)
      this.oPgFrm.Page1.oPag.oMGPREIMG_1_5.value=this.w_MGPREIMG
    endif
    cp_SetControlsValueExtFlds(this,'MOD_GADG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_amgPag1 as StdContainer
  Width  = 306
  height = 250
  stdWidth  = 306
  stdheight = 250
  resizeXpos=252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMGSRCPRG_1_1 as StdField with uid="RKSLXJVMQM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MGSRCPRG", cQueryName = "MGSRCPRG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 134101261,;
   bGlobalFont=.t.,;
    Height=21, Width=183, Left=96, Top=25, InputMask=replicate('X',50)

  add object oMGDESCRI_1_3 as StdField with uid="KIIHMNRHCB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MGDESCRI", cQueryName = "MGDESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 200296719,;
   bGlobalFont=.t.,;
    Height=21, Width=183, Left=96, Top=48, InputMask=replicate('X',254)

  add object oMGPREIMG_1_5 as StdField with uid="BVQBNNHCHG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MGPREIMG", cQueryName = "MGPREIMG",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 18745613,;
   bGlobalFont=.t.,;
    Height=21, Width=183, Left=96, Top=72, InputMask=replicate('X',254)


  add object oObj_1_7 as cp_showimage with uid="CNCUXMIORX",left=96, top=97, width=150,height=150,;
    caption='Anteprima',;
   bGlobalFont=.t.,;
    default="",stretch=1,;
    nPag=1;
    , ToolTipText = "Immagine di anteprima del Gadget";
    , HelpContextID = 52202109


  add object oObj_1_8 as cp_askpictfile with uid="BLCKDNSVLG",left=281, top=71, width=24,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_MGPREIMG",;
    nPag=1;
    , ToolTipText = "Scegli immagine di preview";
    , HelpContextID = 210121258

  add object oStr_1_2 as StdString with uid="JTWUNJASQF",Visible=.t., Left=14, Top=29,;
    Alignment=1, Width=81, Height=18,;
    Caption="Sorgente prg:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BWYPNNLPUL",Visible=.t., Left=10, Top=52,;
    Alignment=1, Width=85, Height=18,;
    Caption="Nome modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="AEHJHZHNRX",Visible=.t., Left=33, Top=75,;
    Alignment=1, Width=62, Height=18,;
    Caption="Anteprima:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_amg','MOD_GADG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MGSRCPRG=MOD_GADG.MGSRCPRG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
