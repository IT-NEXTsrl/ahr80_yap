* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bkc                                                        *
*              Controlli finali dimensioni                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_4]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-06                                                      *
* Last revis.: 2009-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bkc",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_bkc as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODICE = space(15)
  w_MESS = space(100)
  w_OK = .f.
  w_PADRE = .NULL.
  * --- WorkFile variables
  DET_DIME_idx=0
  TOT_DETT_idx=0
  DETCOMBO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Controlli Finali al salvataggio totalizzatore IVA
    this.w_PADRE = This.Oparentobject
    do case
      case this.pEXEC="A"
        * --- Esegue controlli finali di coerenza delle dimensioni
        this.w_OK = .t.
        this.w_MESS = " "
        if this.w_OK and this.oParentObject.w_DITIPDIM= "CC" AND this.w_PADRE.cFunction<>"Query"
          * --- Controllo imputazione della misura
          * --- Read from DETCOMBO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DETCOMBO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DETCOMBO_idx,2],.t.,this.DETCOMBO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" DETCOMBO where ";
                  +"DC_FONTE = "+cp_ToStrODBC(this.oParentObject.w_DI_FONTE);
                  +" and DCCODDIS = "+cp_ToStrODBC(this.oParentObject.w_DICODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  DC_FONTE = this.oParentObject.w_DI_FONTE;
                  and DCCODDIS = this.oParentObject.w_DICODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              this.w_MESS = AH_MSGFORMAT("Nessuna opzione specificata, impossibile confermare")
              this.w_OK = .f.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pEXEC="B"
        * --- Azzera opzioni combo
        if Type("this.w_PADRE.GSCG_MDC.CNT")="O"
          Select (this.w_PADRE.GSCG_MDC.CNT.ctrsname)
          if reccount()<>0
            ZAP
            this.oParentObject.GSCG_MDC.BlankRec()     
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DET_DIME'
    this.cWorkTables[2]='TOT_DETT'
    this.cWorkTables[3]='DETCOMBO'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
