* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_stp                                                        *
*              Stampa tipi di pagamento                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2009-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_stp",oParentObject))

* --- Class definition
define class tgsar_stp as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 486
  Height = 145
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-14"
  HelpContextID=194414743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  MOD_PAGA_IDX = 0
  cPrg = "gsar_stp"
  cComment = "Stampa tipi di pagamento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_inicod = space(10)
  w_fincod = space(10)
  w_DESCRI = space(50)
  w_DESCRI1 = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_stpPag1","gsar_stp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oinicod_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='MOD_PAGA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_stp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_inicod=space(10)
      .w_fincod=space(10)
      .w_DESCRI=space(50)
      .w_DESCRI1=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_inicod))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_fincod))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=inicod
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_inicod) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_inicod)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_inicod))
          select MPCODICE,MPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_inicod)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_inicod) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oinicod_1_1'),i_cWhere,'GSAR_AMP',"Tipi pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_inicod)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_inicod);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_inicod)
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_inicod = NVL(_Link_.MPCODICE,space(10))
      this.w_DESCRI = NVL(cp_TransLoadField('_Link_.MPDESCRI'),space(50))
    else
      if i_cCtrl<>'Load'
        this.w_inicod = space(10)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_fincod)) OR  (.w_inicod<=.w_fincod)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_inicod = space(10)
        this.w_DESCRI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_inicod Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=fincod
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_fincod) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_fincod)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_fincod))
          select MPCODICE,MPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_fincod)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_fincod) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'ofincod_1_2'),i_cWhere,'GSAR_AMP',"Tipi pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_fincod)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_fincod);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_fincod)
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_fincod = NVL(_Link_.MPCODICE,space(10))
      this.w_DESCRI1 = NVL(cp_TransLoadField('_Link_.MPDESCRI'),space(50))
    else
      if i_cCtrl<>'Load'
        this.w_fincod = space(10)
      endif
      this.w_DESCRI1 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_fincod>=.w_inicod) or (empty(.w_inicod))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_fincod = space(10)
        this.w_DESCRI1 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_fincod Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oinicod_1_1.value==this.w_inicod)
      this.oPgFrm.Page1.oPag.oinicod_1_1.value=this.w_inicod
    endif
    if not(this.oPgFrm.Page1.oPag.ofincod_1_2.value==this.w_fincod)
      this.oPgFrm.Page1.oPag.ofincod_1_2.value=this.w_fincod
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_5.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_5.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_6.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_6.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_fincod)) OR  (.w_inicod<=.w_fincod))  and not(empty(.w_inicod))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oinicod_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((.w_fincod>=.w_inicod) or (empty(.w_inicod)))  and not(empty(.w_fincod))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ofincod_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_stpPag1 as StdContainer
  Width  = 482
  height = 145
  stdWidth  = 482
  stdheight = 145
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oinicod_1_1 as StdField with uid="FNBPZRBYCL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_inicod", cQueryName = "inicod",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Tipo pagamento di inizio selezione",;
    HelpContextID = 152007290,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=94, Top=8, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_inicod"

  func oinicod_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oinicod_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oinicod_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oinicod_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi pagamenti",'',this.parent.oContained
  endproc
  proc oinicod_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_inicod
     i_obj.ecpSave()
  endproc

  add object ofincod_1_2 as StdField with uid="EOZOUVPWIG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_fincod", cQueryName = "fincod",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Tipo di pagamento di fine selezione",;
    HelpContextID = 151988138,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=94, Top=33, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_fincod"

  func ofincod_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc ofincod_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ofincod_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'ofincod_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi pagamenti",'',this.parent.oContained
  endproc
  proc ofincod_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_fincod
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_5 as StdField with uid="WHKAJIKLKR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 100728266,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=207, Top=8, InputMask=replicate('X',50)

  add object oDESCRI1_1_6 as StdField with uid="KRHFMCWBRP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 100728266,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=207, Top=33, InputMask=replicate('X',50)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=94, top=73, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 164458522


  add object oBtn_1_9 as StdButton with uid="ULZIWTNFKU",left=378, top=97, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 251510250;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="IDWVGWNCOR",left=428, top=97, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 251510250;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="ZWZTTNPQVR",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="HBSEMTSSKV",Visible=.t., Left=4, Top=33,;
    Alignment=1, Width=88, Height=15,;
    Caption="A tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LWHMXJXRYK",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=88, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_stp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
