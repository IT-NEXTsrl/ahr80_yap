* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kkw                                                        *
*              Archiviazione rapida documenti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_66]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-26                                                      *
* Last revis.: 2015-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kkw",oParentObject))

* --- Class definition
define class tgsut_kkw as StdForm
  Top    = 138
  Left   = 113

  * --- Standard Properties
  Width  = 592
  Height = 85
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-09-23"
  HelpContextID=233390953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsut_kkw"
  cComment = "Archiviazione rapida documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OPERAZIONE = space(1)
  w_MODALLEG = space(1)
  w_ARCHIVIO = space(20)
  w_FILMODL = space(1)
  w_FILMODF = space(1)
  w_FILMODE = space(1)
  w_FILMODI = space(1)
  w_FILMODS = space(1)
  w_CODUTE = 0
  w_SELCODCLA = space(10)
  w_CLASSE = space(15)
  w_DESCRI = space(40)
  w_CDPUBWEB = space(1)
  w_CDCLAINF = space(15)
  w_CONFMASK = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kkwPag1","gsut_kkw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLASSE_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OPERAZIONE=space(1)
      .w_MODALLEG=space(1)
      .w_ARCHIVIO=space(20)
      .w_FILMODL=space(1)
      .w_FILMODF=space(1)
      .w_FILMODE=space(1)
      .w_FILMODI=space(1)
      .w_FILMODS=space(1)
      .w_CODUTE=0
      .w_SELCODCLA=space(10)
      .w_CLASSE=space(15)
      .w_DESCRI=space(40)
      .w_CDPUBWEB=space(1)
      .w_CDCLAINF=space(15)
      .w_CONFMASK=.f.
      .w_OPERAZIONE=oParentObject.w_OPERAZIONE
      .w_MODALLEG=oParentObject.w_MODALLEG
      .w_ARCHIVIO=oParentObject.w_ARCHIVIO
      .w_CLASSE=oParentObject.w_CLASSE
      .w_DESCRI=oParentObject.w_DESCRI
      .w_CDPUBWEB=oParentObject.w_CDPUBWEB
      .w_CDCLAINF=oParentObject.w_CDCLAINF
      .w_CONFMASK=oParentObject.w_CONFMASK
          .DoRTCalc(1,3,.f.)
        .w_FILMODL = IIF(.w_OPERAZIONE="C" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "L", "-")
        .w_FILMODF = IIF(.w_OPERAZIONE$"CS" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "F", "-")
        .w_FILMODE = IIF(.w_OPERAZIONE$"CS" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "E", "-")
        .w_FILMODI = IIF(.w_OPERAZIONE$"CS" OR .w_OPERAZIONE='B', "I", "-")
        .w_FILMODS = IIF(empty(.w_ARCHIVIO), "-", "S")
        .w_CODUTE = i_CODUTE
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CLASSE))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,14,.f.)
        .w_CONFMASK = True
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_OPERAZIONE=.w_OPERAZIONE
      .oParentObject.w_MODALLEG=.w_MODALLEG
      .oParentObject.w_ARCHIVIO=.w_ARCHIVIO
      .oParentObject.w_CLASSE=.w_CLASSE
      .oParentObject.w_DESCRI=.w_DESCRI
      .oParentObject.w_CDPUBWEB=.w_CDPUBWEB
      .oParentObject.w_CDCLAINF=.w_CDCLAINF
      .oParentObject.w_CONFMASK=.w_CONFMASK
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_FILMODL = IIF(.w_OPERAZIONE="C" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "L", "-")
            .w_FILMODF = IIF(.w_OPERAZIONE$"CS" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "F", "-")
            .w_FILMODE = IIF(.w_OPERAZIONE$"CS" AND g_DMIP<>'S' AND .w_OPERAZIONE<>'B', "E", "-")
            .w_FILMODI = IIF(.w_OPERAZIONE$"CS" OR .w_OPERAZIONE='B', "I", "-")
            .w_FILMODS = IIF(empty(.w_ARCHIVIO), "-", "S")
        .DoRTCalc(9,14,.t.)
            .w_CONFMASK = True
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.enabled = this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.visible=!this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLASSE
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLASSE))
          select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSE)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_CLASSE)+"%");

            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_CLASSE)+"%");

            select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLASSE) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLASSE_1_11'),i_cWhere,'GSUT_MCD',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLASSE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLASSE)
            select CDCODCLA,CDDESCLA,CDMODALL,CDPUBWEB,CDCLAINF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSE = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCRI = NVL(_Link_.CDDESCLA,space(40))
      this.w_MODALLEG = NVL(_Link_.CDMODALL,space(1))
      this.w_CDPUBWEB = NVL(_Link_.CDPUBWEB,space(1))
      this.w_CDCLAINF = NVL(_Link_.CDCLAINF,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSE = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_MODALLEG = space(1)
      this.w_CDPUBWEB = space(1)
      this.w_CDCLAINF = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=INLIST(.w_MODALLEG,.w_FILMODF, .w_FILMODE, .w_FILMODI, .w_FILMODL, .w_FILMODS) AND SUBSTR( CHKPECLA( .w_CLASSE, .w_CODUTE, .w_ARCHIVIO), 2, 1 ) = 'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento")
        endif
        this.w_CLASSE = space(15)
        this.w_DESCRI = space(40)
        this.w_MODALLEG = space(1)
        this.w_CDPUBWEB = space(1)
        this.w_CDCLAINF = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_11.value==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_11.value=this.w_CLASSE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_13.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_13.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.RadioValue()==this.w_CDPUBWEB)
      this.oPgFrm.Page1.oPag.oCDPUBWEB_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(INLIST(.w_MODALLEG,.w_FILMODF, .w_FILMODE, .w_FILMODI, .w_FILMODL, .w_FILMODS) AND SUBSTR( CHKPECLA( .w_CLASSE, .w_CODUTE, .w_ARCHIVIO), 2, 1 ) = 'S')  and not(empty(.w_CLASSE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLASSE_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kkw
      if empty(this.w_CLASSE)
        =ah_ErrorMsg('Occorre indicare una classe documentale',48)
        i_bres=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kkwPag1 as StdContainer
  Width  = 588
  height = 85
  stdWidth  = 588
  stdheight = 85
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLASSE_1_11 as StdField with uid="ZKGFHWQBEA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLASSE", cQueryName = "CLASSE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per archiviare documenti nella classe selezionata oppure archivazione con scanner in modalit� collegamento",;
    ToolTipText = "Classe documentale",;
    HelpContextID = 211688742,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=134, Left=157, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLASSE"

  func oCLASSE_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSE_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSE_1_11.mZoom
    vx_exec(""+ IIF(lower(oParentObject.Class) = 'tgsut_btb', 'QUERY\gsut1kkw.VZM', 'QUERY\gsut_kkw.VZM') +"",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oCLASSE_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLASSE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_13 as StdField with uid="BCQHKBKQOX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe",;
    HelpContextID = 8336950,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=293, Top=11, InputMask=replicate('X',40)

  add object oCDPUBWEB_1_14 as StdCheck with uid="RWQSQMBOSF",rtseq=13,rtrep=.f.,left=157, top=42, caption="Pubblica su Web",;
    ToolTipText = "Se attivo: l'indice creato potr� essere pubblicato su web",;
    HelpContextID = 40827544,;
    cFormVar="w_CDPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDPUBWEB_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCDPUBWEB_1_14.GetRadio()
    this.Parent.oContained.w_CDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCDPUBWEB_1_14.SetRadio()
    this.Parent.oContained.w_CDPUBWEB=trim(this.Parent.oContained.w_CDPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_CDPUBWEB=='S',1,;
      0)
  endfunc

  func oCDPUBWEB_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODALLEG<>'I')
    endwith
   endif
  endfunc

  func oCDPUBWEB_1_14.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' or .w_OPERAZIONE="B" or .w_MODALLEG="S")
    endwith
  endfunc


  add object oBtn_1_17 as StdButton with uid="ADLSFZBDXO",left=483, top=36, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per procedere con l'archiviazione";
    , HelpContextID = 90199574;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="THOBMELFTL",left=534, top=36, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Sospende l'operazione di archiviazione rapida";
    , HelpContextID = 226073530;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="JBWSDNOZTJ",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=148, Height=18,;
    Caption="Classe documentale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kkw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
