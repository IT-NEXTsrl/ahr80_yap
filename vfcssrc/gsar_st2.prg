* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_st2                                                        *
*              Stampa tributi                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2008-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_st2",oParentObject))

* --- Class definition
define class tgsar_st2 as StdForm
  Top    = 50
  Left   = 90

  * --- Standard Properties
  Width  = 613
  Height = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-01-08"
  HelpContextID=74020713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  TRI_BUTI_IDX = 0
  cPrg = "gsar_st2"
  cComment = "Stampa tributi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_tipo = space(1)
  w_TRIB1 = space(5)
  w_TRIB2 = space(5)
  w_des1 = space(60)
  w_des2 = space(60)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_st2Pag1","gsar_st2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.otipo_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TRI_BUTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_tipo=space(1)
      .w_TRIB1=space(5)
      .w_TRIB2=space(5)
      .w_des1=space(60)
      .w_des2=space(60)
        .w_tipo = 'R'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TRIB1))
          .link_1_3('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TRIB2))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(4,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRIB1_1_3.enabled = this.oPgFrm.Page1.oPag.oTRIB1_1_3.mCond()
    this.oPgFrm.Page1.oPag.oTRIB2_1_4.enabled = this.oPgFrm.Page1.oPag.oTRIB2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRIB1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRIB1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_TRIB1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_TRIB1))
          select TRCODTRI,TRDESTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRIB1)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRIB1) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oTRIB1_1_3'),i_cWhere,'GSAR_ATB',"Elenco tributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRIB1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_TRIB1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_TRIB1)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRIB1 = NVL(_Link_.TRCODTRI,space(5))
      this.w_des1 = NVL(_Link_.TRDESTRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_TRIB1 = space(5)
      endif
      this.w_des1 = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY (.w_TRIB2) or .w_TRIB1<=.w_TRIB2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endif
        this.w_TRIB1 = space(5)
        this.w_des1 = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRIB1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRIB2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRIB2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_TRIB2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_TRIB2))
          select TRCODTRI,TRDESTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRIB2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRIB2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oTRIB2_1_4'),i_cWhere,'GSAR_ATB',"Elenco tributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRIB2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_TRIB2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_TRIB2)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRIB2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_des2 = NVL(_Link_.TRDESTRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_TRIB2 = space(5)
      endif
      this.w_des2 = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TRIB1) or .w_TRIB2>=.w_TRIB1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endif
        this.w_TRIB2 = space(5)
        this.w_des2 = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRIB2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.otipo_1_1.RadioValue()==this.w_tipo)
      this.oPgFrm.Page1.oPag.otipo_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIB1_1_3.value==this.w_TRIB1)
      this.oPgFrm.Page1.oPag.oTRIB1_1_3.value=this.w_TRIB1
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIB2_1_4.value==this.w_TRIB2)
      this.oPgFrm.Page1.oPag.oTRIB2_1_4.value=this.w_TRIB2
    endif
    if not(this.oPgFrm.Page1.oPag.odes1_1_5.value==this.w_des1)
      this.oPgFrm.Page1.oPag.odes1_1_5.value=this.w_des1
    endif
    if not(this.oPgFrm.Page1.oPag.odes2_1_6.value==this.w_des2)
      this.oPgFrm.Page1.oPag.odes2_1_6.value=this.w_des2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY (.w_TRIB2) or .w_TRIB1<=.w_TRIB2)  and (.w_tipo<>' ')  and not(empty(.w_TRIB1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRIB1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
          case   not(EMPTY(.w_TRIB1) or .w_TRIB2>=.w_TRIB1)  and (.w_tipo<>' ')  and not(empty(.w_TRIB2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRIB2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_st2Pag1 as StdContainer
  Width  = 609
  height = 182
  stdWidth  = 609
  stdheight = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object otipo_1_1 as StdCombo with uid="UMKIHEXLGU",value=4,rtseq=1,rtrep=.f.,left=101,top=7,width=111,height=21;
    , HelpContextID = 66258634;
    , cFormVar="w_tipo",RowSource=""+"IRPEF,"+"INPS,"+"Contr. prev.,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func otipo_1_1.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'I',;
    iif(this.value =3,'P',;
    iif(this.value =4,' ',;
    space(1))))))
  endfunc
  func otipo_1_1.GetRadio()
    this.Parent.oContained.w_tipo = this.RadioValue()
    return .t.
  endfunc

  func otipo_1_1.SetRadio()
    this.Parent.oContained.w_tipo=trim(this.Parent.oContained.w_tipo)
    this.value = ;
      iif(this.Parent.oContained.w_tipo=='R',1,;
      iif(this.Parent.oContained.w_tipo=='I',2,;
      iif(this.Parent.oContained.w_tipo=='P',3,;
      iif(this.Parent.oContained.w_tipo=='',4,;
      0))))
  endfunc

  add object oTRIB1_1_3 as StdField with uid="XIXDAAGFWZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRIB1", cQueryName = "TRIB1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice tributo iniziale � maggiore di quello finale",;
    ToolTipText = "Codice tributo di inizio selezione",;
    HelpContextID = 17993674,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=101, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_TRIB1"

  func oTRIB1_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipo<>' ')
    endwith
   endif
  endfunc

  func oTRIB1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRIB1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRIB1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oTRIB1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'',this.parent.oContained
  endproc
  proc oTRIB1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_TRIB1
     i_obj.ecpSave()
  endproc

  add object oTRIB2_1_4 as StdField with uid="MQCAURXZFP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRIB2", cQueryName = "TRIB2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice tributo iniziale � maggiore di quello finale",;
    ToolTipText = "Codice tributo di fine selezione",;
    HelpContextID = 16945098,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=101, Top=71, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_TRIB2"

  func oTRIB2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipo<>' ')
    endwith
   endif
  endfunc

  func oTRIB2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRIB2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRIB2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oTRIB2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'',this.parent.oContained
  endproc
  proc oTRIB2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_TRIB2
     i_obj.ecpSave()
  endproc

  add object odes1_1_5 as StdField with uid="YOKBYPNNRC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_des1", cQueryName = "des1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 70310858,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=166, Top=41, InputMask=replicate('X',60)

  add object odes2_1_6 as StdField with uid="PMZZVDXWSG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_des2", cQueryName = "des2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 70245322,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=166, Top=71, InputMask=replicate('X',60)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=101, top=106, width=498,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103976934


  add object oBtn_1_9 as StdButton with uid="GVLYBPGDIE",left=500, top=133, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 67768870;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="DKQWJOSNJH",left=550, top=133, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66703290;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="AWBPINQIBT",Visible=.t., Left=5, Top=74,;
    Alignment=1, Width=95, Height=15,;
    Caption="A tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MKMFMDSBAW",Visible=.t., Left=5, Top=43,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="PXUGTMDPRX",Visible=.t., Left=2, Top=106,;
    Alignment=1, Width=98, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="OFRQUDWCQP",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=95, Height=18,;
    Caption="Tip.tributo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_st2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
