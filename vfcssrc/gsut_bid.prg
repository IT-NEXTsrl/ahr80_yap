* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bid                                                        *
*              Import dati                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-21                                                      *
* Last revis.: 2017-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bid",oParentObject)
return(i_retval)

define class tgsut_bid as StdBatch
  * --- Local variables
  w_TMPPATH = space(254)
  w_PADRE = .NULL.
  w_CODAZI = space(5)
  w_LOOP = 0
  w_DELPATH = space(254)
  w_NFILE = 0
  w_CHKAZI = space(254)
  w_DBF = space(254)
  w_OK = .f.
  w_MULTIAZI = .f.
  w_DELREC = .f.
  w_LOOPFLD = 0
  w_CHIAVE = space(40)
  w_IDX = 0
  w_NFLD = 0
  w_NOXDC = .f.
  w_NREC = 0
  w_FILT_AZI = .f.
  w_CAMPO = space(30)
  w_BINS = .f.
  w_CNAME = space(30)
  w_PHNAME = space(50)
  w_CONN = 0
  w_IDXTABLE = 0
  w_LoopIFK = 0
  w_LTABLE = space(200)
  w_LCPHTABLE = space(200)
  w_OK = 0
  w_IDXTABLEIFK = 0
  w_LoopCIFK = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa dati da un'altra azienda in questa...
    *     Riceve uno zip con i dati, passi che esegue:
    *     a) apre lo zip e scompatta i DBF in una cartella temporanea
    *     b) elimina le integrit� referenziali delle tabelle dell'azienda
    *     c) passa i dati (prima svogle una delete e poi una serie di insert)
    *     se la insert fallisce copia in undbf i dati non passati
    *     d) ricostruice l'integrit� referenziale degli archivi
    *     e) elimina dalla temp i dbf creati
    *     
    this.w_CODAZI = i_CODAZI
    =cp_ReadXdc()
    this.w_PADRE = This.oParentObject
    this.oParentObject.w_Msg = ""
    AddMsgNL("Inizio procedura di importazione %1", this , time() )
    * --- Cartella temporanea in cui vado a salvare i DBF provenienti dal
    *     file compresso.
    this.w_TMPPATH = Tempadhoc()+"\"+Sys(2015)+"\"
    * --- Cartella da cancellare
    this.w_DELPATH = this.w_TMPPATH
    * --- Tento di cancellarla...
    if Not DeleteFolder( this.w_TMPPATH )
      AddMsgNL("Impossibile cancellare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
      i_retcode = 'stop'
      return
    endif
    * --- La creo..
    if Not Directory( this.w_TMPPATH ) 
      * --- Creo la cartella..
      if Not MakeDir(this.w_TMPPATH)
        AddMsgNL("Impossibile creare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Procedo con la scomppatazione...
    AddMsgNL("Apertura pacchetto %1 ", this , this.oParentObject.w_PATH , , ,, ,)
    Local L_RetMsg
    L_RetMsg = ""
    if ! ZIPUNZIP("U", this.oParentObject.w_PATH , this.w_TMPPATH , this.oParentObject.w_PWD, @L_RetMsg)
      AddMsgNL("Impossibile scompattare il pacchetto.%0%1 ", this , L_RetMsg)
      * --- Cancello la cartella temporanea di scompattazione Dbf
      if Not DeleteFolder( this.w_DELPATH )
        AddMsgNL("Impossibile cancellare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
      endif
      i_retcode = 'stop'
      return
    endif
    AddMsgNL("Pacchetto %1 aperto", this , this.oParentObject.w_PATH , , ,, ,)
    this.w_TMPPATH = this.w_TMPPATH+"\EXPORT\"
    this.w_NFILE = ADIR(aDBF,this.w_TMPPATH+"*.DBF" )
    if this.w_NFILE=0
      AddMsgNL("Impossibile recuperare dati all'interno del pacchetto aperto in %1 ", this , this.w_TMPPATH , , ,, ,)
    else
      this.w_CHKAZI = this.w_TMPPATH+"AZIENDA.DBF"
      if cp_fileexist( this.w_CHKAZI )
        * --- Controllo esportazione stessa azienda di destinazione
        Select AZCODAZI from (this.w_CHKAZI) Where AZCODAZI =i_CODAZI into cursor CONTROLLI
        if Not(Used("CONTROLLI") And Reccount("CONTROLLI") >0)
          if Not Ah_YesNo("Il codice azienda di destinazione %1 non � presente tra le aziende di origine%0Si desidera proseguire?", , i_CODAZI)
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Controllo presenza nella tabella azienda di pi� aziende di origine
        Select Count(AZCODAZI) as NUMAZI from (this.w_CHKAZI) into cursor CONTROLLI
        if Used("CONTROLLI") And Reccount("CONTROLLI") >0 And CONTROLLI.NUMAZI>1
          if Not Ah_YesNo("Attenzione: l'anagrafica azienda contiene pi� aziende%0Si desidera proseguire?")
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Prima di continuare devo chiudere il Dbf Azienda che verr� in seguito utilizzato
        if Used("Azienda") 
 
          * --- Chiudo DBF azienda
           
 Select("Azienda") 
 Use
        endif
      else
        if Not Ah_YesNo("Il pacchetto non contiene informazioni in merito all'azienda di partenza%0Si desidera proseguire?", , )
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Elimino le integrit� referenziali per l'azienda corrente su tutti gli archivi
      *     (eliminare per solo quelli importati non � sufficiente)
       public msgwnd 
 msgwnd=createobject("mkdbmsg") 
 msgwnd.caption=cp_MsgFormat(cp_Translate("Rimuove Integrit� referenziali"), this.w_CODAZI ) 
 msgwnd.show()
      * --- Se decido di rimuovere tutte le integrit�
      if this.oParentObject.w_REMALLINT="S"
        this.w_LOOP = 1
        do while this.w_LOOP<=i_dcx.GetTablesCount()
          this.w_CNAME = i_dcx.GetTable( this.w_LOOP )
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
      else
         
 sqlexec(i_ServerConn[1,2],"select * from cpazi where codazi="+cp_ToStrODBC(i_codazi),"_azi_")
        this.w_LOOP = 1
        do while this.w_LOOP<=this.w_NFILE
          * --- Rimuovo integrit�
          this.w_CNAME = Upper( JUSTSTEM ( aDBF[ this.w_LOOP , 1 ] ) )
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LOOP = this.w_LOOP + 1 
        enddo
      endif
       
 msgwnd.End(cp_MsgFormat(cp_Translate("Cancellate integrit�"), this.w_CODAZI)) 
 AddMsgNL(" %1 ", this , msgwnd.edt.text ) 
 msgwnd.Release()
      * --- A questo punto passo con l'inserimento dei dati presenti nel pacchetto
      *     scorro l'array aDBF
      this.w_LOOP = 1
      do while this.w_LOOP<=this.w_NFILE
        * --- Apro il DBF...
        this.w_DBF = Upper( JUSTSTEM ( aDBF[ this.w_LOOP , 1 ] ) )
        * --- E' una tabella multi aziendale...
        if i_dcx.GetTableIdx( this.w_DBF )>0
          this.w_MULTIAZI = UPPER( i_dcx.GetPhTable( i_dcx.GetTableIdx( this.w_DBF ) , this.w_CODAZI ) ) = this.w_DBF
        else
          * --- Se tabella fuori dal dizionario la considero multiaziendale
          this.w_MULTIAZI = .T.
        endif
        * --- Scrivo le informazione sul database..
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LOOP = this.w_LOOP + 1 
      enddo
      * --- Tento la ricostruzione delle integrit� referenziali...
       public msgwnd 
 msgwnd=createobject("mkdbmsg") 
 msgwnd.caption=cp_MsgFormat(cp_Translate("Aggiungi Constraints"), this.w_CODAZI ) 
 msgwnd.show()
      if this.oParentObject.w_REMALLINT="S"
        this.w_LOOP = 1
        do while this.w_LOOP<=i_dcx.GetTablesCount()
          this.w_CNAME = i_dcx.GetTable( this.w_LOOP )
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
      else
        this.w_LOOP = 1
        do while this.w_LOOP<=this.w_NFILE
          * --- Rimuovo integrit�
          this.w_CNAME = Upper( JUSTSTEM ( aDBF[ this.w_LOOP , 1 ] ) )
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LOOP = this.w_LOOP + 1 
        enddo
        if used("_azi_")
           
 select _azi_ 
 use
        endif
      endif
       
 msgwnd.End(cp_MsgFormat(cp_Translate("Ripristinate integrit�"), this.w_CODAZI)) 
 AddMsgNL(" %1 ", this , msgwnd.edt_err.value ) 
 msgwnd.Release()
    endif
    * --- Cancello la cartella temporanea di scompattazione Dbf
    if Not DeleteFolder( this.w_DELPATH )
      AddMsgNL("Impossibile cancellare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
    endif
    AddMsgNL("Fine procedura di importazione %1", this , time() )
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato un DBF migra le informazioni sul datbaase, operazione svolta con:
    *     a) Determinazione dell'elenco dei campi
    *     b) svuotamento dell'archivio sul database
    *     c)  scansione del DBF e riga per riga inserimento sul database
    * --- Costruisco l'array da passare al comando di INSERT
    this.w_OK = .T.
    this.w_NOXDC = .F.
    * --- Recupero informazioni tabella (elenco campi ) o da XDC o tramite
    *     AFIELDS sul DBF se tabella non in analisi.
    *     Se da DBF la procedura si aspetta che la struttura di partenza coincida 
    *     con quella di destinazione.
    Dimension ArrCampi[1,2], Arrkey[1,2]
    if i_Dcx.GetFieldsCount( this.w_DBF )>0
      this.w_LOOPFLD = 1
      do while this.w_LOOPFLD<= i_Dcx.GetFieldsCount( this.w_DBF )
         
 Dimension ArrCampi[ this.w_LOOPFLD , 2 ] 
 ArrCampi[ this.w_LOOPFLD ,1 ] = Alltrim( i_Dcx.GetFieldName( this.w_DBF , this.w_LOOPFLD ) ) 
        this.w_LOOPFLD = this.w_LOOPFLD + 1
      enddo
      * --- Metto il CPCCCHK finale
       
 Dimension ArrCampi[ this.w_LOOPFLD ,2 ] 
 ArrCampi[ this.w_LOOPFLD ,1 ] = "CPCCCHK"
      this.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_DBF ,1 ) )
      this.w_LOOPFLD = 1
      this.w_IDX = 1
      do while this.w_LOOPFLD<>0
        this.w_LOOPFLD = At( "," , this.w_CHIAVE )
        if this.w_LOOPFLD>0
          this.w_CAMPO = Left( this.w_CHIAVE , this.w_LOOPFLD -1 )
        else
          this.w_CAMPO = Alltrim( this.w_CHIAVE )
        endif
         
 Dimension Arrkey[ this.w_IDX ,2 ] 
 Arrkey[ this.w_IDX ,1 ] = this.w_CAMPO
        this.w_CHIAVE = Alltrim( Substr( this.w_CHIAVE , this.w_LOOPFLD+1 , Len ( this.w_CHIAVE ) ) )
        this.w_IDX = this.w_IDX + 1
      enddo
    else
      * --- Tabella non in analisi, provo con AFIELDS
      Use ( this.w_TMPPATH + "\"+aDBF[ this.w_LOOP , 1 ] )
      this.w_NFLD = AFIELDS( aFLD , this.w_DBF )
      USE IN SELECT( this.w_DBF )
      if this.w_NFLD=0
        AddMsgNL("Impossibile determinare struttura archivio %1.", this , this.w_DBF )
        this.w_OK = .F.
      else
        this.w_LOOPFLD = 1
        do while this.w_LOOPFLD<=this.w_NFLD
           
 Dimension ArrCampi[ this.w_LOOPFLD , 2 ] 
 ArrCampi[ this.w_LOOPFLD ,1 ] = Alltrim( aFld( this.w_LOOPFLD ,1 ) ) 
          this.w_LOOPFLD = this.w_LOOPFLD + 1
        enddo
        this.w_NOXDC = .T.
      endif
    endif
    if this.w_OK
      * --- Svuoto l'archivio sul database...
      Local L_nRec 
 L_nRec= -1
      this.w_NREC = L_nRec
      * --- Se aggiornamento � tutto come primo passo cancello tutto l'archivio.
      *     Se tabella multi aziendale, qualsiasi aggiornamento ci sia
      *     mi comporto come se w_UPDMODE fosse 'I'
      *     (altrimenti avrei problemi a cancellare anagrafiche tipo AZIENDA)
      this.w_DELREC = this.oParentObject.w_UPDMODE="I" 
      do case
        case this.w_MULTIAZI
          this.w_FILT_AZI = .F.
          if this.w_DBF="CPWARN"
            * --- Per CPWARN cancello solo i progressivi dell'azienda corrente..
            this.w_CAMPO = "warncode"
            this.w_FILT_AZI = .T.
          else
            * --- Devo recuperare il campo da considerare per il filro multiaziendale
            this.w_CAMPO = GETFLDAZ( this.w_DBF )
            this.w_FILT_AZI = not Empty( this.w_CAMPO )
          endif
          * --- Ho trovato come filtrare, non serve cancellare puntualmente dopo...
          if this.w_FILT_AZI
             
 Dimension ArrDb[1,2] 
 ArrDb[1,1]=this.w_CAMPO 
 ArrDb[1,2]=this.w_CODAZI
            * --- Try
            local bErr_049A4C18
            bErr_049A4C18=bTrsErr
            this.Try_049A4C18()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_049A4C18
            * --- End
            this.w_DELREC = .F.
          else
            * --- Se archivio multi aziendale, e non trovo filtro, vado record per record a cancellare ed inserire
            *     a prescindere da quanto selezionato sulla maschera
            this.w_DELREC = .T.
          endif
        case ( this.oParentObject.w_UPDMODE="T" And not this.w_MULTIAZI ) Or this.w_NOXDC
          * --- Modalit� di cancella tutto, non so come determinare la chiave per
          *     svolgere una cancellazione puntuale
          * --- Try
          local bErr_049A2368
          bErr_049A2368=bTrsErr
          this.Try_049A2368()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_049A2368
          * --- End
          this.w_DELREC = .F.
      endcase
      if not this.w_OK
        * --- Il contenuto del pacchetto per l'archivio non � passato...
        AddMsgNL("Impossibile cancellare contenuto archivio %1, il contenuto del pacchetto, per questo archivio, non � stato quindi trasferito.", this , this.w_DBF )
      else
        if this.w_NREC>-1 And this.oParentObject.w_VERBOSE
          * --- Do il messaggio solo se ho cancellato..
          AddMsgNL("Cancellato contenuto archivio %1, eliminate %2 registrazioni ", this , this.w_DBF, this.w_NREC )
        endif
        * --- Apro il DBF e procedo con la scansione...
        Use ( this.w_TMPPATH + "\"+aDBF[ this.w_LOOP , 1 ] ) IN 0 
 SELECT (aDBF[ this.w_LOOP , 1 ]) 
 Go Top
        do while Not Eof( this.w_DBF )
          * --- Valorizzo i campi sull'array con quanto trovo sul DBF...
          this.w_LOOPFLD = 1
          do while this.w_LOOPFLD<= Alen( ArrCampi , 1 )
            * --- I nomio di colonna sul DBF sono lunghi al massimo 10 caratteri
            if this.w_MULTIAZI AND ArrCampi[ this.w_LOOPFLD , 1 ]=this.w_CAMPO AND not EMPTY (NVL(this.w_CAMPO,""))
              * --- --Se i dati arrivano da un altra azienda allora i campi aziendali vengono impostati con il codice dell azienda corrente
              ArrCampi[ this.w_LOOPFLD , 2 ] = this.w_CODAZI
            else
              ArrCampi[ this.w_LOOPFLD , 2 ] = Eval(this.w_DBF+"."+ left(alltrim(ArrCampi[ this.w_LOOPFLD , 1 ]),10) )
            endif
            this.w_LOOPFLD = this.w_LOOPFLD + 1
          enddo
          this.w_BINS = .t.
          if this.w_DELREC
            * --- Popolo l'array per l'aggiornamento...
            this.w_IDX = 1
            do while this.w_IDX<=Alen( ArrKey ,1 ) 
              Arrkey[ this.w_IDX ,2 ] = Eval(this.w_DBF+"."+ Arrkey[ this.w_IDX ,1 ] )
              this.w_IDX = this.w_IDX + 1
            enddo
            * --- Aggiorno / inserisco puntualmente il record...
            *     Provo l'UPDATE, se restituisce un record non serve svolgere la INSERT..
            * --- Try
            local bErr_049A7978
            bErr_049A7978=bTrsErr
            this.Try_049A7978()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_049A7978
            * --- End
          endif
          * --- Tento la INSERT...
          if this.w_BINS
            * --- Try
            local bErr_049A8968
            bErr_049A8968=bTrsErr
            this.Try_049A8968()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_049A8968
            * --- End
          endif
          Select ( this.w_DBF ) 
 Skip
        enddo
        * --- Chiudo il DBF
        USE IN SELECT( this.w_DBF )
      endif
    endif
  endproc
  proc Try_049A4C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- La try perde la visibilit� della variabile locale a pag 2
    Local L_nRec 
 L_nRec= -1
    this.w_OK = Empty( DELETETABLE( this.w_DBF , @ArrDb , this.w_PADRE , this.oParentObject.w_VERBOSE , "" , @L_nRec) )
    this.w_NREC = L_nRec
    return
  proc Try_049A2368()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- La try perde la visibilit� della variabile locale a pag 2
    Local L_nRec 
 L_nRec= -1
    this.w_OK = Empty( DELETETABLE( this.w_DBF , , this.w_PADRE , this.oParentObject.w_VERBOSE , "1=1" , @L_nRec) )
    this.w_NREC = L_nRec
    return
  proc Try_049A7978()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- La try perde la visibilit� della variabile locale a pag 2
    Local L_nRec 
 L_nRec= -1
    if Not Empty( WRITETABLE( this.w_DBF , @ArrCampi ,@ArrKey , this.w_PADRE , this.oParentObject.w_Verbose , "",@L_nRec) )
      AddMsgNL("Comando UPDATE su archivio %1 ha generato errore su una registrazione.", this , this.w_DBF )
    else
      this.w_BINS = L_nRec<=0
    endif
    return
  proc Try_049A8968()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not Empty( INSERTTABLE( this.w_DBF , @ArrCampi , this.w_PADRE , this.oParentObject.w_VERBOSE ) )
      AddMsgNL("Comando INSERT su archivio %1 ha generato errore su una registrazione.", this , this.w_DBF )
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uscita
    * --- Cancello la cartella temporanea di scompattazione Dbf
    if Not DeleteFolder( this.w_DELPATH )
      AddMsgNL("Impossibile cancellare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
    endif
    AddMsgNL("Fine procedura di importazione %1", this , time() )
    if Used("CONTROLLI")
      * --- Chiudo il cursore dei controlli
       
 Select("CONTROLLI") 
 Use
    endif
    * --- Se aperta la maschera di aggiornamento database la chiudo
    if Type("msgwnd")="O"
      msgwnd.Release()
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuove integrit� referenziale
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    * --- Se indice a 0 � una tabella temporanea...
    if this.w_IDXTABLE<>0
      this.w_PHNAME = i_dcx.GetPhTable( i_dcx.GetTableIdx( this.w_CNAME ) , this.w_CODAZI )
      this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
      if this.w_CONN<>0
         
 cp_RemoveReferences( this.w_CNAME , this.w_PHNAME , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) , this.w_CODAZI ) 
 msgwnd.add( AH_MSGFORMAT( "Integrit� cancellate per tabella %1 " , this.w_CNAME ) )
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisco integrit�
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    * --- Se indice a 0 � una tabella temporanea...
    if this.w_IDXTABLE<>0
      this.w_PHNAME = i_dcx.GetPhTable( i_dcx.GetTableIdx( this.w_CNAME ) , this.w_CODAZI )
      this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
      if this.w_CONN<>0
        cp_MakeForeingKey(this.w_CNAME , this.w_PHNAME , this.w_CONN , i_DCX , cp_GetDatabaseType( this.w_CONN) , this.w_CODAZI )
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo integrit� referenziali delle tabelle che mi puntano
    this.w_LoopIFK = 1
    do while this.w_LoopIFK<=i_Dcx.GetFKCount( this.w_CNAME )
      this.w_LTABLE = I_DCX.GetFKTable( this.w_CNAME , this.w_LoopIFK )
      this.w_IDXTABLEIFK = cp_OpenTable( Alltrim(this.w_LTABLE) ,.T.)
      this.w_CONN = i_TableProp[ this.w_IDXTABLEIFK , 3 ] 
      this.w_LCPHTABLE = I_DCX.GetPhTable(I_DCX.GetTableIdx( this.w_LTABLE ), "xxx" )
       
 cp_RemoveReferences( this.w_LTABLE , this.w_LCPHTABLE , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) , this.w_CODAZI ) 
 msgwnd.add( AH_MSGFORMAT( "Integrit� cancellate per tabella %1 " , this.w_LTABLE ) )
      this.w_LoopIFK = this.w_LoopIFK + 1 
    enddo
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisco integrit� referenziali delle tabelle che mi puntano
     
 Local cMsg,xServers[1,3],nServers 
 cMsg=""
    this.w_LoopCIFK = 1
    do while this.w_LoopCIFK<=i_Dcx.GetFKCount( this.w_CNAME )
      this.w_LTABLE = I_DCX.GetFKTable( this.w_CNAME , this.w_LoopCIFK )
      this.w_LCPHTABLE = I_DCX.GetPhTable(I_DCX.GetTableIdx( this.w_LTABLE ), "xxx" )
      this.w_IDXTABLEIFK = cp_OpenTable( Alltrim(this.w_LTABLE) ,.T.)
      this.w_CONN = i_TableProp[ this.w_IDXTABLEIFK , 3 ] 
      this.w_OK = db_CreateTableStruct( this.w_LTABLE , this.w_LCPHTABLE , "" , this.w_Conn , i_DCX , @cMsg,"index",@xServers,@nServers )
      this.w_LoopCIFK = this.w_LoopCIFK + 1 
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
