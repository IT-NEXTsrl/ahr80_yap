* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_afd                                                        *
*              Filtri data registrazione                                       *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_18]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-07                                                      *
* Last revis.: 2014-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_afd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_afd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_afd")
  return

* --- Class definition
define class tgsar_afd as StdPCForm
  Width  = 418
  Height = 472
  Top    = 20
  Left   = 15
  cComment = "Filtri data registrazione"
  cPrg = "gsar_afd"
  HelpContextID=59340649
  add object cnt as tcgsar_afd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_afd as PCContext
  w_FLCODAZI = space(5)
  w_FLNOFLPN = space(1)
  w_FLDATPNP = 0
  w_FLDATPNS = 0
  w_FLNOFLDC = space(1)
  w_FLDATDCP = 0
  w_FLDATDCS = 0
  w_FLNOFLMM = space(1)
  w_FLDATMMP = 0
  w_FLDATMMS = 0
  w_FLNOFLTE = space(1)
  w_FLDATTEP = 0
  w_FLDATTES = 0
  w_FLNOFLCE = space(1)
  w_FLDATCEP = 0
  w_FLDATCES = 0
  w_FLNOFLRT = space(1)
  w_FLDATRTP = 0
  w_FLDATRTS = 0
  w_FLNOFLIM = space(1)
  w_FLDATIMP = 0
  w_FLDATIMS = 0
  w_FLNOFLFA = space(1)
  w_FLDATIFA = 0
  w_FLDATFFA = 0
  w_FLDATCFL = space(8)
  w_FLNOFLSM = space(1)
  w_FLGIOINT = 0
  w_FLNUMDOC = 0
  w_FLDTUCOS = space(8)
  proc Save(oFrom)
    this.w_FLCODAZI = oFrom.w_FLCODAZI
    this.w_FLNOFLPN = oFrom.w_FLNOFLPN
    this.w_FLDATPNP = oFrom.w_FLDATPNP
    this.w_FLDATPNS = oFrom.w_FLDATPNS
    this.w_FLNOFLDC = oFrom.w_FLNOFLDC
    this.w_FLDATDCP = oFrom.w_FLDATDCP
    this.w_FLDATDCS = oFrom.w_FLDATDCS
    this.w_FLNOFLMM = oFrom.w_FLNOFLMM
    this.w_FLDATMMP = oFrom.w_FLDATMMP
    this.w_FLDATMMS = oFrom.w_FLDATMMS
    this.w_FLNOFLTE = oFrom.w_FLNOFLTE
    this.w_FLDATTEP = oFrom.w_FLDATTEP
    this.w_FLDATTES = oFrom.w_FLDATTES
    this.w_FLNOFLCE = oFrom.w_FLNOFLCE
    this.w_FLDATCEP = oFrom.w_FLDATCEP
    this.w_FLDATCES = oFrom.w_FLDATCES
    this.w_FLNOFLRT = oFrom.w_FLNOFLRT
    this.w_FLDATRTP = oFrom.w_FLDATRTP
    this.w_FLDATRTS = oFrom.w_FLDATRTS
    this.w_FLNOFLIM = oFrom.w_FLNOFLIM
    this.w_FLDATIMP = oFrom.w_FLDATIMP
    this.w_FLDATIMS = oFrom.w_FLDATIMS
    this.w_FLNOFLFA = oFrom.w_FLNOFLFA
    this.w_FLDATIFA = oFrom.w_FLDATIFA
    this.w_FLDATFFA = oFrom.w_FLDATFFA
    this.w_FLDATCFL = oFrom.w_FLDATCFL
    this.w_FLNOFLSM = oFrom.w_FLNOFLSM
    this.w_FLGIOINT = oFrom.w_FLGIOINT
    this.w_FLNUMDOC = oFrom.w_FLNUMDOC
    this.w_FLDTUCOS = oFrom.w_FLDTUCOS
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_FLCODAZI = this.w_FLCODAZI
    oTo.w_FLNOFLPN = this.w_FLNOFLPN
    oTo.w_FLDATPNP = this.w_FLDATPNP
    oTo.w_FLDATPNS = this.w_FLDATPNS
    oTo.w_FLNOFLDC = this.w_FLNOFLDC
    oTo.w_FLDATDCP = this.w_FLDATDCP
    oTo.w_FLDATDCS = this.w_FLDATDCS
    oTo.w_FLNOFLMM = this.w_FLNOFLMM
    oTo.w_FLDATMMP = this.w_FLDATMMP
    oTo.w_FLDATMMS = this.w_FLDATMMS
    oTo.w_FLNOFLTE = this.w_FLNOFLTE
    oTo.w_FLDATTEP = this.w_FLDATTEP
    oTo.w_FLDATTES = this.w_FLDATTES
    oTo.w_FLNOFLCE = this.w_FLNOFLCE
    oTo.w_FLDATCEP = this.w_FLDATCEP
    oTo.w_FLDATCES = this.w_FLDATCES
    oTo.w_FLNOFLRT = this.w_FLNOFLRT
    oTo.w_FLDATRTP = this.w_FLDATRTP
    oTo.w_FLDATRTS = this.w_FLDATRTS
    oTo.w_FLNOFLIM = this.w_FLNOFLIM
    oTo.w_FLDATIMP = this.w_FLDATIMP
    oTo.w_FLDATIMS = this.w_FLDATIMS
    oTo.w_FLNOFLFA = this.w_FLNOFLFA
    oTo.w_FLDATIFA = this.w_FLDATIFA
    oTo.w_FLDATFFA = this.w_FLDATFFA
    oTo.w_FLDATCFL = this.w_FLDATCFL
    oTo.w_FLNOFLSM = this.w_FLNOFLSM
    oTo.w_FLGIOINT = this.w_FLGIOINT
    oTo.w_FLNUMDOC = this.w_FLNUMDOC
    oTo.w_FLDTUCOS = this.w_FLDTUCOS
    PCContext::Load(oTo)
enddefine

define class tcgsar_afd as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 418
  Height = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-06-25"
  HelpContextID=59340649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  FILDTREG_IDX = 0
  AZIENDA_IDX = 0
  cFile = "FILDTREG"
  cKeySelect = "FLCODAZI"
  cKeyWhere  = "FLCODAZI=this.w_FLCODAZI"
  cKeyWhereODBC = '"FLCODAZI="+cp_ToStrODBC(this.w_FLCODAZI)';

  cKeyWhereODBCqualified = '"FILDTREG.FLCODAZI="+cp_ToStrODBC(this.w_FLCODAZI)';

  cPrg = "gsar_afd"
  cComment = "Filtri data registrazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FLCODAZI = space(5)
  w_FLNOFLPN = space(1)
  o_FLNOFLPN = space(1)
  w_FLDATPNP = 0
  w_FLDATPNS = 0
  w_FLNOFLDC = space(1)
  o_FLNOFLDC = space(1)
  w_FLDATDCP = 0
  w_FLDATDCS = 0
  w_FLNOFLMM = space(1)
  o_FLNOFLMM = space(1)
  w_FLDATMMP = 0
  w_FLDATMMS = 0
  w_FLNOFLTE = space(1)
  o_FLNOFLTE = space(1)
  w_FLDATTEP = 0
  w_FLDATTES = 0
  w_FLNOFLCE = space(1)
  o_FLNOFLCE = space(1)
  w_FLDATCEP = 0
  w_FLDATCES = 0
  w_FLNOFLRT = space(1)
  o_FLNOFLRT = space(1)
  w_FLDATRTP = 0
  w_FLDATRTS = 0
  w_FLNOFLIM = space(1)
  o_FLNOFLIM = space(1)
  w_FLDATIMP = 0
  w_FLDATIMS = 0
  w_FLNOFLFA = space(1)
  o_FLNOFLFA = space(1)
  w_FLDATIFA = 0
  w_FLDATFFA = 0
  w_FLDATCFL = ctod('  /  /  ')
  w_FLNOFLSM = space(1)
  o_FLNOFLSM = space(1)
  w_FLGIOINT = 0
  w_FLNUMDOC = 0
  w_FLDTUCOS = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_afdPag1","gsar_afd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 893194
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLNOFLPN_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='FILDTREG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FILDTREG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FILDTREG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_afd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from FILDTREG where FLCODAZI=KeySet.FLCODAZI
    *
    i_nConn = i_TableProp[this.FILDTREG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FILDTREG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FILDTREG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FILDTREG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FLCODAZI',this.w_FLCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLCODAZI = NVL(FLCODAZI,space(5))
          * evitabile
          *.link_1_1('Load')
        .w_FLNOFLPN = NVL(FLNOFLPN,space(1))
        .w_FLDATPNP = NVL(FLDATPNP,0)
        .w_FLDATPNS = NVL(FLDATPNS,0)
        .w_FLNOFLDC = NVL(FLNOFLDC,space(1))
        .w_FLDATDCP = NVL(FLDATDCP,0)
        .w_FLDATDCS = NVL(FLDATDCS,0)
        .w_FLNOFLMM = NVL(FLNOFLMM,space(1))
        .w_FLDATMMP = NVL(FLDATMMP,0)
        .w_FLDATMMS = NVL(FLDATMMS,0)
        .w_FLNOFLTE = NVL(FLNOFLTE,space(1))
        .w_FLDATTEP = NVL(FLDATTEP,0)
        .w_FLDATTES = NVL(FLDATTES,0)
        .w_FLNOFLCE = NVL(FLNOFLCE,space(1))
        .w_FLDATCEP = NVL(FLDATCEP,0)
        .w_FLDATCES = NVL(FLDATCES,0)
        .w_FLNOFLRT = NVL(FLNOFLRT,space(1))
        .w_FLDATRTP = NVL(FLDATRTP,0)
        .w_FLDATRTS = NVL(FLDATRTS,0)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_FLNOFLIM = NVL(FLNOFLIM,space(1))
        .w_FLDATIMP = NVL(FLDATIMP,0)
        .w_FLDATIMS = NVL(FLDATIMS,0)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(ah_MsgFormat("Rispetto alla data di sistema inserire l'intervallo%0di giorni che si vogliono visualizzare nell'elenco"))
        .w_FLNOFLFA = NVL(FLNOFLFA,space(1))
        .w_FLDATIFA = NVL(FLDATIFA,0)
        .w_FLDATFFA = NVL(FLDATFFA,0)
        .w_FLDATCFL = NVL(cp_ToDate(FLDATCFL),ctod("  /  /  "))
        .w_FLNOFLSM = NVL(FLNOFLSM,space(1))
        .w_FLGIOINT = NVL(FLGIOINT,0)
        .w_FLNUMDOC = NVL(FLNUMDOC,0)
        .w_FLDTUCOS = NVL(cp_ToDate(FLDTUCOS),ctod("  /  /  "))
        cp_LoadRecExtFlds(this,'FILDTREG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_FLCODAZI = space(5)
      .w_FLNOFLPN = space(1)
      .w_FLDATPNP = 0
      .w_FLDATPNS = 0
      .w_FLNOFLDC = space(1)
      .w_FLDATDCP = 0
      .w_FLDATDCS = 0
      .w_FLNOFLMM = space(1)
      .w_FLDATMMP = 0
      .w_FLDATMMS = 0
      .w_FLNOFLTE = space(1)
      .w_FLDATTEP = 0
      .w_FLDATTES = 0
      .w_FLNOFLCE = space(1)
      .w_FLDATCEP = 0
      .w_FLDATCES = 0
      .w_FLNOFLRT = space(1)
      .w_FLDATRTP = 0
      .w_FLDATRTS = 0
      .w_FLNOFLIM = space(1)
      .w_FLDATIMP = 0
      .w_FLDATIMS = 0
      .w_FLNOFLFA = space(1)
      .w_FLDATIFA = 0
      .w_FLDATFFA = 0
      .w_FLDATCFL = ctod("  /  /  ")
      .w_FLNOFLSM = space(1)
      .w_FLGIOINT = 0
      .w_FLNUMDOC = 0
      .w_FLDTUCOS = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_FLCODAZI))
          .link_1_1('Full')
          endif
        .w_FLNOFLPN = IIF(g_COGE<>'S' or empty(.w_FLNOFLPN),'N',.w_FLNOFLPN)
        .w_FLDATPNP = IIF(.w_FLNOFLPN<>'S',0,.w_FLDATPNP)
        .w_FLDATPNS = IIF(.w_FLNOFLPN<>'S',0,.w_FLDATPNP)
        .w_FLNOFLDC = IIF(g_MAGA<>'S' or empty(.w_FLNOFLDC),'N',.w_FLNOFLDC)
        .w_FLDATDCP = IIF(.w_FLNOFLDC<>'S',0,.w_FLDATDCP)
        .w_FLDATDCS = IIF(.w_FLNOFLDC<>'S',0,.w_FLDATDCS)
        .w_FLNOFLMM = IIF(g_MAGA<>'S' or empty(.w_FLNOFLMM),'N',.w_FLNOFLMM)
        .w_FLDATMMP = IIF(.w_FLNOFLMM<>'S',0,.w_FLDATMMP)
        .w_FLDATMMS = IIF(.w_FLNOFLMM<>'S',0,.w_FLDATMMS)
        .w_FLNOFLTE = IIF(g_APPLICATION = "ad hoc ENTERPRISE" ,IIF(g_TESO<>'S' or empty(.w_FLNOFLTE),'N',.w_FLNOFLTE),'N')
        .w_FLDATTEP = IIF(.w_FLNOFLTE<>'S',0,.w_FLDATTEP)
        .w_FLDATTES = IIF(.w_FLNOFLTE<>'S',0,.w_FLDATTES)
        .w_FLNOFLCE = IIF(NOT('CESP' $ UPPER(i_cModules)) or empty(.w_FLNOFLCE),'N',.w_FLNOFLCE)
        .w_FLDATCEP = IIF(.w_FLNOFLCE<>'S',0,.w_FLDATCEP)
        .w_FLDATCES = IIF(.w_FLNOFLCE<>'S',0,.w_FLDATCES)
        .w_FLNOFLRT = IIF(NOT('RITE' $ UPPER(i_cModules)) or empty(.w_FLNOFLRT),'N',.w_FLNOFLRT)
        .w_FLDATRTP = IIF(.w_FLNOFLRT<>'S',0,.w_FLDATRTP)
        .w_FLDATRTS = IIF(.w_FLNOFLRT<>'S',0,.w_FLDATRTS)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_FLNOFLIM = 'N'
        .w_FLDATIMP = IIF(.w_FLNOFLIM<>'S',0,.w_FLDATIMP)
        .w_FLDATIMS = IIF(.w_FLNOFLIM<>'S',0,.w_FLDATIMS)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(ah_MsgFormat("Rispetto alla data di sistema inserire l'intervallo%0di giorni che si vogliono visualizzare nell'elenco"))
        .w_FLNOFLFA = 'N'
        .w_FLDATIFA = IIF(.w_FLNOFLFA<>'S',0,.w_FLDATIFA)
        .w_FLDATFFA = IIF(.w_FLNOFLFA<>'S',0,.w_FLDATFFA)
          .DoRTCalc(26,26,.f.)
        .w_FLNOFLSM = 'N'
        .w_FLGIOINT = IIF(.w_FLNOFLSM<>'S',0,.w_FLGIOINT)
        .w_FLNUMDOC = IIF(.w_FLNOFLSM<>'S',0,.w_FLNUMDOC)
      endif
    endwith
    cp_BlankRecExtFlds(this,'FILDTREG')
    this.DoRTCalc(30,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oFLNOFLPN_1_12.enabled = i_bVal
      .Page1.oPag.oFLDATPNP_1_13.enabled = i_bVal
      .Page1.oPag.oFLDATPNS_1_14.enabled = i_bVal
      .Page1.oPag.oFLNOFLDC_1_15.enabled = i_bVal
      .Page1.oPag.oFLDATDCP_1_16.enabled = i_bVal
      .Page1.oPag.oFLDATDCS_1_17.enabled = i_bVal
      .Page1.oPag.oFLNOFLMM_1_18.enabled = i_bVal
      .Page1.oPag.oFLDATMMP_1_19.enabled = i_bVal
      .Page1.oPag.oFLDATMMS_1_20.enabled = i_bVal
      .Page1.oPag.oFLNOFLTE_1_21.enabled = i_bVal
      .Page1.oPag.oFLDATTEP_1_22.enabled = i_bVal
      .Page1.oPag.oFLDATTES_1_23.enabled = i_bVal
      .Page1.oPag.oFLNOFLCE_1_24.enabled = i_bVal
      .Page1.oPag.oFLDATCEP_1_25.enabled = i_bVal
      .Page1.oPag.oFLDATCES_1_26.enabled = i_bVal
      .Page1.oPag.oFLNOFLRT_1_27.enabled = i_bVal
      .Page1.oPag.oFLDATRTP_1_28.enabled = i_bVal
      .Page1.oPag.oFLDATRTS_1_29.enabled = i_bVal
      .Page1.oPag.oFLNOFLIM_1_34.enabled = i_bVal
      .Page1.oPag.oFLDATIMP_1_35.enabled = i_bVal
      .Page1.oPag.oFLDATIMS_1_36.enabled = i_bVal
      .Page1.oPag.oFLNOFLFA_1_39.enabled = i_bVal
      .Page1.oPag.oFLDATIFA_1_40.enabled = i_bVal
      .Page1.oPag.oFLDATFFA_1_41.enabled = i_bVal
      .Page1.oPag.oFLDATCFL_1_42.enabled = i_bVal
      .Page1.oPag.oFLNOFLSM_1_46.enabled = i_bVal
      .Page1.oPag.oFLGIOINT_1_47.enabled = i_bVal
      .Page1.oPag.oFLNUMDOC_1_48.enabled = i_bVal
      .Page1.oPag.oFLDTUCOS_1_51.enabled = i_bVal
      .Page1.oPag.oBtn_1_31.enabled = .Page1.oPag.oBtn_1_31.mCond()
      .Page1.oPag.oBtn_1_32.enabled = .Page1.oPag.oBtn_1_32.mCond()
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_37.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'FILDTREG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FILDTREG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLCODAZI,"FLCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLPN,"FLNOFLPN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATPNP,"FLDATPNP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATPNS,"FLDATPNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLDC,"FLNOFLDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATDCP,"FLDATDCP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATDCS,"FLDATDCS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLMM,"FLNOFLMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATMMP,"FLDATMMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATMMS,"FLDATMMS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLTE,"FLNOFLTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATTEP,"FLDATTEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATTES,"FLDATTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLCE,"FLNOFLCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATCEP,"FLDATCEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATCES,"FLDATCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLRT,"FLNOFLRT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATRTP,"FLDATRTP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATRTS,"FLDATRTS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLIM,"FLNOFLIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATIMP,"FLDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATIMS,"FLDATIMS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLFA,"FLNOFLFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATIFA,"FLDATIFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATFFA,"FLDATFFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDATCFL,"FLDATCFL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOFLSM,"FLNOFLSM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLGIOINT,"FLGIOINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNUMDOC,"FLNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDTUCOS,"FLDTUCOS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FILDTREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.FILDTREG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into FILDTREG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FILDTREG')
        i_extval=cp_InsertValODBCExtFlds(this,'FILDTREG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FLCODAZI,FLNOFLPN,FLDATPNP,FLDATPNS,FLNOFLDC"+;
                  ",FLDATDCP,FLDATDCS,FLNOFLMM,FLDATMMP,FLDATMMS"+;
                  ",FLNOFLTE,FLDATTEP,FLDATTES,FLNOFLCE,FLDATCEP"+;
                  ",FLDATCES,FLNOFLRT,FLDATRTP,FLDATRTS,FLNOFLIM"+;
                  ",FLDATIMP,FLDATIMS,FLNOFLFA,FLDATIFA,FLDATFFA"+;
                  ",FLDATCFL,FLNOFLSM,FLGIOINT,FLNUMDOC,FLDTUCOS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_FLCODAZI)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLPN)+;
                  ","+cp_ToStrODBC(this.w_FLDATPNP)+;
                  ","+cp_ToStrODBC(this.w_FLDATPNS)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLDC)+;
                  ","+cp_ToStrODBC(this.w_FLDATDCP)+;
                  ","+cp_ToStrODBC(this.w_FLDATDCS)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLMM)+;
                  ","+cp_ToStrODBC(this.w_FLDATMMP)+;
                  ","+cp_ToStrODBC(this.w_FLDATMMS)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLTE)+;
                  ","+cp_ToStrODBC(this.w_FLDATTEP)+;
                  ","+cp_ToStrODBC(this.w_FLDATTES)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLCE)+;
                  ","+cp_ToStrODBC(this.w_FLDATCEP)+;
                  ","+cp_ToStrODBC(this.w_FLDATCES)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLRT)+;
                  ","+cp_ToStrODBC(this.w_FLDATRTP)+;
                  ","+cp_ToStrODBC(this.w_FLDATRTS)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLIM)+;
                  ","+cp_ToStrODBC(this.w_FLDATIMP)+;
                  ","+cp_ToStrODBC(this.w_FLDATIMS)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLFA)+;
                  ","+cp_ToStrODBC(this.w_FLDATIFA)+;
                  ","+cp_ToStrODBC(this.w_FLDATFFA)+;
                  ","+cp_ToStrODBC(this.w_FLDATCFL)+;
                  ","+cp_ToStrODBC(this.w_FLNOFLSM)+;
                  ","+cp_ToStrODBC(this.w_FLGIOINT)+;
                  ","+cp_ToStrODBC(this.w_FLNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_FLDTUCOS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FILDTREG')
        i_extval=cp_InsertValVFPExtFlds(this,'FILDTREG')
        cp_CheckDeletedKey(i_cTable,0,'FLCODAZI',this.w_FLCODAZI)
        INSERT INTO (i_cTable);
              (FLCODAZI,FLNOFLPN,FLDATPNP,FLDATPNS,FLNOFLDC,FLDATDCP,FLDATDCS,FLNOFLMM,FLDATMMP,FLDATMMS,FLNOFLTE,FLDATTEP,FLDATTES,FLNOFLCE,FLDATCEP,FLDATCES,FLNOFLRT,FLDATRTP,FLDATRTS,FLNOFLIM,FLDATIMP,FLDATIMS,FLNOFLFA,FLDATIFA,FLDATFFA,FLDATCFL,FLNOFLSM,FLGIOINT,FLNUMDOC,FLDTUCOS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FLCODAZI;
                  ,this.w_FLNOFLPN;
                  ,this.w_FLDATPNP;
                  ,this.w_FLDATPNS;
                  ,this.w_FLNOFLDC;
                  ,this.w_FLDATDCP;
                  ,this.w_FLDATDCS;
                  ,this.w_FLNOFLMM;
                  ,this.w_FLDATMMP;
                  ,this.w_FLDATMMS;
                  ,this.w_FLNOFLTE;
                  ,this.w_FLDATTEP;
                  ,this.w_FLDATTES;
                  ,this.w_FLNOFLCE;
                  ,this.w_FLDATCEP;
                  ,this.w_FLDATCES;
                  ,this.w_FLNOFLRT;
                  ,this.w_FLDATRTP;
                  ,this.w_FLDATRTS;
                  ,this.w_FLNOFLIM;
                  ,this.w_FLDATIMP;
                  ,this.w_FLDATIMS;
                  ,this.w_FLNOFLFA;
                  ,this.w_FLDATIFA;
                  ,this.w_FLDATFFA;
                  ,this.w_FLDATCFL;
                  ,this.w_FLNOFLSM;
                  ,this.w_FLGIOINT;
                  ,this.w_FLNUMDOC;
                  ,this.w_FLDTUCOS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.FILDTREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.FILDTREG_IDX,i_nConn)
      *
      * update FILDTREG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'FILDTREG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FLNOFLPN="+cp_ToStrODBC(this.w_FLNOFLPN)+;
             ",FLDATPNP="+cp_ToStrODBC(this.w_FLDATPNP)+;
             ",FLDATPNS="+cp_ToStrODBC(this.w_FLDATPNS)+;
             ",FLNOFLDC="+cp_ToStrODBC(this.w_FLNOFLDC)+;
             ",FLDATDCP="+cp_ToStrODBC(this.w_FLDATDCP)+;
             ",FLDATDCS="+cp_ToStrODBC(this.w_FLDATDCS)+;
             ",FLNOFLMM="+cp_ToStrODBC(this.w_FLNOFLMM)+;
             ",FLDATMMP="+cp_ToStrODBC(this.w_FLDATMMP)+;
             ",FLDATMMS="+cp_ToStrODBC(this.w_FLDATMMS)+;
             ",FLNOFLTE="+cp_ToStrODBC(this.w_FLNOFLTE)+;
             ",FLDATTEP="+cp_ToStrODBC(this.w_FLDATTEP)+;
             ",FLDATTES="+cp_ToStrODBC(this.w_FLDATTES)+;
             ",FLNOFLCE="+cp_ToStrODBC(this.w_FLNOFLCE)+;
             ",FLDATCEP="+cp_ToStrODBC(this.w_FLDATCEP)+;
             ",FLDATCES="+cp_ToStrODBC(this.w_FLDATCES)+;
             ",FLNOFLRT="+cp_ToStrODBC(this.w_FLNOFLRT)+;
             ",FLDATRTP="+cp_ToStrODBC(this.w_FLDATRTP)+;
             ",FLDATRTS="+cp_ToStrODBC(this.w_FLDATRTS)+;
             ",FLNOFLIM="+cp_ToStrODBC(this.w_FLNOFLIM)+;
             ",FLDATIMP="+cp_ToStrODBC(this.w_FLDATIMP)+;
             ",FLDATIMS="+cp_ToStrODBC(this.w_FLDATIMS)+;
             ",FLNOFLFA="+cp_ToStrODBC(this.w_FLNOFLFA)+;
             ",FLDATIFA="+cp_ToStrODBC(this.w_FLDATIFA)+;
             ",FLDATFFA="+cp_ToStrODBC(this.w_FLDATFFA)+;
             ",FLDATCFL="+cp_ToStrODBC(this.w_FLDATCFL)+;
             ",FLNOFLSM="+cp_ToStrODBC(this.w_FLNOFLSM)+;
             ",FLGIOINT="+cp_ToStrODBC(this.w_FLGIOINT)+;
             ",FLNUMDOC="+cp_ToStrODBC(this.w_FLNUMDOC)+;
             ",FLDTUCOS="+cp_ToStrODBC(this.w_FLDTUCOS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'FILDTREG')
        i_cWhere = cp_PKFox(i_cTable  ,'FLCODAZI',this.w_FLCODAZI  )
        UPDATE (i_cTable) SET;
              FLNOFLPN=this.w_FLNOFLPN;
             ,FLDATPNP=this.w_FLDATPNP;
             ,FLDATPNS=this.w_FLDATPNS;
             ,FLNOFLDC=this.w_FLNOFLDC;
             ,FLDATDCP=this.w_FLDATDCP;
             ,FLDATDCS=this.w_FLDATDCS;
             ,FLNOFLMM=this.w_FLNOFLMM;
             ,FLDATMMP=this.w_FLDATMMP;
             ,FLDATMMS=this.w_FLDATMMS;
             ,FLNOFLTE=this.w_FLNOFLTE;
             ,FLDATTEP=this.w_FLDATTEP;
             ,FLDATTES=this.w_FLDATTES;
             ,FLNOFLCE=this.w_FLNOFLCE;
             ,FLDATCEP=this.w_FLDATCEP;
             ,FLDATCES=this.w_FLDATCES;
             ,FLNOFLRT=this.w_FLNOFLRT;
             ,FLDATRTP=this.w_FLDATRTP;
             ,FLDATRTS=this.w_FLDATRTS;
             ,FLNOFLIM=this.w_FLNOFLIM;
             ,FLDATIMP=this.w_FLDATIMP;
             ,FLDATIMS=this.w_FLDATIMS;
             ,FLNOFLFA=this.w_FLNOFLFA;
             ,FLDATIFA=this.w_FLDATIFA;
             ,FLDATFFA=this.w_FLDATFFA;
             ,FLDATCFL=this.w_FLDATCFL;
             ,FLNOFLSM=this.w_FLNOFLSM;
             ,FLGIOINT=this.w_FLGIOINT;
             ,FLNUMDOC=this.w_FLNUMDOC;
             ,FLDTUCOS=this.w_FLDTUCOS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FILDTREG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.FILDTREG_IDX,i_nConn)
      *
      * delete FILDTREG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FLCODAZI',this.w_FLCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FILDTREG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FILDTREG_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_FLNOFLPN<>.w_FLNOFLPN
            .w_FLDATPNP = IIF(.w_FLNOFLPN<>'S',0,.w_FLDATPNP)
        endif
        if .o_FLNOFLPN<>.w_FLNOFLPN
            .w_FLDATPNS = IIF(.w_FLNOFLPN<>'S',0,.w_FLDATPNP)
        endif
        .DoRTCalc(5,5,.t.)
        if .o_FLNOFLDC<>.w_FLNOFLDC
            .w_FLDATDCP = IIF(.w_FLNOFLDC<>'S',0,.w_FLDATDCP)
        endif
        if .o_FLNOFLDC<>.w_FLNOFLDC
            .w_FLDATDCS = IIF(.w_FLNOFLDC<>'S',0,.w_FLDATDCS)
        endif
        .DoRTCalc(8,8,.t.)
        if .o_FLNOFLMM<>.w_FLNOFLMM
            .w_FLDATMMP = IIF(.w_FLNOFLMM<>'S',0,.w_FLDATMMP)
        endif
        if .o_FLNOFLMM<>.w_FLNOFLMM
            .w_FLDATMMS = IIF(.w_FLNOFLMM<>'S',0,.w_FLDATMMS)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_FLNOFLTE<>.w_FLNOFLTE
            .w_FLDATTEP = IIF(.w_FLNOFLTE<>'S',0,.w_FLDATTEP)
        endif
        if .o_FLNOFLTE<>.w_FLNOFLTE
            .w_FLDATTES = IIF(.w_FLNOFLTE<>'S',0,.w_FLDATTES)
        endif
        .DoRTCalc(14,14,.t.)
        if .o_FLNOFLCE<>.w_FLNOFLCE
            .w_FLDATCEP = IIF(.w_FLNOFLCE<>'S',0,.w_FLDATCEP)
        endif
        if .o_FLNOFLCE<>.w_FLNOFLCE
            .w_FLDATCES = IIF(.w_FLNOFLCE<>'S',0,.w_FLDATCES)
        endif
        .DoRTCalc(17,17,.t.)
        if .o_FLNOFLRT<>.w_FLNOFLRT
            .w_FLDATRTP = IIF(.w_FLNOFLRT<>'S',0,.w_FLDATRTP)
        endif
        if .o_FLNOFLRT<>.w_FLNOFLRT
            .w_FLDATRTS = IIF(.w_FLNOFLRT<>'S',0,.w_FLDATRTS)
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(20,20,.t.)
        if .o_FLNOFLIM<>.w_FLNOFLIM
            .w_FLDATIMP = IIF(.w_FLNOFLIM<>'S',0,.w_FLDATIMP)
        endif
        if .o_FLNOFLIM<>.w_FLNOFLIM
            .w_FLDATIMS = IIF(.w_FLNOFLIM<>'S',0,.w_FLDATIMS)
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(ah_MsgFormat("Rispetto alla data di sistema inserire l'intervallo%0di giorni che si vogliono visualizzare nell'elenco"))
        .DoRTCalc(23,23,.t.)
        if .o_FLNOFLFA<>.w_FLNOFLFA
            .w_FLDATIFA = IIF(.w_FLNOFLFA<>'S',0,.w_FLDATIFA)
        endif
        if .o_FLNOFLFA<>.w_FLNOFLFA
            .w_FLDATFFA = IIF(.w_FLNOFLFA<>'S',0,.w_FLDATFFA)
        endif
        .DoRTCalc(26,27,.t.)
        if .o_FLNOFLSM<>.w_FLNOFLSM
            .w_FLGIOINT = IIF(.w_FLNOFLSM<>'S',0,.w_FLGIOINT)
        endif
        if .o_FLNOFLSM<>.w_FLNOFLSM
            .w_FLNUMDOC = IIF(.w_FLNOFLSM<>'S',0,.w_FLNUMDOC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(30,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate(ah_MsgFormat("Rispetto alla data di sistema inserire l'intervallo%0di giorni che si vogliono visualizzare nell'elenco"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLNOFLPN_1_12.enabled = this.oPgFrm.Page1.oPag.oFLNOFLPN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFLDATPNP_1_13.enabled = this.oPgFrm.Page1.oPag.oFLDATPNP_1_13.mCond()
    this.oPgFrm.Page1.oPag.oFLDATPNS_1_14.enabled = this.oPgFrm.Page1.oPag.oFLDATPNS_1_14.mCond()
    this.oPgFrm.Page1.oPag.oFLNOFLDC_1_15.enabled = this.oPgFrm.Page1.oPag.oFLNOFLDC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oFLDATDCP_1_16.enabled = this.oPgFrm.Page1.oPag.oFLDATDCP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFLDATDCS_1_17.enabled = this.oPgFrm.Page1.oPag.oFLDATDCS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.enabled = this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.enabled = this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.enabled = this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.mCond()
    this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.enabled = this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.enabled = this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.mCond()
    this.oPgFrm.Page1.oPag.oFLDATTES_1_23.enabled = this.oPgFrm.Page1.oPag.oFLDATTES_1_23.mCond()
    this.oPgFrm.Page1.oPag.oFLNOFLCE_1_24.enabled = this.oPgFrm.Page1.oPag.oFLNOFLCE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oFLDATCEP_1_25.enabled = this.oPgFrm.Page1.oPag.oFLDATCEP_1_25.mCond()
    this.oPgFrm.Page1.oPag.oFLDATCES_1_26.enabled = this.oPgFrm.Page1.oPag.oFLDATCES_1_26.mCond()
    this.oPgFrm.Page1.oPag.oFLNOFLRT_1_27.enabled = this.oPgFrm.Page1.oPag.oFLNOFLRT_1_27.mCond()
    this.oPgFrm.Page1.oPag.oFLDATRTP_1_28.enabled = this.oPgFrm.Page1.oPag.oFLDATRTP_1_28.mCond()
    this.oPgFrm.Page1.oPag.oFLDATRTS_1_29.enabled = this.oPgFrm.Page1.oPag.oFLDATRTS_1_29.mCond()
    this.oPgFrm.Page1.oPag.oFLDATIMP_1_35.enabled = this.oPgFrm.Page1.oPag.oFLDATIMP_1_35.mCond()
    this.oPgFrm.Page1.oPag.oFLDATIMS_1_36.enabled = this.oPgFrm.Page1.oPag.oFLDATIMS_1_36.mCond()
    this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.enabled = this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.mCond()
    this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.enabled = this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.mCond()
    this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.enabled = this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.mCond()
    this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.enabled = this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.visible=!this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.mHide()
    this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.visible=!this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.mHide()
    this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.visible=!this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.visible=!this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.mHide()
    this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.visible=!this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.mHide()
    this.oPgFrm.Page1.oPag.oFLDATTES_1_23.visible=!this.oPgFrm.Page1.oPag.oFLDATTES_1_23.mHide()
    this.oPgFrm.Page1.oPag.oFLNOFLFA_1_39.visible=!this.oPgFrm.Page1.oPag.oFLNOFLFA_1_39.mHide()
    this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.visible=!this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.mHide()
    this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.visible=!this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oFLNOFLSM_1_46.visible=!this.oPgFrm.Page1.oPag.oFLNOFLSM_1_46.mHide()
    this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.visible=!this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.mHide()
    this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.visible=!this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oFLDTUCOS_1_51.visible=!this.oPgFrm.Page1.oPag.oFLDTUCOS_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FLCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_FLCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_FLCODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLNOFLPN_1_12.RadioValue()==this.w_FLNOFLPN)
      this.oPgFrm.Page1.oPag.oFLNOFLPN_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATPNP_1_13.value==this.w_FLDATPNP)
      this.oPgFrm.Page1.oPag.oFLDATPNP_1_13.value=this.w_FLDATPNP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATPNS_1_14.value==this.w_FLDATPNS)
      this.oPgFrm.Page1.oPag.oFLDATPNS_1_14.value=this.w_FLDATPNS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLDC_1_15.RadioValue()==this.w_FLNOFLDC)
      this.oPgFrm.Page1.oPag.oFLNOFLDC_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATDCP_1_16.value==this.w_FLDATDCP)
      this.oPgFrm.Page1.oPag.oFLDATDCP_1_16.value=this.w_FLDATDCP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATDCS_1_17.value==this.w_FLDATDCS)
      this.oPgFrm.Page1.oPag.oFLDATDCS_1_17.value=this.w_FLDATDCS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.RadioValue()==this.w_FLNOFLMM)
      this.oPgFrm.Page1.oPag.oFLNOFLMM_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.value==this.w_FLDATMMP)
      this.oPgFrm.Page1.oPag.oFLDATMMP_1_19.value=this.w_FLDATMMP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.value==this.w_FLDATMMS)
      this.oPgFrm.Page1.oPag.oFLDATMMS_1_20.value=this.w_FLDATMMS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.RadioValue()==this.w_FLNOFLTE)
      this.oPgFrm.Page1.oPag.oFLNOFLTE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.value==this.w_FLDATTEP)
      this.oPgFrm.Page1.oPag.oFLDATTEP_1_22.value=this.w_FLDATTEP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATTES_1_23.value==this.w_FLDATTES)
      this.oPgFrm.Page1.oPag.oFLDATTES_1_23.value=this.w_FLDATTES
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLCE_1_24.RadioValue()==this.w_FLNOFLCE)
      this.oPgFrm.Page1.oPag.oFLNOFLCE_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATCEP_1_25.value==this.w_FLDATCEP)
      this.oPgFrm.Page1.oPag.oFLDATCEP_1_25.value=this.w_FLDATCEP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATCES_1_26.value==this.w_FLDATCES)
      this.oPgFrm.Page1.oPag.oFLDATCES_1_26.value=this.w_FLDATCES
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLRT_1_27.RadioValue()==this.w_FLNOFLRT)
      this.oPgFrm.Page1.oPag.oFLNOFLRT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATRTP_1_28.value==this.w_FLDATRTP)
      this.oPgFrm.Page1.oPag.oFLDATRTP_1_28.value=this.w_FLDATRTP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATRTS_1_29.value==this.w_FLDATRTS)
      this.oPgFrm.Page1.oPag.oFLDATRTS_1_29.value=this.w_FLDATRTS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLIM_1_34.RadioValue()==this.w_FLNOFLIM)
      this.oPgFrm.Page1.oPag.oFLNOFLIM_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATIMP_1_35.value==this.w_FLDATIMP)
      this.oPgFrm.Page1.oPag.oFLDATIMP_1_35.value=this.w_FLDATIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATIMS_1_36.value==this.w_FLDATIMS)
      this.oPgFrm.Page1.oPag.oFLDATIMS_1_36.value=this.w_FLDATIMS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLFA_1_39.RadioValue()==this.w_FLNOFLFA)
      this.oPgFrm.Page1.oPag.oFLNOFLFA_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.value==this.w_FLDATIFA)
      this.oPgFrm.Page1.oPag.oFLDATIFA_1_40.value=this.w_FLDATIFA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.value==this.w_FLDATFFA)
      this.oPgFrm.Page1.oPag.oFLDATFFA_1_41.value=this.w_FLDATFFA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATCFL_1_42.value==this.w_FLDATCFL)
      this.oPgFrm.Page1.oPag.oFLDATCFL_1_42.value=this.w_FLDATCFL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOFLSM_1_46.RadioValue()==this.w_FLNOFLSM)
      this.oPgFrm.Page1.oPag.oFLNOFLSM_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.value==this.w_FLGIOINT)
      this.oPgFrm.Page1.oPag.oFLGIOINT_1_47.value=this.w_FLGIOINT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.value==this.w_FLNUMDOC)
      this.oPgFrm.Page1.oPag.oFLNUMDOC_1_48.value=this.w_FLNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDTUCOS_1_51.value==this.w_FLDTUCOS)
      this.oPgFrm.Page1.oPag.oFLDTUCOS_1_51.value=this.w_FLDTUCOS
    endif
    cp_SetControlsValueExtFlds(this,'FILDTREG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLNOFLPN = this.w_FLNOFLPN
    this.o_FLNOFLDC = this.w_FLNOFLDC
    this.o_FLNOFLMM = this.w_FLNOFLMM
    this.o_FLNOFLTE = this.w_FLNOFLTE
    this.o_FLNOFLCE = this.w_FLNOFLCE
    this.o_FLNOFLRT = this.w_FLNOFLRT
    this.o_FLNOFLIM = this.w_FLNOFLIM
    this.o_FLNOFLFA = this.w_FLNOFLFA
    this.o_FLNOFLSM = this.w_FLNOFLSM
    return

enddefine

* --- Define pages as container
define class tgsar_afdPag1 as StdContainer
  Width  = 414
  height = 472
  stdWidth  = 414
  stdheight = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLNOFLPN_1_12 as StdCombo with uid="PYKMLGYMOI",rtseq=2,rtrep=.f.,left=128,top=37,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per primanota";
    , HelpContextID = 47531612;
    , cFormVar="w_FLNOFLPN",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLPN_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLPN_1_12.GetRadio()
    this.Parent.oContained.w_FLNOFLPN = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLPN_1_12.SetRadio()
    this.Parent.oContained.w_FLNOFLPN=trim(this.Parent.oContained.w_FLNOFLPN)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLPN=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLPN=='S',2,;
      0))
  endfunc

  func oFLNOFLPN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  add object oFLDATPNP_1_13 as StdField with uid="VBYREBKRJF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLDATPNP", cQueryName = "FLDATPNP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 235136602,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=37

  func oFLDATPNP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLPN='S')
    endwith
   endif
  endfunc

  add object oFLDATPNS_1_14 as StdField with uid="DXZRHDMIEW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FLDATPNS", cQueryName = "FLDATPNS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 235136599,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=37

  func oFLDATPNS_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLPN='S')
    endwith
   endif
  endfunc


  add object oFLNOFLDC_1_15 as StdCombo with uid="UINHEVQBBL",rtseq=5,rtrep=.f.,left=128,top=66,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per documenti";
    , HelpContextID = 220903833;
    , cFormVar="w_FLNOFLDC",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLDC_1_15.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLDC_1_15.GetRadio()
    this.Parent.oContained.w_FLNOFLDC = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLDC_1_15.SetRadio()
    this.Parent.oContained.w_FLNOFLDC=trim(this.Parent.oContained.w_FLNOFLDC)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLDC=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLDC=='S',2,;
      0))
  endfunc

  func oFLNOFLDC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MAGA='S')
    endwith
   endif
  endfunc

  add object oFLDATDCP_1_16 as StdField with uid="MPCGPTKHIH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FLDATDCP", cQueryName = "FLDATDCP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 100407718,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=66

  func oFLDATDCP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLDC='S')
    endwith
   endif
  endfunc

  add object oFLDATDCS_1_17 as StdField with uid="CYPTHIMTZP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FLDATDCS", cQueryName = "FLDATDCS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 100407721,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=66

  func oFLDATDCS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLDC='S')
    endwith
   endif
  endfunc


  add object oFLNOFLMM_1_18 as StdCombo with uid="TVNICGTXXY",rtseq=8,rtrep=.f.,left=128,top=95,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per movimenti di magazzino";
    , HelpContextID = 47531613;
    , cFormVar="w_FLNOFLMM",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLMM_1_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLMM_1_18.GetRadio()
    this.Parent.oContained.w_FLNOFLMM = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLMM_1_18.SetRadio()
    this.Parent.oContained.w_FLNOFLMM=trim(this.Parent.oContained.w_FLNOFLMM)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLMM=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLMM=='S',2,;
      0))
  endfunc

  func oFLNOFLMM_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MAGA='S')
    endwith
   endif
  endfunc

  func oFLNOFLMM_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDATMMP_1_19 as StdField with uid="GQNVPYSJCJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FLDATMMP", cQueryName = "FLDATMMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 17032794,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=95

  func oFLDATMMP_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLMM='S')
    endwith
   endif
  endfunc

  func oFLDATMMP_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDATMMS_1_20 as StdField with uid="KGBXJCRWYQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FLDATMMS", cQueryName = "FLDATMMS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 17032791,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=95

  func oFLDATMMS_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLMM='S')
    endwith
   endif
  endfunc

  func oFLDATMMS_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oFLNOFLTE_1_21 as StdCombo with uid="WMGZLVVEOW",rtseq=11,rtrep=.f.,left=128,top=124,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per movimenti di tesoreria";
    , HelpContextID = 220903835;
    , cFormVar="w_FLNOFLTE",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLTE_1_21.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLTE_1_21.GetRadio()
    this.Parent.oContained.w_FLNOFLTE = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLTE_1_21.SetRadio()
    this.Parent.oContained.w_FLNOFLTE=trim(this.Parent.oContained.w_FLNOFLTE)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLTE=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLTE=='S',2,;
      0))
  endfunc

  func oFLNOFLTE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" And g_TESO='S')
    endwith
   endif
  endfunc

  func oFLNOFLTE_1_21.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oFLDATTEP_1_22 as StdField with uid="EQVPEWPALA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FLDATTEP", cQueryName = "FLDATTEP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 100407718,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=124

  func oFLDATTEP_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLTE='S')
    endwith
   endif
  endfunc

  func oFLDATTEP_1_22.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oFLDATTES_1_23 as StdField with uid="XEKESWMJFQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FLDATTES", cQueryName = "FLDATTES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 100407721,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=124

  func oFLDATTES_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLTE='S')
    endwith
   endif
  endfunc

  func oFLDATTES_1_23.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oFLNOFLCE_1_24 as StdCombo with uid="VRXMUNEAWS",rtseq=14,rtrep=.f.,left=128,top=153,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per movimenti cespiti";
    , HelpContextID = 220903835;
    , cFormVar="w_FLNOFLCE",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLCE_1_24.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLCE_1_24.GetRadio()
    this.Parent.oContained.w_FLNOFLCE = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLCE_1_24.SetRadio()
    this.Parent.oContained.w_FLNOFLCE=trim(this.Parent.oContained.w_FLNOFLCE)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLCE=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLCE=='S',2,;
      0))
  endfunc

  func oFLNOFLCE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('CESP' $ UPPER(i_cModules))
    endwith
   endif
  endfunc

  add object oFLDATCEP_1_25 as StdField with uid="BHKKGKODMI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FLDATCEP", cQueryName = "FLDATCEP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 83630502,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=153

  func oFLDATCEP_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLCE='S')
    endwith
   endif
  endfunc

  add object oFLDATCES_1_26 as StdField with uid="HONVTLJEFJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FLDATCES", cQueryName = "FLDATCES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 83630505,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=153

  func oFLDATCES_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLCE='S')
    endwith
   endif
  endfunc


  add object oFLNOFLRT_1_27 as StdCombo with uid="BOUWIXITQH",rtseq=17,rtrep=.f.,left=128,top=182,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per movimenti ritenute";
    , HelpContextID = 220903850;
    , cFormVar="w_FLNOFLRT",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLRT_1_27.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLRT_1_27.GetRadio()
    this.Parent.oContained.w_FLNOFLRT = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLRT_1_27.SetRadio()
    this.Parent.oContained.w_FLNOFLRT=trim(this.Parent.oContained.w_FLNOFLRT)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLRT=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLRT=='S',2,;
      0))
  endfunc

  func oFLNOFLRT_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('RITE' $ UPPER(i_cModules))
    endwith
   endif
  endfunc

  add object oFLDATRTP_1_28 as StdField with uid="MWAFARDIWL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FLDATRTP", cQueryName = "FLDATRTP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 66853286,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=182

  func oFLDATRTP_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLRT='S')
    endwith
   endif
  endfunc

  add object oFLDATRTS_1_29 as StdField with uid="WYFXOKJDCE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_FLDATRTS", cQueryName = "FLDATRTS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 66853289,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=182

  func oFLDATRTS_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLRT='S')
    endwith
   endif
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="JVEMJRDEOI",left=460, top=12, width=164,height=18,;
    caption='GSAR_BFD',;
   bGlobalFont=.t.,;
    prg="GSAR_BFD",;
    cEvent = "Update end",;
    nPag=1;
    , ToolTipText = "Valorizza variabili pubbliche";
    , HelpContextID = 79491242


  add object oBtn_1_31 as StdButton with uid="AUOAMTMNJZ",left=304, top=423, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 4185578;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="IRZAPOTGVJ",left=355, top=423, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 52023226;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oFLNOFLIM_1_34 as StdCombo with uid="HBDBCAQATS",rtseq=20,rtrep=.f.,left=128,top=211,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per import documenti";
    , HelpContextID = 47531613;
    , cFormVar="w_FLNOFLIM",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLIM_1_34.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLIM_1_34.GetRadio()
    this.Parent.oContained.w_FLNOFLIM = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLIM_1_34.SetRadio()
    this.Parent.oContained.w_FLNOFLIM=trim(this.Parent.oContained.w_FLNOFLIM)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLIM=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLIM=='S',2,;
      0))
  endfunc

  add object oFLDATIMP_1_35 as StdField with uid="GJCXIUFDHD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FLDATIMP", cQueryName = "FLDATIMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data documento",;
    HelpContextID = 84141658,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=289, Top=211

  func oFLDATIMP_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLIM='S')
    endwith
   endif
  endfunc

  add object oFLDATIMS_1_36 as StdField with uid="VMQBHMWRCY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FLDATIMS", cQueryName = "FLDATIMS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data documento",;
    HelpContextID = 84141655,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=347, Top=211

  func oFLDATIMS_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLIM='S')
    endwith
   endif
  endfunc


  add object oObj_1_37 as cp_calclbl with uid="YUITSVTACH",left=6, top=424, width=240,height=42,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=8,fontUnderline=.f.,bGlobalFont=.t.,fontItalic=.f.,alignment=1,fontname="Arial",fontBold=.f.,;
    nPag=1;
    , HelpContextID = 118656998


  add object oFLNOFLFA_1_39 as StdCombo with uid="BCFBCSTRXA",rtseq=23,rtrep=.f.,left=128,top=239,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date per fatturazione differita";
    , HelpContextID = 220903831;
    , cFormVar="w_FLNOFLFA",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLFA_1_39.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLFA_1_39.GetRadio()
    this.Parent.oContained.w_FLNOFLFA = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLFA_1_39.SetRadio()
    this.Parent.oContained.w_FLNOFLFA=trim(this.Parent.oContained.w_FLNOFLFA)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLFA=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLFA=='S',2,;
      0))
  endfunc

  func oFLNOFLFA_1_39.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDATIFA_1_40 as StdField with uid="GTIEVZKOSQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FLDATIFA", cQueryName = "FLDATIFA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data documento",;
    HelpContextID = 184293783,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=288, Top=239

  func oFLDATIFA_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLFA='S')
    endwith
   endif
  endfunc

  func oFLDATIFA_1_40.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDATFFA_1_41 as StdField with uid="MHKJTFFVDN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_FLDATFFA", cQueryName = "FLDATFFA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data documento",;
    HelpContextID = 133962135,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=346, Top=239

  func oFLDATFFA_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLFA='S')
    endwith
   endif
  endfunc

  func oFLDATFFA_1_41.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDATCFL_1_42 as StdField with uid="HNOYTGGZEW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_FLDATCFL", cQueryName = "FLDATCFL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio estrazione rate per elaborazione cash flow",;
    HelpContextID = 83630498,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=309, Top=289


  add object oFLNOFLSM_1_46 as StdCombo with uid="BCQFTICDXY",rtseq=27,rtrep=.f.,left=128,top=327,width=146,height=21;
    , ToolTipText = "Check utilizzo intervallo date e  documenti ricostruzione saldi magazzino";
    , HelpContextID = 220903843;
    , cFormVar="w_FLNOFLSM",RowSource=""+"Nessuna selezione,"+"Intervallo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNOFLSM_1_46.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLNOFLSM_1_46.GetRadio()
    this.Parent.oContained.w_FLNOFLSM = this.RadioValue()
    return .t.
  endfunc

  func oFLNOFLSM_1_46.SetRadio()
    this.Parent.oContained.w_FLNOFLSM=trim(this.Parent.oContained.w_FLNOFLSM)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOFLSM=='N',1,;
      iif(this.Parent.oContained.w_FLNOFLSM=='S',2,;
      0))
  endfunc

  func oFLNOFLSM_1_46.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLGIOINT_1_47 as StdField with uid="XDATXAHPKS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_FLGIOINT", cQueryName = "FLGIOINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data odierna (se 0 nessun filtro)",;
    HelpContextID = 88847958,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=288, Top=327

  func oFLGIOINT_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLSM='S')
    endwith
   endif
  endfunc

  func oFLGIOINT_1_47.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLNUMDOC_1_48 as StdField with uid="LUIVTLAKMH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_FLNUMDOC", cQueryName = "FLNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica il numero di documenti da processare per volta (se 0 tutti i documenti)",;
    HelpContextID = 174016103,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=128, Top=355

  func oFLNUMDOC_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNOFLSM='S')
    endwith
   endif
  endfunc

  func oFLNUMDOC_1_48.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLDTUCOS_1_51 as StdField with uid="KVDGDUAPVT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_FLDTUCOS", cQueryName = "FLDTUCOS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data da cui partire per determinare ultimo prezzo costo",;
    HelpContextID = 182511191,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=128, Top=391

  func oFLDTUCOS_1_51.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="JKHKTTJNJI",Visible=.t., Left=24, Top=37,;
    Alignment=1, Width=99, Height=15,;
    Caption="Primanota:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="RSRVWWQFHE",Visible=.t., Left=24, Top=67,;
    Alignment=1, Width=99, Height=15,;
    Caption="Documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="VMTXSRIKAP",Visible=.t., Left=24, Top=96,;
    Alignment=1, Width=99, Height=15,;
    Caption="Mov.magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="XVFXQRFTRF",Visible=.t., Left=24, Top=125,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tesoreria:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="GGIAORZJJV",Visible=.t., Left=24, Top=154,;
    Alignment=1, Width=99, Height=15,;
    Caption="Cespiti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YAWWNBWQAA",Visible=.t., Left=24, Top=183,;
    Alignment=1, Width=99, Height=15,;
    Caption="Ritenute:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WMPYGPXNCW",Visible=.t., Left=128, Top=12,;
    Alignment=0, Width=146, Height=15,;
    Caption="Tipo sel."  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FZYMWHNFBZ",Visible=.t., Left=285, Top=12,;
    Alignment=0, Width=47, Height=15,;
    Caption="N. GG -"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BOXVIPUMFG",Visible=.t., Left=345, Top=12,;
    Alignment=0, Width=50, Height=15,;
    Caption="N. GG +"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="QSDLIXPAJB",Visible=.t., Left=24, Top=212,;
    Alignment=1, Width=99, Height=18,;
    Caption="Import doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="PVJRDYJZJG",Visible=.t., Left=24, Top=239,;
    Alignment=1, Width=99, Height=18,;
    Caption="Gen. Fatt.:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="KJPHPTYUVG",Visible=.t., Left=17, Top=329,;
    Alignment=1, Width=107, Height=18,;
    Caption="Ric. saldi magaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="LTZYPWDWZS",Visible=.t., Left=16, Top=357,;
    Alignment=1, Width=108, Height=18,;
    Caption="Num. Doc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="NYXXYPZNFY",Visible=.t., Left=13, Top=392,;
    Alignment=1, Width=110, Height=18,;
    Caption="Ultimo prezzo costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="GGVUODIGYF",Visible=.t., Left=13, Top=290,;
    Alignment=1, Width=294, Height=18,;
    Caption="Data di inizio estrazione rate per elab. cash flow:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="MPCKQPZOXR",left=114, top=9, width=289,height=23

  add object oBox_1_38 as StdBox with uid="MPEDYLNWDK",left=7, top=323, width=396,height=61

  add object oBox_1_44 as StdBox with uid="ZOGGHAUTIV",left=7, top=30, width=396,height=246

  add object oBox_1_50 as StdBox with uid="WWJWHDTAYQ",left=7, top=387, width=396,height=32

  add object oBox_1_54 as StdBox with uid="YAWFSJBVKE",left=7, top=278, width=396,height=44
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_afd','FILDTREG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FLCODAZI=FILDTREG.FLCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
