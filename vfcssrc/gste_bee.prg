* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bee                                                        *
*              generazione file cbi/sepa                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_99]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-01                                                      *
* Last revis.: 2016-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr,pNomFIL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bee",oParentObject,m.pCODSTR,m.pCODCON,m.pTIPCON,m.pCODRAG,m.pARRKEY,m.pHANDLE,m.pAZIONE,m.pLogErr,m.pNomFIL)
return(i_retval)

define class tgste_bee as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pCODCON = space(15)
  pTIPCON = space(1)
  pCODRAG = space(10)
  pARRKEY = space(10)
  pHANDLE = 0
  pAZIONE = space(1)
  pLogErr = .NULL.
  pNomFIL = space(200)
  w_CODSTR = space(10)
  w_HANDLEFILE = 0
  w_CODELE = space(10)
  w_CODTAB = space(30)
  w_INDICE = 0
  w_PATHED = space(200)
  w_CODAZI = space(5)
  w_NOMFIL = space(200)
  w_CODENT = space(15)
  w_FLPRIN = space(1)
  w_TIPFIL = space(1)
  w_INDENT = 0
  w_LCODSTR = space(10)
  w_FLATAB = space(15)
  w_CODNOR = space(10)
  w_INDPROG = 0
  w_PROGRE = space(14)
  w_CODESE = space(5)
  w_oERRORLOG = .NULL.
  w_SERIAL = space(10)
  w_NUMDOC = 0
  w_NUMPRO = 0
  w_ALFDOC = space(15)
  w_ALFPRO = space(15)
  w_DATREG = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  CODCUC = space(2)
  w_CREAIND = .f.
  * --- WorkFile variables
  CONTROPA_idx=0
  VASTRUTT_idx=0
  ENT_DETT_idx=0
  DOC_MAST_idx=0
  VAELEMEN_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da gestioni che creano file EDI
    *     1. GSVA_BGF
    * --- Codice struttura
    * --- Codice intestatario
    * --- Tipo intestatario
    * --- Codice raggruppamento
    * --- Array chiavi
    * --- riferimento file
    * --- Azione File
    *     U Creazione file unico multidato
    *     M Creazione multipla di file non multidato
    *     S Creazione singolo file non multidato
    * --- controllo per uscire da eventuli loop
    this.w_CODSTR = this.pCODSTR
    this.w_LCODSTR = this.pCODSTR
    this.w_CODAZI = i_CODAZI
    this.w_FLPRIN = "N"
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STNOMFIL,STCODENT,STTIPFIL,STINDENT,STFLATAB,STCODNOR"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STNOMFIL,STCODENT,STTIPFIL,STINDENT,STFLATAB,STCODNOR;
        from (i_cTable) where;
            STCODICE = this.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOMFIL = NVL(cp_ToDate(_read_.STNOMFIL),cp_NullValue(_read_.STNOMFIL))
      this.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_INDENT = NVL(cp_ToDate(_read_.STINDENT),cp_NullValue(_read_.STINDENT))
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      this.w_CODNOR = NVL(cp_ToDate(_read_.STCODNOR),cp_NullValue(_read_.STCODNOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not empty(this.w_CODNOR)
      vq_exec("query\Normal",this,"NORMA")
    endif
     
 righe="alen(parrkey,1)" 
 colon="alen(parrkey,2)" 
 dimension arrkey(&righe,&colon) 
 array="parrkey" 
 acopy(&array,arrkey) 
 CountSegm=0 
 CountRows=0 
 FileStream=" " 
    * --- Determino Tabella Principale nell'entit�
    * --- Select from ENT_DETT
    i_nConn=i_TableProp[this.ENT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_DETT_idx,2],.t.,this.ENT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select EN_TABLE,ENPRINCI  from "+i_cTable+" ENT_DETT ";
          +" where ENPRINCI='S'";
           ,"_Curs_ENT_DETT")
    else
      select EN_TABLE,ENPRINCI from (i_cTable);
       where ENPRINCI="S";
        into cursor _Curs_ENT_DETT
    endif
    if used('_Curs_ENT_DETT')
      select _Curs_ENT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = _Curs_ENT_DETT.EN_TABLE
      Exit
        select _Curs_ENT_DETT
        continue
      enddo
      use
    endif
    this.w_INDPROG = 1
    * --- Eseguo valorizzazione tabelle TMP_EDI
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = Alltrim(Nvl(_Curs_GSVA1BGE.ELCODTAB,Space(30)))
      ah_Msg("Creazione tabella: %1",.T.,.F.,.F., ALLTRIM(this.w_CODTAB))
       
 Dimension Arprog(this.w_INDPROG,3) 
 Arprog[this.w_INDPROG,1]=this.w_CODTAB 
 Arprog[this.w_INDPROG,2]=0 
 Arprog[this.w_INDPROG,3]=0
      a=creacuredi(this.w_CODSTR,Alltrim(_Curs_gsva1bge.ELCODTAB),@Arrkey)
      this.w_INDPROG = this.w_INDPROG + 1
      * --- a=creacuredi(w_CODSTR,Alltrim(gsva1bge->ELCODTAB),@Arrkey)
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    if this.pAZIONE $ "S-M"
      this.w_HANDLEFILE = -1
       
 CountSect=0 
 CountIndent=0 
 TestRiga=.t.
      this.w_NOMFIL = this.pNomFIL
      if Not cp_Fileexist(Alltrim(this.w_NOMFIL)) 
        if cp_Fileexist(Alltrim(this.w_NOMFIL))
          Delete file Alltrim(this.w_NOMFIL)
        endif
        this.w_HANDLEFILE = FCREATE(Alltrim(this.w_NOMFIL))
      endif
      if this.w_HANDLEFILE<0
        i_retcode = 'stop'
        return
      endif
      Predi=" "
    endif
    GesProgBar("M", this, 0,100, ah_msgFormat("Generazione file CBI/SEPA"))
    GesProgBar("S", this, 1, 100)
    if this.pAZIONE $ "S-M"
      * --- Eseguo scruttura elementi di apertura file
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      * --- Select from GSVA0BVS
      do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
      if used('_Curs_GSVA0BVS')
        select _Curs_GSVA0BVS
        locate for 1=1
        do while not(eof())
        this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
         
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"A","",this.w_INDENT,0,@Arprog)
          select _Curs_GSVA0BVS
          continue
        enddo
        use
      endif
    else
      this.w_oERRORLOG = this.pLogErr
      this.w_HANDLEFILE = this.pHANDLE
    endif
    * --- Cerco eventuale elemento stream per creare file
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODCUC"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODCUC;
        from (i_cTable) where;
            ANTIPCON = this.pTIPCON;
            and ANCODICE = this.pCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_CODCUC = NVL(cp_ToDate(_read_.ANCODCUC),cp_NullValue(_read_.ANCODCUC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TIPFIL="X"
      * --- Prima era cos�  IIF(w_CODCUC='02',.T.,.F.)
      this.w_CREAIND = .T.
      * --- Select from VAELEMEN
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB  from "+i_cTable+" VAELEMEN ";
            +" where ELCODSTR="+cp_ToStrODBC(this.pCODSTR)+" AND EL__TIPO='L'";
             ,"_Curs_VAELEMEN")
      else
        select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB from (i_cTable);
         where ELCODSTR=this.pCODSTR AND EL__TIPO="L";
          into cursor _Curs_VAELEMEN
      endif
      if used('_Curs_VAELEMEN')
        select _Curs_VAELEMEN
        locate for 1=1
        do while not(eof())
         
 Filestream=gsar_bgi(null,_Curs_VAELEMEN.ELCODCLA,Alltrim(_Curs_VAELEMEN.ELCODTAB),ARRKEY[1,1],this.w_CREAIND)
        exit
          select _Curs_VAELEMEN
          continue
        enddo
        use
      endif
    endif
    vq_exec("QUERY\GSARTBGE",this,"TMPNODI")
     Index On ELCODPAD Tag idx1 collate "MACHINE" 
    GesProgBar("S", this, 100, 100)
    * --- Select from GSVA0BVS
    do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
    if used('_Curs_GSVA0BVS')
      select _Curs_GSVA0BVS
      locate for 1=1
      do while not(eof())
      this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
       
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"","",this.w_INDENT,0,@Arprog,.t.)
        select _Curs_GSVA0BVS
        continue
      enddo
      use
    endif
    GesProgBar("Q", this)
    if Used("TMPNODI")
      Select TMPNODI 
 Use
    endif
    if Used("NORMA")
      Select NORMA 
 Use
    endif
    if this.pAZIONE $ "S-M"
      * --- Eseguo scruttura elementi di chiusura file
      * --- Select from GSVA0BVS
      do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
      if used('_Curs_GSVA0BVS')
        select _Curs_GSVA0BVS
        locate for 1=1
        do while not(eof())
        this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
         
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"C","",this.w_INDENT,0,@Arprog)
          select _Curs_GSVA0BVS
          continue
        enddo
        use
      endif
    endif
    * --- Chiudo cursori tabelle utilizzate
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      if USED(alltrim(_Curs_gsva1bge.ELCODTAB))
        ah_Msg("Eliminazione tabella: %1",.T.,.F.,.F., alltrim(_Curs_gsva1bge.ELCODTAB))
         
 select(alltrim(_Curs_gsva1bge.ELCODTAB)) 
 USE
      endif
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    if this.pAZIONE $ "S-M"
      if this.pAZIONE = "S"
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
      endif
      if not (FCLOSE(this.w_HANDLEFILE))
        ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_NOMFIL)
      endif
    endif
  endproc


  proc Init(oParentObject,pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr,pNomFIL)
    this.pCODSTR=pCODSTR
    this.pCODCON=pCODCON
    this.pTIPCON=pTIPCON
    this.pCODRAG=pCODRAG
    this.pARRKEY=pARRKEY
    this.pHANDLE=pHANDLE
    this.pAZIONE=pAZIONE
    this.pLogErr=pLogErr
    this.pNomFIL=pNomFIL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='ENT_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='VAELEMEN'
    this.cWorkTables[6]='CONTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ENT_DETT')
      use in _Curs_ENT_DETT
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_VAELEMEN')
      use in _Curs_VAELEMEN
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gste_bee
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr,pNomFIL"
endproc
