* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spv                                                        *
*              Importi contabili                                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-07                                                      *
* Last revis.: 2008-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spv",oParentObject))

* --- Class definition
define class tgscg_spv as StdForm
  Top    = 22
  Left   = 81

  * --- Standard Properties
  Width  = 407
  Height = 120
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-01-24"
  HelpContextID=141174121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  cPrg = "gscg_spv"
  cComment = "Importi contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DEC = 0
  w_TIPREG = space(1)
  w_CAOVAL = 0
  w_VALNAZ = space(3)
  w_DATRIF = ctod('  /  /  ')
  w_CONFERMA = .F.
  w_VALNAZ = space(3)
  w_CODVAL = space(5)
  w_IMPOVAL = 0
  o_IMPOVAL = 0
  w_IMPORTO = 0
  w_CAONAZ = 0
  w_TEMPN = 0
  w_TEST = .F.
  w_pTipo = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_spvPag1","gscg_spv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMPOVAL_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DEC=0
      .w_TIPREG=space(1)
      .w_CAOVAL=0
      .w_VALNAZ=space(3)
      .w_DATRIF=ctod("  /  /  ")
      .w_CONFERMA=.f.
      .w_VALNAZ=space(3)
      .w_CODVAL=space(5)
      .w_IMPOVAL=0
      .w_IMPORTO=0
      .w_CAONAZ=0
      .w_TEMPN=0
      .w_TEST=.f.
      .w_pTipo=space(10)
      .w_DEC=oParentObject.w_DEC
      .w_TIPREG=oParentObject.w_TIPREG
      .w_CAOVAL=oParentObject.w_CAOVAL
      .w_VALNAZ=oParentObject.w_VALNAZ
      .w_DATRIF=oParentObject.w_DATRIF
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_VALNAZ=oParentObject.w_VALNAZ
      .w_CODVAL=oParentObject.w_CODVAL
      .w_IMPOVAL=oParentObject.w_IMPOVAL
      .w_IMPORTO=oParentObject.w_IMPORTO
      .w_CAONAZ=oParentObject.w_CAONAZ
      .w_pTipo=oParentObject.w_pTipo
          .DoRTCalc(1,5,.f.)
        .w_CONFERMA = .T.
          .DoRTCalc(7,12,.f.)
        .w_TEST = .F.
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DEC=.w_DEC
      .oParentObject.w_TIPREG=.w_TIPREG
      .oParentObject.w_CAOVAL=.w_CAOVAL
      .oParentObject.w_VALNAZ=.w_VALNAZ
      .oParentObject.w_DATRIF=.w_DATRIF
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_VALNAZ=.w_VALNAZ
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_IMPOVAL=.w_IMPOVAL
      .oParentObject.w_IMPORTO=.w_IMPORTO
      .oParentObject.w_CAONAZ=.w_CAONAZ
      .oParentObject.w_pTipo=.w_pTipo
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_IMPOVAL<>.w_IMPOVAL
            .w_IMPORTO = val2mon(.w_IMPOVAL,.w_CAOVAL, .w_CAONAZ, .w_DATRIF, .w_VALNAZ,.w_DEC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DHLIQUEANF()
    with this
          * --- Calcolo importo
          .w_IMPOVAL = IIF(not .w_TEST ,mon2val( iif( .w_PTIPO='DARE' , this.oparentobject.oparentobject .w_PNIMPDAR , this.oparentobject.oparentobject .w_PNIMPAVE ) , .w_CAOVAL, .w_CAONAZ,  .w_DATRIF, .w_VALNAZ,.w_DEC), .w_IMPOVAL)
          .w_IMPORTO = iif( not .w_TEST , val2mon(.w_IMPOVAL,.w_CAOVAL, .w_CAONAZ, .w_DATRIF, .w_VALNAZ,.w_DEC) , .w_IMPORTO )
          .w_TEST = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_IMPOVAL Gotfocus")
          .Calculate_DHLIQUEANF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_8.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_8.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPOVAL_1_9.value==this.w_IMPOVAL)
      this.oPgFrm.Page1.oPag.oIMPOVAL_1_9.value=this.w_IMPOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPORTO_1_10.value==this.w_IMPORTO)
      this.oPgFrm.Page1.oPag.oIMPORTO_1_10.value=this.w_IMPORTO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMPOVAL = this.w_IMPOVAL
    return

enddefine

* --- Define pages as container
define class tgscg_spvPag1 as StdContainer
  Width  = 403
  height = 120
  stdWidth  = 403
  stdheight = 120
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODVAL_1_8 as StdField with uid="QXPLQMXNOT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Valuta cliente/fornitore",;
    HelpContextID = 134245926,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=73, Top=10, InputMask=replicate('X',5)

  add object oIMPOVAL_1_9 as StdField with uid="AQGSXHGGOR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IMPOVAL", cQueryName = "IMPOVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo nella valuta originaria",;
    HelpContextID = 28693370,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=126, Top=10, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oIMPORTO_1_10 as StdField with uid="IXTKAUBLTC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPORTO", cQueryName = "IMPORTO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo nella valuta di conto",;
    HelpContextID = 17443974,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=126, Top=34, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"


  add object oBtn_1_12 as StdButton with uid="YROZAZEHSM",left=291, top=66, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 139698758;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="DWCJHZKGZK",left=342, top=66, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 133856698;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="ECAAOJECVP",Visible=.t., Left=5, Top=10,;
    Alignment=1, Width=65, Height=15,;
    Caption="Importo in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FCZBCJZXPL",Visible=.t., Left=52, Top=34,;
    Alignment=1, Width=69, Height=15,;
    Caption="pari a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BCFJFZDGNW",Visible=.t., Left=271, Top=34,;
    Alignment=0, Width=125, Height=15,;
    Caption="in valuta di conto."  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
