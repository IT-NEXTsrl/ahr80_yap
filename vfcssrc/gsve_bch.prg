* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bch                                                        *
*              Check controllo validit� colli                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-06                                                      *
* Last revis.: 2001-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bch",oParentObject)
return(i_retval)

define class tgsve_bch as StdBatch
  * --- Local variables
  w_MESS = space(50)
  w_TmpS = space(3)
  * --- WorkFile variables
  CON_COLL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalle maschere GSVE_KDA e GSAC_KDA.
    *     Controllo validit� del Collo selezionato:nelle tipologie colli deve essere presente,
    *      per il collo selezionato, la Confezione legata all'articolo movimentato.
    if Not Empty( this.oParentObject.w_MVCODCOL )
      * --- La READ esegue una semplice interrogazione su Tipologie Colli con filtro
      *     su Codice Collo e codice confezione (derivanti dai documenti).
      *     Se non trova niente significa che per il collo indicato non � specificata la confezione
      *     legata all'articolo.
      * --- Read from CON_COLL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CON_COLL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_COLL_idx,2],.t.,this.CON_COLL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TCCODCON"+;
          " from "+i_cTable+" CON_COLL where ";
              +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOL);
              +" and TCCODCON = "+cp_ToStrODBC(this.oParentObject.w_CODCONF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TCCODCON;
          from (i_cTable) where;
              TCCODICE = this.oParentObject.w_MVCODCOL;
              and TCCODCON = this.oParentObject.w_CODCONF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TmpS = NVL(cp_ToDate(_read_.TCCODCON),cp_NullValue(_read_.TCCODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty( this.w_TmpS )
        ah_ErrorMsg("Codice collo incongruente con l'articolo movimentato")
        this.oParentObject.w_RESCHK = -1
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CON_COLL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
