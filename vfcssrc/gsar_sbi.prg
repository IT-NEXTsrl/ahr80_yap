* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sbi                                                        *
*              Stampa brogliaccio INTRA                                        *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sbi",oParentObject))

* --- Class definition
define class tgsar_sbi as StdForm
  Top    = 33
  Left   = 86

  * --- Standard Properties
  Width  = 531
  Height = 254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-02"
  HelpContextID=107575145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  ELEIMAST_IDX = 0
  ELEIDETT_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  AZDATINT_IDX = 0
  cPrg = "gsar_sbi"
  cComment = "Stampa brogliaccio INTRA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ReadAzie = space(5)
  w_ReadAzieDatInt = space(5)
  w_TIPOA = space(1)
  w_TIPOC = space(1)
  w_TIPOS = space(1)
  w_TIPOA_S = space(1)
  w_AZAICFPI = space(12)
  w_AZPIVAZI = space(12)
  w_AZINPECE = 0
  w_AZINPEAC = 0
  w_AZNPRGEL = 0
  w_AZINPECS = 0
  w_AZINPEAS = 0
  w_TIPOGENE = space(2)
  w_ANNORIFE = space(4)
  w_TIPOCESS = space(1)
  w_PERICESS = 0
  w_TIPOACQU = space(1)
  w_PERIACQU = 0
  w_TIPOCESS_S = space(1)
  w_PERICESS_S = 0
  w_TIPOACQU_S = space(1)
  w_SOGGDELE = space(1)
  o_SOGGDELE = space(1)
  w_PERIACQU_S = 0
  w_PIVAPRES = space(12)
  w_CEMESINI = 0
  w_CEMESFIN = 0
  w_ACMESINI = 0
  w_ACMESFIN = 0
  w_CEMESINI_S = 0
  w_CEMESFIN_S = 0
  w_ACMESINI_S = 0
  w_ACMESFIN_S = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sbiPag1","gsar_sbi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOGENE_1_14
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ELEIMAST'
    this.cWorkTables[3]='ELEIDETT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='AZDATINT'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAR_BBI with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ReadAzie=space(5)
      .w_ReadAzieDatInt=space(5)
      .w_TIPOA=space(1)
      .w_TIPOC=space(1)
      .w_TIPOS=space(1)
      .w_TIPOA_S=space(1)
      .w_AZAICFPI=space(12)
      .w_AZPIVAZI=space(12)
      .w_AZINPECE=0
      .w_AZINPEAC=0
      .w_AZNPRGEL=0
      .w_AZINPECS=0
      .w_AZINPEAS=0
      .w_TIPOGENE=space(2)
      .w_ANNORIFE=space(4)
      .w_TIPOCESS=space(1)
      .w_PERICESS=0
      .w_TIPOACQU=space(1)
      .w_PERIACQU=0
      .w_TIPOCESS_S=space(1)
      .w_PERICESS_S=0
      .w_TIPOACQU_S=space(1)
      .w_SOGGDELE=space(1)
      .w_PERIACQU_S=0
      .w_PIVAPRES=space(12)
      .w_CEMESINI=0
      .w_CEMESFIN=0
      .w_ACMESINI=0
      .w_ACMESFIN=0
      .w_CEMESINI_S=0
      .w_CEMESFIN_S=0
      .w_ACMESINI_S=0
      .w_ACMESFIN_S=0
        .w_ReadAzie = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ReadAzie))
          .link_1_1('Full')
        endif
        .w_ReadAzieDatInt = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ReadAzieDatInt))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,13,.f.)
        .w_TIPOGENE = 'EN'
        .w_ANNORIFE = str(year(i_datsys),4,0)
        .w_TIPOCESS = .w_TIPOC
        .w_PERICESS = .w_AZINPECE+1
        .w_TIPOACQU = .w_TIPOA
        .w_PERIACQU = .w_AZINPEAC+1
        .w_TIPOCESS_S = .w_TIPOS
        .w_PERICESS_S = .w_AZINPECS+1
        .w_TIPOACQU_S = .w_TIPOA_S
        .w_SOGGDELE = 'S'
        .w_PERIACQU_S = .w_AZINPEAS+1
        .w_PIVAPRES = IIF(.w_SOGGDELE<>'S', .w_AZPIVAZI, .w_AZAICFPI)
        .w_CEMESINI = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3)-2 )
        .w_CEMESFIN = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3) )
        .w_ACMESINI = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3)-2 )
        .w_ACMESFIN = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3) )
        .w_CEMESINI_S = iif( .w_TIPOCESS_S="M", .w_PERICESS_S, (.w_PERICESS_S*3)-2 )
        .w_CEMESFIN_S = iif( .w_TIPOCESS_S="M", .w_PERICESS_S, (.w_PERICESS_S*3) )
        .w_ACMESINI_S = iif( .w_TIPOACQU_S="M", .w_PERIACQU_S, (.w_PERIACQU_S*3)-2 )
        .w_ACMESFIN_S = iif( .w_TIPOACQU_S="M", .w_PERIACQU_S, (.w_PERIACQU_S*3) )
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_sbi
     this.NotifyEvent('Valuta')
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,24,.t.)
        if .o_SOGGDELE<>.w_SOGGDELE
            .w_PIVAPRES = IIF(.w_SOGGDELE<>'S', .w_AZPIVAZI, .w_AZAICFPI)
        endif
            .w_CEMESINI = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3)-2 )
            .w_CEMESFIN = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3) )
            .w_ACMESINI = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3)-2 )
            .w_ACMESFIN = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3) )
            .w_CEMESINI_S = iif( .w_TIPOCESS_S="M", .w_PERICESS_S, (.w_PERICESS_S*3)-2 )
            .w_CEMESFIN_S = iif( .w_TIPOCESS_S="M", .w_PERICESS_S, (.w_PERICESS_S*3) )
            .w_ACMESINI_S = iif( .w_TIPOACQU_S="M", .w_PERIACQU_S, (.w_PERIACQU_S*3)-2 )
            .w_ACMESFIN_S = iif( .w_TIPOACQU_S="M", .w_PERIACQU_S, (.w_PERIACQU_S*3) )
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPOCESS_1_16.enabled = this.oPgFrm.Page1.oPag.oTIPOCESS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPERICESS_1_17.enabled = this.oPgFrm.Page1.oPag.oPERICESS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oTIPOACQU_1_18.enabled = this.oPgFrm.Page1.oPag.oTIPOACQU_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPERIACQU_1_19.enabled = this.oPgFrm.Page1.oPag.oPERIACQU_1_19.mCond()
    this.oPgFrm.Page1.oPag.oTIPOCESS_S_1_20.enabled = this.oPgFrm.Page1.oPag.oTIPOCESS_S_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPERICESS_S_1_21.enabled = this.oPgFrm.Page1.oPag.oPERICESS_S_1_21.mCond()
    this.oPgFrm.Page1.oPag.oTIPOACQU_S_1_22.enabled = this.oPgFrm.Page1.oPag.oTIPOACQU_S_1_22.mCond()
    this.oPgFrm.Page1.oPag.oPERIACQU_S_1_24.enabled = this.oPgFrm.Page1.oPag.oPERIACQU_S_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadAzie
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzie) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzie)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZAICFPI,AZNPRGEL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_ReadAzie);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_ReadAzie)
            select AZCODAZI,AZPIVAZI,AZAICFPI,AZNPRGEL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzie = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZAICFPI = NVL(_Link_.AZAICFPI,space(12))
      this.w_AZNPRGEL = NVL(_Link_.AZNPRGEL,0)
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzie = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZAICFPI = space(12)
      this.w_AZNPRGEL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzie Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ReadAzieDatInt
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZDATINT_IDX,3]
    i_lTable = "AZDATINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2], .t., this.AZDATINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzieDatInt) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzieDatInt)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODAZI,ITAITIPE,ITAITIPV,ITAITIPS,ITINPECE,ITINPEAC,ITINPECS,ITAITIAS,ITINPEAS";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODAZI="+cp_ToStrODBC(this.w_ReadAzieDatInt);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODAZI',this.w_ReadAzieDatInt)
            select ITCODAZI,ITAITIPE,ITAITIPV,ITAITIPS,ITINPECE,ITINPEAC,ITINPECS,ITAITIAS,ITINPEAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzieDatInt = NVL(_Link_.ITCODAZI,space(5))
      this.w_TIPOC = NVL(_Link_.ITAITIPE,space(1))
      this.w_TIPOA = NVL(_Link_.ITAITIPV,space(1))
      this.w_TIPOS = NVL(_Link_.ITAITIPS,space(1))
      this.w_AZINPECE = NVL(_Link_.ITINPECE,0)
      this.w_AZINPEAC = NVL(_Link_.ITINPEAC,0)
      this.w_AZINPECS = NVL(_Link_.ITINPECS,0)
      this.w_TIPOA_S = NVL(_Link_.ITAITIAS,space(1))
      this.w_AZINPEAS = NVL(_Link_.ITINPEAS,0)
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzieDatInt = space(5)
      endif
      this.w_TIPOC = space(1)
      this.w_TIPOA = space(1)
      this.w_TIPOS = space(1)
      this.w_AZINPECE = 0
      this.w_AZINPEAC = 0
      this.w_AZINPECS = 0
      this.w_TIPOA_S = space(1)
      this.w_AZINPEAS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])+'\'+cp_ToStr(_Link_.ITCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZDATINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzieDatInt Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOGENE_1_14.RadioValue()==this.w_TIPOGENE)
      this.oPgFrm.Page1.oPag.oTIPOGENE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNORIFE_1_15.value==this.w_ANNORIFE)
      this.oPgFrm.Page1.oPag.oANNORIFE_1_15.value=this.w_ANNORIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCESS_1_16.RadioValue()==this.w_TIPOCESS)
      this.oPgFrm.Page1.oPag.oTIPOCESS_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERICESS_1_17.value==this.w_PERICESS)
      this.oPgFrm.Page1.oPag.oPERICESS_1_17.value=this.w_PERICESS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOACQU_1_18.RadioValue()==this.w_TIPOACQU)
      this.oPgFrm.Page1.oPag.oTIPOACQU_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIACQU_1_19.value==this.w_PERIACQU)
      this.oPgFrm.Page1.oPag.oPERIACQU_1_19.value=this.w_PERIACQU
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCESS_S_1_20.RadioValue()==this.w_TIPOCESS_S)
      this.oPgFrm.Page1.oPag.oTIPOCESS_S_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERICESS_S_1_21.value==this.w_PERICESS_S)
      this.oPgFrm.Page1.oPag.oPERICESS_S_1_21.value=this.w_PERICESS_S
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOACQU_S_1_22.RadioValue()==this.w_TIPOACQU_S)
      this.oPgFrm.Page1.oPag.oTIPOACQU_S_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOGGDELE_1_23.RadioValue()==this.w_SOGGDELE)
      this.oPgFrm.Page1.oPag.oSOGGDELE_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIACQU_S_1_24.value==this.w_PERIACQU_S)
      this.oPgFrm.Page1.oPag.oPERIACQU_S_1_24.value=this.w_PERIACQU_S
    endif
    if not(this.oPgFrm.Page1.oPag.oPIVAPRES_1_25.value==this.w_PIVAPRES)
      this.oPgFrm.Page1.oPag.oPIVAPRES_1_25.value=this.w_PIVAPRES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANNORIFE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNORIFE_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ANNORIFE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERICESS)) or not((.w_PeriCess<5.and..w_TipoCess="T").or.(.w_PeriCess<13.and..w_TipoCess="M")))  and (.w_TIPOGENE$"EN-CE-EB")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERICESS_1_17.SetFocus()
            i_bnoObbl = !empty(.w_PERICESS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIACQU)) or not((.w_PeriAcqu<5.and..w_TipoAcqu="T").or.(.w_PeriAcqu<13.and..w_TipoAcqu="M")))  and (.w_TIPOGENE$"EN-AC-EB")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIACQU_1_19.SetFocus()
            i_bnoObbl = !empty(.w_PERIACQU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERICESS_S)) or not((.w_PeriCess_s<5.and..w_TipoCess_s="T").or.(.w_PeriCess_s<13.and..w_TipoCess_s="M")))  and (.w_TIPOGENE$"EN-CS-ES")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERICESS_S_1_21.SetFocus()
            i_bnoObbl = !empty(.w_PERICESS_S)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIACQU_S)) or not((.w_PeriAcqu_s<5.and..w_TipoAcqu_s="T").or.(.w_PeriAcqu_s<13.and..w_TipoAcqu_s="M")))  and (.w_TIPOGENE$"EN-AS-ES")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIACQU_S_1_24.SetFocus()
            i_bnoObbl = !empty(.w_PERIACQU_S)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SOGGDELE = this.w_SOGGDELE
    return

enddefine

* --- Define pages as container
define class tgsar_sbiPag1 as StdContainer
  Width  = 527
  height = 254
  stdWidth  = 527
  stdheight = 254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOGENE_1_14 as StdCombo with uid="DRKVJQLRWU",rtseq=14,rtrep=.f.,left=143,top=45,width=220,height=21;
    , HelpContextID = 56285051;
    , cFormVar="w_TIPOGENE",RowSource=""+"Acquisti e cessioni UE di beni e servizi,"+"Cessioni di beni UE,"+"Acquisti di beni UE,"+"Acquisti e cessioni di beni UE,"+"Cessioni di servizi UE,"+"Acquisti di servizi UE,"+"Acquisti e cessioni di servizi UE", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOGENE_1_14.RadioValue()
    return(iif(this.value =1,'EN',;
    iif(this.value =2,'CE',;
    iif(this.value =3,'AC',;
    iif(this.value =4,'EB',;
    iif(this.value =5,'CS',;
    iif(this.value =6,'AS',;
    iif(this.value =7,'ES',;
    space(2)))))))))
  endfunc
  func oTIPOGENE_1_14.GetRadio()
    this.Parent.oContained.w_TIPOGENE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOGENE_1_14.SetRadio()
    this.Parent.oContained.w_TIPOGENE=trim(this.Parent.oContained.w_TIPOGENE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOGENE=='EN',1,;
      iif(this.Parent.oContained.w_TIPOGENE=='CE',2,;
      iif(this.Parent.oContained.w_TIPOGENE=='AC',3,;
      iif(this.Parent.oContained.w_TIPOGENE=='EB',4,;
      iif(this.Parent.oContained.w_TIPOGENE=='CS',5,;
      iif(this.Parent.oContained.w_TIPOGENE=='AS',6,;
      iif(this.Parent.oContained.w_TIPOGENE=='ES',7,;
      0)))))))
  endfunc

  add object oANNORIFE_1_15 as StdField with uid="YIAUFXCPPW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANNORIFE", cQueryName = "ANNORIFE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento della generazione",;
    HelpContextID = 133514421,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=461, Top=43, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)


  add object oTIPOCESS_1_16 as StdCombo with uid="NVAAGDJHXM",rtseq=16,rtrep=.f.,left=143,top=74,width=108,height=21;
    , HelpContextID = 52090761;
    , cFormVar="w_TIPOCESS",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCESS_1_16.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOCESS_1_16.GetRadio()
    this.Parent.oContained.w_TIPOCESS = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCESS_1_16.SetRadio()
    this.Parent.oContained.w_TIPOCESS=trim(this.Parent.oContained.w_TIPOCESS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCESS=='M',1,;
      iif(this.Parent.oContained.w_TIPOCESS=='T',2,;
      0))
  endfunc

  func oTIPOCESS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE $ 'EN-CE-EB')
    endwith
   endif
  endfunc

  add object oPERICESS_1_17 as StdField with uid="RVFQYRHFFJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PERICESS", cQueryName = "PERICESS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51704649,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=461, Top=74, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERICESS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE$"EN-CE-EB")
    endwith
   endif
  endfunc

  func oPERICESS_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriCess<5.and..w_TipoCess="T").or.(.w_PeriCess<13.and..w_TipoCess="M"))
    endwith
    return bRes
  endfunc


  add object oTIPOACQU_1_18 as StdCombo with uid="RREMSCVSFP",rtseq=18,rtrep=.f.,left=143,top=105,width=109,height=21;
    , HelpContextID = 16439179;
    , cFormVar="w_TIPOACQU",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOACQU_1_18.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOACQU_1_18.GetRadio()
    this.Parent.oContained.w_TIPOACQU = this.RadioValue()
    return .t.
  endfunc

  func oTIPOACQU_1_18.SetRadio()
    this.Parent.oContained.w_TIPOACQU=trim(this.Parent.oContained.w_TIPOACQU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOACQU=='M',1,;
      iif(this.Parent.oContained.w_TIPOACQU=='T',2,;
      0))
  endfunc

  func oTIPOACQU_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE $ 'EN-AC-EB')
    endwith
   endif
  endfunc

  add object oPERIACQU_1_19 as StdField with uid="XARJSOQYEP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PERIACQU", cQueryName = "PERIACQU",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16053067,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=461, Top=105, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERIACQU_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE$"EN-AC-EB")
    endwith
   endif
  endfunc

  func oPERIACQU_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriAcqu<5.and..w_TipoAcqu="T").or.(.w_PeriAcqu<13.and..w_TipoAcqu="M"))
    endwith
    return bRes
  endfunc


  add object oTIPOCESS_S_1_20 as StdCombo with uid="ZUMPEZYMFJ",rtseq=20,rtrep=.f.,left=143,top=136,width=108,height=21;
    , HelpContextID = 52113529;
    , cFormVar="w_TIPOCESS_S",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCESS_S_1_20.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOCESS_S_1_20.GetRadio()
    this.Parent.oContained.w_TIPOCESS_S = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCESS_S_1_20.SetRadio()
    this.Parent.oContained.w_TIPOCESS_S=trim(this.Parent.oContained.w_TIPOCESS_S)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCESS_S=='M',1,;
      iif(this.Parent.oContained.w_TIPOCESS_S=='T',2,;
      0))
  endfunc

  func oTIPOCESS_S_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE $ 'EN-CS-ES')
    endwith
   endif
  endfunc

  add object oPERICESS_S_1_21 as StdField with uid="QONJLTMMCZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PERICESS_S", cQueryName = "PERICESS_S",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51727417,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=461, Top=136, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERICESS_S_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE$"EN-CS-ES")
    endwith
   endif
  endfunc

  func oPERICESS_S_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriCess_s<5.and..w_TipoCess_s="T").or.(.w_PeriCess_s<13.and..w_TipoCess_s="M"))
    endwith
    return bRes
  endfunc


  add object oTIPOACQU_S_1_22 as StdCombo with uid="ZBKAPKMMUW",rtseq=22,rtrep=.f.,left=143,top=167,width=109,height=21;
    , HelpContextID = 16461947;
    , cFormVar="w_TIPOACQU_S",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOACQU_S_1_22.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOACQU_S_1_22.GetRadio()
    this.Parent.oContained.w_TIPOACQU_S = this.RadioValue()
    return .t.
  endfunc

  func oTIPOACQU_S_1_22.SetRadio()
    this.Parent.oContained.w_TIPOACQU_S=trim(this.Parent.oContained.w_TIPOACQU_S)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOACQU_S=='M',1,;
      iif(this.Parent.oContained.w_TIPOACQU_S=='T',2,;
      0))
  endfunc

  func oTIPOACQU_S_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE $ 'EN-AS-ES')
    endwith
   endif
  endfunc

  add object oSOGGDELE_1_23 as StdCheck with uid="RQSLXMRIXS",rtseq=23,rtrep=.f.,left=143, top=201, caption="Soggetto delegato",;
    HelpContextID = 52579691,;
    cFormVar="w_SOGGDELE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOGGDELE_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOGGDELE_1_23.GetRadio()
    this.Parent.oContained.w_SOGGDELE = this.RadioValue()
    return .t.
  endfunc

  func oSOGGDELE_1_23.SetRadio()
    this.Parent.oContained.w_SOGGDELE=trim(this.Parent.oContained.w_SOGGDELE)
    this.value = ;
      iif(this.Parent.oContained.w_SOGGDELE=='S',1,;
      0)
  endfunc

  add object oPERIACQU_S_1_24 as StdField with uid="JDTKTZNMVL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PERIACQU_S", cQueryName = "PERIACQU_S",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16075835,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=461, Top=167, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERIACQU_S_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOGENE$"EN-AS-ES")
    endwith
   endif
  endfunc

  func oPERIACQU_S_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriAcqu_s<5.and..w_TipoAcqu_s="T").or.(.w_PeriAcqu_s<13.and..w_TipoAcqu_s="M"))
    endwith
    return bRes
  endfunc

  add object oPIVAPRES_1_25 as StdField with uid="EWYIZJAREO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PIVAPRES", cQueryName = "PIVAPRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 253937847,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=143, Top=10, InputMask=replicate('X',12)


  add object oBtn_1_37 as StdButton with uid="QVTYADDHUR",left=414, top=199, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 107546394;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        do GSAR_BBI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_PIvaPres))
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="YGANWFFQSX",left=466, top=199, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100257722;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_26 as StdString with uid="VQOSUHYKYA",Visible=.t., Left=20, Top=10,;
    Alignment=1, Width=117, Height=15,;
    Caption="P.IVA presentatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="INGPWRYEZE",Visible=.t., Left=402, Top=43,;
    Alignment=1, Width=56, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="RLZKXTZDAJ",Visible=.t., Left=402, Top=76,;
    Alignment=1, Width=56, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GFENFDBDCZ",Visible=.t., Left=20, Top=73,;
    Alignment=1, Width=117, Height=18,;
    Caption="Cessioni di beni UE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GQTHHAIISM",Visible=.t., Left=20, Top=104,;
    Alignment=1, Width=117, Height=18,;
    Caption="Acquisti di beni UE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="VNMVIFRGQL",Visible=.t., Left=402, Top=105,;
    Alignment=1, Width=56, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VASMWNTQBD",Visible=.t., Left=20, Top=46,;
    Alignment=1, Width=117, Height=15,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QVTEWOCYGM",Visible=.t., Left=14, Top=136,;
    Alignment=1, Width=123, Height=18,;
    Caption="Cessioni di servizi UE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="JRYJMAGQJE",Visible=.t., Left=21, Top=168,;
    Alignment=1, Width=116, Height=18,;
    Caption="Acquisti di servizi UE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="FGFRCBSJEF",Visible=.t., Left=387, Top=139,;
    Alignment=1, Width=71, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="HXWHKIBQNT",Visible=.t., Left=387, Top=169,;
    Alignment=1, Width=71, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sbi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
