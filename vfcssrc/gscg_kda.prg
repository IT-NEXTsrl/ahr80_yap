* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kda                                                        *
*              Aggiornamento data applicazione                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-07                                                      *
* Last revis.: 2012-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kda",oParentObject))

* --- Class definition
define class tgscg_kda as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 693
  Height = 567
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-27"
  HelpContextID=185981591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gscg_kda"
  cComment = "Aggiornamento data applicazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_TIPO = space(1)
  w_DITIPCON = space(1)
  w_DITIPIVA = space(1)
  w_DICODICE_INI = space(15)
  w_DICODICE_INI = space(15)
  w_DICODICE_FIN = space(15)
  w_DICODICE_FIN = space(15)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_NUMERO1 = 0
  w_SERINI = space(3)
  w_NUMERO2 = 0
  w_SERFIN = space(3)
  w_DIDATAPL = ctod('  /  /  ')
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_TESTSEL = space(1)
  w_TIPOPE = space(1)
  w_ZoomDic_Inte = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdaPag1","gscg_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDICODICE_INI_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDic_Inte = this.oPgFrm.Pages(1).oPag.ZoomDic_Inte
    DoDefault()
    proc Destroy()
      this.w_ZoomDic_Inte = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_DITIPCON=space(1)
      .w_DITIPIVA=space(1)
      .w_DICODICE_INI=space(15)
      .w_DICODICE_INI=space(15)
      .w_DICODICE_FIN=space(15)
      .w_DICODICE_FIN=space(15)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_NUMERO1=0
      .w_SERINI=space(3)
      .w_NUMERO2=0
      .w_SERFIN=space(3)
      .w_DIDATAPL=ctod("  /  /  ")
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_TESTSEL=space(1)
      .w_TIPOPE=space(1)
        .w_OBTEST = Cp_CharToDate('  -  -    ')
        .w_TIPO = 'C'
        .w_DITIPCON = 'C'
        .w_DITIPIVA = 'S'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DICODICE_INI))
          .link_1_7('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_DICODICE_INI))
          .link_1_8('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DICODICE_FIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DICODICE_FIN))
          .link_1_10('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomDic_Inte.Calculate()
          .DoRTCalc(9,18,.f.)
        .w_TIPOPE = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomDic_Inte.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDic_Inte.Calculate()
    endwith
  return

  proc Calculate_HZKZEXDCUL()
    with this
          * --- Aggiorna editabilit� bottone
          GSCG_BDL(this;
              ,'SELX';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDICODICE_INI_1_7.visible=!this.oPgFrm.Page1.oPag.oDICODICE_INI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDICODICE_INI_1_8.visible=!this.oPgFrm.Page1.oPag.oDICODICE_INI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_9.visible=!this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_10.visible=!this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDic_Inte.Event(cEvent)
        if lower(cEvent)==lower("w_zoomdic_inte row checked") or lower(cEvent)==lower("w_zoomdic_inte row unchecked") or lower(cEvent)==lower("zoomdic_inte menucheck")
          .Calculate_HZKZEXDCUL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DICODICE_INI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE_INI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_DICODICE_INI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE_INI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE_INI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDICODICE_INI_1_7'),i_cWhere,'GSAR_ACL',"CLIENTI",'GSAR1ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE_INI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODICE_INI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_DICODICE_INI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE_INI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE_INI = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DICODICE_FIN) OR  .w_DICODICE_INI<=.w_DICODICE_FIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_DICODICE_INI = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE_INI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODICE_INI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE_INI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_DICODICE_INI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE_INI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DICODICE_INI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE_INI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDICODICE_INI_1_8'),i_cWhere,'GSAR_ACL',"CLIENTI",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE_INI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODICE_INI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_DICODICE_INI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE_INI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE_INI = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DICODICE_FIN) OR  .w_DICODICE_INI<=.w_DICODICE_FIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_DICODICE_INI = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE_INI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODICE_FIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE_FIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_DICODICE_FIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE_FIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE_FIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDICODICE_FIN_1_9'),i_cWhere,'GSAR_ACL',"CLIENTI",'GSAR1ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE_FIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODICE_FIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_DICODICE_FIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE_FIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE_FIN = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICODICE_FIN>=.w_DICODICE_INI or empty(.w_DICODICE_INI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_DICODICE_FIN = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE_FIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODICE_FIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE_FIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_DICODICE_FIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE_FIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DICODICE_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE_FIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDICODICE_FIN_1_10'),i_cWhere,'GSAR_ACL',"CLIENTI",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE_FIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DICODICE_FIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_DICODICE_FIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE_FIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE_FIN = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICODICE_FIN>=.w_DICODICE_INI or empty(.w_DICODICE_INI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_DICODICE_FIN = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE_FIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDITIPCON_1_5.RadioValue()==this.w_DITIPCON)
      this.oPgFrm.Page1.oPag.oDITIPCON_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPIVA_1_6.RadioValue()==this.w_DITIPIVA)
      this.oPgFrm.Page1.oPag.oDITIPIVA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODICE_INI_1_7.value==this.w_DICODICE_INI)
      this.oPgFrm.Page1.oPag.oDICODICE_INI_1_7.value=this.w_DICODICE_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODICE_INI_1_8.value==this.w_DICODICE_INI)
      this.oPgFrm.Page1.oPag.oDICODICE_INI_1_8.value=this.w_DICODICE_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_9.value==this.w_DICODICE_FIN)
      this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_9.value=this.w_DICODICE_FIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_10.value==this.w_DICODICE_FIN)
      this.oPgFrm.Page1.oPag.oDICODICE_FIN_1_10.value=this.w_DICODICE_FIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_11.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_11.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_12.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_12.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_13.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_13.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINI_1_14.value==this.w_SERINI)
      this.oPgFrm.Page1.oPag.oSERINI_1_14.value=this.w_SERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO2_1_15.value==this.w_NUMERO2)
      this.oPgFrm.Page1.oPag.oNUMERO2_1_15.value=this.w_NUMERO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSERFIN_1_16.value==this.w_SERFIN)
      this.oPgFrm.Page1.oPag.oSERFIN_1_16.value=this.w_SERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATAPL_1_18.value==this.w_DIDATAPL)
      this.oPgFrm.Page1.oPag.oDIDATAPL_1_18.value=this.w_DIDATAPL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_23.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_23.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_24.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_24.value=this.w_DESCR2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DICODICE_FIN) OR  .w_DICODICE_INI<=.w_DICODICE_FIN)  and not(UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")  and not(empty(.w_DICODICE_INI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_INI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(empty(.w_DICODICE_FIN) OR  .w_DICODICE_INI<=.w_DICODICE_FIN)  and not(UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")  and not(empty(.w_DICODICE_INI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_INI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(.w_DICODICE_FIN>=.w_DICODICE_INI or empty(.w_DICODICE_INI))  and not(UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")  and not(empty(.w_DICODICE_FIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_FIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(.w_DICODICE_FIN>=.w_DICODICE_INI or empty(.w_DICODICE_INI))  and not(UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")  and not(empty(.w_DICODICE_FIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_FIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(empty(.w_DATA2) OR .w_DATA1<=.w_DATA2 OR EMPTY (.w_DATA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_DATA1) OR .w_DATA1<=.w_DATA2 OR EMPTY (.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO1_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intervallo di numeri � vuoto")
          case   not(.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO2_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'intervallo di numeri � vuoto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kdaPag1 as StdContainer
  Width  = 689
  height = 567
  stdWidth  = 689
  stdheight = 567
  resizeXpos=248
  resizeYpos=336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDITIPCON_1_5 as StdCombo with uid="EPYXLYJWXW",rtseq=3,rtrep=.f.,left=111,top=11,width=83,height=22, enabled=.f.;
    , ToolTipText = "Tipo lettera d'intento";
    , HelpContextID = 211523452;
    , cFormVar="w_DITIPCON",RowSource=""+"Ricevute,"+"Emesse", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPCON_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDITIPCON_1_5.GetRadio()
    this.Parent.oContained.w_DITIPCON = this.RadioValue()
    return .t.
  endfunc

  func oDITIPCON_1_5.SetRadio()
    this.Parent.oContained.w_DITIPCON=trim(this.Parent.oContained.w_DITIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPCON=='C',1,;
      iif(this.Parent.oContained.w_DITIPCON=='F',2,;
      0))
  endfunc


  add object oDITIPIVA_1_6 as StdCombo with uid="HBRLLHGGLR",rtseq=4,rtrep=.f.,left=336,top=11,width=139,height=22, enabled=.f.;
    , ToolTipText = "Tipo IVA";
    , HelpContextID = 157575287;
    , cFormVar="w_DITIPIVA",RowSource=""+"No applicazione IVA,"+"Con IVA agevolata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPIVA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oDITIPIVA_1_6.GetRadio()
    this.Parent.oContained.w_DITIPIVA = this.RadioValue()
    return .t.
  endfunc

  func oDITIPIVA_1_6.SetRadio()
    this.Parent.oContained.w_DITIPIVA=trim(this.Parent.oContained.w_DITIPIVA)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPIVA=='S',1,;
      iif(this.Parent.oContained.w_DITIPIVA=='N',2,;
      0))
  endfunc

  add object oDICODICE_INI_1_7 as StdField with uid="MRSFOXHTFW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DICODICE_INI", cQueryName = "DICODICE_INI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Cliente di inizio selezione",;
    HelpContextID = 117995669,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=111, Top=43, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODICE_INI"

  func oDICODICE_INI_1_7.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oDICODICE_INI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_INI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_INI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDICODICE_INI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"CLIENTI",'GSAR1ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oDICODICE_INI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_DICODICE_INI
     i_obj.ecpSave()
  endproc

  add object oDICODICE_INI_1_8 as StdField with uid="ZNAUBMXTQV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DICODICE_INI", cQueryName = "DICODICE_INI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Cliente di inizio selezione",;
    HelpContextID = 117995669,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=111, Top=43, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODICE_INI"

  func oDICODICE_INI_1_8.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oDICODICE_INI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_INI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_INI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDICODICE_INI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"CLIENTI",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oDICODICE_INI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_DICODICE_INI
     i_obj.ecpSave()
  endproc

  add object oDICODICE_FIN_1_9 as StdField with uid="IIYGZMLHDJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DICODICE_FIN", cQueryName = "DICODICE_FIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Cliente di fine selezione",;
    HelpContextID = 117689237,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=111, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODICE_FIN"

  func oDICODICE_FIN_1_9.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oDICODICE_FIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_FIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_FIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDICODICE_FIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"CLIENTI",'GSAR1ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oDICODICE_FIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_DICODICE_FIN
     i_obj.ecpSave()
  endproc

  add object oDICODICE_FIN_1_10 as StdField with uid="HGSAILDEKL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DICODICE_FIN", cQueryName = "DICODICE_FIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Cliente di fine selezione",;
    HelpContextID = 117689237,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=111, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DICODICE_FIN"

  func oDICODICE_FIN_1_10.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oDICODICE_FIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_FIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_FIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDICODICE_FIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"CLIENTI",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oDICODICE_FIN_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_DICODICE_FIN
     i_obj.ecpSave()
  endproc

  add object oDATA1_1_11 as StdField with uid="RANWBBMHQF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data lettere d'intento di inizio selezione",;
    HelpContextID = 241983542,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=111, Top=128

  func oDATA1_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) OR .w_DATA1<=.w_DATA2 OR EMPTY (.w_DATA1))
    endwith
    return bRes
  endfunc

  add object oDATA2_1_12 as StdField with uid="PSGKJCXJTH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data lettera d'intento di fine selezione",;
    HelpContextID = 243032118,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=336, Top=128

  func oDATA2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) OR .w_DATA1<=.w_DATA2 OR EMPTY (.w_DATA2))
    endwith
    return bRes
  endfunc

  add object oNUMERO1_1_13 as StdField with uid="FCYDSGVJQB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'intervallo di numeri � vuoto",;
    ToolTipText = "Numero protocollo lettera d'intento di inizio selezione",;
    HelpContextID = 8387370,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=111, Top=158, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO1_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO2))
    endwith
    return bRes
  endfunc

  add object oSERINI_1_14 as StdField with uid="EFKHLLLAPE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SERINI", cQueryName = "SERINI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 112966362,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=199, Top=158, InputMask=replicate('X',3)

  add object oNUMERO2_1_15 as StdField with uid="TGTYFINBLY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMERO2", cQueryName = "NUMERO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'intervallo di numeri � vuoto",;
    ToolTipText = "Numero protocollo lettera d'intento di fine selezione",;
    HelpContextID = 8387370,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=336, Top=158, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO2_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERO1<=.w_NUMERO2 OR EMPTY(.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oSERFIN_1_16 as StdField with uid="GFPWBOWKHW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SERFIN", cQueryName = "SERFIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 34519770,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=424, Top=158, InputMask=replicate('X',3)


  add object oBtn_1_17 as StdButton with uid="QYBPKEREZO",left=617, top=135, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 70016442;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDIDATAPL_1_18 as StdField with uid="YVMPESXQNQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DIDATAPL", cQueryName = "DIDATAPL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prima applicazione",;
    HelpContextID = 241473406,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=432, Top=519


  add object oBtn_1_19 as StdButton with uid="IRRNBHIDEG",left=562, top=506, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 259943402;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSCG_BDL(this.Parent.oContained,"AGRE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TESTSEL ='S' And Not Empty(.w_DIDATAPL))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="GHGKBVVCTI",left=617, top=506, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 259943402;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCR1_1_23 as StdField with uid="CHLOPGXLBM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243379146,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=254, Top=43, InputMask=replicate('X',40)

  add object oDESCR2_1_24 as StdField with uid="MINMVUHPXL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226601930,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=254, Top=77, InputMask=replicate('X',40)


  add object ZoomDic_Inte as cp_szoombox with uid="HIODHWCYDD",left=-7, top=189, width=687,height=311,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="Dic_Inte",cZoomFile="Gscg_Kda",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 172891674


  add object oBtn_1_37 as StdButton with uid="GQMUPTTTCY",left=5, top=506, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare le dichiarazioni di intento da aggiornare";
    , HelpContextID = 259943402;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSCG_BDL(this.Parent.oContained,"SELS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="ZCYHLDNLVF",left=60, top=506, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare le dichiarazioni di intento da aggiornare";
    , HelpContextID = 259943402;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSCG_BDL(this.Parent.oContained,"SELD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_39 as StdButton with uid="GUQBLQMZBT",left=115, top=506, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione delle dichiarazioni di intento da aggiornare";
    , HelpContextID = 259943402;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSCG_BDL(this.Parent.oContained,"SELI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="CHYKWFCCZI",Visible=.t., Left=61, Top=12,;
    Alignment=1, Width=47, Height=18,;
    Caption="Tipo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="XPICARBJFC",Visible=.t., Left=283, Top=15,;
    Alignment=1, Width=47, Height=18,;
    Caption="IVA:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=7, Top=43,;
    Alignment=1, Width=101, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=7, Top=77,;
    Alignment=1, Width=101, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VRRKIERCIL",Visible=.t., Left=45, Top=131,;
    Alignment=1, Width=63, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="CWEJJFNYEI",Visible=.t., Left=268, Top=131,;
    Alignment=1, Width=62, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ATCMVPYSGD",Visible=.t., Left=22, Top=161,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="IXXJOBDKAP",Visible=.t., Left=254, Top=161,;
    Alignment=1, Width=76, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GGAILYTBLJ",Visible=.t., Left=188, Top=161,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DPPBNNOMRH",Visible=.t., Left=413, Top=161,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="GXRPOEYLPZ",Visible=.t., Left=259, Top=522,;
    Alignment=1, Width=168, Height=18,;
    Caption="Data prima applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BLDKQRIAGX",Visible=.t., Left=51, Top=104,;
    Alignment=1, Width=58, Height=18,;
    Caption="Protocollo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_35 as StdBox with uid="POEKPGSVWF",left=51, top=120, width=408,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
