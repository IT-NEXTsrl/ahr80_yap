* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bsm                                                        *
*              Elabora stampa schede magazzin                                  *
*                                                                              *
*      Author: TAM Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_138]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-07                                                      *
* Last revis.: 2014-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bsm",oParentObject)
return(i_retval)

define class tgsma_bsm as StdBatch
  * --- Local variables
  w_Messaggio = space(10)
  * --- WorkFile variables
  OUTPUTMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa movimenti di magazzino (da GSMA_SSM)
    * --- Variabili caller
    if this.oParentObject.w_VISSALDI="S"
      * --- Richiesta conferma
      this.w_Messaggio = "ATTENZIONE%0L'elaborazione della stampa schede pu� richiedere molto tempo%0in relazione alla dimensione degli archivi%0%0Confermi l'elaborazione?"
      if NOT ah_YesNo(this.w_Messaggio)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Costruisco la tabella temporanea su cui si basa l'output utente
    if this.oParentObject.w_VISSALDI="S"
      * --- Visualizza saldi precedenti
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\gsma9ssm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Non visualizza saldi precedenti
      * --- Create temporary table OUTPUTMP
      i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsmaassm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.OUTPUTMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Variabili da passare al report
    WITH this.oParentObject
    L_DATINI = .w_DATINI
    L_DATFIN = .w_DATFIN
    L_CODINI = .w_CODINI
    L_CODFIN = .w_CODFIN
    L_CODMAG = .w_CODMAG
    L_VISSALDI = .w_VISSALDI
    L_CLASSE = .w_CLASSE
    ENDWITH
    if this.oParentObject.w_CLASSE="S"
      * --- Se attivato il filtro sulla classe ABC devo prima aggiornare il campo 
      *     Prclaabc e poi eliminare quelli di classe C
      * --- Write into OUTPUTMP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MMCODART,MMCODMAG"
        do vq_exec with 'Gsma10ssm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OUTPUTMP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OUTPUTMP.MMCODART = _t2.MMCODART";
                +" and "+"OUTPUTMP.MMCODMAG = _t2.MMCODMAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRCLAABC = _t2.Prclaabc";
            +i_ccchkf;
            +" from "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OUTPUTMP.MMCODART = _t2.MMCODART";
                +" and "+"OUTPUTMP.MMCODMAG = _t2.MMCODMAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP, "+i_cQueryTable+" _t2 set ";
            +"OUTPUTMP.PRCLAABC = _t2.Prclaabc";
            +Iif(Empty(i_ccchkf),"",",OUTPUTMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OUTPUTMP.MMCODART = t2.MMCODART";
                +" and "+"OUTPUTMP.MMCODMAG = t2.MMCODMAG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set (";
            +"PRCLAABC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.Prclaabc";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OUTPUTMP.MMCODART = _t2.MMCODART";
                +" and "+"OUTPUTMP.MMCODMAG = _t2.MMCODMAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OUTPUTMP set ";
            +"PRCLAABC = _t2.Prclaabc";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MMCODART = "+i_cQueryTable+".MMCODART";
                +" and "+i_cTable+".MMCODMAG = "+i_cQueryTable+".MMCODMAG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRCLAABC = (select Prclaabc from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Se voglio una stampa che discrimina per la classe ABC allora lancio il report solo se non ci sono situazioni anomale
      *     Altrimenti lancio un report che evidenzia tali situazioni (classe ABC non valorizzata)
      vq_exec("query\GSMA11SSM",this,"__TMP__")
      if RECCOUNT("__TMP__")>0
        if AH_YESNO("Impossibile eseguire la stampa: esistono articoli con classe ABC non definita%0Si vuole eseguire una stampa delle anomalie riscontrate?")
          cp_chprn("QUERY\gsma2ssm.FRX","",this.oParentObject)
        endif
        * --- Drop temporary table OUTPUTMP
        i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('OUTPUTMP')
        endif
        i_retcode = 'stop'
        return
      else
        * --- Elimino i record con classe ABC valorizzata a C
        * --- Delete from OUTPUTMP
        i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".MMCODART = "+i_cQueryTable+".MMCODART";
                +" and "+i_cTable+".MMCODMAG = "+i_cQueryTable+".MMCODMAG";
        
          do vq_exec with 'Gsma12ssm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    * --- Lancio il report
    vx_exec(""+alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_OREP)+"",this)
    * --- Chiudo il cursore temporaneo
    if used("__TMP__")
       
 Select __TMP__ 
 Use
    endif
    * --- Rilascio la tabella temporanea
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*OUTPUTMP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
