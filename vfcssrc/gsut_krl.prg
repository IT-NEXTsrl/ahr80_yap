* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krl                                                        *
*              Elenco report                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_56]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-13                                                      *
* Last revis.: 2013-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_krl
CLEAR CLASS tgsut_krlPag1
* --- Fine Area Manuale
return(createobject("tgsut_krl",oParentObject))

* --- Class definition
define class tgsut_krl as StdForm
  Top    = 75
  Left   = 48

  * --- Standard Properties
  Width  = 761
  Height = 240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-07"
  HelpContextID=152485015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_krl"
  cComment = "Elenco report"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MOD_SEL = space(1)
  w_SELEZ = space(1)
  o_SELEZ = space(1)
  w_INDICE = 0
  o_INDICE = 0
  w_EXCLWRD = space(1)
  w_SELECTED = 0
  w_MULCHECK = .F.
  w_Zoom_Printer = .NULL.
  w_Zoom_Select = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_krl
  w_oMultiReport=.NULL.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krlPag1","gsut_krl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZ_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom_Printer = this.oPgFrm.Pages(1).oPag.Zoom_Printer
    this.w_Zoom_Select = this.oPgFrm.Pages(1).oPag.Zoom_Select
    DoDefault()
    proc Destroy()
      this.w_Zoom_Printer = .NULL.
      this.w_Zoom_Select = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT_BRL(this,"S", .w_oMultiReport)
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MOD_SEL=space(1)
      .w_SELEZ=space(1)
      .w_INDICE=0
      .w_EXCLWRD=space(1)
      .w_SELECTED=0
      .w_MULCHECK=.f.
      .w_MOD_SEL=oParentObject.w_MOD_SEL
      .w_EXCLWRD=oParentObject.w_EXCLWRD
      .oPgFrm.Page1.oPag.Zoom_Printer.Calculate()
      .oPgFrm.Page1.oPag.Zoom_Select.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_SELEZ = 'N'
        .w_INDICE = IIF(.w_MOD_SEL='M', .w_Zoom_Printer.GetVar('INDICE'), .w_Zoom_Select.GetVar('INDICE'))
          .DoRTCalc(4,5,.f.)
        .w_MULCHECK = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_krl
    *--- Setto oggetto MultiReport
    this.w_oMultiReport=this.oParentObject.w_oMultiReport
    *--- Inizializzo valore w_SELECTED
    this.w_SELECTED=this.oParentObject.w_oMultiReport.GetNumReport()
    *--- Nascondo zoom
    If this.w_MOD_SEL='M'
       this.w_Zoom_Select.visible=.f.
    else
       this.w_Zoom_Printer.visible=.f.
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MOD_SEL=.w_MOD_SEL
      .oParentObject.w_EXCLWRD=.w_EXCLWRD
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom_Printer.Calculate()
        .oPgFrm.Page1.oPag.Zoom_Select.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_INDICE = IIF(.w_MOD_SEL='M', .w_Zoom_Printer.GetVar('INDICE'), .w_Zoom_Select.GetVar('INDICE'))
        if .o_SELEZ<>.w_SELEZ
          .Calculate_TFDRROTDSX()
        endif
        if .o_INDICE<>.w_INDICE
          .Calculate_CDZNELYIQR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom_Printer.Calculate()
        .oPgFrm.Page1.oPag.Zoom_Select.Calculate()
    endwith
  return

  proc Calculate_RDQHOCWIEZ()
    with this
          * --- Calcola zoom
          GSUT_BRL(this;
              ,'Z';
              ,.w_oMultiReport;
             )
    endwith
  endproc
  proc Calculate_GDFKCFUCHH()
    with this
          * --- Cambio stampante al doppio click
          GSUT_BRL(this;
              ,'P';
              ,.w_oMultiReport;
             )
    endwith
  endproc
  proc Calculate_TFDRROTDSX()
    with this
          * --- Selezione/deselezione tutti
          GSUT_BRL(this;
              ,'D';
              ,.w_oMultiReport;
             )
    endwith
  endproc
  proc Calculate_GITFTJUGHY()
    with this
          * --- Seleziono report al doppio click
          .oParentObject.w_RETINDEX = .w_INDICE
          Esci(this;
             )
    endwith
  endproc
  proc Calculate_CDZNELYIQR()
    with this
          * --- Cambio colore zoom_select
          Zoom_Select_color(this;
             )
    endwith
  endproc
  proc Calculate_JQGPIEOYBI()
    with this
          * --- Check su un report (se non si arriva da un multiple check)
      if NOT .w_MULCHECK
          GSUT_BRL(this;
              ,'X';
              ,.w_oMultiReport;
             )
      endif
          .w_MULCHECK = .F.
    endwith
  endproc
  proc Calculate_KUYKTSUJSR()
    with this
          * --- Uncheck su un report (se non si arriva da un multiple check)
      if NOT .w_MULCHECK
          GSUT_BRL(this;
              ,'U';
              ,.w_oMultiReport;
             )
      endif
          .w_MULCHECK = .F.
    endwith
  endproc
  proc Calculate_WRHSDAIQGQ()
    with this
          * --- Selezioni/Deslezioni multiple da tasto destro
          GSUT_BRL(this;
              ,'R';
              ,.w_oMultiReport;
             )
    endwith
  endproc
  proc Calculate_OBDDHCSUEF()
    with this
          * --- Selezioni/Deslezioni multiple da tasto SHIFT
          .w_MULCHECK = .T.
          GSUT_BRL(this;
              ,'R';
              ,.w_oMultiReport;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSELEZ_1_6.visible=!this.oPgFrm.Page1.oPag.oSELEZ_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom_Printer.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoom_Select.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_RDQHOCWIEZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoom_printer selected")
          .Calculate_GDFKCFUCHH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoom_select selected")
          .Calculate_GITFTJUGHY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_CDZNELYIQR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer row checked")
          .Calculate_JQGPIEOYBI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer row unchecked")
          .Calculate_KUYKTSUJSR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer rowcheckall") or lower(cEvent)==lower("w_Zoom_Printer rowcheckfrom") or lower(cEvent)==lower("w_Zoom_Printer rowcheckto") or lower(cEvent)==lower("w_Zoom_Printer rowinvertselection") or lower(cEvent)==lower("w_Zoom_Printer rowuncheckall")
          .Calculate_WRHSDAIQGQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Zoom_Printer multiple rows checked") or lower(cEvent)==lower("w_Zoom_Printer multiple rows unchecked")
          .Calculate_OBDDHCSUEF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_6.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsut_krl
      *--- Testo se ho selezioanto almeno un report
      If This.w_MOD_SEL='M'
         local l_oldArea, l_numXCHK
         l_oldArea=Select()
         Select(This.w_Zoom_Printer.cCursor)
         Count for XCHK=1 to l_numXCHK
         If l_numXCHK=0
           ah_ErrorMsg("Selezionare almeno un report","!")
           i_bRes=.f.
         Endif
         Select(l_oldArea)
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELEZ = this.w_SELEZ
    this.o_INDICE = this.w_INDICE
    return

enddefine

* --- Define pages as container
define class tgsut_krlPag1 as StdContainer
  Width  = 757
  height = 240
  stdWidth  = 757
  stdheight = 240
  resizeXpos=244
  resizeYpos=116
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom_Printer as cp_szoombox with uid="ZHYPWGJHCG",left=3, top=4, width=748,height=156,;
    caption='Zoom_Printer',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cZoomFile="GSUT_KRL",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="AZIENDA",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,;
    nPag=1;
    , ToolTipText = "Doppio click per cambiare la stampante del report selezionato";
    , HelpContextID = 791329


  add object Zoom_Select as cp_zoombox with uid="DGHFRGBCVF",left=12, top=4, width=730,height=156,;
    caption='Zoom_Select',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cZoomFile=IIF(oParentObject.w_EXCEL,"GSUT1KRL","GSUT_KRL"),bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="AZIENDA",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,bRetriveAllRows=.f.,;
    nPag=1;
    , ToolTipText = "Doppio click per selezionare un report";
    , HelpContextID = 226309294


  add object oBtn_1_4 as StdButton with uid="YSRFFHTDMW",left=646, top=189, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 152513766;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="OJYROTBZFC",left=699, top=189, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159802438;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ_1_6 as StdRadio with uid="JBMVWNCPJY",rtseq=2,rtrep=.f.,left=10, top=202, width=134,height=34;
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 251709222
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 251709222
      this.Buttons(2).Top=16
      this.SetAll("Width",132)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZ_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZ_1_6.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_6.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='N',2,;
      0))
  endfunc

  func oSELEZ_1_6.mHide()
    with this.Parent.oContained
      return (.w_MOD_SEL='C')
    endwith
  endfunc


  add object oBtn_1_7 as StdButton with uid="VIPYCQGTTW",left=593, top=189, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cambiare la stampante dei report selezionati";
    , HelpContextID = 250613210;
    , Caption='\<Cambia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSUT_BRL(this.Parent.oContained,"P", .w_oMultiReport)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SELECTED<>0)
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MOD_SEL='C')
     endwith
    endif
  endfunc

  add object oStr_1_12 as StdString with uid="MRMGOYYYRX",Visible=.t., Left=7, Top=164,;
    Alignment=0, Width=195, Height=17,;
    Caption="Report solo testo"    , forecolor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="MGOSZSMGGW",Visible=.t., Left=7, Top=178,;
    Alignment=0, Width=195, Height=17,;
    Caption="Report grafici"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="JPQPHRMMSA",Visible=.t., Left=239, Top=260,;
    Alignment=0, Width=390, Height=18,;
    Caption="Attenzione: gli zoom zoom_printer e zoom_select sono sovrapposti"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="PAADJQXHZD",Visible=.t., Left=575, Top=167,;
    Alignment=0, Width=16, Height=18,;
    Caption="EX"    , backcolor=RGB(90,200,70), forecolor=RGB(90,200,70), BackStyle=1;
  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_EXCLWRD<>'E')
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="SWKTAFPYAD",Visible=.t., Left=601, Top=167,;
    Alignment=0, Width=146, Height=18,;
    Caption="Modello Excel presente"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_EXCLWRD<>'E')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_krl
Procedure Esci(pParent)
   pParent.ecpquit()
EndProc

Procedure Zoom_Select_color(pParent)
   local l_macro,l_i,l_count
   l_count=0
   For l_i=1 to pParent.w_Zoom_Select.nFields
     If pParent.w_Zoom_Select.cFields[l_i]=='EXCLWRD'
       l_count=l_i
       Exit
     Endif
   Next
   If l_count>0
     l_macro=pParent.w_Zoom_Select.grd.columns[l_count].dynamicforecolor
     pParent.w_Zoom_Select.grd.columns[l_count].text1.forecolor=&l_macro
     pParent.w_Zoom_Select.grd.columns[l_count].text1.SelectedForeColor=&l_macro

     l_macro=pParent.w_Zoom_Select.grd.columns[l_count].dynamicbackcolor
     pParent.w_Zoom_Select.grd.columns[l_count].text1.backcolor=&l_macro
     pParent.w_Zoom_Select.grd.columns[l_count].text1.SelectedBackColor=&l_macro
   Endif
Endproc
* --- Fine Area Manuale
