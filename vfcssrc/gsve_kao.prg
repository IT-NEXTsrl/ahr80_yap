* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kao                                                        *
*              Seleziona acconto da ordini                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_27]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-27                                                      *
* Last revis.: 2007-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kao",oParentObject))

* --- Class definition
define class tgsve_kao as StdForm
  Top    = 24
  Left   = 40

  * --- Standard Properties
  Width  = 591
  Height = 295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-12-04"
  HelpContextID=135646615
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_kao"
  cComment = "Seleziona acconto da ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVSERIAL = space(10)
  w_MVACCONT = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_MVCODCON = space(15)
  w_MVTIPCON = space(1)
  w_MVCODVAL = space(3)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_accont = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kaoPag1","gsve_kao",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_accont = this.oPgFrm.Pages(1).oPag.accont
    DoDefault()
    proc Destroy()
      this.w_accont = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVSERIAL=space(10)
      .w_MVACCONT=0
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCODCON=space(15)
      .w_MVTIPCON=space(1)
      .w_MVCODVAL=space(3)
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_MVACCONT=oParentObject.w_MVACCONT
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_CODVAL=oParentObject.w_CODVAL
      .w_CAOVAL=oParentObject.w_CAOVAL
        .w_MVSERIAL = .w_accont.getVar('MVSERIAL')
        .w_MVACCONT = .w_accont.getVar('MVACCONT')
        .w_MVNUMDOC = .w_accont.getVar('MVNUMDOC')
        .w_MVALFDOC = .w_accont.getVar('MVALFDOC')
        .w_MVDATDOC = .w_accont.getVar('MVDATDOC')
      .oPgFrm.Page1.oPag.accont.Calculate()
          .DoRTCalc(6,8,.f.)
        .w_CODVAL = .w_accont.getVar('MVCODVAL')
        .w_CAOVAL = GETCAM(.w_CODVAL, I_DATSYS)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_MVACCONT=.w_MVACCONT
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_CAOVAL=.w_CAOVAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_MVSERIAL = .w_accont.getVar('MVSERIAL')
            .w_MVACCONT = .w_accont.getVar('MVACCONT')
            .w_MVNUMDOC = .w_accont.getVar('MVNUMDOC')
            .w_MVALFDOC = .w_accont.getVar('MVALFDOC')
            .w_MVDATDOC = .w_accont.getVar('MVDATDOC')
        .oPgFrm.Page1.oPag.accont.Calculate()
        .DoRTCalc(6,8,.t.)
            .w_CODVAL = .w_accont.getVar('MVCODVAL')
            .w_CAOVAL = GETCAM(.w_CODVAL, I_DATSYS)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.accont.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.accont.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kaoPag1 as StdContainer
  Width  = 587
  height = 295
  stdWidth  = 587
  stdheight = 295
  resizeXpos=387
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object accont as cp_zoombox with uid="VDIHZMPVNQ",left=1, top=4, width=590,height=237,;
    caption='accont',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_QAO",bOptions=.F.,bAdvOptions=.F.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 211065082


  add object oBtn_1_10 as StdButton with uid="MLSTORCJML",left=472, top=246, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 135675366;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="QRQVOKNATB",left=525, top=246, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermre";
    , HelpContextID = 142964038;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kao','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
