* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_src                                                        *
*              Stampa aree destinazione budget                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-01                                                      *
* Last revis.: 2011-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_src",oParentObject))

* --- Class definition
define class tgsar_src as StdForm
  Top    = 20
  Left   = 67

  * --- Standard Properties
  Width  = 555
  Height = 167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-05"
  HelpContextID=107575145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  PER_ELAB_IDX = 0
  CONTBILC_IDX = 0
  REG_EXTM_IDX = 0
  GRU_REGO_IDX = 0
  STR_ANAC_IDX = 0
  RAGMCENC_IDX = 0
  cPrg = "gsar_src"
  cComment = "Stampa aree destinazione budget"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_CODINI = space(15)
  o_CODINI = space(15)
  w_CODFIN = space(15)
  w_DATAOBSO = ctod('  /  /  ')
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_PDATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_srcPag1","gsar_src",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='PER_ELAB'
    this.cWorkTables[2]='CONTBILC'
    this.cWorkTables[3]='REG_EXTM'
    this.cWorkTables[4]='GRU_REGO'
    this.cWorkTables[5]='STR_ANAC'
    this.cWorkTables[6]='RAGMCENC'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+","+alltrim(THIS.w_OREP)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODINI))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_9('Full')
        endif
          .DoRTCalc(4,6,.f.)
        .w_PDATINIZ = i_datsys
        .w_obsodat1 = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(1,2,.t.)
        if .o_CODINI<>.w_CODINI
          .link_1_9('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAGMCENC_IDX,3]
    i_lTable = "RAGMCENC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2], .t., this.RAGMCENC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_MRC',True,'RAGMCENC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_CODINI))
          select RCCODICE,RCDTOBSO,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('RAGMCENC','*','RCCODICE',cp_AbsName(oSource.parent,'oCODINI_1_2'),i_cWhere,'GSCA_MRC',"AREE DESTINAZIONE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDTOBSO,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_CODINI)
            select RCCODICE,RCDTOBSO,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.RCCODICE,space(15))
      this.w_CODFIN = NVL(_Link_.RCCODICE,space(15))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.RCDTOBSO),ctod("  /  /  "))
      this.w_DESCR1 = NVL(_Link_.RCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_CODFIN = space(15)
      this.w_DATAOBSO = ctod("  /  /  ")
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODFIN) or (.w_CODFIN>=.w_CODINI)) AND (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato o obsoleto")
        endif
        this.w_CODINI = space(15)
        this.w_CODFIN = space(15)
        this.w_DATAOBSO = ctod("  /  /  ")
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RAGMCENC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAGMCENC_IDX,3]
    i_lTable = "RAGMCENC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2], .t., this.RAGMCENC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_MRC',True,'RAGMCENC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_CODFIN))
          select RCCODICE,RCDTOBSO,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('RAGMCENC','*','RCCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_9'),i_cWhere,'GSCA_MRC',"AREE DESTINAZIONE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDTOBSO,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDTOBSO,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_CODFIN)
            select RCCODICE,RCDTOBSO,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.RCCODICE,space(15))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.RCDTOBSO),ctod("  /  /  "))
      this.w_DESCR2 = NVL(_Link_.RCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DATAOBSO = ctod("  /  /  ")
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CODFIN) or (.w_CODFIN>=.w_CODINI) AND (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato o obsoleto")
        endif
        this.w_CODFIN = space(15)
        this.w_DATAOBSO = ctod("  /  /  ")
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RAGMCENC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_2.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_2.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_9.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_9.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_11.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_11.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_12.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_12.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_13.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_13.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_14.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_CODFIN) or (.w_CODFIN>=.w_CODINI)) AND (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato o obsoleto")
          case   not(empty(.w_CODFIN) or (.w_CODFIN>=.w_CODINI) AND (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato o obsoleto")
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    return

enddefine

* --- Define pages as container
define class tgsar_srcPag1 as StdContainer
  Width  = 551
  height = 167
  stdWidth  = 551
  stdheight = 167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_2 as StdField with uid="SNVFWRHPQO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato o obsoleto",;
    ToolTipText = "Codice area di destinazione di inizio selezione",;
    HelpContextID = 138142682,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=116, Top=12, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="RAGMCENC", cZoomOnZoom="GSCA_MRC", oKey_1_1="RCCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RAGMCENC','*','RCCODICE',cp_AbsName(this.parent,'oCODINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_MRC',"AREE DESTINAZIONE",'',this.parent.oContained
  endproc
  proc oCODINI_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_MRC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RCCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc


  add object oObj_1_3 as cp_outputCombo with uid="ZLERXIVPWT",left=116, top=91, width=425,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 70422502


  add object oBtn_1_4 as StdButton with uid="UXMMQJVNYM",left=441, top=117, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 251806230;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="DCQIJUDCNQ",left=492, top=117, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 251806230;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODFIN_1_9 as StdField with uid="UELRVTJOBO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato o obsoleto",;
    ToolTipText = "Codice area di destinazione di fine selezione",;
    HelpContextID = 59696090,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=116, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="RAGMCENC", cZoomOnZoom="GSCA_MRC", oKey_1_1="RCCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RAGMCENC','*','RCCODICE',cp_AbsName(this.parent,'oCODFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_MRC',"AREE DESTINAZIONE",'',this.parent.oContained
  endproc
  proc oCODFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCA_MRC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RCCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCR1_1_11 as StdField with uid="SFSZIPQMEZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 64970,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=250, Top=12, InputMask=replicate('X',40)

  add object oDESCR2_1_12 as StdField with uid="KJZFFWCMJQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251723210,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=250, Top=38, InputMask=replicate('X',40)

  add object oPDATINIZ_1_13 as StdField with uid="HIXCHCOCKE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 58793392,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=116, Top=63

  add object oobsodat1_1_14 as StdCheck with uid="ASFGDGRRXZ",rtseq=8,rtrep=.f.,left=250, top=65, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsolete",;
    HelpContextID = 21832215,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_14.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_14.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oStr_1_6 as StdString with uid="FTALGJEXWD",Visible=.t., Left=47, Top=10,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="AODTUKLQNY",Visible=.t., Left=6, Top=93,;
    Alignment=1, Width=106, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MPOEUSJYIP",Visible=.t., Left=47, Top=35,;
    Alignment=1, Width=65, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QZOPXCXLQT",Visible=.t., Left=11, Top=63,;
    Alignment=1, Width=101, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_src','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
