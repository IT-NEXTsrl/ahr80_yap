* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bim                                                        *
*              Menu contestuale impianti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-02-20                                                      *
* Last revis.: 2014-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bim",oParentObject)
return(i_retval)

define class tgszm_bim as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_IMCODICE = space(10)
  w_CPROWORD = 0
  w_NUMERORIGA = 0
  w_NUMREC = 0
  w_ASVALATT = space(14)
  w_GSAR_MRA = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tasto destro sulla maschera GSAG_KIR (Elenco impianti\componenti)
    * --- Assegno i valori alle variabili
    this.w_IMCODICE = g_oMenu.getbyindexkeyvalue(1)
    this.w_OBJECT = GSAG_MIM()
    this.w_GSAR_MRA = this.w_OBJECT.GSAR_MRA
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.EcpFilter()     
    * --- Valorizzo i campi chiave
    this.w_OBJECT.w_IMCODICE = this.w_IMCODICE
    * --- Carico il record richiesto
    this.w_OBJECT.EcpSave()     
    * --- Mi posiziono sulla riga che ho selezionato
    this.w_CPROWORD = g_oMenu.oparentobject.w_NUMERORIGA
    this.w_OBJECT.Firstrow()     
    this.w_NUMREC = this.w_OBJECT.Search("NVL(t_CPROWORD, 0)="+CP_TOSTR(this.w_CPROWORD) +" And Not Deleted() ")
    if this.w_NUMREC<>-1
      this.w_OBJECT.SetRow(this.w_NUMREC)     
    endif
    this.w_ASVALATT = g_oMenu.oparentobject.w_ASVALATT
    this.w_GSAR_MRA.Firstrow()     
    this.w_NUMREC = this.w_GSAR_MRA.Search("NVL(t_ASVALATT, 0)="+CP_TOSTR(this.w_ASVALATT) +" And Not Deleted() ")
    if this.w_NUMREC<>-1
      this.w_GSAR_MRA.SetRow(this.w_NUMREC)     
    endif
    this.w_OBJECT = NULL
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
