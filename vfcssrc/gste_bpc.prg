* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bpc                                                        *
*              Elimina partite in distinte cancellate                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-06-12                                                      *
* Last revis.: 2015-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bpc",oParentObject)
return(i_retval)

define class tgste_bpc as StdBatch
  * --- Local variables
  w_bUnderTran = .f.
  w_RETVAL = space(254)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina partite in distinte cancellate (GSTE_KCP-GSTE_ADI)
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if !btrserr
      * --- Try
      local bErr_02E9D898
      bErr_02E9D898=bTrsErr
      this.Try_02E9D898()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore in cancellazione partite .%0%1", MESSAGE())
      endif
      bTrsErr=bTrsErr or bErr_02E9D898
      * --- End
    endif
    if this.w_bUnderTran
      * --- FUORI
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    else
      * --- DAGESTIONE
      if Not EMPTY(this.w_RETVAL)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_RETVAL
      endif
    endif
  endproc
  proc Try_02E9D898()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
            +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'gste_kcp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if upper(this.oparentobject.class)="TGSTE_KCP"
      * --- Se non lanciato da manutenzione distinte elimino partite appese da primanota
      * --- Delete from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with 'QUERY\gstepkcp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ah_errormsg("Cancellazione partite terminata: eliminate numero %1 partite appese",48, " ",ALLTRIM(STR(i_rows)))
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
