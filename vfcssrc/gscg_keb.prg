* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_keb                                                        *
*              Generazione file sd�-Basilea2                                   *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_168]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-05                                                      *
* Last revis.: 2013-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_keb",oParentObject))

* --- Class definition
define class tgscg_keb as StdForm
  Top    = 19
  Left   = 19

  * --- Standard Properties
  Width  = 691
  Height = 304
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-21"
  HelpContextID=65676649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=61

  * --- Constant Properties
  _IDX = 0
  TIR_MAST_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  VOCIRICL_IDX = 0
  CONTROPA_IDX = 0
  CONF_INT_IDX = 0
  cPrg = "gscg_keb"
  cComment = "Generazione file sd�-Basilea2"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_GESTIONE = space(1)
  o_GESTIONE = space(1)
  w_codaz1 = space(5)
  o_codaz1 = space(5)
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODUTE = space(10)
  w_CODIDE = space(5)
  w_CODIDA = space(4)
  w_PROATT = space(3)
  w_PATHF = space(200)
  w_TIPFILE = space(4)
  w_PATHB = space(200)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_UTENTE = 0
  w_gestbu = space(1)
  w_ESE = space(4)
  o_ESE = space(4)
  w_ESSALDI = space(1)
  o_ESSALDI = space(1)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_totdat = space(1)
  o_totdat = space(1)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_monete = space(1)
  o_monete = space(1)
  w_divisa = space(3)
  o_divisa = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_simval = space(5)
  w_SBDESCRI = space(35)
  w_PROVVI = space(1)
  o_PROVVI = space(1)
  w_CONFRONTO = space(1)
  w_FLURE = space(2)
  o_FLURE = space(2)
  w_FLURF = space(1)
  o_FLURF = space(1)
  w_CODBUN = space(3)
  w_FLUIQ = space(1)
  w_SUPERBU = space(15)
  w_PATH = space(150)
  o_PATH = space(150)
  w_PROGRE = space(10)
  w_FILE = space(150)
  w_ANONIMO = space(1)
  w_CONSOLIDATO = space(1)
  w_descri = space(30)
  w_decimi = 0
  w_BUDESCRI = space(35)
  w_CAMVAL = 0
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_TIPVOC = space(10)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_TIPSTA = space(1)
  w_PRDECTOT = 0
  w_PRINIESE = ctod('  /  /  ')
  w_AZ_INDWE = space(100)
  w_CODOLT = 0
  w_ARROT = space(1)
  w_PATHO = space(200)
  w_SETMRK = space(1)
  w_LPATH = .F.
  * --- Area Manuale = Declare Variables
  * --- gscg_keb
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSCG_KEB'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kebPag1","gscg_keb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_16
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gscg_keb
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       DO CASE
             CASE oparentobject = 'B'
                   .cComment = AH_MSGFORMAT('Generazione file Sd�-Basilea2')
             CASE oparentobject = 'O'
                   .cComment = AH_MSGFORMAT("Generazione Bilancio e Oltre")
             CASE oparentobject = 'M'
                   .cComment = AH_MSGFORMAT("Generazione file unico fisco azienda")               
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='TIR_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='BUSIUNIT'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='SB_MAST'
    this.cWorkTables[7]='VOCIRICL'
    this.cWorkTables[8]='CONTROPA'
    this.cWorkTables[9]='CONF_INT'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BEB with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_keb
    this.bUpdated=.t.
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GESTIONE=space(1)
      .w_codaz1=space(5)
      .w_CODAZI=space(5)
      .w_CODUTE=space(10)
      .w_CODIDE=space(5)
      .w_CODIDA=space(4)
      .w_PROATT=space(3)
      .w_PATHF=space(200)
      .w_TIPFILE=space(4)
      .w_PATHB=space(200)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_UTENTE=0
      .w_gestbu=space(1)
      .w_ESE=space(4)
      .w_ESSALDI=space(1)
      .w_VALAPP=space(3)
      .w_totdat=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_ESPREC=space(1)
      .w_monete=space(1)
      .w_divisa=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_simval=space(5)
      .w_SBDESCRI=space(35)
      .w_PROVVI=space(1)
      .w_CONFRONTO=space(1)
      .w_FLURE=space(2)
      .w_FLURF=space(1)
      .w_CODBUN=space(3)
      .w_FLUIQ=space(1)
      .w_SUPERBU=space(15)
      .w_PATH=space(150)
      .w_PROGRE=space(10)
      .w_FILE=space(150)
      .w_ANONIMO=space(1)
      .w_CONSOLIDATO=space(1)
      .w_descri=space(30)
      .w_decimi=0
      .w_BUDESCRI=space(35)
      .w_CAMVAL=0
      .w_EXTRAEUR=0
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_TIPVOC=space(10)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_TIPSTA=space(1)
      .w_PRDECTOT=0
      .w_PRINIESE=ctod("  /  /  ")
      .w_AZ_INDWE=space(100)
      .w_CODOLT=0
      .w_ARROT=space(1)
      .w_PATHO=space(200)
      .w_SETMRK=space(1)
      .w_LPATH=.f.
        .w_GESTIONE = this.oParentObject
        .w_codaz1 = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_codaz1))
          .link_1_2('Full')
        endif
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_3('Full')
        endif
        .w_CODUTE = i_CODUTE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODUTE))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,8,.f.)
        .w_TIPFILE = ICASE(.w_GESTIONE='M','.'+Alltrim(.w_PROATT),.w_GESTIONE='B','.XML','XLS')
          .DoRTCalc(10,10,.f.)
        .w_MASABI = ' '
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1))
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_UTENTE = g_CODUTE
          .DoRTCalc(15,15,.f.)
        .w_ESE = g_codese
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_ESE))
          .link_1_16('Full')
        endif
        .w_ESSALDI = IIF(.w_MASABI='S',' ','S')
        .w_VALAPP = g_PERVAL
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_VALAPP))
          .link_1_18('Full')
        endif
        .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        .w_data1 = iif(.w_totdat='T',CTOD('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        .w_data2 = iif(.w_totdat='T',CTOD('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        .w_monete = 'c'
        .w_divisa = IIF(.w_monete='c',.w_VALAPP,.w_divisa)
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_divisa))
          .link_1_24('Full')
        endif
          .DoRTCalc(25,27,.f.)
        .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        .w_CONFRONTO = ' '
        .w_FLURE = iif(.w_FLURF='RF',' ',.w_FLURE)
        .w_FLURF = iif(.w_FLURE='RE',' ',.w_FLURF)
        .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_CODBUN))
          .link_1_32('Full')
        endif
        .w_FLUIQ = 'IQ'
        .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_34('Full')
        endif
        .w_PATH = ICASE(.w_GESTIONE='B',.w_PATHB,.w_GESTIONE='M',.w_PATHF,.w_PATHO)
        .w_PROGRE = Right('00'+Alltrim(Str(DAY( I_DATSYS))),2)+Right('00'+Alltrim(Str(MONTH( I_DATSYS))),2)+Alltrim(Str(Year( I_DATSYS)))+Right('00'+Alltrim(Str(Hour( Datetime()))),2) +Right('00'+Alltrim(Str(Minute(Datetime()))),2)+Right('00'+Alltrim(Str(Sec(Datetime()))),2)
        .w_FILE = iCASE(.w_GESTIONE='B','SDI_BASILEA2_'+STRTRAN(DToc( i_DATSYS ),.w_SETMRK,'-')+'.XML',.w_GESTIONE='O',Alltrim(Str(.w_CODOLT))+Alltrim(.w_PROGRE)+'.XLS','IU'+Alltrim(.w_CODIDE)+Alltrim(.w_CODIDA)+'.'+Alltrim(.w_PROATT))
          .DoRTCalc(38,38,.f.)
        .w_CONSOLIDATO = 'N'
          .DoRTCalc(40,42,.f.)
        .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
          .DoRTCalc(44,46,.f.)
        .w_TIPVOC = 'M'
          .DoRTCalc(48,49,.f.)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_esepre))
          .link_1_64('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_65('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_TIPSTA = 'T'
          .DoRTCalc(54,55,.f.)
        .w_AZ_INDWE = 'http://www.supermercato.it/Category.pasp?txtCatalog=MainCatalog&txtCategory=mondo_imprese&Restart=1'
          .DoRTCalc(57,57,.f.)
        .w_ARROT = 'N'
    endwith
    this.DoRTCalc(59,61,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODAZ1<>.w_CODAZ1
          .link_1_2('Full')
        endif
        if .o_CODAZI<>.w_CODAZI
          .link_1_3('Full')
        endif
          .link_1_4('Full')
        .DoRTCalc(5,8,.t.)
            .w_TIPFILE = ICASE(.w_GESTIONE='M','.'+Alltrim(.w_PROATT),.w_GESTIONE='B','.XML','XLS')
        .DoRTCalc(10,11,.t.)
          .link_1_12('Full')
        .DoRTCalc(13,17,.t.)
        if .o_ESE<>.w_ESE
          .link_1_18('Full')
        endif
        if .o_ESSALDI<>.w_ESSALDI
            .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data1 = iif(.w_totdat='T',CTOD('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data2 = iif(.w_totdat='T',CTOD('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        endif
        if .o_data1<>.w_data1.or. .o_totdat<>.w_totdat.or. .o_PROVVI<>.w_PROVVI
            .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        endif
        .DoRTCalc(23,23,.t.)
        if .o_monete<>.w_monete.or. .o_VALAPP<>.w_VALAPP
            .w_divisa = IIF(.w_monete='c',.w_VALAPP,.w_divisa)
          .link_1_24('Full')
        endif
        .DoRTCalc(25,27,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        endif
        .DoRTCalc(29,29,.t.)
        if .o_FLURF<>.w_FLURF
            .w_FLURE = iif(.w_FLURF='RF',' ',.w_FLURE)
        endif
        if .o_FLURE<>.w_FLURE
            .w_FLURF = iif(.w_FLURE='RE',' ',.w_FLURF)
        endif
        if .o_totdat<>.w_totdat.or. .o_ESPREC<>.w_ESPREC.or. .o_data1<>.w_data1.or. .o_ESSALDI<>.w_ESSALDI
            .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
          .link_1_32('Full')
        endif
        .DoRTCalc(33,33,.t.)
        if .o_data1<>.w_data1.or. .o_ESPREC<>.w_ESPREC.or. .o_totdat<>.w_totdat.or. .o_ESSALDI<>.w_ESSALDI
            .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION="ADHOC REVOLUTION",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
          .link_1_34('Full')
        endif
        if .o_GESTIONE<>.w_GESTIONE
            .w_PATH = ICASE(.w_GESTIONE='B',.w_PATHB,.w_GESTIONE='M',.w_PATHF,.w_PATHO)
        endif
            .w_PROGRE = Right('00'+Alltrim(Str(DAY( I_DATSYS))),2)+Right('00'+Alltrim(Str(MONTH( I_DATSYS))),2)+Alltrim(Str(Year( I_DATSYS)))+Right('00'+Alltrim(Str(Hour( Datetime()))),2) +Right('00'+Alltrim(Str(Minute(Datetime()))),2)+Right('00'+Alltrim(Str(Sec(Datetime()))),2)
        .DoRTCalc(37,42,.t.)
        if .o_EXTRAEUR<>.w_EXTRAEUR.or. .o_divisa<>.w_divisa
            .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        endif
        .DoRTCalc(44,49,.t.)
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_64('Full')
        endif
          .link_1_65('Full')
        .DoRTCalc(52,55,.t.)
            .w_AZ_INDWE = 'http://www.supermercato.it/Category.pasp?txtCatalog=MainCatalog&txtCategory=mondo_imprese&Restart=1'
        if .o_PATH<>.w_PATH
          .Calculate_MMWNFHGZRT()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(57,61,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_RUNXPAWRYE()
    with this
          * --- Aggiunge estensione xml al path+file
          .w_FILE = Alltrim(.w_FILE) + IIF( Upper(Right( Alltrim( .w_FILE ) ,4 ) )<>Alltrim(.w_TIPFILE) , Alltrim(.w_TIPFILE) ,'' )
    endwith
  endproc
  proc Calculate_MMWNFHGZRT()
    with this
          * --- Aggiunge \ al path e controllo esistenza
          .w_PATH = IIF(right(alltrim(.w_PATH),1)='\' or empty(.w_PATH),.w_PATH,alltrim(.w_PATH)+iif(len(alltrim(.w_PATH))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PATH,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ototdat_1_19.enabled = this.oPgFrm.Page1.oPag.ototdat_1_19.mCond()
    this.oPgFrm.Page1.oPag.odata1_1_20.enabled = this.oPgFrm.Page1.oPag.odata1_1_20.mCond()
    this.oPgFrm.Page1.oPag.odata2_1_21.enabled = this.oPgFrm.Page1.oPag.odata2_1_21.mCond()
    this.oPgFrm.Page1.oPag.oESPREC_1_22.enabled = this.oPgFrm.Page1.oPag.oESPREC_1_22.mCond()
    this.oPgFrm.Page1.oPag.odivisa_1_24.enabled = this.oPgFrm.Page1.oPag.odivisa_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPROVVI_1_28.enabled = this.oPgFrm.Page1.oPag.oPROVVI_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCODBUN_1_32.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_34.enabled = this.oPgFrm.Page1.oPag.oSUPERBU_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_57.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_17.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oESPREC_1_22.visible=!this.oPgFrm.Page1.oPag.oESPREC_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSBDESCRI_1_27.visible=!this.oPgFrm.Page1.oPag.oSBDESCRI_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCONFRONTO_1_29.visible=!this.oPgFrm.Page1.oPag.oCONFRONTO_1_29.mHide()
    this.oPgFrm.Page1.oPag.oFLURE_1_30.visible=!this.oPgFrm.Page1.oPag.oFLURE_1_30.mHide()
    this.oPgFrm.Page1.oPag.oFLURF_1_31.visible=!this.oPgFrm.Page1.oPag.oFLURF_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCODBUN_1_32.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_32.mHide()
    this.oPgFrm.Page1.oPag.oFLUIQ_1_33.visible=!this.oPgFrm.Page1.oPag.oFLUIQ_1_33.mHide()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_34.visible=!this.oPgFrm.Page1.oPag.oSUPERBU_1_34.mHide()
    this.oPgFrm.Page1.oPag.oANONIMO_1_41.visible=!this.oPgFrm.Page1.oPag.oANONIMO_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCONSOLIDATO_1_42.visible=!this.oPgFrm.Page1.oPag.oCONSOLIDATO_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oBUDESCRI_1_54.visible=!this.oPgFrm.Page1.oPag.oBUDESCRI_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oCODOLT_1_74.visible=!this.oPgFrm.Page1.oPag.oCODOLT_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oARROT_1_76.visible=!this.oPgFrm.Page1.oPag.oARROT_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_FILE Changed")
          .Calculate_RUNXPAWRYE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codaz1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codaz1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codaz1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI,AZCODOLT";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_codaz1)
            select AZCODAZI,AZFLBUNI,AZCODOLT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codaz1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
      this.w_CODOLT = NVL(_Link_.AZCODOLT,0)
    else
      if i_cCtrl<>'Load'
        this.w_codaz1 = space(5)
      endif
      this.w_gestbu = space(1)
      this.w_CODOLT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codaz1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHB2,COPATHFA,COCODIDE,COCODIDA,COPROATT,COPATHBO";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHB2,COPATHFA,COCODIDE,COCODIDA,COPROATT,COPATHBO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATHB = NVL(_Link_.COPATHB2,space(200))
      this.w_PATHF = NVL(_Link_.COPATHFA,space(200))
      this.w_CODIDE = NVL(_Link_.COCODIDE,space(5))
      this.w_CODIDA = NVL(_Link_.COCODIDA,space(4))
      this.w_PROATT = NVL(_Link_.COPROATT,space(3))
      this.w_PATHO = NVL(_Link_.COPATHBO,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PATHB = space(200)
      this.w_PATHF = space(200)
      this.w_CODIDE = space(5)
      this.w_CODIDA = space(4)
      this.w_PROATT = space(3)
      this.w_PATHO = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_INT_IDX,3]
    i_lTable = "CONF_INT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2], .t., this.CONF_INT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODUTE,CISETMRK";
                   +" from "+i_cTable+" "+i_lTable+" where CICODUTE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODUTE',this.w_CODUTE)
            select CICODUTE,CISETMRK;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CICODUTE,space(10))
      this.w_SETMRK = NVL(_Link_.CISETMRK,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = space(10)
      endif
      this.w_SETMRK = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_INT_IDX,2])+'\'+cp_ToStr(_Link_.CICODUTE,1)
      cp_ShowWarn(i_cKey,this.CONF_INT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_16'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        endif
        this.w_ESE = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_DECNAZ = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=divisa
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_divisa) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_divisa)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_divisa))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_divisa)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_divisa)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_divisa)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_divisa) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'odivisa_1_24'),i_cWhere,'GSAR_AVL',"Divise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_divisa)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_divisa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_divisa)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_divisa = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_simval = NVL(_Link_.VASIMVAL,space(5))
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_divisa = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_EXTRAEUR = 0
      this.w_simval = space(5)
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_divisa Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_codaz1;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_32'),i_cWhere,'GSAR_MBU',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MSB',True,'SB_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SBCODICE like "+cp_ToStrODBC(trim(this.w_SUPERBU)+"%");

          i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SBCODICE',trim(this.w_SUPERBU))
          select SBCODICE,SBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUPERBU)==trim(_Link_.SBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SUPERBU) and !this.bDontReportError
            deferred_cp_zoom('SB_MAST','*','SBCODICE',cp_AbsName(oSource.parent,'oSUPERBU_1_34'),i_cWhere,'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',oSource.xKey(1))
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_PRINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
      this.w_PRINIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_PRDECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
      this.w_PRDECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_16.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_16.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_17.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ototdat_1_19.RadioValue()==this.w_totdat)
      this.oPgFrm.Page1.oPag.ototdat_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odata1_1_20.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_20.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_21.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_21.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oESPREC_1_22.RadioValue()==this.w_ESPREC)
      this.oPgFrm.Page1.oPag.oESPREC_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.omonete_1_23.RadioValue()==this.w_monete)
      this.oPgFrm.Page1.oPag.omonete_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odivisa_1_24.value==this.w_divisa)
      this.oPgFrm.Page1.oPag.odivisa_1_24.value=this.w_divisa
    endif
    if not(this.oPgFrm.Page1.oPag.osimval_1_26.value==this.w_simval)
      this.oPgFrm.Page1.oPag.osimval_1_26.value=this.w_simval
    endif
    if not(this.oPgFrm.Page1.oPag.oSBDESCRI_1_27.value==this.w_SBDESCRI)
      this.oPgFrm.Page1.oPag.oSBDESCRI_1_27.value=this.w_SBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_28.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFRONTO_1_29.RadioValue()==this.w_CONFRONTO)
      this.oPgFrm.Page1.oPag.oCONFRONTO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLURE_1_30.RadioValue()==this.w_FLURE)
      this.oPgFrm.Page1.oPag.oFLURE_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLURF_1_31.RadioValue()==this.w_FLURF)
      this.oPgFrm.Page1.oPag.oFLURF_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_32.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_32.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLUIQ_1_33.RadioValue()==this.w_FLUIQ)
      this.oPgFrm.Page1.oPag.oFLUIQ_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSUPERBU_1_34.value==this.w_SUPERBU)
      this.oPgFrm.Page1.oPag.oSUPERBU_1_34.value=this.w_SUPERBU
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_35.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_35.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oFILE_1_39.value==this.w_FILE)
      this.oPgFrm.Page1.oPag.oFILE_1_39.value=this.w_FILE
    endif
    if not(this.oPgFrm.Page1.oPag.oANONIMO_1_41.RadioValue()==this.w_ANONIMO)
      this.oPgFrm.Page1.oPag.oANONIMO_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSOLIDATO_1_42.RadioValue()==this.w_CONSOLIDATO)
      this.oPgFrm.Page1.oPag.oCONSOLIDATO_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBUDESCRI_1_54.value==this.w_BUDESCRI)
      this.oPgFrm.Page1.oPag.oBUDESCRI_1_54.value=this.w_BUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_57.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_57.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODOLT_1_74.value==this.w_CODOLT)
      this.oPgFrm.Page1.oPag.oCODOLT_1_74.value=this.w_CODOLT
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT_1_76.RadioValue()==this.w_ARROT)
      this.oPgFrm.Page1.oPag.oARROT_1_76.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ESE)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESE_1_16.SetFocus()
            i_bnoObbl = !empty(.w_ESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio storicizzato")
          case   not((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo date non valido o data inferiore a quella di storicizzazione")
          case   not(empty(.w_data1) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non valido")
          case   (empty(.w_divisa))  and (.w_monete<>'c')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odivisa_1_24.SetFocus()
            i_bnoObbl = !empty(.w_divisa)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_57.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_64.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GESTIONE = this.w_GESTIONE
    this.o_codaz1 = this.w_codaz1
    this.o_CODAZI = this.w_CODAZI
    this.o_ESE = this.w_ESE
    this.o_ESSALDI = this.w_ESSALDI
    this.o_VALAPP = this.w_VALAPP
    this.o_totdat = this.w_totdat
    this.o_data1 = this.w_data1
    this.o_ESPREC = this.w_ESPREC
    this.o_monete = this.w_monete
    this.o_divisa = this.w_divisa
    this.o_PROVVI = this.w_PROVVI
    this.o_FLURE = this.w_FLURE
    this.o_FLURF = this.w_FLURF
    this.o_PATH = this.w_PATH
    this.o_EXTRAEUR = this.w_EXTRAEUR
    return

enddefine

* --- Define pages as container
define class tgscg_kebPag1 as StdContainer
  Width  = 687
  height = 304
  stdWidth  = 687
  stdheight = 304
  resizeXpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_16 as StdField with uid="HDHRXGXWEA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 65371578,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=118, Top=8, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oESSALDI_1_17 as StdCheck with uid="LHILIIGAEZ",rtseq=17,rtrep=.f.,left=189, top=8, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 85746246,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_17.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_17.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_17.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S')
    endwith
  endfunc


  add object ototdat_1_19 as StdCombo with uid="BRHXJMVGFR",rtseq=19,rtrep=.f.,left=118,top=36,width=136,height=21;
    , ToolTipText = "Tipo bilancio selezionato";
    , HelpContextID = 110203190;
    , cFormVar="w_totdat",RowSource=""+"Totale esercizio,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ototdat_1_19.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func ototdat_1_19.GetRadio()
    this.Parent.oContained.w_totdat = this.RadioValue()
    return .t.
  endfunc

  func ototdat_1_19.SetRadio()
    this.Parent.oContained.w_totdat=trim(this.Parent.oContained.w_totdat)
    this.value = ;
      iif(this.Parent.oContained.w_totdat=='T',1,;
      iif(this.Parent.oContained.w_totdat=='D',2,;
      0))
  endfunc

  func ototdat_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object odata1_1_20 as StdField with uid="LRAOKJALSD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo date non valido o data inferiore a quella di storicizzazione",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 7437770,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=338, Top=36

  func odata1_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata1_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_data2) or .w_data2>=.w_data1) AND .w_data1>.w_FINSTO)
    endwith
    return bRes
  endfunc

  add object odata2_1_21 as StdField with uid="VDVPBDFQIF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non valido",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 6389194,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=441, Top=36

  func odata2_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata2_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc

  add object oESPREC_1_22 as StdCheck with uid="NULXFFIFIL",rtseq=22,rtrep=.f.,left=526, top=36, caption="Saldi es. precedente",;
    ToolTipText = "Considera i saldi dell'esercizio precedente",;
    HelpContextID = 205704634,;
    cFormVar="w_ESPREC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESPREC_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESPREC_1_22.GetRadio()
    this.Parent.oContained.w_ESPREC = this.RadioValue()
    return .t.
  endfunc

  func oESPREC_1_22.SetRadio()
    this.Parent.oContained.w_ESPREC=trim(this.Parent.oContained.w_ESPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ESPREC=='S',1,;
      0)
  endfunc

  func oESPREC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTDAT='D' AND .w_DATA1=.w_DATINIESE)
    endwith
   endif
  endfunc

  func oESPREC_1_22.mHide()
    with this.Parent.oContained
      return (.w_PROVVI='S' OR .w_GESTIONE<>'B')
    endwith
  endfunc


  add object omonete_1_23 as StdCombo with uid="DFMUFDTKKC",rtseq=23,rtrep=.f.,left=118,top=66,width=136,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 121491258;
    , cFormVar="w_monete",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func omonete_1_23.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func omonete_1_23.GetRadio()
    this.Parent.oContained.w_monete = this.RadioValue()
    return .t.
  endfunc

  func omonete_1_23.SetRadio()
    this.Parent.oContained.w_monete=trim(this.Parent.oContained.w_monete)
    this.value = ;
      iif(this.Parent.oContained.w_monete=='c',1,;
      iif(this.Parent.oContained.w_monete=='a',2,;
      0))
  endfunc

  add object odivisa_1_24 as StdField with uid="YVASPWERFS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_divisa", cQueryName = "divisa",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 189355466,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=338, Top=66, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_divisa"

  func odivisa_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete<>'c')
    endwith
   endif
  endfunc

  func odivisa_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc odivisa_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc odivisa_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'odivisa_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Divise",'',this.parent.oContained
  endproc
  proc odivisa_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_divisa
     i_obj.ecpSave()
  endproc

  add object osimval_1_26 as StdField with uid="ERKUGGURPJ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_simval", cQueryName = "simval",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 22865114,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=384, Top=66, InputMask=replicate('X',5)

  add object oSBDESCRI_1_27 as StdField with uid="FWMLDOVXTV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SBDESCRI", cQueryName = "SBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 191929745,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=254, Top=160, InputMask=replicate('X',35)

  func oSBDESCRI_1_27.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc


  add object oPROVVI_1_28 as StdCombo with uid="WZWYZYTQAG",value=3,rtseq=28,rtrep=.f.,left=118,top=100,width=136,height=21;
    , ToolTipText = "Selezione dello stato dei movimenti da stampare";
    , HelpContextID = 86957578;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_28.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPROVVI_1_28.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_28.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oCONFRONTO_1_29 as StdCheck with uid="JEGLMRCLLH",rtseq=29,rtrep=.f.,left=259, top=101, caption="Confronto es. precedente",;
    ToolTipText = "Confronto con es. precedente",;
    HelpContextID = 259976342,;
    cFormVar="w_CONFRONTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFRONTO_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFRONTO_1_29.GetRadio()
    this.Parent.oContained.w_CONFRONTO = this.RadioValue()
    return .t.
  endfunc

  func oCONFRONTO_1_29.SetRadio()
    this.Parent.oContained.w_CONFRONTO=trim(this.Parent.oContained.w_CONFRONTO)
    this.value = ;
      iif(this.Parent.oContained.w_CONFRONTO=='S',1,;
      0)
  endfunc

  func oCONFRONTO_1_29.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'B')
    endwith
  endfunc

  add object oFLURE_1_30 as StdCheck with uid="YFREBVJAEQ",rtseq=30,rtrep=.f.,left=532, top=97, caption="Unico - RE",;
    ToolTipText = "Se attivo elabora quadro RE",;
    HelpContextID = 256017578,;
    cFormVar="w_FLURE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLURE_1_30.RadioValue()
    return(iif(this.value =1,'RE',;
    ' '))
  endfunc
  func oFLURE_1_30.GetRadio()
    this.Parent.oContained.w_FLURE = this.RadioValue()
    return .t.
  endfunc

  func oFLURE_1_30.SetRadio()
    this.Parent.oContained.w_FLURE=trim(this.Parent.oContained.w_FLURE)
    this.value = ;
      iif(this.Parent.oContained.w_FLURE=='RE',1,;
      0)
  endfunc

  func oFLURE_1_30.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'M')
    endwith
  endfunc

  add object oFLURF_1_31 as StdCheck with uid="ODCZTEIOJP",rtseq=31,rtrep=.f.,left=532, top=116, caption="Unico - RF",;
    ToolTipText = "Se attivo elabora quadro RF",;
    HelpContextID = 254969002,;
    cFormVar="w_FLURF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLURF_1_31.RadioValue()
    return(iif(this.value =1,'RF',;
    ' '))
  endfunc
  func oFLURF_1_31.GetRadio()
    this.Parent.oContained.w_FLURF = this.RadioValue()
    return .t.
  endfunc

  func oFLURF_1_31.SetRadio()
    this.Parent.oContained.w_FLURF=trim(this.Parent.oContained.w_FLURF)
    this.value = ;
      iif(this.Parent.oContained.w_FLURF=='RF',1,;
      0)
  endfunc

  func oFLURF_1_31.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'M')
    endwith
  endfunc

  add object oCODBUN_1_32 as StdField with uid="IMRWTDQUOB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 5476826,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=118, Top=130, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_MBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_gestbu $ 'ET') and EMPTY(.w_SUPERBU) and .w_ESSALDI<>'S' and g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oCODBUN_1_32.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODBUN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MBU',"Business Unit",'',this.parent.oContained
  endproc
  proc oCODBUN_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_codaz1
     i_obj.w_BUCODICE=this.parent.oContained.w_CODBUN
     i_obj.ecpSave()
  endproc

  add object oFLUIQ_1_33 as StdCheck with uid="DVQVUGMPOD",rtseq=33,rtrep=.f.,left=532, top=135, caption="Irap - IQ",;
    ToolTipText = "Se attivo elabora quadro IQ",;
    HelpContextID = 244024490,;
    cFormVar="w_FLUIQ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLUIQ_1_33.RadioValue()
    return(iif(this.value =1,'IQ',;
    ' '))
  endfunc
  func oFLUIQ_1_33.GetRadio()
    this.Parent.oContained.w_FLUIQ = this.RadioValue()
    return .t.
  endfunc

  func oFLUIQ_1_33.SetRadio()
    this.Parent.oContained.w_FLUIQ=trim(this.Parent.oContained.w_FLUIQ)
    this.value = ;
      iif(this.Parent.oContained.w_FLUIQ=='IQ',1,;
      0)
  endfunc

  func oFLUIQ_1_33.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'M')
    endwith
  endfunc

  add object oSUPERBU_1_34 as StdField with uid="YUWOKATWYZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SUPERBU", cQueryName = "SUPERBU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo Business Unit -attivo solo senza riporto saldi",;
    HelpContextID = 58733862,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=118, Top=160, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SB_MAST", cZoomOnZoom="GSAR_MSB", oKey_1_1="SBCODICE", oKey_1_2="this.w_SUPERBU"

  func oSUPERBU_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_gestbu $ 'ET') AND EMPTY(.w_CODBUN) and .w_ESSALDI<>'S' AND g_APPLICATION<>"ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oSUPERBU_1_34.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oSUPERBU_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oSUPERBU_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUPERBU_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SB_MAST','*','SBCODICE',cp_AbsName(this.parent,'oSUPERBU_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this.parent.oContained
  endproc
  proc oSUPERBU_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MSB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SBCODICE=this.parent.oContained.w_SUPERBU
     i_obj.ecpSave()
  endproc

  add object oPATH_1_35 as StdField with uid="HBNCUVPVRI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path file da generare (impostabile di default in contropartite e parametri)",;
    HelpContextID = 60595978,;
   bGlobalFont=.t.,;
    Height=21, Width=521, Left=118, Top=194, InputMask=replicate('X',150)


  add object oBtn_1_37 as StdButton with uid="PSACVHWWYM",left=641, top=195, width=22,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per scegliere destinazione file";
    , HelpContextID = 65475626;
  , bGlobalFont=.t.

    proc oBtn_1_37.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILE_1_39 as StdField with uid="KDKEYWXRRW",rtseq=37,rtrep=.f.,;
    cFormVar = "w_FILE", cQueryName = "FILE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il file da generare",;
    HelpContextID = 60823466,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=118, Top=223, InputMask=replicate('X',150)

  add object oANONIMO_1_41 as StdCheck with uid="WHKEUYKXGV",rtseq=38,rtrep=.f.,left=118, top=274, caption="Esportazione anonima",;
    ToolTipText = "Se attivo non riporta codice fiscale",;
    HelpContextID = 34005754,;
    cFormVar="w_ANONIMO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANONIMO_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANONIMO_1_41.GetRadio()
    this.Parent.oContained.w_ANONIMO = this.RadioValue()
    return .t.
  endfunc

  func oANONIMO_1_41.SetRadio()
    this.Parent.oContained.w_ANONIMO=trim(this.Parent.oContained.w_ANONIMO)
    this.value = ;
      iif(this.Parent.oContained.w_ANONIMO=='S',1,;
      0)
  endfunc

  func oANONIMO_1_41.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'B')
    endwith
  endfunc

  add object oCONSOLIDATO_1_42 as StdCheck with uid="KNYWFYYDOJ",rtseq=39,rtrep=.f.,left=284, top=274, caption="Consolidato",;
    ToolTipText = "Se attivo il file generato sar� marcato come di bilancio consolidato",;
    HelpContextID = 224614010,;
    cFormVar="w_CONSOLIDATO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONSOLIDATO_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCONSOLIDATO_1_42.GetRadio()
    this.Parent.oContained.w_CONSOLIDATO = this.RadioValue()
    return .t.
  endfunc

  func oCONSOLIDATO_1_42.SetRadio()
    this.Parent.oContained.w_CONSOLIDATO=trim(this.Parent.oContained.w_CONSOLIDATO)
    this.value = ;
      iif(this.Parent.oContained.w_CONSOLIDATO=='S',1,;
      0)
  endfunc

  func oCONSOLIDATO_1_42.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'B')
    endwith
  endfunc


  add object oBtn_1_44 as StdButton with uid="UXHDCHQMFU",left=532, top=253, width=48,height=45,;
    CpPicture="bmp\root.bmp", caption="", nPag=1;
    , ToolTipText = "Collegamento al supermercato dell'informazione";
    , HelpContextID = 25269270;
    , Caption='\<Sd�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSUT_BPW(this.Parent.oContained,"EXECUTE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GESTIONE<>'B')
     endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="NDASJAFEBO",left=582, top=253, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 65647898;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        do GSCG_BEB with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_monete='c' or ((not empty(.w_divisa))and(not empty(.w_CAMVAL)))) And Not Empty( .w_PATH ))
      endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="NLMNHXMBPQ",left=632, top=253, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 25269270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBUDESCRI_1_54 as StdField with uid="YIIXBMVRTI",rtseq=42,rtrep=.f.,;
    cFormVar = "w_BUDESCRI", cQueryName = "BUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 191925153,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=163, Top=130, InputMask=replicate('X',35)

  func oBUDESCRI_1_54.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oCAMVAL_1_57 as StdField with uid="KVTIQCJDFU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 58658778,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=529, Top=66, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
    endwith
   endif
  endfunc

  add object oCODOLT_1_74 as StdField with uid="WXSCXCZKDK",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CODOLT", cQueryName = "CODOLT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 181834202,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=530, Top=100

  func oCODOLT_1_74.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'O')
    endwith
  endfunc


  add object oARROT_1_76 as StdCombo with uid="GKMTMDLFLT",rtseq=58,rtrep=.f.,left=530,top=130,width=151,height=21;
    , ToolTipText = "Arrotonda importi";
    , HelpContextID = 240496378;
    , cFormVar="w_ARROT",RowSource=""+"Importi originali,"+"Importi arrotondati,"+"Importi troncati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARROT_1_76.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARROT_1_76.GetRadio()
    this.Parent.oContained.w_ARROT = this.RadioValue()
    return .t.
  endfunc

  func oARROT_1_76.SetRadio()
    this.Parent.oContained.w_ARROT=trim(this.Parent.oContained.w_ARROT)
    this.value = ;
      iif(this.Parent.oContained.w_ARROT=='N',1,;
      iif(this.Parent.oContained.w_ARROT=='S',2,;
      iif(this.Parent.oContained.w_ARROT=='T',3,;
      0)))
  endfunc

  func oARROT_1_76.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'O')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="BPUEMPCWXC",Visible=.t., Left=418, Top=36,;
    Alignment=1, Width=20, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="SSOHJFPBMQ",Visible=.t., Left=21, Top=8,;
    Alignment=1, Width=95, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="WFFEHCMRQG",Visible=.t., Left=262, Top=66,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="VMCBWEBFFB",Visible=.t., Left=21, Top=160,;
    Alignment=1, Width=95, Height=15,;
    Caption="Gruppo B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="ALDRSKHBRW",Visible=.t., Left=21, Top=63,;
    Alignment=1, Width=95, Height=18,;
    Caption="Tipo valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="VYHUHVRQQI",Visible=.t., Left=462, Top=67,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="PUKCKPISOE",Visible=.t., Left=21, Top=36,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="ADKVKJVYMA",Visible=.t., Left=256, Top=36,;
    Alignment=1, Width=80, Height=15,;
    Caption="Periodo dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="EYGEZFSUTW",Visible=.t., Left=21, Top=130,;
    Alignment=1, Width=95, Height=15,;
    Caption="B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="TSHJITFASN",Visible=.t., Left=21, Top=100,;
    Alignment=1, Width=95, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="EHJMGLHYKT",Visible=.t., Left=6, Top=194,;
    Alignment=1, Width=110, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="BFSLHDKDFF",Visible=.t., Left=6, Top=223,;
    Alignment=1, Width=110, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="BSOZPEONIJ",Visible=.t., Left=430, Top=101,;
    Alignment=1, Width=98, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'O')
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="EVCJSTUCGA",Visible=.t., Left=431, Top=130,;
    Alignment=1, Width=95, Height=15,;
    Caption="Arrotondamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'O')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_keb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
