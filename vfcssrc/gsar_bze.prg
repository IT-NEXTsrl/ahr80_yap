* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bze                                                        *
*              Doppio zoom entit�                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_13]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-22                                                      *
* Last revis.: 2007-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bze",oParentObject)
return(i_retval)

define class tgsar_bze as StdBatch
  * --- Local variables
  w_CODICE = space(10)
  w_TIPENT = space(1)
  w_PROG = space(8)
  w_DXBTN = .f.
  * --- WorkFile variables
  ENT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato negli Zoom on Zoom
    if i_curform=.NULL.
      i_retcode = 'stop'
      return
    endif
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      this.w_DXBTN = .T.
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_CODICE = &cCurs..ENCODICE
    endif
    * --- Read from ENT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ENT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_MAST_idx,2],.t.,this.ENT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ENTIPENT"+;
        " from "+i_cTable+" ENT_MAST where ";
            +"ENCODICE = "+cp_ToStrODBC(this.w_CODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ENTIPENT;
        from (i_cTable) where;
            ENCODICE = this.w_CODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPENT = NVL(cp_ToDate(_read_.ENTIPENT),cp_NullValue(_read_.ENTIPENT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
    *     presente nella variabile
    this.w_PROG = "GSAR_MEN('"+this.w_TIPENT+"')"
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"ENCODICE",this.w_CODICE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ENT_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
