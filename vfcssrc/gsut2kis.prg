* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut2kis                                                        *
*              Leggenda                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-23                                                      *
* Last revis.: 2011-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut2kis",oParentObject))

* --- Class definition
define class tgsut2kis as StdForm
  Top    = 10
  Left   = 12

  * --- Standard Properties
  Width  = 194
  Height = 174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-07-01"
  HelpContextID=1459049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut2kis"
  cComment = "Leggenda"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_COLORE1 = space(1)
  w_COLORE2 = space(1)
  w_COLORE3 = space(1)
  w_COLORE4 = space(1)
  w_COLORE5 = space(1)
  w_COLORE6 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut2kisPag1","gsut2kis",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COLORE1=space(1)
      .w_COLORE2=space(1)
      .w_COLORE3=space(1)
      .w_COLORE4=space(1)
      .w_COLORE5=space(1)
      .w_COLORE6=space(1)
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(8454016)
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(8454143)
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate(12632256)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(4227327)
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(16744576)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate(16759295)
    endwith
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(8454016)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(8454143)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(12632256)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(4227327)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(16744576)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(16759295)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(8454016)
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(8454143)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(12632256)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(4227327)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(16744576)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(16759295)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOLORE1_1_6.value==this.w_COLORE1)
      this.oPgFrm.Page1.oPag.oCOLORE1_1_6.value=this.w_COLORE1
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE2_1_7.value==this.w_COLORE2)
      this.oPgFrm.Page1.oPag.oCOLORE2_1_7.value=this.w_COLORE2
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE3_1_8.value==this.w_COLORE3)
      this.oPgFrm.Page1.oPag.oCOLORE3_1_8.value=this.w_COLORE3
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE4_1_9.value==this.w_COLORE4)
      this.oPgFrm.Page1.oPag.oCOLORE4_1_9.value=this.w_COLORE4
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE5_1_10.value==this.w_COLORE5)
      this.oPgFrm.Page1.oPag.oCOLORE5_1_10.value=this.w_COLORE5
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE6_1_12.value==this.w_COLORE6)
      this.oPgFrm.Page1.oPag.oCOLORE6_1_12.value=this.w_COLORE6
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut2kisPag1 as StdContainer
  Width  = 190
  height = 174
  stdWidth  = 190
  stdheight = 174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOLORE1_1_6 as StdField with uid="WRNHKFHKTN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COLORE1", cQueryName = "COLORE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 94515162,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=143, Top=13, InputMask=replicate('X',1)

  add object oCOLORE2_1_7 as StdField with uid="WOQERGPWEE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COLORE2", cQueryName = "COLORE2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 173920294,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=143, Top=39, InputMask=replicate('X',1)

  add object oCOLORE3_1_8 as StdField with uid="OXRABRHTRV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COLORE3", cQueryName = "COLORE3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 173920294,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=143, Top=65, InputMask=replicate('X',1)

  add object oCOLORE4_1_9 as StdField with uid="EQOJKRBULZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COLORE4", cQueryName = "COLORE4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 173920294,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=143, Top=91, InputMask=replicate('X',1)

  add object oCOLORE5_1_10 as StdField with uid="PNHVGYULZV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COLORE5", cQueryName = "COLORE5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 173920294,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=143, Top=117, InputMask=replicate('X',1)

  add object oCOLORE6_1_12 as StdField with uid="BRJRKMXJVA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COLORE6", cQueryName = "COLORE6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 173920294,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=144, Top=144, InputMask=replicate('X',1)


  add object oObj_1_13 as cp_setobjprop with uid="QFKIHTTZWY",left=211, top=16, width=148,height=23,;
    caption='Confermato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE1",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 123238713


  add object oObj_1_14 as cp_setobjprop with uid="CPDMAWQBHM",left=211, top=39, width=148,height=23,;
    caption='Provvisorio',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE2",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 13534341


  add object oObj_1_15 as cp_setobjprop with uid="YRFHWHTXPR",left=211, top=62, width=148,height=23,;
    caption='Ignorato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE3",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 142608629


  add object oObj_1_16 as cp_setobjprop with uid="SBOKNLROPC",left=211, top=85, width=148,height=23,;
    caption='Indice non creato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE4",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 133239736


  add object oObj_1_17 as cp_setobjprop with uid="ENCWPOPNNH",left=211, top=108, width=148,height=23,;
    caption='Documento inesistente',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE5",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 54172595


  add object oObj_1_18 as cp_setobjprop with uid="ABIWVZYDNW",left=211, top=131, width=148,height=23,;
    caption='Copia di backup',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORE6",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 226253681

  add object oStr_1_1 as StdString with uid="UAMXOEIVGS",Visible=.t., Left=70, Top=13,;
    Alignment=1, Width=70, Height=18,;
    Caption="Confermato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="VDTSAZHNZO",Visible=.t., Left=70, Top=39,;
    Alignment=1, Width=70, Height=18,;
    Caption="Provvisorio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="EHXZXCJRBK",Visible=.t., Left=75, Top=65,;
    Alignment=1, Width=65, Height=18,;
    Caption="Ignorato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="QJGFADBGHZ",Visible=.t., Left=9, Top=117,;
    Alignment=1, Width=131, Height=18,;
    Caption="Documento inesistente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ZVYUSAUOYT",Visible=.t., Left=12, Top=91,;
    Alignment=1, Width=128, Height=18,;
    Caption="Indice non creato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="LVIMSOPEZX",Visible=.t., Left=10, Top=144,;
    Alignment=1, Width=131, Height=18,;
    Caption="Copia di backup:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut2kis','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
