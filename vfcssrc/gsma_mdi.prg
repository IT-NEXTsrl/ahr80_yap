* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mdi                                                        *
*              Dettagli importi                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-13                                                      *
* Last revis.: 2012-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mdi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mdi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mdi")
  return

* --- Class definition
define class tgsma_mdi as StdPCForm
  Width  = 653
  Height = 308
  Top    = 39
  Left   = 89
  cComment = "Dettagli importi"
  cPrg = "gsma_mdi"
  HelpContextID=80378729
  add object cnt as tcgsma_mdi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mdi as PCContext
  w_DECODART = space(20)
  w_DEROWNUM = 0
  w_DEVALPRA = 0
  w_DEPREMIN = 0
  w_DEPREMAX = 0
  w_IMPOR = space(15)
  w_INDET = space(15)
  w_OLTRE = space(15)
  w_DESCAORD = 0
  w_DEPREMED = 0
  w_DEMAXPER = 0
  w_DEMINPER = 0
  proc Save(i_oFrom)
    this.w_DECODART = i_oFrom.w_DECODART
    this.w_DEROWNUM = i_oFrom.w_DEROWNUM
    this.w_DEVALPRA = i_oFrom.w_DEVALPRA
    this.w_DEPREMIN = i_oFrom.w_DEPREMIN
    this.w_DEPREMAX = i_oFrom.w_DEPREMAX
    this.w_IMPOR = i_oFrom.w_IMPOR
    this.w_INDET = i_oFrom.w_INDET
    this.w_OLTRE = i_oFrom.w_OLTRE
    this.w_DESCAORD = i_oFrom.w_DESCAORD
    this.w_DEPREMED = i_oFrom.w_DEPREMED
    this.w_DEMAXPER = i_oFrom.w_DEMAXPER
    this.w_DEMINPER = i_oFrom.w_DEMINPER
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DECODART = this.w_DECODART
    i_oTo.w_DEROWNUM = this.w_DEROWNUM
    i_oTo.w_DEVALPRA = this.w_DEVALPRA
    i_oTo.w_DEPREMIN = this.w_DEPREMIN
    i_oTo.w_DEPREMAX = this.w_DEPREMAX
    i_oTo.w_IMPOR = this.w_IMPOR
    i_oTo.w_INDET = this.w_INDET
    i_oTo.w_OLTRE = this.w_OLTRE
    i_oTo.w_DESCAORD = this.w_DESCAORD
    i_oTo.w_DEPREMED = this.w_DEPREMED
    i_oTo.w_DEMAXPER = this.w_DEMAXPER
    i_oTo.w_DEMINPER = this.w_DEMINPER
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mdi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 653
  Height = 308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-29"
  HelpContextID=80378729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAR_DETT_IDX = 0
  cFile = "TAR_DETT"
  cKeySelect = "DECODART,DEROWNUM"
  cKeyWhere  = "DECODART=this.w_DECODART and DEROWNUM=this.w_DEROWNUM"
  cKeyDetail  = "DECODART=this.w_DECODART and DEROWNUM=this.w_DEROWNUM"
  cKeyWhereODBC = '"DECODART="+cp_ToStrODBC(this.w_DECODART)';
      +'+" and DEROWNUM="+cp_ToStrODBC(this.w_DEROWNUM)';

  cKeyDetailWhereODBC = '"DECODART="+cp_ToStrODBC(this.w_DECODART)';
      +'+" and DEROWNUM="+cp_ToStrODBC(this.w_DEROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TAR_DETT.DECODART="+cp_ToStrODBC(this.w_DECODART)';
      +'+" and TAR_DETT.DEROWNUM="+cp_ToStrODBC(this.w_DEROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAR_DETT.DEVALPRA'
  cPrg = "gsma_mdi"
  cComment = "Dettagli importi"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DECODART = space(20)
  w_DEROWNUM = 0
  w_DEVALPRA = 0
  o_DEVALPRA = 0
  w_DEPREMIN = 0
  o_DEPREMIN = 0
  w_DEPREMAX = 0
  w_IMPOR = space(15)
  w_INDET = space(15)
  w_OLTRE = space(15)
  w_DESCAORD = 0
  w_DEPREMED = 0
  o_DEPREMED = 0
  w_DEMAXPER = 0
  o_DEMAXPER = 0
  w_DEMINPER = 0
  o_DEMINPER = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_mdiPag1","gsma_mdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TAR_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAR_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAR_DETT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsma_mdi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsma_mdi
    If Type('this.oParentObject.oParentObject')='O'
       this.cFunction=this.oParentObject.oParentObject.cFunction
    Endif
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAR_DETT where DECODART=KeySet.DECODART
    *                            and DEROWNUM=KeySet.DEROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsma_mdi
      * --- Setta Ordine per Valore'
      i_cOrder = 'order by DESCAORD Asc '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2],this.bLoadRecFilter,this.TAR_DETT_IDX,"gsma_mdi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAR_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAR_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAR_DETT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DECODART',this.w_DECODART  ,'DEROWNUM',this.w_DEROWNUM  )
      select * from (i_cTable) TAR_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DECODART = NVL(DECODART,space(20))
        .w_DEROWNUM = NVL(DEROWNUM,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAR_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DEVALPRA = NVL(DEVALPRA,0)
          .w_DEPREMIN = NVL(DEPREMIN,0)
          .w_DEPREMAX = NVL(DEPREMAX,0)
        .w_IMPOR = IIF(.w_DEVALPRA=-2 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Part.imp./ind.','')
        .w_INDET = IIF(.w_DEVALPRA=-1 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Indeterminabile','')
        .w_OLTRE = IIF(.w_DEVALPRA=0 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Oltre','')
          .w_DESCAORD = NVL(DESCAORD,0)
          .w_DEPREMED = NVL(DEPREMED,0)
          .w_DEMAXPER = NVL(DEMAXPER,0)
          .w_DEMINPER = NVL(DEMINPER,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DECODART=space(20)
      .w_DEROWNUM=0
      .w_DEVALPRA=0
      .w_DEPREMIN=0
      .w_DEPREMAX=0
      .w_IMPOR=space(15)
      .w_INDET=space(15)
      .w_OLTRE=space(15)
      .w_DESCAORD=0
      .w_DEPREMED=0
      .w_DEMAXPER=0
      .w_DEMINPER=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_DEPREMIN = iif(.w_DEPREMED=0 OR .w_DEMINPER=0 ,.w_DEPREMIN,.w_DEPREMED*(1-.w_DEMINPER/100))
        .w_DEPREMAX = iif(.w_DEPREMED=0  OR .w_DEMAXPER=0,IIF(.w_DEPREMAX=0,.w_DEPREMIN,.w_DEPREMAX),.w_DEPREMED*(1+.w_DEMAXPER/100))
        .w_IMPOR = IIF(.w_DEVALPRA=-2 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Part.imp./ind.','')
        .w_INDET = IIF(.w_DEVALPRA=-1 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Indeterminabile','')
        .w_OLTRE = IIF(.w_DEVALPRA=0 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Oltre','')
        .w_DESCAORD = IIF(.w_DEVALPRA=-2,9000000000,IIF(.w_DEVALPRA=-1,8000000000,IIF(.w_DEVALPRA=0,7000000000,.w_DEVALPRA)))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAR_DETT')
    this.DoRTCalc(10,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TAR_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODART,"DECODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEROWNUM,"DEROWNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DEVALPRA N(18,4);
      ,t_DEPREMIN N(18,6);
      ,t_DEPREMAX N(18,6);
      ,t_IMPOR C(15);
      ,t_INDET C(15);
      ,t_OLTRE C(15);
      ,t_DEPREMED N(18,6);
      ,t_DEMAXPER N(5,2);
      ,t_DEMINPER N(5,2);
      ,CPROWNUM N(10);
      ,t_DESCAORD N(18,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mdibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEVALPRA_2_1.controlsource=this.cTrsName+'.t_DEVALPRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMIN_2_2.controlsource=this.cTrsName+'.t_DEPREMIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMAX_2_3.controlsource=this.cTrsName+'.t_DEPREMAX'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMPOR_2_4.controlsource=this.cTrsName+'.t_IMPOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oINDET_2_5.controlsource=this.cTrsName+'.t_INDET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_6.controlsource=this.cTrsName+'.t_OLTRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMED_2_8.controlsource=this.cTrsName+'.t_DEPREMED'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEMAXPER_2_9.controlsource=this.cTrsName+'.t_DEMAXPER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEMINPER_2_10.controlsource=this.cTrsName+'.t_DEMINPER'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(136)
    this.AddVLine(265)
    this.AddVLine(393)
    this.AddVLine(521)
    this.AddVLine(573)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVALPRA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2])
      *
      * insert into TAR_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAR_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'TAR_DETT')
        i_cFldBody=" "+;
                  "(DECODART,DEROWNUM,DEVALPRA,DEPREMIN,DEPREMAX"+;
                  ",DESCAORD,DEPREMED,DEMAXPER,DEMINPER,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DECODART)+","+cp_ToStrODBC(this.w_DEROWNUM)+","+cp_ToStrODBC(this.w_DEVALPRA)+","+cp_ToStrODBC(this.w_DEPREMIN)+","+cp_ToStrODBC(this.w_DEPREMAX)+;
             ","+cp_ToStrODBC(this.w_DESCAORD)+","+cp_ToStrODBC(this.w_DEPREMED)+","+cp_ToStrODBC(this.w_DEMAXPER)+","+cp_ToStrODBC(this.w_DEMINPER)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAR_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'TAR_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DECODART',this.w_DECODART,'DEROWNUM',this.w_DEROWNUM)
        INSERT INTO (i_cTable) (;
                   DECODART;
                  ,DEROWNUM;
                  ,DEVALPRA;
                  ,DEPREMIN;
                  ,DEPREMAX;
                  ,DESCAORD;
                  ,DEPREMED;
                  ,DEMAXPER;
                  ,DEMINPER;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DECODART;
                  ,this.w_DEROWNUM;
                  ,this.w_DEVALPRA;
                  ,this.w_DEPREMIN;
                  ,this.w_DEPREMAX;
                  ,this.w_DESCAORD;
                  ,this.w_DEPREMED;
                  ,this.w_DEMAXPER;
                  ,this.w_DEMINPER;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DEPREMIN<>0 AND t_DEPREMAX<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAR_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAR_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DEPREMIN<>0 AND t_DEPREMAX<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAR_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAR_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DEVALPRA="+cp_ToStrODBC(this.w_DEVALPRA)+;
                     ",DEPREMIN="+cp_ToStrODBC(this.w_DEPREMIN)+;
                     ",DEPREMAX="+cp_ToStrODBC(this.w_DEPREMAX)+;
                     ",DESCAORD="+cp_ToStrODBC(this.w_DESCAORD)+;
                     ",DEPREMED="+cp_ToStrODBC(this.w_DEPREMED)+;
                     ",DEMAXPER="+cp_ToStrODBC(this.w_DEMAXPER)+;
                     ",DEMINPER="+cp_ToStrODBC(this.w_DEMINPER)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAR_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DEVALPRA=this.w_DEVALPRA;
                     ,DEPREMIN=this.w_DEPREMIN;
                     ,DEPREMAX=this.w_DEPREMAX;
                     ,DESCAORD=this.w_DESCAORD;
                     ,DEPREMED=this.w_DEPREMED;
                     ,DEMAXPER=this.w_DEMAXPER;
                     ,DEMINPER=this.w_DEMINPER;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DEPREMIN<>0 AND t_DEPREMAX<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAR_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DEPREMIN<>0 AND t_DEPREMAX<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAR_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAR_DETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_DEPREMED<>.w_DEPREMED.or. .o_DEMINPER<>.w_DEMINPER
          .w_DEPREMIN = iif(.w_DEPREMED=0 OR .w_DEMINPER=0 ,.w_DEPREMIN,.w_DEPREMED*(1-.w_DEMINPER/100))
        endif
        if .o_DEPREMIN<>.w_DEPREMIN.or. .o_DEPREMED<>.w_DEPREMED.or. .o_DEMAXPER<>.w_DEMAXPER
          .w_DEPREMAX = iif(.w_DEPREMED=0  OR .w_DEMAXPER=0,IIF(.w_DEPREMAX=0,.w_DEPREMIN,.w_DEPREMAX),.w_DEPREMED*(1+.w_DEMAXPER/100))
        endif
          .w_IMPOR = IIF(.w_DEVALPRA=-2 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Part.imp./ind.','')
          .w_INDET = IIF(.w_DEVALPRA=-1 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Indeterminabile','')
          .w_OLTRE = IIF(.w_DEVALPRA=0 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Oltre','')
        if .o_DEVALPRA<>.w_DEVALPRA
          .w_DESCAORD = IIF(.w_DEVALPRA=-2,9000000000,IIF(.w_DEVALPRA=-1,8000000000,IIF(.w_DEVALPRA=0,7000000000,.w_DEVALPRA)))
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESCAORD with this.w_DESCAORD
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVALPRA_2_1.value==this.w_DEVALPRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVALPRA_2_1.value=this.w_DEVALPRA
      replace t_DEVALPRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEVALPRA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMIN_2_2.value==this.w_DEPREMIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMIN_2_2.value=this.w_DEPREMIN
      replace t_DEPREMIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMAX_2_3.value==this.w_DEPREMAX)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMAX_2_3.value=this.w_DEPREMAX
      replace t_DEPREMAX with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMAX_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPOR_2_4.value==this.w_IMPOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPOR_2_4.value=this.w_IMPOR
      replace t_IMPOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMPOR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINDET_2_5.value==this.w_INDET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINDET_2_5.value=this.w_INDET
      replace t_INDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINDET_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_6.value==this.w_OLTRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_6.value=this.w_OLTRE
      replace t_OLTRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOLTRE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMED_2_8.value==this.w_DEPREMED)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMED_2_8.value=this.w_DEPREMED
      replace t_DEPREMED with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMED_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMAXPER_2_9.value==this.w_DEMAXPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMAXPER_2_9.value=this.w_DEMAXPER
      replace t_DEMAXPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMAXPER_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMINPER_2_10.value==this.w_DEMINPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMINPER_2_10.value=this.w_DEMINPER
      replace t_DEMINPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEMINPER_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'TAR_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DEPREMIN<=.w_DEPREMAX OR .w_DEPREMAX=0) and (.w_DEPREMIN<>0 AND .w_DEPREMAX<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMIN_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La tariffa minima deve essere minore o uguale allla tariffa massima")
        case   not(.w_DEPREMIN<=.w_DEPREMAX) and (.w_DEPREMIN<>0 AND .w_DEPREMAX<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEPREMAX_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La tariffa massima deve essere maggiore o uguale alla tariffa minima")
      endcase
      if .w_DEPREMIN<>0 AND .w_DEPREMAX<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DEVALPRA = this.w_DEVALPRA
    this.o_DEPREMIN = this.w_DEPREMIN
    this.o_DEPREMED = this.w_DEPREMED
    this.o_DEMAXPER = this.w_DEMAXPER
    this.o_DEMINPER = this.w_DEMINPER
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DEPREMIN<>0 AND t_DEPREMAX<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DEVALPRA=0
      .w_DEPREMIN=0
      .w_DEPREMAX=0
      .w_IMPOR=space(15)
      .w_INDET=space(15)
      .w_OLTRE=space(15)
      .w_DESCAORD=0
      .w_DEPREMED=0
      .w_DEMAXPER=0
      .w_DEMINPER=0
      .DoRTCalc(1,3,.f.)
        .w_DEPREMIN = iif(.w_DEPREMED=0 OR .w_DEMINPER=0 ,.w_DEPREMIN,.w_DEPREMED*(1-.w_DEMINPER/100))
        .w_DEPREMAX = iif(.w_DEPREMED=0  OR .w_DEMAXPER=0,IIF(.w_DEPREMAX=0,.w_DEPREMIN,.w_DEPREMAX),.w_DEPREMED*(1+.w_DEMAXPER/100))
        .w_IMPOR = IIF(.w_DEVALPRA=-2 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Part.imp./ind.','')
        .w_INDET = IIF(.w_DEVALPRA=-1 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Indeterminabile','')
        .w_OLTRE = IIF(.w_DEVALPRA=0 AND .w_DEPREMIN<>0 AND .w_DEPREMAX<>0,'Oltre','')
        .w_DESCAORD = IIF(.w_DEVALPRA=-2,9000000000,IIF(.w_DEVALPRA=-1,8000000000,IIF(.w_DEVALPRA=0,7000000000,.w_DEVALPRA)))
    endwith
    this.DoRTCalc(10,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DEVALPRA = t_DEVALPRA
    this.w_DEPREMIN = t_DEPREMIN
    this.w_DEPREMAX = t_DEPREMAX
    this.w_IMPOR = t_IMPOR
    this.w_INDET = t_INDET
    this.w_OLTRE = t_OLTRE
    this.w_DESCAORD = t_DESCAORD
    this.w_DEPREMED = t_DEPREMED
    this.w_DEMAXPER = t_DEMAXPER
    this.w_DEMINPER = t_DEMINPER
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DEVALPRA with this.w_DEVALPRA
    replace t_DEPREMIN with this.w_DEPREMIN
    replace t_DEPREMAX with this.w_DEPREMAX
    replace t_IMPOR with this.w_IMPOR
    replace t_INDET with this.w_INDET
    replace t_OLTRE with this.w_OLTRE
    replace t_DESCAORD with this.w_DESCAORD
    replace t_DEPREMED with this.w_DEPREMED
    replace t_DEMAXPER with this.w_DEMAXPER
    replace t_DEMINPER with this.w_DEMINPER
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mdiPag1 as StdContainer
  Width  = 649
  height = 308
  stdWidth  = 649
  stdheight = 308
  resizeYpos=59
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=0, width=634,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DEVALPRA",Label1="Fino a valore pratica",Field2="DEPREMIN",Label2="Minimo",Field3="DEPREMAX",Label3="Massimo",Field4="DEPREMED",Label4="Val. medio liquidazione",Field5="DEMAXPER",Label5="% Aum.",Field6="DEMINPER",Label6="% Dim.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32723334

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=19,;
    width=631+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=20,width=630+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mdiBodyRow as CPBodyRowCnt
  Width=621
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDEVALPRA_2_1 as StdTrsField with uid="DYPAUSTBMO",rtseq=3,rtrep=.t.,;
    cFormVar="w_DEVALPRA",value=0,;
    ToolTipText = "Valore della pratica",;
    HelpContextID = 3944055,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, cSayPict=['@Z '+v_PU(80)], cGetPict=['@Z '+v_GU(80)]

  proc oDEVALPRA_2_1.mAfter
    with this.Parent.oContained
      this.Parent.oContained.oParentObject.bUpdated=.T.
    endwith
  endproc

  add object oDEPREMIN_2_2 as StdTrsField with uid="WQFECATZAF",rtseq=4,rtrep=.t.,;
    cFormVar="w_DEPREMIN",value=0,;
    ToolTipText = "Tariffa Minima",;
    HelpContextID = 52638076,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La tariffa minima deve essere minore o uguale allla tariffa massima",;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=129, Top=0, cSayPict=["99999999999.999999"], cGetPict=["99999999999.999999"]

  proc oDEPREMIN_2_2.mAfter
    with this.Parent.oContained
      this.Parent.oContained.oParentObject.bUpdated=.T.
    endwith
  endproc

  func oDEPREMIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DEPREMIN<=.w_DEPREMAX OR .w_DEPREMAX=0)
    endwith
    return bRes
  endfunc

  add object oDEPREMAX_2_3 as StdTrsField with uid="JVRDDNDHMR",rtseq=5,rtrep=.t.,;
    cFormVar="w_DEPREMAX",value=0,;
    ToolTipText = "Tariffa massima",;
    HelpContextID = 215797390,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La tariffa massima deve essere maggiore o uguale alla tariffa minima",;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=257, Top=0, cSayPict=["99999999999.999999"], cGetPict=["99999999999.999999"]

  func oDEPREMAX_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DEPREMIN<=.w_DEPREMAX)
    endwith
    return bRes
  endfunc

  add object oIMPOR_2_4 as StdTrsField with uid="BJLWWVUWQU",rtseq=6,rtrep=.t.,;
    cFormVar="w_IMPOR",value=space(15),enabled=.f.,;
    HelpContextID = 11130502,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, InputMask=replicate('X',15)

  add object oINDET_2_5 as StdTrsField with uid="VWXRNLCLMZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_INDET",value=space(15),enabled=.f.,;
    HelpContextID = 12523398,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, InputMask=replicate('X',15)

  add object oOLTRE_2_6 as StdTrsField with uid="KNQORRNVLK",rtseq=8,rtrep=.t.,;
    cFormVar="w_OLTRE",value=space(15),enabled=.f.,;
    HelpContextID = 2288154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, InputMask=replicate('X',15)

  add object oDEPREMED_2_8 as StdTrsField with uid="SSJLAOTPAC",rtseq=10,rtrep=.t.,;
    cFormVar="w_DEPREMED",value=0,;
    ToolTipText = "valore medio di liquidazione",;
    HelpContextID = 52638086,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=126, Left=385, Top=0, cSayPict=["99999999999.999999"], cGetPict=["99999999999.999999"]

  add object oDEMAXPER_2_9 as StdTrsField with uid="VWSJNZRGEU",rtseq=11,rtrep=.t.,;
    cFormVar="w_DEMAXPER",value=0,;
    ToolTipText = "% di aumento per il calcolo del valore massimo di liquidazione",;
    HelpContextID = 251945336,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=50, Left=513, Top=0

  add object oDEMINPER_2_10 as StdTrsField with uid="ROVOIFCFFE",rtseq=12,rtrep=.t.,;
    cFormVar="w_DEMINPER",value=0,;
    ToolTipText = "% di diminuzione per il calcolo del valore minimo di liquidazione",;
    HelpContextID = 261906808,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=50, Left=566, Top=0
  add object oLast as LastKeyMover
  * ---
  func oDEVALPRA_2_1.When()
    return(.t.)
  proc oDEVALPRA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDEVALPRA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mdi','TAR_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DECODART=TAR_DETT.DECODART";
  +" and "+i_cAliasName2+".DEROWNUM=TAR_DETT.DEROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
