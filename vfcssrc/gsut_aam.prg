* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aam                                                        *
*              Account Mail                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-28                                                      *
* Last revis.: 2016-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aam"))

* --- Class definition
define class tgsut_aam as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 758
  Height = 536+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-06"
  HelpContextID=125222039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=45

  * --- Constant Properties
  ACC_MAIL_IDX = 0
  cpusers_IDX = 0
  cFile = "ACC_MAIL"
  cKeySelect = "AMSERIAL"
  cKeyWhere  = "AMSERIAL=this.w_AMSERIAL"
  cKeyWhereODBC = '"AMSERIAL="+cp_ToStrODBC(this.w_AMSERIAL)';

  cKeyWhereODBCqualified = '"ACC_MAIL.AMSERIAL="+cp_ToStrODBC(this.w_AMSERIAL)';

  cPrg = "gsut_aam"
  cComment = "Account Mail"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AMSERIAL = space(5)
  w_ISALT = .F.
  w_AM_INVIO = space(1)
  o_AM_INVIO = space(1)
  w_AM_INVIO = space(1)
  w_AMTYPSEA = 0
  o_AMTYPSEA = 0
  w_DESCRIZIONE = space(100)
  w_SMTP = .F.
  w_AMFLGPEC = space(1)
  w_UTCODICE = 0
  w_AMUTENTE = 0
  o_AMUTENTE = 0
  w_AM_EMAIL = space(254)
  w_INF_EMAIL = space(254)
  w_AMSERVER = space(254)
  w_AM_PORTA = 0
  w_AMAUTSRV = space(1)
  o_AMAUTSRV = space(1)
  w_PWDCL = space(0)
  o_PWDCL = space(0)
  w_AM_LOGIN = space(254)
  w_PASSWD = space(254)
  o_PASSWD = space(254)
  w_PASSWD2 = space(254)
  o_PASSWD2 = space(254)
  w_AMPASSWD = space(254)
  w_AMMODAUT = space(15)
  w_AMMODCON = space(10)
  w_AMWORKEY = space(20)
  w_AMSERVEI = space(254)
  w_AMSAMAIL = 0
  w_AM_PORTI = 0
  w_AMFILOGG = space(100)
  w_AMDATINV = ctot('')
  w_AMFILMIT = space(254)
  w_NOTESMTP = space(0)
  w_NOTEOUTL = space(0)
  w_NOTESSL = space(0)
  w_NOTEINFI = space(0)
  w_NOTEMAPI = space(0)
  w_NOTE = space(0)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTDESUTE = space(35)
  w_AMSHARED = space(1)
  w_AMCC_EML = space(254)
  w_AMCCNEML = space(254)
  w_AM_FIRMA = space(0)
  w_PASSWD = space(254)

  * --- Autonumbered Variables
  op_AMSERIAL = this.W_AMSERIAL
  * --- Area Manuale = Declare Variables
  * --- gsut_aam
  w_GSUT_MMU = .NULL.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ACC_MAIL','gsut_aam')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aamPag1","gsut_aam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Account Mail")
      .Pages(1).HelpContextID = 38843914
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='ACC_MAIL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ACC_MAIL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ACC_MAIL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AMSERIAL = NVL(AMSERIAL,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ACC_MAIL where AMSERIAL=KeySet.AMSERIAL
    *
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ACC_MAIL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ACC_MAIL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ACC_MAIL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AMSERIAL',this.w_AMSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = isalt()
        .w_DESCRIZIONE = space(100)
        .w_UTCODICE = 0
        .w_PWDCL = space(0)
        .w_PASSWD = space(254)
        .w_NOTESMTP = "L'applicativo utilizzer� il servizio SMTP presente sul server specificato. Selezionando questo supporto non si ha la necessit� di definire un profilo di posta per ogni utente. La funzione mette a disposizione un client di posta per il solo invio."
        .w_NOTEOUTL = "L'applicativo utilizzer� il programma Microsoft Outlook installato nella macchina per l'invio della posta elettronica."
        .w_NOTESSL = "L'applicativo utilizzer� il servizio SMTP con SSL presente sul server specificato. Selezionando questo supporto � necessario specificare il profilo di posta (eventualmente PEC). La funzione mette a disposizione un client di posta per il solo invio."
        .w_NOTEINFI = "Account di tipo Infinity"
        .w_NOTEMAPI = "L'interfaccia MAPI utilizza le impostazioni di posta elettronica presenti sul sistema operativo. Per utilizzare questo supporto occorre definire quindi un profilo di posta per ogni utente. L'applicativo utilizzer� il client di posta della macchina."
        .w_UTDESUTE = space(35)
        .w_PASSWD = space(254)
        .w_AMSERIAL = NVL(AMSERIAL,space(5))
        .op_AMSERIAL = .w_AMSERIAL
        .w_AM_INVIO = NVL(AM_INVIO,space(1))
        .w_AM_INVIO = NVL(AM_INVIO,space(1))
        .w_AMTYPSEA = NVL(AMTYPSEA,0)
        .w_SMTP = .w_AM_INVIO $ 'SP' 
        .w_AMFLGPEC = NVL(AMFLGPEC,space(1))
        .w_AMUTENTE = NVL(AMUTENTE,0)
          .link_1_11('Load')
        .w_AM_EMAIL = NVL(AM_EMAIL,space(254))
        .w_INF_EMAIL = IIF(.w_AM_INVIO='I' AND .w_AMTYPSEA=4, .w_INF_EMAIL, '')
        .w_AMSERVER = NVL(AMSERVER,space(254))
        .w_AM_PORTA = NVL(AM_PORTA,0)
        .w_AMAUTSRV = NVL(AMAUTSRV,space(1))
        .w_AM_LOGIN = NVL(AM_LOGIN,space(254))
        .w_PASSWD2 = iif(.w_PWDCL='S', .w_PASSWD, "")
        .w_AMPASSWD = NVL(AMPASSWD,space(254))
        .w_AMMODAUT = NVL(AMMODAUT,space(15))
        .w_AMMODCON = NVL(AMMODCON,space(10))
        .w_AMWORKEY = NVL(AMWORKEY,space(20))
        .w_AMSERVEI = NVL(AMSERVEI,space(254))
        .w_AMSAMAIL = NVL(AMSAMAIL,0)
        .w_AM_PORTI = NVL(AM_PORTI,0)
        .w_AMFILOGG = NVL(AMFILOGG,space(100))
        .w_AMDATINV = NVL(AMDATINV,ctot(""))
        .w_AMFILMIT = NVL(AMFILMIT,space(254))
        .w_NOTE = ICASE(.w_AM_INVIO='M',  .w_NOTEMAPI, .w_AM_INVIO='O',  .w_NOTEOUTL, .w_AM_INVIO='S',  .w_NOTESMTP, .w_AM_INVIO='P',  .w_NOTESSL, .w_AM_INVIO='I',  .w_NOTEINFI,'')
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(iif(.w_PWDCL="S", "", "*"))
        .w_AMSHARED = NVL(AMSHARED,space(1))
        .w_AMCC_EML = NVL(AMCC_EML,space(254))
        .w_AMCCNEML = NVL(AMCCNEML,space(254))
        .w_AM_FIRMA = NVL(AM_FIRMA,space(0))
        cp_LoadRecExtFlds(this,'ACC_MAIL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMSERIAL = space(5)
      .w_ISALT = .f.
      .w_AM_INVIO = space(1)
      .w_AM_INVIO = space(1)
      .w_AMTYPSEA = 0
      .w_DESCRIZIONE = space(100)
      .w_SMTP = .f.
      .w_AMFLGPEC = space(1)
      .w_UTCODICE = 0
      .w_AMUTENTE = 0
      .w_AM_EMAIL = space(254)
      .w_INF_EMAIL = space(254)
      .w_AMSERVER = space(254)
      .w_AM_PORTA = 0
      .w_AMAUTSRV = space(1)
      .w_PWDCL = space(0)
      .w_AM_LOGIN = space(254)
      .w_PASSWD = space(254)
      .w_PASSWD2 = space(254)
      .w_AMPASSWD = space(254)
      .w_AMMODAUT = space(15)
      .w_AMMODCON = space(10)
      .w_AMWORKEY = space(20)
      .w_AMSERVEI = space(254)
      .w_AMSAMAIL = 0
      .w_AM_PORTI = 0
      .w_AMFILOGG = space(100)
      .w_AMDATINV = ctot("")
      .w_AMFILMIT = space(254)
      .w_NOTESMTP = space(0)
      .w_NOTEOUTL = space(0)
      .w_NOTESSL = space(0)
      .w_NOTEINFI = space(0)
      .w_NOTEMAPI = space(0)
      .w_NOTE = space(0)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_UTDESUTE = space(35)
      .w_AMSHARED = space(1)
      .w_AMCC_EML = space(254)
      .w_AMCCNEML = space(254)
      .w_AM_FIRMA = space(0)
      .w_PASSWD = space(254)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_ISALT = isalt()
        .w_AM_INVIO = 'S'
        .w_AM_INVIO = 'S'
        .w_AMTYPSEA = IIF(.w_AM_INVIO<>'I', 0, .w_AMTYPSEA)
          .DoRTCalc(6,6,.f.)
        .w_SMTP = .w_AM_INVIO $ 'SP' 
        .w_AMFLGPEC = iif(isnull(.w_GSUT_MMU) or .w_AM_INVIO<>"P", 'N', .w_AMFLGPEC)
          .DoRTCalc(9,9,.f.)
        .w_AMUTENTE = iif(.w_SMTP, 0, .w_UTCODICE)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_AMUTENTE))
          .link_1_11('Full')
          endif
        .w_AM_EMAIL = IIF(.w_AM_INVIO$'MOI' AND .w_AMTYPSEA<>4,ah_msgformat('Account %1 - (Utente %2) %3', ICASE(.w_AM_INVIO='M', 'MAPI',.w_AM_INVIO='O', 'Outlook','Infinity'), alltrim(str(.w_AMUTENTE,5,0)), IIF (.w_AM_INVIO='I', 'tipo autenticazione ' + alltrim(.w_DESCRIZIONE),'') ),'')
        .w_INF_EMAIL = IIF(.w_AM_INVIO='I' AND .w_AMTYPSEA=4, .w_INF_EMAIL, '')
          .DoRTCalc(13,13,.f.)
        .w_AM_PORTA = icase( .w_AM_INVIO='S' ,25 ,.w_AM_INVIO='P' ,465,0 )
        .w_AMAUTSRV = iif(.w_SMTP, 'S', 'N')
          .DoRTCalc(16,16,.f.)
        .w_AM_LOGIN = ' '
          .DoRTCalc(18,18,.f.)
        .w_PASSWD2 = iif(.w_PWDCL='S', .w_PASSWD, "")
        .w_AMPASSWD = iif(.w_AMAUTSRV='S' OR .w_AMTYPSEA=4 , iif((.w_PASSWD==.w_PASSWD2 OR  .w_AMTYPSEA=4) and not empty(.w_PASSWD), CIFRACNF(.w_PASSWD, "C"), .w_AMPASSWD), ' ')
        .w_AMMODAUT = '-auth'
        .w_AMMODCON = '-ssl'
        .w_AMWORKEY = 'DATA'
          .DoRTCalc(24,24,.f.)
        .w_AMSAMAIL = 1
        .w_AM_PORTI = 995
          .DoRTCalc(27,29,.f.)
        .w_NOTESMTP = "L'applicativo utilizzer� il servizio SMTP presente sul server specificato. Selezionando questo supporto non si ha la necessit� di definire un profilo di posta per ogni utente. La funzione mette a disposizione un client di posta per il solo invio."
        .w_NOTEOUTL = "L'applicativo utilizzer� il programma Microsoft Outlook installato nella macchina per l'invio della posta elettronica."
        .w_NOTESSL = "L'applicativo utilizzer� il servizio SMTP con SSL presente sul server specificato. Selezionando questo supporto � necessario specificare il profilo di posta (eventualmente PEC). La funzione mette a disposizione un client di posta per il solo invio."
        .w_NOTEINFI = "Account di tipo Infinity"
        .w_NOTEMAPI = "L'interfaccia MAPI utilizza le impostazioni di posta elettronica presenti sul sistema operativo. Per utilizzare questo supporto occorre definire quindi un profilo di posta per ogni utente. L'applicativo utilizzer� il client di posta della macchina."
        .w_NOTE = ICASE(.w_AM_INVIO='M',  .w_NOTEMAPI, .w_AM_INVIO='O',  .w_NOTEOUTL, .w_AM_INVIO='S',  .w_NOTESMTP, .w_AM_INVIO='P',  .w_NOTESSL, .w_AM_INVIO='I',  .w_NOTEINFI,'')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(iif(.w_PWDCL="S", "", "*"))
          .DoRTCalc(36,40,.f.)
        .w_AMSHARED = iif(.w_SMTP, 'N', 'S')
      endif
    endwith
    cp_BlankRecExtFlds(this,'ACC_MAIL')
    this.DoRTCalc(42,45,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEACM","w_AMSERIAL")
      .op_AMSERIAL = .w_AMSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAM_INVIO_1_3.enabled = i_bVal
      .Page1.oPag.oAM_INVIO_1_4.enabled = i_bVal
      .Page1.oPag.oAMTYPSEA_1_5.enabled = i_bVal
      .Page1.oPag.oAMFLGPEC_1_9.enabled = i_bVal
      .Page1.oPag.oAMUTENTE_1_11.enabled = i_bVal
      .Page1.oPag.oAM_EMAIL_1_16.enabled = i_bVal
      .Page1.oPag.oINF_EMAIL_1_17.enabled = i_bVal
      .Page1.oPag.oAMSERVER_1_18.enabled = i_bVal
      .Page1.oPag.oAM_PORTA_1_19.enabled = i_bVal
      .Page1.oPag.oAMAUTSRV_1_20.enabled = i_bVal
      .Page1.oPag.oPWDCL_1_21.enabled = i_bVal
      .Page1.oPag.oAM_LOGIN_1_22.enabled = i_bVal
      .Page1.oPag.oPASSWD_1_23.enabled = i_bVal
      .Page1.oPag.oPASSWD2_1_24.enabled = i_bVal
      .Page1.oPag.oAMMODAUT_1_27.enabled = i_bVal
      .Page1.oPag.oAMMODCON_1_28.enabled = i_bVal
      .Page1.oPag.oAMWORKEY_1_29.enabled = i_bVal
      .Page1.oPag.oAMSERVEI_1_30.enabled = i_bVal
      .Page1.oPag.oAM_PORTI_1_32.enabled = i_bVal
      .Page1.oPag.oAMSHARED_1_60.enabled = i_bVal
      .Page1.oPag.oAMCC_EML_1_61.enabled = i_bVal
      .Page1.oPag.oAMCCNEML_1_62.enabled = i_bVal
      .Page1.oPag.oAM_FIRMA_1_63.enabled = i_bVal
      .Page1.oPag.oPASSWD_1_74.enabled = i_bVal
      .Page1.oPag.oBtn_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_70.enabled = .Page1.oPag.oBtn_1_70.mCond()
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oAM_EMAIL_1_16.enabled = .t.
        .Page1.oPag.oINF_EMAIL_1_17.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ACC_MAIL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMSERIAL,"AMSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_INVIO,"AM_INVIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_INVIO,"AM_INVIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMTYPSEA,"AMTYPSEA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMFLGPEC,"AMFLGPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMUTENTE,"AMUTENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_EMAIL,"AM_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMSERVER,"AMSERVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_PORTA,"AM_PORTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMAUTSRV,"AMAUTSRV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_LOGIN,"AM_LOGIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMPASSWD,"AMPASSWD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMMODAUT,"AMMODAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMMODCON,"AMMODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMWORKEY,"AMWORKEY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMSERVEI,"AMSERVEI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMSAMAIL,"AMSAMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_PORTI,"AM_PORTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMFILOGG,"AMFILOGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMDATINV,"AMDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMFILMIT,"AMFILMIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMSHARED,"AMSHARED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCC_EML,"AMCC_EML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCCNEML,"AMCCNEML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AM_FIRMA,"AM_FIRMA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
    i_lTable = "ACC_MAIL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ACC_MAIL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- gsut_aam
    endproc
    
    ***** Ridefinisco ecpSave per chiudere la maschera 
    ***** in caricamento nuovo account se lanciato da GSUT_MMU
    Proc ecpSave()
       If This.cFunction="Load" And not isnull(this.w_GSUT_MMU) 
         If This.TerminateEdit() And This.CheckForm()
           DoDefault()
           this.cFunction="Query"
           this.ecpQuit()
         Endif
       Else
         DoDefault()
       Endif
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ACC_MAIL_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEACM","w_AMSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ACC_MAIL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ACC_MAIL')
        i_extval=cp_InsertValODBCExtFlds(this,'ACC_MAIL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AMSERIAL,AM_INVIO,AMTYPSEA,AMFLGPEC,AMUTENTE"+;
                  ",AM_EMAIL,AMSERVER,AM_PORTA,AMAUTSRV,AM_LOGIN"+;
                  ",AMPASSWD,AMMODAUT,AMMODCON,AMWORKEY,AMSERVEI"+;
                  ",AMSAMAIL,AM_PORTI,AMFILOGG,AMDATINV,AMFILMIT"+;
                  ",UTCC,UTCV,UTDC,UTDV,AMSHARED"+;
                  ",AMCC_EML,AMCCNEML,AM_FIRMA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AMSERIAL)+;
                  ","+cp_ToStrODBC(this.w_AM_INVIO)+;
                  ","+cp_ToStrODBC(this.w_AMTYPSEA)+;
                  ","+cp_ToStrODBC(this.w_AMFLGPEC)+;
                  ","+cp_ToStrODBCNull(this.w_AMUTENTE)+;
                  ","+cp_ToStrODBC(this.w_AM_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_AMSERVER)+;
                  ","+cp_ToStrODBC(this.w_AM_PORTA)+;
                  ","+cp_ToStrODBC(this.w_AMAUTSRV)+;
                  ","+cp_ToStrODBC(this.w_AM_LOGIN)+;
                  ","+cp_ToStrODBC(this.w_AMPASSWD)+;
                  ","+cp_ToStrODBC(this.w_AMMODAUT)+;
                  ","+cp_ToStrODBC(this.w_AMMODCON)+;
                  ","+cp_ToStrODBC(this.w_AMWORKEY)+;
                  ","+cp_ToStrODBC(this.w_AMSERVEI)+;
                  ","+cp_ToStrODBC(this.w_AMSAMAIL)+;
                  ","+cp_ToStrODBC(this.w_AM_PORTI)+;
                  ","+cp_ToStrODBC(this.w_AMFILOGG)+;
                  ","+cp_ToStrODBC(this.w_AMDATINV)+;
                  ","+cp_ToStrODBC(this.w_AMFILMIT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_AMSHARED)+;
                  ","+cp_ToStrODBC(this.w_AMCC_EML)+;
                  ","+cp_ToStrODBC(this.w_AMCCNEML)+;
                  ","+cp_ToStrODBC(this.w_AM_FIRMA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ACC_MAIL')
        i_extval=cp_InsertValVFPExtFlds(this,'ACC_MAIL')
        cp_CheckDeletedKey(i_cTable,0,'AMSERIAL',this.w_AMSERIAL)
        INSERT INTO (i_cTable);
              (AMSERIAL,AM_INVIO,AMTYPSEA,AMFLGPEC,AMUTENTE,AM_EMAIL,AMSERVER,AM_PORTA,AMAUTSRV,AM_LOGIN,AMPASSWD,AMMODAUT,AMMODCON,AMWORKEY,AMSERVEI,AMSAMAIL,AM_PORTI,AMFILOGG,AMDATINV,AMFILMIT,UTCC,UTCV,UTDC,UTDV,AMSHARED,AMCC_EML,AMCCNEML,AM_FIRMA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AMSERIAL;
                  ,this.w_AM_INVIO;
                  ,this.w_AMTYPSEA;
                  ,this.w_AMFLGPEC;
                  ,this.w_AMUTENTE;
                  ,this.w_AM_EMAIL;
                  ,this.w_AMSERVER;
                  ,this.w_AM_PORTA;
                  ,this.w_AMAUTSRV;
                  ,this.w_AM_LOGIN;
                  ,this.w_AMPASSWD;
                  ,this.w_AMMODAUT;
                  ,this.w_AMMODCON;
                  ,this.w_AMWORKEY;
                  ,this.w_AMSERVEI;
                  ,this.w_AMSAMAIL;
                  ,this.w_AM_PORTI;
                  ,this.w_AMFILOGG;
                  ,this.w_AMDATINV;
                  ,this.w_AMFILMIT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_AMSHARED;
                  ,this.w_AMCC_EML;
                  ,this.w_AMCCNEML;
                  ,this.w_AM_FIRMA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ACC_MAIL_IDX,i_nConn)
      *
      * update ACC_MAIL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ACC_MAIL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AM_INVIO="+cp_ToStrODBC(this.w_AM_INVIO)+;
             ",AMTYPSEA="+cp_ToStrODBC(this.w_AMTYPSEA)+;
             ",AMFLGPEC="+cp_ToStrODBC(this.w_AMFLGPEC)+;
             ",AMUTENTE="+cp_ToStrODBCNull(this.w_AMUTENTE)+;
             ",AM_EMAIL="+cp_ToStrODBC(this.w_AM_EMAIL)+;
             ",AMSERVER="+cp_ToStrODBC(this.w_AMSERVER)+;
             ",AM_PORTA="+cp_ToStrODBC(this.w_AM_PORTA)+;
             ",AMAUTSRV="+cp_ToStrODBC(this.w_AMAUTSRV)+;
             ",AM_LOGIN="+cp_ToStrODBC(this.w_AM_LOGIN)+;
             ",AMPASSWD="+cp_ToStrODBC(this.w_AMPASSWD)+;
             ",AMMODAUT="+cp_ToStrODBC(this.w_AMMODAUT)+;
             ",AMMODCON="+cp_ToStrODBC(this.w_AMMODCON)+;
             ",AMWORKEY="+cp_ToStrODBC(this.w_AMWORKEY)+;
             ",AMSERVEI="+cp_ToStrODBC(this.w_AMSERVEI)+;
             ",AMSAMAIL="+cp_ToStrODBC(this.w_AMSAMAIL)+;
             ",AM_PORTI="+cp_ToStrODBC(this.w_AM_PORTI)+;
             ",AMFILOGG="+cp_ToStrODBC(this.w_AMFILOGG)+;
             ",AMDATINV="+cp_ToStrODBC(this.w_AMDATINV)+;
             ",AMFILMIT="+cp_ToStrODBC(this.w_AMFILMIT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",AMSHARED="+cp_ToStrODBC(this.w_AMSHARED)+;
             ",AMCC_EML="+cp_ToStrODBC(this.w_AMCC_EML)+;
             ",AMCCNEML="+cp_ToStrODBC(this.w_AMCCNEML)+;
             ",AM_FIRMA="+cp_ToStrODBC(this.w_AM_FIRMA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ACC_MAIL')
        i_cWhere = cp_PKFox(i_cTable  ,'AMSERIAL',this.w_AMSERIAL  )
        UPDATE (i_cTable) SET;
              AM_INVIO=this.w_AM_INVIO;
             ,AMTYPSEA=this.w_AMTYPSEA;
             ,AMFLGPEC=this.w_AMFLGPEC;
             ,AMUTENTE=this.w_AMUTENTE;
             ,AM_EMAIL=this.w_AM_EMAIL;
             ,AMSERVER=this.w_AMSERVER;
             ,AM_PORTA=this.w_AM_PORTA;
             ,AMAUTSRV=this.w_AMAUTSRV;
             ,AM_LOGIN=this.w_AM_LOGIN;
             ,AMPASSWD=this.w_AMPASSWD;
             ,AMMODAUT=this.w_AMMODAUT;
             ,AMMODCON=this.w_AMMODCON;
             ,AMWORKEY=this.w_AMWORKEY;
             ,AMSERVEI=this.w_AMSERVEI;
             ,AMSAMAIL=this.w_AMSAMAIL;
             ,AM_PORTI=this.w_AM_PORTI;
             ,AMFILOGG=this.w_AMFILOGG;
             ,AMDATINV=this.w_AMDATINV;
             ,AMFILMIT=this.w_AMFILMIT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,AMSHARED=this.w_AMSHARED;
             ,AMCC_EML=this.w_AMCC_EML;
             ,AMCCNEML=this.w_AMCCNEML;
             ,AM_FIRMA=this.w_AM_FIRMA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsut_aam
    If not btrserr AND g_AccountMail=this.w_AMSERIAL
        * Supporto MAIL --- CFGMAIL imposta: g_MITTEN,g_INVIO,g_SRVMAIL,g_SRVPORTA,g_CCEmail,g_CCNEmail,g_UEFIRM
        g_MITTEN = CFGMAIL(g_CODUTE,"S",.T.)
    Endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ACC_MAIL_IDX,i_nConn)
      *
      * delete ACC_MAIL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AMSERIAL',this.w_AMSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ACC_MAIL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ACC_MAIL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AMTYPSEA = IIF(.w_AM_INVIO<>'I', 0, .w_AMTYPSEA)
        endif
        .DoRTCalc(6,6,.t.)
            .w_SMTP = .w_AM_INVIO $ 'SP' 
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AMFLGPEC = iif(isnull(.w_GSUT_MMU) or .w_AM_INVIO<>"P", 'N', .w_AMFLGPEC)
        endif
        .DoRTCalc(9,9,.t.)
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AMUTENTE = iif(.w_SMTP, 0, .w_UTCODICE)
          .link_1_11('Full')
        endif
        if .o_AMTYPSEA<>.w_AMTYPSEA.or. .o_AM_INVIO<>.w_AM_INVIO
          .Calculate_KSJCCPEXCH()
        endif
        if .o_AM_INVIO<>.w_AM_INVIO.or. .o_AMUTENTE<>.w_AMUTENTE.or. .o_AMTYPSEA<>.w_AMTYPSEA
            .w_AM_EMAIL = IIF(.w_AM_INVIO$'MOI' AND .w_AMTYPSEA<>4,ah_msgformat('Account %1 - (Utente %2) %3', ICASE(.w_AM_INVIO='M', 'MAPI',.w_AM_INVIO='O', 'Outlook','Infinity'), alltrim(str(.w_AMUTENTE,5,0)), IIF (.w_AM_INVIO='I', 'tipo autenticazione ' + alltrim(.w_DESCRIZIONE),'') ),'')
        endif
        if .o_AM_INVIO<>.w_AM_INVIO.or. .o_AMTYPSEA<>.w_AMTYPSEA.or. .o_AMUTENTE<>.w_AMUTENTE
            .w_INF_EMAIL = IIF(.w_AM_INVIO='I' AND .w_AMTYPSEA=4, .w_INF_EMAIL, '')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AM_PORTA = icase( .w_AM_INVIO='S' ,25 ,.w_AM_INVIO='P' ,465,0 )
        endif
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AMAUTSRV = iif(.w_SMTP, 'S', 'N')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_AMAUTSRV<>.w_AMAUTSRV
            .w_AM_LOGIN = ' '
        endif
        .DoRTCalc(18,18,.t.)
        if .o_PWDCL<>.w_PWDCL.or. .o_PASSWD<>.w_PASSWD
            .w_PASSWD2 = iif(.w_PWDCL='S', .w_PASSWD, "")
        endif
        if .o_PASSWD2<>.w_PASSWD2.or. .o_AMAUTSRV<>.w_AMAUTSRV.or. .o_PASSWD<>.w_PASSWD
            .w_AMPASSWD = iif(.w_AMAUTSRV='S' OR .w_AMTYPSEA=4 , iif((.w_PASSWD==.w_PASSWD2 OR  .w_AMTYPSEA=4) and not empty(.w_PASSWD), CIFRACNF(.w_PASSWD, "C"), .w_AMPASSWD), ' ')
        endif
        .DoRTCalc(21,25,.t.)
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AM_PORTI = 995
        endif
        .DoRTCalc(27,34,.t.)
            .w_NOTE = ICASE(.w_AM_INVIO='M',  .w_NOTEMAPI, .w_AM_INVIO='O',  .w_NOTEOUTL, .w_AM_INVIO='S',  .w_NOTESMTP, .w_AM_INVIO='P',  .w_NOTESSL, .w_AM_INVIO='I',  .w_NOTEINFI,'')
        if .o_PWDCL<>.w_PWDCL
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(iif(.w_PWDCL="S", "", "*"))
        endif
        .DoRTCalc(36,40,.t.)
        if .o_AM_INVIO<>.w_AM_INVIO
            .w_AMSHARED = iif(.w_SMTP, 'N', 'S')
        endif
        if .o_AM_INVIO<>.w_AM_INVIO.or. .o_AMTYPSEA<>.w_AMTYPSEA
          .Calculate_OXQEYRHRBT()
        endif
        if .o_PASSWD<>.w_PASSWD
          .Calculate_DNKBBAUMDW()
        endif
        * --- Area Manuale = Calculate
        * --- gsut_aam
        **valorizza sempre il campo a 1 (funzionalit� attualmente non gestita da infinity)
        this.w_AMSAMAIL=1
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,45,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate(iif(.w_PWDCL="S", "", "*"))
    endwith
  return

  proc Calculate_KSJCCPEXCH()
    with this
          * --- Descrizione Mail
          MailTypeDesc(this;
             )
    endwith
  endproc
  proc Calculate_UVNNKBXDJK()
    with this
          * --- PASSWD
          .w_PASSWD = CIFRACNF(.w_AMPASSWD, "D")
    endwith
  endproc
  proc Calculate_YERQGHNUBN()
    with this
          * --- gsut_bam
     if not isnull(.w_GSUT_MMU)
          gsut_bam(this;
              ,.w_AMSERIAL;
             )
     endif
    endwith
  endproc
  proc Calculate_OXQEYRHRBT()
    with this
          * --- Aggiorna Flag PEC infinity
          .w_AMFLGPEC = IIF (.w_AM_INVIO='I' , IIF ( .w_AMTYPSEA=1 OR .w_AMTYPSEA=3 , 'S','N'   ) , .w_AMFLGPEC)
    endwith
  endproc
  proc Calculate_DNKBBAUMDW()
    with this
          * --- Aggiorna campo Login per infinity
     if .w_AMTYPSEA=4
          .w_AM_LOGIN = .w_PASSWD
     endif
    endwith
  endproc
  proc Calculate_KAWNNPXAJW()
    with this
          * --- Valorizzazione INF_EMAIL
          .w_INF_EMAIL = STRTRAN(.w_AM_EMAIL, 'infinity:', '')
    endwith
  endproc
  proc Calculate_ZCQRPXABTK()
    with this
          * --- AM_EMAIL = INF_EMAIL
     if .w_AM_INVIO='I' AND .w_AMTYPSEA=4
          .w_AM_EMAIL = 'infinity:' + .w_INF_EMAIL
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.enabled = this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.enabled = this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.enabled = this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.enabled = this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.mCond()
    this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.enabled = this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.mCond()
    this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.enabled = this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPASSWD_1_23.enabled = this.oPgFrm.Page1.oPag.oPASSWD_1_23.mCond()
    this.oPgFrm.Page1.oPag.oPASSWD_1_74.enabled = this.oPgFrm.Page1.oPag.oPASSWD_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oINF_EMAIL_1_17
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.visible=!this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.mHide()
    this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.visible=!this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.mHide()
    this.oPgFrm.Page1.oPag.oAMTYPSEA_1_5.visible=!this.oPgFrm.Page1.oPag.oAMTYPSEA_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.visible=!this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.mHide()
    this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.visible=!this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.visible=!this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.mHide()
    this.oPgFrm.Page1.oPag.oINF_EMAIL_1_17.visible=!this.oPgFrm.Page1.oPag.oINF_EMAIL_1_17.mHide()
    this.oPgFrm.Page1.oPag.oAMSERVER_1_18.visible=!this.oPgFrm.Page1.oPag.oAMSERVER_1_18.mHide()
    this.oPgFrm.Page1.oPag.oAM_PORTA_1_19.visible=!this.oPgFrm.Page1.oPag.oAM_PORTA_1_19.mHide()
    this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.visible=!this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPWDCL_1_21.visible=!this.oPgFrm.Page1.oPag.oPWDCL_1_21.mHide()
    this.oPgFrm.Page1.oPag.oAM_LOGIN_1_22.visible=!this.oPgFrm.Page1.oPag.oAM_LOGIN_1_22.mHide()
    this.oPgFrm.Page1.oPag.oPASSWD_1_23.visible=!this.oPgFrm.Page1.oPag.oPASSWD_1_23.mHide()
    this.oPgFrm.Page1.oPag.oPASSWD2_1_24.visible=!this.oPgFrm.Page1.oPag.oPASSWD2_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oAMMODAUT_1_27.visible=!this.oPgFrm.Page1.oPag.oAMMODAUT_1_27.mHide()
    this.oPgFrm.Page1.oPag.oAMMODCON_1_28.visible=!this.oPgFrm.Page1.oPag.oAMMODCON_1_28.mHide()
    this.oPgFrm.Page1.oPag.oAMWORKEY_1_29.visible=!this.oPgFrm.Page1.oPag.oAMWORKEY_1_29.mHide()
    this.oPgFrm.Page1.oPag.oAMSERVEI_1_30.visible=!this.oPgFrm.Page1.oPag.oAMSERVEI_1_30.mHide()
    this.oPgFrm.Page1.oPag.oAM_PORTI_1_32.visible=!this.oPgFrm.Page1.oPag.oAM_PORTI_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oUTDESUTE_1_58.visible=!this.oPgFrm.Page1.oPag.oUTDESUTE_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_70.visible=!this.oPgFrm.Page1.oPag.oBtn_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oPASSWD_1_74.visible=!this.oPgFrm.Page1.oPag.oPASSWD_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("w_AMAUTSRV Changed")
          .Calculate_UVNNKBXDJK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_YERQGHNUBN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_KAWNNPXAJW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_ZCQRPXABTK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AMUTENTE
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AMUTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_AMUTENTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_AMUTENTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_AMUTENTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oAMUTENTE_1_11'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AMUTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_AMUTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_AMUTENTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AMUTENTE = NVL(_Link_.code,0)
      this.w_UTDESUTE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AMUTENTE = 0
      endif
      this.w_UTDESUTE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AMUTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.RadioValue()==this.w_AM_INVIO)
      this.oPgFrm.Page1.oPag.oAM_INVIO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.RadioValue()==this.w_AM_INVIO)
      this.oPgFrm.Page1.oPag.oAM_INVIO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMTYPSEA_1_5.RadioValue()==this.w_AMTYPSEA)
      this.oPgFrm.Page1.oPag.oAMTYPSEA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.RadioValue()==this.w_AMFLGPEC)
      this.oPgFrm.Page1.oPag.oAMFLGPEC_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.value==this.w_AMUTENTE)
      this.oPgFrm.Page1.oPag.oAMUTENTE_1_11.value=this.w_AMUTENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.value==this.w_AM_EMAIL)
      this.oPgFrm.Page1.oPag.oAM_EMAIL_1_16.value=this.w_AM_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oINF_EMAIL_1_17.value==this.w_INF_EMAIL)
      this.oPgFrm.Page1.oPag.oINF_EMAIL_1_17.value=this.w_INF_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAMSERVER_1_18.value==this.w_AMSERVER)
      this.oPgFrm.Page1.oPag.oAMSERVER_1_18.value=this.w_AMSERVER
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_PORTA_1_19.value==this.w_AM_PORTA)
      this.oPgFrm.Page1.oPag.oAM_PORTA_1_19.value=this.w_AM_PORTA
    endif
    if not(this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.RadioValue()==this.w_AMAUTSRV)
      this.oPgFrm.Page1.oPag.oAMAUTSRV_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDCL_1_21.RadioValue()==this.w_PWDCL)
      this.oPgFrm.Page1.oPag.oPWDCL_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_LOGIN_1_22.value==this.w_AM_LOGIN)
      this.oPgFrm.Page1.oPag.oAM_LOGIN_1_22.value=this.w_AM_LOGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPASSWD_1_23.value==this.w_PASSWD)
      this.oPgFrm.Page1.oPag.oPASSWD_1_23.value=this.w_PASSWD
    endif
    if not(this.oPgFrm.Page1.oPag.oPASSWD2_1_24.value==this.w_PASSWD2)
      this.oPgFrm.Page1.oPag.oPASSWD2_1_24.value=this.w_PASSWD2
    endif
    if not(this.oPgFrm.Page1.oPag.oAMMODAUT_1_27.RadioValue()==this.w_AMMODAUT)
      this.oPgFrm.Page1.oPag.oAMMODAUT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMMODCON_1_28.RadioValue()==this.w_AMMODCON)
      this.oPgFrm.Page1.oPag.oAMMODCON_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMWORKEY_1_29.value==this.w_AMWORKEY)
      this.oPgFrm.Page1.oPag.oAMWORKEY_1_29.value=this.w_AMWORKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oAMSERVEI_1_30.value==this.w_AMSERVEI)
      this.oPgFrm.Page1.oPag.oAMSERVEI_1_30.value=this.w_AMSERVEI
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_PORTI_1_32.value==this.w_AM_PORTI)
      this.oPgFrm.Page1.oPag.oAM_PORTI_1_32.value=this.w_AM_PORTI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_44.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_44.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDESUTE_1_58.value==this.w_UTDESUTE)
      this.oPgFrm.Page1.oPag.oUTDESUTE_1_58.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oAMSHARED_1_60.RadioValue()==this.w_AMSHARED)
      this.oPgFrm.Page1.oPag.oAMSHARED_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCC_EML_1_61.value==this.w_AMCC_EML)
      this.oPgFrm.Page1.oPag.oAMCC_EML_1_61.value=this.w_AMCC_EML
    endif
    if not(this.oPgFrm.Page1.oPag.oAMCCNEML_1_62.value==this.w_AMCCNEML)
      this.oPgFrm.Page1.oPag.oAMCCNEML_1_62.value=this.w_AMCCNEML
    endif
    if not(this.oPgFrm.Page1.oPag.oAM_FIRMA_1_63.value==this.w_AM_FIRMA)
      this.oPgFrm.Page1.oPag.oAM_FIRMA_1_63.value=this.w_AM_FIRMA
    endif
    if not(this.oPgFrm.Page1.oPag.oPASSWD_1_74.value==this.w_PASSWD)
      this.oPgFrm.Page1.oPag.oPASSWD_1_74.value=this.w_PASSWD
    endif
    cp_SetControlsValueExtFlds(this,'ACC_MAIL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_AMAUTSRV<>"S" OR NOT EMPTY(.w_AM_LOGIN) AND NOT EMPTY(.w_AMPASSWD))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Login e password sono dati obbligatori")
          case   (empty(.w_AMUTENTE))  and not(.w_SMTP  OR .w_AMTYPSEA=4)  and (empty(.w_UTCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAMUTENTE_1_11.SetFocus()
            i_bnoObbl = !empty(.w_AMUTENTE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non definito")
          case   ((empty(.w_AM_EMAIL) and (.w_AMTYPSEA=4)) or not(not empty(.w_AM_EMAIL)))  and not(.w_AM_INVIO='I' AND .w_AMTYPSEA=4)  and (.w_SMTP OR .w_AMTYPSEA=4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAM_EMAIL_1_16.SetFocus()
            i_bnoObbl = !empty(.w_AM_EMAIL) or !(.w_AMTYPSEA=4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Compilare l'indirizzo mail")
          case   (empty(.w_INF_EMAIL) and (.w_AMTYPSEA=4))  and not(.w_AM_INVIO<>'I' OR .w_AMTYPSEA<>4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINF_EMAIL_1_17.SetFocus()
            i_bnoObbl = !empty(.w_INF_EMAIL) or !(.w_AMTYPSEA=4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Compilare l'indirizzo mail")
          case   (empty(.w_AMSERVER))  and not(!.w_SMTP )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAMSERVER_1_18.SetFocus()
            i_bnoObbl = !empty(.w_AMSERVER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PASSWD))  and not(.w_AMAUTSRV<>"S" )  and (.cFunction="Load")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPASSWD_1_23.SetFocus()
            i_bnoObbl = !empty(.w_PASSWD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_PASSWD) or .w_PASSWD==.w_PASSWD2)  and not(.w_AMAUTSRV<>"S" or .cFunction<>"Load" or empty(.w_PASSWD) or .w_PWDCL="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPASSWD2_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le password digitate non corrispondono")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AM_INVIO = this.w_AM_INVIO
    this.o_AMTYPSEA = this.w_AMTYPSEA
    this.o_AMUTENTE = this.w_AMUTENTE
    this.o_AMAUTSRV = this.w_AMAUTSRV
    this.o_PWDCL = this.w_PWDCL
    this.o_PASSWD = this.w_PASSWD
    this.o_PASSWD2 = this.w_PASSWD2
    return

  func CanEdit()
    local i_res
    i_res=cp_IsAdministrator(.t.)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=cp_IsAdministrator(.t.)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=cp_IsAdministrator(.t.)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eseguire operazione. Utente non amministratore"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_aamPag1 as StdContainer
  Width  = 754
  height = 536
  stdWidth  = 754
  stdheight = 536
  resizeYpos=464
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAM_INVIO_1_3 as StdCombo with uid="EJBQWOOKQZ",rtseq=3,rtrep=.f.,left=147,top=18,width=169,height=22;
    , ToolTipText = "Supporto per l'invio mail (interfaccia MAPI o servizio SMTP o servizio SMTP con SSL o Microsoft Outlook)";
    , HelpContextID = 44432981;
    , cFormVar="w_AM_INVIO",RowSource=""+"Interfaccia MAPI,"+"Microsoft Outlook,"+"Servizio SMTP,"+"Servizio SMTP con SSL,"+"Infinity", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAM_INVIO_1_3.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'O',;
    iif(this.value =3,'S',;
    iif(this.value =4,'P',;
    iif(this.value =5,'I',;
    space(1)))))))
  endfunc
  func oAM_INVIO_1_3.GetRadio()
    this.Parent.oContained.w_AM_INVIO = this.RadioValue()
    return .t.
  endfunc

  func oAM_INVIO_1_3.SetRadio()
    this.Parent.oContained.w_AM_INVIO=trim(this.Parent.oContained.w_AM_INVIO)
    this.value = ;
      iif(this.Parent.oContained.w_AM_INVIO=='M',1,;
      iif(this.Parent.oContained.w_AM_INVIO=='O',2,;
      iif(this.Parent.oContained.w_AM_INVIO=='S',3,;
      iif(this.Parent.oContained.w_AM_INVIO=='P',4,;
      iif(this.Parent.oContained.w_AM_INVIO=='I',5,;
      0)))))
  endfunc

  func oAM_INVIO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAM_INVIO_1_3.mHide()
    with this.Parent.oContained
      return (g_REVI<>'S')
    endwith
  endfunc


  add object oAM_INVIO_1_4 as StdCombo with uid="PKYKQQMKBW",rtseq=4,rtrep=.f.,left=147,top=18,width=169,height=22;
    , ToolTipText = "Supporto per l'invio mail (interfaccia MAPI o servizio SMTP o servizio SMTP con SSL o Microsoft Outlook)";
    , HelpContextID = 44432981;
    , cFormVar="w_AM_INVIO",RowSource=""+"Interfaccia MAPI,"+"Microsoft Outlook,"+"Servizio SMTP,"+"Servizio SMTP con SSL", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAM_INVIO_1_4.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'O',;
    iif(this.value =3,'S',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oAM_INVIO_1_4.GetRadio()
    this.Parent.oContained.w_AM_INVIO = this.RadioValue()
    return .t.
  endfunc

  func oAM_INVIO_1_4.SetRadio()
    this.Parent.oContained.w_AM_INVIO=trim(this.Parent.oContained.w_AM_INVIO)
    this.value = ;
      iif(this.Parent.oContained.w_AM_INVIO=='M',1,;
      iif(this.Parent.oContained.w_AM_INVIO=='O',2,;
      iif(this.Parent.oContained.w_AM_INVIO=='S',3,;
      iif(this.Parent.oContained.w_AM_INVIO=='P',4,;
      0))))
  endfunc

  func oAM_INVIO_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oAM_INVIO_1_4.mHide()
    with this.Parent.oContained
      return (g_REVI='S')
    endwith
  endfunc


  add object oAMTYPSEA_1_5 as StdCombo with uid="VAFAPKGNHP",value=1,rtseq=5,rtrep=.f.,left=530,top=18,width=215,height=22;
    , ToolTipText = "Tipo di ricerca account Infinity per determinare l'account mail mittente da utilizzare";
    , HelpContextID = 2798009;
    , cFormVar="w_AMTYPSEA",RowSource=""+"Aziendale standard,"+"Aziendale PEC,"+"Personale standard,"+"Personale PEC,"+"Per indirizzo email specificato ", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAMTYPSEA_1_5.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    0))))))
  endfunc
  func oAMTYPSEA_1_5.GetRadio()
    this.Parent.oContained.w_AMTYPSEA = this.RadioValue()
    return .t.
  endfunc

  func oAMTYPSEA_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AMTYPSEA==0,1,;
      iif(this.Parent.oContained.w_AMTYPSEA==1,2,;
      iif(this.Parent.oContained.w_AMTYPSEA==2,3,;
      iif(this.Parent.oContained.w_AMTYPSEA==3,4,;
      iif(this.Parent.oContained.w_AMTYPSEA==4,5,;
      0)))))
  endfunc

  func oAMTYPSEA_1_5.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'I')
    endwith
  endfunc

  add object oAMFLGPEC_1_9 as StdCheck with uid="VKUQKKCIGG",rtseq=8,rtrep=.f.,left=147, top=44, caption="PEC", tabstop=.f.,;
    ToolTipText = "Account di invio di posta elettronica certificata",;
    HelpContextID = 63476151,;
    cFormVar="w_AMFLGPEC", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oAMFLGPEC_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAMFLGPEC_1_9.GetRadio()
    this.Parent.oContained.w_AMFLGPEC = this.RadioValue()
    return .t.
  endfunc

  func oAMFLGPEC_1_9.SetRadio()
    this.Parent.oContained.w_AMFLGPEC=trim(this.Parent.oContained.w_AMFLGPEC)
    this.value = ;
      iif(this.Parent.oContained.w_AMFLGPEC=='S',1,;
      0)
  endfunc

  func oAMFLGPEC_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AM_INVIO='P' OR .w_AMTYPSEA=4)
    endwith
   endif
  endfunc

  func oAMFLGPEC_1_9.mHide()
    with this.Parent.oContained
      return (!.w_SMTP and .w_AMTYPSEA<>4)
    endwith
  endfunc

  add object oAMUTENTE_1_11 as StdField with uid="WMWUVWMSDT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AMUTENTE", cQueryName = "AMUTENTE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Utente non definito",;
    ToolTipText = "Codice utente al quale � associabile l'account",;
    HelpContextID = 98542005,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=440, Top=44, cSayPict='"9999"', cGetPict='"9999"', cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_AMUTENTE"

  func oAMUTENTE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_UTCODICE))
    endwith
   endif
  endfunc

  func oAMUTENTE_1_11.mHide()
    with this.Parent.oContained
      return (.w_SMTP  OR .w_AMTYPSEA=4)
    endwith
  endfunc

  func oAMUTENTE_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oAMUTENTE_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oAM_EMAIL_1_16 as StdField with uid="HZSLMCQXUO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AM_EMAIL", cQueryName = "AM_EMAIL",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Compilare l'indirizzo mail",;
    HelpContextID = 227671634,;
   bGlobalFont=.t.,;
    Height=21, Width=598, Left=147, Top=129, InputMask=replicate('X',254)

  func oAM_EMAIL_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SMTP OR .w_AMTYPSEA=4)
    endwith
   endif
  endfunc

  func oAM_EMAIL_1_16.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO='I' AND .w_AMTYPSEA=4)
    endwith
  endfunc

  func oAM_EMAIL_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_AM_EMAIL))
    endwith
    return bRes
  endfunc

  func oAM_EMAIL_1_16.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_AMTYPSEA=4
    endwith
    return i_bres
  endfunc

  add object oINF_EMAIL_1_17 as StdField with uid="EIQRBYUEOM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_INF_EMAIL", cQueryName = "AM_EMAIL",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Compilare l'indirizzo mail",;
    HelpContextID = 114658161,;
   bGlobalFont=.t.,;
    Height=21, Width=598, Left=147, Top=129, InputMask=replicate('X',254)

  func oINF_EMAIL_1_17.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'I' OR .w_AMTYPSEA<>4)
    endwith
  endfunc

  func oINF_EMAIL_1_17.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_AMTYPSEA=4
    endwith
    return i_bres
  endfunc

  add object oAMSERVER_1_18 as StdField with uid="QCLJOEFXMI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AMSERVER", cQueryName = "AMSERVER",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 220119464,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=154, InputMask=replicate('X',254)

  func oAMSERVER_1_18.mHide()
    with this.Parent.oContained
      return (!.w_SMTP )
    endwith
  endfunc

  add object oAM_PORTA_1_19 as StdField with uid="NMYIAMTLNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AM_PORTA", cQueryName = "AM_PORTA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero porta sulla quale il servizio riceve le mail da inviare (usualmente per smtp 25, per SMTP con SSL 465)",;
    HelpContextID = 21168569,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=690, Top=153

  func oAM_PORTA_1_19.mHide()
    with this.Parent.oContained
      return (!.w_SMTP)
    endwith
  endfunc

  add object oAMAUTSRV_1_20 as StdCheck with uid="YAHGLULTAX",rtseq=15,rtrep=.f.,left=147, top=177, caption="Autenticazione utente",;
    ToolTipText = "Se attivo abilita autenticazione utente/password durante invio mail",;
    HelpContextID = 267379108,;
    cFormVar="w_AMAUTSRV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAMAUTSRV_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAMAUTSRV_1_20.GetRadio()
    this.Parent.oContained.w_AMAUTSRV = this.RadioValue()
    return .t.
  endfunc

  func oAMAUTSRV_1_20.SetRadio()
    this.Parent.oContained.w_AMAUTSRV=trim(this.Parent.oContained.w_AMAUTSRV)
    this.value = ;
      iif(this.Parent.oContained.w_AMAUTSRV=='S',1,;
      0)
  endfunc

  func oAMAUTSRV_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AM_INVIO='S')
    endwith
   endif
  endfunc

  func oAMAUTSRV_1_20.mHide()
    with this.Parent.oContained
      return (!.w_SMTP)
    endwith
  endfunc

  add object oPWDCL_1_21 as StdCheck with uid="ZOZEABRGYY",rtseq=16,rtrep=.f.,left=362, top=177, caption="Mostra password",;
    ToolTipText = "Visualizza la password in chiaro",;
    HelpContextID = 209606902,;
    cFormVar="w_PWDCL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPWDCL_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPWDCL_1_21.GetRadio()
    this.Parent.oContained.w_PWDCL = this.RadioValue()
    return .t.
  endfunc

  func oPWDCL_1_21.SetRadio()
    this.Parent.oContained.w_PWDCL=trim(this.Parent.oContained.w_PWDCL)
    this.value = ;
      iif(this.Parent.oContained.w_PWDCL=='S',1,;
      0)
  endfunc

  func oPWDCL_1_21.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" or .cFunction<>"Load")
    endwith
  endfunc

  add object oAM_LOGIN_1_22 as StdField with uid="GKTBZTULHR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AM_LOGIN", cQueryName = "AM_LOGIN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Login di autenticazione",;
    HelpContextID = 62455380,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=199, InputMask=replicate('X',254)

  func oAM_LOGIN_1_22.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S")
    endwith
  endfunc

  add object oPASSWD_1_23 as StdField with uid="ZPQEAMNMKX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PASSWD", cQueryName = "PASSWD",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 247516426,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=223, InputMask=replicate('X',254), passwordchar="*"

  func oPASSWD_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction="Load")
    endwith
   endif
  endfunc

  func oPASSWD_1_23.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" )
    endwith
  endfunc

  add object oPASSWD2_1_24 as StdField with uid="JUIKNTUSHO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PASSWD2", cQueryName = "PASSWD2",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Le password digitate non corrispondono",;
    HelpContextID = 247516426,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=247, InputMask=replicate('X',254), passwordchar="*"

  func oPASSWD2_1_24.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" or .cFunction<>"Load" or empty(.w_PASSWD) or .w_PWDCL="S")
    endwith
  endfunc

  func oPASSWD2_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_PASSWD) or .w_PASSWD==.w_PASSWD2)
    endwith
    return bRes
  endfunc


  add object oBtn_1_26 as StdButton with uid="WEULJWSHPP",left=697, top=195, width=48,height=45,;
    CpPicture="BMP\LOG_PWD.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Modifica password";
    , HelpContextID = 238365478;
    , caption='\<Mod.pw';
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      do gsut_kmp with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" or .cFunction<>"Edit")
     endwith
    endif
  endfunc


  add object oAMMODAUT_1_27 as StdCombo with uid="HUYPANPKDK",rtseq=21,rtrep=.f.,left=147,top=273,width=169,height=22;
    , ToolTipText = "Modalit� autenticazione";
    , HelpContextID = 49619366;
    , cFormVar="w_AMMODAUT",RowSource=""+"Normale,"+"Login,"+"Cram-md5,"+"Plain", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAMMODAUT_1_27.RadioValue()
    return(iif(this.value =1,'-auth',;
    iif(this.value =2,'-auth-login',;
    iif(this.value =3,'-auth-cram-md5',;
    iif(this.value =4,'-auth-plain',;
    space(15))))))
  endfunc
  func oAMMODAUT_1_27.GetRadio()
    this.Parent.oContained.w_AMMODAUT = this.RadioValue()
    return .t.
  endfunc

  func oAMMODAUT_1_27.SetRadio()
    this.Parent.oContained.w_AMMODAUT=trim(this.Parent.oContained.w_AMMODAUT)
    this.value = ;
      iif(this.Parent.oContained.w_AMMODAUT=='-auth',1,;
      iif(this.Parent.oContained.w_AMMODAUT=='-auth-login',2,;
      iif(this.Parent.oContained.w_AMMODAUT=='-auth-cram-md5',3,;
      iif(this.Parent.oContained.w_AMMODAUT=='-auth-plain',4,;
      0))))
  endfunc

  func oAMMODAUT_1_27.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc


  add object oAMMODCON_1_28 as StdCombo with uid="JYQHODNKCH",rtseq=22,rtrep=.f.,left=486,top=273,width=83,height=22;
    , ToolTipText = "Parametro di criptazione del protocollo di comunicazione";
    , HelpContextID = 252370516;
    , cFormVar="w_AMMODCON",RowSource=""+"SSL/TLS,"+"STARTTLS", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAMMODCON_1_28.RadioValue()
    return(iif(this.value =1,'-ssl',;
    iif(this.value =2,'-starttls',;
    space(10))))
  endfunc
  func oAMMODCON_1_28.GetRadio()
    this.Parent.oContained.w_AMMODCON = this.RadioValue()
    return .t.
  endfunc

  func oAMMODCON_1_28.SetRadio()
    this.Parent.oContained.w_AMMODCON=trim(this.Parent.oContained.w_AMMODCON)
    this.value = ;
      iif(this.Parent.oContained.w_AMMODCON=='-ssl',1,;
      iif(this.Parent.oContained.w_AMMODCON=='-starttls',2,;
      0))
  endfunc

  func oAMMODCON_1_28.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc

  add object oAMWORKEY_1_29 as StdField with uid="TCWSJEUATS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AMWORKEY", cQueryName = "AMWORKEY",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Parola chiave che identifica inizio corpo mail (gestione log errori di invio mail)",;
    HelpContextID = 135561633,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=147, Top=299, InputMask=replicate('X',20)

  func oAMWORKEY_1_29.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc

  add object oAMSERVEI_1_30 as StdField with uid="OHMJGXFLUA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_AMSERVEI", cQueryName = "AMSERVEI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Server su cui ricevere le mail",;
    HelpContextID = 220119473,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=337, InputMask=replicate('X',254)

  func oAMSERVEI_1_30.mHide()
    with this.Parent.oContained
      return (!.w_SMTP OR !.w_ISALT)
    endwith
  endfunc

  add object oAM_PORTI_1_32 as StdField with uid="SPIPYYFXZY",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AM_PORTI", cQueryName = "AM_PORTI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero porta sulla quale il servizio riceve mail da client",;
    HelpContextID = 21168561,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=690, Top=336

  func oAM_PORTI_1_32.mHide()
    with this.Parent.oContained
      return (!.w_SMTP OR !.w_ISALT)
    endwith
  endfunc


  add object oBtn_1_36 as StdButton with uid="WYFZPWCUPG",left=697, top=289, width=48,height=45,;
    CpPicture="BMP\Implodi.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai filtri leggi PEC ";
    , HelpContextID = 133043626;
    , caption='\<Filtri';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      do GSUT2KLP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.cFunction=='Edit' OR .cFunction=='Load') AND .w_AMAUTSRV='S')
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not .w_ISALT )
     endwith
    endif
  endfunc

  add object oNOTE_1_44 as StdMemo with uid="HPPGVMXKPX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 130109654,;
   bGlobalFont=.t.,;
    Height=50, Width=598, Left=147, Top=72


  add object oObj_1_56 as cp_setobjprop with uid="KWUURUPULT",left=809, top=181, width=120,height=21,;
    caption='OLDPSW',;
   bGlobalFont=.t.,;
    cObj="w_PASSWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 201634330

  add object oUTDESUTE_1_58 as StdField with uid="BRQYEWHNCT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235907445,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=497, Top=44, InputMask=replicate('X',35)

  func oUTDESUTE_1_58.mHide()
    with this.Parent.oContained
      return (.w_SMTP  OR .w_AMTYPSEA=4)
    endwith
  endfunc

  add object oAMSHARED_1_60 as StdCheck with uid="ALQQZUCFXK",rtseq=41,rtrep=.f.,left=147, top=383, caption="Account condiviso",;
    ToolTipText = "Se attivo, utilizza i dati mail (cc, ccn, firma) dell'utente se e solo se i campi sottostanti sono vuoti",;
    HelpContextID = 36422070,;
    cFormVar="w_AMSHARED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAMSHARED_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAMSHARED_1_60.GetRadio()
    this.Parent.oContained.w_AMSHARED = this.RadioValue()
    return .t.
  endfunc

  func oAMSHARED_1_60.SetRadio()
    this.Parent.oContained.w_AMSHARED=trim(this.Parent.oContained.w_AMSHARED)
    this.value = ;
      iif(this.Parent.oContained.w_AMSHARED=='S',1,;
      0)
  endfunc

  add object oAMCC_EML_1_61 as StdField with uid="RXYSGUGFWO",rtseq=42,rtrep=.f.,;
    cFormVar = "w_AMCC_EML", cQueryName = "AMCC_EML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia le mail inviate dal gestionale",;
    HelpContextID = 44973650,;
   bGlobalFont=.t.,;
    Height=21, Width=598, Left=147, Top=406, InputMask=replicate('X',254)

  add object oAMCCNEML_1_62 as StdField with uid="RHGHPEXDPC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AMCCNEML", cQueryName = "AMCCNEML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo per inviare in copia nascosta le mail inviate dal gestionale",;
    HelpContextID = 27147858,;
   bGlobalFont=.t.,;
    Height=21, Width=598, Left=147, Top=430, InputMask=replicate('X',254)

  add object oAM_FIRMA_1_63 as StdMemo with uid="RCWAFHFNMS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_AM_FIRMA", cQueryName = "AM_FIRMA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo da inserire nell'E-mail",;
    HelpContextID = 240320071,;
   bGlobalFont=.t.,;
    Height=79, Width=598, Left=147, Top=454


  add object oBtn_1_70 as StdButton with uid="JKCWGYZOAS",left=697, top=242, width=48,height=45,;
    CpPicture="bmp\play.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per eseguire test invio mail";
    , HelpContextID = 218968772;
    , Caption="Test invio";
  , bGlobalFont=.t.

    proc oBtn_1_70.Click()
      do GSUT_KTP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_70.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_70.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AM_INVIO<>"P")
     endwith
    endif
  endfunc

  add object oPASSWD_1_74 as StdField with uid="MMLRFBSNRA",rtseq=45,rtrep=.f.,;
    cFormVar = "w_PASSWD", cQueryName = "PASSWD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 247516426,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=147, Top=154, InputMask=replicate('X',254), passwordchar="*"

  func oPASSWD_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction="Load")
    endwith
   endif
  endfunc

  func oPASSWD_1_74.mHide()
    with this.Parent.oContained
      return (.w_AMTYPSEA<>4)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="ZPTVUAXYKQ",Visible=.t., Left=7, Top=273,;
    Alignment=1, Width=138, Height=18,;
    Caption="Modalit� autenticazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="OWPNLMPZMT",Visible=.t., Left=353, Top=273,;
    Alignment=1, Width=130, Height=18,;
    Caption="Sicurezza connessione:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="EMDLYULUHU",Visible=.t., Left=643, Top=339,;
    Alignment=1, Width=45, Height=18,;
    Caption="Porta:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (!.w_SMTP OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="OALWDAGUBE",Visible=.t., Left=7, Top=339,;
    Alignment=1, Width=138, Height=18,;
    Caption="Server posta in entrata:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (!.w_SMTP OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="BGLGZVTWMY",Visible=.t., Left=7, Top=156,;
    Alignment=1, Width=138, Height=18,;
    Caption="Server posta in uscita:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (!.w_SMTP OR .w_AM_INVIO='I')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="ICNEJVQYQY",Visible=.t., Left=643, Top=156,;
    Alignment=1, Width=45, Height=18,;
    Caption="Porta:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (!.w_SMTP)
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="AMTNCZSSHZ",Visible=.t., Left=7, Top=129,;
    Alignment=1, Width=138, Height=18,;
    Caption="Indirizzo mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="RBMCOSWASZ",Visible=.t., Left=7, Top=18,;
    Alignment=1, Width=138, Height=18,;
    Caption="Supporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="JFOZZYJCDM",Visible=.t., Left=7, Top=72,;
    Alignment=1, Width=138, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZCODNQPZII",Visible=.t., Left=7, Top=203,;
    Alignment=1, Width=138, Height=18,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S")
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="DPMAPYYZBW",Visible=.t., Left=7, Top=227,;
    Alignment=1, Width=138, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" )
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="ZIYQKJOVJR",Visible=.t., Left=7, Top=251,;
    Alignment=1, Width=138, Height=18,;
    Caption="Conferma password:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_AMAUTSRV<>"S" or .cFunction<>"Load" or empty(.w_PASSWD) or .w_PWDCL="S")
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="MZUKKUSOLK",Visible=.t., Left=53, Top=301,;
    Alignment=1, Width=92, Height=18,;
    Caption="Inizio corpo mail:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'P')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="CBOHGJARKA",Visible=.t., Left=389, Top=48,;
    Alignment=1, Width=51, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_SMTP OR .w_AMTYPSEA=4)
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="YHKRDHOHUA",Visible=.t., Left=7, Top=408,;
    Alignment=1, Width=138, Height=18,;
    Caption="CC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="VKQPELHVKS",Visible=.t., Left=7, Top=432,;
    Alignment=1, Width=138, Height=18,;
    Caption="CCN:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="SMOZJEGEPF",Visible=.t., Left=7, Top=456,;
    Alignment=1, Width=138, Height=18,;
    Caption="Firma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="KOLOBWXERI",Visible=.t., Left=7, Top=365,;
    Alignment=0, Width=53, Height=18,;
    Caption="Dati mail"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="ROBRHQICSS",Visible=.t., Left=382, Top=18,;
    Alignment=1, Width=145, Height=18,;
    Caption="Tipo ricerca account:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (.w_AM_INVIO<>'I')
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="RXQFEBOJLP",Visible=.t., Left=7, Top=156,;
    Alignment=1, Width=138, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_AMTYPSEA<>4)
    endwith
  endfunc

  add object oBox_1_67 as StdBox with uid="CYLPYZCZEW",left=4, top=381, width=743,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aam','ACC_MAIL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AMSERIAL=ACC_MAIL.AMSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_aam
Func MailTypeDesc(oParent)
    Local oTypeMail
    oTypeMail = m.oParent.GetCtrl('w_AMTYPSEA')
    oParent.w_DESCRIZIONE = m.oTypeMail.DisplayValue
    release oTypeMail
EndFunc
* --- Fine Area Manuale
