* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve2bsd                                                        *
*              Elab. cursore stampa documenti secondari                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRS_161][VRS_4]                                                *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2012-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR,w_MultiReportVarEnv
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve2bsd",oParentObject,m.w_CURSOR,m.w_MultiReportVarEnv)
return(i_retval)

define class tgsve2bsd as StdBatch
  * --- Local variables
  w_CURSOR = space(254)
  w_MultiReportVarEnv = .NULL.
  w_DECUNI = 0
  w_SIMVAL = space(5)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_LENSCF = 0
  w_TIPCON = space(1)
  w_CODICE1 = space(20)
  w_KEYOBS = ctod("  /  /  ")
  w_CODART = space(20)
  w_CODCON = space(15)
  w_LINGUA = space(3)
  w_MVDESART = space(40)
  w_KEYDESART = space(40)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione cursore report secondari
    * --- Se il report associato � diverso da GSMD_FAT,GSMA_DDT o GSMA_FAC
    *     chiedo all'utente se vuole stampare i dettagli lotti/matricole.
    this.w_MultiReportVarEnv.AddVariable("L_IMPONI", 0)     
    this.w_MultiReportVarEnv.AddVariable("L_LOGO", GETBMP(I_CODAZI))     
    * --- Passo queste variabili al report per compatibilit�
    this.w_MultiReportVarEnv.AddVariable("L_ARTESE", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DESOPE", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DATINI", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_DATFIN", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_NDIC", 0)     
    this.w_MultiReportVarEnv.AddVariable("L_ADIC", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DDIC", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_TIPOPER", "")     
    this.w_MultiReportVarEnv.AddVariable("L_IMPONI", 0)     
    * --- Stampa Etichette Matricole
    this.w_MultiReportVarEnv.AddVariable("L_STAMPQTA", "N")     
    this.w_MultiReportVarEnv.AddVariable("L_STAMPA1", .T.)     
    this.w_MultiReportVarEnv.AddVariable("L_STAMPA2", .F.)     
    this.w_MultiReportVarEnv.AddVariable("L_TIPBAR", "T")     
    this.w_MultiReportVarEnv.AddVariable("L_VAL2", "")     
    this.w_MultiReportVarEnv.AddVariable("L_FORMATO2", "@Z"+V_PU[38])     
    this.w_MultiReportVarEnv.AddVariable("L_PREMAS", "N")     
    if Isalt() and This.oParentObject.w_PRTIPCAU="N"
      this.w_MultiReportVarEnv.AddVariable("L_RifPrat", This.oParentObject.w_RifPra)     
      this.w_MultiReportVarEnv.AddVariable("L_RifOgg", This.oParentObject.w_RifOgg)     
      this.w_MultiReportVarEnv.AddVariable("L_RifVal", This.oParentObject.w_Rifval)     
      this.w_MultiReportVarEnv.AddVariable("L_RifDat", This.oParentObject.w_RifDat)     
      this.w_MultiReportVarEnv.AddVariable("L_RifSup", This.oParentObject.w_RifSup)     
      this.w_MultiReportVarEnv.AddVariable("L_RifRighe", This.oParentObject.w_RifRighe)     
      this.w_MultiReportVarEnv.AddVariable("L_RifRigu", This.oParentObject.w_Rifgu)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFMINMAX", This.oParentObject.w_RIFMINMAX)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFSCOFIN", This.oParentObject.w_RIFSCOFIN)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATDIR", This.oParentObject.w_RIFDATDIR)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATONO", This.oParentObject.w_RIFDATONO)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATTEM", This.oParentObject.w_RIFDATTEM)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATSPA", This.oParentObject.w_RIFDATSPA)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFQTA", This.oParentObject.w_RIFQTA)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFNOIMPO", This.oParentObject.w_RIFNOIMPO)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFCOEDIF", This.oParentObject.w_RIFCOEDIF)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFIMP", This.oParentObject.w_RIFIMP)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATGEN", This.oParentObject.w_RIFDATGEN)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFPARTI", This.oParentObject.w_RIFPARTI)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFRESTIT", This.oParentObject.w_RIFRESTIT)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFRAGFAS", This.oParentObject.w_RIFRAGFAS)     
    endif
    L_rwtota=1
    L_rwdocu=0
    SELECT(this.w_CURSOR)
    * --- Marco ULTRIG solo se esistono i campi MVSERIAL e ULTRIG
    if TYPE("ULTRIG")<>"U" AND TYPE("MVSERIAL")<>"U"
      * --- Marca l'ultima riga del documento
      select MVSERIAL,count(MVSERIAL) from (this.w_CURSOR) Group By Mvserial into array AR_APPOGGIO
      L_rwtota=1
      L_rwdocu=0
      select(this.w_CURSOR)
      go top
      scan
      L_rwdocu=L_rwdocu+1
      if L_rwdocu=AR_APPOGGIO(L_rwtota,2)
        replace ULTRIG with "S"
        L_rwtota=L_rwtota+1
        L_rwdocu=0
      endif
      endscan
    endif
    this.w_MultiReportVarEnv.AddVariable("L_rwtota", L_rwtota)     
    this.w_MultiReportVarEnv.AddVariable("L_rwdocu", L_rwdocu)     
    * --- Controllo se il cliente/fornitore imputato in testata del documento
    * --- ha il flag di codifica settato.Il controllo viene effettuato su un campo
    * --- che si trova nell'anagrafica Clienti/Fornitori (ANFLCODI)
    if TYPE("MVCODART")<>"U" AND TYPE("MVCODICE")<>"U" AND TYPE("MVCODCON")<>"U" AND TYPE("MVTIPCON")<>"U" AND TYPE("MVDATREG")<>"U" AND TYPE("ANCODLIN")<>"U" AND TYPE("MVDESART")<>"U" AND TYPE("MVDESSUP")<>"U" AND TYPE("MVTIPRIG")<>"U" AND TYPE("ANFLCODI")<>"U"
      SELECT (this.w_CURSOR)
      GO TOP
      SCAN FOR NVL(ANFLCODI," ")="S" AND NVL(MVTIPRIG," ")<>"D"
      this.w_CODART = NVL(MVCODART,"")
      this.w_MVCODICE = NVL(MVCODICE," ")
      this.w_CODCON = MVCODCON
      this.w_MVDESART = NVL(MVDESART, SPACE(40) ) 
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CADESART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CADESART;
          from (i_cTable) where;
              CACODICE = this.w_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_KEYDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_TIPCON = MVTIPCON
      this.w_KEYOBS = CP_TODATE(MVDATREG)
      this.w_CODICE1 = SPACE(20)
      this.w_LENSCF = 0
      * --- Select from KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CACODICE,CADTOBSO,CALENSCF,CADESART  from "+i_cTable+" KEY_ARTI ";
            +" where CACODART= "+cp_ToStrODBC(this.w_CODART)+" AND CACODCON= "+cp_ToStrODBC(this.w_CODCON)+" AND CATIPCON= "+cp_ToStrODBC(this.w_TIPCON)+" AND CACODICE<> "+cp_ToStrODBC(this.w_CODART)+" ";
            +" order by CATIPBAR DESC,CALENSCF, CACODICE";
             ,"_Curs_KEY_ARTI")
      else
        select CACODICE,CADTOBSO,CALENSCF,CADESART from (i_cTable);
         where CACODART= this.w_CODART AND CACODCON= this.w_CODCON AND CATIPCON= this.w_TIPCON AND CACODICE<> this.w_CODART ;
         order by CATIPBAR DESC,CALENSCF, CACODICE;
          into cursor _Curs_KEY_ARTI
      endif
      if used('_Curs_KEY_ARTI')
        select _Curs_KEY_ARTI
        locate for 1=1
        do while not(eof())
        if (CP_TODATE(_Curs_KEY_ARTI.CADTOBSO) > this.w_KEYOBS) OR EMPTY(NVL(CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)," "))
          this.w_CODICE1 = _Curs_KEY_ARTI.CACODICE 
          this.w_LENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
          if this.w_MVDESART=this.w_KEYDESART
            this.w_DESART1 = NVL(_Curs_KEY_ARTI.CADESART,space(40))
          else
            * --- privilegia la descrizione modificata manualmente 
            this.w_DESART1 = this.w_MVDESART
          endif
          if this.w_CODICE1= this.w_MVCODICE
            * --- Nel caso sia stato trovato come codice di ricerca valido lo stesso usato sul documento
            *     esce dal ciclo e mantiene questo.
            EXIT
          endif
        endif
          select _Curs_KEY_ARTI
          continue
        enddo
        use
      endif
      SELECT (this.w_CURSOR)
      this.w_LINGUA = NVL(ANCODLIN,"   ")
      if NOT EMPTY(NVL(this.w_CODICE1,""))
         
 DECLARE ARRRIS (2) 
 Tradlin(this.w_CODICE1,this.w_LINGUA,"TRADARTI",@ARRRIS,"B") 
 this.w_DESART1=IIF(NOT EMPTY(ARRRIS(1)), ARRRIS(1), this.w_DESART1) 
 this.w_DESSUP1=ARRRIS(2)
        SELECT (this.w_CURSOR)
        if NOT EMPTY(NVL(this.w_DESART1," "))
          REPLACE MVDESART WITH this.w_DESART1
        endif
        if NOT EMPTY(NVL(this.w_DESSUP1," "))
          REPLACE MVDESSUP WITH this.w_DESSUP1
        endif
        * --- Se Attivo Flag Codifica Cliente/Fornitore Esclusiva in Azienda
        *     Prendo solo la prima parte del codice di ricerca (elimino il suffisso da destra)
        if g_FLCESC = "S" And this.w_LENSCF <> 0
          this.w_CODICE1 = Left(Alltrim(this.w_CODICE1),Len(Alltrim(this.w_CODICE1))-this.w_LENSCF)
        endif
        REPLACE mvcodice WITH this.w_CODICE1
      endif
      ENDSCAN
    endif
  endproc


  proc Init(oParentObject,w_CURSOR,w_MultiReportVarEnv)
    this.w_CURSOR=w_CURSOR
    this.w_MultiReportVarEnv=w_MultiReportVarEnv
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR,w_MultiReportVarEnv"
endproc
