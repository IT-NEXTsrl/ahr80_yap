* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_msa                                                        *
*              Dati anagrafici storicizzati                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-10                                                      *
* Last revis.: 2014-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_msa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_msa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_msa")
  return

* --- Class definition
define class tgsar_msa as StdPCForm
  Width  = 665
  Height = 520
  Top    = 10
  Left   = 10
  cComment = "Dati anagrafici storicizzati"
  cPrg = "gsar_msa"
  HelpContextID=97089385
  add object cnt as tcgsar_msa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_msa as PCContext
  w_SSTIPCON = space(1)
  w_CPROWORD = 0
  w_SSDATINI = space(8)
  w_SSDATFIN = space(8)
  w_SSDESCRI = space(60)
  w_SSDESCR2 = space(60)
  w_SSINDIRI = space(35)
  w_SSINDIR2 = space(35)
  w_SSCODFIS = space(16)
  w_SSPARIVA = space(12)
  w_SS___CAP = space(9)
  w_SSLOCALI = space(30)
  w_SSPROVIN = space(2)
  w_SSNAZION = space(3)
  w_SSINDWEB = space(254)
  w_SS_EMAIL = space(254)
  w_SSTELEFO = space(18)
  w_SSTELFAX = space(18)
  w_SSNUMCEL = space(18)
  w_SSCODICE = space(15)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_SSDESCR3 = space(60)
  w_SSDESCR4 = space(60)
  w_SSINDIR3 = space(35)
  w_SSINDIR4 = space(35)
  w_SSLOCAL2 = space(30)
  w_SS_EMPEC = space(254)
  proc Save(i_oFrom)
    this.w_SSTIPCON = i_oFrom.w_SSTIPCON
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SSDATINI = i_oFrom.w_SSDATINI
    this.w_SSDATFIN = i_oFrom.w_SSDATFIN
    this.w_SSDESCRI = i_oFrom.w_SSDESCRI
    this.w_SSDESCR2 = i_oFrom.w_SSDESCR2
    this.w_SSINDIRI = i_oFrom.w_SSINDIRI
    this.w_SSINDIR2 = i_oFrom.w_SSINDIR2
    this.w_SSCODFIS = i_oFrom.w_SSCODFIS
    this.w_SSPARIVA = i_oFrom.w_SSPARIVA
    this.w_SS___CAP = i_oFrom.w_SS___CAP
    this.w_SSLOCALI = i_oFrom.w_SSLOCALI
    this.w_SSPROVIN = i_oFrom.w_SSPROVIN
    this.w_SSNAZION = i_oFrom.w_SSNAZION
    this.w_SSINDWEB = i_oFrom.w_SSINDWEB
    this.w_SS_EMAIL = i_oFrom.w_SS_EMAIL
    this.w_SSTELEFO = i_oFrom.w_SSTELEFO
    this.w_SSTELFAX = i_oFrom.w_SSTELFAX
    this.w_SSNUMCEL = i_oFrom.w_SSNUMCEL
    this.w_SSCODICE = i_oFrom.w_SSCODICE
    this.w_DESNAZ = i_oFrom.w_DESNAZ
    this.w_CODISO = i_oFrom.w_CODISO
    this.w_SSDESCR3 = i_oFrom.w_SSDESCR3
    this.w_SSDESCR4 = i_oFrom.w_SSDESCR4
    this.w_SSINDIR3 = i_oFrom.w_SSINDIR3
    this.w_SSINDIR4 = i_oFrom.w_SSINDIR4
    this.w_SSLOCAL2 = i_oFrom.w_SSLOCAL2
    this.w_SS_EMPEC = i_oFrom.w_SS_EMPEC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_SSTIPCON = this.w_SSTIPCON
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SSDATINI = this.w_SSDATINI
    i_oTo.w_SSDATFIN = this.w_SSDATFIN
    i_oTo.w_SSDESCRI = this.w_SSDESCRI
    i_oTo.w_SSDESCR2 = this.w_SSDESCR2
    i_oTo.w_SSINDIRI = this.w_SSINDIRI
    i_oTo.w_SSINDIR2 = this.w_SSINDIR2
    i_oTo.w_SSCODFIS = this.w_SSCODFIS
    i_oTo.w_SSPARIVA = this.w_SSPARIVA
    i_oTo.w_SS___CAP = this.w_SS___CAP
    i_oTo.w_SSLOCALI = this.w_SSLOCALI
    i_oTo.w_SSPROVIN = this.w_SSPROVIN
    i_oTo.w_SSNAZION = this.w_SSNAZION
    i_oTo.w_SSINDWEB = this.w_SSINDWEB
    i_oTo.w_SS_EMAIL = this.w_SS_EMAIL
    i_oTo.w_SSTELEFO = this.w_SSTELEFO
    i_oTo.w_SSTELFAX = this.w_SSTELFAX
    i_oTo.w_SSNUMCEL = this.w_SSNUMCEL
    i_oTo.w_SSCODICE = this.w_SSCODICE
    i_oTo.w_DESNAZ = this.w_DESNAZ
    i_oTo.w_CODISO = this.w_CODISO
    i_oTo.w_SSDESCR3 = this.w_SSDESCR3
    i_oTo.w_SSDESCR4 = this.w_SSDESCR4
    i_oTo.w_SSINDIR3 = this.w_SSINDIR3
    i_oTo.w_SSINDIR4 = this.w_SSINDIR4
    i_oTo.w_SSLOCAL2 = this.w_SSLOCAL2
    i_oTo.w_SS_EMPEC = this.w_SS_EMPEC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_msa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 665
  Height = 520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-27"
  HelpContextID=97089385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  SED_STOR_IDX = 0
  NAZIONI_IDX = 0
  cFile = "SED_STOR"
  cKeySelect = "SSTIPCON,SSCODICE"
  cKeyWhere  = "SSTIPCON=this.w_SSTIPCON and SSCODICE=this.w_SSCODICE"
  cKeyDetail  = "SSTIPCON=this.w_SSTIPCON and SSDATINI=this.w_SSDATINI and SSDATFIN=this.w_SSDATFIN and SSCODICE=this.w_SSCODICE"
  cKeyWhereODBC = '"SSTIPCON="+cp_ToStrODBC(this.w_SSTIPCON)';
      +'+" and SSCODICE="+cp_ToStrODBC(this.w_SSCODICE)';

  cKeyDetailWhereODBC = '"SSTIPCON="+cp_ToStrODBC(this.w_SSTIPCON)';
      +'+" and SSDATINI="+cp_ToStrODBC(this.w_SSDATINI,"D")';
      +'+" and SSDATFIN="+cp_ToStrODBC(this.w_SSDATFIN,"D")';
      +'+" and SSCODICE="+cp_ToStrODBC(this.w_SSCODICE)';

  cKeyWhereODBCqualified = '"SED_STOR.SSTIPCON="+cp_ToStrODBC(this.w_SSTIPCON)';
      +'+" and SED_STOR.SSCODICE="+cp_ToStrODBC(this.w_SSCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'SED_STOR.CPROWORD '
  cPrg = "gsar_msa"
  cComment = "Dati anagrafici storicizzati"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SSTIPCON = space(1)
  w_CPROWORD = 0
  w_SSDATINI = ctod('  /  /  ')
  w_SSDATFIN = ctod('  /  /  ')
  w_SSDESCRI = space(60)
  w_SSDESCR2 = space(60)
  w_SSINDIRI = space(35)
  w_SSINDIR2 = space(35)
  w_SSCODFIS = space(16)
  w_SSPARIVA = space(12)
  w_SS___CAP = space(9)
  w_SSLOCALI = space(30)
  w_SSPROVIN = space(2)
  w_SSNAZION = space(3)
  w_SSINDWEB = space(254)
  w_SS_EMAIL = space(254)
  w_SSTELEFO = space(18)
  w_SSTELFAX = space(18)
  w_SSNUMCEL = space(18)
  w_SSCODICE = space(15)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_SSDESCR3 = space(60)
  w_SSDESCR4 = space(60)
  w_SSINDIR3 = space(35)
  w_SSINDIR4 = space(35)
  w_SSLOCAL2 = space(30)
  w_SS_EMPEC = space(254)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_msaPag1","gsar_msa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='NAZIONI'
    this.cWorkTables[2]='SED_STOR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SED_STOR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SED_STOR_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_msa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_13_joined
    link_2_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from SED_STOR where SSTIPCON=KeySet.SSTIPCON
    *                            and SSDATINI=KeySet.SSDATINI
    *                            and SSDATFIN=KeySet.SSDATFIN
    *                            and SSCODICE=KeySet.SSCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.SED_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2],this.bLoadRecFilter,this.SED_STOR_IDX,"gsar_msa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SED_STOR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SED_STOR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SED_STOR '
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SSTIPCON',this.w_SSTIPCON  ,'SSCODICE',this.w_SSCODICE  )
      select * from (i_cTable) SED_STOR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SSTIPCON = NVL(SSTIPCON,space(1))
        .w_SSCODICE = NVL(SSCODICE,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'SED_STOR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESNAZ = space(35)
          .w_CODISO = space(3)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SSDATINI = NVL(cp_ToDate(SSDATINI),ctod("  /  /  "))
          .w_SSDATFIN = NVL(cp_ToDate(SSDATFIN),ctod("  /  /  "))
          .w_SSDESCRI = NVL(SSDESCRI,space(60))
          .w_SSDESCR2 = NVL(SSDESCR2,space(60))
          .w_SSINDIRI = NVL(SSINDIRI,space(35))
          .w_SSINDIR2 = NVL(SSINDIR2,space(35))
          .w_SSCODFIS = NVL(SSCODFIS,space(16))
          .w_SSPARIVA = NVL(SSPARIVA,space(12))
          .w_SS___CAP = NVL(SS___CAP,space(9))
          .w_SSLOCALI = NVL(SSLOCALI,space(30))
          .w_SSPROVIN = NVL(SSPROVIN,space(2))
          .w_SSNAZION = NVL(SSNAZION,space(3))
          if link_2_13_joined
            this.w_SSNAZION = NVL(NACODNAZ213,NVL(this.w_SSNAZION,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ213,space(35))
            this.w_CODISO = NVL(NACODISO213,space(3))
          else
          .link_2_13('Load')
          endif
          .w_SSINDWEB = NVL(SSINDWEB,space(254))
          .w_SS_EMAIL = NVL(SS_EMAIL,space(254))
          .w_SSTELEFO = NVL(SSTELEFO,space(18))
          .w_SSTELFAX = NVL(SSTELFAX,space(18))
          .w_SSNUMCEL = NVL(SSNUMCEL,space(18))
          .w_SSDESCR3 = NVL(SSDESCR3,space(60))
          .w_SSDESCR4 = NVL(SSDESCR4,space(60))
          .w_SSINDIR3 = NVL(SSINDIR3,space(35))
          .w_SSINDIR4 = NVL(SSINDIR4,space(35))
          .w_SSLOCAL2 = NVL(SSLOCAL2,space(30))
          .w_SS_EMPEC = NVL(SS_EMPEC,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace SSDATINI with .w_SSDATINI
          replace SSDATFIN with .w_SSDATFIN
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_SSTIPCON=space(1)
      .w_CPROWORD=10
      .w_SSDATINI=ctod("  /  /  ")
      .w_SSDATFIN=ctod("  /  /  ")
      .w_SSDESCRI=space(60)
      .w_SSDESCR2=space(60)
      .w_SSINDIRI=space(35)
      .w_SSINDIR2=space(35)
      .w_SSCODFIS=space(16)
      .w_SSPARIVA=space(12)
      .w_SS___CAP=space(9)
      .w_SSLOCALI=space(30)
      .w_SSPROVIN=space(2)
      .w_SSNAZION=space(3)
      .w_SSINDWEB=space(254)
      .w_SS_EMAIL=space(254)
      .w_SSTELEFO=space(18)
      .w_SSTELFAX=space(18)
      .w_SSNUMCEL=space(18)
      .w_SSCODICE=space(15)
      .w_DESNAZ=space(35)
      .w_CODISO=space(3)
      .w_SSDESCR3=space(60)
      .w_SSDESCR4=space(60)
      .w_SSINDIR3=space(35)
      .w_SSINDIR4=space(35)
      .w_SSLOCAL2=space(30)
      .w_SS_EMPEC=space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_SSDATFIN = i_DATSYS
        .DoRTCalc(5,14,.f.)
        if not(empty(.w_SSNAZION))
         .link_2_13('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'SED_STOR')
    this.DoRTCalc(15,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oSSDESCR2_2_5.enabled = i_bVal
      .Page1.oPag.oSSINDIRI_2_6.enabled = i_bVal
      .Page1.oPag.oSSINDIR2_2_7.enabled = i_bVal
      .Page1.oPag.oSS___CAP_2_10.enabled = i_bVal
      .Page1.oPag.oSSLOCALI_2_11.enabled = i_bVal
      .Page1.oPag.oSSPROVIN_2_12.enabled = i_bVal
      .Page1.oPag.oSSNAZION_2_13.enabled = i_bVal
      .Page1.oPag.oSSINDWEB_2_14.enabled = i_bVal
      .Page1.oPag.oSS_EMAIL_2_15.enabled = i_bVal
      .Page1.oPag.oSSTELEFO_2_16.enabled = i_bVal
      .Page1.oPag.oSSTELFAX_2_17.enabled = i_bVal
      .Page1.oPag.oSSNUMCEL_2_18.enabled = i_bVal
      .Page1.oPag.oSSDESCR3_2_21.enabled = i_bVal
      .Page1.oPag.oSSDESCR4_2_22.enabled = i_bVal
      .Page1.oPag.oSSINDIR3_2_23.enabled = i_bVal
      .Page1.oPag.oSSINDIR4_2_24.enabled = i_bVal
      .Page1.oPag.oSSLOCAL2_2_25.enabled = i_bVal
      .Page1.oPag.oSS_EMPEC_2_26.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'SED_STOR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SED_STOR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SSTIPCON,"SSTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SSCODICE,"SSCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_SSDATINI D(8);
      ,t_SSDATFIN D(8);
      ,t_SSDESCRI C(60);
      ,t_SSDESCR2 C(60);
      ,t_SSINDIRI C(35);
      ,t_SSINDIR2 C(35);
      ,t_SSCODFIS C(16);
      ,t_SSPARIVA C(12);
      ,t_SS___CAP C(9);
      ,t_SSLOCALI C(30);
      ,t_SSPROVIN C(2);
      ,t_SSNAZION C(3);
      ,t_SSINDWEB C(254);
      ,t_SS_EMAIL C(254);
      ,t_SSTELEFO C(18);
      ,t_SSTELFAX C(18);
      ,t_SSNUMCEL C(18);
      ,t_DESNAZ C(35);
      ,t_CODISO C(3);
      ,t_SSDESCR3 C(60);
      ,t_SSDESCR4 C(60);
      ,t_SSINDIR3 C(35);
      ,t_SSINDIR4 C(35);
      ,t_SSLOCAL2 C(30);
      ,t_SS_EMPEC C(254);
      ,SSDATINI D(8);
      ,SSDATFIN D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_msabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATINI_2_2.controlsource=this.cTrsName+'.t_SSDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATFIN_2_3.controlsource=this.cTrsName+'.t_SSDATFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSSDESCRI_2_4.controlsource=this.cTrsName+'.t_SSDESCRI'
    this.oPgFRm.Page1.oPag.oSSDESCR2_2_5.controlsource=this.cTrsName+'.t_SSDESCR2'
    this.oPgFRm.Page1.oPag.oSSINDIRI_2_6.controlsource=this.cTrsName+'.t_SSINDIRI'
    this.oPgFRm.Page1.oPag.oSSINDIR2_2_7.controlsource=this.cTrsName+'.t_SSINDIR2'
    this.oPgFRm.Page1.oPag.oSSCODFIS_2_8.controlsource=this.cTrsName+'.t_SSCODFIS'
    this.oPgFRm.Page1.oPag.oSSPARIVA_2_9.controlsource=this.cTrsName+'.t_SSPARIVA'
    this.oPgFRm.Page1.oPag.oSS___CAP_2_10.controlsource=this.cTrsName+'.t_SS___CAP'
    this.oPgFRm.Page1.oPag.oSSLOCALI_2_11.controlsource=this.cTrsName+'.t_SSLOCALI'
    this.oPgFRm.Page1.oPag.oSSPROVIN_2_12.controlsource=this.cTrsName+'.t_SSPROVIN'
    this.oPgFRm.Page1.oPag.oSSNAZION_2_13.controlsource=this.cTrsName+'.t_SSNAZION'
    this.oPgFRm.Page1.oPag.oSSINDWEB_2_14.controlsource=this.cTrsName+'.t_SSINDWEB'
    this.oPgFRm.Page1.oPag.oSS_EMAIL_2_15.controlsource=this.cTrsName+'.t_SS_EMAIL'
    this.oPgFRm.Page1.oPag.oSSTELEFO_2_16.controlsource=this.cTrsName+'.t_SSTELEFO'
    this.oPgFRm.Page1.oPag.oSSTELFAX_2_17.controlsource=this.cTrsName+'.t_SSTELFAX'
    this.oPgFRm.Page1.oPag.oSSNUMCEL_2_18.controlsource=this.cTrsName+'.t_SSNUMCEL'
    this.oPgFRm.Page1.oPag.oDESNAZ_2_19.controlsource=this.cTrsName+'.t_DESNAZ'
    this.oPgFRm.Page1.oPag.oCODISO_2_20.controlsource=this.cTrsName+'.t_CODISO'
    this.oPgFRm.Page1.oPag.oSSDESCR3_2_21.controlsource=this.cTrsName+'.t_SSDESCR3'
    this.oPgFRm.Page1.oPag.oSSDESCR4_2_22.controlsource=this.cTrsName+'.t_SSDESCR4'
    this.oPgFRm.Page1.oPag.oSSINDIR3_2_23.controlsource=this.cTrsName+'.t_SSINDIR3'
    this.oPgFRm.Page1.oPag.oSSINDIR4_2_24.controlsource=this.cTrsName+'.t_SSINDIR4'
    this.oPgFRm.Page1.oPag.oSSLOCAL2_2_25.controlsource=this.cTrsName+'.t_SSLOCAL2'
    this.oPgFRm.Page1.oPag.oSS_EMPEC_2_26.controlsource=this.cTrsName+'.t_SS_EMPEC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(51)
    this.AddVLine(123)
    this.AddVLine(197)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SED_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SED_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2])
      *
      * insert into SED_STOR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SED_STOR')
        i_extval=cp_InsertValODBCExtFlds(this,'SED_STOR')
        i_cFldBody=" "+;
                  "(SSTIPCON,CPROWORD,SSDATINI,SSDATFIN,SSDESCRI"+;
                  ",SSDESCR2,SSINDIRI,SSINDIR2,SSCODFIS,SSPARIVA"+;
                  ",SS___CAP,SSLOCALI,SSPROVIN,SSNAZION,SSINDWEB"+;
                  ",SS_EMAIL,SSTELEFO,SSTELFAX,SSNUMCEL,SSCODICE"+;
                  ",SSDESCR3,SSDESCR4,SSINDIR3,SSINDIR4,SSLOCAL2"+;
                  ",SS_EMPEC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SSTIPCON)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_SSDATINI)+","+cp_ToStrODBC(this.w_SSDATFIN)+","+cp_ToStrODBC(this.w_SSDESCRI)+;
             ","+cp_ToStrODBC(this.w_SSDESCR2)+","+cp_ToStrODBC(this.w_SSINDIRI)+","+cp_ToStrODBC(this.w_SSINDIR2)+","+cp_ToStrODBC(this.w_SSCODFIS)+","+cp_ToStrODBC(this.w_SSPARIVA)+;
             ","+cp_ToStrODBC(this.w_SS___CAP)+","+cp_ToStrODBC(this.w_SSLOCALI)+","+cp_ToStrODBC(this.w_SSPROVIN)+","+cp_ToStrODBCNull(this.w_SSNAZION)+","+cp_ToStrODBC(this.w_SSINDWEB)+;
             ","+cp_ToStrODBC(this.w_SS_EMAIL)+","+cp_ToStrODBC(this.w_SSTELEFO)+","+cp_ToStrODBC(this.w_SSTELFAX)+","+cp_ToStrODBC(this.w_SSNUMCEL)+","+cp_ToStrODBC(this.w_SSCODICE)+;
             ","+cp_ToStrODBC(this.w_SSDESCR3)+","+cp_ToStrODBC(this.w_SSDESCR4)+","+cp_ToStrODBC(this.w_SSINDIR3)+","+cp_ToStrODBC(this.w_SSINDIR4)+","+cp_ToStrODBC(this.w_SSLOCAL2)+;
             ","+cp_ToStrODBC(this.w_SS_EMPEC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SED_STOR')
        i_extval=cp_InsertValVFPExtFlds(this,'SED_STOR')
        cp_CheckDeletedKey(i_cTable,0,'SSTIPCON',this.w_SSTIPCON,'SSDATINI',this.w_SSDATINI,'SSDATFIN',this.w_SSDATFIN,'SSCODICE',this.w_SSCODICE)
        INSERT INTO (i_cTable) (;
                   SSTIPCON;
                  ,CPROWORD;
                  ,SSDATINI;
                  ,SSDATFIN;
                  ,SSDESCRI;
                  ,SSDESCR2;
                  ,SSINDIRI;
                  ,SSINDIR2;
                  ,SSCODFIS;
                  ,SSPARIVA;
                  ,SS___CAP;
                  ,SSLOCALI;
                  ,SSPROVIN;
                  ,SSNAZION;
                  ,SSINDWEB;
                  ,SS_EMAIL;
                  ,SSTELEFO;
                  ,SSTELFAX;
                  ,SSNUMCEL;
                  ,SSCODICE;
                  ,SSDESCR3;
                  ,SSDESCR4;
                  ,SSINDIR3;
                  ,SSINDIR4;
                  ,SSLOCAL2;
                  ,SS_EMPEC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SSTIPCON;
                  ,this.w_CPROWORD;
                  ,this.w_SSDATINI;
                  ,this.w_SSDATFIN;
                  ,this.w_SSDESCRI;
                  ,this.w_SSDESCR2;
                  ,this.w_SSINDIRI;
                  ,this.w_SSINDIR2;
                  ,this.w_SSCODFIS;
                  ,this.w_SSPARIVA;
                  ,this.w_SS___CAP;
                  ,this.w_SSLOCALI;
                  ,this.w_SSPROVIN;
                  ,this.w_SSNAZION;
                  ,this.w_SSINDWEB;
                  ,this.w_SS_EMAIL;
                  ,this.w_SSTELEFO;
                  ,this.w_SSTELFAX;
                  ,this.w_SSNUMCEL;
                  ,this.w_SSCODICE;
                  ,this.w_SSDESCR3;
                  ,this.w_SSDESCR4;
                  ,this.w_SSINDIR3;
                  ,this.w_SSINDIR4;
                  ,this.w_SSLOCAL2;
                  ,this.w_SS_EMPEC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.SED_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_SSDATINI)) and not(Empty(t_SSDATFIN))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'SED_STOR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and SSDATINI="+cp_ToStrODBC(&i_TN.->SSDATINI)+;
                 " and SSDATFIN="+cp_ToStrODBC(&i_TN.->SSDATFIN)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'SED_STOR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and SSDATINI=&i_TN.->SSDATINI;
                      and SSDATFIN=&i_TN.->SSDATFIN;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_SSDATINI)) and not(Empty(t_SSDATFIN))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and SSDATINI="+cp_ToStrODBC(&i_TN.->SSDATINI)+;
                            " and SSDATFIN="+cp_ToStrODBC(&i_TN.->SSDATFIN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and SSDATINI=&i_TN.->SSDATINI;
                            and SSDATFIN=&i_TN.->SSDATFIN;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace SSDATINI with this.w_SSDATINI
              replace SSDATFIN with this.w_SSDATFIN
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update SED_STOR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'SED_STOR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SSDESCRI="+cp_ToStrODBC(this.w_SSDESCRI)+;
                     ",SSDESCR2="+cp_ToStrODBC(this.w_SSDESCR2)+;
                     ",SSINDIRI="+cp_ToStrODBC(this.w_SSINDIRI)+;
                     ",SSINDIR2="+cp_ToStrODBC(this.w_SSINDIR2)+;
                     ",SSCODFIS="+cp_ToStrODBC(this.w_SSCODFIS)+;
                     ",SSPARIVA="+cp_ToStrODBC(this.w_SSPARIVA)+;
                     ",SS___CAP="+cp_ToStrODBC(this.w_SS___CAP)+;
                     ",SSLOCALI="+cp_ToStrODBC(this.w_SSLOCALI)+;
                     ",SSPROVIN="+cp_ToStrODBC(this.w_SSPROVIN)+;
                     ",SSNAZION="+cp_ToStrODBCNull(this.w_SSNAZION)+;
                     ",SSINDWEB="+cp_ToStrODBC(this.w_SSINDWEB)+;
                     ",SS_EMAIL="+cp_ToStrODBC(this.w_SS_EMAIL)+;
                     ",SSTELEFO="+cp_ToStrODBC(this.w_SSTELEFO)+;
                     ",SSTELFAX="+cp_ToStrODBC(this.w_SSTELFAX)+;
                     ",SSNUMCEL="+cp_ToStrODBC(this.w_SSNUMCEL)+;
                     ",SSDESCR3="+cp_ToStrODBC(this.w_SSDESCR3)+;
                     ",SSDESCR4="+cp_ToStrODBC(this.w_SSDESCR4)+;
                     ",SSINDIR3="+cp_ToStrODBC(this.w_SSINDIR3)+;
                     ",SSINDIR4="+cp_ToStrODBC(this.w_SSINDIR4)+;
                     ",SSLOCAL2="+cp_ToStrODBC(this.w_SSLOCAL2)+;
                     ",SS_EMPEC="+cp_ToStrODBC(this.w_SS_EMPEC)+;
                     ",SSDATINI="+cp_ToStrODBC(this.w_SSDATINI)+;
                     ",SSDATFIN="+cp_ToStrODBC(this.w_SSDATFIN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and SSDATINI="+cp_ToStrODBC(SSDATINI)+;
                             " and SSDATFIN="+cp_ToStrODBC(SSDATFIN)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'SED_STOR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SSDESCRI=this.w_SSDESCRI;
                     ,SSDESCR2=this.w_SSDESCR2;
                     ,SSINDIRI=this.w_SSINDIRI;
                     ,SSINDIR2=this.w_SSINDIR2;
                     ,SSCODFIS=this.w_SSCODFIS;
                     ,SSPARIVA=this.w_SSPARIVA;
                     ,SS___CAP=this.w_SS___CAP;
                     ,SSLOCALI=this.w_SSLOCALI;
                     ,SSPROVIN=this.w_SSPROVIN;
                     ,SSNAZION=this.w_SSNAZION;
                     ,SSINDWEB=this.w_SSINDWEB;
                     ,SS_EMAIL=this.w_SS_EMAIL;
                     ,SSTELEFO=this.w_SSTELEFO;
                     ,SSTELFAX=this.w_SSTELFAX;
                     ,SSNUMCEL=this.w_SSNUMCEL;
                     ,SSDESCR3=this.w_SSDESCR3;
                     ,SSDESCR4=this.w_SSDESCR4;
                     ,SSINDIR3=this.w_SSINDIR3;
                     ,SSINDIR4=this.w_SSINDIR4;
                     ,SSLOCAL2=this.w_SSLOCAL2;
                     ,SS_EMPEC=this.w_SS_EMPEC;
                     ,SSDATINI=this.w_SSDATINI;
                     ,SSDATFIN=this.w_SSDATFIN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and SSDATINI=&i_TN.->SSDATINI;
                                      and SSDATFIN=&i_TN.->SSDATFIN;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SED_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_SSDATINI)) and not(Empty(t_SSDATFIN))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete SED_STOR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and SSDATINI="+cp_ToStrODBC(&i_TN.->SSDATINI)+;
                            " and SSDATFIN="+cp_ToStrODBC(&i_TN.->SSDATFIN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and SSDATINI=&i_TN.->SSDATINI;
                              and SSDATFIN=&i_TN.->SSDATFIN;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_SSDATINI)) and not(Empty(t_SSDATFIN))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SED_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SED_STOR_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SSNAZION
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SSNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_SSNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_SSNAZION))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SSNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SSNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oSSNAZION_2_13'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SSNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_SSNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_SSNAZION)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SSNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_SSNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
      this.w_CODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SSNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.NACODNAZ as NACODNAZ213"+ ",link_2_13.NADESNAZ as NADESNAZ213"+ ",link_2_13.NACODISO as NACODISO213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on SED_STOR.SSNAZION=link_2_13.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and SED_STOR.SSNAZION=link_2_13.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSSDESCR2_2_5.value==this.w_SSDESCR2)
      this.oPgFrm.Page1.oPag.oSSDESCR2_2_5.value=this.w_SSDESCR2
      replace t_SSDESCR2 with this.oPgFrm.Page1.oPag.oSSDESCR2_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSINDIRI_2_6.value==this.w_SSINDIRI)
      this.oPgFrm.Page1.oPag.oSSINDIRI_2_6.value=this.w_SSINDIRI
      replace t_SSINDIRI with this.oPgFrm.Page1.oPag.oSSINDIRI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSINDIR2_2_7.value==this.w_SSINDIR2)
      this.oPgFrm.Page1.oPag.oSSINDIR2_2_7.value=this.w_SSINDIR2
      replace t_SSINDIR2 with this.oPgFrm.Page1.oPag.oSSINDIR2_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSCODFIS_2_8.value==this.w_SSCODFIS)
      this.oPgFrm.Page1.oPag.oSSCODFIS_2_8.value=this.w_SSCODFIS
      replace t_SSCODFIS with this.oPgFrm.Page1.oPag.oSSCODFIS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSPARIVA_2_9.value==this.w_SSPARIVA)
      this.oPgFrm.Page1.oPag.oSSPARIVA_2_9.value=this.w_SSPARIVA
      replace t_SSPARIVA with this.oPgFrm.Page1.oPag.oSSPARIVA_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSS___CAP_2_10.value==this.w_SS___CAP)
      this.oPgFrm.Page1.oPag.oSS___CAP_2_10.value=this.w_SS___CAP
      replace t_SS___CAP with this.oPgFrm.Page1.oPag.oSS___CAP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSLOCALI_2_11.value==this.w_SSLOCALI)
      this.oPgFrm.Page1.oPag.oSSLOCALI_2_11.value=this.w_SSLOCALI
      replace t_SSLOCALI with this.oPgFrm.Page1.oPag.oSSLOCALI_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSPROVIN_2_12.value==this.w_SSPROVIN)
      this.oPgFrm.Page1.oPag.oSSPROVIN_2_12.value=this.w_SSPROVIN
      replace t_SSPROVIN with this.oPgFrm.Page1.oPag.oSSPROVIN_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSNAZION_2_13.value==this.w_SSNAZION)
      this.oPgFrm.Page1.oPag.oSSNAZION_2_13.value=this.w_SSNAZION
      replace t_SSNAZION with this.oPgFrm.Page1.oPag.oSSNAZION_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSINDWEB_2_14.value==this.w_SSINDWEB)
      this.oPgFrm.Page1.oPag.oSSINDWEB_2_14.value=this.w_SSINDWEB
      replace t_SSINDWEB with this.oPgFrm.Page1.oPag.oSSINDWEB_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSS_EMAIL_2_15.value==this.w_SS_EMAIL)
      this.oPgFrm.Page1.oPag.oSS_EMAIL_2_15.value=this.w_SS_EMAIL
      replace t_SS_EMAIL with this.oPgFrm.Page1.oPag.oSS_EMAIL_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSTELEFO_2_16.value==this.w_SSTELEFO)
      this.oPgFrm.Page1.oPag.oSSTELEFO_2_16.value=this.w_SSTELEFO
      replace t_SSTELEFO with this.oPgFrm.Page1.oPag.oSSTELEFO_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSTELFAX_2_17.value==this.w_SSTELFAX)
      this.oPgFrm.Page1.oPag.oSSTELFAX_2_17.value=this.w_SSTELFAX
      replace t_SSTELFAX with this.oPgFrm.Page1.oPag.oSSTELFAX_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSNUMCEL_2_18.value==this.w_SSNUMCEL)
      this.oPgFrm.Page1.oPag.oSSNUMCEL_2_18.value=this.w_SSNUMCEL
      replace t_SSNUMCEL with this.oPgFrm.Page1.oPag.oSSNUMCEL_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_2_19.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_2_19.value=this.w_DESNAZ
      replace t_DESNAZ with this.oPgFrm.Page1.oPag.oDESNAZ_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODISO_2_20.value==this.w_CODISO)
      this.oPgFrm.Page1.oPag.oCODISO_2_20.value=this.w_CODISO
      replace t_CODISO with this.oPgFrm.Page1.oPag.oCODISO_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSDESCR3_2_21.value==this.w_SSDESCR3)
      this.oPgFrm.Page1.oPag.oSSDESCR3_2_21.value=this.w_SSDESCR3
      replace t_SSDESCR3 with this.oPgFrm.Page1.oPag.oSSDESCR3_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSDESCR4_2_22.value==this.w_SSDESCR4)
      this.oPgFrm.Page1.oPag.oSSDESCR4_2_22.value=this.w_SSDESCR4
      replace t_SSDESCR4 with this.oPgFrm.Page1.oPag.oSSDESCR4_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSINDIR3_2_23.value==this.w_SSINDIR3)
      this.oPgFrm.Page1.oPag.oSSINDIR3_2_23.value=this.w_SSINDIR3
      replace t_SSINDIR3 with this.oPgFrm.Page1.oPag.oSSINDIR3_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSINDIR4_2_24.value==this.w_SSINDIR4)
      this.oPgFrm.Page1.oPag.oSSINDIR4_2_24.value=this.w_SSINDIR4
      replace t_SSINDIR4 with this.oPgFrm.Page1.oPag.oSSINDIR4_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSSLOCAL2_2_25.value==this.w_SSLOCAL2)
      this.oPgFrm.Page1.oPag.oSSLOCAL2_2_25.value=this.w_SSLOCAL2
      replace t_SSLOCAL2 with this.oPgFrm.Page1.oPag.oSSLOCAL2_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSS_EMPEC_2_26.value==this.w_SS_EMPEC)
      this.oPgFrm.Page1.oPag.oSS_EMPEC_2_26.value=this.w_SS_EMPEC
      replace t_SS_EMPEC with this.oPgFrm.Page1.oPag.oSS_EMPEC_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATINI_2_2.value==this.w_SSDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATINI_2_2.value=this.w_SSDATINI
      replace t_SSDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATINI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATFIN_2_3.value==this.w_SSDATFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATFIN_2_3.value=this.w_SSDATFIN
      replace t_SSDATFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATFIN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDESCRI_2_4.value==this.w_SSDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDESCRI_2_4.value=this.w_SSDESCRI
      replace t_SSDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDESCRI_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'SED_STOR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_msa
      if i_bres
         i_bres=GSAR_BSA(this)
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_SSDATFIN) OR .w_SSDATFIN>=.w_SSDATINI) and (not(Empty(.w_SSDATINI)) and not(Empty(.w_SSDATFIN)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSSDATFIN_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di fine validit� non valida")
      endcase
      if not(Empty(.w_SSDATINI)) and not(Empty(.w_SSDATFIN))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_SSDATINI)) and not(Empty(t_SSDATFIN)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_SSDATINI=ctod("  /  /  ")
      .w_SSDATFIN=ctod("  /  /  ")
      .w_SSDESCRI=space(60)
      .w_SSDESCR2=space(60)
      .w_SSINDIRI=space(35)
      .w_SSINDIR2=space(35)
      .w_SSCODFIS=space(16)
      .w_SSPARIVA=space(12)
      .w_SS___CAP=space(9)
      .w_SSLOCALI=space(30)
      .w_SSPROVIN=space(2)
      .w_SSNAZION=space(3)
      .w_SSINDWEB=space(254)
      .w_SS_EMAIL=space(254)
      .w_SSTELEFO=space(18)
      .w_SSTELFAX=space(18)
      .w_SSNUMCEL=space(18)
      .w_DESNAZ=space(35)
      .w_CODISO=space(3)
      .w_SSDESCR3=space(60)
      .w_SSDESCR4=space(60)
      .w_SSINDIR3=space(35)
      .w_SSINDIR4=space(35)
      .w_SSLOCAL2=space(30)
      .w_SS_EMPEC=space(254)
      .DoRTCalc(1,3,.f.)
        .w_SSDATFIN = i_DATSYS
      .DoRTCalc(5,14,.f.)
      if not(empty(.w_SSNAZION))
        .link_2_13('Full')
      endif
    endwith
    this.DoRTCalc(15,28,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SSDATINI = t_SSDATINI
    this.w_SSDATFIN = t_SSDATFIN
    this.w_SSDESCRI = t_SSDESCRI
    this.w_SSDESCR2 = t_SSDESCR2
    this.w_SSINDIRI = t_SSINDIRI
    this.w_SSINDIR2 = t_SSINDIR2
    this.w_SSCODFIS = t_SSCODFIS
    this.w_SSPARIVA = t_SSPARIVA
    this.w_SS___CAP = t_SS___CAP
    this.w_SSLOCALI = t_SSLOCALI
    this.w_SSPROVIN = t_SSPROVIN
    this.w_SSNAZION = t_SSNAZION
    this.w_SSINDWEB = t_SSINDWEB
    this.w_SS_EMAIL = t_SS_EMAIL
    this.w_SSTELEFO = t_SSTELEFO
    this.w_SSTELFAX = t_SSTELFAX
    this.w_SSNUMCEL = t_SSNUMCEL
    this.w_DESNAZ = t_DESNAZ
    this.w_CODISO = t_CODISO
    this.w_SSDESCR3 = t_SSDESCR3
    this.w_SSDESCR4 = t_SSDESCR4
    this.w_SSINDIR3 = t_SSINDIR3
    this.w_SSINDIR4 = t_SSINDIR4
    this.w_SSLOCAL2 = t_SSLOCAL2
    this.w_SS_EMPEC = t_SS_EMPEC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SSDATINI with this.w_SSDATINI
    replace t_SSDATFIN with this.w_SSDATFIN
    replace t_SSDESCRI with this.w_SSDESCRI
    replace t_SSDESCR2 with this.w_SSDESCR2
    replace t_SSINDIRI with this.w_SSINDIRI
    replace t_SSINDIR2 with this.w_SSINDIR2
    replace t_SSCODFIS with this.w_SSCODFIS
    replace t_SSPARIVA with this.w_SSPARIVA
    replace t_SS___CAP with this.w_SS___CAP
    replace t_SSLOCALI with this.w_SSLOCALI
    replace t_SSPROVIN with this.w_SSPROVIN
    replace t_SSNAZION with this.w_SSNAZION
    replace t_SSINDWEB with this.w_SSINDWEB
    replace t_SS_EMAIL with this.w_SS_EMAIL
    replace t_SSTELEFO with this.w_SSTELEFO
    replace t_SSTELFAX with this.w_SSTELFAX
    replace t_SSNUMCEL with this.w_SSNUMCEL
    replace t_DESNAZ with this.w_DESNAZ
    replace t_CODISO with this.w_CODISO
    replace t_SSDESCR3 with this.w_SSDESCR3
    replace t_SSDESCR4 with this.w_SSDESCR4
    replace t_SSINDIR3 with this.w_SSINDIR3
    replace t_SSINDIR4 with this.w_SSINDIR4
    replace t_SSLOCAL2 with this.w_SSLOCAL2
    replace t_SS_EMPEC with this.w_SS_EMPEC
    if i_srv='A'
      replace SSDATINI with this.w_SSDATINI
      replace SSDATFIN with this.w_SSDATFIN
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_msaPag1 as StdContainer
  Width  = 661
  height = 520
  stdWidth  = 661
  stdheight = 520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=4, width=642,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="SSDATINI",Label2="Valida dal",Field3="SSDATFIN",Label3="Fino al",Field4="SSDESCRI",Label4="Ragione sociale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219001466

  add object oStr_1_2 as StdString with uid="VJOTWGYBQW",Visible=.t., Left=43, Top=206,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="GDCPEAGGHM",Visible=.t., Left=8, Top=362,;
    Alignment=1, Width=92, Height=18,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="FDHXAJRHLL",Visible=.t., Left=26, Top=413,;
    Alignment=1, Width=74, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="AOMFQLYXMM",Visible=.t., Left=56, Top=468,;
    Alignment=1, Width=44, Height=15,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LRQXYDMVGC",Visible=.t., Left=28, Top=439,;
    Alignment=1, Width=72, Height=18,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="VVMFXKPARO",Visible=.t., Left=12, Top=310,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="PGBXULOCDF",Visible=.t., Left=28, Top=336,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FWEMOWWKLX",Visible=.t., Left=442, Top=206,;
    Alignment=1, Width=44, Height=15,;
    Caption="Tel.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JTGRCRHBVE",Visible=.t., Left=448, Top=232,;
    Alignment=1, Width=38, Height=15,;
    Caption="FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GEIRODGILB",Visible=.t., Left=433, Top=258,;
    Alignment=1, Width=53, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="AZYXOAXCAD",Visible=.t., Left=420, Top=362,;
    Alignment=1, Width=36, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="IROQWGAVNR",Visible=.t., Left=398, Top=413,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cod.ISO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="XMEWVEQOMU",Visible=.t., Left=56, Top=494,;
    Alignment=1, Width=44, Height=15,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=23,;
    width=636+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=24,width=635+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oSSDESCR2_2_5.Refresh()
      this.Parent.oSSINDIRI_2_6.Refresh()
      this.Parent.oSSINDIR2_2_7.Refresh()
      this.Parent.oSSCODFIS_2_8.Refresh()
      this.Parent.oSSPARIVA_2_9.Refresh()
      this.Parent.oSS___CAP_2_10.Refresh()
      this.Parent.oSSLOCALI_2_11.Refresh()
      this.Parent.oSSPROVIN_2_12.Refresh()
      this.Parent.oSSNAZION_2_13.Refresh()
      this.Parent.oSSINDWEB_2_14.Refresh()
      this.Parent.oSS_EMAIL_2_15.Refresh()
      this.Parent.oSSTELEFO_2_16.Refresh()
      this.Parent.oSSTELFAX_2_17.Refresh()
      this.Parent.oSSNUMCEL_2_18.Refresh()
      this.Parent.oDESNAZ_2_19.Refresh()
      this.Parent.oCODISO_2_20.Refresh()
      this.Parent.oSSDESCR3_2_21.Refresh()
      this.Parent.oSSDESCR4_2_22.Refresh()
      this.Parent.oSSINDIR3_2_23.Refresh()
      this.Parent.oSSINDIR4_2_24.Refresh()
      this.Parent.oSSLOCAL2_2_25.Refresh()
      this.Parent.oSS_EMPEC_2_26.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oSSDESCR2_2_5 as StdTrsField with uid="RVOJOYADIV",rtseq=6,rtrep=.t.,;
    cFormVar="w_SSDESCR2",value=space(60),;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 223338152,;
    cTotal="", bFixedPos=.t., cQueryName = "SSDESCR2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=199, Top=125, InputMask=replicate('X',60)

  add object oSSINDIRI_2_6 as StdTrsField with uid="LYLQFXOEYT",rtseq=7,rtrep=.t.,;
    cFormVar="w_SSINDIRI",value=space(35),;
    ToolTipText = "Indirizzo",;
    HelpContextID = 137793169,;
    cTotal="", bFixedPos=.t., cQueryName = "SSINDIRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=105, Top=206, InputMask=replicate('X',35)

  add object oSSINDIR2_2_7 as StdTrsField with uid="QKLAZDBKTL",rtseq=8,rtrep=.t.,;
    cFormVar="w_SSINDIR2",value=space(35),;
    ToolTipText = "Dati aggiuntivi all'indirizzo",;
    HelpContextID = 137793192,;
    cTotal="", bFixedPos=.t., cQueryName = "SSINDIR2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=105, Top=232, InputMask=replicate('X',35)

  add object oSSCODFIS_2_8 as StdTrsField with uid="PXLBKEUXSV",rtseq=9,rtrep=.t.,;
    cFormVar="w_SSCODFIS",value=space(16),enabled=.f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 80351609,;
    cTotal="", bFixedPos=.t., cQueryName = "SSCODFIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=105, Top=310, InputMask=replicate('X',16)

  add object oSSPARIVA_2_9 as StdTrsField with uid="VTVBXORDPD",rtseq=10,rtrep=.t.,;
    cFormVar="w_SSPARIVA",value=space(12),enabled=.f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 144499047,;
    cTotal="", bFixedPos=.t., cQueryName = "SSPARIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=105, Top=336, InputMask=replicate('X',12)

  add object oSS___CAP_2_10 as StdTrsField with uid="JCTVLMZIKW",rtseq=11,rtrep=.t.,;
    cFormVar="w_SS___CAP",value=space(9),;
    HelpContextID = 208940682,;
    cTotal="", bFixedPos=.t., cQueryName = "SS___CAP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=105, Top=362, InputMask=replicate('X',9)

  add object oSSLOCALI_2_11 as StdTrsField with uid="ZTQFTSKWCW",rtseq=12,rtrep=.t.,;
    cFormVar="w_SSLOCALI",value=space(30),;
    HelpContextID = 4546193,;
    cTotal="", bFixedPos=.t., cQueryName = "SSLOCALI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=184, Top=362, InputMask=replicate('X',30)

  add object oSSPROVIN_2_12 as StdTrsField with uid="LGEVEMOFZO",rtseq=13,rtrep=.t.,;
    cFormVar="w_SSPROVIN",value=space(2),;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 92135796,;
    cTotal="", bFixedPos=.t., cQueryName = "SSPROVIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=459, Top=362, InputMask=replicate('X',2)

  add object oSSNAZION_2_13 as StdTrsField with uid="ZNXXTEXWUW",rtseq=14,rtrep=.t.,;
    cFormVar="w_SSNAZION",value=space(3),;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 115555980,;
    cTotal="", bFixedPos=.t., cQueryName = "SSNAZION",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=105, Top=413, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_SSNAZION"

  func oSSNAZION_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oSSNAZION_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oSSNAZION_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oSSNAZION_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oSSNAZION_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_SSNAZION
    i_obj.ecpSave()
  endproc

  add object oSSINDWEB_2_14 as StdTrsField with uid="RWYGIAKZRB",rtseq=15,rtrep=.t.,;
    cFormVar="w_SSINDWEB",value=space(254),;
    ToolTipText = "Indirizzo sito Internet",;
    HelpContextID = 97087848,;
    cTotal="", bFixedPos=.t., cQueryName = "SSINDWEB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=527, Left=105, Top=439, InputMask=replicate('X',254)

  add object oSS_EMAIL_2_15 as StdTrsField with uid="HVOFOSFSMI",rtseq=16,rtrep=.t.,;
    cFormVar="w_SS_EMAIL",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 5362034,;
    cTotal="", bFixedPos=.t., cQueryName = "SS_EMAIL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=527, Left=105, Top=465, InputMask=replicate('X',254)

  add object oSSTELEFO_2_16 as StdTrsField with uid="DFVWHDWRCZ",rtseq=17,rtrep=.t.,;
    cFormVar="w_SSTELEFO",value=space(18),;
    ToolTipText = "Numero telefonico principale",;
    HelpContextID = 71377269,;
    cTotal="", bFixedPos=.t., cQueryName = "SSTELEFO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=492, Top=206, InputMask=replicate('X',18)

  add object oSSTELFAX_2_17 as StdTrsField with uid="EXEXAMYMBB",rtseq=18,rtrep=.t.,;
    cFormVar="w_SSTELFAX",value=space(18),;
    ToolTipText = "Numero di TELEX o FAX",;
    HelpContextID = 180280962,;
    cTotal="", bFixedPos=.t., cQueryName = "SSTELFAX",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=492, Top=232, InputMask=replicate('X',18)

  add object oSSNUMCEL_2_18 as StdTrsField with uid="THJOVWIAGS",rtseq=19,rtrep=.t.,;
    cFormVar="w_SSNUMCEL",value=space(18),;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 39895410,;
    cTotal="", bFixedPos=.t., cQueryName = "SSNUMCEL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=492, Top=258, InputMask=replicate('X',18)

  add object oDESNAZ_2_19 as StdTrsField with uid="UOZXFJELII",rtseq=21,rtrep=.t.,;
    cFormVar="w_DESNAZ",value=space(35),enabled=.f.,;
    HelpContextID = 124124618,;
    cTotal="", bFixedPos=.t., cQueryName = "DESNAZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=140, Top=413, InputMask=replicate('X',35)

  add object oCODISO_2_20 as StdTrsField with uid="ZFFCLQONYW",rtseq=22,rtrep=.t.,;
    cFormVar="w_CODISO",value=space(3),enabled=.f.,;
    ToolTipText = "Codice ISO nazione",;
    HelpContextID = 21750746,;
    cTotal="", bFixedPos=.t., cQueryName = "CODISO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=459, Top=413, InputMask=replicate('X',3)

  add object oSSDESCR3_2_21 as StdTrsField with uid="JKMCWJGFGT",rtseq=23,rtrep=.t.,;
    cFormVar="w_SSDESCR3",value=space(60),;
    HelpContextID = 223338151,;
    cTotal="", bFixedPos=.t., cQueryName = "SSDESCR3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=199, Top=152, InputMask=replicate('X',60)

  add object oSSDESCR4_2_22 as StdTrsField with uid="WMUUPOTYAB",rtseq=24,rtrep=.t.,;
    cFormVar="w_SSDESCR4",value=space(60),;
    HelpContextID = 223338150,;
    cTotal="", bFixedPos=.t., cQueryName = "SSDESCR4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=199, Top=179, InputMask=replicate('X',60)

  add object oSSINDIR3_2_23 as StdTrsField with uid="HJOONROZDQ",rtseq=25,rtrep=.t.,;
    cFormVar="w_SSINDIR3",value=space(35),;
    HelpContextID = 137793191,;
    cTotal="", bFixedPos=.t., cQueryName = "SSINDIR3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=105, Top=258, InputMask=replicate('X',35)

  add object oSSINDIR4_2_24 as StdTrsField with uid="AYPPJSQQXK",rtseq=26,rtrep=.t.,;
    cFormVar="w_SSINDIR4",value=space(35),;
    HelpContextID = 137793190,;
    cTotal="", bFixedPos=.t., cQueryName = "SSINDIR4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=105, Top=284, InputMask=replicate('X',35)

  add object oSSLOCAL2_2_25 as StdTrsField with uid="UOKYEQBMCW",rtseq=27,rtrep=.t.,;
    cFormVar="w_SSLOCAL2",value=space(30),;
    HelpContextID = 4546216,;
    cTotal="", bFixedPos=.t., cQueryName = "SSLOCAL2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=184, Top=387, InputMask=replicate('X',30)

  add object oSS_EMPEC_2_26 as StdTrsField with uid="WHZVINOOAY",rtseq=28,rtrep=.t.,;
    cFormVar="w_SS_EMPEC",value=space(254),;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 257020265,;
    cTotal="", bFixedPos=.t., cQueryName = "SS_EMPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=527, Left=105, Top=491, InputMask=replicate('X',254)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_msaBodyRow as CPBodyRowCnt
  Width=626
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="UBNHQHAXGC",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17105558,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oSSDATINI_2_2 as StdTrsField with uid="MOGBDXGXGJ",rtseq=3,rtrep=.t.,;
    cFormVar="w_SSDATINI",value=ctod("  /  /  "),isprimarykey=.t.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 121888401,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=41, Top=0

  add object oSSDATFIN_2_3 as StdTrsField with uid="DSNKSNGGJZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_SSDATFIN",value=ctod("  /  /  "),isprimarykey=.t.,;
    ToolTipText = "Data fine validit�",;
    HelpContextID = 96215412,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine validit� non valida",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=115, Top=0

  func oSSDATFIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SSDATFIN) OR .w_SSDATFIN>=.w_SSDATINI)
    endwith
    return bRes
  endfunc

  add object oSSDESCRI_2_4 as StdTrsField with uid="LOMWPVWPBD",rtseq=5,rtrep=.t.,;
    cFormVar="w_SSDESCRI",value=space(60),;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 223338129,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=433, Left=188, Top=0, InputMask=replicate('X',60)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_msa','SED_STOR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SSTIPCON=SED_STOR.SSTIPCON";
  +" and "+i_cAliasName2+".SSCODICE=SED_STOR.SSCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
