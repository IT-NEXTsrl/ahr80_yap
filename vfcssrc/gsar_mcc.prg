* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mcc                                                        *
*              Conti correnti                                                  *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_215]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-23                                                      *
* Last revis.: 2016-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mcc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mcc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mcc")
  return

* --- Class definition
define class tgsar_mcc as StdPCForm
  Width  = 713
  Height = 272
  Top    = 5
  Left   = 7
  cComment = "Conti correnti"
  cPrg = "gsar_mcc"
  HelpContextID=97089385
  add object cnt as tcgsar_mcc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mcc as PCContext
  w_CCTIPCON = space(1)
  w_CCCODCON = space(15)
  w_ANCODBAN = space(10)
  w_ANNUMCOR = space(24)
  w_AN__IBAN = space(35)
  w_AN__BBAN = space(30)
  w_CCCODBAN = space(10)
  w_CCCONCOR = space(25)
  w_FLBEST = space(1)
  w_DESBAN = space(50)
  w_CODABI = space(5)
  w_CODCAB = space(5)
  w_PASEUR = space(2)
  w_CCDESBAN = space(50)
  w_CCCINCAB = space(2)
  w_CCCINABI = space(1)
  w_CCCODVAL = space(3)
  w_CCCOBBAN = space(23)
  w_IBAN = space(30)
  w_PAESE = space(2)
  w_CCCOIBAN = space(34)
  w_CODIBAN = space(34)
  w_CCCODBIC = space(11)
  w_DEFAULT = space(1)
  w_CCNUMTEL = space(18)
  w_CC_EMAIL = space(50)
  w_CC_EMPEC = space(50)
  w_CCOBSOLE = space(1)
  w_PASEURO = space(10)
  w_CIFRA = space(2)
  w_CCCODVAL = space(3)
  w_SIMVAL = space(3)
  w_CHECKCIN = space(1)
  w_DESABI = space(80)
  w_DESCAB = space(40)
  w_CODPA = space(2)
  proc Save(i_oFrom)
    this.w_CCTIPCON = i_oFrom.w_CCTIPCON
    this.w_CCCODCON = i_oFrom.w_CCCODCON
    this.w_ANCODBAN = i_oFrom.w_ANCODBAN
    this.w_ANNUMCOR = i_oFrom.w_ANNUMCOR
    this.w_AN__IBAN = i_oFrom.w_AN__IBAN
    this.w_AN__BBAN = i_oFrom.w_AN__BBAN
    this.w_CCCODBAN = i_oFrom.w_CCCODBAN
    this.w_CCCONCOR = i_oFrom.w_CCCONCOR
    this.w_FLBEST = i_oFrom.w_FLBEST
    this.w_DESBAN = i_oFrom.w_DESBAN
    this.w_CODABI = i_oFrom.w_CODABI
    this.w_CODCAB = i_oFrom.w_CODCAB
    this.w_PASEUR = i_oFrom.w_PASEUR
    this.w_CCDESBAN = i_oFrom.w_CCDESBAN
    this.w_CCCINCAB = i_oFrom.w_CCCINCAB
    this.w_CCCINABI = i_oFrom.w_CCCINABI
    this.w_CCCODVAL = i_oFrom.w_CCCODVAL
    this.w_CCCOBBAN = i_oFrom.w_CCCOBBAN
    this.w_IBAN = i_oFrom.w_IBAN
    this.w_PAESE = i_oFrom.w_PAESE
    this.w_CCCOIBAN = i_oFrom.w_CCCOIBAN
    this.w_CODIBAN = i_oFrom.w_CODIBAN
    this.w_CCCODBIC = i_oFrom.w_CCCODBIC
    this.w_DEFAULT = i_oFrom.w_DEFAULT
    this.w_CCNUMTEL = i_oFrom.w_CCNUMTEL
    this.w_CC_EMAIL = i_oFrom.w_CC_EMAIL
    this.w_CC_EMPEC = i_oFrom.w_CC_EMPEC
    this.w_CCOBSOLE = i_oFrom.w_CCOBSOLE
    this.w_PASEURO = i_oFrom.w_PASEURO
    this.w_CIFRA = i_oFrom.w_CIFRA
    this.w_CCCODVAL = i_oFrom.w_CCCODVAL
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_CHECKCIN = i_oFrom.w_CHECKCIN
    this.w_DESABI = i_oFrom.w_DESABI
    this.w_DESCAB = i_oFrom.w_DESCAB
    this.w_CODPA = i_oFrom.w_CODPA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CCTIPCON = this.w_CCTIPCON
    i_oTo.w_CCCODCON = this.w_CCCODCON
    i_oTo.w_ANCODBAN = this.w_ANCODBAN
    i_oTo.w_ANNUMCOR = this.w_ANNUMCOR
    i_oTo.w_AN__IBAN = this.w_AN__IBAN
    i_oTo.w_AN__BBAN = this.w_AN__BBAN
    i_oTo.w_CCCODBAN = this.w_CCCODBAN
    i_oTo.w_CCCONCOR = this.w_CCCONCOR
    i_oTo.w_FLBEST = this.w_FLBEST
    i_oTo.w_DESBAN = this.w_DESBAN
    i_oTo.w_CODABI = this.w_CODABI
    i_oTo.w_CODCAB = this.w_CODCAB
    i_oTo.w_PASEUR = this.w_PASEUR
    i_oTo.w_CCDESBAN = this.w_CCDESBAN
    i_oTo.w_CCCINCAB = this.w_CCCINCAB
    i_oTo.w_CCCINABI = this.w_CCCINABI
    i_oTo.w_CCCODVAL = this.w_CCCODVAL
    i_oTo.w_CCCOBBAN = this.w_CCCOBBAN
    i_oTo.w_IBAN = this.w_IBAN
    i_oTo.w_PAESE = this.w_PAESE
    i_oTo.w_CCCOIBAN = this.w_CCCOIBAN
    i_oTo.w_CODIBAN = this.w_CODIBAN
    i_oTo.w_CCCODBIC = this.w_CCCODBIC
    i_oTo.w_DEFAULT = this.w_DEFAULT
    i_oTo.w_CCNUMTEL = this.w_CCNUMTEL
    i_oTo.w_CC_EMAIL = this.w_CC_EMAIL
    i_oTo.w_CC_EMPEC = this.w_CC_EMPEC
    i_oTo.w_CCOBSOLE = this.w_CCOBSOLE
    i_oTo.w_PASEURO = this.w_PASEURO
    i_oTo.w_CIFRA = this.w_CIFRA
    i_oTo.w_CCCODVAL = this.w_CCCODVAL
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_CHECKCIN = this.w_CHECKCIN
    i_oTo.w_DESABI = this.w_DESABI
    i_oTo.w_DESCAB = this.w_DESCAB
    i_oTo.w_CODPA = this.w_CODPA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mcc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 713
  Height = 272
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-28"
  HelpContextID=97089385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  BAN_CONTI_IDX = 0
  BAN_CHE_IDX = 0
  VALUTE_IDX = 0
  cFile = "BAN_CONTI"
  cKeySelect = "CCTIPCON,CCCODCON"
  cKeyWhere  = "CCTIPCON=this.w_CCTIPCON and CCCODCON=this.w_CCCODCON"
  cKeyDetail  = "CCTIPCON=this.w_CCTIPCON and CCCODCON=this.w_CCCODCON and CCCODBAN=this.w_CCCODBAN and CCCONCOR=this.w_CCCONCOR"
  cKeyWhereODBC = '"CCTIPCON="+cp_ToStrODBC(this.w_CCTIPCON)';
      +'+" and CCCODCON="+cp_ToStrODBC(this.w_CCCODCON)';

  cKeyDetailWhereODBC = '"CCTIPCON="+cp_ToStrODBC(this.w_CCTIPCON)';
      +'+" and CCCODCON="+cp_ToStrODBC(this.w_CCCODCON)';
      +'+" and CCCODBAN="+cp_ToStrODBC(this.w_CCCODBAN)';
      +'+" and CCCONCOR="+cp_ToStrODBC(this.w_CCCONCOR)';

  cKeyWhereODBCqualified = '"BAN_CONTI.CCTIPCON="+cp_ToStrODBC(this.w_CCTIPCON)';
      +'+" and BAN_CONTI.CCCODCON="+cp_ToStrODBC(this.w_CCCODCON)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mcc"
  cComment = "Conti correnti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCTIPCON = space(1)
  w_CCCODCON = space(15)
  w_ANCODBAN = space(10)
  o_ANCODBAN = space(10)
  w_ANNUMCOR = space(24)
  w_AN__IBAN = space(35)
  w_AN__BBAN = space(30)
  w_CCCODBAN = space(10)
  o_CCCODBAN = space(10)
  w_CCCONCOR = space(25)
  o_CCCONCOR = space(25)
  w_FLBEST = space(1)
  o_FLBEST = space(1)
  w_DESBAN = space(50)
  w_CODABI = space(5)
  o_CODABI = space(5)
  w_CODCAB = space(5)
  o_CODCAB = space(5)
  w_PASEUR = space(2)
  w_CCDESBAN = space(50)
  w_CCCINCAB = space(2)
  o_CCCINCAB = space(2)
  w_CCCINABI = space(1)
  o_CCCINABI = space(1)
  w_CCCODVAL = space(3)
  w_CCCOBBAN = space(23)
  o_CCCOBBAN = space(23)
  w_IBAN = space(30)
  w_PAESE = space(2)
  w_CCCOIBAN = space(34)
  o_CCCOIBAN = space(34)
  w_CODIBAN = space(34)
  o_CODIBAN = space(34)
  w_CCCODBIC = space(11)
  w_DEFAULT = space(1)
  w_CCNUMTEL = space(18)
  w_CC_EMAIL = space(50)
  w_CC_EMPEC = space(50)
  w_CCOBSOLE = space(1)
  w_PASEURO = space(10)
  o_PASEURO = space(10)
  w_CIFRA = space(2)
  o_CIFRA = space(2)
  w_CCCODVAL = space(3)
  w_SIMVAL = space(3)
  w_CHECKCIN = space(1)
  w_DESABI = space(80)
  w_DESCAB = space(40)
  w_CODPA = space(2)
  o_CODPA = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_mcc
  * Ridefinisco il tasto F6, effettuo questa operazione per non permettere di
  * cancellare conti che sono stati gi� utilizzati
  Proc F6()
  * --- disabilita F6 per righe non cancellabili
  	if inlist(this.cFunction,'Edit','Load') And this.ActiveControl.name='OBODY'
      bOK=.f.
      this.NotifyEvent('Cancella')
  		If Not bOK
  			AH_ERRORMSG("Impossibile cancellare la riga:%0Il conto corrente � gi� stato utilizzato nelle scadenze")
  		else
  	    dodefault()
  		Endif
  	Endif
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mccPag1","gsar_mcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='BAN_CONTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.BAN_CONTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.BAN_CONTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mcc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_36_joined
    link_2_36_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from BAN_CONTI where CCTIPCON=KeySet.CCTIPCON
    *                            and CCCODCON=KeySet.CCCODCON
    *                            and CCCODBAN=KeySet.CCCODBAN
    *                            and CCCONCOR=KeySet.CCCONCOR
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2],this.bLoadRecFilter,this.BAN_CONTI_IDX,"gsar_mcc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('BAN_CONTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "BAN_CONTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' BAN_CONTI '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_36_joined=this.AddJoinedLink_2_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCTIPCON',this.w_CCTIPCON  ,'CCCODCON',this.w_CCCODCON  )
      select * from (i_cTable) BAN_CONTI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ANCODBAN = iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)
        .w_ANNUMCOR = iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)
        .w_CCTIPCON = NVL(CCTIPCON,space(1))
        .w_CCCODCON = NVL(CCCODCON,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'BAN_CONTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_FLBEST = space(1)
          .w_DESBAN = space(50)
          .w_CODABI = space(5)
          .w_CODCAB = space(5)
          .w_PAESE = space(2)
          .w_PASEURO = space(10)
          .w_SIMVAL = space(3)
          .w_CHECKCIN = space(1)
          .w_DESABI = space(80)
          .w_DESCAB = space(40)
        .w_CODPA = iif(islita(),.w_PAESE,'  ')
        .w_AN__IBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' ,this.oParentObject .w_AN__IBAN)
        .w_AN__BBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' , this.oParentObject .w_AN__BBAN)
          .w_CCCODBAN = NVL(CCCODBAN,space(10))
          if link_2_1_joined
            this.w_CCCODBAN = NVL(BACODBAN201,NVL(this.w_CCCODBAN,space(10)))
            this.w_CODABI = NVL(BACODABI201,space(5))
            this.w_CODCAB = NVL(BACODCAB201,space(5))
            this.w_DESBAN = NVL(BADESBAN201,space(50))
            this.w_PASEURO = NVL(BAPASEUR201,space(10))
            this.w_FLBEST = NVL(BAFLBEST201,space(1))
            this.w_DESABI = NVL(BADESABI201,space(80))
            this.w_DESCAB = NVL(BADESFIL201,space(40))
            this.w_PAESE = NVL(BAPASEUR201,space(2))
          else
          .link_2_1('Load')
          endif
          .w_CCCONCOR = NVL(CCCONCOR,space(25))
        .w_PASEUR = iif(! empty(.w_PASEURO),.w_PASEURO,'IT')
          .w_CCDESBAN = NVL(CCDESBAN,space(50))
          .w_CCCINCAB = NVL(CCCINCAB,space(2))
          .w_CCCINABI = NVL(CCCINABI,space(1))
          .w_CCCODVAL = NVL(CCCODVAL,space(3))
          .w_CCCOBBAN = NVL(CCCOBBAN,space(23))
        .w_IBAN = IIF(.w_FLBEST='S' OR ! ISLITA(), .w_IBAN, .w_CCCOBBAN)
          .w_CCCOIBAN = NVL(CCCOIBAN,space(34))
        .w_CODIBAN = iif(! empty(.w_CCCOIBAN), .w_CCCOIBAN,' ')
          .w_CCCODBIC = NVL(CCCODBIC,space(11))
        .w_DEFAULT = iif(iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)=.w_CCCODBAN AND iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)=.w_CCCONCOR AND not empty(nvl(.w_CCCODBAN,'')),'S',' ')
          .w_CCNUMTEL = NVL(CCNUMTEL,space(18))
          .w_CC_EMAIL = NVL(CC_EMAIL,space(50))
          .w_CC_EMPEC = NVL(CC_EMPEC,space(50))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
          .w_CCOBSOLE = NVL(CCOBSOLE,space(1))
        .w_CIFRA = iif(! empty(.w_CCCINCAB) AND ISLITA(),.w_CCCINCAB,.w_CIFRA)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
          .w_CCCODVAL = NVL(CCCODVAL,space(3))
          if link_2_36_joined
            this.w_CCCODVAL = NVL(VACODVAL236,NVL(this.w_CCCODVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL236,space(3))
          else
          .link_2_36('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CCCODBAN with .w_CCCODBAN
          replace CCCONCOR with .w_CCCONCOR
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_AN__IBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' ,this.oParentObject .w_AN__IBAN)
        .w_AN__BBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' , this.oParentObject .w_AN__BBAN)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_mcc
    * ---
    Update (this.ctrsname) Set t_CIFRA=SUBSTR(t_CCCOIBAN,3,2),t_CODPA=SUBSTR(t_CCCOIBAN,1,2),t_IBAN=SUBSTR(t_CCCOIBAN,5,30)
    this.Workfromtrs()
    this.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CCTIPCON=space(1)
      .w_CCCODCON=space(15)
      .w_ANCODBAN=space(10)
      .w_ANNUMCOR=space(24)
      .w_AN__IBAN=space(35)
      .w_AN__BBAN=space(30)
      .w_CCCODBAN=space(10)
      .w_CCCONCOR=space(25)
      .w_FLBEST=space(1)
      .w_DESBAN=space(50)
      .w_CODABI=space(5)
      .w_CODCAB=space(5)
      .w_PASEUR=space(2)
      .w_CCDESBAN=space(50)
      .w_CCCINCAB=space(2)
      .w_CCCINABI=space(1)
      .w_CCCODVAL=space(3)
      .w_CCCOBBAN=space(23)
      .w_IBAN=space(30)
      .w_PAESE=space(2)
      .w_CCCOIBAN=space(34)
      .w_CODIBAN=space(34)
      .w_CCCODBIC=space(11)
      .w_DEFAULT=space(1)
      .w_CCNUMTEL=space(18)
      .w_CC_EMAIL=space(50)
      .w_CC_EMPEC=space(50)
      .w_CCOBSOLE=space(1)
      .w_PASEURO=space(10)
      .w_CIFRA=space(2)
      .w_CCCODVAL=space(3)
      .w_SIMVAL=space(3)
      .w_CHECKCIN=space(1)
      .w_DESABI=space(80)
      .w_DESCAB=space(40)
      .w_CODPA=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_ANCODBAN = iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)
        .w_ANNUMCOR = iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)
        .w_AN__IBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' ,this.oParentObject .w_AN__IBAN)
        .w_AN__BBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' , this.oParentObject .w_AN__BBAN)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CCCODBAN))
         .link_2_1('Full')
        endif
        .DoRTCalc(8,12,.f.)
        .w_PASEUR = iif(! empty(.w_PASEURO),.w_PASEURO,'IT')
        .DoRTCalc(14,17,.f.)
        .w_CCCOBBAN = iif(.w_FLBEST='S', .w_CCCOBBAN, CALCBBAN(.w_CCCINABI,.w_CCCINCAB,.w_CODABI,.w_CODCAB,SUBSTR(.w_CCCONCOR,1,12),' ',.w_FLBEST))
        .w_IBAN = IIF(.w_FLBEST='S' OR ! ISLITA(), .w_IBAN, .w_CCCOBBAN)
        .DoRTCalc(20,21,.f.)
        .w_CODIBAN = iif(! empty(.w_CCCOIBAN), .w_CCCOIBAN,' ')
        .DoRTCalc(23,23,.f.)
        .w_DEFAULT = iif(iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)=.w_CCCODBAN AND iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)=.w_CCCONCOR AND not empty(nvl(.w_CCCODBAN,'')),'S',' ')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
        .DoRTCalc(25,27,.f.)
        .w_CCOBSOLE = 'N'
        .DoRTCalc(29,29,.f.)
        .w_CIFRA = iif(! empty(.w_CCCINCAB) AND ISLITA(),.w_CCCINCAB,.w_CIFRA)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CCCODVAL))
         .link_2_36('Full')
        endif
        .DoRTCalc(32,35,.f.)
        .w_CODPA = iif(islita(),.w_PAESE,'  ')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'BAN_CONTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPASEUR_2_7.enabled = i_bVal
      .Page1.oPag.oCCDESBAN_2_8.enabled = i_bVal
      .Page1.oPag.oCCCINCAB_2_9.enabled = i_bVal
      .Page1.oPag.oCCCINABI_2_10.enabled = i_bVal
      .Page1.oPag.oIBAN_2_14.enabled = i_bVal
      .Page1.oPag.oCODIBAN_2_17.enabled = i_bVal
      .Page1.oPag.oCCCODBIC_2_18.enabled = i_bVal
      .Page1.oPag.oCCNUMTEL_2_25.enabled = i_bVal
      .Page1.oPag.oCC_EMAIL_2_26.enabled = i_bVal
      .Page1.oPag.oCC_EMPEC_2_27.enabled = i_bVal
      .Page1.oPag.oCIFRA_2_34.enabled = i_bVal
      .Page1.oPag.oCCCODVAL_2_36.enabled = i_bVal
      .Page1.oPag.oCODPA_2_46.enabled = i_bVal
      .Page1.oPag.oBtn_2_40.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'BAN_CONTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPCON,"CCTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODCON,"CCCODCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CCCODBAN C(10);
      ,t_CCCONCOR C(25);
      ,t_DESBAN C(50);
      ,t_CODABI C(5);
      ,t_CODCAB C(5);
      ,t_PASEUR C(2);
      ,t_CCDESBAN C(50);
      ,t_CCCINCAB C(2);
      ,t_CCCINABI C(1);
      ,t_CCCOBBAN C(23);
      ,t_IBAN C(30);
      ,t_CODIBAN C(34);
      ,t_CCCODBIC C(11);
      ,t_DEFAULT N(3);
      ,t_CCNUMTEL C(18);
      ,t_CC_EMAIL C(50);
      ,t_CC_EMPEC C(50);
      ,t_CIFRA C(2);
      ,t_SIMVAL C(3);
      ,t_DESABI C(80);
      ,t_DESCAB C(40);
      ,t_CODPA C(2);
      ,CCCODBAN C(10);
      ,CCCONCOR C(25);
      ,t_AN__IBAN C(35);
      ,t_AN__BBAN C(30);
      ,t_FLBEST C(1);
      ,t_CCCODVAL C(3);
      ,t_PAESE C(2);
      ,t_CCCOIBAN C(34);
      ,t_CCOBSOLE C(1);
      ,t_PASEURO C(10);
      ,t_CHECKCIN C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mccbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODBAN_2_1.controlsource=this.cTrsName+'.t_CCCODBAN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCONCOR_2_2.controlsource=this.cTrsName+'.t_CCCONCOR'
    this.oPgFRm.Page1.oPag.oDESBAN_2_4.controlsource=this.cTrsName+'.t_DESBAN'
    this.oPgFRm.Page1.oPag.oCODABI_2_5.controlsource=this.cTrsName+'.t_CODABI'
    this.oPgFRm.Page1.oPag.oCODCAB_2_6.controlsource=this.cTrsName+'.t_CODCAB'
    this.oPgFRm.Page1.oPag.oPASEUR_2_7.controlsource=this.cTrsName+'.t_PASEUR'
    this.oPgFRm.Page1.oPag.oCCDESBAN_2_8.controlsource=this.cTrsName+'.t_CCDESBAN'
    this.oPgFRm.Page1.oPag.oCCCINCAB_2_9.controlsource=this.cTrsName+'.t_CCCINCAB'
    this.oPgFRm.Page1.oPag.oCCCINABI_2_10.controlsource=this.cTrsName+'.t_CCCINABI'
    this.oPgFRm.Page1.oPag.oCCCOBBAN_2_13.controlsource=this.cTrsName+'.t_CCCOBBAN'
    this.oPgFRm.Page1.oPag.oIBAN_2_14.controlsource=this.cTrsName+'.t_IBAN'
    this.oPgFRm.Page1.oPag.oCODIBAN_2_17.controlsource=this.cTrsName+'.t_CODIBAN'
    this.oPgFRm.Page1.oPag.oCCCODBIC_2_18.controlsource=this.cTrsName+'.t_CCCODBIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.controlsource=this.cTrsName+'.t_DEFAULT'
    this.oPgFRm.Page1.oPag.oCCNUMTEL_2_25.controlsource=this.cTrsName+'.t_CCNUMTEL'
    this.oPgFRm.Page1.oPag.oCC_EMAIL_2_26.controlsource=this.cTrsName+'.t_CC_EMAIL'
    this.oPgFRm.Page1.oPag.oCC_EMPEC_2_27.controlsource=this.cTrsName+'.t_CC_EMPEC'
    this.oPgFRm.Page1.oPag.oCIFRA_2_34.controlsource=this.cTrsName+'.t_CIFRA'
    this.oPgFRm.Page1.oPag.oCCCODVAL_2_36.controlsource=this.cTrsName+'.t_CCCODVAL'
    this.oPgFRm.Page1.oPag.oSIMVAL_2_38.controlsource=this.cTrsName+'.t_SIMVAL'
    this.oPgFRm.Page1.oPag.oDESABI_2_44.controlsource=this.cTrsName+'.t_DESABI'
    this.oPgFRm.Page1.oPag.oDESCAB_2_45.controlsource=this.cTrsName+'.t_DESCAB'
    this.oPgFRm.Page1.oPag.oCODPA_2_46.controlsource=this.cTrsName+'.t_CODPA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(97)
    this.AddVLine(284)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODBAN_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
      *
      * insert into BAN_CONTI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'BAN_CONTI')
        i_extval=cp_InsertValODBCExtFlds(this,'BAN_CONTI')
        i_cFldBody=" "+;
                  "(CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCDESBAN"+;
                  ",CCCINCAB,CCCINABI,CCCODVAL,CCCOBBAN,CCCOIBAN"+;
                  ",CCCODBIC,CCNUMTEL,CC_EMAIL,CC_EMPEC,CCOBSOLE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCTIPCON)+","+cp_ToStrODBC(this.w_CCCODCON)+","+cp_ToStrODBCNull(this.w_CCCODBAN)+","+cp_ToStrODBC(this.w_CCCONCOR)+","+cp_ToStrODBC(this.w_CCDESBAN)+;
             ","+cp_ToStrODBC(this.w_CCCINCAB)+","+cp_ToStrODBC(this.w_CCCINABI)+","+cp_ToStrODBC(this.w_CCCODVAL)+","+cp_ToStrODBC(this.w_CCCOBBAN)+","+cp_ToStrODBC(this.w_CCCOIBAN)+;
             ","+cp_ToStrODBC(this.w_CCCODBIC)+","+cp_ToStrODBC(this.w_CCNUMTEL)+","+cp_ToStrODBC(this.w_CC_EMAIL)+","+cp_ToStrODBC(this.w_CC_EMPEC)+","+cp_ToStrODBC(this.w_CCOBSOLE)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'BAN_CONTI')
        i_extval=cp_InsertValVFPExtFlds(this,'BAN_CONTI')
        cp_CheckDeletedKey(i_cTable,0,'CCTIPCON',this.w_CCTIPCON,'CCCODCON',this.w_CCCODCON,'CCCODBAN',this.w_CCCODBAN,'CCCONCOR',this.w_CCCONCOR)
        INSERT INTO (i_cTable) (;
                   CCTIPCON;
                  ,CCCODCON;
                  ,CCCODBAN;
                  ,CCCONCOR;
                  ,CCDESBAN;
                  ,CCCINCAB;
                  ,CCCINABI;
                  ,CCCODVAL;
                  ,CCCOBBAN;
                  ,CCCOIBAN;
                  ,CCCODBIC;
                  ,CCNUMTEL;
                  ,CC_EMAIL;
                  ,CC_EMPEC;
                  ,CCOBSOLE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CCTIPCON;
                  ,this.w_CCCODCON;
                  ,this.w_CCCODBAN;
                  ,this.w_CCCONCOR;
                  ,this.w_CCDESBAN;
                  ,this.w_CCCINCAB;
                  ,this.w_CCCINABI;
                  ,this.w_CCCODVAL;
                  ,this.w_CCCOBBAN;
                  ,this.w_CCCOIBAN;
                  ,this.w_CCCODBIC;
                  ,this.w_CCNUMTEL;
                  ,this.w_CC_EMAIL;
                  ,this.w_CC_EMPEC;
                  ,this.w_CCOBSOLE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsar_mcc
    this.NotifyEvent('Controlli')
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(NVL(t_CCCODBAN,'')) AND NOT EMPTY(NVL(t_CCCONCOR,''))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'BAN_CONTI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CCCODBAN="+cp_ToStrODBC(&i_TN.->CCCODBAN)+;
                 " and CCCONCOR="+cp_ToStrODBC(&i_TN.->CCCONCOR)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'BAN_CONTI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CCCODBAN=&i_TN.->CCCODBAN;
                      and CCCONCOR=&i_TN.->CCCONCOR;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(NVL(t_CCCODBAN,'')) AND NOT EMPTY(NVL(t_CCCONCOR,''))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CCCODBAN="+cp_ToStrODBC(&i_TN.->CCCODBAN)+;
                            " and CCCONCOR="+cp_ToStrODBC(&i_TN.->CCCONCOR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CCCODBAN=&i_TN.->CCCODBAN;
                            and CCCONCOR=&i_TN.->CCCONCOR;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CCCODBAN with this.w_CCCODBAN
              replace CCCONCOR with this.w_CCCONCOR
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update BAN_CONTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'BAN_CONTI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CCDESBAN="+cp_ToStrODBC(this.w_CCDESBAN)+;
                     ",CCCINCAB="+cp_ToStrODBC(this.w_CCCINCAB)+;
                     ",CCCINABI="+cp_ToStrODBC(this.w_CCCINABI)+;
                     ",CCCODVAL="+cp_ToStrODBC(this.w_CCCODVAL)+;
                     ",CCCOBBAN="+cp_ToStrODBC(this.w_CCCOBBAN)+;
                     ",CCCOIBAN="+cp_ToStrODBC(this.w_CCCOIBAN)+;
                     ",CCCODBIC="+cp_ToStrODBC(this.w_CCCODBIC)+;
                     ",CCNUMTEL="+cp_ToStrODBC(this.w_CCNUMTEL)+;
                     ",CC_EMAIL="+cp_ToStrODBC(this.w_CC_EMAIL)+;
                     ",CC_EMPEC="+cp_ToStrODBC(this.w_CC_EMPEC)+;
                     ",CCOBSOLE="+cp_ToStrODBC(this.w_CCOBSOLE)+;
                     ",CCCODBAN="+cp_ToStrODBC(this.w_CCCODBAN)+;
                     ",CCCONCOR="+cp_ToStrODBC(this.w_CCCONCOR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CCCODBAN="+cp_ToStrODBC(CCCODBAN)+;
                             " and CCCONCOR="+cp_ToStrODBC(CCCONCOR)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'BAN_CONTI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CCDESBAN=this.w_CCDESBAN;
                     ,CCCINCAB=this.w_CCCINCAB;
                     ,CCCINABI=this.w_CCCINABI;
                     ,CCCODVAL=this.w_CCCODVAL;
                     ,CCCOBBAN=this.w_CCCOBBAN;
                     ,CCCOIBAN=this.w_CCCOIBAN;
                     ,CCCODBIC=this.w_CCCODBIC;
                     ,CCNUMTEL=this.w_CCNUMTEL;
                     ,CC_EMAIL=this.w_CC_EMAIL;
                     ,CC_EMPEC=this.w_CC_EMPEC;
                     ,CCOBSOLE=this.w_CCOBSOLE;
                     ,CCCODBAN=this.w_CCCODBAN;
                     ,CCCONCOR=this.w_CCCONCOR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CCCODBAN=&i_TN.->CCCODBAN;
                                      and CCCONCOR=&i_TN.->CCCONCOR;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(NVL(t_CCCODBAN,'')) AND NOT EMPTY(NVL(t_CCCONCOR,''))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete BAN_CONTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CCCODBAN="+cp_ToStrODBC(&i_TN.->CCCODBAN)+;
                            " and CCCONCOR="+cp_ToStrODBC(&i_TN.->CCCONCOR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CCCODBAN=&i_TN.->CCCODBAN;
                              and CCCONCOR=&i_TN.->CCCONCOR;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(NVL(t_CCCODBAN,'')) AND NOT EMPTY(NVL(t_CCCONCOR,''))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_AN__IBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' ,this.oParentObject .w_AN__IBAN)
          .w_AN__BBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' , this.oParentObject .w_AN__BBAN)
        .DoRTCalc(7,12,.t.)
        if .o_PASEURO<>.w_PASEURO
          .w_PASEUR = iif(! empty(.w_PASEURO),.w_PASEURO,'IT')
        endif
        if .o_CCCONCOR<>.w_CCCONCOR.or. .o_CODABI<>.w_CODABI.or. .o_CODCAB<>.w_CODCAB
          .Calculate_GNBCSRRHIR()
        endif
        .DoRTCalc(14,17,.t.)
        if .o_CCCODBAN<>.w_CCCODBAN.or. .o_CCCONCOR<>.w_CCCONCOR.or. .o_CCCINABI<>.w_CCCINABI.or. .o_FLBEST<>.w_FLBEST.or. .o_CCCINCAB<>.w_CCCINCAB
          .w_CCCOBBAN = iif(.w_FLBEST='S', .w_CCCOBBAN, CALCBBAN(.w_CCCINABI,.w_CCCINCAB,.w_CODABI,.w_CODCAB,SUBSTR(.w_CCCONCOR,1,12),' ',.w_FLBEST))
        endif
        if .o_CCCOBBAN<>.w_CCCOBBAN
          .w_IBAN = IIF(.w_FLBEST='S' OR ! ISLITA(), .w_IBAN, .w_CCCOBBAN)
        endif
        .DoRTCalc(20,21,.t.)
        if .o_CCCOIBAN<>.w_CCCOIBAN
          .w_CODIBAN = iif(! empty(.w_CCCOIBAN), .w_CCCOIBAN,' ')
        endif
        .DoRTCalc(23,23,.t.)
        if .o_ANCODBAN<>.w_ANCODBAN.or. .o_CCCOBBAN<>.w_CCCOBBAN
          .w_DEFAULT = iif(iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)=.w_CCCODBAN AND iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)=.w_CCCONCOR AND not empty(nvl(.w_CCCODBAN,'')),'S',' ')
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
        if .o_CODPA<>.w_CODPA.or. .o_CCCOBBAN<>.w_CCCOBBAN
          .Calculate_AIEAWJSJAD()
        endif
        .DoRTCalc(25,29,.t.)
        if .o_CCCINCAB<>.w_CCCINCAB
          .w_CIFRA = iif(! empty(.w_CCCINCAB) AND ISLITA(),.w_CCCINCAB,.w_CIFRA)
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
        if .o_CCCONCOR<>.w_CCCONCOR.or. .o_CCCINABI<>.w_CCCINABI.or. .o_CCCOBBAN<>.w_CCCOBBAN.or. .o_CCCODBAN<>.w_CCCODBAN.or. .o_CODPA<>.w_CODPA.or. .o_CIFRA<>.w_CIFRA.or. .o_CODIBAN<>.w_CODIBAN
          .Calculate_VYQFTIYFIG()
        endif
        if .o_CCCINABI<>.w_CCCINABI
          .Calculate_DJOGELCKVN()
        endif
        if .o_CCCODBAN<>.w_CCCODBAN
          .Calculate_MQKACRNPAH()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,36,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_AN__IBAN with this.w_AN__IBAN
      replace t_AN__BBAN with this.w_AN__BBAN
      replace t_FLBEST with this.w_FLBEST
      replace t_CCCODVAL with this.w_CCCODVAL
      replace t_PAESE with this.w_PAESE
      replace t_CCCOIBAN with this.w_CCCOIBAN
      replace t_CCOBSOLE with this.w_CCOBSOLE
      replace t_PASEURO with this.w_PASEURO
      replace t_CHECKCIN with this.w_CHECKCIN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
    endwith
  return
  proc Calculate_GNBCSRRHIR()
    with this
          * --- Calcolo il codice CIN
          .w_CCCINABI = iif(.w_FLBEST # 'S' and islita(),CALCCIN('C',.w_CODABI,.w_CODCAB,.w_CCCONCOR),.w_CCCINABI)
    endwith
  endproc
  proc Calculate_AIEAWJSJAD()
    with this
          * --- Calcolo caratteri di controllo
          .w_CCCINCAB = iif(.w_FLBEST#'S' and islita(),CHECKIBAN('C',.w_CODPA+'00'+.w_CCCOBBAN),.w_CCCINCAB)
    endwith
  endproc
  proc Calculate_VYQFTIYFIG()
    with this
          * --- Calcolo codice IBAN
          .w_CCCOIBAN = iif(.w_FLBEST = 'S' or ! islita(),.w_CODIBAN,CALCIBAN(.w_CODPA,.w_CIFRA,.w_CCCINABI,.w_CODABI,.w_CODCAB,.w_CCCONCOR,.w_FLBEST))
    endwith
  endproc
  proc Calculate_DJOGELCKVN()
    with this
          * --- Controllo il codice CIN
          .w_CHECKCIN = iif(.w_FLBEST # 'S' and islita(),CALCCIN('V',.w_CODABI,.w_CODCAB,.w_CCCONCOR,.w_CCCINABI),' ')
    endwith
  endproc
  proc Calculate_JOFDMNXOKF()
    with this
          * --- Valorizzo i campi relativi al codice IBAN
          .w_CODPA = iif(empty(.w_CODPA),'IT',.w_CODPA)
          .w_IBAN = .w_CCCOBBAN
          .w_CIFRA = .w_CCCINCAB
    endwith
  endproc
  proc Calculate_MQKACRNPAH()
    with this
          * --- Valorizzo il campo codice paese (IBAN)
          .w_CODPA = iif(islita(),.w_PAESE,'  ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCONCOR_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCONCOR_2_2.mCond()
    this.oPgFrm.Page1.oPag.oPASEUR_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPASEUR_2_7.mCond()
    this.oPgFrm.Page1.oPag.oCCCINCAB_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCCCINCAB_2_9.mCond()
    this.oPgFrm.Page1.oPag.oCCCINABI_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCCCINABI_2_10.mCond()
    this.oPgFrm.Page1.oPag.oIBAN_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oIBAN_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEFAULT_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDEFAULT_2_19.mCond()
    this.oPgFrm.Page1.oPag.oCIFRA_2_34.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCIFRA_2_34.mCond()
    this.oPgFrm.Page1.oPag.oCODPA_2_46.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCODPA_2_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_40.enabled =this.oPgFrm.Page1.oPag.oBtn_2_40.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oCCCOBBAN_2_13.visible=!this.oPgFrm.Page1.oPag.oCCCOBBAN_2_13.mHide()
    this.oPgFrm.Page1.oPag.oIBAN_2_14.visible=!this.oPgFrm.Page1.oPag.oIBAN_2_14.mHide()
    this.oPgFrm.Page1.oPag.oCODIBAN_2_17.visible=!this.oPgFrm.Page1.oPag.oCODIBAN_2_17.mHide()
    this.oPgFrm.Page1.oPag.oCIFRA_2_34.visible=!this.oPgFrm.Page1.oPag.oCIFRA_2_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_40.visible=!this.oPgFrm.Page1.oPag.oBtn_2_40.mHide()
    this.oPgFrm.Page1.oPag.oCODPA_2_46.visible=!this.oPgFrm.Page1.oPag.oCODPA_2_46.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_GNBCSRRHIR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Event(cEvent)
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_AIEAWJSJAD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Event(cEvent)
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_JOFDMNXOKF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODBAN
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aba',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CCCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CCCODBAN))
          select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oCCCODBAN_2_1'),i_cWhere,'gsar_aba',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CCCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CCCODBAN)
            select BACODBAN,BACODABI,BACODCAB,BADESBAN,BAPASEUR,BAFLBEST,BADESABI,BADESFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_CODABI = NVL(_Link_.BACODABI,space(5))
      this.w_CODCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
      this.w_PASEURO = NVL(_Link_.BAPASEUR,space(10))
      this.w_FLBEST = NVL(_Link_.BAFLBEST,space(1))
      this.w_DESABI = NVL(_Link_.BADESABI,space(80))
      this.w_DESCAB = NVL(_Link_.BADESFIL,space(40))
      this.w_PAESE = NVL(_Link_.BAPASEUR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODBAN = space(10)
      endif
      this.w_CODABI = space(5)
      this.w_CODCAB = space(5)
      this.w_DESBAN = space(50)
      this.w_PASEURO = space(10)
      this.w_FLBEST = space(1)
      this.w_DESABI = space(80)
      this.w_DESCAB = space(40)
      this.w_PAESE = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.BACODBAN as BACODBAN201"+ ",link_2_1.BACODABI as BACODABI201"+ ",link_2_1.BACODCAB as BACODCAB201"+ ",link_2_1.BADESBAN as BADESBAN201"+ ",link_2_1.BAPASEUR as BAPASEUR201"+ ",link_2_1.BAFLBEST as BAFLBEST201"+ ",link_2_1.BADESABI as BADESABI201"+ ",link_2_1.BADESFIL as BADESFIL201"+ ",link_2_1.BAPASEUR as BAPASEUR201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on BAN_CONTI.CCCODBAN=link_2_1.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and BAN_CONTI.CCCODBAN=link_2_1.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCODVAL
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CCCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CCCODVAL))
          select VACODVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCCCODVAL_2_36'),i_cWhere,'GSAR_AVL',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CCCODVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODVAL = space(3)
      endif
      this.w_SIMVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_36.VACODVAL as VACODVAL236"+ ",link_2_36.VASIMVAL as VASIMVAL236"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_36 on BAN_CONTI.CCCODVAL=link_2_36.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_36"
          i_cKey=i_cKey+'+" and BAN_CONTI.CCCODVAL=link_2_36.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESBAN_2_4.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_2_4.value=this.w_DESBAN
      replace t_DESBAN with this.oPgFrm.Page1.oPag.oDESBAN_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODABI_2_5.value==this.w_CODABI)
      this.oPgFrm.Page1.oPag.oCODABI_2_5.value=this.w_CODABI
      replace t_CODABI with this.oPgFrm.Page1.oPag.oCODABI_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAB_2_6.value==this.w_CODCAB)
      this.oPgFrm.Page1.oPag.oCODCAB_2_6.value=this.w_CODCAB
      replace t_CODCAB with this.oPgFrm.Page1.oPag.oCODCAB_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPASEUR_2_7.value==this.w_PASEUR)
      this.oPgFrm.Page1.oPag.oPASEUR_2_7.value=this.w_PASEUR
      replace t_PASEUR with this.oPgFrm.Page1.oPag.oPASEUR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESBAN_2_8.value==this.w_CCDESBAN)
      this.oPgFrm.Page1.oPag.oCCDESBAN_2_8.value=this.w_CCDESBAN
      replace t_CCDESBAN with this.oPgFrm.Page1.oPag.oCCDESBAN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCINCAB_2_9.value==this.w_CCCINCAB)
      this.oPgFrm.Page1.oPag.oCCCINCAB_2_9.value=this.w_CCCINCAB
      replace t_CCCINCAB with this.oPgFrm.Page1.oPag.oCCCINCAB_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCINABI_2_10.value==this.w_CCCINABI)
      this.oPgFrm.Page1.oPag.oCCCINABI_2_10.value=this.w_CCCINABI
      replace t_CCCINABI with this.oPgFrm.Page1.oPag.oCCCINABI_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCOBBAN_2_13.value==this.w_CCCOBBAN)
      this.oPgFrm.Page1.oPag.oCCCOBBAN_2_13.value=this.w_CCCOBBAN
      replace t_CCCOBBAN with this.oPgFrm.Page1.oPag.oCCCOBBAN_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIBAN_2_14.value==this.w_IBAN)
      this.oPgFrm.Page1.oPag.oIBAN_2_14.value=this.w_IBAN
      replace t_IBAN with this.oPgFrm.Page1.oPag.oIBAN_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIBAN_2_17.value==this.w_CODIBAN)
      this.oPgFrm.Page1.oPag.oCODIBAN_2_17.value=this.w_CODIBAN
      replace t_CODIBAN with this.oPgFrm.Page1.oPag.oCODIBAN_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODBIC_2_18.value==this.w_CCCODBIC)
      this.oPgFrm.Page1.oPag.oCCCODBIC_2_18.value=this.w_CCCODBIC
      replace t_CCCODBIC with this.oPgFrm.Page1.oPag.oCCCODBIC_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNUMTEL_2_25.value==this.w_CCNUMTEL)
      this.oPgFrm.Page1.oPag.oCCNUMTEL_2_25.value=this.w_CCNUMTEL
      replace t_CCNUMTEL with this.oPgFrm.Page1.oPag.oCCNUMTEL_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_EMAIL_2_26.value==this.w_CC_EMAIL)
      this.oPgFrm.Page1.oPag.oCC_EMAIL_2_26.value=this.w_CC_EMAIL
      replace t_CC_EMAIL with this.oPgFrm.Page1.oPag.oCC_EMAIL_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_EMPEC_2_27.value==this.w_CC_EMPEC)
      this.oPgFrm.Page1.oPag.oCC_EMPEC_2_27.value=this.w_CC_EMPEC
      replace t_CC_EMPEC with this.oPgFrm.Page1.oPag.oCC_EMPEC_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRA_2_34.value==this.w_CIFRA)
      this.oPgFrm.Page1.oPag.oCIFRA_2_34.value=this.w_CIFRA
      replace t_CIFRA with this.oPgFrm.Page1.oPag.oCIFRA_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODVAL_2_36.value==this.w_CCCODVAL)
      this.oPgFrm.Page1.oPag.oCCCODVAL_2_36.value=this.w_CCCODVAL
      replace t_CCCODVAL with this.oPgFrm.Page1.oPag.oCCCODVAL_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_2_38.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_2_38.value=this.w_SIMVAL
      replace t_SIMVAL with this.oPgFrm.Page1.oPag.oSIMVAL_2_38.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABI_2_44.value==this.w_DESABI)
      this.oPgFrm.Page1.oPag.oDESABI_2_44.value=this.w_DESABI
      replace t_DESABI with this.oPgFrm.Page1.oPag.oDESABI_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAB_2_45.value==this.w_DESCAB)
      this.oPgFrm.Page1.oPag.oDESCAB_2_45.value=this.w_DESCAB
      replace t_DESCAB with this.oPgFrm.Page1.oPag.oDESCAB_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPA_2_46.value==this.w_CODPA)
      this.oPgFrm.Page1.oPag.oCODPA_2_46.value=this.w_CODPA
      replace t_CODPA with this.oPgFrm.Page1.oPag.oCODPA_2_46.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODBAN_2_1.value==this.w_CCCODBAN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODBAN_2_1.value=this.w_CCCODBAN
      replace t_CCCODBAN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODBAN_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCONCOR_2_2.value==this.w_CCCONCOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCONCOR_2_2.value=this.w_CCCONCOR
      replace t_CCCONCOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCONCOR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.RadioValue()==this.w_DEFAULT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.SetRadio()
      replace t_DEFAULT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.value
    endif
    cp_SetControlsValueExtFlds(this,'BAN_CONTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not((NOT EMPTY(NVL(.w_CCCODBAN,'')) AND NOT EMPTY(NVL(.w_CCCONCOR,''))) OR (EMPTY(NVL(.w_CCCODBAN,'')) AND EMPTY(NVL(.w_CCCONCOR,''))))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Inserire il conto corrente")
      endcase
      if NOT EMPTY(NVL(.w_CCCODBAN,'')) AND NOT EMPTY(NVL(.w_CCCONCOR,''))
        * --- Area Manuale = Check Row
        * --- gsar_mcc
        if .w_FLBEST# 'S' and CHECKIBAN('V',.w_CODPA+.w_CIFRA+.w_IBAN) # 'OK' and inlist(.cFunction,'Edit','Load') and islita() 
           Ah_ErrorMsg('Conto corrente: %1 codice IBAN errato','48','',ALLTRIM(.w_CCCONCOR))
        endif
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANCODBAN = this.w_ANCODBAN
    this.o_CCCODBAN = this.w_CCCODBAN
    this.o_CCCONCOR = this.w_CCCONCOR
    this.o_FLBEST = this.w_FLBEST
    this.o_CODABI = this.w_CODABI
    this.o_CODCAB = this.w_CODCAB
    this.o_CCCINCAB = this.w_CCCINCAB
    this.o_CCCINABI = this.w_CCCINABI
    this.o_CCCOBBAN = this.w_CCCOBBAN
    this.o_CCCOIBAN = this.w_CCCOIBAN
    this.o_CODIBAN = this.w_CODIBAN
    this.o_PASEURO = this.w_PASEURO
    this.o_CIFRA = this.w_CIFRA
    this.o_CODPA = this.w_CODPA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(NVL(t_CCCODBAN,'')) AND NOT EMPTY(NVL(t_CCCONCOR,'')))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_AN__IBAN=space(35)
      .w_AN__BBAN=space(30)
      .w_CCCODBAN=space(10)
      .w_CCCONCOR=space(25)
      .w_FLBEST=space(1)
      .w_DESBAN=space(50)
      .w_CODABI=space(5)
      .w_CODCAB=space(5)
      .w_PASEUR=space(2)
      .w_CCDESBAN=space(50)
      .w_CCCINCAB=space(2)
      .w_CCCINABI=space(1)
      .w_CCCODVAL=space(3)
      .w_CCCOBBAN=space(23)
      .w_IBAN=space(30)
      .w_PAESE=space(2)
      .w_CCCOIBAN=space(34)
      .w_CODIBAN=space(34)
      .w_CCCODBIC=space(11)
      .w_DEFAULT=space(1)
      .w_CCNUMTEL=space(18)
      .w_CC_EMAIL=space(50)
      .w_CC_EMPEC=space(50)
      .w_CCOBSOLE=space(1)
      .w_PASEURO=space(10)
      .w_CIFRA=space(2)
      .w_CCCODVAL=space(3)
      .w_SIMVAL=space(3)
      .w_CHECKCIN=space(1)
      .w_DESABI=space(80)
      .w_DESCAB=space(40)
      .w_CODPA=space(2)
      .DoRTCalc(1,4,.f.)
        .w_AN__IBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' ,this.oParentObject .w_AN__IBAN)
        .w_AN__BBAN = IIF ( lower(this.oParentObject.class)='tgsar_ano' ,'' , this.oParentObject .w_AN__BBAN)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CCCODBAN))
        .link_2_1('Full')
      endif
      .DoRTCalc(8,12,.f.)
        .w_PASEUR = iif(! empty(.w_PASEURO),.w_PASEURO,'IT')
      .DoRTCalc(14,17,.f.)
        .w_CCCOBBAN = iif(.w_FLBEST='S', .w_CCCOBBAN, CALCBBAN(.w_CCCINABI,.w_CCCINCAB,.w_CODABI,.w_CODCAB,SUBSTR(.w_CCCONCOR,1,12),' ',.w_FLBEST))
        .w_IBAN = IIF(.w_FLBEST='S' OR ! ISLITA(), .w_IBAN, .w_CCCOBBAN)
      .DoRTCalc(20,21,.f.)
        .w_CODIBAN = iif(! empty(.w_CCCOIBAN), .w_CCCOIBAN,' ')
      .DoRTCalc(23,23,.f.)
        .w_DEFAULT = iif(iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NOCODBAN, this.oParentObject .w_ANCODBAN)=.w_CCCODBAN AND iif(lower(this.oParentObject.class)='tgsar_ano', this.oParentObject .w_NONUMCOR, this.oParentObject .w_ANNUMCOR)=.w_CCCONCOR AND not empty(nvl(.w_CCCODBAN,'')),'S',' ')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_29.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_30.Calculate()
      .DoRTCalc(25,27,.f.)
        .w_CCOBSOLE = 'N'
      .DoRTCalc(29,29,.f.)
        .w_CIFRA = iif(! empty(.w_CCCINCAB) AND ISLITA(),.w_CCCINCAB,.w_CIFRA)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_35.Calculate()
      .DoRTCalc(31,31,.f.)
      if not(empty(.w_CCCODVAL))
        .link_2_36('Full')
      endif
      .DoRTCalc(32,35,.f.)
        .w_CODPA = iif(islita(),.w_PAESE,'  ')
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_AN__IBAN = t_AN__IBAN
    this.w_AN__BBAN = t_AN__BBAN
    this.w_CCCODBAN = t_CCCODBAN
    this.w_CCCONCOR = t_CCCONCOR
    this.w_FLBEST = t_FLBEST
    this.w_DESBAN = t_DESBAN
    this.w_CODABI = t_CODABI
    this.w_CODCAB = t_CODCAB
    this.w_PASEUR = t_PASEUR
    this.w_CCDESBAN = t_CCDESBAN
    this.w_CCCINCAB = t_CCCINCAB
    this.w_CCCINABI = t_CCCINABI
    this.w_CCCODVAL = t_CCCODVAL
    this.w_CCCOBBAN = t_CCCOBBAN
    this.w_IBAN = t_IBAN
    this.w_PAESE = t_PAESE
    this.w_CCCOIBAN = t_CCCOIBAN
    this.w_CODIBAN = t_CODIBAN
    this.w_CCCODBIC = t_CCCODBIC
    this.w_DEFAULT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.RadioValue(.t.)
    this.w_CCNUMTEL = t_CCNUMTEL
    this.w_CC_EMAIL = t_CC_EMAIL
    this.w_CC_EMPEC = t_CC_EMPEC
    this.w_CCOBSOLE = t_CCOBSOLE
    this.w_PASEURO = t_PASEURO
    this.w_CIFRA = t_CIFRA
    this.w_CCCODVAL = t_CCCODVAL
    this.w_SIMVAL = t_SIMVAL
    this.w_CHECKCIN = t_CHECKCIN
    this.w_DESABI = t_DESABI
    this.w_DESCAB = t_DESCAB
    this.w_CODPA = t_CODPA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_AN__IBAN with this.w_AN__IBAN
    replace t_AN__BBAN with this.w_AN__BBAN
    replace t_CCCODBAN with this.w_CCCODBAN
    replace t_CCCONCOR with this.w_CCCONCOR
    replace t_FLBEST with this.w_FLBEST
    replace t_DESBAN with this.w_DESBAN
    replace t_CODABI with this.w_CODABI
    replace t_CODCAB with this.w_CODCAB
    replace t_PASEUR with this.w_PASEUR
    replace t_CCDESBAN with this.w_CCDESBAN
    replace t_CCCINCAB with this.w_CCCINCAB
    replace t_CCCINABI with this.w_CCCINABI
    replace t_CCCODVAL with this.w_CCCODVAL
    replace t_CCCOBBAN with this.w_CCCOBBAN
    replace t_IBAN with this.w_IBAN
    replace t_PAESE with this.w_PAESE
    replace t_CCCOIBAN with this.w_CCCOIBAN
    replace t_CODIBAN with this.w_CODIBAN
    replace t_CCCODBIC with this.w_CCCODBIC
    replace t_DEFAULT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEFAULT_2_19.ToRadio()
    replace t_CCNUMTEL with this.w_CCNUMTEL
    replace t_CC_EMAIL with this.w_CC_EMAIL
    replace t_CC_EMPEC with this.w_CC_EMPEC
    replace t_CCOBSOLE with this.w_CCOBSOLE
    replace t_PASEURO with this.w_PASEURO
    replace t_CIFRA with this.w_CIFRA
    replace t_CCCODVAL with this.w_CCCODVAL
    replace t_SIMVAL with this.w_SIMVAL
    replace t_CHECKCIN with this.w_CHECKCIN
    replace t_DESABI with this.w_DESABI
    replace t_DESCAB with this.w_DESCAB
    replace t_CODPA with this.w_CODPA
    if i_srv='A'
      replace CCCODBAN with this.w_CCCODBAN
      replace CCCONCOR with this.w_CCCONCOR
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mccPag1 as StdContainer
  Width  = 709
  height = 272
  stdWidth  = 709
  stdheight = 272
  resizeXpos=209
  resizeYpos=136
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=5, width=314,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CCCODBAN",Label1="Codice banca",Field2="CCCONCOR",Label2="Num. conto corrente",Field3="DEFAULT",Label3="Def.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219001466

  add object oStr_1_5 as StdString with uid="LUSDHGVTCC",Visible=.t., Left=436, Top=197,;
    Alignment=0, Width=6, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION" or  ISLESP())
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="WXPSUFXGCR",Visible=.t., Left=473, Top=197,;
    Alignment=0, Width=6, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION" or  ISLESP())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="NBOSENCRYJ",Visible=.t., Left=326, Top=221,;
    Alignment=1, Width=77, Height=18,;
    Caption="Codice BBAN:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_FLBEST = 'S')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="OPOFXFNHOE",Visible=.t., Left=332, Top=5,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="EDXMQYEWBA",Visible=.t., Left=335, Top=167,;
    Alignment=1, Width=68, Height=18,;
    Caption="Codice BIC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="LQHIGEPXHO",Visible=.t., Left=324, Top=140,;
    Alignment=1, Width=79, Height=18,;
    Caption="CIN EUR.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OBOBQIDPJR",Visible=.t., Left=331, Top=32,;
    Alignment=1, Width=72, Height=18,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="NUAOBTXREA",Visible=.t., Left=329, Top=113,;
    Alignment=1, Width=74, Height=18,;
    Caption="Descr. C/C:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="XOSZKHORNM",Visible=.t., Left=325, Top=194,;
    Alignment=1, Width=78, Height=18,;
    Caption="IBAN/Cod. int.:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_FLBEST<>'S' AND !ISLESP())
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="FPKYGTJWFA",Visible=.t., Left=331, Top=194,;
    Alignment=1, Width=72, Height=18,;
    Caption="Codice IBAN:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_FLBEST='S' or  ISLESP())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="SVANSQFVQE",Visible=.t., Left=338, Top=249,;
    Alignment=1, Width=65, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=25,;
    width=312+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=26,width=311+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='BAN_CHE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESBAN_2_4.Refresh()
      this.Parent.oCODABI_2_5.Refresh()
      this.Parent.oCODCAB_2_6.Refresh()
      this.Parent.oPASEUR_2_7.Refresh()
      this.Parent.oCCDESBAN_2_8.Refresh()
      this.Parent.oCCCINCAB_2_9.Refresh()
      this.Parent.oCCCINABI_2_10.Refresh()
      this.Parent.oCCCOBBAN_2_13.Refresh()
      this.Parent.oIBAN_2_14.Refresh()
      this.Parent.oCODIBAN_2_17.Refresh()
      this.Parent.oCCCODBIC_2_18.Refresh()
      this.Parent.oCCNUMTEL_2_25.Refresh()
      this.Parent.oCC_EMAIL_2_26.Refresh()
      this.Parent.oCC_EMPEC_2_27.Refresh()
      this.Parent.oCIFRA_2_34.Refresh()
      this.Parent.oCCCODVAL_2_36.Refresh()
      this.Parent.oSIMVAL_2_38.Refresh()
      this.Parent.oDESABI_2_44.Refresh()
      this.Parent.oDESCAB_2_45.Refresh()
      this.Parent.oCODPA_2_46.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='BAN_CHE'
        oDropInto=this.oBodyCol.oRow.oCCCODBAN_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESBAN_2_4 as StdTrsField with uid="NZWCDUAKBX",rtseq=10,rtrep=.t.,;
    cFormVar="w_DESBAN",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione banca",;
    HelpContextID = 57802186,;
    cTotal="", bFixedPos=.t., cQueryName = "DESBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=409, Top=4, InputMask=replicate('X',50)

  add object oCODABI_2_5 as StdTrsField with uid="WGRAEJUIIR",rtseq=11,rtrep=.t.,;
    cFormVar="w_CODABI",value=space(5),enabled=.f.,;
    ToolTipText = "Codice ABI",;
    HelpContextID = 140764122,;
    cTotal="", bFixedPos=.t., cQueryName = "CODABI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=409, Top=31, InputMask=replicate('X',5)

  add object oCODCAB_2_6 as StdTrsField with uid="IXPVCNZDVO",rtseq=12,rtrep=.t.,;
    cFormVar="w_CODCAB",value=space(5),enabled=.f.,;
    ToolTipText = "Codice CAB",;
    HelpContextID = 259122138,;
    cTotal="", bFixedPos=.t., cQueryName = "CODCAB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=409, Top=58, InputMask=replicate('X',5)

  add object oPASEUR_2_7 as StdTrsField with uid="BMVUDFEBBU",rtseq=13,rtrep=.t.,;
    cFormVar="w_PASEUR",value=space(2),;
    ToolTipText = "Paese banca",;
    HelpContextID = 237961482,;
    cTotal="", bFixedPos=.t., cQueryName = "PASEUR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=409, Top=85, InputMask=replicate('X',2)

  func oPASEUR_2_7.mCond()
    with this.Parent.oContained
      return (.w_FLBEST = 'S')
    endwith
  endfunc

  add object oCCDESBAN_2_8 as StdTrsField with uid="PQNPVAKTTB",rtseq=14,rtrep=.t.,;
    cFormVar="w_CCDESBAN",value=space(50),;
    ToolTipText = "Descrizione conto corrente",;
    HelpContextID = 240119692,;
    cTotal="", bFixedPos=.t., cQueryName = "CCDESBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=409, Top=112, InputMask=replicate('X',50)

  add object oCCCINCAB_2_9 as StdTrsField with uid="ERMLNKNYMM",rtseq=15,rtrep=.t.,;
    cFormVar="w_CCCINCAB",value=space(2),;
    ToolTipText = "Codice CIN europeo",;
    HelpContextID = 228327320,;
    cTotal="", bFixedPos=.t., cQueryName = "CCCINCAB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=409, Top=139, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2)

  func oCCCINCAB_2_9.mCond()
    with this.Parent.oContained
      return (.w_FLBEST<>'S')
    endwith
  endfunc

  add object oCCCINABI_2_10 as StdTrsField with uid="VAAMHGLAUI",rtseq=16,rtrep=.t.,;
    cFormVar="w_CCCINABI",value=space(1),;
    ToolTipText = "Codice CIN italiano",;
    HelpContextID = 6553711,;
    cTotal="", bFixedPos=.t., cQueryName = "CCCINABI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=519, Top=139, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oCCCINABI_2_10.mCond()
    with this.Parent.oContained
      return (.w_FLBEST<>'S')
    endwith
  endfunc

  add object oCCCOBBAN_2_13 as StdTrsField with uid="OVHFASBOWG",rtseq=18,rtrep=.t.,;
    cFormVar="w_CCCOBBAN",value=space(23),enabled=.f.,;
    ToolTipText = "Codice BBAN",;
    HelpContextID = 257294220,;
    cTotal="", bFixedPos=.t., cQueryName = "CCCOBBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=409, Top=220, InputMask=replicate('X',23)

  func oCCCOBBAN_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBEST='S')
    endwith
    endif
  endfunc

  add object oIBAN_2_14 as StdTrsField with uid="FSOHTJKZWR",rtseq=19,rtrep=.t.,;
    cFormVar="w_IBAN",value=space(30),;
    ToolTipText = "Codice BBAN",;
    HelpContextID = 91693178,;
    cTotal="", bFixedPos=.t., cQueryName = "IBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=479, Top=193, InputMask=replicate('X',30)

  func oIBAN_2_14.mCond()
    with this.Parent.oContained
      return (! islita())
    endwith
  endfunc

  func oIBAN_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBEST = 'S' or  ISLESP())
    endwith
    endif
  endfunc

  add object oCODIBAN_2_17 as StdTrsField with uid="BBNYQWCXFV",rtseq=22,rtrep=.t.,;
    cFormVar="w_CODIBAN",value=space(34),;
    ToolTipText = "Codice IBAN",;
    HelpContextID = 6022106,;
    cTotal="", bFixedPos=.t., cQueryName = "CODIBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=409, Top=193, cSayPict=[repl('!',34)], cGetPict=[repl('!',34)], InputMask=replicate('X',34)

  func oCODIBAN_2_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBEST # 'S' AND ! ISLESP())
    endwith
    endif
  endfunc

  add object oCCCODBIC_2_18 as StdTrsField with uid="XJDDBGGTRE",rtseq=23,rtrep=.t.,;
    cFormVar="w_CCCODBIC",value=space(11),;
    ToolTipText = "Codice BIC",;
    HelpContextID = 13238377,;
    cTotal="", bFixedPos=.t., cQueryName = "CCCODBIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=124, Left=409, Top=166, InputMask=replicate('X',11)

  add object oCCNUMTEL_2_25 as StdTrsField with uid="SOOWCOXPYC",rtseq=25,rtrep=.t.,;
    cFormVar="w_CCNUMTEL",value=space(18),;
    ToolTipText = "Numero telefonico",;
    HelpContextID = 56668274,;
    cTotal="", bFixedPos=.t., cQueryName = "CCNUMTEL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=65, Top=219, InputMask=replicate('X',18)

  add object oCC_EMAIL_2_26 as StdTrsField with uid="SOLRZAXIRD",rtseq=26,rtrep=.t.,;
    cFormVar="w_CC_EMAIL",value=space(50),;
    HelpContextID = 5357682,;
    cTotal="", bFixedPos=.t., cQueryName = "CC_EMAIL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=65, Top=245, InputMask=replicate('X',50)

  add object oCC_EMPEC_2_27 as StdTrsField with uid="TCMSSGMXIY",rtseq=27,rtrep=.t.,;
    cFormVar="w_CC_EMPEC",value=space(50),;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 257015913,;
    cTotal="", bFixedPos=.t., cQueryName = "CC_EMPEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=409, Top=245, InputMask=replicate('X',50)

  add object oCIFRA_2_34 as StdTrsField with uid="FYOLQZHDJN",rtseq=30,rtrep=.t.,;
    cFormVar="w_CIFRA",value=space(2),;
    ToolTipText = "Cifra di controllo numerica (2)",;
    HelpContextID = 23251418,;
    cTotal="", bFixedPos=.t., cQueryName = "CIFRA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=444, Top=193, cSayPict=['XX'], cGetPict=['XX'], InputMask=replicate('X',2)

  func oCIFRA_2_34.mCond()
    with this.Parent.oContained
      return (! islita())
    endwith
  endfunc

  func oCIFRA_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION" or .w_FLBEST = 'S' or  ISLESP())
    endwith
    endif
  endfunc

  add object oCCCODVAL_2_36 as StdTrsField with uid="KVTFSGECSF",rtseq=31,rtrep=.t.,;
    cFormVar="w_CCCODVAL",value=space(3),;
    ToolTipText = "Codice valuta conto corrente",;
    HelpContextID = 80347250,;
    cTotal="", bFixedPos=.t., cQueryName = "CCCODVAL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=609, Top=139, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CCCODVAL"

  func oCCCODVAL_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODVAL_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCCCODVAL_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCCCODVAL_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'',this.parent.oContained
  endproc
  proc oCCCODVAL_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CCCODVAL
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_2_38 as StdTrsField with uid="QITUAVBBLN",rtseq=32,rtrep=.t.,;
    cFormVar="w_SIMVAL",value=space(3),enabled=.f.,;
    ToolTipText = "Simbolo valuta",;
    HelpContextID = 90069210,;
    cTotal="", bFixedPos=.t., cQueryName = "SIMVAL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=660, Top=139, InputMask=replicate('X',3)

  add object oBtn_2_40 as StdButton with uid="ZUJGQQFCGW",width=48,height=45,;
   left=654, top=222,;
    CpPicture="bmp\refresh.ico", caption="", nPag=2;
    , ToolTipText = "Se premuto viene ricalcolato il codice CIN\IBAN";
    , HelpContextID = 97089290;
    , caption='\<Ricalcola',ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricalcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_40.mCond()
    with this.Parent.oContained
      return (inlist(.cFunction,'Edit','Load'))
    endwith
  endfunc

  func oBtn_2_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBEST = 'S' or ! islita())
    endwith
   endif
  endfunc

  add object oDESABI_2_44 as StdTrsField with uid="FWJSYBPQMH",rtseq=34,rtrep=.t.,;
    cFormVar="w_DESABI",value=space(80),enabled=.f.,;
    HelpContextID = 140705226,;
    cTotal="", bFixedPos=.t., cQueryName = "DESABI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=461, Top=31, InputMask=replicate('X',80)

  add object oDESCAB_2_45 as StdTrsField with uid="VSSTOKDEZF",rtseq=35,rtrep=.t.,;
    cFormVar="w_DESCAB",value=space(40),enabled=.f.,;
    HelpContextID = 259063242,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=461, Top=58, InputMask=replicate('X',40)

  add object oCODPA_2_46 as StdTrsField with uid="LHROZNPRZM",rtseq=36,rtrep=.t.,;
    cFormVar="w_CODPA",value=space(2),;
    ToolTipText = "Codice paese",;
    HelpContextID = 23389146,;
    cTotal="", bFixedPos=.t., cQueryName = "CODPA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=409, Top=193, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2)

  func oCODPA_2_46.mCond()
    with this.Parent.oContained
      return (.w_PASEUR<>'IT')
    endwith
  endfunc

  func oCODPA_2_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION" or .w_FLBEST = 'S' or  ISLESP())
    endwith
    endif
  endfunc

  add object oStr_2_20 as StdString with uid="UITDGUSCZB",Visible=.t., Left=453, Top=140,;
    Alignment=1, Width=59, Height=18,;
    Caption="CIN Ita.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="CFXTSDDKTI",Visible=.t., Left=329, Top=59,;
    Alignment=1, Width=74, Height=18,;
    Caption="Codice CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="SGGGMOUSOO",Visible=.t., Left=356, Top=86,;
    Alignment=1, Width=47, Height=18,;
    Caption="Paese:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="MJXADGWEQA",Visible=.t., Left=10, Top=219,;
    Alignment=1, Width=53, Height=18,;
    Caption="Num.tel.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="KUJKYLKNRG",Visible=.t., Left=25, Top=245,;
    Alignment=1, Width=38, Height=18,;
    Caption="e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="NTCKHAVWPH",Visible=.t., Left=559, Top=140,;
    Alignment=1, Width=49, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mccBodyRow as CPBodyRowCnt
  Width=302
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCCCODBAN_2_1 as StdTrsField with uid="LZBCCDYWPF",rtseq=7,rtrep=.t.,;
    cFormVar="w_CCCODBAN",value=space(10),isprimarykey=.t.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 255197068,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=91, Left=0, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1), cLinkFile="BAN_CHE", cZoomOnZoom="gsar_aba", oKey_1_1="BACODBAN", oKey_1_2="this.w_CCCODBAN"

  func oCCCODBAN_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODBAN_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCCODBAN_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCCCODBAN_2_1.readonly and this.parent.oCCCODBAN_2_1.isprimarykey)
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oCCCODBAN_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aba',"Elenco banche",'',this.parent.oContained
   endif
  endproc
  proc oCCCODBAN_2_1.mZoomOnZoom
    local i_obj
    i_obj=gsar_aba()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CCCODBAN
    i_obj.ecpSave()
  endproc

  add object oCCCONCOR_2_2 as StdTrsField with uid="CISHZREPRS",rtseq=8,rtrep=.t.,;
    cFormVar="w_CCCONCOR",value=space(25),isprimarykey=.t.,;
    ToolTipText = "Numero conto corrente",;
    HelpContextID = 227934088,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=186, Left=91, Top=-1, InputMask=replicate('X',25), BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1)

  func oCCCONCOR_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(NVL(.w_CCCONCOR,'')))
    endwith
  endfunc

  add object oDEFAULT_2_19 as StdTrsCheck with uid="SZJWPBUHYE",rtrep=.t.,;
    cFormVar="w_DEFAULT",  caption="",;
    ToolTipText = "Conto corrente di default",;
    HelpContextID = 197931574,;
    Left=280, Top=0, Width=17,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDEFAULT_2_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEFAULT,&i_cF..t_DEFAULT),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oDEFAULT_2_19.GetRadio()
    this.Parent.oContained.w_DEFAULT = this.RadioValue()
    return .t.
  endfunc

  func oDEFAULT_2_19.ToRadio()
    this.Parent.oContained.w_DEFAULT=trim(this.Parent.oContained.w_DEFAULT)
    return(;
      iif(this.Parent.oContained.w_DEFAULT=='S',1,;
      0))
  endfunc

  func oDEFAULT_2_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDEFAULT_2_19.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CCCODBAN) AND NOT EMPTY(.w_CCCONCOR))
    endwith
  endfunc

  add object oObj_2_28 as cp_runprogram with uid="VCWCKVUEEI",width=214,height=20,;
   left=-2, top=254,;
    caption='GSAR_BC1',;
   bGlobalFont=.t.,;
    prg="GSAR_BC1('DEF')",;
    cEvent = "w_DEFAULT Changed",;
    nPag=2;
    , HelpContextID = 41742487

  add object oObj_2_29 as cp_runprogram with uid="OAOGJIUHVO",width=214,height=20,;
   left=-2, top=276,;
    caption='GSAR_BC3',;
   bGlobalFont=.t.,;
    prg="GSAR_BC3",;
    cEvent = "Cancella",;
    nPag=2;
    , ToolTipText = "Lanciato dall'area manuale declare variables";
    , HelpContextID = 41742489

  add object oObj_2_30 as cp_runprogram with uid="UFSDKYXZIM",width=214,height=20,;
   left=-2, top=298,;
    caption='GSAR_BC1',;
   bGlobalFont=.t.,;
    prg="GSAR_BC1('BAN')",;
    cEvent = "w_CCCODBAN Changed",;
    nPag=2;
    , HelpContextID = 41742487

  add object oObj_2_35 as cp_runprogram with uid="NJOLNPIQEB",width=214,height=20,;
   left=-2, top=319,;
    caption='GSAR_BC1(CHK)',;
   bGlobalFont=.t.,;
    prg="GSAR_BC1('CHK')",;
    cEvent = "Controlli",;
    nPag=2;
    , HelpContextID = 89962007
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCCCODBAN_2_1.When()
    return(.t.)
  proc oCCCODBAN_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCCCODBAN_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mcc','BAN_CONTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCTIPCON=BAN_CONTI.CCTIPCON";
  +" and "+i_cAliasName2+".CCCODCON=BAN_CONTI.CCCODCON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
