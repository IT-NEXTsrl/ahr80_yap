* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_amh                                                        *
*              Marchi                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_11]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2012-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_amh"))

* --- Class definition
define class tgsar_amh as StdForm
  Top    = 30
  Left   = 30

  * --- Standard Properties
  Width  = 375
  Height = 79+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-12"
  HelpContextID=210335593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  MARCHI_IDX = 0
  cFile = "MARCHI"
  cKeySelect = "MACODICE"
  cKeyWhere  = "MACODICE=this.w_MACODICE"
  cKeyWhereODBC = '"MACODICE="+cp_ToStrODBC(this.w_MACODICE)';

  cKeyWhereODBCqualified = '"MARCHI.MACODICE="+cp_ToStrODBC(this.w_MACODICE)';

  cPrg = "gsar_amh"
  cComment = "Marchi"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AMH'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MACODICE = space(5)
  w_MADESCRI = space(35)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MARCHI','gsar_amh')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_amhPag1","gsar_amh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Marchio")
      .Pages(1).HelpContextID = 56692422
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='MARCHI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MARCHI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MARCHI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MACODICE = NVL(MACODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MARCHI where MACODICE=KeySet.MACODICE
    *
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MARCHI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MARCHI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MARCHI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VALATT = .w_MACODICE
        .w_TABKEY = 'MARCHI'
        .w_DELIMM = .f.
        .w_MACODICE = NVL(MACODICE,space(5))
        .w_MADESCRI = NVL(MADESCRI,space(35))
        cp_LoadRecExtFlds(this,'MARCHI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MACODICE = space(5)
      .w_MADESCRI = space(35)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_VALATT = .w_MACODICE
        .w_TABKEY = 'MARCHI'
        .w_DELIMM = .f.
      endif
    endwith
    cp_BlankRecExtFlds(this,'MARCHI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      .Page1.oPag.oBtn_1_10.enabled = .Page1.oPag.oBtn_1_10.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oMACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMACODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MARCHI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODICE,"MACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADESCRI,"MADESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    i_lTable = "MARCHI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MARCHI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SMH with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MARCHI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MARCHI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MARCHI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MARCHI')
        i_extval=cp_InsertValODBCExtFlds(this,'MARCHI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MACODICE,MADESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MACODICE)+;
                  ","+cp_ToStrODBC(this.w_MADESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MARCHI')
        i_extval=cp_InsertValVFPExtFlds(this,'MARCHI')
        cp_CheckDeletedKey(i_cTable,0,'MACODICE',this.w_MACODICE)
        INSERT INTO (i_cTable);
              (MACODICE,MADESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MACODICE;
                  ,this.w_MADESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MARCHI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MARCHI_IDX,i_nConn)
      *
      * update MARCHI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MARCHI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MADESCRI="+cp_ToStrODBC(this.w_MADESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MARCHI')
        i_cWhere = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
        UPDATE (i_cTable) SET;
              MADESCRI=this.w_MADESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MARCHI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MARCHI_IDX,i_nConn)
      *
      * delete MARCHI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MACODICE',this.w_MACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMACODICE_1_1.value==this.w_MACODICE)
      this.oPgFrm.Page1.oPag.oMACODICE_1_1.value=this.w_MACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMADESCRI_1_2.value==this.w_MADESCRI)
      this.oPgFrm.Page1.oPag.oMADESCRI_1_2.value=this.w_MADESCRI
    endif
    cp_SetControlsValueExtFlds(this,'MARCHI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_amhPag1 as StdContainer
  Width  = 371
  height = 79
  stdWidth  = 371
  stdheight = 79
  resizeXpos=184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMACODICE_1_1 as StdField with uid="OTJPMIDYDS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MACODICE", cQueryName = "MACODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del marchio",;
    HelpContextID = 17432331,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=80, Top=16, InputMask=replicate('X',5)

  add object oMADESCRI_1_2 as StdField with uid="LQGOODKNSX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MADESCRI", cQueryName = "MADESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione marchio",;
    HelpContextID = 200281871,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=80, Top=54, InputMask=replicate('X',35)


  add object oBtn_1_8 as StdButton with uid="UHLKEQDFSQ",left=206, top=6, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 222440998;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp' , caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"MARCHI",.w_MACODICE,"GSAR_AMH","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_MACODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="YYRCTQLCDE",left=258, top=6, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 167538576;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"MARCHI",.w_MACODICE,"GSAR_AMH","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_MACODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="BJRFWTRDLX",left=310, top=6, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 264962342;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp' , caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"MARCHI",.w_MACODICE,"GSAR_AMH","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_MACODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' or g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="XQKSLSNYFU",Visible=.t., Left=1, Top=16,;
    Alignment=1, Width=77, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="LWSFHEDMMS",Visible=.t., Left=1, Top=54,;
    Alignment=1, Width=77, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_amh','MARCHI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MACODICE=MARCHI.MACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
