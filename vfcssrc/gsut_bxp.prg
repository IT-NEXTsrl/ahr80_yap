* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bxp                                                        *
*              Esporta PDF, HTML, RTF                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_216]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-09                                                      *
* Last revis.: 2017-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pFDFFile,pLanguage,pSessionCode,poParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bxp",oParentObject,m.pOper,m.pFDFFile,m.pLanguage,m.pSessionCode,m.poParentObject)
return(i_retval)

define class tgsut_bxp as StdBatch
  * --- Local variables
  w_REPORTCOUNTER = 0
  pOper = 0
  pFDFFile = space(1)
  pLanguage = space(3)
  pSessionCode = space(10)
  poParentObject = .NULL.
  w_RET = 0
  w_NomeRep = space(10)
  w_NomeFile = space(10)
  w_Formato = 0
  w_SCELTA = 0
  w_DIRLAV = space(10)
  w_MESS = space(10)
  w_EXT = space(10)
  w_FDFFile = .f.
  w_DescReport = space(100)
  w_FRONTE = space(1)
  w_MITTEN = space(40)
  w_MAIMIT = space(18)
  w_INVIAA = space(40)
  w_UTEAZI = space(40)
  w_OGGFAX = space(0)
  w_RIFERI = space(40)
  w_FAXMIT = space(18)
  w_SOGFAX = space(0)
  w_FAXA = space(18)
  w_TELMIT = space(18)
  w_NomeFrs = space(10)
  w_WETIPCLF = space(1)
  w_WECODCLF = space(15)
  w_WEDESCSM = space(1)
  w_AZCDESWE = space(1)
  w_AZ_EMLDE = space(75)
  w_AZ_EMLSM = space(75)
  w_AZNOMDSC = space(50)
  w_AZ_DOMWE = space(100)
  w_WE_LOGIN = space(75)
  w_WEAVVEML = space(1)
  w_WEINDEML = space(50)
  w_WETXTEML = space(100)
  w_WEAVVSMS = space(1)
  w_WENUMSMS = space(20)
  w_WETXTSMS = space(100)
  w_WEAVVFAX = space(1)
  w_WENUMFAX = space(20)
  w_WEACCATT = space(1)
  w_MaskRejected = .f.
  w_WEPERMESSI = space(1)
  w_WETITDOC = space(60)
  w_WEDESDOC = space(100)
  w_WENOMALL = space(60)
  w_DESCRITTORE = space(50)
  w_TIPOPUBBL = space(10)
  w_OPERAZIONE = space(10)
  w_VEDIFAX = space(10)
  w_RETVAL = 0
  w_CODAZI = space(5)
  w_VALTIPCON = space(1)
  w_VALCODCON = space(15)
  w_AN_EMAIL = space(50)
  w_AN_EMPEC = space(254)
  w_EXTFORMAT = space(10)
  w_OBJREPORT = .NULL.
  w_ANTELFAX = space(18)
  w_RETVAL = 0
  w_ZCPFITYPE = space(10)
  w_ZCPFIPKEY = space(50)
  w_SFEXTFOR = space(10)
  w_IDMSSA = space(1)
  oReport_Text = .NULL.
  w_ZCPFNAME = space(200)
  w_ZCPFTITLE = space(200)
  w_ZCPFDESCRI = space(0)
  w_ZCPCFOLDER = 0
  w_ZCPDFOLDER = space(50)
  w_ZCPPFOLDER = space(200)
  w_ZCPTFOLDER = space(1)
  w_TIPDES1 = space(1)
  w_TIPDES2 = space(1)
  w_TIPDES3 = space(1)
  w_TIPDES4 = space(1)
  w_TIPDES5 = space(1)
  w_TIPDES6 = space(1)
  w_TIPDES7 = space(1)
  w_TIPDES8 = space(1)
  w_TIPDES9 = space(1)
  w_TIPDES10 = space(1)
  w_TIPCON1 = space(1)
  w_TIPCON2 = space(1)
  w_TIPCON3 = space(1)
  w_TIPCON4 = space(1)
  w_TIPCON5 = space(1)
  w_TIPCON6 = space(1)
  w_TIPCON7 = space(1)
  w_TIPCON8 = space(1)
  w_TIPCON9 = space(1)
  w_TIPCON10 = space(1)
  w_CODAGE1 = space(5)
  w_CODAGE2 = space(5)
  w_CODAGE3 = space(5)
  w_CODAGE4 = space(5)
  w_CODAGE5 = space(5)
  w_CODAGE6 = space(5)
  w_CODAGE7 = space(5)
  w_CODAGE8 = space(5)
  w_CODAGE9 = space(5)
  w_CODAGE10 = space(5)
  w_CODCON1 = space(15)
  w_CODCON2 = space(15)
  w_CODCON3 = space(15)
  w_CODCON4 = space(15)
  w_CODCON5 = space(15)
  w_CODCON6 = space(15)
  w_CODCON7 = space(15)
  w_CODCON8 = space(15)
  w_CODCON9 = space(15)
  w_CODCON10 = space(15)
  w_CODGRU1 = 0
  w_CODGRU2 = 0
  w_CODGRU3 = 0
  w_CODGRU4 = 0
  w_CODGRU5 = 0
  w_CODGRU6 = 0
  w_CODGRU7 = 0
  w_CODGRU8 = 0
  w_CODGRU9 = 0
  w_CODGRU10 = 0
  w_CODROL1 = 0
  w_CODROL2 = 0
  w_CODROL3 = 0
  w_CODROL4 = 0
  w_CODROL5 = 0
  w_CODROL6 = 0
  w_CODROL7 = 0
  w_CODROL8 = 0
  w_CODROL9 = 0
  w_CODROL10 = 0
  w_READ1 = space(1)
  w_READ2 = space(1)
  w_READ3 = space(1)
  w_READ4 = space(1)
  w_READ5 = space(1)
  w_READ6 = space(1)
  w_READ7 = space(1)
  w_READ8 = space(1)
  w_READ9 = space(1)
  w_READ10 = space(1)
  w_WRITE1 = space(1)
  w_WRITE2 = space(1)
  w_WRITE3 = space(1)
  w_WRITE4 = space(1)
  w_WRITE5 = space(1)
  w_WRITE6 = space(1)
  w_WRITE7 = space(1)
  w_WRITE8 = space(1)
  w_WRITE9 = space(1)
  w_WRITE10 = space(1)
  w_DELETE1 = space(1)
  w_DELETE2 = space(1)
  w_DELETE3 = space(1)
  w_DELETE4 = space(1)
  w_DELETE5 = space(1)
  w_DELETE6 = space(1)
  w_DELETE7 = space(1)
  w_DELETE8 = space(1)
  w_DELETE9 = space(1)
  w_DELETE10 = space(1)
  w_RISERVATO = space(1)
  w_DVCODELA = 0
  w_DVDESELA = space(50)
  w_DVDATCRE = ctod("  /  /  ")
  w_DV_PARAM = space(0)
  w_DV__NOTE = space(0)
  w_DVPATHFI = space(100)
  w_DESGRU1 = space(50)
  w_DESGRU2 = space(50)
  w_DESGRU3 = space(50)
  w_DESGRU4 = space(50)
  w_DESGRU5 = space(50)
  w_DESGRU6 = space(50)
  w_DESGRU7 = space(50)
  w_DESGRU8 = space(50)
  w_DESGRU9 = space(50)
  w_DESGRU10 = space(50)
  w_DESCON1 = space(50)
  w_DESCON2 = space(50)
  w_DESCON3 = space(50)
  w_DESCON4 = space(50)
  w_DESCON5 = space(50)
  w_DESCON6 = space(50)
  w_DESCON7 = space(50)
  w_DESCON8 = space(50)
  w_DESCON9 = space(50)
  w_DESCON10 = space(50)
  w_DESAGE1 = space(50)
  w_DESAGE2 = space(50)
  w_DESAGE3 = space(50)
  w_DESAGE4 = space(50)
  w_DESAGE5 = space(50)
  w_DESAGE6 = space(50)
  w_DESAGE7 = space(50)
  w_DESAGE8 = space(50)
  w_DESAGE9 = space(50)
  w_DESAGE10 = space(50)
  w_FOLDCOMP = 0
  w_FOLDCOMU = 0
  w_PATHDOC = space(200)
  w_INCLASSEDOC = space(15)
  w_INARCHIVIO = space(1)
  w_COPATHDS = space(200)
  w_MODALLEG = space(1)
  w_CDCLAINF = space(15)
  w_OPZIONI = space(254)
  w_PagAtt = 0
  w_NFileCom = space(254)
  w_OLDEMAIL = space(254)
  w_OLDEMPEC = space(254)
  w_OLDFAX = space(254)
  w_OLDDEST = space(254)
  w_MOALMAIL = space(1)
  w_OFCODMOD = space(5)
  w_DIALOG = space(1)
  w_STOPFAX = .f.
  w_MVSERIAL = space(10)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_EMAIL = space(10)
  w_EMPEC = space(10)
  w_FAXNO = space(10)
  w_EMAIL_LETTANELCICLOCORRENTE = space(10)
  w_EMPEC_LETTANELCICLOCORRENTE = space(10)
  w_FAXNO_LETTONELCICLOCORRENTE = space(10)
  w_StessaEmailperTuttiIDocumenti = .f.
  w_StessaPECperTuttiIDocumenti = .f.
  w_StessoFaxperTuttiIDocumenti = .f.
  w_StessoIntestatarioperTuttiIDocumenti = .f.
  w_ANDESCRI = space(60)
  w_ANDESCRI_LETTONELCICLOCORRENTE = space(60)
  w_COUNTER = 0
  w_MVSERIAL_CURSOR = space(10)
  w_MVCLADOC = space(2)
  w_MVCODDES = space(5)
  w_NOCODICE = space(15)
  w_MVFLVEAC = space(1)
  w_DDNOMDES = space(40)
  w_NOVERSED = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  WECONTI_idx=0
  UTE_NTI_idx=0
  OUT_PUTS_idx=0
  CONTROPA_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  STMPFILE_idx=0
  DOC_MAST_idx=0
  OFF_NOMI_idx=0
  PROMCLAS_idx=0
  MOD_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esporta nei Formati PDF, HTM, RTF (da CP_CHPRN)
    * --- Nome del Report di Stampa
    * --- Nome del File di Export
    * --- 5=PDF, 6=HTM, 7=RTF (Formato del File da generare)
    if type("g_UEFORM")<>"C"
      cp_SetGlobalVar("g_UEFORM","PDF")
    endif
    * --- Read from STMPFILE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SFEXTFOR"+;
        " from "+i_cTable+" STMPFILE where ";
            +"SFFORMAT = "+cp_ToStrODBC(cp_GetGlobalVar("g_UEFORM"));
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SFEXTFOR;
        from (i_cTable) where;
            SFFORMAT = cp_GetGlobalVar("g_UEFORM");
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SFEXTFOR = NVL(cp_ToDate(_read_.SFEXTFOR),cp_NullValue(_read_.SFEXTFOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Check parametro lingua
    this.pLanguage = IIF(Vartype(this.pLanguage)="C" and Not(Empty(this.pLanguage)), this.pLanguage, g_PROJECTLANGUAGE)
    this.pSessionCode = IIF(Vartype(this.pSessionCode)="C" and Not(Empty(this.pSessionCode)), this.pSessionCode, sys(2015))
    * --- Inizializzazioni    
    this.w_RET = 0
    if this.pOper=6
      * --- Infinity D.M.S. stand alone
      this.w_SCELTA = 5
      this.w_IDMSSA = "S"
    else
      this.w_SCELTA = this.pOper
    endif
    this.w_FDFFile = iif(type("pFDFFile")="C", (this.pFDFFile="S"), .F.)
    * --- Inoltra su: 1 = Disco; 2 = Email, 3 = Fax, 4= NET FOLDER
    if this.w_SCELTA<>0
      * --- non deve chiamare la mcalc() dell' oggetto chiamante (darebbe errore)
      this.bUpdateParentObject=.f.
      WITH this.oParentObject
      * --- Divido report grafico da solo testo, oReport_Text non viene utilizzato per la creazione del file
      this.w_OBJREPORT=createobject("Multireport")
      this.oReport_Text=createobject("Multireport")
      .oSplitReport.SplitTextGraphic(this.oReport_Text, this.w_OBJREPORT)
      this.w_NomeRep = cp_GetStdFile(.cNomeReport.FirstReport(),iif(this.w_FDFFile,"FDF","FRX"))
      this.w_NomeFile = alltrim (.w_Txt_File)
      this.w_Formato = iif(val(.prTyExp)>4, val(.prTyExp), 5)
      * --- Solo se su File
      this.w_DescReport = .cDescReport
      * --- Verifica Estensione Corretta
      this.w_EXT = .oOpt_File.displayvalue
      * --- Estensione del formato
      this.w_EXTFORMAT = .w_Opt_File
      ENDWITH
      * --- Controllo i formati disponibili
      do case
        case this.w_SCELTA=2 OR (this.w_SCELTA=3 And g_TIPFAX="E")
          if this.w_SCELTA=3 And g_TIPFAX="E"
            this.w_Formato = "PDF"
            this.w_EXTFORMAT = this.w_Formato
          else
            * --- Inoltro E-Mail - Seleziona Formato Allegato
            this.w_Formato = IIF(g_UETIAL="A", 1, 0)
            if this.w_Formato=0
              do GSUT_KXP with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_EXTFORMAT = this.w_Formato
            else
              this.w_Formato = IIF(g_UETIAL="A", cp_GetGlobalVar("g_UEFORM") , "PDF")
              this.w_EXTFORMAT = this.w_SFEXTFOR
            endif
          endif
          this.w_EXT = IIF(TYPE("this.w_Formato")="N", cp_GetGlobalVar("g_UEFORM") , this.w_Formato )
          this.w_EXTFORMAT = IIF(TYPE("this.w_Formato")="N", this.w_SFEXTFOR, this.w_EXTFORMAT )
          this.w_Formato = 5
        case this.w_SCELTA=3 And g_TIPFAX<>"E"
          * --- Inoltro FAX - Seleziona Formato Allegato
          if type("g_FRTFAX")="L" or type("g_FRTFAX")="U"
            ah_ErrorMsg("Servizio FAX non configurato","STOP","")
            this.w_Formato = -1
          else
            this.w_Formato = IIF(g_FRTFAX>4, g_FRTFAX, 5)
          endif
          do case
            case this.w_Formato = 7
              this.w_EXTFORMAT = "RTF"
            case this.w_Formato = 6
              this.w_EXTFORMAT = "HTML"
            otherwise
              this.w_EXTFORMAT = "PDF"
          endcase
        case this.w_SCELTA=4
          this.w_WENOMALL = IIF(NOT EMPTY(i_WEALLENAME), i_WEALLENAME, SYS(2015))
          this.w_WETITDOC = IIF(NOT EMPTY(i_WEALLETITLE), i_WEALLETITLE, this.w_DescReport)
          this.w_WEDESDOC = this.w_DescReport
          * --- Rinomina allegato per evitare problemi con spazi
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Inoltro E-Mail per servizio WE - Se necessario richiama finestra per parametri WE
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZCDESWE,AZ_EMLDE,AZ_EMLSM,AZNOMDSC,AZ_DOMWE"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZCDESWE,AZ_EMLDE,AZ_EMLSM,AZNOMDSC,AZ_DOMWE;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AZCDESWE = NVL(cp_ToDate(_read_.AZCDESWE),cp_NullValue(_read_.AZCDESWE))
            this.w_AZ_EMLDE = NVL(cp_ToDate(_read_.AZ_EMLDE),cp_NullValue(_read_.AZ_EMLDE))
            this.w_AZ_EMLSM = NVL(cp_ToDate(_read_.AZ_EMLSM),cp_NullValue(_read_.AZ_EMLSM))
            this.w_AZNOMDSC = NVL(cp_ToDate(_read_.AZNOMDSC),cp_NullValue(_read_.AZNOMDSC))
            this.w_AZ_DOMWE = NVL(cp_ToDate(_read_.AZ_DOMWE),cp_NullValue(_read_.AZ_DOMWE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Da parametri azienda legge se deve richiedere la conferma del destinatario
          this.w_WETIPCLF = "C"
          this.w_WECODCLF = " "
          this.w_WEDESCSM = " "
          this.w_WEPERMESSI = "4"
          this.w_WETITDOC = WEVERTXT(this.w_WETITDOC,.F.)
          this.w_WEDESDOC = WEVERTXT(this.w_WEDESDOC,.F.)
          if not empty(i_WEDEST)
            this.w_WETIPCLF = LEFT(i_WEDEST,1)
            this.w_WECODCLF = SUBSTR(i_WEDEST,2)
            * --- Legge i dati per proporli sulla maschera (se richiesto
            * --- Read from WECONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.WECONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2],.t.,this.WECONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "WEDESCSM,WE_LOGIN,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEACCATT"+;
                " from "+i_cTable+" WECONTI where ";
                    +"WETIPCON = "+cp_ToStrODBC(this.w_WETIPCLF);
                    +" and WECODICE = "+cp_ToStrODBC(this.w_WECODCLF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                WEDESCSM,WE_LOGIN,WEAVVEML,WEINDEML,WETXTEML,WEAVVSMS,WENUMSMS,WETXTSMS,WEAVVFAX,WENUMFAX,WEACCATT;
                from (i_cTable) where;
                    WETIPCON = this.w_WETIPCLF;
                    and WECODICE = this.w_WECODCLF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_WEDESCSM = NVL(cp_ToDate(_read_.WEDESCSM),cp_NullValue(_read_.WEDESCSM))
              this.w_WE_LOGIN = NVL(cp_ToDate(_read_.WE_LOGIN),cp_NullValue(_read_.WE_LOGIN))
              this.w_WEAVVEML = NVL(cp_ToDate(_read_.WEAVVEML),cp_NullValue(_read_.WEAVVEML))
              this.w_WEINDEML = NVL(cp_ToDate(_read_.WEINDEML),cp_NullValue(_read_.WEINDEML))
              this.w_WETXTEML = NVL(cp_ToDate(_read_.WETXTEML),cp_NullValue(_read_.WETXTEML))
              this.w_WEAVVSMS = NVL(cp_ToDate(_read_.WEAVVSMS),cp_NullValue(_read_.WEAVVSMS))
              this.w_WENUMSMS = NVL(cp_ToDate(_read_.WENUMSMS),cp_NullValue(_read_.WENUMSMS))
              this.w_WETXTSMS = NVL(cp_ToDate(_read_.WETXTSMS),cp_NullValue(_read_.WETXTSMS))
              this.w_WEAVVFAX = NVL(cp_ToDate(_read_.WEAVVFAX),cp_NullValue(_read_.WEAVVFAX))
              this.w_WENUMFAX = NVL(cp_ToDate(_read_.WENUMFAX),cp_NullValue(_read_.WENUMFAX))
              this.w_WEACCATT = NVL(cp_ToDate(_read_.WEACCATT),cp_NullValue(_read_.WEACCATT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows=0 or this.w_WEACCATT<>"S"
              if this.w_WETIPCLF="F"
                ah_ErrorMsg("Account non attivo o non specificato per il fornitore %1%0Impossibile continuare","!","", this.w_WECODCLF)
              else
                ah_ErrorMsg("Account non attivo o non specificato per il cliente %1%0Impossibile continuare","!","", this.w_WECODCLF)
              endif
              i_retcode = 'stop'
              return
            endif
          endif
          * --- Se il destinatario non � noto oppure se l'ho richiesto a livello di parametri lancio finestra
          if empty(this.w_WECODCLF) or this.w_AZCDESWE="S"
            * --- Lancia Maschera
            this.w_MaskRejected = .T.
            do GSUT1KXP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_MaskRejected
              ah_ErrorMsg("Operazione sospesa come richiesto","i","")
              i_retcode = 'stop'
              return
            endif
          endif
          * --- Se � stato individuato il destinatario allora determina modalit� di inoltro
          if empty(this.w_WECODCLF)
            ah_ErrorMsg("Non � stato individuato il destinatario%0Impossibile continuare","!","")
            i_retcode = 'stop'
            return
          endif
          * --- Controlla e definisce i_EMAIL in base alla modalit� di inoltro.
          i_EMAIL = " "
          i_EMAIL_PEC = " "
          * --- Controlla se definito indirizzo e-mail
          if this.w_WEDESCSM="S" 
            if not empty(this.w_AZ_EMLSM)
              i_EMAIL = STRTRAN(this.w_AZ_EMLSM,"%",alltrim(this.w_WE_LOGIN))
            else
              ah_ErrorMsg("Modalit� di inoltro simple mail%0Non � stato indicato l'indirizzo E-mail di inoltro (parametri WE)","!","")
              i_retcode = 'stop'
              return
            endif
          endif
          * --- Controlla se definito indirizzo e-mail
          if this.w_WEDESCSM="D" 
            if not empty(this.w_AZ_EMLDE)
              i_EMAIL = alltrim(this.w_AZ_EMLDE)
            else
              ah_ErrorMsg("Modalit� di inoltro con descrittore%0Non � stato indicato l'indirizzo E-mail di inoltro (parametri WE)","!","")
              i_retcode = 'stop'
              return
            endif
            if empty(this.w_AZNOMDSC)
              ah_ErrorMsg("Modalit� di inoltro con descrittore%0Non � stato indicato il nome del descrittore (parametri WE)","!","")
              i_retcode = 'stop'
              return
            endif
            if empty(this.w_AZ_DOMWE)
              ah_ErrorMsg("Modalit� di inoltro con descrittore%0Non � stato indicato il nome del dominio (parametri WE)","!","")
              i_retcode = 'stop'
              return
            endif
          endif
        case this.w_SCELTA=5
          * --- Fissa Estensione Corretta
          this.w_EXT = "PDF"
          * --- La variabile 'w_EXT' � stata mantenuta poich� usata in GSUT_BFA e GSUT_BPI
          this.w_EXTFORMAT = "PDF"
          * --- Pulizia della variabile w_DescReport
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Controllo abilitazione modulo e chiamata routine GSCP_BFA
          this.w_RETVAL = -1
          if g_IZCP$"SA" AND g_CPIN="N" AND g_DMIP="N" AND this.w_IDMSSA<>"S"
            * --- CPZ
            this.w_RETVAL = GSCP_BFA(this,1)
          else
            * --- INFINITY
            this.w_RETVAL = GSUT_BPI(this,1)
          endif
          if this.w_RETVAL=0
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDMODALL,CDCLAINF"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_INCLASSEDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDMODALL,CDCLAINF;
                from (i_cTable) where;
                    CDCODCLA = this.w_INCLASSEDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
              this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_IDMSSA="S" AND this.w_MODALLEG<>"S" AND NOT EMPTY(this.w_INCLASSEDOC)
                ah_ErrorMsg("La modalit� della classe documentale deve essere <Infinity D.M.S. stand alone>.","!","")
                this.w_RETVAL = -1
              case g_REVI="S" AND EMPTY(this.w_CDCLAINF)
                ah_ErrorMsg("La classe documentale selezionata [%1] non � associata a nessuna classe Infinity.",48,"",alltrim(this.w_INCLASSEDOC))
                this.w_RETVAL = -1
            endcase
          endif
          if this.w_RETVAL=-1
            i_retcode = 'stop'
            return
          endif
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO,COPATHDS"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO,COPATHDS;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PATHDOC = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_MODALLEG="S"
            this.w_PATHDOC = this.w_COPATHDS
            if empty(this.w_PATHDOC)
              ah_ErrorMsg("Inserire il path xml descrittore nell'apposita gestione","!","")
              this.w_RETVAL = -1
            endif
          else
            if (g_IZCP$"SA" or g_CPIN="S" or g_DMIP="S") and empty(this.w_PATHDOC)
              ah_ErrorMsg("Inserire il path documenti nell'apposita gestione","!","")
              this.w_RETVAL = -1
            endif
          endif
          if this.w_RETVAL=-1
            i_retcode = 'stop'
            return
          endif
      endcase
      * --- Se formato > 4 significa RTF o HTM o PDF
      if this.w_Formato>4
        * --- Determina nome dell'ALLEGATO (w_NOMEFILE) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        do case
          case this.w_SCELTA=1
            this.w_NomeFile = FORCEEXT(this.w_NomeFile, this.w_EXTFORMAT)
          case this.w_SCELTA=2 or this.w_SCELTA=3
            if this.w_FDFFile and this.w_SCELTA=2
              * --- Solo per EMAIL
              this.w_NomeFile = this.w_NomeRep+".FDF"
            else
              * --- Se � vuota la descrizione del report (presa dall'output utente) allora usa il nome file
              *     indicato sulla PRINT SYSTEM
              if not empty(this.w_DescReport)
                * --- Prende il nome del report dall'output utente (elimina caratteri non permessi)
                this.w_NomeFile = WEVERTXT(alltrim(Justfname(this.w_DescReport)),.F.,"A")
              endif
              * --- Forzo l'estensione
              this.w_NomeFile = FORCEEXT(this.w_NomeFile, this.w_EXTFORMAT)
            endif
          case this.w_SCELTA=4
            if this.w_FDFFile
              this.w_NomeFile = FORCEEXT(this.w_NomeRep,".FDF")
            else
              this.w_NomeFile = FORCEEXT(this.w_WENOMALL, this.w_EXTFORMAT)
            endif
          case this.w_SCELTA=5
            if g_IZCP$"SA" AND g_CPIN="N" AND g_DMIP="N" AND this.w_IDMSSA<>"S"
              * --- CPZ
              this.w_RETVAL = GSCP_BFA(this,2)
            else
              * --- INFINITY
              this.w_RETVAL = GSUT_BPI(this,2)
            endif
            if this.w_RETVAL=-1
              i_retcode = 'stop'
              return
            endif
        endcase
        * --- Nel caso di PDF/A, dal nome file elimino i caratteri '/A'
        this.w_NomeFile = strtran(this.w_NomeFile,"/A","")
        * --- Aggiunge a w_NOMEFILE la directory di lavoro w_DIRLAV
        do case
          case this.w_SCELTA=1
            this.w_DIRLAV = tempadhoc()
          case this.w_SCELTA=5
            if g_IZCP$"SA" AND g_CPIN="N" AND g_DMIP="N" AND this.w_IDMSSA<>"S"
              * --- CPZ
              this.w_RETVAL = GSCP_BFA(this,3)
            else
              * --- INFINITY
              this.w_RETVAL = GSUT_BPI(this,3)
            endif
            if this.w_RETVAL=-1
              i_retcode = 'stop'
              return
            endif
          otherwise
            this.w_DIRLAV = tempadhoc()
        endcase
        if AT( ":",this.w_NomeFile)=0 and AT( "\\",this.w_NomeFile)=0
          this.w_NomeFile = ADDBS(this.w_DIRLAV) + this.w_NomeFile
        endif
        * --- Creazione fisica ALLEGATO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if not this.w_FDFFile and (this.w_SCELTA=1 OR this.w_SCELTA=2 OR (this.w_SCELTA=3 AND g_TIPFAX$"ME" ) OR this.w_SCELTA=4 OR this.w_SCELTA=5)
          * --- Genera il File Allegato (tranne se FAX su Stampante Virtuale)
          * --- Export su File o E-MAIL o FAX Servizio MAPI
          * --- Chiama la Funzione di Generazione dei File secondo il Formato 
          L_PagGen = -1
          this.w_OPZIONI = this.oParentObject.cOption
          RET = Print_To_File(this.w_NomeFile, this.w_OBJREPORT, this.w_EXT, this.w_OPZIONI, @L_PagGen)
          if not RET
            ah_ErrorMsg("Errore durante la generazione del file","stop","")
            i_retcode = 'stop'
            return
          endif
          if L_PagGen>0
            * --- La generazione di immagini ha creato file multipli
            this.w_NFileCom = JUSTPATH(this.w_NomeFile) + "\" + JUSTSTEM(this.w_NomeFile)
            this.w_NomeFile = ""
            this.w_PagAtt = 1
            do while this.w_PagAtt<=L_PagGen
              this.w_NomeFile = this.w_NomeFile + this.w_NFileCom + "_Pag_" + ALLTRIM(STR(this.w_PagAtt)) + "." + this.w_EXTFORMAT +"; "
              this.w_PagAtt = this.w_PagAtt + 1
            enddo
            this.w_NomeFile = LEFT(this.w_NomeFile, LEN(this.w_NomeFile)-2)
          endif
        endif
        * --- Invio Allegato (Mail, Fax, Net-Folder) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Controllo se l'utente ha premuto il bottone E-mail oppure il bottone con servizio
        *     mailToFax
        do case
          case this.w_SCELTA=2 OR (this.w_SCELTA=3 And g_TIPFAX="E")
            * --- Inoltro E-Mail - Controllo se il servizio MAPI e' attivato, oppure 
            *     se attivo invio attraverso supporto SMTP
            if oCPMapi.mapiService=1 or g_INVIO$"SP" or (this.w_SCELTA=3 And g_TIPFAX="E")
              * Variabili publiche per mail 
 i_EMAILSUBJECT = IIF( EMPTY( i_EMAILSUBJECT ), iif(this.w_SCELTA=2, Ah_MsgFormat("e-mail da %1 %2", alltrim(i_msgtitle), g_VERSION), Ah_MsgFormat("fax da %1 %2", alltrim(i_msgtitle), g_VERSION)), i_EMAILSUBJECT ) 
 i_EMAILTEXT = iif(this.w_SCELTA=2,SPACE(1)+IIF(EMPTY(g_UEFIRM), "", CHR(13)+CHR(10)+g_UEFIRM),"") 
              if type("this.oParentObject")="O" AND type("this.oparentobject.noParentObject")="O" and type("this.oparentobject.noParentObject.class")="C" and upper(this.oparentobject.noParentObject.class)="TGSTE_BZ2" 
                if this.oparentobject.noParentObject.w_NUMCLI=1
                  do case
                    case UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
                      this.w_VALTIPCON = this.oparentobject.noParentObject.w_COTIPCON
                      this.w_VALCODCON = this.oparentobject.noParentObject.w_COCODCON
                      * --- Caso particolare, legge l'indirizzo di sollecito dalle destinazioni diverse
                      * --- Read from DES_DIVE
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "DD_EMAIL,DD_EMPEC,DDTELFAX"+;
                          " from "+i_cTable+" DES_DIVE where ";
                              +"DDTIPCON = "+cp_ToStrODBC(this.w_VALTIPCON);
                              +" and DDCODICE = "+cp_ToStrODBC(this.w_VALCODCON);
                              +" and DDTIPRIF = "+cp_ToStrODBC("SO");
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          DD_EMAIL,DD_EMPEC,DDTELFAX;
                          from (i_cTable) where;
                              DDTIPCON = this.w_VALTIPCON;
                              and DDCODICE = this.w_VALCODCON;
                              and DDTIPRIF = "SO";
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_AN_EMAIL = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
                        this.w_AN_EMPEC = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
                        this.w_ANTELFAX = NVL(cp_ToDate(_read_.DDTELFAX),cp_NullValue(_read_.DDTELFAX))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if (empty(nvl(this.w_AN_EMAIL,"")) And this.w_SCELTA=2) or (empty(nvl(this.w_ANTELFAX,"")) And this.w_SCELTA=3) 
                        * --- Mi riporto al caso stadard, legge l'indirizzo email dall'anagrafica CONTI
                        * --- Read from CONTI
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.CONTI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "AN_EMAIL,ANTELFAX"+;
                            " from "+i_cTable+" CONTI where ";
                                +"ANTIPCON = "+cp_ToStrODBC(this.w_VALTIPCON);
                                +" and ANCODICE = "+cp_ToStrODBC(this.w_VALCODCON);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            AN_EMAIL,ANTELFAX;
                            from (i_cTable) where;
                                ANTIPCON = this.w_VALTIPCON;
                                and ANCODICE = this.w_VALCODCON;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
                          this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                      endif
                      if (empty(nvl(this.w_AN_EMPEC,"")) And this.w_SCELTA=2)
                        * --- Mi riporto al caso stadard, legge l'indirizzo PEC dall'anagrafica CONTI
                        * --- Read from CONTI
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.CONTI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "AN_EMPEC"+;
                            " from "+i_cTable+" CONTI where ";
                                +"ANTIPCON = "+cp_ToStrODBC(this.w_VALTIPCON);
                                +" and ANCODICE = "+cp_ToStrODBC(this.w_VALCODCON);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            AN_EMPEC;
                            from (i_cTable) where;
                                ANTIPCON = this.w_VALTIPCON;
                                and ANCODICE = this.w_VALCODCON;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                      endif
                    otherwise
                      * --- Caso stadard, legge l'indirizzo email dall'anagrafica CONTI
                      * --- Read from CONTI
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.CONTI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "AN_EMAIL,AN_EMPEC,ANTELFAX"+;
                          " from "+i_cTable+" CONTI where ";
                              +"ANTIPCON = "+cp_ToStrODBC(this.w_VALTIPCON);
                              +" and ANCODICE = "+cp_ToStrODBC(this.w_VALCODCON);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          AN_EMAIL,AN_EMPEC,ANTELFAX;
                          from (i_cTable) where;
                              ANTIPCON = this.w_VALTIPCON;
                              and ANCODICE = this.w_VALCODCON;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
                        this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
                        this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                  endcase
                  i_EMAIL = this.w_AN_EMAIL
                  i_EMAIL_PEC = this.w_AN_EMPEC
                  i_FAXNO = this.w_ANTELFAX
                else
                  AH_ERRORMSG("Attenzione: il documento inserito come allegato contiene informazioni riferite a pi� clienti",48)
                endif
              endif
              if this.w_SCELTA=2
                if VARTYPE(i_OFALLENAME)="C" AND !EMPTY(i_OFALLENAME)
                  if G_OFFE="S" AND (UPPER(THIS.OPARENTOBJECT.NOPARENTOBJECT.CLASS)="TGSOF_AOF" or UPPER(THIS.OPARENTOBJECT.NOPARENTOBJECT.CLASS)="TGSOF_KRO" or UPPER(THIS.OPARENTOBJECT.NOPARENTOBJECT.CLASS)="TGSOF_BGN")
                    if UPPER(THIS.OPARENTOBJECT.NOPARENTOBJECT.CLASS)="TGSOF_BGN"
                      this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_CODMOD
                    else
                      this.w_OFCODMOD = THIS.OPARENTOBJECT.NOPARENTOBJECT.w_OFCODMOD
                    endif
                    * --- Read from MOD_OFFE
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "MOALMAIL"+;
                        " from "+i_cTable+" MOD_OFFE where ";
                            +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        MOALMAIL;
                        from (i_cTable) where;
                            MOCODICE = this.w_OFCODMOD;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    do case
                      case this.w_MOALMAIL="M"
                        this.w_NomeFile = ALLTRIM(i_OFALLENAME)
                      case this.w_MOALMAIL $ "E-S"
                        if Not Empty(ALLTRIM(i_OFALLENAME))
                          this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                        else
                          this.w_NomeFile = ALLTRIM(this.w_NomeFile)
                        endif
                    endcase
                  else
                    this.w_NomeFile = ALLTRIM(this.w_NomeFile)+"; "+ALLTRIM(i_OFALLENAME)
                  endif
                endif
                if EMPTY( i_EMAIL ) AND TYPE("this.oparentobject.noparentobject.oparentobject.w_EMAIL")="C"
                  i_EMAIL = this.oparentobject.noparentobject.oparentobject.w_EMAIL
                endif
                if EMPTY( i_EMAIL_PEC ) AND TYPE("this.oparentobject.noparentobject.oparentobject.w_EMPEC")="C"
                  i_EMAIL_PEC = this.oparentobject.noparentobject.oparentobject.w_EMPEC
                endif
                if EMPTY( i_EMAIL ) AND TYPE("this.oparentobject.noparentobject.w_EMAIL")="C"
                  i_EMAIL = this.oparentobject.noparentobject.w_EMAIL
                endif
                if EMPTY( i_EMAIL_PEC ) AND TYPE("this.oparentobject.noparentobject.w_EMPEC")="C"
                  i_EMAIL_PEC = this.oparentobject.noparentobject.w_EMPEC
                endif
                this.w_OLDEMAIL = I_EMAIL
                this.w_OLDEMPEC = i_EMAIL_PEC
                this.w_OLDFAX = i_FAXNO
                this.w_OLDDEST = I_DEST
                * --- Se non � specificato un intestatario, i documenti da stampare potrebbero comunque
                *     avere tutti lo stesso destinatario, e quindi si controlla se risulta lo stesso indirizzo email per tutti
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                i_EMAIL = this.w_EMAIL
                i_EMAIL_PEC = this.w_EMPEC
                i_FAXNO = this.w_FAXNO
                i_DEST = this.w_OLDDEST
                if EMPTY(i_EMAIL)
                  i_EMAIL = this.w_OLDEMAIL
                endif
                if EMPTY(i_EMAIL_PEC)
                  i_EMAIL_PEC = this.w_OLDEMPEC
                endif
                if EMPTY( i_FAXNO)
                  i_FAXNO = this.w_OLDFAX
                endif
                if EMPTY( i_DEST)
                  i_DEST = this.w_OLDDEST
                endif
                * --- Se ha trovato un solo indirizzo, lo utilizza
                RET = EMAIL(this.w_NomeFile, g_UEDIAL)
              else
                * --- leggo il valore della variabile e lo metto in w_VEDIFAX
                * --- Read from UTE_NTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.UTE_NTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "UTDIAFAX,UTFROFAX"+;
                    " from "+i_cTable+" UTE_NTI where ";
                        +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    UTDIAFAX,UTFROFAX;
                    from (i_cTable) where;
                        UTCODICE = i_CODUTE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VEDIFAX = NVL(cp_ToDate(_read_.UTDIAFAX),cp_NullValue(_read_.UTDIAFAX))
                  this.w_FRONTE = NVL(cp_ToDate(_read_.UTFROFAX),cp_NullValue(_read_.UTFROFAX))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if (alltrim(this.w_VEDIFAX) = "S") or (alltrim(this.w_VEDIFAX) = "A" and empty(i_FAXNO) )
                  this.w_DIALOG = "S"
                else
                  this.w_DIALOG = "N"
                endif
                * --- Se l'utente ha selezionato il servizio MailToFax apro la stessa maschera
                *     dell'invio E-Mail
                this.w_RET = EMAIL(this.w_NomeFile, this.w_DIALOG, .F., i_FAXNO, i_FAXSUBJECT, "", .T., IIF(empty(this.w_FRONTE),"N", this.w_FRONTE)="S")
              endif
            else
              ah_ErrorMsg("Sevizio MAPI non disponibile","STOP","")
            endif
          case this.w_SCELTA=3
            this.w_OLDFAX = i_FAXNO
            this.w_OLDDEST = I_DEST
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_FAXNO = this.w_FAXNO
            if EMPTY( i_FAXNO)
              i_FAXNO = this.w_OLDFAX
            endif
            if EMPTY( I_DEST)
              i_DEST =this.w_OLDDEST
            endif
            * --- Inoltro FAX
            do case
              case g_TIPFAX = "M"
                * --- Servizio MAPI
                this.w_FRONTE = " "
                this.w_MITTEN = g_RAGAZI
                * --- Si usa sempre l'indirizzo email dell'azienda mittente, inserito in 'Servizi Fax email ...'
                this.w_MAIMIT = g_MITTEN
                this.w_NomeFrs = ""
                this.w_UTEAZI = g_DESUTE
                this.w_OGGFAX = " "
                this.w_RIFERI = " "
                this.w_FAXMIT = g_TELFAX
                this.w_SOGFAX = " "
                this.w_TELMIT = g_TELEFO
                this.w_FAXA = i_FAXNO
                this.w_INVIAA = i_DEST
                * --- leggo il valore della variabile e lo metto in w_VEDIFAX
                * --- Read from UTE_NTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.UTE_NTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "UTDIAFAX,UTFROFAX"+;
                    " from "+i_cTable+" UTE_NTI where ";
                        +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    UTDIAFAX,UTFROFAX;
                    from (i_cTable) where;
                        UTCODICE = i_CODUTE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VEDIFAX = NVL(cp_ToDate(_read_.UTDIAFAX),cp_NullValue(_read_.UTDIAFAX))
                  this.w_FRONTE = NVL(cp_ToDate(_read_.UTFROFAX),cp_NullValue(_read_.UTFROFAX))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_FRONTE = IIF(empty(this.w_FRONTE),"N",this.w_FRONTE)
                this.w_STOPFAX = .F.
                if (alltrim(this.w_VEDIFAX) = "S") or (alltrim(this.w_VEDIFAX) = "A" and empty(i_FAXNO) )
                  * --- Maschera dati FAX
                  this.w_STOPFAX = .T.
                  do GSUT_KAX with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                if EMPTY(this.w_INVIAA) OR EMPTY(this.w_FAXA) OR this.w_STOPFAX
                  if this.w_STOPFAX
                    this.w_MESS = "Invio fax sospeso come richiesto"
                  else
                    this.w_MESS = "Impossibile inviare fax, numero e/o destinatario non specificati"
                  endif
                  ah_ErrorMsg(this.w_MESS,48)
                else
                  if this.w_FRONTE="S"
                    * --- Prepara Frontespizio
                    CREATE CURSOR TMP_FRS (FSINVIAA C(40), FSRIFERI C(40), FS__NUMA C(18), ; 
 FSSOGGET M(10), FSOGGETT M(10), FS__DATA D(8), FSRAGSDA C(40), FSUTENTE C(40), ; 
 FS_NUMD C(18), FS_EMAIL C(50), FSTELEFO C(18), FS__TIME C(10))
                    INSERT INTO TMP_FRS (FSINVIAA, FSRIFERI, FS__NUMA, ; 
 FSSOGGET, FSOGGETT, FS__DATA, FSRAGSDA, FSUTENTE, ; 
 FS_NUMD, FS_EMAIL, FSTELEFO, FS__TIME) ; 
 VALUES (this.w_INVIAA, this.w_RIFERI, this.w_FAXA, ; 
 this.w_SOGFAX, this.w_OGGFAX, DATE(), this.w_MITTEN, this.w_UTEAZI, ; 
 this.w_FAXMIT, this.w_MAIMIT, this.w_TELMIT, TIME())
                    this.w_NomeFrs = this.w_DIRLAV + "\FRS." + this.w_EXTFORMAT
                    * --- Chiama la Funzione di Generazione dei File secondo il Formato (A.M. Function & Procedures)
                    do case
                      case this.w_Formato=5
                        RET = PDF_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
                      case this.w_Formato=6
                        RET = HTM_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
                      case this.w_Formato=7
                        RET = RTF_Format(this.w_NomeFrs, "QUERY\GSUT3FRS.FRX")
                    endcase
                    if Used( "TMP_FRS" )
                      SELECT TMP_FRS
                      USE
                    endif
                    if RET=.F.
                      ah_ErrorMsg("Errore in generazione file frontespizio","STOP","")
                      i_retcode = 'stop'
                      return
                    endif
                  endif
                  * --- Inoltro
                  RET = FAX_MAPI(this.w_NomeFile, this.w_NomeFrs)
                endif
              case g_TIPFAX = "S"
                * --- Stampante Virtuale
                RET = FAX_STA(this.w_OBJREPORT, "FRX")
              case g_TIPFAX="R"
                * --- Rendering Subsystem
                i_FAXNO = " "
                i_DEST = " "
                RET = FAX_RSS(this.w_OBJREPORT, "FRX")
              case .T.
                ah_ErrorMsg("Servizio FAX non attivato","STOP","")
            endcase
          case this.w_SCELTA=4
            * --- Inoltro E-Mail su NET-FOLDER
            *     Se MAPI non attivo e non ho invio attraverso supporto SMTP
            if oCPMapi.mapiService<>1 And g_INVIO="M"
              ah_ErrorMsg("Sevizio MAPI non disponibile","STOP","")
            else
              * Variabili publiche per mail 
 i_EMAILSUBJECT = IIF ( EMPTY(i_EMAILSUBJECT), Ah_MsgFormat("%1: utilizzo servizi NET-FOLDER", alltrim(i_msgtitle)), i_EMAILSUBJECT+Ah_MsgFormat("%1utilizzo servizi NET-FOLDER",": ")) 
 i_EMAILTEXT = Ah_MsgFormat("Vedere Allegati")
              do case
                case this.w_WEDESCSM="S"
                  * --- Se la modalit� � SIMPLE MAIL si limita ad inviare l'allegato gi� pronto
                  RET = EMAIL(this.w_NomeFile, g_UEDIAL, .T.)
                case this.w_WEDESCSM="D"
                  * --- Se la modalit� prevede il descrittore, occorre prepararlo
                  this.w_DESCRITTORE = tempadhoc()+"\"+alltrim(this.w_AZNOMDSC)
                  Dimension L_ARRAYDOC(1,10)
                  L_ARRAYDOC(1,1) = alltrim(this.w_WE_LOGIN)
                  L_ARRAYDOC(1,2) = alltrim(substr(this.w_NOMEFILE, rat("\",this.w_NOMEFILE)+1 ) )
                  L_ARRAYDOC(1,3) = alltrim(this.w_WETITDOC)
                  L_ARRAYDOC(1,4) = alltrim(this.w_WEPERMESSI)
                  L_ARRAYDOC(1,5) = alltrim(this.w_WEDESDOC)
                  L_ARRAYDOC(1,6) = IIF(this.w_WEAVVEML="S","SI","NO")
                  L_ARRAYDOC(1,7) = rtrim(this.w_WETXTEML)
                  L_ARRAYDOC(1,8) = IIF(this.w_WEAVVSMS="S","SI","NO")
                  L_ARRAYDOC(1,9) = rtrim(this.w_WETXTSMS)
                  L_ARRAYDOC(1,10) = IIF(this.w_WEAVVFAX="S","SI","NO")
                  * --- Lancia programma di creazione descrittore XML
                  this.w_TIPOPUBBL = "NETFOLDER"
                  this.w_OPERAZIONE = "INSERT"
                  this.w_RET = WEDESXML(this.w_DESCRITTORE,this.w_AZ_DOMWE,this.w_TIPOPUBBL,this.w_OPERAZIONE, @L_ARRAYDOC)
                  if this.w_RET
                    * --- Invia e-mail
                    dimension L_AllegatiWE(2)
                    L_AllegatiWE(1) = this.w_DESCRITTORE
                    L_AllegatiWE(2) = this.w_NOMEFILE
                    this.w_RET = EMAIL( @L_AllegatiWE , g_UEDIAL, .T.)
                  endif
              endcase
            endif
          case this.w_SCELTA=5
            if g_IZCP$"SA" AND g_CPIN="N" AND g_DMIP="N" AND this.w_IDMSSA<>"S"
              * --- CPZ
              this.w_RETVAL = GSCP_BFA(this,4)
            else
              * --- INFINITY
              this.w_RETVAL = GSUT_BPI(this,4)
            endif
            if this.w_RETVAL=-1
              i_retcode = 'stop'
              return
            endif
        endcase
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DescReport = alltrim(this.w_DescReport)
    this.w_DescReport = strtran(this.w_DescReport,"\","_")
    this.w_DescReport = strtran(this.w_DescReport,"/","_")
    this.w_DescReport = strtran(this.w_DescReport,":","_")
    this.w_DescReport = strtran(this.w_DescReport,"*","_")
    this.w_DescReport = strtran(this.w_DescReport,"?","_")
    this.w_DescReport = strtran(this.w_DescReport,"<","_")
    this.w_DescReport = strtran(this.w_DescReport,">","_")
    this.w_DescReport = strtran(this.w_DescReport,"|","_")
    this.w_DescReport = strtran(this.w_DescReport,'"',"_")
    this.w_DescReport = strtran(this.w_DescReport,"(","_")
    this.w_DescReport = strtran(this.w_DescReport,")","_")
    this.w_DescReport = strtran(this.w_DescReport," ","_")
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_EMAIL = ""
    this.w_EMPEC = ""
    this.w_FAXNO = ""
    this.w_EMAIL_LETTANELCICLOCORRENTE = ""
    this.w_EMPEC_LETTANELCICLOCORRENTE = ""
    this.w_FAXNO_LETTONELCICLOCORRENTE = ""
    this.w_StessaEmailperTuttiIDocumenti = .T.
    this.w_StessaPECperTuttiIDocumenti = .T.
    this.w_StessoFaxperTuttiIDocumenti = .T.
    this.w_StessoIntestatarioperTuttiIDocumenti = .T.
    this.w_ANDESCRI = ""
    this.w_ANDESCRI = this.w_ANDESCRI_LETTONELCICLOCORRENTE
    this.w_REPORTCOUNTER = 1
    * --- w_COUNTER conta a quanti documenti sono stati esaminati
    *     Si utilizza per memorizzare il primo indirizzo email e il primo fax
    this.w_COUNTER = 1
    * --- Scorre tutti i documenti da stampare e verifica se l'indirizzo email � sempre lostesso
    do while this.w_REPORTCOUNTER <= ALEN( this.w_OBJREPORT.aReportList ,1 ) AND (this.w_StessaEmailperTuttiIDocumenti OR this.w_StessaPECperTuttiIDocumenti OR this.w_StessoFaxperTuttiIDocumenti OR this.w_StessoIntestatarioperTuttiIDocumenti)
      * --- Per ogni stampa controlla se nel cursore c'� il campo MVSERIAL
      AFIELDS(aColumnNames, this.w_OBJREPORT.aReportList[ this.w_REPORTCOUNTER , 2 ] )
      this.w_MVSERIAL_CURSOR = SYS(2015)
      if ASCAN(aColumnNames,"MVSERIAL") <> 0
        SELECT DISTINCT MVSERIAL FROM ( this.w_OBJREPORT.aReportList[ this.w_REPORTCOUNTER , 2 ] ) INTO CURSOR ( this.w_MVSERIAL_CURSOR )
        if USED( this.w_MVSERIAL_CURSOR )
          SELECT( this.w_MVSERIAL_CURSOR )
          GO TOP
          SCAN
          this.w_MVSERIAL = MVSERIAL
          this.w_EMAIL_LETTANELCICLOCORRENTE = ""
          this.w_EMPEC_LETTANELCICLOCORRENTE = ""
          this.w_FAXNO_LETTONELCICLOCORRENTE = ""
          this.w_ANDESCRI_LETTONELCICLOCORRENTE = ""
          * --- Legge l'intestatario del documento
          if g_APPLICATION = "ad hoc ENTERPRISE"
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVTIPCON,MVCODCON,MVCLADOC,MVCODDES,MVCODNOM"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVTIPCON,MVCODCON,MVCLADOC,MVCODDES,MVCODNOM;
                from (i_cTable) where;
                    MVSERIAL = this.w_MVSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
              this.w_ANCODICE = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
              this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
              this.w_MVCODDES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
              this.w_NOCODICE = NVL(cp_ToDate(_read_.MVCODNOM),cp_NullValue(_read_.MVCODNOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_NOCODICE = NVL( this.w_NOCODICE, "" )
          else
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVTIPCON,MVCODCON,MVCLADOC,MVCODDES,MVFLVEAC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVTIPCON,MVCODCON,MVCLADOC,MVCODDES,MVFLVEAC;
                from (i_cTable) where;
                    MVSERIAL = this.w_MVSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
              this.w_ANCODICE = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
              this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
              this.w_MVCODDES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
              this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if g_APPLICATION = "ad hoc ENTERPRISE"
            * --- Legge indirizzo email e numero di fax della sede
            if NOT EMPTY( NVL( this.w_MVCODDES, "" ) )
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DD_EMAIL,DD_EMPEC,DDTELFAX"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DD_EMAIL,DD_EMPEC,DDTELFAX;
                  from (i_cTable) where;
                      DDTIPCON = this.w_ANTIPCON;
                      and DDCODICE = this.w_ANCODICE;
                      and DDCODDES = this.w_MVCODDES;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_EMAIL_LETTANELCICLOCORRENTE = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
                this.w_EMPEC_LETTANELCICLOCORRENTE = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
                this.w_FAXNO_LETTONELCICLOCORRENTE = NVL(cp_ToDate(_read_.DDTELFAX),cp_NullValue(_read_.DDTELFAX))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_EMAIL_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_EMAIL_LETTANELCICLOCORRENTE, "" ) ) )
              this.w_EMPEC_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_EMPEC_LETTANELCICLOCORRENTE, "" ) ) )
              this.w_FAXNO_LETTONELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_FAXNO_LETTONELCICLOCORRENTE, "" ) ) )
            endif
          else
            * --- Legge indirizzo email e numero di fax della sede
            this.w_DDNOMDES = ""
            if this.w_MVCLADOC $ "FA-DT-OR-NC"
              this.w_NOVERSED = EMPTY(this.w_MVCODDES)
              this.w_EMAIL_LETTANELCICLOCORRENTE = GSAR_BRM( this,"E_M",this.w_ANTIPCON,this.w_ANCODICE,IIF(this.w_MVCLADOC $ "DT-OR","NO","FA"), "R", this.w_MVCODDES , this.w_NOVERSED and this.w_MVCLADOC $ "DT-OR")
              this.w_EMPEC_LETTANELCICLOCORRENTE = GSAR_BRM( this,"PEC",this.w_ANTIPCON,this.w_ANCODICE,IIF(this.w_MVCLADOC $ "DT-OR","NO","FA"), "R", this.w_MVCODDES , this.w_NOVERSED and this.w_MVCLADOC $ "DT-OR")
              this.w_FAXNO_LETTONELCICLOCORRENTE = GSAR_BRM(this,"E_M",this.w_ANTIPCON,this.w_ANCODICE,IIF(this.w_MVCLADOC $ "DT-OR","NO","FA"), "X",this.w_MVCODDES, this.w_NOVERSED and this.w_MVCLADOC $ "DT-OR")
            else
              if NOT EMPTY( NVL( this.w_MVCODDES, "" ) ) AND this.w_MVFLVEAC<>"A"
                * --- Read from DES_DIVE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DES_DIVE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DD_EMAIL,DD_EMPEC,DDNUMFAX"+;
                    " from "+i_cTable+" DES_DIVE where ";
                        +"DDTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                        +" and DDCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                        +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DD_EMAIL,DD_EMPEC,DDNUMFAX;
                    from (i_cTable) where;
                        DDTIPCON = this.w_ANTIPCON;
                        and DDCODICE = this.w_ANCODICE;
                        and DDCODDES = this.w_MVCODDES;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_EMAIL_LETTANELCICLOCORRENTE = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
                  this.w_EMPEC_LETTANELCICLOCORRENTE = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
                  this.w_FAXNO_LETTONELCICLOCORRENTE = NVL(cp_ToDate(_read_.DDNUMFAX),cp_NullValue(_read_.DDNUMFAX))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
            if NOT EMPTY( NVL( this.w_MVCODDES, "" ) )
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDNOMDES"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDNOMDES;
                  from (i_cTable) where;
                      DDTIPCON = this.w_ANTIPCON;
                      and DDCODICE = this.w_ANCODICE;
                      and DDCODDES = this.w_MVCODDES;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DDNOMDES = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.w_EMAIL_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_EMAIL_LETTANELCICLOCORRENTE, "" ) ) )
            this.w_EMPEC_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_EMPEC_LETTANELCICLOCORRENTE, "" ) ) )
            this.w_FAXNO_LETTONELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_FAXNO_LETTONELCICLOCORRENTE, "" ) ) )
          endif
          if g_APPLICATION = "ad hoc ENTERPRISE" AND this.w_ANTIPCON = "G"
            * --- Se il conto � generico si legge dal nominativo in OFF_NOMI
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NODESCRI,NO_EMAIL,NO_EMPEC,NOTELFAX"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NODESCRI,NO_EMAIL,NO_EMPEC,NOTELFAX;
                from (i_cTable) where;
                    NOCODICE = this.w_NOCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANDESCRI_LETTONELCICLOCORRENTE = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
              this.w_AN_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
              this.w_AN_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
              this.w_ANTELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI,AN_EMAIL,AN_EMPEC,ANTELFAX"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI,AN_EMAIL,AN_EMPEC,ANTELFAX;
                from (i_cTable) where;
                    ANTIPCON = this.w_ANTIPCON;
                    and ANCODICE = this.w_ANCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANDESCRI_LETTONELCICLOCORRENTE = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
              this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
              this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_ANDESCRI_LETTONELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_ANDESCRI_LETTONELCICLOCORRENTE , "" ) ) )
          * --- Se l'indirizzo email della sede non � disponibile, si usa quello dell'intestatario
          if EMPTY( this.w_EMAIL_LETTANELCICLOCORRENTE )
            this.w_EMAIL_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_AN_EMAIL, "" ) ) )
          endif
          * --- Se l'indirizzo email PEC della sede non � disponibile, si usa quello dell'intestatario
          if EMPTY( this.w_EMPEC_LETTANELCICLOCORRENTE )
            this.w_EMPEC_LETTANELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_AN_EMPEC, "" ) ) )
          endif
          * --- Se il fax della sede non � disponibile, si usa quello dell'intestatario
          if EMPTY( this.w_FAXNO_LETTONELCICLOCORRENTE )
            this.w_FAXNO_LETTONELCICLOCORRENTE = LOWER( ALLTRIM( NVL( this.w_ANTELFAX, "" ) ) )
          endif
          if this.w_COUNTER = 1
            * --- Memorizza il primo indirizzo email e il primo fax
            this.w_EMAIL = this.w_EMAIL_LETTANELCICLOCORRENTE
            this.w_EMPEC = this.w_EMPEC_LETTANELCICLOCORRENTE
            this.w_FAXNO = this.w_FAXNO_LETTONELCICLOCORRENTE
            this.w_ANDESCRI = this.w_ANDESCRI_LETTONELCICLOCORRENTE
          else
            * --- Email
            if this.w_EMAIL <> this.w_EMAIL_LETTANELCICLOCORRENTE AND this.w_StessaEmailperTuttiIDocumenti
              * --- Se si trova un documento in cui l'email � diversa da quelli gi� esaminati, l'email non viene specificata
              this.w_StessaEmailperTuttiIDocumenti = .F.
              this.w_EMAIL = ""
            endif
            * --- PEC
            if this.w_EMPEC <> this.w_EMPEC_LETTANELCICLOCORRENTE AND this.w_StessaPECperTuttiIDocumenti
              * --- Se si trova un documento in cui l'email PEC � diversa da quelli gi� esaminati, l'email PEC non viene specificata
              this.w_StessaPECperTuttiIDocumenti = .F.
              this.w_EMPEC = ""
            endif
            * --- Fax
            if this.w_FAXNO <> this.w_FAXNO_LETTONELCICLOCORRENTE AND this.w_StessoFaxperTuttiIDocumenti
              * --- Se si trova un documento in cui il fax � diverso da quelli gi� esaminati, il fax non viene specificato
              this.w_StessoFaxperTuttiIDocumenti = .F.
              this.w_FAXNO = ""
            endif
            * --- Intestatario
            if this.w_ANDESCRI <> this.w_ANDESCRI_LETTONELCICLOCORRENTE AND this.w_StessoIntestatarioperTuttiIDocumenti
              * --- Se si trova un documento in cui il fax � diverso da quelli gi� esaminati, il fax non viene specificato
              this.w_StessoIntestatarioperTuttiIDocumenti = .F.
              this.w_ANDESCRI = ""
            endif
          endif
          this.w_COUNTER = this.w_COUNTER + 1
          ENDSCAN
          USE
        endif
        this.w_REPORTCOUNTER = this.w_REPORTCOUNTER + 1
      else
        * --- Esce dal ciclo senza assegnare l'indirizzo email
        this.w_StessaEmailperTuttiIDocumenti = .F.
        this.w_StessaPECperTuttiIDocumenti = .F.
        this.w_EMAIL = ""
        this.w_EMPEC = ""
        this.w_StessoFaxperTuttiIDocumenti = .F.
        this.w_FAXNO = ""
        this.w_StessoIntestatarioperTuttiIDocumenti = .F.
        this.w_ANDESCRI = ""
        this.w_ANCODICE = ""
        this.w_REPORTCOUNTER = ALEN( this.w_OBJREPORT.aReportList ,1 ) + 1
      endif
    enddo
    if this.w_StessoIntestatarioperTuttiIDocumenti
      i_CODDES = IIF ( EMPTY (i_CODDES) , this.w_ANCODICE , i_CODDES)
      i_DEST = IIF (not EMPTY (this.w_DDNOMDES) ,this.w_DDNOMDES, this.w_ANDESCRI )
    endif
    if this.w_StessaEmailperTuttiIDocumenti
      this.w_AN_EMAIL = NVL( this.w_AN_EMAIL, "" )
      if EMPTY( this.w_EMAIL )
        * --- Se non c'� l'email della sede si usa quella dell'intestatario
        this.w_EMAIL = this.w_AN_EMAIL
      endif
    endif
    if this.w_StessaPECperTuttiIDocumenti
      this.w_AN_EMPEC = NVL( this.w_AN_EMPEC, "" )
      if EMPTY( this.w_EMPEC )
        * --- Se non c'� l'email PEC della sede si usa quella dell'intestatario
        this.w_EMPEC = this.w_AN_EMPEC
      endif
    endif
    if this.w_StessoFaxperTuttiIDocumenti
      this.w_ANTELFAX = NVL( this.w_ANTELFAX, "" )
      if EMPTY( this.w_FAXNO )
        * --- Se non c'� ilfax della sede si usa quello dell'intestatario
        this.w_FAXNO = this.w_ANTELFAX
      endif
    endif
  endproc


  proc Init(oParentObject,pOper,pFDFFile,pLanguage,pSessionCode,poParentObject)
    this.pOper=pOper
    this.pFDFFile=pFDFFile
    this.pLanguage=pLanguage
    this.pSessionCode=pSessionCode
    this.poParentObject=poParentObject
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='WECONTI'
    this.cWorkTables[3]='UTE_NTI'
    this.cWorkTables[4]='OUT_PUTS'
    this.cWorkTables[5]='CONTROPA'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='DES_DIVE'
    this.cWorkTables[8]='STMPFILE'
    this.cWorkTables[9]='DOC_MAST'
    this.cWorkTables[10]='OFF_NOMI'
    this.cWorkTables[11]='PROMCLAS'
    this.cWorkTables[12]='MOD_OFFE'
    return(this.OpenAllTables(12))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pFDFFile,pLanguage,pSessionCode,poParentObject"
endproc
