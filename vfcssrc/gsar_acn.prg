* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_acn                                                        *
*              Commesse                                                        *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_51]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-06-17                                                      *
* Last revis.: 2008-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_acn"))

* --- Class definition
define class tgsar_acn as StdForm
  Top    = 6
  Left   = 20

  * --- Standard Properties
  Width  = 532
  Height = 462+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-09"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Constant Properties
  CAN_TIER_IDX = 0
  MODASPED_IDX = 0
  VETTORI_IDX = 0
  PORTI_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  MAGAZZIN_IDX = 0
  LISTINI_IDX = 0
  KEY_ARTI_IDX = 0
  CPAR_DEF_IDX = 0
  cFile = "CAN_TIER"
  cKeySelect = "CNCODCAN"
  cKeyWhere  = "CNCODCAN=this.w_CNCODCAN"
  cKeyWhereODBC = '"CNCODCAN="+cp_ToStrODBC(this.w_CNCODCAN)';

  cKeyWhereODBCqualified = '"CAN_TIER.CNCODCAN="+cp_ToStrODBC(this.w_CNCODCAN)';

  cPrg = "gsar_acn"
  cComment = "Commesse"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0ACN'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CNCODCAN = space(15)
  w_CNDESCAN = space(100)
  w_CNCODVAL = space(3)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CNIMPORT = 0
  w_CALCPIC = 0
  w_CNDATINI = ctod('  /  /  ')
  w_CNDATFIN = ctod('  /  /  ')
  w_CN_STATO = space(1)
  w_CN__NOTE = space(0)
  w_CNTIPCON = space(1)
  w_CNDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CNDTINVA = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_codi = space(15)
  w_desc = space(30)
  w_CNINDIRI = space(30)
  w_CN___CAP = space(8)
  w_CNLOCALI = space(30)
  w_CNPROVIN = space(2)
  w_CNRESPON = space(40)
  w_CNCODCON = space(15)
  w_RAGSOC = space(40)
  w_CNTELEFO = space(18)
  w_CN_EMAIL = space(254)
  w_CNCODVET = space(5)
  w_DESVET = space(35)
  w_CNCODPOR = space(1)
  w_DESPOR = space(30)
  w_CNCODSPE = space(3)
  w_DESSPE = space(35)
  w_RESCHK = 0
  w_CNCODLIS = space(5)
  w_CNSERVIZ = space(41)
  w_DESSER = space(40)
  w_DESLIS = space(40)
  w_CNFLAGIM = space(1)
  w_VALLIS = space(3)
  w_TIPART = space(1)
  w_CNPRJMOD = space(200)
  o_CNPRJMOD = space(200)
  w_ReadPar = space(10)
  w_PRJREL = space(4)
  w_MSPROJ = space(1)
  w_MSFile = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAN_TIER','gsar_acn')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_acnPag1","gsar_acn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Commesse")
      .Pages(1).HelpContextID = 214250357
      .Pages(2).addobject("oPag","tgsar_acnPag2","gsar_acn",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati anagrafici")
      .Pages(2).HelpContextID = 152848633
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCNCODCAN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_MSFile = this.oPgFrm.Pages(2).oPag.MSFile
      DoDefault()
    proc Destroy()
      this.w_MSFile = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='MODASPED'
    this.cWorkTables[2]='VETTORI'
    this.cWorkTables[3]='PORTI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='LISTINI'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='CPAR_DEF'
    this.cWorkTables[10]='CAN_TIER'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAN_TIER_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAN_TIER_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CNCODCAN = NVL(CNCODCAN,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_31_joined
    link_2_31_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAN_TIER where CNCODCAN=KeySet.CNCODCAN
    *
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAN_TIER')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAN_TIER.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAN_TIER '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_31_joined=this.AddJoinedLink_2_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CNCODCAN',this.w_CNCODCAN  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DECTOT = 0
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_RAGSOC = space(40)
        .w_DESVET = space(35)
        .w_DESPOR = space(30)
        .w_DESSPE = space(35)
        .w_RESCHK = 0
        .w_DESSER = space(40)
        .w_DESLIS = space(40)
        .w_VALLIS = space(3)
        .w_TIPART = space(1)
        .w_ReadPar = 'TAM'
        .w_PRJREL = space(4)
        .w_MSPROJ = space(1)
        .w_CNCODCAN = NVL(CNCODCAN,space(15))
        .w_CNDESCAN = NVL(CNDESCAN,space(100))
        .w_CNCODVAL = NVL(CNCODVAL,space(3))
          if link_1_3_joined
            this.w_CNCODVAL = NVL(VACODVAL103,NVL(this.w_CNCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT103,0)
          else
          .link_1_3('Load')
          endif
        .w_CNIMPORT = NVL(CNIMPORT,0)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_CNDATINI = NVL(cp_ToDate(CNDATINI),ctod("  /  /  "))
        .w_CNDATFIN = NVL(cp_ToDate(CNDATFIN),ctod("  /  /  "))
        .w_CN_STATO = NVL(CN_STATO,space(1))
        .w_CN__NOTE = NVL(CN__NOTE,space(0))
        .w_CNTIPCON = NVL(CNTIPCON,space(1))
        .w_CNDTOBSO = NVL(cp_ToDate(CNDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CNDTINVA = NVL(cp_ToDate(CNDTINVA),ctod("  /  /  "))
        .w_codi = .w_CNCODCAN
        .w_desc = .w_CNDESCAN
        .w_CNINDIRI = NVL(CNINDIRI,space(30))
        .w_CN___CAP = NVL(CN___CAP,space(8))
        .w_CNLOCALI = NVL(CNLOCALI,space(30))
        .w_CNPROVIN = NVL(CNPROVIN,space(2))
        .w_CNRESPON = NVL(CNRESPON,space(40))
        .w_CNCODCON = NVL(CNCODCON,space(15))
          if link_2_9_joined
            this.w_CNCODCON = NVL(ANCODICE209,NVL(this.w_CNCODCON,space(15)))
            this.w_RAGSOC = NVL(ANDESCRI209,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO209),ctod("  /  /  "))
          else
          .link_2_9('Load')
          endif
        .w_CNTELEFO = NVL(CNTELEFO,space(18))
        .w_CN_EMAIL = NVL(CN_EMAIL,space(254))
        .w_CNCODVET = NVL(CNCODVET,space(5))
          if link_2_13_joined
            this.w_CNCODVET = NVL(VTCODVET213,NVL(this.w_CNCODVET,space(5)))
            this.w_DESVET = NVL(VTDESVET213,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(VTDTOBSO213),ctod("  /  /  "))
          else
          .link_2_13('Load')
          endif
        .w_CNCODPOR = NVL(CNCODPOR,space(1))
          if link_2_15_joined
            this.w_CNCODPOR = NVL(POCODPOR215,NVL(this.w_CNCODPOR,space(1)))
            this.w_DESPOR = NVL(PODESPOR215,space(30))
          else
          .link_2_15('Load')
          endif
        .w_CNCODSPE = NVL(CNCODSPE,space(3))
          if link_2_17_joined
            this.w_CNCODSPE = NVL(SPCODSPE217,NVL(this.w_CNCODSPE,space(3)))
            this.w_DESSPE = NVL(SPDESSPE217,space(35))
          else
          .link_2_17('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_CNCODLIS = NVL(CNCODLIS,space(5))
          .link_2_30('Load')
        .w_CNSERVIZ = NVL(CNSERVIZ,space(41))
          if link_2_31_joined
            this.w_CNSERVIZ = NVL(CACODICE231,NVL(this.w_CNSERVIZ,space(41)))
            this.w_DESSER = NVL(CADESART231,space(40))
            this.w_TIPART = NVL(CA__TIPO231,space(1))
          else
          .link_2_31('Load')
          endif
        .w_CNFLAGIM = NVL(CNFLAGIM,space(1))
        .w_CNPRJMOD = NVL(CNPRJMOD,space(200))
        .oPgFrm.Page2.oPag.MSFile.Calculate()
          .link_2_47('Load')
        cp_LoadRecExtFlds(this,'CAN_TIER')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CNCODCAN = space(15)
      .w_CNDESCAN = space(100)
      .w_CNCODVAL = space(3)
      .w_DECTOT = 0
      .w_CNIMPORT = 0
      .w_CALCPIC = 0
      .w_CNDATINI = ctod("  /  /  ")
      .w_CNDATFIN = ctod("  /  /  ")
      .w_CN_STATO = space(1)
      .w_CN__NOTE = space(0)
      .w_CNTIPCON = space(1)
      .w_CNDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CNDTINVA = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_codi = space(15)
      .w_desc = space(30)
      .w_CNINDIRI = space(30)
      .w_CN___CAP = space(8)
      .w_CNLOCALI = space(30)
      .w_CNPROVIN = space(2)
      .w_CNRESPON = space(40)
      .w_CNCODCON = space(15)
      .w_RAGSOC = space(40)
      .w_CNTELEFO = space(18)
      .w_CN_EMAIL = space(254)
      .w_CNCODVET = space(5)
      .w_DESVET = space(35)
      .w_CNCODPOR = space(1)
      .w_DESPOR = space(30)
      .w_CNCODSPE = space(3)
      .w_DESSPE = space(35)
      .w_RESCHK = 0
      .w_CNCODLIS = space(5)
      .w_CNSERVIZ = space(41)
      .w_DESSER = space(40)
      .w_DESLIS = space(40)
      .w_CNFLAGIM = space(1)
      .w_VALLIS = space(3)
      .w_TIPART = space(1)
      .w_CNPRJMOD = space(200)
      .w_ReadPar = space(10)
      .w_PRJREL = space(4)
      .w_MSPROJ = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CNCODVAL = g_PERVAL
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CNCODVAL))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,5,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
          .DoRTCalc(7,8,.f.)
        .w_CN_STATO = 'A'
          .DoRTCalc(10,10,.f.)
        .w_CNTIPCON = 'C'
          .DoRTCalc(12,17,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(19,19,.f.)
        .w_codi = .w_CNCODCAN
        .w_desc = .w_CNDESCAN
        .DoRTCalc(22,27,.f.)
          if not(empty(.w_CNCODCON))
          .link_2_9('Full')
          endif
        .DoRTCalc(28,31,.f.)
          if not(empty(.w_CNCODVET))
          .link_2_13('Full')
          endif
        .DoRTCalc(32,33,.f.)
          if not(empty(.w_CNCODPOR))
          .link_2_15('Full')
          endif
        .DoRTCalc(34,35,.f.)
          if not(empty(.w_CNCODSPE))
          .link_2_17('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(36,36,.f.)
        .w_RESCHK = 0
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_CNCODLIS))
          .link_2_30('Full')
          endif
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_CNSERVIZ))
          .link_2_31('Full')
          endif
          .DoRTCalc(40,41,.f.)
        .w_CNFLAGIM = 'N'
          .DoRTCalc(43,44,.f.)
        .w_CNPRJMOD = .w_CNPRJMOD
        .oPgFrm.Page2.oPag.MSFile.Calculate()
        .w_ReadPar = 'TAM'
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_ReadPar))
          .link_2_47('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAN_TIER')
    this.DoRTCalc(47,48,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCNCODCAN_1_1.enabled = i_bVal
      .Page1.oPag.oCNDESCAN_1_2.enabled = i_bVal
      .Page1.oPag.oCNCODVAL_1_3.enabled = i_bVal
      .Page1.oPag.oCNIMPORT_1_5.enabled = i_bVal
      .Page1.oPag.oCNDATINI_1_7.enabled = i_bVal
      .Page1.oPag.oCNDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oCN_STATO_1_9.enabled = i_bVal
      .Page1.oPag.oCN__NOTE_1_10.enabled = i_bVal
      .Page1.oPag.oCNDTOBSO_1_12.enabled = i_bVal
      .Page1.oPag.oCNDTINVA_1_20.enabled = i_bVal
      .Page2.oPag.oCNINDIRI_2_4.enabled = i_bVal
      .Page2.oPag.oCN___CAP_2_5.enabled = i_bVal
      .Page2.oPag.oCNLOCALI_2_6.enabled = i_bVal
      .Page2.oPag.oCNPROVIN_2_7.enabled = i_bVal
      .Page2.oPag.oCNRESPON_2_8.enabled = i_bVal
      .Page2.oPag.oCNCODCON_2_9.enabled = i_bVal
      .Page2.oPag.oCNTELEFO_2_11.enabled = i_bVal
      .Page2.oPag.oCN_EMAIL_2_12.enabled = i_bVal
      .Page2.oPag.oCNCODVET_2_13.enabled = i_bVal
      .Page2.oPag.oCNCODPOR_2_15.enabled = i_bVal
      .Page2.oPag.oCNCODSPE_2_17.enabled = i_bVal
      .Page2.oPag.oCNCODLIS_2_30.enabled = i_bVal
      .Page2.oPag.oCNSERVIZ_2_31.enabled = i_bVal
      .Page2.oPag.oCNFLAGIM_2_36.enabled = i_bVal
      .Page2.oPag.oCNPRJMOD_2_44.enabled = i_bVal
      .Page1.oPag.oObj_1_28.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page2.oPag.MSFile.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCNCODCAN_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCNCODCAN_1_1.enabled = .t.
        .Page1.oPag.oCNDESCAN_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAN_TIER',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODCAN,"CNCODCAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNDESCAN,"CNDESCAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODVAL,"CNCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNIMPORT,"CNIMPORT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNDATINI,"CNDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNDATFIN,"CNDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CN_STATO,"CN_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CN__NOTE,"CN__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNTIPCON,"CNTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNDTOBSO,"CNDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNDTINVA,"CNDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNINDIRI,"CNINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CN___CAP,"CN___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNLOCALI,"CNLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNPROVIN,"CNPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNRESPON,"CNRESPON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODCON,"CNCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNTELEFO,"CNTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CN_EMAIL,"CN_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODVET,"CNCODVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODPOR,"CNCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODSPE,"CNCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODLIS,"CNCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNSERVIZ,"CNSERVIZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNFLAGIM,"CNFLAGIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNPRJMOD,"CNPRJMOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    i_lTable = "CAN_TIER"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAN_TIER_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SCN with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAN_TIER_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAN_TIER
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAN_TIER')
        i_extval=cp_InsertValODBCExtFlds(this,'CAN_TIER')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CNCODCAN,CNDESCAN,CNCODVAL,CNIMPORT,CNDATINI"+;
                  ",CNDATFIN,CN_STATO,CN__NOTE,CNTIPCON,CNDTOBSO"+;
                  ",UTCC,UTCV,UTDC,UTDV,CNDTINVA"+;
                  ",CNINDIRI,CN___CAP,CNLOCALI,CNPROVIN,CNRESPON"+;
                  ",CNCODCON,CNTELEFO,CN_EMAIL,CNCODVET,CNCODPOR"+;
                  ",CNCODSPE,CNCODLIS,CNSERVIZ,CNFLAGIM,CNPRJMOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CNCODCAN)+;
                  ","+cp_ToStrODBC(this.w_CNDESCAN)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODVAL)+;
                  ","+cp_ToStrODBC(this.w_CNIMPORT)+;
                  ","+cp_ToStrODBC(this.w_CNDATINI)+;
                  ","+cp_ToStrODBC(this.w_CNDATFIN)+;
                  ","+cp_ToStrODBC(this.w_CN_STATO)+;
                  ","+cp_ToStrODBC(this.w_CN__NOTE)+;
                  ","+cp_ToStrODBC(this.w_CNTIPCON)+;
                  ","+cp_ToStrODBC(this.w_CNDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CNDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CNINDIRI)+;
                  ","+cp_ToStrODBC(this.w_CN___CAP)+;
                  ","+cp_ToStrODBC(this.w_CNLOCALI)+;
                  ","+cp_ToStrODBC(this.w_CNPROVIN)+;
                  ","+cp_ToStrODBC(this.w_CNRESPON)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODCON)+;
                  ","+cp_ToStrODBC(this.w_CNTELEFO)+;
                  ","+cp_ToStrODBC(this.w_CN_EMAIL)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODVET)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODPOR)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODSPE)+;
                  ","+cp_ToStrODBCNull(this.w_CNCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_CNSERVIZ)+;
                  ","+cp_ToStrODBC(this.w_CNFLAGIM)+;
                  ","+cp_ToStrODBC(this.w_CNPRJMOD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAN_TIER')
        i_extval=cp_InsertValVFPExtFlds(this,'CAN_TIER')
        cp_CheckDeletedKey(i_cTable,0,'CNCODCAN',this.w_CNCODCAN)
        INSERT INTO (i_cTable);
              (CNCODCAN,CNDESCAN,CNCODVAL,CNIMPORT,CNDATINI,CNDATFIN,CN_STATO,CN__NOTE,CNTIPCON,CNDTOBSO,UTCC,UTCV,UTDC,UTDV,CNDTINVA,CNINDIRI,CN___CAP,CNLOCALI,CNPROVIN,CNRESPON,CNCODCON,CNTELEFO,CN_EMAIL,CNCODVET,CNCODPOR,CNCODSPE,CNCODLIS,CNSERVIZ,CNFLAGIM,CNPRJMOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CNCODCAN;
                  ,this.w_CNDESCAN;
                  ,this.w_CNCODVAL;
                  ,this.w_CNIMPORT;
                  ,this.w_CNDATINI;
                  ,this.w_CNDATFIN;
                  ,this.w_CN_STATO;
                  ,this.w_CN__NOTE;
                  ,this.w_CNTIPCON;
                  ,this.w_CNDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CNDTINVA;
                  ,this.w_CNINDIRI;
                  ,this.w_CN___CAP;
                  ,this.w_CNLOCALI;
                  ,this.w_CNPROVIN;
                  ,this.w_CNRESPON;
                  ,this.w_CNCODCON;
                  ,this.w_CNTELEFO;
                  ,this.w_CN_EMAIL;
                  ,this.w_CNCODVET;
                  ,this.w_CNCODPOR;
                  ,this.w_CNCODSPE;
                  ,this.w_CNCODLIS;
                  ,this.w_CNSERVIZ;
                  ,this.w_CNFLAGIM;
                  ,this.w_CNPRJMOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gsar_acn
    * Se installato il modulo Produzione su Commesse
    * lancia un Batch che genera l' Attivita' legata al Capo Progetto
    If g_COMM='S'
     this.Notifyevent('CreaCapi')
    Endif
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAN_TIER_IDX,i_nConn)
      *
      * update CAN_TIER
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAN_TIER')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CNDESCAN="+cp_ToStrODBC(this.w_CNDESCAN)+;
             ",CNCODVAL="+cp_ToStrODBCNull(this.w_CNCODVAL)+;
             ",CNIMPORT="+cp_ToStrODBC(this.w_CNIMPORT)+;
             ",CNDATINI="+cp_ToStrODBC(this.w_CNDATINI)+;
             ",CNDATFIN="+cp_ToStrODBC(this.w_CNDATFIN)+;
             ",CN_STATO="+cp_ToStrODBC(this.w_CN_STATO)+;
             ",CN__NOTE="+cp_ToStrODBC(this.w_CN__NOTE)+;
             ",CNTIPCON="+cp_ToStrODBC(this.w_CNTIPCON)+;
             ",CNDTOBSO="+cp_ToStrODBC(this.w_CNDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CNDTINVA="+cp_ToStrODBC(this.w_CNDTINVA)+;
             ",CNINDIRI="+cp_ToStrODBC(this.w_CNINDIRI)+;
             ",CN___CAP="+cp_ToStrODBC(this.w_CN___CAP)+;
             ",CNLOCALI="+cp_ToStrODBC(this.w_CNLOCALI)+;
             ",CNPROVIN="+cp_ToStrODBC(this.w_CNPROVIN)+;
             ",CNRESPON="+cp_ToStrODBC(this.w_CNRESPON)+;
             ",CNCODCON="+cp_ToStrODBCNull(this.w_CNCODCON)+;
             ",CNTELEFO="+cp_ToStrODBC(this.w_CNTELEFO)+;
             ",CN_EMAIL="+cp_ToStrODBC(this.w_CN_EMAIL)+;
             ",CNCODVET="+cp_ToStrODBCNull(this.w_CNCODVET)+;
             ",CNCODPOR="+cp_ToStrODBCNull(this.w_CNCODPOR)+;
             ",CNCODSPE="+cp_ToStrODBCNull(this.w_CNCODSPE)+;
             ",CNCODLIS="+cp_ToStrODBCNull(this.w_CNCODLIS)+;
             ",CNSERVIZ="+cp_ToStrODBCNull(this.w_CNSERVIZ)+;
             ",CNFLAGIM="+cp_ToStrODBC(this.w_CNFLAGIM)+;
             ",CNPRJMOD="+cp_ToStrODBC(this.w_CNPRJMOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAN_TIER')
        i_cWhere = cp_PKFox(i_cTable  ,'CNCODCAN',this.w_CNCODCAN  )
        UPDATE (i_cTable) SET;
              CNDESCAN=this.w_CNDESCAN;
             ,CNCODVAL=this.w_CNCODVAL;
             ,CNIMPORT=this.w_CNIMPORT;
             ,CNDATINI=this.w_CNDATINI;
             ,CNDATFIN=this.w_CNDATFIN;
             ,CN_STATO=this.w_CN_STATO;
             ,CN__NOTE=this.w_CN__NOTE;
             ,CNTIPCON=this.w_CNTIPCON;
             ,CNDTOBSO=this.w_CNDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CNDTINVA=this.w_CNDTINVA;
             ,CNINDIRI=this.w_CNINDIRI;
             ,CN___CAP=this.w_CN___CAP;
             ,CNLOCALI=this.w_CNLOCALI;
             ,CNPROVIN=this.w_CNPROVIN;
             ,CNRESPON=this.w_CNRESPON;
             ,CNCODCON=this.w_CNCODCON;
             ,CNTELEFO=this.w_CNTELEFO;
             ,CN_EMAIL=this.w_CN_EMAIL;
             ,CNCODVET=this.w_CNCODVET;
             ,CNCODPOR=this.w_CNCODPOR;
             ,CNCODSPE=this.w_CNCODSPE;
             ,CNCODLIS=this.w_CNCODLIS;
             ,CNSERVIZ=this.w_CNSERVIZ;
             ,CNFLAGIM=this.w_CNFLAGIM;
             ,CNPRJMOD=this.w_CNPRJMOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAN_TIER_IDX,i_nConn)
      *
      * delete CAN_TIER
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CNCODCAN',this.w_CNCODCAN  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsar_acn
    if g_PERCAN='S' OR g_COMM='S'
      this.NotifyEvent('ControlliDoc')
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(7,10,.t.)
            .w_CNTIPCON = 'C'
        .DoRTCalc(12,19,.t.)
            .w_codi = .w_CNCODCAN
            .w_desc = .w_CNDESCAN
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(22,44,.t.)
        if .o_CNPRJMOD<>.w_CNPRJMOD
            .w_CNPRJMOD = .w_CNPRJMOD
        endif
        .oPgFrm.Page2.oPag.MSFile.Calculate()
          .link_2_47('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(47,48,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page2.oPag.MSFile.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCNCODVAL_1_3.enabled = this.oPgFrm.Page1.oPag.oCNCODVAL_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCNDATINI_1_7.enabled = this.oPgFrm.Page1.oPag.oCNDATINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCNDATFIN_1_8.enabled = this.oPgFrm.Page1.oPag.oCNDATFIN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCN_STATO_1_9.enabled = this.oPgFrm.Page1.oPag.oCN_STATO_1_9.mCond()
    this.oPgFrm.Page2.oPag.oCNPRJMOD_2_44.enabled = this.oPgFrm.Page2.oPag.oCNPRJMOD_2_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCN_STATO_1_9.visible=!this.oPgFrm.Page1.oPag.oCN_STATO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page2.oPag.MSFile.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CNCODVAL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CNCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CNCODVAL))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCNCODVAL_1_3'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CNCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CNCODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CNCODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.VACODVAL as VACODVAL103"+ ",link_1_3.VADECTOT as VADECTOT103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on CAN_TIER.CNCODVAL=link_1_3.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and CAN_TIER.CNCODVAL=link_1_3.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CNCODCON
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CNTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CNTIPCON;
                     ,'ANCODICE',trim(this.w_CNCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CNTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CNTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CNCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCNCODCON_2_9'),i_cWhere,'GSAR_BZC',"Elenco clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CNTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CNTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CNCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CNTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CNTIPCON;
                       ,'ANCODICE',this.w_CNCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_RAGSOC = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODCON = space(15)
      endif
      this.w_RAGSOC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CNCODCON = space(15)
        this.w_RAGSOC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.ANCODICE as ANCODICE209"+ ",link_2_9.ANDESCRI as ANDESCRI209"+ ",link_2_9.ANDTOBSO as ANDTOBSO209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CAN_TIER.CNCODCON=link_2_9.ANCODICE"+" and CAN_TIER.CNTIPCON=link_2_9.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CAN_TIER.CNCODCON=link_2_9.ANCODICE(+)"'+'+" and CAN_TIER.CNTIPCON=link_2_9.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CNCODVET
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CNCODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CNCODVET))
          select VTCODVET,VTDESVET,VTDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CNCODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CNCODVET)+"%");

            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CNCODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCNCODVET_2_13'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CNCODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CNCODVET)
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VTDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODVET = space(5)
      endif
      this.w_DESVET = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice vettore inesistente oppure obsoleto")
        endif
        this.w_CNCODVET = space(5)
        this.w_DESVET = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VETTORI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.VTCODVET as VTCODVET213"+ ",link_2_13.VTDESVET as VTDESVET213"+ ",link_2_13.VTDTOBSO as VTDTOBSO213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on CAN_TIER.CNCODVET=link_2_13.VTCODVET"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and CAN_TIER.CNCODVET=link_2_13.VTCODVET(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CNCODPOR
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APO',True,'PORTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" POCODPOR like "+cp_ToStrODBC(trim(this.w_CNCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by POCODPOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'POCODPOR',trim(this.w_CNCODPOR))
          select POCODPOR,PODESPOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by POCODPOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODPOR)==trim(_Link_.POCODPOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNCODPOR) and !this.bDontReportError
            deferred_cp_zoom('PORTI','*','POCODPOR',cp_AbsName(oSource.parent,'oCNCODPOR_2_15'),i_cWhere,'GSAR_APO',"Porti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                     +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',oSource.xKey(1))
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_CNCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_CNCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_DESPOR = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODPOR = space(1)
      endif
      this.w_DESPOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.POCODPOR as POCODPOR215"+ ","+cp_TransLinkFldName('link_2_15.PODESPOR')+" as PODESPOR215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on CAN_TIER.CNCODPOR=link_2_15.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and CAN_TIER.CNCODPOR=link_2_15.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CNCODSPE
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_CNCODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_CNCODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNCODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oCNCODSPE_2_17'),i_cWhere,'GSAR_ASP',"Modalita spedizione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_CNCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_CNCODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.SPCODSPE as SPCODSPE217"+ ","+cp_TransLinkFldName('link_2_17.SPDESSPE')+" as SPDESSPE217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on CAN_TIER.CNCODSPE=link_2_17.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and CAN_TIER.CNCODSPE=link_2_17.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CNCODLIS
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CNCODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_CNCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_CNCODVAL;
                     ,'LSCODLIS',trim(this.w_CNCODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oCNCODLIS_2_30'),i_cWhere,'GSAR_ALI',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CNCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valuta diversa dalla valuta di commessa.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_CNCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CNCODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_CNCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_CNCODVAL;
                       ,'LSCODLIS',this.w_CNCODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=g_CODEUR or .w_VALLIS=g_CODLIR
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta diversa dalla valuta di commessa.")
        endif
        this.w_CNCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNSERVIZ
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNSERVIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CNSERVIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CNSERVIZ))
          select CACODICE,CADESART,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNSERVIZ)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNSERVIZ) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCNSERVIZ_2_31'),i_cWhere,'GSMA_ACA',"Servizi",'gspc_zse.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNSERVIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CNSERVIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CNSERVIZ)
            select CACODICE,CADESART,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNSERVIZ = NVL(_Link_.CACODICE,space(41))
      this.w_DESSER = NVL(_Link_.CADESART,space(40))
      this.w_TIPART = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CNSERVIZ = space(41)
      endif
      this.w_DESSER = space(40)
      this.w_TIPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART='M' or .w_TIPART='F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CNSERVIZ = space(41)
        this.w_DESSER = space(40)
        this.w_TIPART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNSERVIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_31.CACODICE as CACODICE231"+ ",link_2_31.CADESART as CADESART231"+ ",link_2_31.CA__TIPO as CA__TIPO231"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_31 on CAN_TIER.CNSERVIZ=link_2_31.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_31"
          i_cKey=i_cKey+'+" and CAN_TIER.CNSERVIZ=link_2_31.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ReadPar
  func Link_2_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDMSPROJ,PDPRJREL";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_ReadPar)
            select PDCHIAVE,PDMSPROJ,PDPRJREL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PDCHIAVE,space(10))
      this.w_MSPROJ = NVL(_Link_.PDMSPROJ,space(1))
      this.w_PRJREL = NVL(_Link_.PDPRJREL,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(10)
      endif
      this.w_MSPROJ = space(1)
      this.w_PRJREL = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCNCODCAN_1_1.value==this.w_CNCODCAN)
      this.oPgFrm.Page1.oPag.oCNCODCAN_1_1.value=this.w_CNCODCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDESCAN_1_2.value==this.w_CNDESCAN)
      this.oPgFrm.Page1.oPag.oCNDESCAN_1_2.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCNCODVAL_1_3.RadioValue()==this.w_CNCODVAL)
      this.oPgFrm.Page1.oPag.oCNCODVAL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCNIMPORT_1_5.value==this.w_CNIMPORT)
      this.oPgFrm.Page1.oPag.oCNIMPORT_1_5.value=this.w_CNIMPORT
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDATINI_1_7.value==this.w_CNDATINI)
      this.oPgFrm.Page1.oPag.oCNDATINI_1_7.value=this.w_CNDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDATFIN_1_8.value==this.w_CNDATFIN)
      this.oPgFrm.Page1.oPag.oCNDATFIN_1_8.value=this.w_CNDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCN_STATO_1_9.RadioValue()==this.w_CN_STATO)
      this.oPgFrm.Page1.oPag.oCN_STATO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCN__NOTE_1_10.value==this.w_CN__NOTE)
      this.oPgFrm.Page1.oPag.oCN__NOTE_1_10.value=this.w_CN__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDTOBSO_1_12.value==this.w_CNDTOBSO)
      this.oPgFrm.Page1.oPag.oCNDTOBSO_1_12.value=this.w_CNDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDTINVA_1_20.value==this.w_CNDTINVA)
      this.oPgFrm.Page1.oPag.oCNDTINVA_1_20.value=this.w_CNDTINVA
    endif
    if not(this.oPgFrm.Page2.oPag.ocodi_2_1.value==this.w_codi)
      this.oPgFrm.Page2.oPag.ocodi_2_1.value=this.w_codi
    endif
    if not(this.oPgFrm.Page2.oPag.odesc_2_2.value==this.w_desc)
      this.oPgFrm.Page2.oPag.odesc_2_2.value=this.w_desc
    endif
    if not(this.oPgFrm.Page2.oPag.oCNINDIRI_2_4.value==this.w_CNINDIRI)
      this.oPgFrm.Page2.oPag.oCNINDIRI_2_4.value=this.w_CNINDIRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCN___CAP_2_5.value==this.w_CN___CAP)
      this.oPgFrm.Page2.oPag.oCN___CAP_2_5.value=this.w_CN___CAP
    endif
    if not(this.oPgFrm.Page2.oPag.oCNLOCALI_2_6.value==this.w_CNLOCALI)
      this.oPgFrm.Page2.oPag.oCNLOCALI_2_6.value=this.w_CNLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oCNPROVIN_2_7.value==this.w_CNPROVIN)
      this.oPgFrm.Page2.oPag.oCNPROVIN_2_7.value=this.w_CNPROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCNRESPON_2_8.value==this.w_CNRESPON)
      this.oPgFrm.Page2.oPag.oCNRESPON_2_8.value=this.w_CNRESPON
    endif
    if not(this.oPgFrm.Page2.oPag.oCNCODCON_2_9.value==this.w_CNCODCON)
      this.oPgFrm.Page2.oPag.oCNCODCON_2_9.value=this.w_CNCODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oRAGSOC_2_10.value==this.w_RAGSOC)
      this.oPgFrm.Page2.oPag.oRAGSOC_2_10.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCNTELEFO_2_11.value==this.w_CNTELEFO)
      this.oPgFrm.Page2.oPag.oCNTELEFO_2_11.value=this.w_CNTELEFO
    endif
    if not(this.oPgFrm.Page2.oPag.oCN_EMAIL_2_12.value==this.w_CN_EMAIL)
      this.oPgFrm.Page2.oPag.oCN_EMAIL_2_12.value=this.w_CN_EMAIL
    endif
    if not(this.oPgFrm.Page2.oPag.oCNCODVET_2_13.value==this.w_CNCODVET)
      this.oPgFrm.Page2.oPag.oCNCODVET_2_13.value=this.w_CNCODVET
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_2_14.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_2_14.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page2.oPag.oCNCODPOR_2_15.value==this.w_CNCODPOR)
      this.oPgFrm.Page2.oPag.oCNCODPOR_2_15.value=this.w_CNCODPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPOR_2_16.value==this.w_DESPOR)
      this.oPgFrm.Page2.oPag.oDESPOR_2_16.value=this.w_DESPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oCNCODSPE_2_17.value==this.w_CNCODSPE)
      this.oPgFrm.Page2.oPag.oCNCODSPE_2_17.value=this.w_CNCODSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSPE_2_18.value==this.w_DESSPE)
      this.oPgFrm.Page2.oPag.oDESSPE_2_18.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oCNCODLIS_2_30.value==this.w_CNCODLIS)
      this.oPgFrm.Page2.oPag.oCNCODLIS_2_30.value=this.w_CNCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCNSERVIZ_2_31.value==this.w_CNSERVIZ)
      this.oPgFrm.Page2.oPag.oCNSERVIZ_2_31.value=this.w_CNSERVIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSER_2_34.value==this.w_DESSER)
      this.oPgFrm.Page2.oPag.oDESSER_2_34.value=this.w_DESSER
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_35.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_35.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCNFLAGIM_2_36.RadioValue()==this.w_CNFLAGIM)
      this.oPgFrm.Page2.oPag.oCNFLAGIM_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCNPRJMOD_2_44.value==this.w_CNPRJMOD)
      this.oPgFrm.Page2.oPag.oCNPRJMOD_2_44.value=this.w_CNPRJMOD
    endif
    cp_SetControlsValueExtFlds(this,'CAN_TIER')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CNCODCAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCNCODCAN_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CNCODCAN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CNCODVAL))  and (g_COMM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCNCODVAL_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CNCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CNDATINI))  and (g_COMM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCNDATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CNDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CNDATFIN))  and (g_COMM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCNDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CNDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CN_STATO<>'S')  and not(g_COMM<>'S')  and (.w_CN_STATO<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCN_STATO_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile settare stato <storicizzata>")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CNCODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCNCODCON_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CNCODVET))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCNCODVET_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice vettore inesistente oppure obsoleto")
          case   not(.w_VALLIS=g_CODEUR or .w_VALLIS=g_CODLIR)  and not(empty(.w_CNCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCNCODLIS_2_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta diversa dalla valuta di commessa.")
          case   not((.w_TIPART='M' or .w_TIPART='F'))  and not(empty(.w_CNSERVIZ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCNSERVIZ_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(FILE(.w_CNPRJMOD) Or Empty(.w_CNPRJMOD))  and (.w_MSPROJ='S' OR .w_PRJREL='2003')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCNPRJMOD_2_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_acn
      * verifico se lo stato della commessa � Da Storicizzare
      * se ho impostato la data obsolescenza e se
      * tutte le attivit� sono in stato completato
      if g_COMM='S' And .w_CN_STATO='C'
      if Empty( .w_CNDTOBSO )
      	i_bnoChk = .f.
        i_bRes = .f.
        i_cErrorMsg = Ah_MsgFormat("Impostare la data obsolescenza per settare lo stato della commessa a 'Da storicizzare'")
      Else
        This.NotifyEvent('CheckAtt')
        if .w_RESCHK<>0
         i_bRes = .f.
        EndIf
      Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DECTOT = this.w_DECTOT
    this.o_CNPRJMOD = this.w_CNPRJMOD
    return

  func CanEdit()
    local i_res
    i_res=this.w_CN_STATO<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare una commessa storicizzata"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_CN_STATO<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eliminare una commessa storicizzata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_acnPag1 as StdContainer
  Width  = 528
  height = 462
  stdWidth  = 528
  stdheight = 462
  resizeYpos=304
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCNCODCAN_1_1 as StdField with uid="EMZVYWDLVJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CNCODCAN", cQueryName = "CNCODCAN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice alfanumerico della commessa/cantiere",;
    HelpContextID = 250999948,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=91, Top=10, InputMask=replicate('X',15)

  add object oCNDESCAN_1_2 as StdField with uid="WVHNBOCAKJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della commessa/cantiere",;
    HelpContextID = 235922572,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=230, Top=10, InputMask=replicate('X',100)


  add object oCNCODVAL_1_3 as StdCombo with uid="URBHZBBEDJ",rtseq=3,rtrep=.f.,left=91,top=44,width=85,height=21;
    , ToolTipText = "Codice valuta importi immessi nel modulo produzione su commessa";
    , HelpContextID = 200668302;
    , cFormVar="w_CNCODVAL",RowSource=""+"Valuta nazionale,"+"Valuta di conto", bObbl = .t. , nPag = 1;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oCNCODVAL_1_3.RadioValue()
    return(iif(this.value =1,alltrim(g_CODLIR),;
    iif(this.value =2,alltrim(g_PERVAL),;
    space(3))))
  endfunc
  func oCNCODVAL_1_3.GetRadio()
    this.Parent.oContained.w_CNCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oCNCODVAL_1_3.SetRadio()
    this.Parent.oContained.w_CNCODVAL=trim(this.Parent.oContained.w_CNCODVAL)
    this.value = ;
      iif(this.Parent.oContained.w_CNCODVAL==alltrim(g_CODLIR),1,;
      iif(this.Parent.oContained.w_CNCODVAL==alltrim(g_PERVAL),2,;
      0))
  endfunc

  func oCNCODVAL_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oCNCODVAL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_CNCODLIS)
        bRes2=.link_2_30('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCNCODVAL_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCNIMPORT_1_5 as StdField with uid="CWHIXIRFEU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CNIMPORT", cQueryName = "CNIMPORT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo commessa",;
    HelpContextID = 37196934,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=252, Top=44, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oCNDATINI_1_7 as StdField with uid="WZSHABETYB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CNDATINI", cQueryName = "CNDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio commessa",;
    HelpContextID = 133962607,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=91, Top=76

  func oCNDATINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  add object oCNDATFIN_1_8 as StdField with uid="HEEWNPVFTX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CNDATFIN", cQueryName = "CNDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine commessa",;
    HelpContextID = 83630964,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=252, Top=76

  func oCNDATFIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc


  add object oCN_STATO_1_9 as StdCombo with uid="HEHDHCWTFC",rtseq=9,rtrep=.f.,left=390,top=76,width=105,height=21;
    , ToolTipText = "Stato commessa";
    , HelpContextID = 267400331;
    , cFormVar="w_CN_STATO",RowSource=""+"Aperta,"+"Da storicizzare,"+"Storicizzata", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Impossibile settare stato <storicizzata>";
  , bGlobalFont=.t.


  func oCN_STATO_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oCN_STATO_1_9.GetRadio()
    this.Parent.oContained.w_CN_STATO = this.RadioValue()
    return .t.
  endfunc

  func oCN_STATO_1_9.SetRadio()
    this.Parent.oContained.w_CN_STATO=trim(this.Parent.oContained.w_CN_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_CN_STATO=='A',1,;
      iif(this.Parent.oContained.w_CN_STATO=='C',2,;
      iif(this.Parent.oContained.w_CN_STATO=='S',3,;
      0)))
  endfunc

  func oCN_STATO_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CN_STATO<>'S')
    endwith
   endif
  endfunc

  func oCN_STATO_1_9.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oCN_STATO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CN_STATO<>'S')
    endwith
    return bRes
  endfunc

  add object oCN__NOTE_1_10 as StdMemo with uid="DDYRXKFUFD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CN__NOTE", cQueryName = "CN__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note sulla commessa/cantiere",;
    HelpContextID = 38024341,;
   bGlobalFont=.t.,;
    Height=325, Width=492, Left=25, Top=110

  add object oCNDTOBSO_1_12 as StdField with uid="JJPQYNRTMM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CNDTOBSO", cQueryName = "CNDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 255911051,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=438, Top=439, tabstop=.f.

  add object oCNDTINVA_1_20 as StdField with uid="GIEQRXWCPK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CNDTINVA", cQueryName = "CNDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 60875929,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=224, Top=439, tabstop=.f.


  add object oObj_1_28 as cp_runprogram with uid="YKZYZFOYCY",left=-3, top=561, width=171,height=18,;
    caption='GSAR_BDC',;
   bGlobalFont=.t.,;
    prg="GSAR_BDC",;
    cEvent = "ControlliDoc",;
    nPag=1;
    , ToolTipText = "Controlla che la commessa non sia presente nei documenti";
    , HelpContextID = 239275863


  add object oObj_1_30 as cp_runprogram with uid="RSORDPTPAW",left=-2, top=478, width=168,height=18,;
    caption='GSPC_BGC',;
   bGlobalFont=.t.,;
    prg="GSPC_BGC",;
    cEvent = "CreaCapi",;
    nPag=1;
    , ToolTipText = "Se attivo il modulo produzione su commessa genera i capo conti";
    , HelpContextID = 28237993


  add object oObj_1_31 as cp_runprogram with uid="WBRIZIVRPL",left=-2, top=499, width=168,height=18,;
    caption='GSPC_BG2',;
   bGlobalFont=.t.,;
    prg="GSPC_BG2('Elimina')",;
    cEvent = "EliminaCapi",;
    nPag=1;
    , ToolTipText = "Se attivo il modulo produzione su commessa elimina i progetti";
    , HelpContextID = 28237976


  add object oObj_1_32 as cp_runprogram with uid="XCVSEKDFOX",left=-2, top=519, width=168,height=18,;
    caption='GSPC_BG2',;
   bGlobalFont=.t.,;
    prg="GSPC_BG2('CheckAtt')",;
    cEvent = "CheckAtt",;
    nPag=1;
    , ToolTipText = "Check stato commessa (visible solo se attiva la commessa)";
    , HelpContextID = 28237976

  add object oStr_1_13 as StdString with uid="JHKWPTEDKI",Visible=.t., Left=312, Top=442,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NGNAPLYJPL",Visible=.t., Left=7, Top=10,;
    Alignment=1, Width=76, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="LROBRLRNGW",Visible=.t., Left=130, Top=442,;
    Alignment=1, Width=92, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WZCQJZURJF",Visible=.t., Left=7, Top=44,;
    Alignment=1, Width=76, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PGYNGRXCSX",Visible=.t., Left=25, Top=93,;
    Alignment=0, Width=44, Height=15,;
    Caption="Note"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="OQCIQSAIMJ",Visible=.t., Left=196, Top=44,;
    Alignment=1, Width=54, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OGBDXFSHVV",Visible=.t., Left=4, Top=76,;
    Alignment=1, Width=79, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="XXQKNGPSDF",Visible=.t., Left=179, Top=76,;
    Alignment=1, Width=71, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BBVNDKVMJA",Visible=.t., Left=340, Top=76,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc
enddefine
define class tgsar_acnPag2 as StdContainer
  Width  = 528
  height = 462
  stdWidth  = 528
  stdheight = 462
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocodi_2_1 as StdField with uid="VQJTMQZVFG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_codi", cQueryName = "codi",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice alfanumerico della commessa/cantiere",;
    HelpContextID = 166084134,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=93, Top=10, InputMask=replicate('X',15)

  add object odesc_2_2 as StdField with uid="GAEBQDIVFN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_desc", cQueryName = "desc",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della commessa/cantiere",;
    HelpContextID = 165749814,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=232, Top=10, InputMask=replicate('X',30)

  add object oCNINDIRI_2_4 as StdField with uid="CYZOHEOSXL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CNINDIRI", cQueryName = "CNINDIRI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo completo della commessa/cantiere",;
    HelpContextID = 150377617,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=93, Top=48, InputMask=replicate('X',30)

  add object oCN___CAP_2_5 as StdField with uid="YPHLTPCDZH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CN___CAP", cQueryName = "CN___CAP",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice avviamento postale",;
    HelpContextID = 221525130,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=93, Top=75, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8), bHasZoom = .t. 

  proc oCN___CAP_2_5.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CN___CAP",".w_CNLOCALI",".w_CNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCNLOCALI_2_6 as StdField with uid="JTELRBKQNK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CNLOCALI", cQueryName = "CNLOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� dove � situata la commessa/cantiere",;
    HelpContextID = 251304815,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=231, Top=74, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oCNLOCALI_2_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CN___CAP",".w_CNLOCALI",".w_CNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCNPROVIN_2_7 as StdField with uid="FZUQBLBBSM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CNPROVIN", cQueryName = "CNPROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di appartenenza",;
    HelpContextID = 79551348,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=496, Top=74, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCNPROVIN_2_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_CNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCNRESPON_2_8 as StdField with uid="ZIRYCHNFWS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CNRESPON", cQueryName = "CNRESPON",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento responsabile della commessa/cantiere",;
    HelpContextID = 17761420,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=93, Top=102, InputMask=replicate('X',40)

  add object oCNCODCON_2_9 as StdField with uid="JCHVHQSUGV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CNCODCON", cQueryName = "CNCODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice eventuale cliente legato alla commessa",;
    HelpContextID = 250999948,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=93, Top=129, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CNTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CNCODCON"

  func oCNCODCON_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNCODCON_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNCODCON_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CNTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CNTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCNCODCON_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'',this.parent.oContained
  endproc
  proc oCNCODCON_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CNTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CNCODCON
     i_obj.ecpSave()
  endproc

  add object oRAGSOC_2_10 as StdField with uid="OCGCVPVOUI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 239190250,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=238, Top=129, InputMask=replicate('X',40)

  add object oCNTELEFO_2_11 as StdField with uid="DCEHEMPYAG",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CNTELEFO", cQueryName = "CNTELEFO",;
    bObbl = .f. , nPag = 2, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 209642635,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=93, Top=156, InputMask=replicate('X',18)

  add object oCN_EMAIL_2_12 as StdField with uid="MMCEDLHCPE",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CN_EMAIL", cQueryName = "CN_EMAIL",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 261213042,;
   bGlobalFont=.t.,;
    Height=21, Width=428, Left=92, Top=183, InputMask=replicate('X',254)

  add object oCNCODVET_2_13 as StdField with uid="TVHTCHRMHL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CNCODVET", cQueryName = "CNCODVET",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice vettore inesistente oppure obsoleto",;
    ToolTipText = "Codice del vettore",;
    HelpContextID = 200668294,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=93, Top=210, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CNCODVET"

  func oCNCODVET_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNCODVET_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNCODVET_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCNCODVET_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCNCODVET_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CNCODVET
     i_obj.ecpSave()
  endproc

  add object oDESVET_2_14 as StdField with uid="FJMLSQDGBH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 232652234,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=157, Top=210, InputMask=replicate('X',35)

  add object oCNCODPOR_2_15 as StdField with uid="YXKBTZMRHE",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CNCODPOR", cQueryName = "CNCODPOR",;
    bObbl = .f. , nPag = 2, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Codice del porto",;
    HelpContextID = 32896136,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=93, Top=237, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="PORTI", cZoomOnZoom="GSAR_APO", oKey_1_1="POCODPOR", oKey_1_2="this.w_CNCODPOR"

  func oCNCODPOR_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNCODPOR_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNCODPOR_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PORTI','*','POCODPOR',cp_AbsName(this.parent,'oCNCODPOR_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APO',"Porti",'',this.parent.oContained
  endproc
  proc oCNCODPOR_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_POCODPOR=this.parent.oContained.w_CNCODPOR
     i_obj.ecpSave()
  endproc

  add object oDESPOR_2_16 as StdField with uid="PBSQAWPOKL",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESPOR", cQueryName = "DESPOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 256114122,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=157, Top=237, InputMask=replicate('X',30)

  add object oCNCODSPE_2_17 as StdField with uid="LTTSVKILMF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CNCODSPE", cQueryName = "CNCODSPE",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modo di spedizione",;
    HelpContextID = 250999957,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=93, Top=264, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_CNCODSPE"

  func oCNCODSPE_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNCODSPE_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNCODSPE_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oCNCODSPE_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Modalita spedizione",'',this.parent.oContained
  endproc
  proc oCNCODSPE_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_CNCODSPE
     i_obj.ecpSave()
  endproc

  add object oDESSPE_2_18 as StdField with uid="LTQHFHMCDN",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 204537290,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=157, Top=264, InputMask=replicate('X',35)

  add object oCNCODLIS_2_30 as StdField with uid="HNEFYXXNCB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CNCODLIS", cQueryName = "CNCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta diversa dalla valuta di commessa.",;
    ToolTipText = "Codice listino",;
    HelpContextID = 168430457,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=91, Top=315, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_CNCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_CNCODLIS"

  func oCNCODLIS_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNCODLIS_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNCODLIS_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_CNCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_CNCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oCNCODLIS_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'',this.parent.oContained
  endproc
  proc oCNCODLIS_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LSVALLIS=w_CNCODVAL
     i_obj.w_LSCODLIS=this.parent.oContained.w_CNCODLIS
     i_obj.ecpSave()
  endproc

  add object oCNSERVIZ_2_31 as StdField with uid="LOIJIGNFRV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CNSERVIZ", cQueryName = "CNSERVIZ",;
    bObbl = .f. , nPag = 2, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice servizio (manodopera)",;
    HelpContextID = 81857408,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=91, Top=342, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CNSERVIZ"

  func oCNSERVIZ_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNSERVIZ_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNSERVIZ_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCNSERVIZ_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Servizi",'gspc_zse.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCNSERVIZ_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CNSERVIZ
     i_obj.ecpSave()
  endproc

  add object oDESSER_2_34 as StdField with uid="NGHQLIDTQN",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESSER", cQueryName = "DESSER",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio",;
    HelpContextID = 266403274,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=91, Top=369, InputMask=replicate('X',40)

  add object oDESLIS_2_35 as StdField with uid="QCTJLVZFNL",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 245890506,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=155, Top=315, InputMask=replicate('X',40)

  add object oCNFLAGIM_2_36 as StdCheck with uid="PDFXMOKZAN",rtseq=42,rtrep=.f.,left=91, top=396, caption="Aggiornamento immediato",;
    ToolTipText = "Flag aggiornamento immediato mov. commessa",;
    HelpContextID = 81214323,;
    cFormVar="w_CNFLAGIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCNFLAGIM_2_36.RadioValue()
    return(iif(this.value =1,'A',;
    'N'))
  endfunc
  func oCNFLAGIM_2_36.GetRadio()
    this.Parent.oContained.w_CNFLAGIM = this.RadioValue()
    return .t.
  endfunc

  func oCNFLAGIM_2_36.SetRadio()
    this.Parent.oContained.w_CNFLAGIM=trim(this.Parent.oContained.w_CNFLAGIM)
    this.value = ;
      iif(this.Parent.oContained.w_CNFLAGIM=='A',1,;
      0)
  endfunc

  add object oCNPRJMOD_2_44 as StdField with uid="VZNVQVBOYP",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CNPRJMOD", cQueryName = "CNPRJMOD",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 76686486,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=89, Top=439, InputMask=replicate('X',200)

  func oCNPRJMOD_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MSPROJ='S' OR .w_PRJREL='2003')
    endwith
   endif
  endfunc

  func oCNPRJMOD_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (FILE(.w_CNPRJMOD) Or Empty(.w_CNPRJMOD))
    endwith
    return bRes
  endfunc


  add object MSFile as cp_askfile with uid="QYJHWVQTTG",left=496, top=438, width=24,height=23,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_CNPRJMOD",cExt="mp?",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file modello MSProject";
    , HelpContextID = 158964182

  add object oStr_2_3 as StdString with uid="IBECNRNTYP",Visible=.t., Left=17, Top=10,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="NNJZLZDKPU",Visible=.t., Left=1, Top=48,;
    Alignment=1, Width=84, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="QKUVUKIGWQ",Visible=.t., Left=1, Top=75,;
    Alignment=1, Width=84, Height=15,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="ZMZKSZAKLZ",Visible=.t., Left=169, Top=74,;
    Alignment=1, Width=60, Height=15,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="YKXWOSEICJ",Visible=.t., Left=1, Top=102,;
    Alignment=1, Width=84, Height=15,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="JTXVDKONXB",Visible=.t., Left=1, Top=156,;
    Alignment=1, Width=84, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="VPYMAQAEKW",Visible=.t., Left=1, Top=183,;
    Alignment=1, Width=84, Height=15,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="XPECUIDKSU",Visible=.t., Left=1, Top=210,;
    Alignment=1, Width=84, Height=15,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="TZAQRYYPUS",Visible=.t., Left=1, Top=237,;
    Alignment=1, Width=84, Height=15,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="VCIMYLCMAI",Visible=.t., Left=1, Top=264,;
    Alignment=1, Width=84, Height=15,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="WDOPZISLCU",Visible=.t., Left=456, Top=74,;
    Alignment=1, Width=37, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="FGIPSSEUVQ",Visible=.t., Left=1, Top=129,;
    Alignment=1, Width=84, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="NYUUHUQYVD",Visible=.t., Left=1, Top=315,;
    Alignment=1, Width=84, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="ZKACIDZOFW",Visible=.t., Left=1, Top=342,;
    Alignment=1, Width=84, Height=15,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="YRKHUKWXIT",Visible=.t., Left=2, Top=294,;
    Alignment=0, Width=65, Height=15,;
    Caption="Dati MDO"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="GVOJANNSIE",Visible=.t., Left=1, Top=369,;
    Alignment=1, Width=84, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="NYPFBJJBXA",Visible=.t., Left=0, Top=419,;
    Alignment=0, Width=96, Height=15,;
    Caption="Dati MSProject:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="DEKFMDWGCF",Visible=.t., Left=1, Top=443,;
    Alignment=1, Width=84, Height=15,;
    Caption="File modello:"  ;
  , bGlobalFont=.t.

  add object oBox_2_37 as StdBox with uid="AWXJGJGSJS",left=1, top=310, width=524,height=2

  add object oBox_2_42 as StdBox with uid="AQKPOXBYEO",left=-1, top=434, width=524,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_acn','CAN_TIER','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CNCODCAN=CAN_TIER.CNCODCAN";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
