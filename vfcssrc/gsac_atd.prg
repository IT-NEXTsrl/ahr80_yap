* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_atd                                                        *
*              Causali documenti di acquisto                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_430]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-13                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsac_atd
If g_ACQU<>'S'
    AH_ErrorMsg("Funzionalit� consentita solo dal modulo ciclo acquisti")
 Return .t.
EndIf

* --- Fine Area Manuale
return(createobject("tgsac_atd"))

* --- Class definition
define class tgsac_atd as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 744
  Height = 553+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-17"
  HelpContextID=92956521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=195

  * --- Constant Properties
  TIP_DOCU_IDX = 0
  CAU_CONT_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  DOC_COLL_IDX = 0
  CLA_RIGD_IDX = 0
  REPO_DOC_IDX = 0
  OUT_PUTS_IDX = 0
  LISTINI_IDX = 0
  CAU_CESP_IDX = 0
  METCALSP_IDX = 0
  MODMRIFE_IDX = 0
  VASTRUTT_IDX = 0
  DATI_AGG_IDX = 0
  cFile = "TIP_DOCU"
  cKeySelect = "TDTIPDOC"
  cKeyWhere  = "TDTIPDOC=this.w_TDTIPDOC"
  cKeyWhereODBC = '"TDTIPDOC="+cp_ToStrODBC(this.w_TDTIPDOC)';

  cKeyWhereODBCqualified = '"TIP_DOCU.TDTIPDOC="+cp_ToStrODBC(this.w_TDTIPDOC)';

  cPrg = "gsac_atd"
  cComment = "Causali documenti di acquisto"
  icon = "anag.ico"
  cAutoZoom = 'GSAC0ATD'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TDTIPDOC = space(5)
  o_TDTIPDOC = space(5)
  w_TDDESDOC = space(35)
  w_TDFLVEAC = space(1)
  o_TDFLVEAC = space(1)
  w_TDCATDOC = space(2)
  o_TDCATDOC = space(2)
  w_TDEMERIC = space(1)
  o_TDEMERIC = space(1)
  w_TFFLGEFA = space(1)
  w_TDPRODOC = space(2)
  w_TDCAUMAG = space(5)
  o_TDCAUMAG = space(5)
  w_DESMAG = space(35)
  w_FLCOMM = space(1)
  w_FLAVAL = space(1)
  w_CAUCOL = space(5)
  w_FLCLFR = space(1)
  w_TDCODMAG = space(5)
  w_TDFLMGPR = space(1)
  w_TDCODMAT = space(5)
  w_TDFLMTPR = space(1)
  w_TDCAUCON = space(5)
  o_TDCAUCON = space(5)
  w_FLRIFE = space(1)
  w_DESCAU = space(35)
  w_TIPDOC = space(2)
  w_SERDOC = space(10)
  w_SERPRO = space(10)
  w_TDALFDOC = space(10)
  w_TDFLPDOC = space(1)
  w_TDSERPRO = space(10)
  w_TDFLPPRO = space(1)
  w_TDNUMSCO = 0
  w_TDCODLIS = space(5)
  w_TDQTADEF = 0
  w_TDPROVVI = space(1)
  w_DESLIS = space(40)
  w_DESAPP = space(30)
  w_TDASPETT = space(30)
  w_TDFLPREF = space(1)
  w_TDFLINTE = space(1)
  o_TDFLINTE = space(1)
  w_TDNOPRSC = space(1)
  w_TDFLACCO = space(1)
  o_TDFLACCO = space(1)
  w_TDFLPACK = space(1)
  w_TDFLDATT = space(1)
  w_TDFLORAT = space(1)
  w_TFFLRAGG = space(1)
  w_TDFLCRIS = space(1)
  w_TDFLPROV = space(1)
  w_TDFLDTPR = space(1)
  w_TDFLBACA = space(1)
  w_TDFLESPF = space(1)
  w_TDBOLDOG = space(1)
  w_TDFLCCAU = space(1)
  w_TDMODDES = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TDSEQPRE = space(1)
  w_TDSEQSCO = space(1)
  w_TDSEQMA1 = space(1)
  w_TDSEQMA2 = space(1)
  w_IVALIS = space(1)
  w_FLELGM = space(1)
  w_FLANAL = space(1)
  w_CCFLANAL = space(1)
  w_TDCAUPFI = space(5)
  w_TDCAUCOD = space(5)
  w_CCDATOBS = ctod('  /  /  ')
  w_TDFLEXPL = space(1)
  o_TDFLEXPL = space(1)
  w_TDPROSTA = 0
  w_TDEXPAUT = space(1)
  w_TIPREG = space(1)
  w_RESCHK = 0
  w_MOVIM = .F.
  w_FLRISE = space(1)
  w_FLCASC = space(1)
  w_OFLQRIO = space(1)
  w_TDFLSILI = space(1)
  o_TDFLSILI = space(1)
  w_TDPRZVAC = space(1)
  w_TDPRZDES = space(1)
  w_TDFLVALO = space(1)
  w_TDCHKTOT = space(1)
  w_TDFLBLEV = space(1)
  w_TDFLSPIN = space(1)
  w_TDFLQRIO = space(1)
  w_TDFLPREV = space(1)
  w_TDFLAPCA = space(1)
  w_TDRIPCON = space(1)
  w_TDNOSTCO = space(1)
  w_TDFLGETR = space(1)
  w_TDFLSPIM = space(1)
  w_MSDESIMB = space(40)
  w_TDFLSPTR = space(1)
  w_MSDESTRA = space(40)
  w_TDRIPINC = space(1)
  w_TDRIPIMB = space(1)
  w_TDRIPTRA = space(1)
  w_TDMCALSI = space(5)
  w_TDMCALST = space(5)
  w_TDSINCFL = 0
  w_CODI = space(5)
  w_DESC = space(35)
  w_TDFLRISC = space(1)
  w_DESCAUCE = space(40)
  w_DESPFI = space(35)
  w_DESCOD = space(35)
  w_TDFLANAL = space(1)
  o_TDFLANAL = space(1)
  w_TDFLELAN = space(1)
  w_TDVOCECR = space(1)
  w_TD_SEGNO = space(1)
  w_TDASSCES = space(1)
  o_TDASSCES = space(1)
  w_TDCAUCES = space(5)
  w_TDCONASS = space(1)
  w_TDCODSTR = space(10)
  w_TDFLARCO = space(1)
  o_TDFLARCO = space(1)
  w_TDLOTDIF = space(1)
  w_TDTIPIMB = space(1)
  o_TDTIPIMB = space(1)
  w_TDCAUPFI = space(5)
  w_TDCAUCOD = space(5)
  w_CODI = space(5)
  w_TDFLEXPL = space(1)
  w_DESC = space(35)
  w_TDVALCOM = space(1)
  w_TDCOSEPL = space(1)
  w_TDEXPAUT = space(1)
  w_TDFLCOMM = space(1)
  o_TDFLCOMM = space(1)
  w_TDFLCASH = space(1)
  w_TDFLNORC = space(1)
  w_TDMAXLEV = 0
  w_TDFLINTE = space(10)
  w_CATPFI = space(2)
  w_CATCOD = space(2)
  w_FLICOD = space(1)
  w_FLIPFI = space(1)
  w_DTOBSOCA = ctod('  /  /  ')
  w_FLTCOM = space(1)
  w_FLDCOM = space(1)
  w_CODI = space(5)
  w_TDDESRIF = space(18)
  w_TDMODRIF = space(5)
  w_DESC = space(35)
  w_TDFLNSRI = space(1)
  w_TDTPNDOC = space(3)
  w_TDFLVSRI = space(1)
  w_TDTPVDOC = space(3)
  w_TDFLRIDE = space(1)
  w_TDTPRDES = space(3)
  w_TDESCCL1 = space(3)
  w_TDESCCL2 = space(3)
  w_TDESCCL3 = space(3)
  w_TDESCCL4 = space(3)
  w_TDESCCL5 = space(3)
  w_TDSTACL1 = space(3)
  w_TDSTACL2 = space(3)
  w_TDSTACL3 = space(3)
  w_TDSTACL4 = space(3)
  w_TDSTACL5 = space(3)
  w_DESMOD = space(35)
  w_TDFLIMPA = space(1)
  w_TDFLIMAC = space(1)
  w_CODI = space(5)
  w_DESC = space(35)
  w_TDFLNSTA = space(1)
  w_TDFLSTLM = space(1)
  w_PRGSTA = space(8)
  w_PRGALT = space(8)
  w_DESSTRU = space(30)
  w_TDMINVAL = space(3)
  w_TDMINIMP = 0
  w_TDFLATIP = space(1)
  w_TDFLGEIN = space(1)
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_OBJ_CTRL = .F.
  w_TDFLIA01 = space(1)
  o_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  o_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  o_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  o_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  o_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  o_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_DATOOLTI = space(100)
  w_MSG_AGGLETT = space(10)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TDTIPDFE = space(4)
  w_TDDESEST = space(35)
  w_TDCODCLA = space(5)

  * --- Children pointers
  GSAC_MDC = .NULL.
  GSVE_MTD = .NULL.
  w_CAMAGG01 = .NULL.
  w_CAMAGG02 = .NULL.
  w_CAMAGG03 = .NULL.
  w_CAMAGG04 = .NULL.
  w_CAMAGG05 = .NULL.
  w_CAMAGG06 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIP_DOCU','gsac_atd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_atdPag1","gsac_atd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 217980213
      .Pages(2).addobject("oPag","tgsac_atdPag2","gsac_atd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Automatismi")
      .Pages(2).HelpContextID = 46356127
      .Pages(3).addobject("oPag","tgsac_atdPag3","gsac_atd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Gestioni collegate")
      .Pages(3).HelpContextID = 55249808
      .Pages(4).addobject("oPag","tgsac_atdPag4","gsac_atd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Origini")
      .Pages(4).HelpContextID = 9199642
      .Pages(5).addobject("oPag","tgsac_atdPag5","gsac_atd",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Report")
      .Pages(5).HelpContextID = 101450518
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTDTIPDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CAMAGG01 = this.oPgFrm.Pages(4).oPag.CAMAGG01
      this.w_CAMAGG02 = this.oPgFrm.Pages(4).oPag.CAMAGG02
      this.w_CAMAGG03 = this.oPgFrm.Pages(4).oPag.CAMAGG03
      this.w_CAMAGG04 = this.oPgFrm.Pages(4).oPag.CAMAGG04
      this.w_CAMAGG05 = this.oPgFrm.Pages(4).oPag.CAMAGG05
      this.w_CAMAGG06 = this.oPgFrm.Pages(4).oPag.CAMAGG06
      DoDefault()
    proc Destroy()
      this.w_CAMAGG01 = .NULL.
      this.w_CAMAGG02 = .NULL.
      this.w_CAMAGG03 = .NULL.
      this.w_CAMAGG04 = .NULL.
      this.w_CAMAGG05 = .NULL.
      this.w_CAMAGG06 = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='DOC_COLL'
    this.cWorkTables[5]='CLA_RIGD'
    this.cWorkTables[6]='REPO_DOC'
    this.cWorkTables[7]='OUT_PUTS'
    this.cWorkTables[8]='LISTINI'
    this.cWorkTables[9]='CAU_CESP'
    this.cWorkTables[10]='METCALSP'
    this.cWorkTables[11]='MODMRIFE'
    this.cWorkTables[12]='VASTRUTT'
    this.cWorkTables[13]='DATI_AGG'
    this.cWorkTables[14]='TIP_DOCU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(14))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIP_DOCU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIP_DOCU_IDX,3]
  return

  function CreateChildren()
    this.GSAC_MDC = CREATEOBJECT('stdDynamicChild',this,'GSAC_MDC',this.oPgFrm.Page4.oPag.oLinkPC_4_7)
    this.GSVE_MTD = CREATEOBJECT('stdDynamicChild',this,'GSVE_MTD',this.oPgFrm.Page5.oPag.oLinkPC_5_5)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAC_MDC)
      this.GSAC_MDC.DestroyChildrenChain()
      this.GSAC_MDC=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_7')
    if !ISNULL(this.GSVE_MTD)
      this.GSVE_MTD.DestroyChildrenChain()
      this.GSVE_MTD=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_5')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAC_MDC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSVE_MTD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAC_MDC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSVE_MTD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAC_MDC.NewDocument()
    this.GSVE_MTD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAC_MDC.SetKey(;
            .w_TDTIPDOC,"DCCODICE";
            )
      this.GSVE_MTD.SetKey(;
            .w_TDTIPDOC,"LGCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAC_MDC.ChangeRow(this.cRowID+'      1',1;
             ,.w_TDTIPDOC,"DCCODICE";
             )
      .GSVE_MTD.ChangeRow(this.cRowID+'      1',1;
             ,.w_TDTIPDOC,"LGCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAC_MDC)
        i_f=.GSAC_MDC.BuildFilter()
        if !(i_f==.GSAC_MDC.cQueryFilter)
          i_fnidx=.GSAC_MDC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAC_MDC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAC_MDC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAC_MDC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAC_MDC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSVE_MTD)
        i_f=.GSVE_MTD.BuildFilter()
        if !(i_f==.GSVE_MTD.cQueryFilter)
          i_fnidx=.GSVE_MTD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVE_MTD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVE_MTD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVE_MTD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVE_MTD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TDTIPDOC = NVL(TDTIPDOC,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_2_24_joined
    link_2_24_joined=.f.
    local link_2_25_joined
    link_2_25_joined=.f.
    local link_3_30_joined
    link_3_30_joined=.f.
    local link_3_32_joined
    link_3_32_joined=.f.
    local link_3_36_joined
    link_3_36_joined=.f.
    local link_3_37_joined
    link_3_37_joined=.f.
    local link_4_4_joined
    link_4_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIP_DOCU where TDTIPDOC=KeySet.TDTIPDOC
    *
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIP_DOCU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIP_DOCU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIP_DOCU '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_24_joined=this.AddJoinedLink_2_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_25_joined=this.AddJoinedLink_2_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_30_joined=this.AddJoinedLink_3_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_32_joined=this.AddJoinedLink_3_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_36_joined=this.AddJoinedLink_3_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_37_joined=this.AddJoinedLink_3_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_4_joined=this.AddJoinedLink_4_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMAG = space(35)
        .w_FLCOMM = space(1)
        .w_FLAVAL = space(1)
        .w_CAUCOL = space(5)
        .w_FLCLFR = space(1)
        .w_FLRIFE = space(1)
        .w_DESCAU = space(35)
        .w_TIPDOC = space(2)
        .w_SERDOC = space(10)
        .w_SERPRO = space(10)
        .w_DESLIS = space(40)
        .w_DESAPP = space(30)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_IVALIS = space(1)
        .w_FLELGM = space(1)
        .w_FLANAL = space(1)
        .w_CCFLANAL = space(1)
        .w_CCDATOBS = ctod("  /  /  ")
        .w_TIPREG = space(1)
        .w_RESCHK = 0
        .w_MOVIM = .f.
        .w_FLRISE = space(1)
        .w_FLCASC = space(1)
        .w_OFLQRIO = space(1)
        .w_MSDESIMB = space(40)
        .w_MSDESTRA = space(40)
        .w_DESCAUCE = space(40)
        .w_DESPFI = space(35)
        .w_DESCOD = space(35)
        .w_TDFLINTE = space(10)
        .w_CATPFI = space(2)
        .w_CATCOD = space(2)
        .w_FLICOD = space(1)
        .w_FLIPFI = space(1)
        .w_DTOBSOCA = ctod("  /  /  ")
        .w_FLTCOM = space(1)
        .w_FLDCOM = space(1)
        .w_DESMOD = space(35)
        .w_DESSTRU = space(30)
        .w_DASERIAL = '0000000001'
        .w_DACAM_01 = space(30)
        .w_DACAM_02 = space(30)
        .w_DACAM_04 = space(30)
        .w_DACAM_03 = space(30)
        .w_DACAM_05 = space(30)
        .w_DACAM_06 = space(30)
        .w_OBJ_CTRL = .f.
        .w_DATOOLTI = "Se attivo: il campo aggiuntivo %1 sar� importato"
        .w_MSG_AGGLETT = space(10)
        .w_TDTIPDOC = NVL(TDTIPDOC,space(5))
        .w_TDDESDOC = NVL(TDDESDOC,space(35))
        .w_TDFLVEAC = NVL(TDFLVEAC,space(1))
        .w_TDCATDOC = NVL(TDCATDOC,space(2))
        .w_TDEMERIC = NVL(TDEMERIC,space(1))
        .w_TFFLGEFA = NVL(TFFLGEFA,space(1))
        .w_TDPRODOC = NVL(TDPRODOC,space(2))
        .w_TDCAUMAG = NVL(TDCAUMAG,space(5))
          if link_1_8_joined
            this.w_TDCAUMAG = NVL(CMCODICE108,NVL(this.w_TDCAUMAG,space(5)))
            this.w_DESMAG = NVL(CMDESCRI108,space(35))
            this.w_CAUCOL = NVL(CMCAUCOL108,space(5))
            this.w_FLCLFR = NVL(CMFLCLFR108,space(1))
            this.w_FLAVAL = NVL(CMFLAVAL108,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CMDTOBSO108),ctod("  /  /  "))
            this.w_FLELGM = NVL(CMFLELGM108,space(1))
            this.w_FLCOMM = NVL(CMFLCOMM108,space(1))
            this.w_FLCASC = NVL(CMFLCASC108,space(1))
            this.w_FLRISE = NVL(CMFLRISE108,space(1))
          else
          .link_1_8('Load')
          endif
        .w_TDCODMAG = NVL(TDCODMAG,space(5))
          if link_1_14_joined
            this.w_TDCODMAG = NVL(MGCODMAG114,NVL(this.w_TDCODMAG,space(5)))
            this.w_DESAPP = NVL(MGDESMAG114,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .w_TDFLMGPR = NVL(TDFLMGPR,space(1))
        .w_TDCODMAT = NVL(TDCODMAT,space(5))
          if link_1_16_joined
            this.w_TDCODMAT = NVL(MGCODMAG116,NVL(this.w_TDCODMAT,space(5)))
            this.w_DESAPP = NVL(MGDESMAG116,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO116),ctod("  /  /  "))
          else
          .link_1_16('Load')
          endif
        .w_TDFLMTPR = NVL(TDFLMTPR,space(1))
        .w_TDCAUCON = NVL(TDCAUCON,space(5))
          if link_1_18_joined
            this.w_TDCAUCON = NVL(CCCODICE118,NVL(this.w_TDCAUCON,space(5)))
            this.w_DESCAU = NVL(CCDESCRI118,space(35))
            this.w_TIPDOC = NVL(CCTIPDOC118,space(2))
            this.w_SERDOC = NVL(CCSERDOC118,space(10))
            this.w_SERPRO = NVL(CCSERPRO118,space(10))
            this.w_FLANAL = NVL(CCFLANAL118,space(1))
            this.w_CCFLANAL = NVL(CCFLANAL118,space(1))
            this.w_FLRIFE = NVL(CCFLRIFE118,space(1))
            this.w_CCDATOBS = NVL(cp_ToDate(CCDTOBSO118),ctod("  /  /  "))
            this.w_TIPREG = NVL(CCTIPREG118,space(1))
          else
          .link_1_18('Load')
          endif
        .w_TDALFDOC = NVL(TDALFDOC,space(10))
        .w_TDFLPDOC = NVL(TDFLPDOC,space(1))
        .w_TDSERPRO = NVL(TDSERPRO,space(10))
        .w_TDFLPPRO = NVL(TDFLPPRO,space(1))
        .w_TDNUMSCO = NVL(TDNUMSCO,0)
        .w_TDCODLIS = NVL(TDCODLIS,space(5))
          if link_1_29_joined
            this.w_TDCODLIS = NVL(LSCODLIS129,NVL(this.w_TDCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS129,space(40))
            this.w_IVALIS = NVL(LSIVALIS129,space(1))
          else
          .link_1_29('Load')
          endif
        .w_TDQTADEF = NVL(TDQTADEF,0)
        .w_TDPROVVI = NVL(TDPROVVI,space(1))
        .w_TDASPETT = NVL(TDASPETT,space(30))
        .w_TDFLPREF = NVL(TDFLPREF,space(1))
        .w_TDFLINTE = NVL(TDFLINTE,space(1))
        .w_TDNOPRSC = NVL(TDNOPRSC,space(1))
        .w_TDFLACCO = NVL(TDFLACCO,space(1))
        .w_TDFLPACK = NVL(TDFLPACK,space(1))
        .w_TDFLDATT = NVL(TDFLDATT,space(1))
        .w_TDFLORAT = NVL(TDFLORAT,space(1))
        .w_TFFLRAGG = NVL(TFFLRAGG,space(1))
        .w_TDFLCRIS = NVL(TDFLCRIS,space(1))
        .w_TDFLPROV = NVL(TDFLPROV,space(1))
        .w_TDFLDTPR = NVL(TDFLDTPR,space(1))
        .w_TDFLBACA = NVL(TDFLBACA,space(1))
        .w_TDFLESPF = NVL(TDFLESPF,space(1))
        .w_TDBOLDOG = NVL(TDBOLDOG,space(1))
        .w_TDFLCCAU = NVL(TDFLCCAU,space(1))
        .w_TDMODDES = NVL(TDMODDES,space(1))
        .w_TDSEQPRE = NVL(TDSEQPRE,space(1))
        .w_TDSEQSCO = NVL(TDSEQSCO,space(1))
        .w_TDSEQMA1 = NVL(TDSEQMA1,space(1))
        .w_TDSEQMA2 = NVL(TDSEQMA2,space(1))
        .w_TDCAUPFI = NVL(TDCAUPFI,space(5))
          * evitabile
          *.link_1_74('Load')
        .w_TDCAUCOD = NVL(TDCAUCOD,space(5))
          * evitabile
          *.link_1_75('Load')
        .w_TDFLEXPL = NVL(TDFLEXPL,space(1))
        .w_TDPROSTA = NVL(TDPROSTA,0)
        .w_TDEXPAUT = NVL(TDEXPAUT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .w_TDFLSILI = NVL(TDFLSILI,space(1))
        .w_TDPRZVAC = NVL(TDPRZVAC,space(1))
        .w_TDPRZDES = NVL(TDPRZDES,space(1))
        .w_TDFLVALO = NVL(TDFLVALO,space(1))
        .w_TDCHKTOT = NVL(TDCHKTOT,space(1))
        .w_TDFLBLEV = NVL(TDFLBLEV,space(1))
        .w_TDFLSPIN = NVL(TDFLSPIN,space(1))
        .w_TDFLQRIO = NVL(TDFLQRIO,space(1))
        .w_TDFLPREV = NVL(TDFLPREV,space(1))
        .w_TDFLAPCA = NVL(TDFLAPCA,space(1))
        .w_TDRIPCON = NVL(TDRIPCON,space(1))
        .w_TDNOSTCO = NVL(TDNOSTCO,space(1))
        .w_TDFLGETR = NVL(TDFLGETR,space(1))
        .w_TDFLSPIM = NVL(TDFLSPIM,space(1))
        .w_TDFLSPTR = NVL(TDFLSPTR,space(1))
        .w_TDRIPINC = NVL(TDRIPINC,space(1))
        .w_TDRIPIMB = NVL(TDRIPIMB,space(1))
        .w_TDRIPTRA = NVL(TDRIPTRA,space(1))
        .w_TDMCALSI = NVL(TDMCALSI,space(5))
          if link_2_24_joined
            this.w_TDMCALSI = NVL(MSCODICE224,NVL(this.w_TDMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI224,space(40))
          else
          .link_2_24('Load')
          endif
        .w_TDMCALST = NVL(TDMCALST,space(5))
          if link_2_25_joined
            this.w_TDMCALST = NVL(MSCODICE225,NVL(this.w_TDMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI225,space(40))
          else
          .link_2_25('Load')
          endif
        .w_TDSINCFL = NVL(TDSINCFL,0)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLRISC = NVL(TDFLRISC,space(1))
        .w_TDFLANAL = NVL(TDFLANAL,space(1))
        .w_TDFLELAN = NVL(TDFLELAN,space(1))
        .w_TDVOCECR = NVL(TDVOCECR,space(1))
        .w_TD_SEGNO = NVL(TD_SEGNO,space(1))
        .w_TDASSCES = NVL(TDASSCES,space(1))
        .w_TDCAUCES = NVL(TDCAUCES,space(5))
          if link_3_30_joined
            this.w_TDCAUCES = NVL(CCCODICE330,NVL(this.w_TDCAUCES,space(5)))
            this.w_DESCAUCE = NVL(CCDESCRI330,space(40))
            this.w_DTOBSOCA = NVL(cp_ToDate(CCDTOBSO330),ctod("  /  /  "))
          else
          .link_3_30('Load')
          endif
        .w_TDCONASS = NVL(TDCONASS,space(1))
        .w_TDCODSTR = NVL(TDCODSTR,space(10))
          if link_3_32_joined
            this.w_TDCODSTR = NVL(STCODICE332,NVL(this.w_TDCODSTR,space(10)))
            this.w_DESSTRU = NVL(STDESCRI332,space(30))
          else
          .link_3_32('Load')
          endif
        .w_TDFLARCO = NVL(TDFLARCO,space(1))
        .w_TDLOTDIF = NVL(TDLOTDIF,space(1))
        .w_TDTIPIMB = NVL(TDTIPIMB,space(1))
        .w_TDCAUPFI = NVL(TDCAUPFI,space(5))
          if link_3_36_joined
            this.w_TDCAUPFI = NVL(TDTIPDOC336,NVL(this.w_TDCAUPFI,space(5)))
            this.w_DESPFI = NVL(TDDESDOC336,space(35))
            this.w_CATPFI = NVL(TDCATDOC336,space(2))
            this.w_FLIPFI = NVL(TDFLINTE336,space(1))
            this.w_FLTCOM = NVL(TDFLCOMM336,space(1))
          else
          .link_3_36('Load')
          endif
        .w_TDCAUCOD = NVL(TDCAUCOD,space(5))
          if link_3_37_joined
            this.w_TDCAUCOD = NVL(TDTIPDOC337,NVL(this.w_TDCAUCOD,space(5)))
            this.w_DESCOD = NVL(TDDESDOC337,space(35))
            this.w_CATCOD = NVL(TDCATDOC337,space(2))
            this.w_FLICOD = NVL(TDFLINTE337,space(1))
            this.w_FLDCOM = NVL(TDFLCOMM337,space(1))
          else
          .link_3_37('Load')
          endif
        .w_CODI = .w_TDTIPDOC
        .w_TDFLEXPL = NVL(TDFLEXPL,space(1))
        .w_DESC = .w_TDDESDOC
        .w_TDVALCOM = NVL(TDVALCOM,space(1))
        .w_TDCOSEPL = NVL(TDCOSEPL,space(1))
        .w_TDEXPAUT = NVL(TDEXPAUT,space(1))
        .w_TDFLCOMM = NVL(TDFLCOMM,space(1))
        .w_TDFLCASH = NVL(TDFLCASH,space(1))
        .w_TDFLNORC = NVL(TDFLNORC,space(1))
        .w_TDMAXLEV = NVL(TDMAXLEV,0)
        .w_CODI = .w_TDTIPDOC
        .w_TDDESRIF = NVL(TDDESRIF,space(18))
        .w_TDMODRIF = NVL(TDMODRIF,space(5))
          if link_4_4_joined
            this.w_TDMODRIF = NVL(MDCODICE404,NVL(this.w_TDMODRIF,space(5)))
            this.w_DESMOD = NVL(MDDESCRI404,space(35))
          else
          .link_4_4('Load')
          endif
        .w_DESC = .w_TDDESDOC
        .w_TDFLNSRI = NVL(TDFLNSRI,space(1))
        .w_TDTPNDOC = NVL(TDTPNDOC,space(3))
          * evitabile
          *.link_4_9('Load')
        .w_TDFLVSRI = NVL(TDFLVSRI,space(1))
        .w_TDTPVDOC = NVL(TDTPVDOC,space(3))
          * evitabile
          *.link_4_11('Load')
        .w_TDFLRIDE = NVL(TDFLRIDE,space(1))
        .w_TDTPRDES = NVL(TDTPRDES,space(3))
          * evitabile
          *.link_4_13('Load')
        .w_TDESCCL1 = NVL(TDESCCL1,space(3))
          * evitabile
          *.link_4_17('Load')
        .w_TDESCCL2 = NVL(TDESCCL2,space(3))
          * evitabile
          *.link_4_18('Load')
        .w_TDESCCL3 = NVL(TDESCCL3,space(3))
          * evitabile
          *.link_4_19('Load')
        .w_TDESCCL4 = NVL(TDESCCL4,space(3))
          * evitabile
          *.link_4_20('Load')
        .w_TDESCCL5 = NVL(TDESCCL5,space(3))
          * evitabile
          *.link_4_21('Load')
        .w_TDSTACL1 = NVL(TDSTACL1,space(3))
          * evitabile
          *.link_4_22('Load')
        .w_TDSTACL2 = NVL(TDSTACL2,space(3))
          * evitabile
          *.link_4_23('Load')
        .w_TDSTACL3 = NVL(TDSTACL3,space(3))
          * evitabile
          *.link_4_24('Load')
        .w_TDSTACL4 = NVL(TDSTACL4,space(3))
          * evitabile
          *.link_4_25('Load')
        .w_TDSTACL5 = NVL(TDSTACL5,space(3))
          * evitabile
          *.link_4_26('Load')
        .w_TDFLIMPA = NVL(TDFLIMPA,space(1))
        .w_TDFLIMAC = NVL(TDFLIMAC,space(1))
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLNSTA = NVL(TDFLNSTA,space(1))
        .w_TDFLSTLM = NVL(TDFLSTLM,space(1))
        .w_PRGSTA = 'GSAC_MDV'
        .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
        .w_TDMINVAL = NVL(TDMINVAL,space(3))
        .w_TDMINIMP = NVL(TDMINIMP,0)
        .w_TDFLATIP = NVL(TDFLATIP,space(1))
        .w_TDFLGEIN = NVL(TDFLGEIN,space(1))
          .link_4_40('Load')
        .w_TDFLIA01 = NVL(TDFLIA01,space(1))
        .w_TDFLIA02 = NVL(TDFLIA02,space(1))
        .w_TDFLIA03 = NVL(TDFLIA03,space(1))
        .w_TDFLIA04 = NVL(TDFLIA04,space(1))
        .w_TDFLIA05 = NVL(TDFLIA05,space(1))
        .w_TDFLIA06 = NVL(TDFLIA06,space(1))
        .w_TDFLRA01 = NVL(TDFLRA01,space(1))
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        .w_TDFLRA02 = NVL(TDFLRA02,space(1))
        .w_TDFLRA03 = NVL(TDFLRA03,space(1))
        .w_TDFLRA04 = NVL(TDFLRA04,space(1))
        .w_TDFLRA05 = NVL(TDFLRA05,space(1))
        .w_TDFLRA06 = NVL(TDFLRA06,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_TDTIPDFE = NVL(TDTIPDFE,space(4))
        .w_TDDESEST = NVL(TDDESEST,space(35))
        .w_TDCODCLA = NVL(TDCODCLA,space(5))
        cp_LoadRecExtFlds(this,'TIP_DOCU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsac_atd
    if this.w_TDFLVEAC <> 'A' OR this.w_TDCATDOC='OR'
      this.BlankRec()
    endif
    * - Memorizzo il valore iniziale di TDFLQRIO
    This.w_OFLQRIO = This.w_TDFLQRIO
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TDTIPDOC = space(5)
      .w_TDDESDOC = space(35)
      .w_TDFLVEAC = space(1)
      .w_TDCATDOC = space(2)
      .w_TDEMERIC = space(1)
      .w_TFFLGEFA = space(1)
      .w_TDPRODOC = space(2)
      .w_TDCAUMAG = space(5)
      .w_DESMAG = space(35)
      .w_FLCOMM = space(1)
      .w_FLAVAL = space(1)
      .w_CAUCOL = space(5)
      .w_FLCLFR = space(1)
      .w_TDCODMAG = space(5)
      .w_TDFLMGPR = space(1)
      .w_TDCODMAT = space(5)
      .w_TDFLMTPR = space(1)
      .w_TDCAUCON = space(5)
      .w_FLRIFE = space(1)
      .w_DESCAU = space(35)
      .w_TIPDOC = space(2)
      .w_SERDOC = space(10)
      .w_SERPRO = space(10)
      .w_TDALFDOC = space(10)
      .w_TDFLPDOC = space(1)
      .w_TDSERPRO = space(10)
      .w_TDFLPPRO = space(1)
      .w_TDNUMSCO = 0
      .w_TDCODLIS = space(5)
      .w_TDQTADEF = 0
      .w_TDPROVVI = space(1)
      .w_DESLIS = space(40)
      .w_DESAPP = space(30)
      .w_TDASPETT = space(30)
      .w_TDFLPREF = space(1)
      .w_TDFLINTE = space(1)
      .w_TDNOPRSC = space(1)
      .w_TDFLACCO = space(1)
      .w_TDFLPACK = space(1)
      .w_TDFLDATT = space(1)
      .w_TDFLORAT = space(1)
      .w_TFFLRAGG = space(1)
      .w_TDFLCRIS = space(1)
      .w_TDFLPROV = space(1)
      .w_TDFLDTPR = space(1)
      .w_TDFLBACA = space(1)
      .w_TDFLESPF = space(1)
      .w_TDBOLDOG = space(1)
      .w_TDFLCCAU = space(1)
      .w_TDMODDES = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TDSEQPRE = space(1)
      .w_TDSEQSCO = space(1)
      .w_TDSEQMA1 = space(1)
      .w_TDSEQMA2 = space(1)
      .w_IVALIS = space(1)
      .w_FLELGM = space(1)
      .w_FLANAL = space(1)
      .w_CCFLANAL = space(1)
      .w_TDCAUPFI = space(5)
      .w_TDCAUCOD = space(5)
      .w_CCDATOBS = ctod("  /  /  ")
      .w_TDFLEXPL = space(1)
      .w_TDPROSTA = 0
      .w_TDEXPAUT = space(1)
      .w_TIPREG = space(1)
      .w_RESCHK = 0
      .w_MOVIM = .f.
      .w_FLRISE = space(1)
      .w_FLCASC = space(1)
      .w_OFLQRIO = space(1)
      .w_TDFLSILI = space(1)
      .w_TDPRZVAC = space(1)
      .w_TDPRZDES = space(1)
      .w_TDFLVALO = space(1)
      .w_TDCHKTOT = space(1)
      .w_TDFLBLEV = space(1)
      .w_TDFLSPIN = space(1)
      .w_TDFLQRIO = space(1)
      .w_TDFLPREV = space(1)
      .w_TDFLAPCA = space(1)
      .w_TDRIPCON = space(1)
      .w_TDNOSTCO = space(1)
      .w_TDFLGETR = space(1)
      .w_TDFLSPIM = space(1)
      .w_MSDESIMB = space(40)
      .w_TDFLSPTR = space(1)
      .w_MSDESTRA = space(40)
      .w_TDRIPINC = space(1)
      .w_TDRIPIMB = space(1)
      .w_TDRIPTRA = space(1)
      .w_TDMCALSI = space(5)
      .w_TDMCALST = space(5)
      .w_TDSINCFL = 0
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_TDFLRISC = space(1)
      .w_DESCAUCE = space(40)
      .w_DESPFI = space(35)
      .w_DESCOD = space(35)
      .w_TDFLANAL = space(1)
      .w_TDFLELAN = space(1)
      .w_TDVOCECR = space(1)
      .w_TD_SEGNO = space(1)
      .w_TDASSCES = space(1)
      .w_TDCAUCES = space(5)
      .w_TDCONASS = space(1)
      .w_TDCODSTR = space(10)
      .w_TDFLARCO = space(1)
      .w_TDLOTDIF = space(1)
      .w_TDTIPIMB = space(1)
      .w_TDCAUPFI = space(5)
      .w_TDCAUCOD = space(5)
      .w_CODI = space(5)
      .w_TDFLEXPL = space(1)
      .w_DESC = space(35)
      .w_TDVALCOM = space(1)
      .w_TDCOSEPL = space(1)
      .w_TDEXPAUT = space(1)
      .w_TDFLCOMM = space(1)
      .w_TDFLCASH = space(1)
      .w_TDFLNORC = space(1)
      .w_TDMAXLEV = 0
      .w_TDFLINTE = space(10)
      .w_CATPFI = space(2)
      .w_CATCOD = space(2)
      .w_FLICOD = space(1)
      .w_FLIPFI = space(1)
      .w_DTOBSOCA = ctod("  /  /  ")
      .w_FLTCOM = space(1)
      .w_FLDCOM = space(1)
      .w_CODI = space(5)
      .w_TDDESRIF = space(18)
      .w_TDMODRIF = space(5)
      .w_DESC = space(35)
      .w_TDFLNSRI = space(1)
      .w_TDTPNDOC = space(3)
      .w_TDFLVSRI = space(1)
      .w_TDTPVDOC = space(3)
      .w_TDFLRIDE = space(1)
      .w_TDTPRDES = space(3)
      .w_TDESCCL1 = space(3)
      .w_TDESCCL2 = space(3)
      .w_TDESCCL3 = space(3)
      .w_TDESCCL4 = space(3)
      .w_TDESCCL5 = space(3)
      .w_TDSTACL1 = space(3)
      .w_TDSTACL2 = space(3)
      .w_TDSTACL3 = space(3)
      .w_TDSTACL4 = space(3)
      .w_TDSTACL5 = space(3)
      .w_DESMOD = space(35)
      .w_TDFLIMPA = space(1)
      .w_TDFLIMAC = space(1)
      .w_CODI = space(5)
      .w_DESC = space(35)
      .w_TDFLNSTA = space(1)
      .w_TDFLSTLM = space(1)
      .w_PRGSTA = space(8)
      .w_PRGALT = space(8)
      .w_DESSTRU = space(30)
      .w_TDMINVAL = space(3)
      .w_TDMINIMP = 0
      .w_TDFLATIP = space(1)
      .w_TDFLGEIN = space(1)
      .w_DASERIAL = space(10)
      .w_DACAM_01 = space(30)
      .w_DACAM_02 = space(30)
      .w_DACAM_04 = space(30)
      .w_DACAM_03 = space(30)
      .w_DACAM_05 = space(30)
      .w_DACAM_06 = space(30)
      .w_OBJ_CTRL = .f.
      .w_TDFLIA01 = space(1)
      .w_TDFLIA02 = space(1)
      .w_TDFLIA03 = space(1)
      .w_TDFLIA04 = space(1)
      .w_TDFLIA05 = space(1)
      .w_TDFLIA06 = space(1)
      .w_TDFLRA01 = space(1)
      .w_TDFLRA02 = space(1)
      .w_TDFLRA03 = space(1)
      .w_TDFLRA04 = space(1)
      .w_TDFLRA05 = space(1)
      .w_TDFLRA06 = space(1)
      .w_DATOOLTI = space(100)
      .w_MSG_AGGLETT = space(10)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TDTIPDFE = space(4)
      .w_TDDESEST = space(35)
      .w_TDCODCLA = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_TDFLVEAC = 'A'
        .w_TDCATDOC = 'DI'
        .w_TDEMERIC = .w_TDFLVEAC
          .DoRTCalc(6,6,.f.)
        .w_TDPRODOC = CALCPD(.w_TDCATDOC,.w_TDFLVEAC)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_TDCAUMAG))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,13,.f.)
        .w_TDCODMAG = SPACE(5)
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_TDCODMAG))
          .link_1_14('Full')
          endif
        .w_TDFLMGPR = 'D'
        .w_TDCODMAT = SPACE(5)
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_TDCODMAT))
          .link_1_16('Full')
          endif
        .w_TDFLMTPR = 'D'
        .w_TDCAUCON = IIF(.w_TDCATDOC $ 'FA-NC', .w_TDCAUCON, SPACE(5))
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_TDCAUCON))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,23,.f.)
        .w_TDALFDOC = IIF(NOT EMPTY(.w_SERDOC) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERDOC,.w_TDALFDOC)
        .w_TDFLPDOC = IIF(.w_TDEMERIC='V' , 'S', ' ')
        .w_TDSERPRO = IIF(NOT EMPTY(.w_SERPRO) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERPRO,.w_TDSERPRO)
        .w_TDFLPPRO = IIF(empty(.w_TDCAUCON), 'N', 'P')
        .w_TDNUMSCO = g_NUMSCO
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_TDCODLIS))
          .link_1_29('Full')
          endif
          .DoRTCalc(30,30,.f.)
        .w_TDPROVVI = 'N'
          .DoRTCalc(32,35,.f.)
        .w_TDFLINTE = IIF(.w_TDCATDOC='DI', 'N', 'F')
          .DoRTCalc(37,39,.f.)
        .w_TDFLDATT = IIF(.w_TDFLACCO='S','S',' ')
        .w_TDFLORAT = IIF(.w_TDFLACCO='S','S',' ')
        .w_TFFLRAGG = '1'
          .DoRTCalc(43,43,.f.)
        .w_TDFLPROV = 'N'
          .DoRTCalc(45,45,.f.)
        .w_TDFLBACA = ' '
          .DoRTCalc(47,47,.f.)
        .w_TDBOLDOG = ' '
        .w_TDFLCCAU = ' '
        .w_TDMODDES = 'S'
        .w_OBTEST = i_datsys
        .DoRTCalc(52,61,.f.)
          if not(empty(.w_TDCAUPFI))
          .link_1_74('Full')
          endif
        .DoRTCalc(62,62,.f.)
          if not(empty(.w_TDCAUCOD))
          .link_1_75('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
          .DoRTCalc(63,68,.f.)
        .w_MOVIM = .f.
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
          .DoRTCalc(70,72,.f.)
        .w_TDFLSILI = IIF(.w_TDCATDOC='DT', 'S', ' ')
        .w_TDPRZVAC = ' '
        .w_TDPRZDES = ' '
          .DoRTCalc(76,76,.f.)
        .w_TDCHKTOT = ' '
        .w_TDFLBLEV = 'N'
          .DoRTCalc(79,79,.f.)
        .w_TDFLQRIO = ' '
        .w_TDFLPREV = ' '
        .w_TDFLAPCA = 'N'
        .w_TDRIPCON = IIF( .w_TDFLINTE <> "N", .w_TDRIPCON, " " )
        .w_TDNOSTCO = 'N'
        .w_TDFLGETR = 'N'
        .w_TDFLSPIM = 'N'
          .DoRTCalc(87,87,.f.)
        .w_TDFLSPTR = 'N'
          .DoRTCalc(89,89,.f.)
        .w_TDRIPINC = " "
        .w_TDRIPIMB = " "
        .w_TDRIPTRA = " "
        .DoRTCalc(93,93,.f.)
          if not(empty(.w_TDMCALSI))
          .link_2_24('Full')
          endif
        .DoRTCalc(94,94,.f.)
          if not(empty(.w_TDMCALST))
          .link_2_25('Full')
          endif
          .DoRTCalc(95,95,.f.)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
        .w_TDFLRISC = ' '
          .DoRTCalc(99,101,.f.)
        .w_TDFLANAL = ' '
        .w_TDFLELAN = ' '
        .w_TDVOCECR = 'C'
        .w_TD_SEGNO = 'D'
        .w_TDASSCES = 'N'
        .w_TDCAUCES = IIF(.w_TDASSCES<>'M', SPACE(5),.w_TDCAUCES)
        .DoRTCalc(107,107,.f.)
          if not(empty(.w_TDCAUCES))
          .link_3_30('Full')
          endif
        .w_TDCONASS = IIF(.w_TDCATDOC<>'DT','N','F')
        .DoRTCalc(109,109,.f.)
          if not(empty(.w_TDCODSTR))
          .link_3_32('Full')
          endif
        .w_TDFLARCO = ' '
        .w_TDLOTDIF = IIF(.w_TDFLARCO<>'S', 'I', .w_TDLOTDIF)
        .w_TDTIPIMB = IIF(.w_TDFLARCO<>'S' Or Empty(.w_FLCASC), 'N', .w_TDTIPIMB)
        .w_TDCAUPFI = .w_TDCAUPFI
        .DoRTCalc(113,113,.f.)
          if not(empty(.w_TDCAUPFI))
          .link_3_36('Full')
          endif
        .w_TDCAUCOD = .w_TDCAUCOD
        .DoRTCalc(114,114,.f.)
          if not(empty(.w_TDCAUCOD))
          .link_3_37('Full')
          endif
        .w_CODI = .w_TDTIPDOC
        .w_TDFLEXPL = IIF(g_VEFA='S' And .w_TDFLARCO='S' And g_DISB<>'S' Or .w_TDTIPIMB<>'N','S',.w_TDFLEXPL)
        .w_DESC = .w_TDDESDOC
        .w_TDVALCOM = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'N', .w_TDVALCOM)
        .w_TDCOSEPL = 'N'
        .w_TDEXPAUT = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'S', IIF(g_DISB='S',.w_TDEXPAUT,' '))
        .w_TDFLCOMM = IIF(.w_FLCOMM='N' OR .w_FLCOMM=' ', ' ', 'S')
        .w_TDFLCASH = ' '
        .w_TDFLNORC = 'N'
        .w_TDMAXLEV = 99
          .DoRTCalc(125,132,.f.)
        .w_CODI = .w_TDTIPDOC
        .DoRTCalc(134,135,.f.)
          if not(empty(.w_TDMODRIF))
          .link_4_4('Full')
          endif
        .w_DESC = .w_TDDESDOC
        .DoRTCalc(137,138,.f.)
          if not(empty(.w_TDTPNDOC))
          .link_4_9('Full')
          endif
        .DoRTCalc(139,140,.f.)
          if not(empty(.w_TDTPVDOC))
          .link_4_11('Full')
          endif
        .DoRTCalc(141,142,.f.)
          if not(empty(.w_TDTPRDES))
          .link_4_13('Full')
          endif
        .DoRTCalc(143,143,.f.)
          if not(empty(.w_TDESCCL1))
          .link_4_17('Full')
          endif
        .DoRTCalc(144,144,.f.)
          if not(empty(.w_TDESCCL2))
          .link_4_18('Full')
          endif
        .DoRTCalc(145,145,.f.)
          if not(empty(.w_TDESCCL3))
          .link_4_19('Full')
          endif
        .DoRTCalc(146,146,.f.)
          if not(empty(.w_TDESCCL4))
          .link_4_20('Full')
          endif
        .DoRTCalc(147,147,.f.)
          if not(empty(.w_TDESCCL5))
          .link_4_21('Full')
          endif
        .DoRTCalc(148,148,.f.)
          if not(empty(.w_TDSTACL1))
          .link_4_22('Full')
          endif
        .DoRTCalc(149,149,.f.)
          if not(empty(.w_TDSTACL2))
          .link_4_23('Full')
          endif
        .DoRTCalc(150,150,.f.)
          if not(empty(.w_TDSTACL3))
          .link_4_24('Full')
          endif
        .DoRTCalc(151,151,.f.)
          if not(empty(.w_TDSTACL4))
          .link_4_25('Full')
          endif
        .DoRTCalc(152,152,.f.)
          if not(empty(.w_TDSTACL5))
          .link_4_26('Full')
          endif
          .DoRTCalc(153,155,.f.)
        .w_CODI = .w_TDTIPDOC
        .w_DESC = .w_TDDESDOC
          .DoRTCalc(158,158,.f.)
        .w_TDFLSTLM = 'N'
        .w_PRGSTA = 'GSAC_MDV'
        .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
          .DoRTCalc(162,164,.f.)
        .w_TDFLATIP = 'N'
        .w_TDFLGEIN = iif(.w_TDCATDOC='DT',.w_TDFLGEIN,'N')
        .w_DASERIAL = '0000000001'
        .DoRTCalc(167,167,.f.)
          if not(empty(.w_DASERIAL))
          .link_4_40('Full')
          endif
          .DoRTCalc(168,174,.f.)
        .w_TDFLIA01 = 'N'
        .w_TDFLIA02 = 'N'
        .w_TDFLIA03 = 'N'
        .w_TDFLIA04 = 'N'
        .w_TDFLIA05 = 'N'
        .w_TDFLIA06 = 'N'
        .w_TDFLRA01 = IIF(.w_TDFLIA01<>'S', 'N', EVL(NVL(.w_TDFLRA01, 'N'), 'N') )
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        .w_TDFLRA02 = IIF(.w_TDFLIA02<>'S', 'N', EVL(NVL(.w_TDFLRA02, 'N'), 'N') )
        .w_TDFLRA03 = IIF(.w_TDFLIA03<>'S', 'N', EVL(NVL(.w_TDFLRA03, 'N'), 'N') )
        .w_TDFLRA04 = IIF(.w_TDFLIA04<>'S', 'N', EVL(NVL(.w_TDFLRA04, 'N'), 'N') )
        .w_TDFLRA05 = IIF(.w_TDFLIA05<>'S', 'N', EVL(NVL(.w_TDFLRA05, 'N'), 'N') )
        .w_TDFLRA06 = IIF(.w_TDFLIA06<>'S', 'N', EVL(NVL(.w_TDFLRA06, 'N'), 'N') )
        .w_DATOOLTI = "Se attivo: il campo aggiuntivo %1 sar� importato"
          .DoRTCalc(188,192,.f.)
        .w_TDTIPDFE = Space(4)
        .w_TDDESEST = Space(35)
        .w_TDCODCLA = Space(5)
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIP_DOCU')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTDTIPDOC_1_1.enabled = i_bVal
      .Page1.oPag.oTDDESDOC_1_2.enabled = i_bVal
      .Page1.oPag.oTDCATDOC_1_4.enabled = i_bVal
      .Page1.oPag.oTDEMERIC_1_5.enabled = i_bVal
      .Page1.oPag.oTDCAUMAG_1_8.enabled = i_bVal
      .Page1.oPag.oTDCODMAG_1_14.enabled = i_bVal
      .Page1.oPag.oTDFLMGPR_1_15.enabled = i_bVal
      .Page1.oPag.oTDCODMAT_1_16.enabled = i_bVal
      .Page1.oPag.oTDFLMTPR_1_17.enabled = i_bVal
      .Page1.oPag.oTDCAUCON_1_18.enabled = i_bVal
      .Page1.oPag.oTDALFDOC_1_24.enabled = i_bVal
      .Page1.oPag.oTDFLPDOC_1_25.enabled = i_bVal
      .Page1.oPag.oTDSERPRO_1_26.enabled = i_bVal
      .Page1.oPag.oTDFLPPRO_1_27.enabled = i_bVal
      .Page1.oPag.oTDNUMSCO_1_28.enabled = i_bVal
      .Page1.oPag.oTDCODLIS_1_29.enabled = i_bVal
      .Page1.oPag.oTDQTADEF_1_30.enabled = i_bVal
      .Page1.oPag.oTDPROVVI_1_31.enabled = i_bVal
      .Page1.oPag.oTDFLPREF_1_35.enabled = i_bVal
      .Page1.oPag.oTDFLINTE_1_36.enabled = i_bVal
      .Page1.oPag.oTDNOPRSC_1_37.enabled = i_bVal
      .Page1.oPag.oTDFLACCO_1_38.enabled = i_bVal
      .Page1.oPag.oTDFLPACK_1_39.enabled = i_bVal
      .Page1.oPag.oTDFLDTPR_1_45.enabled = i_bVal
      .Page1.oPag.oTDFLBACA_1_46.enabled = i_bVal
      .Page1.oPag.oTDBOLDOG_1_48.enabled = i_bVal
      .Page1.oPag.oTDFLCCAU_1_50.enabled = i_bVal
      .Page1.oPag.oTDMODDES_1_51.enabled = i_bVal
      .Page1.oPag.oTDSEQPRE_1_61.enabled = i_bVal
      .Page1.oPag.oTDSEQSCO_1_62.enabled = i_bVal
      .Page1.oPag.oTDSEQMA1_1_63.enabled = i_bVal
      .Page1.oPag.oTDSEQMA2_1_64.enabled = i_bVal
      .Page2.oPag.oTDFLSILI_2_2.enabled = i_bVal
      .Page2.oPag.oTDPRZVAC_2_3.enabled = i_bVal
      .Page2.oPag.oTDPRZDES_2_4.enabled = i_bVal
      .Page2.oPag.oTDFLVALO_2_5.enabled = i_bVal
      .Page2.oPag.oTDCHKTOT_2_6.enabled = i_bVal
      .Page2.oPag.oTDFLBLEV_2_7.enabled = i_bVal
      .Page2.oPag.oTDFLSPIN_2_8.enabled = i_bVal
      .Page2.oPag.oTDFLQRIO_2_9.enabled = i_bVal
      .Page2.oPag.oTDFLPREV_2_10.enabled = i_bVal
      .Page2.oPag.oTDFLAPCA_2_11.enabled = i_bVal
      .Page2.oPag.oTDRIPCON_2_12.enabled = i_bVal
      .Page2.oPag.oTDNOSTCO_2_13.enabled = i_bVal
      .Page2.oPag.oTDFLGETR_2_14.enabled = i_bVal
      .Page2.oPag.oTDFLSPIM_2_15.enabled = i_bVal
      .Page2.oPag.oTDFLSPTR_2_18.enabled = i_bVal
      .Page2.oPag.oTDRIPINC_2_21.enabled = i_bVal
      .Page2.oPag.oTDRIPIMB_2_22.enabled = i_bVal
      .Page2.oPag.oTDRIPTRA_2_23.enabled = i_bVal
      .Page2.oPag.oTDMCALSI_2_24.enabled = i_bVal
      .Page2.oPag.oTDMCALST_2_25.enabled = i_bVal
      .Page2.oPag.oTDSINCFL_2_26.enabled = i_bVal
      .Page2.oPag.oTDFLRISC_2_30.enabled = i_bVal
      .Page3.oPag.oTDFLANAL_3_25.enabled = i_bVal
      .Page3.oPag.oTDFLELAN_3_26.enabled = i_bVal
      .Page3.oPag.oTDVOCECR_3_27.enabled = i_bVal
      .Page3.oPag.oTD_SEGNO_3_28.enabled = i_bVal
      .Page3.oPag.oTDASSCES_3_29.enabled = i_bVal
      .Page3.oPag.oTDCAUCES_3_30.enabled = i_bVal
      .Page3.oPag.oTDCONASS_3_31.enabled = i_bVal
      .Page3.oPag.oTDCODSTR_3_32.enabled = i_bVal
      .Page3.oPag.oTDFLARCO_3_33.enabled = i_bVal
      .Page3.oPag.oTDLOTDIF_3_34.enabled = i_bVal
      .Page3.oPag.oTDTIPIMB_3_35.enabled = i_bVal
      .Page3.oPag.oTDCAUPFI_3_36.enabled = i_bVal
      .Page3.oPag.oTDCAUCOD_3_37.enabled = i_bVal
      .Page3.oPag.oTDFLEXPL_3_40.enabled = i_bVal
      .Page3.oPag.oTDVALCOM_3_42.enabled = i_bVal
      .Page3.oPag.oTDCOSEPL_3_43.enabled = i_bVal
      .Page3.oPag.oTDEXPAUT_3_44.enabled = i_bVal
      .Page3.oPag.oTDFLCOMM_3_45.enabled = i_bVal
      .Page3.oPag.oTDFLCASH_3_46.enabled = i_bVal
      .Page3.oPag.oTDFLNORC_3_47.enabled = i_bVal
      .Page3.oPag.oTDMAXLEV_3_48.enabled = i_bVal
      .Page4.oPag.oTDDESRIF_4_3.enabled = i_bVal
      .Page4.oPag.oTDMODRIF_4_4.enabled = i_bVal
      .Page4.oPag.oTDFLNSRI_4_8.enabled = i_bVal
      .Page4.oPag.oTDTPNDOC_4_9.enabled = i_bVal
      .Page4.oPag.oTDFLVSRI_4_10.enabled = i_bVal
      .Page4.oPag.oTDTPVDOC_4_11.enabled = i_bVal
      .Page4.oPag.oTDFLRIDE_4_12.enabled = i_bVal
      .Page4.oPag.oTDTPRDES_4_13.enabled = i_bVal
      .Page4.oPag.oTDESCCL1_4_17.enabled = i_bVal
      .Page4.oPag.oTDESCCL2_4_18.enabled = i_bVal
      .Page4.oPag.oTDESCCL3_4_19.enabled = i_bVal
      .Page4.oPag.oTDESCCL4_4_20.enabled = i_bVal
      .Page4.oPag.oTDESCCL5_4_21.enabled = i_bVal
      .Page4.oPag.oTDSTACL1_4_22.enabled = i_bVal
      .Page4.oPag.oTDSTACL2_4_23.enabled = i_bVal
      .Page4.oPag.oTDSTACL3_4_24.enabled = i_bVal
      .Page4.oPag.oTDSTACL4_4_25.enabled = i_bVal
      .Page4.oPag.oTDSTACL5_4_26.enabled = i_bVal
      .Page4.oPag.oTDFLIMPA_4_36.enabled = i_bVal
      .Page4.oPag.oTDFLIMAC_4_37.enabled = i_bVal
      .Page5.oPag.oTDFLNSTA_5_4.enabled = i_bVal
      .Page5.oPag.oTDFLSTLM_5_7.enabled = i_bVal
      .Page1.oPag.oTDFLGEIN_1_96.enabled = i_bVal
      .Page4.oPag.oTDFLIA01_4_48.enabled = i_bVal
      .Page4.oPag.oTDFLIA02_4_49.enabled = i_bVal
      .Page4.oPag.oTDFLIA03_4_50.enabled = i_bVal
      .Page4.oPag.oTDFLIA04_4_51.enabled = i_bVal
      .Page4.oPag.oTDFLIA05_4_52.enabled = i_bVal
      .Page4.oPag.oTDFLIA06_4_53.enabled = i_bVal
      .Page4.oPag.oTDFLRA01_4_55.enabled = i_bVal
      .Page4.oPag.oTDFLRA02_4_65.enabled = i_bVal
      .Page4.oPag.oTDFLRA03_4_66.enabled = i_bVal
      .Page4.oPag.oTDFLRA04_4_67.enabled = i_bVal
      .Page4.oPag.oTDFLRA05_4_68.enabled = i_bVal
      .Page4.oPag.oTDFLRA06_4_69.enabled = i_bVal
      .Page3.oPag.oTDTIPDFE_3_61.enabled = i_bVal
      .Page3.oPag.oTDDESEST_3_62.enabled = i_bVal
      .Page3.oPag.oTDCODCLA_3_63.enabled = i_bVal
      .Page2.oPag.oBtn_2_33.enabled = .Page2.oPag.oBtn_2_33.mCond()
      .Page1.oPag.oObj_1_84.enabled = i_bVal
      .Page1.oPag.oObj_1_85.enabled = i_bVal
      .Page1.oPag.oObj_1_89.enabled = i_bVal
      .Page2.oPag.oObj_2_1.enabled = i_bVal
      .Page4.oPag.CAMAGG01.enabled = i_bVal
      .Page4.oPag.CAMAGG02.enabled = i_bVal
      .Page4.oPag.CAMAGG03.enabled = i_bVal
      .Page4.oPag.CAMAGG04.enabled = i_bVal
      .Page4.oPag.CAMAGG05.enabled = i_bVal
      .Page4.oPag.CAMAGG06.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTDTIPDOC_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTDTIPDOC_1_1.enabled = .t.
      endif
    endwith
    this.GSAC_MDC.SetStatus(i_cOp)
    this.GSVE_MTD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TIP_DOCU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAC_MDC.SetChildrenStatus(i_cOp)
  *  this.GSVE_MTD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPDOC,"TDTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESDOC,"TDDESDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVEAC,"TDFLVEAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCATDOC,"TDCATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEMERIC,"TDEMERIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TFFLGEFA,"TFFLGEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRODOC,"TDPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUMAG,"TDCAUMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODMAG,"TDCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLMGPR,"TDFLMGPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODMAT,"TDCODMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLMTPR,"TDFLMTPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCON,"TDCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDALFDOC,"TDALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPDOC,"TDFLPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSERPRO,"TDSERPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPPRO,"TDFLPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNUMSCO,"TDNUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODLIS,"TDCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDQTADEF,"TDQTADEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPROVVI,"TDPROVVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDASPETT,"TDASPETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPREF,"TDFLPREF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLINTE,"TDFLINTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNOPRSC,"TDNOPRSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLACCO,"TDFLACCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPACK,"TDFLPACK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLDATT,"TDFLDATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLORAT,"TDFLORAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TFFLRAGG,"TFFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCRIS,"TDFLCRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPROV,"TDFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLDTPR,"TDFLDTPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLBACA,"TDFLBACA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLESPF,"TDFLESPF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDBOLDOG,"TDBOLDOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCCAU,"TDFLCCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMODDES,"TDMODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQPRE,"TDSEQPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQSCO,"TDSEQSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQMA1,"TDSEQMA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSEQMA2,"TDSEQMA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUPFI,"TDCAUPFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCOD,"TDCAUCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLEXPL,"TDFLEXPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPROSTA,"TDPROSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEXPAUT,"TDEXPAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSILI,"TDFLSILI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRZVAC,"TDPRZVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDPRZDES,"TDPRZDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVALO,"TDFLVALO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCHKTOT,"TDCHKTOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLBLEV,"TDFLBLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPIN,"TDFLSPIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLQRIO,"TDFLQRIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLPREV,"TDFLPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLAPCA,"TDFLAPCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPCON,"TDRIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDNOSTCO,"TDNOSTCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLGETR,"TDFLGETR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPIM,"TDFLSPIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSPTR,"TDFLSPTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPINC,"TDRIPINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPIMB,"TDRIPIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDRIPTRA,"TDRIPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALSI,"TDMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMCALST,"TDMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSINCFL,"TDSINCFL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRISC,"TDFLRISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLANAL,"TDFLANAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLELAN,"TDFLELAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDVOCECR,"TDVOCECR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TD_SEGNO,"TD_SEGNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDASSCES,"TDASSCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCES,"TDCAUCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCONASS,"TDCONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODSTR,"TDCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLARCO,"TDFLARCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDLOTDIF,"TDLOTDIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPIMB,"TDTIPIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUPFI,"TDCAUPFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCAUCOD,"TDCAUCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLEXPL,"TDFLEXPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDVALCOM,"TDVALCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCOSEPL,"TDCOSEPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDEXPAUT,"TDEXPAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCOMM,"TDFLCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLCASH,"TDFLCASH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNORC,"TDFLNORC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMAXLEV,"TDMAXLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESRIF,"TDDESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMODRIF,"TDMODRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNSRI,"TDFLNSRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPNDOC,"TDTPNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLVSRI,"TDFLVSRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPVDOC,"TDTPVDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRIDE,"TDFLRIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTPRDES,"TDTPRDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL1,"TDESCCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL2,"TDESCCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL3,"TDESCCL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL4,"TDESCCL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDESCCL5,"TDESCCL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL1,"TDSTACL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL2,"TDSTACL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL3,"TDSTACL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL4,"TDSTACL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDSTACL5,"TDSTACL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIMPA,"TDFLIMPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIMAC,"TDFLIMAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLNSTA,"TDFLNSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLSTLM,"TDFLSTLM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMINVAL,"TDMINVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDMINIMP,"TDMINIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLATIP,"TDFLATIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLGEIN,"TDFLGEIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA01,"TDFLIA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA02,"TDFLIA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA03,"TDFLIA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA04,"TDFLIA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA05,"TDFLIA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLIA06,"TDFLIA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA01,"TDFLRA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA02,"TDFLRA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA03,"TDFLRA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA04,"TDFLRA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA05,"TDFLRA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDFLRA06,"TDFLRA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDTIPDFE,"TDTIPDFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESEST,"TDDESEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODCLA,"TDCODCLA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsac_atd
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo e Categoria
    IF NOT EMPTY(i_cWhere)
       IF AT('TDFLVEAC', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and TDFLVEAC='A'"
       ENDIF
       IF AT('TDCATDOC', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and TDCATDOC<>'OR'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    i_lTable = "TIP_DOCU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIP_DOCU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_SCD with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIP_DOCU_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIP_DOCU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIP_DOCU')
        i_extval=cp_InsertValODBCExtFlds(this,'TIP_DOCU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDEMERIC"+;
                  ",TFFLGEFA,TDPRODOC,TDCAUMAG,TDCODMAG,TDFLMGPR"+;
                  ",TDCODMAT,TDFLMTPR,TDCAUCON,TDALFDOC,TDFLPDOC"+;
                  ",TDSERPRO,TDFLPPRO,TDNUMSCO,TDCODLIS,TDQTADEF"+;
                  ",TDPROVVI,TDASPETT,TDFLPREF,TDFLINTE,TDNOPRSC"+;
                  ",TDFLACCO,TDFLPACK,TDFLDATT,TDFLORAT,TFFLRAGG"+;
                  ",TDFLCRIS,TDFLPROV,TDFLDTPR,TDFLBACA,TDFLESPF"+;
                  ",TDBOLDOG,TDFLCCAU,TDMODDES,TDSEQPRE,TDSEQSCO"+;
                  ",TDSEQMA1,TDSEQMA2,TDCAUPFI,TDCAUCOD,TDFLEXPL"+;
                  ",TDPROSTA,TDEXPAUT,TDFLSILI,TDPRZVAC,TDPRZDES"+;
                  ",TDFLVALO,TDCHKTOT,TDFLBLEV,TDFLSPIN,TDFLQRIO"+;
                  ",TDFLPREV,TDFLAPCA,TDRIPCON,TDNOSTCO,TDFLGETR"+;
                  ",TDFLSPIM,TDFLSPTR,TDRIPINC,TDRIPIMB,TDRIPTRA"+;
                  ",TDMCALSI,TDMCALST,TDSINCFL,TDFLRISC,TDFLANAL"+;
                  ",TDFLELAN,TDVOCECR,TD_SEGNO,TDASSCES,TDCAUCES"+;
                  ",TDCONASS,TDCODSTR,TDFLARCO,TDLOTDIF,TDTIPIMB"+;
                  ",TDVALCOM,TDCOSEPL,TDFLCOMM,TDFLCASH,TDFLNORC"+;
                  ",TDMAXLEV,TDDESRIF,TDMODRIF,TDFLNSRI,TDTPNDOC"+;
                  ",TDFLVSRI,TDTPVDOC,TDFLRIDE,TDTPRDES,TDESCCL1"+;
                  ",TDESCCL2,TDESCCL3,TDESCCL4,TDESCCL5,TDSTACL1"+;
                  ",TDSTACL2,TDSTACL3,TDSTACL4,TDSTACL5,TDFLIMPA"+;
                  ",TDFLIMAC,TDFLNSTA,TDFLSTLM,TDMINVAL,TDMINIMP"+;
                  ",TDFLATIP,TDFLGEIN,TDFLIA01,TDFLIA02,TDFLIA03"+;
                  ",TDFLIA04,TDFLIA05,TDFLIA06,TDFLRA01,TDFLRA02"+;
                  ",TDFLRA03,TDFLRA04,TDFLRA05,TDFLRA06,UTCC"+;
                  ",UTCV,UTDC,UTDV,TDTIPDFE,TDDESEST"+;
                  ",TDCODCLA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TDTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_TDDESDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLVEAC)+;
                  ","+cp_ToStrODBC(this.w_TDCATDOC)+;
                  ","+cp_ToStrODBC(this.w_TDEMERIC)+;
                  ","+cp_ToStrODBC(this.w_TFFLGEFA)+;
                  ","+cp_ToStrODBC(this.w_TDPRODOC)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUMAG)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODMAG)+;
                  ","+cp_ToStrODBC(this.w_TDFLMGPR)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODMAT)+;
                  ","+cp_ToStrODBC(this.w_TDFLMTPR)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCON)+;
                  ","+cp_ToStrODBC(this.w_TDALFDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLPDOC)+;
                  ","+cp_ToStrODBC(this.w_TDSERPRO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPPRO)+;
                  ","+cp_ToStrODBC(this.w_TDNUMSCO)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODLIS)+;
                  ","+cp_ToStrODBC(this.w_TDQTADEF)+;
                  ","+cp_ToStrODBC(this.w_TDPROVVI)+;
                  ","+cp_ToStrODBC(this.w_TDASPETT)+;
                  ","+cp_ToStrODBC(this.w_TDFLPREF)+;
                  ","+cp_ToStrODBC(this.w_TDFLINTE)+;
                  ","+cp_ToStrODBC(this.w_TDNOPRSC)+;
                  ","+cp_ToStrODBC(this.w_TDFLACCO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPACK)+;
                  ","+cp_ToStrODBC(this.w_TDFLDATT)+;
                  ","+cp_ToStrODBC(this.w_TDFLORAT)+;
                  ","+cp_ToStrODBC(this.w_TFFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_TDFLCRIS)+;
                  ","+cp_ToStrODBC(this.w_TDFLPROV)+;
                  ","+cp_ToStrODBC(this.w_TDFLDTPR)+;
                  ","+cp_ToStrODBC(this.w_TDFLBACA)+;
                  ","+cp_ToStrODBC(this.w_TDFLESPF)+;
                  ","+cp_ToStrODBC(this.w_TDBOLDOG)+;
                  ","+cp_ToStrODBC(this.w_TDFLCCAU)+;
                  ","+cp_ToStrODBC(this.w_TDMODDES)+;
                  ","+cp_ToStrODBC(this.w_TDSEQPRE)+;
                  ","+cp_ToStrODBC(this.w_TDSEQSCO)+;
                  ","+cp_ToStrODBC(this.w_TDSEQMA1)+;
                  ","+cp_ToStrODBC(this.w_TDSEQMA2)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUPFI)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCOD)+;
                  ","+cp_ToStrODBC(this.w_TDFLEXPL)+;
                  ","+cp_ToStrODBC(this.w_TDPROSTA)+;
                  ","+cp_ToStrODBC(this.w_TDEXPAUT)+;
                  ","+cp_ToStrODBC(this.w_TDFLSILI)+;
                  ","+cp_ToStrODBC(this.w_TDPRZVAC)+;
                  ","+cp_ToStrODBC(this.w_TDPRZDES)+;
                  ","+cp_ToStrODBC(this.w_TDFLVALO)+;
                  ","+cp_ToStrODBC(this.w_TDCHKTOT)+;
                  ","+cp_ToStrODBC(this.w_TDFLBLEV)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPIN)+;
                  ","+cp_ToStrODBC(this.w_TDFLQRIO)+;
                  ","+cp_ToStrODBC(this.w_TDFLPREV)+;
                  ","+cp_ToStrODBC(this.w_TDFLAPCA)+;
                  ","+cp_ToStrODBC(this.w_TDRIPCON)+;
                  ","+cp_ToStrODBC(this.w_TDNOSTCO)+;
                  ","+cp_ToStrODBC(this.w_TDFLGETR)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPIM)+;
                  ","+cp_ToStrODBC(this.w_TDFLSPTR)+;
                  ","+cp_ToStrODBC(this.w_TDRIPINC)+;
                  ","+cp_ToStrODBC(this.w_TDRIPIMB)+;
                  ","+cp_ToStrODBC(this.w_TDRIPTRA)+;
                  ","+cp_ToStrODBCNull(this.w_TDMCALSI)+;
                  ","+cp_ToStrODBCNull(this.w_TDMCALST)+;
                  ","+cp_ToStrODBC(this.w_TDSINCFL)+;
                  ","+cp_ToStrODBC(this.w_TDFLRISC)+;
                  ","+cp_ToStrODBC(this.w_TDFLANAL)+;
                  ","+cp_ToStrODBC(this.w_TDFLELAN)+;
                  ","+cp_ToStrODBC(this.w_TDVOCECR)+;
                  ","+cp_ToStrODBC(this.w_TD_SEGNO)+;
                  ","+cp_ToStrODBC(this.w_TDASSCES)+;
                  ","+cp_ToStrODBCNull(this.w_TDCAUCES)+;
                  ","+cp_ToStrODBC(this.w_TDCONASS)+;
                  ","+cp_ToStrODBCNull(this.w_TDCODSTR)+;
                  ","+cp_ToStrODBC(this.w_TDFLARCO)+;
                  ","+cp_ToStrODBC(this.w_TDLOTDIF)+;
                  ","+cp_ToStrODBC(this.w_TDTIPIMB)+;
                  ","+cp_ToStrODBC(this.w_TDVALCOM)+;
                  ","+cp_ToStrODBC(this.w_TDCOSEPL)+;
                  ","+cp_ToStrODBC(this.w_TDFLCOMM)+;
                  ","+cp_ToStrODBC(this.w_TDFLCASH)+;
                  ","+cp_ToStrODBC(this.w_TDFLNORC)+;
                  ","+cp_ToStrODBC(this.w_TDMAXLEV)+;
                  ","+cp_ToStrODBC(this.w_TDDESRIF)+;
                  ","+cp_ToStrODBCNull(this.w_TDMODRIF)+;
                  ","+cp_ToStrODBC(this.w_TDFLNSRI)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPNDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLVSRI)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPVDOC)+;
                  ","+cp_ToStrODBC(this.w_TDFLRIDE)+;
                  ","+cp_ToStrODBCNull(this.w_TDTPRDES)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL1)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL2)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL3)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL4)+;
                  ","+cp_ToStrODBCNull(this.w_TDESCCL5)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL1)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL2)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL3)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL4)+;
                  ","+cp_ToStrODBCNull(this.w_TDSTACL5)+;
                  ","+cp_ToStrODBC(this.w_TDFLIMPA)+;
                  ","+cp_ToStrODBC(this.w_TDFLIMAC)+;
                  ","+cp_ToStrODBC(this.w_TDFLNSTA)+;
                  ","+cp_ToStrODBC(this.w_TDFLSTLM)+;
                  ","+cp_ToStrODBC(this.w_TDMINVAL)+;
                  ","+cp_ToStrODBC(this.w_TDMINIMP)+;
                  ","+cp_ToStrODBC(this.w_TDFLATIP)+;
                  ","+cp_ToStrODBC(this.w_TDFLGEIN)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA01)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA02)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA03)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA04)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA05)+;
                  ","+cp_ToStrODBC(this.w_TDFLIA06)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA01)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA02)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA03)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA04)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA05)+;
                  ","+cp_ToStrODBC(this.w_TDFLRA06)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_TDTIPDFE)+;
                  ","+cp_ToStrODBC(this.w_TDDESEST)+;
                  ","+cp_ToStrODBC(this.w_TDCODCLA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIP_DOCU')
        i_extval=cp_InsertValVFPExtFlds(this,'TIP_DOCU')
        cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',this.w_TDTIPDOC)
        INSERT INTO (i_cTable);
              (TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDEMERIC,TFFLGEFA,TDPRODOC,TDCAUMAG,TDCODMAG,TDFLMGPR,TDCODMAT,TDFLMTPR,TDCAUCON,TDALFDOC,TDFLPDOC,TDSERPRO,TDFLPPRO,TDNUMSCO,TDCODLIS,TDQTADEF,TDPROVVI,TDASPETT,TDFLPREF,TDFLINTE,TDNOPRSC,TDFLACCO,TDFLPACK,TDFLDATT,TDFLORAT,TFFLRAGG,TDFLCRIS,TDFLPROV,TDFLDTPR,TDFLBACA,TDFLESPF,TDBOLDOG,TDFLCCAU,TDMODDES,TDSEQPRE,TDSEQSCO,TDSEQMA1,TDSEQMA2,TDCAUPFI,TDCAUCOD,TDFLEXPL,TDPROSTA,TDEXPAUT,TDFLSILI,TDPRZVAC,TDPRZDES,TDFLVALO,TDCHKTOT,TDFLBLEV,TDFLSPIN,TDFLQRIO,TDFLPREV,TDFLAPCA,TDRIPCON,TDNOSTCO,TDFLGETR,TDFLSPIM,TDFLSPTR,TDRIPINC,TDRIPIMB,TDRIPTRA,TDMCALSI,TDMCALST,TDSINCFL,TDFLRISC,TDFLANAL,TDFLELAN,TDVOCECR,TD_SEGNO,TDASSCES,TDCAUCES,TDCONASS,TDCODSTR,TDFLARCO,TDLOTDIF,TDTIPIMB,TDVALCOM,TDCOSEPL,TDFLCOMM,TDFLCASH,TDFLNORC,TDMAXLEV,TDDESRIF,TDMODRIF,TDFLNSRI,TDTPNDOC,TDFLVSRI,TDTPVDOC,TDFLRIDE,TDTPRDES,TDESCCL1,TDESCCL2,TDESCCL3,TDESCCL4,TDESCCL5,TDSTACL1,TDSTACL2,TDSTACL3,TDSTACL4,TDSTACL5,TDFLIMPA,TDFLIMAC,TDFLNSTA,TDFLSTLM,TDMINVAL,TDMINIMP,TDFLATIP,TDFLGEIN,TDFLIA01,TDFLIA02,TDFLIA03,TDFLIA04,TDFLIA05,TDFLIA06,TDFLRA01,TDFLRA02,TDFLRA03,TDFLRA04,TDFLRA05,TDFLRA06,UTCC,UTCV,UTDC,UTDV,TDTIPDFE,TDDESEST,TDCODCLA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TDTIPDOC;
                  ,this.w_TDDESDOC;
                  ,this.w_TDFLVEAC;
                  ,this.w_TDCATDOC;
                  ,this.w_TDEMERIC;
                  ,this.w_TFFLGEFA;
                  ,this.w_TDPRODOC;
                  ,this.w_TDCAUMAG;
                  ,this.w_TDCODMAG;
                  ,this.w_TDFLMGPR;
                  ,this.w_TDCODMAT;
                  ,this.w_TDFLMTPR;
                  ,this.w_TDCAUCON;
                  ,this.w_TDALFDOC;
                  ,this.w_TDFLPDOC;
                  ,this.w_TDSERPRO;
                  ,this.w_TDFLPPRO;
                  ,this.w_TDNUMSCO;
                  ,this.w_TDCODLIS;
                  ,this.w_TDQTADEF;
                  ,this.w_TDPROVVI;
                  ,this.w_TDASPETT;
                  ,this.w_TDFLPREF;
                  ,this.w_TDFLINTE;
                  ,this.w_TDNOPRSC;
                  ,this.w_TDFLACCO;
                  ,this.w_TDFLPACK;
                  ,this.w_TDFLDATT;
                  ,this.w_TDFLORAT;
                  ,this.w_TFFLRAGG;
                  ,this.w_TDFLCRIS;
                  ,this.w_TDFLPROV;
                  ,this.w_TDFLDTPR;
                  ,this.w_TDFLBACA;
                  ,this.w_TDFLESPF;
                  ,this.w_TDBOLDOG;
                  ,this.w_TDFLCCAU;
                  ,this.w_TDMODDES;
                  ,this.w_TDSEQPRE;
                  ,this.w_TDSEQSCO;
                  ,this.w_TDSEQMA1;
                  ,this.w_TDSEQMA2;
                  ,this.w_TDCAUPFI;
                  ,this.w_TDCAUCOD;
                  ,this.w_TDFLEXPL;
                  ,this.w_TDPROSTA;
                  ,this.w_TDEXPAUT;
                  ,this.w_TDFLSILI;
                  ,this.w_TDPRZVAC;
                  ,this.w_TDPRZDES;
                  ,this.w_TDFLVALO;
                  ,this.w_TDCHKTOT;
                  ,this.w_TDFLBLEV;
                  ,this.w_TDFLSPIN;
                  ,this.w_TDFLQRIO;
                  ,this.w_TDFLPREV;
                  ,this.w_TDFLAPCA;
                  ,this.w_TDRIPCON;
                  ,this.w_TDNOSTCO;
                  ,this.w_TDFLGETR;
                  ,this.w_TDFLSPIM;
                  ,this.w_TDFLSPTR;
                  ,this.w_TDRIPINC;
                  ,this.w_TDRIPIMB;
                  ,this.w_TDRIPTRA;
                  ,this.w_TDMCALSI;
                  ,this.w_TDMCALST;
                  ,this.w_TDSINCFL;
                  ,this.w_TDFLRISC;
                  ,this.w_TDFLANAL;
                  ,this.w_TDFLELAN;
                  ,this.w_TDVOCECR;
                  ,this.w_TD_SEGNO;
                  ,this.w_TDASSCES;
                  ,this.w_TDCAUCES;
                  ,this.w_TDCONASS;
                  ,this.w_TDCODSTR;
                  ,this.w_TDFLARCO;
                  ,this.w_TDLOTDIF;
                  ,this.w_TDTIPIMB;
                  ,this.w_TDVALCOM;
                  ,this.w_TDCOSEPL;
                  ,this.w_TDFLCOMM;
                  ,this.w_TDFLCASH;
                  ,this.w_TDFLNORC;
                  ,this.w_TDMAXLEV;
                  ,this.w_TDDESRIF;
                  ,this.w_TDMODRIF;
                  ,this.w_TDFLNSRI;
                  ,this.w_TDTPNDOC;
                  ,this.w_TDFLVSRI;
                  ,this.w_TDTPVDOC;
                  ,this.w_TDFLRIDE;
                  ,this.w_TDTPRDES;
                  ,this.w_TDESCCL1;
                  ,this.w_TDESCCL2;
                  ,this.w_TDESCCL3;
                  ,this.w_TDESCCL4;
                  ,this.w_TDESCCL5;
                  ,this.w_TDSTACL1;
                  ,this.w_TDSTACL2;
                  ,this.w_TDSTACL3;
                  ,this.w_TDSTACL4;
                  ,this.w_TDSTACL5;
                  ,this.w_TDFLIMPA;
                  ,this.w_TDFLIMAC;
                  ,this.w_TDFLNSTA;
                  ,this.w_TDFLSTLM;
                  ,this.w_TDMINVAL;
                  ,this.w_TDMINIMP;
                  ,this.w_TDFLATIP;
                  ,this.w_TDFLGEIN;
                  ,this.w_TDFLIA01;
                  ,this.w_TDFLIA02;
                  ,this.w_TDFLIA03;
                  ,this.w_TDFLIA04;
                  ,this.w_TDFLIA05;
                  ,this.w_TDFLIA06;
                  ,this.w_TDFLRA01;
                  ,this.w_TDFLRA02;
                  ,this.w_TDFLRA03;
                  ,this.w_TDFLRA04;
                  ,this.w_TDFLRA05;
                  ,this.w_TDFLRA06;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_TDTIPDFE;
                  ,this.w_TDDESEST;
                  ,this.w_TDCODCLA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIP_DOCU_IDX,i_nConn)
      *
      * update TIP_DOCU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIP_DOCU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TDDESDOC="+cp_ToStrODBC(this.w_TDDESDOC)+;
             ",TDFLVEAC="+cp_ToStrODBC(this.w_TDFLVEAC)+;
             ",TDCATDOC="+cp_ToStrODBC(this.w_TDCATDOC)+;
             ",TDEMERIC="+cp_ToStrODBC(this.w_TDEMERIC)+;
             ",TFFLGEFA="+cp_ToStrODBC(this.w_TFFLGEFA)+;
             ",TDPRODOC="+cp_ToStrODBC(this.w_TDPRODOC)+;
             ",TDCAUMAG="+cp_ToStrODBCNull(this.w_TDCAUMAG)+;
             ",TDCODMAG="+cp_ToStrODBCNull(this.w_TDCODMAG)+;
             ",TDFLMGPR="+cp_ToStrODBC(this.w_TDFLMGPR)+;
             ",TDCODMAT="+cp_ToStrODBCNull(this.w_TDCODMAT)+;
             ",TDFLMTPR="+cp_ToStrODBC(this.w_TDFLMTPR)+;
             ",TDCAUCON="+cp_ToStrODBCNull(this.w_TDCAUCON)+;
             ",TDALFDOC="+cp_ToStrODBC(this.w_TDALFDOC)+;
             ",TDFLPDOC="+cp_ToStrODBC(this.w_TDFLPDOC)+;
             ",TDSERPRO="+cp_ToStrODBC(this.w_TDSERPRO)+;
             ",TDFLPPRO="+cp_ToStrODBC(this.w_TDFLPPRO)+;
             ",TDNUMSCO="+cp_ToStrODBC(this.w_TDNUMSCO)+;
             ",TDCODLIS="+cp_ToStrODBCNull(this.w_TDCODLIS)+;
             ",TDQTADEF="+cp_ToStrODBC(this.w_TDQTADEF)+;
             ",TDPROVVI="+cp_ToStrODBC(this.w_TDPROVVI)+;
             ",TDASPETT="+cp_ToStrODBC(this.w_TDASPETT)+;
             ",TDFLPREF="+cp_ToStrODBC(this.w_TDFLPREF)+;
             ",TDFLINTE="+cp_ToStrODBC(this.w_TDFLINTE)+;
             ",TDNOPRSC="+cp_ToStrODBC(this.w_TDNOPRSC)+;
             ",TDFLACCO="+cp_ToStrODBC(this.w_TDFLACCO)+;
             ",TDFLPACK="+cp_ToStrODBC(this.w_TDFLPACK)+;
             ",TDFLDATT="+cp_ToStrODBC(this.w_TDFLDATT)+;
             ",TDFLORAT="+cp_ToStrODBC(this.w_TDFLORAT)+;
             ",TFFLRAGG="+cp_ToStrODBC(this.w_TFFLRAGG)+;
             ",TDFLCRIS="+cp_ToStrODBC(this.w_TDFLCRIS)+;
             ",TDFLPROV="+cp_ToStrODBC(this.w_TDFLPROV)+;
             ",TDFLDTPR="+cp_ToStrODBC(this.w_TDFLDTPR)+;
             ",TDFLBACA="+cp_ToStrODBC(this.w_TDFLBACA)+;
             ",TDFLESPF="+cp_ToStrODBC(this.w_TDFLESPF)+;
             ",TDBOLDOG="+cp_ToStrODBC(this.w_TDBOLDOG)+;
             ",TDFLCCAU="+cp_ToStrODBC(this.w_TDFLCCAU)+;
             ",TDMODDES="+cp_ToStrODBC(this.w_TDMODDES)+;
             ",TDSEQPRE="+cp_ToStrODBC(this.w_TDSEQPRE)+;
             ",TDSEQSCO="+cp_ToStrODBC(this.w_TDSEQSCO)+;
             ",TDSEQMA1="+cp_ToStrODBC(this.w_TDSEQMA1)+;
             ",TDSEQMA2="+cp_ToStrODBC(this.w_TDSEQMA2)+;
             ",TDCAUPFI="+cp_ToStrODBCNull(this.w_TDCAUPFI)+;
             ",TDCAUCOD="+cp_ToStrODBCNull(this.w_TDCAUCOD)+;
             ",TDFLEXPL="+cp_ToStrODBC(this.w_TDFLEXPL)+;
             ",TDPROSTA="+cp_ToStrODBC(this.w_TDPROSTA)+;
             ",TDEXPAUT="+cp_ToStrODBC(this.w_TDEXPAUT)+;
             ",TDFLSILI="+cp_ToStrODBC(this.w_TDFLSILI)+;
             ",TDPRZVAC="+cp_ToStrODBC(this.w_TDPRZVAC)+;
             ",TDPRZDES="+cp_ToStrODBC(this.w_TDPRZDES)+;
             ",TDFLVALO="+cp_ToStrODBC(this.w_TDFLVALO)+;
             ",TDCHKTOT="+cp_ToStrODBC(this.w_TDCHKTOT)+;
             ",TDFLBLEV="+cp_ToStrODBC(this.w_TDFLBLEV)+;
             ",TDFLSPIN="+cp_ToStrODBC(this.w_TDFLSPIN)+;
             ",TDFLQRIO="+cp_ToStrODBC(this.w_TDFLQRIO)+;
             ",TDFLPREV="+cp_ToStrODBC(this.w_TDFLPREV)+;
             ",TDFLAPCA="+cp_ToStrODBC(this.w_TDFLAPCA)+;
             ",TDRIPCON="+cp_ToStrODBC(this.w_TDRIPCON)+;
             ",TDNOSTCO="+cp_ToStrODBC(this.w_TDNOSTCO)+;
             ",TDFLGETR="+cp_ToStrODBC(this.w_TDFLGETR)+;
             ",TDFLSPIM="+cp_ToStrODBC(this.w_TDFLSPIM)+;
             ",TDFLSPTR="+cp_ToStrODBC(this.w_TDFLSPTR)+;
             ",TDRIPINC="+cp_ToStrODBC(this.w_TDRIPINC)+;
             ",TDRIPIMB="+cp_ToStrODBC(this.w_TDRIPIMB)+;
             ",TDRIPTRA="+cp_ToStrODBC(this.w_TDRIPTRA)+;
             ",TDMCALSI="+cp_ToStrODBCNull(this.w_TDMCALSI)+;
             ",TDMCALST="+cp_ToStrODBCNull(this.w_TDMCALST)+;
             ",TDSINCFL="+cp_ToStrODBC(this.w_TDSINCFL)+;
             ",TDFLRISC="+cp_ToStrODBC(this.w_TDFLRISC)+;
             ",TDFLANAL="+cp_ToStrODBC(this.w_TDFLANAL)+;
             ",TDFLELAN="+cp_ToStrODBC(this.w_TDFLELAN)+;
             ",TDVOCECR="+cp_ToStrODBC(this.w_TDVOCECR)+;
             ",TD_SEGNO="+cp_ToStrODBC(this.w_TD_SEGNO)+;
             ",TDASSCES="+cp_ToStrODBC(this.w_TDASSCES)+;
             ",TDCAUCES="+cp_ToStrODBCNull(this.w_TDCAUCES)+;
             ",TDCONASS="+cp_ToStrODBC(this.w_TDCONASS)+;
             ",TDCODSTR="+cp_ToStrODBCNull(this.w_TDCODSTR)+;
             ",TDFLARCO="+cp_ToStrODBC(this.w_TDFLARCO)+;
             ",TDLOTDIF="+cp_ToStrODBC(this.w_TDLOTDIF)+;
             ",TDTIPIMB="+cp_ToStrODBC(this.w_TDTIPIMB)+;
             ",TDVALCOM="+cp_ToStrODBC(this.w_TDVALCOM)+;
             ",TDCOSEPL="+cp_ToStrODBC(this.w_TDCOSEPL)+;
             ",TDFLCOMM="+cp_ToStrODBC(this.w_TDFLCOMM)+;
             ",TDFLCASH="+cp_ToStrODBC(this.w_TDFLCASH)+;
             ",TDFLNORC="+cp_ToStrODBC(this.w_TDFLNORC)+;
             ",TDMAXLEV="+cp_ToStrODBC(this.w_TDMAXLEV)+;
             ",TDDESRIF="+cp_ToStrODBC(this.w_TDDESRIF)+;
             ",TDMODRIF="+cp_ToStrODBCNull(this.w_TDMODRIF)+;
             ",TDFLNSRI="+cp_ToStrODBC(this.w_TDFLNSRI)+;
             ",TDTPNDOC="+cp_ToStrODBCNull(this.w_TDTPNDOC)+;
             ",TDFLVSRI="+cp_ToStrODBC(this.w_TDFLVSRI)+;
             ",TDTPVDOC="+cp_ToStrODBCNull(this.w_TDTPVDOC)+;
             ",TDFLRIDE="+cp_ToStrODBC(this.w_TDFLRIDE)+;
             ",TDTPRDES="+cp_ToStrODBCNull(this.w_TDTPRDES)+;
             ",TDESCCL1="+cp_ToStrODBCNull(this.w_TDESCCL1)+;
             ",TDESCCL2="+cp_ToStrODBCNull(this.w_TDESCCL2)+;
             ",TDESCCL3="+cp_ToStrODBCNull(this.w_TDESCCL3)+;
             ",TDESCCL4="+cp_ToStrODBCNull(this.w_TDESCCL4)+;
             ",TDESCCL5="+cp_ToStrODBCNull(this.w_TDESCCL5)+;
             ",TDSTACL1="+cp_ToStrODBCNull(this.w_TDSTACL1)+;
             ",TDSTACL2="+cp_ToStrODBCNull(this.w_TDSTACL2)+;
             ""
             i_nnn=i_nnn+;
             ",TDSTACL3="+cp_ToStrODBCNull(this.w_TDSTACL3)+;
             ",TDSTACL4="+cp_ToStrODBCNull(this.w_TDSTACL4)+;
             ",TDSTACL5="+cp_ToStrODBCNull(this.w_TDSTACL5)+;
             ",TDFLIMPA="+cp_ToStrODBC(this.w_TDFLIMPA)+;
             ",TDFLIMAC="+cp_ToStrODBC(this.w_TDFLIMAC)+;
             ",TDFLNSTA="+cp_ToStrODBC(this.w_TDFLNSTA)+;
             ",TDFLSTLM="+cp_ToStrODBC(this.w_TDFLSTLM)+;
             ",TDMINVAL="+cp_ToStrODBC(this.w_TDMINVAL)+;
             ",TDMINIMP="+cp_ToStrODBC(this.w_TDMINIMP)+;
             ",TDFLATIP="+cp_ToStrODBC(this.w_TDFLATIP)+;
             ",TDFLGEIN="+cp_ToStrODBC(this.w_TDFLGEIN)+;
             ",TDFLIA01="+cp_ToStrODBC(this.w_TDFLIA01)+;
             ",TDFLIA02="+cp_ToStrODBC(this.w_TDFLIA02)+;
             ",TDFLIA03="+cp_ToStrODBC(this.w_TDFLIA03)+;
             ",TDFLIA04="+cp_ToStrODBC(this.w_TDFLIA04)+;
             ",TDFLIA05="+cp_ToStrODBC(this.w_TDFLIA05)+;
             ",TDFLIA06="+cp_ToStrODBC(this.w_TDFLIA06)+;
             ",TDFLRA01="+cp_ToStrODBC(this.w_TDFLRA01)+;
             ",TDFLRA02="+cp_ToStrODBC(this.w_TDFLRA02)+;
             ",TDFLRA03="+cp_ToStrODBC(this.w_TDFLRA03)+;
             ",TDFLRA04="+cp_ToStrODBC(this.w_TDFLRA04)+;
             ",TDFLRA05="+cp_ToStrODBC(this.w_TDFLRA05)+;
             ",TDFLRA06="+cp_ToStrODBC(this.w_TDFLRA06)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",TDTIPDFE="+cp_ToStrODBC(this.w_TDTIPDFE)+;
             ",TDDESEST="+cp_ToStrODBC(this.w_TDDESEST)+;
             ",TDCODCLA="+cp_ToStrODBC(this.w_TDCODCLA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIP_DOCU')
        i_cWhere = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
        UPDATE (i_cTable) SET;
              TDDESDOC=this.w_TDDESDOC;
             ,TDFLVEAC=this.w_TDFLVEAC;
             ,TDCATDOC=this.w_TDCATDOC;
             ,TDEMERIC=this.w_TDEMERIC;
             ,TFFLGEFA=this.w_TFFLGEFA;
             ,TDPRODOC=this.w_TDPRODOC;
             ,TDCAUMAG=this.w_TDCAUMAG;
             ,TDCODMAG=this.w_TDCODMAG;
             ,TDFLMGPR=this.w_TDFLMGPR;
             ,TDCODMAT=this.w_TDCODMAT;
             ,TDFLMTPR=this.w_TDFLMTPR;
             ,TDCAUCON=this.w_TDCAUCON;
             ,TDALFDOC=this.w_TDALFDOC;
             ,TDFLPDOC=this.w_TDFLPDOC;
             ,TDSERPRO=this.w_TDSERPRO;
             ,TDFLPPRO=this.w_TDFLPPRO;
             ,TDNUMSCO=this.w_TDNUMSCO;
             ,TDCODLIS=this.w_TDCODLIS;
             ,TDQTADEF=this.w_TDQTADEF;
             ,TDPROVVI=this.w_TDPROVVI;
             ,TDASPETT=this.w_TDASPETT;
             ,TDFLPREF=this.w_TDFLPREF;
             ,TDFLINTE=this.w_TDFLINTE;
             ,TDNOPRSC=this.w_TDNOPRSC;
             ,TDFLACCO=this.w_TDFLACCO;
             ,TDFLPACK=this.w_TDFLPACK;
             ,TDFLDATT=this.w_TDFLDATT;
             ,TDFLORAT=this.w_TDFLORAT;
             ,TFFLRAGG=this.w_TFFLRAGG;
             ,TDFLCRIS=this.w_TDFLCRIS;
             ,TDFLPROV=this.w_TDFLPROV;
             ,TDFLDTPR=this.w_TDFLDTPR;
             ,TDFLBACA=this.w_TDFLBACA;
             ,TDFLESPF=this.w_TDFLESPF;
             ,TDBOLDOG=this.w_TDBOLDOG;
             ,TDFLCCAU=this.w_TDFLCCAU;
             ,TDMODDES=this.w_TDMODDES;
             ,TDSEQPRE=this.w_TDSEQPRE;
             ,TDSEQSCO=this.w_TDSEQSCO;
             ,TDSEQMA1=this.w_TDSEQMA1;
             ,TDSEQMA2=this.w_TDSEQMA2;
             ,TDCAUPFI=this.w_TDCAUPFI;
             ,TDCAUCOD=this.w_TDCAUCOD;
             ,TDFLEXPL=this.w_TDFLEXPL;
             ,TDPROSTA=this.w_TDPROSTA;
             ,TDEXPAUT=this.w_TDEXPAUT;
             ,TDFLSILI=this.w_TDFLSILI;
             ,TDPRZVAC=this.w_TDPRZVAC;
             ,TDPRZDES=this.w_TDPRZDES;
             ,TDFLVALO=this.w_TDFLVALO;
             ,TDCHKTOT=this.w_TDCHKTOT;
             ,TDFLBLEV=this.w_TDFLBLEV;
             ,TDFLSPIN=this.w_TDFLSPIN;
             ,TDFLQRIO=this.w_TDFLQRIO;
             ,TDFLPREV=this.w_TDFLPREV;
             ,TDFLAPCA=this.w_TDFLAPCA;
             ,TDRIPCON=this.w_TDRIPCON;
             ,TDNOSTCO=this.w_TDNOSTCO;
             ,TDFLGETR=this.w_TDFLGETR;
             ,TDFLSPIM=this.w_TDFLSPIM;
             ,TDFLSPTR=this.w_TDFLSPTR;
             ,TDRIPINC=this.w_TDRIPINC;
             ,TDRIPIMB=this.w_TDRIPIMB;
             ,TDRIPTRA=this.w_TDRIPTRA;
             ,TDMCALSI=this.w_TDMCALSI;
             ,TDMCALST=this.w_TDMCALST;
             ,TDSINCFL=this.w_TDSINCFL;
             ,TDFLRISC=this.w_TDFLRISC;
             ,TDFLANAL=this.w_TDFLANAL;
             ,TDFLELAN=this.w_TDFLELAN;
             ,TDVOCECR=this.w_TDVOCECR;
             ,TD_SEGNO=this.w_TD_SEGNO;
             ,TDASSCES=this.w_TDASSCES;
             ,TDCAUCES=this.w_TDCAUCES;
             ,TDCONASS=this.w_TDCONASS;
             ,TDCODSTR=this.w_TDCODSTR;
             ,TDFLARCO=this.w_TDFLARCO;
             ,TDLOTDIF=this.w_TDLOTDIF;
             ,TDTIPIMB=this.w_TDTIPIMB;
             ,TDVALCOM=this.w_TDVALCOM;
             ,TDCOSEPL=this.w_TDCOSEPL;
             ,TDFLCOMM=this.w_TDFLCOMM;
             ,TDFLCASH=this.w_TDFLCASH;
             ,TDFLNORC=this.w_TDFLNORC;
             ,TDMAXLEV=this.w_TDMAXLEV;
             ,TDDESRIF=this.w_TDDESRIF;
             ,TDMODRIF=this.w_TDMODRIF;
             ,TDFLNSRI=this.w_TDFLNSRI;
             ,TDTPNDOC=this.w_TDTPNDOC;
             ,TDFLVSRI=this.w_TDFLVSRI;
             ,TDTPVDOC=this.w_TDTPVDOC;
             ,TDFLRIDE=this.w_TDFLRIDE;
             ,TDTPRDES=this.w_TDTPRDES;
             ,TDESCCL1=this.w_TDESCCL1;
             ,TDESCCL2=this.w_TDESCCL2;
             ,TDESCCL3=this.w_TDESCCL3;
             ,TDESCCL4=this.w_TDESCCL4;
             ,TDESCCL5=this.w_TDESCCL5;
             ,TDSTACL1=this.w_TDSTACL1;
             ,TDSTACL2=this.w_TDSTACL2;
             ,TDSTACL3=this.w_TDSTACL3;
             ,TDSTACL4=this.w_TDSTACL4;
             ,TDSTACL5=this.w_TDSTACL5;
             ,TDFLIMPA=this.w_TDFLIMPA;
             ,TDFLIMAC=this.w_TDFLIMAC;
             ,TDFLNSTA=this.w_TDFLNSTA;
             ,TDFLSTLM=this.w_TDFLSTLM;
             ,TDMINVAL=this.w_TDMINVAL;
             ,TDMINIMP=this.w_TDMINIMP;
             ,TDFLATIP=this.w_TDFLATIP;
             ,TDFLGEIN=this.w_TDFLGEIN;
             ,TDFLIA01=this.w_TDFLIA01;
             ,TDFLIA02=this.w_TDFLIA02;
             ,TDFLIA03=this.w_TDFLIA03;
             ,TDFLIA04=this.w_TDFLIA04;
             ,TDFLIA05=this.w_TDFLIA05;
             ,TDFLIA06=this.w_TDFLIA06;
             ,TDFLRA01=this.w_TDFLRA01;
             ,TDFLRA02=this.w_TDFLRA02;
             ,TDFLRA03=this.w_TDFLRA03;
             ,TDFLRA04=this.w_TDFLRA04;
             ,TDFLRA05=this.w_TDFLRA05;
             ,TDFLRA06=this.w_TDFLRA06;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,TDTIPDFE=this.w_TDTIPDFE;
             ,TDDESEST=this.w_TDDESEST;
             ,TDCODCLA=this.w_TDCODCLA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAC_MDC : Saving
      this.GSAC_MDC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TDTIPDOC,"DCCODICE";
             )
      this.GSAC_MDC.mReplace()
      * --- GSVE_MTD : Saving
      this.GSVE_MTD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TDTIPDOC,"LGCODICE";
             )
      this.GSVE_MTD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsac_atd
    * --- Controllo Origini: se la causale di magazzino diminuisce l'esistenza
    * --- o aumenta il riservato non posso attivare il check Raggruppa.
    * --- Attivazione check Raggruppa nelle Origini se attivo flag Calcolo Qt� Riordino
    this.NotifyEvent('ChkOrigini')
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAC_MDC : Deleting
    this.GSAC_MDC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TDTIPDOC,"DCCODICE";
           )
    this.GSAC_MDC.mDelete()
    * --- GSVE_MTD : Deleting
    this.GSVE_MTD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TDTIPDOC,"LGCODICE";
           )
    this.GSVE_MTD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIP_DOCU_IDX,i_nConn)
      *
      * delete TIP_DOCU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TDTIPDOC',this.w_TDTIPDOC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_TDFLVEAC = 'A'
        .DoRTCalc(4,4,.t.)
        if .o_TDFLVEAC<>.w_TDFLVEAC
            .w_TDEMERIC = .w_TDFLVEAC
        endif
        .DoRTCalc(6,6,.t.)
            .w_TDPRODOC = CALCPD(.w_TDCATDOC,.w_TDFLVEAC)
        .DoRTCalc(8,13,.t.)
        if .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDCODMAG = SPACE(5)
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDCODMAT = SPACE(5)
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCAUCON = IIF(.w_TDCATDOC $ 'FA-NC', .w_TDCAUCON, SPACE(5))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,23,.t.)
        if .o_TDCAUCON<>.w_TDCAUCON
            .w_TDALFDOC = IIF(NOT EMPTY(.w_SERDOC) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERDOC,.w_TDALFDOC)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDEMERIC<>.w_TDEMERIC
            .w_TDFLPDOC = IIF(.w_TDEMERIC='V' , 'S', ' ')
        endif
        if .o_TDCAUCON<>.w_TDCAUCON
            .w_TDSERPRO = IIF(NOT EMPTY(.w_SERPRO) AND (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC'), .w_SERPRO,.w_TDSERPRO)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDCAUCON<>.w_TDCAUCON
            .w_TDFLPPRO = IIF(empty(.w_TDCAUCON), 'N', 'P')
        endif
        .DoRTCalc(28,35,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLINTE = IIF(.w_TDCATDOC='DI', 'N', 'F')
        endif
        .DoRTCalc(37,39,.t.)
        if .o_TDFLACCO<>.w_TDFLACCO
            .w_TDFLDATT = IIF(.w_TDFLACCO='S','S',' ')
        endif
        if .o_TDFLACCO<>.w_TDFLACCO
            .w_TDFLORAT = IIF(.w_TDFLACCO='S','S',' ')
        endif
            .w_TFFLRAGG = '1'
        .DoRTCalc(43,47,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDFLVEAC<>.w_TDFLVEAC
            .w_TDBOLDOG = ' '
        endif
        .DoRTCalc(49,49,.t.)
        if .o_TDTIPDOC<>.w_TDTIPDOC
            .w_TDMODDES = 'S'
        endif
        .DoRTCalc(51,60,.t.)
          .link_1_74('Full')
          .link_1_75('Full')
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .DoRTCalc(63,72,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLSILI = IIF(.w_TDCATDOC='DT', 'S', ' ')
        endif
        .DoRTCalc(74,81,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDFLINTE<>.w_TDFLINTE
            .w_TDFLAPCA = 'N'
        endif
        if .o_TDFLINTE<>.w_TDFLINTE
            .w_TDRIPCON = IIF( .w_TDFLINTE <> "N", .w_TDRIPCON, " " )
        endif
        .DoRTCalc(84,95,.t.)
            .w_CODI = .w_TDTIPDOC
            .w_DESC = .w_TDDESDOC
        if .o_TDFLVEAC<>.w_TDFLVEAC.or. .o_TDFLINTE<>.w_TDFLINTE.or. .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLRISC = ' '
        endif
        .DoRTCalc(99,102,.t.)
        if .o_TDFLANAL<>.w_TDFLANAL
            .w_TDFLELAN = ' '
        endif
        .DoRTCalc(104,106,.t.)
        if .o_TDASSCES<>.w_TDASSCES
            .w_TDCAUCES = IIF(.w_TDASSCES<>'M', SPACE(5),.w_TDCAUCES)
          .link_3_30('Full')
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCONASS = IIF(.w_TDCATDOC<>'DT','N','F')
        endif
        .DoRTCalc(109,110,.t.)
        if .o_TDFLARCO<>.w_TDFLARCO
            .w_TDLOTDIF = IIF(.w_TDFLARCO<>'S', 'I', .w_TDLOTDIF)
        endif
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDTIPIMB = IIF(.w_TDFLARCO<>'S' Or Empty(.w_FLCASC), 'N', .w_TDTIPIMB)
        endif
        .DoRTCalc(113,114,.t.)
            .w_CODI = .w_TDTIPDOC
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_TDTIPIMB<>.w_TDTIPIMB
            .w_TDFLEXPL = IIF(g_VEFA='S' And .w_TDFLARCO='S' And g_DISB<>'S' Or .w_TDTIPIMB<>'N','S',.w_TDFLEXPL)
        endif
            .w_DESC = .w_TDDESDOC
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC
            .w_TDVALCOM = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'N', .w_TDVALCOM)
        endif
        .DoRTCalc(119,119,.t.)
        if .o_TDFLARCO<>.w_TDFLARCO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_TDTIPIMB<>.w_TDTIPIMB
            .w_TDEXPAUT = IIF(g_VEFA='S' And g_DISB <>'S' And .w_TDFLARCO='S' Or .w_TDTIPIMB<>'N', 'S', IIF(g_DISB='S',.w_TDEXPAUT,' '))
        endif
        if .o_TDCATDOC<>.w_TDCATDOC.or. .o_TDCAUMAG<>.w_TDCAUMAG
            .w_TDFLCOMM = IIF(.w_FLCOMM='N' OR .w_FLCOMM=' ', ' ', 'S')
        endif
        if .o_TDFLCOMM<>.w_TDFLCOMM
            .w_TDFLCASH = ' '
        endif
        .DoRTCalc(123,123,.t.)
        if .o_TDFLEXPL<>.w_TDFLEXPL
            .w_TDMAXLEV = 99
        endif
        .DoRTCalc(125,132,.t.)
            .w_CODI = .w_TDTIPDOC
        .DoRTCalc(134,135,.t.)
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(137,155,.t.)
            .w_CODI = .w_TDTIPDOC
            .w_DESC = .w_TDDESDOC
        .DoRTCalc(158,159,.t.)
            .w_PRGSTA = 'GSAC_MDV'
            .w_PRGALT = IIF(.w_TDFLVEAC='A' AND g_ACQU<>'S', 'GSACAMDV', 'GSVEAMDV')
        .DoRTCalc(162,165,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDFLGEIN = iif(.w_TDCATDOC='DT',.w_TDFLGEIN,'N')
        endif
          .link_4_40('Full')
        .DoRTCalc(168,180,.t.)
        if .o_TDFLIA01<>.w_TDFLIA01
            .w_TDFLRA01 = IIF(.w_TDFLIA01<>'S', 'N', EVL(NVL(.w_TDFLRA01, 'N'), 'N') )
        endif
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
        if .o_TDFLIA02<>.w_TDFLIA02
            .w_TDFLRA02 = IIF(.w_TDFLIA02<>'S', 'N', EVL(NVL(.w_TDFLRA02, 'N'), 'N') )
        endif
        if .o_TDFLIA03<>.w_TDFLIA03
            .w_TDFLRA03 = IIF(.w_TDFLIA03<>'S', 'N', EVL(NVL(.w_TDFLRA03, 'N'), 'N') )
        endif
        if .o_TDFLIA04<>.w_TDFLIA04
            .w_TDFLRA04 = IIF(.w_TDFLIA04<>'S', 'N', EVL(NVL(.w_TDFLRA04, 'N'), 'N') )
        endif
        if .o_TDFLIA05<>.w_TDFLIA05
            .w_TDFLRA05 = IIF(.w_TDFLIA05<>'S', 'N', EVL(NVL(.w_TDFLRA05, 'N'), 'N') )
        endif
        if .o_TDFLIA06<>.w_TDFLIA06
            .w_TDFLRA06 = IIF(.w_TDFLIA06<>'S', 'N', EVL(NVL(.w_TDFLRA06, 'N'), 'N') )
        endif
        if .o_TDFLSILI<>.w_TDFLSILI
          .Calculate_FQEPSKTYNM()
        endif
        .DoRTCalc(187,192,.t.)
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDTIPDFE = Space(4)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDDESEST = Space(35)
        endif
        if .o_TDCATDOC<>.w_TDCATDOC
            .w_TDCODCLA = Space(5)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page4.oPag.CAMAGG01.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_01, " "), "Campo 1" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG02.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_02, " "), "Campo 2" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG03.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_03, " "), "Campo 3" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG04.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_04, " "), "Campo 4" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG05.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_05, " "), "Campo 5" )) + IIF( g_COLON, ":", ""))
        .oPgFrm.Page4.oPag.CAMAGG06.Calculate(ALLTRIM(EVL(NVL(.w_DACAM_06, " "), "Campo 6" )) + IIF( g_COLON, ":", ""))
    endwith
  return

  proc Calculate_FQJJFNNWOT()
    with this
          * --- Inizializza etichette campi aggiuntivi
      if !EMPTY(NVL(.w_DACAM_01, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA01")
      endif
      if !EMPTY(NVL(.w_DACAM_01, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_01) )
      endif
      if !EMPTY(NVL(.w_DACAM_02, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA02")
      endif
      if !EMPTY(NVL(.w_DACAM_02, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_02) )
      endif
      if !EMPTY(NVL(.w_DACAM_03, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA03")
      endif
      if !EMPTY(NVL(.w_DACAM_03, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_03) )
      endif
      if !EMPTY(NVL(.w_DACAM_04, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA04")
      endif
      if !EMPTY(NVL(.w_DACAM_04, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_04) )
      endif
      if !EMPTY(NVL(.w_DACAM_05, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA05")
      endif
      if !EMPTY(NVL(.w_DACAM_05, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_05) )
      endif
      if !EMPTY(NVL(.w_DACAM_06, ' '))
          .w_OBJ_CTRL = this.getCtrl("w"+"_TDFLIA06")
      endif
      if !EMPTY(NVL(.w_DACAM_06, ' '))
          .w_OBJ_CTRL.TooltipText = ah_MsgFormat(.w_DATOOLTI, ALLTRIM(.w_DACAM_06) )
      endif
    endwith
  endproc
  proc Calculate_FQEPSKTYNM()
    with this
          * --- Attivazione/disattivazione flag "Aggiornamento lettere di intento"
     if Upper(This.cFunction)='EDIT'
          .w_MSG_AGGLETT = Ah_ErrorMsg("Attenzione, attivando/disattivando il flag � necessario ricostruire%0l'importo utilizzato sulle singole dichiarazioni")
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.enabled = this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oTDCODMAG_1_14.enabled = this.oPgFrm.Page1.oPag.oTDCODMAG_1_14.mCond()
    this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.enabled = this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.mCond()
    this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.enabled = this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.mCond()
    this.oPgFrm.Page1.oPag.oTDCAUCON_1_18.enabled = this.oPgFrm.Page1.oPag.oTDCAUCON_1_18.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPDOC_1_25.enabled = this.oPgFrm.Page1.oPag.oTDFLPDOC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oTDFLPPRO_1_27.enabled = this.oPgFrm.Page1.oPag.oTDFLPPRO_1_27.mCond()
    this.oPgFrm.Page1.oPag.oTDFLINTE_1_36.enabled = this.oPgFrm.Page1.oPag.oTDFLINTE_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.enabled = this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.mCond()
    this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.enabled = this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.mCond()
    this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.enabled = this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.mCond()
    this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.enabled = this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.mCond()
    this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.enabled = this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.mCond()
    this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.enabled = this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.mCond()
    this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.enabled = this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.mCond()
    this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.enabled = this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.enabled = this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.mCond()
    this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.enabled = this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.mCond()
    this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.enabled = this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.mCond()
    this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.enabled = this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.enabled = this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.mCond()
    this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.enabled = this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.mCond()
    this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.enabled = this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.mCond()
    this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.enabled = this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.mCond()
    this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.enabled = this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.mCond()
    this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.enabled = this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.mCond()
    this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.enabled = this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.mCond()
    this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.enabled = this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.mCond()
    this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.enabled = this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.mCond()
    this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.enabled = this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.mCond()
    this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.enabled = this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.mCond()
    this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.enabled = this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.mCond()
    this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.enabled = this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA01_4_55.enabled = this.oPgFrm.Page4.oPag.oTDFLRA01_4_55.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA02_4_65.enabled = this.oPgFrm.Page4.oPag.oTDFLRA02_4_65.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA03_4_66.enabled = this.oPgFrm.Page4.oPag.oTDFLRA03_4_66.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA04_4_67.enabled = this.oPgFrm.Page4.oPag.oTDFLRA04_4_67.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA05_4_68.enabled = this.oPgFrm.Page4.oPag.oTDFLRA05_4_68.mCond()
    this.oPgFrm.Page4.oPag.oTDFLRA06_4_69.enabled = this.oPgFrm.Page4.oPag.oTDFLRA06_4_69.mCond()
    this.oPgFrm.Page3.oPag.oTDTIPDFE_3_61.enabled = this.oPgFrm.Page3.oPag.oTDTIPDFE_3_61.mCond()
    this.oPgFrm.Page3.oPag.oTDDESEST_3_62.enabled = this.oPgFrm.Page3.oPag.oTDDESEST_3_62.mCond()
    this.oPgFrm.Page3.oPag.oTDCODCLA_3_63.enabled = this.oPgFrm.Page3.oPag.oTDCODCLA_3_63.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTDEMERIC_1_5.visible=!this.oPgFrm.Page1.oPag.oTDEMERIC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.visible=!this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.mHide()
    this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.visible=!this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.mHide()
    this.oPgFrm.Page1.oPag.oTDFLBACA_1_46.visible=!this.oPgFrm.Page1.oPag.oTDFLBACA_1_46.mHide()
    this.oPgFrm.Page1.oPag.oTDBOLDOG_1_48.visible=!this.oPgFrm.Page1.oPag.oTDBOLDOG_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.visible=!this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.visible=!this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.mHide()
    this.oPgFrm.Page2.oPag.oTDFLAPCA_2_11.visible=!this.oPgFrm.Page2.oPag.oTDFLAPCA_2_11.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.visible=!this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.mHide()
    this.oPgFrm.Page2.oPag.oTDNOSTCO_2_13.visible=!this.oPgFrm.Page2.oPag.oTDNOSTCO_2_13.mHide()
    this.oPgFrm.Page2.oPag.oTDFLGETR_2_14.visible=!this.oPgFrm.Page2.oPag.oTDFLGETR_2_14.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPINC_2_21.visible=!this.oPgFrm.Page2.oPag.oTDRIPINC_2_21.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPIMB_2_22.visible=!this.oPgFrm.Page2.oPag.oTDRIPIMB_2_22.mHide()
    this.oPgFrm.Page2.oPag.oTDRIPTRA_2_23.visible=!this.oPgFrm.Page2.oPag.oTDRIPTRA_2_23.mHide()
    this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.visible=!this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_4.visible=!this.oPgFrm.Page3.oPag.oStr_3_4.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_5.visible=!this.oPgFrm.Page3.oPag.oStr_3_5.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_6.visible=!this.oPgFrm.Page3.oPag.oStr_3_6.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_7.visible=!this.oPgFrm.Page3.oPag.oStr_3_7.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_8.visible=!this.oPgFrm.Page3.oPag.oStr_3_8.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_9.visible=!this.oPgFrm.Page3.oPag.oStr_3_9.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_10.visible=!this.oPgFrm.Page3.oPag.oStr_3_10.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_11.visible=!this.oPgFrm.Page3.oPag.oStr_3_11.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_12.visible=!this.oPgFrm.Page3.oPag.oStr_3_12.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_13.visible=!this.oPgFrm.Page3.oPag.oStr_3_13.mHide()
    this.oPgFrm.Page3.oPag.oDESCAUCE_3_14.visible=!this.oPgFrm.Page3.oPag.oDESCAUCE_3_14.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_15.visible=!this.oPgFrm.Page3.oPag.oStr_3_15.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_17.visible=!this.oPgFrm.Page3.oPag.oStr_3_17.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_18.visible=!this.oPgFrm.Page3.oPag.oStr_3_18.mHide()
    this.oPgFrm.Page3.oPag.oDESPFI_3_19.visible=!this.oPgFrm.Page3.oPag.oDESPFI_3_19.mHide()
    this.oPgFrm.Page3.oPag.oDESCOD_3_20.visible=!this.oPgFrm.Page3.oPag.oDESCOD_3_20.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_21.visible=!this.oPgFrm.Page3.oPag.oStr_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_23.visible=!this.oPgFrm.Page3.oPag.oStr_3_23.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_24.visible=!this.oPgFrm.Page3.oPag.oStr_3_24.mHide()
    this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.visible=!this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.mHide()
    this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.visible=!this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.mHide()
    this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.visible=!this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.mHide()
    this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.visible=!this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.mHide()
    this.oPgFrm.Page3.oPag.oTDASSCES_3_29.visible=!this.oPgFrm.Page3.oPag.oTDASSCES_3_29.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.visible=!this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.mHide()
    this.oPgFrm.Page3.oPag.oTDCONASS_3_31.visible=!this.oPgFrm.Page3.oPag.oTDCONASS_3_31.mHide()
    this.oPgFrm.Page3.oPag.oTDCODSTR_3_32.visible=!this.oPgFrm.Page3.oPag.oTDCODSTR_3_32.mHide()
    this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.visible=!this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.mHide()
    this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.visible=!this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.mHide()
    this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.visible=!this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.visible=!this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.mHide()
    this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.visible=!this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.mHide()
    this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.visible=!this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.mHide()
    this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.visible=!this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.mHide()
    this.oPgFrm.Page3.oPag.oTDCOSEPL_3_43.visible=!this.oPgFrm.Page3.oPag.oTDCOSEPL_3_43.mHide()
    this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.visible=!this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.mHide()
    this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.visible=!this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.mHide()
    this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.visible=!this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.mHide()
    this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.visible=!this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.mHide()
    this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.visible=!this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.mHide()
    this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.visible=!this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.mHide()
    this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.visible=!this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_27.visible=!this.oPgFrm.Page4.oPag.oStr_4_27.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_28.visible=!this.oPgFrm.Page4.oPag.oStr_4_28.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_32.visible=!this.oPgFrm.Page4.oPag.oStr_4_32.mHide()
    this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.visible=!this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.mHide()
    this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.visible=!this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.mHide()
    this.oPgFrm.Page1.oPag.oTDFLGEIN_1_96.visible=!this.oPgFrm.Page1.oPag.oTDFLGEIN_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_33.visible=!this.oPgFrm.Page2.oPag.oBtn_2_33.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_59.visible=!this.oPgFrm.Page3.oPag.oStr_3_59.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsac_atd
    IF Upper(CEVENT)='INIT'
      IF Upper(this.GSAC_MDC.class)='STDDYNAMICCHILD'
          This.oPgFrm.Pages[4].opag.uienable(.T.)
          This.oPgFrm.ActivePage=1
      ENDIF
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_89.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_1.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_FQJJFNNWOT()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.CAMAGG01.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG02.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG03.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG04.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG05.Event(cEvent)
      .oPgFrm.Page4.oPag.CAMAGG06.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TDCAUMAG
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_TDCAUMAG))
          select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_TDCAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oTDCAUMAG_1_8'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_TDCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_TDCAUMAG)
            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCLFR,CMFLAVAL,CMDTOBSO,CMFLELGM,CMFLCOMM,CMFLCASC,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESMAG = NVL(_Link_.CMDESCRI,space(35))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_FLCLFR = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FLAVAL = NVL(_Link_.CMFLAVAL,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLELGM = NVL(_Link_.CMFLELGM,space(1))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLRISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUMAG = space(5)
      endif
      this.w_DESMAG = space(35)
      this.w_CAUCOL = space(5)
      this.w_FLCLFR = space(1)
      this.w_FLAVAL = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLELGM = space(1)
      this.w_FLCOMM = space(1)
      this.w_FLCASC = space(1)
      this.w_FLRISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUMA(.w_TDCAUMAG,.w_CAUCOL,.w_TFFLGEFA,.w_FLAVAL,.w_DATOBSO,.w_OBTEST, 'A', .w_TDFLVEAC, .w_TDCATDOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TDCAUMAG = space(5)
        this.w_DESMAG = space(35)
        this.w_CAUCOL = space(5)
        this.w_FLCLFR = space(1)
        this.w_FLAVAL = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLELGM = space(1)
        this.w_FLCOMM = space(1)
        this.w_FLCASC = space(1)
        this.w_FLRISE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.CMCODICE as CMCODICE108"+ ",link_1_8.CMDESCRI as CMDESCRI108"+ ",link_1_8.CMCAUCOL as CMCAUCOL108"+ ",link_1_8.CMFLCLFR as CMFLCLFR108"+ ",link_1_8.CMFLAVAL as CMFLAVAL108"+ ",link_1_8.CMDTOBSO as CMDTOBSO108"+ ",link_1_8.CMFLELGM as CMFLELGM108"+ ",link_1_8.CMFLCOMM as CMFLCOMM108"+ ",link_1_8.CMFLCASC as CMFLCASC108"+ ",link_1_8.CMFLRISE as CMFLRISE108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on TIP_DOCU.TDCAUMAG=link_1_8.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUMAG=link_1_8.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODMAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_TDCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_TDCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oTDCODMAG_1_14'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_TDCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_TDCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODMAG = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_TDCODMAG = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.MGCODMAG as MGCODMAG114"+ ",link_1_14.MGDESMAG as MGDESMAG114"+ ",link_1_14.MGDTOBSO as MGDTOBSO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on TIP_DOCU.TDCODMAG=link_1_14.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODMAG=link_1_14.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODMAT
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_TDCODMAT))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_TDCODMAT)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_TDCODMAT)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oTDCODMAT_1_16'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_TDCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_TDCODMAT)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODMAT = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_TDCODMAT = space(5)
        this.w_DESAPP = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.MGCODMAG as MGCODMAG116"+ ",link_1_16.MGDESMAG as MGDESMAG116"+ ",link_1_16.MGDTOBSO as MGDTOBSO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on TIP_DOCU.TDCODMAT=link_1_16.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODMAT=link_1_16.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_TDCAUCON))
          select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oTDCAUCON_1_18'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSAC_ATD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TDCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TDCAUCON)
            select CCCODICE,CCDESCRI,CCTIPDOC,CCSERDOC,CCSERPRO,CCFLANAL,CCFLRIFE,CCDTOBSO,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_SERDOC = NVL(_Link_.CCSERDOC,space(10))
      this.w_SERPRO = NVL(_Link_.CCSERPRO,space(10))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_CCFLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CCDATOBS = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCON = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPDOC = space(2)
      this.w_SERDOC = space(10)
      this.w_SERPRO = space(10)
      this.w_FLANAL = space(1)
      this.w_CCFLANAL = space(1)
      this.w_FLRIFE = space(1)
      this.w_CCDATOBS = ctod("  /  /  ")
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CCFLANAL<>'S' OR g_PERCCR<>'S' OR (.w_TDFLELAN<>'S' AND .w_TDFLANAL<>'S')) AND CHKCAUDO(.w_TDCATDOC, ' ', .w_TDFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG) AND (.w_CCDATOBS>.w_OBTEST or EMPTY(nvl(.w_CCDATOBS,' ')))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)")
        endif
        this.w_TDCAUCON = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPDOC = space(2)
        this.w_SERDOC = space(10)
        this.w_SERPRO = space(10)
        this.w_FLANAL = space(1)
        this.w_CCFLANAL = space(1)
        this.w_FLRIFE = space(1)
        this.w_CCDATOBS = ctod("  /  /  ")
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.CCCODICE as CCCODICE118"+ ",link_1_18.CCDESCRI as CCDESCRI118"+ ",link_1_18.CCTIPDOC as CCTIPDOC118"+ ",link_1_18.CCSERDOC as CCSERDOC118"+ ",link_1_18.CCSERPRO as CCSERPRO118"+ ",link_1_18.CCFLANAL as CCFLANAL118"+ ",link_1_18.CCFLANAL as CCFLANAL118"+ ",link_1_18.CCFLRIFE as CCFLRIFE118"+ ",link_1_18.CCDTOBSO as CCDTOBSO118"+ ",link_1_18.CCTIPREG as CCTIPREG118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on TIP_DOCU.TDCAUCON=link_1_18.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCON=link_1_18.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODLIS
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_TDCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_TDCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_TDCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_TDCODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oTDCODLIS_1_29'),i_cWhere,'GSAR_ALI',"Elenco listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_TDCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_TDCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.LSCODLIS as LSCODLIS129"+ ",link_1_29.LSDESLIS as LSDESLIS129"+ ",link_1_29.LSIVALIS as LSIVALIS129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on TIP_DOCU.TDCODLIS=link_1_29.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODLIS=link_1_29.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUPFI
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUPFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUPFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUPFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUPFI)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUPFI = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUPFI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUPFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDCAUCOD
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUCOD)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCOD = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCOD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDMCALSI
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALSI_2_24'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_24.MSCODICE as MSCODICE224"+ ",link_2_24.MSDESCRI as MSDESCRI224"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_24 on TIP_DOCU.TDMCALSI=link_2_24.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_24"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALSI=link_2_24.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMCALST
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_TDMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_TDMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oTDMCALST_2_25'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_TDMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_TDMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TDMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_25.MSCODICE as MSCODICE225"+ ",link_2_25.MSDESCRI as MSDESCRI225"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_25 on TIP_DOCU.TDMCALST=link_2_25.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_25"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMCALST=link_2_25.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCES
  func Link_3_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_TDCAUCES))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_TDCAUCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_TDCAUCES)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oTDCAUCES_3_30'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_TDCAUCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_TDCAUCES)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUCE = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCES = space(5)
      endif
      this.w_DESCAUCE = space(40)
      this.w_DTOBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_TDCAUCES = space(5)
        this.w_DESCAUCE = space(40)
        this.w_DTOBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CESP_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_30.CCCODICE as CCCODICE330"+ ",link_3_30.CCDESCRI as CCDESCRI330"+ ",link_3_30.CCDTOBSO as CCDTOBSO330"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_30 on TIP_DOCU.TDCAUCES=link_3_30.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_30"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCES=link_3_30.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCODSTR
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_TDCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_TDCODSTR))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oTDCODSTR_3_32'),i_cWhere,'GSVA_AST',"Elenco strutture EDI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_TDCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_TDCODSTR)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTRU = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TDCODSTR = space(10)
      endif
      this.w_DESSTRU = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_32.STCODICE as STCODICE332"+ ",link_3_32.STDESCRI as STDESCRI332"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_32 on TIP_DOCU.TDCODSTR=link_3_32.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_32"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCODSTR=link_3_32.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUPFI
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUPFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TDCAUPFI)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TDCAUPFI))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUPFI)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUPFI) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTDCAUPFI_3_36'),i_cWhere,'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUPFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUPFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUPFI)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUPFI = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESPFI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATPFI = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLIPFI = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLTCOM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUPFI = space(5)
      endif
      this.w_DESPFI = space(35)
      this.w_CATPFI = space(2)
      this.w_FLIPFI = space(1)
      this.w_FLTCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TDCAUPFI) OR (.w_CATPFI='DI' AND (.w_FLIPFI='N' OR .w_TDFLINTE=.w_FLIPFI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
        endif
        this.w_TDCAUPFI = space(5)
        this.w_DESPFI = space(35)
        this.w_CATPFI = space(2)
        this.w_FLIPFI = space(1)
        this.w_FLTCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUPFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_36.TDTIPDOC as TDTIPDOC336"+ ",link_3_36.TDDESDOC as TDDESDOC336"+ ",link_3_36.TDCATDOC as TDCATDOC336"+ ",link_3_36.TDFLINTE as TDFLINTE336"+ ",link_3_36.TDFLCOMM as TDFLCOMM336"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_36 on TIP_DOCU.TDCAUPFI=link_3_36.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_36"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUPFI=link_3_36.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDCAUCOD
  func Link_3_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_BZC',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TDCAUCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TDCAUCOD))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUCOD)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUCOD) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTDCAUCOD_3_37'),i_cWhere,'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDCAUCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDCAUCOD)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUCOD = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCOD = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATCOD = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLICOD = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLDCOM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUCOD = space(5)
      endif
      this.w_DESCOD = space(35)
      this.w_CATCOD = space(2)
      this.w_FLICOD = space(1)
      this.w_FLDCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TDCAUCOD) OR (.w_CATCOD='DI' AND (.w_FLICOD='N' OR .w_TDFLINTE=.w_FLICOD))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
        endif
        this.w_TDCAUCOD = space(5)
        this.w_DESCOD = space(35)
        this.w_CATCOD = space(2)
        this.w_FLICOD = space(1)
        this.w_FLDCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_37.TDTIPDOC as TDTIPDOC337"+ ",link_3_37.TDDESDOC as TDDESDOC337"+ ",link_3_37.TDCATDOC as TDCATDOC337"+ ",link_3_37.TDFLINTE as TDFLINTE337"+ ",link_3_37.TDFLCOMM as TDFLCOMM337"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_37 on TIP_DOCU.TDCAUCOD=link_3_37.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_37"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDCAUCOD=link_3_37.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDMODRIF
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODMRIFE_IDX,3]
    i_lTable = "MODMRIFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2], .t., this.MODMRIFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDMODRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MRR',True,'MODMRIFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_TDMODRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_TDMODRIF))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDMODRIF)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_TDMODRIF)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_TDMODRIF)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDMODRIF) and !this.bDontReportError
            deferred_cp_zoom('MODMRIFE','*','MDCODICE',cp_AbsName(oSource.parent,'oTDMODRIF_4_4'),i_cWhere,'GSAR_MRR',"Modelli riferimenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDMODRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_TDMODRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_TDMODRIF)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDMODRIF = NVL(_Link_.MDCODICE,space(5))
      this.w_DESMOD = NVL(_Link_.MDDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TDMODRIF = space(5)
      endif
      this.w_DESMOD = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODMRIFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDMODRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODMRIFE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODMRIFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_4.MDCODICE as MDCODICE404"+ ",link_4_4.MDDESCRI as MDDESCRI404"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_4 on TIP_DOCU.TDMODRIF=link_4_4.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_4"
          i_cKey=i_cKey+'+" and TIP_DOCU.TDMODRIF=link_4_4.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TDTPNDOC
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPNDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPNDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPNDOC))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPNDOC)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPNDOC) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPNDOC_4_9'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPNDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPNDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPNDOC)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPNDOC = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPNDOC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPNDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDTPVDOC
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPVDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPVDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPVDOC))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPVDOC)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPVDOC) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPVDOC_4_11'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPVDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPVDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPVDOC)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPVDOC = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPVDOC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPVDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDTPRDES
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTPRDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDTPRDES)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDTPRDES))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDTPRDES)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDTPRDES) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDTPRDES_4_13'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTPRDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDTPRDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDTPRDES)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTPRDES = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDTPRDES = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTPRDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL1
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL1))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL1)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL1) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL1_4_17'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL1)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL1 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL2
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL2))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL2)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL2) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL2_4_18'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL2)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL2 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL3
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL3))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL3)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL3) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL3_4_19'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL3)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL3 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL4
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL4))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL4)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL4) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL4_4_20'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL4)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL4 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL4 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDESCCL5
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDESCCL5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDESCCL5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDESCCL5))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDESCCL5)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDESCCL5) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDESCCL5_4_21'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDESCCL5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDESCCL5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDESCCL5)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDESCCL5 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDESCCL5 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDESCCL5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL1
  func Link_4_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL1))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL1)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL1) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL1_4_22'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL1)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL1 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL2
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL2))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL2)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL2) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL2_4_23'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL2)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL2 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL3
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL3))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL3)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL3) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL3_4_24'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL3)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL3 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL4
  func Link_4_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL4))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL4)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL4) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL4_4_25'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL4)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL4 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL4 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDSTACL5
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDSTACL5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_TDSTACL5)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_TDSTACL5))
          select TRCODCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDSTACL5)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TDSTACL5) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oTDSTACL5_4_26'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDSTACL5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_TDSTACL5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_TDSTACL5)
            select TRCODCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDSTACL5 = NVL(_Link_.TRCODCLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TDSTACL5 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDSTACL5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DASERIAL
  func Link_4_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_lTable = "DATI_AGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2], .t., this.DATI_AGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06";
                   +" from "+i_cTable+" "+i_lTable+" where DASERIAL="+cp_ToStrODBC(this.w_DASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DASERIAL',this.w_DASERIAL)
            select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASERIAL = NVL(_Link_.DASERIAL,space(10))
      this.w_DACAM_01 = NVL(_Link_.DACAM_01,space(30))
      this.w_DACAM_02 = NVL(_Link_.DACAM_02,space(30))
      this.w_DACAM_03 = NVL(_Link_.DACAM_03,space(30))
      this.w_DACAM_04 = NVL(_Link_.DACAM_04,space(30))
      this.w_DACAM_05 = NVL(_Link_.DACAM_05,space(30))
      this.w_DACAM_06 = NVL(_Link_.DACAM_06,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DASERIAL = space(10)
      endif
      this.w_DACAM_01 = space(30)
      this.w_DACAM_02 = space(30)
      this.w_DACAM_03 = space(30)
      this.w_DACAM_04 = space(30)
      this.w_DACAM_05 = space(30)
      this.w_DACAM_06 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])+'\'+cp_ToStr(_Link_.DASERIAL,1)
      cp_ShowWarn(i_cKey,this.DATI_AGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTDTIPDOC_1_1.value==this.w_TDTIPDOC)
      this.oPgFrm.Page1.oPag.oTDTIPDOC_1_1.value=this.w_TDTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_2.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_2.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.RadioValue()==this.w_TDCATDOC)
      this.oPgFrm.Page1.oPag.oTDCATDOC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDEMERIC_1_5.RadioValue()==this.w_TDEMERIC)
      this.oPgFrm.Page1.oPag.oTDEMERIC_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCAUMAG_1_8.value==this.w_TDCAUMAG)
      this.oPgFrm.Page1.oPag.oTDCAUMAG_1_8.value=this.w_TDCAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_9.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_9.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODMAG_1_14.value==this.w_TDCODMAG)
      this.oPgFrm.Page1.oPag.oTDCODMAG_1_14.value=this.w_TDCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLMGPR_1_15.RadioValue()==this.w_TDFLMGPR)
      this.oPgFrm.Page1.oPag.oTDFLMGPR_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.value==this.w_TDCODMAT)
      this.oPgFrm.Page1.oPag.oTDCODMAT_1_16.value=this.w_TDCODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.RadioValue()==this.w_TDFLMTPR)
      this.oPgFrm.Page1.oPag.oTDFLMTPR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCAUCON_1_18.value==this.w_TDCAUCON)
      this.oPgFrm.Page1.oPag.oTDCAUCON_1_18.value=this.w_TDCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_20.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_20.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTDALFDOC_1_24.value==this.w_TDALFDOC)
      this.oPgFrm.Page1.oPag.oTDALFDOC_1_24.value=this.w_TDALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPDOC_1_25.RadioValue()==this.w_TDFLPDOC)
      this.oPgFrm.Page1.oPag.oTDFLPDOC_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSERPRO_1_26.value==this.w_TDSERPRO)
      this.oPgFrm.Page1.oPag.oTDSERPRO_1_26.value=this.w_TDSERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPPRO_1_27.RadioValue()==this.w_TDFLPPRO)
      this.oPgFrm.Page1.oPag.oTDFLPPRO_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDNUMSCO_1_28.value==this.w_TDNUMSCO)
      this.oPgFrm.Page1.oPag.oTDNUMSCO_1_28.value=this.w_TDNUMSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCODLIS_1_29.value==this.w_TDCODLIS)
      this.oPgFrm.Page1.oPag.oTDCODLIS_1_29.value=this.w_TDCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTDQTADEF_1_30.value==this.w_TDQTADEF)
      this.oPgFrm.Page1.oPag.oTDQTADEF_1_30.value=this.w_TDQTADEF
    endif
    if not(this.oPgFrm.Page1.oPag.oTDPROVVI_1_31.RadioValue()==this.w_TDPROVVI)
      this.oPgFrm.Page1.oPag.oTDPROVVI_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_32.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_32.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPREF_1_35.RadioValue()==this.w_TDFLPREF)
      this.oPgFrm.Page1.oPag.oTDFLPREF_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLINTE_1_36.RadioValue()==this.w_TDFLINTE)
      this.oPgFrm.Page1.oPag.oTDFLINTE_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDNOPRSC_1_37.RadioValue()==this.w_TDNOPRSC)
      this.oPgFrm.Page1.oPag.oTDNOPRSC_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLACCO_1_38.RadioValue()==this.w_TDFLACCO)
      this.oPgFrm.Page1.oPag.oTDFLACCO_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLPACK_1_39.RadioValue()==this.w_TDFLPACK)
      this.oPgFrm.Page1.oPag.oTDFLPACK_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLDTPR_1_45.RadioValue()==this.w_TDFLDTPR)
      this.oPgFrm.Page1.oPag.oTDFLDTPR_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLBACA_1_46.RadioValue()==this.w_TDFLBACA)
      this.oPgFrm.Page1.oPag.oTDFLBACA_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDBOLDOG_1_48.RadioValue()==this.w_TDBOLDOG)
      this.oPgFrm.Page1.oPag.oTDBOLDOG_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLCCAU_1_50.RadioValue()==this.w_TDFLCCAU)
      this.oPgFrm.Page1.oPag.oTDFLCCAU_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDMODDES_1_51.RadioValue()==this.w_TDMODDES)
      this.oPgFrm.Page1.oPag.oTDMODDES_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQPRE_1_61.RadioValue()==this.w_TDSEQPRE)
      this.oPgFrm.Page1.oPag.oTDSEQPRE_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQSCO_1_62.RadioValue()==this.w_TDSEQSCO)
      this.oPgFrm.Page1.oPag.oTDSEQSCO_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQMA1_1_63.RadioValue()==this.w_TDSEQMA1)
      this.oPgFrm.Page1.oPag.oTDSEQMA1_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.RadioValue()==this.w_TDSEQMA2)
      this.oPgFrm.Page1.oPag.oTDSEQMA2_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.RadioValue()==this.w_TDFLSILI)
      this.oPgFrm.Page2.oPag.oTDFLSILI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDPRZVAC_2_3.RadioValue()==this.w_TDPRZVAC)
      this.oPgFrm.Page2.oPag.oTDPRZVAC_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDPRZDES_2_4.RadioValue()==this.w_TDPRZDES)
      this.oPgFrm.Page2.oPag.oTDPRZDES_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLVALO_2_5.RadioValue()==this.w_TDFLVALO)
      this.oPgFrm.Page2.oPag.oTDFLVALO_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDCHKTOT_2_6.RadioValue()==this.w_TDCHKTOT)
      this.oPgFrm.Page2.oPag.oTDCHKTOT_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLBLEV_2_7.RadioValue()==this.w_TDFLBLEV)
      this.oPgFrm.Page2.oPag.oTDFLBLEV_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPIN_2_8.RadioValue()==this.w_TDFLSPIN)
      this.oPgFrm.Page2.oPag.oTDFLSPIN_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLQRIO_2_9.RadioValue()==this.w_TDFLQRIO)
      this.oPgFrm.Page2.oPag.oTDFLQRIO_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLPREV_2_10.RadioValue()==this.w_TDFLPREV)
      this.oPgFrm.Page2.oPag.oTDFLPREV_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLAPCA_2_11.RadioValue()==this.w_TDFLAPCA)
      this.oPgFrm.Page2.oPag.oTDFLAPCA_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.RadioValue()==this.w_TDRIPCON)
      this.oPgFrm.Page2.oPag.oTDRIPCON_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDNOSTCO_2_13.RadioValue()==this.w_TDNOSTCO)
      this.oPgFrm.Page2.oPag.oTDNOSTCO_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLGETR_2_14.RadioValue()==this.w_TDFLGETR)
      this.oPgFrm.Page2.oPag.oTDFLGETR_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPIM_2_15.RadioValue()==this.w_TDFLSPIM)
      this.oPgFrm.Page2.oPag.oTDFLSPIM_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESIMB_2_16.value==this.w_MSDESIMB)
      this.oPgFrm.Page2.oPag.oMSDESIMB_2_16.value=this.w_MSDESIMB
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLSPTR_2_18.RadioValue()==this.w_TDFLSPTR)
      this.oPgFrm.Page2.oPag.oTDFLSPTR_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSDESTRA_2_19.value==this.w_MSDESTRA)
      this.oPgFrm.Page2.oPag.oMSDESTRA_2_19.value=this.w_MSDESTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPINC_2_21.RadioValue()==this.w_TDRIPINC)
      this.oPgFrm.Page2.oPag.oTDRIPINC_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPIMB_2_22.RadioValue()==this.w_TDRIPIMB)
      this.oPgFrm.Page2.oPag.oTDRIPIMB_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDRIPTRA_2_23.RadioValue()==this.w_TDRIPTRA)
      this.oPgFrm.Page2.oPag.oTDRIPTRA_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALSI_2_24.value==this.w_TDMCALSI)
      this.oPgFrm.Page2.oPag.oTDMCALSI_2_24.value=this.w_TDMCALSI
    endif
    if not(this.oPgFrm.Page2.oPag.oTDMCALST_2_25.value==this.w_TDMCALST)
      this.oPgFrm.Page2.oPag.oTDMCALST_2_25.value=this.w_TDMCALST
    endif
    if not(this.oPgFrm.Page2.oPag.oTDSINCFL_2_26.RadioValue()==this.w_TDSINCFL)
      this.oPgFrm.Page2.oPag.oTDSINCFL_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_28.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_28.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_29.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_29.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.RadioValue()==this.w_TDFLRISC)
      this.oPgFrm.Page2.oPag.oTDFLRISC_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCAUCE_3_14.value==this.w_DESCAUCE)
      this.oPgFrm.Page3.oPag.oDESCAUCE_3_14.value=this.w_DESCAUCE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPFI_3_19.value==this.w_DESPFI)
      this.oPgFrm.Page3.oPag.oDESPFI_3_19.value=this.w_DESPFI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCOD_3_20.value==this.w_DESCOD)
      this.oPgFrm.Page3.oPag.oDESCOD_3_20.value=this.w_DESCOD
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.RadioValue()==this.w_TDFLANAL)
      this.oPgFrm.Page3.oPag.oTDFLANAL_3_25.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.RadioValue()==this.w_TDFLELAN)
      this.oPgFrm.Page3.oPag.oTDFLELAN_3_26.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.RadioValue()==this.w_TDVOCECR)
      this.oPgFrm.Page3.oPag.oTDVOCECR_3_27.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.RadioValue()==this.w_TD_SEGNO)
      this.oPgFrm.Page3.oPag.oTD_SEGNO_3_28.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDASSCES_3_29.RadioValue()==this.w_TDASSCES)
      this.oPgFrm.Page3.oPag.oTDASSCES_3_29.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.value==this.w_TDCAUCES)
      this.oPgFrm.Page3.oPag.oTDCAUCES_3_30.value=this.w_TDCAUCES
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCONASS_3_31.RadioValue()==this.w_TDCONASS)
      this.oPgFrm.Page3.oPag.oTDCONASS_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCODSTR_3_32.value==this.w_TDCODSTR)
      this.oPgFrm.Page3.oPag.oTDCODSTR_3_32.value=this.w_TDCODSTR
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.RadioValue()==this.w_TDFLARCO)
      this.oPgFrm.Page3.oPag.oTDFLARCO_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.RadioValue()==this.w_TDLOTDIF)
      this.oPgFrm.Page3.oPag.oTDLOTDIF_3_34.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.RadioValue()==this.w_TDTIPIMB)
      this.oPgFrm.Page3.oPag.oTDTIPIMB_3_35.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.value==this.w_TDCAUPFI)
      this.oPgFrm.Page3.oPag.oTDCAUPFI_3_36.value=this.w_TDCAUPFI
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.value==this.w_TDCAUCOD)
      this.oPgFrm.Page3.oPag.oTDCAUCOD_3_37.value=this.w_TDCAUCOD
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_39.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_39.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.RadioValue()==this.w_TDFLEXPL)
      this.oPgFrm.Page3.oPag.oTDFLEXPL_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_41.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_41.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.RadioValue()==this.w_TDVALCOM)
      this.oPgFrm.Page3.oPag.oTDVALCOM_3_42.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCOSEPL_3_43.RadioValue()==this.w_TDCOSEPL)
      this.oPgFrm.Page3.oPag.oTDCOSEPL_3_43.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.RadioValue()==this.w_TDEXPAUT)
      this.oPgFrm.Page3.oPag.oTDEXPAUT_3_44.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.RadioValue()==this.w_TDFLCOMM)
      this.oPgFrm.Page3.oPag.oTDFLCOMM_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.RadioValue()==this.w_TDFLCASH)
      this.oPgFrm.Page3.oPag.oTDFLCASH_3_46.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDFLNORC_3_47.RadioValue()==this.w_TDFLNORC)
      this.oPgFrm.Page3.oPag.oTDFLNORC_3_47.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.value==this.w_TDMAXLEV)
      this.oPgFrm.Page3.oPag.oTDMAXLEV_3_48.value=this.w_TDMAXLEV
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_2.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESRIF_4_3.value==this.w_TDDESRIF)
      this.oPgFrm.Page4.oPag.oTDDESRIF_4_3.value=this.w_TDDESRIF
    endif
    if not(this.oPgFrm.Page4.oPag.oTDMODRIF_4_4.value==this.w_TDMODRIF)
      this.oPgFrm.Page4.oPag.oTDMODRIF_4_4.value=this.w_TDMODRIF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESC_4_5.value==this.w_DESC)
      this.oPgFrm.Page4.oPag.oDESC_4_5.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLNSRI_4_8.RadioValue()==this.w_TDFLNSRI)
      this.oPgFrm.Page4.oPag.oTDFLNSRI_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.value==this.w_TDTPNDOC)
      this.oPgFrm.Page4.oPag.oTDTPNDOC_4_9.value=this.w_TDTPNDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLVSRI_4_10.RadioValue()==this.w_TDFLVSRI)
      this.oPgFrm.Page4.oPag.oTDFLVSRI_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.value==this.w_TDTPVDOC)
      this.oPgFrm.Page4.oPag.oTDTPVDOC_4_11.value=this.w_TDTPVDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRIDE_4_12.RadioValue()==this.w_TDFLRIDE)
      this.oPgFrm.Page4.oPag.oTDFLRIDE_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.value==this.w_TDTPRDES)
      this.oPgFrm.Page4.oPag.oTDTPRDES_4_13.value=this.w_TDTPRDES
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL1_4_17.value==this.w_TDESCCL1)
      this.oPgFrm.Page4.oPag.oTDESCCL1_4_17.value=this.w_TDESCCL1
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL2_4_18.value==this.w_TDESCCL2)
      this.oPgFrm.Page4.oPag.oTDESCCL2_4_18.value=this.w_TDESCCL2
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL3_4_19.value==this.w_TDESCCL3)
      this.oPgFrm.Page4.oPag.oTDESCCL3_4_19.value=this.w_TDESCCL3
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL4_4_20.value==this.w_TDESCCL4)
      this.oPgFrm.Page4.oPag.oTDESCCL4_4_20.value=this.w_TDESCCL4
    endif
    if not(this.oPgFrm.Page4.oPag.oTDESCCL5_4_21.value==this.w_TDESCCL5)
      this.oPgFrm.Page4.oPag.oTDESCCL5_4_21.value=this.w_TDESCCL5
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL1_4_22.value==this.w_TDSTACL1)
      this.oPgFrm.Page4.oPag.oTDSTACL1_4_22.value=this.w_TDSTACL1
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL2_4_23.value==this.w_TDSTACL2)
      this.oPgFrm.Page4.oPag.oTDSTACL2_4_23.value=this.w_TDSTACL2
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL3_4_24.value==this.w_TDSTACL3)
      this.oPgFrm.Page4.oPag.oTDSTACL3_4_24.value=this.w_TDSTACL3
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL4_4_25.value==this.w_TDSTACL4)
      this.oPgFrm.Page4.oPag.oTDSTACL4_4_25.value=this.w_TDSTACL4
    endif
    if not(this.oPgFrm.Page4.oPag.oTDSTACL5_4_26.value==this.w_TDSTACL5)
      this.oPgFrm.Page4.oPag.oTDSTACL5_4_26.value=this.w_TDSTACL5
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMOD_4_35.value==this.w_DESMOD)
      this.oPgFrm.Page4.oPag.oDESMOD_4_35.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.RadioValue()==this.w_TDFLIMPA)
      this.oPgFrm.Page4.oPag.oTDFLIMPA_4_36.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.RadioValue()==this.w_TDFLIMAC)
      this.oPgFrm.Page4.oPag.oTDFLIMAC_4_37.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_2.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESC_5_3.value==this.w_DESC)
      this.oPgFrm.Page5.oPag.oDESC_5_3.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.RadioValue()==this.w_TDFLNSTA)
      this.oPgFrm.Page5.oPag.oTDFLNSTA_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTDFLSTLM_5_7.RadioValue()==this.w_TDFLSTLM)
      this.oPgFrm.Page5.oPag.oTDFLSTLM_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSTRU_3_57.value==this.w_DESSTRU)
      this.oPgFrm.Page3.oPag.oDESSTRU_3_57.value=this.w_DESSTRU
    endif
    if not(this.oPgFrm.Page1.oPag.oTDFLGEIN_1_96.RadioValue()==this.w_TDFLGEIN)
      this.oPgFrm.Page1.oPag.oTDFLGEIN_1_96.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA01_4_48.RadioValue()==this.w_TDFLIA01)
      this.oPgFrm.Page4.oPag.oTDFLIA01_4_48.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA02_4_49.RadioValue()==this.w_TDFLIA02)
      this.oPgFrm.Page4.oPag.oTDFLIA02_4_49.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA03_4_50.RadioValue()==this.w_TDFLIA03)
      this.oPgFrm.Page4.oPag.oTDFLIA03_4_50.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA04_4_51.RadioValue()==this.w_TDFLIA04)
      this.oPgFrm.Page4.oPag.oTDFLIA04_4_51.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA05_4_52.RadioValue()==this.w_TDFLIA05)
      this.oPgFrm.Page4.oPag.oTDFLIA05_4_52.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLIA06_4_53.RadioValue()==this.w_TDFLIA06)
      this.oPgFrm.Page4.oPag.oTDFLIA06_4_53.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA01_4_55.RadioValue()==this.w_TDFLRA01)
      this.oPgFrm.Page4.oPag.oTDFLRA01_4_55.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA02_4_65.RadioValue()==this.w_TDFLRA02)
      this.oPgFrm.Page4.oPag.oTDFLRA02_4_65.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA03_4_66.RadioValue()==this.w_TDFLRA03)
      this.oPgFrm.Page4.oPag.oTDFLRA03_4_66.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA04_4_67.RadioValue()==this.w_TDFLRA04)
      this.oPgFrm.Page4.oPag.oTDFLRA04_4_67.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA05_4_68.RadioValue()==this.w_TDFLRA05)
      this.oPgFrm.Page4.oPag.oTDFLRA05_4_68.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTDFLRA06_4_69.RadioValue()==this.w_TDFLRA06)
      this.oPgFrm.Page4.oPag.oTDFLRA06_4_69.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTDTIPDFE_3_61.value==this.w_TDTIPDFE)
      this.oPgFrm.Page3.oPag.oTDTIPDFE_3_61.value=this.w_TDTIPDFE
    endif
    if not(this.oPgFrm.Page3.oPag.oTDDESEST_3_62.value==this.w_TDDESEST)
      this.oPgFrm.Page3.oPag.oTDDESEST_3_62.value=this.w_TDDESEST
    endif
    if not(this.oPgFrm.Page3.oPag.oTDCODCLA_3_63.value==this.w_TDCODCLA)
      this.oPgFrm.Page3.oPag.oTDCODCLA_3_63.value=this.w_TDCODCLA
    endif
    cp_SetControlsValueExtFlds(this,'TIP_DOCU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TDTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDTIPDOC_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TDTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TDCATDOC))  and (Not .w_MOVIM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCATDOC_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TDCATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TDCAUMAG)) or not(CHKCAUMA(.w_TDCAUMAG,.w_CAUCOL,.w_TFFLGEFA,.w_FLAVAL,.w_DATOBSO,.w_OBTEST, 'A', .w_TDFLVEAC, .w_TDCATDOC)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCAUMAG_1_8.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (NOT EMPTY(.w_TDCAUMAG))  and not(empty(.w_TDCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCODMAG_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(EMPTY(.w_CAUCOL))  and (NOT EMPTY(.w_CAUCOL))  and not(empty(.w_TDCODMAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCODMAT_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   ((empty(.w_TDCAUCON)) or not((.w_CCFLANAL<>'S' OR g_PERCCR<>'S' OR (.w_TDFLELAN<>'S' AND .w_TDFLANAL<>'S')) AND CHKCAUDO(.w_TDCATDOC, ' ', .w_TDFLVEAC, .w_TIPDOC,.w_FLRIFE,.w_TIPREG) AND (.w_CCDATOBS>.w_OBTEST or EMPTY(nvl(.w_CCDATOBS,' ')))))  and (g_COGE='S' AND .w_TDCATDOC $ 'FA-NC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCAUCON_1_18.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)")
          case   not(NOT (g_COGE='S' AND .w_TDCATDOC $ 'FA-NC'))  and (NOT(.w_TDCATDOC $ 'FA-NC'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDFLPDOC_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo numerazione documento incongruente")
          case   not(.w_TDFLPPRO='P' AND NOT empty(.w_TDCAUCON)  OR .w_TDFLPPRO<>'P' )  and (g_COGE<>'S' OR NOT .w_TDCATDOC $ 'FA-NC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDFLPPRO_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo numerazione protocollo incongruente")
          case   not(.w_TDNUMSCO<g_NUMSCO+1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDNUMSCO_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire al massimo il num. sconti utilizzati definito nei dati azienda")
          case   not((.w_TDCATDOC $  'FA-NC' And .w_TDFLVALO='S') Or Empty( .w_TDFLVALO ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oTDFLVALO_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attivabile solo per fatture o note di credito")
          case   (empty(.w_TDVOCECR))  and not(NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))  and ((g_PERCCR='S' AND .w_TDFLANAL='S')  OR (g_COMM='S' AND .w_TDFLCOMM $ 'M-S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDVOCECR_3_27.SetFocus()
            i_bnoObbl = !empty(.w_TDVOCECR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TD_SEGNO))  and not(g_PERCCR<>'S' OR .w_TDFLANAL<>'S')  and (g_PERCCR='S' AND .w_TDFLANAL='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTD_SEGNO_3_28.SetFocus()
            i_bnoObbl = !empty(.w_TD_SEGNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TDCAUCES)) or not(.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)))  and not(g_CESP<>'S')  and (.w_TDASSCES='M')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUCES_3_30.SetFocus()
            i_bnoObbl = !empty(.w_TDCAUCES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
          case   not(EMPTY(.w_TDCAUPFI) OR (.w_CATPFI='DI' AND (.w_FLIPFI='N' OR .w_TDFLINTE=.w_FLIPFI)))  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (g_EACD='S'  AND .w_TDFLARCO='S')  and not(empty(.w_TDCAUPFI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUPFI_3_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
          case   not(EMPTY(.w_TDCAUCOD) OR (.w_CATCOD='DI' AND (.w_FLICOD='N' OR .w_TDFLINTE=.w_FLICOD)))  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (g_EACD='S' AND .w_TDFLARCO='S')  and not(empty(.w_TDCAUCOD))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDCAUCOD_3_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente o obsoleta")
          case   not(.w_TDMAXLEV>0)  and not(g_EACD<>'S' OR .w_TDFLARCO<>'S')  and (NOT EMPTY(.w_TDCAUCOD) AND g_DISB='S' AND .w_TDFLARCO='S' AND .w_TDFLEXPL<>'D' And .w_TDTIPIMB='N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTDMAXLEV_3_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, numero massimo livelli di esplosione non specificato")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAC_MDC.CheckForm()
      if i_bres
        i_bres=  .GSAC_MDC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSVE_MTD.CheckForm()
      if i_bres
        i_bres=  .GSVE_MTD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsac_atd
      * --- Controlli
      if i_bRes
         .w_RESCHK=0
           .NotifyEvent('CheckFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Controllo che le causali per imballi a perdere e rendere gestiscano l'intestatario
      if i_bRes And .w_TDFLARCO='S' And .w_TDTIPIMB<>'N' And (.w_FLIPFI='N' Or .w_FLICOD='N' Or .w_FLTCOM='S' Or .w_FLDCOM='S')
         i_bRes = .f.
         i_bnoChk = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Gestione imballi attiva: le causali per imballi a rendere e a perdere devono gestire l'intestatario e non avere attiva la gestione progetti")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TDTIPDOC = this.w_TDTIPDOC
    this.o_TDFLVEAC = this.w_TDFLVEAC
    this.o_TDCATDOC = this.w_TDCATDOC
    this.o_TDEMERIC = this.w_TDEMERIC
    this.o_TDCAUMAG = this.w_TDCAUMAG
    this.o_TDCAUCON = this.w_TDCAUCON
    this.o_TDFLINTE = this.w_TDFLINTE
    this.o_TDFLACCO = this.w_TDFLACCO
    this.o_TDFLEXPL = this.w_TDFLEXPL
    this.o_TDFLSILI = this.w_TDFLSILI
    this.o_TDFLANAL = this.w_TDFLANAL
    this.o_TDASSCES = this.w_TDASSCES
    this.o_TDFLARCO = this.w_TDFLARCO
    this.o_TDTIPIMB = this.w_TDTIPIMB
    this.o_TDFLCOMM = this.w_TDFLCOMM
    this.o_TDFLIA01 = this.w_TDFLIA01
    this.o_TDFLIA02 = this.w_TDFLIA02
    this.o_TDFLIA03 = this.w_TDFLIA03
    this.o_TDFLIA04 = this.w_TDFLIA04
    this.o_TDFLIA05 = this.w_TDFLIA05
    this.o_TDFLIA06 = this.w_TDFLIA06
    * --- GSAC_MDC : Depends On
    this.GSAC_MDC.SaveDependsOn()
    * --- GSVE_MTD : Depends On
    this.GSVE_MTD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsac_atdPag1 as StdContainer
  Width  = 740
  height = 554
  stdWidth  = 740
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTDTIPDOC_1_1 as StdField with uid="MUWPDNBNFZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TDTIPDOC", cQueryName = "TDTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di documento",;
    HelpContextID = 205249927,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=140, Top=9, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5)

  add object oTDDESDOC_1_2 as StdField with uid="JZUJKSOEBJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202431879,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=204, Top=9, InputMask=replicate('X',35)


  add object oTDCATDOC_1_4 as StdCombo with uid="NJVBJQVNIH",rtseq=4,rtrep=.f.,left=140,top=44,width=134,height=21;
    , ToolTipText = "Categoria di appartenenza del documento";
    , HelpContextID = 201649543;
    , cFormVar="w_TDCATDOC",RowSource=""+"Documento interno,"+"Doc. di trasporto,"+"Fattura,"+"Nota di credito", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTDCATDOC_1_4.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    space(2))))))
  endfunc
  func oTDCATDOC_1_4.GetRadio()
    this.Parent.oContained.w_TDCATDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDCATDOC_1_4.SetRadio()
    this.Parent.oContained.w_TDCATDOC=trim(this.Parent.oContained.w_TDCATDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDCATDOC=='DI',1,;
      iif(this.Parent.oContained.w_TDCATDOC=='DT',2,;
      iif(this.Parent.oContained.w_TDCATDOC=='FA',3,;
      iif(this.Parent.oContained.w_TDCATDOC=='NC',4,;
      0))))
  endfunc

  func oTDCATDOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_MOVIM)
    endwith
   endif
  endfunc


  add object oTDEMERIC_1_5 as StdCombo with uid="HCCAONOGSA",rtseq=5,rtrep=.f.,left=140,top=73,width=134,height=21;
    , ToolTipText = "Documento emesso/ricevuto: utilizzato per effettuare il controllo sulla numerazione del documento";
    , HelpContextID = 250137991;
    , cFormVar="w_TDEMERIC",RowSource=""+"Documento emesso,"+"Documento ricevuto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDEMERIC_1_5.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTDEMERIC_1_5.GetRadio()
    this.Parent.oContained.w_TDEMERIC = this.RadioValue()
    return .t.
  endfunc

  func oTDEMERIC_1_5.SetRadio()
    this.Parent.oContained.w_TDEMERIC=trim(this.Parent.oContained.w_TDEMERIC)
    this.value = ;
      iif(this.Parent.oContained.w_TDEMERIC=='V',1,;
      iif(this.Parent.oContained.w_TDEMERIC=='A',2,;
      0))
  endfunc

  func oTDEMERIC_1_5.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-OR' or isalt())
    endwith
  endfunc

  add object oTDCAUMAG_1_8 as StdField with uid="WYGMEZMCKN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TDCAUMAG", cQueryName = "TDCAUMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di movimentazione magazzino associata al documento",;
    HelpContextID = 218829437,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=109, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_TDCAUMAG"

  func oTDCAUMAG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUMAG_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUMAG_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oTDCAUMAG_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oTDCAUMAG_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_TDCAUMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_9 as StdField with uid="WTSJHCLHFP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 98046518,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=203, Top=109, InputMask=replicate('X',35)

  add object oTDCODMAG_1_14 as StdField with uid="UJBNFGSKOS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_TDCODMAG", cQueryName = "TDCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 201921149,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=138, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_TDCODMAG"

  func oTDCODMAG_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUMAG))
    endwith
   endif
  endfunc

  func oTDCODMAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODMAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODMAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oTDCODMAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oTDCODMAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_TDCODMAG
     i_obj.ecpSave()
  endproc


  add object oTDFLMGPR_1_15 as StdCombo with uid="UYYSHOJBNG",rtseq=15,rtrep=.f.,left=367,top=138,width=125,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino sui documenti sia caricati manualmente che generati o importati o di esplosione";
    , HelpContextID = 110510728;
    , cFormVar="w_TDFLMGPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDFLMGPR_1_15.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLMGPR_1_15.GetRadio()
    this.Parent.oContained.w_TDFLMGPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLMGPR_1_15.SetRadio()
    this.Parent.oContained.w_TDFLMGPR=trim(this.Parent.oContained.w_TDFLMGPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLMGPR=='D',1,;
      iif(this.Parent.oContained.w_TDFLMGPR=='O',2,;
      iif(this.Parent.oContained.w_TDFLMGPR=='F',3,;
      iif(this.Parent.oContained.w_TDFLMGPR=='I',4,;
      iif(this.Parent.oContained.w_TDFLMGPR=='P',5,;
      0)))))
  endfunc

  add object oTDCODMAT_1_16 as StdField with uid="NCYXYQMRBM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TDCODMAT", cQueryName = "TDCODMAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino collegato",;
    HelpContextID = 201921162,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=167, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_TDCODMAT"

  func oTDCODMAT_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDCODMAT_1_16.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  func oTDCODMAT_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODMAT_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODMAT_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oTDCODMAT_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oTDCODMAT_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_TDCODMAT
     i_obj.ecpSave()
  endproc


  add object oTDFLMTPR_1_17 as StdCombo with uid="RYBIACZKTN",rtseq=17,rtrep=.f.,left=367,top=167,width=125,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino sui documenti sia caricati manualmente che generati o importati o di esplosione";
    , HelpContextID = 60179080;
    , cFormVar="w_TDFLMTPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDFLMTPR_1_17.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLMTPR_1_17.GetRadio()
    this.Parent.oContained.w_TDFLMTPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLMTPR_1_17.SetRadio()
    this.Parent.oContained.w_TDFLMTPR=trim(this.Parent.oContained.w_TDFLMTPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLMTPR=='D',1,;
      iif(this.Parent.oContained.w_TDFLMTPR=='O',2,;
      iif(this.Parent.oContained.w_TDFLMTPR=='F',3,;
      iif(this.Parent.oContained.w_TDFLMTPR=='I',4,;
      iif(this.Parent.oContained.w_TDFLMTPR=='P',5,;
      0)))))
  endfunc

  func oTDFLMTPR_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDFLMTPR_1_17.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oTDCAUCON_1_18 as StdField with uid="PRJGJIHRCU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_TDCAUCON", cQueryName = "TDCAUCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o incongruente o obsoleta! (Verificare eventuale presenza del check analitica su entrambe le causali)",;
    ToolTipText = "Causale contabile associata al documento",;
    HelpContextID = 217378172,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=196, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_TDCAUCON"

  func oTDCAUCON_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S' AND .w_TDCATDOC $ 'FA-NC')
    endwith
   endif
  endfunc

  func oTDCAUCON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oTDCAUCON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSAC_ATD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oTDCAUCON_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_TDCAUCON
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_20 as StdField with uid="WXWZJWXWUK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63836726,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=203, Top=196, InputMask=replicate('X',35)

  add object oTDALFDOC_1_24 as StdField with uid="HWIWWJSNZF",rtseq=24,rtrep=.f.,;
    cFormVar = "w_TDALFDOC", cQueryName = "TDALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale parte alfanumerica del documento proposta di default",;
    HelpContextID = 215616903,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=140, Top=225, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oTDFLPDOC_1_25 as StdCheck with uid="KVFYIOCHUA",rtseq=25,rtrep=.f.,left=231, top=229, caption="Numeraz. vendite",;
    ToolTipText = "Numerazione documento allineata a quelli di vendita",;
    HelpContextID = 205110663,;
    cFormVar="w_TDFLPDOC", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Tipo numerazione documento incongruente";
   , bGlobalFont=.t.


  func oTDFLPDOC_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPDOC_1_25.GetRadio()
    this.Parent.oContained.w_TDFLPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPDOC_1_25.SetRadio()
    this.Parent.oContained.w_TDFLPDOC=trim(this.Parent.oContained.w_TDFLPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPDOC=='S',1,;
      0)
  endfunc

  func oTDFLPDOC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_TDCATDOC $ 'FA-NC'))
    endwith
   endif
  endfunc

  func oTDFLPDOC_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (g_COGE='S' AND .w_TDCATDOC $ 'FA-NC'))
    endwith
    return bRes
  endfunc

  add object oTDSERPRO_1_26 as StdField with uid="RNLALBRMSF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_TDSERPRO", cQueryName = "TDSERPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie del protocollo proposta in automatico",;
    HelpContextID = 266343045,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=140, Top=254, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)


  add object oTDFLPPRO_1_27 as StdCombo with uid="OPBJSYSTJL",rtseq=27,rtrep=.f.,left=231,top=254,width=112,height=21;
    , ToolTipText = "Tipo di numerazione protocollo";
    , HelpContextID = 264651397;
    , cFormVar="w_TDFLPPRO",RowSource=""+"Per anno,"+"Per esercizio,"+"Libera,"+"Non gestita,"+"Da cau. contabile", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Tipo numerazione protocollo incongruente";
  , bGlobalFont=.t.


  func oTDFLPPRO_1_27.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'N',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oTDFLPPRO_1_27.GetRadio()
    this.Parent.oContained.w_TDFLPPRO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPPRO_1_27.SetRadio()
    this.Parent.oContained.w_TDFLPPRO=trim(this.Parent.oContained.w_TDFLPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPPRO=='D',1,;
      iif(this.Parent.oContained.w_TDFLPPRO=='E',2,;
      iif(this.Parent.oContained.w_TDFLPPRO=='L',3,;
      iif(this.Parent.oContained.w_TDFLPPRO=='N',4,;
      iif(this.Parent.oContained.w_TDFLPPRO=='P',5,;
      0)))))
  endfunc

  func oTDFLPPRO_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S' OR NOT .w_TDCATDOC $ 'FA-NC')
    endwith
   endif
  endfunc

  func oTDFLPPRO_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDFLPPRO='P' AND NOT empty(.w_TDCAUCON)  OR .w_TDFLPPRO<>'P' )
    endwith
    return bRes
  endfunc

  add object oTDNUMSCO_1_28 as StdField with uid="KBHLQLNZQN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_TDNUMSCO", cQueryName = "TDNUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire al massimo il num. sconti utilizzati definito nei dati azienda",;
    ToolTipText = "Numero massimo di sconti/maggiorazioni utilizzate (max. 4)",;
    HelpContextID = 44024453,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=544, Top=254, cSayPict='"9"', cGetPict='"9"'

  func oTDNUMSCO_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDNUMSCO<g_NUMSCO+1)
    endwith
    return bRes
  endfunc

  add object oTDCODLIS_1_29 as StdField with uid="EUEHPSDHWW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_TDCODLIS", cQueryName = "TDCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino inesistente",;
    ToolTipText = "Listino proposto da utilizzare in mancanza del listino associato all'intestatario",;
    HelpContextID = 83291511,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=283, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_TDCODLIS"

  func oTDCODLIS_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODLIS_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODLIS_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oTDCODLIS_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Elenco listini",'',this.parent.oContained
  endproc
  proc oTDCODLIS_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_TDCODLIS
     i_obj.ecpSave()
  endproc

  add object oTDQTADEF_1_30 as StdField with uid="PXWTHWGZOP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_TDQTADEF", cQueryName = "TDQTADEF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale quantit� di riga proposta di default durante l'inserimento delle righe",;
    HelpContextID = 48165500,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=312, cSayPict='"99999999.999"', cGetPict='"99999999.999"'


  add object oTDPROVVI_1_31 as StdCombo with uid="RGNOPVHCJN",rtseq=31,rtrep=.f.,left=358,top=312,width=138,height=21;
    , ToolTipText = "Default stato documenti ";
    , HelpContextID = 96264831;
    , cFormVar="w_TDPROVVI",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTDPROVVI_1_31.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTDPROVVI_1_31.GetRadio()
    this.Parent.oContained.w_TDPROVVI = this.RadioValue()
    return .t.
  endfunc

  func oTDPROVVI_1_31.SetRadio()
    this.Parent.oContained.w_TDPROVVI=trim(this.Parent.oContained.w_TDPROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_TDPROVVI=='N',1,;
      iif(this.Parent.oContained.w_TDPROVVI=='S',2,;
      0))
  endfunc

  add object oDESLIS_1_32 as StdField with uid="YMEJVXRTQD",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39260726,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=203, Top=283, InputMask=replicate('X',40)

  add object oTDFLPREF_1_35 as StdCheck with uid="VQVYBSLCUI",rtseq=35,rtrep=.f.,left=555, top=12, caption="Preferenziale",;
    ToolTipText = "Se attivo: codice documento preferenziale della categoria di appartenenza",;
    HelpContextID = 29770364,;
    cFormVar="w_TDFLPREF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPREF_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPREF_1_35.GetRadio()
    this.Parent.oContained.w_TDFLPREF = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPREF_1_35.SetRadio()
    this.Parent.oContained.w_TDFLPREF=trim(this.Parent.oContained.w_TDFLPREF)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPREF=='S',1,;
      0)
  endfunc

  add object oTDFLINTE_1_36 as StdCheck with uid="KCSRSKQEYO",rtseq=36,rtrep=.f.,left=555, top=31, caption="Fornitore",;
    ToolTipText = "Se attivo: documento intestato a fornitore, altrimenti no intestazione",;
    HelpContextID = 223756923,;
    cFormVar="w_TDFLINTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLINTE_1_36.RadioValue()
    return(iif(this.value =1,'F',;
    'N'))
  endfunc
  func oTDFLINTE_1_36.GetRadio()
    this.Parent.oContained.w_TDFLINTE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLINTE_1_36.SetRadio()
    this.Parent.oContained.w_TDFLINTE=trim(this.Parent.oContained.w_TDFLINTE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLINTE=='F',1,;
      0)
  endfunc

  func oTDFLINTE_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='DI')
    endwith
   endif
  endfunc

  add object oTDNOPRSC_1_37 as StdCheck with uid="CNVJXVQNOG",rtseq=37,rtrep=.f.,left=555, top=50, caption="No prezzo/sconto",;
    ToolTipText = "Se attivo: il documento non prevede l'inserimento dei prezzi e sconti",;
    HelpContextID = 29999737,;
    cFormVar="w_TDNOPRSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDNOPRSC_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDNOPRSC_1_37.GetRadio()
    this.Parent.oContained.w_TDNOPRSC = this.RadioValue()
    return .t.
  endfunc

  func oTDNOPRSC_1_37.SetRadio()
    this.Parent.oContained.w_TDNOPRSC=trim(this.Parent.oContained.w_TDNOPRSC)
    this.value = ;
      iif(this.Parent.oContained.w_TDNOPRSC=='S',1,;
      0)
  endfunc

  add object oTDFLACCO_1_38 as StdCheck with uid="GAZRKHAQKH",rtseq=38,rtrep=.f.,left=555, top=69, caption="Dati accompagnatori",;
    ToolTipText = "Se attivo: documento con dati accompagnatori",;
    HelpContextID = 30818949,;
    cFormVar="w_TDFLACCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLACCO_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLACCO_1_38.GetRadio()
    this.Parent.oContained.w_TDFLACCO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLACCO_1_38.SetRadio()
    this.Parent.oContained.w_TDFLACCO=trim(this.Parent.oContained.w_TDFLACCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLACCO=='S',1,;
      0)
  endfunc

  add object oTDFLPACK_1_39 as StdCheck with uid="SDDKIEATNN",rtseq=39,rtrep=.f.,left=555, top=88, caption="Packing List",;
    ToolTipText = "Se attivo: documento elaborato attraverso la Packing List",;
    HelpContextID = 12993153,;
    cFormVar="w_TDFLPACK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLPACK_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPACK_1_39.GetRadio()
    this.Parent.oContained.w_TDFLPACK = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPACK_1_39.SetRadio()
    this.Parent.oContained.w_TDFLPACK=trim(this.Parent.oContained.w_TDFLPACK)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPACK=='S',1,;
      0)
  endfunc

  add object oTDFLDTPR_1_45 as StdCheck with uid="RSKHMFITHZ",rtseq=45,rtrep=.f.,left=555, top=107, caption="Dati consegna",;
    ToolTipText = "Se attivo: gestisce i dati di consegna sui documenti",;
    HelpContextID = 50741896,;
    cFormVar="w_TDFLDTPR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLDTPR_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLDTPR_1_45.GetRadio()
    this.Parent.oContained.w_TDFLDTPR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLDTPR_1_45.SetRadio()
    this.Parent.oContained.w_TDFLDTPR=trim(this.Parent.oContained.w_TDFLDTPR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLDTPR=='S',1,;
      0)
  endfunc

  add object oTDFLBACA_1_46 as StdCheck with uid="UEWCGRWHNO",rtseq=46,rtrep=.f.,left=555, top=126, caption="Basi di calcolo",;
    ToolTipText = "Se attivo, abilita la gestione delle basi di calcolo sui documenti",;
    HelpContextID = 266748535,;
    cFormVar="w_TDFLBACA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLBACA_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLBACA_1_46.GetRadio()
    this.Parent.oContained.w_TDFLBACA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLBACA_1_46.SetRadio()
    this.Parent.oContained.w_TDFLBACA=trim(this.Parent.oContained.w_TDFLBACA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLBACA=='S',1,;
      0)
  endfunc

  func oTDFLBACA_1_46.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oTDBOLDOG_1_48 as StdCheck with uid="RMOTHJZZJT",rtseq=48,rtrep=.f.,left=555, top=145, caption="Bolla doganale",;
    ToolTipText = "Se attivo, la prima nota generata dalla contabilizzazione non creer� righe omaggio contabili",;
    HelpContextID = 209124739,;
    cFormVar="w_TDBOLDOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDBOLDOG_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDBOLDOG_1_48.GetRadio()
    this.Parent.oContained.w_TDBOLDOG = this.RadioValue()
    return .t.
  endfunc

  func oTDBOLDOG_1_48.SetRadio()
    this.Parent.oContained.w_TDBOLDOG=trim(this.Parent.oContained.w_TDBOLDOG)
    this.value = ;
      iif(this.Parent.oContained.w_TDBOLDOG=='S',1,;
      0)
  endfunc

  func oTDBOLDOG_1_48.mHide()
    with this.Parent.oContained
      return (.w_TDFLVEAC='V' Or Not .w_TDCATDOC $ 'FA-NC')
    endwith
  endfunc

  add object oTDFLCCAU_1_50 as StdCheck with uid="ZXSLUBLRQP",rtseq=49,rtrep=.f.,left=555, top=164, caption="Cambio causale mag.",;
    ToolTipText = "Attivo: abilita la modifica della causale di magazzino nei dati di riga documenti",;
    HelpContextID = 32916107,;
    cFormVar="w_TDFLCCAU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLCCAU_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLCCAU_1_50.GetRadio()
    this.Parent.oContained.w_TDFLCCAU = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCCAU_1_50.SetRadio()
    this.Parent.oContained.w_TDFLCCAU=trim(this.Parent.oContained.w_TDFLCCAU)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCCAU=='S',1,;
      0)
  endfunc

  add object oTDMODDES_1_51 as StdCheck with uid="TJGGPXKTYD",rtseq=50,rtrep=.f.,left=555, top=183, caption="Modifica descrizione articolo",;
    ToolTipText = "Se attivo, sulla riga del documento risulter� possibile modificare la descrizione/ descrizione supplementare dell'articolo",;
    HelpContextID = 50967177,;
    cFormVar="w_TDMODDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDMODDES_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDMODDES_1_51.GetRadio()
    this.Parent.oContained.w_TDMODDES = this.RadioValue()
    return .t.
  endfunc

  func oTDMODDES_1_51.SetRadio()
    this.Parent.oContained.w_TDMODDES=trim(this.Parent.oContained.w_TDMODDES)
    this.value = ;
      iif(this.Parent.oContained.w_TDMODDES=='S',1,;
      0)
  endfunc

  add object oTDSEQPRE_1_61 as StdCheck with uid="ZIPEZWZBNP",rtseq=53,rtrep=.f.,left=10, top=397, caption="Prezzo unitario",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 265294459,;
    cFormVar="w_TDSEQPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQPRE_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQPRE_1_61.GetRadio()
    this.Parent.oContained.w_TDSEQPRE = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQPRE_1_61.SetRadio()
    this.Parent.oContained.w_TDSEQPRE=trim(this.Parent.oContained.w_TDSEQPRE)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQPRE=='S',1,;
      0)
  endfunc

  add object oTDSEQSCO_1_62 as StdCheck with uid="POKNWEJPPZ",rtseq=54,rtrep=.f.,left=10, top=420, caption="Sconti",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 47190661,;
    cFormVar="w_TDSEQSCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQSCO_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQSCO_1_62.GetRadio()
    this.Parent.oContained.w_TDSEQSCO = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQSCO_1_62.SetRadio()
    this.Parent.oContained.w_TDSEQSCO=trim(this.Parent.oContained.w_TDSEQSCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQSCO=='S',1,;
      0)
  endfunc

  add object oTDSEQMA1_1_63 as StdCheck with uid="IKFBVEEYIO",rtseq=55,rtrep=.f.,left=130, top=397, caption="Magazzino principale",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 214962791,;
    cFormVar="w_TDSEQMA1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQMA1_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQMA1_1_63.GetRadio()
    this.Parent.oContained.w_TDSEQMA1 = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQMA1_1_63.SetRadio()
    this.Parent.oContained.w_TDSEQMA1=trim(this.Parent.oContained.w_TDSEQMA1)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQMA1=='S',1,;
      0)
  endfunc

  add object oTDSEQMA2_1_64 as StdCheck with uid="FIEDUBBJNU",rtseq=56,rtrep=.f.,left=130, top=420, caption="Magazzino collegato",;
    ToolTipText = "Se attivo: fuori sequenza durante l'inserimento riga documento",;
    HelpContextID = 214962792,;
    cFormVar="w_TDSEQMA2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDSEQMA2_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDSEQMA2_1_64.GetRadio()
    this.Parent.oContained.w_TDSEQMA2 = this.RadioValue()
    return .t.
  endfunc

  func oTDSEQMA2_1_64.SetRadio()
    this.Parent.oContained.w_TDSEQMA2=trim(this.Parent.oContained.w_TDSEQMA2)
    this.value = ;
      iif(this.Parent.oContained.w_TDSEQMA2=='S',1,;
      0)
  endfunc

  func oTDSEQMA2_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oTDSEQMA2_1_64.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc


  add object oObj_1_84 as cp_runprogram with uid="PMPPDSGNKI",left=1, top=561, width=179,height=19,;
    caption='GSAR_BTD(CA)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('CA')",;
    cEvent = "CheckFinali",;
    nPag=1;
    , ToolTipText = "Check all'F10 (da gsac_atd/gsve_atd)";
    , HelpContextID = 48846378


  add object oObj_1_85 as cp_runprogram with uid="WXOYMSQPBQ",left=190, top=561, width=205,height=20,;
    caption='GSAR_BTD(ED)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('ED')",;
    cEvent = "Edit Started",;
    nPag=1;
    , ToolTipText = "Verifica se la casuale � gia stata movimentata (impedisce modifica tipo)";
    , HelpContextID = 48859178


  add object oObj_1_89 as cp_runprogram with uid="WYBNWTKUEH",left=422, top=558, width=142,height=26,;
    caption='GSVE_BCR',;
   bGlobalFont=.t.,;
    prg="GSVE_BCR",;
    cEvent = "ChkOrigini",;
    nPag=1;
    , HelpContextID = 45109432

  add object oTDFLGEIN_1_96 as StdCheck with uid="SRIFJGRQKA",rtseq=166,rtrep=.f.,left=555, top=202, caption="Genera INTRA",;
    ToolTipText = "Se attivo genera gli elenchi intra per i documenti di trasporto",;
    HelpContextID = 197770620,;
    cFormVar="w_TDFLGEIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDFLGEIN_1_96.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLGEIN_1_96.GetRadio()
    this.Parent.oContained.w_TDFLGEIN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGEIN_1_96.SetRadio()
    this.Parent.oContained.w_TDFLGEIN=trim(this.Parent.oContained.w_TDFLGEIN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGEIN=='S',1,;
      0)
  endfunc

  func oTDFLGEIN_1_96.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="TUXWQXSLSC",Visible=.t., Left=13, Top=9,;
    Alignment=1, Width=125, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="URDHUVEFSK",Visible=.t., Left=13, Top=44,;
    Alignment=1, Width=125, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="KGFERLFAFF",Visible=.t., Left=13, Top=109,;
    Alignment=1, Width=125, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="DCBJINRORY",Visible=.t., Left=13, Top=196,;
    Alignment=1, Width=125, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="SBXJJFXBZB",Visible=.t., Left=60, Top=138,;
    Alignment=1, Width=78, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="KBYKWINDGB",Visible=.t., Left=8, Top=167,;
    Alignment=1, Width=130, Height=15,;
    Caption="Magazzino collegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="GDHHWGEPPR",Visible=.t., Left=360, Top=254,;
    Alignment=1, Width=178, Height=15,;
    Caption="Sconti/maggiorazioni utilizzati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="VFEIKLGIRJ",Visible=.t., Left=13, Top=225,;
    Alignment=1, Width=125, Height=15,;
    Caption="Serie documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="LWAEZJBJIY",Visible=.t., Left=7, Top=371,;
    Alignment=0, Width=265, Height=18,;
    Caption="Campi fuori sequenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="WMDIWTJOFP",Visible=.t., Left=13, Top=283,;
    Alignment=1, Width=125, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="IFHSDAFYKO",Visible=.t., Left=51, Top=312,;
    Alignment=1, Width=87, Height=15,;
    Caption="Qt� proposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="QFPIGWOGYK",Visible=.t., Left=19, Top=254,;
    Alignment=1, Width=119, Height=18,;
    Caption="Serie protocollo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="KATNUYNIVZ",Visible=.t., Left=220, Top=138,;
    Alignment=1, Width=143, Height=18,;
    Caption="Valorizzazione magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="ELTLPSSSWG",Visible=.t., Left=218, Top=167,;
    Alignment=1, Width=145, Height=18,;
    Caption="Valorizzazione magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="LLDJZYJYKY",Visible=.t., Left=8, Top=74,;
    Alignment=1, Width=130, Height=15,;
    Caption="Doc. emesso/ricevuto:"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'FA-NC-OR-RF' or isalt())
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="OQYIBKOOCQ",Visible=.t., Left=222, Top=312,;
    Alignment=1, Width=134, Height=18,;
    Caption="Stato dei documenti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_66 as StdBox with uid="OXZGYUDHMG",left=4, top=390, width=279,height=57
enddefine
define class tgsac_atdPag2 as StdContainer
  Width  = 740
  height = 554
  stdWidth  = 740
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_1 as cp_runprogram with uid="TXVYSMWUJJ",left=5, top=555, width=205,height=20,;
    caption='GSAR_BTD(AS)',;
   bGlobalFont=.t.,;
    prg="GSAR_BTD('AS')",;
    cEvent = "w_TDASSCES Changed,w_TDCAUCON Changed",;
    nPag=2;
    , ToolTipText = "Verifica l'associazione cespiti (impedisce di impostare movimenti cespiti senza cau. Cont.)";
    , HelpContextID = 48919594

  add object oTDFLSILI_2_2 as StdCheck with uid="PGVMWEBFAE",rtseq=73,rtrep=.f.,left=32, top=61, caption="Agg.lettere di intento",;
    ToolTipText = "Se attivo: aggiorna gli importi relativi ad eventuali lettere di intento",;
    HelpContextID = 118078849,;
    cFormVar="w_TDFLSILI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSILI_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLSILI_2_2.GetRadio()
    this.Parent.oContained.w_TDFLSILI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSILI_2_2.SetRadio()
    this.Parent.oContained.w_TDFLSILI=trim(this.Parent.oContained.w_TDFLSILI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSILI=='S',1,;
      0)
  endfunc

  func oTDFLSILI_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC $ 'DT-FA-NC')
    endwith
   endif
  endfunc

  func oTDFLSILI_2_2.mHide()
    with this.Parent.oContained
      return (NOT .w_TDCATDOC $ 'DT-FA-NC')
    endwith
  endfunc

  add object oTDPRZVAC_2_3 as StdCheck with uid="RBFCRVZYZK",rtseq=74,rtrep=.f.,left=32, top=86, caption="Prezzo default U.C.A.",;
    ToolTipText = "Prezzo e sconti, in assenza di altre condizioni valide, verranno calcolati dall'ultimo movimento di acquisto",;
    HelpContextID = 107799161,;
    cFormVar="w_TDPRZVAC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDPRZVAC_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDPRZVAC_2_3.GetRadio()
    this.Parent.oContained.w_TDPRZVAC = this.RadioValue()
    return .t.
  endfunc

  func oTDPRZVAC_2_3.SetRadio()
    this.Parent.oContained.w_TDPRZVAC=trim(this.Parent.oContained.w_TDPRZVAC)
    this.value = ;
      iif(this.Parent.oContained.w_TDPRZVAC=='S',1,;
      0)
  endfunc

  add object oTDPRZDES_2_4 as StdCheck with uid="IKJBEPAXYS",rtseq=75,rtrep=.f.,left=32, top=111, caption="Ricalcolo prezzi",;
    ToolTipText = "Se attivo, in importazione vengono ricalcolati prezzo e sconti in base ai dati del documento di destinazione",;
    HelpContextID = 74244745,;
    cFormVar="w_TDPRZDES", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDPRZDES_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDPRZDES_2_4.GetRadio()
    this.Parent.oContained.w_TDPRZDES = this.RadioValue()
    return .t.
  endfunc

  func oTDPRZDES_2_4.SetRadio()
    this.Parent.oContained.w_TDPRZDES=trim(this.Parent.oContained.w_TDPRZDES)
    this.value = ;
      iif(this.Parent.oContained.w_TDPRZDES=='S',1,;
      0)
  endfunc

  add object oTDFLVALO_2_5 as StdCheck with uid="ZIIQAGEIFH",rtseq=76,rtrep=.f.,left=32, top=136, caption="Modifica valore",;
    ToolTipText = "Fattura spese, consente la modifica in valore di dt rivalorizzati o fatture immediate",;
    HelpContextID = 249150843,;
    cFormVar="w_TDFLVALO", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Attivabile solo per fatture o note di credito";
   , bGlobalFont=.t.


  func oTDFLVALO_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLVALO_2_5.GetRadio()
    this.Parent.oContained.w_TDFLVALO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLVALO_2_5.SetRadio()
    this.Parent.oContained.w_TDFLVALO=trim(this.Parent.oContained.w_TDFLVALO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLVALO=='S',1,;
      0)
  endfunc

  func oTDFLVALO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TDCATDOC $  'FA-NC' And .w_TDFLVALO='S') Or Empty( .w_TDFLVALO ))
    endwith
    return bRes
  endfunc

  add object oTDCHKTOT_2_6 as StdCheck with uid="ZXBBYGQYTV",rtseq=77,rtrep=.f.,left=32, top=161, caption="Righe normali <> 0",;
    ToolTipText = "Attivo: impedisce il salvataggio del documento se presenti righe a valore 0",;
    HelpContextID = 57807498,;
    cFormVar="w_TDCHKTOT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDCHKTOT_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDCHKTOT_2_6.GetRadio()
    this.Parent.oContained.w_TDCHKTOT = this.RadioValue()
    return .t.
  endfunc

  func oTDCHKTOT_2_6.SetRadio()
    this.Parent.oContained.w_TDCHKTOT=trim(this.Parent.oContained.w_TDCHKTOT)
    this.value = ;
      iif(this.Parent.oContained.w_TDCHKTOT=='S',1,;
      0)
  endfunc

  add object oTDFLBLEV_2_7 as StdCheck with uid="PJHYBBYPMU",rtseq=78,rtrep=.f.,left=32, top=186, caption="Blocca doc. evasi",;
    ToolTipText = "Se attivo: impedisce la modifica dei documenti evasi (anche parzialmente)",;
    HelpContextID = 182862476,;
    cFormVar="w_TDFLBLEV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLBLEV_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLBLEV_2_7.GetRadio()
    this.Parent.oContained.w_TDFLBLEV = this.RadioValue()
    return .t.
  endfunc

  func oTDFLBLEV_2_7.SetRadio()
    this.Parent.oContained.w_TDFLBLEV=trim(this.Parent.oContained.w_TDFLBLEV)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLBLEV=='S',1,;
      0)
  endfunc

  add object oTDFLSPIN_2_8 as StdCheck with uid="RAIGPBIDES",rtseq=79,rtrep=.f.,left=32, top=211, caption="Spese incasso",;
    ToolTipText = "Attivo: sul documento le spese di incasso saranno calcolate e modificabili",;
    HelpContextID = 638332,;
    cFormVar="w_TDFLSPIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPIN_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLSPIN_2_8.GetRadio()
    this.Parent.oContained.w_TDFLSPIN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPIN_2_8.SetRadio()
    this.Parent.oContained.w_TDFLSPIN=trim(this.Parent.oContained.w_TDFLSPIN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPIN=='S',1,;
      0)
  endfunc

  add object oTDFLQRIO_2_9 as StdCheck with uid="KMOSIDXBZZ",rtseq=80,rtrep=.f.,left=32, top=236, caption="Calcolo qt� riordino",;
    ToolTipText = "Se attivo: la quantit� viene ricalcolata in base alla quantit� minima e al lotto di riordino. Check no evasione e raggruppa nelle origini obbligatorio.",;
    HelpContextID = 237616507,;
    cFormVar="w_TDFLQRIO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLQRIO_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLQRIO_2_9.GetRadio()
    this.Parent.oContained.w_TDFLQRIO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLQRIO_2_9.SetRadio()
    this.Parent.oContained.w_TDFLQRIO=trim(this.Parent.oContained.w_TDFLQRIO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLQRIO=='S',1,;
      0)
  endfunc

  add object oTDFLPREV_2_10 as StdCheck with uid="YXKRIWCVMG",rtseq=81,rtrep=.f.,left=32, top=261, caption="Calcolo data evasione",;
    ToolTipText = "Se attivo: la data prevista evasione viene calcolata in base ai giorni approvvigionamento",;
    HelpContextID = 29770380,;
    cFormVar="w_TDFLPREV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLPREV_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLPREV_2_10.GetRadio()
    this.Parent.oContained.w_TDFLPREV = this.RadioValue()
    return .t.
  endfunc

  func oTDFLPREV_2_10.SetRadio()
    this.Parent.oContained.w_TDFLPREV=trim(this.Parent.oContained.w_TDFLPREV)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLPREV=='S',1,;
      0)
  endfunc

  add object oTDFLAPCA_2_11 as StdCheck with uid="EYKGDCBJDX",rtseq=82,rtrep=.f.,left=32, top=286, caption="Applica contributi accessori",;
    ToolTipText = "Se attivo sul documento sar� applicato il contributo accessorio. Il contributo con applicazione su peso viene applicato solo se la categoria documento � fattura o nota di credito",;
    HelpContextID = 248922743,;
    cFormVar="w_TDFLAPCA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLAPCA_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDFLAPCA_2_11.GetRadio()
    this.Parent.oContained.w_TDFLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLAPCA_2_11.SetRadio()
    this.Parent.oContained.w_TDFLAPCA=trim(this.Parent.oContained.w_TDFLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLAPCA=='S',1,;
      0)
  endfunc

  func oTDFLAPCA_2_11.mHide()
    with this.Parent.oContained
      return (g_COAC='N' OR .w_TDFLINTE='N')
    endwith
  endfunc

  add object oTDRIPCON_2_12 as StdCheck with uid="TOFXIKTXEB",rtseq=83,rtrep=.f.,left=32, top=308, caption="Ripartisce contributi accessori",;
    ToolTipText = "Permette la ripartizione del valore dei contributi accessori sul valore fiscale dell'articolo che li ha generati",;
    HelpContextID = 222035324,;
    cFormVar="w_TDRIPCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPCON_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPCON_2_12.GetRadio()
    this.Parent.oContained.w_TDRIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPCON_2_12.SetRadio()
    this.Parent.oContained.w_TDRIPCON=trim(this.Parent.oContained.w_TDRIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPCON=='S',1,;
      0)
  endfunc

  func oTDRIPCON_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC $ "FA-NC" AND g_COAC = "S" AND .w_TDFLINTE <> "N")
    endwith
   endif
  endfunc

  func oTDRIPCON_2_12.mHide()
    with this.Parent.oContained
      return (NOT ( .w_TDCATDOC $ "FA-NC" AND g_COAC = "S" AND .w_TDFLINTE <> "N" ))
    endwith
  endfunc

  add object oTDNOSTCO_2_13 as StdCheck with uid="AAHNRPZIJX",rtseq=84,rtrep=.f.,left=33, top=337, caption="Non stampa i contributi accessori",;
    ToolTipText = "Se attivo sulla stampa del documento non saranno riportate le righe di contributo accessorio",;
    HelpContextID = 66699909,;
    cFormVar="w_TDNOSTCO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDNOSTCO_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDNOSTCO_2_13.GetRadio()
    this.Parent.oContained.w_TDNOSTCO = this.RadioValue()
    return .t.
  endfunc

  func oTDNOSTCO_2_13.SetRadio()
    this.Parent.oContained.w_TDNOSTCO=trim(this.Parent.oContained.w_TDNOSTCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDNOSTCO=='S',1,;
      0)
  endfunc

  func oTDNOSTCO_2_13.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oTDFLGETR_2_14 as StdCheck with uid="KYLTSFMISU",rtseq=85,rtrep=.f.,left=32, top=360, caption="Gestione scheda di trasporto",;
    ToolTipText = "Se attivo la causale gestisce la stampa della scheda di trasporto",;
    HelpContextID = 70664840,;
    cFormVar="w_TDFLGETR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLGETR_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLGETR_2_14.GetRadio()
    this.Parent.oContained.w_TDFLGETR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLGETR_2_14.SetRadio()
    this.Parent.oContained.w_TDFLGETR=trim(this.Parent.oContained.w_TDFLGETR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLGETR=='S',1,;
      0)
  endfunc

  func oTDFLGETR_2_14.mHide()
    with this.Parent.oContained
      return (!(.w_TDCATDOC='FA' OR .w_TDCATDOC='DT'))
    endwith
  endfunc

  add object oTDFLSPIM_2_15 as StdCheck with uid="IVXEZLNTUQ",rtseq=86,rtrep=.f.,left=331, top=61, caption="Cal. spese imballo",;
    ToolTipText = "Se attivo: calcola le spese di imballo utilizzando il metodo specificato",;
    HelpContextID = 638333,;
    cFormVar="w_TDFLSPIM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPIM_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPIM_2_15.GetRadio()
    this.Parent.oContained.w_TDFLSPIM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPIM_2_15.SetRadio()
    this.Parent.oContained.w_TDFLSPIM=trim(this.Parent.oContained.w_TDFLSPIM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPIM=='S',1,;
      0)
  endfunc

  add object oMSDESIMB_2_16 as StdField with uid="EXXNFXGNRC",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MSDESIMB", cQueryName = "MSDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 118542072,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=386, Top=189, InputMask=replicate('X',40)

  add object oTDFLSPTR_2_18 as StdCheck with uid="PYKSBMCTMR",rtseq=88,rtrep=.f.,left=331, top=86, caption="Cal. spese trasporto",;
    ToolTipText = "Se attivo: calcola le spese di trasporto utilizzando il metodo specificato",;
    HelpContextID = 267797128,;
    cFormVar="w_TDFLSPTR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDFLSPTR_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLSPTR_2_18.GetRadio()
    this.Parent.oContained.w_TDFLSPTR = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSPTR_2_18.SetRadio()
    this.Parent.oContained.w_TDFLSPTR=trim(this.Parent.oContained.w_TDFLSPTR)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSPTR=='S',1,;
      0)
  endfunc

  add object oMSDESTRA_2_19 as StdField with uid="TLNMVKZNNK",rtseq=89,rtrep=.f.,;
    cFormVar = "w_MSDESTRA", cQueryName = "MSDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66007303,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=386, Top=214, InputMask=replicate('X',40)

  add object oTDRIPINC_2_21 as StdCheck with uid="GTJXIWQURJ",rtseq=90,rtrep=.f.,left=331, top=111, caption="Ripartisce spese incasso",;
    ToolTipText = "Ripartisce spese incasso",;
    HelpContextID = 121372039,;
    cFormVar="w_TDRIPINC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPINC_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPINC_2_21.GetRadio()
    this.Parent.oContained.w_TDRIPINC = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPINC_2_21.SetRadio()
    this.Parent.oContained.w_TDRIPINC=trim(this.Parent.oContained.w_TDRIPINC)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPINC=='S',1,;
      0)
  endfunc

  func oTDRIPINC_2_21.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S')
    endwith
  endfunc

  add object oTDRIPIMB_2_22 as StdCheck with uid="PBBMHENXKC",rtseq=91,rtrep=.f.,left=331, top=136, caption="Ripartisce spese imballo",;
    ToolTipText = "Ripartisce spese imballo",;
    HelpContextID = 121372040,;
    cFormVar="w_TDRIPIMB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPIMB_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDRIPIMB_2_22.GetRadio()
    this.Parent.oContained.w_TDRIPIMB = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPIMB_2_22.SetRadio()
    this.Parent.oContained.w_TDRIPIMB=trim(this.Parent.oContained.w_TDRIPIMB)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPIMB=='S',1,;
      0)
  endfunc

  func oTDRIPIMB_2_22.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S')
    endwith
  endfunc

  add object oTDRIPTRA_2_23 as StdCheck with uid="VMAVNFCYFX",rtseq=92,rtrep=.f.,left=331, top=161, caption="Ripartisce spese trasporto",;
    ToolTipText = "Ripartisce spese trasporto",;
    HelpContextID = 63177335,;
    cFormVar="w_TDRIPTRA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTDRIPTRA_2_23.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oTDRIPTRA_2_23.GetRadio()
    this.Parent.oContained.w_TDRIPTRA = this.RadioValue()
    return .t.
  endfunc

  func oTDRIPTRA_2_23.SetRadio()
    this.Parent.oContained.w_TDRIPTRA=trim(this.Parent.oContained.w_TDRIPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_TDRIPTRA=="S",1,;
      0)
  endfunc

  func oTDRIPTRA_2_23.mHide()
    with this.Parent.oContained
      return (g_DETCON = 'S')
    endwith
  endfunc

  add object oTDMCALSI_2_24 as StdField with uid="JLHSZNGLBT",rtseq=93,rtrep=.f.,;
    cFormVar = "w_TDMCALSI", cQueryName = "TDMCALSI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di imballo",;
    HelpContextID = 181252735,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=331, Top=189, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALSI"

  func oTDMCALSI_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALSI_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALSI_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALSI_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALSI_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALSI
     i_obj.ecpSave()
  endproc

  add object oTDMCALST_2_25 as StdField with uid="DRHPOHHQPC",rtseq=94,rtrep=.f.,;
    cFormVar = "w_TDMCALST", cQueryName = "TDMCALST",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 181252746,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=331, Top=214, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_TDMCALST"

  func oTDMCALST_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMCALST_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMCALST_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oTDMCALST_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oTDMCALST_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_TDMCALST
     i_obj.ecpSave()
  endproc


  add object oTDSINCFL_2_26 as StdCombo with uid="ROIYQNNVWC",value=1,rtseq=95,rtrep=.f.,left=331,top=241,width=134,height=21;
    , HelpContextID = 44307074;
    , cFormVar="w_TDSINCFL",RowSource=""+"Non gestito,"+"Positivo,"+"Negativo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDSINCFL_2_26.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,+1,;
    iif(this.value =3,-1,;
    0))))
  endfunc
  func oTDSINCFL_2_26.GetRadio()
    this.Parent.oContained.w_TDSINCFL = this.RadioValue()
    return .t.
  endfunc

  func oTDSINCFL_2_26.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TDSINCFL==0,1,;
      iif(this.Parent.oContained.w_TDSINCFL==+1,2,;
      iif(this.Parent.oContained.w_TDSINCFL==-1,3,;
      0)))
  endfunc

  proc oTDSINCFL_2_26.mDefault
    with this.Parent.oContained
      if empty(.w_TDSINCFL)
        .w_TDSINCFL = 0
      endif
    endwith
  endproc

  add object oCODI_2_28 as StdField with uid="UNGZWUCIGL",rtseq=96,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87872474,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=143, Top=21, InputMask=replicate('X',5)

  add object oDESC_2_29 as StdField with uid="OYBEZXDPPM",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88206794,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=207, Top=21, InputMask=replicate('X',35)


  add object oTDFLRISC_2_30 as StdCombo with uid="FRVSWBXGZU",value=3,rtseq=98,rtrep=.f.,left=331,top=266,width=134,height=21;
    , ToolTipText = "Stampa esposizione finanziaria. Incrementa: aumenta l'esposizione finanziaria del fornitore. Decrementa l'esposizione finanziaria del fornitore";
    , HelpContextID = 149308025;
    , cFormVar="w_TDFLRISC",RowSource=""+"Incrementa,"+"Decrementa,"+"Non partecipa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTDFLRISC_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTDFLRISC_2_30.GetRadio()
    this.Parent.oContained.w_TDFLRISC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRISC_2_30.SetRadio()
    this.Parent.oContained.w_TDFLRISC=trim(this.Parent.oContained.w_TDFLRISC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRISC=='S',1,;
      iif(this.Parent.oContained.w_TDFLRISC=='D',2,;
      iif(this.Parent.oContained.w_TDFLRISC=='',3,;
      0)))
  endfunc

  func oTDFLRISC_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLINTE='F')
    endwith
   endif
  endfunc

  func oTDFLRISC_2_30.mHide()
    with this.Parent.oContained
      return (.w_TDFLINTE<>'F' Or .w_TDCATDOC='DI')
    endwith
  endfunc


  add object oBtn_2_33 as StdButton with uid="PKKSUWHQWC",left=679, top=61, width=48,height=45,;
    CpPicture="bmp\INTENTI.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al visualizza utilizzo dichiarazioni di intento per causale";
    , HelpContextID = 92956426;
    , caption='\<Dic. Int.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_33.Click()
      do GSCG_KDZ with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_2_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_TDCATDOC $ 'DT-FA-NC')
     endwith
    endif
  endfunc

  add object oStr_2_17 as StdString with uid="IWCGUKYCVV",Visible=.t., Left=200, Top=189,;
    Alignment=1, Width=125, Height=18,;
    Caption="M. c. spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="BPBWYLXMQF",Visible=.t., Left=192, Top=214,;
    Alignment=1, Width=133, Height=18,;
    Caption="M. c. spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="GUJIWKUXLV",Visible=.t., Left=24, Top=21,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="NJBMOAAHMQ",Visible=.t., Left=202, Top=266,;
    Alignment=1, Width=123, Height=17,;
    Caption="Esposizione finaziaria:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (.w_TDFLINTE<>'F' Or .w_TDCATDOC='DI')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="HFNIEPGVYG",Visible=.t., Left=263, Top=241,;
    Alignment=1, Width=62, Height=18,;
    Caption="Cash flow:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsac_atdPag3 as StdContainer
  Width  = 740
  height = 554
  stdWidth  = 740
  stdheight = 554
  resizeYpos=227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCAUCE_3_14 as StdField with uid="SDMRNHSDUX",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESCAUCE", cQueryName = "DESCAUCE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 63836795,;
   bGlobalFont=.t.,;
    Height=21, Width=339, Left=212, Top=167, InputMask=replicate('X',40)

  func oDESCAUCE_3_14.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oDESPFI_3_19 as StdField with uid="XNHSJVLOIL",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESPFI", cQueryName = "DESPFI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 137040438,;
   bGlobalFont=.t.,;
    Height=21, Width=341, Left=210, Top=301, InputMask=replicate('X',35)

  func oDESPFI_3_19.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oDESCOD_3_20 as StdField with uid="HMKEHMOSQK",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESCOD", cQueryName = "DESCOD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61739574,;
   bGlobalFont=.t.,;
    Height=21, Width=341, Left=210, Top=329, InputMask=replicate('X',35)

  func oDESCOD_3_20.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDFLANAL_3_25 as StdCheck with uid="ZDPRWZIYVV",rtseq=102,rtrep=.f.,left=147, top=56, caption="Dati analitica",;
    ToolTipText = "Se attivo: abilita l'inserimento dei dati di analitica sul documento",;
    HelpContextID = 215368322,;
    cFormVar="w_TDFLANAL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLANAL_3_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLANAL_3_25.GetRadio()
    this.Parent.oContained.w_TDFLANAL = this.RadioValue()
    return .t.
  endfunc

  func oTDFLANAL_3_25.SetRadio()
    this.Parent.oContained.w_TDFLANAL=trim(this.Parent.oContained.w_TDFLANAL)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLANAL=='S',1,;
      0)
  endfunc

  func oTDFLANAL_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oTDFLANAL_3_25.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oTDFLELAN_3_26 as StdCheck with uid="SOZYCQKRKP",rtseq=103,rtrep=.f.,left=147, top=84, caption="Movimento di analitica",;
    ToolTipText = "Se attivo movimenta l'analitica",;
    HelpContextID = 186008196,;
    cFormVar="w_TDFLELAN", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLELAN_3_26.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLELAN_3_26.GetRadio()
    this.Parent.oContained.w_TDFLELAN = this.RadioValue()
    return .t.
  endfunc

  func oTDFLELAN_3_26.SetRadio()
    this.Parent.oContained.w_TDFLELAN=trim(this.Parent.oContained.w_TDFLELAN)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLELAN=='S',1,;
      0)
  endfunc

  func oTDFLELAN_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_TDFLANAL='S')
    endwith
   endif
  endfunc

  func oTDFLELAN_3_26.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc


  add object oTDVOCECR_3_27 as StdCombo with uid="UZLRKJSNPK",rtseq=104,rtrep=.f.,left=431,top=56,width=107,height=21;
    , ToolTipText = "Voce costo/ricavo di analitica";
    , HelpContextID = 66732680;
    , cFormVar="w_TDVOCECR",RowSource=""+"di costo,"+"di ricavo", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oTDVOCECR_3_27.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTDVOCECR_3_27.GetRadio()
    this.Parent.oContained.w_TDVOCECR = this.RadioValue()
    return .t.
  endfunc

  func oTDVOCECR_3_27.SetRadio()
    this.Parent.oContained.w_TDVOCECR=trim(this.Parent.oContained.w_TDVOCECR)
    this.value = ;
      iif(this.Parent.oContained.w_TDVOCECR=='C',1,;
      iif(this.Parent.oContained.w_TDVOCECR=='R',2,;
      0))
  endfunc

  func oTDVOCECR_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' AND .w_TDFLANAL='S')  OR (g_COMM='S' AND .w_TDFLCOMM $ 'M-S'))
    endwith
   endif
  endfunc

  func oTDVOCECR_3_27.mHide()
    with this.Parent.oContained
      return (NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))
    endwith
  endfunc


  add object oTD_SEGNO_3_28 as StdCombo with uid="AZHJBWLFLD",rtseq=105,rtrep=.f.,left=431,top=83,width=107,height=21;
    , ToolTipText = "Segno dare/avere per analitica";
    , HelpContextID = 165752187;
    , cFormVar="w_TD_SEGNO",RowSource=""+"Dare,"+"Avere", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oTD_SEGNO_3_28.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTD_SEGNO_3_28.GetRadio()
    this.Parent.oContained.w_TD_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oTD_SEGNO_3_28.SetRadio()
    this.Parent.oContained.w_TD_SEGNO=trim(this.Parent.oContained.w_TD_SEGNO)
    this.value = ;
      iif(this.Parent.oContained.w_TD_SEGNO=='D',1,;
      iif(this.Parent.oContained.w_TD_SEGNO=='A',2,;
      0))
  endfunc

  func oTD_SEGNO_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_TDFLANAL='S')
    endwith
   endif
  endfunc

  func oTD_SEGNO_3_28.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc


  add object oTDASSCES_3_29 as StdCombo with uid="AUPTBSJRNS",rtseq=106,rtrep=.f.,left=147,top=135,width=138,height=21;
    , ToolTipText = "Tipo di associazione cespiti legata alla riga del documento";
    , HelpContextID = 50131593;
    , cFormVar="w_TDASSCES",RowSource=""+"Movimenti cespiti,"+"Cespiti,"+"Non gestita", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDASSCES_3_29.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDASSCES_3_29.GetRadio()
    this.Parent.oContained.w_TDASSCES = this.RadioValue()
    return .t.
  endfunc

  func oTDASSCES_3_29.SetRadio()
    this.Parent.oContained.w_TDASSCES=trim(this.Parent.oContained.w_TDASSCES)
    this.value = ;
      iif(this.Parent.oContained.w_TDASSCES=='M',1,;
      iif(this.Parent.oContained.w_TDASSCES=='C',2,;
      iif(this.Parent.oContained.w_TDASSCES=='N',3,;
      0)))
  endfunc

  func oTDASSCES_3_29.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oTDCAUCES_3_30 as StdField with uid="HZODBXGJLU",rtseq=107,rtrep=.f.,;
    cFormVar = "w_TDCAUCES", cQueryName = "TDCAUCES",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Causale cespiti associata",;
    HelpContextID = 51057289,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=167, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_TDCAUCES"

  func oTDCAUCES_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDASSCES='M')
    endwith
   endif
  endfunc

  func oTDCAUCES_3_30.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  func oTDCAUCES_3_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCES_3_30.ecpDrop(oSource)
    this.Parent.oContained.link_3_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCES_3_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oTDCAUCES_3_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oTDCAUCES_3_30.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_TDCAUCES
     i_obj.ecpSave()
  endproc


  add object oTDCONASS_3_31 as StdCombo with uid="YAXMQKVVPH",rtseq=108,rtrep=.f.,left=559,top=134,width=165,height=21;
    , ToolTipText = "Tipologia documento per generazione scritture di assestamento";
    , HelpContextID = 11080329;
    , cFormVar="w_TDCONASS",RowSource=""+"Fatture da emettere/ricevere,"+"Storno fat. da emettere/ricevere,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDCONASS_3_31.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTDCONASS_3_31.GetRadio()
    this.Parent.oContained.w_TDCONASS = this.RadioValue()
    return .t.
  endfunc

  func oTDCONASS_3_31.SetRadio()
    this.Parent.oContained.w_TDCONASS=trim(this.Parent.oContained.w_TDCONASS)
    this.value = ;
      iif(this.Parent.oContained.w_TDCONASS=='F',1,;
      iif(this.Parent.oContained.w_TDCONASS=='S',2,;
      iif(this.Parent.oContained.w_TDCONASS=='N',3,;
      0)))
  endfunc

  func oTDCONASS_3_31.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oTDCODSTR_3_32 as StdField with uid="MJUUZHWLFP",rtseq=109,rtrep=.f.,;
    cFormVar = "w_TDCODSTR", cQueryName = "TDCODSTR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 34149000,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=147, Top=221, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_TDCODSTR"

  func oTDCODSTR_3_32.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  func oTDCODSTR_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCODSTR_3_32.ecpDrop(oSource)
    this.Parent.oContained.link_3_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCODSTR_3_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oTDCODSTR_3_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture EDI",'',this.parent.oContained
  endproc
  proc oTDCODSTR_3_32.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_TDCODSTR
     i_obj.ecpSave()
  endproc

  add object oTDFLARCO_3_33 as StdCheck with uid="OSRCEYFDKK",rtseq=110,rtrep=.f.,left=146, top=274, caption="Articoli composti",;
    ToolTipText = "Se attivo: la causale gestisce l'evasione degli articoli composti",;
    HelpContextID = 14041733,;
    cFormVar="w_TDFLARCO", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLARCO_3_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLARCO_3_33.GetRadio()
    this.Parent.oContained.w_TDFLARCO = this.RadioValue()
    return .t.
  endfunc

  func oTDFLARCO_3_33.SetRadio()
    this.Parent.oContained.w_TDFLARCO=trim(this.Parent.oContained.w_TDFLARCO)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLARCO=='S',1,;
      0)
  endfunc

  func oTDFLARCO_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S')
    endwith
   endif
  endfunc

  func oTDFLARCO_3_33.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S')
    endwith
  endfunc


  add object oTDLOTDIF_3_34 as StdCombo with uid="GGEQPZEIUK",rtseq=111,rtrep=.f.,left=431,top=249,width=124,height=21;
    , ToolTipText = "Attribuzione differita lotti/matricole";
    , HelpContextID = 200695172;
    , cFormVar="w_TDLOTDIF",RowSource=""+"Immediata,"+"Alla conferma,"+"Differita", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDLOTDIF_3_34.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTDLOTDIF_3_34.GetRadio()
    this.Parent.oContained.w_TDLOTDIF = this.RadioValue()
    return .t.
  endfunc

  func oTDLOTDIF_3_34.SetRadio()
    this.Parent.oContained.w_TDLOTDIF=trim(this.Parent.oContained.w_TDLOTDIF)
    this.value = ;
      iif(this.Parent.oContained.w_TDLOTDIF=='I',1,;
      iif(this.Parent.oContained.w_TDLOTDIF=='C',2,;
      iif(this.Parent.oContained.w_TDLOTDIF=='D',3,;
      0)))
  endfunc

  func oTDLOTDIF_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S' AND .w_TDFLARCO<>'S')
    endwith
   endif
  endfunc

  func oTDLOTDIF_3_34.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR .w_TDFLARCO='S' OR (empty(NVL(.w_FLCASC,' ')) AND EMPTY(NVL(.w_FLRISE, ' '))))
    endwith
  endfunc


  add object oTDTIPIMB_3_35 as StdCombo with uid="CIQWNWJPHM",rtseq=112,rtrep=.f.,left=431,top=275,width=124,height=21;
    , ToolTipText = "Gestione degli imballi a rendere e/o a perdere sui documenti. Gestione cauzioni consente l'utilizzo di tutte le tipologie di imballo.";
    , HelpContextID = 121363848;
    , cFormVar="w_TDTIPIMB",RowSource=""+"Non gestito,"+"A perdere,"+"A perdere e rendere,"+"Gestione cauzioni", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDTIPIMB_3_35.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'R',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oTDTIPIMB_3_35.GetRadio()
    this.Parent.oContained.w_TDTIPIMB = this.RadioValue()
    return .t.
  endfunc

  func oTDTIPIMB_3_35.SetRadio()
    this.Parent.oContained.w_TDTIPIMB=trim(this.Parent.oContained.w_TDTIPIMB)
    this.value = ;
      iif(this.Parent.oContained.w_TDTIPIMB=='N',1,;
      iif(this.Parent.oContained.w_TDTIPIMB=='P',2,;
      iif(this.Parent.oContained.w_TDTIPIMB=='R',3,;
      iif(this.Parent.oContained.w_TDTIPIMB=='C',4,;
      0))))
  endfunc

  func oTDTIPIMB_3_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S')
    endwith
   endif
  endfunc

  func oTDTIPIMB_3_35.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_TDFLARCO <>'S' Or Empty(.w_FLCASC))
    endwith
  endfunc

  add object oTDCAUPFI_3_36 as StdField with uid="NJMVPCTMDL",rtseq=113,rtrep=.f.,;
    cFormVar = "w_TDCAUPFI", cQueryName = "TDCAUPFI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale documento generato associato alla testata (prodotti finiti, articolo kit, imballi resi)",;
    HelpContextID = 725631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=301, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TDCAUPFI"

  func oTDCAUPFI_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S'  AND .w_TDFLARCO='S')
    endwith
   endif
  endfunc

  func oTDCAUPFI_3_36.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDCAUPFI_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUPFI_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUPFI_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTDCAUPFI_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTDCAUPFI_3_36.mZoomOnZoom
    local i_obj
    i_obj=GSVE_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TDCAUPFI
     i_obj.ecpSave()
  endproc

  add object oTDCAUCOD_3_37 as StdField with uid="ABAGTSOXDH",rtseq=114,rtrep=.f.,;
    cFormVar = "w_TDCAUCOD", cQueryName = "TDCAUCOD",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale documento generato associato alle singole righe (componenti, articoli nel kit, imballi consegnati)",;
    HelpContextID = 217378182,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=329, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_BZC", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TDCAUCOD"

  func oTDCAUCOD_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDFLARCO='S')
    endwith
   endif
  endfunc

  func oTDCAUCOD_3_37.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDCAUCOD_3_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUCOD_3_37.ecpDrop(oSource)
    this.Parent.oContained.link_3_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUCOD_3_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTDCAUCOD_3_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_BZC',"Causali documenti",'GSVE_KAC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTDCAUCOD_3_37.mZoomOnZoom
    local i_obj
    i_obj=GSVE_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TDCAUCOD
     i_obj.ecpSave()
  endproc

  add object oCODI_3_39 as StdField with uid="TFNAODIMUT",rtseq=115,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87872474,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=131, Top=7, InputMask=replicate('X',5)


  add object oTDFLEXPL_3_40 as StdCombo with uid="YSEAQIZVPC",rtseq=116,rtrep=.f.,left=146,top=357,width=405,height=21;
    , ToolTipText = "Metodo di generazione dei documenti di evasione, se da documento di origine o da esplosione distinta base";
    , HelpContextID = 118899330;
    , cFormVar="w_TDFLEXPL",RowSource=""+"Documento di origine o espl.distinta (in alternativa),"+"Solo esplosione distinta,"+"Solo documento origine", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDFLEXPL_3_40.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTDFLEXPL_3_40.GetRadio()
    this.Parent.oContained.w_TDFLEXPL = this.RadioValue()
    return .t.
  endfunc

  func oTDFLEXPL_3_40.SetRadio()
    this.Parent.oContained.w_TDFLEXPL=trim(this.Parent.oContained.w_TDFLEXPL)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLEXPL=='S',1,;
      iif(this.Parent.oContained.w_TDFLEXPL=='N',2,;
      iif(this.Parent.oContained.w_TDFLEXPL=='D',3,;
      0)))
  endfunc

  func oTDFLEXPL_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_EACD='S' AND .w_TDFLARCO='S' And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDFLEXPL_3_40.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oDESC_3_41 as StdField with uid="FRXMCTVTRH",rtseq=117,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88206794,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=195, Top=7, InputMask=replicate('X',35)


  add object oTDVALCOM_3_42 as StdCombo with uid="QGJTJTUXVQ",rtseq=118,rtrep=.f.,left=146,top=383,width=405,height=21;
    , ToolTipText = "Metodo di valorizzazione del prodotto finito in funzione dei componenti associati ";
    , HelpContextID = 226737533;
    , cFormVar="w_TDVALCOM",RowSource=""+"Documento principale,"+"Documento di evasione prodotti finiti,"+"Documento principale e documento di evasione prodotti finiti,"+"Non gestito", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDVALCOM_3_42.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oTDVALCOM_3_42.GetRadio()
    this.Parent.oContained.w_TDVALCOM = this.RadioValue()
    return .t.
  endfunc

  func oTDVALCOM_3_42.SetRadio()
    this.Parent.oContained.w_TDVALCOM=trim(this.Parent.oContained.w_TDVALCOM)
    this.value = ;
      iif(this.Parent.oContained.w_TDVALCOM=='D',1,;
      iif(this.Parent.oContained.w_TDVALCOM=='P',2,;
      iif(this.Parent.oContained.w_TDVALCOM=='E',3,;
      iif(this.Parent.oContained.w_TDVALCOM=='N',4,;
      0))))
  endfunc

  func oTDVALCOM_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_EACD='S' AND .w_TDFLARCO='S' And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDVALCOM_3_42.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDCOSEPL_3_43 as StdCheck with uid="ZWFPWEIXGO",rtseq=119,rtrep=.f.,left=577, top=381, caption="Valorizza da esplosione",;
    ToolTipText = "Se attivo valorizza prodotto finito in base ai componenti effettivamente presenti nella maschera di esplosione componenti",;
    HelpContextID = 83432066,;
    cFormVar="w_TDCOSEPL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDCOSEPL_3_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDCOSEPL_3_43.GetRadio()
    this.Parent.oContained.w_TDCOSEPL = this.RadioValue()
    return .t.
  endfunc

  func oTDCOSEPL_3_43.SetRadio()
    this.Parent.oContained.w_TDCOSEPL=trim(this.Parent.oContained.w_TDCOSEPL)
    this.value = ;
      iif(this.Parent.oContained.w_TDCOSEPL=='S',1,;
      0)
  endfunc

  func oTDCOSEPL_3_43.mHide()
    with this.Parent.oContained
      return ((.w_TDEXPAUT = 'S' and .w_TDFLEXPL='N' ) or .w_TDVALCOM='N' OR g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oTDEXPAUT_3_44 as StdCheck with uid="WWCQCSNKDW",rtseq=120,rtrep=.f.,left=146, top=409, caption="Esplosione automatica",;
    ToolTipText = "Se attivo: genera automaticamente i documenti associati ai p.finito e/o componenti",;
    HelpContextID = 13775498,;
    cFormVar="w_TDEXPAUT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDEXPAUT_3_44.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDEXPAUT_3_44.GetRadio()
    this.Parent.oContained.w_TDEXPAUT = this.RadioValue()
    return .t.
  endfunc

  func oTDEXPAUT_3_44.SetRadio()
    this.Parent.oContained.w_TDEXPAUT=trim(this.Parent.oContained.w_TDEXPAUT)
    this.value = ;
      iif(this.Parent.oContained.w_TDEXPAUT=='S',1,;
      0)
  endfunc

  func oTDEXPAUT_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_EACD='S' AND .w_TDFLARCO='S' AND .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDEXPAUT_3_44.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc


  add object oTDFLCOMM_3_45 as StdCombo with uid="TMQJASEQLU",value=3,rtseq=121,rtrep=.f.,left=146,top=452,width=203,height=22;
    , ToolTipText = "Se attivo: il documento abilita l'input delle commesse relativo alla gestione progetti";
    , HelpContextID = 34192765;
    , cFormVar="w_TDFLCOMM",RowSource=""+"Si (sempre obbligatoria),"+"Si (obbligatoriet� da cau. mag.),"+"No (non gestita)", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTDFLCOMM_3_45.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTDFLCOMM_3_45.GetRadio()
    this.Parent.oContained.w_TDFLCOMM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCOMM_3_45.SetRadio()
    this.Parent.oContained.w_TDFLCOMM=trim(this.Parent.oContained.w_TDFLCOMM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCOMM=='S',1,;
      iif(this.Parent.oContained.w_TDFLCOMM=='M',2,;
      iif(this.Parent.oContained.w_TDFLCOMM=='',3,;
      0)))
  endfunc

  func oTDFLCOMM_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oTDFLCOMM_3_45.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oTDFLCASH_3_46 as StdCheck with uid="HCIUXGGQMV",rtseq=122,rtrep=.f.,left=368, top=452, caption="Cash flow commessa",;
    ToolTipText = "Se attivo: il documento concorre alla elaborazione del cash flow di commessa",;
    HelpContextID = 267797118,;
    cFormVar="w_TDFLCASH", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLCASH_3_46.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLCASH_3_46.GetRadio()
    this.Parent.oContained.w_TDFLCASH = this.RadioValue()
    return .t.
  endfunc

  func oTDFLCASH_3_46.SetRadio()
    this.Parent.oContained.w_TDFLCASH=trim(this.Parent.oContained.w_TDFLCASH)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLCASH=='S',1,;
      0)
  endfunc

  func oTDFLCASH_3_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')
    endwith
   endif
  endfunc

  func oTDFLCASH_3_46.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oTDFLNORC_3_47 as StdCheck with uid="YCRMLZIYWL",rtseq=123,rtrep=.f.,left=577, top=452, caption="No ricalcolo commessa",;
    ToolTipText = "Se attivo in fase di import documenti per le righe con commessa vuota non verr� valorizzata con quella specificata nella riga precedente",;
    HelpContextID = 245777017,;
    cFormVar="w_TDFLNORC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTDFLNORC_3_47.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLNORC_3_47.GetRadio()
    this.Parent.oContained.w_TDFLNORC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNORC_3_47.SetRadio()
    this.Parent.oContained.w_TDFLNORC=trim(this.Parent.oContained.w_TDFLNORC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNORC=='S',1,;
      0)
  endfunc

  add object oTDMAXLEV_3_48 as StdField with uid="AZJRJYNUPG",rtseq=124,rtrep=.f.,;
    cFormVar = "w_TDMAXLEV", cQueryName = "TDMAXLEV",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, numero massimo livelli di esplosione non specificato",;
    ToolTipText = "Numero massimo livelli di esplosione distinta base",;
    HelpContextID = 205238924,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=524, Top=409, cSayPict="'99'", cGetPict="'99'"

  func oTDMAXLEV_3_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TDCAUCOD) AND g_DISB='S' AND .w_TDFLARCO='S' AND .w_TDFLEXPL<>'D' And .w_TDTIPIMB='N')
    endwith
   endif
  endfunc

  func oTDMAXLEV_3_48.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  func oTDMAXLEV_3_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TDMAXLEV>0)
    endwith
    return bRes
  endfunc

  add object oDESSTRU_3_57 as StdField with uid="DIFWYFRGLM",rtseq=162,rtrep=.f.,;
    cFormVar = "w_DESSTRU", cQueryName = "DESSTRU",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura EDI",;
    HelpContextID = 34476598,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=244, Top=221, InputMask=replicate('X',30)

  add object oTDTIPDFE_3_61 as StdField with uid="JFTGSMLRCV",rtseq=193,rtrep=.f.,;
    cFormVar = "w_TDTIPDFE", cQueryName = "TDTIPDFE",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia documento per fatturazione elettronica",;
    HelpContextID = 63185531,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=146, Top=506, InputMask=replicate('X',4)

  func oTDTIPDFE_3_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oTDDESEST_3_62 as StdField with uid="JLPPLMXXMZ",rtseq=194,rtrep=.f.,;
    cFormVar = "w_TDDESEST", cQueryName = "TDDESEST",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione esterna relativa alla tipologia documento per fatturazione elettronica",;
    HelpContextID = 82780810,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=431, Top=506, InputMask=replicate('X',35)

  func oTDDESEST_3_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oTDCODCLA_3_63 as StdField with uid="JRFZLXFZCR",rtseq=195,rtrep=.f.,;
    cFormVar = "w_TDCODCLA", cQueryName = "TDCODCLA",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 234286473,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=146, Top=532, InputMask=replicate('X',5)

  func oTDCODCLA_3_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC='FA' OR .w_TDCATDOC='NC')
    endwith
   endif
  endfunc

  add object oStr_3_4 as StdString with uid="NPDUQUYLYJ",Visible=.t., Left=31, Top=221,;
    Alignment=1, Width=114, Height=18,;
    Caption="Struttura EDI:"  ;
  , bGlobalFont=.t.

  func oStr_3_4.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_3_5 as StdString with uid="WVPMVKPYEO",Visible=.t., Left=301, Top=275,;
    Alignment=1, Width=128, Height=18,;
    Caption="Tipologia imballi:"  ;
  , bGlobalFont=.t.

  func oStr_3_5.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_TDFLARCO <>'S' Or Empty(.w_FLCASC))
    endwith
  endfunc

  add object oStr_3_6 as StdString with uid="SUXYWALZKA",Visible=.t., Left=272, Top=249,;
    Alignment=1, Width=157, Height=18,;
    Caption="Imputazione lotti/matricole:"  ;
  , bGlobalFont=.t.

  func oStr_3_6.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR .w_TDFLARCO='S' OR (empty(NVL(.w_FLCASC,' ')) AND EMPTY(NVL(.w_FLRISE, ' '))))
    endwith
  endfunc

  add object oStr_3_7 as StdString with uid="FMKVGKYAZB",Visible=.t., Left=470, Top=137,;
    Alignment=1, Width=85, Height=18,;
    Caption="Contabilizza:"  ;
  , bGlobalFont=.t.

  func oStr_3_7.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oStr_3_8 as StdString with uid="HXSVBAPVKE",Visible=.t., Left=572, Top=110,;
    Alignment=0, Width=159, Height=18,;
    Caption="Scritture di assestamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_8.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oStr_3_9 as StdString with uid="LMDQPTIWZX",Visible=.t., Left=374, Top=410,;
    Alignment=1, Width=148, Height=18,;
    Caption="Livello max di esplosione:"  ;
  , bGlobalFont=.t.

  func oStr_3_9.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_10 as StdString with uid="GXIHGBSOSN",Visible=.t., Left=3, Top=382,;
    Alignment=1, Width=141, Height=18,;
    Caption="Valorizza componenti:"  ;
  , bGlobalFont=.t.

  func oStr_3_10.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_11 as StdString with uid="YPPXSYPMKJ",Visible=.t., Left=16, Top=428,;
    Alignment=0, Width=141, Height=19,;
    Caption="Gestione progetti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_11.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_3_12 as StdString with uid="SAYUCPTWZQ",Visible=.t., Left=16, Top=110,;
    Alignment=0, Width=190, Height=18,;
    Caption="Gestione cespiti ammortizzabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_12.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_13 as StdString with uid="DNGYJZDSJC",Visible=.t., Left=3, Top=164,;
    Alignment=1, Width=141, Height=18,;
    Caption="Causale cespite:"  ;
  , bGlobalFont=.t.

  func oStr_3_13.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_15 as StdString with uid="LRVOLPSNRI",Visible=.t., Left=16, Top=33,;
    Alignment=0, Width=124, Height=18,;
    Caption="Contabilit� analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_15.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' OR g_COMM='S'))
    endwith
  endfunc

  add object oStr_3_16 as StdString with uid="CAUMIQITUP",Visible=.t., Left=16, Top=191,;
    Alignment=0, Width=280, Height=19,;
    Caption="Magazzino produzione - Vendite funzioni avanzate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S')
    endwith
  endfunc

  add object oStr_3_17 as StdString with uid="AXDNERUTJM",Visible=.t., Left=3, Top=301,;
    Alignment=1, Width=141, Height=18,;
    Caption="Documento di testata:"  ;
  , bGlobalFont=.t.

  func oStr_3_17.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_18 as StdString with uid="UVFFVFRBGT",Visible=.t., Left=3, Top=329,;
    Alignment=1, Width=141, Height=18,;
    Caption="Documento di riga:"  ;
  , bGlobalFont=.t.

  func oStr_3_18.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_21 as StdString with uid="XCACPQSEZR",Visible=.t., Left=3, Top=356,;
    Alignment=1, Width=141, Height=18,;
    Caption="Tipo evasione:"  ;
  , bGlobalFont=.t.

  func oStr_3_21.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' OR .w_TDFLARCO<>'S')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="DAWCHWTZTM",Visible=.t., Left=360, Top=56,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tipo voce:"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (NOT ((g_PERCCR='S' AND .w_TDFLANAL='S') OR  (g_COMM='S' AND .w_TDFLCOMM $ 'M-S')))
    endwith
  endfunc

  add object oStr_3_23 as StdString with uid="KJGEGFHYRZ",Visible=.t., Left=360, Top=84,;
    Alignment=1, Width=69, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_3_23.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S' OR .w_TDFLANAL<>'S')
    endwith
  endfunc

  add object oStr_3_24 as StdString with uid="NNFXRSGGQW",Visible=.t., Left=3, Top=134,;
    Alignment=1, Width=141, Height=18,;
    Caption="Associazione cespite:"  ;
  , bGlobalFont=.t.

  func oStr_3_24.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S')
    endwith
  endfunc

  add object oStr_3_38 as StdString with uid="FNSEGCKRWP",Visible=.t., Left=12, Top=7,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_59 as StdString with uid="ECONLEUPKX",Visible=.t., Left=38, Top=454,;
    Alignment=1, Width=106, Height=18,;
    Caption="Gestione progetti:"  ;
  , bGlobalFont=.t.

  func oStr_3_59.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC<>'DT')
    endwith
  endfunc

  add object oStr_3_60 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=16, Top=482,;
    Alignment=0, Width=257, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_64 as StdString with uid="ALGPCPZQRH",Visible=.t., Left=45, Top=506,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_65 as StdString with uid="ZUYFDBCYJI",Visible=.t., Left=308, Top=506,;
    Alignment=1, Width=121, Height=18,;
    Caption="Descrizione esterna:"  ;
  , bGlobalFont=.t.

  add object oStr_3_66 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=31, Top=532,;
    Alignment=1, Width=113, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_3_1 as StdBox with uid="YWAYZGVPXX",left=4, top=210, width=727,height=2

  add object oBox_3_2 as StdBox with uid="DQATRRMWVH",left=3, top=126, width=727,height=2

  add object oBox_3_3 as StdBox with uid="CJDTNRGGDX",left=2, top=51, width=730,height=1

  add object oBox_3_58 as StdBox with uid="WOEXISLZJK",left=9, top=446, width=727,height=2

  add object oBox_3_67 as StdBox with uid="EOSUPFRRUG",left=9, top=500, width=727,height=1
enddefine
define class tgsac_atdPag4 as StdContainer
  Width  = 740
  height = 554
  stdWidth  = 740
  stdheight = 554
  resizeYpos=205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_2 as StdField with uid="TTJYFJGKZV",rtseq=133,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87872474,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=129, Top=19, InputMask=replicate('X',5)

  add object oTDDESRIF_4_3 as StdField with uid="HNOBTEMZSF",rtseq=134,rtrep=.f.,;
    cFormVar = "w_TDDESRIF", cQueryName = "TDDESRIF",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 235986308,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=129, Top=46, InputMask=replicate('X',18)

  add object oTDMODRIF_4_4 as StdField with uid="YBPOCHOBPT",rtseq=135,rtrep=.f.,;
    cFormVar = "w_TDMODRIF", cQueryName = "TDMODRIF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello dei riferimenti in lingua dei documenti di destinazione di questa causale",;
    HelpContextID = 251022724,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=129, Top=73, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MODMRIFE", cZoomOnZoom="GSAR_MRR", oKey_1_1="MDCODICE", oKey_1_2="this.w_TDMODRIF"

  func oTDMODRIF_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDMODRIF_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDMODRIF_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODMRIFE','*','MDCODICE',cp_AbsName(this.parent,'oTDMODRIF_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MRR',"Modelli riferimenti",'',this.parent.oContained
  endproc
  proc oTDMODRIF_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MRR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_TDMODRIF
     i_obj.ecpSave()
  endproc

  add object oDESC_4_5 as StdField with uid="TWMLMNTAWI",rtseq=136,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88206794,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=193, Top=19, InputMask=replicate('X',35)


  add object oLinkPC_4_7 as stdDynamicChildContainer with uid="BYZKFFFDZE",left=14, top=114, width=700, height=172, bOnScreen=.t.;


  add object oTDFLNSRI_4_8 as StdCheck with uid="XXRUOSIWXL",rtseq=137,rtrep=.f.,left=14, top=311, caption="Nostro riferimento",;
    ToolTipText = "Se attivo: durante l'import genera la riga riferimento del documento di origine",;
    HelpContextID = 44450431,;
    cFormVar="w_TDFLNSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLNSRI_4_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLNSRI_4_8.GetRadio()
    this.Parent.oContained.w_TDFLNSRI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNSRI_4_8.SetRadio()
    this.Parent.oContained.w_TDFLNSRI=trim(this.Parent.oContained.w_TDFLNSRI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNSRI=='S',1,;
      0)
  endfunc

  add object oTDTPNDOC_4_9 as StdField with uid="TDTOVJVHMM",rtseq=138,rtrep=.f.,;
    cFormVar = "w_TDTPNDOC", cQueryName = "TDTPNDOC",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga nostro riferimento",;
    HelpContextID = 206888327,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=311, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPNDOC"

  func oTDTPNDOC_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLNSRI='S')
    endwith
   endif
  endfunc

  func oTDTPNDOC_4_9.mHide()
    with this.Parent.oContained
      return (.w_TDFLNSRI<>'S')
    endwith
  endfunc

  func oTDTPNDOC_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPNDOC_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPNDOC_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPNDOC_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPNDOC_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPNDOC
     i_obj.ecpSave()
  endproc

  add object oTDFLVSRI_4_10 as StdCheck with uid="KXUNSESXMA",rtseq=139,rtrep=.f.,left=14, top=334, caption="Vostro riferimento",;
    ToolTipText = "Se attivo: durante l'import genera la riga vostro riferimento del documento di origine",;
    HelpContextID = 52839039,;
    cFormVar="w_TDFLVSRI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLVSRI_4_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLVSRI_4_10.GetRadio()
    this.Parent.oContained.w_TDFLVSRI = this.RadioValue()
    return .t.
  endfunc

  func oTDFLVSRI_4_10.SetRadio()
    this.Parent.oContained.w_TDFLVSRI=trim(this.Parent.oContained.w_TDFLVSRI)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLVSRI=='S',1,;
      0)
  endfunc

  add object oTDTPVDOC_4_11 as StdField with uid="XWEIBZYMMR",rtseq=140,rtrep=.f.,;
    cFormVar = "w_TDTPVDOC", cQueryName = "TDTPVDOC",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga vostro riferimento",;
    HelpContextID = 198499719,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=334, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPVDOC"

  func oTDTPVDOC_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLVSRI='S')
    endwith
   endif
  endfunc

  func oTDTPVDOC_4_11.mHide()
    with this.Parent.oContained
      return (.w_TDFLVSRI<>'S')
    endwith
  endfunc

  func oTDTPVDOC_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPVDOC_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPVDOC_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPVDOC_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPVDOC_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPVDOC
     i_obj.ecpSave()
  endproc

  add object oTDFLRIDE_4_12 as StdCheck with uid="GSNENOGBJF",rtseq=141,rtrep=.f.,left=14, top=357, caption="Rif. descrittivo",;
    ToolTipText = "Se attivo: durante l'import genera la riga rif. descrittivo",;
    HelpContextID = 149308027,;
    cFormVar="w_TDFLRIDE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLRIDE_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLRIDE_4_12.GetRadio()
    this.Parent.oContained.w_TDFLRIDE = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRIDE_4_12.SetRadio()
    this.Parent.oContained.w_TDFLRIDE=trim(this.Parent.oContained.w_TDFLRIDE)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRIDE=='S',1,;
      0)
  endfunc

  add object oTDTPRDES_4_13 as StdField with uid="CCBHYIYPEC",rtseq=142,rtrep=.f.,;
    cFormVar = "w_TDTPRDES", cQueryName = "TDTPRDES",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia assegnata alla riga rif. desc. ordini",;
    HelpContextID = 65741449,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=357, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDTPRDES"

  func oTDTPRDES_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLRIDE='S')
    endwith
   endif
  endfunc

  func oTDTPRDES_4_13.mHide()
    with this.Parent.oContained
      return (.w_TDFLRIDE<>'S')
    endwith
  endfunc

  func oTDTPRDES_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDTPRDES_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDTPRDES_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDTPRDES_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDTPRDES_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDTPRDES
     i_obj.ecpSave()
  endproc

  add object oTDESCCL1_4_17 as StdField with uid="YRWYUXPMDR",rtseq=143,rtrep=.f.,;
    cFormVar = "w_TDESCCL1", cQueryName = "TDESCCL1",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati dalla causale",;
    HelpContextID = 235064729,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=508, Top=313, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL1"

  func oTDESCCL1_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL1_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL1_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL1_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL1_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL1
     i_obj.ecpSave()
  endproc

  add object oTDESCCL2_4_18 as StdField with uid="OIXQSORJBD",rtseq=144,rtrep=.f.,;
    cFormVar = "w_TDESCCL2", cQueryName = "TDESCCL2",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati dalla causale",;
    HelpContextID = 235064728,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=553, Top=313, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL2"

  func oTDESCCL2_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL2_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL2_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL2_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL2_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL2
     i_obj.ecpSave()
  endproc

  add object oTDESCCL3_4_19 as StdField with uid="TDDRKSBZPX",rtseq=145,rtrep=.f.,;
    cFormVar = "w_TDESCCL3", cQueryName = "TDESCCL3",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati dalla causale",;
    HelpContextID = 235064727,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=598, Top=313, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL3"

  func oTDESCCL3_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL3_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL3_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL3_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL3_4_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL3
     i_obj.ecpSave()
  endproc

  add object oTDESCCL4_4_20 as StdField with uid="PFTSPHWWAI",rtseq=146,rtrep=.f.,;
    cFormVar = "w_TDESCCL4", cQueryName = "TDESCCL4",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati dalla causale",;
    HelpContextID = 235064726,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=643, Top=313, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL4"

  func oTDESCCL4_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL4_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL4_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL4_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL4_4_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL4
     i_obj.ecpSave()
  endproc

  add object oTDESCCL5_4_21 as StdField with uid="ZBXGRCEKPC",rtseq=147,rtrep=.f.,;
    cFormVar = "w_TDESCCL5", cQueryName = "TDESCCL5",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non importare sui documenti generati dalla causale",;
    HelpContextID = 235064725,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=688, Top=313, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDESCCL5"

  func oTDESCCL5_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDESCCL5_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDESCCL5_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDESCCL5_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDESCCL5_4_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDESCCL5
     i_obj.ecpSave()
  endproc

  add object oTDSTACL1_4_22 as StdField with uid="KDWKYAOGCD",rtseq=148,rtrep=.f.,;
    cFormVar = "w_TDSTACL1", cQueryName = "TDSTACL1",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati dalla causale",;
    HelpContextID = 237039001,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=508, Top=339, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL1"

  func oTDSTACL1_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL1_4_22.ecpDrop(oSource)
    this.Parent.oContained.link_4_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL1_4_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL1_4_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL1_4_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL1
     i_obj.ecpSave()
  endproc

  add object oTDSTACL2_4_23 as StdField with uid="LCQZAHLAKB",rtseq=149,rtrep=.f.,;
    cFormVar = "w_TDSTACL2", cQueryName = "TDSTACL2",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati dalla causale",;
    HelpContextID = 237039000,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=553, Top=339, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL2"

  func oTDSTACL2_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL2_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL2_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL2_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL2_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL2
     i_obj.ecpSave()
  endproc

  add object oTDSTACL3_4_24 as StdField with uid="ULBMSODDKY",rtseq=150,rtrep=.f.,;
    cFormVar = "w_TDSTACL3", cQueryName = "TDSTACL3",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati dalla causale",;
    HelpContextID = 237038999,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=598, Top=339, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL3"

  func oTDSTACL3_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL3_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL3_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL3_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL3_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL3
     i_obj.ecpSave()
  endproc

  add object oTDSTACL4_4_25 as StdField with uid="PZWLFDLNRC",rtseq=151,rtrep=.f.,;
    cFormVar = "w_TDSTACL4", cQueryName = "TDSTACL4",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati dalla causale",;
    HelpContextID = 237038998,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=643, Top=339, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL4"

  func oTDSTACL4_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL4_4_25.ecpDrop(oSource)
    this.Parent.oContained.link_4_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL4_4_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL4_4_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL4_4_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL4
     i_obj.ecpSave()
  endproc

  add object oTDSTACL5_4_26 as StdField with uid="PVMNNMGPUP",rtseq=152,rtrep=.f.,;
    cFormVar = "w_TDSTACL5", cQueryName = "TDSTACL5",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo riga da non stampare sui documenti generati dalla causale",;
    HelpContextID = 237038997,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=688, Top=339, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_TDSTACL5"

  func oTDSTACL5_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDSTACL5_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDSTACL5_4_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oTDSTACL5_4_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oTDSTACL5_4_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_TDSTACL5
     i_obj.ecpSave()
  endproc

  add object oDESMOD_4_35 as StdField with uid="HRKBUYDRNQ",rtseq=153,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 62394934,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=204, Top=73, InputMask=replicate('X',35)

  add object oTDFLIMPA_4_36 as StdCheck with uid="MALVBTXVXQ",rtseq=154,rtrep=.f.,left=14, top=403, caption="Controllo dati pagamento",;
    ToolTipText = "Se attivo, effettua il controllo di congruenza dei dati di pagamento (in import documenti)",;
    HelpContextID = 206979703,;
    cFormVar="w_TDFLIMPA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIMPA_4_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLIMPA_4_36.GetRadio()
    this.Parent.oContained.w_TDFLIMPA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIMPA_4_36.SetRadio()
    this.Parent.oContained.w_TDFLIMPA=trim(this.Parent.oContained.w_TDFLIMPA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIMPA=='S',1,;
      0)
  endfunc

  func oTDFLIMPA_4_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLIMPA_4_36.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTDFLIMAC_4_37 as StdCheck with uid="QHZCBHDHNX",rtseq=155,rtrep=.f.,left=14, top=424, caption="Controllo dati accompagnatori",;
    ToolTipText = "Se attivo, effettua il controllo di congruenza dei dati accompagnatori (in import documenti)",;
    HelpContextID = 206979705,;
    cFormVar="w_TDFLIMAC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIMAC_4_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLIMAC_4_37.GetRadio()
    this.Parent.oContained.w_TDFLIMAC = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIMAC_4_37.SetRadio()
    this.Parent.oContained.w_TDFLIMAC=trim(this.Parent.oContained.w_TDFLIMAC)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIMAC=='S',1,;
      0)
  endfunc

  func oTDFLIMAC_4_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDCATDOC<>'RF')
    endwith
   endif
  endfunc

  func oTDFLIMAC_4_37.mHide()
    with this.Parent.oContained
      return (.w_TDCATDOC='RF')
    endwith
  endfunc

  add object oTDFLIA01_4_48 as StdCheck with uid="ZGQSFXDDMD",rtseq=175,rtrep=.t.,left=571, top=400, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653095,;
    cFormVar="w_TDFLIA01", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA01_4_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA01_4_48.GetRadio()
    this.Parent.oContained.w_TDFLIA01 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA01_4_48.SetRadio()
    this.Parent.oContained.w_TDFLIA01=trim(this.Parent.oContained.w_TDFLIA01)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA01=='S',1,;
      0)
  endfunc

  add object oTDFLIA02_4_49 as StdCheck with uid="RYRSHVGIYH",rtseq=176,rtrep=.t.,left=571, top=425, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653096,;
    cFormVar="w_TDFLIA02", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA02_4_49.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA02_4_49.GetRadio()
    this.Parent.oContained.w_TDFLIA02 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA02_4_49.SetRadio()
    this.Parent.oContained.w_TDFLIA02=trim(this.Parent.oContained.w_TDFLIA02)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA02=='S',1,;
      0)
  endfunc

  add object oTDFLIA03_4_50 as StdCheck with uid="CFAQBEEMTJ",rtseq=177,rtrep=.t.,left=571, top=448, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653097,;
    cFormVar="w_TDFLIA03", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA03_4_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA03_4_50.GetRadio()
    this.Parent.oContained.w_TDFLIA03 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA03_4_50.SetRadio()
    this.Parent.oContained.w_TDFLIA03=trim(this.Parent.oContained.w_TDFLIA03)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA03=='S',1,;
      0)
  endfunc

  add object oTDFLIA04_4_51 as StdCheck with uid="QPBXOWCELL",rtseq=178,rtrep=.t.,left=571, top=471, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653098,;
    cFormVar="w_TDFLIA04", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA04_4_51.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA04_4_51.GetRadio()
    this.Parent.oContained.w_TDFLIA04 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA04_4_51.SetRadio()
    this.Parent.oContained.w_TDFLIA04=trim(this.Parent.oContained.w_TDFLIA04)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA04=='S',1,;
      0)
  endfunc

  add object oTDFLIA05_4_52 as StdCheck with uid="JUACTYYBAI",rtseq=179,rtrep=.t.,left=571, top=494, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653099,;
    cFormVar="w_TDFLIA05", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA05_4_52.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA05_4_52.GetRadio()
    this.Parent.oContained.w_TDFLIA05 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA05_4_52.SetRadio()
    this.Parent.oContained.w_TDFLIA05=trim(this.Parent.oContained.w_TDFLIA05)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA05=='S',1,;
      0)
  endfunc

  add object oTDFLIA06_4_53 as StdCheck with uid="UFQFMTVLPF",rtseq=180,rtrep=.t.,left=571, top=517, caption=" ",;
    ToolTipText = "Se attivo: il campo aggiuntivo sar� importato",;
    HelpContextID = 5653100,;
    cFormVar="w_TDFLIA06", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oTDFLIA06_4_53.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDFLIA06_4_53.GetRadio()
    this.Parent.oContained.w_TDFLIA06 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLIA06_4_53.SetRadio()
    this.Parent.oContained.w_TDFLIA06=trim(this.Parent.oContained.w_TDFLIA06)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLIA06=='S',1,;
      0)
  endfunc


  add object oTDFLRA01_4_55 as StdCombo with uid="LTJVJLANZZ",rtseq=181,rtrep=.t.,left=622,top=402,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090279;
    , cFormVar="w_TDFLRA01",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA01_4_55.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA01_4_55.GetRadio()
    this.Parent.oContained.w_TDFLRA01 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA01_4_55.SetRadio()
    this.Parent.oContained.w_TDFLRA01=trim(this.Parent.oContained.w_TDFLRA01)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA01=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA01=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA01=='A',3,;
      0)))
  endfunc

  func oTDFLRA01_4_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA01='S')
    endwith
   endif
  endfunc


  add object CAMAGG01 as cp_calclbl with uid="JZCKXLHDHV",left=365, top=401, width=202,height=20,;
    caption='Campo aggiuntivo 1',;
   bGlobalFont=.t.,;
    caption="Campo 1",Alignment =1,;
    nPag=4;
    , HelpContextID = 225263252


  add object CAMAGG02 as cp_calclbl with uid="YBFIFIXNYF",left=365, top=426, width=202,height=20,;
    caption='Campo aggiuntivo 2',;
   bGlobalFont=.t.,;
    caption="Campo 2",Alignment =1,;
    nPag=4;
    , HelpContextID = 225262996


  add object CAMAGG03 as cp_calclbl with uid="EELMGPXNWQ",left=365, top=449, width=202,height=20,;
    caption='Campo aggiuntivo 3',;
   bGlobalFont=.t.,;
    caption="Campo 3",alignment =1,;
    nPag=4;
    , HelpContextID = 225262740


  add object CAMAGG04 as cp_calclbl with uid="WWUGRPNAOG",left=365, top=472, width=202,height=20,;
    caption='Campo aggiuntivo 4',;
   bGlobalFont=.t.,;
    caption="Campo 4",alignment =1,;
    nPag=4;
    , HelpContextID = 225262484


  add object CAMAGG05 as cp_calclbl with uid="BLEZPMMBEJ",left=365, top=495, width=202,height=20,;
    caption='Campo aggiuntivo 5',;
   bGlobalFont=.t.,;
    caption="Campo 5",alignment =1,;
    nPag=4;
    , HelpContextID = 225262228


  add object CAMAGG06 as cp_calclbl with uid="ORVYGKKIKW",left=365, top=518, width=202,height=20,;
    caption='Campo aggiuntivo 6',;
   bGlobalFont=.t.,;
    caption="Campo 6",alignment =1,;
    nPag=4;
    , HelpContextID = 225261972


  add object oTDFLRA02_4_65 as StdCombo with uid="FIIEIQYREQ",rtseq=182,rtrep=.t.,left=622,top=425,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090280;
    , cFormVar="w_TDFLRA02",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA02_4_65.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA02_4_65.GetRadio()
    this.Parent.oContained.w_TDFLRA02 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA02_4_65.SetRadio()
    this.Parent.oContained.w_TDFLRA02=trim(this.Parent.oContained.w_TDFLRA02)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA02=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA02=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA02=='A',3,;
      0)))
  endfunc

  func oTDFLRA02_4_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA02='S')
    endwith
   endif
  endfunc


  add object oTDFLRA03_4_66 as StdCombo with uid="HSGFALZDKG",rtseq=183,rtrep=.t.,left=622,top=448,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090281;
    , cFormVar="w_TDFLRA03",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA03_4_66.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA03_4_66.GetRadio()
    this.Parent.oContained.w_TDFLRA03 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA03_4_66.SetRadio()
    this.Parent.oContained.w_TDFLRA03=trim(this.Parent.oContained.w_TDFLRA03)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA03=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA03=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA03=='A',3,;
      0)))
  endfunc

  func oTDFLRA03_4_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA03='S')
    endwith
   endif
  endfunc


  add object oTDFLRA04_4_67 as StdCombo with uid="CHVURPHYST",rtseq=184,rtrep=.t.,left=622,top=471,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090282;
    , cFormVar="w_TDFLRA04",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA04_4_67.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA04_4_67.GetRadio()
    this.Parent.oContained.w_TDFLRA04 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA04_4_67.SetRadio()
    this.Parent.oContained.w_TDFLRA04=trim(this.Parent.oContained.w_TDFLRA04)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA04=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA04=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA04=='A',3,;
      0)))
  endfunc

  func oTDFLRA04_4_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA04='S')
    endwith
   endif
  endfunc


  add object oTDFLRA05_4_68 as StdCombo with uid="YUYZHJIIIJ",rtseq=185,rtrep=.t.,left=622,top=494,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090283;
    , cFormVar="w_TDFLRA05",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA05_4_68.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA05_4_68.GetRadio()
    this.Parent.oContained.w_TDFLRA05 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA05_4_68.SetRadio()
    this.Parent.oContained.w_TDFLRA05=trim(this.Parent.oContained.w_TDFLRA05)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA05=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA05=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA05=='A',3,;
      0)))
  endfunc

  func oTDFLRA05_4_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA05='S')
    endwith
   endif
  endfunc


  add object oTDFLRA06_4_69 as StdCombo with uid="HFTHUADXUI",rtseq=186,rtrep=.t.,left=622,top=517,width=118,height=21;
    , ToolTipText = "Imposta la modalit� di rottura dei documenti (Nessuna rottura, su dati differenti, anche in presenza di valori vuoti nel campo)";
    , HelpContextID = 15090284;
    , cFormVar="w_TDFLRA06",RowSource=""+"No,"+"Si,"+"Tassativa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTDFLRA06_4_69.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTDFLRA06_4_69.GetRadio()
    this.Parent.oContained.w_TDFLRA06 = this.RadioValue()
    return .t.
  endfunc

  func oTDFLRA06_4_69.SetRadio()
    this.Parent.oContained.w_TDFLRA06=trim(this.Parent.oContained.w_TDFLRA06)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLRA06=='N',1,;
      iif(this.Parent.oContained.w_TDFLRA06=='S',2,;
      iif(this.Parent.oContained.w_TDFLRA06=='A',3,;
      0)))
  endfunc

  func oTDFLRA06_4_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TDFLIA06='S')
    endwith
   endif
  endfunc

  add object oStr_4_1 as StdString with uid="CSITQOXQUW",Visible=.t., Left=10, Top=19,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_6 as StdString with uid="VRWDDEAQLK",Visible=.t., Left=14, Top=95,;
    Alignment=0, Width=556, Height=15,;
    Caption="Documenti di origine"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="CTCLNLIUKJ",Visible=.t., Left=419, Top=289,;
    Alignment=0, Width=305, Height=15,;
    Caption="Filtra tipologie righe documenti"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="YDTHVQNBFV",Visible=.t., Left=414, Top=339,;
    Alignment=1, Width=91, Height=15,;
    Caption="Non stampare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="YSZOOZQXFQ",Visible=.t., Left=414, Top=313,;
    Alignment=1, Width=91, Height=15,;
    Caption="Non importare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="SCVRGDDOCH",Visible=.t., Left=144, Top=311,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_27.mHide()
    with this.Parent.oContained
      return (.w_TDFLNSRI<>'S')
    endwith
  endfunc

  add object oStr_4_28 as StdString with uid="EDQQCYDXLC",Visible=.t., Left=144, Top=334,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_28.mHide()
    with this.Parent.oContained
      return (.w_TDFLVSRI<>'S')
    endwith
  endfunc

  add object oStr_4_29 as StdString with uid="QFGZXOCPZK",Visible=.t., Left=16, Top=289,;
    Alignment=0, Width=277, Height=15,;
    Caption="Genera righe riferimenti"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="PEEYOTWPAN",Visible=.t., Left=144, Top=357,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  func oStr_4_32.mHide()
    with this.Parent.oContained
      return (.w_TDFLRIDE<>'S')
    endwith
  endfunc

  add object oStr_4_33 as StdString with uid="XHVNPIDBLO",Visible=.t., Left=7, Top=46,;
    Alignment=1, Width=119, Height=18,;
    Caption="Descrizione vs.rif.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="FBAYGPXGRM",Visible=.t., Left=15, Top=73,;
    Alignment=1, Width=111, Height=18,;
    Caption="Modello riferimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="MQITMZPWRN",Visible=.t., Left=16, Top=378,;
    Alignment=0, Width=277, Height=18,;
    Caption="Controlli in fase di import"  ;
  , bGlobalFont=.t.

  add object oStr_4_56 as StdString with uid="JULKPNDXUZ",Visible=.t., Left=361, Top=378,;
    Alignment=0, Width=201, Height=18,;
    Caption="Campi aggiuntivi"  ;
  , bGlobalFont=.t.

  add object oStr_4_57 as StdString with uid="GNGJNJUWXN",Visible=.t., Left=566, Top=378,;
    Alignment=0, Width=56, Height=18,;
    Caption="Importa"  ;
  , bGlobalFont=.t.

  add object oStr_4_64 as StdString with uid="PPOFDIRCFV",Visible=.t., Left=622, Top=378,;
    Alignment=2, Width=118, Height=18,;
    Caption="Rottura"  ;
  , bGlobalFont=.t.

  add object oBox_4_30 as StdBox with uid="IBTMLGVNUO",left=355, top=288, width=2,height=88

  add object oBox_4_31 as StdBox with uid="OQARUZHWDE",left=13, top=307, width=716,height=1

  add object oBox_4_39 as StdBox with uid="FJOCQIAVPQ",left=13, top=397, width=716,height=1

  add object oBox_4_71 as StdBox with uid="WBIIGAKISN",left=355, top=379, width=2,height=159
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsac_mdc",lower(this.oContained.GSAC_MDC.class))=0
        this.oContained.GSAC_MDC.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsac_atdPag5 as StdContainer
  Width  = 740
  height = 554
  stdWidth  = 740
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_5_2 as StdField with uid="XJTTZDEFUM",rtseq=156,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 87872474,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=134, Top=30, InputMask=replicate('X',5)

  add object oDESC_5_3 as StdField with uid="LZZEYGGJDH",rtseq=157,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88206794,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=198, Top=30, InputMask=replicate('X',35)

  add object oTDFLNSTA_5_4 as StdCheck with uid="ZZPZNUZEAZ",rtseq=158,rtrep=.f.,left=27, top=91, caption="No stampa immediata",;
    ToolTipText = "Se attivo: disabilita la stampa immediata del documento",;
    HelpContextID = 44450423,;
    cFormVar="w_TDFLNSTA", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oTDFLNSTA_5_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTDFLNSTA_5_4.GetRadio()
    this.Parent.oContained.w_TDFLNSTA = this.RadioValue()
    return .t.
  endfunc

  func oTDFLNSTA_5_4.SetRadio()
    this.Parent.oContained.w_TDFLNSTA=trim(this.Parent.oContained.w_TDFLNSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLNSTA=='S',1,;
      0)
  endfunc


  add object oLinkPC_5_5 as stdDynamicChildContainer with uid="YYCILGGURB",left=15, top=117, width=673, height=289, bOnScreen=.t.;



  add object oTDFLSTLM_5_7 as StdCombo with uid="DNXLCKJKSW",rtseq=159,rtrep=.f.,left=526,top=380,width=156,height=21;
    , ToolTipText = "Se attivo stampa report secondari/produzione";
    , HelpContextID = 201964925;
    , cFormVar="w_TDFLSTLM",RowSource=""+"No,"+"S�,"+"S� opzionali con conferma", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oTDFLSTLM_5_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oTDFLSTLM_5_7.GetRadio()
    this.Parent.oContained.w_TDFLSTLM = this.RadioValue()
    return .t.
  endfunc

  func oTDFLSTLM_5_7.SetRadio()
    this.Parent.oContained.w_TDFLSTLM=trim(this.Parent.oContained.w_TDFLSTLM)
    this.value = ;
      iif(this.Parent.oContained.w_TDFLSTLM=='N',1,;
      iif(this.Parent.oContained.w_TDFLSTLM=='S',2,;
      iif(this.Parent.oContained.w_TDFLSTLM=='B',3,;
      0)))
  endfunc

  add object oStr_5_1 as StdString with uid="LQDMPVFJME",Visible=.t., Left=15, Top=30,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo documento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_6 as StdString with uid="UXOWSYRQYR",Visible=.t., Left=15, Top=67,;
    Alignment=0, Width=651, Height=18,;
    Caption="Report documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_8 as StdString with uid="TJVILIJMHM",Visible=.t., Left=400, Top=380,;
    Alignment=1, Width=125, Height=18,;
    Caption="Report integrativi:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsve_mtd",lower(this.oContained.GSVE_MTD.class))=0
        this.oContained.GSVE_MTD.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_atd','TIP_DOCU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TDTIPDOC=TIP_DOCU.TDTIPDOC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
