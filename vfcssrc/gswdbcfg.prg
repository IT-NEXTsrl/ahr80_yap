* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gswdbcfg                                                        *
*              Connessione guidata al database                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-22                                                      *
* Last revis.: 2016-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gswdbcfg
DECLARE SHORT SQLConfigDataSource in odbccp32.DLL INTEGER hwndParent,INTEGER fRequest,STRING @lpszDriver,STRING @lpszAttributes
DECLARE SHORT SQLCreateDataSource in odbccp32.DLL INTEGER hwnd,STRING @lpszDS

PUBLIC ARRAY i_serverconn(1,6)
PUBLIC ARRAY i_tableprop(1,10)
PUBLIC i_ntables
i_ntables=0
i_serverconn=0
i_serverconn[1,6]="VFP"
i_codute=0
CREATE CURSOR cpusrgrp (groupcode N(5,0),Usercode N(5,0))
CREATE CURSOR cpprgsec (progname c(50))
CREATE CURSOR cpgroups (code N(4),name C(20),grptype C(1))
PUBLIC g_bDisableCfgGest
g_bDisableCfgGest=.T.
g_bShowCpToolBar=.F.
ocptoolbar.Visible=.F.

* Impostazioni internazionali

SET DATE ITALIAN
SET MARK TO "/"
SET CENTURY ON
SET POINT TO ','
SET SEPARATOR TO '.'

i_nGridLineColor=RGB(236,233,216)
i_cBackColor=RGB(255,231,162)
i_nEBackColor=RGB(246,246,246)
i_nOBLCOLOR=RGB(255,128,128)
SET STATUS BAR OFF
i_bWindowMenu = .f.
g_bShowCPToolBar = .t.
g_bShowDeskTopBar = .t.
g_nNumRecent = 5
i_nSCREENCOLOR = RGB(255, 255, 255)
g_bShowToolMenu = .t.
g_bShowMenu = .t.
g_bShowHideMenu = .T.
i_nZBTNSHW='S'
i_nDTLROWCLR=RGB(215,215,253)
If Type('i_udisabledforecolor')='U'
	Public i_udisabledforecolor
	Public i_udisabledbackcolor
	i_udisabledforecolor=Rgb(0,41,91)
	i_udisabledbackcolor=Rgb(233,238,238)
EndIf
If type('g_UseProgBar')='U'
	public g_UseProgBar
	g_UseProgBar=.t.	
EndIf
* Nuova Interfaccia
i_Visualtheme = 6
i_cViewMode = 'A'
g_bRecordMark = .F.
i_cMenuTab = "T"
i_cDeskMenu = 'S'
i_bGradientBck = .T.
* Font
i_cFontName = "Arial"
i_cCboxFontName = "Arial"
i_cBtnFontName = "Arial"
i_cPageFontName = "Tahoma"
i_cGrdFontName = "Arial"
i_cLblFontName = "Arial"
i_nFontSize = 9
i_nPageFontSize = 9
i_nCboxFontSize = 8
i_nBtnFontSize = 7
i_nGrdFontSize = 9
i_nLblFontSize = 9

public g_cp_IsAdministrator_0
g_cp_IsAdministrator_0 = .f.

public g_NMASKFIXEDWIDTH
public g_NMASKFIXEDHEIGHT
public g_CMASKSTARTUPDIMENSION
g_NMASKFIXEDWIDTH = 0
g_NMASKFIXEDHEIGHT = 0
g_CMASKSTARTUPDIMENSION = "S"
* --- Fine Area Manuale
return(createobject("tgswdbcfg",oParentObject))

* --- Class definition
define class tgswdbcfg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 665
  Height = 384+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-27"
  HelpContextID=57098601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  _IDX = 0
  cPrg = "gswdbcfg"
  cComment = "Connessione guidata al database"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ISALT = .F.
  w_CUR_STEP = 0
  w_TOT_STEP = 0
  w_DBTYPE = space(10)
  o_DBTYPE = space(10)
  w_CFGTYPE = space(1)
  o_CFGTYPE = space(1)
  w_DRV_NAME = space(50)
  w_SRV_NAME = space(50)
  o_SRV_NAME = space(50)
  w_IST_NAME = space(50)
  w_SERVICE = space(50)
  w_PROTOCOL = space(20)
  w_SRV_PORT = space(10)
  w_CONNTYPE = space(1)
  w_USER__ID = space(50)
  w_PASSWORD = space(50)
  w_DB__MODE = space(1)
  o_DB__MODE = space(1)
  w_DB__NAME = space(30)
  w_DSNTYPE = space(4)
  w_DRVONAME = space(50)
  w_ODBCNAME = space(50)
  w_CONNTYPE = space(1)
  w_USER__ID = space(50)
  w_PASSWORD = space(50)
  w_DB__MODE = space(1)
  w_DB__NAME = space(30)
  w_LOAD_XDC = .F.
  w_CHECKUNC = .F.
  w_MSKINGES = .F.
  w_MASKBCKG = .F.
  w_DISIDXCR = .F.
  w_POSTINON = .F.
  w_DISIDXIN = .F.
  w_CIFRA_AS = .F.
  w_ENMODCFG = .F.
  w_FLATTOOL = 0
  w_FLATGEST = 0
  w_FLATPRIN = 0
  w_TERMPRIN = space(1)
  w_APPTITLE = space(100)
  w_ORACVERS = space(2)
  w_MSG = space(0)
  w_VERIFCON = .F.
  w_ODBCINFO = space(0)
  w_OLDPATH = space(254)
  w_POSQLDIR = space(200)
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gswdbcfg
  * Posiziono la finestra sempre al centro dell'applicativo
  Autocenter=.T.
  bNoRightClick=.T.
  * Array con i tipi di database gestiti
  * Contiene le informazioni necessarie per
  * la visualizzazione, la ricerca dei driver/ODBC
  * e la ricerca degli eventuali archivi dimostrativi da
  * ripristinare
  * Colonna Contenuto
  *    1     Nome tipo database da visualizzare
  *    2     Path relativo alla cartella exe dove
  *               sono posizionati gli archivi demo
  *    3     Etichetta da posizionare nel cp3start.cnf
  *    4     Etichetta per identificazione driver ODBC
  *             Serve per la ricerca nel registro dei driver installati
  *    5     Libreria identificazione driver ODBC
  *             Serve per la ricerca fra gli ODBC definiti a che
  *                driver � associato
  *    6     ID tipo database per controlli nella machera/batch
  *************************************************************
  dimension w_DbEngines(9,6)
     w_DbEngines(1,1)="SQL Server 2000/MSDE"
     w_DbEngines(1,2)="sqlbck\"
     w_DbEngines(1,3)="SQLServer"
     w_DbEngines(1,4)="SQL Server"
     w_DbEngines(1,5)="SQLSRV32.DLL"
     w_DbEngines(1,6)="SQL2000"
  
     w_DbEngines(2,1)="SQL Server 2005/Express"
     w_DbEngines(2,2)="sqlbck\"
     w_DbEngines(2,3)="SQLServer"
     w_DbEngines(2,4)="SQL Native Client"
     w_DbEngines(2,5)="SQLNCLI.DLL"
     w_DbEngines(2,6)="SQL2005"
  
     w_DbEngines(3,1)="Oracle standard edition"
     w_DbEngines(3,2)=""
     w_DbEngines(3,3)="Oracle"
     w_DbEngines(3,4)="Oracle in"
     w_DbEngines(3,5)="SQORA32.DLL"
     w_DbEngines(3,6)="Oracle"
  
     *w_DbEngines(4,1)="IBM Db2 Universal Database"
     *w_DbEngines(4,2)=""
     *w_DbEngines(4,3)="DB2"
     *w_DbEngines(4,4)="IBM DB2"
     *w_DbEngines(4,5)="DB2CLI.DLL"
     *w_DbEngines(4,6)="IBMDB2"
  
     w_DbEngines(4,1)="SQL Server 2008/Express"
     w_DbEngines(4,2)="sqlbck\"
     w_DbEngines(4,3)="SQLServer"
     w_DbEngines(4,4)="SQL Server Native Client 10.0"
     w_DbEngines(4,5)="SQLNCLI10.DLL"
     w_DbEngines(4,6)="SQL2008"
  
     w_DbEngines(5,1)="SQL Server 2012/Express e successive"
     w_DbEngines(5,2)="sqlbck\"
     w_DbEngines(5,3)="SQLServer"
     w_DbEngines(5,4)="SQL Server Native Client 11.0"
     w_DbEngines(5,5)="SQLNCLI11.DLL"
     w_DbEngines(5,6)="SQL2012"
  
     w_DbEngines(6,1)="ODBC Driver 11 for SQL Server"
     w_DbEngines(6,2)="sqlbck\"
     w_DbEngines(6,3)="SQLServer"
     w_DbEngines(6,4)="ODBC Driver 11 for SQL Server"
     w_DbEngines(6,5)="MSODBCSQL11.DLL"
     w_DbEngines(6,6)="SQL2014"
  
     w_DbEngines(7,1)="ODBC Driver 13 for SQL Server"
     w_DbEngines(7,2)="sqlbck\"
     w_DbEngines(7,3)="SQLServer"
     w_DbEngines(7,4)="ODBC Driver 13 for SQL Server"
     w_DbEngines(7,5)="MSODBCSQL13.DLL"
     w_DbEngines(7,6)="SQL2016"
     
     w_DbEngines(8,1)="PostgreSQL 9.3"
     w_DbEngines(8,2)=""
     w_DbEngines(8,3)="PostgreSQL"
     w_DbEngines(8,4)="PostgreSQL ANSI"
     w_DbEngines(8,5)="PSQLODBC30A.DLL"
     w_DbEngines(8,6)="PostgreSQL ANSI"
  
     w_DbEngines(9,1)="PostgreSQL 9.3 Driver"
     w_DbEngines(9,2)=""
     w_DbEngines(9,3)="PostgreSQL"
     w_DbEngines(9,4)="PostgreSQL ODBC Driver(ANSI)"
     w_DbEngines(9,5)="PSQLODBC30A.DLL"
     w_DbEngines(9,6)="PostgreSQL ODBC"
     
  * Array con la lista degli ODBC presenti sulla macchina
  * filtrati per tipo di driver selezionato
  * Colonna  Contenuto
  *    1       Nome ODBC
  *    2       Database da utilizzare
  *    3       Driver ODBC
  *    4       Utente impostato
  *    5       Server per la connessione
  *    6       Utilizzo connessione trusted
  *    7       Descrizione ODBC
  * Le informazioni non presenti o non disponibili poich�
  * proprietarie di alcuni tipi di database vengono
  * sostituite con l'etichetta <Non disponibile>
  *************************************************************
  dimension w_ODBCList(1,7)
  *Inizializzazione combo tipo database
  w_LastODBCDRV="SQL2000"
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgswdbcfgPag1","gswdbcfg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgswdbcfgPag2","gswdbcfg",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(3).addobject("oPag","tgswdbcfgPag3","gswdbcfg",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Pag.3")
      .Pages(4).addobject("oPag","tgswdbcfgPag4","gswdbcfg",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pag.4")
      .Pages(5).addobject("oPag","tgswdbcfgPag5","gswdbcfg",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Pag.5")
      .Pages(6).addobject("oPag","tgswdbcfgPag6","gswdbcfg",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Pag.6")
      .Pages(6).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gswdbcfg
    if isAHE()
      local L_CTRL
      L_CTRL=this.Parent.getctrl("w_DBTYPE")
      L_CTRL.Clear()
      L_CTRL.Init()
      L_CTRL = .null.
      release L_CTRL
    endif
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_STEPS = this.oPgFrm.Pages(2).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(3).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(4).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(5).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(6).oPag.STEPS
    DoDefault()
    proc Destroy()
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISALT=.f.
      .w_CUR_STEP=0
      .w_TOT_STEP=0
      .w_DBTYPE=space(10)
      .w_CFGTYPE=space(1)
      .w_DRV_NAME=space(50)
      .w_SRV_NAME=space(50)
      .w_IST_NAME=space(50)
      .w_SERVICE=space(50)
      .w_PROTOCOL=space(20)
      .w_SRV_PORT=space(10)
      .w_CONNTYPE=space(1)
      .w_USER__ID=space(50)
      .w_PASSWORD=space(50)
      .w_DB__MODE=space(1)
      .w_DB__NAME=space(30)
      .w_DSNTYPE=space(4)
      .w_DRVONAME=space(50)
      .w_ODBCNAME=space(50)
      .w_CONNTYPE=space(1)
      .w_USER__ID=space(50)
      .w_PASSWORD=space(50)
      .w_DB__MODE=space(1)
      .w_DB__NAME=space(30)
      .w_LOAD_XDC=.f.
      .w_CHECKUNC=.f.
      .w_MSKINGES=.f.
      .w_MASKBCKG=.f.
      .w_DISIDXCR=.f.
      .w_POSTINON=.f.
      .w_DISIDXIN=.f.
      .w_CIFRA_AS=.f.
      .w_ENMODCFG=.f.
      .w_FLATTOOL=0
      .w_FLATGEST=0
      .w_FLATPRIN=0
      .w_TERMPRIN=space(1)
      .w_APPTITLE=space(100)
      .w_ORACVERS=space(2)
      .w_MSG=space(0)
      .w_VERIFCON=.f.
      .w_ODBCINFO=space(0)
      .w_OLDPATH=space(254)
      .w_POSQLDIR=space(200)
        .w_ISALT = ISALT()
        .w_CUR_STEP = 1
        .w_TOT_STEP = this.oPGFRM.PageCount
        .w_DBTYPE = .w_LastODBCDRV
        .w_CFGTYPE = IIF(LEFT(.w_DBTYPE,5)='SQL20', 'S', IIF(EMPTY(NVL(.w_CFGTYPE, ' ')) OR .w_CFGTYPE='S', 'C', .w_CFGTYPE))
          .DoRTCalc(6,6,.f.)
        .w_SRV_NAME = IIF(.w_CFGTYPE='S' OR EMPTY(.w_SRV_NAME), Alltrim(Substr(Sys(0),1,At('#',Sys(0))-1)) ,.w_SRV_NAME )
        .w_IST_NAME = ICASE(LEFT(.w_DBTYPE,5)<>'SQL20', '', .w_DBTYPE='SQL2000', '', (.w_DBTYPE='SQL2005' OR .w_DBTYPE='SQL2008'  OR .w_DBTYPE='SQL2012') AND (.w_CFGTYPE='S' OR EMPTY(.w_IST_NAME)), 'SQLEXPRESS', .w_IST_NAME)
        .w_SERVICE = IIF(.w_DBTYPE='Oracle', .w_SRV_NAME , '')
        .w_PROTOCOL = IIF(.w_DBTYPE='IBMDB2', 'TCPIP', '')
        .w_SRV_PORT = IIF(.w_DBTYPE='PostgreSQL', '5432', IIF(.w_DBTYPE='IBMDB2', '50000', ''))
        .w_CONNTYPE = IIF(.w_DBTYPE<>'SQL2005' AND .w_DBTYPE<>'SQL2008' AND .w_DBTYPE<>'SQL2012', 'U', IIF(.w_CFGTYPE='S' OR EMPTY(.w_CONNTYPE), 'T', .w_CONNTYPE))
        .w_USER__ID = IIF(LEFT(.w_DBTYPE,5)='SQL20' AND EMPTY(.w_USER__ID), 'sa',IIF( .w_DBTYPE='PostgreSQL','postgres',''))
          .DoRTCalc(14,14,.f.)
        .w_DB__MODE = ICASE(.w_CFGTYPE='O','E', .w_CFGTYPE='S', 'R', 'E')
        .w_DB__NAME = IIF(.w_DB__MODE<>'R' AND !EMPTY(.w_DB__NAME), .w_DB__NAME, "")
        .w_DSNTYPE = 'HKCU'
          .DoRTCalc(18,19,.f.)
        .w_CONNTYPE = IIF(.w_DBTYPE<>'SQL2005' AND .w_DBTYPE<>'SQL2008'  AND .w_DBTYPE<>'SQL2012' , 'U', IIF(.w_CFGTYPE='S' OR EMPTY(.w_CONNTYPE), 'T', .w_CONNTYPE))
        .w_USER__ID = IIF(LEFT(.w_DBTYPE,5)='SQL20' AND EMPTY(.w_USER__ID), 'sa', .w_USER__ID)
          .DoRTCalc(22,22,.f.)
        .w_DB__MODE = IIF(.w_CFGTYPE='O','E', IIF(LEFT(.w_DBTYPE,5)='SQL20', IIF(EMPTY(.w_DB__MODE), 'R', .w_DB__MODE), 'E'))
        .w_DB__NAME = ''
        .w_LOAD_XDC = .T.
        .w_CHECKUNC = .w_ISALT
        .w_MSKINGES = .F.
        .w_MASKBCKG = .F.
        .w_DISIDXCR = .F.
        .w_POSTINON = .F.
        .w_DISIDXIN = .F.
        .w_CIFRA_AS = .F.
        .w_ENMODCFG = .w_ISALT
        .w_FLATTOOL = IIF(.w_ISALT, 0, 2)
        .w_FLATGEST = IIF(.w_ISALT, 0, 2)
        .w_FLATPRIN = IIF(.w_ISALT, 0, 2)
        .w_TERMPRIN = '#'
          .DoRTCalc(38,38,.f.)
        .w_ORACVERS = '9'
      .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
          .DoRTCalc(40,40,.f.)
        .w_VERIFCON = .F.
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(AH_Msgformat("La procedura guidata permette di configurare la connessione al database %0per questa e per tutte le altre postazioni di lavoro."))
          .DoRTCalc(42,43,.f.)
        .w_POSQLDIR = IIF(.w_DBTYPE='PostgreSQL',GetDirPostgres(),'')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_12.enabled = this.oPgFrm.Page3.oPag.oBtn_3_12.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_16.enabled = this.oPgFrm.Page3.oPag.oBtn_3_16.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_6.enabled = this.oPgFrm.Page4.oPag.oBtn_4_6.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_7.enabled = this.oPgFrm.Page4.oPag.oBtn_4_7.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_14.enabled = this.oPgFrm.Page4.oPag.oBtn_4_14.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_18.enabled = this.oPgFrm.Page4.oPag.oBtn_4_18.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_19.enabled = this.oPgFrm.Page5.oPag.oBtn_5_19.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_4.enabled = this.oPgFrm.Page6.oPag.oBtn_6_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gswdbcfg
    * Nascondo le tab
    This.oTabMenu.Visible=.F.
    This.oTabMenu.Enabled=.F.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_DBTYPE<>.w_DBTYPE
            .w_CFGTYPE = IIF(LEFT(.w_DBTYPE,5)='SQL20', 'S', IIF(EMPTY(NVL(.w_CFGTYPE, ' ')) OR .w_CFGTYPE='S', 'C', .w_CFGTYPE))
        endif
        .DoRTCalc(6,6,.t.)
        if .o_DBTYPE<>.w_DBTYPE
            .w_SRV_NAME = IIF(.w_CFGTYPE='S' OR EMPTY(.w_SRV_NAME), Alltrim(Substr(Sys(0),1,At('#',Sys(0))-1)) ,.w_SRV_NAME )
        endif
        if .o_DBTYPE<>.w_DBTYPE.or. .o_CFGTYPE<>.w_CFGTYPE
            .w_IST_NAME = ICASE(LEFT(.w_DBTYPE,5)<>'SQL20', '', .w_DBTYPE='SQL2000', '', (.w_DBTYPE='SQL2005' OR .w_DBTYPE='SQL2008'  OR .w_DBTYPE='SQL2012') AND (.w_CFGTYPE='S' OR EMPTY(.w_IST_NAME)), 'SQLEXPRESS', .w_IST_NAME)
        endif
        if .o_DBTYPE<>.w_DBTYPE.or. .o_SRV_NAME<>.w_SRV_NAME
            .w_SERVICE = IIF(.w_DBTYPE='Oracle', .w_SRV_NAME , '')
        endif
        if .o_DBTYPE<>.w_DBTYPE
            .w_PROTOCOL = IIF(.w_DBTYPE='IBMDB2', 'TCPIP', '')
        endif
        if .o_DBTYPE<>.w_DBTYPE
            .w_SRV_PORT = IIF(.w_DBTYPE='PostgreSQL', '5432', IIF(.w_DBTYPE='IBMDB2', '50000', ''))
        endif
        if .o_DBTYPE<>.w_DBTYPE.or. .o_CFGTYPE<>.w_CFGTYPE
            .w_CONNTYPE = IIF(.w_DBTYPE<>'SQL2005' AND .w_DBTYPE<>'SQL2008' AND .w_DBTYPE<>'SQL2012', 'U', IIF(.w_CFGTYPE='S' OR EMPTY(.w_CONNTYPE), 'T', .w_CONNTYPE))
        endif
        if .o_DBTYPE<>.w_DBTYPE.or. .o_CFGTYPE<>.w_CFGTYPE
            .w_USER__ID = IIF(LEFT(.w_DBTYPE,5)='SQL20' AND EMPTY(.w_USER__ID), 'sa',IIF( .w_DBTYPE='PostgreSQL','postgres',''))
        endif
        .DoRTCalc(14,14,.t.)
        if .o_CFGTYPE<>.w_CFGTYPE.or. .o_DBTYPE<>.w_DBTYPE
            .w_DB__MODE = ICASE(.w_CFGTYPE='O','E', .w_CFGTYPE='S', 'R', 'E')
        endif
        if .o_CFGTYPE<>.w_CFGTYPE.or. .o_DBTYPE<>.w_DBTYPE
            .w_DB__NAME = IIF(.w_DB__MODE<>'R' AND !EMPTY(.w_DB__NAME), .w_DB__NAME, "")
        endif
        .DoRTCalc(17,19,.t.)
        if .o_DBTYPE<>.w_DBTYPE.or. .o_CFGTYPE<>.w_CFGTYPE
            .w_CONNTYPE = IIF(.w_DBTYPE<>'SQL2005' AND .w_DBTYPE<>'SQL2008'  AND .w_DBTYPE<>'SQL2012' , 'U', IIF(.w_CFGTYPE='S' OR EMPTY(.w_CONNTYPE), 'T', .w_CONNTYPE))
        endif
        if .o_DBTYPE<>.w_DBTYPE.or. .o_CFGTYPE<>.w_CFGTYPE
            .w_USER__ID = IIF(LEFT(.w_DBTYPE,5)='SQL20' AND EMPTY(.w_USER__ID), 'sa', .w_USER__ID)
        endif
        .DoRTCalc(22,22,.t.)
        if .o_CFGTYPE<>.w_CFGTYPE.or. .o_DBTYPE<>.w_DBTYPE
            .w_DB__MODE = IIF(.w_CFGTYPE='O','E', IIF(LEFT(.w_DBTYPE,5)='SQL20', IIF(EMPTY(.w_DB__MODE), 'R', .w_DB__MODE), 'E'))
        endif
        if .o_CFGTYPE<>.w_CFGTYPE.or. .o_DBTYPE<>.w_DBTYPE
            .w_DB__NAME = ''
        endif
        .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        if .o_DB__MODE<>.w_DB__MODE
          .Calculate_AXYAQDQNXA()
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(AH_Msgformat("La procedura guidata permette di configurare la connessione al database %0per questa e per tutte le altre postazioni di lavoro."))
        .DoRTCalc(25,43,.t.)
        if .o_DBTYPE<>.w_DBTYPE
            .w_POSQLDIR = IIF(.w_DBTYPE='PostgreSQL',GetDirPostgres(),'')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(AH_Msgformat("La procedura guidata permette di configurare la connessione al database %0per questa e per tutte le altre postazioni di lavoro."))
    endwith
  return

  proc Calculate_BOWYRKWDAG()
    with this
          * --- Aggiorna ODBC
          gsbdbcfg(this;
              ,'AGGODBC';
             )
    endwith
  endproc
  proc Calculate_KEYWWCLPMJ()
    with this
          * --- Aggiornamento dati ODBC
          gsbdbcfg(this;
              ,'INFOODBC';
             )
    endwith
  endproc
  proc Calculate_AXYAQDQNXA()
    with this
          * --- Imposta nome database
          gsbdbcfg(this;
              ,'SETDBNAME';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oDRV_NAME_3_1.enabled = this.oPgFrm.Page3.oPag.oDRV_NAME_3_1.mCond()
    this.oPgFrm.Page3.oPag.oSRV_NAME_3_2.enabled = this.oPgFrm.Page3.oPag.oSRV_NAME_3_2.mCond()
    this.oPgFrm.Page3.oPag.oIST_NAME_3_3.enabled = this.oPgFrm.Page3.oPag.oIST_NAME_3_3.mCond()
    this.oPgFrm.Page3.oPag.oSERVICE_3_4.enabled = this.oPgFrm.Page3.oPag.oSERVICE_3_4.mCond()
    this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.enabled = this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.mCond()
    this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.enabled = this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.mCond()
    this.oPgFrm.Page3.oPag.oCONNTYPE_3_7.enabled = this.oPgFrm.Page3.oPag.oCONNTYPE_3_7.mCond()
    this.oPgFrm.Page3.oPag.oUSER__ID_3_8.enabled = this.oPgFrm.Page3.oPag.oUSER__ID_3_8.mCond()
    this.oPgFrm.Page3.oPag.oPASSWORD_3_9.enabled = this.oPgFrm.Page3.oPag.oPASSWORD_3_9.mCond()
    this.oPgFrm.Page3.oPag.oDB__MODE_3_10.enabled = this.oPgFrm.Page3.oPag.oDB__MODE_3_10.mCond()
    this.oPgFrm.Page3.oPag.oDB__NAME_3_11.enabled = this.oPgFrm.Page3.oPag.oDB__NAME_3_11.mCond()
    this.oPgFrm.Page4.oPag.oDSNTYPE_4_1.enabled = this.oPgFrm.Page4.oPag.oDSNTYPE_4_1.mCond()
    this.oPgFrm.Page4.oPag.oCONNTYPE_4_9.enabled = this.oPgFrm.Page4.oPag.oCONNTYPE_4_9.mCond()
    this.oPgFrm.Page4.oPag.oUSER__ID_4_10.enabled = this.oPgFrm.Page4.oPag.oUSER__ID_4_10.mCond()
    this.oPgFrm.Page4.oPag.oPASSWORD_4_11.enabled = this.oPgFrm.Page4.oPag.oPASSWORD_4_11.mCond()
    this.oPgFrm.Page4.oPag.oDB__MODE_4_12.enabled = this.oPgFrm.Page4.oPag.oDB__MODE_4_12.mCond()
    this.oPgFrm.Page4.oPag.oDB__NAME_4_13.enabled = this.oPgFrm.Page4.oPag.oDB__NAME_4_13.mCond()
    this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.enabled = this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_12.enabled = this.oPgFrm.Page3.oPag.oBtn_3_12.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_13.enabled = this.oPgFrm.Page3.oPag.oBtn_3_13.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_14.enabled = this.oPgFrm.Page3.oPag.oBtn_3_14.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_15.enabled = this.oPgFrm.Page3.oPag.oBtn_3_15.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_15.enabled = this.oPgFrm.Page4.oPag.oBtn_4_15.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_16.enabled = this.oPgFrm.Page4.oPag.oBtn_4_16.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_17.enabled = this.oPgFrm.Page4.oPag.oBtn_4_17.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_16.enabled = this.oPgFrm.Page5.oPag.oBtn_5_16.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_17.enabled = this.oPgFrm.Page5.oPag.oBtn_5_17.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_18.enabled = this.oPgFrm.Page5.oPag.oBtn_5_18.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_1.enabled = this.oPgFrm.Page6.oPag.oBtn_6_1.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_5.visible=!this.oPgFrm.Page2.oPag.oBtn_2_5.mHide()
    this.oPgFrm.Page3.oPag.oSERVICE_3_4.visible=!this.oPgFrm.Page3.oPag.oSERVICE_3_4.mHide()
    this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.visible=!this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.mHide()
    this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.visible=!this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.mHide()
    this.oPgFrm.Page3.oPag.oDB__NAME_3_11.visible=!this.oPgFrm.Page3.oPag.oDB__NAME_3_11.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_15.visible=!this.oPgFrm.Page3.oPag.oBtn_3_15.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_23.visible=!this.oPgFrm.Page3.oPag.oStr_3_23.mHide()
    this.oPgFrm.Page4.oPag.oDB__NAME_4_13.visible=!this.oPgFrm.Page4.oPag.oDB__NAME_4_13.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_17.visible=!this.oPgFrm.Page4.oPag.oBtn_4_17.mHide()
    this.oPgFrm.Page5.oPag.oDISIDXIN_5_7.visible=!this.oPgFrm.Page5.oPag.oDISIDXIN_5_7.mHide()
    this.oPgFrm.Page5.oPag.oORACVERS_5_15.visible=!this.oPgFrm.Page5.oPag.oORACVERS_5_15.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_18.visible=!this.oPgFrm.Page5.oPag.oBtn_5_18.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_2.visible=!this.oPgFrm.Page6.oPag.oBtn_6_2.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_28.visible=!this.oPgFrm.Page3.oPag.oStr_3_28.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_29.visible=!this.oPgFrm.Page3.oPag.oStr_3_29.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_30.visible=!this.oPgFrm.Page3.oPag.oStr_3_30.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_26.visible=!this.oPgFrm.Page4.oPag.oStr_4_26.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_30.visible=!this.oPgFrm.Page5.oPag.oStr_5_30.mHide()
    this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.visible=!this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_34.visible=!this.oPgFrm.Page3.oPag.oStr_3_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("AggODBC") or lower(cEvent)==lower("w_DSNTYPE Changed")
          .Calculate_BOWYRKWDAG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ODBCNAME Changed") or lower(cEvent)==lower("AggInfoODBC")
          .Calculate_KEYWWCLPMJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page3.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page4.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page5.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page6.oPag.STEPS.Event(cEvent)
        if lower(cEvent)==lower("SetDbName")
          .Calculate_AXYAQDQNXA()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oDBTYPE_2_1.RadioValue()==this.w_DBTYPE)
      this.oPgFrm.Page2.oPag.oDBTYPE_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCFGTYPE_2_2.RadioValue()==this.w_CFGTYPE)
      this.oPgFrm.Page2.oPag.oCFGTYPE_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDRV_NAME_3_1.RadioValue()==this.w_DRV_NAME)
      this.oPgFrm.Page3.oPag.oDRV_NAME_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oSRV_NAME_3_2.value==this.w_SRV_NAME)
      this.oPgFrm.Page3.oPag.oSRV_NAME_3_2.value=this.w_SRV_NAME
    endif
    if not(this.oPgFrm.Page3.oPag.oIST_NAME_3_3.value==this.w_IST_NAME)
      this.oPgFrm.Page3.oPag.oIST_NAME_3_3.value=this.w_IST_NAME
    endif
    if not(this.oPgFrm.Page3.oPag.oSERVICE_3_4.value==this.w_SERVICE)
      this.oPgFrm.Page3.oPag.oSERVICE_3_4.value=this.w_SERVICE
    endif
    if not(this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.value==this.w_PROTOCOL)
      this.oPgFrm.Page3.oPag.oPROTOCOL_3_5.value=this.w_PROTOCOL
    endif
    if not(this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.value==this.w_SRV_PORT)
      this.oPgFrm.Page3.oPag.oSRV_PORT_3_6.value=this.w_SRV_PORT
    endif
    if not(this.oPgFrm.Page3.oPag.oCONNTYPE_3_7.RadioValue()==this.w_CONNTYPE)
      this.oPgFrm.Page3.oPag.oCONNTYPE_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oUSER__ID_3_8.value==this.w_USER__ID)
      this.oPgFrm.Page3.oPag.oUSER__ID_3_8.value=this.w_USER__ID
    endif
    if not(this.oPgFrm.Page3.oPag.oPASSWORD_3_9.value==this.w_PASSWORD)
      this.oPgFrm.Page3.oPag.oPASSWORD_3_9.value=this.w_PASSWORD
    endif
    if not(this.oPgFrm.Page3.oPag.oDB__MODE_3_10.RadioValue()==this.w_DB__MODE)
      this.oPgFrm.Page3.oPag.oDB__MODE_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDB__NAME_3_11.value==this.w_DB__NAME)
      this.oPgFrm.Page3.oPag.oDB__NAME_3_11.value=this.w_DB__NAME
    endif
    if not(this.oPgFrm.Page4.oPag.oDSNTYPE_4_1.RadioValue()==this.w_DSNTYPE)
      this.oPgFrm.Page4.oPag.oDSNTYPE_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDRVONAME_4_3.RadioValue()==this.w_DRVONAME)
      this.oPgFrm.Page4.oPag.oDRVONAME_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oODBCNAME_4_4.RadioValue()==this.w_ODBCNAME)
      this.oPgFrm.Page4.oPag.oODBCNAME_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCONNTYPE_4_9.RadioValue()==this.w_CONNTYPE)
      this.oPgFrm.Page4.oPag.oCONNTYPE_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oUSER__ID_4_10.value==this.w_USER__ID)
      this.oPgFrm.Page4.oPag.oUSER__ID_4_10.value=this.w_USER__ID
    endif
    if not(this.oPgFrm.Page4.oPag.oPASSWORD_4_11.value==this.w_PASSWORD)
      this.oPgFrm.Page4.oPag.oPASSWORD_4_11.value=this.w_PASSWORD
    endif
    if not(this.oPgFrm.Page4.oPag.oDB__MODE_4_12.RadioValue()==this.w_DB__MODE)
      this.oPgFrm.Page4.oPag.oDB__MODE_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDB__NAME_4_13.value==this.w_DB__NAME)
      this.oPgFrm.Page4.oPag.oDB__NAME_4_13.value=this.w_DB__NAME
    endif
    if not(this.oPgFrm.Page5.oPag.oLOAD_XDC_5_1.RadioValue()==this.w_LOAD_XDC)
      this.oPgFrm.Page5.oPag.oLOAD_XDC_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCHECKUNC_5_2.RadioValue()==this.w_CHECKUNC)
      this.oPgFrm.Page5.oPag.oCHECKUNC_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oMSKINGES_5_3.RadioValue()==this.w_MSKINGES)
      this.oPgFrm.Page5.oPag.oMSKINGES_5_3.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oMASKBCKG_5_4.RadioValue()==this.w_MASKBCKG)
      this.oPgFrm.Page5.oPag.oMASKBCKG_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDISIDXCR_5_5.RadioValue()==this.w_DISIDXCR)
      this.oPgFrm.Page5.oPag.oDISIDXCR_5_5.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPOSTINON_5_6.RadioValue()==this.w_POSTINON)
      this.oPgFrm.Page5.oPag.oPOSTINON_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDISIDXIN_5_7.RadioValue()==this.w_DISIDXIN)
      this.oPgFrm.Page5.oPag.oDISIDXIN_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oCIFRA_AS_5_8.RadioValue()==this.w_CIFRA_AS)
      this.oPgFrm.Page5.oPag.oCIFRA_AS_5_8.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oENMODCFG_5_9.RadioValue()==this.w_ENMODCFG)
      this.oPgFrm.Page5.oPag.oENMODCFG_5_9.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLATTOOL_5_10.RadioValue()==this.w_FLATTOOL)
      this.oPgFrm.Page5.oPag.oFLATTOOL_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLATGEST_5_11.RadioValue()==this.w_FLATGEST)
      this.oPgFrm.Page5.oPag.oFLATGEST_5_11.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oFLATPRIN_5_12.RadioValue()==this.w_FLATPRIN)
      this.oPgFrm.Page5.oPag.oFLATPRIN_5_12.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTERMPRIN_5_13.value==this.w_TERMPRIN)
      this.oPgFrm.Page5.oPag.oTERMPRIN_5_13.value=this.w_TERMPRIN
    endif
    if not(this.oPgFrm.Page5.oPag.oAPPTITLE_5_14.value==this.w_APPTITLE)
      this.oPgFrm.Page5.oPag.oAPPTITLE_5_14.value=this.w_APPTITLE
    endif
    if not(this.oPgFrm.Page5.oPag.oORACVERS_5_15.RadioValue()==this.w_ORACVERS)
      this.oPgFrm.Page5.oPag.oORACVERS_5_15.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oMSG_6_5.value==this.w_MSG)
      this.oPgFrm.Page6.oPag.oMSG_6_5.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.value==this.w_POSQLDIR)
      this.oPgFrm.Page3.oPag.oPOSQLDIR_3_33.value=this.w_POSQLDIR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(LEFT(.w_DBTYPE,5)='SQL20' OR (LEFT(.w_DBTYPE,5)<>'SQL20' And .w_CFGTYPE<>'S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFGTYPE_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La configurazione semplificata � selezionabile solo con il tipo di database SQLServer")
          case   not(LEFT(.w_DBTYPE,5)='SQL20' OR .w_CONNTYPE<>'T')  and (.w_CFGTYPE='C' AND LEFT(.w_DBTYPE,5)='SQL20')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCONNTYPE_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La connessione attraverso la modalit� trusted connection � valida solo per database di tipo SQLServer")
          case   not(INLIST(Upper(LEFT(.w_DBTYPE,5)), 'SQL20', 'POSTG') OR .w_DB__MODE='E')  and (.w_CFGTYPE='C' and INLIST(Upper(LEFT(.w_DBTYPE,5)), 'SQL20', 'POSTG'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDB__MODE_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Archivi demo non trovati oppure database differente da SQLServer o PostgreSQL")
          case   not(LEFT(.w_DBTYPE,5)='SQL20' OR .w_CONNTYPE<>'T')  and (.w_CFGTYPE='O' AND LEFT(.w_DBTYPE,5)='SQL20')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCONNTYPE_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La connessione attraverso la modalit� trusted connection � valida solo per database di tipo SQLServer")
          case   not(LEFT(.w_DBTYPE,5)='SQL20' OR .w_DB__MODE='E')  and (.w_CFGTYPE='O' and LEFT(.w_DBTYPE,5)='SQL20' and 1=0)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDB__MODE_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Archivi demo non trovati oppure database differente da SQLServer")
          case   not(AT('bin',LOWER(.w_POSQLDIR))=0)  and not(.w_DBTYPE<>'PostgreSQL')  and (.w_DBTYPE='PostgreSQL')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPOSQLDIR_3_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso indicato non � corretto.%0Nel percoso dell'installazione impostare la directory bin di PostgreSQL.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DBTYPE = this.w_DBTYPE
    this.o_CFGTYPE = this.w_CFGTYPE
    this.o_SRV_NAME = this.w_SRV_NAME
    this.o_DB__MODE = this.w_DB__MODE
    return

enddefine

* --- Define pages as container
define class tgswdbcfgPag1 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_2 as StdButton with uid="IBRDXZOPMJ",left=453, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=1;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_1_3 as StdButton with uid="JUAVHGMTDZ",left=400, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=1;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="NLRIOAGGKF",left=506, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_1_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="ABKYYMEJLO",left=559, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_11 as cp_calclbl with uid="UFNLBKNVLO",left=7, top=37, width=648,height=49,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 120899046

  add object oStr_1_6 as StdString with uid="JXZYDUXRFF",Visible=.t., Left=7, Top=85,;
    Alignment=0, Width=609, Height=18,;
    Caption="Verr� richiesto il tipo di database a cui connettersi e alcuni parametri necessari per la connessione."  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="URYVCGNFAN",left=391, top=323, width=223,height=2
enddefine
define class tgswdbcfgPag2 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDBTYPE_2_1 as StdTableComboODBC_DRV with uid="FRJNOIDHZX",rtseq=4,rtrep=.f.,left=170,top=70,width=294,height=22;
    , HelpContextID = 116868406;
    , cFormVar="w_DBTYPE",tablefilter="", bObbl = .f. , nPag = 2;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.



  add object oCFGTYPE_2_2 as StdCombo with uid="PCEYPJTCON",rtseq=5,rtrep=.f.,left=170,top=135,width=294,height=22;
    , HelpContextID = 226395866;
    , cFormVar="w_CFGTYPE",RowSource=""+"Semplificata,"+"Standard,"+"Avanzata", bObbl = .f. , nPag = 2;
    , sErrorMsg = "La configurazione semplificata � selezionabile solo con il tipo di database SQLServer";
  , bGlobalFont=.t.


  func oCFGTYPE_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oCFGTYPE_2_2.GetRadio()
    this.Parent.oContained.w_CFGTYPE = this.RadioValue()
    return .t.
  endfunc

  func oCFGTYPE_2_2.SetRadio()
    this.Parent.oContained.w_CFGTYPE=trim(this.Parent.oContained.w_CFGTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_CFGTYPE=='S',1,;
      iif(this.Parent.oContained.w_CFGTYPE=='C',2,;
      iif(this.Parent.oContained.w_CFGTYPE=='O',3,;
      0)))
  endfunc

  func oCFGTYPE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_DBTYPE,5)='SQL20' OR (LEFT(.w_DBTYPE,5)<>'SQL20' And .w_CFGTYPE<>'S'))
    endwith
    return bRes
  endfunc


  add object oBtn_2_3 as StdButton with uid="OXWSHLFWQX",left=453, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=2;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="IBGLZEVYNM",left=400, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=2;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_2_5 as StdButton with uid="WQCQKNSWMF",left=506, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_2_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_2_6 as StdButton with uid="FPTOWIGFRA",left=559, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="KPTJALNOOY",left=13, top=11, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=2;
    , HelpContextID = 232953818

  add object oStr_2_7 as StdString with uid="MEGSMJNLPX",Visible=.t., Left=17, Top=38,;
    Alignment=0, Width=590, Height=18,;
    Caption="Specificare il tipo di database utilizzato"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="PAROWEPHLF",Visible=.t., Left=22, Top=74,;
    Alignment=1, Width=143, Height=19,;
    Caption="Tipo database:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_9 as StdString with uid="DQAJXTGKCD",Visible=.t., Left=17, Top=105,;
    Alignment=0, Width=590, Height=18,;
    Caption="Specificare il tipo di configurazione da utilizzare"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="KLKSXVAMGJ",Visible=.t., Left=22, Top=136,;
    Alignment=1, Width=143, Height=19,;
    Caption="Configurazione:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_12 as StdBox with uid="QFMHNXZTPY",left=391, top=323, width=223,height=2
enddefine
define class tgswdbcfgPag3 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDRV_NAME_3_1 as StdTableComboSELODBC_DRV with uid="HQYLPTQZAL",rtseq=6,rtrep=.f.,left=144,top=51,width=363,height=22;
    , HelpContextID = 220367493;
    , cFormVar="w_DRV_NAME",tablefilter="", bObbl = .f. , nPag = 3;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func oDRV_NAME_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  add object oSRV_NAME_3_2 as StdField with uid="URJZWZKBPI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SRV_NAME", cQueryName = "SRV_NAME",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 220367253,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=76, InputMask=replicate('X',50)

  func oSRV_NAME_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  add object oIST_NAME_3_3 as StdField with uid="JLLTUVYHDB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IST_NAME", cQueryName = "IST_NAME",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 220375349,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=101, InputMask=replicate('X',50)

  func oIST_NAME_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_CFGTYPE='C' OR (.w_CFGTYPE='S' and !.w_ISALT)) AND LEFT(.w_DBTYPE,5)='SQL20')
    endwith
   endif
  endfunc

  add object oSERVICE_3_4 as StdField with uid="ZIEXYHMFLM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SERVICE", cQueryName = "SERVICE",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 75770150,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=126, InputMask=replicate('X',50)

  func oSERVICE_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  func oSERVICE_3_4.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'Oracle')
    endwith
  endfunc

  add object oPROTOCOL_3_5 as StdField with uid="BSRNYVTALH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PROTOCOL", cQueryName = "PROTOCOL",;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 81921602,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=150, InputMask=replicate('X',20)

  func oPROTOCOL_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  func oPROTOCOL_3_5.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'IBMDB2')
    endwith
  endfunc

  add object oSRV_PORT_3_6 as StdField with uid="JWNXEKBNNG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SRV_PORT", cQueryName = "SRV_PORT",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 16610938,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=424, Top=150, InputMask=replicate('X',10)

  func oSRV_PORT_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  func oSRV_PORT_3_6.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'IBMDB2' and .w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc


  add object oCONNTYPE_3_7 as StdCombo with uid="LDPYIKIZQY",rtseq=12,rtrep=.f.,left=144,top=177,width=363,height=22;
    , HelpContextID = 187429483;
    , cFormVar="w_CONNTYPE",RowSource=""+"UserID e password,"+"Trusted connection", bObbl = .f. , nPag = 3;
    , sErrorMsg = "La connessione attraverso la modalit� trusted connection � valida solo per database di tipo SQLServer";
  , bGlobalFont=.t.


  func oCONNTYPE_3_7.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oCONNTYPE_3_7.GetRadio()
    this.Parent.oContained.w_CONNTYPE = this.RadioValue()
    return .t.
  endfunc

  func oCONNTYPE_3_7.SetRadio()
    this.Parent.oContained.w_CONNTYPE=trim(this.Parent.oContained.w_CONNTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_CONNTYPE=='U',1,;
      iif(this.Parent.oContained.w_CONNTYPE=='T',2,;
      0))
  endfunc

  func oCONNTYPE_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C' AND LEFT(.w_DBTYPE,5)='SQL20')
    endwith
   endif
  endfunc

  func oCONNTYPE_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_DBTYPE,5)='SQL20' OR .w_CONNTYPE<>'T')
    endwith
    return bRes
  endfunc

  add object oUSER__ID_3_8 as StdField with uid="HPMXTDCODJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_USER__ID", cQueryName = "USER__ID",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 237017206,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=201, InputMask=replicate('X',50)

  func oUSER__ID_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C' AND .w_CONNTYPE='U')
    endwith
   endif
  endfunc

  add object oPASSWORD_3_9 as StdField with uid="HRTGVZOINH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PASSWORD", cQueryName = "PASSWORD",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 23147834,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=228, InputMask=replicate('X',50), PasswordChar='*'

  func oPASSWORD_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C' AND .w_CONNTYPE='U')
    endwith
   endif
  endfunc


  add object oDB__MODE_3_10 as StdCombo with uid="UZYOVCQYIZ",rtseq=15,rtrep=.f.,left=144,top=255,width=363,height=22;
    , HelpContextID = 13497723;
    , cFormVar="w_DB__MODE",RowSource=""+"Usa database esistente,"+"Crea nuovo database,"+"Ripristina archivi demo", bObbl = .f. , nPag = 3;
    , sErrorMsg = "Archivi demo non trovati oppure database differente da SQLServer o PostgreSQL";
  , bGlobalFont=.t.


  func oDB__MODE_3_10.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'N',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oDB__MODE_3_10.GetRadio()
    this.Parent.oContained.w_DB__MODE = this.RadioValue()
    return .t.
  endfunc

  func oDB__MODE_3_10.SetRadio()
    this.Parent.oContained.w_DB__MODE=trim(this.Parent.oContained.w_DB__MODE)
    this.value = ;
      iif(this.Parent.oContained.w_DB__MODE=='E',1,;
      iif(this.Parent.oContained.w_DB__MODE=='N',2,;
      iif(this.Parent.oContained.w_DB__MODE=='R',3,;
      0)))
  endfunc

  func oDB__MODE_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C' and INLIST(Upper(LEFT(.w_DBTYPE,5)), 'SQL20', 'POSTG'))
    endwith
   endif
  endfunc

  func oDB__MODE_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (INLIST(Upper(LEFT(.w_DBTYPE,5)), 'SQL20', 'POSTG') OR .w_DB__MODE='E')
    endwith
    return bRes
  endfunc

  add object oDB__NAME_3_11 as StdField with uid="VOGJTADNHR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DB__NAME", cQueryName = "DB__NAME",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220334725,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=144, Top=282, InputMask=replicate('X',30)

  func oDB__NAME_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='C')
    endwith
   endif
  endfunc

  func oDB__NAME_3_11.mHide()
    with this.Parent.oContained
      return (LEFT(.w_DBTYPE,5)<>'SQL20' and .w_DBTYPE<>'IBMDB2' and .w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc


  add object oBtn_3_12 as StdButton with uid="GWQYEDTRLI",left=144, top=339, width=48,height=45,;
    CpPicture="..\EXE\BMP\DIBA20.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per verificare i parametri di connessione";
    , HelpContextID = 208226743;
    , Caption='V\<erifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_12.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"CHECK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Upper(LEFT(.w_DBTYPE,5))='SQL20' or .w_DB__MODE='E')
      endwith
    endif
  endfunc


  add object oBtn_3_13 as StdButton with uid="KFEDYUZQUB",left=453, top=339, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=3;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_13.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_3_14 as StdButton with uid="GHHBEBJDMM",left=400, top=339, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=3;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_14.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_3_15 as StdButton with uid="KIGLHDMHOH",left=506, top=339, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_15.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_3_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_3_16 as StdButton with uid="KJJZMUCIKP",left=559, top=339, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_16.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="WVSVYMCMKH",left=13, top=11, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=3;
    , HelpContextID = 232953818

  add object oPOSQLDIR_3_33 as StdField with uid="QLUGEBYRFX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_POSQLDIR", cQueryName = "POSQLDIR",;
    bObbl = .f. , nPag = 3, value=space(200), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso indicato non � corretto.%0Nel percoso dell'installazione impostare la directory bin di PostgreSQL.",;
    HelpContextID = 173063352,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=144, Top=309, InputMask=replicate('X',200), bHasZoom = .t. 

  func oPOSQLDIR_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DBTYPE='PostgreSQL')
    endwith
   endif
  endfunc

  func oPOSQLDIR_3_33.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc

  func oPOSQLDIR_3_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (AT('bin',LOWER(.w_POSQLDIR))=0)
    endwith
    return bRes
  endfunc

  proc oPOSQLDIR_3_33.mZoom
    this.parent.oContained.w_POSQLDIR=Cp_getdir()
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_3_17 as StdString with uid="GWICJIDTEX",Visible=.t., Left=8, Top=79,;
    Alignment=1, Width=130, Height=18,;
    Caption="Server:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="JNAPBGIJLI",Visible=.t., Left=8, Top=104,;
    Alignment=1, Width=130, Height=18,;
    Caption="Istanza:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="IESGLHJJDV",Visible=.t., Left=8, Top=177,;
    Alignment=1, Width=130, Height=18,;
    Caption="Connetti tramite:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="IMMZPZDBUE",Visible=.t., Left=8, Top=203,;
    Alignment=1, Width=130, Height=18,;
    Caption="Username:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="RVZWRKDAAD",Visible=.t., Left=8, Top=232,;
    Alignment=1, Width=130, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="KOVRIJTAQX",Visible=.t., Left=8, Top=255,;
    Alignment=1, Width=130, Height=18,;
    Caption="All'accesso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="RRMSFYYPOB",Visible=.t., Left=8, Top=283,;
    Alignment=1, Width=130, Height=18,;
    Caption="Nome database:"  ;
  , bGlobalFont=.t.

  func oStr_3_23.mHide()
    with this.Parent.oContained
      return (LEFT(.w_DBTYPE,5)<>'SQL20' and .w_DBTYPE<>'IBMDB2' and .w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc

  add object oStr_3_24 as StdString with uid="SNQXNWHMYR",Visible=.t., Left=17, Top=31,;
    Alignment=0, Width=579, Height=18,;
    Caption="Specificare i parametri necessari per la connessione al database"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="YCCKPZGSUS",Visible=.t., Left=8, Top=51,;
    Alignment=1, Width=130, Height=18,;
    Caption="Driver:"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="DJNNHBMQMM",Visible=.t., Left=8, Top=130,;
    Alignment=1, Width=130, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  func oStr_3_28.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'Oracle')
    endwith
  endfunc

  add object oStr_3_29 as StdString with uid="XRTHYRYWWA",Visible=.t., Left=8, Top=153,;
    Alignment=1, Width=130, Height=18,;
    Caption="Protocollo:"  ;
  , bGlobalFont=.t.

  func oStr_3_29.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'IBMDB2')
    endwith
  endfunc

  add object oStr_3_30 as StdString with uid="GJNYZZLRIU",Visible=.t., Left=301, Top=153,;
    Alignment=1, Width=122, Height=18,;
    Caption="Porta:"  ;
  , bGlobalFont=.t.

  func oStr_3_30.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'IBMDB2' and .w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc

  add object oStr_3_34 as StdString with uid="RCLWNISZRG",Visible=.t., Left=1, Top=311,;
    Alignment=1, Width=137, Height=18,;
    Caption="Percorso installazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_34.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'PostgreSQL')
    endwith
  endfunc

  add object oBox_3_31 as StdBox with uid="SFWNZERRDA",left=391, top=334, width=223,height=2
enddefine
define class tgswdbcfgPag4 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDSNTYPE_4_1 as StdCombo with uid="SUKWRKSNER",rtseq=17,rtrep=.f.,left=155,top=60,width=363,height=22;
    , HelpContextID = 226363850;
    , cFormVar="w_DSNTYPE",RowSource=""+"utente,"+"di sistema", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oDSNTYPE_4_1.RadioValue()
    return(iif(this.value =1,'HKCU',;
    iif(this.value =2,'HKLM',;
    space(4))))
  endfunc
  func oDSNTYPE_4_1.GetRadio()
    this.Parent.oContained.w_DSNTYPE = this.RadioValue()
    return .t.
  endfunc

  func oDSNTYPE_4_1.SetRadio()
    this.Parent.oContained.w_DSNTYPE=trim(this.Parent.oContained.w_DSNTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_DSNTYPE=='HKCU',1,;
      iif(this.Parent.oContained.w_DSNTYPE=='HKLM',2,;
      0))
  endfunc

  func oDSNTYPE_4_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O')
    endwith
   endif
  endfunc


  add object oDRVONAME_4_3 as StdTableComboSELODBC_DRV with uid="WNAZJFVNRZ",rtseq=18,rtrep=.f.,left=155,top=89,width=363,height=22;
    , HelpContextID = 221416069;
    , cFormVar="w_DRVONAME",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oODBCNAME_4_4 as StdTableComboODBC with uid="IJIKPMLGTD",rtseq=19,rtrep=.f.,left=155,top=114,width=363,height=22;
    , HelpContextID = 222287829;
    , cFormVar="w_ODBCNAME",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.



  add object oBtn_4_6 as StdButton with uid="TMDYQOCPLR",left=520, top=90, width=48,height=45,;
    CpPicture="..\EXE\BMP\CARICA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per creare un nuovo ODBC";
    , HelpContextID = 200922922;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_6.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEWODBC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_7 as StdButton with uid="OAMVASMCYJ",left=570, top=90, width=48,height=45,;
    CpPicture="..\EXE\BMP\REFRESH.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per aggiornare la lista degli ODBC";
    , HelpContextID = 100178535;
    , Caption='A\<ggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_7.Click()
      this.parent.oContained.NotifyEvent("AggODBC")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_8 as StdButton with uid="GVIRUQABPM",left=155, top=137, width=48,height=45,;
    CpPicture="..\EXE\BMP\INFO.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per visualizzare le informazioni dell'ODBC selezionato";
    , HelpContextID = 49376890;
    , Caption='In\<fo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_8.Click()
      with this.Parent.oContained
        ah_ErrorMsg(ALLTRIM(.w_ODBCINFO), 64, ah_Msgformat("Informazioni ODBC"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_ODBCNAME))
      endwith
    endif
  endfunc


  add object oCONNTYPE_4_9 as StdCombo with uid="JTXNMNHBIN",rtseq=20,rtrep=.f.,left=155,top=185,width=363,height=22;
    , HelpContextID = 187429483;
    , cFormVar="w_CONNTYPE",RowSource=""+"UserID e password,"+"Trusted connection", bObbl = .f. , nPag = 4;
    , sErrorMsg = "La connessione attraverso la modalit� trusted connection � valida solo per database di tipo SQLServer";
  , bGlobalFont=.t.


  func oCONNTYPE_4_9.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oCONNTYPE_4_9.GetRadio()
    this.Parent.oContained.w_CONNTYPE = this.RadioValue()
    return .t.
  endfunc

  func oCONNTYPE_4_9.SetRadio()
    this.Parent.oContained.w_CONNTYPE=trim(this.Parent.oContained.w_CONNTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_CONNTYPE=='U',1,;
      iif(this.Parent.oContained.w_CONNTYPE=='T',2,;
      0))
  endfunc

  func oCONNTYPE_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O' AND LEFT(.w_DBTYPE,5)='SQL20')
    endwith
   endif
  endfunc

  func oCONNTYPE_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_DBTYPE,5)='SQL20' OR .w_CONNTYPE<>'T')
    endwith
    return bRes
  endfunc

  add object oUSER__ID_4_10 as StdField with uid="WDEJUPRLYB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_USER__ID", cQueryName = "USER__ID",;
    bObbl = .f. , nPag = 4, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 237017206,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=155, Top=209, InputMask=replicate('X',50)

  func oUSER__ID_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O' AND .w_CONNTYPE='U')
    endwith
   endif
  endfunc

  add object oPASSWORD_4_11 as StdField with uid="WXJXHCKMMZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PASSWORD", cQueryName = "PASSWORD",;
    bObbl = .f. , nPag = 4, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 23147834,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=155, Top=236, InputMask=replicate('X',50), PasswordChar='*'

  func oPASSWORD_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O' AND .w_CONNTYPE='U')
    endwith
   endif
  endfunc


  add object oDB__MODE_4_12 as StdCombo with uid="PHZDGIXUEV",rtseq=23,rtrep=.f.,left=155,top=263,width=363,height=22;
    , HelpContextID = 13497723;
    , cFormVar="w_DB__MODE",RowSource=""+"Usa database esistente,"+"Crea nuovo database,"+"Ripristina archivi demo", bObbl = .f. , nPag = 4;
    , sErrorMsg = "Archivi demo non trovati oppure database differente da SQLServer";
  , bGlobalFont=.t.


  func oDB__MODE_4_12.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'N',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oDB__MODE_4_12.GetRadio()
    this.Parent.oContained.w_DB__MODE = this.RadioValue()
    return .t.
  endfunc

  func oDB__MODE_4_12.SetRadio()
    this.Parent.oContained.w_DB__MODE=trim(this.Parent.oContained.w_DB__MODE)
    this.value = ;
      iif(this.Parent.oContained.w_DB__MODE=='E',1,;
      iif(this.Parent.oContained.w_DB__MODE=='N',2,;
      iif(this.Parent.oContained.w_DB__MODE=='R',3,;
      0)))
  endfunc

  func oDB__MODE_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O' and LEFT(.w_DBTYPE,5)='SQL20' and 1=0)
    endwith
   endif
  endfunc

  func oDB__MODE_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEFT(.w_DBTYPE,5)='SQL20' OR .w_DB__MODE='E')
    endwith
    return bRes
  endfunc

  add object oDB__NAME_4_13 as StdField with uid="ZPGXDQNOBJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DB__NAME", cQueryName = "DB__NAME",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220334725,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=155, Top=287, InputMask=replicate('X',30)

  func oDB__NAME_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFGTYPE='O' and .w_DB__MODE='N')
    endwith
   endif
  endfunc

  func oDB__NAME_4_13.mHide()
    with this.Parent.oContained
      return (LEFT(.w_DBTYPE,5)<>'SQL20' And .w_DBTYPE<>'IBMDB2')
    endwith
  endfunc


  add object oBtn_4_14 as StdButton with uid="ICPDRPRGBH",left=155, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\DIBA20.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per verificare i parametri di connessione";
    , HelpContextID = 208226743;
    , Caption='V\<erifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_14.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"CHECK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_15 as StdButton with uid="IZXBTSLEIG",left=464, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=4;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_15.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_4_16 as StdButton with uid="MUGAITKPGP",left=411, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=4;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_16.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_4_17 as StdButton with uid="KBPWRTXEDU",left=517, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_17.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_4_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_4_18 as StdButton with uid="XPMWRIGDBP",left=570, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_18.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="CFKFHYHCTZ",left=24, top=11, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=4;
    , HelpContextID = 232953818

  add object oStr_4_19 as StdString with uid="CJHXZKDCKR",Visible=.t., Left=28, Top=38,;
    Alignment=0, Width=590, Height=18,;
    Caption="Specificare il datasource da utilizzare per la connessione al database"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="PMXDDJPJCV",Visible=.t., Left=3, Top=116,;
    Alignment=1, Width=145, Height=18,;
    Caption="Data source name:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="KYKFIIXEUD",Visible=.t., Left=17, Top=89,;
    Alignment=1, Width=131, Height=18,;
    Caption="Driver:"  ;
  , bGlobalFont=.t.

  add object oStr_4_25 as StdString with uid="FPXIXZHVTA",Visible=.t., Left=19, Top=263,;
    Alignment=1, Width=130, Height=18,;
    Caption="All'accesso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="CLYBNTEWSR",Visible=.t., Left=19, Top=291,;
    Alignment=1, Width=130, Height=18,;
    Caption="Nome database:"  ;
  , bGlobalFont=.t.

  func oStr_4_26.mHide()
    with this.Parent.oContained
      return (LEFT(.w_DBTYPE,5)<>'SQL20')
    endwith
  endfunc

  add object oStr_4_27 as StdString with uid="IOJWYVXUJU",Visible=.t., Left=17, Top=64,;
    Alignment=1, Width=131, Height=18,;
    Caption="Analizza DSN:"  ;
  , bGlobalFont=.t.

  add object oStr_4_28 as StdString with uid="KQUHXMBTJL",Visible=.t., Left=19, Top=185,;
    Alignment=1, Width=130, Height=18,;
    Caption="Connetti tramite:"  ;
  , bGlobalFont=.t.

  add object oStr_4_29 as StdString with uid="YAFFPJHWSQ",Visible=.t., Left=19, Top=211,;
    Alignment=1, Width=130, Height=18,;
    Caption="Username:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="VIOEBXECCB",Visible=.t., Left=19, Top=240,;
    Alignment=1, Width=130, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oBox_4_24 as StdBox with uid="ZZRFHTKWTJ",left=395, top=323, width=223,height=2
enddefine
define class tgswdbcfgPag5 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLOAD_XDC_5_1 as StdCheck with uid="BIOGSEUVOS",rtseq=25,rtrep=.f.,left=16, top=58, caption="Carica XDC in ingresso",;
    ToolTipText = "Disattivando quest'opzione l'ingresso alla procedura sar� pi� rapido",;
    HelpContextID = 181478137,;
    cFormVar="w_LOAD_XDC", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oLOAD_XDC_5_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oLOAD_XDC_5_1.GetRadio()
    this.Parent.oContained.w_LOAD_XDC = this.RadioValue()
    return .t.
  endfunc

  func oLOAD_XDC_5_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LOAD_XDC==.T.,1,;
      0)
  endfunc

  add object oCHECKUNC_5_2 as StdCheck with uid="RVZMZNMFMS",rtseq=26,rtrep=.f.,left=409, top=58, caption="Effettua controllo su path UNC",;
    ToolTipText = "Si consiglia di lasciare attivata quest'opzione (richiesta mappatura logica cartella installazione)",;
    HelpContextID = 110123881,;
    cFormVar="w_CHECKUNC", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oCHECKUNC_5_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHECKUNC_5_2.GetRadio()
    this.Parent.oContained.w_CHECKUNC = this.RadioValue()
    return .t.
  endfunc

  func oCHECKUNC_5_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHECKUNC==.T.,1,;
      0)
  endfunc

  add object oMSKINGES_5_3 as StdCheck with uid="GPKVPIYUYG",rtseq=27,rtrep=.f.,left=16, top=82, caption="Maschera accesso sempre espansa",;
    ToolTipText = "Abilita/disabilita la possibilit� di aprire la maschera di accesso alla procedura sempre espansa",;
    HelpContextID = 147244825,;
    cFormVar="w_MSKINGES", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oMSKINGES_5_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oMSKINGES_5_3.GetRadio()
    this.Parent.oContained.w_MSKINGES = this.RadioValue()
    return .t.
  endfunc

  func oMSKINGES_5_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MSKINGES==.T.,1,;
      0)
  endfunc

  add object oMASKBCKG_5_4 as StdCheck with uid="QQYMNKGTMF",rtseq=28,rtrep=.f.,left=409, top=82, caption="Utilizzo sfondi per le maschere",;
    ToolTipText = "Abilita/disabilita la possibilit� di utilizzare sfondi per le maschere. Si consiglia di disabilitare il parametro",;
    HelpContextID = 200723187,;
    cFormVar="w_MASKBCKG", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oMASKBCKG_5_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oMASKBCKG_5_4.GetRadio()
    this.Parent.oContained.w_MASKBCKG = this.RadioValue()
    return .t.
  endfunc

  func oMASKBCKG_5_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MASKBCKG==.T.,1,;
      0)
  endfunc

  add object oDISIDXCR_5_5 as StdCheck with uid="KMYNQAMOTE",rtseq=29,rtrep=.f.,left=16, top=106, caption="Disabilita creazione indici",;
    ToolTipText = "Abilita/disabilita la creazione degli indici per le tabelle temporanee",;
    HelpContextID = 153566344,;
    cFormVar="w_DISIDXCR", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oDISIDXCR_5_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oDISIDXCR_5_5.GetRadio()
    this.Parent.oContained.w_DISIDXCR = this.RadioValue()
    return .t.
  endfunc

  func oDISIDXCR_5_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISIDXCR==.T.,1,;
      0)
  endfunc

  add object oPOSTINON_5_6 as StdCheck with uid="TUDOMEVTWT",rtseq=30,rtrep=.f.,left=409, top=106, caption="Disabilito messaggistica Post-IN",;
    ToolTipText = "Disattivando quest'opzione non si possono pi� allegare POST-IN alle tabelle (messaggi Warning, POST-IN integrati)",;
    HelpContextID = 260195140,;
    cFormVar="w_POSTINON", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPOSTINON_5_6.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oPOSTINON_5_6.GetRadio()
    this.Parent.oContained.w_POSTINON = this.RadioValue()
    return .t.
  endfunc

  func oPOSTINON_5_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_POSTINON==.T.,1,;
      0)
  endfunc

  add object oDISIDXIN_5_7 as StdCheck with uid="YWSPJHETIA",rtseq=31,rtrep=.f.,left=16, top=130, caption="Disabilita creazione indici nella generazione dell'inventario",;
    ToolTipText = "Abilita/disabilita la creazione degli indici nell'elaborazione dell'inventario",;
    HelpContextID = 114869116,;
    cFormVar="w_DISIDXIN", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oDISIDXIN_5_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oDISIDXIN_5_7.GetRadio()
    this.Parent.oContained.w_DISIDXIN = this.RadioValue()
    return .t.
  endfunc

  func oDISIDXIN_5_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISIDXIN==.T.,1,;
      0)
  endfunc

  func oDISIDXIN_5_7.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oCIFRA_AS_5_8 as StdCheck with uid="FHHQOPJBZV",rtseq=32,rtrep=.f.,left=409, top=130, caption="Cifratura accesso silente",;
    ToolTipText = "Abilita/disabilita la cifratura per l'accesso silente",;
    HelpContextID = 268397689,;
    cFormVar="w_CIFRA_AS", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oCIFRA_AS_5_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCIFRA_AS_5_8.GetRadio()
    this.Parent.oContained.w_CIFRA_AS = this.RadioValue()
    return .t.
  endfunc

  func oCIFRA_AS_5_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CIFRA_AS==.T.,1,;
      0)
  endfunc

  add object oENMODCFG_5_9 as StdCheck with uid="ROGAUEKILM",rtseq=33,rtrep=.f.,left=16, top=154, caption="Abilita modelli di configurazione gestioni",;
    HelpContextID = 198385267,;
    cFormVar="w_ENMODCFG", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oENMODCFG_5_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oENMODCFG_5_9.GetRadio()
    this.Parent.oContained.w_ENMODCFG = this.RadioValue()
    return .t.
  endfunc

  func oENMODCFG_5_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ENMODCFG==.T.,1,;
      0)
  endfunc

  add object oFLATTOOL_5_10 as StdCheck with uid="RQWEIPWTJE",rtseq=34,rtrep=.f.,left=16, top=205, caption="Toolbars",;
    ToolTipText = "Abilita/disabilita l'utilizzo dell'effetto piatto sulle toolbars",;
    HelpContextID = 19996578,;
    cFormVar="w_FLATTOOL", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLATTOOL_5_10.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oFLATTOOL_5_10.GetRadio()
    this.Parent.oContained.w_FLATTOOL = this.RadioValue()
    return .t.
  endfunc

  func oFLATTOOL_5_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLATTOOL==2,1,;
      0)
  endfunc

  add object oFLATGEST_5_11 as StdCheck with uid="TNKAGYZKWZ",rtseq=35,rtrep=.f.,left=213, top=205, caption="Gestioni",;
    ToolTipText = "Abilita/disabilita l'utilizzo dell'effetto piatto sulle gestioni",;
    HelpContextID = 107028394,;
    cFormVar="w_FLATGEST", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLATGEST_5_11.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oFLATGEST_5_11.GetRadio()
    this.Parent.oContained.w_FLATGEST = this.RadioValue()
    return .t.
  endfunc

  func oFLATGEST_5_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLATGEST==2,1,;
      0)
  endfunc

  add object oFLATPRIN_5_12 as StdCheck with uid="DPXLIXGALC",rtseq=36,rtrep=.f.,left=393, top=205, caption="Printsystem",;
    ToolTipText = "Abilita/disabilita l'utilizzo dell'effetto piatto sulla printsystem",;
    HelpContextID = 202301532,;
    cFormVar="w_FLATPRIN", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLATPRIN_5_12.RadioValue()
    return(iif(this.value =1,2,;
    0))
  endfunc
  func oFLATPRIN_5_12.GetRadio()
    this.Parent.oContained.w_FLATPRIN = this.RadioValue()
    return .t.
  endfunc

  func oFLATPRIN_5_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLATPRIN==2,1,;
      0)
  endfunc

  add object oTERMPRIN_5_13 as StdField with uid="GTEWQXFSBF",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TERMPRIN", cQueryName = "TERMPRIN",;
    bObbl = .f. , nPag = 5, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Indica i caratteri che identificano le stampanti terminal server",;
    HelpContextID = 202692220,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=277, Top=240, InputMask=replicate('X',1)

  add object oAPPTITLE_5_14 as StdField with uid="DWQXAXOGXR",rtseq=38,rtrep=.f.,;
    cFormVar = "w_APPTITLE", cQueryName = "APPTITLE",;
    bObbl = .f. , nPag = 5, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Il testo verr� visualizzato come eventuale titolo aggiuntivo sulla barra dell'applicazione",;
    HelpContextID = 176024757,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=277, Top=269, InputMask=replicate('X',100)


  add object oORACVERS_5_15 as StdCombo with uid="ZLPSNTZFNA",rtseq=39,rtrep=.f.,left=279,top=298,width=213,height=22;
    , ToolTipText = "Permette di impostare la versione di Oracle utilizzata";
    , HelpContextID = 121644601;
    , cFormVar="w_ORACVERS",RowSource=""+"8i,"+"9i o successivi", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oORACVERS_5_15.RadioValue()
    return(iif(this.value =1,'8',;
    iif(this.value =2,'9',;
    space(2))))
  endfunc
  func oORACVERS_5_15.GetRadio()
    this.Parent.oContained.w_ORACVERS = this.RadioValue()
    return .t.
  endfunc

  func oORACVERS_5_15.SetRadio()
    this.Parent.oContained.w_ORACVERS=trim(this.Parent.oContained.w_ORACVERS)
    this.value = ;
      iif(this.Parent.oContained.w_ORACVERS=='8',1,;
      iif(this.Parent.oContained.w_ORACVERS=='9',2,;
      0))
  endfunc

  func oORACVERS_5_15.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'Oracle')
    endwith
  endfunc


  add object oBtn_5_16 as StdButton with uid="TLUTJVVGEW",left=453, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=5;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_16.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_5_17 as StdButton with uid="FXZHUNUMSV",left=400, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=5;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_17.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_5_18 as StdButton with uid="AZWBHTPIRX",left=506, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=5;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_18.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_5_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_5_19 as StdButton with uid="QEUPHKVQTX",left=559, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_19.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="YBVUIREWSR",left=13, top=11, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=5;
    , HelpContextID = 232953818

  add object oStr_5_20 as StdString with uid="LJKHXHDSZM",Visible=.t., Left=17, Top=38,;
    Alignment=0, Width=567, Height=18,;
    Caption="Specificare le impostazioni da inserire nel file di configurazione"  ;
  , bGlobalFont=.t.

  add object oStr_5_22 as StdString with uid="ZXBPEIFRFZ",Visible=.t., Left=12, Top=242,;
    Alignment=1, Width=260, Height=18,;
    Caption="Identificatore stampanti terminal server:"  ;
  , bGlobalFont=.t.

  add object oStr_5_23 as StdString with uid="NWUCKOSMNK",Visible=.t., Left=17, Top=185,;
    Alignment=0, Width=260, Height=18,;
    Caption="Applica effetto piatto dei bottoni a"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="TWUXJRZDDD",Visible=.t., Left=12, Top=271,;
    Alignment=1, Width=260, Height=18,;
    Caption="Titolo applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="OSRWFPQVUC",Visible=.t., Left=12, Top=298,;
    Alignment=1, Width=260, Height=18,;
    Caption="Versione di Oracle utilizzata:"  ;
  , bGlobalFont=.t.

  func oStr_5_30.mHide()
    with this.Parent.oContained
      return (.w_DBTYPE<>'Oracle')
    endwith
  endfunc

  add object oBox_5_24 as StdBox with uid="ZPIBIMWTED",left=11, top=36, width=649,height=144

  add object oBox_5_25 as StdBox with uid="IJCXIYLYYX",left=12, top=54, width=646,height=2

  add object oBox_5_26 as StdBox with uid="RUJEPQIQPO",left=11, top=184, width=649,height=51

  add object oBox_5_27 as StdBox with uid="NWHKNJPSQB",left=12, top=202, width=646,height=2

  add object oBox_5_29 as StdBox with uid="LVAFXCMXRN",left=391, top=323, width=223,height=2
enddefine
define class tgswdbcfgPag6 as StdContainer
  Width  = 661
  height = 384
  stdWidth  = 661
  stdheight = 384
  resizeXpos=245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_6_1 as StdButton with uid="GQHIIYYKFA",left=453, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=6;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 223168774;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_1.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_6_2 as StdButton with uid="IWBJGXMPRW",left=506, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=6;
    , ToolTipText = "Conferma i parametri e accede alla procedura";
    , HelpContextID = 57078042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_2.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  func oBtn_6_2.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CUR_STEP<.w_TOT_STEP)
     endwith
    endif
  endfunc


  add object oBtn_6_3 as StdButton with uid="TETDEFZTJF",left=400, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=6;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 123236853;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_3.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_6_4 as StdButton with uid="WKTZAXAJFS",left=559, top=330, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 265621766;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_4.Click()
      with this.Parent.oContained
        gsbdbcfg(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMSG_6_5 as StdMemo with uid="FTUVBAYCJH",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 56785210,;
    FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=230, Width=596, Left=17, Top=86, readonly=.T. , BorderStyle=0, BackStyle=0, ScrollBars=0, TabStop=.F.


  add object STEPS as cp_calclbl with uid="SQMIVPMHYS",left=13, top=11, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=6;
    , HelpContextID = 232953818

  add object oStr_6_6 as StdString with uid="FRQJCBPEWP",Visible=.t., Left=17, Top=38,;
    Alignment=0, Width=600, Height=18,;
    Caption="Tutte le impostazioni necessarie all'accesso e alla configurazione della procedura sono state indicate."  ;
  , bGlobalFont=.t.

  add object oStr_6_7 as StdString with uid="ZWGVYXPDPN",Visible=.t., Left=17, Top=57,;
    Alignment=0, Width=600, Height=18,;
    Caption="Premere Ok per salvare il file di configurazione ed entrare nella procedura."  ;
  , bGlobalFont=.t.

  add object oBox_6_9 as StdBox with uid="QBDPTQDBTI",left=391, top=323, width=223,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gswdbcfg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gswdbcfg
Define Class StdTableComboODBC_DRV As StdTableCombo
	* Ridefinita classe per recuperare i driver ODBC installati
	* sulla macchina
	Proc Init
		* --- Zucchetti Aulla - Inizio Interfaccia
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		If Vartype(This.bNoBackColor)='U'
			This.BackColor=i_nEBackColor
		Endif

		* --- Zucchetti Aulla - Fine Interfaccia
		This.ToolTipText=cp_Translate(This.ToolTipText)
		This.StatusBarText=Padr(This.ToolTipText,100)
		Local l_numf, l_i
		#Define HKLM 0x80000002 && HKEY_LOCAL_MACHINE

		Local oWMILocator, oWMI, oStdRegProv, cComputer, nLastError, cLastError
		cComputer = "."
		oStdRegProv = .Null.

		nLastError = 0
		cLastError = ""

		* Open root\default namespace to get registry provider
		oWMILocator = Createobject("WbemScripting.SWbemLocator")
		oWMI = oWMILocator.ConnectServer(cComputer, "root\default")
		* We are getting a class, *not* an instance.
		oStdRegProv = oWMI.Get("StdRegProv")

		* We need to use COMARRAY to change array behaviour to
		* zero-based arrays passed by reference.
		* Without this, the REG_BINARY and REG_MULTI_SZ types won't work.
		Comarray(oStdRegProv, 10)

		Local cKey, nHive
		Local nValue, cValue, aValues [1]
		Local cName, cDir, cFXP, nCount, cString, oByte
		Local lWin9x, nType, lWriteAccess, SeekResult
		Local oParentObject
		oParentObject=This.Parent.Parent.Parent.Parent
		nHive = HKLM
		cKey = "SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"
		Local aValues[1], aTypes[1], aKeys[1]
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		If oStdRegProv.EnumValues(nHive, cKey, @aValues, @aTypes)=0
			Dimension This.combovalues(1)
			This.combovalues(1)=""
			For nCount = 1 To Alen(aValues)
				cName = aValues[nCount]
				nType = aTypes[nCount]
				vValue = .Null.
				PosEnd=At(" ",cName,2)
				ChkStr=""
				If PosEnd>0
					ChkStr=Left(cName, PosEnd-1)
				Endif
				If cName="SQL Server" Or cName="SQL Native" Or cName="PostgreSQL" Or cName="ODBC Driver 11 for SQL Server"  Or cName="ODBC Driver 13 for SQL Server"  Then
					ChkStr=cName
				Endif
				SeekResult = Ascan(oParentObject.w_DbEngines, ChkStr,1,-1,4,15)
				If SeekResult >0
					If Ascan(This.combovalues,oParentObject.w_DbEngines(SeekResult,6),1,-1,1,15)=0
						If !Empty(This.combovalues(1))
							Dimension This.combovalues(Alen(This.combovalues)+1)
						Endif
						This.combovalues(Alen(This.combovalues))= oParentObject.w_DbEngines(SeekResult,6)
						This.AddItem(oParentObject.w_DbEngines(SeekResult,1))
						oParentObject.w_LastODBCDRV=oParentObject.w_DbEngines(SeekResult,6)
					Endif
				Endif
			Next
		Endif
		This.nValues = Iif(Vartype(This.combovalues)='L', 0, Alen(This.combovalues))
		This.SetFont()
		On Error &olderr
	Endproc
Enddefine


Define Class StdTableComboSELODBC_DRV As StdTableCombo
	* Ridefinita classe per recuperare i driver ODBC installati
	* sulla macchina filtrati per tipo database selezionato
	Proc Init
		* --- Zucchetti Aulla - Inizio Interfaccia
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		If Vartype(This.bNoBackColor)='U'
			This.BackColor=i_nEBackColor
		Endif
		* --- Zucchetti Aulla - Fine Interfaccia
		This.Clear()
		This.ToolTipText=cp_Translate(This.ToolTipText)
		This.StatusBarText=Padr(This.ToolTipText,100)
		Local l_numf, l_i

		#Define HKLM 0x80000002 && HKEY_LOCAL_MACHINE

		Local oWMILocator, oWMI, oStdRegProv, cComputer, nLastError, cLastError
		cComputer = "."
		oStdRegProv = .Null.

		nLastError = 0
		cLastError = ""

		* Open root\default namespace to get registry provider
		oWMILocator = Createobject("WbemScripting.SWbemLocator")
		oWMI = oWMILocator.ConnectServer(cComputer, "root\default")
		* We are getting a class, *not* an instance.
		oStdRegProv = oWMI.Get("StdRegProv")

		* We need to use COMARRAY to change array behaviour to
		* zero-based arrays passed by reference.
		* Without this, the REG_BINARY and REG_MULTI_SZ types won't work.
		Comarray(oStdRegProv, 10)

		Local cKey, nHive
		Local nValue, cValue, aValues [1]
		Local cName, cDir, cFXP, nCount, cString, oByte
		Local lWin9x, nType, lWriteAccess, SeekResult
		Local oParentObject
		oParentObject=This.Parent.Parent.Parent.Parent
		nHive = HKLM
		cKey = "SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"
		Local aValues[1], aTypes[1], aKeys[1]
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		If oStdRegProv.EnumValues(nHive, cKey, @aValues, @aTypes)=0
			Dimension This.combovalues(1)
			This.combovalues(1)=""
			Local oDBSel, sDbSel, DbIdx
			sDbSel = oParentObject.w_DBTYPE
			DbIdx = Ascan(oParentObject.w_DbEngines, sDbSel,1,-1,6,15)
			If DbIdx>0
				For nCount = 1 To Alen(aValues)
					cName = aValues[nCount]
					nType = aTypes[nCount]
					vValue = .Null.
					PosEnd=At(" ",cName,2)
					ChkStr=""
					If PosEnd>0
						ChkStr=Left(cName, PosEnd-1)
					Endif
					If cName="SQL Server" Or cName="SQL Native" Or cName="PostgreSQL" Or cName="ODBC Driver 11 for SQL Server"  Or cName="ODBC Driver 13 for SQL Server"  Then
						ChkStr=cName
					Endif
					If oParentObject.w_DbEngines(DbIdx,4)==ChkStr
						If !Empty(This.combovalues(1))
							Dimension This.combovalues(Alen(This.combovalues)+1)
						Endif
						This.combovalues(Alen(This.combovalues))= cName
						This.AddItem(cName)
					Endif
				Next
			Endif
		Endif
		This.nValues = Iif(Vartype(This.combovalues)='L', 0, Alen(This.combovalues))
		This.SetFont()
		This.Value=1
		On Error &olderr
	Endproc
Enddefine

Define Class StdTableComboODBC As StdTableCombo
	* Ridefinita classe per recuperare gli ODBC definiti
	* sulla macchina filtrati per tipo database selezionato
	Proc Init
		* --- Zucchetti Aulla - Inizio Interfaccia
		This.SpecialEffect = i_nSpecialEffect
		This.BorderColor = i_nBorderColor
		If Vartype(This.bNoBackColor)='U'
			This.BackColor=i_nEBackColor
		Endif
		* --- Zucchetti Aulla - Fine Interfaccia
		This.ToolTipText=cp_Translate(This.ToolTipText)
		This.StatusBarText=Padr(This.ToolTipText,100)
		Local l_numf, l_i

		#Define HKLM 0x80000002 && HKEY_LOCAL_MACHINE
		#Define HKCU 0x80000001 && HKEY_CURRENT_USER
		Local oWMILocator, oWMI, cComputer, nLastError, cLastError
		cComputer = "."
		oStdRegProv = .Null.

		nLastError = 0
		cLastError = ""
		This.Clear()
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		* Open root\default namespace to get registry provider
		oWMILocator = Createobject("WbemScripting.SWbemLocator")
		oWMI = oWMILocator.ConnectServer(cComputer, "root\default")
		* We are getting a class, *not* an instance.
		oStdRegProv = oWMI.Get("StdRegProv")

		* We need to use COMARRAY to change array behaviour to
		* zero-based arrays passed by reference.
		* Without this, the REG_BINARY and REG_MULTI_SZ types won't work.
		Comarray(oStdRegProv, 10)
		Local cKey, nHive
		Local nValue, cValue, aValues [1]
		Local cName, cDir, cFXP, nCount, cString, oByte
		Local lWin9x, nType, lWriteAccess
		Local oParentObject
		oParentObject=This.Parent.Parent.Parent.Parent
		nHive = Iif(oParentObject.w_DSNTYPE='HKCU', HKCU, HKLM )
		cKey = "SOFTWARE\ODBC\ODBC.INI"
		Dimension This.combovalues(1)
		This.combovalues(1)=.F.
		Dimension oParentObject.w_ODBCList(1,7)
		oParentObject.w_ODBCList=.F.
		Local oDBSel, sDbSel
		sDbSel = oParentObject.w_DBTYPE
		If !Empty(Nvl(sDbSel, ' '))
			This.EnumerateRegistry(nHive, cKey, 0, sDbSel)
		Endif
		This.nValues = Iif(Vartype(This.combovalues)='L', 0, Alen(This.combovalues))
		This.SetFont()
		On Error &olderr
	Endproc

	* procedure per accesso al registry per recuperare i dati degli ODBC
	Procedure EnumerateRegistry
		Lparameters nHive, cKey, nLevel, sDbSel
		Local nCount, cName, nType, cType, vValue
		Local aValues[1], aTypes[1], aKeys[1]
		Local NomeDB, NomeDrv, UsedID, NomeServer, ConnTrusted, Descrizione
		Local oParentObject
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		oParentObject=This.Parent.Parent.Parent.Parent
		ah_msg("Analisi ODBC %1",.T.,.T.,0,Alltrim(cKey))
		oStdRegProv.EnumValues(nHive, cKey, @aValues, @aTypes)
		If Alen(aValues)>0 And !Empty(Nvl(aValues(1), ' '))
			For nCount = 1 To Alen(aValues)
				cName = aValues[nCount]
				nType = aTypes[nCount]
				cType = This.GetValueType(nType)
				vValue = .Null.
				If This.GetValue(nHive, cKey, cName, nType, @vValue)
					Do Case
						Case cName="Database"
							NomeDB = vValue
						Case cName="Driver"
							NomeDrv = vValue
						Case cName="LastUser" Or cName="UserID"
							UsedID = vValue
						Case cName=="Server" Or cName=="ServerName"
							NomeServer = vValue
						Case cName="Trusted_Connection"
							ConnTrusted = vValue
						Case cName="Description"
							Descrizione = vValue
					Endcase
				Endif
			Next
			If Vartype(NomeDrv)='C'
				SeekResult = Ascan(oParentObject.w_DbEngines, sDbSel,1,-1,6,15)
				If SeekResult>0 And oParentObject.w_DbEngines(SeekResult,5)=Upper(Justfname(NomeDrv))
					If !Empty(This.combovalues(1))
						Dimension This.combovalues(Alen(This.combovalues)+1)
					Endif
					NomeDB = Iif(Vartype(NomeDB)='C', NomeDB, ah_msgformat("<Non disponibile>"))
					NomeDrv = Iif(Vartype(NomeDrv)='C', NomeDrv, ah_msgformat("<Non disponibile>"))
					UsedID = Iif(Vartype(UsedID)='C', UsedID, ah_msgformat("<Non disponibile>"))
					NomeServer = Iif(Vartype(NomeServer)='C', NomeServer, ah_msgformat("<Non disponibile>"))
					ConnTrusted = Iif(Vartype(ConnTrusted)='C', ConnTrusted, ah_msgformat("<Non disponibile>"))
					Descrizione = Iif(Vartype(Descrizione)='C', Descrizione, ah_msgformat("<Non disponibile>"))
					If Vartype(oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),3))='C' And !Empty(oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),3))
						Dimension oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1)+1,7)
					Endif
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),1)=Justfname(cKey)
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),2)=NomeDB
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),3)=NomeDrv
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),4)=UsedID
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),5)=NomeServer
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),6)=ConnTrusted
					oParentObject.w_ODBCList(Alen(oParentObject.w_ODBCList,1),7)=Descrizione
					This.combovalues(Alen(This.combovalues))= Justfname(cKey)
					This.AddItem(Justfname(cKey))
				Endif
			Endif
		Endif
		oStdRegProv.EnumKey(nHive, cKey, @aKeys)
		If Alen(aKeys)>0 And !Empty(Nvl(aKeys(1), ' '))
			For nCount = 1 To Alen(aKeys)
				This.EnumerateRegistry(nHive, cKey + "\" + aKeys[nCount], nLevel + 1, sDbSel)
			Endfor
		Endif
		Wait Clear
		On Error &olderr
	Endproc

	Procedure GetValue
		Lparameters nHive, cKey, cName, nType, vReturnValue
		* vReturnValue must be passed by reference for array types

		Local cValue, nValue, aValues[1], nResult, lSuccess
		lSuccess = .F.
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		Do Case
			Case nType = 0 && REG_NONE
				* Not supported
				vReturnValue = .F.
			Case nType = 1 && REG_SZ
				cValue = Space(0)
				nResult = oStdRegProv.GetStringValue(nHive, cKey, cName, @cValue)
				If nResult = 0
					vReturnValue = cValue
					lSuccess = .T.
				Else
					This.ProcessError(nResult, "GetStringValue")
				Endif
			Case nType = 2 && REG_EXPAND_SZ
				cValue = Space(0)
				nResult = oStdRegProv.GetExpandedStringValue(nHive, cKey, cName, @cValue)
				If nResult = 0
					vReturnValue = cValue
					lSuccess = .T.
				Else
					This.ProcessError(nResult, "GetExpandedStringValue")
				Endif
			Case nType = 3 && REG_BINARY
				* The registry has an array of hex values (max value FF), we get an array of
				* decimal values.
				nResult = oStdRegProv.GetBinaryValue(nHive, cKey, cName, @aValues)
				If nResult = 0 And Alen(aValues) # 0
					Dimension vReturnValue[alen(aValues)]
					Acopy(aValues, vReturnValue)
					lSuccess = .T.
				Else
					This.ProcessError(nResult, "GetBinaryValue")

				Endif
			Case nType = 4 && REG_DWORD
				nValue = 0
				nResult = oStdRegProv.GetDWORDValue(nHive, cKey, cName, @nValue)
				If nResult = 0
					vReturnValue = nValue
					lSuccess = .T.
				Else
					This.ProcessError(nResult, "GetDWORDValue")
				Endif
			Case nType = 7 && REG_MULTI_SZ
				nResult = oStdRegProv.GetMultiStringValue(nHive, cKey, cName, @aValues)
				If nResult = 0 And Alen(aValues) # 0
					Dimension vReturnValue[alen(aValues)]
					Acopy(aValues, vReturnValue)
					lSuccess = .T.
				Else
					This.ProcessError(nResult, "GetMultiStringValue")
				Endif
			Otherwise
				* The other types aren't supported by these methods
				vReturnValue = .F.
		Endcase
		On Error &olderr
		Return lSuccess
	Endproc


	Procedure GetValueType
		Lparameters nType
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		Local cType
		Do Case
			Case nType = 0
				* No type. Consists of hex data.
				cType = "REG_NONE"
			Case nType = 1
				* String
				cType = "REG_SZ"
			Case nType = 2
				* Expanded string (expands environment variables)
				cType = "REG_EXPAND_SZ"
			Case nType = 3
				* Array of binary values
				cType = "REG_BINARY"
			Case nType = 4
				* A double word (32 bit value)
				cType = "REG_DWORD"
			Case nType = 5
				* Big endian double word
				cType = "REG_DWORD_BIG_ENDIAN"
			Case nType = 6
				* Symbolic link
				cType = "REG_LINK"
			Case nType = 7
				* Array of strings
				cType = "REG_MULTI_SZ"
			Case nType = 8
				* Resource list in the resource map.
				cType = "REG_RESOURCE_LIST"
			Case nType = 9
				* The resource list in the hardware description key
				cType = "REG_FULL_RESOURCE_DESCRIPTOR"
			Case nType = 10
				* As it says.
				cType = "REG_RESOURCE_REQUIREMENTS_LIST"
			Case nType = 11
				* 64 Bit
				cType = "REG_QWORD"
		Endcase
		On Error &olderr
		Return cType
	Endproc


	Procedure EnumKey
		Lparameters nHive, cKey, aKeys
		* Pass aKeys by reference
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		Local nResult
		nResult = This.oStdRegProv.EnumKey(nHive, cKey, @aKeys)
		If nResult # 0
			* Call failed.
			Dimension aKeys[1]
			aKeys = .Null.
			This.ProcessError(nResult, "EnumKey")
		Endif
		On Error &olderr
		Return Not Isnull(aKeys)
	Endproc


	Procedure EnumValues
		Lparameters nHive, cKey, aValues, aTypes
		* Pass aValues and aTypes by reference
		olderr = On("ERROR")
		err = .F.
		On Error err=.T.
		Local nResult
		nResult = This.oStdRegProv.EnumValues(nHive, cKey, @aValues, @aTypes)
		If nResult # 0
			* Call failed.
			Dimension aValues[1], aTypes[1]
			Store .Null. To aValues, aTypes
			This.ProcessError(nResult, "EnumValues")
		Endif
		On Error &olderr
		Return Not Isnull(aValues)
	Endproc

Enddefine

procedure GetDirPostgres(pParent)
    If !("vfp2c32"$Lower(Set("Library")))
      SET LIBRARY TO vfp2c32.fll ADDITIVE
    EndIf
    LOCAL cStr,cPath
    cPath=' '
    ARegistryKeys("aRegServicesKey", 0x80000002, "SYSTEM\CurrentControlSet\Services")
    For each cStr in aRegServicesKey
    If Lower(Left(cStr, 10)) == "postgresql"
       Exit
    EndIf
    Next
   vbs=CREATEOBJECT("WScript.Shell")
   cPath = vbs.RegRead("HKLM\SYSTEM\CurrentControlSet\Services\"+m.cStr+"\ImagePath")
   cPath = alltrim(CHRTRAN(cPath,'"',''))
   cPath = ADDBS(SUBSTR(cPath,1,AT('bin',cPath)+2))     
   return(cPath)
endproc
* --- Fine Area Manuale
