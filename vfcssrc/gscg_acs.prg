* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_acs                                                        *
*              Causali contributo INPS                                         *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_22]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-26                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_acs"))

* --- Class definition
define class tgscg_acs as StdForm
  Top    = 55
  Left   = 125

  * --- Standard Properties
  Width  = 544
  Height = 143+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-03"
  HelpContextID=109716841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CAU_INPS_IDX = 0
  cFile = "CAU_INPS"
  cKeySelect = "CS_CAUSA"
  cKeyWhere  = "CS_CAUSA=this.w_CS_CAUSA"
  cKeyWhereODBC = '"CS_CAUSA="+cp_ToStrODBC(this.w_CS_CAUSA)';

  cKeyWhereODBCqualified = '"CAU_INPS.CS_CAUSA="+cp_ToStrODBC(this.w_CS_CAUSA)';

  cPrg = "gscg_acs"
  cComment = "Causali contributo INPS"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CS_CAUSA = space(5)
  w_CSDESCRI = space(60)
  w_CSPERINI = space(40)
  w_CSPERFIN = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_INPS','gscg_acs')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_acsPag1","gscg_acs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causali INPS")
      .Pages(1).HelpContextID = 61283626
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCS_CAUSA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAU_INPS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_INPS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_INPS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CS_CAUSA = NVL(CS_CAUSA,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_INPS where CS_CAUSA=KeySet.CS_CAUSA
    *
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_INPS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_INPS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_INPS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CS_CAUSA',this.w_CS_CAUSA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CS_CAUSA = NVL(CS_CAUSA,space(5))
        .w_CSDESCRI = NVL(CSDESCRI,space(60))
        .w_CSPERINI = NVL(CSPERINI,space(40))
        .w_CSPERFIN = NVL(CSPERFIN,space(40))
        cp_LoadRecExtFlds(this,'CAU_INPS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CS_CAUSA = space(5)
      .w_CSDESCRI = space(60)
      .w_CSPERINI = space(40)
      .w_CSPERFIN = space(40)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_INPS')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCS_CAUSA_1_1.enabled = i_bVal
      .Page1.oPag.oCSDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCSPERINI_1_4.enabled = i_bVal
      .Page1.oPag.oCSPERFIN_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCS_CAUSA_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCS_CAUSA_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAU_INPS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CS_CAUSA,"CS_CAUSA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSDESCRI,"CSDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSPERINI,"CSPERINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSPERFIN,"CSPERFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    i_lTable = "CAU_INPS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_INPS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SCS with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_INPS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_INPS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_INPS')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_INPS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CS_CAUSA,CSDESCRI,CSPERINI,CSPERFIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CS_CAUSA)+;
                  ","+cp_ToStrODBC(this.w_CSDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CSPERINI)+;
                  ","+cp_ToStrODBC(this.w_CSPERFIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_INPS')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_INPS')
        cp_CheckDeletedKey(i_cTable,0,'CS_CAUSA',this.w_CS_CAUSA)
        INSERT INTO (i_cTable);
              (CS_CAUSA,CSDESCRI,CSPERINI,CSPERFIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CS_CAUSA;
                  ,this.w_CSDESCRI;
                  ,this.w_CSPERINI;
                  ,this.w_CSPERFIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_INPS_IDX,i_nConn)
      *
      * update CAU_INPS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_INPS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CSDESCRI="+cp_ToStrODBC(this.w_CSDESCRI)+;
             ",CSPERINI="+cp_ToStrODBC(this.w_CSPERINI)+;
             ",CSPERFIN="+cp_ToStrODBC(this.w_CSPERFIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_INPS')
        i_cWhere = cp_PKFox(i_cTable  ,'CS_CAUSA',this.w_CS_CAUSA  )
        UPDATE (i_cTable) SET;
              CSDESCRI=this.w_CSDESCRI;
             ,CSPERINI=this.w_CSPERINI;
             ,CSPERFIN=this.w_CSPERFIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_INPS_IDX,i_nConn)
      *
      * delete CAU_INPS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CS_CAUSA',this.w_CS_CAUSA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCS_CAUSA_1_1.value==this.w_CS_CAUSA)
      this.oPgFrm.Page1.oPag.oCS_CAUSA_1_1.value=this.w_CS_CAUSA
    endif
    if not(this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value==this.w_CSDESCRI)
      this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value=this.w_CSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCSPERINI_1_4.value==this.w_CSPERINI)
      this.oPgFrm.Page1.oPag.oCSPERINI_1_4.value=this.w_CSPERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCSPERFIN_1_5.value==this.w_CSPERFIN)
      this.oPgFrm.Page1.oPag.oCSPERFIN_1_5.value=this.w_CSPERFIN
    endif
    cp_SetControlsValueExtFlds(this,'CAU_INPS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_acsPag1 as StdContainer
  Width  = 540
  height = 143
  stdWidth  = 540
  stdheight = 143
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCS_CAUSA_1_1 as StdField with uid="DQYFPQUJIT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CS_CAUSA", cQueryName = "CS_CAUSA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale",;
    HelpContextID = 47129191,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=44, Left=107, Top=18, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oCSDESCRI_1_2 as StdField with uid="FJLCADHLOD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CSDESCRI", cQueryName = "CSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 32469615,;
   bGlobalFont=.t.,;
    Height=21, Width=429, Left=107, Top=44, InputMask=replicate('X',60)

  add object oCSPERINI_1_4 as StdField with uid="RKXHIQOCGA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CSPERINI", cQueryName = "CSPERINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Tipo inizio periodo riferimento",;
    HelpContextID = 136301969,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=152, Top=80, InputMask=replicate('X',40)

  add object oCSPERFIN_1_5 as StdField with uid="OPMVMXGKHA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CSPERFIN", cQueryName = "CSPERFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Tipo fine periodo riferimento",;
    HelpContextID = 81801844,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=152, Top=105, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="AHRODTISOS",Visible=.t., Left=24, Top=44,;
    Alignment=1, Width=80, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VSUFHCXQLI",Visible=.t., Left=4, Top=80,;
    Alignment=1, Width=145, Height=15,;
    Caption="Tipo periodo iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="JSEQBPSWNB",Visible=.t., Left=4, Top=105,;
    Alignment=1, Width=145, Height=15,;
    Caption="Tipo periodo finale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HFQVHWZFUG",Visible=.t., Left=24, Top=20,;
    Alignment=1, Width=80, Height=15,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_acs','CAU_INPS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CS_CAUSA=CAU_INPS.CS_CAUSA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
