* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_blu                                                        *
*              Lancia bottoni ultimi prezzi/costi                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-04                                                      *
* Last revis.: 2011-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_blu",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_blu as StdBatch
  * --- Local variables
  pOPER = space(2)
  w_PADRE = .NULL.
  w_MVCODCON = space(15)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_DECUNI = 0
  w_MVCODICE = space(20)
  w_MVCODART = space(20)
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia il bottone utlimi prezzi/costi da:
    *     GSVE_MDV, GSAC_MDV, GSOR_MDV
    *     GSOF_MDO, GSVE_KIU
    * --- Da GSVE_MDV, GSAC_MDV, GSOR_MDV per il bottone U
    * --- Da GSOF_MDO
    * --- DA GSPS_MVD
    * --- DA GSMA_MVM
    * --- Da GSVE_KIU
    this.w_PADRE = This.OparentObject
    do case
      case left(this.pOPER,1)$"VA"
        * --- Da documenti
        * --- La variabile MVCODCON non sar� valorizzata con il campo MVCODCON 
        *     dei documenti ma con la variabile XCONORN la quale contiene o l'intestatario 
        *     o il Per Conto Di nel caso in cui questo sia valorizzato sul documento e sia attiva
        *     la gestione Per conto di in Azienda.
        this.w_MVCODCON = this.w_PADRE.w_XCONORN
        this.w_MVDATDOC = this.w_PADRE.w_MVDATDOC
        this.w_MVCODVAL = this.w_PADRE.w_MVCODVAL
        this.w_MVCAOVAL = this.w_PADRE.w_MVCAOVAL
        this.w_DECUNI = this.w_PADRE.w_DECUNI
        this.w_MVCODICE = this.w_PADRE.w_MVCODICE
        this.w_MVCODART = this.w_PADRE.w_MVCODART
        if RIGHT(this.pOPER,1)="U"
          if left(this.pOPER,1)="V"
            do GSAR_KUP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            do GSAR_KUF with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          do GSAR_KUC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case left(this.pOPER,1)="O"
        * --- lanciato da documento offerta
        this.w_MVCODICE = this.oParentObject.w_ODCODICE
        this.w_MVDATDOC = this.oParentObject.w_DATDOC
        this.w_MVCODVAL = this.oParentObject.w_CODVAL
        this.w_MVCODCON = this.oParentObject.w_CODCLI
        this.w_DECUNI = this.w_PADRE.w_DECUNI
        this.w_MVCODART = this.oParentObject.w_ODCODART
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL,I_DATSYS)
        if this.pOPER="OU"
          do GSAR_KUP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GSAR_KUC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case left(this.pOPER,1)="G"
        * --- lanciato da P.O.S.
        this.w_MVCODICE = this.oParentObject.w_MDCODICE
        this.w_MVDATDOC = this.oParentObject.w_MDDATREG
        this.w_MVCODVAL = this.oParentObject.w_MDCODVAL
        this.w_MVCODCON = this.oParentObject.w_MDCODCLI
        this.w_DECUNI = this.w_PADRE.w_DECUNI
        this.w_MVCODART = this.oParentObject.w_MDCODART
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL,I_DATSYS)
        if this.pOPER="GU"
          do GSAR_KUP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GSAR_KUC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case left(this.pOPER,1)="M"
        * --- lanciato da Movimenti Magazzino
        this.w_MVCODICE = this.oParentObject.w_MMCODICE
        this.w_MVDATDOC = this.oParentObject.w_MMDATREG
        this.w_MVCODVAL = this.oParentObject.w_MMCODVAL
        this.w_MVCODCON = this.oParentObject.w_MMCODCON
        this.w_DECUNI = this.w_PADRE.w_DECUNI
        this.w_MVCODART = this.oParentObject.w_MMCODART
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL,I_DATSYS)
        if this.pOPER="MU"
          if this.oParentObject.w_MMTIPCON="C"
            do GSAR_KUP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            do GSAR_KUF with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          do GSAR_KUC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case left(this.pOPER,1)="U"
        * --- lanciato da verifica prezzi con U.C.A.
        this.w_MVCODICE = this.oParentObject.w_LICODICE
        this.w_MVDATDOC = this.oParentObject.w_LIDATDOC
        this.w_MVCODVAL = this.oParentObject.w_LICODVAL
        this.w_MVCODCON = this.oParentObject.w_LICODCON
        this.w_DECUNI = 5
        this.w_MVCODART = this.oParentObject.w_ARCODART
        this.w_MVCAOVAL = this.oParentObject.w_LICAOVAL
        if this.pOPER="UU"
          do GSAR_KUP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GSAR_KUC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
