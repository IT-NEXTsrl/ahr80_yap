* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kcg                                                        *
*              Salva configurazione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_31]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-20                                                      *
* Last revis.: 2009-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kcg",oParentObject))

* --- Class definition
define class tgsut_kcg as StdForm
  Top    = 8
  Left   = 14

  * --- Standard Properties
  Width  = 543
  Height = 446
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-03-02"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  CFG_GEST_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsut_kcg"
  cComment = "Salva configurazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DESCFG = space(50)
  w_TIPCFG = space(1)
  o_TIPCFG = space(1)
  w_UTEGRP = 0
  w_UTEGRP = 0
  w_DESCRI = space(20)
  w_CODAZI = space(5)
  w_MODSAV = space(1)
  w_DEFCFG = space(1)
  w_ROWNUM = 0
  w_GSNOME = space(30)
  w_OLDCFG = space(50)
  w_RAGAZI = space(40)
  w_zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kcgPag1","gsut_kcg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDESCFG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoom = this.oPgFrm.Pages(1).oPag.zoom
    DoDefault()
    proc Destroy()
      this.w_zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='CFG_GEST'
    this.cWorkTables[4]='AZIENDA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        Salva_DblClick(this,"N")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DESCFG=space(50)
      .w_TIPCFG=space(1)
      .w_UTEGRP=0
      .w_UTEGRP=0
      .w_DESCRI=space(20)
      .w_CODAZI=space(5)
      .w_MODSAV=space(1)
      .w_DEFCFG=space(1)
      .w_ROWNUM=0
      .w_GSNOME=space(30)
      .w_OLDCFG=space(50)
      .w_RAGAZI=space(40)
      .w_GSNOME=oParentObject.w_GSNOME
      .oPgFrm.Page1.oPag.zoom.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_TIPCFG = 'I'
        .w_UTEGRP = IIF(.w_TIPCFG='U',.w_UTEGRP, 0)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_UTEGRP))
          .link_1_9('Full')
        endif
        .w_UTEGRP = IIF(.w_TIPCFG='G',.w_UTEGRP, 0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_UTEGRP))
          .link_1_10('Full')
        endif
        .w_DESCRI = IIF(.w_TIPCFG='I', 'Installazione', .w_DESCRI)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODAZI))
          .link_1_12('Full')
        endif
        .w_MODSAV = 'N'
        .w_DEFCFG = 'N'
        .w_ROWNUM = .w_ZOOM.GetVar("CPROWNUM")
          .DoRTCalc(10,10,.f.)
        .w_OLDCFG = .w_zoom.GetVar("CGDESCFG")
    endwith
    this.DoRTCalc(12,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_GSNOME=.w_GSNOME
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.zoom.Calculate()
        .DoRTCalc(1,2,.t.)
        if .o_TIPCFG<>.w_TIPCFG
            .w_UTEGRP = IIF(.w_TIPCFG='U',.w_UTEGRP, 0)
          .link_1_9('Full')
        endif
        if .o_TIPCFG<>.w_TIPCFG
            .w_UTEGRP = IIF(.w_TIPCFG='G',.w_UTEGRP, 0)
          .link_1_10('Full')
        endif
        if .o_TIPCFG<>.w_TIPCFG
            .w_DESCRI = IIF(.w_TIPCFG='I', 'Installazione', .w_DESCRI)
        endif
        .DoRTCalc(6,8,.t.)
            .w_ROWNUM = .w_ZOOM.GetVar("CPROWNUM")
        .DoRTCalc(10,10,.t.)
            .w_OLDCFG = .w_zoom.GetVar("CGDESCFG")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoom.Calculate()
    endwith
  return

  proc Calculate_OJNHDJMJQR()
    with this
          * --- Dbl click salva configurazione
          Salva_DblClick(this;
              ,'O';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oUTEGRP_1_9.visible=!this.oPgFrm.Page1.oPag.oUTEGRP_1_9.mHide()
    this.oPgFrm.Page1.oPag.oUTEGRP_1_10.visible=!this.oPgFrm.Page1.oPag.oUTEGRP_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.zoom.Event(cEvent)
        if lower(cEvent)==lower("w_zoom selected")
          .Calculate_OJNHDJMJQR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTEGRP
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTEGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTEGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTEGRP) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oUTEGRP_1_9'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTEGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTEGRP = NVL(_Link_.CODE,0)
      this.w_DESCRI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTEGRP = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTEGRP
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTEGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTEGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTEGRP) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oUTEGRP_1_10'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTEGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTEGRP = NVL(_Link_.CODE,0)
      this.w_DESCRI = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTEGRP = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_CODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_CODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oCODAZI_1_12'),i_cWhere,'',"Aziende",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESCFG_1_2.value==this.w_DESCFG)
      this.oPgFrm.Page1.oPag.oDESCFG_1_2.value=this.w_DESCFG
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCFG_1_7.RadioValue()==this.w_TIPCFG)
      this.oPgFrm.Page1.oPag.oTIPCFG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEGRP_1_9.value==this.w_UTEGRP)
      this.oPgFrm.Page1.oPag.oUTEGRP_1_9.value=this.w_UTEGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEGRP_1_10.value==this.w_UTEGRP)
      this.oPgFrm.Page1.oPag.oUTEGRP_1_10.value=this.w_UTEGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_11.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODAZI_1_12.value==this.w_CODAZI)
      this.oPgFrm.Page1.oPag.oCODAZI_1_12.value=this.w_CODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oMODSAV_1_13.RadioValue()==this.w_MODSAV)
      this.oPgFrm.Page1.oPag.oMODSAV_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFCFG_1_15.RadioValue()==this.w_DEFCFG)
      this.oPgFrm.Page1.oPag.oDEFCFG_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_22.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_22.value=this.w_RAGAZI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DESCFG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESCFG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DESCFG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_UTEGRP))  and not(INLIST(.w_TIPCFG, 'G', 'I'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTEGRP_1_9.SetFocus()
            i_bnoObbl = !empty(.w_UTEGRP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_UTEGRP))  and not(INLIST(.w_TIPCFG, 'U', 'I'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTEGRP_1_10.SetFocus()
            i_bnoObbl = !empty(.w_UTEGRP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kcg
      *--- Controllo Default
      If i_bRes
         i_bRes=GSUT_BCG(This, 'C')
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCFG = this.w_TIPCFG
    return

enddefine

* --- Define pages as container
define class tgsut_kcgPag1 as StdContainer
  Width  = 539
  height = 446
  stdWidth  = 539
  stdheight = 446
  resizeXpos=370
  resizeYpos=159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object zoom as cp_zoombox with uid="EZNGQCLILV",left=17, top=44, width=512,height=226,;
    caption='Object',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="CFG_GEST",cZoomFile="GSUT_KCG",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 78824422

  add object oDESCFG_1_2 as StdField with uid="BUXSXJTFUT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DESCFG", cQueryName = "DESCFG",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nuova configurazione",;
    HelpContextID = 96417334,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=109, Top=309, InputMask=replicate('X',50)


  add object oTIPCFG_1_7 as StdCombo with uid="CSNCYEFXDU",rtseq=2,rtrep=.f.,left=109,top=337,width=113,height=21;
    , ToolTipText = "La configurazione pu� essere valida per un utente, un gruppo o per l'installazione";
    , HelpContextID = 96406326;
    , cFormVar="w_TIPCFG",RowSource=""+"Utente,"+"Gruppo,"+"Installazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCFG_1_7.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,'I',;
    space(1)))))
  endfunc
  func oTIPCFG_1_7.GetRadio()
    this.Parent.oContained.w_TIPCFG = this.RadioValue()
    return .t.
  endfunc

  func oTIPCFG_1_7.SetRadio()
    this.Parent.oContained.w_TIPCFG=trim(this.Parent.oContained.w_TIPCFG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCFG=='U',1,;
      iif(this.Parent.oContained.w_TIPCFG=='G',2,;
      iif(this.Parent.oContained.w_TIPCFG=='I',3,;
      0)))
  endfunc

  add object oUTEGRP_1_9 as StdField with uid="DGSCTVAEOU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_UTEGRP", cQueryName = "UTEGRP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 260204102,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=226, Top=337, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UTEGRP"

  func oUTEGRP_1_9.mHide()
    with this.Parent.oContained
      return (INLIST(.w_TIPCFG, 'G', 'I'))
    endwith
  endfunc

  func oUTEGRP_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTEGRP_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTEGRP_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oUTEGRP_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oUTEGRP_1_10 as StdField with uid="XLKZRCLRLO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_UTEGRP", cQueryName = "UTEGRP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 260204102,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=226, Top=337, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_UTEGRP"

  func oUTEGRP_1_10.mHide()
    with this.Parent.oContained
      return (INLIST(.w_TIPCFG, 'U', 'I'))
    endwith
  endfunc

  func oUTEGRP_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTEGRP_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTEGRP_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oUTEGRP_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESCRI_1_11 as StdField with uid="FEGUOMKVNW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 142554678,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=289, Top=337, InputMask=replicate('X',20)

  add object oCODAZI_1_12 as StdField with uid="TXKSPWCQIL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODAZI", cQueryName = "CODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la configurazione � attiva solo per l'azienda valorizzata",;
    HelpContextID = 150753318,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=109, Top=366, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CODAZI"

  func oCODAZI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAZI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAZI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oCODAZI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Aziende",'',this.parent.oContained
  endproc


  add object oMODSAV_1_13 as StdCombo with uid="MAUJTAXQAL",rtseq=7,rtrep=.f.,left=109,top=396,width=113,height=21;
    , ToolTipText = "Modalit� salvataggio";
    , HelpContextID = 75387078;
    , cFormVar="w_MODSAV",RowSource=""+"Automatico,"+"Con richiesta,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMODSAV_1_13.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'D',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oMODSAV_1_13.GetRadio()
    this.Parent.oContained.w_MODSAV = this.RadioValue()
    return .t.
  endfunc

  func oMODSAV_1_13.SetRadio()
    this.Parent.oContained.w_MODSAV=trim(this.Parent.oContained.w_MODSAV)
    this.value = ;
      iif(this.Parent.oContained.w_MODSAV=='A',1,;
      iif(this.Parent.oContained.w_MODSAV=='D',2,;
      iif(this.Parent.oContained.w_MODSAV=='N',3,;
      0)))
  endfunc

  add object oDEFCFG_1_15 as StdCheck with uid="MGOLHDSJVM",rtseq=8,rtrep=.f.,left=288, top=395, caption="Default",;
    ToolTipText = "Se attivo la configurazione viene subito visualizzata all'apertura della gestione",;
    HelpContextID = 96364086,;
    cFormVar="w_DEFCFG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDEFCFG_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDEFCFG_1_15.GetRadio()
    this.Parent.oContained.w_DEFCFG = this.RadioValue()
    return .t.
  endfunc

  func oDEFCFG_1_15.SetRadio()
    this.Parent.oContained.w_DEFCFG=trim(this.Parent.oContained.w_DEFCFG)
    this.value = ;
      iif(this.Parent.oContained.w_DEFCFG=='S',1,;
      0)
  endfunc


  add object oBtn_1_16 as StdButton with uid="QYACFJOWLI",left=432, top=393, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare una nuova configurazione";
    , HelpContextID = 99144474;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DESCFG))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="OVEULLOQUH",left=484, top=393, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91855802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRAGAZI_1_22 as StdField with uid="MLNYSIDLBD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 150762262,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=190, Top=366, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="GCAMOPFXZO",Visible=.t., Left=21, Top=10,;
    Alignment=0, Width=357, Height=17,;
    Caption="Doppio click per salvare su una configurazione gi� esistente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="VQNDOXIHRU",Visible=.t., Left=21, Top=282,;
    Alignment=0, Width=142, Height=17,;
    Caption="Nuova configurazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="MKWOMAXJZR",Visible=.t., Left=6, Top=309,;
    Alignment=1, Width=101, Height=18,;
    Caption="Configurazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XMACPDKSNN",Visible=.t., Left=21, Top=337,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JSEAFHLANE",Visible=.t., Left=21, Top=398,;
    Alignment=1, Width=86, Height=17,;
    Caption="Salvataggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="USHKKUIYVJ",Visible=.t., Left=8, Top=366,;
    Alignment=1, Width=99, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JXSRJEQIWS",Visible=.t., Left=21, Top=27,;
    Alignment=0, Width=357, Height=17,;
    Caption="In verde le configurazioni di default"    , forecolor = RGB(0,198,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_5 as StdBox with uid="KWEPNHULPA",left=25, top=297, width=495,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kcg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kcg
Proc Salva_DblClick(pParent, pTipo)
    local l_Num,l_DesCfg, l_Mask, l_ret
    with pParent
          l_Mask=.oParentObject.w_pPARENT
          If pTipo='N'
             l_Num=0
             l_DesCfg=.w_DESCFG
          Else
             l_Num=.w_ROWNUM
             l_DesCfg=.w_OLDCFG
          Endif
          l_ret=GSUT_BCP(pParent;
              ,.oParentObject.w_pPARENT;
              ,.NULL.;
              ,'A';
              ,.w_GSNOME;
              ,l_Num;
             )
      if l_ret>0 or l_ret=-1
       Ah_ErrorMsg("Salvataggio della configurazione %1 completato","i","",Alltrim(l_DesCfg))
      endif
      *--- Se ho salvato per la stessa azienda carico la cfg
      if l_ret<>-1
         *--- Memorizzo il CPROWNUM della configurazione
         l_Mask.nCfgGest = l_ret
         *--- Cambio caption maschera
         if RAT(SPACE(5)+"(",l_Mask.Caption)>0
            l_Mask.Caption = SUBSTR(l_Mask.Caption, 1, RAT(SPACE(5)+"(",l_Mask.Caption)-1) + Space(5) + "("+ALLTRIM(l_DesCfg)+")"
         else
            l_Mask.Caption = l_Mask.Caption + Space(5) + "("+ALLTRIM(l_DesCfg)+")"
         endif
      endif
       l_Mask=.null.
   endwith


* --- Fine Area Manuale
