* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mmd                                                        *
*              RepWizard Step 2                                                *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-14                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mmd")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mmd")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mmd")
  return

* --- Class definition
define class tgsut_mmd as StdPCForm
  Width  = 553
  Height = 317
  Top    = 13
  Left   = 4
  cComment = "RepWizard Step 2"
  cPrg = "gsut_mmd"
  HelpContextID=197739369
  add object cnt as tcgsut_mmd
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mmd as PCContext
  w_WRSERIAL = space(10)
  w_CURCAMPI = space(10)
  w_WRCONFIG = space(15)
  w_CPROWORD = 0
  w_WRREPFLD = space(20)
  w_WRCURFLD = space(240)
  w_WRFLDESC = space(40)
  w_WR__AREA = space(15)
  w_WRTYPEOB = 0
  w_WRCALFOR = space(3)
  w_WRVARINV = space(240)
  w_FIELDTYPE = space(1)
  w_CURPARAM = space(10)
  proc Save(i_oFrom)
    this.w_WRSERIAL = i_oFrom.w_WRSERIAL
    this.w_CURCAMPI = i_oFrom.w_CURCAMPI
    this.w_WRCONFIG = i_oFrom.w_WRCONFIG
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_WRREPFLD = i_oFrom.w_WRREPFLD
    this.w_WRCURFLD = i_oFrom.w_WRCURFLD
    this.w_WRFLDESC = i_oFrom.w_WRFLDESC
    this.w_WR__AREA = i_oFrom.w_WR__AREA
    this.w_WRTYPEOB = i_oFrom.w_WRTYPEOB
    this.w_WRCALFOR = i_oFrom.w_WRCALFOR
    this.w_WRVARINV = i_oFrom.w_WRVARINV
    this.w_FIELDTYPE = i_oFrom.w_FIELDTYPE
    this.w_CURPARAM = i_oFrom.w_CURPARAM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_WRSERIAL = this.w_WRSERIAL
    i_oTo.w_CURCAMPI = this.w_CURCAMPI
    i_oTo.w_WRCONFIG = this.w_WRCONFIG
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_WRREPFLD = this.w_WRREPFLD
    i_oTo.w_WRCURFLD = this.w_WRCURFLD
    i_oTo.w_WRFLDESC = this.w_WRFLDESC
    i_oTo.w_WR__AREA = this.w_WR__AREA
    i_oTo.w_WRTYPEOB = this.w_WRTYPEOB
    i_oTo.w_WRCALFOR = this.w_WRCALFOR
    i_oTo.w_WRVARINV = this.w_WRVARINV
    i_oTo.w_FIELDTYPE = this.w_FIELDTYPE
    i_oTo.w_CURPARAM = this.w_CURPARAM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mmd as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 553
  Height = 317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=197739369
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  WIZDREP_IDX = 0
  cFile = "WIZDREP"
  cKeySelect = "WRSERIAL,WRCONFIG"
  cKeyWhere  = "WRSERIAL=this.w_WRSERIAL and WRCONFIG=this.w_WRCONFIG"
  cKeyDetail  = "WRSERIAL=this.w_WRSERIAL and WRCONFIG=this.w_WRCONFIG and WR__AREA=this.w_WR__AREA"
  cKeyWhereODBC = '"WRSERIAL="+cp_ToStrODBC(this.w_WRSERIAL)';
      +'+" and WRCONFIG="+cp_ToStrODBC(this.w_WRCONFIG)';

  cKeyDetailWhereODBC = '"WRSERIAL="+cp_ToStrODBC(this.w_WRSERIAL)';
      +'+" and WRCONFIG="+cp_ToStrODBC(this.w_WRCONFIG)';
      +'+" and WR__AREA="+cp_ToStrODBC(this.w_WR__AREA)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"WIZDREP.WRSERIAL="+cp_ToStrODBC(this.w_WRSERIAL)';
      +'+" and WIZDREP.WRCONFIG="+cp_ToStrODBC(this.w_WRCONFIG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'WIZDREP.CPROWORD '
  cPrg = "gsut_mmd"
  cComment = "RepWizard Step 2"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_WRSERIAL = space(10)
  w_CURCAMPI = space(10)
  w_WRCONFIG = space(15)
  w_CPROWORD = 0
  w_WRREPFLD = space(20)
  w_WRCURFLD = space(240)
  w_WRFLDESC = space(40)
  w_WR__AREA = space(15)
  w_WRTYPEOB = 0
  o_WRTYPEOB = 0
  w_WRCALFOR = space(3)
  w_WRVARINV = space(240)
  w_FIELDTYPE = space(1)
  w_CURPARAM = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mmdPag1","gsut_mmd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='WIZDREP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.WIZDREP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.WIZDREP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mmd'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from WIZDREP where WRSERIAL=KeySet.WRSERIAL
    *                            and WRCONFIG=KeySet.WRCONFIG
    *                            and WR__AREA=KeySet.WR__AREA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.WIZDREP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2],this.bLoadRecFilter,this.WIZDREP_IDX,"gsut_mmd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('WIZDREP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "WIZDREP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' WIZDREP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'WRSERIAL',this.w_WRSERIAL  ,'WRCONFIG',this.w_WRCONFIG  )
      select * from (i_cTable) WIZDREP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_WRSERIAL = NVL(WRSERIAL,space(10))
        .w_CURCAMPI = this.oParentObject.w_CURCAMPI
        .w_WRCONFIG = NVL(WRCONFIG,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CURPARAM = this.oParentObject.w_CURPARAM
        cp_LoadRecExtFlds(this,'WIZDREP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_FIELDTYPE = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_WRREPFLD = NVL(WRREPFLD,space(20))
          .w_WRCURFLD = NVL(WRCURFLD,space(240))
          .w_WRFLDESC = NVL(WRFLDESC,space(40))
          .w_WR__AREA = NVL(WR__AREA,space(15))
          .w_WRTYPEOB = NVL(WRTYPEOB,0)
          .w_WRCALFOR = NVL(WRCALFOR,space(3))
          .w_WRVARINV = NVL(WRVARINV,space(240))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CURCAMPI = this.oParentObject.w_CURCAMPI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CURPARAM = this.oParentObject.w_CURPARAM
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_WRSERIAL=space(10)
      .w_CURCAMPI=space(10)
      .w_WRCONFIG=space(15)
      .w_CPROWORD=10
      .w_WRREPFLD=space(20)
      .w_WRCURFLD=space(240)
      .w_WRFLDESC=space(40)
      .w_WR__AREA=space(15)
      .w_WRTYPEOB=0
      .w_WRCALFOR=space(3)
      .w_WRVARINV=space(240)
      .w_FIELDTYPE=space(1)
      .w_CURPARAM=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CURCAMPI = this.oParentObject.w_CURCAMPI
        .DoRTCalc(3,7,.f.)
        .w_WR__AREA = 'Dettaglio'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(9,9,.f.)
        .w_WRCALFOR = IIF(.w_WRTYPEOB#8,' ',.w_WRCALFOR)
        .w_WRVARINV = '0'
        .DoRTCalc(12,12,.f.)
        .w_CURPARAM = this.oParentObject.w_CURPARAM
      endif
    endwith
    cp_BlankRecExtFlds(this,'WIZDREP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oWRCALFOR_2_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'WIZDREP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.WIZDREP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_WRSERIAL,"WRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_WRCONFIG,"WRCONFIG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_WRREPFLD C(20);
      ,t_WRCURFLD C(240);
      ,t_WRFLDESC C(40);
      ,t_WRCALFOR N(3);
      ,CPROWNUM N(10);
      ,t_WR__AREA C(15);
      ,t_WRTYPEOB N(2);
      ,t_WRVARINV C(240);
      ,t_FIELDTYPE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mmdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oWRREPFLD_2_2.controlsource=this.cTrsName+'.t_WRREPFLD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oWRCURFLD_2_3.controlsource=this.cTrsName+'.t_WRCURFLD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oWRFLDESC_2_4.controlsource=this.cTrsName+'.t_WRFLDESC'
    this.oPgFRm.Page1.oPag.oWRCALFOR_2_8.controlsource=this.cTrsName+'.t_WRCALFOR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(40)
    this.AddVLine(195)
    this.AddVLine(349)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.WIZDREP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.WIZDREP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2])
      *
      * insert into WIZDREP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'WIZDREP')
        i_extval=cp_InsertValODBCExtFlds(this,'WIZDREP')
        i_cFldBody=" "+;
                  "(WRSERIAL,WRCONFIG,CPROWORD,WRREPFLD,WRCURFLD"+;
                  ",WRFLDESC,WR__AREA,WRTYPEOB,WRCALFOR,WRVARINV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_WRSERIAL)+","+cp_ToStrODBC(this.w_WRCONFIG)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_WRREPFLD)+","+cp_ToStrODBC(this.w_WRCURFLD)+;
             ","+cp_ToStrODBC(this.w_WRFLDESC)+","+cp_ToStrODBC(this.w_WR__AREA)+","+cp_ToStrODBC(this.w_WRTYPEOB)+","+cp_ToStrODBC(this.w_WRCALFOR)+","+cp_ToStrODBC(this.w_WRVARINV)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'WIZDREP')
        i_extval=cp_InsertValVFPExtFlds(this,'WIZDREP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'WRSERIAL',this.w_WRSERIAL,'WRCONFIG',this.w_WRCONFIG,'WR__AREA',this.w_WR__AREA)
        INSERT INTO (i_cTable) (;
                   WRSERIAL;
                  ,WRCONFIG;
                  ,CPROWORD;
                  ,WRREPFLD;
                  ,WRCURFLD;
                  ,WRFLDESC;
                  ,WR__AREA;
                  ,WRTYPEOB;
                  ,WRCALFOR;
                  ,WRVARINV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_WRSERIAL;
                  ,this.w_WRCONFIG;
                  ,this.w_CPROWORD;
                  ,this.w_WRREPFLD;
                  ,this.w_WRCURFLD;
                  ,this.w_WRFLDESC;
                  ,this.w_WR__AREA;
                  ,this.w_WRTYPEOB;
                  ,this.w_WRCALFOR;
                  ,this.w_WRVARINV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.WIZDREP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (! EMPTY(t_WRREPFLD)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'WIZDREP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'WIZDREP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (! EMPTY(t_WRREPFLD)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update WIZDREP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'WIZDREP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",WRREPFLD="+cp_ToStrODBC(this.w_WRREPFLD)+;
                     ",WRCURFLD="+cp_ToStrODBC(this.w_WRCURFLD)+;
                     ",WRFLDESC="+cp_ToStrODBC(this.w_WRFLDESC)+;
                     ",WRTYPEOB="+cp_ToStrODBC(this.w_WRTYPEOB)+;
                     ",WRCALFOR="+cp_ToStrODBC(this.w_WRCALFOR)+;
                     ",WRVARINV="+cp_ToStrODBC(this.w_WRVARINV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'WIZDREP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,WRREPFLD=this.w_WRREPFLD;
                     ,WRCURFLD=this.w_WRCURFLD;
                     ,WRFLDESC=this.w_WRFLDESC;
                     ,WRTYPEOB=this.w_WRTYPEOB;
                     ,WRCALFOR=this.w_WRCALFOR;
                     ,WRVARINV=this.w_WRVARINV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.WIZDREP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (! EMPTY(t_WRREPFLD)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete WIZDREP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (! EMPTY(t_WRREPFLD)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.WIZDREP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.WIZDREP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_CURCAMPI = this.oParentObject.w_CURCAMPI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(3,9,.t.)
        if .o_WRTYPEOB<>.w_WRTYPEOB
          .w_WRCALFOR = IIF(.w_WRTYPEOB#8,' ',.w_WRCALFOR)
        endif
        .DoRTCalc(11,12,.t.)
          .w_CURPARAM = this.oParentObject.w_CURPARAM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_WR__AREA with this.w_WR__AREA
      replace t_WRTYPEOB with this.w_WRTYPEOB
      replace t_WRVARINV with this.w_WRVARINV
      replace t_FIELDTYPE with this.w_FIELDTYPE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_UQYULQRYGG()
    with this
          * --- Aggiorno la lista dei campi da assegnare
          GSUT_BRW(this;
              ,'AGGLIST';
             )
          .w_WRFLDESC = iif(empty(.w_WRREPFLD),' ',.w_WRFLDESC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oWRCURFLD_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oWRCURFLD_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oWRFLDESC_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oWRFLDESC_2_4.mCond()
    this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("AGGLISTA") or lower(cEvent)==lower("w_WRCURFLD Changed")
          .Calculate_UQYULQRYGG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.RadioValue()==this.w_WRCALFOR)
      this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.SetRadio()
      replace t_WRCALFOR with this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRREPFLD_2_2.value==this.w_WRREPFLD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRREPFLD_2_2.value=this.w_WRREPFLD
      replace t_WRREPFLD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRREPFLD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRCURFLD_2_3.value==this.w_WRCURFLD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRCURFLD_2_3.value=this.w_WRCURFLD
      replace t_WRCURFLD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRCURFLD_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRFLDESC_2_4.value==this.w_WRFLDESC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRFLDESC_2_4.value=this.w_WRFLDESC
      replace t_WRFLDESC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oWRFLDESC_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'WIZDREP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if ! EMPTY(.w_WRREPFLD)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_WRTYPEOB = this.w_WRTYPEOB
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(! EMPTY(t_WRREPFLD))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=.F.
    if !i_bRes
      cp_ErrorMsg("Cancellazione righe non consentita","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_WRREPFLD=space(20)
      .w_WRCURFLD=space(240)
      .w_WRFLDESC=space(40)
      .w_WR__AREA=space(15)
      .w_WRTYPEOB=0
      .w_WRCALFOR=space(3)
      .w_WRVARINV=space(240)
      .w_FIELDTYPE=space(1)
      .DoRTCalc(1,7,.f.)
        .w_WR__AREA = 'Dettaglio'
      .DoRTCalc(9,9,.f.)
        .w_WRCALFOR = IIF(.w_WRTYPEOB#8,' ',.w_WRCALFOR)
        .w_WRVARINV = '0'
    endwith
    this.DoRTCalc(12,13,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_WRREPFLD = t_WRREPFLD
    this.w_WRCURFLD = t_WRCURFLD
    this.w_WRFLDESC = t_WRFLDESC
    this.w_WR__AREA = t_WR__AREA
    this.w_WRTYPEOB = t_WRTYPEOB
    this.w_WRCALFOR = this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.RadioValue(.t.)
    this.w_WRVARINV = t_WRVARINV
    this.w_FIELDTYPE = t_FIELDTYPE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_WRREPFLD with this.w_WRREPFLD
    replace t_WRCURFLD with this.w_WRCURFLD
    replace t_WRFLDESC with this.w_WRFLDESC
    replace t_WR__AREA with this.w_WR__AREA
    replace t_WRTYPEOB with this.w_WRTYPEOB
    replace t_WRCALFOR with this.oPgFrm.Page1.oPag.oWRCALFOR_2_8.ToRadio()
    replace t_WRVARINV with this.w_WRVARINV
    replace t_FIELDTYPE with this.w_FIELDTYPE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mmdPag1 as StdContainer
  Width  = 549
  height = 317
  stdWidth  = 549
  stdheight = 317
  resizeXpos=296
  resizeYpos=176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=2, width=536,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="WRREPFLD",Label2="Campo report",Field3="WRCURFLD",Label3="Campo cursore",Field4="WRFLDESC",Label4="Desc. campo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118351482

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=21,;
    width=533+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=22,width=532+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oWRCALFOR_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- gsut_mmd
      this.Parent.oContained.oParentObject.NotifyEvent("CambioRiga")
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oWRCALFOR_2_8 as StdTrsCombo with uid="DJXZFTPYWH",rtrep=.t.,;
    cFormVar="w_WRCALFOR", RowSource=""+"Nessuna,"+"Counter,"+"Somma,"+"Media,"+"Minimo,"+"Massimo,"+"Deviazione Std,"+"Varianza" , ;
    ToolTipText = "Eventuale calcolo associato al campo (somma, max, min, ..)",;
    HelpContextID = 12827464,;
    Height=25, Width=135, Left=57, Top=291,;
    cTotal="", cQueryName = "WRCALFOR",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oWRCALFOR_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..WRCALFOR,&i_cF..t_WRCALFOR),this.value)
    return(iif(xVal =1,' ',;
    iif(xVal =2,'COU',;
    iif(xVal =3,'SUM',;
    iif(xVal =4,'MED',;
    iif(xVal =5,'MIN',;
    iif(xVal =6,'MAX',;
    iif(xVal =7,'DEV',;
    iif(xVal =8,'VAR',;
    ' ')))))))))
  endfunc
  func oWRCALFOR_2_8.GetRadio()
    this.Parent.oContained.w_WRCALFOR = this.RadioValue()
    return .t.
  endfunc

  func oWRCALFOR_2_8.ToRadio()
    this.Parent.oContained.w_WRCALFOR=trim(this.Parent.oContained.w_WRCALFOR)
    return(;
      iif(this.Parent.oContained.w_WRCALFOR=='',1,;
      iif(this.Parent.oContained.w_WRCALFOR=='COU',2,;
      iif(this.Parent.oContained.w_WRCALFOR=='SUM',3,;
      iif(this.Parent.oContained.w_WRCALFOR=='MED',4,;
      iif(this.Parent.oContained.w_WRCALFOR=='MIN',5,;
      iif(this.Parent.oContained.w_WRCALFOR=='MAX',6,;
      iif(this.Parent.oContained.w_WRCALFOR=='DEV',7,;
      iif(this.Parent.oContained.w_WRCALFOR=='VAR',8,;
      0)))))))))
  endfunc

  func oWRCALFOR_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oWRCALFOR_2_8.mCond()
    with this.Parent.oContained
      return (.w_WRTYPEOB = 8  AND INLIST(.w_FIELDTYPE ,'N','F','B','I'))
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="VAPIWTIDOZ",Visible=.t., Left=6, Top=292,;
    Alignment=1, Width=49, Height=18,;
    Caption="Tipo Op.:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mmdBodyRow as CPBodyRowCnt
  Width=523
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KUIIZKMCER",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 150679914,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=-2, Top=0

  add object oWRREPFLD_2_2 as StdTrsField with uid="DYNKQPSSDH",rtseq=5,rtrep=.t.,;
    cFormVar="w_WRREPFLD",value=space(20),enabled=.f.,;
    HelpContextID = 8309590,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=33, Top=0, InputMask=replicate('X',20)

  add object oWRCURFLD_2_3 as StdTrsField with uid="MYJUIVFRQG",rtseq=6,rtrep=.t.,;
    cFormVar="w_WRCURFLD",value=space(240),;
    HelpContextID = 5225302,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=150, Left=189, Top=0, InputMask=replicate('X',240), bHasZoom = .t. 

  func oWRCURFLD_2_3.mCond()
    with this.Parent.oContained
      return (! empty(.w_WRREPFLD))
    endwith
  endfunc

  proc oWRCURFLD_2_3.mZoom
    do GSUT_KWZ with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oWRFLDESC_2_4 as StdTrsField with uid="FAKROYQZNG",rtseq=7,rtrep=.t.,;
    cFormVar="w_WRFLDESC",value=space(40),;
    HelpContextID = 231175337,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=174, Left=344, Top=0, InputMask=replicate('X',40)

  func oWRFLDESC_2_4.mCond()
    with this.Parent.oContained
      return (! empty(.w_WRREPFLD))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mmd','WIZDREP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".WRSERIAL=WIZDREP.WRSERIAL";
  +" and "+i_cAliasName2+".WRCONFIG=WIZDREP.WRCONFIG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
