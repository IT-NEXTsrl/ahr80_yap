* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcp                                                        *
*              Aggiorna chiude partite da p.N                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_195]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2014-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcp",oParentObject)
return(i_retval)

define class tgscg_bcp as StdBatch
  * --- Local variables
  w_PTSA = .NULL.
  w_ABB = 0
  w_IMPPNT = 0
  w_CAONAZ = 0
  w_APPO = 0
  w_APPO1 = 0
  w_APPO2 = 0
  w_DATRIF = ctod("  /  /  ")
  w_DECTOP = 0
  w_EURVAL = 0
  w_VALNAZ = space(3)
  w_VAL1A = 0
  w_VAL2A = 0
  w_VAL1P = 0
  w_VAL2P = 0
  w_VALACC = 0
  w_IMPDCA = 0
  w_IMPDCP = 0
  w_INIT = .f.
  w_IMPABB = 0
  w_CODVAL = space(3)
  w_MESS = space(100)
  w_DATREG = ctod("  /  /  ")
  w_CODESE = space(4)
  w_TIPPAG = space(5)
  w_MODPAG = space(10)
  w_CAOAPE = 0
  w_CAOVAL = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_SIMVAL = space(3)
  w_DESRIG = space(50)
  w_IMPSAL = 0
  w_IMPPDP = 0
  w_IMPPDA = 0
  w_CODAGE = space(5)
  w_PADRE = .NULL.
  w_PNOTA = .NULL.
  w_FLAGAC = space(1)
  w_INDICE = 0
  w_TOTABB = 0
  w_TOTIMP = 0
  w_TOTORI = 0
  w_TOTRES = 0
  w_TOTABBNAZ = 0
  w_TOTRESVAL = 0
  w_TOTALE_AB = 0
  w_OKABB = .f.
  w_RIGA = 0
  w_ONLYABB = .f.
  w_OKACC = .f.
  w_NUMRER = 0
  w_FLCRSA = space(1)
  w_IMPABP = 0
  w_IMPABA = 0
  w_TOTACC = 0
  * --- WorkFile variables
  CONTROPA_idx=0
  PAG_2AME_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Partite da Chiusura Partite (in GSCG_MPA chiamato da notifyevent ChiudePartite)
    this.w_PNOTA = this.oParentObject.oParentObject
    this.w_PADRE = this.oParentObject
    * --- Puntatore all'Oggetto Chiude Partite (GSCG_MSP)
    this.w_PTSA = this.oParentObject.w_OGG
    * --- Cambio Moneta di Conto (dalla Primanota)
    this.w_PADRE.Initson()     
    this.w_INDICE = 1
    WITH this.oParentObject.oParentObject
    this.w_MODPAG = SPACE(10)
    this.w_VALNAZ = .w_PNVALNAZ
    this.w_CODVAL = .w_PNCODVAL
    this.w_SIMVAL = g_VALSIM
    this.w_CAOVAL = .w_PNCAOVAL
    this.w_NUMRER = .w_PNNUMRER
    this.w_NUMDOC = .w_PNNUMDOC
    this.w_ALFDOC = .w_PNALFDOC
    this.w_DATREG = .w_PNDATREG
    this.w_DECTOP = .w_DECTOP
    this.w_CAONAZ = .w_CAONAZ
    this.w_DATRIF = IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC)
    this.w_CODESE = .w_PNCOMPET
    this.w_DESRIG = .w_PNDESRIG
    this.w_FLAGAC = .w_FLAGAC
    this.w_CODAGE = .w_PNCODAGE
    ENDWITH
    this.w_CAONAZ = IIF(this.w_CAONAZ=0, GETCAM(g_PERVAL,i_DATSYS), this.w_CAONAZ)
    * --- Controlli Preliminari
    if this.w_PTSA.w_TOTSAL=0
      ah_ErrorMsg("Non ci sono importi da saldare",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Azzera il Transitorio Partite
    this.w_INIT = .F.
    if this.w_PNOTA.cFunction="Load"
      * --- w_PARCAR='S': Trucco per NON ESEGUIRE 2 Volte il Batch GSCG_BSP (Lanciato dalla Blank all'inizio)
      this.oParentObject.oParentObject.w_PARCAR="S"
      this.w_PADRE.BlankRec()     
      this.oParentObject.oParentObject.w_PARCAR=" "
      this.w_INIT = .T.
    else
      * --- Svuoto il temporaneo delle partite se non sono in caricamento...
       
 Select ( this.w_PADRE.cTrsName) 
 Go Top 
 Delete All
    endif
    this.oParentObject.w_OGG = this.w_PTSA
    * --- Cicla sul temporaneo Chiusura Partite
    this.w_PADRE.MarkPos()     
    this.w_IMPPNT = 0
    this.w_IMPDCA = 0
    this.w_IMPDCP = 0
    this.w_IMPABB = 0
    this.w_IMPPDP = 0
    this.w_IMPPDA = 0
    this.w_VAL1A = 0
    this.w_VAL2A = 0
    this.w_VAL1P = 0
    this.w_VAL2P = 0
    this.w_VALACC = 0
    * --- Ricalcolo abbuoni se importo a saldo � inferiore all'importo digitato in primanota 
    SELECT ( this.w_PTSA.cTrsName )
    * --- Se imposto in prima nota manualmente un importo, verifico se esistono
    *     partite saldate con abbuono che hanno importo saldato inferiore al
    *     importo in prima nota, se cos� annullo l'abbuono, altrimenti adeguo l'abbuono
    *     in funzione del residuo tra importo in prima nota e totale partite saldate
    this.w_TOTORI = this.w_PTSA.w_TOTDAV
    if this.w_TOTORI<>0
      this.w_PTSA.MarkPos()     
      * --- w_TOTRES = Residuo in Valuta di Conto
      *     w_TOTRESVAL = Residuo in valuta partita calcolato al momento del suo 
      *                                   utilizzo per aggiungere il residuo all'ultima partita
      this.w_TOTRES = IIF(this.w_CODVAL=this.w_PTSA.w_CODNAZ,this.w_TOTORI-this.w_PTSA.w_TOTSAL,VAL2MON(this.w_PTSA.w_TOTDAO-this.w_PTSA.w_VALSAL, this.w_CAOVAL, this.w_CAONAZ, this.w_DATRIF, this.w_VALNAZ, this.w_DECTOP))
      if this.w_TOTRES=0 
        this.w_RIGA = this.w_PTSA.search("NVL(t_SAIMPABB, 0) <> 0   And Not Deleted() ")
        if this.w_RIGA<>-1
          this.w_OKABB = Ah_yesno("Si desidera procedere con la creazione degli abbuoni?")
        endif
      endif
      SCAN FOR ((t_SAIMPABB>0 AND t_SA_SEGNO="A" AND this.w_PTSA.w_TIPCON="F" ) OR (t_SAIMPABB>0 AND t_SA_SEGNO="D" AND this.w_PTSA.w_TIPCON="C")) 
      this.w_PTSA.WorkFromTrs()     
      * --- w_TOTABBNAZ= Importo abbuono in valuta di conto
      *     w_TOTABB= Importo abbuono in valuta partita
      *     w_TOTIMP= Importo totale partita in valuta partita
      *     w_IMPSAL= Importo saldato in valuta partita
      this.w_TOTABB = NVL(t_SAIMPABB,0)
      this.w_TOTIMP = NVL(t_SATOTIMP,0)
      this.w_IMPSAL = NVL(t_SAIMPSAL,0)
      this.w_TOTABBNAZ = IIF(t_SACODVAL=this.w_PTSA.w_CODNAZ, t_SAIMPABB, VAL2MON(t_SAIMPABB,this.w_CAOVAL, this.w_PTSA.w_CAONAZ, this.w_PTSA.w_DATRIF, this.w_PTSA.w_CODNAZ,this.w_PTSA.w_DECTOP))
      if this.w_IMPSAL<this.w_TOTIMP
        * --- Se il residuo tra quanto saldato e quanto scritto in prima nota � maggiore
        *     o uguale a zero non faccio l'abbuono.
        if this.w_TOTRES>0 or (!this.w_OKABB AND this.w_TOTRES=0 )
          * --- In questo caso ho un acconto
          this.w_TOTIMP = this.w_IMPSAL+ this.w_TOTABB
          this.w_TOTABB = 0
        else
          if this.w_TOTABBNAZ>=ABS(this.w_TOTRES)
            * --- Inserisco un abbuono pari al residuo e azzero il residuo per fare rientrare i casi successivi
            *     nel primo ramo dell' IF() 
            if this.w_TOTRES<>0
              if t_SACODVAL=this.w_PTSA.w_CODNAZ
                this.w_TOTIMP = this.w_IMPSAL+ this.w_TOTABB+this.w_TOTRES
                this.w_TOTABB = ABS(this.w_TOTRES)
              else
                this.w_TOTRESVAL = MON2VAL( this.w_TOTRES , this.w_CAOVAL, this.w_PTSA.w_CAONAZ, this.w_PTSA.w_DATRIF, this.w_PTSA.w_CODNAZ,this.w_PTSA.w_DECTOP)
                this.w_TOTIMP = this.w_IMPSAL+ this.w_TOTABB+this.w_TOTRESVAL
                this.w_TOTABB = ABS(this.w_TOTRESVAL)
              endif
              this.w_TOTRES = 0
            else
              * --- Se il totale residuo � zero potrei cmq fare degli abbuoni in questo caso non devo fare la riga
              *     del Cli\Fornitore ma solo la riga dell'abbuono
              this.w_ONLYABB = .T.
              this.w_TOTIMP = this.w_IMPSAL+ this.w_TOTABB
            endif
          else
            * --- Decremento residuo e lascio situazione abbuoni immutata
            this.w_TOTRES = this.w_TOTRES+this.w_TOTABBNAZ
            if this.w_IMPSAL>=0 AND this.w_IMPSAL<this.w_TOTIMP
              this.w_TOTIMP = this.w_IMPSAL
            endif
          endif
        endif
        REPLACE t_SAIMPABB WITH this.w_TOTABB
        REPLACE t_SAIMPSAL WITH this.w_TOTIMP
      endif
      ENDSCAN
      this.w_PTSA.RePos()     
    endif
    if this.w_PTSA.w_CODNAZ<>this.w_PTSA.w_CODVAO AND this.w_PTSA.w_NUMVAL=1 AND this.w_PTSA.w_TOTDAV<>0
      * --- Legge l' importo direttamente in Valuta
      this.w_VALACC = this.w_PTSA.w_VALRES
    else
      this.w_VALACC = MON2VAL(this.w_PTSA.w_TOTRES,this.w_CAOVAL,1,I_DATSYS,this.w_PTSA.w_VALRES,this.w_PTSA.w_DECTOP)
    endif
    this.w_SIMVAL = this.w_PTSA.w_SIMVAO
    if this.w_VALACC>0 
      * --- Gestione Acconti
      this.w_OKACC = .F.
      if this.w_FLAGAC="S"
        this.w_OKACC = .T.
      else
        this.w_MESS = "Sono state selezionate partite per un importo inferiore a quello imputato%0sulla riga di primanota: vuoi considerare la parte residua%0come partita di acconto (%1)?"
        this.w_OKACC = ah_YesNo(this.w_MESS,,this.w_SIMVAL+SPACE(1)+alltrim(STR(this.w_VALACC,18,this.w_DECTOP)))
      endif
      if this.w_OKACC
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.w_TOTACC = 0
    SELECT ( this.w_PTSA.cTrsName )
    GO TOP
    SCAN FOR t_SACAOAPE<>0 And t_SAIMPSAL<>0 OR t_FLSALD="S"
    * --- Nuova Riga del Temporaneo
    SELECT ( this.w_PADRE.cTrsName )
    if this.w_INIT=.F.
      * --- Append gia' eseguito la prima volta nella BlankRec
      this.w_PADRE.InitRow()     
    endif
    this.w_INIT = .F.
    this.w_PADRE.Initson()     
    SELECT ( this.w_PTSA.cTrsName )
    this.oParentObject.w_PTDATSCA = t_SADATSCA
    this.oParentObject.w_PTNUMPAR = t_SANUMPAR
    this.oParentObject.w_PT_SEGNO = IIF(t_SA_SEGNO="D", "A", "D")
    this.oParentObject.w_PTTIPCON = this.w_PTSA.w_TIPCON
    this.oParentObject.w_PTCODCON = this.w_PTSA.w_CODCON
    this.oParentObject.w_PTMODPAG = t_SAMODPAG
    this.oParentObject.w_PTTOTIMP = t_SAIMPSAL
    this.oParentObject.w_PTSERRIF = t_SASERRIF
    this.oParentObject.w_PTORDRIF = t_SAORDRIF
    this.oParentObject.w_PTNUMRIF = t_SANUMRIF
    * --- Se � un acconto la partita che lo chiude � di creazione
    *     In GSCG_BPK scritture sul database per far puntare l'acconto
    *     selezionato alla partita qui in creazione
    if "Acconto" $ this.oParentObject.w_PTNUMPAR
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTFLCRSA"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PTSERRIF);
              +" and PTROWORD = "+cp_ToStrODBC(this.oParentObject.w_PTORDRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_PTNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTFLCRSA;
          from (i_cTable) where;
              PTSERIAL = this.oParentObject.w_PTSERRIF;
              and PTROWORD = this.oParentObject.w_PTORDRIF;
              and CPROWNUM = this.oParentObject.w_PTNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCRSA = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_FLCRSA="A"
        this.oParentObject.w_PTFLCRSA = "C"
        * --- Un acconto selezionato dall'utente assume come numero partita
        *     il numero registrazione seguito immediatamente da una 'A'.
        *     Questo perch� nel caso di un acconto saldato da chiudi partite in prima nota
        *     non ho riferimenti a fatture (sia la registrazione di acconto che di incasso
        *     non hanno necesariamente numero documento).
        *     Una 'A' alla fine per evitare di creare un numero partita potenzialmente
        *     utilizzabile da una partita normale.
        *     Acconto tolto dal numero partita per evitare che la stessa sia riconsiderata
        *     nelle varie elaborazione relative agli acconti (Like '%acconto%' � il filtro
        *     utilizzato)
        this.oParentObject.w_PTNUMPAR = CANUMPAR(IIF(this.w_FLAGAC="S","R","A"),this.w_CODESE, this.w_NUMRER)
      else
        this.oParentObject.w_PTFLCRSA = "S"
      endif
    endif
    this.oParentObject.w_PTCODVAL = t_SACODVAL
    this.oParentObject.w_PTCAOVAL = t_SACAOVAL
    this.oParentObject.w_PTCAOAPE = t_SACAOAPE
    this.w_EURVAL = t_EURVAL
    this.oParentObject.w_PTDATAPE = IIF(EMPTY(t_SADATAPE), this.w_DATRIF, t_SADATAPE)
    * --- Se la causale associata alla registrazione ha come tipo documento 'No Documento',
    *     come numero, alfa e data, vado a prendere quello dell'ultima registrazione, altrimenti quello della registrazione di origine
    if this.oParentObject.w_TIPDOC="NO"
      this.oParentObject.w_PTNUMDOC = t_SANUMDOC
      this.oParentObject.w_PTALFDOC = t_SAALFDOC
      this.oParentObject.w_PTDATDOC = t_SADATDOC
    else
      this.oParentObject.w_PTNUMDOC = this.w_NUMDOC
      this.oParentObject.w_PTALFDOC = this.w_ALFDOC
      this.oParentObject.w_PTDATDOC = this.w_DATRIF
    endif
    this.oParentObject.w_PTDESRIG = t_SADESRIG
    this.oParentObject.w_PTCODAGE = t_SACODAGE
    this.oParentObject.w_PTBANAPP = t_SABANAPP
    this.oParentObject.w_PTBANNOS = t_SABANNOS
    this.oParentObject.w_PTFLVABD = t_SAFLVABD
    this.w_ABB = t_SAIMPABB
    this.oParentObject.w_PTTOTABB = t_SAIMPABB
    this.oParentObject.w_PTIMPDOC = t_IMPDOC
    * --- Importo Abbuono 
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_ABB<0
      * --- Se Se Abbuono Attivo da Cliente o Abbuono Passivo a Fornitore scrive una nuova riga con cambio di Chiusura
      * --- Storna la parte riferita al cambio di apertura (w_ABB Negativo)
      this.oParentObject.w_PTTOTIMP = this.oParentObject.w_PTTOTIMP + this.w_ABB
    endif
    * --- Calcola Importo Riga Primanota
    this.w_APPO = this.oParentObject.w_PTTOTIMP * IIF(this.oParentObject.w_PT_SEGNO="D", 1, -1)
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_APPO<>0
      * --- Se Valuta...
      this.w_APPO = cp_Round(VAL2MON(this.w_APPO, this.oParentObject.w_PTCAOAPE, this.w_CAONAZ, this.oParentObject.w_PTDATAPE,this.w_VALNAZ),this.w_DECTOP)
    endif
    this.w_IMPPNT = this.w_IMPPNT + this.w_APPO
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOAPE=C.Apertura; w_PTCAOVAL=C.Chiusura)
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND (this.w_EURVAL=0 OR this.w_DATRIF<GETVALUT(g_PERVAL, "VADATEUR"))
      * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_APPO = this.w_ABB * IIF(this.oParentObject.w_PT_SEGNO="D", -1, 1)
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_ABB<>0
      * --- Se Valuta...
      if this.w_ABB<0
        * --- Se Se Abbuono Attivo da Cliente o Abbuono Passivo a Fornitore Il cambio va riferito alla Data Attuale
        this.w_APPO = VAL2MON(this.w_APPO, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATRIF, this.w_VALNAZ)
      else
        this.w_APPO = VAL2MON(this.w_APPO, this.oParentObject.w_PTCAOAPE, this.w_CAONAZ, this.oParentObject.w_PTDATAPE, this.w_VALNAZ)
      endif
    endif
    if this.w_ONLYABB
      * --- In questo caso non devo fare la riga del cliente \fornitore
      this.w_IMPABP = this.w_IMPABP + IIF(this.w_APPO>0, this.w_APPO, 0)
      this.w_IMPABA = this.w_IMPABA + IIF(this.w_APPO>0, 0, this.w_APPO)
    else
      this.w_IMPABB = this.w_IMPABB + this.w_APPO
    endif
    if this.w_FLAGAC="S" 
      * --- Memorizzo acconti selezionati, nell'array specifico dichiarato nell'area manuale 
      *     Declaire variabiles
      DIMENSION this.w_PNOTA.AACC[this.w_INDICE,10]
       
 this.w_PNOTA.AACC[this.w_INDICE,1] = this.oParentObject.w_PTDATSCA 
 this.w_PNOTA.AACC[this.w_INDICE,2] = this.oParentObject.w_PTTOTIMP* IIF(this.oParentObject.w_PT_SEGNO="A", 1, -1) 
 this.w_PNOTA.AACC[this.w_INDICE,3] = this.oParentObject.w_PTMODPAG 
 this.w_PNOTA.AACC[this.w_INDICE,4] = this.oParentObject.w_PTDESRIG 
 this.w_PNOTA.AACC[this.w_INDICE,5] = this.oParentObject.w_PTSERRIF 
 this.w_PNOTA.AACC[this.w_INDICE,6] = this.oParentObject.w_PTORDRIF 
 this.w_PNOTA.AACC[this.w_INDICE,7] = this.oParentObject.w_PTNUMRIF 
 this.w_PNOTA.AACC[this.w_INDICE,8] = this.oParentObject.w_PTCODVAL 
 this.w_PNOTA.AACC[this.w_INDICE,9] = this.oParentObject.w_PTCAOAPE 
 this.w_PNOTA.AACC[this.w_INDICE,10] = this.oParentObject.w_PTTOTABB
      this.w_INDICE = this.w_INDICE +1
      this.w_TOTACC = this.w_TOTACC + this.oParentObject.w_PTTOTIMP* IIF(this.oParentObject.w_PT_SEGNO="A", 1, -1)
    endif
    SELECT ( this.w_PADRE.cTrsName )
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.w_PADRE.TrsFromWork()     
    SELECT ( this.w_PTSA.cTrsName )
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_ABB<0
      * --- Se esiste Abbuono Attivo Cliente o Passivo Fornitore scrive nuova riga
      SELECT ( this.w_PADRE.cTrsName)
      this.w_PADRE.InitRow()     
      this.w_PADRE.Initson()     
      SELECT ( this.w_PTSA.cTrsName )
      this.oParentObject.w_PTDATSCA = t_SADATSCA
      this.oParentObject.w_PTNUMPAR = t_SANUMPAR
      this.oParentObject.w_PT_SEGNO = IIF(t_SA_SEGNO="D", "A", "D")
      this.oParentObject.w_PTTIPCON = this.w_PTSA.w_TIPCON
      this.oParentObject.w_PTCODCON = this.w_PTSA.w_CODCON
      this.oParentObject.w_PTMODPAG = t_SAMODPAG
      this.oParentObject.w_PTTOTIMP = ABS(this.w_ABB)
      this.oParentObject.w_PTFLCRSA = "S"
      this.oParentObject.w_PTCODVAL = t_SACODVAL
      this.oParentObject.w_PTCAOVAL = t_SACAOVAL
      this.oParentObject.w_PTCAOAPE = t_SACAOVAL
      this.w_EURVAL = t_EURVAL
      this.oParentObject.w_PTDATAPE = this.w_DATRIF
      if this.oParentObject.w_TIPDOC="NO"
        this.oParentObject.w_PTNUMDOC = t_SANUMDOC
        this.oParentObject.w_PTALFDOC = t_SAALFDOC
        this.oParentObject.w_PTDATDOC = t_SADATDOC
      else
        this.oParentObject.w_PTNUMDOC = this.w_NUMDOC
        this.oParentObject.w_PTALFDOC = this.w_ALFDOC
        this.oParentObject.w_PTDATDOC = this.w_DATRIF
      endif
      this.oParentObject.w_PTDESRIG = t_SADESRIG
      this.oParentObject.w_PTCODAGE = t_SACODAGE
      this.oParentObject.w_PTFLVABD = t_SAFLVABD
      this.oParentObject.w_PTSERRIF = t_SASERRIF
      this.oParentObject.w_PTORDRIF = t_SAORDRIF
      this.oParentObject.w_PTNUMRIF = t_SANUMRIF
      this.oParentObject.w_PTTOTABB = 0
      this.oParentObject.w_PTIMPDOC = t_IMPDOC
      * --- Calcola Importo Riga Primanota
      this.w_APPO = this.oParentObject.w_PTTOTIMP * IIF(this.oParentObject.w_PT_SEGNO="D", 1, -1)
      if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_APPO<>0
        * --- Se Valuta...
        this.w_APPO = cp_ROUND(VAL2MON(this.w_APPO, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.oParentObject.w_PTDATAPE,this.w_VALNAZ),this.w_DECTOP)
      endif
      this.w_IMPPNT = this.w_IMPPNT + this.w_APPO
      SELECT ( this.w_PADRE.cTrsName)
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      SELECT ( this.w_PTSA.cTrsName )
      if Nvl( t_SAIMPSAL,0)<>0
        this.w_PADRE.TrsFromWork()     
      endif
      SELECT ( this.w_PTSA.cTrsName )
    endif
    ENDSCAN
    * --- Aggiorna Riga Primanota Cli/For/Conto
    this.w_IMPPNT = cp_ROUND(this.w_IMPPNT, this.w_DECTOP)
    if this.w_FLAGAC="S" AND this.w_VALACC>0
      this.w_TOTACC = this.w_TOTACC+this.w_IMPABB
      this.w_IMPABB = 0
    else
      this.w_IMPABB = cp_ROUND(this.w_IMPABB, this.w_DECTOP)
    endif
    * --- Differenze Cambi
    this.w_IMPDCA = cp_ROUND(this.w_VAL2A, this.w_DECTOP) - cp_ROUND(this.w_VAL1A, this.w_DECTOP)
    this.w_IMPDCP = cp_ROUND(this.w_VAL2P, this.w_DECTOP) - cp_ROUND(this.w_VAL1P, this.w_DECTOP)
    WITH this.oParentObject.oParentObject
    .w_TOTDAR = .w_TOTDAR - .w_PNIMPDAR
    .w_TOTAVE = .w_TOTAVE - .w_PNIMPAVE
    .w_NUMACC = this.w_INDICE-1
    .w_TOTACC = this.w_TOTACC
    if this.w_FLAGAC<>"S"
      if this.w_IMPPNT>0
        .w_PNIMPDAR = this.w_IMPPNT
        .w_PNIMPAVE = 0
        .w_SLIMPDAR =IIF(.w_PNIMPDAR-.w_PNIMPAVE>=0,.w_PNIMPDAR-.w_PNIMPAVE,0)
        .w_IMPDAR =.w_PNIMPDAR
        .w_IMPAVE =0
        .w_SLIMPAVE =0
      else
        .w_PNIMPDAR = 0
        .w_PNIMPAVE = ABS(this.w_IMPPNT)
        .w_IMPDAR =0
        .w_IMPAVE =.w_PNIMPAVE
        .w_SLIMPDAR =0
        .w_SLIMPAVE =IIF(.w_PNIMPDAR-.w_PNIMPAVE<0,ABS(.w_PNIMPDAR-.w_PNIMPAVE),0)
      endif
    endif
    * --- Se ho riga a zero e devo creare abbuoni imposto relativo flag per salvare la riga del cliente
    *     con w_PTIMPABB<>0 utilizzato nel GSCG_BPK
    .w_PNFLZERO =IIF(.w_PNIMPDAR-.w_PNIMPAVE=0 and this.w_IMPABB<>0,"S",.w_PNFLZERO)
    .w_TOTDAR = .w_TOTDAR + .w_PNIMPDAR
    .w_TOTAVE = .w_TOTAVE + .w_PNIMPAVE
    .w_SBILAN = .w_TOTDAR-.w_TOTAVE
    * --- Disabilita gli Accessi Automatici successivi
    .w_PARSAL="."
    * --- Aggiorna Totalizzatore Automatismi
    .w_TOTFLAG=AGGFLAG(.w_FLAGTOT, .o_PNIMPDAR+.o_PNIMPAVE, .w_PNIMPDAR+.w_PNIMPAVE, .w_TOTFLAG)
    * --- Aggiorna gli <Old> per prevenire successivi Calcoli
    .o_PNIMPDAR = .w_PNIMPDAR
    .o_PNIMPAVE = .w_PNIMPAVE
    .TrsFromWork()
    * --- Flag Notifica Riga Variata
    SELECT (.cTrsName)
    if i_SRV<>"A"
      replace i_SRV with "U"
    endif
    SELECT ( this.w_PTSA.cTrsName )
    if this.w_IMPDCA<>0 OR this.w_IMPDCP<>0 OR this.w_IMPABB<>0 OR this.w_IMPABP <>0 OR this.w_IMPABA <>0 OR this.oParentObject.oParentObject.cFunction<>"Load"
      * --- Aggiorna Righe Primanota Diff.Cambi, Abbuoni, Acconti ...
       
 .w_IMPDCA = this.w_IMPDCA 
 .w_IMPDCP = this.w_IMPDCP 
 .w_IMPABB = this.w_IMPABB 
 .w_IMPPDP = IIF(this.w_FLAGAC="S",-this.w_IMPDCP,0) 
 .w_IMPPDA = IIF(this.w_FLAGAC="S",-this.w_IMPDCA,0) 
 .w_IMPABP=this.w_IMPABP 
 .w_IMPABA=this.w_IMPABA
      * --- Questo batch crea righe in prima nota dalla gestione partite (GSCG_MPA).
      *     Il problema � che le routine di interface manager considerano modificato
      *     il figlio della partite per le righe create dal batch invocato su GSCG_MPN allo scatenarsi
      *     dell'evento sotto.
      *     Questo fa si che le righe create, sebbene non gestiscano le partite, siano legate
      *     alla gestione partite attualmente aperta per quanto rigurada alcuni valori di base
      *     come ad esempio bUpdated.
      *     Se questo comando � tolto la procedura segnala campo obbligatorio sulal riga
      *     di abbuono, ad esempio, perch� la procedura ricerca il pagamento sulle
      *     partite della riaga anche se non ha partite.
      *     Settando onScreen a .f. le routine interpretano tutto correttamente.
      this.w_PADRE.bOnScreen = .f.
      .NotifyEvent("RigheDaPartite")
      this.w_PADRE.bOnScreen = .t.
    endif
    ENDWITH
    * --- Questa Parte derivata dal Metodo LoadRec
    this.w_PADRE.RePos(.T.)     
    * --- Chiudo la gestione chiusura partite da prima nota (GSCG_MSP)
    this.w_PTSA.cFunction = "Query"
    this.w_PTSA.ecpQuit()     
    this.w_PADRE.ecpSave()     
    this.w_PNOTA.w_FLAGAC = " "
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Differenze Cambio
    this.w_APPO1 = ABS(VAL2MON(this.oParentObject.w_PTTOTIMP, this.oParentObject.w_PTCAOAPE, this.w_CAONAZ, this.oParentObject.w_PTDATAPE, this.w_VALNAZ, this.w_DECTOP))
    this.w_APPO2 = ABS(VAL2MON(this.oParentObject.w_PTTOTIMP, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATRIF, this.w_VALNAZ, this.w_DECTOP))
    if this.oParentObject.w_PT_SEGNO="A"
      * --- Sezione AVERE (Incasso) 
      this.w_APPO1 = this.w_APPO1 * -1
      this.w_APPO2 = this.w_APPO2 * -1
      if this.oParentObject.w_PTCAOVAL<this.oParentObject.w_PTCAOAPE
        * --- Cambio Diminuito Aggiorna D.C. Attiva (tranne se PTTOTIMP Negativo
        if this.oParentObject.w_PTTOTIMP>0
          this.w_VAL1A = this.w_VAL1A + this.w_APPO1
          this.w_VAL2A = this.w_VAL2A + this.w_APPO2
        else
          this.w_VAL1P = this.w_VAL1P + this.w_APPO1
          this.w_VAL2P = this.w_VAL2P + this.w_APPO2
        endif
      else
        * --- Cambio Aumentato Aggiorna D.C. Passiva (tranne se PTTOTIMP Negativo
        if this.oParentObject.w_PTTOTIMP>0
          this.w_VAL1P = this.w_VAL1P + this.w_APPO1
          this.w_VAL2P = this.w_VAL2P + this.w_APPO2
        else
          this.w_VAL1A = this.w_VAL1A + this.w_APPO1
          this.w_VAL2A = this.w_VAL2A + this.w_APPO2
        endif
      endif
    else
      * --- Sezione DARE (Pagamento) 
      if this.oParentObject.w_PTCAOVAL<this.oParentObject.w_PTCAOAPE
        * --- Cambio Diminuito Aggiorna D.C. Passiva (tranne se PTTOTIMP Negativo
        if this.oParentObject.w_PTTOTIMP>0
          this.w_VAL1P = this.w_VAL1P + this.w_APPO1
          this.w_VAL2P = this.w_VAL2P + this.w_APPO2
        else
          this.w_VAL1A = this.w_VAL1A + this.w_APPO1
          this.w_VAL2A = this.w_VAL2A + this.w_APPO2
        endif
      else
        * --- Cambio Aumentato Aggiorna D.C. Attiva (tranne se PTTOTIMP Negativo
        if this.oParentObject.w_PTTOTIMP>0
          this.w_VAL1A = this.w_VAL1A + this.w_APPO1
          this.w_VAL2A = this.w_VAL2A + this.w_APPO2
        else
          this.w_VAL1P = this.w_VAL1P + this.w_APPO1
          this.w_VAL2P = this.w_VAL2P + this.w_APPO2
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COSAPAGA"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COSAPAGA;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPPAG = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Seleziono dai pagamenti la prima rata del tipo pagamento associato agli abbuoni e acconti nella tabella contropartite.
    * --- Select from PAG_2AME
    i_nConn=i_TableProp[this.PAG_2AME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select P2MODPAG  from "+i_cTable+" PAG_2AME ";
          +" where P2CODICE="+cp_ToStrODBC(this.w_TIPPAG)+"";
          +" order by P2NUMRAT";
           ,"_Curs_PAG_2AME")
    else
      select P2MODPAG from (i_cTable);
       where P2CODICE=this.w_TIPPAG;
       order by P2NUMRAT;
        into cursor _Curs_PAG_2AME
    endif
    if used('_Curs_PAG_2AME')
      select _Curs_PAG_2AME
      locate for 1=1
      do while not(eof())
      this.w_MODPAG = _Curs_PAG_2AME.P2MODPAG
        select _Curs_PAG_2AME
        continue
      enddo
      use
    endif
    SELECT (this.w_PADRE.cTrsName)
    if this.w_PNOTA.cFunction="Edit"
      this.w_PADRE.Initrow()     
      this.w_PADRE.Initson()     
    endif
    this.oParentObject.w_PTTOTIMP = this.w_VALACC
    this.oParentObject.w_PTDATSCA = this.w_DATREG
    this.oParentObject.w_PTNUMPAR = CANUMPAR("S", this.w_CODESE)
    this.oParentObject.w_PT_SEGNO = IIF( this.w_PTSA.w_TIPCON $ "C-G", "A", "D")
    this.oParentObject.w_PTTIPCON = this.w_PTSA.w_TIPCON
    this.oParentObject.w_PTCODCON = this.w_PTSA.w_CODCON
    this.oParentObject.w_PTMODPAG = this.w_MODPAG
    this.oParentObject.w_PTFLCRSA = "A"
    this.oParentObject.w_PTCODVAL = this.w_CODVAL
    this.oParentObject.w_PTCAOVAL = this.w_CAOVAL
    this.oParentObject.w_PTCAOAPE = this.w_CAOVAL
    this.oParentObject.w_PTDATAPE = this.w_DATREG
    this.oParentObject.w_PTNUMDOC = this.w_NUMDOC
    this.oParentObject.w_PTALFDOC = this.w_ALFDOC
    this.oParentObject.w_PTDATDOC = this.w_DATRIF
    this.oParentObject.w_PTDESRIG = this.w_DESRIG
    this.oParentObject.w_PTCODAGE = this.w_CODAGE
    this.oParentObject.w_PTTOTABB = 0
    this.oParentObject.w_PTIMPDOC = 0
    this.w_INIT = .F.
    * --- Calcola Importo Riga Primanota
    this.w_APPO = this.oParentObject.w_PTTOTIMP * IIF(this.oParentObject.w_PT_SEGNO="D", 1, -1)
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.w_APPO<>0
      * --- Se Valuta...
      this.w_APPO = cp_Round(VAL2MON(this.w_APPO, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.oParentObject.w_PTDATAPE,this.w_VALNAZ),this.w_DECTOP)
    endif
    this.w_IMPPNT = this.w_IMPPNT + this.w_APPO
    SELECT ( this.w_PADRE.cTrsName )
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.w_PADRE.TrsFromWork()     
    SELECT ( this.w_PTSA.cTrsName )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='PAG_2AME'
    this.cWorkTables[3]='PAR_TITE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PAG_2AME')
      use in _Curs_PAG_2AME
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
