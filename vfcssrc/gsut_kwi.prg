* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kwi                                                        *
*              Wizard integrazione Infinity D.M.S.                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-07                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kwi",oParentObject))

* --- Class definition
define class tgsut_kwi as StdForm
  Top    = 3
  Left   = 5

  * --- Standard Properties
  Width  = 537
  Height = 421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-26"
  HelpContextID=236371095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kwi"
  cComment = "Wizard integrazione Infinity D.M.S."
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_INSTDLL = space(1)
  w_CONFVAR = space(1)
  w_CONFUSER = space(1)
  w_VALIDATECLASS = space(1)
  w_EXPORTCLASS = space(1)
  w_IMPORTCLASS = space(1)
  w_CONVERTIND = space(1)
  w_Msg = space(0)
  w_CDWIZSEC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwiPag1","gsut_kwi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINSTDLL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_INSTDLL=space(1)
      .w_CONFVAR=space(1)
      .w_CONFUSER=space(1)
      .w_VALIDATECLASS=space(1)
      .w_EXPORTCLASS=space(1)
      .w_IMPORTCLASS=space(1)
      .w_CONVERTIND=space(1)
      .w_Msg=space(0)
      .w_CDWIZSEC=space(1)
        .w_INSTDLL = ' '
        .w_CONFVAR = ' '
        .w_CONFUSER = ' '
        .w_VALIDATECLASS = ' '
        .w_EXPORTCLASS = 'S'
        .w_IMPORTCLASS = 'S'
        .w_CONVERTIND = ' '
          .DoRTCalc(8,8,.f.)
        .w_CDWIZSEC = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SEDBCRGIDW()
    with this
          * --- inizializzazione flag
          GSUT_BWB(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_SEDBCRGIDW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINSTDLL_1_1.RadioValue()==this.w_INSTDLL)
      this.oPgFrm.Page1.oPag.oINSTDLL_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFVAR_1_2.RadioValue()==this.w_CONFVAR)
      this.oPgFrm.Page1.oPag.oCONFVAR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFUSER_1_3.RadioValue()==this.w_CONFUSER)
      this.oPgFrm.Page1.oPag.oCONFUSER_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALIDATECLASS_1_4.RadioValue()==this.w_VALIDATECLASS)
      this.oPgFrm.Page1.oPag.oVALIDATECLASS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXPORTCLASS_1_5.RadioValue()==this.w_EXPORTCLASS)
      this.oPgFrm.Page1.oPag.oEXPORTCLASS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPORTCLASS_1_6.RadioValue()==this.w_IMPORTCLASS)
      this.oPgFrm.Page1.oPag.oIMPORTCLASS_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONVERTIND_1_7.RadioValue()==this.w_CONVERTIND)
      this.oPgFrm.Page1.oPag.oCONVERTIND_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_10.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_10.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kwiPag1 as StdContainer
  Width  = 533
  height = 421
  stdWidth  = 533
  stdheight = 421
  resizeXpos=393
  resizeYpos=255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINSTDLL_1_1 as StdCheck with uid="WKCEBSKLAY",rtseq=1,rtrep=.f.,left=10, top=3, caption="Installa dll",;
    HelpContextID = 246431622,;
    cFormVar="w_INSTDLL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINSTDLL_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINSTDLL_1_1.GetRadio()
    this.Parent.oContained.w_INSTDLL = this.RadioValue()
    return .t.
  endfunc

  func oINSTDLL_1_1.SetRadio()
    this.Parent.oContained.w_INSTDLL=trim(this.Parent.oContained.w_INSTDLL)
    this.value = ;
      iif(this.Parent.oContained.w_INSTDLL=='S',1,;
      0)
  endfunc

  add object oCONFVAR_1_2 as StdCheck with uid="LFWAOPLAXP",rtseq=2,rtrep=.f.,left=10, top=26, caption="Configurazione variabili ambiente",;
    HelpContextID = 79818790,;
    cFormVar="w_CONFVAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFVAR_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFVAR_1_2.GetRadio()
    this.Parent.oContained.w_CONFVAR = this.RadioValue()
    return .t.
  endfunc

  func oCONFVAR_1_2.SetRadio()
    this.Parent.oContained.w_CONFVAR=trim(this.Parent.oContained.w_CONFVAR)
    this.value = ;
      iif(this.Parent.oContained.w_CONFVAR=='S',1,;
      0)
  endfunc

  add object oCONFUSER_1_3 as StdCheck with uid="VZBKCZNCFT",rtseq=3,rtrep=.f.,left=10, top=49, caption="Configurazione utenti per Infinity",;
    HelpContextID = 156110728,;
    cFormVar="w_CONFUSER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFUSER_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFUSER_1_3.GetRadio()
    this.Parent.oContained.w_CONFUSER = this.RadioValue()
    return .t.
  endfunc

  func oCONFUSER_1_3.SetRadio()
    this.Parent.oContained.w_CONFUSER=trim(this.Parent.oContained.w_CONFUSER)
    this.value = ;
      iif(this.Parent.oContained.w_CONFUSER=='S',1,;
      0)
  endfunc

  add object oVALIDATECLASS_1_4 as StdCheck with uid="RQVTFBGBZV",rtseq=4,rtrep=.f.,left=10, top=72, caption="Aggiornamento classi e mapping attributi per Infinity D.M.S.",;
    HelpContextID = 114547765,;
    cFormVar="w_VALIDATECLASS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVALIDATECLASS_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVALIDATECLASS_1_4.GetRadio()
    this.Parent.oContained.w_VALIDATECLASS = this.RadioValue()
    return .t.
  endfunc

  func oVALIDATECLASS_1_4.SetRadio()
    this.Parent.oContained.w_VALIDATECLASS=trim(this.Parent.oContained.w_VALIDATECLASS)
    this.value = ;
      iif(this.Parent.oContained.w_VALIDATECLASS=='S',1,;
      0)
  endfunc

  add object oEXPORTCLASS_1_5 as StdCheck with uid="FJPFDSMNXX",rtseq=5,rtrep=.f.,left=10, top=95, caption="Export classi e attributi documentali",;
    HelpContextID = 141516638,;
    cFormVar="w_EXPORTCLASS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXPORTCLASS_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEXPORTCLASS_1_5.GetRadio()
    this.Parent.oContained.w_EXPORTCLASS = this.RadioValue()
    return .t.
  endfunc

  func oEXPORTCLASS_1_5.SetRadio()
    this.Parent.oContained.w_EXPORTCLASS=trim(this.Parent.oContained.w_EXPORTCLASS)
    this.value = ;
      iif(this.Parent.oContained.w_EXPORTCLASS=='S',1,;
      0)
  endfunc

  add object oIMPORTCLASS_1_6 as StdCheck with uid="NUKYEUHVWN",rtseq=6,rtrep=.f.,left=10, top=118, caption="Import classi e attributi documentali",;
    HelpContextID = 141519390,;
    cFormVar="w_IMPORTCLASS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMPORTCLASS_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oIMPORTCLASS_1_6.GetRadio()
    this.Parent.oContained.w_IMPORTCLASS = this.RadioValue()
    return .t.
  endfunc

  func oIMPORTCLASS_1_6.SetRadio()
    this.Parent.oContained.w_IMPORTCLASS=trim(this.Parent.oContained.w_IMPORTCLASS)
    this.value = ;
      iif(this.Parent.oContained.w_IMPORTCLASS=='S',1,;
      0)
  endfunc

  add object oCONVERTIND_1_7 as StdCheck with uid="VVFPQUKYIY",rtseq=7,rtrep=.f.,left=10, top=141, caption="Conversione indici documentali",;
    HelpContextID = 188597937,;
    cFormVar="w_CONVERTIND", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONVERTIND_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONVERTIND_1_7.GetRadio()
    this.Parent.oContained.w_CONVERTIND = this.RadioValue()
    return .t.
  endfunc

  func oCONVERTIND_1_7.SetRadio()
    this.Parent.oContained.w_CONVERTIND=trim(this.Parent.oContained.w_CONVERTIND)
    this.value = ;
      iif(this.Parent.oContained.w_CONVERTIND=='S',1,;
      0)
  endfunc


  add object oBtn_1_8 as StdButton with uid="ZMIZZNTUDJ",left=428, top=372, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la procedura";
    , HelpContextID = 236391654;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSUT_BWI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="JWIBHYOYEX",left=478, top=372, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 243688518;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_10 as StdMemo with uid="ATBBCHAJSX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 236823750,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=183, Width=518, Left=10, Top=186, tabstop = .f., readonly = .t.

  add object oStr_1_11 as StdString with uid="SNFDZXNVYJ",Visible=.t., Left=10, Top=168,;
    Alignment=0, Width=142, Height=18,;
    Caption="Messaggi elaborazione"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kwi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
