* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bzn                                                        *
*              Visualizzazione note prov.                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-01-21                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bzn",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bzn as StdBatch
  * --- Local variables
  pOper = space(5)
  w_NUMREG = 0
  w_NUMNOTA = 0
  w_ALFANOTA = space(2)
  w_ANNO = space(4)
  w_AGENTE = space(5)
  w_DATNOTA = ctod("  /  /  ")
  w_ZOOM = space(10)
  w_OK = .f.
  * --- WorkFile variables
  PAR_TITE_idx=0
  PRO_LIQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le date pagamento nelle provvigioni liquidate
    this.w_ZOOM = this.oParentObject.w_ZoomNpr
    do case
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="AGGIO"
        * --- Aggiorna data pagamento
        this.w_OK = .F.
        NC = this.w_ZOOM.cCursor
        vq_exec("query\GSVE0SZN.VQR",this,"TEMP")
        * --- Try
        local bErr_03BA82E0
        bErr_03BA82E0=bTrsErr
        this.Try_03BA82E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_errormsg("Errore durante aggiornamento. Operazione abbandonata")
        endif
        bTrsErr=bTrsErr or bErr_03BA82E0
        * --- End
    endcase
    if this.w_OK=.T.
      AH_ERRORMSG("Aggiornamento completato")
    endif
    if used("TEMP")
      select TEMP
      use
    endif
  endproc
  proc Try_03BA82E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_AGENTE = PLCODAGE
    this.w_NUMNOTA = PLNUMNOT
    this.w_ALFANOTA = PLALFNOT
    this.w_DATNOTA = PLDATNOT
    SELECT TEMP
    GO TOP
    SCAN FOR CODAGE=this.w_AGENTE AND PLNUMNOT=this.w_NUMNOTA AND PLALFNOT=this.w_ALFANOTA AND PLDATNOT=this.w_DATNOTA
    this.w_NUMREG = TEMP.PLNUMREG
    this.w_ANNO = TEMP.PL__ANNO
    * --- Write into PRO_LIQU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PLDATPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATPAG),'PRO_LIQU','PLDATPAG');
          +i_ccchkf ;
      +" where ";
          +"PLNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
          +" and PL__ANNO = "+cp_ToStrODBC(this.w_ANNO);
             )
    else
      update (i_cTable) set;
          PLDATPAG = this.oParentObject.w_DATPAG;
          &i_ccchkf. ;
       where;
          PLNUMREG = this.w_NUMREG;
          and PL__ANNO = this.w_ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT TEMP
    ENDSCAN
    this.w_OK = .T.
    SELECT &NC
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='PRO_LIQU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
