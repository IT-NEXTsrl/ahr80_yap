* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgc                                                        *
*              Load\save mod.gest.clienti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-02                                                      *
* Last revis.: 2014-02-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_MODE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgc",oParentObject,m.w_MODE)
return(i_retval)

define class tgsut_bgc as StdBatch
  * --- Local variables
  w_MODE = space(10)
  w_FILEINI = space(10)
  w_STRLAV = space(10)
  w_HANFILE = 0
  w_CHAR = space(1)
  w_TIPODB = space(10)
  indice = 0
  w_CRIPTA = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro
    * --- Variabili globali
    * --- Variabili locali
    * --- Apertura file MGC_INF.INI
    this.w_FILEINI = sys(5)+sys(2003)+"\MGC_INF.INI"
    do case
      case this.w_MODE = "load"
        this.w_HANFILE = FOPEN(this.w_FILEINI,0)
        if (this.w_HANFILE = -1)
          * --- Il file non esiste - Scrivo il default e esco
          this.oParentObject.w_DBMS = "F"
          this.oParentObject.w_SERVER = ""
          this.oParentObject.w_DATABASE = ""
          this.oParentObject.w_USER_ID = ""
          this.oParentObject.w_PASSWORD = ""
          return
        endif
        * --- Leggo il file di inizializzazione - DBMS
        this.w_STRLAV = FGETS(this.w_HANFILE)
        this.oParentObject.w_DBMS = SUBSTR(this.w_STRLAV,6,1)
        * --- Leggo il file di inizializzazione - SERVER
        this.w_STRLAV = FGETS(this.w_HANFILE)
        this.oParentObject.w_SERVER = RIGHT(this.w_STRLAV,LEN(this.w_STRLAV)-7)
        * --- Leggo il file di inizializzazione - DATABASE
        this.w_STRLAV = FGETS(this.w_HANFILE)
        this.oParentObject.w_DATABASE = RIGHT(this.w_STRLAV,LEN(this.w_STRLAV)-9)
        * --- Leggo il file di inizializzazione - USER_ID
        this.w_STRLAV = FGETS(this.w_HANFILE)
        this.oParentObject.w_USER_ID = RIGHT(this.w_STRLAV,LEN(this.w_STRLAV)-8)
        * --- Leggo il file di inizializzazione - PASSWORD o PASSWORD_CRT
        this.w_STRLAV = FGETS(this.w_HANFILE)
        if SUBSTR(this.w_STRLAV,9,1) = "="
          this.w_CRIPTA = RIGHT(this.w_STRLAV,LEN(this.w_STRLAV)-9)
          * --- Non decript password
          this.oParentObject.w_PASSWORD = this.w_CRIPTA
        else
          this.w_CRIPTA = RIGHT(this.w_STRLAV,LEN(this.w_STRLAV)-13)
          * --- Decript password
          this.indice = 1
          this.oParentObject.w_PASSWORD = ""
          do while this.indice <= LEN(RTRIM(this.w_CRIPTA))
            this.w_CHAR = SUBSTR(RTRIM(this.w_CRIPTA),this.indice,1)
            this.w_CHAR = CHR((ASC(this.w_CHAR)-this.indice) % 255)
            this.oParentObject.w_PASSWORD = this.oParentObject.w_PASSWORD+this.w_CHAR
            this.indice = this.indice+1
          enddo
        endif
        FCLOSE(this.w_HANFILE)
      case this.w_MODE = "save"
        this.w_HANFILE = FCREATE(this.w_FILEINI,0)
        if (this.w_HANFILE = -1)
          ah_errormsg("Errore durante la creazione del file MGC_INI.INI",16)
          return
        endif
        * --- Scrivo il file di inizializzazione - DBMS
        do case
          case this.oParentObject.w_DBMS="F"
            this.w_TIPODB = "FOXPRO"
          case this.oParentObject.w_DBMS="S"
            this.w_TIPODB = "SQLSERVER"
          case this.oParentObject.w_DBMS="O"
            this.w_TIPODB = "ORACLE"
          case this.oParentObject.w_DBMS="D"
            this.w_TIPODB = "DB2"
          case this.oParentObject.w_DBMS="P"
            this.w_TIPODB = "POSTGRESQL"
        endcase
        this.w_STRLAV = "DBMS="+this.w_TIPODB
        FPUTS(this.w_HANFILE, this.w_STRLAV)
        * --- Scrivo il file di inizializzazione - SERVER
        this.w_STRLAV = "SERVER="+RTRIM(this.oParentObject.w_SERVER)
        FPUTS(this.w_HANFILE, this.w_STRLAV)
        * --- Scrivo il file di inizializzazione - DATABASE
        this.w_STRLAV = "DATABASE="+RTRIM(this.oParentObject.w_DATABASE)
        FPUTS(this.w_HANFILE, this.w_STRLAV)
        * --- Scrivo il file di inizializzazione - USER_ID
        this.w_STRLAV = "USER_ID="+RTRIM(this.oParentObject.w_USER_ID)
        FPUTS(this.w_HANFILE, this.w_STRLAV)
        * --- cript password
        this.indice = 1
        this.w_CRIPTA = ""
        do while this.indice <= LEN(RTRIM(this.oParentObject.w_PASSWORD))
          this.w_CHAR = SUBSTR(RTRIM(this.oParentObject.w_PASSWORD),this.indice,1)
          this.w_CHAR = CHR((ASC(this.w_CHAR)+this.indice) % 255)
          this.w_CRIPTA = this.w_CRIPTA+this.w_CHAR
          this.indice = this.indice+1
        enddo
        * --- Scrivo il file di inizializzazione - PASSWORD
        this.w_STRLAV = "PASSWORD_CRT="+RTRIM(this.w_CRIPTA)
        FPUTS(this.w_HANFILE, this.w_STRLAV)
        * --- Chiusura del file
        FCLOSE(this.w_HANFILE)
    endcase
  endproc


  proc Init(oParentObject,w_MODE)
    this.w_MODE=w_MODE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_MODE"
endproc
