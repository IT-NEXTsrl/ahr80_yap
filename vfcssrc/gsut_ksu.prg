* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ksu                                                        *
*              Politiche accessi utenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_303]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-28                                                      *
* Last revis.: 2013-12-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_ksu
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.)
  Ah_ERRORMSG("Accesso negato",'stop',"Gestione sicurezza")
  return null
endif
* --- Fine Area Manuale
return(createobject("tgsut_ksu",oParentObject))

* --- Class definition
define class tgsut_ksu as StdForm
  Top    = 5
  Left   = 8

  * --- Standard Properties
  Width  = 794
  Height = 480
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-10"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  TSMENUVO_IDX = 0
  CPGROUPS_IDX = 0
  CPUSERS_IDX = 0
  ELENMODU_IDX = 0
  cPrg = "gsut_ksu"
  cComment = "Politiche accessi utenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_PARAM2 = space(1)
  w_PARAM4 = space(2)
  w_CODREL = space(10)
  w_CODUTE = 0
  w_CODGRU = 0
  w_VISUAL = 0
  w_CARICA = 0
  w_MODIF = 0
  w_ERASE = 0
  w_SELEZ = space(1)
  w_DESCGR = space(20)
  w_PRGSEL = space(50)
  w_UTESEL = 0
  w_DPRGSEL = space(50)
  w_DESUTE = space(20)
  w_USRCODE = 0
  w_PROGNAME = space(50)
  w_DUTESEL = space(20)
  w_UTEGRP = space(1)
  w_PROGDES = space(50)
  w_PRNUMERO = 0
  w_PROGRAM = space(20)
  w_PROGDES = space(50)
  w_PRPARAME = space(15)
  w_MODULO2 = space(2)
  w_GESTIONI = space(2)
  w_MODULO = space(4)
  w_DESCGR = space(20)
  w_PRGSEL = space(50)
  w_GRPSEL = 0
  w_DESGRP = space(20)
  w_PARAM3 = space(2)
  w_RECNO = 0
  w_ZOOMENU = .NULL.
  w_ZOOMENG = .NULL.
  w_ZOOMEN2 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ksuPag1","gsut_ksu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMENU = this.oPgFrm.Pages(1).oPag.ZOOMENU
    this.w_ZOOMENG = this.oPgFrm.Pages(1).oPag.ZOOMENG
    this.w_ZOOMEN2 = this.oPgFrm.Pages(1).oPag.ZOOMEN2
    DoDefault()
    proc Destroy()
      this.w_ZOOMENU = .NULL.
      this.w_ZOOMENG = .NULL.
      this.w_ZOOMEN2 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TSMENUVO'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='ELENMODU'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_PARAM2=space(1)
      .w_PARAM4=space(2)
      .w_CODREL=space(10)
      .w_CODUTE=0
      .w_CODGRU=0
      .w_VISUAL=0
      .w_CARICA=0
      .w_MODIF=0
      .w_ERASE=0
      .w_SELEZ=space(1)
      .w_DESCGR=space(20)
      .w_PRGSEL=space(50)
      .w_UTESEL=0
      .w_DPRGSEL=space(50)
      .w_DESUTE=space(20)
      .w_USRCODE=0
      .w_PROGNAME=space(50)
      .w_DUTESEL=space(20)
      .w_UTEGRP=space(1)
      .w_PROGDES=space(50)
      .w_PRNUMERO=0
      .w_PROGRAM=space(20)
      .w_PROGDES=space(50)
      .w_PRPARAME=space(15)
      .w_MODULO2=space(2)
      .w_GESTIONI=space(2)
      .w_MODULO=space(4)
      .w_DESCGR=space(20)
      .w_PRGSEL=space(50)
      .w_GRPSEL=0
      .w_DESGRP=space(20)
      .w_PARAM3=space(2)
      .w_RECNO=0
        .w_PARAM = substr(this.oParentObject,1,1)
        .w_PARAM2 = iif(substr(this.oParentObject,2,1)='R', 'R', 'N')
        .w_PARAM4 = iif(substr(this.oParentObject,3,1)='M', 'M', ' ')
        .w_CODREL = alltrim(strtran(upper(g_VERSION),'REL.'))
      .oPgFrm.Page1.oPag.ZOOMENU.Calculate()
      .oPgFrm.Page1.oPag.ZOOMENG.Calculate()
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODUTE))
          .link_1_7('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODGRU))
          .link_1_8('Full')
        endif
        .w_VISUAL = -1000
        .w_CARICA = -1000
        .w_MODIF = -1000
        .w_ERASE = -1000
        .w_SELEZ = 'D'
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(12,12,.f.)
        .w_PRGSEL = iif(not .w_PARAM <> "U" or .w_PARAM2="R", nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),space(50)), nvl(.w_ZOOMENG.GETVAR('PROGRAM1'),space(50)))
        .w_UTESEL = nvl(.w_ZOOMENU.GETVAR('CODE'),0)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_UTESEL))
          .link_1_29('Full')
        endif
        .w_DPRGSEL = nvl(.w_ZOOMENU.GETVAR('PROGDES'),space(50))
          .DoRTCalc(16,16,.f.)
        .w_USRCODE = nvl(.w_ZOOMENU.GETVAR('CODE'),0)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_USRCODE))
          .link_1_32('Full')
        endif
        .w_PROGNAME = nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),0)
          .DoRTCalc(19,19,.f.)
        .w_UTEGRP = .w_PARAM
          .DoRTCalc(21,21,.f.)
        .w_PRNUMERO = .w_zoomenu.getvar('PRNUMERO')
        .w_PROGRAM = .w_zoomenu.getvar('PROGRAM1')
        .w_PROGDES = .w_zoomenu.getvar('PROGDES')
        .w_PRPARAME = .w_zoomenu.getvar('PARAMETRO')
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .w_MODULO2 = "0000"
        .w_GESTIONI = IIF(.w_PARAM2<>'R' AND .w_PARAM4="M","C","T")
        .w_MODULO = LEFT(IIF(.w_MODULO2="0000","    ",.w_MODULO2) + SPACE( 4 ) , 4 )
          .DoRTCalc(29,29,.f.)
        .w_PRGSEL = iif(not .w_PARAM <> "U" or .w_PARAM2="R", nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),space(50)), nvl(.w_ZOOMENG.GETVAR('PROGRAM1'),space(50)))
        .w_GRPSEL = nvl(.w_ZOOMENG.GETVAR('CODE'),0)
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_GRPSEL))
          .link_1_55('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_PARAM3 = left(this.oParentObject,2)
        .w_RECNO = RECNO(.w_ZOOMENU.cCURSOR)
      .oPgFrm.Page1.oPag.ZOOMEN2.Calculate(.w_RECNO)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_ksu
    * TRADUCE LE DESCRIZIONI DEI MODULI
    DO GSUT_BMA WITH THIS
    
    * --- derivata dalla classe combo da tabella
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_MODULO2")
    CTRL_CONCON.Popola()
    * --- valorizzo a tutti la combo...
    this.SetControlsValue()
    
    * --- RENDE VISIBILI O INVISIBILI I VARI ZOOM
    IF THIS.w_PARAM = "U"
      THIS.w_ZOOMENU.VISIBLE = .T.
      THIS.w_ZOOMEN2.VISIBLE = .T.
      THIS.w_ZOOMENG.VISIBLE = .F.
      IF THIS.w_PARAM4 = "M"
        THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi utenti mobile")
      ELSE
        THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi utenti")
      ENDIF
    ENDIF
    IF THIS.w_PARAM = "G"
      THIS.w_ZOOMENU.VISIBLE = .F.
      THIS.w_ZOOMEN2.VISIBLE = .F.
      THIS.w_ZOOMENG.VISIBLE = .T.
      if THIS.w_PARAM2 = "R"
        IF THIS.w_PARAM4 = "M"
          THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi gruppi mobile per modulo")    
        ELSE
          THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi gruppi per modulo")
        ENDIF
      else
        IF THIS.w_PARAM4 = "M"
          THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi gruppi mobile")    
        ELSE
          THIS.cCOMMENT=AH_MSGFORMAT("Politiche accessi gruppi")
        ENDIF
      endif
    ENDIF
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMENU.Calculate()
        .oPgFrm.Page1.oPag.ZOOMENG.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(1,12,.t.)
            .w_PRGSEL = iif(not .w_PARAM <> "U" or .w_PARAM2="R", nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),space(50)), nvl(.w_ZOOMENG.GETVAR('PROGRAM1'),space(50)))
            .w_UTESEL = nvl(.w_ZOOMENU.GETVAR('CODE'),0)
          .link_1_29('Full')
            .w_DPRGSEL = nvl(.w_ZOOMENU.GETVAR('PROGDES'),space(50))
        .DoRTCalc(16,16,.t.)
            .w_USRCODE = nvl(.w_ZOOMENU.GETVAR('CODE'),0)
          .link_1_32('Full')
            .w_PROGNAME = nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),0)
        .DoRTCalc(19,21,.t.)
            .w_PRNUMERO = .w_zoomenu.getvar('PRNUMERO')
            .w_PROGRAM = .w_zoomenu.getvar('PROGRAM1')
            .w_PROGDES = .w_zoomenu.getvar('PROGDES')
            .w_PRPARAME = .w_zoomenu.getvar('PARAMETRO')
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .DoRTCalc(26,27,.t.)
            .w_MODULO = LEFT(IIF(.w_MODULO2="0000","    ",.w_MODULO2) + SPACE( 4 ) , 4 )
        .DoRTCalc(29,29,.t.)
            .w_PRGSEL = iif(not .w_PARAM <> "U" or .w_PARAM2="R", nvl(.w_ZOOMENU.GETVAR('PROGRAM1'),space(50)), nvl(.w_ZOOMENG.GETVAR('PROGRAM1'),space(50)))
            .w_GRPSEL = nvl(.w_ZOOMENG.GETVAR('CODE'),0)
          .link_1_55('Full')
        .DoRTCalc(32,33,.t.)
            .w_RECNO = RECNO(.w_ZOOMENU.cCURSOR)
        .oPgFrm.Page1.oPag.ZOOMEN2.Calculate(.w_RECNO)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMENU.Calculate()
        .oPgFrm.Page1.oPag.ZOOMENG.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.ZOOMEN2.Calculate(.w_RECNO)
    endwith
  return

  proc Calculate_REGEONNSZY()
    with this
          * --- Gestione ELENMODU
          GSUT_BMO(this;
              ,'P';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGESTIONI_1_50.enabled = this.oPgFrm.Page1.oPag.oGESTIONI_1_50.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODUTE_1_7.visible=!this.oPgFrm.Page1.oPag.oCODUTE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODGRU_1_8.visible=!this.oPgFrm.Page1.oPag.oCODGRU_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDESCGR_1_17.visible=!this.oPgFrm.Page1.oPag.oDESCGR_1_17.mHide()
    this.oPgFrm.Page1.oPag.oPRGSEL_1_28.visible=!this.oPgFrm.Page1.oPag.oPRGSEL_1_28.mHide()
    this.oPgFrm.Page1.oPag.oUTESEL_1_29.visible=!this.oPgFrm.Page1.oPag.oUTESEL_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESUTE_1_31.visible=!this.oPgFrm.Page1.oPag.oDESUTE_1_31.mHide()
    this.oPgFrm.Page1.oPag.oUSRCODE_1_32.visible=!this.oPgFrm.Page1.oPag.oUSRCODE_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oPROGNAME_1_34.visible=!this.oPgFrm.Page1.oPag.oPROGNAME_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDUTESEL_1_36.visible=!this.oPgFrm.Page1.oPag.oDUTESEL_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oPROGDES_1_41.visible=!this.oPgFrm.Page1.oPag.oPROGDES_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oGESTIONI_1_50.visible=!this.oPgFrm.Page1.oPag.oGESTIONI_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oDESCGR_1_53.visible=!this.oPgFrm.Page1.oPag.oDESCGR_1_53.mHide()
    this.oPgFrm.Page1.oPag.oPRGSEL_1_54.visible=!this.oPgFrm.Page1.oPag.oPRGSEL_1_54.mHide()
    this.oPgFrm.Page1.oPag.oGRPSEL_1_55.visible=!this.oPgFrm.Page1.oPag.oGRPSEL_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDESGRP_1_56.visible=!this.oPgFrm.Page1.oPag.oDESGRP_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMENU.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMENG.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_REGEONNSZY()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZOOMEN2.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_7'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_DESCGR = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESCGR = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRU);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRU)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oCODGRU_1_8'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRU)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.CODE,0)
      this.w_DESCGR = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = 0
      endif
      this.w_DESCGR = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTESEL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTESEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTESEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTESEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTESEL)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTESEL = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTESEL = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTESEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=USRCODE
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_USRCODE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_USRCODE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_USRCODE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_USRCODE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_USRCODE = NVL(_Link_.CODE,0)
      this.w_DUTESEL = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_USRCODE = 0
      endif
      this.w_DUTESEL = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_USRCODE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRPSEL
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRPSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRPSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_GRPSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_GRPSEL)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRPSEL = NVL(_Link_.CODE,0)
      this.w_DESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_GRPSEL = 0
      endif
      this.w_DESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRPSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_7.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_7.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_8.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_8.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oVISUAL_1_9.RadioValue()==this.w_VISUAL)
      this.oPgFrm.Page1.oPag.oVISUAL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCARICA_1_10.RadioValue()==this.w_CARICA)
      this.oPgFrm.Page1.oPag.oCARICA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODIF_1_11.RadioValue()==this.w_MODIF)
      this.oPgFrm.Page1.oPag.oMODIF_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oERASE_1_12.RadioValue()==this.w_ERASE)
      this.oPgFrm.Page1.oPag.oERASE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_14.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGR_1_17.value==this.w_DESCGR)
      this.oPgFrm.Page1.oPag.oDESCGR_1_17.value=this.w_DESCGR
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGSEL_1_28.value==this.w_PRGSEL)
      this.oPgFrm.Page1.oPag.oPRGSEL_1_28.value=this.w_PRGSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oUTESEL_1_29.value==this.w_UTESEL)
      this.oPgFrm.Page1.oPag.oUTESEL_1_29.value=this.w_UTESEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_31.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_31.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUSRCODE_1_32.value==this.w_USRCODE)
      this.oPgFrm.Page1.oPag.oUSRCODE_1_32.value=this.w_USRCODE
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGNAME_1_34.value==this.w_PROGNAME)
      this.oPgFrm.Page1.oPag.oPROGNAME_1_34.value=this.w_PROGNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oDUTESEL_1_36.value==this.w_DUTESEL)
      this.oPgFrm.Page1.oPag.oDUTESEL_1_36.value=this.w_DUTESEL
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGDES_1_41.value==this.w_PROGDES)
      this.oPgFrm.Page1.oPag.oPROGDES_1_41.value=this.w_PROGDES
    endif
    if not(this.oPgFrm.Page1.oPag.oMODULO2_1_49.RadioValue()==this.w_MODULO2)
      this.oPgFrm.Page1.oPag.oMODULO2_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGESTIONI_1_50.RadioValue()==this.w_GESTIONI)
      this.oPgFrm.Page1.oPag.oGESTIONI_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGR_1_53.value==this.w_DESCGR)
      this.oPgFrm.Page1.oPag.oDESCGR_1_53.value=this.w_DESCGR
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGSEL_1_54.value==this.w_PRGSEL)
      this.oPgFrm.Page1.oPag.oPRGSEL_1_54.value=this.w_PRGSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oGRPSEL_1_55.value==this.w_GRPSEL)
      this.oPgFrm.Page1.oPag.oGRPSEL_1_55.value=this.w_GRPSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_1_56.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_1_56.value=this.w_DESGRP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_ksuPag1 as StdContainer
  Width  = 790
  height = 480
  stdWidth  = 790
  stdheight = 480
  resizeXpos=533
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMENU as cp_szoombox with uid="YMOEGUYBFY",left=-4, top=58, width=795,height=204,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSUT_KSU",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="TSMENUVO",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Carica",;
    nPag=1;
    , HelpContextID = 78824422


  add object ZOOMENG as cp_szoombox with uid="LZHZXYKPTC",left=-4, top=58, width=795,height=331,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSUT_KVO",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="TSMENUVO",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Carica",;
    nPag=1;
    , HelpContextID = 78824422

  add object oCODUTE_1_7 as StdField with uid="VOTYXSNJNC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente, per filtro selezione",;
    HelpContextID = 78663718,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=375, Top=3, cSayPict='"@Z 9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_7.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  func oCODUTE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCODGRU_1_8 as StdField with uid="OCXHLNRQEZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo, per filtro selezione",;
    HelpContextID = 75649062,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=375, Top=3, cSayPict='"@Z 9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_8.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc

  func oCODGRU_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oCODGRU_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc


  add object oVISUAL_1_9 as StdCombo with uid="VTTHBNWTHO",value=2,rtseq=7,rtrep=.f.,left=266,top=443,width=86,height=21;
    , HelpContextID = 176241494;
    , cFormVar="w_VISUAL",RowSource=""+"Accede,"+"Non accede,"+"No tassativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVISUAL_1_9.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    iif(this.value =3,-1000,;
    0))))
  endfunc
  func oVISUAL_1_9.GetRadio()
    this.Parent.oContained.w_VISUAL = this.RadioValue()
    return .t.
  endfunc

  func oVISUAL_1_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_VISUAL==1,1,;
      iif(this.Parent.oContained.w_VISUAL==0,2,;
      iif(this.Parent.oContained.w_VISUAL==-1000,3,;
      0)))
  endfunc


  add object oCARICA_1_10 as StdCombo with uid="UGWNYDZSVS",value=2,rtseq=8,rtrep=.f.,left=356,top=443,width=86,height=21;
    , HelpContextID = 261431846;
    , cFormVar="w_CARICA",RowSource=""+"Accede,"+"Non accede,"+"No tassativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARICA_1_10.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    iif(this.value =3,-1000,;
    0))))
  endfunc
  func oCARICA_1_10.GetRadio()
    this.Parent.oContained.w_CARICA = this.RadioValue()
    return .t.
  endfunc

  func oCARICA_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CARICA==1,1,;
      iif(this.Parent.oContained.w_CARICA==0,2,;
      iif(this.Parent.oContained.w_CARICA==-1000,3,;
      0)))
  endfunc


  add object oMODIF_1_11 as StdCombo with uid="PSWNSVVFPT",value=2,rtseq=9,rtrep=.f.,left=446,top=443,width=86,height=21;
    , HelpContextID = 20688698;
    , cFormVar="w_MODIF",RowSource=""+"Accede,"+"Non accede,"+"No tassativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMODIF_1_11.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    iif(this.value =3,-1000,;
    0))))
  endfunc
  func oMODIF_1_11.GetRadio()
    this.Parent.oContained.w_MODIF = this.RadioValue()
    return .t.
  endfunc

  func oMODIF_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MODIF==1,1,;
      iif(this.Parent.oContained.w_MODIF==0,2,;
      iif(this.Parent.oContained.w_MODIF==-1000,3,;
      0)))
  endfunc


  add object oERASE_1_12 as StdCombo with uid="YGXVENCPZK",value=2,rtseq=10,rtrep=.f.,left=536,top=443,width=86,height=21;
    , HelpContextID = 21093562;
    , cFormVar="w_ERASE",RowSource=""+"Accede,"+"Non accede,"+"No tassativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oERASE_1_12.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    iif(this.value =3,-1000,;
    0))))
  endfunc
  func oERASE_1_12.GetRadio()
    this.Parent.oContained.w_ERASE = this.RadioValue()
    return .t.
  endfunc

  func oERASE_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ERASE==1,1,;
      iif(this.Parent.oContained.w_ERASE==0,2,;
      iif(this.Parent.oContained.w_ERASE==-1000,3,;
      0)))
  endfunc


  add object oBtn_1_13 as StdButton with uid="GHSZNZLJNF",left=737, top=1, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 99173130;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        gsut_bme(this.Parent.oContained,"INT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ_1_14 as StdRadio with uid="HNXVSCEFDD",rtseq=11,rtrep=.f.,left=4, top=428, width=146,height=34;
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 268384474
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 268384474
      this.Buttons(2).Top=16
      this.SetAll("Width",144)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZ_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ_1_14.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_14.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='D',2,;
      0))
  endfunc


  add object oBtn_1_15 as StdButton with uid="MJIKIMPCXE",left=683, top=425, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i permessi per le funzioni selezionate";
    , HelpContextID = 99173130;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        gsut_bme(this.Parent.oContained,"MOD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCGR_1_17 as StdField with uid="JQDYFPGTJR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCGR", cQueryName = "DESCGR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 13579830,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=433, Top=3, InputMask=replicate('X',20)

  func oDESCGR_1_17.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc


  add object oObj_1_19 as cp_runprogram with uid="VTXWXWTXKL",left=9, top=509, width=157,height=35,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BME('SEL')",;
    cEvent = "w_SELEZ Changed",;
    nPag=1;
    , HelpContextID = 78824422


  add object oBtn_1_20 as StdButton with uid="IPVICJNRQW",left=737, top=425, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire.";
    , HelpContextID = 99173130;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="YJZEJFISFX",left=629, top=425, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare gli elementi selezionati";
    , HelpContextID = 99173130;
    , caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSUT_BME(this.Parent.oContained,"CAN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRGSEL_1_28 as StdField with uid="BENCUKSLPN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PRGSEL", cQueryName = "PRGSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 180257782,;
   bGlobalFont=.t.,;
    Height=21, Width=143, Left=643, Top=262, InputMask=replicate('X',50)

  func oPRGSEL_1_28.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U" or .w_PARAM2="R")
    endwith
  endfunc

  add object oUTESEL_1_29 as StdField with uid="PFEOHNVPDW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_UTESEL", cQueryName = "UTESEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 180250182,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=407, Top=262, cSayPict='"@Z 99999"', cGetPict='"@Z 99999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UTESEL"

  func oUTESEL_1_29.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  func oUTESEL_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUTE_1_31 as StdField with uid="GWHQSPYARW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 78722614,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=456, Top=262, InputMask=replicate('X',20)

  func oDESUTE_1_31.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oUSRCODE_1_32 as StdField with uid="FMYUMAEGHT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_USRCODE", cQueryName = "USRCODE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente selezionato",;
    HelpContextID = 55522630,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=97, Top=344, cSayPict='"@Z 99999"', cGetPict='"@Z 99999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_USRCODE"

  func oUSRCODE_1_32.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  func oUSRCODE_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPROGNAME_1_34 as StdField with uid="TFQVFUVZBZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PROGNAME", cQueryName = "PROGNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 264043461,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=97, Top=369, InputMask=replicate('X',50)

  func oPROGNAME_1_34.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oDUTESEL_1_36 as StdField with uid="HDMXSGUHYB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DUTESEL", cQueryName = "DUTESEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 191801802,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=147, Top=344, InputMask=replicate('X',20)

  func oDUTESEL_1_36.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oPROGDES_1_41 as StdField with uid="HGHIMAPBLU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PROGDES", cQueryName = "PROGDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 61015030,;
   bGlobalFont=.t.,;
    Height=21, Width=203, Left=97, Top=393, InputMask=replicate('X',50)

  func oPROGDES_1_41.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc


  add object oObj_1_48 as cp_runprogram with uid="LOKZCRYCLO",left=268, top=538, width=254,height=22,;
    caption='gsut_bme(VAR)',;
   bGlobalFont=.t.,;
    prg="gsut_bme('VAR')",;
    cEvent = "w_zoomenu selected,w_ZOOMENG selected",;
    nPag=1;
    , HelpContextID = 90763083


  add object oMODULO2_1_49 as StdZTamTableCombo with uid="YGTRMCJOAH",rtseq=26,rtrep=.f.,left=84,top=2,width=178,height=21;
    , ToolTipText = "Seleziona un modulo.";
    , HelpContextID = 238047430;
    , cFormVar="w_MODULO2",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='QUERY\ELENMODU.VQR',cKey='MOCODMOD',cValue='MOTRADES',cOrderBy='MOORDINA',xDefault=space(2);
  , bGlobalFont=.t.



  add object oGESTIONI_1_50 as StdCombo with uid="JUEKPKQHFC",rtseq=27,rtrep=.f.,left=84,top=29,width=153,height=21;
    , ToolTipText = "Seleziona le gestioni.";
    , HelpContextID = 33540433;
    , cFormVar="w_GESTIONI",RowSource=""+"Tutte,"+"Consigliate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGESTIONI_1_50.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    space(2))))
  endfunc
  func oGESTIONI_1_50.GetRadio()
    this.Parent.oContained.w_GESTIONI = this.RadioValue()
    return .t.
  endfunc

  func oGESTIONI_1_50.SetRadio()
    this.Parent.oContained.w_GESTIONI=trim(this.Parent.oContained.w_GESTIONI)
    this.value = ;
      iif(this.Parent.oContained.w_GESTIONI=='T',1,;
      iif(this.Parent.oContained.w_GESTIONI=='C',2,;
      0))
  endfunc

  func oGESTIONI_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PARAM2<>'R' AND .w_PARAM4="M")
    endwith
   endif
  endfunc

  func oGESTIONI_1_50.mHide()
    with this.Parent.oContained
      return (NOT (.w_PARAM2<>'R' AND .w_PARAM4="M"))
    endwith
  endfunc

  add object oDESCGR_1_53 as StdField with uid="EIZPINVSFN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCGR", cQueryName = "DESCGR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 13579830,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=433, Top=3, InputMask=replicate('X',20)

  func oDESCGR_1_53.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc

  add object oPRGSEL_1_54 as StdField with uid="FAGZBKEIAD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PRGSEL", cQueryName = "PRGSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 180257782,;
   bGlobalFont=.t.,;
    Height=21, Width=143, Left=643, Top=389, InputMask=replicate('X',50)

  func oPRGSEL_1_54.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G" or .w_PARAM2="R")
    endwith
  endfunc

  add object oGRPSEL_1_55 as StdField with uid="YSXTANLHLU",rtseq=31,rtrep=.f.,;
    cFormVar = "w_GRPSEL", cQueryName = "GRPSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 180294502,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=407, Top=389, cSayPict='"@Z 99999"', cGetPict='"@Z 99999"', cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_GRPSEL"

  func oGRPSEL_1_55.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc

  func oGRPSEL_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESGRP_1_56 as StdField with uid="TTALCPWICP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 260257334,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=456, Top=389, InputMask=replicate('X',20)

  func oDESGRP_1_56.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc


  add object ZOOMEN2 as cp_zoombox with uid="IOHBVSKLTW",left=301, top=296, width=490,height=124,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="GSUT2KSU",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="TSMENUVO",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "ListaGRP",;
    nPag=1;
    , HelpContextID = 78824422

  add object oStr_1_16 as StdString with uid="VTIURCDSOR",Visible=.t., Left=316, Top=7,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="WZTMYXGKQC",Visible=.t., Left=9, Top=6,;
    Alignment=1, Width=73, Height=18,;
    Caption="Moduli:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UUPAIRXJGZ",Visible=.t., Left=147, Top=443,;
    Alignment=1, Width=115, Height=18,;
    Caption="Autorizzazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SJUKBFUYSM",Visible=.t., Left=282, Top=423,;
    Alignment=2, Width=48, Height=18,;
    Caption="Accede"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TFQLHKZEDD",Visible=.t., Left=366, Top=423,;
    Alignment=2, Width=60, Height=18,;
    Caption="Inserisce"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="SDTVTUIFJJ",Visible=.t., Left=461, Top=423,;
    Alignment=2, Width=56, Height=18,;
    Caption="Modifica"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DZRKKADIRM",Visible=.t., Left=550, Top=423,;
    Alignment=2, Width=48, Height=18,;
    Caption="Elimina"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="HRKMOZTNRI",Visible=.t., Left=298, Top=289,;
    Alignment=2, Width=482, Height=18,;
    Caption="Permessi gruppi di appartenenza dell'utente selezionato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="KJVHZXBAHK",Visible=.t., Left=42, Top=348,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="FDTHAXLYGA",Visible=.t., Left=22, Top=371,;
    Alignment=1, Width=73, Height=18,;
    Caption="Procedura:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="GPNXDTDYVC",Visible=.t., Left=352, Top=263,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="BWKRDWAQHM",Visible=.t., Left=603, Top=263,;
    Alignment=1, Width=39, Height=18,;
    Caption="Proc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U" or .w_PARAM2="R")
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="CCQJMYLWVL",Visible=.t., Left=13, Top=393,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="LGXIRXSWBM",Visible=.t., Left=25, Top=324,;
    Alignment=0, Width=189, Height=18,;
    Caption="Utente/procedura selezionata:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "U")
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="MELHWVSWDX",Visible=.t., Left=325, Top=7,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="SFQNNXRREZ",Visible=.t., Left=361, Top=390,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G")
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="KQQNKAZHWK",Visible=.t., Left=603, Top=390,;
    Alignment=1, Width=39, Height=18,;
    Caption="Proc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> "G" or .w_PARAM2="R")
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="KYRWQHLORR",Visible=.t., Left=9, Top=33,;
    Alignment=1, Width=73, Height=18,;
    Caption="Gestioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (NOT (.w_PARAM2<>'R' AND .w_PARAM4="M"))
    endwith
  endfunc

  add object oBox_1_22 as StdBox with uid="BEYOTNNARV",left=151, top=422, width=474,height=50
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ksu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ksu
* --- Classe per gestire la combo dei moduli attivati
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine
* --- Fine Area Manuale
