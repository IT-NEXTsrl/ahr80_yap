* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bae                                                        *
*              Impostazione maschera ricerca allegati (AET)                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-21                                                      *
* Last revis.: 2010-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bae",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bae as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CALLMASK = .NULL.
  w_OBJCLAS = .NULL.
  w_CODCLA = space(15)
  w_NOMECUR = space(10)
  w_CODCLA = space(5)
  w_CODATTR = space(15)
  w_CAMCUR = space(0)
  * --- WorkFile variables
  PROMCLAS_idx=0
  PRODCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la valorizzazione della classe documentale Pratiche, solo per AET
    * --- valori per pOper:
    *      'I' - inizializzazione classe documentale;
    *      'V' - valorizzazione attributo primario.
    this.w_CALLMASK = this.oParentObject
    if IsAlt() and cTipo="AET"
      do case
        case this.pOper = "I"
          * --- Valorizzo la classe documentale e la imposto a show
          this.w_OBJCLAS = this.w_CALLMASK.getctrl("w_SELCODCLA")
          * --- Leggo il codice della classe documentale  avente il check 'pratica' attivo
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDCODCLA"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCLAPRA = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDCODCLA;
              from (i_cTable) where;
                  CDCLAPRA = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCLA = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OBJCLAS.Value = this.w_CODCLA
          this.w_OBJCLAS.bUpd = .T.
          this.w_OBJCLAS.Valid()     
          this.w_OBJCLAS = .null.
          * --- Per ovviare a problemi docuti alla sequenza ad hai controlli sulle variabili w:_SELCODCLA e per non modificare per tutti i casi i link
          *     leggo direttamente l'archivio solo nel caso di parametro 'AET'
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDRIFTAB"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.w_CODCLA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDRIFTAB;
              from (i_cTable) where;
                  CDCODCLA = this.w_CODCLA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_ARCHIVIO = NVL(cp_ToDate(_read_.CDRIFTAB),cp_NullValue(_read_.CDRIFTAB))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case this.pOper = "V"
          this.w_CODCLA = this.w_CALLMASK.w_SELCODCLA
          this.w_CALLMASK.GSUT_MA1.Markpos()     
          this.w_CALLMASK.GSUT_MA1.FirstRow()     
          do while Not this.w_CALLMASK.GSUT_MA1.Eof_Trs()
            this.w_CALLMASK.GSUT_MA1.SetRow()     
            this.w_CODATTR = this.w_CALLMASK.GSUT_MA1.w_IDCODATT
            * --- Read from PRODCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRODCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDCAMCUR"+;
                " from "+i_cTable+" PRODCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_CODCLA);
                    +" and CDCODATT = "+cp_ToStrODBC(this.w_CODATTR);
                    +" and CDATTPRI = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDCAMCUR;
                from (i_cTable) where;
                    CDCODCLA = this.w_CODCLA;
                    and CDCODATT = this.w_CODATTR;
                    and CDATTPRI = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAMCUR = NVL(cp_ToDate(_read_.CDCAMCUR),cp_NullValue(_read_.CDCAMCUR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Upper(Alltrim(this.w_CAMCUR))="CNCODCAN"
              this.w_CALLMASK.GSUT_MA1.Set("w_IDVALATT" , this.oParentObject.w_CODPRAT)     
            endif
            if Upper(Alltrim(this.w_CAMCUR))="CNDESCAN"
              this.w_CALLMASK.GSUT_MA1.Set("w_IDVALATT" , this.oParentObject.w_DESPRAT)     
            endif
            this.w_CALLMASK.GSUT_MA1.NextRow()     
          enddo
          this.w_CALLMASK.GSUT_MA1.Repos()     
      endcase
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='PRODCLAS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
