* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bm3                                                        *
*              Documenti, cambia causale documento                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_249]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-19                                                      *
* Last revis.: 2016-02-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bm3",oParentObject)
return(i_retval)

define class tgsve_bm3 as StdBatch
  * --- Local variables
  w_MESS = space(10)
  w_OBSMAG = ctod("  /  /  ")
  w_AGGMAG1 = .f.
  w_AGGMAG2 = .f.
  w_GSVE_MDV = .NULL.
  w_TESTMAG = .f.
  w_AGGDATPRO = .f.
  w_ADVISE = .f.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_AGGDATPRO = .f.
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  SALDIART_idx=0
  MAGAZZIN_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Documento al cambiare Della Causale di Magazzino in testata
    if Empty(this.oParentObject.w_MVTIPDOC)
      * --- Ho svuotato il codice causale oppure ho eseguito un link sulla stessa specificando
      *     solo una parte del codice, se si apre lo zoom parziale parte questo batch ma non deve fare niente fino a
      *     che non seleziono una causale
      i_retcode = 'stop'
      return
    endif
    * --- Ordini aperti, si scorre il transitorio per aggiornare le date di fatturazione
    this.w_GSVE_MDV = this.oParentObject
    DO GSVE_BM5 WITH this.w_GSVE_MDV
    if this.oParentObject.w_MVCLADOC="OR" AND this.oParentObject.w_FLORDAPE="S"
      this.w_AGGDATPRO = .F.
      if this.w_GSVE_MDV.NUMROW() > 0
        this.w_AGGDATPRO = ah_YesNo("Aggiorno le date di prossima fatturazione?")
      endif
      this.w_GSVE_MDV.MarkPos()     
      this.w_GSVE_MDV.FirstRow()     
      do while Not this.w_GSVE_MDV.Eof_Trs()
        this.w_GSVE_MDV.SetRow()     
        * --- Se Ordine Aperto ricalcolo la data prossima fatturazione
        if NOT this.w_GSVE_MDV.FullRow()
          * --- Nel caso non sia soddisfatta la condizione di riga piena, il metodo di calcolo viene posto secondo il valore dell'init
          this.oParentObject.w_MVMC_PER = IIF( NOT EMPTY( this.oParentObject.w_ANMTDCLC ), this.oParentObject.w_ANMTDCLC, this.oParentObject.w_TDMTDCLC )
        endif
        if this.w_GSVE_MDV.FullRow() AND this.w_AGGDATPRO
          this.oParentObject.w_MVMC_PER = IIF( NOT EMPTY( this.oParentObject.w_ANMTDCLC ), this.oParentObject.w_ANMTDCLC, this.oParentObject.w_TDMTDCLC )
          this.oParentObject.w_MVDATEVA = NextTime(this.oParentObject.w_MVMC_PER, MAX(this.w_GSVE_MDV.w_MVDATOAI, i_DATSYS))
        endif
        * --- Carica il Temporaneo dei Dati
        this.w_GSVE_MDV.SaveRow()     
        this.w_GSVE_MDV.NextRow()     
      enddo
      * --- Riposizionamento sul Transitorio senza effettuare la SaveDependsOn()
      this.w_GSVE_MDV.RePos(.T.)     
    endif
    * --- Valorizzazione variabile MVPRD nel caso di documenti di acquisto
    if this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC<>"OR"
      if this.oParentObject.w_FLPDOC="S"
        if this.oParentObject.w_MVCLADOC="DT"
          this.oParentObject.w_MVPRD = "DV"
        else
          if this.oParentObject.w_MVCLADOC="DI"
            this.oParentObject.w_MVPRD = "IV"
          else
            this.oParentObject.w_MVPRD = this.oParentObject.w_PRODOC
          endif
        endif
      else
        this.oParentObject.w_MVPRD = this.oParentObject.w_PRODOC
      endif
    endif
    if NOT ((this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVPRD<>"NN") OR (this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC="OR") OR (this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC<>"OR" AND this.oParentObject.w_MVPRD $ "DV-IV"))
      this.oParentObject.op_MVPRD = this.oParentObject.w_MVPRD
      this.oParentObject.w_MVNUMDOC = 0
    endif
    this.w_AGGMAG1 = .T.
    this.w_AGGMAG2 = .T.
    this.w_TESTMAG = .T.
    if NOT EMPTY(this.oParentObject.w_COMAG)
      * --- verifica Magazzini Associati alla Causale
      this.w_OBSMAG = cp_CharToDate("  -  -  ")
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDTOBSO"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_COMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDTOBSO;
          from (i_cTable) where;
              MGCODMAG = this.oParentObject.w_COMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OBSMAG = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT (EMPTY(this.w_OBSMAG) OR this.w_OBSMAG>this.oParentObject.w_OBTEST)
        this.w_MESS = "Codice magazzino associato alla causale documento obsoleto (%1)"
        ah_ErrorMsg(this.w_MESS,,"", ALLTR(this.oParentObject.w_COMAG) )
        this.oParentObject.w_COMAG = SPACE(5)
        this.w_AGGMAG1 = .F.
      else
        if NOT EMPTY(this.oParentObject.w_COMAT)
          this.w_OBSMAG = cp_CharToDate("  -  -  ")
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_COMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_COMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OBSMAG = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT (EMPTY(this.w_OBSMAG) OR this.w_OBSMAG>this.oParentObject.w_OBTEST)
            this.w_MESS = "Codice magazzino collegato associato alla causale documento obsoleto (%1)"
            ah_ErrorMsg(this.w_MESS,,"", ALLTR(this.oParentObject.w_COMAT) )
            this.oParentObject.w_COMAT = SPACE(5)
            this.w_AGGMAG2 = .F.
          endif
        endif
      endif
    endif
    this.w_GSVE_MDV.GSVE_MRT.w_CLADOC = this.oParentObject.w_MVCLADOC
    this.w_GSVE_MDV.GSVE_MRT.mHideControls()     
    this.oParentObject.w_OMAG = IIF(this.w_AGGMAG1, IIF(EMPTY(NVL(this.oParentObject.w_COMAG,"")), g_MAGAZI, this.oParentObject.w_COMAG), this.oParentObject.w_OMAG)
    this.oParentObject.w_OMAT = IIF(this.w_AGGMAG2, this.oParentObject.w_COMAT, this.oParentObject.w_OMAT)
    * --- Se Ric.Fiscale Disablita la Pagina Spedizioni
    if this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_CATDOC="RF"
      * --- Solo da Documenti di Vendita
      this.w_GSVE_MDV.opgfrm.Pages(3).Enabled = IIF(this.oParentObject.w_MVCLADOC = "RF", .F., .T.)
      this.oParentObject.w_MVCODPAG = IIF(this.oParentObject.w_MVCLADOC="RF", g_PAGRIC, this.oParentObject.w_MVCODPAG)
    endif
    if this.oParentObject.o_MVTCAMAG=this.oParentObject.w_MVTCAMAG OR EMPTY(this.oParentObject.o_MVTCAMAG) OR EMPTY(this.oParentObject.w_MVTCAMAG) OR this.w_GSVE_MDV.cFunction<>"Load"
      * --- Segno che la causale di magazzino � diversa
      this.w_TESTMAG = .F.
      if this.oParentObject.o_MVTFRAGG = this.oParentObject.w_MVTFRAGG
        * --- Se la causale di Magazzino non e' variata o e' il primo inserimento esce subito (il test su cFunc e' un di piu', dovrebbe essere editabile solo in caricamento)
        *     e non � cambiato nemmeno il livello di Raggruppamento sulla causale
        if this.oParentObject.w_COMAG = this.oParentObject.o_COMAG And this.oParentObject.w_COMAT = this.oParentObject.o_COMAT and this.oParentObject.o_DOCLIS=this.oParentObject.w_DOCLIS and this.oParentObject.o_PRZVAC=this.oParentObject.w_PRZVAC
          * --- Controllo anche il magazzino preferenziale sulla causale: se non cambia mi fermo
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    this.w_AGGMAG1 = .F.
    this.w_AGGMAG2 = .F.
    * --- Controllo che magazzino devo cambiare
    if this.oParentObject.w_COMAG <> this.oParentObject.o_COMAG
      this.w_AGGMAG1 = .T.
    endif
    if this.oParentObject.w_COMAT <> this.oParentObject.o_COMAT
      this.w_AGGMAG2 = .T.
    endif
    if this.w_AGGMAG1 Or this.w_AGGMAG2
      * --- La ecpDrop fa si che parta il batch GSVE_BM2 che esegue la modifica del magazzino su tutte le righe
      *     Devo per� obbligatoriamente posizionarmi sulla prima riga per eseguire l'aggiornamento correttamente
      this.w_GSVE_MDV.MarkPos()     
      this.w_GSVE_MDV.FirstRow()     
      do while Not this.w_GSVE_MDV.Eof_Trs()
        this.w_GSVE_MDV.SetRow()     
        if this.w_AGGMAG1
          * --- Modifico il magazzino su riga e rileggo i dati
          *     Svuoto anche l'ubicazione
          this.oParentObject.w_MVCODMAG = IIF(this.oParentObject.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.oParentObject.w_FLCASC+this.oParentObject.w_FLRISE+this.oParentObject.w_FLORDI+this.oParentObject.w_FLIMPE))Or (this.oParentObject.w_MV_FLAGG $"+-" And Empty(this.oParentObject.w_MVSERRIF))), IIF(NOT EMPTY(IIF(this.oParentObject.w_FLLOTT<>"C",SPACE(5),this.oParentObject.w_MVCODMAG)),IIF(this.oParentObject.w_FLLOTT<>"C",SPACE(5),this.oParentObject.w_MVCODMAG), CALCMAG(1, this.oParentObject.w_FLMGPR, "     ", this.oParentObject.w_COMAG, this.oParentObject.w_OMAG, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER)), SPACE(5))
          if this.oParentObject.w_MVCODMAG <> this.oParentObject.o_MVCODMAG
            * --- Solo se cambia rispetto a quello precedente
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDTOBSO,MGDESMAG,MGFLUBIC"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDTOBSO,MGDESMAG,MGFLUBIC;
                from (i_cTable) where;
                    MGCODMAG = this.oParentObject.w_MVCODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
              this.oParentObject.w_DESAPP = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
              this.oParentObject.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_MVCODUBI = Space(20)
          endif
        endif
        if this.w_AGGMAG2
          * --- Modifico il magazzino secondario
          this.oParentObject.w_MVCODMAT = IIF(this.oParentObject.w_MVTIPRIG="R" AND NOT EMPTY(this.oParentObject.w_F2CASC+this.oParentObject.w_F2RISE+this.oParentObject.w_F2ORDI+this.oParentObject.w_F2IMPE), CALCMAG(1, this.oParentObject.w_FLMTPR, "     ", this.oParentObject.w_COMAT, this.oParentObject.w_OMAT, this.oParentObject.w_MAGPRE, this.oParentObject.w_MAGTER), SPACE(5))
          if this.oParentObject.w_MVCODMAT <> this.oParentObject.o_MVCODMAT
            * --- Solo se cambia rispetto al precedente
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDTOBSO,MGFLUBIC"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDTOBSO,MGFLUBIC;
                from (i_cTable) where;
                    MGCODMAG = this.oParentObject.w_MVCODMAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
              this.oParentObject.w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_MVCODUB2 = Space(20)
          endif
        endif
        * --- Carica il Temporaneo dei Dati
        this.w_GSVE_MDV.SaveRow()     
        this.w_GSVE_MDV.NextRow()     
      enddo
      * --- Riposizionamento sul Transitorio senza effettuare la SaveDependsOn()
      this.w_GSVE_MDV.RePos(.T.)     
    endif
    * --- Aggiorna Righe Documento 
    this.w_ADVISE = .T.
    this.w_GSVE_MDV.MarkPos()     
    this.w_GSVE_MDV.FirstRow()     
    * --- Scorro il temporaneo e valorizzo causale e causale magazzino collegata
    *     legata al nuovo tipo documento
    do while Not this.w_GSVE_MDV.Eof_Trs()
      this.w_GSVE_MDV.SetRow()     
      * --- La causale di magazzino � stata modificata
      * --- Lanciando la mCalc() in uscita di questo batch verrebbe riaggiornato il prezzo di riga. 
      *     Per evitare questo riaggiorno le o_ dalle quali dipende il calculate del prezzo
      *     Nel caso di effettivo cambio di listino il prezzo viene comunque riaggiornato rilanciando l'evento w_Mvtcolis Changed
      this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_MVTCAMAG
      this.w_OBJCTRL = this.w_GSVE_MDV.GetCtrl( "w_MVCAUMAG" )
      this.oParentObject.w_LIPREZZO = this.oParentObject.w_MVPREZZO
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
      this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
      if this.oParentObject.w_MVTIPRIG<>"D"
        * --- Evito di far scattare la ecpdrop sulle righe descrittive
        this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
      endif
      if this.w_ADVISE AND this.w_TESTMAG
        this.w_MESS = "La causale di magazzino associata al documento � stata modificata in: %1 (precedente: %2)"
        ah_ErrorMsg(this.w_MESS,,"", this.oParentObject.w_MVTCAMAG, this.oParentObject.o_MVTCAMAG )
        this.w_ADVISE = .F.
      endif
      * --- Riporto il livello di Raggruppamento della nuova causale
      this.oParentObject.w_MVFLRAGG = this.oParentObject.w_MVTFRAGG
      * --- Se Ordine Aperto ricalcolo la data prossima fatturazione
      if this.w_GSVE_MDV.FullRow() And this.oParentObject.w_MVCLADOC="OR" AND this.oParentObject.w_FLORDAPE="S" AND (this.w_AGGDATPRO Or ah_YesNo("Aggiorno le date di prossima fatturazione?"))
        this.w_GSVE_MDV.NotifyEvent("NoMsg_CalcolaDataFat")     
        this.w_AGGDATPRO = .T.
      endif
      * --- Calcola i flag w_FM1, w_FM2, w_FPZ, w_FSC
      this.oParentObject.w_FM1 = IIF(this.oParentObject.cFunction="Load", this.oParentObject.w_SEQMA1," ")
      this.oParentObject.w_FM2 = IIF(this.oParentObject.cFunction="Load", this.oParentObject.w_SEQMA2," ")
      this.oParentObject.w_FPZ = IIF(this.oParentObject.cFunction="Load", this.oParentObject.w_SEQPRE," ")
      this.oParentObject.w_FSC = IIF(this.oParentObject.cFunction="Load", this.oParentObject.w_SEQSCO," ")
      * --- Carica il Temporaneo dei Dati
      this.w_GSVE_MDV.SaveRow()     
      this.w_GSVE_MDV.NextRow()     
    enddo
    * --- Riposizionamento sul Transitorio senza effettuare la SaveDependsOn()
    this.w_GSVE_MDV.RePos(.T.)     
    this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
    * --- dopo il riposizionamento riallineo liprezzo per non far scattare il calculate sul prezzo
    this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
    if this.oParentObject.o_MVTCOLIS<>this.oParentObject.w_MVTCOLIS or this.oParentObject.o_PRZVAC<>this.oParentObject.w_PRZVAC 
      this.oparentobject.NotifyEvent("w_MVTCOLIS Changed")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject)
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='SALDIART'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='TIP_DOCU'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
