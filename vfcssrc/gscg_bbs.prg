* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bbs                                                        *
*              Calcolo date periodo o data ultima stampa defiitiva             *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-15                                                      *
* Last revis.: 2010-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPERAZ,pCODATT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bbs",oParentObject,m.pOPERAZ,m.pCODATT)
return(i_retval)

define class tgscg_bbs as StdBatch
  * --- Local variables
  pOPERAZ = space(1)
  pCODATT = space(5)
  w_ULTPER = 0
  w_ULTSTA = ctod("  /  /  ")
  w_APPO = 0
  w_DATA = ctod("  /  /  ")
  w_APPO1 = space(10)
  w_APPO2 = space(4)
  * --- WorkFile variables
  REG_MAR_idx=0
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCG_SRM - Calcolo date intervallo del periodo selezionato
    do case
      case this.pOPERAZ = "C"
        this.w_ULTPER = 0
        if empty(this.oParentObject.w_NUMPER)
          * --- Ho inserito un periodo non valido esco senza calcolare le date
          this.oParentObject.w_DATINI = cp_CharToDate("  -  -    ")
          this.oParentObject.w_DATFIN = cp_CharToDate("  -  -    ")
          i_retcode = 'stop'
          return
        endif
        if g_TIPDEN="M"
          this.w_APPO = this.oParentObject.w_NUMPER
          this.w_APPO2 = this.oParentObject.w_ANNO
          if this.w_APPO>12
            this.w_APPO = 1
            this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
          endif
          * --- Calcolo le date di inizio/fine periodo
          this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
          this.oParentObject.w_DATINI = cp_CharToDate(this.w_APPO1)
          this.w_APPO = this.w_APPO + 1
          if this.w_APPO>12
            this.w_APPO = 1
            this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
          endif
          this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
          this.oParentObject.w_DATFIN = cp_CharToDate(this.w_APPO1)-1
        else
          * --- Calcolo la data di inizio periodo
          this.w_APPO = ((this.oParentObject.w_NUMPER) * 3) - 2
          this.w_APPO2 = this.oParentObject.w_ANNO
          if this.w_APPO>12
            this.w_APPO = 1
            this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
          endif
          this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
          this.oParentObject.w_DATINI = cp_CharToDate(this.w_APPO1)
          * --- Calcolo la data di fine periodo
          this.w_APPO = this.w_APPO+ 3
          if this.w_APPO>12
            this.w_APPO = 1
            this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
          endif
          this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
          this.oParentObject.w_DATFIN = cp_CharToDate(this.w_APPO1)-1
        endif
      case this.pOPERAZ = "L"
        this.oParentObject.w_ULTDAT = cp_CharToDate("  -  -    ")
        * --- Select from QUERY\MAXDATMAR
        do vq_exec with 'QUERY\MAXDATMAR',this,'_Curs_QUERY_MAXDATMAR','',.f.,.t.
        if used('_Curs_QUERY_MAXDATMAR')
          select _Curs_QUERY_MAXDATMAR
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ULTDAT = Cp_Todate(NVL(_Curs_QUERY_MAXDATMAR.MADATSTA,CTOD("  -  -    ")))
            select _Curs_QUERY_MAXDATMAR
            continue
          enddo
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPERAZ,pCODATT)
    this.pOPERAZ=pOPERAZ
    this.pCODATT=pCODATT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='REG_MAR'
    this.cWorkTables[2]='ATTIDETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_QUERY_MAXDATMAR')
      use in _Curs_QUERY_MAXDATMAR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPERAZ,pCODATT"
endproc
