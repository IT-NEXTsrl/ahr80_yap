* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kbk                                                        *
*              Backup database                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_449]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-28                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kbk
* Occorre essere amministratore per svolgere il back Up
if not cp_IsAdministrator(.F.)
   ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
* Verifica l'eventuale presenza di maschere aperte
Local L_Msg
L_Msg=openForm(.t.)
IF Not Empty(L_Msg)
   Ah_ErrorMsg("Chiudere tutte le maschere; maschera %1 aperta",'!','', L_MSG)
   return
Endif

IF VARTYPE(oParentObject)='C' AND (upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2")
   ah_ErrorMsg("Procedura non disponibile per questo tipo di database",'!')
   return
ENDIF
* --- Fine Area Manuale
return(createobject("tgsut_kbk",oParentObject))

* --- Class definition
define class tgsut_kbk as StdForm
  Top    = 26
  Left   = 30

  * --- Standard Properties
  Width  = 597
  Height = 213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=152485015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kbk"
  cComment = "Backup database"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RiorgInd = space(1)
  w_TipDat = space(20)
  w_PATBCK = space(200)
  w_ZipDB = space(1)
  w_NOTE_SQL = space(0)
  w_NOTE_DB2 = space(0)
  w_NOTE_ORACLE = space(0)
  w_NOTE_POSTGRES = space(0)
  w_SERVERNAME = space(20)
  w_connessi = space(250)
  w_conn1 = space(40)
  w_OriDat = space(40)
  w_user = space(20)
  w_passwd = space(20)
  w_PORT = space(6)
  w_FORMATO = space(10)
  w_service = space(20)
  w_Note = space(0)
  w_BckDB = space(1)
  w_CheckDB = space(1)
  w_ReIndexDb = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kbkPag1","gsut_kbk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATBCK_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BBK with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RiorgInd=space(1)
      .w_TipDat=space(20)
      .w_PATBCK=space(200)
      .w_ZipDB=space(1)
      .w_NOTE_SQL=space(0)
      .w_NOTE_DB2=space(0)
      .w_NOTE_ORACLE=space(0)
      .w_NOTE_POSTGRES=space(0)
      .w_SERVERNAME=space(20)
      .w_connessi=space(250)
      .w_conn1=space(40)
      .w_OriDat=space(40)
      .w_user=space(20)
      .w_passwd=space(20)
      .w_PORT=space(6)
      .w_FORMATO=space(10)
      .w_service=space(20)
      .w_Note=space(0)
      .w_BckDB=space(1)
      .w_CheckDB=space(1)
      .w_ReIndexDb=space(1)
        .w_RiorgInd = IIF(VARTYPE(this.oParentObject)='C','S','N')
        .w_TipDat = i_ServerConn[1,6]
          .DoRTCalc(3,3,.f.)
        .w_ZipDB = IIF(.w_RiorgInd='S','N','S')
        .w_NOTE_SQL = "La procedura crea un device nel quale sar� creato il back-up. Se attivo il Check Database non devono essere presenti altri utenti sul database."
        .w_NOTE_DB2 = "La procedura creer� il back up del database. Potrebbe essere necessario lanciare l'applicativo dove � residente il server di database"
        .w_NOTE_ORACLE = "La procedura creer� il dump del database. Potrebbe essere necessario lanciare l'applicativo dove � residente il server di database"
        .w_NOTE_POSTGRES = IIF(vartype(g_PATH_PGDUMP)<>"C" or empty(g_PATH_PGDUMP), "Impostare la variabile (g_PATH_PGDUMP) con il percorso al file pg_dump.exe", "La procedura creer� il dump del database. Potrebbe essere necessario lanciare l'applicativo dove � residente il server di database")
        .w_SERVERNAME = i_ServerConn[1,1]
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
          .DoRTCalc(10,15,.f.)
        .w_FORMATO = "custom"
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_Note = icase( .w_TIPDAT='SQLServer', .w_NOTE_SQL , .w_TIPDAT='Oracle', .w_NOTE_ORACLE ,  .w_TIPDAT='PostgreSQL', .w_NOTE_POSTGRES, .w_NOTE_DB2 )
        .w_BckDB = IIF(.w_RiorgInd='S','N','S')
        .w_CheckDB = IIF(.w_RiorgInd='S','N','S')
        .w_ReIndexDb = IIF(.w_RiorgInd='S','S','N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
    endwith
  return

  proc Calculate_TKGKPESWZF()
    with this
          * --- Cambia caption form
          .cComment = IIF(.w_RiorgInd='S',AH_MSGFORMAT("Riorganizzazione indici"), .Caption)
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPATBCK_1_3.visible=!this.oPgFrm.Page1.oPag.oPATBCK_1_3.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page1.oPag.oZipDB_1_5.visible=!this.oPgFrm.Page1.oPag.oZipDB_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oSERVERNAME_1_12.visible=!this.oPgFrm.Page1.oPag.oSERVERNAME_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oFORMATO_1_23.visible=!this.oPgFrm.Page1.oPag.oFORMATO_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oNote_1_30.visible=!this.oPgFrm.Page1.oPag.oNote_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBckDB_1_31.visible=!this.oPgFrm.Page1.oPag.oBckDB_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCheckDB_1_32.visible=!this.oPgFrm.Page1.oPag.oCheckDB_1_32.mHide()
    this.oPgFrm.Page1.oPag.oReIndexDb_1_33.visible=!this.oPgFrm.Page1.oPag.oReIndexDb_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TKGKPESWZF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTipDat_1_2.value==this.w_TipDat)
      this.oPgFrm.Page1.oPag.oTipDat_1_2.value=this.w_TipDat
    endif
    if not(this.oPgFrm.Page1.oPag.oPATBCK_1_3.value==this.w_PATBCK)
      this.oPgFrm.Page1.oPag.oPATBCK_1_3.value=this.w_PATBCK
    endif
    if not(this.oPgFrm.Page1.oPag.oZipDB_1_5.RadioValue()==this.w_ZipDB)
      this.oPgFrm.Page1.oPag.oZipDB_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERVERNAME_1_12.value==this.w_SERVERNAME)
      this.oPgFrm.Page1.oPag.oSERVERNAME_1_12.value=this.w_SERVERNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oOriDat_1_17.value==this.w_OriDat)
      this.oPgFrm.Page1.oPag.oOriDat_1_17.value=this.w_OriDat
    endif
    if not(this.oPgFrm.Page1.oPag.oFORMATO_1_23.RadioValue()==this.w_FORMATO)
      this.oPgFrm.Page1.oPag.oFORMATO_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNote_1_30.value==this.w_Note)
      this.oPgFrm.Page1.oPag.oNote_1_30.value=this.w_Note
    endif
    if not(this.oPgFrm.Page1.oPag.oBckDB_1_31.RadioValue()==this.w_BckDB)
      this.oPgFrm.Page1.oPag.oBckDB_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCheckDB_1_32.RadioValue()==this.w_CheckDB)
      this.oPgFrm.Page1.oPag.oCheckDB_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oReIndexDb_1_33.RadioValue()==this.w_ReIndexDb)
      this.oPgFrm.Page1.oPag.oReIndexDb_1_33.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kbkPag1 as StdContainer
  Width  = 593
  height = 213
  stdWidth  = 593
  stdheight = 213
  resizeXpos=411
  resizeYpos=59
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTipDat_1_2 as StdField with uid="LZQJXEFKMC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TipDat", cQueryName = "TipDat",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo database di origine",;
    HelpContextID = 210621642,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=133, Top=10, InputMask=replicate('X',20)

  add object oPATBCK_1_3 as StdField with uid="KFFFEKPYIZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PATBCK", cQueryName = "PATBCK",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso e nome del file di Backup, nel caso server DB2 occorre impostare il solo percorso",;
    HelpContextID = 143540982,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=133, Top=109, InputMask=replicate('X',200)

  func oPATBCK_1_3.mHide()
    with this.Parent.oContained
      return (.w_RiorgInd='S')
    endwith
  endfunc


  add object oBtn_1_4 as StdButton with uid="TJKQGNDVNA",left=566, top=109, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la cartella + il file in cui eseguire l'export dei dati";
    , HelpContextID = 152686038;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .w_PATBCK=Cp_Getdir()+.w_OriDat+'.dmp'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipDat<>'Oracle' OR .w_RiorgInd='S')
     endwith
    endif
  endfunc

  add object oZipDB_1_5 as StdCheck with uid="TVOWKQPKFB",rtseq=4,rtrep=.f.,left=133, top=130, caption="Zip del database",;
    ToolTipText = "Se attivo: esegue lo zip (compattazione) del file di Backup generato",;
    HelpContextID = 226634646,;
    cFormVar="w_ZipDB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZipDB_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oZipDB_1_5.GetRadio()
    this.Parent.oContained.w_ZipDB = this.RadioValue()
    return .t.
  endfunc

  func oZipDB_1_5.SetRadio()
    this.Parent.oContained.w_ZipDB=trim(this.Parent.oContained.w_ZipDB)
    this.value = ;
      iif(this.Parent.oContained.w_ZipDB=='S',1,;
      0)
  endfunc

  func oZipDB_1_5.mHide()
    with this.Parent.oContained
      return (.w_TipDat='DB2' OR .w_RiorgInd='S')
    endwith
  endfunc

  add object oSERVERNAME_1_12 as StdField with uid="NOWGLDGHNL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SERVERNAME", cQueryName = "SERVERNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Server",;
    HelpContextID = 264401207,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=133, Top=34, InputMask=replicate('X',20)

  func oSERVERNAME_1_12.mHide()
    with this.Parent.oContained
      return (.w_TipDat='DB2' or g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oOriDat_1_17 as StdField with uid="NAWLETIDOG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_OriDat", cQueryName = "OriDat",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Database di origine",;
    HelpContextID = 210648090,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=133, Top=77, InputMask=replicate('X',40)


  add object oObj_1_18 as cp_runprogram with uid="QEWAEHBVJL",left=13, top=221, width=132,height=22,;
    caption='GSUT_BIN(I)',;
   bGlobalFont=.t.,;
    prg="GSUT_BIN('I')",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Decifra la stringa di connessione";
    , HelpContextID = 245153740


  add object oFORMATO_1_23 as StdCombo with uid="ZOMKKPDQRH",rtseq=16,rtrep=.f.,left=133,top=180,width=110,height=22;
    , HelpContextID = 24719446;
    , cFormVar="w_FORMATO",RowSource=""+"plain,"+"custom,"+"directory,"+"tar", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFORMATO_1_23.RadioValue()
    return(iif(this.value =1,"plain",;
    iif(this.value =2,"custom",;
    iif(this.value =3,"directory",;
    iif(this.value =4,"tar",;
    space(10))))))
  endfunc
  func oFORMATO_1_23.GetRadio()
    this.Parent.oContained.w_FORMATO = this.RadioValue()
    return .t.
  endfunc

  func oFORMATO_1_23.SetRadio()
    this.Parent.oContained.w_FORMATO=trim(this.Parent.oContained.w_FORMATO)
    this.value = ;
      iif(this.Parent.oContained.w_FORMATO=="plain",1,;
      iif(this.Parent.oContained.w_FORMATO=="custom",2,;
      iif(this.Parent.oContained.w_FORMATO=="directory",3,;
      iif(this.Parent.oContained.w_FORMATO=="tar",4,;
      0))))
  endfunc

  func oFORMATO_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPDAT<>'PostgreSQL')
    endwith
  endfunc


  add object oBtn_1_24 as StdButton with uid="XIYKDEZKOE",left=566, top=109, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la cartella in cui eseguire il Backup";
    , HelpContextID = 152686038;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .w_PATBCK=GetFile('bck')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipDat<>'SQLServer' OR .w_RiorgInd='S')
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="CAZLDETYUH",left=566, top=109, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la cartella in cui eseguire il Backup";
    , HelpContextID = 152686038;
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        .w_PATBCK=Cp_GetDir("C:")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipDat<>'DB2' OR .w_RiorgInd='S')
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="GTSWITMPJX",left=566, top=109, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la cartella + il file in cui eseguire l'export dei dati";
    , HelpContextID = 152686038;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        .w_PATBCK=Cp_Getdir()+TTOC(dateTIME(),1)+"_"+.w_OriDat+'.backup'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipDat<>'PostgreSQL' OR .w_RiorgInd='S')
     endwith
    endif
  endfunc


  add object oObj_1_29 as cp_runprogram with uid="SJLJEIKGYK",left=149, top=221, width=160,height=22,;
    caption='GSUT_BIN(C)',;
   bGlobalFont=.t.,;
    prg="GSUT_BIN('C')",;
    cEvent = "ScriviPath",;
    nPag=1;
    , ToolTipText = "Scrive il patch del back up in anagrafica azienda";
    , HelpContextID = 245155276

  add object oNote_1_30 as StdMemo with uid="HQCTUQEMRY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_Note", cQueryName = "Note",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 159609046,;
   bGlobalFont=.t.,;
    Height=88, Width=296, Left=290, Top=10, ReadOnly=.t.

  func oNote_1_30.mHide()
    with this.Parent.oContained
      return (.w_RiorgInd='S')
    endwith
  endfunc

  add object oBckDB_1_31 as StdCheck with uid="OILHZYSYUJ",rtseq=19,rtrep=.f.,left=133, top=157, caption="Backup del database",;
    ToolTipText = "Se attivo: esegue il backup del database",;
    HelpContextID = 226612246,;
    cFormVar="w_BckDB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oBckDB_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oBckDB_1_31.GetRadio()
    this.Parent.oContained.w_BckDB = this.RadioValue()
    return .t.
  endfunc

  func oBckDB_1_31.SetRadio()
    this.Parent.oContained.w_BckDB=trim(this.Parent.oContained.w_BckDB)
    this.value = ;
      iif(this.Parent.oContained.w_BckDB=='S',1,;
      0)
  endfunc

  func oBckDB_1_31.mHide()
    with this.Parent.oContained
      return (.w_TipDat<>'SQLServer' OR .w_RiorgInd='S')
    endwith
  endfunc

  add object oCheckDB_1_32 as StdCheck with uid="EIOKBXGOUF",rtseq=20,rtrep=.f.,left=290, top=157, caption="Check del database",;
    ToolTipText = "Se attivo: esegue il check del database prima di eseguire il Backup",;
    HelpContextID = 198149850,;
    cFormVar="w_CheckDB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCheckDB_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCheckDB_1_32.GetRadio()
    this.Parent.oContained.w_CheckDB = this.RadioValue()
    return .t.
  endfunc

  func oCheckDB_1_32.SetRadio()
    this.Parent.oContained.w_CheckDB=trim(this.Parent.oContained.w_CheckDB)
    this.value = ;
      iif(this.Parent.oContained.w_CheckDB=='S',1,;
      0)
  endfunc

  func oCheckDB_1_32.mHide()
    with this.Parent.oContained
      return (.w_TipDat<>'SQLServer' OR .w_RiorgInd='S')
    endwith
  endfunc

  add object oReIndexDb_1_33 as StdCheck with uid="PXVLEKRJEO",rtseq=21,rtrep=.f.,left=290, top=180, caption="Riorganizzazioni indici",;
    ToolTipText = "Se attivo: esegue la riorganizzazione indici",;
    HelpContextID = 80330106,;
    cFormVar="w_ReIndexDb", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oReIndexDb_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oReIndexDb_1_33.GetRadio()
    this.Parent.oContained.w_ReIndexDb = this.RadioValue()
    return .t.
  endfunc

  func oReIndexDb_1_33.SetRadio()
    this.Parent.oContained.w_ReIndexDb=trim(this.Parent.oContained.w_ReIndexDb)
    this.value = ;
      iif(this.Parent.oContained.w_ReIndexDb=='S',1,;
      0)
  endfunc

  func oReIndexDb_1_33.mHide()
    with this.Parent.oContained
      return (.w_TipDat<>'SQLServer' OR .w_RiorgInd='S')
    endwith
  endfunc


  add object oBtn_1_35 as StdButton with uid="NDASJAFEBO",left=484, top=159, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare il Backup";
    , HelpContextID = 152485110;
    , caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      with this.Parent.oContained
        do GSUT_BBK with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PATBCK))
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_RiorgInd='S')
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="STHYLQEIMY",left=484, top=159, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la riorganizzazione indici";
    , HelpContextID = 152485110;
    , caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        do GSUT_BBK with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PATBCK))
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_RiorgInd<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="IITKQFQBSW",left=537, top=159, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 152485110;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="APVFIGYAYC",Visible=.t., Left=8, Top=109,;
    Alignment=1, Width=123, Height=18,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_RiorgInd='S')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="OMRNCHAFCA",Visible=.t., Left=8, Top=10,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipo database:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="SFBBRXXNAE",Visible=.t., Left=8, Top=38,;
    Alignment=1, Width=123, Height=18,;
    Caption="Server:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_TipDat='DB2' or g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="RVJDUDTWXT",Visible=.t., Left=8, Top=77,;
    Alignment=1, Width=123, Height=18,;
    Caption="Nome database:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TipDat='Oracle')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="DMOEDRYRCR",Visible=.t., Left=8, Top=180,;
    Alignment=1, Width=123, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPDAT<>'PostgreSQL')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="UXOZZLPNPB",Visible=.t., Left=8, Top=77,;
    Alignment=1, Width=123, Height=18,;
    Caption="Nome connessione:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TipDat<>'Oracle')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kbk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
