* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bsr                                                        *
*              ZOOM RISORSE                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-11                                                      *
* Last revis.: 2008-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPRIS,pDESFLD,pMarkPos
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bsr",oParentObject,m.pTIPRIS,m.pDESFLD,m.pMarkPos)
return(i_retval)

define class tgsar1bsr as StdBatch
  * --- Local variables
  pTIPRIS = space(1)
  pDESFLD = space(20)
  pMarkPos = .f.
  w_DPTIPRIS = space(0)
  w_DPCODICE = space(5)
  w_DPDESCRI = space(30)
  w_OBJCTRL = .NULL.
  w_PADRE = .NULL.
  w_NUMMARK = 0
  w_OSOURCE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    this.w_DPTIPRIS = this.pTIPRIS
    vx_exec("gsar1bsr.VZM",this)
    if !EMPTY( this.w_DPCODICE ) AND (VARTYPE( this.pDESFLD )=="C" AND !EMPTY( this.pDESFLD ) )
      * --- Assegno i valori del record selezionato
      if this.pMarkPos
        this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_"+ALLTRIM(this.pDESFLD) )
      else
        this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_"+ALLTRIM(this.pDESFLD) )
      endif
      if this.w_OBJCTRL.mCond()
        this.w_OSOURCE.xKey( 1 ) = this.w_DPTIPRIS
      endif
      this.w_OSOURCE.xKey( 2 ) = this.w_DPCODICE
      if this.pMarkPos
        this.w_NUMMARK = this.w_PADRE.nMarkpos
        this.w_PADRE.nMarkpos = 0
      endif
      this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
      if this.pMarkPos
        this.w_PADRE.nMarkpos = this.w_NUMMARK
      endif
    endif
  endproc


  proc Init(oParentObject,pTIPRIS,pDESFLD,pMarkPos)
    this.pTIPRIS=pTIPRIS
    this.pDESFLD=pDESFLD
    this.pMarkPos=pMarkPos
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPRIS,pDESFLD,pMarkPos"
endproc
