* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ban                                                        *
*              Controllo congruit� conto corrente                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_59]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-07                                                      *
* Last revis.: 2012-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_ban",oParentObject)
return(i_retval)

define class tgsar_ban as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_OK = .f.
  w_TOTFAT = 0
  w_TOTPAG = 0
  w_TOTSPE = 0
  w_TOTSENR = 0
  w_CTRL = .NULL.
  w_PARMSG1 = space(254)
  w_PARMSG2 = space(254)
  * --- WorkFile variables
  DES_DIVE_idx=0
  CONTI_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo congruit� Conto Corrente
    this.w_OK = .T.
    this.w_MESS = ""
    this.w_TOTFAT = 0
    this.w_TOTPAG = 0
    this.w_TOTSPE = 0
    this.w_TOTSENR = 0
    * --- Conto quante sedi di fatturazione, di pagamento e di spedizioni predefinite...
    if EMPTY(this.oParentObject.w_ANDESCRI)
      this.w_CTRL = this.oparentobject.GetCtrl( "w_ANDESCRI" )
      this.w_CTRL.SetFocus()     
      this.w_CTRL = Null
      this.w_MESS = "Ragione sociale non specificata"
      this.w_OK = .F.
    endif
    * --- Select from DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DDTIPRIF, DDPREDEF  from "+i_cTable+" DES_DIVE ";
          +" where DDTIPCON= "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+" And DDCODICE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
           ,"_Curs_DES_DIVE")
    else
      select DDTIPRIF, DDPREDEF from (i_cTable);
       where DDTIPCON= this.oParentObject.w_ANTIPCON And DDCODICE=this.oParentObject.w_ANCODICE;
        into cursor _Curs_DES_DIVE
    endif
    if used('_Curs_DES_DIVE')
      select _Curs_DES_DIVE
      locate for 1=1
      do while not(eof())
      this.w_TOTFAT = this.w_TOTFAT + IIF( _Curs_DES_DIVE.DDTIPRIF="FA",1,0)
      if this.w_TOTFAT>1
        this.w_MESS = "Sono state inserite pi� sedi di fatturazione"
        this.w_OK = .F.
        Exit
      endif
      this.w_TOTPAG = this.w_TOTPAG + IIF( _Curs_DES_DIVE.DDTIPRIF="PA",1,0)
      if this.w_TOTPAG>1
        this.w_MESS = "Sono state inserite pi� sedi di pagamento"
        this.w_OK = .F.
        Exit
      endif
      this.w_TOTSPE = this.w_TOTSPE + IIF( _Curs_DES_DIVE.DDPREDEF="S" , 1, 0)
      if this.w_TOTSPE>1
        this.w_MESS = "Sono state selezionate pi� sedi di consegna predefinite"
        this.w_OK = .F.
        Exit
      endif
      this.w_TOTSENR = this.w_TOTSENR + IIF( _Curs_DES_DIVE.DDTIPRIF="SB",1,0)
      if this.w_TOTSENR>1
        this.w_MESS = "Sono state inserite pi� sedi di stab. org. sogg. n. R."
        this.w_OK = .F.
        Exit
      endif
        select _Curs_DES_DIVE
        continue
      enddo
      use
    endif
    * --- Controllo che il suffisso codice di ricerca non sia gi� 
    *     stato utilizzato per un'altro Cliente o Fornitore
    if this.w_OK And g_FLCESC = "S" And Not Empty(this.oParentObject.w_ANCODESC)
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI  from "+i_cTable+" CONTI ";
            +" where ANTIPCON <>'G' And ANCODESC = "+cp_ToStrODBC(this.oParentObject.w_ANCODESC)+" And ANCODICE<>"+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_CONTI")
      else
        select ANTIPCON,ANCODICE,ANDESCRI from (i_cTable);
         where ANTIPCON <>"G" And ANCODESC = this.oParentObject.w_ANCODESC And ANCODICE<>this.oParentObject.w_ANCODICE;
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        if _Curs_CONTI.ANTIPCON="C"
          this.w_MESS = "Suffisso codice di ricerca gi� utilizzato per cliente%0%1 %2"
        else
          this.w_MESS = "Suffisso codice di ricerca gi� utilizzato per fornitore%0%1 %2"
        endif
        this.w_PARMSG1 = Alltrim(_Curs_CONTI.ANCODICE)
        this.w_PARMSG2 = Alltrim(_Curs_CONTI.ANDESCRI)
        this.w_OK = .F.
        Exit
          select _Curs_CONTI
          continue
        enddo
        use
      endif
    endif
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat(this.w_MESS, this.w_PARMSG1, this.w_PARMSG2)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BAN_CONTI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
