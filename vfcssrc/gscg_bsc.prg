* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsc                                                        *
*              Scorpora IVA da primanota                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2016-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsc",oParentObject)
return(i_retval)

define class tgscg_bsc as StdBatch
  * --- Local variables
  w_APPO = 0
  w_VALNAZ = space(3)
  w_DECTOP = 0
  w_CODVAL = space(3)
  w_TIPDOC = space(2)
  w_CONIVA = space(15)
  * --- WorkFile variables
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola l'IVA; Se tipo Reg.='C': Scorpora IVA dall Imponibile sulle Registrazioni di Primanota
    * --- Lanciato da GSCG_MIV
    select (this.oParentObject.cTrsName)
    if i_srv<>"A" and ( (this.oParentObject.w_IVIMPONI=0 and this.oParentObject.w_IVFLOMAG<>"Z" and this.oParentObject.w_IVFLOMAG<>"S") OR EMPTY (this.oParentObject.w_IVCODIVA)) 
      ah_ErrorMsg("Valore non ammesso: eliminare la riga con <F6>")
      this.oParentObject.w_IVIMPONI = this.oparentobject.o_IVIMPONI
      this.oParentObject.w_IVCODIVA = this.oparentobject.o_IVCODIVA
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_IVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVDESIVA,IVPERIND,IVPERIVA,IVDTOBSO,IVPLAIVA,IVREVCHA,IVREGMAR;
          from (i_cTable) where;
              IVCODIVA = this.oParentObject.w_IVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESIVA = NVL(cp_ToDate(_read_.IVDESIVA),cp_NullValue(_read_.IVDESIVA))
        this.oParentObject.w_IVPERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.oParentObject.w_IVOBSO = NVL(cp_ToDate(_read_.IVDTOBSO),cp_NullValue(_read_.IVDTOBSO))
        this.oParentObject.w_PLAIVA = NVL(cp_ToDate(_read_.IVPLAIVA),cp_NullValue(_read_.IVPLAIVA))
        this.oParentObject.w_REVCHA = NVL(cp_ToDate(_read_.IVREVCHA),cp_NullValue(_read_.IVREVCHA))
        this.oParentObject.w_REGMAR = NVL(cp_ToDate(_read_.IVREGMAR),cp_NullValue(_read_.IVREGMAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_TOTIVA = this.oParentObject.w_TOTIVA - this.oParentObject.w_IVIMPIVA
    this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP - this.oParentObject.O_IVIMPONI
    * --- Prende i Decimali della Valuta Nazionale riferita alla Registrazione
    WITH this.oParentObject.oParentObject
    this.w_DECTOP = .w_DECTOP
    this.w_VALNAZ = .w_PNVALNAZ
    this.w_CODVAL = .w_PNCODVAL
    this.w_TIPDOC = .w_PNTIPDOC
    this.w_CONIVA = .w_CONIVA
    ENDWITH
    if EMPTY(NVL(this.oParentObject.w_IVCODIVA," "))
      this.oParentObject.w_IVIMPONI = 0
      this.oParentObject.w_IVIMPIVA = 0
    else
      if this.oParentObject.w_IVTIPREG="C"
        this.w_APPO = this.oParentObject.w_IVIMPONI/(1+(this.oParentObject.w_PERIVA/100))
        if this.w_CODVAL=g_CODLIR OR this.w_TIPDOC $ "FE-NE" OR (this.w_TIPDOC $ "AU-NU" AND this.oParentObject.w_REVCHA="S")
          * --- Se Lire Arrotonda l' IVA all'importo Superiore
          this.w_APPO = IVAROUND(this.w_APPO, this.w_DECTOP, IIF(this.w_APPO<0, 0, 1),this.w_VALNAZ)
        else
          this.w_APPO = cp_ROUND(this.w_APPO, this.w_DECTOP)
        endif
        * --- Determina l'Iva per differenza
        this.oParentObject.w_IVIMPIVA = this.oParentObject.w_IVIMPONI - this.w_APPO
        this.oParentObject.w_IVIMPONI = this.w_APPO
      else
        this.w_APPO = (this.oParentObject.w_IVIMPONI*this.oParentObject.w_PERIVA)/100
        if this.w_CODVAL=g_CODLIR OR this.w_TIPDOC $ "FE-NE" OR (this.w_TIPDOC $ "AU-NU" AND this.oParentObject.w_REVCHA="S")
          * --- Se Lire Arrotonda l' IVA all'importo Superiore
          this.oParentObject.w_IVIMPIVA = IVAROUND(this.w_APPO, this.w_DECTOP, IIF(this.w_APPO<0, 0, 1),this.w_VALNAZ)
        else
          this.oParentObject.w_IVIMPIVA = cp_ROUND(this.w_APPO, this.w_DECTOP)
        endif
      endif
    endif
    if (this.oParentObject.w_CODIVA <> this.oParentObject.w_IVCODIVA) AND NOT EMPTY(this.oParentObject.w_IVCODIVA)
      * --- Assegno Conto Iva detraibile se riga caricata manualmente
      this.oParentObject.w_IVCODCON = IIF( NOT EMPTY(this.oParentObject.w_IVCODCON), this.oParentObject.w_IVCODCON, this.w_CONIVA)
      this.oParentObject.w_IVCODCOI = SPACE(15)
    endif
    this.oParentObject.w_IMPONI = this.oParentObject.w_IVIMPONI
    this.oParentObject.w_TOTIVA = this.oParentObject.w_TOTIVA + this.oParentObject.w_IVIMPIVA
    this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP + this.oParentObject.w_IVIMPONI
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VOCIIVA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
