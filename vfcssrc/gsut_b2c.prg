* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_b2c                                                        *
*              Click 2 call - infinity communication                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-12-05                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCOMMTYP,pCONTACT,pCHIAMATA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_b2c",oParentObject,m.pCOMMTYP,m.pCONTACT,m.pCHIAMATA)
return(i_retval)

define class tgsut_b2c as StdBatch
  * --- Local variables
  pCOMMTYP = space(2)
  pCONTACT = space(100)
  pCHIAMATA = space(2)
  w_cFolderPath = space(0)
  w_NOCODICE = space(15)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pCOMMTYP = Tipo comunicazione. 
    *     Pu� valere:
    *     'CA' Chiamata
    *     'LC' Log Chiamate
    *     
    *     pCONTACT = Contiene il numero da chiamare (pCOMMTYP='CA')  
    *                            oppure l'elenco dei numeri, separto da virgola, di cui fornire il log (pCOMMTYP='LC') 
    *     
    *     pCHIAMATA = Contiene la tipologia di chiamata
    *     
    if TYPE ("g_click2call")="L" AND g_click2call
      this.w_cFolderPath = ""
      * --- Controllo se esiste la variabile g_pathclick2call se esiste utilizzo il path dichiarato nella variabile pubblica
      if VARTYPE(G_PATHCLICK2CALL)="C"
        this.w_cFolderPath = Alltrim(G_PATHCLICK2CALL)
      else
        this.w_cFolderPath = SYS(5)+SYS(2003)+"\click2call.exe"
      endif
      if NOT EMPTY(this.pCONTACT)
        * --- Normalizzazione del numero di telefono
        *     Tolgo caratteri alfanumerici e li separo da "," quando trovo il "$"
        *     Esempio
        *     0541/334511$02567844$+39 0444 67 09 57
        *     diventa
        *     0541334511,02567844,+390444670957
        if EMPTY(chrtran( this.pCONTACT, "1234567890$+-/\.() ", "" ))
          this.pCONTACT = chrtran( this.pCONTACT, chrtran( this.pCONTACT, "1234567890$", "" ), "" )
          * --- Gestione errori
           
 Local l_OldErr, l_Err 
 l_OldErr = On("Error") 
 l_Err=.F. 
 On Error l_Err=.T.
          local cCmd,cReturn,cError,cLogFile 
 cCmd="" 
 cReturn="" 
 cError="" 
 cLogFile=ADDBS(TEMPADHOC())+"\click2call.log"
          local WshShell, fso, hFile 
 WshShell=.null. 
 fso=.null. 
 hFile=.null.
          WshShell = CreateObject("WScript.Shell")
          if cp_fileexist(this.w_cFolderPath)
            if VARTYPE(WshShell)="O"
              if this.pCOMMTYP="CA"
                cCmd="%comspec% /c "+this.w_cFolderPath+"  "+LRTrim(this.pCONTACT) + " 2> "+ cLogFile
              else
                this.pCONTACT = chrtran( this.pCONTACT, "$", "," )
                cCmd="%comspec% /c "+ this.w_cFolderPath+" -cdr  "+LRTrim(this.pCONTACT) + " 2> "+ cLogFile
              endif
              oExec = WshShell.Run(cCmd,0,.t.)
              fso = CreateObject("Scripting.FileSystemObject")
              hFile = fso.OpenTextFile(cLogFile, 1)
              cError=hFile.ReadAll
              hFile.Close
              hFile=fso.GetFile(cLogFile)
              hFile.Delete
              if not(Empty(cError))
                ah_ErrorMsg("Attenzione ci sono alcuni errori nella comunicazione,%0%1",,"",cError)
              else
                if this.pCOMMTYP="CA"
                  ah_msg( "Chiamata in corso...")
                  if g_AGFA="S"
                    GSFA_BSK(this,this.pCONTACT, this.pCHIAMATA, this.w_NOCODICE)
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                else
                  ah_msg("Creazione log chiamate in corso...")
                endif
              endif
              * --- Ripristino controllo errori
              On Error &l_OldErr
            else
              i_retcode = 'stop'
              return
            endif
          else
            ah_ErrorMsg("Attenzione eseguibile %1 non presente.",,, ALLTRIM(this.w_cFolderPath))
            i_retcode = 'stop'
            return
          endif
        else
          ah_ErrorMsg("Attenzione il numero %1 non risulta valido%0Sono consentiti solo i caratteri 1234567890 +/\-.()",,, ALLTRIM(this.pCONTACT))
          i_retcode = 'stop'
          return
        endif
      else
        ah_ErrorMsg("Non � stato specificato nessun numero%0Impossibile effettuare la chiamata!")
        i_retcode = 'stop'
        return
      endif
    else
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NOCODICE"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODCLI = "+cp_ToStrODBC(g_oMenu.GetByIndexKeyValue(2));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NOCODICE;
          from (i_cTable) where;
              NOCODCLI = g_oMenu.GetByIndexKeyValue(2);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOCODICE = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      GSUT_BSK(.null.,this.pCONTACT,this.pCHIAMATA,this.w_NOCODICE)
    endif
  endproc


  proc Init(oParentObject,pCOMMTYP,pCONTACT,pCHIAMATA)
    this.pCOMMTYP=pCOMMTYP
    this.pCONTACT=pCONTACT
    this.pCHIAMATA=pCHIAMATA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCOMMTYP,pCONTACT,pCHIAMATA"
endproc
