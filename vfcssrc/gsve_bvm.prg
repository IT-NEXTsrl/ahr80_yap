* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bvm                                                        *
*              Visualizzazione documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_86]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2014-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bvm",oParentObject,m.pParam)
return(i_retval)

define class tgsve_bvm as StdBatch
  * --- Local variables
  pParam = space(10)
  w_ZOOM = space(10)
  w_LTOTSCA = 0
  w_LTOTCAR = 0
  * --- WorkFile variables
  TMPVISUVE_idx=0
  TMPVISUAC_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSMA_SZM)
    do case
      case this.pParam $ "InitV-InitA"
        if this.pParam="InitV"
          * --- Create temporary table TMPVISUVE
          i_nIdx=cp_AddTableDef('TMPVISUVE') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVISUVE_proto';
                )
          this.TMPVISUVE_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPVISUAC
          i_nIdx=cp_AddTableDef('TMPVISUAC') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVISUAC_proto';
                )
          this.TMPVISUAC_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      case this.pParam $ "Acq-Ven"
        this.oParentObject.w_VISIBLE = .T.
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        if this.pParam="Ven"
          * --- Create temporary table TMPVISUVE
          i_nIdx=cp_AddTableDef('TMPVISUVE') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSVE_SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVISUVE_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPVISUAC
          i_nIdx=cp_AddTableDef('TMPVISUAC') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSAC_SZM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPVISUAC_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        This.OparentObject.NotifyEvent("Esegui")
        this.oParentObject.w_TOTCAR = 0
        this.oParentObject.w_TOTSCA = 0
        * --- Legge Totali Carichi, Scarichi
        NC = this.w_Zoom.cCursor
        SELECT &NC
        GO TOP
        SUM QTACAR, QTASCA TO this.w_LTOTCAR, this.w_LTOTSCA
        SELECT (NC)
        GO TOP
        this.oParentObject.w_TOTCAR = this.w_LTOTCAR
        this.oParentObject.w_TOTSCA = this.w_LTOTSCA
        if NOT EMPTY(this.oParentObject.w_CODART)
          * --- Eseguo questa operazione nel batch poich� ci sono problemi sul calcolo delle variabili
          *     TOTCAR e TOTSCA nel caso di zoom parziale
          this.oParentObject.w_VISIBLE = .F.
          this.oParentObject.mHideControls()
        endif
        * --- Refresh della griglia
        this.w_zoom.refresh()
      case this.pParam $ "DoneA-DoneV"
        if this.pParam="DoneV"
          * --- Drop temporary table TMPVISUVE
          i_nIdx=cp_GetTableDefIdx('TMPVISUVE')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVISUVE')
          endif
        else
          * --- Drop temporary table TMPVISUAC
          i_nIdx=cp_GetTableDefIdx('TMPVISUAC')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVISUAC')
          endif
        endif
      case this.pParam = "Stampa"
        L_CATDOC= this.oParentObject.w_CATDOC
        L_CODART = this.oParentObject.w_CODART
        L_TIPDOC = this.oParentObject.w_TIPDOC
        L_CODMAG = this.oParentObject.w_CODMAG
        L_DOCINI = this.oParentObject.w_DOCINI
        L_DOCFIN = this.oParentObject.w_DOCFIN
        L_COMMESSA = this.oParentObject.w_COMMESSA
        L_CLISEL = this.oParentObject.w_CLISEL
        L_FORSEL = this.oParentObject.w_FORSEL
        if LOWER(this.oParentObject.class)="tgsac_szm"
          L_CODATT = this.oParentObject.w_CODATT
        else
          L_CATEGO= this.oParentObject.w_CATDO2
          L_AGESEL = this.oParentObject.w_AGESEL
        endif
        L_CAUSEL = this.oParentObject.w_CAUSEL
        L_TIPEVA = this.oParentObject.w_TIPEVA
        L_CENSEL = this.oParentObject.w_CENSEL
        L_VOCSEL = this.oParentObject.w_VOCSEL
        L_MAGALT = this.oParentObject.w_MAGALT
        vx_exec(alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_OREP),this)
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPVISUVE'
    this.cWorkTables[2]='*TMPVISUAC'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
