* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bza                                                        *
*              Zoom gsma_aid                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-15                                                      *
* Last revis.: 2009-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bza",oParentObject)
return(i_retval)

define class tgszm_bza as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_VALNAZ = space(3)
  w_ESERCIZIO = space(4)
  w_DECUNI = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiama l'anagrafica con il dettaglio relativo alla riga selezionata sullo zoom
    this.w_ESERCIZIO = g_oMenu.getValue("DICODESE")
    this.w_CODAZI = i_CODAZI
    this.w_VALNAZ = g_PERVAL
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESERCIZIO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = this.w_CODAZI;
            and ESCODESE = this.w_ESERCIZIO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DECUNI = 2
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    VVU = 20*this.w_DECUNI
    if g_oMenu.cBatchType="L"
       
 OpenGest("A","GSMA_AID","DINUMINV", g_oMenu.getValue("DINUMINV"), "DICODESE", g_oMenu.getValue("DICODESE"), "DICODICE", g_oMenu.getValue("DICODICE")) 
 i_curform.ecpLoad()
    else
      OpenGest(g_oMenu.cBatchType,"GSMA_AID","DINUMINV", g_oMenu.getValue("DINUMINV"), "DICODESE", g_oMenu.getValue("DICODESE"), "DICODICE", g_oMenu.getValue("DICODICE"))
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
