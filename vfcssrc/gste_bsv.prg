* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bsv                                                        *
*              GESTIONE TREE VIEW                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2015-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bsv",oParentObject,m.pTipOp)
return(i_retval)

define class tgste_bsv as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_LASTPOINT = 0
  w_CODATTCUR = space(15)
  w_RELAZ = .NULL.
  w_TIPSTRLOC = space(1)
  w_PATHBMP = space(100)
  w_ELEMEN = .NULL.
  * --- WorkFile variables
  TMP_STR_idx=0
  VAELEMEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView (da GSTE_KVS)
    do case
      case this.pTipop="Reload"
        * --- Aggiorna la Treeview
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        if this.oParentObject.w_TIPVIS="S"
          * --- cLvlSep
          this.oParentObject.w_TREEVIEW.cLvlSep = "."
        else
          this.oParentObject.w_TREEVIEW.cLvlSep = ""
        endif
        if GSAR_BPT(this, this.oParentObject.w_CURSORNA, this.oParentObject.w_CODSTR, this.oParentObject.w_ROOT,this.oParentObject.w_TIPVIS,this.oParentObject.w_FLESCL)>0
          if USED("__tmp__")
            SELECT "__tmp__" 
 USE
          endif
          * --- Riempio la Treeview
          this.w_PATHBMP = ".\BMP\"
          Select ( this.oParentObject.w_CURSORNA ) 
 go top
           
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "APERTURAFILE.ICO") FOR EL__TIPO="A" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "APERTURA.ICO") FOR EL__TIPO="O" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) +"CHIUSURA.ICO") FOR EL__TIPO="K" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "FOLDERCL.ICO") FOR EL__TIPO="C" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "Fisso.ICO") FOR EL__TIPO="G" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "refresh.ICO") FOR EL__TIPO="D" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "zarchivia.ICO") FOR EL__TIPO="T" 
           
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "EspressioneA.ICO") FOR EL__TIPO="E" AND ELMODELE="A" AND Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "calcola.ICO") FOR EL__TIPO="E" AND ELMODELE="E" AND Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "ValoreA.ICO") FOR EL__TIPO="V" AND ELMODELE="A" AND Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "ValoreE.ICO") FOR EL__TIPO="V" AND ELMODELE="E" AND Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "EspressioneA.ICO") FOR EL__TIPO="E" AND ELMODELE="A" AND Empty(Nvl(ELCAMIMP," "))
           
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "EspressioneAimp.ICO") FOR EL__TIPO="E" AND ELMODELE="A" AND Not Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "EspressioneEimp.ICO") FOR EL__TIPO="E" AND ELMODELE="E" AND Not Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "ValoreAimp.ICO") FOR EL__TIPO="V" AND ELMODELE="A" AND Not Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "ValoreEimp.ICO") FOR EL__TIPO="V" AND ELMODELE="E" AND Not Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "EspressioneAimp.ICO") FOR EL__TIPO="E" AND ELMODELE="A" AND Not Empty(Nvl(ELCAMIMP," ")) 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "Scelta.ICO") FOR EL__TIPO="S" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "Attach.ICO") FOR EL__TIPO="L" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "VariabileER.ICO") FOR Nvl(ELINISEZ," ")="S" and EL__TIPO<>"O" 
 replace cpbmpname with fullpath(Alltrim(this.w_PATHBMP) + "Forza.ICO") FOR ELINISEZ="S" 
          if this.oParentObject.w_TIPVIS="S"
            Replace ELEXPRES with Alltrim(ELVALTXT)+STFILLER+"     Tabella: "+Alltrim(Nvl(ELTABIMP," "))+STFILLER+"     Codice: "+Alltrim(ELCODICE)+STFILLER+"     Descrizione: "+Alltrim(ELDESCRI) for ELMODELE="E" 
            Replace lvlkey with Right(alltrim("0000000000"+str(elordass)),10) +lvlkey for Not Empty(Nvl(lvlkey," "))
          endif
        else
          if Not Used ( this.oParentObject.w_CURSORNA )
            ah_ErrorMsg("La struttura non ha nessun elemento",,"")
            i_retcode = 'stop'
            return
          endif
          Select ( this.oParentObject.w_CURSORNA )
          Zap
          ah_ErrorMsg("La struttura non ha nessun elemento",,"")
        endif
        * --- Riempio la Treeview
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        if this.oParentObject.w_TIPVIS="S"
          this.oParentObject.w_TREEVIEW.cNodeShowField = ""
          this.oParentObject.w_TREEVIEW.CleafShowField = ""
          this.oParentObject.w_TREEVIEW.cShowFields = "ELEXPRES"
        else
          this.oParentObject.w_TREEVIEW.cNodeShowField = "ELCODICE"
          this.oParentObject.w_TREEVIEW.CleafShowField = "ELCODICE"
          this.oParentObject.w_TREEVIEW.cShowFields = "STFILLER+STDESCRI"
        endif
        this.oParentObject.notifyevent("Esegui")
        this.oParentObject.w_FL__ELAB = .T.
      case this.pTipop="Elemento"
        * --- Lancia la gestione dell'attivit� selezionata nella tree View
        * --- A seconda del tipo Componente (attivit� o meno ) lancio la gestione appropriata
        this.w_ELEMEN = GSTE_AEL()
        if Type ( "This.w_ELEMEN" )<>"L"
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_ELEMEN.bSec1)
            i_retcode = 'stop'
            return
          endif
          * --- inizializzo la chiave delle attivita componenti
          this.w_ELEMEN.w_ELCODICE = this.oParentObject.w_CODICE
          this.w_ELEMEN.w_ELCODSTR = this.oParentObject.w_CODSTR
          * --- creo il curosre delle solo chiavi
          this.w_ELEMEN.QueryKeySet("ELCODICE='"+this.oParentObject.w_CODICE+"'AND ELCODSTR='"+this.oParentObject.w_CODSTR+ "'","")     
          * --- mi metto in interrogazione e carico eventuali Post In Allegati
          this.w_ELEMEN.LoadRecWarn()     
          this.w_ELEMEN.LoadWarn()     
          oCpToolBar.SetQuery()
        endif
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used("query")
          select "query"
          use
        endif
        * --- Drop temporary table TMP_STR
        i_nIdx=cp_GetTableDefIdx('TMP_STR')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_STR')
        endif
        if used((this.oParentObject.w_CURSORNA))
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
      case this.pTipop="Stampa"
    endcase
  endproc


  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMP_STR'
    this.cWorkTables[2]='VAELEMEN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
