* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kez                                                        *
*              Gestione dati comunicazione                                     *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-27                                                      *
* Last revis.: 2009-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kez",oParentObject))

* --- Class definition
define class tgscg_kez as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 865
  Height = 493+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-08"
  HelpContextID=202758807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gscg_kez"
  cComment = "Gestione dati comunicazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ANNO = space(4)
  o_ANNO = space(4)
  w_TIPSOG = space(1)
  w_RAGPER = space(1)
  w_DEIMPIMP = 0
  w_DEIMPAFF = 0
  w_DEIMPNOI = 0
  w_DEIMPESE = 0
  w_DEIMPNIN = 0
  w_DEIMPLOR = 0
  w_DEPARIVA = space(11)
  w_DECODFIS = space(16)
  w_DETIPSOG = space(1)
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_NUMRIG = 0
  w_DESERIAL = space(10)
  w_DERIFPNT = space(10)
  w_VISPIVA = space(1)
  w_TIPODATI = space(1)
  w_TIPOESCLU = space(1)
  w_TIPOCODIF = space(1)
  w_FILPIVA = space(12)
  w_FLTIPCON = space(1)
  w_FLCODCON = space(15)
  w_DESCLF = space(40)
  w_OBTEST = space(10)
  w_DTOBSO = ctod('  /  /  ')
  w_ZoomElen = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kezPag1","gscg_kez",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati comunicazione")
      .Pages(2).addobject("oPag","tgscg_kezPag2","gscg_kez",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomElen = this.oPgFrm.Pages(1).oPag.ZoomElen
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomElen = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=space(4)
      .w_TIPSOG=space(1)
      .w_RAGPER=space(1)
      .w_DEIMPIMP=0
      .w_DEIMPAFF=0
      .w_DEIMPNOI=0
      .w_DEIMPESE=0
      .w_DEIMPNIN=0
      .w_DEIMPLOR=0
      .w_DEPARIVA=space(11)
      .w_DECODFIS=space(16)
      .w_DETIPSOG=space(1)
      .w_DETIPCON=space(1)
      .w_DECODCON=space(15)
      .w_NUMRIG=0
      .w_DESERIAL=space(10)
      .w_DERIFPNT=space(10)
      .w_VISPIVA=space(1)
      .w_TIPODATI=space(1)
      .w_TIPOESCLU=space(1)
      .w_TIPOCODIF=space(1)
      .w_FILPIVA=space(12)
      .w_FLTIPCON=space(1)
      .w_FLCODCON=space(15)
      .w_DESCLF=space(40)
      .w_OBTEST=space(10)
      .w_DTOBSO=ctod("  /  /  ")
        .w_ANNO = STR(YEAR(i_DATSYS)-1,4)
        .w_TIPSOG = 'T'
        .w_RAGPER = '1'
      .oPgFrm.Page1.oPag.ZoomElen.Calculate(.w_ANNO + .w_TIPSOG + .w_RAGPER)
        .w_DEIMPIMP = .w_ZoomElen.getVar('DEIMPIMP')
        .w_DEIMPAFF = .w_ZoomElen.getVar('DEIMPAFF')
        .w_DEIMPNOI = .w_ZoomElen.getVar('DEIMPNOI')
        .w_DEIMPESE = .w_ZoomElen.getVar('DEIMPESE')
        .w_DEIMPNIN = .w_ZoomElen.getVar('DEIMPNIN')
        .w_DEIMPLOR = .w_ZoomElen.getVar('DEIMPLOR')
        .w_DEPARIVA = nvl(.w_ZoomElen.getVar('DEPARIVA'),space(11))
        .w_DECODFIS = nvl(.w_ZoomElen.getVar('DECODFIS'),space(16))
        .w_DETIPSOG = iif(nvl(.w_ZoomElen.getVar('DETIPSOG'), ' ')='Fornitore', 'F', 'C')
        .w_DETIPCON = iif(nvl(.w_ZoomElen.getVar('DETIPCON'), ' ')='Fornitore', 'F', 'C')
        .w_DECODCON = nvl(.w_ZoomElen.getVar('DECODCON'), space(15))
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_DEPARIVA + .w_DECODFIS + .w_DETIPSOG + .w_DETIPCON + .w_DECODCON + .w_RAGPER)
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .w_NUMRIG = nvl(.w_ZoomDett.getVar('NUMRIG'), 0)
        .w_DESERIAL = nvl(.w_ZoomDett.getVar('DESERIAL'), space(10))
        .w_DERIFPNT = nvl(.w_ZoomDett.getVar('DERIFPNT'), space(10))
        .w_VISPIVA = 'T'
        .w_TIPODATI = 'T'
        .w_TIPOESCLU = 'S'
        .w_TIPOCODIF = 'T'
          .DoRTCalc(22,22,.f.)
        .w_FLTIPCON = 'C'
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_FLCODCON))
          .link_2_10('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_OBTEST = date(iif(empty(.w_ANNO),year(i_datsys)-1,val(.w_ANNO)),1,1)
    endwith
    this.DoRTCalc(27,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomElen.Calculate(.w_ANNO + .w_TIPSOG + .w_RAGPER)
        .DoRTCalc(1,3,.t.)
            .w_DEIMPIMP = .w_ZoomElen.getVar('DEIMPIMP')
            .w_DEIMPAFF = .w_ZoomElen.getVar('DEIMPAFF')
            .w_DEIMPNOI = .w_ZoomElen.getVar('DEIMPNOI')
            .w_DEIMPESE = .w_ZoomElen.getVar('DEIMPESE')
            .w_DEIMPNIN = .w_ZoomElen.getVar('DEIMPNIN')
            .w_DEIMPLOR = .w_ZoomElen.getVar('DEIMPLOR')
            .w_DEPARIVA = nvl(.w_ZoomElen.getVar('DEPARIVA'),space(11))
            .w_DECODFIS = nvl(.w_ZoomElen.getVar('DECODFIS'),space(16))
            .w_DETIPSOG = iif(nvl(.w_ZoomElen.getVar('DETIPSOG'), ' ')='Fornitore', 'F', 'C')
            .w_DETIPCON = iif(nvl(.w_ZoomElen.getVar('DETIPCON'), ' ')='Fornitore', 'F', 'C')
            .w_DECODCON = nvl(.w_ZoomElen.getVar('DECODCON'), space(15))
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_DEPARIVA + .w_DECODFIS + .w_DETIPSOG + .w_DETIPCON + .w_DECODCON + .w_RAGPER)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
            .w_NUMRIG = nvl(.w_ZoomDett.getVar('NUMRIG'), 0)
            .w_DESERIAL = nvl(.w_ZoomDett.getVar('DESERIAL'), space(10))
            .w_DERIFPNT = nvl(.w_ZoomDett.getVar('DERIFPNT'), space(10))
        .DoRTCalc(18,25,.t.)
        if .o_ANNO<>.w_ANNO
            .w_OBTEST = date(iif(empty(.w_ANNO),year(i_datsys)-1,val(.w_ANNO)),1,1)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomElen.Calculate(.w_ANNO + .w_TIPSOG + .w_RAGPER)
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_DEPARIVA + .w_DECODFIS + .w_DETIPSOG + .w_DETIPCON + .w_DECODCON + .w_RAGPER)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomElen.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FLCODCON
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FLTIPCON;
                     ,'ANCODICE',trim(this.w_FLCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FLCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_FLTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FLCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFLCODCON_2_10'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'Codice inesistente o obsoleto.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FLTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FLCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FLTIPCON;
                       ,'ANCODICE',this.w_FLCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FLCODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FLCODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_1.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_1.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSOG_1_2.RadioValue()==this.w_TIPSOG)
      this.oPgFrm.Page1.oPag.oTIPSOG_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGPER_1_3.RadioValue()==this.w_RAGPER)
      this.oPgFrm.Page1.oPag.oRAGPER_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPIMP_1_12.value==this.w_DEIMPIMP)
      this.oPgFrm.Page1.oPag.oDEIMPIMP_1_12.value=this.w_DEIMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPAFF_1_14.value==this.w_DEIMPAFF)
      this.oPgFrm.Page1.oPag.oDEIMPAFF_1_14.value=this.w_DEIMPAFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPNOI_1_16.value==this.w_DEIMPNOI)
      this.oPgFrm.Page1.oPag.oDEIMPNOI_1_16.value=this.w_DEIMPNOI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPESE_1_18.value==this.w_DEIMPESE)
      this.oPgFrm.Page1.oPag.oDEIMPESE_1_18.value=this.w_DEIMPESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPNIN_1_20.value==this.w_DEIMPNIN)
      this.oPgFrm.Page1.oPag.oDEIMPNIN_1_20.value=this.w_DEIMPNIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPLOR_1_22.value==this.w_DEIMPLOR)
      this.oPgFrm.Page1.oPag.oDEIMPLOR_1_22.value=this.w_DEIMPLOR
    endif
    if not(this.oPgFrm.Page2.oPag.oVISPIVA_2_1.RadioValue()==this.w_VISPIVA)
      this.oPgFrm.Page2.oPag.oVISPIVA_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPODATI_2_2.RadioValue()==this.w_TIPODATI)
      this.oPgFrm.Page2.oPag.oTIPODATI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPOESCLU_2_4.RadioValue()==this.w_TIPOESCLU)
      this.oPgFrm.Page2.oPag.oTIPOESCLU_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPOCODIF_2_5.RadioValue()==this.w_TIPOCODIF)
      this.oPgFrm.Page2.oPag.oTIPOCODIF_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILPIVA_2_8.value==this.w_FILPIVA)
      this.oPgFrm.Page2.oPag.oFILPIVA_2_8.value=this.w_FILPIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oFLTIPCON_2_9.RadioValue()==this.w_FLTIPCON)
      this.oPgFrm.Page2.oPag.oFLTIPCON_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLCODCON_2_10.value==this.w_FLCODCON)
      this.oPgFrm.Page2.oPag.oFLCODCON_2_10.value=this.w_FLCODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLF_2_12.value==this.w_DESCLF)
      this.oPgFrm.Page2.oPag.oDESCLF_2_12.value=this.w_DESCLF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>2005 and Val(.w_ANNO)<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO))  and not(empty(.w_FLCODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFLCODCON_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO = this.w_ANNO
    return

enddefine

* --- Define pages as container
define class tgscg_kezPag1 as StdContainer
  Width  = 861
  height = 493
  stdWidth  = 861
  stdheight = 493
  resizeXpos=858
  resizeYpos=158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_1 as StdField with uid="YKHYUAQYCB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di estrazione dati",;
    HelpContextID = 208276742,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=166, Top=6, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_ANNO)>2005 and Val(.w_ANNO)<2100)
    endwith
    return bRes
  endfunc


  add object oTIPSOG_1_2 as StdCombo with uid="QZVMIAVOYJ",rtseq=2,rtrep=.f.,left=245,top=6,width=131,height=21;
    , ToolTipText = "Tipo comunicazione clienti / fornitori / tutti";
    , HelpContextID = 140388662;
    , cFormVar="w_TIPSOG",RowSource=""+"Clienti,"+"Fornitori,"+"Tutti i soggetti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSOG_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPSOG_1_2.GetRadio()
    this.Parent.oContained.w_TIPSOG = this.RadioValue()
    return .t.
  endfunc

  func oTIPSOG_1_2.SetRadio()
    this.Parent.oContained.w_TIPSOG=trim(this.Parent.oContained.w_TIPSOG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSOG=='C',1,;
      iif(this.Parent.oContained.w_TIPSOG=='F',2,;
      iif(this.Parent.oContained.w_TIPSOG=='T',3,;
      0)))
  endfunc


  add object oRAGPER_1_3 as StdCombo with uid="MWFGZTTSLS",rtseq=3,rtrep=.f.,left=559,top=6,width=143,height=21;
    , ToolTipText = "Raggruppa per: partita IVA, partita IVA + soggetto, non raggruppare";
    , HelpContextID = 45781270;
    , cFormVar="w_RAGPER",RowSource=""+"Partita IVA,"+"Partita IVA + soggetto,"+"Non raggruppare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRAGPER_1_3.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    space(1)))))
  endfunc
  func oRAGPER_1_3.GetRadio()
    this.Parent.oContained.w_RAGPER = this.RadioValue()
    return .t.
  endfunc

  func oRAGPER_1_3.SetRadio()
    this.Parent.oContained.w_RAGPER=trim(this.Parent.oContained.w_RAGPER)
    this.value = ;
      iif(this.Parent.oContained.w_RAGPER=='0',1,;
      iif(this.Parent.oContained.w_RAGPER=='1',2,;
      iif(this.Parent.oContained.w_RAGPER=='2',3,;
      0)))
  endfunc


  add object oBtn_1_4 as StdButton with uid="PSTUCDPWKC",left=808, top=5, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 157189866;
    , Caption='\<Visualizza', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Visualizza')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="ZOYZKPUICA",left=5, top=445, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inserire un nuovo dato";
    , HelpContextID = 243166186;
    , Caption='\<Nuovo', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSCG_BEZ(this.Parent.oContained,space(10),1,space(10))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="CHHQRTXBDY",left=61, top=445, width=48,height=45,;
    CpPicture="bmp\modifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per modificare il dato";
    , HelpContextID = 243166186;
    , Caption='\<Modif.', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSCG_BEZ(this.Parent.oContained,.w_DESERIAL, .w_NUMRIG, .w_DERIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="XEDXHWOFBI",left=117, top=445, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di primanota";
    , HelpContextID = 145355274;
    , Caption='Re\<g.Cont', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_DERIFPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DERIFPNT))
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="NDASJAFEBO",left=754, top=445, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 243166186;
    , Caption='S\<tampa', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="NENFZDFCQL",left=808, top=445, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243166186;
    , Caption='\<Esci', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomElen as cp_zoombox with uid="XQPUAZNAPF",left=5, top=52, width=855,height=191,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCG_KEZ",cTable="DAELCLFO",bOptions=.f.,;
    cEvent = "Init,Visualizza",;
    nPag=1;
    , ToolTipText = "Dati elaborati per elenchi clienti / fornitori";
    , HelpContextID = 156114458

  add object oDEIMPIMP_1_12 as StdField with uid="CGNTGLCHIT",rtseq=4,rtrep=.t.,;
    cFormVar = "w_DEIMPIMP", cQueryName = "DEIMPIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni imponibili",;
    HelpContextID = 174568582,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=6, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDEIMPAFF_1_14 as StdField with uid="HBKNTIJHEF",rtseq=5,rtrep=.t.,;
    cFormVar = "w_DEIMPAFF", cQueryName = "DEIMPAFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo imposta afferente",;
    HelpContextID = 228084612,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=147, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDEIMPNOI_1_16 as StdField with uid="ARZHHDJUJH",rtseq=6,rtrep=.t.,;
    cFormVar = "w_DEIMPNOI", cQueryName = "DEIMPNOI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni non imponibili",;
    HelpContextID = 258454655,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=288, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDEIMPESE_1_18 as StdField with uid="AXPNZVOHWO",rtseq=7,rtrep=.t.,;
    cFormVar = "w_DEIMPESE", cQueryName = "DEIMPESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni esenti",;
    HelpContextID = 160975749,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=431, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDEIMPNIN_1_20 as StdField with uid="TCZAJCFAQD",rtseq=8,rtrep=.t.,;
    cFormVar = "w_DEIMPNIN", cQueryName = "DEIMPNIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni imponibile IVA non esposta",;
    HelpContextID = 9980796,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=572, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDEIMPLOR_1_22 as StdField with uid="BGDIWMFODR",rtseq=9,rtrep=.t.,;
    cFormVar = "w_DEIMPLOR", cQueryName = "DEIMPLOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta operazioni imponibili comprensive imposta afferente",;
    HelpContextID = 224900232,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=715, Top=269, cSayPict="v_PV(80)", cGetPict="v_GV(80)"


  add object ZoomDett as cp_zoombox with uid="HQFRKFFZPS",left=5, top=297, width=854,height=145,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCG1KEZ",cTable="DAELCLFD",bOptions=.f.,;
    cEvent = "Init,Visualizza",;
    nPag=1;
    , ToolTipText = "Dettaglio movimenti";
    , HelpContextID = 156114458


  add object oObj_1_37 as cp_outputCombo with uid="ZLERXIVPWT",left=377, top=457, width=359,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 156114458

  add object oStr_1_11 as StdString with uid="FAXLYKJPLN",Visible=.t., Left=18, Top=6,;
    Alignment=1, Width=141, Height=18,;
    Caption="Anno di estrazione dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="EBNVCBFSRP",Visible=.t., Left=6, Top=248,;
    Alignment=2, Width=139, Height=18,;
    Caption="Operazioni imponibili"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EBNKEGFQVC",Visible=.t., Left=147, Top=248,;
    Alignment=2, Width=139, Height=18,;
    Caption="Imposta afferente"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IFSQDMEMWU",Visible=.t., Left=284, Top=248,;
    Alignment=2, Width=139, Height=18,;
    Caption="Non imponibili"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JLDRVGCAUY",Visible=.t., Left=431, Top=248,;
    Alignment=2, Width=139, Height=18,;
    Caption="Operazioni esenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NUFOAXYUSE",Visible=.t., Left=572, Top=248,;
    Alignment=2, Width=140, Height=18,;
    Caption="Impon. IVA non esposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RLFQUZCZMM",Visible=.t., Left=715, Top=248,;
    Alignment=2, Width=139, Height=18,;
    Caption="Compr. impos. afferente"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="LSOHSGNAAR",Visible=.t., Left=434, Top=6,;
    Alignment=1, Width=120, Height=18,;
    Caption="Raggruppa per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="KDNTBCVSBC",Visible=.t., Left=246, Top=457,;
    Alignment=1, Width=127, Height=15,;
    Caption="Tipo di Stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="EPJVPSKQHH",left=5, top=245, width=851,height=24

  add object oBox_1_25 as StdBox with uid="AMTFIGHGVQ",left=146, top=245, width=2,height=51

  add object oBox_1_26 as StdBox with uid="QYWTSJSUTH",left=286, top=245, width=2,height=51

  add object oBox_1_27 as StdBox with uid="EZKHEIUVJR",left=428, top=245, width=2,height=51

  add object oBox_1_28 as StdBox with uid="VBHUFZLDHJ",left=571, top=245, width=2,height=51

  add object oBox_1_29 as StdBox with uid="PCTUQBCYXK",left=712, top=245, width=2,height=51

  add object oBox_1_30 as StdBox with uid="MWAOOAMQIM",left=5, top=267, width=851,height=29
enddefine
define class tgscg_kezPag2 as StdContainer
  Width  = 861
  height = 493
  stdWidth  = 861
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oVISPIVA_2_1 as StdCombo with uid="XNRHABBDBG",rtseq=18,rtrep=.f.,left=321,top=55,width=194,height=21;
    , ToolTipText = "Visualizza soggetti con Partita IVA, senza o tutti";
    , HelpContextID = 117135702;
    , cFormVar="w_VISPIVA",RowSource=""+"Solo soggetti privi di Partita IVA,"+"Con partita IVA,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oVISPIVA_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oVISPIVA_2_1.GetRadio()
    this.Parent.oContained.w_VISPIVA = this.RadioValue()
    return .t.
  endfunc

  func oVISPIVA_2_1.SetRadio()
    this.Parent.oContained.w_VISPIVA=trim(this.Parent.oContained.w_VISPIVA)
    this.value = ;
      iif(this.Parent.oContained.w_VISPIVA=='S',1,;
      iif(this.Parent.oContained.w_VISPIVA=='C',2,;
      iif(this.Parent.oContained.w_VISPIVA=='T',3,;
      0)))
  endfunc


  add object oTIPODATI_2_2 as StdCombo with uid="XDKFEJDVAL",rtseq=19,rtrep=.f.,left=321,top=84,width=194,height=21;
    , ToolTipText = "Visualizza solo dati estratti, solo dati inseriti o tutti";
    , HelpContextID = 240506497;
    , cFormVar="w_TIPODATI",RowSource=""+"Tutti i dati,"+"Solo dati estratti,"+"Solo dati inseriti manualmente", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPODATI_2_2.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTIPODATI_2_2.GetRadio()
    this.Parent.oContained.w_TIPODATI = this.RadioValue()
    return .t.
  endfunc

  func oTIPODATI_2_2.SetRadio()
    this.Parent.oContained.w_TIPODATI=trim(this.Parent.oContained.w_TIPODATI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPODATI=='T',1,;
      iif(this.Parent.oContained.w_TIPODATI=='S',2,;
      iif(this.Parent.oContained.w_TIPODATI=='M',3,;
      0)))
  endfunc


  add object oTIPOESCLU_2_4 as StdCombo with uid="HUVGVSKMVG",rtseq=20,rtrep=.f.,left=321,top=113,width=194,height=21;
    , ToolTipText = "Visualizza solo dati non esclusi, solo dati esclusi o tutti";
    , HelpContextID = 205902126;
    , cFormVar="w_TIPOESCLU",RowSource=""+"Visualizzare anche,"+"Non visualizzare,"+"Visualizzare solo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPOESCLU_2_4.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oTIPOESCLU_2_4.GetRadio()
    this.Parent.oContained.w_TIPOESCLU = this.RadioValue()
    return .t.
  endfunc

  func oTIPOESCLU_2_4.SetRadio()
    this.Parent.oContained.w_TIPOESCLU=trim(this.Parent.oContained.w_TIPOESCLU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOESCLU=='T',1,;
      iif(this.Parent.oContained.w_TIPOESCLU=='S',2,;
      iif(this.Parent.oContained.w_TIPOESCLU=='E',3,;
      0)))
  endfunc


  add object oTIPOCODIF_2_5 as StdCombo with uid="WVVBLDDRQW",rtseq=21,rtrep=.f.,left=321,top=142,width=194,height=21;
    , ToolTipText = "Visualizza solo dati con codifica plurima, senza o tutti";
    , HelpContextID = 6672929;
    , cFormVar="w_TIPOCODIF",RowSource=""+"Tutti,"+"Con codifica non univoca,"+"Con codifica univoca", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPOCODIF_2_5.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oTIPOCODIF_2_5.GetRadio()
    this.Parent.oContained.w_TIPOCODIF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCODIF_2_5.SetRadio()
    this.Parent.oContained.w_TIPOCODIF=trim(this.Parent.oContained.w_TIPOCODIF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCODIF=='T',1,;
      iif(this.Parent.oContained.w_TIPOCODIF=='S',2,;
      iif(this.Parent.oContained.w_TIPOCODIF=='N',3,;
      0)))
  endfunc

  add object oFILPIVA_2_8 as StdField with uid="SIIHTNFRRH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FILPIVA", cQueryName = "FILPIVA",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA da filtrare",;
    HelpContextID = 117106774,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=321, Top=187, InputMask=replicate('X',12)


  add object oFLTIPCON_2_9 as StdCombo with uid="NFSQNKXKGF",rtseq=23,rtrep=.f.,left=101,top=223,width=102,height=21;
    , ToolTipText = "Tipo conto (cliente / fornitore)";
    , HelpContextID = 73690020;
    , cFormVar="w_FLTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLTIPCON_2_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oFLTIPCON_2_9.GetRadio()
    this.Parent.oContained.w_FLTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oFLTIPCON_2_9.SetRadio()
    this.Parent.oContained.w_FLTIPCON=trim(this.Parent.oContained.w_FLTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_FLTIPCON=='C',1,;
      iif(this.Parent.oContained.w_FLTIPCON=='F',2,;
      0))
  endfunc

  func oFLTIPCON_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FLCODCON)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFLCODCON_2_10 as StdField with uid="ZRBHLQSDAV",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FLCODCON", cQueryName = "FLCODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente/fornitore",;
    HelpContextID = 61430692,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=321, Top=223, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FLTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FLCODCON"

  func oFLCODCON_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oFLCODCON_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFLCODCON_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FLTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFLCODCON_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'Codice inesistente o obsoleto.CONTI_VZM',this.parent.oContained
  endproc
  proc oFLCODCON_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FLTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FLCODCON
     i_obj.ecpSave()
  endproc

  add object oDESCLF_2_12 as StdField with uid="ZYVLYCLGUL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 119428150,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=458, Top=223, InputMask=replicate('X',40)

  add object oStr_2_3 as StdString with uid="DFUXYELBNO",Visible=.t., Left=214, Top=84,;
    Alignment=1, Width=104, Height=18,;
    Caption="Tipo dati:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="DZTMAPUNBD",Visible=.t., Left=214, Top=113,;
    Alignment=1, Width=104, Height=18,;
    Caption="Esclusione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="DRDUEXKNJX",Visible=.t., Left=214, Top=189,;
    Alignment=1, Width=104, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="CJRCENSCGA",Visible=.t., Left=214, Top=223,;
    Alignment=1, Width=104, Height=18,;
    Caption="Codice conto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="CKXXHLUHZB",Visible=.t., Left=214, Top=55,;
    Alignment=1, Width=104, Height=18,;
    Caption="Filtro partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="HJCIPFXZVE",Visible=.t., Left=214, Top=142,;
    Alignment=1, Width=104, Height=18,;
    Caption="Codifica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="HIGYKIDZRX",Visible=.t., Left=21, Top=223,;
    Alignment=1, Width=75, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kez','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
