* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_asa                                                        *
*              Saldi articoli                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_63]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_asa"))

* --- Class definition
define class tgsma_asa as StdForm
  Top    = 3
  Left   = 5

  * --- Standard Properties
  Width  = 554
  Height = 376+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=109738857
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  SALDIART_IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  VALUTE_IDX = 0
  cFile = "SALDIART"
  cKeySelect = "SLCODMAG,SLCODICE"
  cKeyWhere  = "SLCODMAG=this.w_SLCODMAG and SLCODICE=this.w_SLCODICE"
  cKeyWhereODBC = '"SLCODMAG="+cp_ToStrODBC(this.w_SLCODMAG)';
      +'+" and SLCODICE="+cp_ToStrODBC(this.w_SLCODICE)';

  cKeyWhereODBCqualified = '"SALDIART.SLCODMAG="+cp_ToStrODBC(this.w_SLCODMAG)';
      +'+" and SALDIART.SLCODICE="+cp_ToStrODBC(this.w_SLCODICE)';

  cPrg = "gsma_asa"
  cComment = "Saldi articoli"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0ASA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(4)
  w_SLCODMAG = space(5)
  w_DESMAG = space(30)
  w_SLCODICE = space(20)
  w_DESART = space(40)
  w_TIPART = space(2)
  w_SLVALUCA = 0
  w_SLCODVAA = space(3)
  w_SLDATUCA = ctod('  /  /  ')
  w_SLVALUPV = 0
  w_SLCODVAV = space(3)
  w_SLDATUPV = ctod('  /  /  ')
  w_SLQTAPER = 0
  w_SLQTAPRO = 0
  w_SLQTRPER = 0
  w_SLQTRPRO = 0
  w_DISPOPER = 0
  w_SLQTOPER = 0
  w_SLQTIPER = 0
  w_DISPCONT = 0
  w_CODART = space(20)
  w_CODMAG = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALDIART','gsma_asa')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_asaPag1","gsma_asa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldi articoli")
      .Pages(1).HelpContextID = 10898392
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSLCODMAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='SALDIART'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALDIART_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALDIART_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SLCODMAG = NVL(SLCODMAG,space(5))
      .w_SLCODICE = NVL(SLCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SALDIART where SLCODMAG=KeySet.SLCODMAG
    *                            and SLCODICE=KeySet.SLCODICE
    *
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALDIART')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALDIART.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SALDIART '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SLCODMAG',this.w_SLCODMAG  ,'SLCODICE',this.w_SLCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMAG = space(30)
        .w_DESART = space(40)
        .w_TIPART = space(2)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPOPE = this.cFunction
        .w_SLCODMAG = NVL(SLCODMAG,space(5))
          if link_1_2_joined
            this.w_SLCODMAG = NVL(MGCODMAG102,NVL(this.w_SLCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG102,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO102),ctod("  /  /  "))
          else
          .link_1_2('Load')
          endif
        .w_SLCODICE = NVL(SLCODICE,space(20))
          if link_1_4_joined
            this.w_SLCODICE = NVL(ARCODART104,NVL(this.w_SLCODICE,space(20)))
            this.w_DESART = NVL(ARDESART104,space(40))
            this.w_TIPART = NVL(ARTIPART104,space(2))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO104),ctod("  /  /  "))
          else
          .link_1_4('Load')
          endif
        .w_SLVALUCA = NVL(SLVALUCA,0)
        .w_SLCODVAA = NVL(SLCODVAA,space(3))
        .w_SLDATUCA = NVL(cp_ToDate(SLDATUCA),ctod("  /  /  "))
        .w_SLVALUPV = NVL(SLVALUPV,0)
        .w_SLCODVAV = NVL(SLCODVAV,space(3))
        .w_SLDATUPV = NVL(cp_ToDate(SLDATUPV),ctod("  /  /  "))
        .w_SLQTAPER = NVL(SLQTAPER,0)
        .w_SLQTAPRO = NVL(SLQTAPRO,0)
        .w_SLQTRPER = NVL(SLQTRPER,0)
        .w_SLQTRPRO = NVL(SLQTRPRO,0)
        .w_DISPOPER = .w_SLQTAPER-.w_SLQTRPER
        .w_SLQTOPER = NVL(SLQTOPER,0)
        .w_SLQTIPER = NVL(SLQTIPER,0)
        .w_DISPCONT = .w_SLQTAPER+.w_SLQTOPER-.w_SLQTIPER-.w_SLQTRPER
        .w_CODART = .w_SLCODICE
        .w_CODMAG = .w_SLCODMAG
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'SALDIART')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPE = space(4)
      .w_SLCODMAG = space(5)
      .w_DESMAG = space(30)
      .w_SLCODICE = space(20)
      .w_DESART = space(40)
      .w_TIPART = space(2)
      .w_SLVALUCA = 0
      .w_SLCODVAA = space(3)
      .w_SLDATUCA = ctod("  /  /  ")
      .w_SLVALUPV = 0
      .w_SLCODVAV = space(3)
      .w_SLDATUPV = ctod("  /  /  ")
      .w_SLQTAPER = 0
      .w_SLQTAPRO = 0
      .w_SLQTRPER = 0
      .w_SLQTRPRO = 0
      .w_DISPOPER = 0
      .w_SLQTOPER = 0
      .w_SLQTIPER = 0
      .w_DISPCONT = 0
      .w_CODART = space(20)
      .w_CODMAG = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_TIPOPE = this.cFunction
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_SLCODMAG))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_SLCODICE))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,16,.f.)
        .w_DISPOPER = .w_SLQTAPER-.w_SLQTRPER
          .DoRTCalc(18,19,.f.)
        .w_DISPCONT = .w_SLQTAPER+.w_SLQTOPER-.w_SLQTIPER-.w_SLQTRPER
        .w_CODART = .w_SLCODICE
        .w_CODMAG = .w_SLCODMAG
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALDIART')
    this.DoRTCalc(24,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSLCODMAG_1_2.enabled = i_bVal
      .Page1.oPag.oSLCODICE_1_4.enabled = i_bVal
      .Page1.oPag.oSLVALUCA_1_7.enabled = i_bVal
      .Page1.oPag.oSLDATUCA_1_9.enabled = i_bVal
      .Page1.oPag.oSLVALUPV_1_10.enabled = i_bVal
      .Page1.oPag.oSLDATUPV_1_12.enabled = i_bVal
      .Page1.oPag.oSLQTAPER_1_13.enabled = i_bVal
      .Page1.oPag.oSLQTRPER_1_15.enabled = i_bVal
      .Page1.oPag.oSLQTOPER_1_18.enabled = i_bVal
      .Page1.oPag.oSLQTIPER_1_19.enabled = i_bVal
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      .Page1.oPag.oBtn_1_32.enabled = .Page1.oPag.oBtn_1_32.mCond()
      .Page1.oPag.oObj_1_37.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSLCODMAG_1_2.enabled = .f.
        .Page1.oPag.oSLCODICE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSLCODMAG_1_2.enabled = .t.
        .Page1.oPag.oSLCODICE_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALDIART',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODMAG,"SLCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODICE,"SLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLVALUCA,"SLVALUCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODVAA,"SLCODVAA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDATUCA,"SLDATUCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLVALUPV,"SLVALUPV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODVAV,"SLCODVAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDATUPV,"SLDATUPV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTAPER,"SLQTAPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTAPRO,"SLQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTRPER,"SLQTRPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTRPRO,"SLQTRPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTOPER,"SLQTOPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLQTIPER,"SLQTIPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_lTable = "SALDIART"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALDIART_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SSA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALDIART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALDIART
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALDIART')
        i_extval=cp_InsertValODBCExtFlds(this,'SALDIART')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SLCODMAG,SLCODICE,SLVALUCA,SLCODVAA,SLDATUCA"+;
                  ",SLVALUPV,SLCODVAV,SLDATUPV,SLQTAPER,SLQTAPRO"+;
                  ",SLQTRPER,SLQTRPRO,SLQTOPER,SLQTIPER,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SLCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_SLCODICE)+;
                  ","+cp_ToStrODBC(this.w_SLVALUCA)+;
                  ","+cp_ToStrODBC(this.w_SLCODVAA)+;
                  ","+cp_ToStrODBC(this.w_SLDATUCA)+;
                  ","+cp_ToStrODBC(this.w_SLVALUPV)+;
                  ","+cp_ToStrODBC(this.w_SLCODVAV)+;
                  ","+cp_ToStrODBC(this.w_SLDATUPV)+;
                  ","+cp_ToStrODBC(this.w_SLQTAPER)+;
                  ","+cp_ToStrODBC(this.w_SLQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_SLQTRPER)+;
                  ","+cp_ToStrODBC(this.w_SLQTRPRO)+;
                  ","+cp_ToStrODBC(this.w_SLQTOPER)+;
                  ","+cp_ToStrODBC(this.w_SLQTIPER)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALDIART')
        i_extval=cp_InsertValVFPExtFlds(this,'SALDIART')
        cp_CheckDeletedKey(i_cTable,0,'SLCODMAG',this.w_SLCODMAG,'SLCODICE',this.w_SLCODICE)
        INSERT INTO (i_cTable);
              (SLCODMAG,SLCODICE,SLVALUCA,SLCODVAA,SLDATUCA,SLVALUPV,SLCODVAV,SLDATUPV,SLQTAPER,SLQTAPRO,SLQTRPER,SLQTRPRO,SLQTOPER,SLQTIPER,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SLCODMAG;
                  ,this.w_SLCODICE;
                  ,this.w_SLVALUCA;
                  ,this.w_SLCODVAA;
                  ,this.w_SLDATUCA;
                  ,this.w_SLVALUPV;
                  ,this.w_SLCODVAV;
                  ,this.w_SLDATUPV;
                  ,this.w_SLQTAPER;
                  ,this.w_SLQTAPRO;
                  ,this.w_SLQTRPER;
                  ,this.w_SLQTRPRO;
                  ,this.w_SLQTOPER;
                  ,this.w_SLQTIPER;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SALDIART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SALDIART_IDX,i_nConn)
      *
      * update SALDIART
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SALDIART')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SLVALUCA="+cp_ToStrODBC(this.w_SLVALUCA)+;
             ",SLCODVAA="+cp_ToStrODBC(this.w_SLCODVAA)+;
             ",SLDATUCA="+cp_ToStrODBC(this.w_SLDATUCA)+;
             ",SLVALUPV="+cp_ToStrODBC(this.w_SLVALUPV)+;
             ",SLCODVAV="+cp_ToStrODBC(this.w_SLCODVAV)+;
             ",SLDATUPV="+cp_ToStrODBC(this.w_SLDATUPV)+;
             ",SLQTAPER="+cp_ToStrODBC(this.w_SLQTAPER)+;
             ",SLQTAPRO="+cp_ToStrODBC(this.w_SLQTAPRO)+;
             ",SLQTRPER="+cp_ToStrODBC(this.w_SLQTRPER)+;
             ",SLQTRPRO="+cp_ToStrODBC(this.w_SLQTRPRO)+;
             ",SLQTOPER="+cp_ToStrODBC(this.w_SLQTOPER)+;
             ",SLQTIPER="+cp_ToStrODBC(this.w_SLQTIPER)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SALDIART')
        i_cWhere = cp_PKFox(i_cTable  ,'SLCODMAG',this.w_SLCODMAG  ,'SLCODICE',this.w_SLCODICE  )
        UPDATE (i_cTable) SET;
              SLVALUCA=this.w_SLVALUCA;
             ,SLCODVAA=this.w_SLCODVAA;
             ,SLDATUCA=this.w_SLDATUCA;
             ,SLVALUPV=this.w_SLVALUPV;
             ,SLCODVAV=this.w_SLCODVAV;
             ,SLDATUPV=this.w_SLDATUPV;
             ,SLQTAPER=this.w_SLQTAPER;
             ,SLQTAPRO=this.w_SLQTAPRO;
             ,SLQTRPER=this.w_SLQTRPER;
             ,SLQTRPRO=this.w_SLQTRPRO;
             ,SLQTOPER=this.w_SLQTOPER;
             ,SLQTIPER=this.w_SLQTIPER;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SALDIART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SALDIART_IDX,i_nConn)
      *
      * delete SALDIART
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SLCODMAG',this.w_SLCODMAG  ,'SLCODICE',this.w_SLCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    if i_bUpd
      with this
            .w_TIPOPE = this.cFunction
        .DoRTCalc(2,16,.t.)
            .w_DISPOPER = .w_SLQTAPER-.w_SLQTRPER
        .DoRTCalc(18,19,.t.)
            .w_DISPCONT = .w_SLQTAPER+.w_SLQTOPER-.w_SLQTIPER-.w_SLQTRPER
            .w_CODART = .w_SLCODICE
            .w_CODMAG = .w_SLCODMAG
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SLCODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_SLCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_SLCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SLCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_SLCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_SLCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SLCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oSLCODMAG_1_2'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_SLCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_SLCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(IIF(UPPER(.w_TIPOPE)='LOAD', CHKSALM(.w_SLCODICE, .w_SLCODMAG), .T.)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice saldo gi� inserito o magazzino inesistente oppure obsoleto")
        endif
        this.w_SLCODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.MGCODMAG as MGCODMAG102"+ ",link_1_2.MGDESMAG as MGDESMAG102"+ ",link_1_2.MGDTOBSO as MGDTOBSO102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on SALDIART.SLCODMAG=link_1_2.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and SALDIART.SLCODMAG=link_1_2.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SLCODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SLCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SLCODICE))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SLCODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_SLCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_SLCODICE)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SLCODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSLCODICE_1_4'),i_cWhere,'GSMA_BZA',"Anagrafica articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SLCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SLCODICE)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODICE = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endif
        this.w_SLCODICE = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ARCODART as ARCODART104"+ ",link_1_4.ARDESART as ARDESART104"+ ",link_1_4.ARTIPART as ARTIPART104"+ ",link_1_4.ARDTOBSO as ARDTOBSO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on SALDIART.SLCODICE=link_1_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and SALDIART.SLCODICE=link_1_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSLCODMAG_1_2.value==this.w_SLCODMAG)
      this.oPgFrm.Page1.oPag.oSLCODMAG_1_2.value=this.w_SLCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_3.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_3.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSLCODICE_1_4.value==this.w_SLCODICE)
      this.oPgFrm.Page1.oPag.oSLCODICE_1_4.value=this.w_SLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_5.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_5.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSLVALUCA_1_7.value==this.w_SLVALUCA)
      this.oPgFrm.Page1.oPag.oSLVALUCA_1_7.value=this.w_SLVALUCA
    endif
    if not(this.oPgFrm.Page1.oPag.oSLCODVAA_1_8.value==this.w_SLCODVAA)
      this.oPgFrm.Page1.oPag.oSLCODVAA_1_8.value=this.w_SLCODVAA
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDATUCA_1_9.value==this.w_SLDATUCA)
      this.oPgFrm.Page1.oPag.oSLDATUCA_1_9.value=this.w_SLDATUCA
    endif
    if not(this.oPgFrm.Page1.oPag.oSLVALUPV_1_10.value==this.w_SLVALUPV)
      this.oPgFrm.Page1.oPag.oSLVALUPV_1_10.value=this.w_SLVALUPV
    endif
    if not(this.oPgFrm.Page1.oPag.oSLCODVAV_1_11.value==this.w_SLCODVAV)
      this.oPgFrm.Page1.oPag.oSLCODVAV_1_11.value=this.w_SLCODVAV
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDATUPV_1_12.value==this.w_SLDATUPV)
      this.oPgFrm.Page1.oPag.oSLDATUPV_1_12.value=this.w_SLDATUPV
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTAPER_1_13.value==this.w_SLQTAPER)
      this.oPgFrm.Page1.oPag.oSLQTAPER_1_13.value=this.w_SLQTAPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTAPRO_1_14.value==this.w_SLQTAPRO)
      this.oPgFrm.Page1.oPag.oSLQTAPRO_1_14.value=this.w_SLQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTRPER_1_15.value==this.w_SLQTRPER)
      this.oPgFrm.Page1.oPag.oSLQTRPER_1_15.value=this.w_SLQTRPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTRPRO_1_16.value==this.w_SLQTRPRO)
      this.oPgFrm.Page1.oPag.oSLQTRPRO_1_16.value=this.w_SLQTRPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPOPER_1_17.value==this.w_DISPOPER)
      this.oPgFrm.Page1.oPag.oDISPOPER_1_17.value=this.w_DISPOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTOPER_1_18.value==this.w_SLQTOPER)
      this.oPgFrm.Page1.oPag.oSLQTOPER_1_18.value=this.w_SLQTOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSLQTIPER_1_19.value==this.w_SLQTIPER)
      this.oPgFrm.Page1.oPag.oSLQTIPER_1_19.value=this.w_SLQTIPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPCONT_1_20.value==this.w_DISPCONT)
      this.oPgFrm.Page1.oPag.oDISPCONT_1_20.value=this.w_DISPCONT
    endif
    cp_SetControlsValueExtFlds(this,'SALDIART')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SLCODMAG)) or not((IIF(UPPER(.w_TIPOPE)='LOAD', CHKSALM(.w_SLCODICE, .w_SLCODMAG), .T.)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSLCODMAG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SLCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice saldo gi� inserito o magazzino inesistente oppure obsoleto")
          case   ((empty(.w_SLCODICE)) or not(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSLCODICE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_asaPag1 as StdContainer
  Width  = 550
  height = 378
  stdWidth  = 550
  stdheight = 378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSLCODMAG_1_2 as StdField with uid="UMPYUJKILL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SLCODMAG", cQueryName = "SLCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice saldo gi� inserito o magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 83294611,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=92, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_SLCODMAG"

  func oSLCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSLCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSLCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oSLCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oSLCODMAG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_SLCODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_3 as StdField with uid="UIOAPWYFHI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 187171274,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=150, Top=9, InputMask=replicate('X',30)

  add object oSLCODICE_1_4 as StdField with uid="XHVRKZNQFU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SLCODICE", cQueryName = "SLCODMAG,SLCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente oppure obsoleto",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 118031979,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=92, Top=34, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_SLCODICE"

  func oSLCODICE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSLCODICE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSLCODICE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSLCODICE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Anagrafica articoli",'',this.parent.oContained
  endproc
  proc oSLCODICE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SLCODICE
     i_obj.ecpSave()
  endproc

  add object oDESART_1_5 as StdField with uid="RGEOZUHMDV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220463562,;
   bGlobalFont=.t.,;
    Height=21, Width=308, Left=241, Top=34, InputMask=replicate('X',40)

  add object oSLVALUCA_1_7 as StdField with uid="ETHYSGDYGV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SLVALUCA", cQueryName = "SLVALUCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo valore fiscale di acquisto",;
    HelpContextID = 58472039,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=63, Top=94, cSayPict="v_PU(18)", cGetPict="v_GU(18)"

  add object oSLCODVAA_1_8 as StdField with uid="KOXIWGTLPM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SLCODVAA", cQueryName = "SLCODVAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta ultimo valore fiscale di acquisto",;
    HelpContextID = 200735129,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=208, Top=94, InputMask=replicate('X',3)

  add object oSLDATUCA_1_9 as StdField with uid="JEQTMNQRLS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SLDATUCA", cQueryName = "SLDATUCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di aggiornamento ultimo valore fiscale di acquisto",;
    HelpContextID = 66786919,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=284, Top=94

  add object oSLVALUPV_1_10 as StdField with uid="TAPUNRWBNC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SLVALUPV", cQueryName = "SLVALUPV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo valore fiscale di vendita",;
    HelpContextID = 209963396,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=63, Top=126, cSayPict="v_PU(18)", cGetPict="v_GU(18)"

  add object oSLCODVAV_1_11 as StdField with uid="SJTLTPXNDW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SLCODVAV", cQueryName = "SLCODVAV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta ultimo valore fiscale di vendita",;
    HelpContextID = 200735108,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=208, Top=126, InputMask=replicate('X',3)

  add object oSLDATUPV_1_12 as StdField with uid="UXJPIGWUUI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SLDATUPV", cQueryName = "SLDATUPV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di aggiornamento ultimo valore fiscale di vendita",;
    HelpContextID = 201648516,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=284, Top=126

  add object oSLQTAPER_1_13 as StdField with uid="AHMJUPEIXS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SLQTAPER", cQueryName = "SLQTAPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza",;
    HelpContextID = 232711800,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=182, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oSLQTAPRO_1_14 as StdField with uid="LVHUPBAFPS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SLQTAPRO", cQueryName = "SLQTAPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza movimenti fuori linea",;
    HelpContextID = 35723659,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=434, Top=183, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oSLQTRPER_1_15 as StdField with uid="QEWBJPGKBC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SLQTRPER", cQueryName = "SLQTRPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� riservata",;
    HelpContextID = 250537592,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=206, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oSLQTRPRO_1_16 as StdField with uid="JEJLUSICFE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SLQTRPRO", cQueryName = "SLQTRPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� riservata fuori lnea",;
    HelpContextID = 17897867,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=434, Top=207, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDISPOPER_1_17 as StdField with uid="IUXQRZKPQF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DISPOPER", cQueryName = "DISPOPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza fra l'esistenza e la quantit� riservata",;
    HelpContextID = 247136904,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=230, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oSLQTOPER_1_18 as StdField with uid="CGEFRRFBKP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SLQTOPER", cQueryName = "SLQTOPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� ordinata ai fornitori",;
    HelpContextID = 247391864,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=276, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oSLQTIPER_1_19 as StdField with uid="MFIBWOQFDW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SLQTIPER", cQueryName = "SLQTIPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� impegnata ai clienti",;
    HelpContextID = 241100408,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=300, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDISPCONT_1_20 as StdField with uid="QFIDYLGPLL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DISPCONT", cQueryName = "DISPCONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� comprensiva dell'ordinato e dell'impegnato",;
    HelpContextID = 50658678,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=261, Top=324, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"


  add object oBtn_1_30 as StdButton with uid="SFYQTBUHWP",left=59, top=333, width=48,height=45,;
    CpPicture="BMP\DISPTEMP.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per consultare la disponibilit� nel tempo";
    , HelpContextID = 44554968;
    , Caption='\<Disp.temp';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      do GSOR_SZD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((NOT (EMPTY(.w_CODMAG) OR EMPTY(.w_CODART))) AND g_ORDI='S')
      endwith
    endif
  endfunc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ORDI<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="KKTWUWGJAF",left=6, top=333, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per consultare le valorizzazioni degli inventari confermati";
    , HelpContextID = 79847048;
    , Caption='\<Inventari';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      do GSMA_SZA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT (EMPTY(.w_SLCODICE) OR EMPTY(.w_SLCODMAG)))
      endwith
    endif
  endfunc


  add object oObj_1_37 as cp_runprogram with uid="RVRSJVYFUJ",left=251, top=394, width=109,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMA_BMS('ART')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 68258790

  add object oStr_1_21 as StdString with uid="BSIZGLJRKR",Visible=.t., Left=10, Top=34,;
    Alignment=1, Width=80, Height=15,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="TVXPSSEIRZ",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=80, Height=15,;
    Caption="Magazzino:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="FZAAIROKMA",Visible=.t., Left=162, Top=204,;
    Alignment=1, Width=93, Height=18,;
    Caption="Qt� riservata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PWRGNXVIJI",Visible=.t., Left=193, Top=183,;
    Alignment=1, Width=62, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XZQZNNCHHR",Visible=.t., Left=159, Top=276,;
    Alignment=1, Width=96, Height=18,;
    Caption="Qt� ordinata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="APJQDGNMCU",Visible=.t., Left=166, Top=300,;
    Alignment=1, Width=89, Height=18,;
    Caption="Qt� impegnata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="CUJSDIXDZE",Visible=.t., Left=262, Top=154,;
    Alignment=0, Width=104, Height=15,;
    Caption="Saldi in linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YXKTVAPDJA",Visible=.t., Left=120, Top=325,;
    Alignment=1, Width=135, Height=15,;
    Caption="Disponibilit� contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="GNYTGQVZRQ",Visible=.t., Left=170, Top=231,;
    Alignment=1, Width=85, Height=15,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XOGAUGUYRE",Visible=.t., Left=5, Top=66,;
    Alignment=0, Width=193, Height=18,;
    Caption="Ultimi valori fiscali"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ZYGFJCWEDZ",Visible=.t., Left=252, Top=127,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="FESLLDVYHR",Visible=.t., Left=252, Top=95,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="IBAMYEZXFF",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=58, Height=15,;
    Caption="Acquisto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VUEJINSQYU",Visible=.t., Left=18, Top=127,;
    Alignment=1, Width=45, Height=15,;
    Caption="Vendita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="GYNDWLQUVW",Visible=.t., Left=185, Top=95,;
    Alignment=1, Width=22, Height=15,;
    Caption="In:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="ZKQZMZTIVM",Visible=.t., Left=185, Top=127,;
    Alignment=1, Width=22, Height=15,;
    Caption="In:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ZFUAPVOXAT",Visible=.t., Left=381, Top=208,;
    Alignment=1, Width=50, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="YLIFYMXWWI",Visible=.t., Left=381, Top=184,;
    Alignment=1, Width=50, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="KIMQMNUBME",Visible=.t., Left=429, Top=154,;
    Alignment=0, Width=117, Height=18,;
    Caption="Saldi fuori linea"  ;
  , bGlobalFont=.t.

  add object oBox_1_31 as StdBox with uid="OUUFKOOZRA",left=2, top=89, width=546,height=1

  add object oBox_1_39 as StdBox with uid="PRCZZAEJSG",left=3, top=120, width=339,height=1

  add object oBox_1_49 as StdBox with uid="CMMNIPSQJQ",left=262, top=174, width=284,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_asa','SALDIART','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SLCODMAG=SALDIART.SLCODMAG";
  +" and "+i_cAliasName2+".SLCODICE=SALDIART.SLCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
