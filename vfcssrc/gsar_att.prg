* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_att                                                        *
*              Tipi transazioni                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2007-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_att"))

* --- Class definition
define class tgsar_att as StdForm
  Top    = 25
  Left   = 22

  * --- Standard Properties
  Width  = 498
  Height = 74+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-12"
  HelpContextID=92895081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  TIPITRAN_IDX = 0
  cFile = "TIPITRAN"
  cKeySelect = "TTCODICE"
  cKeyWhere  = "TTCODICE=this.w_TTCODICE"
  cKeyWhereODBC = '"TTCODICE="+cp_ToStrODBC(this.w_TTCODICE)';

  cKeyWhereODBCqualified = '"TIPITRAN.TTCODICE="+cp_ToStrODBC(this.w_TTCODICE)';

  cPrg = "gsar_att"
  cComment = "Tipi transazioni"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0ATT'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TTCODICE = space(3)
  w_TTDESCRI = space(50)
  w_TTREGSTA = space(1)
  w_TTNATTRA = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPITRAN','gsar_att')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_attPag1","gsar_att",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipo transazione")
      .Pages(1).HelpContextID = 151158308
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTTCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIPITRAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPITRAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPITRAN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TTCODICE = NVL(TTCODICE,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPITRAN where TTCODICE=KeySet.TTCODICE
    *
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPITRAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPITRAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPITRAN '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TTCODICE',this.w_TTCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TTCODICE = NVL(TTCODICE,space(3))
        .w_TTDESCRI = NVL(TTDESCRI,space(50))
        .w_TTREGSTA = NVL(TTREGSTA,space(1))
        .w_TTNATTRA = NVL(TTNATTRA,space(3))
        cp_LoadRecExtFlds(this,'TIPITRAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TTCODICE = space(3)
      .w_TTDESCRI = space(50)
      .w_TTREGSTA = space(1)
      .w_TTNATTRA = space(3)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPITRAN')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTTCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTTDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oTTREGSTA_1_4.enabled = i_bVal
      .Page1.oPag.oTTNATTRA_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTTCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTTCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIPITRAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TTCODICE,"TTCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TTDESCRI,"TTDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TTREGSTA,"TTREGSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TTNATTRA,"TTNATTRA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    i_lTable = "TIPITRAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPITRAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_STT with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPITRAN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPITRAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPITRAN')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPITRAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TTCODICE,TTDESCRI,TTREGSTA,TTNATTRA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TTCODICE)+;
                  ","+cp_ToStrODBC(this.w_TTDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TTREGSTA)+;
                  ","+cp_ToStrODBC(this.w_TTNATTRA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPITRAN')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPITRAN')
        cp_CheckDeletedKey(i_cTable,0,'TTCODICE',this.w_TTCODICE)
        INSERT INTO (i_cTable);
              (TTCODICE,TTDESCRI,TTREGSTA,TTNATTRA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TTCODICE;
                  ,this.w_TTDESCRI;
                  ,this.w_TTREGSTA;
                  ,this.w_TTNATTRA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPITRAN_IDX,i_nConn)
      *
      * update TIPITRAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPITRAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TTDESCRI="+cp_ToStrODBC(this.w_TTDESCRI)+;
             ",TTREGSTA="+cp_ToStrODBC(this.w_TTREGSTA)+;
             ",TTNATTRA="+cp_ToStrODBC(this.w_TTNATTRA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPITRAN')
        i_cWhere = cp_PKFox(i_cTable  ,'TTCODICE',this.w_TTCODICE  )
        UPDATE (i_cTable) SET;
              TTDESCRI=this.w_TTDESCRI;
             ,TTREGSTA=this.w_TTREGSTA;
             ,TTNATTRA=this.w_TTNATTRA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPITRAN_IDX,i_nConn)
      *
      * delete TIPITRAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TTCODICE',this.w_TTCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTTREGSTA_1_4.visible=!this.oPgFrm.Page1.oPag.oTTREGSTA_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTTNATTRA_1_5.visible=!this.oPgFrm.Page1.oPag.oTTNATTRA_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTTCODICE_1_1.value==this.w_TTCODICE)
      this.oPgFrm.Page1.oPag.oTTCODICE_1_1.value=this.w_TTCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTTDESCRI_1_2.value==this.w_TTDESCRI)
      this.oPgFrm.Page1.oPag.oTTDESCRI_1_2.value=this.w_TTDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTTREGSTA_1_4.value==this.w_TTREGSTA)
      this.oPgFrm.Page1.oPag.oTTREGSTA_1_4.value=this.w_TTREGSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oTTNATTRA_1_5.value==this.w_TTNATTRA)
      this.oPgFrm.Page1.oPag.oTTNATTRA_1_5.value=this.w_TTNATTRA
    endif
    cp_SetControlsValueExtFlds(this,'TIPITRAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TTCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTTCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TTCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_attPag1 as StdContainer
  Width  = 494
  height = 74
  stdWidth  = 494
  stdheight = 74
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTTCODICE_1_1 as StdField with uid="XJEUECRCKU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TTCODICE", cQueryName = "TTCODICE",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della natura transazione",;
    HelpContextID = 134877819,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=77, Top=14, InputMask=replicate('X',3)

  add object oTTDESCRI_1_2 as StdField with uid="PUREFANANE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TTDESCRI", cQueryName = "TTDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della natura della transazione",;
    HelpContextID = 49291903,;
   bGlobalFont=.t.,;
    Height=21, Width=365, Left=121, Top=15, InputMask=replicate('X',50)

  add object oTTREGSTA_1_4 as StdField with uid="HIMMOGEZWK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TTREGSTA", cQueryName = "TTREGSTA",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Regime statistico",;
    HelpContextID = 36766327,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=156, Top=46, InputMask=replicate('X',1)

  func oTTREGSTA_1_4.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oTTNATTRA_1_5 as StdField with uid="LJXKFPNEAY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TTNATTRA", cQueryName = "TTNATTRA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo della natura della transazione",;
    HelpContextID = 66896503,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=416, Top=46, InputMask=replicate('X',3)

  func oTTNATTRA_1_5.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="BIDNGMSRPH",Visible=.t., Left=-1, Top=14,;
    Alignment=1, Width=77, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="JKHMNHAUOR",Visible=.t., Left=7, Top=46,;
    Alignment=1, Width=145, Height=18,;
    Caption="Regime statistico:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="EOJEDJBDYI",Visible=.t., Left=249, Top=46,;
    Alignment=1, Width=163, Height=18,;
    Caption="Natura della transazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_att','TIPITRAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TTCODICE=TIPITRAN.TTCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
