* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kgg                                                        *
*              Manutenzione gadget                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-09                                                      *
* Last revis.: 2015-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kgg",oParentObject))

* --- Class definition
define class tgsut_kgg as StdForm
  Top    = 10
  Left   = 14

  * --- Standard Properties
  Width  = 763
  Height = 499
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-27"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  ELABKPIM_IDX = 0
  GAD_MAST_IDX = 0
  MOD_GADG_IDX = 0
  GRP_GADG_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cPrg = "gsut_kgg"
  cComment = "Manutenzione gadget"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(10)
  w_DESINI = space(50)
  w_CODFIN = space(10)
  w_DESFIN = space(50)
  w_GANAME = space(50)
  w_GADESCRI = space(250)
  w_TIPOUG = space(1)
  o_TIPOUG = space(1)
  w_CODUTE = 0
  w_CODGRP = 0
  w_DESCRI = space(20)
  w_SRCPRG = space(50)
  w_GRPCOD = space(5)
  w_INUSE = space(1)
  w_CHKATT = space(1)
  w_FORAZI = space(1)
  w_CODICE = space(10)
  w_INUSO = space(1)
  w_UTEESC = space(1)
  w_RESET = space(1)
  w_TIPPROP = space(1)
  w_TIPARC = space(2)
  w_PATH = space(200)
  o_PATH = space(200)
  w_FILE_NAME = space(50)
  w_FILE_EXT = space(10)
  w_ZoomGadg = .NULL.
  w_LblUso = .NULL.
  w_LblAtt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kggPag1","gsut_kgg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Cruscotto elaborazioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGadg = this.oPgFrm.Pages(1).oPag.ZoomGadg
    this.w_LblUso = this.oPgFrm.Pages(1).oPag.LblUso
    this.w_LblAtt = this.oPgFrm.Pages(1).oPag.LblAtt
    DoDefault()
    proc Destroy()
      this.w_ZoomGadg = .NULL.
      this.w_LblUso = .NULL.
      this.w_LblAtt = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ELABKPIM'
    this.cWorkTables[2]='GAD_MAST'
    this.cWorkTables[3]='MOD_GADG'
    this.cWorkTables[4]='GRP_GADG'
    this.cWorkTables[5]='cpusers'
    this.cWorkTables[6]='cpgroups'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_kgg
    This.w_LblAtt.BackColor = Rgb(255,255,0)
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(10)
      .w_DESINI=space(50)
      .w_CODFIN=space(10)
      .w_DESFIN=space(50)
      .w_GANAME=space(50)
      .w_GADESCRI=space(250)
      .w_TIPOUG=space(1)
      .w_CODUTE=0
      .w_CODGRP=0
      .w_DESCRI=space(20)
      .w_SRCPRG=space(50)
      .w_GRPCOD=space(5)
      .w_INUSE=space(1)
      .w_CHKATT=space(1)
      .w_FORAZI=space(1)
      .w_CODICE=space(10)
      .w_INUSO=space(1)
      .w_UTEESC=space(1)
      .w_RESET=space(1)
      .w_TIPPROP=space(1)
      .w_TIPARC=space(2)
      .w_PATH=space(200)
      .w_FILE_NAME=space(50)
      .w_FILE_EXT=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,6,.f.)
        .w_TIPOUG = 'U'
        .w_CODUTE = IIF(.w_TIPOUG='U',.w_CODUTE, 0)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODUTE))
          .link_1_8('Full')
        endif
        .w_CODGRP = IIF(.w_TIPOUG='G',.w_CODGRP, 0)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODGRP))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_SRCPRG = ''
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_SRCPRG))
          .link_1_11('Full')
        endif
        .w_GRPCOD = ''
        .w_INUSE = ' '
        .w_CHKATT = ' '
        .w_FORAZI = ' '
      .oPgFrm.Page1.oPag.ZoomGadg.Calculate()
        .w_CODICE = .w_ZoomGadg.GetVar('GACODICE')
        .w_INUSO = .w_ZoomGadg.GetVar('INUSE')
      .oPgFrm.Page1.oPag.LblUso.Calculate('In Uso',255)
        .w_UTEESC = ''
      .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitato',0,Rgb(255,255,0))
        .w_RESET = 'S'
        .w_TIPPROP = 'S'
        .w_TIPARC = 'GG'
        .w_PATH = ForcePath(ForceExt("GADGET_LIBRARY_"+Tran(i_codute),"TXT"), Iif(IsAhe(),Addbs(Sys(5)+StrTran(Sys(2003),'EXE',''))+"DOC\DATI_ESTERNI\",Addbs(Sys(5)+Sys(2003))+"FILES_X_IMPORT\"))
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
    this.DoRTCalc(23,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_TIPOUG<>.w_TIPOUG
            .w_CODUTE = IIF(.w_TIPOUG='U',.w_CODUTE, 0)
          .link_1_8('Full')
        endif
        if .o_TIPOUG<>.w_TIPOUG
            .w_CODGRP = IIF(.w_TIPOUG='G',.w_CODGRP, 0)
          .link_1_9('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomGadg.Calculate()
        .DoRTCalc(10,15,.t.)
            .w_CODICE = .w_ZoomGadg.GetVar('GACODICE')
            .w_INUSO = .w_ZoomGadg.GetVar('INUSE')
        .oPgFrm.Page1.oPag.LblUso.Calculate('In Uso',255)
        .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitato',0,Rgb(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        if .o_PATH<>.w_PATH
          .Calculate_EFUVAOVOWH()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGadg.Calculate()
        .oPgFrm.Page1.oPag.LblUso.Calculate('In Uso',255)
        .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitato',0,Rgb(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
  return

  proc Calculate_EFUVAOVOWH()
    with this
          * --- nome file ed estensione
          .w_FILE_NAME = JustStem(.w_PATH)
          .w_FILE_EXT = JustExt(.w_PATH)
          .w_PATH = Sys(2014,.w_PATH)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODUTE_1_8.visible=!this.oPgFrm.Page1.oPag.oCODUTE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODGRP_1_9.visible=!this.oPgFrm.Page1.oPag.oCODGRP_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGadg.Event(cEvent)
      .oPgFrm.Page1.oPag.LblUso.Event(cEvent)
      .oPgFrm.Page1.oPag.LblAtt.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_EFUVAOVOWH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_lTable = "GAD_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2], .t., this.GAD_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GAD_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_CODINI))
          select GACODICE,GA__NOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('GAD_MAST','*','GACODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'',"Gadget",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GA__NOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_CODINI)
            select GACODICE,GA__NOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.GACODICE,space(10))
      this.w_DESINI = NVL(_Link_.GA__NOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(10)
      endif
      this.w_DESINI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty(.w_CODFIN) OR (Upper(.w_CODINI)<=Upper(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice gadget iniziale successivo a quello finale o inesistente")
        endif
        this.w_CODINI = space(10)
        this.w_DESINI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GAD_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GAD_MAST_IDX,3]
    i_lTable = "GAD_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2], .t., this.GAD_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GAD_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_CODFIN))
          select GACODICE,GA__NOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('GAD_MAST','*','GACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'',"Gadget",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GA__NOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GA__NOME";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_CODFIN)
            select GACODICE,GA__NOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.GACODICE,space(10))
      this.w_DESFIN = NVL(_Link_.GA__NOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(10)
      endif
      this.w_DESFIN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_CODINI) AND (Upper(.w_CODINI)<=Upper(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o codice gadget iniziale vuoto o successivo a quello finale")
        endif
        this.w_CODFIN = space(10)
        this.w_DESFIN = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GAD_MAST_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GAD_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCODUTE_1_8'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.code,0)
      this.w_DESCRI = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRP
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oCODGRP_1_9'),i_cWhere,'',"Elenco gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRP = NVL(_Link_.code,0)
      this.w_DESCRI = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRP = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SRCPRG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_lTable = "MOD_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2], .t., this.MOD_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SRCPRG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_AMG',True,'MOD_GADG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGSRCPRG like "+cp_ToStrODBC(trim(this.w_SRCPRG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGSRCPRG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGSRCPRG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGSRCPRG',trim(this.w_SRCPRG))
          select MGSRCPRG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGSRCPRG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SRCPRG)==trim(_Link_.MGSRCPRG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SRCPRG) and !this.bDontReportError
            deferred_cp_zoom('MOD_GADG','*','MGSRCPRG',cp_AbsName(oSource.parent,'oSRCPRG_1_11'),i_cWhere,'GSUT_AMG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG";
                     +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',oSource.xKey(1))
            select MGSRCPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SRCPRG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG";
                   +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(this.w_SRCPRG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',this.w_SRCPRG)
            select MGSRCPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SRCPRG = NVL(_Link_.MGSRCPRG,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SRCPRG = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])+'\'+cp_ToStr(_Link_.MGSRCPRG,1)
      cp_ShowWarn(i_cKey,this.MOD_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SRCPRG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGANAME_1_5.value==this.w_GANAME)
      this.oPgFrm.Page1.oPag.oGANAME_1_5.value=this.w_GANAME
    endif
    if not(this.oPgFrm.Page1.oPag.oGADESCRI_1_6.value==this.w_GADESCRI)
      this.oPgFrm.Page1.oPag.oGADESCRI_1_6.value=this.w_GADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOUG_1_7.RadioValue()==this.w_TIPOUG)
      this.oPgFrm.Page1.oPag.oTIPOUG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_8.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_8.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRP_1_9.value==this.w_CODGRP)
      this.oPgFrm.Page1.oPag.oCODGRP_1_9.value=this.w_CODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSRCPRG_1_11.RadioValue()==this.w_SRCPRG)
      this.oPgFrm.Page1.oPag.oSRCPRG_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRPCOD_1_12.RadioValue()==this.w_GRPCOD)
      this.oPgFrm.Page1.oPag.oGRPCOD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINUSE_1_13.RadioValue()==this.w_INUSE)
      this.oPgFrm.Page1.oPag.oINUSE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKATT_1_14.RadioValue()==this.w_CHKATT)
      this.oPgFrm.Page1.oPag.oCHKATT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFORAZI_1_15.RadioValue()==this.w_FORAZI)
      this.oPgFrm.Page1.oPag.oFORAZI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEESC_1_38.RadioValue()==this.w_UTEESC)
      this.oPgFrm.Page1.oPag.oUTEESC_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRESET_1_50.RadioValue()==this.w_RESET)
      this.oPgFrm.Page1.oPag.oRESET_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPROP_1_51.RadioValue()==this.w_TIPPROP)
      this.oPgFrm.Page1.oPag.oTIPPROP_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_55.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_55.value=this.w_PATH
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Empty(.w_CODFIN) OR (Upper(.w_CODINI)<=Upper(.w_CODFIN)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice gadget iniziale successivo a quello finale o inesistente")
          case   not(!Empty(.w_CODINI) AND (Upper(.w_CODINI)<=Upper(.w_CODFIN)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o codice gadget iniziale vuoto o successivo a quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOUG = this.w_TIPOUG
    this.o_PATH = this.w_PATH
    return

enddefine

* --- Define pages as container
define class tgsut_kggPag1 as StdContainer
  Width  = 759
  height = 499
  stdWidth  = 759
  stdheight = 499
  resizeXpos=695
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="TVWCQGVOVC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice gadget iniziale successivo a quello finale o inesistente",;
    ToolTipText = "Codice gadget",;
    HelpContextID = 205803558,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=91, Top=11, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GAD_MAST", oKey_1_1="GACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GAD_MAST','*','GACODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gadget",'',this.parent.oContained
  endproc

  add object oDESINI_1_2 as StdField with uid="BRPXHMNRTY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gadget",;
    HelpContextID = 205862454,;
   bGlobalFont=.t.,;
    Height=21, Width=557, Left=192, Top=11, InputMask=replicate('X',50)

  add object oCODFIN_1_3 as StdField with uid="LEUKIMHSEQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o codice gadget iniziale vuoto o successivo a quello finale",;
    ToolTipText = "Codice gadget",;
    HelpContextID = 15814694,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=91, Top=34, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GAD_MAST", oKey_1_1="GACODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GAD_MAST','*','GACODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gadget",'',this.parent.oContained
  endproc

  add object oDESFIN_1_4 as StdField with uid="TMJMOKKGJY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gadget",;
    HelpContextID = 15873590,;
   bGlobalFont=.t.,;
    Height=21, Width=557, Left=192, Top=34, InputMask=replicate('X',50)

  add object oGANAME_1_5 as AH_SEARCHFLD with uid="OFXJARVWIN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GANAME", cQueryName = "GANAME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Parola/e da cercare nel nome del gadget",;
    HelpContextID = 137159270,;
   bGlobalFont=.t.,;
    Height=21, Width=657, Left=91, Top=58, InputMask=replicate('X',50)

  add object oGADESCRI_1_6 as AH_SEARCHFLD with uid="IILPYZKGQX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GADESCRI", cQueryName = "GADESCRI",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Parola/e da cercare nella descrizione del gadget",;
    HelpContextID = 110117551,;
   bGlobalFont=.t.,;
    Height=21, Width=657, Left=91, Top=82, InputMask=replicate('X',250)


  add object oTIPOUG_1_7 as StdCombo with uid="CSNCYEFXDU",rtseq=7,rtrep=.f.,left=91,top=106,width=98,height=21;
    , ToolTipText = "La configurazione pu� essere valida per un utente, un gruppo o per l'installazione";
    , HelpContextID = 180030262;
    , cFormVar="w_TIPOUG",RowSource=""+"Utente,"+"Gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOUG_1_7.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oTIPOUG_1_7.GetRadio()
    this.Parent.oContained.w_TIPOUG = this.RadioValue()
    return .t.
  endfunc

  func oTIPOUG_1_7.SetRadio()
    this.Parent.oContained.w_TIPOUG=trim(this.Parent.oContained.w_TIPOUG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOUG=='U',1,;
      iif(this.Parent.oContained.w_TIPOUG=='G',2,;
      0))
  endfunc

  add object oCODUTE_1_8 as StdField with uid="XLWHUGPTFM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtra i gadget personalizzati per questo utente",;
    HelpContextID = 145772582,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=192, Top=106, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOUG<>'U')
    endwith
  endfunc

  func oCODUTE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCODUTE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oCODGRP_1_9 as StdField with uid="MJFVYHVXYO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODGRP", cQueryName = "CODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtra i gadget personalizzati per gli utenti appartenenti a questo gruppo",;
    HelpContextID = 58871846,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=192, Top=106, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_CODGRP"

  func oCODGRP_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOUG<>'G')
    endwith
  endfunc

  func oCODGRP_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRP_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRP_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oCODGRP_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco gruppi",'',this.parent.oContained
  endproc

  add object oDESCRI_1_10 as StdField with uid="QVFKKLYMAG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente o gruppo",;
    HelpContextID = 209663542,;
   bGlobalFont=.t.,;
    Height=21, Width=478, Left=271, Top=106, InputMask=replicate('X',20)


  add object oSRCPRG_1_11 as StdTableCombo with uid="HJTACMGCIR",rtseq=11,rtrep=.f.,left=91,top=131,width=140,height=22;
    , cDescEmptyElement = "Tutti";
    , ToolTipText = "Filtra i gadget per modello";
    , HelpContextID = 176899110;
    , cFormVar="w_SRCPRG",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="MOD_GADG";
    , cTable='MOD_GADG',cKey='MGSRCPRG',cValue='MGDESCRI',cOrderBy='MGSRCPRG',xDefault=space(50);
  , bGlobalFont=.t.


  func oSRCPRG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSRCPRG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oGRPCOD_1_12 as StdTableNoItemCombo with uid="CMMZMEUDAR",rtseq=12,rtrep=.f.,left=91,top=156,width=140,height=22;
    , cDescEmptyElement = "Tutte";
    , ToolTipText = "Filtra i gadget in base all'area di appartenenza";
    , HelpContextID = 122622822;
    , cFormVar="w_GRPCOD",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GRP_GADG',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGCODICE',xDefault=space(5);
  , bGlobalFont=.t.



  add object oINUSE_1_13 as StdCombo with uid="QDITILRMHY",value=1,rtseq=13,rtrep=.f.,left=309,top=156,width=140,height=22;
    , ToolTipText = "Filtra i gadget attualmente in uso da qualche utente";
    , HelpContextID = 222339194;
    , cFormVar="w_INUSE",RowSource=""+"Tutti,"+"In uso,"+"Non in uso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINUSE_1_13.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    ' '))))
  endfunc
  func oINUSE_1_13.GetRadio()
    this.Parent.oContained.w_INUSE = this.RadioValue()
    return .t.
  endfunc

  func oINUSE_1_13.SetRadio()
    this.Parent.oContained.w_INUSE=trim(this.Parent.oContained.w_INUSE)
    this.value = ;
      iif(this.Parent.oContained.w_INUSE=='',1,;
      iif(this.Parent.oContained.w_INUSE=='S',2,;
      iif(this.Parent.oContained.w_INUSE=='N',3,;
      0)))
  endfunc


  add object oCHKATT_1_14 as StdCombo with uid="DGRBKAYLJD",value=1,rtseq=14,rtrep=.f.,left=309,top=131,width=140,height=22;
    , ToolTipText = "Filtra i gadget abilitati o disabilitati";
    , HelpContextID = 127711526;
    , cFormVar="w_CHKATT",RowSource=""+"Tutti,"+"Abilitati,"+"Disabilitati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHKATT_1_14.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    ' '))))
  endfunc
  func oCHKATT_1_14.GetRadio()
    this.Parent.oContained.w_CHKATT = this.RadioValue()
    return .t.
  endfunc

  func oCHKATT_1_14.SetRadio()
    this.Parent.oContained.w_CHKATT=trim(this.Parent.oContained.w_CHKATT)
    this.value = ;
      iif(this.Parent.oContained.w_CHKATT=='',1,;
      iif(this.Parent.oContained.w_CHKATT=='S',2,;
      iif(this.Parent.oContained.w_CHKATT=='N',3,;
      0)))
  endfunc


  add object oFORAZI_1_15 as StdCombo with uid="ZKTNVFHKAG",value=1,rtseq=15,rtrep=.f.,left=543,top=131,width=140,height=22;
    , ToolTipText = "Filtra i gadget aziendali";
    , HelpContextID = 217919574;
    , cFormVar="w_FORAZI",RowSource=""+"Tutti,"+"Aziendali,"+"Non aziendali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFORAZI_1_15.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    ' '))))
  endfunc
  func oFORAZI_1_15.GetRadio()
    this.Parent.oContained.w_FORAZI = this.RadioValue()
    return .t.
  endfunc

  func oFORAZI_1_15.SetRadio()
    this.Parent.oContained.w_FORAZI=trim(this.Parent.oContained.w_FORAZI)
    this.value = ;
      iif(this.Parent.oContained.w_FORAZI=='',1,;
      iif(this.Parent.oContained.w_FORAZI=='S',2,;
      iif(this.Parent.oContained.w_FORAZI=='N',3,;
      0)))
  endfunc


  add object ZoomGadg as cp_szoombox with uid="DXMSEDJBAP",left=0, top=182, width=757,height=209,;
    caption='Zoom Gadget',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,cMenuFile="",cZoomFile="GSUT_KGG",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cTable="GAD_MAST",bQueryOnLoad=.t.,;
    cEvent = "Init,AggiornaCruscotto",;
    nPag=1;
    , HelpContextID = 127061354


  add object oBtn_1_17 as StdButton with uid="AKWJQTHXDN",left=700, top=133, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Ricerca gadget in base ai filtri";
    , HelpContextID = 144857878;
    , Caption='Ricerca';
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      this.parent.oContained.NotifyEvent("AggiornaCruscotto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="KSJAHKXAEV",left=9, top=400, width=48,height=45,;
    CpPicture="addtabl.bmp", caption="", nPag=1;
    , ToolTipText = "Consente l'inserimento di un nuovo gadget";
    , HelpContextID = 175888682;
    , Caption='Nuovo';
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        opengest("L","GSUT_MGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="TXIYPCXWQI",left=59, top=400, width=48,height=45,;
    CpPicture="exe\bmp\apertura.ico", caption="", nPag=1;
    , ToolTipText = "Apre il gadget corrente";
    , HelpContextID = 24686330;
    , Caption='Apri';
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        opengest("A","GSUT_MGA","GACODICE",.w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(Nvl(.w_CODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="FNFRBQUSQG",left=9, top=447, width=48,height=45,;
    CpPicture="BMP\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Consente la modifica del gadget corrente";
    , HelpContextID = 233206055;
    , Caption='Modifica';
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        opengest("M","GSUT_MGA","GACODICE",.w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(Nvl(.w_CODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="AZVFOCNGGA",left=59, top=447, width=48,height=45,;
    CpPicture="canc.bmp", caption="", nPag=1;
    , ToolTipText = "Cancella i gadget selezionati non in uso";
    , HelpContextID = 13697671;
    , Caption='Cancella';
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Delete", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="LQBZFJRBGS",left=119, top=400, width=48,height=45,;
    CpPicture="trfgo.bmp", caption="", nPag=1;
    , ToolTipText = "Abilita i gadget selezionati";
    , HelpContextID = 152679174;
    , Caption='Abilita';
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Enable", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="RHQQFEAHMR",left=169, top=400, width=48,height=45,;
    CpPicture="trfstop.bmp", caption="", nPag=1;
    , ToolTipText = "Disabilita i gadget selezionati";
    , HelpContextID = 228573919;
    , Caption='Disabilita';
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Disable", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="OBBAYCKNBP",left=219, top=447, width=48,height=45,;
    CpPicture="BMP/legenda_colori.bmp", caption="", nPag=1;
    , ToolTipText = "Cambia il tema di default dei gadget selezionati";
    , HelpContextID = 25233610;
    , Caption='Tema';
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"SetDefaultTheme", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="TBFDLZEKKP",left=170, top=447, width=48,height=45,;
    CpPicture="diba3.bmp", caption="", nPag=1;
    , ToolTipText = "Cancella le propriet� personalizzate per utente dai gadget selezionati";
    , HelpContextID = 238123466;
    , Caption='Default';
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"RestoreProperties", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="TFLYPBYDPY",left=700, top=447, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la maschera";
    , HelpContextID = 24746938;
    , Caption='Esci';
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object LblUso as cp_calclbl with uid="BSCTERWHHP",left=688, top=418, width=60,height=17,;
    caption='LblUso',;
   bGlobalFont=.t.,;
    fontUnderline=.f.,bGlobalFont=.f.,caption="In uso",alignment=0,fontname="Arial",fontsize=8,fontBold=.f.,fontItalic=.t.,;
    nPag=1;
    , HelpContextID = 77783990


  add object oUTEESC_1_38 as StdCombo with uid="FBFPBKJITC",value=1,rtseq=18,rtrep=.f.,left=543,top=156,width=140,height=22;
    , ToolTipText = "Filtra i gadget esclusivi per utente";
    , HelpContextID = 110126662;
    , cFormVar="w_UTEESC",RowSource=""+"Tutti,"+"Solo esclusivi,"+"Non esclusivi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUTEESC_1_38.RadioValue()
    return(iif(this.value =1,'',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    ''))))
  endfunc
  func oUTEESC_1_38.GetRadio()
    this.Parent.oContained.w_UTEESC = this.RadioValue()
    return .t.
  endfunc

  func oUTEESC_1_38.SetRadio()
    this.Parent.oContained.w_UTEESC=trim(this.Parent.oContained.w_UTEESC)
    this.value = ;
      iif(this.Parent.oContained.w_UTEESC=='',1,;
      iif(this.Parent.oContained.w_UTEESC=='S',2,;
      iif(this.Parent.oContained.w_UTEESC=='N',3,;
      0)))
  endfunc


  add object LblAtt as cp_calclbl with uid="QROJMHXGBK",left=688, top=400, width=60,height=17,;
    caption='LblAtt',;
   bGlobalFont=.t.,;
    fontUnderline=.f.,bGlobalFont=.f.,caption="Disabilitato",alignment=0,fontname="Arial",fontsize=8,fontBold=.f.,fontItalic=.t.,;
    nPag=1;
    , HelpContextID = 161407926


  add object oBtn_1_44 as StdButton with uid="CTCTPNVDGB",left=269, top=447, width=48,height=45,;
    CpPicture="schede.bmp", caption="", nPag=1;
    , ToolTipText = "Rende generici (utilizzabili da ogni utente) i gadget selezionati";
    , HelpContextID = 245565141;
    , Caption='Generico';
  , bGlobalFont=.t.

    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Generic", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_45 as StdButton with uid="CSCALCFQWE",left=219, top=400, width=48,height=45,;
    CpPicture="abbina.bmp", caption="", nPag=1;
    , ToolTipText = "Abbina i gadget selezionati non in uso all'area scelta";
    , HelpContextID = 107365126;
    , Caption='Abbina';
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"SetGroup", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_46 as StdButton with uid="XBNODWCZFW",left=269, top=400, width=48,height=45,;
    CpPicture="disabbina.bmp", caption="", nPag=1;
    , ToolTipText = "Disabbina i gadget selezionati non in uso dalla loro area";
    , HelpContextID = 157327180;
    , Caption='Disabbina';
  , bGlobalFont=.t.

    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"ResetGroup", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_47 as StdButton with uid="RHCWDNIQCK",left=119, top=447, width=48,height=45,;
    CpPicture="wedel.bmp", caption="", nPag=1;
    , ToolTipText = "Disassocia i gadget selezionati dalla My Gadget";
    , HelpContextID = 145736233;
    , Caption='Disassocia';
  , bGlobalFont=.t.

    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Remove", .w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRESET_1_50 as StdCheck with uid="ENXAWBSDHP",rtseq=19,rtrep=.f.,left=385, top=451, caption="Reset valori",;
    ToolTipText = "Se attivo sbianca i valori delle propriet� per cui � attivo il relativo check reset",;
    HelpContextID = 207538410,;
    cFormVar="w_RESET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRESET_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRESET_1_50.GetRadio()
    this.Parent.oContained.w_RESET = this.RadioValue()
    return .t.
  endfunc

  func oRESET_1_50.SetRadio()
    this.Parent.oContained.w_RESET=trim(this.Parent.oContained.w_RESET)
    this.value = ;
      iif(this.Parent.oContained.w_RESET=='S',1,;
      0)
  endfunc


  add object oTIPPROP_1_51 as StdCombo with uid="RNGIHJDZKI",rtseq=20,rtrep=.f.,left=385,top=427,width=173,height=21;
    , ToolTipText = "Modalit� di esportazione delle propriet� del gadget";
    , HelpContextID = 42732342;
    , cFormVar="w_TIPPROP",RowSource=""+"Solo propriet� standard,"+"Standard e personalizzate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPROP_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPPROP_1_51.GetRadio()
    this.Parent.oContained.w_TIPPROP = this.RadioValue()
    return .t.
  endfunc

  func oTIPPROP_1_51.SetRadio()
    this.Parent.oContained.w_TIPPROP=trim(this.Parent.oContained.w_TIPPROP)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPROP=='S',1,;
      iif(this.Parent.oContained.w_TIPPROP=='T',2,;
      0))
  endfunc


  add object oBtn_1_53 as StdButton with uid="SVOYBCFHDT",left=630, top=447, width=48,height=45,;
    CpPicture="BMP\save.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare la configurazione dei gadget selezionati su file di testo (*.txt) , reimportabile dal carica/salva dati esterni";
    , HelpContextID = 162346054;
    , caption='\<Esporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      with this.Parent.oContained
        GSUT_BGG(this.Parent.oContained,"Export")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_FILE_NAME) And !Empty(.w_FILE_EXT) And Lower(.w_FILE_EXT)=='txt')
      endwith
    endif
  endfunc

  add object oPATH_1_55 as StdField with uid="YJTBDAQENK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso e nome del file di testo (*.txt)",;
    HelpContextID = 26983690,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=385, Top=403, InputMask=replicate('X',200)

  proc oPATH_1_55.mAfter
    with this.Parent.oContained
      .bdontreporterror=.T.
    endwith
  endproc


  add object oObj_1_56 as cp_askfile with uid="OBMZWDHYCM",left=659, top=405, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var='w_PATH',;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 31863338

  add object oStr_1_27 as StdString with uid="GQOTJAGUHO",Visible=.t., Left=3, Top=15,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="YKHVGNJLIN",Visible=.t., Left=3, Top=38,;
    Alignment=1, Width=86, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="ECRPPUIHYJ",Visible=.t., Left=3, Top=62,;
    Alignment=1, Width=86, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="VQOXCKJULX",Visible=.t., Left=3, Top=136,;
    Alignment=1, Width=86, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FBJHWJIWWL",Visible=.t., Left=3, Top=160,;
    Alignment=1, Width=86, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="FNMMQUAHAT",Visible=.t., Left=3, Top=84,;
    Alignment=1, Width=86, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KYSYRXDTTG",Visible=.t., Left=269, Top=160,;
    Alignment=1, Width=40, Height=18,;
    Caption="In uso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="UDPDUDVBLQ",Visible=.t., Left=259, Top=136,;
    Alignment=1, Width=50, Height=18,;
    Caption="Abilitato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="YIHTSBXWAQ",Visible=.t., Left=482, Top=136,;
    Alignment=1, Width=61, Height=18,;
    Caption="Aziendale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="LZQRHUZFTJ",Visible=.t., Left=482, Top=160,;
    Alignment=1, Width=61, Height=18,;
    Caption="Esclusivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="XMACPDKSNN",Visible=.t., Left=3, Top=109,;
    Alignment=1, Width=86, Height=18,;
    Caption="Filtra per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="QMFROMLJQK",Visible=.t., Left=330, Top=430,;
    Alignment=1, Width=53, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ZHUQMDQCAT",Visible=.t., Left=325, Top=406,;
    Alignment=1, Width=58, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oBox_1_33 as StdBox with uid="KDIDXRMAUI",left=6, top=397, width=106,height=99

  add object oBox_1_34 as StdBox with uid="GLFFRWBTFU",left=115, top=397, width=207,height=99

  add object oBox_1_49 as StdBox with uid="MGHNQUJWVZ",left=325, top=397, width=359,height=99
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kgg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kgg
Define Class StdTableNoItemCombo As StdTableCombo
   Proc Init
     DoDefault()
     With This
       .nValues = This.nValues + 1
       Dimension .ComboValues(.nValues)
       .ComboValues[.nValues] = '#####'
       .AddItem('Nessuna')
     Endwith
   Endproc
Enddefine
* --- Fine Area Manuale
