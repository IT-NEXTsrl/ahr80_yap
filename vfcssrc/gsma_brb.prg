* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brb                                                        *
*              Registro beni in lavorazione                                    *
*                                                                              *
*      Author: Zucchetti AT                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-20                                                      *
* Last revis.: 2008-11-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brb",oParentObject)
return(i_retval)

define class tgsma_brb as StdBatch
  * --- Local variables
  w_parto = space(12)
  w_capo = space(5)
  w_provo = space(2)
  w_codo = space(16)
  w_loco = space(30)
  w_indi = space(35)
  w_Capsoc = space(30)
  w_Intervallo = 0
  w_Campo = space(10)
  w_Messaggio = space(10)
  w_DataBloc = ctod("  /  /  ")
  w_Conferma = space(1)
  w_TmpVar = 0
  w_MAGAZ = space(5)
  w_MAGAT = space(5)
  w_CONTPAG = space(1)
  w_ULTPAG = 0
  w_DATARB = ctod("  /  /  ")
  w_MAGCOLL = space(5)
  w_TIPOCLF = space(1)
  w_OLDCODMAG = space(5)
  w_OLDCODBU = space(3)
  * --- WorkFile variables
  AZIENDA_idx=0
  SEDIAZIE_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registro beni in lavorazione
    * --- Per filtro query
    this.w_TIPOCLF = IIF(Empty(this.oParentObject.w_CODCON), " ", this.oParentObject.w_TIPCON)
    * --- Controllo selezioni impostate sulla maschera
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Creazione cursore stampa
    ah_msg("Elaborazione registro beni in lavorazione...")
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- Le 2 query sono necessarie perch� in AHE lo scarico delle materi prime
      *     da conto lavoro avviene tramite Movimento di magazzino, in AHR tramite Doc Interno
      vq_exec("query\GSMA1BRB",this,"__tmp__")
    else
      vq_exec("query\GSMA_BRB",this,"__tmp__")
    endif
    if reccount("__tmp__")=0
      if this.oParentObject.w_SelMod="B"
        if ah_YesNo("Per la selezione effettuata non esistono dati da stampare%0Si desidera ugualmente aggiornare la data di ultima stampa?")
          * --- Lancio la maschera di aggiornamento
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        AH_ERRORMSG("Per la selezione effettuata non esistono dati da stampare",48)
      endif
      if used("__tmp__")
        select __tmp__
        use
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Esecuzione report
     
 CODESE = this.oParentObject.w_CODESE 
 DATINI = this.oParentObject.w_DATINI 
 DATFIN = this.oParentObject.w_DATFIN 
 ULTDAT = this.oParentObject.w_ULTDAT 
 CODMAG = this.oParentObject.w_CODMAG 
 DESMAG = this.oParentObject.w_DESMAG 
 SELMOD = this.oParentObject.w_SELMOD 
 CODINI = this.oParentObject.w_CODINI 
 CODFIN = this.oParentObject.w_CODFIN 
 MASTRO = this.oParentObject.w_MASTRO 
 CATOMO = this.oParentObject.w_CATOMO 
 CODCON = this.oParentObject.w_CODCON 
 TIPCON = this.oParentObject.w_TIPCON 
 BU=this.oParentObject.w_BU 
 CAUSEL=this.oParentObject.w_CAUSEL
    if this.oParentObject.w_SELMOD="S"
      cp_chprn("QUERY\gsma_brb.FRX","",this.oParentObject)
    else
      * --- Leggo dati da impostare nella testata di stampa
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCAPSOC"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI,AZCAPSOC;
          from (i_cTable) where;
              AZCODAZI = this.oParentObject.w_AZIENDA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_indi = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
        this.w_loco = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
        this.w_capo = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
        this.w_provo = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
        this.w_parto = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_codo = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        this.w_Capsoc = NVL(cp_ToDate(_read_.AZCAPSOC),cp_NullValue(_read_.AZCAPSOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se nella tabella Sedi presente nell'anagrafica Dati Azienda � stata 
      *     specificata una Sede Legale.Effettuo questo controllo solo se nella numerazione
      *     registri ho specificato Numerazione Pagine Contestuale alla stampa
      * --- Select from SEDIAZIE
      i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select SEINDIRI,SE___CAP,SELOCALI,SEPROVIN  from "+i_cTable+" SEDIAZIE ";
            +" where SECODAZI="+cp_ToStrODBC(this.oParentObject.w_AZIENDA)+" and SETIPRIF='SL'";
             ,"_Curs_SEDIAZIE")
      else
        select SEINDIRI,SE___CAP,SELOCALI,SEPROVIN from (i_cTable);
         where SECODAZI=this.oParentObject.w_AZIENDA and SETIPRIF="SL";
          into cursor _Curs_SEDIAZIE
      endif
      if used('_Curs_SEDIAZIE')
        select _Curs_SEDIAZIE
        locate for 1=1
        do while not(eof())
        this.w_capo = _Curs_SEDIAZIE.SE___CAP
        this.w_provo = _Curs_SEDIAZIE.SEPROVIN
        this.w_loco = _Curs_SEDIAZIE.SELOCALI
        this.w_indi = _Curs_SEDIAZIE.SEINDIRI
        Exit
          select _Curs_SEDIAZIE
          continue
        enddo
        use
      endif
       
 L_INDI=this.w_INDI 
 L_LOCO=this.w_LOCO 
 L_CAPO=this.w_CAPO 
 L_PROVO=this.w_PROVO 
 L_CODO=this.w_CODO 
 L_PARTO=this.w_PARTO 
 L_CAPSOC=this.w_CAPSOC 
 L_DNUMGM=this.oParentObject.w_DNUMGM 
 L_ULTPLG=this.oParentObject.w_ULTPLG 
 L_PCONGM=this.oParentObject.w_PCONGM
      L_ULTPAG=0
      cp_chprn("QUERY\gsma1brb.FRX","",this.oParentObject)
    endif
    * --- Aggiornamento archivi 
    if this.oParentObject.w_SelMod="B"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used("__tmp__")
      select __tmp__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo delle selezioni impostate sulla maschera
    * --- Data iniziale (Consecutiva all'ultima stampa od uguale all'inizio dell'esercizio)
    if this.oParentObject.w_SELMOD<>"R"
      if ((this.oParentObject.w_DatIni<>this.oParentObject.w_UltDat+1 .and. (.not. empty(this.oParentObject.w_UltDat))) .or. (this.oParentObject.w_DatIni<>this.oParentObject.w_InizEser .and. empty(this.oParentObject.w_UltDat)))
        if this.oParentObject.w_SELMOD="B"
          AH_ERRORMSG("Data iniziale non consecutiva all'ultima stampa",16)
          i_retcode = 'stop'
          return
        else
          if NOT AH_YESNO("Data iniziale non consecutiva all'ultima stampa%0Potrebbero essere ignorate alcune registrazioni%0%0Confermi?")
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
    * --- Intervallo di date
    if (this.oParentObject.w_DatIni<this.oParentObject.w_InizEser .or. this.oParentObject.w_DatFin>this.oParentObject.w_FineEser)
      AH_ERRORMSG("L'intervallo di date non � contenuto nell'esercizio attuale%0Selezionare l'esercizio di competenza corretto per effettuare la stampa con le selezioni impostate",48)
      i_retcode = 'stop'
      return
    endif
    * --- Se ristampa la data finale non deve superare la data di stampa definitiva
    if this.oParentObject.w_SELMOD="R" .and. this.oParentObject.w_DATFIN>this.oParentObject.w_ULTDAT
      AH_ERRORMSG("La data finale supera l'ultima data di stampa definitiva",48)
      i_retcode = 'stop'
      return
    endif
    * --- La data finale non deve inferiore alla data finale
    if this.oParentObject.w_DATINI>this.oParentObject.w_DATFIN
      AH_ERRORMSG("La data iniziale � maggiore di quella finale",48)
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo progressivo ultima riga stampata
    this.w_CONTPAG = this.oParentObject.w_PCONGM
    this.w_DATARB = this.oParentObject.w_DATFIN
    if reccount("__tmp__")=0
      this.w_ULTPAG = this.oParentObject.w_ULTPLG
    else
      this.w_ULTPAG = IIF( Type("L_ULTPAG")<>"N", iif(TYPE("g_PAGENUM")="N" and g_PAGENUM>0, g_PAGENUM+this.oParentObject.w_ULTPLG, 0), L_ULTPAG)
    endif
    * --- Richiesta di conferma per l'aggiornamento
    this.w_Conferma = "N"
    do GSMA_SR1 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiornamento Files
    if this.w_conferma = "S"
      ah_msg("Aggiornamento progressivi registro beni in lavorazione...")
      * --- Try
      local bErr_0386CEB8
      bErr_0386CEB8=bTrsErr
      this.Try_0386CEB8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Messaggio errore aggiornamento archivi
        ah_errormsg("Impossibile marcare come definitive le registrazioni stampate",48)
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_0386CEB8
      * --- End
    endif
    * --- All'init w_CODMAG e w_BU sono inizializzati. Per non sovrascrivere si memorizzano gli attuali valori e si riassegnano
    this.w_OLDCODMAG = this.oParentObject.w_CODMAG
    this.w_OLDCODBU = this.oParentObject.w_BU
    this.oParentobject.Init()
    this.oParentobject.mCalc()
    if !EMPTY( this.w_OLDCODMAG )
      this.oParentObject.w_CODMAG = this.w_OLDCODMAG
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDESMAG,MGMAGRAG,MGDTOBSO"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDESMAG,MGMAGRAG,MGDTOBSO;
          from (i_cTable) where;
              MGCODMAG = this.oParentObject.w_CODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
        this.oParentObject.w_MGMAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
        this.oParentObject.w_MGDTOBSO = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !EMPTY( this.w_OLDCODBU )
      this.oParentObject.w_BU = this.w_OLDCODBU
    endif
    if used("__tmp__")
      select __tmp__
      use
    endif
  endproc
  proc Try_0386CEB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scrittura ultima data ed ultimo numero di riga
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- I progressivi sono aggiornati in Azienda
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZPRPRBE ="+cp_NullLink(cp_ToStrODBC(this.w_ULTPAG),'AZIENDA','AZPRPRBE');
        +",AZSTARBE ="+cp_NullLink(cp_ToStrODBC(this.w_DATARB),'AZIENDA','AZSTARBE');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_AZIENDA);
               )
      else
        update (i_cTable) set;
            AZPRPRBE = this.w_ULTPAG;
            ,AZSTARBE = this.w_DATARB;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.oParentObject.w_AZIENDA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- I progressivi sono aggiornati in NUMEREGI (solo AHE)
      do GSMAEBRB with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Aggiornamento variabili maschera
    wait clear
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='SEDIAZIE'
    this.cWorkTables[3]='MAGAZZIN'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
