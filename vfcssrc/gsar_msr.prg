* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_msr                                                        *
*              Struttura gruppi risorse                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-05                                                      *
* Last revis.: 2013-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_msr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_msr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_msr")
  return

* --- Class definition
define class tgsar_msr as StdPCForm
  Width  = 577
  Height = 258
  Top    = 10
  Left   = 10
  cComment = "Struttura gruppi risorse"
  cPrg = "gsar_msr"
  HelpContextID=97089385
  add object cnt as tcgsar_msr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_msr as PCContext
  w_SRCODICE = space(5)
  w_CPROWORD = 0
  w_SRTIPCOL = space(1)
  w_PATIPRIS = space(1)
  w_SRRISCOL = space(5)
  w_SRDESCRI = space(40)
  w_SRCODPAD = space(5)
  w_SRCOGNOM = space(40)
  w_SRNOME = space(40)
  w_SRDESRIS = space(40)
  w_SRFLRESP = space(1)
  w_ErroreRisorsa = space(1)
  w_OB_TEST = space(8)
  proc Save(i_oFrom)
    this.w_SRCODICE = i_oFrom.w_SRCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SRTIPCOL = i_oFrom.w_SRTIPCOL
    this.w_PATIPRIS = i_oFrom.w_PATIPRIS
    this.w_SRRISCOL = i_oFrom.w_SRRISCOL
    this.w_SRDESCRI = i_oFrom.w_SRDESCRI
    this.w_SRCODPAD = i_oFrom.w_SRCODPAD
    this.w_SRCOGNOM = i_oFrom.w_SRCOGNOM
    this.w_SRNOME = i_oFrom.w_SRNOME
    this.w_SRDESRIS = i_oFrom.w_SRDESRIS
    this.w_SRFLRESP = i_oFrom.w_SRFLRESP
    this.w_ErroreRisorsa = i_oFrom.w_ErroreRisorsa
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_SRCODICE = this.w_SRCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SRTIPCOL = this.w_SRTIPCOL
    i_oTo.w_PATIPRIS = this.w_PATIPRIS
    i_oTo.w_SRRISCOL = this.w_SRRISCOL
    i_oTo.w_SRDESCRI = this.w_SRDESCRI
    i_oTo.w_SRCODPAD = this.w_SRCODPAD
    i_oTo.w_SRCOGNOM = this.w_SRCOGNOM
    i_oTo.w_SRNOME = this.w_SRNOME
    i_oTo.w_SRDESRIS = this.w_SRDESRIS
    i_oTo.w_SRFLRESP = this.w_SRFLRESP
    i_oTo.w_ErroreRisorsa = this.w_ErroreRisorsa
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_msr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 577
  Height = 258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-27"
  HelpContextID=97089385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  STRU_RIS_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "STRU_RIS"
  cKeySelect = "SRCODICE"
  cKeyWhere  = "SRCODICE=this.w_SRCODICE"
  cKeyDetail  = "SRCODICE=this.w_SRCODICE and SRRISCOL=this.w_SRRISCOL"
  cKeyWhereODBC = '"SRCODICE="+cp_ToStrODBC(this.w_SRCODICE)';

  cKeyDetailWhereODBC = '"SRCODICE="+cp_ToStrODBC(this.w_SRCODICE)';
      +'+" and SRRISCOL="+cp_ToStrODBC(this.w_SRRISCOL)';

  cKeyWhereODBCqualified = '"STRU_RIS.SRCODICE="+cp_ToStrODBC(this.w_SRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'STRU_RIS.CPROWORD,STRU_RIS.SRTIPCOL,STRU_RIS.SRRISCOL'
  cPrg = "gsar_msr"
  cComment = "Struttura gruppi risorse"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SRCODICE = space(5)
  w_CPROWORD = 0
  w_SRTIPCOL = space(1)
  o_SRTIPCOL = space(1)
  w_PATIPRIS = space(1)
  w_SRRISCOL = space(5)
  o_SRRISCOL = space(5)
  w_SRDESCRI = space(40)
  w_SRCODPAD = space(5)
  w_SRCOGNOM = space(40)
  w_SRNOME = space(40)
  w_SRDESRIS = space(40)
  w_SRFLRESP = space(1)
  w_ErroreRisorsa = .F.
  w_OB_TEST = ctod('  /  /  ')

  * --- Children pointers
  GSAR_MPH = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAR_MPH additive
    with this
      .Pages(1).addobject("oPag","tgsar_msrPag1","gsar_msr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAR_MPH
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='STRU_RIS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STRU_RIS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STRU_RIS_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MPH = CREATEOBJECT('stdLazyChild',this,'GSAR_MPH')
    return

  procedure NewContext()
    return(createobject('tsgsar_msr'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSAR_MPH)
      this.GSAR_MPH.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSAR_MPH.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MPH)
      this.GSAR_MPH.DestroyChildrenChain()
      this.GSAR_MPH=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MPH.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MPH.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MPH.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MPH.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_SRCODICE,"APCODGRU";
             ,.w_SRRISCOL,"APPERSON";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from STRU_RIS where SRCODICE=KeySet.SRCODICE
    *                            and SRRISCOL=KeySet.SRRISCOL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2],this.bLoadRecFilter,this.STRU_RIS_IDX,"gsar_msr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STRU_RIS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STRU_RIS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STRU_RIS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SRCODICE',this.w_SRCODICE  )
      select * from (i_cTable) STRU_RIS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SRCODPAD = space(5)
        .w_ErroreRisorsa = .f.
        .w_OB_TEST = i_INIDAT
        .w_SRCODICE = NVL(SRCODICE,space(5))
        .w_PATIPRIS = .w_SRTIPCOL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'STRU_RIS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_SRDESCRI = space(40)
          .w_SRCOGNOM = space(40)
          .w_SRNOME = space(40)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_SRTIPCOL = NVL(SRTIPCOL,space(1))
          .w_SRRISCOL = NVL(SRRISCOL,space(5))
          .link_2_4('Load')
        .w_SRDESRIS = IIF(.w_SRTIPCOL='P', ALLTRIM(.w_SRCOGNOM)+" "+ALLTRIM(.w_SRNOME), .w_SRDESCRI)
          .w_SRFLRESP = NVL(SRFLRESP,space(1))
          select (this.cTrsName)
          append blank
          replace SRCODICE with .w_SRCODICE
          replace SRRISCOL with .w_SRRISCOL
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_SRCODICE=space(5)
      .w_CPROWORD=10
      .w_SRTIPCOL=space(1)
      .w_PATIPRIS=space(1)
      .w_SRRISCOL=space(5)
      .w_SRDESCRI=space(40)
      .w_SRCODPAD=space(5)
      .w_SRCOGNOM=space(40)
      .w_SRNOME=space(40)
      .w_SRDESRIS=space(40)
      .w_SRFLRESP=space(1)
      .w_ErroreRisorsa=.f.
      .w_OB_TEST=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_SRTIPCOL = 'P'
        .w_PATIPRIS = .w_SRTIPCOL
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_SRRISCOL))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,9,.f.)
        .w_SRDESRIS = IIF(.w_SRTIPCOL='P', ALLTRIM(.w_SRCOGNOM)+" "+ALLTRIM(.w_SRNOME), .w_SRDESCRI)
        .w_SRFLRESP = ICASE(EMPTY(.w_SRTIPCOL), 'N', .w_SRTIPCOL<>'P', 'N', EMPTY(.w_SRFLRESP), 'N', .w_SRFLRESP)
        .DoRTCalc(12,12,.f.)
        .w_OB_TEST = i_INIDAT
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'STRU_RIS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSAR_MPH.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'STRU_RIS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MPH.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SRCODICE,"SRCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_SRTIPCOL N(3);
      ,t_SRRISCOL C(5);
      ,t_SRDESRIS C(40);
      ,t_SRFLRESP N(3);
      ,SRCODICE C(5);
      ,SRRISCOL C(5);
      ,t_SRDESCRI C(40);
      ,t_SRCOGNOM C(40);
      ,t_SRNOME C(40);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_msrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.controlsource=this.cTrsName+'.t_SRTIPCOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSRRISCOL_2_4.controlsource=this.cTrsName+'.t_SRRISCOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSRDESRIS_2_8.controlsource=this.cTrsName+'.t_SRDESRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.controlsource=this.cTrsName+'.t_SRFLRESP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(54)
    this.AddVLine(154)
    this.AddVLine(257)
    this.AddVLine(498)
    this.AddVLine(530)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2])
      *
      * insert into STRU_RIS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STRU_RIS')
        i_extval=cp_InsertValODBCExtFlds(this,'STRU_RIS')
        i_cFldBody=" "+;
                  "(SRCODICE,CPROWORD,SRTIPCOL,SRRISCOL,SRFLRESP,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SRCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_SRTIPCOL)+","+cp_ToStrODBCNull(this.w_SRRISCOL)+","+cp_ToStrODBC(this.w_SRFLRESP)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STRU_RIS')
        i_extval=cp_InsertValVFPExtFlds(this,'STRU_RIS')
        cp_CheckDeletedKey(i_cTable,0,'SRCODICE',this.w_SRCODICE,'SRRISCOL',this.w_SRRISCOL)
        INSERT INTO (i_cTable) (;
                   SRCODICE;
                  ,CPROWORD;
                  ,SRTIPCOL;
                  ,SRRISCOL;
                  ,SRFLRESP;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SRCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_SRTIPCOL;
                  ,this.w_SRRISCOL;
                  ,this.w_SRFLRESP;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
    this.Calculate_XGDAXPHEVD()
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_SRCODICE<>SRCODICE
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'STRU_RIS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and SRRISCOL="+cp_ToStrODBC(&i_TN.->SRRISCOL)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'STRU_RIS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and SRRISCOL=&i_TN.->SRRISCOL;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_SRCODICE<>SRCODICE
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSAR_MPH.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_SRCODICE,"APCODGRU";
                     ,this.w_SRRISCOL,"APPERSON";
                     )
              this.GSAR_MPH.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and SRRISCOL="+cp_ToStrODBC(&i_TN.->SRRISCOL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and SRRISCOL=&i_TN.->SRRISCOL;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace SRRISCOL with this.w_SRRISCOL
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STRU_RIS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'STRU_RIS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",SRTIPCOL="+cp_ToStrODBC(this.w_SRTIPCOL)+;
                     ",SRFLRESP="+cp_ToStrODBC(this.w_SRFLRESP)+;
                     ",SRRISCOL="+cp_ToStrODBC(this.w_SRRISCOL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and SRRISCOL="+cp_ToStrODBC(SRRISCOL)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'STRU_RIS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,SRTIPCOL=this.w_SRTIPCOL;
                     ,SRFLRESP=this.w_SRFLRESP;
                     ,SRRISCOL=this.w_SRRISCOL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and SRRISCOL=&i_TN.->SRRISCOL;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSAR_MPH.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_SRCODICE,"APCODGRU";
               ,this.w_SRRISCOL,"APPERSON";
               )
          this.GSAR_MPH.mReplace()
          this.GSAR_MPH.bSaveContext=.f.
        endif
      endscan
     this.GSAR_MPH.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAR_MPH : Deleting
        this.GSAR_MPH.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_SRCODICE,"APCODGRU";
               ,this.w_SRRISCOL,"APPERSON";
               )
        this.GSAR_MPH.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete STRU_RIS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and SRRISCOL="+cp_ToStrODBC(&i_TN.->SRRISCOL)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and SRRISCOL=&i_TN.->SRRISCOL;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STRU_RIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STRU_RIS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_PATIPRIS = .w_SRTIPCOL
        .DoRTCalc(5,9,.t.)
          .w_SRDESRIS = IIF(.w_SRTIPCOL='P', ALLTRIM(.w_SRCOGNOM)+" "+ALLTRIM(.w_SRNOME), .w_SRDESCRI)
        if .o_SRTIPCOL<>.w_SRTIPCOL.or. .o_SRRISCOL<>.w_SRRISCOL
          .w_SRFLRESP = ICASE(EMPTY(.w_SRTIPCOL), 'N', .w_SRTIPCOL<>'P', 'N', EMPTY(.w_SRFLRESP), 'N', .w_SRFLRESP)
        endif
        if .o_SRRISCOL<>.w_SRRISCOL
          .Calculate_BWVADKCBHI()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SRDESCRI with this.w_SRDESCRI
      replace t_SRCOGNOM with this.w_SRCOGNOM
      replace t_SRNOME with this.w_SRNOME
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_XGDAXPHEVD()
    with this
          * --- GSAR_BSR(CHK) - CONTROLLI RICORSIONE
          gsar_bsr(this;
              ,'CHK';
             )
    endwith
  endproc
  proc Calculate_BWVADKCBHI()
    with this
          * --- Controllo tipo risorsa
          .w_ErroreRisorsa = IIF(!.w_SRRISCOL==.w_SRCODPAD,.F.,ah_ErrorMsg("Impossibile impostare risorsa o gruppo identico alla risorsa o gruppo origine"))
          .w_SRRISCOL = IIF(.w_ErroreRisorsa,SPACE(5),.w_SRRISCOL)
          .w_SRDESRIS = IIF(.w_ErroreRisorsa,'',.w_SRDESRIS)
          .w_SRCOGNOM = IIF(.w_ErroreRisorsa,'',.w_SRCOGNOM)
          .w_SRDESCRI = IIF(.w_ErroreRisorsa,'',.w_SRDESCRI)
          .w_SRNOME = IIF(.w_ErroreRisorsa,'',.w_SRNOME)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSRFLRESP_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSRFLRESP_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLinkPC_2_12.enabled =this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLinkPC_2_12.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSAR_MPH.visible")=='L' And this.GSAR_MPH.visible And !this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLinkPC_2_12.enabled
      this.GSAR_MPH.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLinkPC_2_12.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLinkPC_2_12.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSAR_MPH.visible")=='L' And this.GSAR_MPH.visible And !this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLinkPC_2_12.visible
      this.GSAR_MPH.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SRRISCOL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SRRISCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_SRTIPCOL;
                     ,'DPCODICE',trim(this.w_SRRISCOL))
          select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SRRISCOL)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_SRTIPCOL);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_SRTIPCOL);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_SRRISCOL)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_SRTIPCOL);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SRRISCOL) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oSRRISCOL_2_4'),i_cWhere,'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SRTIPCOL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SRRISCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_SRRISCOL);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_SRTIPCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_SRTIPCOL;
                       ,'DPCODICE',this.w_SRRISCOL)
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SRRISCOL = NVL(_Link_.DPCODICE,space(5))
      this.w_SRDESCRI = NVL(_Link_.DPDESCRI,space(40))
      this.w_SRCOGNOM = NVL(_Link_.DPCOGNOM,space(40))
      this.w_SRNOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SRRISCOL = space(5)
      endif
      this.w_SRDESCRI = space(40)
      this.w_SRCOGNOM = space(40)
      this.w_SRNOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SRRISCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.RadioValue()==this.w_SRTIPCOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.SetRadio()
      replace t_SRTIPCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRRISCOL_2_4.value==this.w_SRRISCOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRRISCOL_2_4.value=this.w_SRRISCOL
      replace t_SRRISCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRRISCOL_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRDESRIS_2_8.value==this.w_SRDESRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRDESRIS_2_8.value=this.w_SRDESRIS
      replace t_SRDESRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRDESRIS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.RadioValue()==this.w_SRFLRESP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.SetRadio()
      replace t_SRFLRESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'STRU_RIS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSAR_MPH.CheckForm()
      if not(Empty(.w_SRTIPCOL)) and not(Empty(.w_SRRISCOL))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SRTIPCOL = this.w_SRTIPCOL
    this.o_SRRISCOL = this.w_SRRISCOL
    * --- GSAR_MPH : Depends On
    this.GSAR_MPH.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_SRTIPCOL)) and not(Empty(t_SRRISCOL)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_SRTIPCOL=space(1)
      .w_SRRISCOL=space(5)
      .w_SRDESCRI=space(40)
      .w_SRCOGNOM=space(40)
      .w_SRNOME=space(40)
      .w_SRDESRIS=space(40)
      .w_SRFLRESP=space(1)
      .DoRTCalc(1,2,.f.)
        .w_SRTIPCOL = 'P'
      .DoRTCalc(4,5,.f.)
      if not(empty(.w_SRRISCOL))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,9,.f.)
        .w_SRDESRIS = IIF(.w_SRTIPCOL='P', ALLTRIM(.w_SRCOGNOM)+" "+ALLTRIM(.w_SRNOME), .w_SRDESCRI)
        .w_SRFLRESP = ICASE(EMPTY(.w_SRTIPCOL), 'N', .w_SRTIPCOL<>'P', 'N', EMPTY(.w_SRFLRESP), 'N', .w_SRFLRESP)
    endwith
    this.DoRTCalc(12,13,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SRTIPCOL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.RadioValue(.t.)
    this.w_SRRISCOL = t_SRRISCOL
    this.w_SRDESCRI = t_SRDESCRI
    this.w_SRCOGNOM = t_SRCOGNOM
    this.w_SRNOME = t_SRNOME
    this.w_SRDESRIS = t_SRDESRIS
    this.w_SRFLRESP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SRTIPCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRTIPCOL_2_2.ToRadio()
    replace t_SRRISCOL with this.w_SRRISCOL
    replace t_SRDESCRI with this.w_SRDESCRI
    replace t_SRCOGNOM with this.w_SRCOGNOM
    replace t_SRNOME with this.w_SRNOME
    replace t_SRDESRIS with this.w_SRDESRIS
    replace t_SRFLRESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSRFLRESP_2_9.ToRadio()
    if i_srv='A'
      replace SRRISCOL with this.w_SRRISCOL
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_msrPag1 as StdContainer
  Width  = 573
  height = 258
  stdWidth  = 573
  stdheight = 258
  resizeXpos=279
  resizeYpos=170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="WKAAPUZSLU",left=4, top=3, width=562,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="SRTIPCOL",Label2="Tipologia",Field3="SRRISCOL",Label3="Pers./Ris./Gruppi",Field4="SRDESRIS",Label4="Descrizione",Field5="SRFLRESP",Label5="Resp.",Field6="",Label6="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219001466

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=23,;
    width=558+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=24,width=557+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='DIPENDEN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='DIPENDEN'
        oDropInto=this.oBodyCol.oRow.oSRRISCOL_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_msrBodyRow as CPBodyRowCnt
  Width=548
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="JVUDHENDIT",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251329898,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0

  add object oSRTIPCOL_2_2 as StdTrsCombo with uid="UTRDUISWXK",rtrep=.t.,;
    cFormVar="w_SRTIPCOL", RowSource=""+"Persona,"+"Risorsa,"+"Gruppo" , ;
    HelpContextID = 226156430,;
    Height=21, Width=98, Left=49, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSRTIPCOL_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SRTIPCOL,&i_cF..t_SRTIPCOL),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'R',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oSRTIPCOL_2_2.GetRadio()
    this.Parent.oContained.w_SRTIPCOL = this.RadioValue()
    return .t.
  endfunc

  func oSRTIPCOL_2_2.ToRadio()
    this.Parent.oContained.w_SRTIPCOL=trim(this.Parent.oContained.w_SRTIPCOL)
    return(;
      iif(this.Parent.oContained.w_SRTIPCOL=='P',1,;
      iif(this.Parent.oContained.w_SRTIPCOL=='R',2,;
      iif(this.Parent.oContained.w_SRTIPCOL=='G',3,;
      0))))
  endfunc

  func oSRTIPCOL_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSRTIPCOL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SRRISCOL)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSRRISCOL_2_4 as StdTrsField with uid="FPLVHOXNQG",rtseq=5,rtrep=.t.,;
    cFormVar="w_SRRISCOL",value=space(5),nZero=5,isprimarykey=.t.,;
    HelpContextID = 223018894,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=101, Left=148, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_SRTIPCOL", oKey_2_1="DPCODICE", oKey_2_2="this.w_SRRISCOL"

  func oSRRISCOL_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSRRISCOL_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSRRISCOL_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSRRISCOL_2_4.readonly and this.parent.oSRRISCOL_2_4.isprimarykey)
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_SRTIPCOL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_SRTIPCOL)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oSRRISCOL_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
   endif
  endproc
  proc oSRRISCOL_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_SRTIPCOL
     i_obj.w_DPCODICE=this.parent.oContained.w_SRRISCOL
    i_obj.ecpSave()
  endproc

  add object oSRDESRIS_2_8 as StdTrsField with uid="PWOGGGSHZE",rtseq=10,rtrep=.t.,;
    cFormVar="w_SRDESRIS",value=space(40),enabled=.f.,;
    HelpContextID = 240115591,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=239, Left=251, Top=0, InputMask=replicate('X',40)

  add object oSRFLRESP_2_9 as StdTrsCheck with uid="SBKOQUKHCL",rtrep=.t.,;
    cFormVar="w_SRFLRESP",  caption="",;
    HelpContextID = 78069878,;
    Left=493, Top=0, Width=29,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oSRFLRESP_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SRFLRESP,&i_cF..t_SRFLRESP),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oSRFLRESP_2_9.GetRadio()
    this.Parent.oContained.w_SRFLRESP = this.RadioValue()
    return .t.
  endfunc

  func oSRFLRESP_2_9.ToRadio()
    this.Parent.oContained.w_SRFLRESP=trim(this.Parent.oContained.w_SRFLRESP)
    return(;
      iif(this.Parent.oContained.w_SRFLRESP=='S',1,;
      0))
  endfunc

  func oSRFLRESP_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSRFLRESP_2_9.mCond()
    with this.Parent.oContained
      return (.w_SRTIPCOL='P')
    endwith
  endfunc

  add object oLinkPC_2_12 as StdButton with uid="HSQZMUZBRA",width=19,height=19,;
   left=524, top=0,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per accedere alla gestione path import eventi";
    , HelpContextID = 96888362;
  , bGlobalFont=.t.

    proc oLinkPC_2_12.Click()
      this.Parent.oContained.GSAR_MPH.LinkPCClick()
    endproc

  func oLinkPC_2_12.mCond()
    with this.Parent.oContained
      return (.w_SRTIPCOL='P')
    endwith
  endfunc

  func oLinkPC_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
   endif
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_msr','STRU_RIS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SRCODICE=STRU_RIS.SRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
