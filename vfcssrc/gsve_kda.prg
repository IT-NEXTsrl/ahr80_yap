* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kda                                                        *
*              Documenti dati di riga                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_454]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2018-02-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kda",oParentObject))

* --- Class definition
define class tgsve_kda as StdForm
  Top    = 24
  Left   = 23

  * --- Standard Properties
  Width  = 576
  Height = 379+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-13"
  HelpContextID=185978263
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=222

  * --- Constant Properties
  _IDX = 0
  NOMENCLA_IDX = 0
  CACOARTI_IDX = 0
  SALDIART_IDX = 0
  VOCIIVA_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  CLA_RIGD_IDX = 0
  CON_TRAM_IDX = 0
  UNIMIS_IDX = 0
  CONTI_IDX = 0
  NAZIONI_IDX = 0
  DOC_MAST_IDX = 0
  VOC_COST_IDX = 0
  TIP_COLL_IDX = 0
  TIPICONF_IDX = 0
  ART_ICOL_IDX = 0
  CES_PITI_IDX = 0
  CAU_CESP_IDX = 0
  UNIT_LOG_IDX = 0
  COD_AREO_IDX = 0
  CATMCONT_IDX = 0
  PRA_TIPI_IDX = 0
  PRA_MATE_IDX = 0
  cPrg = "gsve_kda"
  cComment = "Documenti dati di riga"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLEDIT = space(1)
  w_MVCLADOC = space(2)
  w_MVIMPEVA = 0
  w_MVRIFORD = space(10)
  w_MVPREZZO = 0
  w_MVFLSCOR = space(1)
  w_FLDTPR = space(1)
  w_CODNAZ = space(3)
  w_AZCODPOR = space(10)
  w_TIPCON = space(1)
  w_MV__NOTE = space(0)
  w_MVDATDOC = ctod('  /  /  ')
  o_MVDATDOC = ctod('  /  /  ')
  w_MVTIPRIG = space(1)
  w_MVCODVAL = space(3)
  w_MVQTAUM1 = 0
  w_AZICOD = space(5)
  w_FLGEFA = space(1)
  w_OCONTRA = space(15)
  w_MVCODCLA = space(3)
  w_MVDATREG = ctod('  /  /  ')
  w_DESCLA = space(30)
  w_MVFLRAGG = space(1)
  w_MVIMPACC = 0
  w_MVIMPNAZ = 0
  w_MVCATCON = space(5)
  w_DESCON = space(35)
  w_MVCODIVA = space(5)
  w_MVCONTRO = space(15)
  w_DESCO2 = space(40)
  w_DESIVA = space(35)
  w_PERIND = 0
  w_PERIVA = 0
  w_MVCONTRA = space(15)
  w_DESCTR = space(50)
  w_FLAGEN = space(1)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_CV = space(3)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_CATCOM = space(3)
  w_IVACON = space(1)
  w_QUACON = space(1)
  w_OIMPACC = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVINICOM = ctod('  /  /  ')
  w_MVFINCOM = ctod('  /  /  ')
  w_MVFLVEAC = space(1)
  w_MVTIPRIG = space(1)
  w_MVSERRIF = space(10)
  w_MVCODART = space(20)
  w_FLCCAU = space(1)
  w_OCAUMAG = space(5)
  w_MVCAUMAG = space(5)
  o_MVCAUMAG = space(5)
  w_OCAUMAG = space(5)
  w_DESCAU = space(35)
  w_DTOBSO = ctod('  /  /  ')
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLIMPE = space(1)
  w_FLAVA1 = space(1)
  w_MVCAUCOL = space(5)
  o_MVCAUCOL = space(5)
  w_MVF2CASC = space(1)
  w_MVF2RISE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLELGM = space(1)
  w_AGGSAL = space(10)
  w_AGGSAL1 = space(10)
  w_MVCODMAG = space(5)
  o_MVCODMAG = space(5)
  w_MVKEYSAL = space(20)
  w_READMAG = space(5)
  w_QTAPER = 0
  w_QTRPER = 0
  w_DESMAG = space(30)
  w_MVCODMAT = space(5)
  o_MVCODMAT = space(5)
  w_READMAT = space(5)
  w_Q2APER = 0
  w_TESCOMP = .F.
  w_Q2RPER = 0
  w_DESMAT = space(30)
  w_MVDATEVA = ctod('  /  /  ')
  w_MVFLEVAS = space(1)
  w_QTDISP = 0
  w_Q2DISP = 0
  w_BOLIVA = space(1)
  w_FLCOMM = space(1)
  w_MVEFFEVA = ctod('  /  /  ')
  w_MVQTAEVA = 0
  w_MVIMPEVA = 0
  w_MVFLTRAS = space(1)
  w_MVNOMENC = space(8)
  o_MVNOMENC = space(8)
  w_DESNOM = space(35)
  w_ONOMENC = space(8)
  w_UMSUPP = space(3)
  w_MVUMSUPP = space(3)
  w_MVMOLSUP = 0
  w_UM1 = space(3)
  w_MVPESNET = 0
  w_TOTMASNE = 0
  w_MVPROORD = space(2)
  w_MVNAZPRO = space(3)
  w_OAIRPOR = space(10)
  w_MVAIRPOR = space(10)
  o_MVAIRPOR = space(10)
  w_ONAZPRO = space(3)
  w_OPROORD = space(2)
  w_MVRIFESC = space(10)
  w_FLANAL = space(1)
  w_MVCODATT = space(15)
  w_MVCODCOS = space(5)
  w_FLGCOM = space(1)
  w_FLPACK = space(1)
  w_RESCHK = 0
  w_DESART = space(40)
  w_MVCODICE = space(20)
  w_OFLEVAS = space(1)
  w_UNMIS1 = space(3)
  w_MVUNIMIS = space(3)
  w_MVQTAEV1 = 0
  w_UM1 = space(3)
  w_UMR = space(3)
  w_MVQTAIMP = 0
  w_MVQTAIM1 = 0
  w_UM1 = space(3)
  w_UMR = space(3)
  w_MVFLPROV = space(1)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_F2CASC = space(1)
  w_F2RISE = space(1)
  w_F2ORDI = space(1)
  w_F2IMPE = space(1)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVROWRIF = 0
  w_SERIAL1 = space(10)
  w_ROWNUM1 = 0
  w_DESVOC = space(40)
  w_MVVOCCEN = space(15)
  w_MVFLNOAN = space(1)
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_VOCTIP = space(1)
  w_DESCA2 = space(35)
  w_FLESUL = space(1)
  w_MVCODLOT = space(20)
  w_FLCOVA = space(1)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MVCODCOL = space(5)
  w_DESCOL = space(35)
  w_MVNUMCOL = 0
  w_CODCONF = space(3)
  w_DESCONF = space(35)
  w_MVUNILOG = space(18)
  w_XCONORN = space(15)
  w_MVCODCEN = space(15)
  w_FLCESP = space(1)
  w_CATCES = space(15)
  w_DESCCESP = space(40)
  w_ASSCES = space(1)
  w_MVCESSER = space(10)
  o_MVCESSER = space(10)
  w_CESPRES = .F.
  w_MVCODCES = space(20)
  o_MVCODCES = space(20)
  w_MCNUMREG = 0
  w_MCCODESE = space(4)
  w_MVCODESE = space(4)
  w_CAUCES = space(5)
  o_CAUCES = space(5)
  w_MVDESART = space(40)
  w_MCDATREG = ctod('  /  /  ')
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_STABEN = space(1)
  w_DESCAUCES = space(40)
  w_MCSERIAL = space(10)
  w_TIPART = space(2)
  w_CETIPO = space(2)
  w_OLDFIN = space(1)
  w_DTOBSOCA = ctod('  /  /  ')
  w_MODDES = space(1)
  w_MVDESSUP = space(0)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_MVTIPPRO = space(2)
  o_MVTIPPRO = space(2)
  w_MVTIPPRO = space(2)
  w_MVTIPPRO = space(2)
  w_MVPERPRO = 0
  o_MVPERPRO = 0
  w_MVIMPPRO = 0
  o_MVIMPPRO = 0
  w_MVTIPPR2 = space(2)
  o_MVTIPPR2 = space(2)
  w_MVTIPPR2 = space(2)
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  o_MVPROCAP = 0
  w_MVIMPCAP = 0
  o_MVIMPCAP = 0
  w_F2UBIC = space(1)
  w_FLUBIC = space(1)
  w_VOCOBSO = ctod('  /  /  ')
  w_DESCRI = space(40)
  w_ISONAZ = space(3)
  w_MVRIFEDI = space(40)
  w_MVCACONT = space(5)
  w_CGDESCRI = space(40)
  w_ANDTOBSO = ctod('  /  /  ')
  w_ARUTISER = space(1)
  w_DECOSINT = 0
  w_DETIPPRA = space(10)
  w_DEMATPRA = space(10)
  w_DEGAZUFF = space(6)
  w_DEPREMIN = 0
  w_DEPREMAX = 0
  w_DECALDIR = space(1)
  w_DECOECAL = 0
  w_OKORD = .F.
  w_MV_FLAGG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kdaPag1","gsve_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsve_kdaPag2","gsve_kda",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Magazzino")
      .Pages(3).addobject("oPag","tgsve_kdaPag3","gsve_kda",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INTRA/P.list")
      .Pages(4).addobject("oPag","tgsve_kdaPag4","gsve_kda",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Provvigioni")
      .Pages(5).addobject("oPag","tgsve_kdaPag5","gsve_kda",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Note di riga")
      .Pages(6).addobject("oPag","tgsve_kdaPag6","gsve_kda",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Cespiti")
      .Pages(7).addobject("oPag","tgsve_kdaPag7","gsve_kda",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Altri dati")
      .Pages(7).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVCODCLA_1_19
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[23]
    this.cWorkTables[1]='NOMENCLA'
    this.cWorkTables[2]='CACOARTI'
    this.cWorkTables[3]='SALDIART'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='CAM_AGAZ'
    this.cWorkTables[7]='CLA_RIGD'
    this.cWorkTables[8]='CON_TRAM'
    this.cWorkTables[9]='UNIMIS'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='NAZIONI'
    this.cWorkTables[12]='DOC_MAST'
    this.cWorkTables[13]='VOC_COST'
    this.cWorkTables[14]='TIP_COLL'
    this.cWorkTables[15]='TIPICONF'
    this.cWorkTables[16]='ART_ICOL'
    this.cWorkTables[17]='CES_PITI'
    this.cWorkTables[18]='CAU_CESP'
    this.cWorkTables[19]='UNIT_LOG'
    this.cWorkTables[20]='COD_AREO'
    this.cWorkTables[21]='CATMCONT'
    this.cWorkTables[22]='PRA_TIPI'
    this.cWorkTables[23]='PRA_MATE'
    return(this.OpenAllTables(23))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsve_kda
    *--- Calcolo i Colli se non � vuoto MVCODCOL
    if g_MADV='S' And inlist(this.oParentObject.cfunction,"Edit","Load") and not Empty(this.oParentObject.w_MVCODCOL) and (this.oParentObject.w_FLPACK='S' or (this.oParentObject.w_FLPACK<>'S' and not empty(this.oParentObject.w_MVUNILOG)))
       this.oParentObject.NotifyEvent('RicalcolaConfezioni')
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLEDIT=space(1)
      .w_MVCLADOC=space(2)
      .w_MVIMPEVA=0
      .w_MVRIFORD=space(10)
      .w_MVPREZZO=0
      .w_MVFLSCOR=space(1)
      .w_FLDTPR=space(1)
      .w_CODNAZ=space(3)
      .w_AZCODPOR=space(10)
      .w_TIPCON=space(1)
      .w_MV__NOTE=space(0)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVTIPRIG=space(1)
      .w_MVCODVAL=space(3)
      .w_MVQTAUM1=0
      .w_AZICOD=space(5)
      .w_FLGEFA=space(1)
      .w_OCONTRA=space(15)
      .w_MVCODCLA=space(3)
      .w_MVDATREG=ctod("  /  /  ")
      .w_DESCLA=space(30)
      .w_MVFLRAGG=space(1)
      .w_MVIMPACC=0
      .w_MVIMPNAZ=0
      .w_MVCATCON=space(5)
      .w_DESCON=space(35)
      .w_MVCODIVA=space(5)
      .w_MVCONTRO=space(15)
      .w_DESCO2=space(40)
      .w_DESIVA=space(35)
      .w_PERIND=0
      .w_PERIVA=0
      .w_MVCONTRA=space(15)
      .w_DESCTR=space(50)
      .w_FLAGEN=space(1)
      .w_CT=space(1)
      .w_CC=space(15)
      .w_CM=space(3)
      .w_CI=ctod("  /  /  ")
      .w_CF=ctod("  /  /  ")
      .w_CV=space(3)
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(15)
      .w_CATCOM=space(3)
      .w_IVACON=space(1)
      .w_QUACON=space(1)
      .w_OIMPACC=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVINICOM=ctod("  /  /  ")
      .w_MVFINCOM=ctod("  /  /  ")
      .w_MVFLVEAC=space(1)
      .w_MVTIPRIG=space(1)
      .w_MVSERRIF=space(10)
      .w_MVCODART=space(20)
      .w_FLCCAU=space(1)
      .w_OCAUMAG=space(5)
      .w_MVCAUMAG=space(5)
      .w_OCAUMAG=space(5)
      .w_DESCAU=space(35)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MVFLCASC=space(1)
      .w_MVFLRISE=space(1)
      .w_MVFLORDI=space(1)
      .w_MVFLIMPE=space(1)
      .w_FLAVA1=space(1)
      .w_MVCAUCOL=space(5)
      .w_MVF2CASC=space(1)
      .w_MVF2RISE=space(1)
      .w_MVF2ORDI=space(1)
      .w_MVF2IMPE=space(1)
      .w_MVFLELGM=space(1)
      .w_AGGSAL=space(10)
      .w_AGGSAL1=space(10)
      .w_MVCODMAG=space(5)
      .w_MVKEYSAL=space(20)
      .w_READMAG=space(5)
      .w_QTAPER=0
      .w_QTRPER=0
      .w_DESMAG=space(30)
      .w_MVCODMAT=space(5)
      .w_READMAT=space(5)
      .w_Q2APER=0
      .w_TESCOMP=.f.
      .w_Q2RPER=0
      .w_DESMAT=space(30)
      .w_MVDATEVA=ctod("  /  /  ")
      .w_MVFLEVAS=space(1)
      .w_QTDISP=0
      .w_Q2DISP=0
      .w_BOLIVA=space(1)
      .w_FLCOMM=space(1)
      .w_MVEFFEVA=ctod("  /  /  ")
      .w_MVQTAEVA=0
      .w_MVIMPEVA=0
      .w_MVFLTRAS=space(1)
      .w_MVNOMENC=space(8)
      .w_DESNOM=space(35)
      .w_ONOMENC=space(8)
      .w_UMSUPP=space(3)
      .w_MVUMSUPP=space(3)
      .w_MVMOLSUP=0
      .w_UM1=space(3)
      .w_MVPESNET=0
      .w_TOTMASNE=0
      .w_MVPROORD=space(2)
      .w_MVNAZPRO=space(3)
      .w_OAIRPOR=space(10)
      .w_MVAIRPOR=space(10)
      .w_ONAZPRO=space(3)
      .w_OPROORD=space(2)
      .w_MVRIFESC=space(10)
      .w_FLANAL=space(1)
      .w_MVCODATT=space(15)
      .w_MVCODCOS=space(5)
      .w_FLGCOM=space(1)
      .w_FLPACK=space(1)
      .w_RESCHK=0
      .w_DESART=space(40)
      .w_MVCODICE=space(20)
      .w_OFLEVAS=space(1)
      .w_UNMIS1=space(3)
      .w_MVUNIMIS=space(3)
      .w_MVQTAEV1=0
      .w_UM1=space(3)
      .w_UMR=space(3)
      .w_MVQTAIMP=0
      .w_MVQTAIM1=0
      .w_UM1=space(3)
      .w_UMR=space(3)
      .w_MVFLPROV=space(1)
      .w_FLCASC=space(1)
      .w_FLRISE=space(1)
      .w_FLORDI=space(1)
      .w_FLIMPE=space(1)
      .w_F2CASC=space(1)
      .w_F2RISE=space(1)
      .w_F2ORDI=space(1)
      .w_F2IMPE=space(1)
      .w_MVSERIAL=space(10)
      .w_CPROWNUM=0
      .w_MVROWRIF=0
      .w_SERIAL1=space(10)
      .w_ROWNUM1=0
      .w_DESVOC=space(40)
      .w_MVVOCCEN=space(15)
      .w_MVFLNOAN=space(1)
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_VOCTIP=space(1)
      .w_DESCA2=space(35)
      .w_FLESUL=space(1)
      .w_MVCODLOT=space(20)
      .w_FLCOVA=space(1)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MVCODCOL=space(5)
      .w_DESCOL=space(35)
      .w_MVNUMCOL=0
      .w_CODCONF=space(3)
      .w_DESCONF=space(35)
      .w_MVUNILOG=space(18)
      .w_XCONORN=space(15)
      .w_MVCODCEN=space(15)
      .w_FLCESP=space(1)
      .w_CATCES=space(15)
      .w_DESCCESP=space(40)
      .w_ASSCES=space(1)
      .w_MVCESSER=space(10)
      .w_CESPRES=.f.
      .w_MVCODCES=space(20)
      .w_MCNUMREG=0
      .w_MCCODESE=space(4)
      .w_MVCODESE=space(4)
      .w_CAUCES=space(5)
      .w_MVDESART=space(40)
      .w_MCDATREG=ctod("  /  /  ")
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_STABEN=space(1)
      .w_DESCAUCES=space(40)
      .w_MCSERIAL=space(10)
      .w_TIPART=space(2)
      .w_CETIPO=space(2)
      .w_OLDFIN=space(1)
      .w_DTOBSOCA=ctod("  /  /  ")
      .w_MODDES=space(1)
      .w_MVDESSUP=space(0)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_MVTIPPRO=space(2)
      .w_MVTIPPRO=space(2)
      .w_MVTIPPRO=space(2)
      .w_MVPERPRO=0
      .w_MVIMPPRO=0
      .w_MVTIPPR2=space(2)
      .w_MVTIPPR2=space(2)
      .w_MVTIPPR2=space(2)
      .w_MVPROCAP=0
      .w_MVIMPCAP=0
      .w_F2UBIC=space(1)
      .w_FLUBIC=space(1)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_ISONAZ=space(3)
      .w_MVRIFEDI=space(40)
      .w_MVCACONT=space(5)
      .w_CGDESCRI=space(40)
      .w_ANDTOBSO=ctod("  /  /  ")
      .w_ARUTISER=space(1)
      .w_DECOSINT=0
      .w_DETIPPRA=space(10)
      .w_DEMATPRA=space(10)
      .w_DEGAZUFF=space(6)
      .w_DEPREMIN=0
      .w_DEPREMAX=0
      .w_DECALDIR=space(1)
      .w_DECOECAL=0
      .w_OKORD=.f.
      .w_MV_FLAGG=space(1)
      .w_FLEDIT=oParentObject.w_FLEDIT
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVIMPEVA=oParentObject.w_MVIMPEVA
      .w_MVRIFORD=oParentObject.w_MVRIFORD
      .w_MVPREZZO=oParentObject.w_MVPREZZO
      .w_MVFLSCOR=oParentObject.w_MVFLSCOR
      .w_FLDTPR=oParentObject.w_FLDTPR
      .w_CODNAZ=oParentObject.w_CODNAZ
      .w_AZCODPOR=oParentObject.w_AZCODPOR
      .w_MV__NOTE=oParentObject.w_MV__NOTE
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVTIPRIG=oParentObject.w_MVTIPRIG
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_MVQTAUM1=oParentObject.w_MVQTAUM1
      .w_AZICOD=oParentObject.w_AZICOD
      .w_FLGEFA=oParentObject.w_FLGEFA
      .w_MVCODCLA=oParentObject.w_MVCODCLA
      .w_MVDATREG=oParentObject.w_MVDATREG
      .w_MVFLRAGG=oParentObject.w_MVFLRAGG
      .w_MVIMPACC=oParentObject.w_MVIMPACC
      .w_MVIMPNAZ=oParentObject.w_MVIMPNAZ
      .w_MVCATCON=oParentObject.w_MVCATCON
      .w_MVCODIVA=oParentObject.w_MVCODIVA
      .w_MVCONTRO=oParentObject.w_MVCONTRO
      .w_PERIVA=oParentObject.w_PERIVA
      .w_MVCONTRA=oParentObject.w_MVCONTRA
      .w_FLAGEN=oParentObject.w_FLAGEN
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_CATCOM=oParentObject.w_CATCOM
      .w_MVINICOM=oParentObject.w_MVINICOM
      .w_MVFINCOM=oParentObject.w_MVFINCOM
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_MVTIPRIG=oParentObject.w_MVTIPRIG
      .w_MVSERRIF=oParentObject.w_MVSERRIF
      .w_MVCODART=oParentObject.w_MVCODART
      .w_FLCCAU=oParentObject.w_FLCCAU
      .w_MVCAUMAG=oParentObject.w_MVCAUMAG
      .w_MVFLCASC=oParentObject.w_MVFLCASC
      .w_MVFLRISE=oParentObject.w_MVFLRISE
      .w_MVFLORDI=oParentObject.w_MVFLORDI
      .w_MVFLIMPE=oParentObject.w_MVFLIMPE
      .w_FLAVA1=oParentObject.w_FLAVA1
      .w_MVCAUCOL=oParentObject.w_MVCAUCOL
      .w_MVF2CASC=oParentObject.w_MVF2CASC
      .w_MVF2RISE=oParentObject.w_MVF2RISE
      .w_MVF2ORDI=oParentObject.w_MVF2ORDI
      .w_MVF2IMPE=oParentObject.w_MVF2IMPE
      .w_MVFLELGM=oParentObject.w_MVFLELGM
      .w_MVCODMAG=oParentObject.w_MVCODMAG
      .w_MVKEYSAL=oParentObject.w_MVKEYSAL
      .w_QTAPER=oParentObject.w_QTAPER
      .w_QTRPER=oParentObject.w_QTRPER
      .w_MVCODMAT=oParentObject.w_MVCODMAT
      .w_Q2APER=oParentObject.w_Q2APER
      .w_TESCOMP=oParentObject.w_TESCOMP
      .w_Q2RPER=oParentObject.w_Q2RPER
      .w_MVDATEVA=oParentObject.w_MVDATEVA
      .w_MVFLEVAS=oParentObject.w_MVFLEVAS
      .w_BOLIVA=oParentObject.w_BOLIVA
      .w_FLCOMM=oParentObject.w_FLCOMM
      .w_MVEFFEVA=oParentObject.w_MVEFFEVA
      .w_MVQTAEVA=oParentObject.w_MVQTAEVA
      .w_MVIMPEVA=oParentObject.w_MVIMPEVA
      .w_MVFLTRAS=oParentObject.w_MVFLTRAS
      .w_MVNOMENC=oParentObject.w_MVNOMENC
      .w_MVUMSUPP=oParentObject.w_MVUMSUPP
      .w_MVMOLSUP=oParentObject.w_MVMOLSUP
      .w_MVPESNET=oParentObject.w_MVPESNET
      .w_MVPROORD=oParentObject.w_MVPROORD
      .w_MVNAZPRO=oParentObject.w_MVNAZPRO
      .w_OAIRPOR=oParentObject.w_OAIRPOR
      .w_MVAIRPOR=oParentObject.w_MVAIRPOR
      .w_ONAZPRO=oParentObject.w_ONAZPRO
      .w_OPROORD=oParentObject.w_OPROORD
      .w_MVRIFESC=oParentObject.w_MVRIFESC
      .w_FLANAL=oParentObject.w_FLANAL
      .w_MVCODATT=oParentObject.w_MVCODATT
      .w_MVCODCOS=oParentObject.w_MVCODCOS
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLPACK=oParentObject.w_FLPACK
      .w_MVCODICE=oParentObject.w_MVCODICE
      .w_UNMIS1=oParentObject.w_UNMIS1
      .w_MVUNIMIS=oParentObject.w_MVUNIMIS
      .w_MVQTAEV1=oParentObject.w_MVQTAEV1
      .w_MVQTAIMP=oParentObject.w_MVQTAIMP
      .w_MVQTAIM1=oParentObject.w_MVQTAIM1
      .w_MVFLPROV=oParentObject.w_MVFLPROV
      .w_FLCASC=oParentObject.w_FLCASC
      .w_FLRISE=oParentObject.w_FLRISE
      .w_FLORDI=oParentObject.w_FLORDI
      .w_FLIMPE=oParentObject.w_FLIMPE
      .w_F2CASC=oParentObject.w_F2CASC
      .w_F2RISE=oParentObject.w_F2RISE
      .w_F2ORDI=oParentObject.w_F2ORDI
      .w_F2IMPE=oParentObject.w_F2IMPE
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_CPROWNUM=oParentObject.w_CPROWNUM
      .w_MVROWRIF=oParentObject.w_MVROWRIF
      .w_MVVOCCEN=oParentObject.w_MVVOCCEN
      .w_MVFLNOAN=oParentObject.w_MVFLNOAN
      .w_CODCOS=oParentObject.w_CODCOS
      .w_VOCTIP=oParentObject.w_VOCTIP
      .w_MVCODLOT=oParentObject.w_MVCODLOT
      .w_UNMIS2=oParentObject.w_UNMIS2
      .w_UNMIS3=oParentObject.w_UNMIS3
      .w_MVCODCOL=oParentObject.w_MVCODCOL
      .w_MVNUMCOL=oParentObject.w_MVNUMCOL
      .w_CODCONF=oParentObject.w_CODCONF
      .w_MVUNILOG=oParentObject.w_MVUNILOG
      .w_XCONORN=oParentObject.w_XCONORN
      .w_MVCODCEN=oParentObject.w_MVCODCEN
      .w_ASSCES=oParentObject.w_ASSCES
      .w_MVCESSER=oParentObject.w_MVCESSER
      .w_MVCODCES=oParentObject.w_MVCODCES
      .w_MVCODESE=oParentObject.w_MVCODESE
      .w_CAUCES=oParentObject.w_CAUCES
      .w_MVDESART=oParentObject.w_MVDESART
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MODDES=oParentObject.w_MODDES
      .w_MVDESSUP=oParentObject.w_MVDESSUP
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVTIPPRO=oParentObject.w_MVTIPPRO
      .w_MVPERPRO=oParentObject.w_MVPERPRO
      .w_MVIMPPRO=oParentObject.w_MVIMPPRO
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVTIPPR2=oParentObject.w_MVTIPPR2
      .w_MVPROCAP=oParentObject.w_MVPROCAP
      .w_MVIMPCAP=oParentObject.w_MVIMPCAP
      .w_F2UBIC=oParentObject.w_F2UBIC
      .w_FLUBIC=oParentObject.w_FLUBIC
      .w_VOCOBSO=oParentObject.w_VOCOBSO
      .w_MVCACONT=oParentObject.w_MVCACONT
      .w_ARUTISER=oParentObject.w_ARUTISER
      .w_DECOSINT=oParentObject.w_DECOSINT
      .w_DETIPPRA=oParentObject.w_DETIPPRA
      .w_DEMATPRA=oParentObject.w_DEMATPRA
      .w_DEGAZUFF=oParentObject.w_DEGAZUFF
      .w_DEPREMIN=oParentObject.w_DEPREMIN
      .w_DEPREMAX=oParentObject.w_DEPREMAX
      .w_DECALDIR=oParentObject.w_DECALDIR
      .w_DECOECAL=oParentObject.w_DECOECAL
      .w_MV_FLAGG=oParentObject.w_MV_FLAGG
          .DoRTCalc(1,3,.f.)
        .w_MVRIFORD = .w_MVRIFORD
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MVRIFORD))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,9,.f.)
        .w_TIPCON = 'G'
          .DoRTCalc(11,17,.f.)
        .w_OCONTRA = .w_MVCONTRA
        .w_MVCODCLA = .w_MVCODCLA
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MVCODCLA))
          .link_1_19('Full')
        endif
          .DoRTCalc(20,24,.f.)
        .w_MVCATCON = .w_MVCATCON
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_MVCATCON))
          .link_1_25('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_MVCODIVA = .w_MVCODIVA
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_MVCODIVA))
          .link_1_27('Full')
        endif
        .w_MVCONTRO = .w_MVCONTRO
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_MVCONTRO))
          .link_1_29('Full')
        endif
          .DoRTCalc(29,32,.f.)
        .w_MVCONTRA = .w_MVCONTRA
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_MVCONTRA))
          .link_1_34('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
          .DoRTCalc(34,46,.f.)
        .w_OIMPACC = .w_MVIMPACC
        .w_OBTEST = .w_MVDATDOC
        .DoRTCalc(49,55,.f.)
        if not(empty(.w_MVCODART))
          .link_2_3('Full')
        endif
          .DoRTCalc(56,56,.f.)
        .w_OCAUMAG = .w_MVCAUMAG
        .w_MVCAUMAG = .w_MVCAUMAG
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_MVCAUMAG))
          .link_2_6('Full')
        endif
        .w_OCAUMAG = .w_MVCAUMAG
          .DoRTCalc(60,61,.f.)
        .w_MVFLCASC = IIF(.w_MVFLPROV='S',' ',.w_FLCASC)
        .w_MVFLRISE = IIF(.w_MVFLPROV='S',' ',.w_FLRISE)
        .w_MVFLORDI = IIF(.w_MVFLPROV='S',' ',.w_FLORDI)
        .w_MVFLIMPE = IIF(.w_MVFLPROV='S',' ',.w_FLIMPE)
        .DoRTCalc(66,67,.f.)
        if not(empty(.w_MVCAUCOL))
          .link_2_15('Full')
        endif
        .w_MVF2CASC = IIF(.w_MVFLPROV='S',' ',.w_F2CASC)
        .w_MVF2RISE = IIF(.w_MVFLPROV='S',' ',.w_F2RISE)
        .w_MVF2ORDI = IIF(.w_MVFLPROV='S',' ',.w_F2ORDI)
        .w_MVF2IMPE = IIF(.w_MVFLPROV='S',' ',.w_F2IMPE)
          .DoRTCalc(72,72,.f.)
        .w_AGGSAL = ALLTRIM(.w_MVFLCASC+.w_MVFLRISE+.w_MVFLORDI+.w_MVFLIMPE)
        .w_AGGSAL1 = ALLTRIM(.w_MVF2CASC+.w_MVF2RISE+.w_MVF2ORDI+.w_MVF2IMPE)
        .w_MVCODMAG = .w_MVCODMAG
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_MVCODMAG))
          .link_2_23('Full')
        endif
        .w_MVKEYSAL = IIF(.w_MVTIPRIG='R' AND (NOT EMPTY(.w_AGGSAL) OR NOT EMPTY(.w_AGGSAL1)) , .w_MVCODART, SPACE(20))
        .w_READMAG = .w_MVCODMAG
        .DoRTCalc(77,77,.f.)
        if not(empty(.w_READMAG))
          .link_2_25('Full')
        endif
        .DoRTCalc(78,81,.f.)
        if not(empty(.w_MVCODMAT))
          .link_2_29('Full')
        endif
        .w_READMAT = .w_MVCODMAT
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_READMAT))
          .link_2_30('Full')
        endif
          .DoRTCalc(83,88,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .w_Q2DISP = .w_Q2APER-.w_Q2RPER
          .DoRTCalc(91,96,.f.)
        .w_MVNOMENC = .w_MVNOMENC
        .DoRTCalc(97,97,.f.)
        if not(empty(.w_MVNOMENC))
          .link_3_2('Full')
        endif
          .DoRTCalc(98,98,.f.)
        .w_ONOMENC = .w_MVNOMENC
          .DoRTCalc(100,100,.f.)
        .w_MVUMSUPP = IIF(.w_ONOMENC=.w_MVNOMENC, .w_MVUMSUPP, .w_UMSUPP)
        .DoRTCalc(101,101,.f.)
        if not(empty(.w_MVUMSUPP))
          .link_3_6('Full')
        endif
          .DoRTCalc(102,102,.f.)
        .w_UM1 = .w_UNMIS1
          .DoRTCalc(104,104,.f.)
        .w_TOTMASNE = .w_MVPESNET*.w_MVQTAUM1
          .DoRTCalc(106,106,.f.)
        .w_MVNAZPRO = .w_MVNAZPRO
        .DoRTCalc(107,107,.f.)
        if not(empty(.w_MVNAZPRO))
          .link_3_12('Full')
        endif
        .w_OAIRPOR = IIF(EMPTY(.w_MVAIRPOR), .w_AZCODPOR, .w_MVAIRPOR)
        .DoRTCalc(109,109,.f.)
        if not(empty(.w_MVAIRPOR))
          .link_3_14('Full')
        endif
        .w_ONAZPRO = IIF(EMPTY(.w_MVNAZPRO), .w_CODNAZ, .w_MVNAZPRO)
        .w_OPROORD = IIF(EMPTY(.w_MVPROORD), g_PROAZI, .w_MVPROORD)
          .DoRTCalc(112,114,.f.)
        .w_MVCODCOS = IIF(EMPTY(.w_MVCODATT), SPACE(5), .w_CODCOS)
      .oPgFrm.Page3.oPag.oObj_3_30.Calculate()
          .DoRTCalc(116,120,.f.)
        .w_OFLEVAS = .w_MVFLEVAS
          .DoRTCalc(122,124,.f.)
        .w_UM1 = .w_UNMIS1
        .w_UMR = .w_MVUNIMIS
          .DoRTCalc(127,128,.f.)
        .w_UM1 = .w_UNMIS1
        .w_UMR = .w_MVUNIMIS
          .DoRTCalc(131,142,.f.)
        .w_SERIAL1 = .w_MVSERIAL
        .w_ROWNUM1 = .w_CPROWNUM
      .oPgFrm.Page2.oPag.oObj_2_86.Calculate()
          .DoRTCalc(145,145,.f.)
        .w_MVVOCCEN = .w_MVVOCCEN
        .DoRTCalc(146,146,.f.)
        if not(empty(.w_MVVOCCEN))
          .link_1_73('Full')
        endif
        .DoRTCalc(147,157,.f.)
        if not(empty(.w_MVCODCOL))
          .link_3_36('Full')
        endif
        .DoRTCalc(158,160,.f.)
        if not(empty(.w_CODCONF))
          .link_3_39('Full')
        endif
        .DoRTCalc(161,162,.f.)
        if not(empty(.w_MVUNILOG))
          .link_3_43('Full')
        endif
        .DoRTCalc(163,171,.f.)
        if not(empty(.w_MVCODCES))
          .link_6_7('Full')
        endif
      .oPgFrm.Page6.oPag.oObj_6_8.Calculate()
        .DoRTCalc(172,175,.f.)
        if not(empty(.w_CAUCES))
          .link_6_14('Full')
        endif
          .DoRTCalc(176,182,.f.)
        .w_MCSERIAL = .w_MVCESSER
      .oPgFrm.Page6.oPag.oObj_6_36.Calculate()
        .DoRTCalc(184,209,.f.)
        if not(empty(.w_MVCACONT))
          .link_1_85('Full')
        endif
          .DoRTCalc(210,220,.f.)
        .w_OKORD = .w_MVTIPRIG='F' AND .w_MVCLADOC$'FA-FE' AND .w_MVIMPEVA=0 AND g_ORDI='S'
    endwith
    this.DoRTCalc(222,222,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_79.enabled = this.oPgFrm.Page2.oPag.oBtn_2_79.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_83.enabled = this.oPgFrm.Page2.oPag.oBtn_2_83.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_9.enabled = this.oPgFrm.Page6.oPag.oBtn_6_9.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_10.enabled = this.oPgFrm.Page6.oPag.oBtn_6_10.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_18.enabled = this.oPgFrm.Page6.oPag.oBtn_6_18.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_20.enabled = this.oPgFrm.Page6.oPag.oBtn_6_20.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_25.enabled = this.oPgFrm.Page6.oPag.oBtn_6_25.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_kda
      * Se il modulo cespiti non � attivato o non sono attivati i flag per la gestione cespiti sui documenti la pagina 5 � disattivata
        this.oPgFrm.Pages[6].Enabled=Not(this.w_FLCESP<>'S' OR g_CESP<>'S' OR this.w_ASSCES='N')
    
    
    
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMVCODCLA_1_19.enabled = i_bVal
      .Page1.oPag.oMVFLRAGG_1_22.enabled = i_bVal
      .Page1.oPag.oMVCATCON_1_25.enabled = i_bVal
      .Page1.oPag.oMVCODIVA_1_27.enabled = i_bVal
      .Page1.oPag.oMVCONTRO_1_29.enabled = i_bVal
      .Page1.oPag.oMVCONTRA_1_34.enabled = i_bVal
      .Page1.oPag.oMVINICOM_1_59.enabled = i_bVal
      .Page1.oPag.oMVFINCOM_1_60.enabled = i_bVal
      .Page2.oPag.oMVCAUMAG_2_6.enabled = i_bVal
      .Page2.oPag.oMVCODMAG_2_23.enabled = i_bVal
      .Page2.oPag.oMVCODMAT_2_29.enabled = i_bVal
      .Page2.oPag.oMVDATEVA_2_35.enabled = i_bVal
      .Page2.oPag.oMVFLEVAS_2_36.enabled = i_bVal
      .Page3.oPag.oMVFLTRAS_3_1.enabled = i_bVal
      .Page3.oPag.oMVNOMENC_3_2.enabled = i_bVal
      .Page3.oPag.oMVMOLSUP_3_7.enabled = i_bVal
      .Page3.oPag.oMVPESNET_3_9.enabled = i_bVal
      .Page3.oPag.oMVPROORD_3_11.enabled = i_bVal
      .Page3.oPag.oMVNAZPRO_3_12.enabled = i_bVal
      .Page3.oPag.oMVAIRPOR_3_14.enabled = i_bVal
      .Page1.oPag.oMVVOCCEN_1_73.enabled = i_bVal
      .Page1.oPag.oMVFLNOAN_1_74.enabled = i_bVal
      .Page3.oPag.oMVCODCOL_3_36.enabled = i_bVal
      .Page3.oPag.oMVNUMCOL_3_38.enabled = i_bVal
      .Page3.oPag.oMVUNILOG_3_43.enabled = i_bVal
      .Page6.oPag.oMVCODCES_6_7.enabled = i_bVal
      .Page6.oPag.oCAUCES_6_14.enabled = i_bVal
      .Page5.oPag.oMVDESSUP_5_2.enabled = i_bVal
      .Page4.oPag.oMVTIPPRO_4_9.enabled = i_bVal
      .Page4.oPag.oMVTIPPRO_4_10.enabled = i_bVal
      .Page4.oPag.oMVTIPPRO_4_11.enabled = i_bVal
      .Page4.oPag.oMVPERPRO_4_12.enabled = i_bVal
      .Page4.oPag.oMVIMPPRO_4_13.enabled = i_bVal
      .Page4.oPag.oMVTIPPR2_4_14.enabled = i_bVal
      .Page4.oPag.oMVTIPPR2_4_15.enabled = i_bVal
      .Page4.oPag.oMVTIPPR2_4_16.enabled = i_bVal
      .Page4.oPag.oMVPROCAP_4_17.enabled = i_bVal
      .Page4.oPag.oMVIMPCAP_4_18.enabled = i_bVal
      .Page1.oPag.oMVCACONT_1_85.enabled = i_bVal
      .Page7.oPag.oDECOSINT_7_7.enabled = i_bVal
      .Page7.oPag.oDETIPPRA_7_8.enabled = i_bVal
      .Page7.oPag.oDEMATPRA_7_9.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = .Page1.oPag.oBtn_1_28.mCond()
      .Page1.oPag.oBtn_1_64.enabled = .Page1.oPag.oBtn_1_64.mCond()
      .Page2.oPag.oBtn_2_79.enabled = .Page2.oPag.oBtn_2_79.mCond()
      .Page2.oPag.oBtn_2_83.enabled = .Page2.oPag.oBtn_2_83.mCond()
      .Page3.oPag.oBtn_3_45.enabled = i_bVal
      .Page6.oPag.oBtn_6_9.enabled = .Page6.oPag.oBtn_6_9.mCond()
      .Page6.oPag.oBtn_6_10.enabled = .Page6.oPag.oBtn_6_10.mCond()
      .Page6.oPag.oBtn_6_18.enabled = .Page6.oPag.oBtn_6_18.mCond()
      .Page6.oPag.oBtn_6_20.enabled = .Page6.oPag.oBtn_6_20.mCond()
      .Page6.oPag.oBtn_6_25.enabled = .Page6.oPag.oBtn_6_25.mCond()
      .Page5.oPag.oBtn_5_9.enabled = .Page5.oPag.oBtn_5_9.mCond()
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page3.oPag.oObj_3_30.enabled = i_bVal
      .Page2.oPag.oObj_2_86.enabled = i_bVal
      .Page6.oPag.oObj_6_8.enabled = i_bVal
      .Page6.oPag.oObj_6_36.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FLEDIT=.w_FLEDIT
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVIMPEVA=.w_MVIMPEVA
      .oParentObject.w_MVRIFORD=.w_MVRIFORD
      .oParentObject.w_MVPREZZO=.w_MVPREZZO
      .oParentObject.w_MVFLSCOR=.w_MVFLSCOR
      .oParentObject.w_FLDTPR=.w_FLDTPR
      .oParentObject.w_CODNAZ=.w_CODNAZ
      .oParentObject.w_AZCODPOR=.w_AZCODPOR
      .oParentObject.w_MV__NOTE=.w_MV__NOTE
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVTIPRIG=.w_MVTIPRIG
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_MVQTAUM1=.w_MVQTAUM1
      .oParentObject.w_AZICOD=.w_AZICOD
      .oParentObject.w_FLGEFA=.w_FLGEFA
      .oParentObject.w_MVCODCLA=.w_MVCODCLA
      .oParentObject.w_MVDATREG=.w_MVDATREG
      .oParentObject.w_MVFLRAGG=.w_MVFLRAGG
      .oParentObject.w_MVIMPACC=.w_MVIMPACC
      .oParentObject.w_MVIMPNAZ=.w_MVIMPNAZ
      .oParentObject.w_MVCATCON=.w_MVCATCON
      .oParentObject.w_MVCODIVA=.w_MVCODIVA
      .oParentObject.w_MVCONTRO=.w_MVCONTRO
      .oParentObject.w_PERIVA=.w_PERIVA
      .oParentObject.w_MVCONTRA=.w_MVCONTRA
      .oParentObject.w_FLAGEN=.w_FLAGEN
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_CATCOM=.w_CATCOM
      .oParentObject.w_MVINICOM=.w_MVINICOM
      .oParentObject.w_MVFINCOM=.w_MVFINCOM
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_MVTIPRIG=.w_MVTIPRIG
      .oParentObject.w_MVSERRIF=.w_MVSERRIF
      .oParentObject.w_MVCODART=.w_MVCODART
      .oParentObject.w_FLCCAU=.w_FLCCAU
      .oParentObject.w_MVCAUMAG=.w_MVCAUMAG
      .oParentObject.w_MVFLCASC=.w_MVFLCASC
      .oParentObject.w_MVFLRISE=.w_MVFLRISE
      .oParentObject.w_MVFLORDI=.w_MVFLORDI
      .oParentObject.w_MVFLIMPE=.w_MVFLIMPE
      .oParentObject.w_FLAVA1=.w_FLAVA1
      .oParentObject.w_MVCAUCOL=.w_MVCAUCOL
      .oParentObject.w_MVF2CASC=.w_MVF2CASC
      .oParentObject.w_MVF2RISE=.w_MVF2RISE
      .oParentObject.w_MVF2ORDI=.w_MVF2ORDI
      .oParentObject.w_MVF2IMPE=.w_MVF2IMPE
      .oParentObject.w_MVFLELGM=.w_MVFLELGM
      .oParentObject.w_MVCODMAG=.w_MVCODMAG
      .oParentObject.w_MVKEYSAL=.w_MVKEYSAL
      .oParentObject.w_QTAPER=.w_QTAPER
      .oParentObject.w_QTRPER=.w_QTRPER
      .oParentObject.w_MVCODMAT=.w_MVCODMAT
      .oParentObject.w_Q2APER=.w_Q2APER
      .oParentObject.w_TESCOMP=.w_TESCOMP
      .oParentObject.w_Q2RPER=.w_Q2RPER
      .oParentObject.w_MVDATEVA=.w_MVDATEVA
      .oParentObject.w_MVFLEVAS=.w_MVFLEVAS
      .oParentObject.w_BOLIVA=.w_BOLIVA
      .oParentObject.w_FLCOMM=.w_FLCOMM
      .oParentObject.w_MVEFFEVA=.w_MVEFFEVA
      .oParentObject.w_MVQTAEVA=.w_MVQTAEVA
      .oParentObject.w_MVIMPEVA=.w_MVIMPEVA
      .oParentObject.w_MVFLTRAS=.w_MVFLTRAS
      .oParentObject.w_MVNOMENC=.w_MVNOMENC
      .oParentObject.w_MVUMSUPP=.w_MVUMSUPP
      .oParentObject.w_MVMOLSUP=.w_MVMOLSUP
      .oParentObject.w_MVPESNET=.w_MVPESNET
      .oParentObject.w_MVPROORD=.w_MVPROORD
      .oParentObject.w_MVNAZPRO=.w_MVNAZPRO
      .oParentObject.w_OAIRPOR=.w_OAIRPOR
      .oParentObject.w_MVAIRPOR=.w_MVAIRPOR
      .oParentObject.w_ONAZPRO=.w_ONAZPRO
      .oParentObject.w_OPROORD=.w_OPROORD
      .oParentObject.w_MVRIFESC=.w_MVRIFESC
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_MVCODATT=.w_MVCODATT
      .oParentObject.w_MVCODCOS=.w_MVCODCOS
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLPACK=.w_FLPACK
      .oParentObject.w_MVCODICE=.w_MVCODICE
      .oParentObject.w_UNMIS1=.w_UNMIS1
      .oParentObject.w_MVUNIMIS=.w_MVUNIMIS
      .oParentObject.w_MVQTAEV1=.w_MVQTAEV1
      .oParentObject.w_MVQTAIMP=.w_MVQTAIMP
      .oParentObject.w_MVQTAIM1=.w_MVQTAIM1
      .oParentObject.w_MVFLPROV=.w_MVFLPROV
      .oParentObject.w_FLCASC=.w_FLCASC
      .oParentObject.w_FLRISE=.w_FLRISE
      .oParentObject.w_FLORDI=.w_FLORDI
      .oParentObject.w_FLIMPE=.w_FLIMPE
      .oParentObject.w_F2CASC=.w_F2CASC
      .oParentObject.w_F2RISE=.w_F2RISE
      .oParentObject.w_F2ORDI=.w_F2ORDI
      .oParentObject.w_F2IMPE=.w_F2IMPE
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_CPROWNUM=.w_CPROWNUM
      .oParentObject.w_MVROWRIF=.w_MVROWRIF
      .oParentObject.w_MVVOCCEN=.w_MVVOCCEN
      .oParentObject.w_MVFLNOAN=.w_MVFLNOAN
      .oParentObject.w_CODCOS=.w_CODCOS
      .oParentObject.w_VOCTIP=.w_VOCTIP
      .oParentObject.w_MVCODLOT=.w_MVCODLOT
      .oParentObject.w_UNMIS2=.w_UNMIS2
      .oParentObject.w_UNMIS3=.w_UNMIS3
      .oParentObject.w_MVCODCOL=.w_MVCODCOL
      .oParentObject.w_MVNUMCOL=.w_MVNUMCOL
      .oParentObject.w_CODCONF=.w_CODCONF
      .oParentObject.w_MVUNILOG=.w_MVUNILOG
      .oParentObject.w_XCONORN=.w_XCONORN
      .oParentObject.w_MVCODCEN=.w_MVCODCEN
      .oParentObject.w_ASSCES=.w_ASSCES
      .oParentObject.w_MVCESSER=.w_MVCESSER
      .oParentObject.w_MVCODCES=.w_MVCODCES
      .oParentObject.w_MVCODESE=.w_MVCODESE
      .oParentObject.w_CAUCES=.w_CAUCES
      .oParentObject.w_MVDESART=.w_MVDESART
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MODDES=.w_MODDES
      .oParentObject.w_MVDESSUP=.w_MVDESSUP
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVTIPPRO=.w_MVTIPPRO
      .oParentObject.w_MVPERPRO=.w_MVPERPRO
      .oParentObject.w_MVIMPPRO=.w_MVIMPPRO
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVTIPPR2=.w_MVTIPPR2
      .oParentObject.w_MVPROCAP=.w_MVPROCAP
      .oParentObject.w_MVIMPCAP=.w_MVIMPCAP
      .oParentObject.w_F2UBIC=.w_F2UBIC
      .oParentObject.w_FLUBIC=.w_FLUBIC
      .oParentObject.w_VOCOBSO=.w_VOCOBSO
      .oParentObject.w_MVCACONT=.w_MVCACONT
      .oParentObject.w_ARUTISER=.w_ARUTISER
      .oParentObject.w_DECOSINT=.w_DECOSINT
      .oParentObject.w_DETIPPRA=.w_DETIPPRA
      .oParentObject.w_DEMATPRA=.w_DEMATPRA
      .oParentObject.w_DEGAZUFF=.w_DEGAZUFF
      .oParentObject.w_DEPREMIN=.w_DEPREMIN
      .oParentObject.w_DEPREMAX=.w_DEPREMAX
      .oParentObject.w_DECALDIR=.w_DECALDIR
      .oParentObject.w_DECOECAL=.w_DECOECAL
      .oParentObject.w_MV_FLAGG=.w_MV_FLAGG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,9,.t.)
            .w_TIPCON = 'G'
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .DoRTCalc(11,47,.t.)
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_OBTEST = .w_MVDATDOC
        endif
        .DoRTCalc(49,54,.t.)
          .link_2_3('Full')
        .DoRTCalc(56,61,.t.)
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLCASC = IIF(.w_MVFLPROV='S',' ',.w_FLCASC)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLRISE = IIF(.w_MVFLPROV='S',' ',.w_FLRISE)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLORDI = IIF(.w_MVFLPROV='S',' ',.w_FLORDI)
        endif
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVFLIMPE = IIF(.w_MVFLPROV='S',' ',.w_FLIMPE)
        endif
        .DoRTCalc(66,66,.t.)
        if .o_MVCAUMAG<>.w_MVCAUMAG
          .link_2_15('Full')
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2CASC = IIF(.w_MVFLPROV='S',' ',.w_F2CASC)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2RISE = IIF(.w_MVFLPROV='S',' ',.w_F2RISE)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2ORDI = IIF(.w_MVFLPROV='S',' ',.w_F2ORDI)
        endif
        if .o_MVCAUCOL<>.w_MVCAUCOL
            .w_MVF2IMPE = IIF(.w_MVFLPROV='S',' ',.w_F2IMPE)
        endif
        .DoRTCalc(72,72,.t.)
            .w_AGGSAL = ALLTRIM(.w_MVFLCASC+.w_MVFLRISE+.w_MVFLORDI+.w_MVFLIMPE)
            .w_AGGSAL1 = ALLTRIM(.w_MVF2CASC+.w_MVF2RISE+.w_MVF2ORDI+.w_MVF2IMPE)
        .DoRTCalc(75,75,.t.)
            .w_MVKEYSAL = IIF(.w_MVTIPRIG='R' AND (NOT EMPTY(.w_AGGSAL) OR NOT EMPTY(.w_AGGSAL1)) , .w_MVCODART, SPACE(20))
        if .o_MVCODMAG<>.w_MVCODMAG
            .w_READMAG = .w_MVCODMAG
          .link_2_25('Full')
        endif
        .DoRTCalc(78,80,.t.)
        if .o_MVCAUMAG<>.w_MVCAUMAG
            .w_MVCODMAT = IIF(EMPTY(.w_MVCAUCOL), SPACE(5), .w_MVCODMAT)
          .link_2_29('Full')
        endif
        if .o_MVCODMAT<>.w_MVCODMAT
            .w_READMAT = .w_MVCODMAT
          .link_2_30('Full')
        endif
        .DoRTCalc(83,100,.t.)
        if .o_MVNOMENC<>.w_MVNOMENC
            .w_MVUMSUPP = IIF(.w_ONOMENC=.w_MVNOMENC, .w_MVUMSUPP, .w_UMSUPP)
          .link_3_6('Full')
        endif
        .DoRTCalc(102,102,.t.)
            .w_UM1 = .w_UNMIS1
        .DoRTCalc(104,104,.t.)
            .w_TOTMASNE = .w_MVPESNET*.w_MVQTAUM1
        .DoRTCalc(106,107,.t.)
            .w_OAIRPOR = IIF(EMPTY(.w_MVAIRPOR), .w_AZCODPOR, .w_MVAIRPOR)
        if .o_MVAIRPOR<>.w_MVAIRPOR
            .w_MVAIRPOR = .w_OAIRPOR
          .link_3_14('Full')
        endif
            .w_ONAZPRO = IIF(EMPTY(.w_MVNAZPRO), .w_CODNAZ, .w_MVNAZPRO)
            .w_OPROORD = IIF(EMPTY(.w_MVPROORD), g_PROAZI, .w_MVPROORD)
        .DoRTCalc(112,114,.t.)
            .w_MVCODCOS = IIF(EMPTY(.w_MVCODATT), SPACE(5), .w_CODCOS)
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate()
        .DoRTCalc(116,124,.t.)
            .w_UM1 = .w_UNMIS1
            .w_UMR = .w_MVUNIMIS
        .DoRTCalc(127,128,.t.)
            .w_UM1 = .w_UNMIS1
            .w_UMR = .w_MVUNIMIS
        .DoRTCalc(131,142,.t.)
            .w_SERIAL1 = .w_MVSERIAL
            .w_ROWNUM1 = .w_CPROWNUM
        .oPgFrm.Page2.oPag.oObj_2_86.Calculate()
        .DoRTCalc(145,159,.t.)
          .link_3_39('Full')
        .oPgFrm.Page6.oPag.oObj_6_8.Calculate()
        .DoRTCalc(161,182,.t.)
        if .o_MVCESSER<>.w_MVCESSER
            .w_MCSERIAL = .w_MVCESSER
        endif
        .oPgFrm.Page6.oPag.oObj_6_36.Calculate()
        .DoRTCalc(184,195,.t.)
        if .o_MVTIPPRO<>.w_MVTIPPRO.or. .o_MVIMPPRO<>.w_MVIMPPRO
            .w_MVPERPRO = iif((.w_MVIMPPRO<>0 AND .w_MVTIPPRO='FO') OR .w_MVTIPPRO$'ES-ET',0,.w_MVPERPRO)
        endif
        if .o_MVTIPPRO<>.w_MVTIPPRO.or. .o_MVPERPRO<>.w_MVPERPRO
            .w_MVIMPPRO = iif((.w_MVPERPRO<>0 AND .w_MVTIPPRO='FO') OR .w_MVTIPPRO$'ES-ET',0,.w_MVIMPPRO)
        endif
        .DoRTCalc(198,200,.t.)
        if .o_MVTIPPR2<>.w_MVTIPPR2.or. .o_MVIMPCAP<>.w_MVIMPCAP
            .w_MVPROCAP = iif((.w_MVIMPCAP<>0 AND .w_MVTIPPR2='FO') OR .w_MVTIPPR2$'ES-ET',0,.w_MVPROCAP)
        endif
        if .o_MVTIPPR2<>.w_MVTIPPR2.or. .o_MVPROCAP<>.w_MVPROCAP
            .w_MVIMPCAP = iif((.w_MVPROCAP<>0 AND .w_MVTIPPR2='FO') OR .w_MVTIPPR2$'ES-ET',0,.w_MVIMPCAP)
        endif
        .DoRTCalc(203,220,.t.)
            .w_OKORD = .w_MVTIPRIG='F' AND .w_MVCLADOC$'FA-FE' AND .w_MVIMPEVA=0 AND g_ORDI='S'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(222,222,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_30.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_86.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_8.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_36.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVFLRAGG_1_22.enabled = this.oPgFrm.Page1.oPag.oMVFLRAGG_1_22.mCond()
    this.oPgFrm.Page1.oPag.oMVCATCON_1_25.enabled = this.oPgFrm.Page1.oPag.oMVCATCON_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMVCODIVA_1_27.enabled = this.oPgFrm.Page1.oPag.oMVCODIVA_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMVCONTRO_1_29.enabled = this.oPgFrm.Page1.oPag.oMVCONTRO_1_29.mCond()
    this.oPgFrm.Page1.oPag.oMVCONTRA_1_34.enabled = this.oPgFrm.Page1.oPag.oMVCONTRA_1_34.mCond()
    this.oPgFrm.Page2.oPag.oMVCAUMAG_2_6.enabled = this.oPgFrm.Page2.oPag.oMVCAUMAG_2_6.mCond()
    this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.enabled = this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.mCond()
    this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.enabled = this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.mCond()
    this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.enabled = this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.mCond()
    this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.enabled = this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.mCond()
    this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.enabled = this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.mCond()
    this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.enabled = this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.mCond()
    this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.enabled = this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMVPESNET_3_9.enabled = this.oPgFrm.Page3.oPag.oMVPESNET_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMVPROORD_3_11.enabled = this.oPgFrm.Page3.oPag.oMVPROORD_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.enabled = this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.mCond()
    this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.enabled = this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.mCond()
    this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.enabled = this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.mCond()
    this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.enabled = this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.mCond()
    this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.enabled = this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.mCond()
    this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.enabled = this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.mCond()
    this.oPgFrm.Page6.oPag.oMVCODCES_6_7.enabled = this.oPgFrm.Page6.oPag.oMVCODCES_6_7.mCond()
    this.oPgFrm.Page6.oPag.oCAUCES_6_14.enabled = this.oPgFrm.Page6.oPag.oCAUCES_6_14.mCond()
    this.oPgFrm.Page5.oPag.oMVDESSUP_5_2.enabled = this.oPgFrm.Page5.oPag.oMVDESSUP_5_2.mCond()
    this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.enabled = this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.mCond()
    this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.enabled = this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.mCond()
    this.oPgFrm.Page4.oPag.oMVPERPRO_4_12.enabled = this.oPgFrm.Page4.oPag.oMVPERPRO_4_12.mCond()
    this.oPgFrm.Page4.oPag.oMVIMPPRO_4_13.enabled = this.oPgFrm.Page4.oPag.oMVIMPPRO_4_13.mCond()
    this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.enabled = this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.mCond()
    this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.enabled = this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.mCond()
    this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.enabled = this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.mCond()
    this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.enabled = this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.mCond()
    this.oPgFrm.Page1.oPag.oMVCACONT_1_85.enabled = this.oPgFrm.Page1.oPag.oMVCACONT_1_85.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_45.enabled = this.oPgFrm.Page3.oPag.oBtn_3_45.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_9.enabled = this.oPgFrm.Page6.oPag.oBtn_6_9.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_10.enabled = this.oPgFrm.Page6.oPag.oBtn_6_10.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_18.enabled = this.oPgFrm.Page6.oPag.oBtn_6_18.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_20.enabled = this.oPgFrm.Page6.oPag.oBtn_6_20.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_25.enabled = this.oPgFrm.Page6.oPag.oBtn_6_25.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Magazzino"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show3
    i_show3=not(IsAlt())
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Provvigioni"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    local i_show6
    i_show6=not(Not IsAlt())
    this.oPgFrm.Pages(7).enabled=i_show6
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Altri dati"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page2.oPag.oMVCODART_2_3.visible=!this.oPgFrm.Page2.oPag.oMVCODART_2_3.mHide()
    this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.visible=!this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.mHide()
    this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.visible=!this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESMAG_2_28.visible=!this.oPgFrm.Page2.oPag.oDESMAG_2_28.mHide()
    this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.visible=!this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.mHide()
    this.oPgFrm.Page2.oPag.oDESMAT_2_34.visible=!this.oPgFrm.Page2.oPag.oDESMAT_2_34.mHide()
    this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.visible=!this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.mHide()
    this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.visible=!this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oQTDISP_2_43.visible=!this.oPgFrm.Page2.oPag.oQTDISP_2_43.mHide()
    this.oPgFrm.Page2.oPag.oQ2DISP_2_44.visible=!this.oPgFrm.Page2.oPag.oQ2DISP_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oMVEFFEVA_2_49.visible=!this.oPgFrm.Page2.oPag.oMVEFFEVA_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAEVA_2_55.visible=!this.oPgFrm.Page2.oPag.oMVQTAEVA_2_55.mHide()
    this.oPgFrm.Page2.oPag.oMVIMPEVA_2_56.visible=!this.oPgFrm.Page2.oPag.oMVIMPEVA_2_56.mHide()
    this.oPgFrm.Page3.oPag.oMVPROORD_3_11.visible=!this.oPgFrm.Page3.oPag.oMVPROORD_3_11.mHide()
    this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.visible=!this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.mHide()
    this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.visible=!this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_25.visible=!this.oPgFrm.Page3.oPag.oStr_3_25.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_26.visible=!this.oPgFrm.Page3.oPag.oStr_3_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_64.visible=!this.oPgFrm.Page1.oPag.oBtn_1_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oDESART_2_60.visible=!this.oPgFrm.Page2.oPag.oDESART_2_60.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAEV1_2_63.visible=!this.oPgFrm.Page2.oPag.oMVQTAEV1_2_63.mHide()
    this.oPgFrm.Page2.oPag.oUM1_2_64.visible=!this.oPgFrm.Page2.oPag.oUM1_2_64.mHide()
    this.oPgFrm.Page2.oPag.oUMR_2_65.visible=!this.oPgFrm.Page2.oPag.oUMR_2_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_66.visible=!this.oPgFrm.Page2.oPag.oStr_2_66.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAIMP_2_67.visible=!this.oPgFrm.Page2.oPag.oMVQTAIMP_2_67.mHide()
    this.oPgFrm.Page2.oPag.oMVQTAIM1_2_68.visible=!this.oPgFrm.Page2.oPag.oMVQTAIM1_2_68.mHide()
    this.oPgFrm.Page2.oPag.oUM1_2_69.visible=!this.oPgFrm.Page2.oPag.oUM1_2_69.mHide()
    this.oPgFrm.Page2.oPag.oUMR_2_70.visible=!this.oPgFrm.Page2.oPag.oUMR_2_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oDESVOC_1_72.visible=!this.oPgFrm.Page1.oPag.oDESVOC_1_72.mHide()
    this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.visible=!this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.mHide()
    this.oPgFrm.Page1.oPag.oMVFLNOAN_1_74.visible=!this.oPgFrm.Page1.oPag.oMVFLNOAN_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page2.oPag.oDESCA2_2_87.visible=!this.oPgFrm.Page2.oPag.oDESCA2_2_87.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_88.visible=!this.oPgFrm.Page2.oPag.oStr_2_88.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_35.visible=!this.oPgFrm.Page3.oPag.oStr_3_35.mHide()
    this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.visible=!this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.mHide()
    this.oPgFrm.Page3.oPag.oDESCOL_3_37.visible=!this.oPgFrm.Page3.oPag.oDESCOL_3_37.mHide()
    this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.visible=!this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.mHide()
    this.oPgFrm.Page3.oPag.oCODCONF_3_39.visible=!this.oPgFrm.Page3.oPag.oCODCONF_3_39.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_40.visible=!this.oPgFrm.Page3.oPag.oStr_3_40.mHide()
    this.oPgFrm.Page3.oPag.oDESCONF_3_41.visible=!this.oPgFrm.Page3.oPag.oDESCONF_3_41.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_42.visible=!this.oPgFrm.Page3.oPag.oStr_3_42.mHide()
    this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.visible=!this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_44.visible=!this.oPgFrm.Page3.oPag.oStr_3_44.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_45.visible=!this.oPgFrm.Page3.oPag.oBtn_3_45.mHide()
    this.oPgFrm.Page6.oPag.oDESCCESP_6_3.visible=!this.oPgFrm.Page6.oPag.oDESCCESP_6_3.mHide()
    this.oPgFrm.Page6.oPag.oMVCODCES_6_7.visible=!this.oPgFrm.Page6.oPag.oMVCODCES_6_7.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_9.visible=!this.oPgFrm.Page6.oPag.oBtn_6_9.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_10.visible=!this.oPgFrm.Page6.oPag.oBtn_6_10.mHide()
    this.oPgFrm.Page6.oPag.oMCNUMREG_6_11.visible=!this.oPgFrm.Page6.oPag.oMCNUMREG_6_11.mHide()
    this.oPgFrm.Page6.oPag.oMCCODESE_6_12.visible=!this.oPgFrm.Page6.oPag.oMCCODESE_6_12.mHide()
    this.oPgFrm.Page6.oPag.oMCDATREG_6_16.visible=!this.oPgFrm.Page6.oPag.oMCDATREG_6_16.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_17.visible=!this.oPgFrm.Page6.oPag.oStr_6_17.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_18.visible=!this.oPgFrm.Page6.oPag.oBtn_6_18.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_19.visible=!this.oPgFrm.Page6.oPag.oStr_6_19.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_20.visible=!this.oPgFrm.Page6.oPag.oBtn_6_20.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_21.visible=!this.oPgFrm.Page6.oPag.oStr_6_21.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_25.visible=!this.oPgFrm.Page6.oPag.oBtn_6_25.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_29.visible=!this.oPgFrm.Page6.oPag.oStr_6_29.mHide()
    this.oPgFrm.Page5.oPag.oNUMDOC_5_3.visible=!this.oPgFrm.Page5.oPag.oNUMDOC_5_3.mHide()
    this.oPgFrm.Page5.oPag.oALFDOC_5_4.visible=!this.oPgFrm.Page5.oPag.oALFDOC_5_4.mHide()
    this.oPgFrm.Page5.oPag.oDATDOC_5_5.visible=!this.oPgFrm.Page5.oPag.oDATDOC_5_5.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_6.visible=!this.oPgFrm.Page5.oPag.oStr_5_6.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_7.visible=!this.oPgFrm.Page5.oPag.oStr_5_7.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_8.visible=!this.oPgFrm.Page5.oPag.oStr_5_8.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_9.visible=!this.oPgFrm.Page5.oPag.oBtn_5_9.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_10.visible=!this.oPgFrm.Page5.oPag.oStr_5_10.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_1.visible=!this.oPgFrm.Page4.oPag.oStr_4_1.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_2.visible=!this.oPgFrm.Page4.oPag.oStr_4_2.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_3.visible=!this.oPgFrm.Page4.oPag.oStr_4_3.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPRO_4_9.visible=!this.oPgFrm.Page4.oPag.oMVTIPPRO_4_9.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.visible=!this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.visible=!this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPR2_4_14.visible=!this.oPgFrm.Page4.oPag.oMVTIPPR2_4_14.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.visible=!this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.mHide()
    this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.visible=!this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.mHide()
    this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.visible=!this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.mHide()
    this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.visible=!this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_46.visible=!this.oPgFrm.Page3.oPag.oStr_3_46.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_47.visible=!this.oPgFrm.Page3.oPag.oStr_3_47.mHide()
    this.oPgFrm.Page3.oPag.oDESCRI_3_48.visible=!this.oPgFrm.Page3.oPag.oDESCRI_3_48.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_49.visible=!this.oPgFrm.Page3.oPag.oStr_3_49.mHide()
    this.oPgFrm.Page3.oPag.oISONAZ_3_50.visible=!this.oPgFrm.Page3.oPag.oISONAZ_3_50.mHide()
    this.oPgFrm.Page2.oPag.oMVRIFEDI_2_91.visible=!this.oPgFrm.Page2.oPag.oMVRIFEDI_2_91.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_92.visible=!this.oPgFrm.Page2.oPag.oStr_2_92.mHide()
    this.oPgFrm.Page1.oPag.oMVCACONT_1_85.visible=!this.oPgFrm.Page1.oPag.oMVCACONT_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oCGDESCRI_1_87.visible=!this.oPgFrm.Page1.oPag.oCGDESCRI_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_52.visible=!this.oPgFrm.Page3.oPag.oStr_3_52.mHide()
    this.oPgFrm.Page7.oPag.oDECOECAL_7_14.visible=!this.oPgFrm.Page7.oPag.oDECOECAL_7_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_30.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_86.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_8.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsve_kda
    *se i cespiti nn sono attivati nn lancia il batch gsce_bcz
    If cEvent='Init' And g_CESP='S'
     this.Notifyevent('Inizio')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVRIFORD
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVRIFORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVRIFORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_MVRIFORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MVRIFORD)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVRIFORD = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVRIFORD = space(10)
      endif
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVRIFORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODCLA
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_MVCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_MVCODCLA))
          select TRCODCLA,TRDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCLA)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oMVCODCLA_1_19'),i_cWhere,'GSAR_ATR',"Tipologie di documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_MVCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_MVCODCLA)
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCLA = NVL(_Link_.TRCODCLA,space(3))
      this.w_DESCLA = NVL(_Link_.TRDESCLA,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCLA = space(3)
      endif
      this.w_DESCLA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCATCON
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_MVCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_MVCATCON))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCATCON)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oMVCATCON_1_25'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_MVCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_MVCATCON)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCATCON = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MVCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODIVA
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_MVCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_MVCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_MVCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oMVCODIVA_1_27'),i_cWhere,'GSAR_AIV',"Codici IVA",'gscg_iva.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO,IVBOLIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_PERIND = NVL(_Link_.IVPERIND,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_BOLIVA = NVL(_Link_.IVBOLIVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
      this.w_PERIND = 0
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_BOLIVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_MVCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIVA = 0
        this.w_PERIND = 0
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_BOLIVA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONTRO
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_MVCONTRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MVCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMVCONTRO_1_29'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita non congruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MVCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_MVCONTRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCO2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONTRO = space(15)
      endif
      this.w_DESCO2 = space(40)
      this.w_ANDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_ANDTOBSO) OR .w_OBTEST<.w_ANDTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto contropartita non congruente")
        endif
        this.w_MVCONTRO = space(15)
        this.w_DESCO2 = space(40)
        this.w_ANDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCONTRA
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_MVCONTRA)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_MVTIPCON;
                     ,'CONUMERO',trim(this.w_MVCONTRA))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCONTRA)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oMVCONTRA_1_34'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_MVCONTRA);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_MVTIPCON;
                       ,'CONUMERO',this.w_MVCONTRA)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCONTRA = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(50))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_QUACON = NVL(_Link_.COQUANTI,space(1))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCONTRA = space(15)
      endif
      this.w_DESCTR = space(50)
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_QUACON = space(1)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVCONTRA = space(15)
        this.w_DESCTR = space(50)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_QUACON = space(1)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCATCES,ARTIPART,ARFLCESP,ARFLESUL";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARDESART,ARCATCES,ARTIPART,ARFLCESP,ARFLESUL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_CATCES = NVL(_Link_.ARCATCES,space(15))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_FLCESP = NVL(_Link_.ARFLCESP,space(1))
      this.w_FLESUL = NVL(_Link_.ARFLESUL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_CATCES = space(15)
      this.w_TIPART = space(2)
      this.w_FLCESP = space(1)
      this.w_FLESUL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUMAG
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MVCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MVCAUMAG))
          select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_MVCAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_MVCAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMVCAUMAG_2_6'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUMAG)
            select CMCODICE,CMDESCRI,CMCAUCOL,CMDTOBSO,CMFLCOMM,CMFLAVAL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_MVCAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
      this.w_FLAVA1 = NVL(_Link_.CMFLAVAL,space(1))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_FLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_FLRISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUMAG = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_MVCAUCOL = space(5)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_FLCOMM = space(1)
      this.w_FLAVA1 = space(1)
      this.w_FLCASC = space(1)
      this.w_FLORDI = space(1)
      this.w_FLIMPE = space(1)
      this.w_FLRISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCAUMA(.w_MVCAUMAG,.w_MVCAUCOL,.w_FLGEFA,.w_FLAVA1,.w_DTOBSO,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVCAUMAG = space(5)
        this.w_DESCAU = space(35)
        this.w_MVCAUCOL = space(5)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_FLCOMM = space(1)
        this.w_FLAVA1 = space(1)
        this.w_FLCASC = space(1)
        this.w_FLORDI = space(1)
        this.w_FLIMPE = space(1)
        this.w_FLRISE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCAUCOL
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVCAUCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVCAUCOL)
            select CMCODICE,CMDESCRI,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCA2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_F2CASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_F2ORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_F2IMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_F2RISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCOL = space(5)
      endif
      this.w_DESCA2 = space(35)
      this.w_F2CASC = space(1)
      this.w_F2ORDI = space(1)
      this.w_F2IMPE = space(1)
      this.w_F2RISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAG
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MVCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAG_2_23'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente o obsoleto")
        endif
        this.w_MVCODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLUBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READMAG
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_READMAG);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MVKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MVKEYSAL;
                       ,'SLCODMAG',this.w_READMAG)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READMAG = NVL(_Link_.SLCODMAG,space(5))
      this.w_QTAPER = NVL(_Link_.SLQTAPER,0)
      this.w_QTRPER = NVL(_Link_.SLQTRPER,0)
    else
      if i_cCtrl<>'Load'
        this.w_READMAG = space(5)
      endif
      this.w_QTAPER = 0
      this.w_QTRPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAT
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAT))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAT)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MVCODMAT)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAT_2_29'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAT)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAT = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_F2UBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAT = space(5)
      endif
      this.w_DESMAT = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_F2UBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente o obsoleto")
        endif
        this.w_MVCODMAT = space(5)
        this.w_DESMAT = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_F2UBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READMAT
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_READMAT);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MVKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MVKEYSAL;
                       ,'SLCODMAG',this.w_READMAT)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READMAT = NVL(_Link_.SLCODMAG,space(5))
      this.w_Q2APER = NVL(_Link_.SLQTAPER,0)
      this.w_Q2RPER = NVL(_Link_.SLQTRPER,0)
    else
      if i_cCtrl<>'Load'
        this.w_READMAT = space(5)
      endif
      this.w_Q2APER = 0
      this.w_Q2RPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVNOMENC
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVNOMENC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_MVNOMENC)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_MVNOMENC))
          select NMCODICE,NMDESCRI,NMUNISUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVNOMENC)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVNOMENC) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oMVNOMENC_3_2'),i_cWhere,'GSAR_ANM',"Nomenclature",'GSMA_AZN.NOMENCLA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVNOMENC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_MVNOMENC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_MVNOMENC)
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVNOMENC = NVL(_Link_.NMCODICE,space(8))
      this.w_DESNOM = NVL(_Link_.NMDESCRI,space(35))
      this.w_UMSUPP = NVL(_Link_.NMUNISUP,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVNOMENC = space(8)
      endif
      this.w_DESNOM = space(35)
      this.w_UMSUPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUTISER='N' OR LEN(ALLTRIM(.w_MVNOMENC))=6 OR LEN(ALLTRIM(.w_MVNOMENC))=5
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVNOMENC = space(8)
        this.w_DESNOM = space(35)
        this.w_UMSUPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVNOMENC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVUMSUPP
  func Link_3_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVUMSUPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVUMSUPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MVUMSUPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MVUMSUPP)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVUMSUPP = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVUMSUPP = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVUMSUPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVNAZPRO
  func Link_3_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVNAZPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_MVNAZPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_MVNAZPRO))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVNAZPRO)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVNAZPRO) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oMVNAZPRO_3_12'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVNAZPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_MVNAZPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_MVNAZPRO)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVNAZPRO = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONAZ = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVNAZPRO = space(3)
      endif
      this.w_ISONAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVNAZPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVAIRPOR
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_AREO_IDX,3]
    i_lTable = "COD_AREO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2], .t., this.COD_AREO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVAIRPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APP',True,'COD_AREO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PPCODICE like "+cp_ToStrODBC(trim(this.w_MVAIRPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPCODICE',trim(this.w_MVAIRPOR))
          select PPCODICE,PPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVAIRPOR)==trim(_Link_.PPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVAIRPOR) and !this.bDontReportError
            deferred_cp_zoom('COD_AREO','*','PPCODICE',cp_AbsName(oSource.parent,'oMVAIRPOR_3_14'),i_cWhere,'GSAR_APP',"Porti/areoporti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',oSource.xKey(1))
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVAIRPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_MVAIRPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_MVAIRPOR)
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVAIRPOR = NVL(_Link_.PPCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.PPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVAIRPOR = space(10)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_AREO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVAIRPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVVOCCEN
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MVVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MVVOCCEN))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMVVOCCEN_1_73'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MVVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MVVOCCEN)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC=.w_VOCTIP AND CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_MVVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODCOL
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_MVCODCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_MVCODCOL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCOL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCOL) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oMVCODCOL_3_36'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_MVCODCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_MVCODCOL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCOL = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCOL = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONF
  func Link_3_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI"+cp_TransInsFldName("TCDESCRI")+",TCFLCOVA";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCONF);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_CODCONF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCONF;
                       ,'TCCODICE',this.w_CODCONF)
            select TCCODICE,TCDESCRI,TCFLCOVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONF = NVL(_Link_.TCCODICE,space(3))
      this.w_DESCONF = NVL(cp_TransLoadField('_Link_.TCDESCRI'),space(35))
      this.w_FLCOVA = NVL(_Link_.TCFLCOVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONF = space(3)
      endif
      this.w_DESCONF = space(35)
      this.w_FLCOVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVUNILOG
  func Link_3_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVUNILOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AUL',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_MVUNILOG)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_MVUNILOG))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVUNILOG)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVUNILOG) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oMVUNILOG_3_43'),i_cWhere,'GSMD_AUL',"Unit� logistiche",'GSMD_KDA.UNIT_LOG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVUNILOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_MVUNILOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_MVUNILOG)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVUNILOG = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_MVUNILOG = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=checksscc('L',.w_MVUNILOG,.t.,.t.,.w_MVCODART,.w_MVCODLOT, .w_MVCODCOL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MVUNILOG = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVUNILOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODCES
  func Link_6_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_MVCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_MVCODCES))
          select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_MVCODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_MVCODCES)+"%");

            select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oMVCODCES_6_7'),i_cWhere,'',"Cespiti",'GSVE_KCE.CES_PITI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_MVCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_MVCODCES)
            select CECODICE,CEDESCRI,CESTABEN,CETIPCES,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCCESP = NVL(_Link_.CEDESCRI,space(40))
      this.w_STABEN = NVL(_Link_.CESTABEN,space(1))
      this.w_CETIPO = NVL(_Link_.CETIPCES,space(2))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCES = space(20)
      endif
      this.w_DESCCESP = space(40)
      this.w_STABEN = space(1)
      this.w_CETIPO = space(2)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) And  IIF(EMPTY(.w_CETIPO),SPACE(2),IIF(.w_CETIPO='CQ','FM','FO'))=.w_TIPART OR EMPTY(.w_CETIPO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente obsoleto o di tipo non valido")
        endif
        this.w_MVCODCES = space(20)
        this.w_DESCCESP = space(40)
        this.w_STABEN = space(1)
        this.w_CETIPO = space(2)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCES
  func Link_6_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUCES))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CAUCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CAUCES)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUCES) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUCES_6_14'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCES)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCES = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAUCES = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCES = space(5)
      endif
      this.w_DESCAUCES = space(40)
      this.w_DTOBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_CAUCES = space(5)
        this.w_DESCAUCES = space(40)
        this.w_DTOBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCACONT
  func Link_1_85(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCACONT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_MVCACONT)+"%");

          i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGCODICE',trim(this.w_MVCACONT))
          select CGCODICE,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCACONT)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCACONT) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGCODICE',cp_AbsName(oSource.parent,'oMVCACONT_1_85'),i_cWhere,'GSAR_MCT',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCODICE',oSource.xKey(1))
            select CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCACONT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCODICE,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_MVCACONT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCODICE',this.w_MVCACONT)
            select CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCACONT = NVL(_Link_.CGCODICE,space(5))
      this.w_CGDESCRI = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MVCACONT = space(5)
      endif
      this.w_CGDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCACONT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVCODCLA_1_19.value==this.w_MVCODCLA)
      this.oPgFrm.Page1.oPag.oMVCODCLA_1_19.value=this.w_MVCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_21.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_21.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLRAGG_1_22.value==this.w_MVFLRAGG)
      this.oPgFrm.Page1.oPag.oMVFLRAGG_1_22.value=this.w_MVFLRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCATCON_1_25.value==this.w_MVCATCON)
      this.oPgFrm.Page1.oPag.oMVCATCON_1_25.value=this.w_MVCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_26.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_26.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODIVA_1_27.value==this.w_MVCODIVA)
      this.oPgFrm.Page1.oPag.oMVCODIVA_1_27.value=this.w_MVCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONTRO_1_29.value==this.w_MVCONTRO)
      this.oPgFrm.Page1.oPag.oMVCONTRO_1_29.value=this.w_MVCONTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCO2_1_30.value==this.w_DESCO2)
      this.oPgFrm.Page1.oPag.oDESCO2_1_30.value=this.w_DESCO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_31.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_31.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCONTRA_1_34.value==this.w_MVCONTRA)
      this.oPgFrm.Page1.oPag.oMVCONTRA_1_34.value=this.w_MVCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTR_1_35.value==this.w_DESCTR)
      this.oPgFrm.Page1.oPag.oDESCTR_1_35.value=this.w_DESCTR
    endif
    if not(this.oPgFrm.Page1.oPag.oMVINICOM_1_59.value==this.w_MVINICOM)
      this.oPgFrm.Page1.oPag.oMVINICOM_1_59.value=this.w_MVINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFINCOM_1_60.value==this.w_MVFINCOM)
      this.oPgFrm.Page1.oPag.oMVFINCOM_1_60.value=this.w_MVFINCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODART_2_3.value==this.w_MVCODART)
      this.oPgFrm.Page2.oPag.oMVCODART_2_3.value=this.w_MVCODART
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCAUMAG_2_6.value==this.w_MVCAUMAG)
      this.oPgFrm.Page2.oPag.oMVCAUMAG_2_6.value=this.w_MVCAUMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_8.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_8.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.value==this.w_MVCAUCOL)
      this.oPgFrm.Page2.oPag.oMVCAUCOL_2_15.value=this.w_MVCAUCOL
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLELGM_2_20.RadioValue()==this.w_MVFLELGM)
      this.oPgFrm.Page2.oPag.oMVFLELGM_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.value==this.w_MVCODMAG)
      this.oPgFrm.Page2.oPag.oMVCODMAG_2_23.value=this.w_MVCODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_28.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_28.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.value==this.w_MVCODMAT)
      this.oPgFrm.Page2.oPag.oMVCODMAT_2_29.value=this.w_MVCODMAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAT_2_34.value==this.w_DESMAT)
      this.oPgFrm.Page2.oPag.oDESMAT_2_34.value=this.w_DESMAT
    endif
    if not(this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.value==this.w_MVDATEVA)
      this.oPgFrm.Page2.oPag.oMVDATEVA_2_35.value=this.w_MVDATEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.RadioValue()==this.w_MVFLEVAS)
      this.oPgFrm.Page2.oPag.oMVFLEVAS_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oQTDISP_2_43.value==this.w_QTDISP)
      this.oPgFrm.Page2.oPag.oQTDISP_2_43.value=this.w_QTDISP
    endif
    if not(this.oPgFrm.Page2.oPag.oQ2DISP_2_44.value==this.w_Q2DISP)
      this.oPgFrm.Page2.oPag.oQ2DISP_2_44.value=this.w_Q2DISP
    endif
    if not(this.oPgFrm.Page2.oPag.oMVEFFEVA_2_49.value==this.w_MVEFFEVA)
      this.oPgFrm.Page2.oPag.oMVEFFEVA_2_49.value=this.w_MVEFFEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAEVA_2_55.value==this.w_MVQTAEVA)
      this.oPgFrm.Page2.oPag.oMVQTAEVA_2_55.value=this.w_MVQTAEVA
    endif
    if not(this.oPgFrm.Page2.oPag.oMVIMPEVA_2_56.value==this.w_MVIMPEVA)
      this.oPgFrm.Page2.oPag.oMVIMPEVA_2_56.value=this.w_MVIMPEVA
    endif
    if not(this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.RadioValue()==this.w_MVFLTRAS)
      this.oPgFrm.Page3.oPag.oMVFLTRAS_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.value==this.w_MVNOMENC)
      this.oPgFrm.Page3.oPag.oMVNOMENC_3_2.value=this.w_MVNOMENC
    endif
    if not(this.oPgFrm.Page3.oPag.oDESNOM_3_3.value==this.w_DESNOM)
      this.oPgFrm.Page3.oPag.oDESNOM_3_3.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page3.oPag.oUMSUPP_3_5.value==this.w_UMSUPP)
      this.oPgFrm.Page3.oPag.oUMSUPP_3_5.value=this.w_UMSUPP
    endif
    if not(this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.value==this.w_MVMOLSUP)
      this.oPgFrm.Page3.oPag.oMVMOLSUP_3_7.value=this.w_MVMOLSUP
    endif
    if not(this.oPgFrm.Page3.oPag.oUM1_3_8.value==this.w_UM1)
      this.oPgFrm.Page3.oPag.oUM1_3_8.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPESNET_3_9.value==this.w_MVPESNET)
      this.oPgFrm.Page3.oPag.oMVPESNET_3_9.value=this.w_MVPESNET
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTMASNE_3_10.value==this.w_TOTMASNE)
      this.oPgFrm.Page3.oPag.oTOTMASNE_3_10.value=this.w_TOTMASNE
    endif
    if not(this.oPgFrm.Page3.oPag.oMVPROORD_3_11.value==this.w_MVPROORD)
      this.oPgFrm.Page3.oPag.oMVPROORD_3_11.value=this.w_MVPROORD
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.value==this.w_MVNAZPRO)
      this.oPgFrm.Page3.oPag.oMVNAZPRO_3_12.value=this.w_MVNAZPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.value==this.w_MVAIRPOR)
      this.oPgFrm.Page3.oPag.oMVAIRPOR_3_14.value=this.w_MVAIRPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_60.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_60.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAEV1_2_63.value==this.w_MVQTAEV1)
      this.oPgFrm.Page2.oPag.oMVQTAEV1_2_63.value=this.w_MVQTAEV1
    endif
    if not(this.oPgFrm.Page2.oPag.oUM1_2_64.value==this.w_UM1)
      this.oPgFrm.Page2.oPag.oUM1_2_64.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUMR_2_65.value==this.w_UMR)
      this.oPgFrm.Page2.oPag.oUMR_2_65.value=this.w_UMR
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAIMP_2_67.value==this.w_MVQTAIMP)
      this.oPgFrm.Page2.oPag.oMVQTAIMP_2_67.value=this.w_MVQTAIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oMVQTAIM1_2_68.value==this.w_MVQTAIM1)
      this.oPgFrm.Page2.oPag.oMVQTAIM1_2_68.value=this.w_MVQTAIM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUM1_2_69.value==this.w_UM1)
      this.oPgFrm.Page2.oPag.oUM1_2_69.value=this.w_UM1
    endif
    if not(this.oPgFrm.Page2.oPag.oUMR_2_70.value==this.w_UMR)
      this.oPgFrm.Page2.oPag.oUMR_2_70.value=this.w_UMR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_72.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_72.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.value==this.w_MVVOCCEN)
      this.oPgFrm.Page1.oPag.oMVVOCCEN_1_73.value=this.w_MVVOCCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLNOAN_1_74.RadioValue()==this.w_MVFLNOAN)
      this.oPgFrm.Page1.oPag.oMVFLNOAN_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCA2_2_87.value==this.w_DESCA2)
      this.oPgFrm.Page2.oPag.oDESCA2_2_87.value=this.w_DESCA2
    endif
    if not(this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.value==this.w_MVCODCOL)
      this.oPgFrm.Page3.oPag.oMVCODCOL_3_36.value=this.w_MVCODCOL
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCOL_3_37.value==this.w_DESCOL)
      this.oPgFrm.Page3.oPag.oDESCOL_3_37.value=this.w_DESCOL
    endif
    if not(this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.value==this.w_MVNUMCOL)
      this.oPgFrm.Page3.oPag.oMVNUMCOL_3_38.value=this.w_MVNUMCOL
    endif
    if not(this.oPgFrm.Page3.oPag.oCODCONF_3_39.value==this.w_CODCONF)
      this.oPgFrm.Page3.oPag.oCODCONF_3_39.value=this.w_CODCONF
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCONF_3_41.value==this.w_DESCONF)
      this.oPgFrm.Page3.oPag.oDESCONF_3_41.value=this.w_DESCONF
    endif
    if not(this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.value==this.w_MVUNILOG)
      this.oPgFrm.Page3.oPag.oMVUNILOG_3_43.value=this.w_MVUNILOG
    endif
    if not(this.oPgFrm.Page6.oPag.oDESCCESP_6_3.value==this.w_DESCCESP)
      this.oPgFrm.Page6.oPag.oDESCCESP_6_3.value=this.w_DESCCESP
    endif
    if not(this.oPgFrm.Page6.oPag.oMVCODCES_6_7.value==this.w_MVCODCES)
      this.oPgFrm.Page6.oPag.oMVCODCES_6_7.value=this.w_MVCODCES
    endif
    if not(this.oPgFrm.Page6.oPag.oMCNUMREG_6_11.value==this.w_MCNUMREG)
      this.oPgFrm.Page6.oPag.oMCNUMREG_6_11.value=this.w_MCNUMREG
    endif
    if not(this.oPgFrm.Page6.oPag.oMCCODESE_6_12.value==this.w_MCCODESE)
      this.oPgFrm.Page6.oPag.oMCCODESE_6_12.value=this.w_MCCODESE
    endif
    if not(this.oPgFrm.Page6.oPag.oCAUCES_6_14.value==this.w_CAUCES)
      this.oPgFrm.Page6.oPag.oCAUCES_6_14.value=this.w_CAUCES
    endif
    if not(this.oPgFrm.Page6.oPag.oMCDATREG_6_16.value==this.w_MCDATREG)
      this.oPgFrm.Page6.oPag.oMCDATREG_6_16.value=this.w_MCDATREG
    endif
    if not(this.oPgFrm.Page6.oPag.oDESCAUCES_6_30.value==this.w_DESCAUCES)
      this.oPgFrm.Page6.oPag.oDESCAUCES_6_30.value=this.w_DESCAUCES
    endif
    if not(this.oPgFrm.Page5.oPag.oMVDESSUP_5_2.value==this.w_MVDESSUP)
      this.oPgFrm.Page5.oPag.oMVDESSUP_5_2.value=this.w_MVDESSUP
    endif
    if not(this.oPgFrm.Page5.oPag.oNUMDOC_5_3.value==this.w_NUMDOC)
      this.oPgFrm.Page5.oPag.oNUMDOC_5_3.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page5.oPag.oALFDOC_5_4.value==this.w_ALFDOC)
      this.oPgFrm.Page5.oPag.oALFDOC_5_4.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page5.oPag.oDATDOC_5_5.value==this.w_DATDOC)
      this.oPgFrm.Page5.oPag.oDATDOC_5_5.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPRO_4_9.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page4.oPag.oMVTIPPRO_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page4.oPag.oMVTIPPRO_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.RadioValue()==this.w_MVTIPPRO)
      this.oPgFrm.Page4.oPag.oMVTIPPRO_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVPERPRO_4_12.value==this.w_MVPERPRO)
      this.oPgFrm.Page4.oPag.oMVPERPRO_4_12.value=this.w_MVPERPRO
    endif
    if not(this.oPgFrm.Page4.oPag.oMVIMPPRO_4_13.value==this.w_MVIMPPRO)
      this.oPgFrm.Page4.oPag.oMVIMPPRO_4_13.value=this.w_MVIMPPRO
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPR2_4_14.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page4.oPag.oMVTIPPR2_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page4.oPag.oMVTIPPR2_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.RadioValue()==this.w_MVTIPPR2)
      this.oPgFrm.Page4.oPag.oMVTIPPR2_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.value==this.w_MVPROCAP)
      this.oPgFrm.Page4.oPag.oMVPROCAP_4_17.value=this.w_MVPROCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.value==this.w_MVIMPCAP)
      this.oPgFrm.Page4.oPag.oMVIMPCAP_4_18.value=this.w_MVIMPCAP
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI_3_48.value==this.w_DESCRI)
      this.oPgFrm.Page3.oPag.oDESCRI_3_48.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oISONAZ_3_50.value==this.w_ISONAZ)
      this.oPgFrm.Page3.oPag.oISONAZ_3_50.value=this.w_ISONAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oMVRIFEDI_2_91.value==this.w_MVRIFEDI)
      this.oPgFrm.Page2.oPag.oMVRIFEDI_2_91.value=this.w_MVRIFEDI
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCACONT_1_85.value==this.w_MVCACONT)
      this.oPgFrm.Page1.oPag.oMVCACONT_1_85.value=this.w_MVCACONT
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDESCRI_1_87.value==this.w_CGDESCRI)
      this.oPgFrm.Page1.oPag.oCGDESCRI_1_87.value=this.w_CGDESCRI
    endif
    if not(this.oPgFrm.Page7.oPag.oDECOSINT_7_7.value==this.w_DECOSINT)
      this.oPgFrm.Page7.oPag.oDECOSINT_7_7.value=this.w_DECOSINT
    endif
    if not(this.oPgFrm.Page7.oPag.oDETIPPRA_7_8.RadioValue()==this.w_DETIPPRA)
      this.oPgFrm.Page7.oPag.oDETIPPRA_7_8.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDEMATPRA_7_9.RadioValue()==this.w_DEMATPRA)
      this.oPgFrm.Page7.oPag.oDEMATPRA_7_9.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDEGAZUFF_7_10.value==this.w_DEGAZUFF)
      this.oPgFrm.Page7.oPag.oDEGAZUFF_7_10.value=this.w_DEGAZUFF
    endif
    if not(this.oPgFrm.Page7.oPag.oDEPREMIN_7_11.value==this.w_DEPREMIN)
      this.oPgFrm.Page7.oPag.oDEPREMIN_7_11.value=this.w_DEPREMIN
    endif
    if not(this.oPgFrm.Page7.oPag.oDEPREMAX_7_12.value==this.w_DEPREMAX)
      this.oPgFrm.Page7.oPag.oDEPREMAX_7_12.value=this.w_DEPREMAX
    endif
    if not(this.oPgFrm.Page7.oPag.oDECALDIR_7_13.RadioValue()==this.w_DECALDIR)
      this.oPgFrm.Page7.oPag.oDECALDIR_7_13.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDECOECAL_7_14.value==this.w_DECOECAL)
      this.oPgFrm.Page7.oPag.oDECOECAL_7_14.value=this.w_DECOECAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MVCODIVA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_MVTIPRIG $ 'RFMA')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODIVA_1_27.SetFocus()
            i_bnoObbl = !empty(.w_MVCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   not((EMPTY(.w_ANDTOBSO) OR .w_OBTEST<.w_ANDTOBSO))  and (.w_MVTIPRIG $ 'RFMA')  and not(empty(.w_MVCONTRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONTRO_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto contropartita non congruente")
          case   not(CHKCONTR(.w_MVCONTRA,.w_MVTIPCON,.w_XCONORN,.w_CATCOM,.w_MVFLSCOR,.w_MVCODVAL,.w_MVDATREG,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and (g_GESCON='S')  and not(empty(.w_MVCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCONTRA_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_MVINICOM) OR .w_MVINICOM<=.w_MVFINCOM)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVFINCOM_1_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVCAUMAG)) or not(CHKCAUMA(.w_MVCAUMAG,.w_MVCAUCOL,.w_FLGEFA,.w_FLAVA1,.w_DTOBSO,.w_OBTEST,'V', .w_MVFLVEAC, .w_MVCLADOC)))  and (.w_FLCCAU = 'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCAUMAG_2_6.SetFocus()
            i_bnoObbl = !empty(.w_MVCAUMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVCODMAG)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_MVTIPRIG <> 'R' OR  EMPTY(.w_AGGSAL))  and (.w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL) and Not .w_TESCOMP)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCODMAG_2_23.SetFocus()
            i_bnoObbl = !empty(.w_MVCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente o obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))  and (NOT EMPTY(.w_MVCAUCOL) AND .w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL1) and Not .w_TESCOMP)  and not(empty(.w_MVCODMAT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMVCODMAT_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente o obsoleto")
          case   not(.w_ARUTISER='N' OR LEN(ALLTRIM(.w_MVNOMENC))=6 OR LEN(ALLTRIM(.w_MVNOMENC))=5)  and (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND (.w_MVFLTRAS<>'S' or .w_ARUTISER='S'))  and not(empty(.w_MVNOMENC))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVNOMENC_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVVOCCEN)) or not(.w_TIPVOC=.w_VOCTIP AND CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.)))  and not(NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))  and (((g_PERCCR='S' AND .w_FLANAL='S' and .w_MVFLNOAN='N' ) OR (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVVOCCEN_1_73.SetFocus()
            i_bnoObbl = !empty(.w_MVVOCCEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(.w_MVNUMCOL<=30000)  and not(.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))  and (not empty(.w_MVCODCOL) and .w_FLCOVA='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVNUMCOL_3_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Superato massimo numero confezioni per riga. Valore massimo 30000.")
          case   not(checksscc('L',.w_MVUNILOG,.t.,.t.,.w_MVCODART,.w_MVCODLOT, .w_MVCODCOL))  and not((.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG)) or g_MADV<>'S' or .w_FLESUL='S')  and (not empty(.w_MVCODLOT) and not empty(.w_MVCODCOL) and .w_MVNUMCOL>0 and .w_FLPACK='S')  and not(empty(.w_MVUNILOG))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVUNILOG_3_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) And  IIF(EMPTY(.w_CETIPO),SPACE(2),IIF(.w_CETIPO='CQ','FM','FO'))=.w_TIPART OR EMPTY(.w_CETIPO))  and not(.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')  and ((.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)  and not(empty(.w_MVCODCES))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMVCODCES_6_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente obsoleto o di tipo non valido")
          case   not(.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA))  and (.w_ASSCES='M' and (.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES)  and not(empty(.w_CAUCES))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oCAUCES_6_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_kda
      *Controllo validit� codice Collo
      .w_RESCHK=0
      .NotifyEvent('CheckColl')
      if .w_RESCHK<>0
         i_bRes=.f.
      endif
      * -- Nel caso di Corrispettivi deve essere obbligatoria la Categoria Contabile
      If Empty(.w_MVCATCON) And .w_MVCLADOC = 'RF'
         i_bRes=.f.
         i_bnoChk = .f.
         i_cErrorMsg = Ah_MsgFormat("Inserire categoria contabile")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVDATDOC = this.w_MVDATDOC
    this.o_MVCAUMAG = this.w_MVCAUMAG
    this.o_MVCAUCOL = this.w_MVCAUCOL
    this.o_MVCODMAG = this.w_MVCODMAG
    this.o_MVCODMAT = this.w_MVCODMAT
    this.o_MVNOMENC = this.w_MVNOMENC
    this.o_MVAIRPOR = this.w_MVAIRPOR
    this.o_MVCESSER = this.w_MVCESSER
    this.o_MVCODCES = this.w_MVCODCES
    this.o_CAUCES = this.w_CAUCES
    this.o_MVTIPPRO = this.w_MVTIPPRO
    this.o_MVPERPRO = this.w_MVPERPRO
    this.o_MVIMPPRO = this.w_MVIMPPRO
    this.o_MVTIPPR2 = this.w_MVTIPPR2
    this.o_MVPROCAP = this.w_MVPROCAP
    this.o_MVIMPCAP = this.w_MVIMPCAP
    return

enddefine

* --- Define pages as container
define class tgsve_kdaPag1 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVCODCLA_1_19 as StdField with uid="CKAJMDYHVM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVCODCLA", cQueryName = "MVCODCLA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia riga documento per eventuali filtri",;
    HelpContextID = 223782649,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=115, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_MVCODCLA"

  func oMVCODCLA_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCLA_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCLA_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oMVCODCLA_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie di documenti",'',this.parent.oContained
  endproc
  proc oMVCODCLA_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_MVCODCLA
     i_obj.ecpSave()
  endproc

  add object oDESCLA_1_21 as StdField with uid="BLUTJCQZOE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 249673930,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=157, Top=14, InputMask=replicate('X',30)

  add object oMVFLRAGG_1_22 as StdField with uid="BDVIQLGPRS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVFLRAGG", cQueryName = "MVFLRAGG",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 25594125,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=523, Top=14, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  func oMVFLRAGG_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGEFA='S')
    endwith
   endif
  endfunc

  add object oMVCATCON_1_25 as StdField with uid="ONQROUUUVK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MVCATCON", cQueryName = "MVCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile per contabilizzazione associata alla riga documento",;
    HelpContextID = 207922924,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=115, Top=44, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_MVCATCON"

  func oMVCATCON_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCATCON_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCATCON_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCATCON_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oMVCATCON_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oMVCATCON_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_MVCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_26 as StdField with uid="FZPVHQLZCU",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28424394,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=178, Top=44, InputMask=replicate('X',35)

  add object oMVCODIVA_1_27 as StdField with uid="SRMIBGZHJS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVCODIVA", cQueryName = "MVCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA (utilizzato in caso di documento non esente)",;
    HelpContextID = 145316103,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=115, Top=74, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_MVCODIVA"

  func oMVCODIVA_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCODIVA_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODIVA_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODIVA_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oMVCODIVA_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'gscg_iva.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oMVCODIVA_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_MVCODIVA
     i_obj.ecpSave()
  endproc


  add object oBtn_1_28 as StdButton with uid="MBAYGNIOMQ",left=491, top=53, width=48,height=45,;
    CpPicture="BMP\COMPONE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la combinazione categoria codice IVA applicata";
    , HelpContextID = 267429937;
    , Caption='\<Comb.Iva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSVE_BFC(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MVTIPRIG <> 'D' And Empty( .w_MVCACONT ))
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MVTIPRIG = 'D' or not Empty( .w_MVCACONT ))
     endwith
    endif
  endfunc

  add object oMVCONTRO_1_29 as StdField with uid="GCUPBCRRPR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MVCONTRO", cQueryName = "MVCONTRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto contropartita non congruente",;
    ToolTipText = "Contropartita di contabilizzazione riga documento (vuoto: prende cat.contabile)",;
    HelpContextID = 196519659,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=115, Top=104, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MVCONTRO"

  func oMVCONTRO_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA')
    endwith
   endif
  endfunc

  func oMVCONTRO_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONTRO_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONTRO_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMVCONTRO_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oMVCONTRO_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MVCONTRO
     i_obj.ecpSave()
  endproc

  add object oDESCO2_1_30 as StdField with uid="TSMGYCUVZE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCO2", cQueryName = "DESCO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229750986,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=104, InputMask=replicate('X',40)

  add object oDESIVA_1_31 as StdField with uid="DEYGVRNGHP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 238794954,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=178, Top=74, InputMask=replicate('X',35)

  add object oMVCONTRA_1_34 as StdField with uid="IZQPYGLPWU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MVCONTRA", cQueryName = "MVCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del contratto applicato",;
    HelpContextID = 196519673,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=115, Top=134, InputMask=replicate('X',15), bHasZoom = .t. , visible=iif(g_GESCON='S', .T., .F.), cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_MVTIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_MVCONTRA"

  func oMVCONTRA_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GESCON='S')
    endwith
   endif
  endfunc

  func oMVCONTRA_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCONTRA_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCONTRA_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oMVCONTRA_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSVE1MDV.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oMVCONTRA_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_MVTIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_MVCONTRA
     i_obj.ecpSave()
  endproc

  add object oDESCTR_1_35 as StdField with uid="KZVSQIJMHJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCTR", cQueryName = "DESCTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 224508106,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=134, InputMask=replicate('X',50), visible=iif(g_GESCON='S', .T., .F.)


  add object oObj_1_53 as cp_runprogram with uid="BAZQKLJNGG",left=394, top=390, width=169,height=19,;
    caption='GSVE_BFC',;
   bGlobalFont=.t.,;
    prg="GSVE_BFC",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 55608745

  add object oMVINICOM_1_59 as StdField with uid="ELKYXQVIAV",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MVINICOM", cQueryName = "MVINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio competenza contabile",;
    HelpContextID = 218580717,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=115, Top=164

  add object oMVFINCOM_1_60 as StdField with uid="TONAFFZLXB",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MVFINCOM", cQueryName = "MVFINCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 213677805,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=250, Top=164

  func oMVFINCOM_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_MVINICOM) OR .w_MVINICOM<=.w_MVFINCOM)
    endwith
    return bRes
  endfunc


  add object oBtn_1_64 as StdButton with uid="XYEYKENOKZ",left=491, top=162, width=48,height=45,;
    CpPicture="BMP\DOCUMENTI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento generato dalla evasione componenti";
    , HelpContextID = 8225236;
    , Caption='\<Compon.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_MVRIFESC, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_64.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVRIFESC) AND g_EACD='S')
      endwith
    endif
  endfunc

  func oBtn_1_64.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_MVRIFESC) OR g_EACD<>'S')
     endwith
    endif
  endfunc

  add object oDESVOC_1_72 as StdField with uid="RRXGULMIJI",rtseq=145,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 211728586,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=259, InputMask=replicate('X',40)

  func oDESVOC_1_72.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  add object oMVVOCCEN_1_73 as StdField with uid="BXYHJRRPSK",rtseq=146,rtrep=.f.,;
    cFormVar = "w_MVVOCCEN", cQueryName = "MVVOCCEN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di ricavo",;
    HelpContextID = 43682068,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=115, Top=259, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MVVOCCEN"

  func oMVVOCCEN_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (((g_PERCCR='S' AND .w_FLANAL='S' and .w_MVFLNOAN='N' ) OR (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D')
    endwith
   endif
  endfunc

  func oMVVOCCEN_1_73.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  func oMVVOCCEN_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVVOCCEN_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVVOCCEN_1_73.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMVVOCCEN_1_73'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oMVVOCCEN_1_73.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MVVOCCEN
     i_obj.ecpSave()
  endproc

  add object oMVFLNOAN_1_74 as StdCheck with uid="MITWDAIHPR",rtseq=147,rtrep=.f.,left=116, top=285, caption="Escludi analitica",;
    ToolTipText = "Se attivo esclude analitica di riga",;
    HelpContextID = 12154604,;
    cFormVar="w_MVFLNOAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMVFLNOAN_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMVFLNOAN_1_74.GetRadio()
    this.Parent.oContained.w_MVFLNOAN = this.RadioValue()
    return .t.
  endfunc

  func oMVFLNOAN_1_74.SetRadio()
    this.Parent.oContained.w_MVFLNOAN=trim(this.Parent.oContained.w_MVFLNOAN)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLNOAN=='S',1,;
      0)
  endfunc

  func oMVFLNOAN_1_74.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' AND .w_FLANAL='S'))
    endwith
  endfunc

  add object oMVCACONT_1_85 as StdField with uid="KVLLBRQSWS",rtseq=209,rtrep=.f.,;
    cFormVar = "w_MVCACONT", cQueryName = "MVCACONT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contributo accessorio",;
    HelpContextID = 24422118,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=115, Top=335, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGCODICE", oKey_1_2="this.w_MVCACONT"

  func oMVCACONT_1_85.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  func oMVCACONT_1_85.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  func oMVCACONT_1_85.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_85('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCACONT_1_85.ecpDrop(oSource)
    this.Parent.oContained.link_1_85('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCACONT_1_85.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATMCONT','*','CGCODICE',cp_AbsName(this.parent,'oMVCACONT_1_85'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"",'',this.parent.oContained
  endproc
  proc oMVCACONT_1_85.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CGCODICE=this.parent.oContained.w_MVCACONT
     i_obj.ecpSave()
  endproc

  add object oCGDESCRI_1_87 as StdField with uid="GOQBGRCCTH",rtseq=210,rtrep=.f.,;
    cFormVar = "w_CGDESCRI", cQueryName = "CGDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208709265,;
   bGlobalFont=.t.,;
    Height=21, Width=290, Left=178, Top=334, InputMask=replicate('X',40)

  func oCGDESCRI_1_87.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="CJAYWZWRLE",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=110, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="PVYGIPDQGQ",Visible=.t., Left=4, Top=44,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GSPIACVNHL",Visible=.t., Left=383, Top=15,;
    Alignment=1, Width=137, Height=15,;
    Caption="Liv. raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BLKSAEZIGO",Visible=.t., Left=4, Top=104,;
    Alignment=1, Width=110, Height=15,;
    Caption="Contropartita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DCCUNSKWCR",Visible=.t., Left=4, Top=134,;
    Alignment=1, Width=110, Height=15,;
    Caption="Contratto:"    , visible=iif(g_GESCON='S', .T., .F.);
  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="YYAZDJRGBF",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=110, Height=15,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="GZXFWMQCGY",Visible=.t., Left=198, Top=165,;
    Alignment=1, Width=49, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="KGBIMNEGYS",Visible=.t., Left=4, Top=165,;
    Alignment=1, Width=110, Height=15,;
    Caption="Competenza da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="FRRXLKVZKC",Visible=.t., Left=23, Top=261,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D') OR .w_VOCTIP<>'R')
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="JJMWTYGWMS",Visible=.t., Left=23, Top=261,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D') OR .w_VOCTIP<>'C')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="LNORNUBEIT",Visible=.t., Left=4, Top=233,;
    Alignment=0, Width=100, Height=18,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (NOT (((g_PERCCR='S' AND .w_FLANAL='S') OR  (g_COMM='S' AND .w_FLGCOM='S')) AND .w_MVTIPRIG<>'D'))
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="MRSIBYWRTC",Visible=.t., Left=-1, Top=336,;
    Alignment=1, Width=115, Height=18,;
    Caption="Categoria contributo:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="TNNHJNBACR",Visible=.t., Left=4, Top=312,;
    Alignment=0, Width=180, Height=18,;
    Caption="Contributi accessori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oBox_1_75 as StdBox with uid="VCWKEHGRUU",left=4, top=253, width=553,height=2

  add object oBox_1_89 as StdBox with uid="QMCXEWVEJJ",left=4, top=330, width=553,height=2
enddefine
define class tgsve_kdaPag2 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVCODART_2_3 as StdField with uid="BIMWERAPQD",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MVCODART", cQueryName = "MVCODART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 257337062,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=118, Top=32, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MVCODART"

  func oMVCODART_2_3.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  func oMVCODART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVCAUMAG_2_6 as StdField with uid="GQYUERZCFD",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MVCAUMAG", cQueryName = "MVCAUMAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di movimentazione magazzino",;
    HelpContextID = 39102195,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVCAUMAG"

  func oMVCAUMAG_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCCAU = 'S')
    endwith
   endif
  endfunc

  func oMVCAUMAG_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCAUMAG_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCAUMAG_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oMVCAUMAG_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oMVCAUMAG_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_MVCAUMAG
     i_obj.ecpSave()
  endproc

  add object oDESCAU_2_8 as StdField with uid="WSJENBSOTG",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 194099402,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=182, Top=61, InputMask=replicate('X',35)

  add object oMVCAUCOL_2_15 as StdField with uid="FJDAUAPNPY",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MVCAUCOL", cQueryName = "MVCAUCOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 206874350,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=119, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVCAUCOL"

  func oMVCAUCOL_2_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  func oMVCAUCOL_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMVFLELGM_2_20 as StdCheck with uid="VACWELHJVE",rtseq=72,rtrep=.f.,left=439, top=61, caption="Movimento fiscale", enabled=.f.,;
    ToolTipText = "Se attivo: riga valida per la contabilit� fiscale di magazzino",;
    HelpContextID = 196512019,;
    cFormVar="w_MVFLELGM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLELGM_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLELGM_2_20.GetRadio()
    this.Parent.oContained.w_MVFLELGM = this.RadioValue()
    return .t.
  endfunc

  func oMVFLELGM_2_20.SetRadio()
    this.Parent.oContained.w_MVFLELGM=trim(this.Parent.oContained.w_MVFLELGM)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLELGM=='S',1,;
      0)
  endfunc

  add object oMVCODMAG_2_23 as StdField with uid="AEBXWVHGZN",rtseq=75,rtrep=.f.,;
    cFormVar = "w_MVCODMAG", cQueryName = "MVCODMAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente o obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 56010483,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAG"

  func oMVCODMAG_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL) and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVCODMAG_2_23.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR  EMPTY(.w_AGGSAL))
    endwith
  endfunc

  func oMVCODMAG_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAG_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAG_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAG_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMVCODMAG_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_2_28 as StdField with uid="MBJYMYZVIG",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159889610,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=182, Top=90, InputMask=replicate('X',30)

  func oDESMAG_2_28.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oMVCODMAT_2_29 as StdField with uid="MCLBWSULNT",rtseq=81,rtrep=.f.,;
    cFormVar = "w_MVCODMAT", cQueryName = "MVCODMAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente o obsoleto",;
    ToolTipText = "Codice eventuale magazzino collegato",;
    HelpContextID = 56010470,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=118, Top=150, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAT"

  func oMVCODMAT_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVCAUCOL) AND .w_MVTIPRIG = 'R' AND NOT EMPTY(.w_AGGSAL1) and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVCODMAT_2_29.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  func oMVCODMAT_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAT_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAT_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAT_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMVCODMAT_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAT
     i_obj.ecpSave()
  endproc

  add object oDESMAT_2_34 as StdField with uid="CYALAXXGKO",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESMAT", cQueryName = "DESMAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 210221258,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=182, Top=150, InputMask=replicate('X',30)

  func oDESMAT_2_34.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oMVDATEVA_2_35 as StdField with uid="SPPHKPSMKJ",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MVDATEVA", cQueryName = "MVDATEVA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione riga",;
    HelpContextID = 94071047,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=118, Top=227

  func oMVDATEVA_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDTPR='S' and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVDATEVA_2_35.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  func oMVDATEVA_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_MVDATEVA>=.w_MVDATDOC OR EMPTY(.w_MVDATEVA))
         bRes=(cp_WarningMsg(thisform.msgFmt("La data di prevista evasione di riga � minore della data documento")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oMVFLEVAS_2_36 as StdCheck with uid="FFILUIPPJU",rtseq=88,rtrep=.f.,left=118, top=254, caption="Evasa",;
    ToolTipText = "Se attivo: riga evasa",;
    HelpContextID = 172586727,;
    cFormVar="w_MVFLEVAS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMVFLEVAS_2_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMVFLEVAS_2_36.GetRadio()
    this.Parent.oContained.w_MVFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oMVFLEVAS_2_36.SetRadio()
    this.Parent.oContained.w_MVFLEVAS=trim(this.Parent.oContained.w_MVFLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLEVAS=='S',1,;
      0)
  endfunc

  func oMVFLEVAS_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDTPR='S'  AND .w_MVCLADOC<>'RF' and Not .w_TESCOMP)
    endwith
   endif
  endfunc

  func oMVFLEVAS_2_36.mHide()
    with this.Parent.oContained
      return (NOT (.w_FLDTPR='S'  AND .w_MVCLADOC<>'RF'))
    endwith
  endfunc

  add object oQTDISP_2_43 as StdField with uid="JJMXAXHMXZ",rtseq=89,rtrep=.f.,;
    cFormVar = "w_QTDISP", cQueryName = "QTDISP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino principale",;
    HelpContextID = 258775290,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=469, Top=90, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oQTDISP_2_43.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR  EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oQ2DISP_2_44 as StdField with uid="FLMIBBJHUN",rtseq=90,rtrep=.f.,;
    cFormVar = "w_Q2DISP", cQueryName = "Q2DISP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino collegato",;
    HelpContextID = 258783994,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=469, Top=150, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oQ2DISP_2_44.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oMVEFFEVA_2_49 as StdField with uid="ZQQLLZYJFO",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MVEFFEVA", cQueryName = "MVEFFEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data effettiva evasione riga",;
    HelpContextID = 79722759,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=252, Top=227

  func oMVEFFEVA_2_49.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oMVQTAEVA_2_55 as StdField with uid="GKSHUFBUDV",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MVQTAEVA", cQueryName = "MVQTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente evasa sulla riga documento",;
    HelpContextID = 75446535,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=474, Top=227, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAEVA_2_55.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oMVIMPEVA_2_56 as StdField with uid="QEKYAUGCAO",rtseq=95,rtrep=.f.,;
    cFormVar = "w_MVIMPEVA", cQueryName = "MVIMPEVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo complessivamente evaso sulla riga documento",;
    HelpContextID = 90683655,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=434, Top=227, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMVIMPEVA_2_56.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVIMPEVA=0) OR .w_MVTIPRIG<>'F')
    endwith
  endfunc

  add object oDESART_2_60 as StdField with uid="QISOWWOTKQ",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193181898,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=271, Top=32, InputMask=replicate('X',40)

  func oDESART_2_60.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  add object oMVQTAEV1_2_63 as StdField with uid="BNMIOCVSRT",rtseq=124,rtrep=.f.,;
    cFormVar = "w_MVQTAEV1", cQueryName = "MVQTAEV1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente evasa sulla riga documento",;
    HelpContextID = 75446519,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=476, Top=254, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAEV1_2_63.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F' OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUM1_2_64 as StdField with uid="UTFGSOSNKI",rtseq=125,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186200134,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=434, Top=254, InputMask=replicate('X',3)

  func oUM1_2_64.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F' OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUMR_2_65 as StdField with uid="VZGXPGNWNC",rtseq=126,rtrep=.f.,;
    cFormVar = "w_UMR", cQueryName = "UMR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186335302,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=434, Top=227, InputMask=replicate('X',3)

  func oUMR_2_65.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oMVQTAIMP_2_67 as StdField with uid="ETJTNCPBEP",rtseq=127,rtrep=.f.,;
    cFormVar = "w_MVQTAIMP", cQueryName = "MVQTAIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente importata dal documento di origine",;
    HelpContextID = 125880042,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=157, Top=281, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAIMP_2_67.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc

  add object oMVQTAIM1_2_68 as StdField with uid="UBUPAULQQA",rtseq=128,rtrep=.f.,;
    cFormVar = "w_MVQTAIM1", cQueryName = "MVQTAIM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� complessivamente importata dal documento di origine espressa nella 1^ U.M.",;
    HelpContextID = 125880073,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=288, Top=281, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMVQTAIM1_2_68.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF) OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUM1_2_69 as StdField with uid="WURLSNGSON",rtseq=129,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186200134,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=249, Top=281, InputMask=replicate('X',3)

  func oUM1_2_69.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF) OR .w_MVUNIMIS=.w_UNMIS1 OR (.w_MVUNIMIS<>.w_UNMIS1 And .w_MVUNIMIS<>.w_UNMIS2 AND .w_MVUNIMIS<>.w_UNMIS3))
    endwith
  endfunc

  add object oUMR_2_70 as StdField with uid="MGYWRHVDOI",rtseq=130,rtrep=.f.,;
    cFormVar = "w_UMR", cQueryName = "UMR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186335302,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=118, Top=281, InputMask=replicate('X',3)

  func oUMR_2_70.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc


  add object oBtn_2_79 as StdButton with uid="USQFNLODSQ",left=513, top=316, width=48,height=45,;
    CpPicture="BMP\DocDest.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai documenti di destinazione che hanno evaso la riga";
    , HelpContextID = 21669609;
    , Tabstop=.f.,Caption='\<Doc. Dest.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_79.Click()
      with this.Parent.oContained
        GSVE_BD3(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_83 as StdButton with uid="RGMQJTWBTF",left=459, top=316, width=48,height=45,;
    CpPicture="bmp\Origine.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle righe evase dei documenti di origine";
    , HelpContextID = 213303567;
    , Tabstop=.f.,Caption='\<Doc. Orig.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_83.Click()
      with this.Parent.oContained
        GSVE_BD3(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_86 as cp_runprogram with uid="OEGXODOFHV",left=-1, top=379, width=213,height=27,;
    caption='GSVE_BEV',;
   bGlobalFont=.t.,;
    prg="GSVE_BEV",;
    cEvent = "w_MVFLEVAS Changed",;
    nPag=2;
    , HelpContextID = 55608764

  add object oDESCA2_2_87 as StdField with uid="FRDHXIINZF",rtseq=151,rtrep=.f.,;
    cFormVar = "w_DESCA2", cQueryName = "DESCA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244431050,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=182, Top=119, InputMask=replicate('X',35)

  func oDESCA2_2_87.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oMVRIFEDI_2_91 as StdField with uid="UWVPTTLLCY",rtseq=208,rtrep=.f.,;
    cFormVar = "w_MVRIFEDI", cQueryName = "MVRIFEDI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento evasione EDI",;
    HelpContextID = 79972623,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=118, Top=310, InputMask=replicate('X',40)

  func oMVRIFEDI_2_91.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_2_37 as StdString with uid="TWVUJFOWGM",Visible=.t., Left=4, Top=90,;
    Alignment=1, Width=113, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oStr_2_38 as StdString with uid="GCGOUWGWOL",Visible=.t., Left=4, Top=150,;
    Alignment=1, Width=113, Height=15,;
    Caption="Mag.collegato:"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="KJIJJQARJE",Visible=.t., Left=4, Top=61,;
    Alignment=1, Width=113, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="NBEBDQOFDW",Visible=.t., Left=10, Top=6,;
    Alignment=0, Width=357, Height=15,;
    Caption="Dati magazzino"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="HHRWLWNAVZ",Visible=.t., Left=422, Top=90,;
    Alignment=1, Width=45, Height=15,;
    Caption="Disp.:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG <> 'R' OR EMPTY(.w_AGGSAL))
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="VZZBCBZEYH",Visible=.t., Left=422, Top=150,;
    Alignment=1, Width=45, Height=15,;
    Caption="Disp.:"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="VUEQCNHENP",Visible=.t., Left=10, Top=201,;
    Alignment=0, Width=115, Height=15,;
    Caption="Evasione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="XUSPDROJXP",Visible=.t., Left=28, Top=227,;
    Alignment=1, Width=89, Height=15,;
    Caption="Prevista:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="VARBXKVLAC",Visible=.t., Left=200, Top=227,;
    Alignment=1, Width=50, Height=15,;
    Caption="Effettiva:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (.w_FLDTPR<>'S' AND .w_MVQTAEVA=0 AND .w_MVIMPEVA=0)
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="JONDIRZACX",Visible=.t., Left=334, Top=227,;
    Alignment=1, Width=98, Height=15,;
    Caption="Quantit� evasa:"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVQTAEVA=0) OR .w_MVTIPRIG='F')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="IRMLVIVGZN",Visible=.t., Left=333, Top=227,;
    Alignment=1, Width=99, Height=15,;
    Caption="Importo evaso:"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return ((.w_FLDTPR<>'S' AND .w_MVIMPEVA=0) OR .w_MVTIPRIG<>'F')
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="LFJDBJJRGT",Visible=.t., Left=4, Top=32,;
    Alignment=1, Width=113, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (.w_MVCODART=.w_MVCODICE)
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="IHWDVGCOAV",Visible=.t., Left=3, Top=281,;
    Alignment=1, Width=114, Height=18,;
    Caption="Quantit� importata:"  ;
  , bGlobalFont=.t.

  func oStr_2_66.mHide()
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'DF' OR EMPTY(.w_MVSERRIF))
    endwith
  endfunc

  add object oStr_2_88 as StdString with uid="SWOEEGOSRH",Visible=.t., Left=4, Top=119,;
    Alignment=1, Width=113, Height=18,;
    Caption="Causale collegata:"  ;
  , bGlobalFont=.t.

  func oStr_2_88.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVCAUCOL) OR .w_MVTIPRIG<>'R' OR EMPTY(.w_AGGSAL1))
    endwith
  endfunc

  add object oStr_2_92 as StdString with uid="FFLARVCVTO",Visible=.t., Left=11, Top=312,;
    Alignment=1, Width=106, Height=18,;
    Caption="Riferimento EDI:"  ;
  , bGlobalFont=.t.

  func oStr_2_92.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oBox_2_47 as StdBox with uid="FGGBKXZAQL",left=7, top=24, width=554,height=2

  add object oBox_2_52 as StdBox with uid="XTETPGHRNT",left=10, top=219, width=552,height=2
enddefine
define class tgsve_kdaPag3 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMVFLTRAS_3_1 as StdCombo with uid="ZIHZVFCINR",value=3,rtseq=96,rtrep=.f.,left=180,top=36,width=160,height=21;
    , ToolTipText = "Compilazione campi dati Intrastat: nessuno - solo statistici - statistici e fiscali";
    , HelpContextID = 223966951;
    , cFormVar="w_MVFLTRAS",RowSource=""+"Nessuna valorizzazione,"+"Solo dati statistici,"+"Dati statistici e fiscali,"+"Solo dati fiscali", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oMVFLTRAS_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'Z',;
    iif(this.value =3,' ',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oMVFLTRAS_3_1.GetRadio()
    this.Parent.oContained.w_MVFLTRAS = this.RadioValue()
    return .t.
  endfunc

  func oMVFLTRAS_3_1.SetRadio()
    this.Parent.oContained.w_MVFLTRAS=trim(this.Parent.oContained.w_MVFLTRAS)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLTRAS=='S',1,;
      iif(this.Parent.oContained.w_MVFLTRAS=='Z',2,;
      iif(this.Parent.oContained.w_MVFLTRAS=='',3,;
      iif(this.Parent.oContained.w_MVFLTRAS=='I',4,;
      0))))
  endfunc

  func oMVFLTRAS_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFM' AND g_INTR='S'  AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oMVNOMENC_3_2 as StdField with uid="SHVQQALYXF",rtseq=97,rtrep=.f.,;
    cFormVar = "w_MVNOMENC", cQueryName = "MVNOMENC",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice nomenclatura",;
    HelpContextID = 180745975,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=180, Top=68, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_MVNOMENC"

  func oMVNOMENC_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND (.w_MVFLTRAS<>'S' or .w_ARUTISER='S'))
    endwith
   endif
  endfunc

  func oMVNOMENC_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVNOMENC_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVNOMENC_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oMVNOMENC_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"Nomenclature",'GSMA_AZN.NOMENCLA_VZM',this.parent.oContained
  endproc
  proc oMVNOMENC_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_MVNOMENC
     i_obj.ecpSave()
  endproc

  add object oDESNOM_3_3 as StdField with uid="YNDZFDIPWE",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44480714,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=267, Top=68, InputMask=replicate('X',35)

  add object oUMSUPP_3_5 as StdField with uid="YROUJEWMCI",rtseq=100,rtrep=.f.,;
    cFormVar = "w_UMSUPP", cQueryName = "UMSUPP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 261074874,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=180, Top=97, InputMask=replicate('X',3)

  add object oMVMOLSUP_3_7 as StdField with uid="XMPGXFCEFC",rtseq=102,rtrep=.f.,;
    cFormVar = "w_MVMOLSUP", cQueryName = "MVMOLSUP",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Moltiplicatore U.M. supplementare rispetto alla 1^U.M.",;
    HelpContextID = 53082390,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=242, Top=97, cSayPict='"9999.999"', cGetPict='"9999.999"'

  func oMVMOLSUP_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_MVFLTRAS<>'S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oUM1_3_8 as StdField with uid="IIJFWNEYHG",rtseq=103,rtrep=.f.,;
    cFormVar = "w_UM1", cQueryName = "UM1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 186200134,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=323, Top=97, InputMask=replicate('X',3)

  add object oMVPESNET_3_9 as StdField with uid="ZDLVFFAAHG",rtseq=104,rtrep=.f.,;
    cFormVar = "w_MVPESNET", cQueryName = "MVPESNET",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa netta unitaria in kg.",;
    HelpContextID = 244328730,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=180, Top=126, cSayPict='"99999.999"', cGetPict='"99999.999"'

  func oMVPESNET_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND .w_MVFLTRAS<>'S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  add object oTOTMASNE_3_10 as StdField with uid="JUEQNZAYEM",rtseq=105,rtrep=.f.,;
    cFormVar = "w_TOTMASNE", cQueryName = "TOTMASNE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226991493,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=315, Top=126, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oMVPROORD_3_11 as StdField with uid="BGTGPLMRLF",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MVPROORD", cQueryName = "MVPROORD",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di origine/destinazione della merce",;
    HelpContextID = 10671862,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=180, Top=155, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oMVPROORD_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_MVFLTRAS<>'S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  func oMVPROORD_3_11.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oMVNAZPRO_3_12 as StdField with uid="OKERSSTIEF",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MVNAZPRO", cQueryName = "MVNAZPRO",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione di provenienza della merce (se non specificato, prende naz. fornitore)",;
    HelpContextID = 251918059,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=354, Top=155, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_MVNAZPRO"

  func oMVNAZPRO_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_MVFLTRAS<>'S' AND .w_ARUTISER='N')
    endwith
   endif
  endfunc

  func oMVNAZPRO_3_12.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  func oMVNAZPRO_3_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVNAZPRO_3_12.ecpDrop(oSource)
    this.Parent.oContained.link_3_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVNAZPRO_3_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oMVNAZPRO_3_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oMVNAZPRO_3_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_MVNAZPRO
     i_obj.ecpSave()
  endproc

  add object oMVAIRPOR_3_14 as StdField with uid="GRJKZXSLKE",rtseq=109,rtrep=.f.,;
    cFormVar = "w_MVAIRPOR", cQueryName = "MVAIRPOR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice porto/aeroporto da dove � stata spedita la merce a cliente",;
    HelpContextID = 259835624,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=180, Top=184, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COD_AREO", cZoomOnZoom="GSAR_APP", oKey_1_1="PPCODICE", oKey_1_2="this.w_MVAIRPOR"

  func oMVAIRPOR_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'FRM' AND g_INTR='S' AND .w_MVFLTRAS<>'S')
    endwith
   endif
  endfunc

  func oMVAIRPOR_3_14.mHide()
    with this.Parent.oContained
      return ((NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  func oMVAIRPOR_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVAIRPOR_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVAIRPOR_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_AREO','*','PPCODICE',cp_AbsName(this.parent,'oMVAIRPOR_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APP',"Porti/areoporti",'',this.parent.oContained
  endproc
  proc oMVAIRPOR_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PPCODICE=this.parent.oContained.w_MVAIRPOR
     i_obj.ecpSave()
  endproc


  add object oObj_3_30 as cp_runprogram with uid="PEOFZMAZIK",left=-6, top=380, width=154,height=22,;
    caption='GSVE_BCH',;
   bGlobalFont=.t.,;
    prg="GSVE_BCH",;
    cEvent = "CheckColl",;
    nPag=3;
    , HelpContextID = 212826706

  add object oMVCODCOL_3_36 as StdField with uid="NITHPCULIX",rtseq=157,rtrep=.f.,;
    cFormVar = "w_MVCODCOL", cQueryName = "MVCODCOL",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia collo associata all'unit� di misura dell'articolo",;
    HelpContextID = 223782638,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=79, Top=250, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_MVCODCOL"

  func oMVCODCOL_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPACK='S')
    endwith
   endif
  endfunc

  func oMVCODCOL_3_36.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  func oMVCODCOL_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCOL_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCOL_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oMVCODCOL_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oMVCODCOL_3_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_MVCODCOL
     i_obj.ecpSave()
  endproc

  add object oDESCOL_3_37 as StdField with uid="XQOKUFRWKB",rtseq=158,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61978826,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=136, Top=250, InputMask=replicate('X',35)

  func oDESCOL_3_37.mHide()
    with this.Parent.oContained
      return ((.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG)))
    endwith
  endfunc

  add object oMVNUMCOL_3_38 as StdField with uid="ALNMBMTVNQ",rtseq=159,rtrep=.f.,;
    cFormVar = "w_MVNUMCOL", cQueryName = "MVNUMCOL",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Superato massimo numero confezioni per riga. Valore massimo 30000.",;
    ToolTipText = "Numero confezioni per riga",;
    HelpContextID = 213907182,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=494, Top=279, cSayPict='"99999"', cGetPict='"99999"'

  func oMVNUMCOL_3_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MVCODCOL) and .w_FLCOVA='S')
    endwith
   endif
  endfunc

  func oMVNUMCOL_3_38.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  func oMVNUMCOL_3_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVNUMCOL<=30000)
    endwith
    return bRes
  endfunc

  add object oCODCONF_3_39 as StdField with uid="NHZQBTOGGX",rtseq=160,rtrep=.f.,;
    cFormVar = "w_CODCONF", cQueryName = "CODCONF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo confezione associata all'articolo",;
    HelpContextID = 239952166,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=79, Top=279, InputMask=replicate('X',3), cLinkFile="TIPICONF", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCONF", oKey_2_1="TCCODICE", oKey_2_2="this.w_CODCONF"

  func oCODCONF_3_39.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  func oCODCONF_3_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCONF)
        bRes2=.link_3_39('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCONF_3_41 as StdField with uid="JDTBRLINCS",rtseq=161,rtrep=.f.,;
    cFormVar = "w_DESCONF", cQueryName = "DESCONF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240011062,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=136, Top=279, InputMask=replicate('X',35)

  func oDESCONF_3_41.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  add object oMVUNILOG_3_43 as StdField with uid="UMKOBJSIZO",rtseq=162,rtrep=.f.,;
    cFormVar = "w_MVUNILOG", cQueryName = "MVUNILOG",;
    bObbl = .f. , nPag = 3, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero unit� logistica",;
    HelpContextID = 67536627,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=136, Top=307, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", cZoomOnZoom="GSMD_AUL", oKey_1_1="UL__SSCC", oKey_1_2="this.w_MVUNILOG"

  func oMVUNILOG_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MVCODLOT) and not empty(.w_MVCODCOL) and .w_MVNUMCOL>0 and .w_FLPACK='S')
    endwith
   endif
  endfunc

  func oMVUNILOG_3_43.mHide()
    with this.Parent.oContained
      return ((.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG)) or g_MADV<>'S' or .w_FLESUL='S')
    endwith
  endfunc

  func oMVUNILOG_3_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVUNILOG_3_43.ecpDrop(oSource)
    this.Parent.oContained.link_3_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVUNILOG_3_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oMVUNILOG_3_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AUL',"Unit� logistiche",'GSMD_KDA.UNIT_LOG_VZM',this.parent.oContained
  endproc
  proc oMVUNILOG_3_43.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AUL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UL__SSCC=this.parent.oContained.w_MVUNILOG
     i_obj.ecpSave()
  endproc


  add object oBtn_3_45 as StdButton with uid="YIYFCAXAUS",left=280, top=309, width=21,height=19,;
    caption="...", nPag=3;
    , ToolTipText = "Premere per creare una nuova unit� logistica";
    , HelpContextID = 186179286;
    , caption='...';
  , bGlobalFont=.t.

    proc oBtn_3_45.Click()
      with this.Parent.oContained
        do GSMD_BDV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_MVCODLOT) and not empty(.w_MVCODCOL) and .w_MVNUMCOL>0 And Inlist( .oparentobject.cFunction ,'Edit', 'Load'))
      endwith
    endif
  endfunc

  func oBtn_3_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLPACK<>'S' or g_MADV<>'S' or .w_FLESUL='S')
     endwith
    endif
  endfunc

  add object oDESCRI_3_48 as StdField with uid="WTRNAQXJQU",rtseq=206,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109164746,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=279, Top=184, InputMask=replicate('X',40)

  func oDESCRI_3_48.mHide()
    with this.Parent.oContained
      return ((NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oISONAZ_3_50 as StdField with uid="IJPKXAXNUL",rtseq=207,rtrep=.f.,;
    cFormVar = "w_ISONAZ", cQueryName = "ISONAZ",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109505146,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=462, Top=155, InputMask=replicate('X',3)

  func oISONAZ_3_50.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_15 as StdString with uid="CCJDFVCXKJ",Visible=.t., Left=4, Top=7,;
    Alignment=0, Width=240, Height=15,;
    Caption="Dati INTRA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_16 as StdString with uid="YUGLZHEKFC",Visible=.t., Left=33, Top=68,;
    Alignment=1, Width=144, Height=15,;
    Caption="Nomenclatura:"  ;
  , bGlobalFont=.t.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'N')
    endwith
  endfunc

  add object oStr_3_17 as StdString with uid="QFOTVSYKHP",Visible=.t., Left=34, Top=97,;
    Alignment=1, Width=144, Height=15,;
    Caption="U.M. supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="XFRZKNAXKT",Visible=.t., Left=218, Top=97,;
    Alignment=2, Width=22, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="VWAQNFHYIP",Visible=.t., Left=34, Top=126,;
    Alignment=1, Width=144, Height=15,;
    Caption="Massa netta unitaria:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="HAHYBAICCU",Visible=.t., Left=262, Top=126,;
    Alignment=1, Width=51, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="PKMFYFDURG",Visible=.t., Left=421, Top=126,;
    Alignment=0, Width=33, Height=15,;
    Caption="Kg."  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="ZZRKFHKLXW",Visible=.t., Left=10, Top=219,;
    Alignment=0, Width=108, Height=15,;
    Caption="Dati Packing List"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S')
    endwith
  endfunc

  add object oStr_3_25 as StdString with uid="LSKFIJCZCU",Visible=.t., Left=212, Top=157,;
    Alignment=1, Width=141, Height=18,;
    Caption="Paese di destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_25.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_26 as StdString with uid="LFASIZCZDH",Visible=.t., Left=34, Top=157,;
    Alignment=1, Width=144, Height=15,;
    Caption="Provincia di origine:"  ;
  , bGlobalFont=.t.

  func oStr_3_26.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_35 as StdString with uid="DUMHMMQQXA",Visible=.t., Left=11, Top=250,;
    Alignment=1, Width=66, Height=15,;
    Caption="Tipo colli:"  ;
  , bGlobalFont=.t.

  func oStr_3_35.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  add object oStr_3_40 as StdString with uid="RTVIFESQWJ",Visible=.t., Left=12, Top=279,;
    Alignment=1, Width=65, Height=18,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_3_40.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  add object oStr_3_42 as StdString with uid="YOCXNKXDSG",Visible=.t., Left=400, Top=281,;
    Alignment=1, Width=93, Height=18,;
    Caption="Qta. Confezioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_42.mHide()
    with this.Parent.oContained
      return (.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))
    endwith
  endfunc

  add object oStr_3_44 as StdString with uid="QEDQHTDZQA",Visible=.t., Left=9, Top=308,;
    Alignment=1, Width=126, Height=18,;
    Caption="Num. unit� logistica:"  ;
  , bGlobalFont=.t.

  func oStr_3_44.mHide()
    with this.Parent.oContained
      return ((.w_FLPACK<>'S' and EMPTY(.w_MVUNILOG))  or g_MADV<>'S' or .w_FLESUL='S')
    endwith
  endfunc

  add object oStr_3_46 as StdString with uid="MQQAYBARNU",Visible=.t., Left=19, Top=184,;
    Alignment=1, Width=159, Height=18,;
    Caption="Porto/aeroporto di origine:"  ;
  , bGlobalFont=.t.

  func oStr_3_46.mHide()
    with this.Parent.oContained
      return ((NOT(.w_MVFLVEAC='V' AND .w_MVTIPRIG = 'R' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_47 as StdString with uid="ERQURYBOZC",Visible=.t., Left=4, Top=184,;
    Alignment=1, Width=174, Height=18,;
    Caption="Porto/aeroporto di destinazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_47.mHide()
    with this.Parent.oContained
      return ((NOT(.w_MVFLVEAC='A' AND .w_MVTIPRIG = 'R' AND g_INTR='S')) OR g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_49 as StdString with uid="WKRNPEQEYZ",Visible=.t., Left=404, Top=158,;
    Alignment=1, Width=55, Height=18,;
    Caption="Cod. ISO:"  ;
  , bGlobalFont=.t.

  func oStr_3_49.mHide()
    with this.Parent.oContained
      return (NOT (.w_MVTIPRIG $ 'FRM' AND g_INTR='S'))
    endwith
  endfunc

  add object oStr_3_52 as StdString with uid="ERIUYGJTBK",Visible=.t., Left=50, Top=68,;
    Alignment=1, Width=127, Height=15,;
    Caption="Codice servizio:"  ;
  , bGlobalFont=.t.

  func oStr_3_52.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
  endfunc

  add object oStr_3_53 as StdString with uid="SNPOLPVPXG",Visible=.t., Left=6, Top=37,;
    Alignment=1, Width=171, Height=17,;
    Caption="Manutenzione elenchi INTRA:"  ;
  , bGlobalFont=.t.

  add object oBox_3_23 as StdBox with uid="XDVBDDTNLL",left=2, top=24, width=548,height=2

  add object oBox_3_24 as StdBox with uid="EKIFJCQFJZ",left=2, top=238, width=548,height=2
enddefine
define class tgsve_kdaPag4 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMVTIPPRO_4_9 as StdCombo with uid="KRTFQIVOMZ",rtseq=193,rtrep=.f.,left=166,top=53,width=176,height=21;
    , HelpContextID = 261854955;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Suggerita da tab.provvigioni,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPRO_4_9.RadioValue()
    return(iif(this.value =1,'ST',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPRO_4_9.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_4_9.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='ST',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',4,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPRO_4_9.mHide()
    with this.Parent.oContained
      return (g_calpro$'GI-DI')
    endwith
  endfunc


  add object oMVTIPPRO_4_10 as StdCombo with uid="ILGCUIDBLC",rtseq=194,rtrep=.f.,left=166,top=53,width=176,height=21;
    , HelpContextID = 261854955;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Calcolata da tab.provvigioni,"+"Forzata,"+"Esclusa,"+"Calcolata da contratto,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPRO_4_10.RadioValue()
    return(iif(this.value =1,'CT',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'ES',;
    iif(this.value =4,'CC',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPRO_4_10.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_4_10.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='CT',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',4,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPRO_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_calpro$'DI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPRO_4_10.mHide()
    with this.Parent.oContained
      return (g_calpro$'DI-GD')
    endwith
  endfunc


  add object oMVTIPPRO_4_11 as StdCombo with uid="ARCUOTIPJA",rtseq=195,rtrep=.f.,left=166,top=53,width=176,height=21;
    , HelpContextID = 261854955;
    , cFormVar="w_MVTIPPRO",RowSource=""+"Da calcolare,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPRO_4_11.RadioValue()
    return(iif(this.value =1,'DC',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    space(2))))))
  endfunc
  func oMVTIPPRO_4_11.GetRadio()
    this.Parent.oContained.w_MVTIPPRO = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPRO_4_11.SetRadio()
    this.Parent.oContained.w_MVTIPPRO=trim(this.Parent.oContained.w_MVTIPPRO)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPRO=='DC',1,;
      iif(this.Parent.oContained.w_MVTIPPRO=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPRO=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPRO=='ES',4,;
      0))))
  endfunc

  func oMVTIPPRO_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_calpro$'GI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPRO_4_11.mHide()
    with this.Parent.oContained
      return (g_calpro$'GD-GI')
    endwith
  endfunc

  add object oMVPERPRO_4_12 as StdField with uid="QZCSCCTRMN",rtseq=196,rtrep=.f.,;
    cFormVar = "w_MVPERPRO", cQueryName = "MVPERPRO",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigioni agente applicato alla riga documento",;
    HelpContextID = 260036331,;
   bGlobalFont=.t.,;
    Height=20, Width=57, Left=166, Top=78, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMVPERPRO_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVIMPPRO=0  AND g_PERAGE='S' AND .w_MVTIPPRO='FO')
    endwith
   endif
  endfunc

  add object oMVIMPPRO_4_13 as StdField with uid="HRSMINDCEA",rtseq=197,rtrep=.f.,;
    cFormVar = "w_MVIMPPRO", cQueryName = "MVIMPPRO",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo provvigioni agente riferito alla riga documento",;
    HelpContextID = 261637867,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=364, Top=78, cSayPict="v_PU(38+VVL)", cGetPict="v_GU(38+VVL)"

  func oMVIMPPRO_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVPERPRO=0 AND g_PERAGE='S' AND .w_MVTIPPRO='FO')
    endwith
   endif
  endfunc


  add object oMVTIPPR2_4_14 as StdCombo with uid="MYXZZLAUPT",rtseq=198,rtrep=.f.,left=166,top=104,width=176,height=21;
    , HelpContextID = 261854984;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Suggerita da tab.provvigioni,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPR2_4_14.RadioValue()
    return(iif(this.value =1,'ST',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPR2_4_14.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_4_14.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='ST',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',4,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPR2_4_14.mHide()
    with this.Parent.oContained
      return (g_calpro$'GI-DI' or .w_MVPROCAP=999.9)
    endwith
  endfunc


  add object oMVTIPPR2_4_15 as StdCombo with uid="OSHPWARMKM",rtseq=199,rtrep=.f.,left=166,top=104,width=176,height=21;
    , HelpContextID = 261854984;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Calcolata da tab.provvigioni,"+"Forzata,"+"Esclusa,"+"Calcolata da contratto,"+"Esclusa da tab.provvigioni", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPR2_4_15.RadioValue()
    return(iif(this.value =1,'CT',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'ES',;
    iif(this.value =4,'CC',;
    iif(this.value =5,'ET',;
    space(2)))))))
  endfunc
  func oMVTIPPR2_4_15.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_4_15.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='CT',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',4,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ET',5,;
      0)))))
  endfunc

  func oMVTIPPR2_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_calpro$'DI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPR2_4_15.mHide()
    with this.Parent.oContained
      return (g_calpro$'DI-GD' or .w_MVPROCAP=999.9)
    endwith
  endfunc


  add object oMVTIPPR2_4_16 as StdCombo with uid="NWMYAAZQUK",rtseq=200,rtrep=.f.,left=166,top=104,width=176,height=21;
    , HelpContextID = 261854984;
    , cFormVar="w_MVTIPPR2",RowSource=""+"Da calcolare,"+"Forzata,"+"Calcolata da contratto,"+"Esclusa", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMVTIPPR2_4_16.RadioValue()
    return(iif(this.value =1,'DC',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'CC',;
    iif(this.value =4,'ES',;
    space(2))))))
  endfunc
  func oMVTIPPR2_4_16.GetRadio()
    this.Parent.oContained.w_MVTIPPR2 = this.RadioValue()
    return .t.
  endfunc

  func oMVTIPPR2_4_16.SetRadio()
    this.Parent.oContained.w_MVTIPPR2=trim(this.Parent.oContained.w_MVTIPPR2)
    this.value = ;
      iif(this.Parent.oContained.w_MVTIPPR2=='DC',1,;
      iif(this.Parent.oContained.w_MVTIPPR2=='FO',2,;
      iif(this.Parent.oContained.w_MVTIPPR2=='CC',3,;
      iif(this.Parent.oContained.w_MVTIPPR2=='ES',4,;
      0))))
  endfunc

  func oMVTIPPR2_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(g_calpro$'GI-GD'))
    endwith
   endif
  endfunc

  func oMVTIPPR2_4_16.mHide()
    with this.Parent.oContained
      return (g_calpro$'GD-GI' or .w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oMVPROCAP_4_17 as StdField with uid="YJGMPKTEOL",rtseq=201,rtrep=.f.,;
    cFormVar = "w_MVPROCAP", cQueryName = "MVPROCAP",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigioni capoarea applicato alla riga documento",;
    HelpContextID = 211998442,;
   bGlobalFont=.t.,;
    Height=20, Width=57, Left=166, Top=129, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMVPROCAP_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVIMPCAP=0  AND g_PERAGE='S' AND .w_MVTIPPR2='FO')
    endwith
   endif
  endfunc

  func oMVPROCAP_4_17.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oMVIMPCAP_4_18 as StdField with uid="VRAQDTUXBD",rtseq=202,rtrep=.f.,;
    cFormVar = "w_MVIMPCAP", cQueryName = "MVIMPCAP",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo provvigioni capoarea riferito alla riga documento",;
    HelpContextID = 211306218,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=364, Top=129, cSayPict="v_PU(38+VVL)", cGetPict="v_GU(38+VVL)"

  func oMVIMPCAP_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVTIPRIG $ 'RFMA' AND .w_MVPROCAP=0 AND g_PERAGE='S' AND .w_MVTIPPR2='FO')
    endwith
   endif
  endfunc

  func oMVIMPCAP_4_18.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_4_1 as StdString with uid="MHDFWSNQAR",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=159, Height=18,;
    Caption="Tipo provvigione capoarea:"  ;
  , bGlobalFont=.t.

  func oStr_4_1.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_4_2 as StdString with uid="BEMFCTLCDB",Visible=.t., Left=232, Top=129,;
    Alignment=1, Width=129, Height=15,;
    Caption="Importo provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_4_2.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_4_3 as StdString with uid="ALCPNXYBSQ",Visible=.t., Left=9, Top=129,;
    Alignment=1, Width=155, Height=15,;
    Caption="% Provvigioni capoarea:"  ;
  , bGlobalFont=.t.

  func oStr_4_3.mHide()
    with this.Parent.oContained
      return (.w_MVPROCAP=999.9)
    endwith
  endfunc

  add object oStr_4_4 as StdString with uid="GPJSHZHSQE",Visible=.t., Left=30, Top=53,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo provvigione agente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_6 as StdString with uid="UOINJVPPOF",Visible=.t., Left=20, Top=22,;
    Alignment=0, Width=108, Height=15,;
    Caption="Dati provvigioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_7 as StdString with uid="AWOANEOYNF",Visible=.t., Left=232, Top=78,;
    Alignment=1, Width=129, Height=15,;
    Caption="Importo provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="CMJIVKTHUM",Visible=.t., Left=35, Top=78,;
    Alignment=1, Width=129, Height=15,;
    Caption="% Provvigioni agente:"  ;
  , bGlobalFont=.t.

  add object oBox_4_5 as StdBox with uid="WQZFUUADIS",left=12, top=41, width=548,height=2
enddefine
define class tgsve_kdaPag5 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  resizeXpos=409
  resizeYpos=172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVDESSUP_5_2 as StdMemo with uid="LVMPQWUALM",rtseq=189,rtrep=.f.,;
    cFormVar = "w_MVDESSUP", cQueryName = "MVDESSUP",;
    bObbl = .f. , nPag = 5, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni aggiuntive di riga",;
    HelpContextID = 59730198,;
   bGlobalFont=.t.,;
    Height=316, Width=555, Left=3, Top=10

  func oMVDESSUP_5_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODDES='S')
    endwith
   endif
  endfunc

  add object oNUMDOC_5_3 as StdField with uid="LVRGSUQNBT",rtseq=190,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Riferimento numero ordine associato alla riga di acconto",;
    HelpContextID = 212928554,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=95, Top=334

  func oNUMDOC_5_3.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc

  add object oALFDOC_5_4 as StdField with uid="YARLTGYTBP",rtseq=191,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento alfa ordine associato alla riga di acconto",;
    HelpContextID = 212959738,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=225, Top=334, InputMask=replicate('X',10)

  func oALFDOC_5_4.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc

  add object oDATDOC_5_5 as StdField with uid="DYJJYKBYYR",rtseq=192,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento data ordine associato alla riga di acconto",;
    HelpContextID = 212905162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=341, Top=334

  func oDATDOC_5_5.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc


  add object oBtn_5_9 as StdButton with uid="NHALVYTZNW",left=513, top=334, width=48,height=45,;
    CpPicture="BMP\Visualiz.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per selezionare l'ordine al quale riferire l'acconto fatturato";
    , HelpContextID = 77232922;
    , tabstop=.f.,caption='\<Ordini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_9.Click()
      with this.Parent.oContained
        do GSVE_BAO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OKORD=.T.)
      endwith
    endif
  endfunc

  func oBtn_5_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OKORD=.F.)
     endwith
    endif
  endfunc

  add object oStr_5_6 as StdString with uid="FSFPEFDLTO",Visible=.t., Left=-1, Top=334,;
    Alignment=1, Width=94, Height=15,;
    Caption="Rif.ordine n.:"  ;
  , bGlobalFont=.t.

  func oStr_5_6.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc

  add object oStr_5_7 as StdString with uid="CXQMDVCXYF",Visible=.t., Left=309, Top=334,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_5_7.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc

  add object oStr_5_8 as StdString with uid="MQNUNSTNDX",Visible=.t., Left=217, Top=334,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_5_8.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MVRIFORD))
    endwith
  endfunc

  add object oStr_5_10 as StdString with uid="OGJOGTBCGM",Visible=.t., Left=374, Top=336,;
    Alignment=1, Width=133, Height=15,;
    Caption="Acconto da ordine:"  ;
  , bGlobalFont=.t.

  func oStr_5_10.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVRIFORD) OR .w_OKORD=.F.)
    endwith
  endfunc
enddefine
define class tgsve_kdaPag6 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCCESP_6_3 as StdField with uid="OAEPJJPSXV",rtseq=167,rtrep=.f.,;
    cFormVar = "w_DESCCESP", cQueryName = "DESCCESP",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192002170,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=252, Top=46, InputMask=replicate('X',40)

  func oDESCCESP_6_3.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oMVCODCES_6_7 as StdField with uid="GEFRLJPVDY",rtseq=171,rtrep=.f.,;
    cFormVar = "w_MVCODCES", cQueryName = "MVCODCES",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente obsoleto o di tipo non valido",;
    ToolTipText = "Codice cespite associato al servizio",;
    HelpContextID = 44652825,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=95, Top=46, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", oKey_1_1="CECODICE", oKey_1_2="this.w_MVCODCES"

  func oMVCODCES_6_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)
    endwith
   endif
  endfunc

  func oMVCODCES_6_7.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  func oMVCODCES_6_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCES_6_7.ecpDrop(oSource)
    this.Parent.oContained.link_6_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODCES_6_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oMVCODCES_6_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Cespiti",'GSVE_KCE.CES_PITI_VZM',this.parent.oContained
  endproc


  add object oObj_6_8 as cp_runprogram with uid="LKOJASGIOU",left=364, top=380, width=251,height=26,;
    caption='GSCE_BCZ(VALO)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Valo')",;
    cEvent = "Valorizzacespiti,Inizio",;
    nPag=6;
    , HelpContextID = 26197568


  add object oBtn_6_9 as StdButton with uid="YBQUNRLAGI",left=95, top=82, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per accedere al cespite";
    , HelpContextID = 123635809;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_9.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Aprc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVCODCES))
      endwith
    endif
  endfunc

  func oBtn_6_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
     endwith
    endif
  endfunc


  add object oBtn_6_10 as StdButton with uid="LKAVKCVGDO",left=150, top=82, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per caricare un nuovo cespite";
    , HelpContextID = 226281514;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_10.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Load")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (EMPTY(.w_MVCESSER) and (.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  ))
      endwith
    endif
  endfunc

  func oBtn_6_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N' OR .oparentobject.cfunction ='Query')
     endwith
    endif
  endfunc

  add object oMCNUMREG_6_11 as StdField with uid="EPHEKPDBXH",rtseq=172,rtrep=.f.,;
    cFormVar = "w_MCNUMREG", cQueryName = "MCNUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37746189,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=95, Top=187

  func oMCNUMREG_6_11.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oMCCODESE_6_12 as StdField with uid="NJKQZWFAIM",rtseq=173,rtrep=.f.,;
    cFormVar = "w_MCCODESE", cQueryName = "MCCODESE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 190233077,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=169, Top=187, InputMask=replicate('X',4)

  func oMCCODESE_6_12.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oCAUCES_6_14 as StdField with uid="OUVKFFNMTQ",rtseq=175,rtrep=.f.,;
    cFormVar = "w_CAUCES", cQueryName = "CAUCES",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Causale cespite associata alla causale contabile",;
    HelpContextID = 223452378,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=95, Top=159, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUCES"

  func oCAUCES_6_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ASSCES='M' and (.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES)
    endwith
   endif
  endfunc

  func oCAUCES_6_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUCES_6_14.ecpDrop(oSource)
    this.Parent.oContained.link_6_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUCES_6_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCAUCES_6_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oCAUCES_6_14.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUCES
     i_obj.ecpSave()
  endproc

  add object oMCDATREG_6_16 as StdField with uid="XPUMKALZSQ",rtseq=177,rtrep=.f.,;
    cFormVar = "w_MCDATREG", cQueryName = "MCDATREG",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 43734541,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=273, Top=187

  func oMCDATREG_6_16.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc


  add object oBtn_6_18 as StdButton with uid="RAMJUFWRYR",left=95, top=226, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per accedere al movimento cespite";
    , HelpContextID = 123635809;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_18.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Apri")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MCSERIAL))
      endwith
    endif
  endfunc

  func oBtn_6_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
     endwith
    endif
  endfunc


  add object oBtn_6_20 as StdButton with uid="STIXJUGQJA",left=355, top=187, width=48,height=45,;
    CpPicture="bmp\mcesp.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per associare un movimento cespite esistente al dettaglio documento";
    , HelpContextID = 145315542;
    , tabstop=.f.,caption='\<Mov.cesp.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_20.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Movi")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ASSCES='M' and (.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)
      endwith
    endif
  endfunc

  func oBtn_6_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N'  OR .oparentobject.cfunction ='Query')
     endwith
    endif
  endfunc


  add object oBtn_6_25 as StdButton with uid="MJZCZTTNPM",left=150, top=226, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per caricare un nuovo movimento cespite e associarlo al dettaglio documento";
    , HelpContextID = 226281514;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_25.Click()
      with this.Parent.oContained
        GSCE_BCZ(this.Parent.oContained,"Newm")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ASSCES='M' and (.oparentobject.cfunction ='Load' OR .oparentobject.cfunction ='Edit'  )  and .w_CESPRES=.T.)
      endwith
    endif
  endfunc

  func oBtn_6_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N' OR .oparentobject.cfunction ='Query')
     endwith
    endif
  endfunc

  add object oDESCAUCES_6_30 as StdField with uid="PCPVUHCJRW",rtseq=182,rtrep=.f.,;
    cFormVar = "w_DESCAUCES", cQueryName = "DESCAUCES",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 74337451,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=169, Top=159, InputMask=replicate('X',40)


  add object oObj_6_36 as cp_runprogram with uid="IPNLGZPYEB",left=-6, top=380, width=366,height=26,;
    caption='GSCE_BCZ(CAMB)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCZ('Camb')",;
    cEvent = "w_MVCODCES Changed,w_CAUCES Changed",;
    nPag=6;
    , HelpContextID = 12626752

  add object oStr_6_17 as StdString with uid="OBKXSGYWCQ",Visible=.t., Left=251, Top=187,;
    Alignment=0, Width=19, Height=18,;
    Caption="Del"  ;
  , bGlobalFont=.t.

  func oStr_6_17.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oStr_6_19 as StdString with uid="HNWIJIURFO",Visible=.t., Left=22, Top=47,;
    Alignment=1, Width=71, Height=18,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  func oStr_6_19.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oStr_6_21 as StdString with uid="RUTJMTYSGH",Visible=.t., Left=47, Top=190,;
    Alignment=1, Width=46, Height=18,;
    Caption="Reg.N.:"  ;
  , bGlobalFont=.t.

  func oStr_6_21.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oStr_6_26 as StdString with uid="XKTMBWMLAP",Visible=.t., Left=11, Top=18,;
    Alignment=0, Width=240, Height=18,;
    Caption="Cespite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_28 as StdString with uid="DMGMBEYNWC",Visible=.t., Left=11, Top=131,;
    Alignment=0, Width=130, Height=18,;
    Caption="Movimento cespite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_29 as StdString with uid="XQETIRZIIR",Visible=.t., Left=13, Top=159,;
    Alignment=1, Width=80, Height=18,;
    Caption="Caus.cespite:"  ;
  , bGlobalFont=.t.

  func oStr_6_29.mHide()
    with this.Parent.oContained
      return (.w_FLCESP<>'S' OR g_CESP<>'S' OR .w_ASSCES='N')
    endwith
  endfunc

  add object oBox_6_34 as StdBox with uid="UCRKBCSYHO",left=5, top=35, width=548,height=2

  add object oBox_6_37 as StdBox with uid="WBUOYFXKAR",left=6, top=148, width=548,height=2
enddefine
define class tgsve_kdaPag7 as StdContainer
  Width  = 572
  height = 379
  stdWidth  = 572
  stdheight = 379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDECOSINT_7_7 as StdField with uid="IJJSMUKXNA",rtseq=213,rtrep=.f.,;
    cFormVar = "w_DECOSINT", cQueryName = "DECOSINT",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107395190,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=123, Top=133, cSayPict="v_PV(38+VVL)", cGetPict="v_GU(38+VVL)"


  add object oDETIPPRA_7_8 as StdTableCombo with uid="HZAOCAJLUA",rtseq=214,rtrep=.f.,left=123,top=36,width=222,height=21;
    , ToolTipText = "Tipo pratica";
    , HelpContextID = 261859465;
    , cFormVar="w_DETIPPRA",tablefilter="", bObbl = .f. , nPag = 7;
    , cTable='PRA_TIPI',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.



  add object oDEMATPRA_7_9 as StdTableCombo with uid="VAITIZUKPV",rtseq=215,rtrep=.f.,left=123,top=67,width=222,height=21;
    , ToolTipText = "Materia pratica";
    , HelpContextID = 258218121;
    , cFormVar="w_DEMATPRA",tablefilter="", bObbl = .f. , nPag = 7;
    , cTable='PRA_MATE',cKey='MPCODICE',cValue='MPDESCRI',cOrderBy='MPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  add object oDEGAZUFF_7_10 as StdField with uid="QDDSQGKREX",rtseq=216,rtrep=.f.,;
    cFormVar = "w_DEGAZUFF", cQueryName = "DEGAZUFF",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 100370300,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=123, Top=99, InputMask=replicate('X',6)

  add object oDEPREMIN_7_11 as StdField with uid="VWTEBUFEXO",rtseq=217,rtrep=.f.,;
    cFormVar = "w_DEPREMIN", cQueryName = "DEPREMIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 213718916,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=123, Top=167, cSayPict="v_PV(38+VVU)"

  add object oDEPREMAX_7_12 as StdField with uid="VFEDTIVMCT",rtseq=218,rtrep=.f.,;
    cFormVar = "w_DEPREMAX", cQueryName = "DEPREMAX",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 54716530,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=123, Top=201, cSayPict="v_PV(38+VVU)"


  add object oDECALDIR_7_13 as StdCombo with uid="CEIVFSMUCC",rtseq=219,rtrep=.f.,left=123,top=235,width=139,height=21, enabled=.f.;
    , ToolTipText = "Criterio da utilizzare per calcolare i diritti e gli onorari";
    , HelpContextID = 68896648;
    , cFormVar="w_DECALDIR",RowSource=""+"% di sconto su min,"+"Applica il minimo,"+"Applica il medio,"+"Applica il massimo,"+"Coeff. di calcolo,"+"Raddoppia il max,"+"Quadruplica il max,"+"% di aumento su max", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oDECALDIR_7_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'M',;
    iif(this.value =4,'X',;
    iif(this.value =5,'C',;
    iif(this.value =6,'R',;
    iif(this.value =7,'Q',;
    iif(this.value =8,'A',;
    space(1))))))))))
  endfunc
  func oDECALDIR_7_13.GetRadio()
    this.Parent.oContained.w_DECALDIR = this.RadioValue()
    return .t.
  endfunc

  func oDECALDIR_7_13.SetRadio()
    this.Parent.oContained.w_DECALDIR=trim(this.Parent.oContained.w_DECALDIR)
    this.value = ;
      iif(this.Parent.oContained.w_DECALDIR=='S',1,;
      iif(this.Parent.oContained.w_DECALDIR=='I',2,;
      iif(this.Parent.oContained.w_DECALDIR=='M',3,;
      iif(this.Parent.oContained.w_DECALDIR=='X',4,;
      iif(this.Parent.oContained.w_DECALDIR=='C',5,;
      iif(this.Parent.oContained.w_DECALDIR=='R',6,;
      iif(this.Parent.oContained.w_DECALDIR=='Q',7,;
      iif(this.Parent.oContained.w_DECALDIR=='A',8,;
      0))))))))
  endfunc

  add object oDECOECAL_7_14 as StdField with uid="ZNNATBPDKE",rtseq=220,rtrep=.f.,;
    cFormVar = "w_DECOECAL", cQueryName = "DECOECAL",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Coefficiente di calcolo obbligatorio oppure minore di zero",;
    ToolTipText = "Coefficiente di calcolo, % di sconto, % di maggiorazione",;
    HelpContextID = 222738558,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=297, Top=235, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDECOECAL_7_14.mHide()
    with this.Parent.oContained
      return (.w_DECALDIR $ 'RQ')
    endwith
  endfunc

  add object oStr_7_1 as StdString with uid="UKZIEKHLUT",Visible=.t., Left=46, Top=36,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipo pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_7_2 as StdString with uid="LRXVTNELGP",Visible=.t., Left=30, Top=67,;
    Alignment=1, Width=83, Height=18,;
    Caption="Materia pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_7_3 as StdString with uid="JTDXDYRFDK",Visible=.t., Left=36, Top=133,;
    Alignment=1, Width=77, Height=18,;
    Caption="Costo interno:"  ;
  , bGlobalFont=.t.

  add object oStr_7_4 as StdString with uid="PKCWAZXXDN",Visible=.t., Left=17, Top=100,;
    Alignment=1, Width=96, Height=18,;
    Caption="Gazzetta Ufficiale:"  ;
  , bGlobalFont=.t.

  add object oStr_7_5 as StdString with uid="CHZGRFHTVQ",Visible=.t., Left=49, Top=167,;
    Alignment=1, Width=64, Height=18,;
    Caption="Tariffa min.:"  ;
  , bGlobalFont=.t.

  add object oStr_7_6 as StdString with uid="NIVTYAJTLJ",Visible=.t., Left=47, Top=201,;
    Alignment=1, Width=66, Height=18,;
    Caption="Tariffa max.:"  ;
  , bGlobalFont=.t.

  add object oStr_7_15 as StdString with uid="MWNOCVIENR",Visible=.t., Left=15, Top=235,;
    Alignment=0, Width=98, Height=18,;
    Caption="Criterio di calcolo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
