* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bef                                                        *
*              Generazione disco INTRA                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_821]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bef",oParentObject)
return(i_retval)

define class tgsar_bef as StdBatch
  * --- Local variables
  w_MESS = space(0)
  w_ImpGrStat = 0
  w_STATNAZ = 0
  Trovati = .f.
  Scritti = 0
  UltimaSez = space(2)
  w_UltimaSez1 = space(2)
  Handle = 0
  RecordComu = space(10)
  NumRiga = 0
  RecordSpec = space(10)
  SpazioLib = 0
  Errori = .f.
  UltimoTipo = space(2)
  w_PIvaDele = space(11)
  w_NUMDIS = 0
  w_NUMELE = 0
  w_MESS = space(30)
  w_AGGIPROG = .f.
  w_Sez1RigC = 0
  w_Sez1RigA = 0
  w_Sez1ImpC = 0
  w_Sez1ImpA = 0
  w_Sez2RigC = 0
  w_Sez2RigA = 0
  w_Sez2ImpC = 0
  w_Sez2ImpA = 0
  w_Sez3RigC = 0
  w_Sez3RigA = 0
  w_Sez3ImpC = 0
  w_Sez3ImpA = 0
  w_Sez4RigC = 0
  w_Sez4RigA = 0
  w_Sez4ImpC = 0
  w_Sez4ImpA = 0
  w_AGGIORNA = space(1)
  w_BLOCK = .f.
  w_CAMB = 0
  w_PIDATREG = ctod("  /  /  ")
  w_DECOR = 0
  w_NOMENC = space(8)
  w_UNIMIS = space(3)
  w_ARRSUP = 0
  w_NUMFATT = 0
  w_ALFAFATT = space(10)
  w_DATAFATT = ctod("  /  /  ")
  w_PIPERSER = space(6)
  w_PIMODINC = space(1)
  w_PIPAEPAG = space(2)
  w_PROG = 0
  w_PAEPAG = space(3)
  w_AGGCESS = 0
  w_AGGACQU = 0
  w_APPERIAC_S = 0
  w_APPERIAC = 0
  w_APPERICE = 0
  w_APPERICE_S = 0
  w_NProgEleA_Beni = 0
  w_NProgEleA_Serv = 0
  w_NProgEleC_Beni = 0
  w_NProgEleC_Serv = 0
  w_ProgEle = 0
  w_IMPNULL = .f.
  w_oERRORLOG = .NULL.
  Confermato = space(10)
  Gruppo = space(10)
  w_ANPARIVA = space(10)
  GruppoExpr = space(10)
  w_NAISOCLF = space(10)
  Record = 0
  w_NAISODES = space(10)
  w_PIIMPNAZ = 0
  w_NAISOPRO = space(2)
  w_PIIMPVAL = 0
  w_NAISOORI = space(2)
  w_PIMASNET = 0
  w_SPMODSPE = space(10)
  w_PIQTASUP = 0
  w_PICONDCO = space(1)
  w_PIVALSTA = 0
  w_TIPOMOV = space(2)
  w_PINOMENC = space(8)
  w_PINATTRA = space(3)
  w_TIPAC = space(1)
  w_TOTNAZ = 0
  w_IESELSTA = 0
  w_IESENAZ = 0
  w_Sez1Righe = 0
  w_Sez2Righe = 0
  w_Sez3Righe = 0
  w_Sez4Righe = 0
  w_Sez1Impor = 0
  w_Sez2Impor = 0
  w_Sez3Impor = 0
  w_Sez4Impor = 0
  w_Rigavalida = 0
  SaveReco = space(10)
  w_ImpoGrup = 0
  w_Sez2Impapp = space(10)
  w_PISEZDOG = space(6)
  w_PIANNREG = space(4)
  w_PIPRORET = space(6)
  w_PIPROGSE = space(5)
  OldDefault = space(10)
  NewDefault = space(10)
  w_CAS1 = space(1)
  w_CAS2 = space(1)
  w_CAS3 = space(1)
  w_CAS4 = space(1)
  w_CAS5 = space(1)
  w_PERAZI = space(1)
  w_MS = space(2)
  w_TR = space(1)
  w_STRACVE = space(2)
  w_YEART = space(2)
  w_YEAR = space(2)
  w_CODAZI = space(5)
  w_AZRAGAZI = space(40)
  w_TTCOGTIT = space(25)
  w_TTNOMTIT = space(25)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  NOMENCLA_idx=0
  SEDIAZIE_idx=0
  ELEIDETT_idx=0
  TITOLARI_idx=0
  AZDATINT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione elenchi intra su file (da GSAR_KEF)
    * --- 3) I dati devono essere divisi su piu' records nel seguente ordine:
    * --- 1) Un solo file per disco con nome SCAMBI.CEE
    * --- 2) File di tipo sequenziale che puo' contenere sia cessioni che acquisti
    * --- a) Un record contenente i dati del frontespizio dell'elenco
    * --- b) Un record per ciascuna riga dettaglio della sezione 1 (se presente)
    * --- c) Un record per ciascuna riga dettaglio della sezione 2 (se presente)
    * --- 4) I dati possono essere riportati su piu' dischi
    * --- 5) I records di dettaglio si differenziano a seconda della periodicit� di presentazione (mensile, trimestrale, annuale)
    * --- 6) I campi numerici devono essere riempiti con zeri nelle cifre non significative
    * --- Variabili della maschera
    * --- Variabili locali
    this.oParentObject.w_NProgEle = iif(this.oParentObject.w_NProgEle=0,1,this.oParentObject.w_NProgEle)
    this.Trovati = .f.
    this.Scritti = 0
    this.UltimaSez = "  "
    this.w_UltimaSez1 = "  "
    this.Handle = -1
    this.RecordComu = ""
    this.NumRiga = 0
    this.RecordSpec = ""
    this.SpazioLib = 0
    this.Errori = .f.
    this.UltimoTipo = "  "
    this.w_PIvaDele = space(11)
    this.w_NUMDIS = 1
    this.w_NUMELE = 0
    this.w_MESS = "Si desidera stampare l'etichetta?"
    this.w_AGGIPROG = .F.
    this.w_Sez1RigC = 0
    this.w_Sez1RigA = 0
    this.w_Sez1ImpC = 0
    this.w_Sez1ImpA = 0
    this.w_Sez2RigC = 0
    this.w_Sez2RigA = 0
    this.w_Sez2ImpC = 0
    this.w_Sez2ImpA = 0
    this.w_Sez3RigC = 0
    this.w_Sez3RigA = 0
    this.w_Sez3ImpC = 0
    this.w_Sez3ImpA = 0
    this.w_Sez4RigC = 0
    this.w_Sez4RigA = 0
    this.w_Sez4ImpC = 0
    this.w_Sez4ImpA = 0
    this.w_AGGIORNA = "N"
    this.w_BLOCK = .F.
    this.oParentObject.w_PIvaPres = LEFT(ALLTRIM(this.oParentObject.w_PIvaPres), 11)
    this.w_ARRSUP = IIF(this.oParentObject.w_VALUTA = g_CODEUR, 0, 0.00000001)
    this.w_NProgEleA_Beni = 0
    this.w_NProgEleA_Serv = 0
    this.w_NProgEleC_Beni = 0
    this.w_NProgEleC_Serv = 0
    this.w_ProgEle = 0
    * --- Controllo selezioni obbligatorie
    if (this.oParentObject.w_Acq_Serv="S" And empty(this.oParentObject.w_PeriAser)) Or (this.oParentObject.w_Acq_Beni="S" And empty(this.oParentObject.w_PeriAcqu)) Or (this.oParentObject.w_Cess_Beni="S" and empty(this.oParentObject.w_PeriCess)) Or (this.oParentObject.w_Cess_Serv="S" and empty(this.oParentObject.w_PeriCser))
      ah_errormsg("Specificare il periodo di rifermento",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CESS_BENI="S" and !((this.oParentObject.w_PeriCess<5 and this.oParentObject.w_TipoCess="T") or (this.oParentObject.w_PeriCess<13 and this.oParentObject.w_TipoCess="M"))
      ah_errormsg("Il periodo di riferimento delle cessioni di beni non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CESS_SERV="S" and !((this.oParentObject.w_PeriCser<5 and this.oParentObject.w_TipoCserv="T") or (this.oParentObject.w_PeriCser<13 and this.oParentObject.w_TipoCserv="M"))
      ah_errormsg("Il periodo di riferimento delle cessioni di servizi non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_ACQ_BENI="S" and !((this.oParentObject.w_PeriAcqu<5 and this.oParentObject.w_TipoAcqu="T") or (this.oParentObject.w_PeriAcqu<13 and this.oParentObject.w_TipoAcqu="M"))
      ah_errormsg("Il periodo di riferimento degli acquisti di beni non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_ACQ_SERV="S" and !((this.oParentObject.w_PeriAser<5 and this.oParentObject.w_TipoAserv="T") or (this.oParentObject.w_PeriAser<13 and this.oParentObject.w_TipoAserv="M"))
      ah_errormsg("Il periodo di riferimento degli acquisti di servizi non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Azinpece>=this.oParentObject.w_PeriCess AND this.oParentObject.w_Cess_Beni="S"
      if not ah_yesno("Periodo cessioni di beni gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_Azinpecs>=this.oParentObject.w_PeriCser AND this.oParentObject.w_Cess_Serv="S"
      if not ah_yesno("Periodo cessioni di servizi gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_Azinpeac>=this.oParentObject.w_Periacqu And this.oParentObject.w_Acq_Beni="S"
      if not ah_yesno("Periodo acquisti di beni gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_Azinpeas>=this.oParentObject.w_PeriAser And this.oParentObject.w_Acq_Serv="S"
      if not ah_yesno("Periodo acquisti di servizi gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- YYY Cursore per la stampa delle etichette dei dischetti
    create cursor ETICHETTE (PAIV C(11), DESO C(40), DOFI C(30), NUEL N(3,0), NUDI N(3,0), DITO N(3,0), PREL C(6), ULEL C(6))
    * --- Cursore per la stampa delle righe di dettaglio
    create cursor RIGDETT (PROG N(10), TIPOMOV C(2), STATO C(10), CODIVA C(12),IMPEUR N(18,4),IMPVAL N(18,4),DEC N(1),; 
 NATRA C(3),PERET N(2), ANNRET C(4),SEGNO C(1),NOMENCL C(8), MASSNET N(12,3), UNITSUP N(10),; 
 VALSTAT N(18,4), CODCONS C(1), MODTRAS C(1),PAEPRO C(2), PAEORI C(2), PAEDEST C(2),PROVORI C(2), PROVDES C(2), PITIPPER C(1),; 
 NUMFATT N(15,0),ALFAFATT C(10),DATAFATT D(8),PIPERSER C(1),PIMODINC C(1),PIPAEPAG C(2) ,PAEPAG C(3),; 
 PISEZDOG C(6),PIANNREG C(4),PIPRORET C(6),PIPROGSE C(6))
    * --- Creazione file e verifica supporto
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrittura file ascii
    if this.Handle > -1
      if empty(this.oParentObject.w_AZPIVAZI)
        ah_ErrorMsg("Partita IVA azienda non trovata",,"")
        this.Handle = fclose( this.Handle )
        ERASE (this.oParentObject.w_NomeFile)
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Soggetto delegato
      if this.oParentObject.w_SOGGDELE = "S"
        this.w_PIvaDele = this.oParentObject.w_AZAICFPI
        if empty(this.w_PIvaDele)
          ah_ErrorMsg("Partita IVA soggetto delegato non trovata",,"")
          this.Handle = fclose( this.Handle )
          ERASE (this.oParentObject.w_NomeFile)
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Crea un cursore temporaneo ordinato per tipo operazione (Ordine: AC - RA - CE -RE)
      vq_exec("query\GSAR_BEF",this,"EleIntra")
      * --- Le rettifiche nello stesso periodo sono da considerare Acquisti/Cessioni (se in meno, in negativo)
      SELECT IIF(PITIPMOV $ "AC-CE-AS-CS", LEFT(PITIPMOV, 1), IIF(PITIPMOV$"RC-RS", "C", "A")) AS SEZIONE, ; 
 iif( PITIPMOV$"AC-CE-RC-RA","1","2") as SEZIONE1,; 
 IIF(PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), IIF(PITIPMOV="RA", "AC", "CE"), PITIPMOV) AS PITIPMOV, ; 
 IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" )), "    ", PIANNRET) AS PIANNRET, ; 
 IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" )), 99*0, PIPERRET) AS PIPERRET, ; 
 ANPARIVA, PINATTRA, PINOMENC, IIF(PICONDCO="N", " ", PICONDCO) AS PICONDCO, PIMODTRA, NAISOPRO, NAISODES, NAISOORI, PIPROORI, PIPRODES, ; 
 PISERIAL, PIDATREG, PI__ANNO, PINUMREG, PIDATCOM, PIDATDOC, PINUMDOC, PIALFDOC, ; 
 PITIPCON, PICODCON, PIVALORI, PIVALNAZ, CPROWORD, PITIPRET, ; 
 PIIMPNAZ * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIIMPNAZ, ; 
 PIIMPVAL * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIIMPVAL, ; 
 PIMASNET * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIMASNET, ; 
 PIQTASUP * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIQTASUP, ; 
 PIVALSTA * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND ( PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIACQU, this.oParentObject.w_PERICESS) OR NVL(PITIPPER," ")="A" ), -1, 1) AS PIVALSTA, ; 
 PINAZDES, ANNAZION, NAISOCLF, SPMODSPE, SPCODSPE, ; 
 NACODNAZ, NACODNAZ1, NACODNAZ2, NACODNAZ3, NVL(PITIPPER, " ") AS PITIPPER,; 
 PIPERSER,PIMODINC,PIPAEPAG,DATAFATT,NUMFATT,ALFAFATT,VADECTOT,PAEPAG,; 
 PISEZDOG,PIANNREG,PIPRORET,PIPROGSE; 
 FROM EleIntra ; 
 INTO CURSOR EleIntra ORDER BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,29,53,54,56,55,49,50,48,45,46,47
      SELECT EleIntra
      GO TOP
      * --- Scrittura righe di dettaglio
      do while .not. eof()
        * --- Quando cambia la sezione (A o C) scrive la riga di frontespizio
        if Sezione<>this.UltimaSez Or (Sezione="C" And this.oParentObject.w_Rottura_Ces="S" And Sezione1<>this.w_UltimaSez1) Or (Sezione="A" And this.oParentObject.w_Rottura_Acq="S" And Sezione1<>this.w_UltimaSez1)
          if NOT EMPTY(ALLTRIM(this.UltimaSez))
            this.oParentObject.w_NProgEle = this.oParentObject.w_NProgEle + 1
          endif
          this.UltimaSez = Sezione
          this.w_UltimaSez1 = Sezione1
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Salvo i dati riassuntivi dell'elenco
          if this.UltimaSez="C"
            if this.oParentObject.w_Rottura_Ces="N" Or this.w_UltimaSez1="1"
              this.w_Sez1RigC = this.w_Sez1Righe
              this.w_Sez1ImpC = this.w_Sez1Impor
              this.w_Sez2RigC = this.w_Sez2Righe
              this.w_Sez2ImpC = this.w_Sez2Impor
            endif
            if this.oParentObject.w_Rottura_Ces="N" Or this.w_UltimaSez1="2"
              this.w_Sez3RigC = this.w_Sez3Righe
              this.w_Sez3ImpC = this.w_Sez3Impor
              this.w_Sez4RigC = this.w_Sez4Righe
              this.w_Sez4ImpC = this.w_Sez4Impor
            endif
            if this.oParentObject.w_Rottura_Ces="S"
              if this.w_Sez1Righe<>0 or this.w_Sez2Righe<>0 
                this.w_NProgEleC_Beni = this.oParentObject.w_NProgEle
              endif
              if this.w_Sez3Righe<>0 or this.w_Sez4Righe<>0
                this.w_NProgEleC_Serv = this.oParentObject.w_NProgEle
              endif
            else
              if this.w_Sez1Righe<>0 or this.w_Sez2Righe<>0 Or this.w_Sez3Righe<>0 or this.w_Sez4Righe<>0
                this.w_NProgEleC_Beni = this.oParentObject.w_NProgEle
              endif
            endif
          else
            if this.oParentObject.w_Rottura_Acq="N" Or this.w_UltimaSez1="1"
              this.w_Sez1RigA = this.w_Sez1Righe
              this.w_Sez1ImpA = this.w_Sez1Impor
              this.w_Sez2RigA = this.w_Sez2Righe
              this.w_Sez2ImpA = this.w_Sez2Impor
            endif
            if this.oParentObject.w_Rottura_Acq="N" Or this.w_UltimaSez1="2"
              this.w_Sez3RigA = this.w_Sez3Righe
              this.w_Sez3ImpA = this.w_Sez3Impor
              this.w_Sez4RigA = this.w_Sez4Righe
              this.w_Sez4ImpA = this.w_Sez4Impor
            endif
            if this.oParentObject.w_Rottura_Acq="S"
              if this.w_Sez1Righe<>0 Or this.w_Sez2Righe<>0
                this.w_NProgEleA_Beni = this.oParentObject.w_NProgEle
              endif
              if this.w_Sez3Righe<>0 Or this.w_Sez4Righe<>0
                this.w_NProgEleA_Serv = this.oParentObject.w_NProgEle
              endif
            else
              if this.w_Sez1Righe<>0 or this.w_Sez2Righe<>0 Or this.w_Sez3Righe<>0 or this.w_Sez4Righe<>0
                this.w_NProgEleA_Beni = this.oParentObject.w_NProgEle
              endif
            endif
          endif
        endif
        * --- Quando cambia il tipo movimento azzera il contatore di riga
        if PITIPMOV <> this.UltimoTipo
          this.UltimoTipo = PITIPMOV
          this.NumRiga = 0
        endif
        this.w_PIDATREG = PIDATREG
        this.w_DECOR = Nvl(VADECTOT,0)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Impostazione della variabile che indica se sono stati trovati dati da scrivere
        this.Trovati = .T.
        select EleIntra
        skip
      enddo
      this.Handle = fclose( this.Handle )
      SELECT RIGDETT
      if Reccount()=0
        AH_ERRORMSG("Non � possibile presentare righe a importo nullo",48)
        this.w_AGGIPROG = .T.
        this.w_IMPNULL = .T.
        this.Pag12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_AGGIORNA="S"
          this.oParentObject.w_PERICESS = this.oParentObject.w_PERICESS+1
          this.oParentObject.w_PERICSER = this.oParentObject.w_PERICSER+1
          this.oParentObject.w_PERIACQU = this.oParentObject.w_PERIACQU+1
          this.oParentObject.w_PERIASER = this.oParentObject.w_PERIASER+1
        endif
        ERASE (this.oParentObject.w_NomeFile)
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      if .not. this.Errori
        if this.Trovati
          if this.w_BLOCK
            ah_ErrorMsg("Generazione abortita",,"")
            ERASE (this.oParentObject.w_NomeFile)
          else
            ah_ErrorMsg("Generazione terminata",,"")
            this.w_AGGIPROG = .T.
          endif
        else
          ah_ErrorMsg("Non esistono dati da elaborare",,"")
          ERASE (this.oParentObject.w_NomeFile)
        endif
      else
        ah_ErrorMsg("Generazione incompleta. Ripetere l'operazione",,"")
      endif
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
      * --- Chiusura Cursore
      if used( "ERROR" )
        select ERROR
        use
      endif
      if used( "ELEINTRA" )
        select eleintra
        use
      endif
      if this.w_BLOCK
        this.oParentObject.w_NProgEle = IIF(this.oParentObject.w_AZNPRGEL=0,1,this.oParentObject.w_AZNPRGEL)
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Cursore stampa etichette
    select PAIV, DESO, DOFI, iif(this.oParentObject.w_Rottura_Ces="S",iif(this.w_Sez1RigC<>0 or this.w_Sez2RigC<>0,1,0)+iif(this.w_Sez3RigC<>0 or this.w_Sez4RigC<>0,1,0),iif(this.w_Sez1RigC<>0 or this.w_Sez2RigC<>0 or this.w_Sez3RigC<>0 or this.w_Sez4RigC<>0,1,0))+;
    iif(this.oParentObject.w_Rottura_Acq="S",iif(this.w_Sez1RigA<>0 OR this.w_Sez2RigA<>0,1,0)+iif(this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0,1,0),iif(this.w_Sez1RigA<>0 OR this.w_Sez2RigA<>0 or this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0,1,0)) as NUEL, NUDI,DITO,;
    iif(this.w_NProgEleA_Beni<>0,this.w_NProgEleA_Beni,iif(this.w_NProgEleA_Serv<>0,this.w_NProgEleA_Serv,iif(this.w_NProgEleC_Beni<>0,this.w_NProgEleC_Beni,this.w_NProgEleC_Serv))) as PREL, ;
    iif(this.w_NProgEleC_Serv<>0,this.w_NProgEleC_Serv,iif(this.w_NProgEleC_Beni<>0,this.w_NProgEleC_Beni,iif(this.w_NProgEleA_Serv<>0,this.w_NProgEleA_Serv,this.w_NProgEleA_Beni))) as ULEL ;
    from ETICHETTE group by PAIV,DESO,DOFI,NUDI,DITO order by NUDI into cursor __TMP__
    * --- Conta il numero dei dischetti
    select count(*) from __TMP__ into array L_DISTOT
    if this.Confermato AND this.Trovati
      this.w_MESS = "Si desidera stampare le etichette?"
      if ah_YesNo(this.w_MESS)AND L_DISTOT>0
        cp_chprn("QUERY\GSAR_ETI", " ", this)
      endif
      this.w_APPERIAC = this.oParentObject.w_PERIACQU
      this.w_APPERICE = this.oParentObject.w_PERICESS
      this.w_APPERICE_S = this.oParentObject.w_PERICSER
      this.w_APPERIAC_S = this.oParentObject.w_PERIASER
      this.Pag12()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Stampa Riepilogo Cessioni \ Acquisti
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_FrontCar="S"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_AGGIORNA="S"
      this.oParentObject.w_PERICESS = this.oParentObject.w_PERICESS+1
      this.oParentObject.w_PERICSER = this.oParentObject.w_PERICSER+1
      this.oParentObject.w_PERIACQU = this.oParentObject.w_PERIACQU+1
      this.oParentObject.w_PERIASER = this.oParentObject.w_PERIASER+1
    endif
    this.Page_11()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione File Ascii
    * --- Variabili
    this.Confermato = .T.
    * --- Richiesta azzeramento file esistente
    if file( this.oParentObject.w_NomeFile )
      if NOT ah_YesNo("File esistente! Ricreo?")
        this.Confermato = .F.
        this.w_NUMDIS = this.w_NUMDIS+1
      endif
    endif
    * --- Creazione file ascii
    if this.Confermato
      this.Handle = fcreate( this.oParentObject.w_NomeFile, 0 )
      if this.Handle < 0
        ah_ErrorMsg("Impossibile creare il file %1",,"", this.oParentObject.w_NomeFile )
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Dettaglio
    * --- Scrittura Frontespizio
    * --- Calcolo numero righe di dettaglio per la sezione corrente ed importo complessivo
    this.w_Sez1Righe = 0
    this.w_ImpoGrup = 0
    this.w_ImpGrStat = 0
    this.w_Sez2Righe = 0
    this.w_Sez2Impapp = 0
    this.w_Sez1Impor = 0
    this.w_Sez2Impor = 0
    this.w_Sez3Righe = 0
    this.w_Sez3Impor = 0
    this.w_Sez4Righe = 0
    this.w_Sez4Impor = 0
    this.w_Rigavalida = 0
    this.SaveReco = recno()
    * --- Calcolo il totale dei record in base al raggruppamento
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_Sez1Righe> 0 or this.w_Sez2Righe> 0 or this.w_Sez3Righe>0 or this.w_Sez4Righe>0
      * --- Composizione record (parte comune)
      * --- A seconda della scelta nella generazione (nella maschera) il primo campo del tracciato sar� EROA oppure INTRA
      if this.oParentObject.w_VALUTA = g_PERVAL
        this.RecordComu = "EUROX"
      else
        this.RecordComu = "INTRA"
      endif
      this.RecordComu = this.RecordComu+right( repl("0",11) + alltrim(this.oParentObject.w_PIvaPres) , 11)
      this.RecordComu = this.RecordComu+right( repl("0",6) + alltrim(str( this.oParentObject.w_NProgEle, 6, 0 )) , 6)
      * --- Tipo record (0=frontespiz. , 1=dettaglio sez.1 , 2=dettaglio sez.2)
      this.RecordComu = this.RecordComu+"0"
      * --- Numero progressivo riga dettaglio (0 per frontespizio)
      this.RecordComu = this.RecordComu+"00000"
      * --- Composizione record (parte specifica)
      * --- Tipo riepilogo
      this.RecordSpec = this.UltimaSez
      * --- Anno di riferimento
      this.RecordSpec = this.RecordSpec+right( this.oParentObject.w_AnnoRife , 2)
      * --- Periodicit�
      this.RecordSpec = this.RecordSpec+iif( this.UltimaSez="A" ,iif(this.w_UltimaSez1="1",this.oParentObject.w_TipoAcqu,this.oParentObject.w_TipoAserv),iif(this.w_UltimaSez1="1",this.oParentObject.w_TipoCess,this.oParentObject.w_TipoCserv))
      * --- Periodo
      this.RecordSpec = this.RecordSpec+right( "00" + alltrim(str( iif( this.UltimaSez="A",iif(this.w_UltimaSez1="1",this.oParentObject.w_PeriAcqu,this.oParentObject.w_PeriAser),iif(this.w_UltimaSez1="1",this.oParentObject.w_PeriCess,this.oParentObject.w_PericSer) ) , 2, 0 )) ,2)
      this.RecordSpec = this.RecordSpec+right( repl("0",11) + alltrim(this.oParentObject.w_AZPIVAZI) , 11)
      this.RecordSpec = this.RecordSpec+iif(this.UltimaSez="C" and this.w_UltimaSez1="1" And this.oParentObject.w_TipoCess="T",this.oParentObject.w_PrimPres,iif(this.UltimaSez="C" and this.w_UltimaSez1="2" And this.oParentObject.w_TipoCserv="T",this.oParentObject.w_PrimPres_CS,iif(this.UltimaSez="A" and this.w_UltimaSez1="1" And this.oParentObject.w_TipoAcqu="T",this.oParentObject.w_PrimPres_AB,iif(this.UltimaSez="A" and this.w_UltimaSez1="2" And this.oParentObject.w_TipoAserv="T",this.oParentObject.w_PrimPres_AS,"0"))))
      this.RecordSpec = this.RecordSpec+this.oParentObject.w_CessAtti
      this.RecordSpec = this.RecordSpec+right( repl("0",11) + alltrim(this.w_PIvaDele) , 11)
      this.RecordSpec = this.RecordSpec+right( repl("0",5) + alltrim(str( this.w_Sez1Righe, 5, 0 )) , 5)
      this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( this.w_Sez1Impor, 13, 0 )), 13)
      this.RecordSpec = this.RecordSpec+right( repl("0",5) + alltrim(str( this.w_Sez2Righe, 5, 0 )) , 5)
      * --- Se l'importo della sezione 2 � negativo bisogna introdurre un carattere di controllo.
      if this.w_Sez2Impor>=0
        this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( this.w_Sez2Impor, 13, 0 )), 13)
      else
        this.w_Sez2Impapp = right( repl("0",13) + alltrim(str( abs(this.w_Sez2Impor), 13, 0 )), 13)
        this.w_Sez2Impapp = left(this.w_Sez2Impapp,12)+chr(112+val(right(this.w_Sez2Impapp,1)))
        this.RecordSpec = this.RecordSpec+this.w_Sez2Impapp
      endif
      this.RecordSpec = this.RecordSpec+right( repl("0",5) + alltrim(str( this.w_Sez3Righe, 5, 0 )) , 5)
      this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( this.w_Sez3Impor, 13, 0 )), 13)
      this.RecordSpec = this.RecordSpec+right( repl("0",5) + alltrim(str( this.w_Sez4Righe, 5, 0 )) , 5)
      this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( this.w_Sez4Impor, 13, 0 )), 13)
      * --- Scrittura
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Scritti = fputs( this.Handle, this.RecordComu + this.RecordSpec )
      * --- Aggiorna Cursore per stampa etichette
      this.w_NUMELE = this.w_NUMELE+1
      insert into ETICHETTE values (substr(this.RecordComu,6,11), IIF(this.oParentObject.w_SOGGDELE="S", this.oParentObject.w_AZAIRAGS, g_ragazi), IIF(this.oParentObject.w_SOGGDELE="S", this.oParentObject.w_AZAICITT, this.oParentObject.w_AZLOCALI), ; 
 this.w_NUMELE, this.w_NUMDIS,0, substr(this.RecordComu,24,5), substr(this.RecordComu,24,5))
      * --- Verifica
      if this.Scritti <> len(this.RecordComu + this.RecordSpec)+2
        ah_ErrorMsg("ATTENZIONE: Verificare record frontespizio sezione %1",,"", this.UltimaSez)
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Dettaglio
    * --- Gestione dei campi che possono assumere il valore NULL
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISOCLF = iif( isnull( NAISOCLF ), space(2) , alltrim(NAISOCLF) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
    this.w_SPMODSPE = iif( isnull( SPMODSPE ), space(2) , alltrim(SPMODSPE) )
    this.w_PICONDCO = IIF(EMPTY(NVL(PICONDCO," ")) OR PICONDCO="N", SPACE(1), ALLTRIM(PICONDCO))
    * --- Raggruppamento righe
    * --- Non � stato gestito a livello di query perch�:
    * --- 1) Varia in base alla periodicit� di presentazione degli elenchi.
    * --- 2) Un'azienda pu� avere periodicit� differenti fra acquisti e cessioni.
    * --- 3) Le rettifiche devono essere raggruppate
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_PIIMPNAZ <> 0 or this.w_PIVALSTA<>0 or (this.w_RIGAVALIDA>0 And PITIPMOV $ "RX-RS")
      this.NumRiga = this.NumRiga + 1
      * --- Composizione record (parte comune)
      if this.oParentObject.w_VALUTA = g_PERVAL
        this.RecordComu = "EUROX"
      else
        this.RecordComu = "INTRA"
      endif
      this.RecordComu = this.RecordComu+right( repl("0",11) + alltrim(this.oParentObject.w_PIvaPres) , 11)
      this.RecordComu = this.RecordComu+right( repl("0",6) + alltrim(str( this.oParentObject.w_NProgEle, 6, 0 )) , 6)
      * --- Tipo record (0=frontespiz. , 1=dettaglio sez.1 , 2=dettaglio sez.2)
      this.RecordComu = this.RecordComu+iif(PITIPMOV $ "CE-AC","1",iif(PITIPMOV $ "RC-RA","2",iif(PITIPMOV $ "CS-AS","3","4")))
      this.w_TIPOMOV = NVL(PITIPMOV,SPACE(2))
      * --- Numero progressivo riga dettaglio (0 per frontespizio)
      this.RecordComu = this.RecordComu+right( repl("0",5) + alltrim(str(this.NumRiga,5,0)) , 5)
      * --- Nel caso di rettifiche di servizi se l'ammontare delle operazioni in Euro
      *     � uguale a zero non effettuo il controllo sulla partita IVA
      if NOT(PITIPMOV $ "RX-RS" And this.w_PIIMPNAZ=0)
        if empty(this.w_ANPARIVA)
          if NVL(PITIPCON,"")="C"
            this.w_oERRORLOG.AddMsgLog("Partita IVA cliente %1 non trovata", alltrim(NVL(PICODCON,"")) )     
          else
            this.w_oERRORLOG.AddMsgLog("Partita IVA fornitore %1 non trovata", alltrim(NVL(PICODCON,"")) )     
          endif
          SELECT EleIntra
          this.w_BLOCK = .T.
        endif
        * --- Se il codice ISO della nazione del cliente/fornitore non � stato inserito viene segnalato l'errore all'utente
        *     non generando niente
        if empty(this.w_NAISOCLF)
          if NVL(PITIPCON,"")="C"
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO cliente non trovato",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
          else
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO fornitore non trovato",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
          endif
          SELECT EleIntra
          this.w_BLOCK = .T.
        endif
      endif
      if !empty (NVL(PINOMENC,SPACE(8))) and ((this.oParentObject.w_TIPOACQU = "M" and PITIPMOV = "AC") or (this.oParentObject.w_TIPOCESS = "M" and PITIPMOV = "CE")) AND this.w_NAISOCLF <>"SM"
        this.w_NOMENC = NVL(PINOMENC,SPACE(8))
        * --- Read from NOMENCLA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NOMENCLA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2],.t.,this.NOMENCLA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NMUNISUP"+;
            " from "+i_cTable+" NOMENCLA where ";
                +"NMCODICE = "+cp_ToStrODBC(this.w_NOMENC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NMUNISUP;
            from (i_cTable) where;
                NMCODICE = this.w_NOMENC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNIMIS = NVL(cp_ToDate(_read_.NMUNISUP),cp_NullValue(_read_.NMUNISUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PIQTASUP=0 and !EMPTY(this.w_UNIMIS) and this.w_UNIMIS<>"ZZZ" and not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
          this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: quantit� relativa all'unit� supplementare uguale a zero (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
          SELECT EleIntra
        endif
        if this.w_PIMASNET=0 And (EMPTY(this.w_UNIMIS) OR this.w_UNIMIS="ZZZ") And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
          this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: quantit� relativa alla massa netta uguale a zero (solo warning)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
          SELECT EleIntra
        endif
      endif
      * --- Composizione record (parte specifica)
      this.RecordSpec = ""
      do case
        case PITIPMOV $ "RC-RA"
          do case
            case PITIPPER="M"
              * --- Rettifiche Acquisti/Cessioni Mensili
              this.RecordSpec = this.RecordSpec+right( repl("0",2) + alltrim( str(NVL(PIPERRET,0),2,0) ) , 2)
              this.RecordSpec = this.RecordSpec+"0"
              this.RecordSpec = this.RecordSpec+right( repl("0",2) + alltrim( right(NVL(PIANNRET,SPACE(4)),2) ) , 2)
            case PITIPPER="T"
              * --- Rettifiche Acquisti/Cessioni trimestrali
              this.RecordSpec = this.RecordSpec+"00"
              this.RecordSpec = this.RecordSpec+right("0" + alltrim(str(NVL(PIPERRET,0))), 1)
              this.RecordSpec = this.RecordSpec+right( repl("0",2) + alltrim( right(NVL(PIANNRET,SPACE(4)),2) ) , 2)
            case PITIPPER="A"
              * --- Rettifiche Acquisti/Cessioni Annuali
              this.RecordSpec = this.RecordSpec+"00"
              this.RecordSpec = this.RecordSpec+"0"
              this.RecordSpec = this.RecordSpec+right( repl("0",2) + alltrim( right(NVL(PIANNRET,SPACE(4)),2) ) , 2)
          endcase
        case PITIPMOV$"RS-RX"
          * --- Nel caso di rettifiche di servizi controllo che non sia vuota la sezione doganale,
          *     l'anno di registrazione,il protocollo della dichiarazione ed il progressivo della sezione 3
          if EMPTY(PISEZDOG)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: sezione doganale mancante", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            this.w_BLOCK = .T.
          endif
          if EMPTY(PIANNREG)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: anno di registrazione mancante", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            this.w_BLOCK = .T.
          endif
          if EMPTY(PIPRORET)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: protocollo della dichiarazione mancante", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            this.w_BLOCK = .T.
          endif
          if EMPTY(PIPROGSE)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: progressivo della sezione 3 mancante", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            this.w_BLOCK = .T.
          endif
          * --- Sezione doganale in cui � stata registrata la dichiarazione da rettificare
          this.RecordSpec = this.RecordSpec+right( repl("0",6) + alltrim(PISEZDOG) ,6)
          this.w_PISEZDOG = right( repl("0",6) + alltrim(PISEZDOG) ,6)
          * --- Anno di registrazione della dichiarazione da rettificare
          this.RecordSpec = this.RecordSpec+Right(Piannreg,2)
          this.w_PIANNREG = PIANNREG
          * --- Protocollo della dichiarazione da rettificare
          this.RecordSpec = this.RecordSpec+right( repl("0",6) + alltrim(PIPRORET) ,6)
          this.w_PIPRORET = right( repl("0",6) + alltrim(PIPRORET) ,6)
          * --- Progressivo della sezione 3 da rettificare
          this.RecordSpec = this.RecordSpec+right( repl("0",5) + alltrim(PIPROGSE) ,5)
          this.w_PIPROGSE = right( repl("0",5) + alltrim(PIPROGSE) ,5)
      endcase
      * --- Nel caso di rettifica di servizi se l'ammontare delle operazioni in euro � uguale a zero non
      *     devo scrivere il codice dello stato membro ed il codice IVA
      if PITIPMOV $ "RS-RX" And this.w_PIIMPNAZ=0
        this.RecordSpec = this.RecordSpec+space(2)+SPACE(12)
      else
        this.RecordSpec = this.RecordSpec+right( space(2)+NVL(this.w_NAISOCLF,SPACE(2)) , 2)
        this.RecordSpec = this.RecordSpec+right(this.w_ANPARIVA + SPACE(12-len(this.w_ANPARIVA)), 12)
      endif
      * --- Dichiarazione Mensile, Trimestrale, Annuale e Rettifiche
      * --- Rettifiche
      if PITIPMOV $ "RC-RA"
        this.RecordSpec = this.RecordSpec+iif(EMPTY(NVL(PITIPRET," ")),"+",PITIPRET)
      endif
      if this.oParentObject.w_VALUTA=g_PERVAL
        this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str(cp_ROUND(iif(cp_round(this.w_PIIMPNAZ,0)=0 And this.w_PIIMPNAZ<>0,1,this.w_PIIMPNAZ),0),13)),13)
      else
        this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( cp_round((this.w_PIIMPNAZ/1000)-this.w_ARRSUP,0),13,0)) , 13)
      endif
      * --- Acquisti e Rettifiche Acquisti
      if this.UltimaSez = "A" 
        if PITIPMOV $ "RS-RX" And this.w_PIIMPNAZ=0
          this.RecordSpec = this.RecordSpec+repl("0",13)
        else
          this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str(cp_ROUND(iif(cp_round(this.w_PIIMPVAL,0)=0 And this.w_PIIMPVAL<>0,1,this.w_PIIMPVAL),0),13)),13)
        endif
      endif
      do case
        case PITIPMOV $ "RC-RA-AC-CE"
          this.RecordSpec = this.RecordSpec+right( " " + alltrim( Alltrim(Nvl(PINATTRA,SPACE(3)))) , 1)
          this.w_PINATTRA = alltrim( Alltrim(Nvl(PINATTRA,SPACE(3))))
          * --- Controllo su Natura Transazione
          if EMPTY(this.w_PINATTRA)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: natura della transazione mancante (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            SELECT EleIntra
          endif
          * --- Se il periodo � annuale la nomenclatura non viene riportata (il campo � ripempito con 8 zeri)
          this.w_PINOMENC = alltrim( NVL(PINOMENC,SPACE(8)))
          * --- Controllo su Nomenclatura
          if EMPTY(this.w_PINOMENC)
            this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: nomenclatura mancante (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
            SELECT EleIntra
          endif
          this.RecordSpec = this.RecordSpec+right( repl("0",8) + alltrim( NVL(PINOMENC,SPACE(8))) , 8)
          * --- Se inferiori all'unita' mette 1
          if this.w_NAISOCLF <>"SM"
            * --- Non nazione SANMARINO
            this.w_PIMASNET = IIF(cp_ROUND(this.w_PIMASNET, 0)<1 AND this.w_PIMASNET<>0, 1, cp_ROUND(this.w_PIMASNET, 0))
            this.w_PIQTASUP = IIF(cp_ROUND(this.w_PIQTASUP, 0)<1 And this.w_PIQTASUP<>0, 1, cp_ROUND(this.w_PIQTASUP, 0))
          endif
          * --- Dichiarazione Mensile
          if (this.UltimaSez="C" AND this.oParentObject.w_TipoCess="M") OR (this.UltimaSez="A" AND this.oParentObject.w_TipoAcqu="M")
            if PITIPMOV $ "AC-CE"
              * --- Nel caso di "Cessione oppure Acquisti" di beni scrivo la massa netta  e
              *     l'unit� supplementare
              this.RecordSpec = this.RecordSpec+right( repl("0",10) + alltrim(str( cp_round(this.w_PIMASNET, 0) ,10,0)) , 10)
              this.RecordSpec = this.RecordSpec+right( repl("0",10) + alltrim(str( cp_round(this.w_PIQTASUP, 0) ,10,0)) , 10)
            endif
            * --- Valore Statistico : soltanto messaggio di warning
            if this.w_PIVALSTA=0 AND ((this.UltimaSez="C" AND this.oParentObject.w_DTOBBL $ "CE") OR (this.UltimaSez="A" AND this.oParentObject.w_DTOBBL $ "AE")) AND this.w_NAISOCLF <>"SM" And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
              this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: valore statistico uguale a zero (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
              SELECT EleIntra
            endif
            if this.oParentObject.w_VALUTA=g_PERVAL
              this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str(cp_ROUND(iif(cp_round(this.w_PIVALSTA,0)=0 And this.w_PIVALSTA<>0,1,this.w_PIVALSTA),0),13)),13)
            else
              this.RecordSpec = this.RecordSpec+right( repl("0",13) + alltrim(str( cp_round((this.w_PIVALSTA/1000)-this.w_ARRSUP,0),13,0)) , 13)
            endif
            if PITIPMOV $ "AC-CE"
              * --- Nel caso di "Cessione oppure Acquisti" di beni controllo la valorizzazione
              *     delle condizioni di consegna
              if EMPTY(NVL(this.w_PICONDCO,"")) and ((this.UltimaSez="C" and this.oParentObject.w_DTOBBL $ "CE") or (this.UltimaSez="A" and this.oParentObject.w_DTOBBL $ "AE")) AND this.w_NAISOCLF <>"SM" And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice condizione di consegna non trovato (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
                SELECT EleIntra
              endif
              if this.w_NAISOCLF <>"SM"
                this.RecordSpec = this.RecordSpec + this.w_PICONDCO
              else
                this.RecordSpec = this.RecordSpec + SPACE(1)
              endif
              if empty(this.w_SPMODSPE) and ((this.UltimaSez="C" and this.oParentObject.w_DTOBBL $ "CE") or (this.UltimaSez="A" and this.oParentObject.w_DTOBBL $ "AE")) AND this.w_NAISOCLF <>"SM" And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice modalit� di trasporto non trovato (solo warning)", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
                SELECT EleIntra
              endif
              this.RecordSpec = this.RecordSpec+right( "0"+alltrim(this.w_SPMODSPE), 1)
              * --- Acquisti e Rettifiche Acquisti
              if this.w_NAISOCLF <>"SM"
                if this.UltimaSez = "A"
                  * --- Controllo dati obbligatori
                  if empty(this.w_NAISOPRO) And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                    this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO del paese di provenienza non trovato (solo warning)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
                    SELECT EleIntra
                  endif
                  if empty(this.w_NAISOORI) And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                    this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO del paese di origine non trovato (solo warnig)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
                    SELECT EleIntra
                  endif
                  if EMPTY(NVL(PIPRODES,SPACE(2))) And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                    this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: provincia di destinazione non trovata (solo warnig)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
                    SELECT EleIntra
                  endif
                  this.RecordSpec = this.RecordSpec+left(NVL(this.w_NAISOPRO,SPACE(2))+space(2), 2)
                  this.RecordSpec = this.RecordSpec+left(NVL(this.w_NAISOORI,SPACE(2))+space(2), 2)
                  this.RecordSpec = this.RecordSpec+left( NVL(PIPRODES,SPACE(2))+space(2), 2)
                else
                  if empty(this.w_NAISODES) And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                    this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO del paese di destinazione non trovato (solo warning)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
                    SELECT EleIntra
                  endif
                  if EMPTY(NVL(PIPROORI,SPACE(2))) And not Alltrim(Nvl(PINATTRA,SPACE(3))) $ "ABCDEFGHI"
                    this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: provincia di origine non trovata (solo warnig)",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
                    SELECT EleIntra
                  endif
                  this.RecordSpec = this.RecordSpec+left(NVL(this.w_NAISODES,SPACE(2))+space(2), 2)
                  this.RecordSpec = this.RecordSpec+left( NVL(PIPROORI,SPACE(2))+space(2), 2)
                endif
              else
                if this.UltimaSez = "A"
                  this.RecordSpec = this.RecordSpec+space(6)
                else
                  this.RecordSpec = this.RecordSpec+space(4)
                endif
              endif
            endif
          endif
          * --- Per evitare errori nella insert del cursore Rigdett assegno alla variabile
          *     w_Datafatt una data vuota
          this.w_DATAFATT = cp_CharToDate("  -  -  ")
        case PITIPMOV $ "AS-CS-RS-RX" 
          * --- Nel caso di rettifica di servizi se l'importo in valuta � uguale a zero non
          *     devo scrivere l'ammontare delle operazioni in valuta
          if PITIPMOV $ "RS-RX" And this.w_PIIMPNAZ=0
            this.RecordSpec = this.RecordSpec+space(15)+space(6)+repl("0",6)+space(1)+space(1)+space(2)
            this.w_NUMFATT = 0
            this.w_ALFAFATT = space(10)
            this.w_DATAFATT = cp_CharToDate("  -  -  ")
            this.w_PINOMENC = SPACE(6)
            this.w_PIPERSER = space(1)
            this.w_PIMODINC = space(1)
            this.w_PAEPAG = space(3)
            this.w_PIPAEPAG = space(2)
          else
            this.RecordSpec = this.RecordSpec+iif(NUMFATT=0,space(15),LEFT (ALLTRIM(STR(NUMFATT,15))+ALLTRIM(ALFAFATT)+SPACE(15),15) )
            this.w_NUMFATT = NUMFATT
            this.w_ALFAFATT = ALFAFATT
            this.RecordSpec = this.RecordSpec+DATAFATT
            this.w_DATAFATT = IIF(this.oParentObject.w_TIPOGENESERVIZI="C",CP_TODATE(PIDATDOC),cp_CharToDate("  -  -  "))
            this.RecordSpec = this.RecordSpec+LEFT(alltrim( NVL(PINOMENC,SPACE(6))) + repl("0",6),6)
            this.w_PINOMENC = alltrim( NVL(PINOMENC,SPACE(6)))
            this.RecordSpec = this.RecordSpec+left( PIPERSER+space(1), 1)
            this.w_PIPERSER = PIPERSER
            this.RecordSpec = this.RecordSpec+left( PIMODINC+space(1), 1)
            this.w_PIMODINC = PIMODINC
            this.w_PAEPAG = PAEPAG
            * --- Il controllo sul codice servizio non deve essere effettuato se la tipologia operazione
            *     � uguale a rettifiche di servizi e l'ammontare delle operazioni in Euro
            *     � uguale a zero
            if EMPTY(this.w_PINOMENC)
              this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice servizio mancante", ltrim(str(NVL(PINUMREG,0),6,0)), dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))) )     
              this.w_BLOCK = .T.
            endif
            if empty(this.w_PIPAEPAG)
              this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO del paese di pagamento non trovato",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
              this.w_BLOCK = .T.
              SELECT EleIntra
            endif
            this.RecordSpec = this.RecordSpec+left(this.w_PIPAEPAG+space(2), 2)
          endif
      endcase
      if this.oParentObject.w_VALUTA = g_PERVAL
        this.w_PIIMPNAZ = cp_Round(iif(cp_round(this.w_PIIMPNAZ,0)=0 And this.w_PIIMPNAZ<>0,1,this.w_PIIMPNAZ),0)
        this.w_PIIMPVAL = cp_Round(iif(cp_round(this.w_PIIMPVAL,0)=0 And this.w_PIIMPVAL<>0,1,this.w_PIIMPVAL),0)
        this.w_PIVALSTA = cp_Round(iif(cp_round(this.w_PIVALSTA,0)=0 And this.w_PIVALSTA<>0,1,this.w_PIVALSTA),0)
        this.w_PIMASNET = cp_Round(iif(cp_round(this.w_PIMASNET,0)=0 And this.w_PIMASNET<>0,1,this.w_PIMASNET),0)
        this.w_PIQTASUP = cp_Round(iif(cp_round(this.w_PIQTASUP,0)=0 And this.w_PIQTASUP<>0,1,this.w_PIQTASUP),0)
      endif
      * --- Carico cursore per la stampa delle righe di dettaglio
      insert into RIGDETT values (this.NumRiga,this.w_TIPOMOV,this.w_NAISOCLF,this.w_ANPARIVA,this.w_PIIMPNAZ,this.w_PIIMPVAL,; 
 this.w_DECOR,this.w_PINATTRA,NVL(ELEINTRA.PIPERRET,0),; 
 NVL(ELEINTRA.PIANNRET," "),NVL(ELEINTRA.PITIPRET,"+"),this.w_PINOMENC,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA,; 
 this.w_PICONDCO,this.w_SPMODSPE,this.w_NAISOPRO,this.w_NAISOORI,this.w_NAISODES,; 
 NVL(ELEINTRA.PIPROORI,SPACE(2)),NVL(ELEINTRA.PIPRODES,SPACE(2)), NVL(ELEINTRA.PITIPPER," "),; 
 this.w_NUMFATT,this.w_ALFAFATT,this.w_DATAFATT,this.w_PIPERSER,this.w_PIMODINC,this.w_PIPAEPAG,this.w_PAEPAG,; 
 this.w_PISEZDOG,this.w_PIANNREG,this.w_PIPRORET,this.w_PIPROGSE)
      * --- Scrittura
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Scritti = fputs( this.Handle, this.RecordComu + this.RecordSpec )
      * --- Aggiorna Cursore per stampa etichette
      this.w_NUMELE = this.w_NUMELE+1
      insert into ETICHETTE values (substr(this.RecordComu,6,11), IIF(this.oParentObject.w_SOGGDELE="S", this.oParentObject.w_AZAIRAGS, g_ragazi), IIF(this.oParentObject.w_SOGGDELE="S", this.oParentObject.w_AZAICITT, this.oParentObject.w_AZLOCALI), ;
      this.w_NUMELE, this.w_NUMDIS,0, substr(this.RecordComu,24,5), substr(this.RecordComu,24,5))
      * --- Verifica
      if this.Scritti <> len(this.RecordComu + this.RecordSpec)+2
        ah_ErrorMsg("ATTENZIONE: Verificare record dettaglio sezione %1 numero %2",,"", this.UltimaSez, str(this.NumRiga,5,0) )
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica spazio su disco
    * --- Il controllo dello spazio libero viene realizzato solo se l'unita' e' stata mappata.
    if substr(this.oParentObject.w_NomeFile,2,1)=":"
      * --- Memorizza la posizione corrente
      this.OldDefault = sys(5)+sys(2003)
      * --- Si posiziona sul disco di destinazione
      this.NewDefault = left(this.oParentObject.w_NomeFile,2)
      set default to (this.NewDefault)
      this.SpazioLib = diskspace()
      * --- Cicla fino a quando viene selezionato un device con spazio sufficiente
      this.SpazioLib = 0
      do while this.SpazioLib <= 10000
        * --- Legge lo spazio libero
        this.SpazioLib = diskspace()
        * --- Richiede la selezione di un nuovo disco
        if this.SpazioLib <= 10000
          * --- Chiusura File Ascii su disco corrente
          this.Handle = fclose( this.Handle )
          * --- Richiesta nuovo disco
          if .not.ah_YesNo("Spazio su disco esaurito%0Inserire un nuovo disco formattato%0%0Proseguo?")
            this.Errori = .T.
            i_retcode = 'stop'
            return
          else
            * --- Creazione File Ascii sul nuovo disco
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      enddo
      * --- Torna nella posizione di partenza
      set default to (this.OldDefault)
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do while (.not. eof()) .and. this.UltimaSez=Sezione And ( (this.UltimaSez="C" and ((this.oParentObject.w_Rottura_Ces="S" And this.w_UltimaSez1=Sezione1) Or this.oParentObject.w_Rottura_Ces="N") ) Or (this.UltimaSez="A" and ((this.oParentObject.w_Rottura_Acq="S" And this.w_UltimaSez1=Sezione1) Or this.oParentObject.w_Rottura_Acq="N")))
      this.w_TOTNAZ = 0
      this.w_STATNAZ = 0
      this.Gruppo = ""
      this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
      this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
      this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
      this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
      this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
      this.Page_10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Raggruppamento
      this.Record = recno()
      this.w_TIPAC = PITIPMOV
      this.w_ImpoGrup = 0
      this.w_ImpGrStat = 0
      this.w_Rigavalida = 0
      do while (.not. eof()) .and. this.Gruppo = eval( this.GruppoExpr ) and this.w_TIPAC=PITIPMOV
        * --- Totalizza i dati del record corrente
        this.Record = recno()
        if this.oParentObject.w_VALUTA <> PIVALNAZ and this.oParentObject.w_VALUTA=g_PERVAL
          this.w_TOTNAZ = VAL2MON(NVL(PIIMPNAZ,0), GETCAM(PIVALNAZ,this.w_PIDATREG), 1,this.w_PIDATREG,this.oParentObject.w_VALUTA,GETVALUT(this.oParentObject.w_VALUTA,"VADECTOT"))
          this.w_STATNAZ = VAL2MON(NVL(PIVALSTA,0),GETCAM(PIVALNAZ,this.w_PIDATREG), 1,this.w_PIDATREG,this.oParentObject.w_VALUTA,GETVALUT(this.oParentObject.w_VALUTA,"VADECTOT"))
          this.w_ImpoGrup = this.w_ImpoGrup + iif(nvl(PITIPRET,"+")="-" And PITIPMOV $ "RC-RA", -1 * this.w_TOTNAZ, this.w_TOTNAZ)
          this.w_ImpGrStat = this.w_ImpGrStat + iif(nvl(PITIPRET,"+")="-" And PITIPMOV $ "RC-RA", -1 * this.w_STATNAZ, this.w_STATNAZ)
        else
          this.w_ImpoGrup = this.w_ImpoGrup + iif(nvl(PITIPRET,"+")="-" And PITIPMOV $ "RC-RA", -1 * NVL(PIIMPNAZ,0), NVL(PIIMPNAZ,0))
          this.w_ImpGrStat = this.w_ImpGrStat + iif(nvl(PITIPRET,"+")="-" And PITIPMOV $ "RC-RA", -1 * NVL(PIVALSTA,0), NVL(PIVALSTA,0))
        endif
        * --- Nel caso di rettifica di servizi considero solo le righe dove � presente la 
        *     sezione doganale,l'anno di registrazione,il protocollo della dichiarazione e
        *     il progressivo della sezione 3
        if PITIPMOV $ "RS-RX" 
          this.w_Rigavalida = this.w_Rigavalida+IIF(NOT EMPTY(PISEZDOG) AND NOT EMPTY(PIANNREG) AND NOT EMPTY(PIPROGSE) AND NOT EMPTY(PIPRORET),1,0)
        endif
        * --- Passa al record successivo
        skip
        this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
        this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
        this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
        this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
        this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
      enddo
      goto this.Record
      do case
        case PITIPMOV $ "CE-AC"
          this.w_Sez1Righe = IIF(this.w_ImpoGrup<>0 or this.w_ImpGrStat<>0,this.w_Sez1Righe + 1,this.w_Sez1Righe)
          this.w_Sez1Impor = IIF(this.oParentObject.w_Valuta=g_Codeur,this.w_Sez1Impor + iif(cp_round(NVL(this.w_ImpoGrup ,0),0)=0 And this.w_Impogrup<>0,1,cp_round(NVL(this.w_ImpoGrup ,0),0)),this.w_Sez1Impor + cp_round((NVL(this.w_ImpoGrup ,0)/1000)-this.w_ARRSUP,0))
        case PITIPMOV $ "RC-RA" 
          this.w_Sez2Righe = IIF(this.w_ImpoGrup<>0 or this.w_ImpGrStat<>0,this.w_Sez2Righe + 1,this.w_Sez2Righe)
          this.w_Sez2Impor = IIF(this.oParentObject.w_Valuta=g_Codeur,this.w_Sez2Impor + iif(cp_round(NVL(this.w_ImpoGrup ,0),0)=0 And this.w_Impogrup<>0,iif(this.w_ImpoGrup<0,-1,1),cp_round(NVL(this.w_ImpoGrup ,0),0)),this.w_Sez2Impor + cp_round((NVL(this.w_ImpoGrup ,0)/1000)-this.w_ARRSUP,0))
        case PITIPMOV $ "CS-AS" 
          this.w_Sez3Righe = IIF(this.w_ImpoGrup<>0,this.w_Sez3Righe + 1,this.w_Sez3Righe)
          this.w_Sez3Impor = IIF(this.oParentObject.w_Valuta=g_Codeur,this.w_Sez3Impor + iif(cp_round(NVL(this.w_ImpoGrup ,0),0)=0 And this.w_Impogrup<>0,1,cp_round(NVL(this.w_ImpoGrup ,0),0)),this.w_Sez3Impor + cp_round((NVL(this.w_ImpoGrup ,0)/1000)-this.w_ARRSUP,0))
        case PITIPMOV $ "RS-RX" 
          this.w_Sez4Righe = IIF(this.w_Rigavalida<>0,this.w_Sez4Righe + 1,this.w_Sez4Righe)
          this.w_Sez4Impor = IIF(this.oParentObject.w_Valuta=g_Codeur,this.w_Sez4Impor + iif(cp_round(NVL(this.w_ImpoGrup ,0),0)=0 And this.w_Impogrup<>0,1,cp_round(NVL(this.w_ImpoGrup ,0),0)),this.w_Sez4Impor + cp_round((NVL(this.w_ImpoGrup ,0)/1000)-this.w_ARRSUP,0))
      endcase
      skip
    enddo
    goto this.SaveReco
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Frontespizio
    * --- Variabili Locali utilizzate nella stampa del frontespizio
    * --- Preparo il cursore __TMP__per stampare su PDF.I dati saranno elaborati dalla CP_FFDF
    CREATE CURSOR __TMP__ ( MS C(2), TR C(1), ANNO C(2), ANNOT C(2),NDISC N(10),; 
 OBPARIVA C(12), OBCOGN C(25), OBNOME C(25),OBRAGSOC C(40),; 
 CAS1 C(1), CAS2 C(1),CAS3 C(1),CAS4 C(1),CAS5 C(1),DELPARIVA C(16), DELRAGSOC C(40),; 
 S1TOTRIG N(6), S1IMP N(15,3),S2TOTRIG N(6), S2IMP N(15,3),S3TOTRIG N(6),S3IMP N(15,3),; 
 S4TOTRIG N(6),S4IMP N(15,3),DATE D(8))
    * --- Definisco Array bidimensionale da passare alla funzione CP_FFDF per la creazione del file FDF
    DECLARE ARRFDF (3,2)
    * --- Riempio l'Array ARRFDF con i nomi dei campi che nel Modello PDF sono rappresentati come N, N>1, celle distinte.
    ARRFDF (1,1) = "MS"
    ARRFDF (1,2) = 2
    ARRFDF (2,1) = "ANNO"
    ARRFDF (2,2) = 2
    ARRFDF (3,1) = "ANNOT"
    ARRFDF (3,2) = 2
    * --- Leggo il flag Azienda o Persona Fisica
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPERAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPERAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERAZI = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se persona fisica leggo dalla tabella titolari il nome ed il cognome
    if this.w_PERAZI="S"
      * --- Read from TITOLARI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TITOLARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2],.t.,this.TITOLARI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TTCOGTIT,TTNOMTIT"+;
          " from "+i_cTable+" TITOLARI where ";
              +"TTCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TTCOGTIT,TTNOMTIT;
          from (i_cTable) where;
              TTCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TTCOGTIT = NVL(cp_ToDate(_read_.TTCOGTIT),cp_NullValue(_read_.TTCOGTIT))
        this.w_TTNOMTIT = NVL(cp_ToDate(_read_.TTNOMTIT),cp_NullValue(_read_.TTNOMTIT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_AZRAGAZI = space(40)
    else
      this.w_TTCOGTIT = space(25)
      this.w_TTNOMTIT = space(25)
      this.w_AZRAGAZI = g_RAGAZI
    endif
    * --- Cessioni di beni
    if this.w_Sez1RigC<>0 OR this.w_Sez2RigC<>0 Or this.w_Sez3RigC<>0 OR this.w_Sez4RigC<>0
      * --- Verifico se devo creare uno o due frontespizi 
      if this.oParentObject.w_Rottura_Ces="S" or this.oParentObject.w_CESS_BENI<>this.oParentObject.w_CESS_SERV
        if this.w_Sez1RigC<>0 OR this.w_Sez2RigC<>0 
          * --- Stampa frontespizio cartaceo Mod. INTRA - 1
          this.w_ProgEle = this.w_NProgEleC_Beni
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES="0" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES="8" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES="9" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          * --- Se il numero del mese (M) � minore di nove nella stampa lo rappresento con 0M
          if this.w_APPERICE > 9
            this.w_STRACVE = alltrim(str(this.w_APPERICE))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERICE))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOCESS="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOCESS="T",alltrim(str(this.w_APPERICE)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOCESS="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOCESS="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 this.w_Sez1RigC,this.w_Sez1ImpC,this.w_Sez2RigC,this.w_Sez2ImpC,0,0,0,0,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRACFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRACFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRACFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRACFL1.FDF","",this.oParentObject)
          endif
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Cessioni di Servizi
        if this.w_Sez3RigC<>0 OR this.w_Sez4RigC<>0
          * --- Stampa frontespizio cartaceo Mod. INTRA - 1
          this.w_ProgEle = iif(this.oParentObject.w_Rottura_Ces="S",this.w_NProgEleC_Serv,this.w_NProgEleC_Beni)
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES_CS="0" And this.oParentObject.w_TIPOCSERV="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES_CS="8" And this.oParentObject.w_TIPOCSERV="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES_CS="9" And this.oParentObject.w_TIPOCSERV="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          * --- Se il numero del mese (M) � minore di nove nella stampa lo rappresento con 0M
          if this.w_APPERICE_S > 9
            this.w_STRACVE = alltrim(str(this.w_APPERICE_S))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERICE_S))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOCSERV="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOCSERV="T",alltrim(str(this.w_APPERICE_S)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOCSERV="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOCSERV="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 0,0,0,0,this.w_Sez3Rigc,this.w_Sez3Impc,this.w_Sez4Rigc,this.w_Sez4Impc,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRACFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRACFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRACFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRACFL1.FDF","",this.oParentObject)
          endif
          * --- Ritardo l'operazione successiva
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        if this.w_Sez1RigC<>0 OR this.w_Sez2RigC<>0 Or this.w_Sez3RigC<>0 OR this.w_Sez4RigC<>0
          * --- Stampa frontespizio cartaceo Mod. INTRA - 1
          this.w_ProgEle = this.w_NProgEleC_Beni
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES="0" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES="8" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES="9" And this.oParentObject.w_TIPOCESS="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          * --- Se il numero del mese (M) � minore di nove nella stampa lo rappresento con 0M
          if this.w_APPERICE > 9
            this.w_STRACVE = alltrim(str(this.w_APPERICE))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERICE))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOCESS="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOCESS="T",alltrim(str(this.w_APPERICE)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOCESS="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOCESS="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 this.w_Sez1RigC,this.w_Sez1ImpC,this.w_Sez2RigC,this.w_Sez2ImpC,this.w_Sez3Rigc,this.w_Sez3Impc,this.w_Sez4Rigc,this.w_Sez4Impc,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRACFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRACFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRACFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRACFL1.FDF","",this.oParentObject)
          endif
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Acquisti
    if this.w_Sez1RigA<>0 or this.w_Sez2RigA<>0 Or this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0
      if this.oParentObject.w_Rottura_Acq="S" or this.oParentObject.w_ACQ_BENI<>this.oParentObject.w_ACQ_SERV
        if this.w_Sez1RigA<>0 or this.w_Sez2RigA<>0
          * --- Stampa frontespizio cartaceo Mod. INTRA - 2
          this.w_ProgEle = this.w_NProgEleA_Beni
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES_AB="0" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES_AB="8" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES_AB="9" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          if this.w_APPERIAC > 9
            this.w_STRACVE = alltrim(str(this.w_APPERIAC))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERIAC))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOACQU="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOACQU="T",alltrim(str(this.w_APPERIAC)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOACQU="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOACQU="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 this.w_Sez1RigA,this.w_Sez1ImpA,this.w_Sez2RigA,this.w_Sez2ImpA,0,0,0,0,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRAAFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRAAFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRAAFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRAAFL1.FDF","",this.oParentObject)
          endif
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0
          * --- Stampa frontespizio cartaceo Mod. INTRA - 2
          this.w_ProgEle = iif(this.oParentObject.w_Rottura_Acq="S",this.w_NProgEleA_serv,this.w_NProgEleA_Beni)
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES_AS="0" And this.oParentObject.w_TIPOASERV="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES_AS="8" And this.oParentObject.w_TIPOASERV="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES_AS="9" And this.oParentObject.w_TIPOASERV="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          if this.w_APPERIAC_S > 9
            this.w_STRACVE = alltrim(str(this.w_APPERIAC_S))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERIAC_S))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOASERV="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOASERV="T",alltrim(str(this.w_APPERIAC_S)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOASERV="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOASERV="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 0,0,0,0,this.w_Sez3RigA,this.w_Sez3ImpA,this.w_Sez4RigA,this.w_Sez4ImpA,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRAAFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRAAFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRAAFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRAAFL1.FDF","",this.oParentObject)
          endif
        endif
      else
        if this.w_Sez1RigA<>0 or this.w_Sez2RigA<>0 or this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0
          * --- Stampa frontespizio cartaceo Mod. INTRA - 2
          this.w_ProgEle = this.w_NProgEleA_Beni
          * --- Nel caso la periodicit� � mensile non valorizzo le variabili contenenti le 
          *     impostazioni sul trimestre
          this.w_CAS1 = IIF(this.oParentObject.w_PRIMPRES_AB="0" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS2 = IIF(this.oParentObject.w_PRIMPRES_AB="8" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS3 = IIF(this.oParentObject.w_PRIMPRES_AB="9" And this.oParentObject.w_TIPOACQU="T","X","")
          this.w_CAS4 = IIF(this.oParentObject.w_CESSATTI="7" or this.oParentObject.w_CESSATTI="9","X","")
          this.w_CAS5 = IIF(this.oParentObject.w_CESSATTI="8" or this.oParentObject.w_CESSATTI="9","X","")
          if this.w_APPERIAC > 9
            this.w_STRACVE = alltrim(str(this.w_APPERIAC))
          else
            this.w_STRACVE = "0" + alltrim(str(this.w_APPERIAC))
          endif
          this.w_MS = IIF(this.oParentObject.w_TIPOACQU="M",this.w_STRACVE,"")
          this.w_TR = IIF(this.oParentObject.w_TIPOACQU="T",alltrim(str(this.w_APPERIAC)),"")
          this.w_YEAR = IIF(this.oParentObject.w_TIPOACQU="M",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          this.w_YEART = IIF(this.oParentObject.w_TIPOACQU="T",RIGHT(this.oParentObject.w_AnnoRife,2),"")
          * --- Memorizzo i dati nel cursore __TMP__
          INSERT INTO __TMP__ VALUES (this.w_MS,this.w_TR,this.w_YEAR,this.w_YEART,this.w_ProgEle,this.oParentObject.w_AZPIVAZI,; 
 this.w_TTCOGTIT,this.w_TTNOMTIT,this.w_AZRAGAZI,this.w_CAS1,this.w_CAS2,this.w_CAS3,this.w_CAS4,this.w_CAS5,; 
 iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAICFPI,space(16)),iif (this.oParentObject.w_SOGGDELE="S",this.oParentObject.w_AZAIRAGS,space(40)),; 
 this.w_Sez1RigA,this.w_Sez1ImpA,this.w_Sez2RigA,this.w_Sez2ImpA,this.w_Sez3RigA,this.w_Sez3ImpA,this.w_Sez4RigA,this.w_Sez4ImpA,i_datsys)
          if this.oParentObject.w_VALUTA=g_CODEUR
            * --- Funzione di creazione dell' FDF
            result1=cp_ffdf(" ","INTRAAFE.PDF",this," ",@ARRFDF)
            * --- Funzione di visualizzazione su PDF
            CP_CHPRN(tempadhoc()+"\INTRAAFE1.FDF","",this.oParentObject)
          else
            result1=cp_ffdf(" ","INTRAAFL.PDF",this," ",@ARRFDF)
            CP_CHPRN(tempadhoc()+"\INTRAAFL1.FDF","",this.oParentObject)
          endif
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Riepilogo Cessioni
    L_DEC=this.oParentObject.w_DECIM
    L_ANNO=this.oParentObject.w_AnnoRife
    L_PIVAZI=this.oParentObject.w_PIVAPRES
    if (this.oParentObject.w_CESS_BENI="S" OR this.oParentObject.w_CESS_SERV="S") AND (this.w_Sez1RigC<>0 OR this.w_Sez2RigC<>0 OR this.w_Sez3RigC<>0 OR this.w_Sez4RigC<>0)
      L_TIPOREPO=ah_Msgformat("RIEPILOGO DELLE CESSIONI INTRACOMUNITARIE DI BENI E DEI SERVIZI RESI")
      * --- Stampa Cessioni
      if this.w_Sez1RigC<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 1 - CESSIONI DI BENI REGISTRATE NEL PERIODO")
        L_TIPOPER=this.oParentObject.w_TIPOCESS
        L_PERIODO=this.w_APPERICE
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "CE" ORDER BY PROG
        cp_chprn("QUERY\GSAR_SEF", " ", this)
      endif
      * --- Stampa Rettifiche
      if this.w_Sez2RigC<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 2 - RETTIFICHE ALLE CESSIONI DI BENI RELATIVE A PERIODI PRECEDENTI")
        L_TIPOPER=this.oParentObject.w_TIPOCESS
        L_PERIODO=this.w_APPERICE
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "RC" ORDER BY PROG
        cp_chprn("QUERY\GSAR1SEF", " ", this)
      endif
      * --- Stampa Servizi resi
      if this.w_Sez3RigC<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 3 - SERVIZI RESI REGISTRATI NEL PERIODO")
        L_TIPOPER=this.oParentObject.w_TIPOCSERV
        L_PERIODO=this.w_APPERICE_S
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "CS" ORDER BY PROG
        cp_chprn("QUERY\GSAR3SEF","",this.oParentObject)
      endif
      * --- Stampa Rettifiche Servizi resi
      if this.w_Sez4RigC<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 4 - RETTIFICHE AI SERVIZI RESI INDICATI IN SEZIONE 3 DI PERIODI PRECEDENTI")
        L_TIPOPER=this.oParentObject.w_TIPOCSERV
        L_PERIODO=this.w_APPERICE_S
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "RS" ORDER BY PROG
        cp_chprn("QUERY\GSAR4SEF","",this.oParentObject)
      endif
    endif
    * --- Stampa Riepilogo Acquisti
    if (this.oParentObject.w_ACQ_BENI="S" or this.oParentObject.w_ACQ_SERV="S") AND (this.w_Sez1RigA<>0 or this.w_Sez2RigA<>0 or this.w_Sez3RigA<>0 or this.w_Sez4RigA<>0 )
      L_TIPOREPO=Ah_Msgformat("RIEPILOGO DEGLI ACQUISTI INTRACOMUNITARI DI BENI E DEI SERVIZI RICEVUTI")
      * --- Stampa Acquisti
      if this.w_Sez1RigA<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 1 - ACQUISTI DI BENI REGISTRATI NEL PERIODO")
        L_TIPOPER=this.oParentObject.w_TIPOACQU
        L_PERIODO=this.w_APPERIAC
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "AC" ORDER BY PROG
        cp_chprn("QUERY\GSARASEF","",this.oParentObject)
      endif
      * --- Stampa Rettifiche
      if this.w_Sez2RigA<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 2 - RETTIFICHE AGLI ACQUISTI DI BENI RELATIVE A PERIODI PRECEDENTI")
        L_TIPOPER=this.oParentObject.w_TIPOACQU
        L_PERIODO=this.w_APPERIAC
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "RA" ORDER BY PROG
        cp_chprn("QUERY\GSAR1ASEF","",this.oParentObject)
      endif
      * --- Stampa Servizi ricevuti
      if this.w_Sez3RigA<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 3 - SERVIZI RICEVUTI REGISTRATI NEL PERIODO")
        L_TIPOPER=this.oParentObject.w_TIPOASERV
        L_PERIODO=this.w_APPERIAC_S
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "AS" ORDER BY PROG
        cp_chprn("QUERY\GSAR3ASEF","",this.oParentObject)
      endif
      * --- Stampa Rettifica Servizi ricevuti
      if this.w_Sez4RigA<>0
        L_TIPOSEZ=ah_Msgformat("SEZIONE 4 - RETTIFICHE AI SERVIZI RICEVUTI INDICATI IN SEZIONE 3 DI PERIODI PRECEDENTI")
        L_TIPOPER=this.oParentObject.w_TIPOASERV
        L_PERIODO=this.w_APPERIAC_S
        SELECT * FROM RIGDETT INTO CURSOR __TMP__ WHERE TIPOMOV = "RX" ORDER BY PROG
        cp_chprn("QUERY\GSAR4ASEF","",this.oParentObject)
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.Gruppo = ""
    this.Page_10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Azzero le variabili
    STORE 0 TO this.w_PIIMPNAZ,this.w_PIIMPVAL,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA,this.w_IESENAZ,this.w_IESELSTA,this.w_RIGAVALIDA
    this.w_TIPAC = PITIPMOV
    * --- Raggruppamento
    this.Record = recno()
    do while (.not. eof()) .and. this.Gruppo = eval( this.GruppoExpr ) and this.w_TIPAC=PITIPMOV
      * --- Totalizza i dati del record corrente
      this.Record = recno()
      * --- Se la valuta naz. della registrazione � LIRE ma la valuta nella maschera �  EURO converto l'importo
      if this.oParentObject.w_VALUTA <> PIVALNAZ and this.oParentObject.w_VALUTA = g_PERVAL
        this.w_IESENAZ = VAL2MON(NVL(PIIMPNAZ,0), GETCAM(PIVALNAZ,this.w_PIDATREG), 1,this.w_PIDATREG,this.oParentObject.w_VALUTA,GETVALUT(this.oParentObject.w_VALUTA,"VADECTOT"))
        this.w_IESELSTA = VAL2MON(NVL(PIVALSTA,0),GETCAM(PIVALNAZ,this.w_PIDATREG), 1,this.w_PIDATREG,this.oParentObject.w_VALUTA,GETVALUT(this.oParentObject.w_VALUTA,"VADECTOT"))
        this.w_PIIMPNAZ = this.w_PIIMPNAZ + this.w_IESENAZ
        this.w_PIVALSTA = this.w_PIVALSTA + this.w_IESELSTA
      else
        this.w_PIIMPNAZ = this.w_PIIMPNAZ + NVL(PIIMPNAZ, 0)
        this.w_PIVALSTA = this.w_PIVALSTA + NVL(PIVALSTA, 0)
      endif
      this.w_PIIMPVAL = this.w_PIIMPVAL + NVL(PIIMPVAL, 0)
      this.w_PIMASNET = this.w_PIMASNET + NVL(PIMASNET, 0)
      this.w_PIQTASUP = this.w_PIQTASUP + NVL(PIQTASUP, 0)
      * --- Nel caso di rettifica di servizi considero solo le righe dove � presente la 
      *     sezione doganale,l'anno di registrazione,il protocollo della dichiarazione e
      *     il progressivo della sezione 3
      if PITIPMOV $ "RS-RX" 
        this.w_Rigavalida = this.w_Rigavalida+IIF(NOT EMPTY(PISEZDOG) AND NOT EMPTY(PIANNREG) AND NOT EMPTY(PIPROGSE) AND NOT EMPTY(PIPRORET),1,0)
      endif
      skip
      this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
      this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
      this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
      this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
      this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
    enddo
    * --- Si posiziona sull'ultimo record che apparteneva all'intervallo di raggruppamento
    goto this.Record
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    this.w_PIPAEPAG = iif( isnull( PIPAEPAG ), space(2) , alltrim(PIPAEPAG) )
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Stringa di raggruppamento
    do case
      case PITIPMOV $ "CE-RC"
        if this.oParentObject.w_TIPOCESS = "M"
          * --- Cessioni / Rettifiche mensili
          this.Gruppo = this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
          this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISODES+NVL(PIPROORI,SPACE(2)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+this.w_NAISODES+NVL(PIPROORI,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          * --- Cessioni / Rettifiche trimestrali 
          this.Gruppo = this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      case PITIPMOV $ "AC-RA"
        * --- Acquisti
        if this.oParentObject.w_TIPOACQU = "M"
          this.Gruppo = this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
          this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          * --- Acquisti trimestrali o annuali (se annuali il codice nomenclatura viene eliminato dal raggruppamento)
          this.Gruppo = this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+Alltrim(Nvl(PINATTRA,SPACE(3)))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      case PITIPMOV $ "CS-AS"
        * --- Cessioni / Acquisti di servizi mensili / trimestrali
        this.Gruppo = this.w_ANPARIVA+STR(NUMFATT)+IIF(EMPTY(ALFAFATT),SPACE(10),ALFAFATT)+IIF(EMPTY(DATAFATT),SPACE(6),DATAFATT)
        this.Gruppo = this.Gruppo+NVL(PINOMENC,SPACE(8))+NVL(PIPERSER,SPACE(1))+NVL(PIMODINC,SPACE(1))+this.w_PIPAEPAG
        this.GruppoExpr = "this.w_ANPARIVA+STR(NUMFATT)+IIF(EMPTY(ALFAFATT),SPACE(10),ALFAFATT)+IIF(EMPTY(DATAFATT),SPACE(6),DATAFATT)"
        this.GruppoExpr = this.GruppoExpr+"+NVL(PINOMENC,SPACE(8))+NVL(PIPERSER,SPACE(1))+NVL(PIMODINC,SPACE(1))+this.w_PIPAEPAG"
      case PITIPMOV $ "RS-RX"
        * --- Rettifica di cessioni / acquisti di servizi mensili / trimestrali
        this.Gruppo = PISEZDOG+PIANNREG+PIPRORET+PIPROGSE
        this.GruppoExpr = "PISEZDOG+PIANNREG+PIPRORET+PIPROGSE"
    endcase
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("ETICHETTE")
      select ETICHETTE
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("TEMPO")
      select TEMPO
      use
    endif
    if used("RIGDETT")
      select RIGDETT
      use
    endif
  endproc


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MESS = IIF ( NOT this.w_IMPNULL , "Si desidera aggiornare il progressivo disco e i progressivi periodo?", "Si desidera aggiornare i progressivi periodo?")
    if this.w_AGGIPROG
      if ah_YesNo(this.w_MESS)
        if this.oParentObject.w_ACQ_BENI="S"
          this.oParentObject.w_PERIACQU = IIF((this.oParentObject.w_PeriAcqu=4 and this.oParentObject.w_TipoAcqu="T") or (this.oParentObject.w_PeriAcqu=12 and this.oParentObject.w_TipoAcqu="M"),0,this.oParentObject.w_PERIACQU)
        else
          this.oParentObject.w_PERIACQU = this.oParentObject.w_AZINPEAC
        endif
        if this.oParentObject.w_ACQ_SERV="S"
          this.oParentObject.w_PERIASER = IIF((this.oParentObject.w_PeriAser=4 and this.oParentObject.w_TipoAserv="T") or (this.oParentObject.w_PeriAser=12 and this.oParentObject.w_TipoAserv="M"),0,this.oParentObject.w_PeriAser)
        else
          this.oParentObject.w_PERIASER = this.oParentObject.w_AZINPEAS
        endif
        if this.oParentObject.w_CESS_BENI="S"
          this.oParentObject.w_PERICESS = IIF((this.oParentObject.w_PeriCess=4 and this.oParentObject.w_TipoCess="T") or (this.oParentObject.w_PeriCess=12 and this.oParentObject.w_TipoCess="M"),0,this.oParentObject.w_PERICESS)
        else
          this.oParentObject.w_PERICESS = this.oParentObject.w_AZINPECE
        endif
        if this.oParentObject.w_CESS_SERV="S"
          this.oParentObject.w_PERICSER = IIF((this.oParentObject.w_PeriCser=4 and this.oParentObject.w_TipoCserv="T") or (this.oParentObject.w_PeriCser=12 and this.oParentObject.w_TipoCserv="M"),0,this.oParentObject.w_PeriCser)
        else
          this.oParentObject.w_PERICSER = this.oParentObject.w_AZINPECS
        endif
        this.w_AGGIORNA = "S"
        this.oParentObject.w_NProgEle = this.oParentObject.w_NProgEle + 1
        * --- Write into AZDATINT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZDATINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZDATINT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZDATINT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ITINPEAC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PERIACQU),'AZDATINT','ITINPEAC');
          +",ITINPECE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PERICESS),'AZDATINT','ITINPECE');
          +",ITINPECS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PERICSER),'AZDATINT','ITINPECS');
          +",ITINPEAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PERIASER),'AZDATINT','ITINPEAS');
              +i_ccchkf ;
          +" where ";
              +"ITCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              ITINPEAC = this.oParentObject.w_PERIACQU;
              ,ITINPECE = this.oParentObject.w_PERICESS;
              ,ITINPECS = this.oParentObject.w_PERICSER;
              ,ITINPEAS = this.oParentObject.w_PERIASER;
              &i_ccchkf. ;
           where;
              ITCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NOT this.w_IMPNULL
          * --- Write into AZIENDA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AZNPRGEL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NProgEle),'AZIENDA','AZNPRGEL');
                +i_ccchkf ;
            +" where ";
                +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   )
          else
            update (i_cTable) set;
                AZNPRGEL = this.oParentObject.w_NProgEle;
                &i_ccchkf. ;
             where;
                AZCODAZI = i_CODAZI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Se sono presenti movimenti di tipo cessione/acquisto di servizi devo aggiornare
        *     il campo progressivo sezione da rettificare inserendo il progressivo della sezione 3.Per controllare
        *     se esistono movimenti di questo tipo lancio la query Gsar1bef impostanto 
        *     come filtri la partita IVA,numero alfa e data fattura,nomenclatura,modalit�
        *     di erogazione,modalit� di incasso e paese di pagamento
        SELECT RIGDETT
        GO TOP
        SCAN FOR TIPOMOV $ "CS-AS"
        this.w_ANPARIVA = CODIVA
        this.w_NUMFATT = NUMFATT
        this.w_ALFAFATT = ALFAFATT
        this.w_DATAFATT = DATAFATT
        this.w_PINOMENC = NOMENCL
        this.w_PIPERSER = PIPERSER
        this.w_PIMODINC = PIMODINC
        this.w_PIPAEPAG = PAEPAG
        this.w_TIPOMOV = TIPOMOV
        this.w_PROG = +right( repl("0",5) + alltrim(str(Prog,5,0)) , 5)
        * --- Write into ELEIDETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ELEIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELEIDETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="PISERIAL,CPROWNUM"
          do vq_exec with 'GSAR1BEF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELEIDETT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ELEIDETT.PISERIAL = _t2.PISERIAL";
                  +" and "+"ELEIDETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PIPROGSE ="+cp_NullLink(cp_ToStrODBC(this.w_PROG),'ELEIDETT','PIPROGSE');
          +",PIANNREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNORIFE),'ELEIDETT','PIANNREG');
              +i_ccchkf;
              +" from "+i_cTable+" ELEIDETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ELEIDETT.PISERIAL = _t2.PISERIAL";
                  +" and "+"ELEIDETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELEIDETT, "+i_cQueryTable+" _t2 set ";
          +"ELEIDETT.PIPROGSE ="+cp_NullLink(cp_ToStrODBC(this.w_PROG),'ELEIDETT','PIPROGSE');
          +",ELEIDETT.PIANNREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNORIFE),'ELEIDETT','PIANNREG');
              +Iif(Empty(i_ccchkf),"",",ELEIDETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ELEIDETT.PISERIAL = t2.PISERIAL";
                  +" and "+"ELEIDETT.CPROWNUM = t2.CPROWNUM";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELEIDETT set (";
              +"PIPROGSE,";
              +"PIANNREG";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(this.w_PROG),'ELEIDETT','PIPROGSE')+",";
              +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNORIFE),'ELEIDETT','PIANNREG')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ELEIDETT.PISERIAL = _t2.PISERIAL";
                  +" and "+"ELEIDETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELEIDETT set ";
          +"PIPROGSE ="+cp_NullLink(cp_ToStrODBC(this.w_PROG),'ELEIDETT','PIPROGSE');
          +",PIANNREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNORIFE),'ELEIDETT','PIANNREG');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".PISERIAL = "+i_cQueryTable+".PISERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PIPROGSE ="+cp_NullLink(cp_ToStrODBC(this.w_PROG),'ELEIDETT','PIPROGSE');
          +",PIANNREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNORIFE),'ELEIDETT','PIANNREG');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ENDSCAN
        AH_ERRORMSG("Aggiornamento terminato",48)
      else
        this.oParentObject.w_NProgEle = IIF(this.oParentObject.w_AZNPRGEL=0,1,this.oParentObject.w_AZNPRGEL)
      endif
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritardo l'operazione successiva
    if type("g_PDFDELAY")="N"
      wait "" timeout g_PDFDELAY
    else
      wait "" timeout 5
    endif
    * --- Pulisco il Cursore __TMP__ pronto per essere caricato con il record degli acquisti
    SELECT __TMP__
    ZAP
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='NOMENCLA'
    this.cWorkTables[6]='SEDIAZIE'
    this.cWorkTables[7]='ELEIDETT'
    this.cWorkTables[8]='TITOLARI'
    this.cWorkTables[9]='AZDATINT'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
