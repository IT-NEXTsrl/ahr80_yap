* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_baa                                                        *
*              Aggiornamento dati analitica                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2014-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_baa",oParentObject)
return(i_retval)

define class tgsca_baa as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_DATREG = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_CONFERMA = .f.
  w_CODCOS = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  AGG_ANAL_idx=0
  DOC_DETT_idx=0
  VOC_COST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA DA BOTTONE MANUTENZIONE (da GSCA_SZM)
    this.w_SERIAL = this.oParentObject.w_SERIALE
    this.w_FLVEAC = IIF(LEFT(this.oParentObject.w_TIPMOV,5)="(D.A.","A","V")
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVDATREG"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVDATREG;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(CHKCONS("C"+this.w_FLVEAC,this.w_DATREG,"B","S"))
      * --- Esegue controllo data consolidamento
      i_retcode = 'stop'
      return
    endif
    * --- Delete from AGG_ANAL
    i_nConn=i_TableProp[this.AGG_ANAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore in cancellazione AGG_ANAL'
      return
    endif
    * --- Insert into AGG_ANAL
    i_nConn=i_TableProp[this.AGG_ANAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSCA_BAA",this.AGG_ANAL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if i_Rows<>0
      * --- Lancia la Manutenzione
      this.w_CONFERMA = .F.
      do GSCA_KAA with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Chiude i Cursori che non servono piu'
      if this.w_CONFERMA
        * --- Aggiorna Documenti di Evasione
        *     Select per comodit� in realt� e' una READ (x i documenti ho solo una riga..)
        * --- Select from AGG_ANAL
        i_nConn=i_TableProp[this.AGG_ANAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2],.t.,this.AGG_ANAL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" AGG_ANAL ";
              +" where MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL)+"";
               ,"_Curs_AGG_ANAL")
        else
          select * from (i_cTable);
           where MVSERIAL = this.w_SERIAL;
            into cursor _Curs_AGG_ANAL
        endif
        if used('_Curs_AGG_ANAL')
          select _Curs_AGG_ANAL
          locate for 1=1
          do while not(eof())
          * --- Read from VOC_COST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOC_COST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VCTIPCOS"+;
              " from "+i_cTable+" VOC_COST where ";
                  +"VCCODICE = "+cp_ToStrODBC(_Curs_AGG_ANAL.MVVOCCEN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VCTIPCOS;
              from (i_cTable) where;
                  VCCODICE = _Curs_AGG_ANAL.MVVOCCEN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODCEN ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVCODCEN),'DOC_DETT','MVCODCEN');
            +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVVOCCEN),'DOC_DETT','MVVOCCEN');
            +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVCODCOM),'DOC_DETT','MVCODCOM');
            +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC("A"),'DOC_DETT','MVTIPATT');
            +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVINICOM),'DOC_DETT','MVINICOM');
            +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVFINCOM),'DOC_DETT','MVFINCOM');
            +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(_Curs_AGG_ANAL.MVCODATT),'DOC_DETT','MVCODATT');
            +",MVCODCOS ="+cp_NullLink(cp_ToStrODBC(this.w_CODCOS),'DOC_DETT','MVCODCOS');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(_Curs_AGG_ANAL.MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_AGG_ANAL.CPROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_AGG_ANAL.MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVCODCEN = _Curs_AGG_ANAL.MVCODCEN;
                ,MVVOCCEN = _Curs_AGG_ANAL.MVVOCCEN;
                ,MVCODCOM = _Curs_AGG_ANAL.MVCODCOM;
                ,MVTIPATT = "A";
                ,MVINICOM = _Curs_AGG_ANAL.MVINICOM;
                ,MVFINCOM = _Curs_AGG_ANAL.MVFINCOM;
                ,MVCODATT = _Curs_AGG_ANAL.MVCODATT;
                ,MVCODCOS = this.w_CODCOS;
                &i_ccchkf. ;
             where;
                MVSERIAL = _Curs_AGG_ANAL.MVSERIAL;
                and CPROWNUM = _Curs_AGG_ANAL.CPROWNUM;
                and MVNUMRIF = _Curs_AGG_ANAL.MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_AGG_ANAL
            continue
          enddo
          use
        endif
      endif
      * --- Ripulisce le tabelle temporanee
      * --- Delete from AGG_ANAL
      i_nConn=i_TableProp[this.AGG_ANAL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione AGG_ANAL'
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='AGG_ANAL'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='VOC_COST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_AGG_ANAL')
      use in _Curs_AGG_ANAL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
