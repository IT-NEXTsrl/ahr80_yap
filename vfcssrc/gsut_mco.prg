* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mco                                                        *
*              Manutenzione tabella conversioni                                *
*                                                                              *
*      Author: Zucchetti TAM SpA (SM)                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-01                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsut_mco
if not cp_IsAdministrator(.F.)
   ah_ErrorMsg('Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema','!')
   return
endif
* --- Fine Area Manuale
return(createobject("tgsut_mco"))

* --- Class definition
define class tgsut_mco as StdTrsForm
  Top    = 1
  Left   = 2

  * --- Standard Properties
  Width  = 667
  Height = 382+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=171359383
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONVERSI_IDX = 0
  AHEPATCH_IDX = 0
  cFile = "CONVERSI"
  cKeySelect = "COCODREL"
  cKeyWhere  = "COCODREL=this.w_COCODREL"
  cKeyDetail  = "COCODREL=this.w_COCODREL and COORDINE=this.w_COORDINE"
  cKeyWhereODBC = '"COCODREL="+cp_ToStrODBC(this.w_COCODREL)';

  cKeyDetailWhereODBC = '"COCODREL="+cp_ToStrODBC(this.w_COCODREL)';
      +'+" and COORDINE="+cp_ToStrODBC(this.w_COORDINE)';

  cKeyWhereODBCqualified = '"CONVERSI.COCODREL="+cp_ToStrODBC(this.w_COCODREL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsut_mco"
  cComment = "Manutenzione tabella conversioni"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODREL = space(15)
  w_RELEASE = space(10)
  w_PATCH = 0
  w_COORDINE = space(5)
  w_CONOMPRO = space(50)
  w_CODESCON = space(100)
  w_COESEGUI = space(1)
  w_COABILIT = space(1)
  w_CORIPETI = space(1)
  w_COESEOBB = space(1)
  w_COERRORE = space(0)
  w_CODATESE = ctod('  /  /  ')
  w_COUTEESE = 0
  w_DESAGG = space(50)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONVERSI','gsut_mco')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mcoPag1","gsut_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Procedure di conversione")
      .Pages(1).HelpContextID = 261416981
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOCODREL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='CONVERSI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONVERSI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONVERSI_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_COCODREL = NVL(COCODREL,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONVERSI where COCODREL=KeySet.COCODREL
    *                            and COORDINE=KeySet.COORDINE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2],this.bLoadRecFilter,this.CONVERSI_IDX,"gsut_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONVERSI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONVERSI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONVERSI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODREL',this.w_COCODREL  )
      select * from (i_cTable) CONVERSI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RELEASE = space(10)
        .w_PATCH = 0
        .w_DESAGG = space(50)
        .w_COCODREL = NVL(COCODREL,space(15))
          if link_1_1_joined
            this.w_COCODREL = NVL(PTCODREL101,NVL(this.w_COCODREL,space(15)))
            this.w_RELEASE = NVL(PTRELEAS101,space(10))
            this.w_PATCH = NVL(PTNUMPAT101,0)
          else
          .link_1_1('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONVERSI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_COORDINE = NVL(COORDINE,space(5))
          .w_CONOMPRO = NVL(CONOMPRO,space(50))
          .w_CODESCON = NVL(CODESCON,space(100))
          .w_COESEGUI = NVL(COESEGUI,space(1))
          .w_COABILIT = NVL(COABILIT,space(1))
          .w_CORIPETI = NVL(CORIPETI,space(1))
          .w_COESEOBB = NVL(COESEOBB,space(1))
          .w_COERRORE = NVL(COERRORE,space(0))
          .w_CODATESE = NVL(cp_ToDate(CODATESE),ctod("  /  /  "))
          .w_COUTEESE = NVL(COUTEESE,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace COORDINE with .w_COORDINE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_COCODREL=space(15)
      .w_RELEASE=space(10)
      .w_PATCH=0
      .w_COORDINE=space(5)
      .w_CONOMPRO=space(50)
      .w_CODESCON=space(100)
      .w_COESEGUI=space(1)
      .w_COABILIT=space(1)
      .w_CORIPETI=space(1)
      .w_COESEOBB=space(1)
      .w_COERRORE=space(0)
      .w_CODATESE=ctod("  /  /  ")
      .w_COUTEESE=0
      .w_DESAGG=space(50)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_COCODREL))
         .link_1_1('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONVERSI')
    this.DoRTCalc(2,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOCODREL_1_1.enabled = i_bVal
      .Page1.oPag.oCOABILIT_2_5.enabled = i_bVal
      .Page1.oPag.oCORIPETI_2_6.enabled = i_bVal
      .Page1.oPag.oCOESEOBB_2_7.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCOCODREL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCOCODREL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CONVERSI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODREL,"COCODREL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
    i_lTable = "CONVERSI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONVERSI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_COORDINE C(5);
      ,t_CONOMPRO C(50);
      ,t_CODESCON C(100);
      ,t_COESEGUI N(3);
      ,t_COABILIT N(3);
      ,t_CORIPETI N(3);
      ,t_COESEOBB N(3);
      ,t_COERRORE M(10);
      ,t_CODATESE D(8);
      ,t_COUTEESE N(4);
      ,COORDINE C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1.controlsource=this.cTrsName+'.t_COORDINE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCONOMPRO_2_2.controlsource=this.cTrsName+'.t_CONOMPRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODESCON_2_3.controlsource=this.cTrsName+'.t_CODESCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.controlsource=this.cTrsName+'.t_COESEGUI'
    this.oPgFRm.Page1.oPag.oCOABILIT_2_5.controlsource=this.cTrsName+'.t_COABILIT'
    this.oPgFRm.Page1.oPag.oCORIPETI_2_6.controlsource=this.cTrsName+'.t_CORIPETI'
    this.oPgFRm.Page1.oPag.oCOESEOBB_2_7.controlsource=this.cTrsName+'.t_COESEOBB'
    this.oPgFRm.Page1.oPag.oCOERRORE_2_8.controlsource=this.cTrsName+'.t_COERRORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESE_2_10.controlsource=this.cTrsName+'.t_CODATESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOUTEESE_2_11.controlsource=this.cTrsName+'.t_COUTEESE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(69)
    this.AddVLine(157)
    this.AddVLine(503)
    this.AddVLine(525)
    this.AddVLine(603)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONVERSI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
      *
      * insert into CONVERSI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONVERSI')
        i_extval=cp_InsertValODBCExtFlds(this,'CONVERSI')
        i_cFldBody=" "+;
                  "(COCODREL,COORDINE,CONOMPRO,CODESCON,COESEGUI"+;
                  ",COABILIT,CORIPETI,COESEOBB,COERRORE,CODATESE"+;
                  ",COUTEESE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_COCODREL)+","+cp_ToStrODBC(this.w_COORDINE)+","+cp_ToStrODBC(this.w_CONOMPRO)+","+cp_ToStrODBC(this.w_CODESCON)+","+cp_ToStrODBC(this.w_COESEGUI)+;
             ","+cp_ToStrODBC(this.w_COABILIT)+","+cp_ToStrODBC(this.w_CORIPETI)+","+cp_ToStrODBC(this.w_COESEOBB)+","+cp_ToStrODBC(this.w_COERRORE)+","+cp_ToStrODBC(this.w_CODATESE)+;
             ","+cp_ToStrODBC(this.w_COUTEESE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONVERSI')
        i_extval=cp_InsertValVFPExtFlds(this,'CONVERSI')
        cp_CheckDeletedKey(i_cTable,0,'COCODREL',this.w_COCODREL,'COORDINE',this.w_COORDINE)
        INSERT INTO (i_cTable) (;
                   COCODREL;
                  ,COORDINE;
                  ,CONOMPRO;
                  ,CODESCON;
                  ,COESEGUI;
                  ,COABILIT;
                  ,CORIPETI;
                  ,COESEOBB;
                  ,COERRORE;
                  ,CODATESE;
                  ,COUTEESE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_COCODREL;
                  ,this.w_COORDINE;
                  ,this.w_CONOMPRO;
                  ,this.w_CODESCON;
                  ,this.w_COESEGUI;
                  ,this.w_COABILIT;
                  ,this.w_CORIPETI;
                  ,this.w_COESEOBB;
                  ,this.w_COERRORE;
                  ,this.w_CODATESE;
                  ,this.w_COUTEESE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CONVERSI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_COORDINE<>space(5)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONVERSI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and COORDINE="+cp_ToStrODBC(&i_TN.->COORDINE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONVERSI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and COORDINE=&i_TN.->COORDINE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_COORDINE<>space(5)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and COORDINE="+cp_ToStrODBC(&i_TN.->COORDINE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and COORDINE=&i_TN.->COORDINE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace COORDINE with this.w_COORDINE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONVERSI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONVERSI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CONOMPRO="+cp_ToStrODBC(this.w_CONOMPRO)+;
                     ",CODESCON="+cp_ToStrODBC(this.w_CODESCON)+;
                     ",COESEGUI="+cp_ToStrODBC(this.w_COESEGUI)+;
                     ",COABILIT="+cp_ToStrODBC(this.w_COABILIT)+;
                     ",CORIPETI="+cp_ToStrODBC(this.w_CORIPETI)+;
                     ",COESEOBB="+cp_ToStrODBC(this.w_COESEOBB)+;
                     ",COERRORE="+cp_ToStrODBC(this.w_COERRORE)+;
                     ",CODATESE="+cp_ToStrODBC(this.w_CODATESE)+;
                     ",COUTEESE="+cp_ToStrODBC(this.w_COUTEESE)+;
                     ",COORDINE="+cp_ToStrODBC(this.w_COORDINE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and COORDINE="+cp_ToStrODBC(COORDINE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONVERSI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CONOMPRO=this.w_CONOMPRO;
                     ,CODESCON=this.w_CODESCON;
                     ,COESEGUI=this.w_COESEGUI;
                     ,COABILIT=this.w_COABILIT;
                     ,CORIPETI=this.w_CORIPETI;
                     ,COESEOBB=this.w_COESEOBB;
                     ,COERRORE=this.w_COERRORE;
                     ,CODATESE=this.w_CODATESE;
                     ,COUTEESE=this.w_COUTEESE;
                     ,COORDINE=this.w_COORDINE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and COORDINE=&i_TN.->COORDINE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONVERSI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_COORDINE<>space(5)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONVERSI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and COORDINE="+cp_ToStrODBC(&i_TN.->COORDINE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and COORDINE=&i_TN.->COORDINE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_COORDINE<>space(5)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPATCH_1_6.visible=!this.oPgFrm.Page1.oPag.oPATCH_1_6.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODREL
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AHEPATCH_IDX,3]
    i_lTable = "AHEPATCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2], .t., this.AHEPATCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODREL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AHEPATCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PTCODREL like "+cp_ToStrODBC(trim(this.w_COCODREL)+"%");

          i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PTCODREL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PTCODREL',trim(this.w_COCODREL))
          select PTCODREL,PTRELEAS,PTNUMPAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PTCODREL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODREL)==trim(_Link_.PTCODREL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODREL) and !this.bDontReportError
            deferred_cp_zoom('AHEPATCH','*','PTCODREL',cp_AbsName(oSource.parent,'oCOCODREL_1_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                     +" from "+i_cTable+" "+i_lTable+" where PTCODREL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODREL',oSource.xKey(1))
            select PTCODREL,PTRELEAS,PTNUMPAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODREL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                   +" from "+i_cTable+" "+i_lTable+" where PTCODREL="+cp_ToStrODBC(this.w_COCODREL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODREL',this.w_COCODREL)
            select PTCODREL,PTRELEAS,PTNUMPAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODREL = NVL(_Link_.PTCODREL,space(15))
      this.w_RELEASE = NVL(_Link_.PTRELEAS,space(10))
      this.w_PATCH = NVL(_Link_.PTNUMPAT,0)
    else
      if i_cCtrl<>'Load'
        this.w_COCODREL = space(15)
      endif
      this.w_RELEASE = space(10)
      this.w_PATCH = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2])+'\'+cp_ToStr(_Link_.PTCODREL,1)
      cp_ShowWarn(i_cKey,this.AHEPATCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODREL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AHEPATCH_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.PTCODREL as PTCODREL101"+ ",link_1_1.PTRELEAS as PTRELEAS101"+ ",link_1_1.PTNUMPAT as PTNUMPAT101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONVERSI.COCODREL=link_1_1.PTCODREL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONVERSI.COCODREL=link_1_1.PTCODREL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCOCODREL_1_1.value==this.w_COCODREL)
      this.oPgFrm.Page1.oPag.oCOCODREL_1_1.value=this.w_COCODREL
    endif
    if not(this.oPgFrm.Page1.oPag.oRELEASE_1_4.value==this.w_RELEASE)
      this.oPgFrm.Page1.oPag.oRELEASE_1_4.value=this.w_RELEASE
    endif
    if not(this.oPgFrm.Page1.oPag.oPATCH_1_6.value==this.w_PATCH)
      this.oPgFrm.Page1.oPag.oPATCH_1_6.value=this.w_PATCH
    endif
    if not(this.oPgFrm.Page1.oPag.oCOABILIT_2_5.RadioValue()==this.w_COABILIT)
      this.oPgFrm.Page1.oPag.oCOABILIT_2_5.SetRadio()
      replace t_COABILIT with this.oPgFrm.Page1.oPag.oCOABILIT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCORIPETI_2_6.RadioValue()==this.w_CORIPETI)
      this.oPgFrm.Page1.oPag.oCORIPETI_2_6.SetRadio()
      replace t_CORIPETI with this.oPgFrm.Page1.oPag.oCORIPETI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOESEOBB_2_7.RadioValue()==this.w_COESEOBB)
      this.oPgFrm.Page1.oPag.oCOESEOBB_2_7.SetRadio()
      replace t_COESEOBB with this.oPgFrm.Page1.oPag.oCOESEOBB_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOERRORE_2_8.value==this.w_COERRORE)
      this.oPgFrm.Page1.oPag.oCOERRORE_2_8.value=this.w_COERRORE
      replace t_COERRORE with this.oPgFrm.Page1.oPag.oCOERRORE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGG_1_8.value==this.w_DESAGG)
      this.oPgFrm.Page1.oPag.oDESAGG_1_8.value=this.w_DESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1.value==this.w_COORDINE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1.value=this.w_COORDINE
      replace t_COORDINE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCONOMPRO_2_2.value==this.w_CONOMPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCONOMPRO_2_2.value=this.w_CONOMPRO
      replace t_CONOMPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCONOMPRO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODESCON_2_3.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODESCON_2_3.value=this.w_CODESCON
      replace t_CODESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODESCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.RadioValue()==this.w_COESEGUI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.SetRadio()
      replace t_COESEGUI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESE_2_10.value==this.w_CODATESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESE_2_10.value=this.w_CODATESE
      replace t_CODATESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUTEESE_2_11.value==this.w_COUTEESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUTEESE_2_11.value=this.w_COUTEESE
      replace t_COUTEESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUTEESE_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'CONVERSI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_COORDINE<>space(5));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_COORDINE) and (.w_COORDINE<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOORDINE_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if .w_COORDINE<>space(5)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_COORDINE<>space(5))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COORDINE=space(5)
      .w_CONOMPRO=space(50)
      .w_CODESCON=space(100)
      .w_COESEGUI=space(1)
      .w_COABILIT=space(1)
      .w_CORIPETI=space(1)
      .w_COESEOBB=space(1)
      .w_COERRORE=space(0)
      .w_CODATESE=ctod("  /  /  ")
      .w_COUTEESE=0
    endwith
    this.DoRTCalc(1,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_COORDINE = t_COORDINE
    this.w_CONOMPRO = t_CONOMPRO
    this.w_CODESCON = t_CODESCON
    this.w_COESEGUI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.RadioValue(.t.)
    this.w_COABILIT = this.oPgFrm.Page1.oPag.oCOABILIT_2_5.RadioValue(.t.)
    this.w_CORIPETI = this.oPgFrm.Page1.oPag.oCORIPETI_2_6.RadioValue(.t.)
    this.w_COESEOBB = this.oPgFrm.Page1.oPag.oCOESEOBB_2_7.RadioValue(.t.)
    this.w_COERRORE = t_COERRORE
    this.w_CODATESE = t_CODATESE
    this.w_COUTEESE = t_COUTEESE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_COORDINE with this.w_COORDINE
    replace t_CONOMPRO with this.w_CONOMPRO
    replace t_CODESCON with this.w_CODESCON
    replace t_COESEGUI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOESEGUI_2_4.ToRadio()
    replace t_COABILIT with this.oPgFrm.Page1.oPag.oCOABILIT_2_5.ToRadio()
    replace t_CORIPETI with this.oPgFrm.Page1.oPag.oCORIPETI_2_6.ToRadio()
    replace t_COESEOBB with this.oPgFrm.Page1.oPag.oCOESEOBB_2_7.ToRadio()
    replace t_COERRORE with this.w_COERRORE
    replace t_CODATESE with this.w_CODATESE
    replace t_COUTEESE with this.w_COUTEESE
    if i_srv='A'
      replace COORDINE with this.w_COORDINE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mcoPag1 as StdContainer
  Width  = 663
  height = 382
  stdWidth  = 663
  stdheight = 382
  resizeXpos=414
  resizeYpos=183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCODREL_1_1 as StdField with uid="EJZLZOSAJU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COCODREL", cQueryName = "COCODREL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice release",;
    HelpContextID = 13254770,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=138, Left=103, Top=11, cSayPict="'XXXXXXXXXXXXXXX'", cGetPict="'XXXXXXXXXXXXXXX'", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AHEPATCH", oKey_1_1="PTCODREL", oKey_1_2="this.w_COCODREL"

  func oCOCODREL_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODREL_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODREL_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCOCODREL_1_1.readonly and this.parent.oCOCODREL_1_1.isprimarykey)
    do cp_zoom with 'AHEPATCH','*','PTCODREL',cp_AbsName(this.parent,'oCOCODREL_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oRELEASE_1_4 as StdField with uid="UEHSLYGTLH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RELEASE", cQueryName = "RELEASE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Release",;
    HelpContextID = 26265366,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=331, Top=11, InputMask=replicate('X',10)

  add object oPATCH_1_6 as StdField with uid="OAKVHHPEJJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PATCH", cQueryName = "PATCH",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero patch",;
    HelpContextID = 251609846,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=489, Top=11, cSayPict='"@Z 999"', cGetPict='"@Z 999"'

  func oPATCH_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATCH=0)
    endwith
    endif
  endfunc

  add object oDESAGG_1_8 as StdField with uid="NTLSJDLOIF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESAGG", cQueryName = "DESAGG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiornamento",;
    HelpContextID = 169003466,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=103, Top=35, InputMask=replicate('X',50)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=68, width=652,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="COORDINE",Label1="Ordine",Field2="CONOMPRO",Label2="Programma",Field3="CODESCON",Label3="Descrizione conversione",Field4="COESEGUI",Label4="",Field5="CODATESE",Label5="Eseguita.. Il",Field6="COUTEESE",Label6="da",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49420678

  add object oStr_1_2 as StdString with uid="CBRNJFNLLQ",Visible=.t., Left=4, Top=11,;
    Alignment=1, Width=97, Height=15,;
    Caption="Cod. release:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="PAGMEBYEIB",Visible=.t., Left=252, Top=11,;
    Alignment=1, Width=76, Height=15,;
    Caption="Release:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="IHNNLEHDJJ",Visible=.t., Left=426, Top=11,;
    Alignment=1, Width=60, Height=15,;
    Caption="Patch:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (.w_PATCH=0)
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="RGQIVTJWGS",Visible=.t., Left=4, Top=35,;
    Alignment=1, Width=97, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=87,;
    width=648+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=88,width=647+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCOABILIT_2_5.Refresh()
      this.Parent.oCORIPETI_2_6.Refresh()
      this.Parent.oCOESEOBB_2_7.Refresh()
      this.Parent.oCOERRORE_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCOABILIT_2_5 as StdTrsCheck with uid="VSKXNUGLVB",rtrep=.t.,;
    cFormVar="w_COABILIT",  caption="Abilitata",;
    ToolTipText = "Se attivo: conversione abilitata",;
    HelpContextID = 185409658,;
    Left=551, Top=307,;
    cTotal="", cQueryName = "COABILIT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oCOABILIT_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COABILIT,&i_cF..t_COABILIT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOABILIT_2_5.GetRadio()
    this.Parent.oContained.w_COABILIT = this.RadioValue()
    return .t.
  endfunc

  func oCOABILIT_2_5.ToRadio()
    this.Parent.oContained.w_COABILIT=trim(this.Parent.oContained.w_COABILIT)
    return(;
      iif(this.Parent.oContained.w_COABILIT=='S',1,;
      0))
  endfunc

  func oCOABILIT_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCORIPETI_2_6 as StdTrsCheck with uid="YMBEIZVKEK",rtrep=.t.,;
    cFormVar="w_CORIPETI",  caption="Ripetibile",;
    ToolTipText = "Se attivo: la conversione pu� essere ripetibile",;
    HelpContextID = 192597905,;
    Left=551, Top=329,;
    cTotal="", cQueryName = "CORIPETI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oCORIPETI_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CORIPETI,&i_cF..t_CORIPETI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCORIPETI_2_6.GetRadio()
    this.Parent.oContained.w_CORIPETI = this.RadioValue()
    return .t.
  endfunc

  func oCORIPETI_2_6.ToRadio()
    this.Parent.oContained.w_CORIPETI=trim(this.Parent.oContained.w_CORIPETI)
    return(;
      iif(this.Parent.oContained.w_CORIPETI=='S',1,;
      0))
  endfunc

  func oCORIPETI_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOESEOBB_2_7 as StdTrsCheck with uid="NCHUJLQLND",rtrep=.t.,;
    cFormVar="w_COESEOBB",  caption="Obbligatoria",;
    ToolTipText = "Se attivo: la conversione � obbligatoria all'avvio procedura",;
    HelpContextID = 35757976,;
    Left=551, Top=351,;
    cTotal="", cQueryName = "COESEOBB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oCOESEOBB_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COESEOBB,&i_cF..t_COESEOBB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOESEOBB_2_7.GetRadio()
    this.Parent.oContained.w_COESEOBB = this.RadioValue()
    return .t.
  endfunc

  func oCOESEOBB_2_7.ToRadio()
    this.Parent.oContained.w_COESEOBB=trim(this.Parent.oContained.w_COESEOBB)
    return(;
      iif(this.Parent.oContained.w_COESEOBB=='S',1,;
      0))
  endfunc

  func oCOESEOBB_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOERRORE_2_8 as StdTrsMemo with uid="ZVFOMTVISF",rtseq=11,rtrep=.t.,;
    cFormVar="w_COERRORE",value=space(0),enabled=.f.,;
    HelpContextID = 22192021,;
    cTotal="", bFixedPos=.t., cQueryName = "COERRORE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=75, Width=527, Left=8, Top=303

  add object oStr_2_9 as StdString with uid="CYVPTEJEZA",Visible=.t., Left=8, Top=286,;
    Alignment=0, Width=290, Height=15,;
    Caption="Messaggio errore"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mcoBodyRow as CPBodyRowCnt
  Width=638
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCOORDINE_2_1 as StdTrsField with uid="XHXIPPXUEF",rtseq=4,rtrep=.t.,;
    cFormVar="w_COORDINE",value=space(5),nZero=5,isprimarykey=.t.,;
    ToolTipText = "Ordine conversione",;
    HelpContextID = 137494421,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=60, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], InputMask=replicate('X',5)

  add object oCONOMPRO_2_2 as StdTrsField with uid="GIYAPGSHDJ",rtseq=5,rtrep=.t.,;
    cFormVar="w_CONOMPRO",value=space(50),;
    ToolTipText = "Nome procedura di conversione",;
    HelpContextID = 10817419,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=60, Top=0, InputMask=replicate('X',50)

  add object oCODESCON_2_3 as StdTrsField with uid="GCRYBFCPTE",rtseq=6,rtrep=.t.,;
    cFormVar="w_CODESCON",value=space(100),;
    ToolTipText = "Breve descrizione operazione",;
    HelpContextID = 223326092,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=342, Left=149, Top=0, InputMask=replicate('X',100)

  add object oCOESEGUI_2_4 as StdTrsCheck with uid="VJHHQLGKAZ",rtrep=.t.,;
    cFormVar="w_COESEGUI",  caption="",;
    ToolTipText = "Conversione eseguita",;
    HelpContextID = 169975697,;
    Left=496, Top=0, Width=16,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.F.,AutoSize=.F.;
   , bGlobalFont=.t.


  func oCOESEGUI_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COESEGUI,&i_cF..t_COESEGUI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOESEGUI_2_4.GetRadio()
    this.Parent.oContained.w_COESEGUI = this.RadioValue()
    return .t.
  endfunc

  func oCOESEGUI_2_4.ToRadio()
    this.Parent.oContained.w_COESEGUI=trim(this.Parent.oContained.w_COESEGUI)
    return(;
      iif(this.Parent.oContained.w_COESEGUI=='S',1,;
      0))
  endfunc

  func oCOESEGUI_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCODATESE_2_10 as StdTrsField with uid="HGNNMJVBBS",rtseq=12,rtrep=.t.,;
    cFormVar="w_CODATESE",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data ultima esecizione conversione",;
    HelpContextID = 188985237,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=519, Top=0

  add object oCOUTEESE_2_11 as StdTrsField with uid="MRHONFVNGI",rtseq=13,rtrep=.t.,;
    cFormVar="w_COUTEESE",value=0,enabled=.f.,;
    ToolTipText = "Utente",;
    HelpContextID = 203399061,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=596, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCOORDINE_2_1.When()
    return(.t.)
  proc oCOORDINE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCOORDINE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mco','CONVERSI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODREL=CONVERSI.COCODREL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
