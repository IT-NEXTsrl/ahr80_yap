* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gcp                                                        *
*              Gadget CalendarPlus                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-04-01                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gcp",oParentObject))

* --- Class definition
define class tgsut_gcp as StdGadgetForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 226
  Height = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-21"
  HelpContextID=165067927
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gcp"
  cComment = "Gadget CalendarPlus"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATE = ctod('  /  /  ')
  w_QUERY = space(150)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_INITIME = ctot('')
  w_ENDTIME = ctot('')
  w_CODE = space(8)
  w_STATUS = space(3)
  w_TYPE = space(1)
  w_SERIAL = space(20)
  w_CALCSTR = space(20)
  w_ZOOM = space(100)
  o_ZOOM = space(100)
  w_CALCCOLOR = 0
  w_PROGRAM = space(250)
  w_PRGDESCRI = space(50)
  w_FOOTERDESCRI = space(50)
  w_NOGOTOMENU = .F.
  w_WORKINI = space(4)
  w_WORKFIN = space(4)
  w_INTERVAL = space(4)
  w_RET = .F.
  w_POSTDATE = ctod('  /  /  ')
  o_POSTDATE = ctod('  /  /  ')
  w_Calendar = .NULL.
  w_CalcString = .NULL.
  w_NextBtn = .NULL.
  w_PrevBtn = .NULL.
  w_oGrid = .NULL.
  w_oFooter = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gcp
  ADD Object oBalloon AS cp_balloontip
  
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     With This
       .cAlertMsg = ""
       m.ggname = 'frm'+Alltrim(.Name)
       m.l_oldError = On("Error")
       On Error &ggname..cAlertMsg=Message()
       
       .w_Calendar.nFontColor = .nFontColor
       .w_Calendar.Color =  .BackColor
       .w_oGrid.ForeColor = Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
  		 .w_CalcColor = .nFontColor
  		 .w_PrevBtn.BorderColor = .nFontColor
  		 .w_PrevBtn.oTriangle.BackColor = .nFontColor
  		 .w_PrevBtn.oTriangle.BorderColor = .nFontColor
       .w_NextBtn.BorderColor = .nFontColor
       .w_NextBtn.oTriangle.BackColor = .nFontColor
       .w_NextBtn.oTriangle.BorderColor = .nFontColor
       
       On error &l_oldError
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=2 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=2, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gcpPag1","gsut_gcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgsut_gcpPag2","gsut_gcp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Calendar = this.oPgFrm.Pages(1).oPag.Calendar
    this.w_CalcString = this.oPgFrm.Pages(2).oPag.CalcString
    this.w_NextBtn = this.oPgFrm.Pages(2).oPag.NextBtn
    this.w_PrevBtn = this.oPgFrm.Pages(2).oPag.PrevBtn
    this.w_oGrid = this.oPgFrm.Pages(2).oPag.oGrid
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    DoDefault()
    proc Destroy()
      this.w_Calendar = .NULL.
      this.w_CalcString = .NULL.
      this.w_NextBtn = .NULL.
      this.w_PrevBtn = .NULL.
      this.w_oGrid = .NULL.
      this.w_oFooter = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATE=ctod("  /  /  ")
      .w_QUERY=space(150)
      .w_TITLE=space(254)
      .w_INITIME=ctot("")
      .w_ENDTIME=ctot("")
      .w_CODE=space(8)
      .w_STATUS=space(3)
      .w_TYPE=space(1)
      .w_SERIAL=space(20)
      .w_CALCSTR=space(20)
      .w_ZOOM=space(100)
      .w_CALCCOLOR=0
      .w_PROGRAM=space(250)
      .w_PRGDESCRI=space(50)
      .w_FOOTERDESCRI=space(50)
      .w_NOGOTOMENU=.f.
      .w_WORKINI=space(4)
      .w_WORKFIN=space(4)
      .w_INTERVAL=space(4)
      .w_RET=.f.
      .w_POSTDATE=ctod("  /  /  ")
      .oPgFrm.Page1.oPag.Calendar.Calculate(.w_DATE,.w_QUERY)
          .DoRTCalc(1,5,.f.)
        .w_CODE = iif(Vartype(.w_oGrid.Getvar('Code'))<>'X',.w_oGrid.Getvar('Code'),'')
        .w_STATUS = iif( Vartype(.w_oGrid.Getvar('Status')) <> 'X' ,iif(Lower(.w_oGrid.Getvar('Status')) = 'appuntamento', 'A', 'TD'),'')
          .DoRTCalc(8,8,.f.)
        .w_SERIAL = iif( Vartype(.w_oGrid.Getvar('ATSERIAL')) <> 'X', .w_oGrid.Getvar('ATSERIAL') ,'')
        .w_CALCSTR = ALLTRIM(TRANSFORM(.w_POSTDATE))
      .oPgFrm.Page2.oPag.CalcString.Calculate(.w_CALCSTR,.w_CALCCOLOR)
      .oPgFrm.Page2.oPag.NextBtn.Calculate()
      .oPgFrm.Page2.oPag.PrevBtn.Calculate()
      .oPgFrm.Page2.oPag.oGrid.Calculate(.w_ZOOM)
          .DoRTCalc(11,11,.f.)
        .w_CALCCOLOR = RGB(225,255,255)
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_FOOTERDESCRI, .w_PROGRAM, .w_PRGDESCRI)
          .DoRTCalc(13,14,.f.)
        .w_FOOTERDESCRI = "Crea nuovo.."
          .DoRTCalc(16,20,.f.)
        .w_POSTDATE = .w_Calendar.CalGrid.dToday
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Calendar.Calculate(.w_DATE,.w_QUERY)
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        .DoRTCalc(1,5,.t.)
            .w_CODE = iif(Vartype(.w_oGrid.Getvar('Code'))<>'X',.w_oGrid.Getvar('Code'),'')
            .w_STATUS = iif( Vartype(.w_oGrid.Getvar('Status')) <> 'X' ,iif(Lower(.w_oGrid.Getvar('Status')) = 'appuntamento', 'A', 'TD'),'')
        .DoRTCalc(8,8,.t.)
            .w_SERIAL = iif( Vartype(.w_oGrid.Getvar('ATSERIAL')) <> 'X', .w_oGrid.Getvar('ATSERIAL') ,'')
        if .o_POSTDATE<>.w_POSTDATE
            .w_CALCSTR = ALLTRIM(TRANSFORM(.w_POSTDATE))
        endif
        .oPgFrm.Page2.oPag.CalcString.Calculate(.w_CALCSTR,.w_CALCCOLOR)
        .oPgFrm.Page2.oPag.NextBtn.Calculate()
        .oPgFrm.Page2.oPag.PrevBtn.Calculate()
        if .o_ZOOM<>.w_ZOOM
        .oPgFrm.Page2.oPag.oGrid.Calculate(.w_ZOOM)
        endif
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_FOOTERDESCRI, .w_PROGRAM, .w_PRGDESCRI)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Calendar.Calculate(.w_DATE,.w_QUERY)
        .oPgFrm.Page2.oPag.CalcString.Calculate(.w_CALCSTR,.w_CALCCOLOR)
        .oPgFrm.Page2.oPag.NextBtn.Calculate()
        .oPgFrm.Page2.oPag.PrevBtn.Calculate()
        .oPgFrm.Page2.oPag.oGrid.Calculate(.w_ZOOM)
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_FOOTERDESCRI, .w_PROGRAM, .w_PRGDESCRI)
    endwith
  return

  proc Calculate_GKBVNWACEG()
    with this
          * --- Open Activity
     if .w_TYPE='A'
          cp_OpenActivity(this;
              ,.w_SERIAL;
             )
     endif
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
    endwith
  endproc
  proc Calculate_TVCJQUSHOG()
    with this
          * --- Elabora cursore
          gsut_bpo(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_gcp
    *-- Selezione singola o doppio click
    If Left(Lower(cevent),23) = "w_calendar singleselect" Or Lower(cevent) = "w_calendar doubleclickselect" Or Left(Lower(cevent),22) = "w_calendar arrowbutton"
        this.w_POSTDATE = This.w_Calendar.CalGrid.dDateSelected
        If Lower(cevent) = "w_calendar doubleclickselect"
          This.oPgFrm.ActivePage = 2
        Endif
    Endif
    If Lower(cevent) = "activatepage 2" And Lower(Alltrim(cHomeDir))==Lower(Alltrim(Sys(5)+Curdir())) && aggiorno se sono puntato correttamente
        this.o_POSTDATE = this.w_POSTDATE
        This.SetControlsValue()
        This.w_CALCSTR = ALLTRIM(TRANSFORM(this.w_POSTDATE))
        This.oPgFrm.Page2.oPag.CalcString.Calculate(this.w_CALCSTR)
        This.w_INITIME = DTOT(This.w_POSTDATE)
        This.w_ENDTIME = DTOT(This.w_POSTDATE)+86399
        This.NotifyEvent("ZoomRefresh")
    Endif
    If Lower(cevent) = "activatepage 1"
    	With This.w_Calendar
    		if this.w_POSTDATE <> .CalGrid.dDateSelected
    		  If MONTH(this.w_POSTDATE) <> MONTH(.CalGrid.dDateSelected) Or YEAR(this.w_POSTDATE) <> YEAR(.CalGrid.dDateSelected)
            Local l_dDate
            l_dDate = Evl(.dCalDate, .CalGrid.dToday)
            .dCalDate = Gomonth(m.l_dDate,Month(this.w_POSTDATE)-Month(.CalGrid.dDateSelected)+(YEAR(this.w_POSTDATE)-YEAR(.CalGrid.dDateSelected))*12)
            If Not .CalHeader.bJustYear
              .CalHeader.nMon = MONTH(.dCalDate)
              .CalHeader.HeaderDate.MonthLbl.Caption = .CalHeader.aMonth[MONTH(.dCalDate)]
            Endif
            .CalHeader.nYear = YEAR(.dCalDate)
            .CalHeader.HeaderDate.YearLbl.Caption = Alltrim(Str(.CalHeader.nYear))
            .CalHeader.AdjustDate()
            
            .CalGrid.nCurrMonth = MONTH(.dCalDate)
            .CalGrid.nCurrYear = YEAR(.dCalDate)
            
            .CalGrid.nFirstDay=Date(Year(.dCalDate),Month(.dCalDate),1) - Dow(Date(Year(.dCalDate),Month(.dCalDate),1),2) + 1
            *--- Tolgo 7 gg per centrare il calendario e lasciare sempre una riga con il mese precedente e quello successivo
            .CalGrid.nFirstDay=Iif(Day(.CalGrid.nFirstDay) = 1,.CalGrid.nFirstDay-7,.CalGrid.nFirstDay)
            
            .CalGrid.CleanGrid()
            .CalGrid.Resize()
    		  Endif
          If Not .CalGrid.bYearView
            .CalGrid.SelectDate(.F.,DAY(this.w_POSTDATE))
          Else
            .CalGrid.dDateSelected = this.w_POSTDATE
          Endif
    		Endif
    	EndWith
    Endif
    
    If Lower(cevent)='w_ogrid dblclick' And This.w_Type='P'
      This.cAlertMsg = ""
      m.ggname = 'frm'+Alltrim(This.Name)
      m.l_oldError = On("Error")
      On Error &ggname..cAlertMsg=Message()
      
    	This.LockScreen = .F.
    	Do GSUT_KTD With this.w_Code
    	This.w_Calendar.CalGrid.CleanGrid()
      
      On error &l_oldError
      
    	This.NotifyEvent("ZoomRefresh")
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Calendar.Event(cEvent)
        if lower(cEvent)==lower("w_ogrid DblClick")
          .Calculate_GKBVNWACEG()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.CalcString.Event(cEvent)
      .oPgFrm.Page2.oPag.NextBtn.Event(cEvent)
      .oPgFrm.Page2.oPag.PrevBtn.Event(cEvent)
      .oPgFrm.Page2.oPag.oGrid.Event(cEvent)
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_oGrid after query")
          .Calculate_TVCJQUSHOG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_gcp
    If Lower(cevent) = "calendarupdate"
      This.w_Calendar.CalGrid.SelectDate(.f.,Day(This.w_POSTDATE))
    EndIf
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    this.o_ZOOM = this.w_ZOOM
    this.o_POSTDATE = this.w_POSTDATE
    return

enddefine

* --- Define pages as container
define class tgsut_gcpPag1 as StdContainer
  Width  = 226
  height = 267
  stdWidth  = 226
  stdheight = 267
  resizeXpos=52
  resizeYpos=90
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Calendar as cp_GadgetCalendar with uid="WVHWGTXZFO",left=2, top=1, width=225,height=243,;
    caption='Calendar',;
   bGlobalFont=.t.,;
    cEvent = "GadgetArranged,GadgetOnDemand,GadgetOnTimer,oheader RefreshOnDemand,CalendarUpdate",;
    nPag=1;
    , HelpContextID = 182263144


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=2, top=244, width=225,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 193805338
enddefine
define class tgsut_gcpPag2 as StdContainer
  Width  = 226
  height = 267
  stdWidth  = 226
  stdheight = 267
  resizeXpos=129
  resizeYpos=140
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CalcString as cp_calclbl with uid="JFRAKNOMZM",left=-3, top=10, width=234,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    alignment=2,fontname="Arial",caption="label text",fontsize=11,fontBold=.t.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.f.,Anchor=0,;
    nPag=2;
    , HelpContextID = 193805338


  add object NextBtn as NextCntCalendar with uid="BYLOAHJYPP",left=198, top=10, width=18,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 193805338


  add object PrevBtn as PrevCntCalendar with uid="SQOTBRXBGQ",left=12, top=10, width=18,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 193805338


  add object oGrid as cp_VirtualZoom with uid="DKKFDHJZBW",left=-2, top=33, width=226,height=235,;
    caption='Griglia',;
   bGlobalFont=.t.,;
    HotTrackingColor=16777215,HighLightColor=16777215,ScrollBarsWidth=8,bTrsHeaderTitle=.f.,HeaderFontBold=.t.,HeaderFontItalic=.f.,HeaderFontSize=0,;
    cEvent = "GadgetOnInit,ZoomRefresh,oheader RefreshOnDemand,CalendarUpdate",;
    nPag=2;
    , HelpContextID = 100351130
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gcp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_gcp
*--- Apertura postit al dblClick
Proc cp_OpenPostit(pParent, pCode, pStatus)
*--- Rimuovo il postit dallo zoom
  local oPostIt

  if i_ServerConn[1,2]<>0
    =cp_SQL(i_ServerConn[1,2],"select * from postit where code="+cp_ToStrODBC(pCode),"postit_c")
  else
    select * from postit where code=pCode into cursor postit_c
  endif  
  If VARTYPE(g_Alertmanager)<>'O' OR ( VARTYPE(i_bDisableNewPostIt)='L' And i_bDisableNewPostIt) or i_VisualTheme=-1
    m.oPostIt=createobject('postit','set',i_ServerConn[1,2], .null., .F., .T., .T.) &&Postit editabile, non integrabile, non linkato
    AddProperty(m.oPostIt,'cGadgetParent',pParent.Name)
    m.oPostIt.oPstCnt.cStatus = pStatus
  Else
    m.oPostIt = g_Alertmanager.AddAlert("cp_AdvPostIt", 'set', i_ServerConn[1,2], .F.)
    m.oPostIt.DoAlert()
    m.oPostIt = .Null.
  Endif  
  use in select("postit_c")
  if i_ServerConn[1,2]<>0
    =cp_SQL(i_ServerConn[1,2],"update postit set status="+cp_ToStrODBC(pStatus)+" where code="+cp_ToStrODBC(pCode))
  else
    update postit set status=pStatus where code=pCode
  endif
Endproc

*--- Apertura attivit� al dblClick
Proc cp_OpenActivity(pParent, pCode)
    OpenGest('A','GSAG_AAT','ATSERIAL',pCode)
Endproc

*--- Nuova attivit�
Proc cp_NewActivity(pParent)
    OpenGest('L','GSAG_AAT')
    Local obj
    obj = i_curform.GetCtrl("w_DATINI")
    obj.Value = pParent.w_POSTDATE
    obj.Valid()
    obj = .Null.
Endproc


*-- Ridefinizione Click del Footer, visualizzo cContextMenu
Proc cp_ContextByClick(cContextMenu)
    local loPopupMenu, nLeft, nTop, l_Item
    If Not Empty(m.cContextMenu)
       public i_PopupCurForm, oFooter, l_Iter
       oFooter = Sys(1270)
       Do While Type("m.oFooter")="O" And Type("m.oFooter.Parent")="O" And Type("m.oFooter.Parent.oContained")<>"O"
          oFooter = m.oFooter.Parent
       EndDo
       If Type("m.oFooter.Parent.oContained")="O" And !IsNull(m.oFooter.Parent.oContained)
         i_PopupCurForm = oFooter.Parent.oContained
         loPopupMenu = Createobject("cbPopupMenu")
         vu_context(m.cContextMenu, m.loPopupMenu)
         loPopupMenu.InitMenu(.T.)
         loPopupMenu.ShowMenu()
         release i_PopupCurForm
       Endif
       oFooter = .Null.
    Endif
EndProc

*-- Cambio data nella pagina 2
Proc ChangeDay(oObj,nNum)
   oObj.w_POSTDATE = oObj.w_POSTDATE + m.nNum
   oObj.SetControlsValue()
   oObj.w_CALCSTR = ALLTRIM(TRANSFORM(oObj.w_POSTDATE))
   oObj.oPgFrm.Page2.oPag.CalcString.Calculate(oObj.w_CALCSTR)
   oObj.w_INITIME = DTOT(oObj.w_POSTDATE)
   oObj.w_ENDTIME = DTOT(oObj.w_POSTDATE)+86399
   oObj.NotifyEvent("ZoomRefresh")
Endproc

*-- Bottone destro a pagina 2
Define class NextCntCalendar as SideTriangleCnt

nRotation = 90
MousePointer = 15

Proc Init()
  With This
    DoDefault()
    .oTriangle.ToolTipText = "Successivo"
    .oTriangle.MousePointer = 15
    .oTriangle.Move(Round((.Width-.oTriangle.Width)/2,0),Round((.Height-.oTriangle.Height)/2,0))
  EndWith
Endproc

Proc Event(cEvent)
Endproc

Proc Calculate()
Endproc

Proc Click()
    ChangeDay(thisform,1)
Endproc

Proc oTriangle.Click()
    ChangeDay(thisform,1)
Endproc

Enddefine


*-- Bottone sinistro a pagina 2
Define class PrevCntCalendar as SideTriangleCnt

nRotation = 270
MousePointer = 15

Proc Init()
  With This
    DoDefault()
    .oTriangle.ToolTipText = "Precedente"
    .oTriangle.MousePointer = 15
    .oTriangle.Move(Round((.Width-.oTriangle.Width)/2,0),Round((.Height-.oTriangle.Height)/2,0)+1)
  EndWith
Endproc

Proc Event(cEvent)
Endproc

Proc Calculate()
Endproc

Proc Click()
    ChangeDay(thisform,-1)
Endproc

Proc oTriangle.Click()
    ChangeDay(thisform,-1)
Endproc

Enddefine
* --- Fine Area Manuale
