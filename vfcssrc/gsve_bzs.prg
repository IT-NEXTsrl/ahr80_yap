* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bzs                                                        *
*              Controllo storno lettera di intento                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-28                                                      *
* Last revis.: 2009-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bzs",oParentObject)
return(i_retval)

define class tgsve_bzs as StdBatch
  * --- Local variables
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo storno lettera di intento (lancialo allo w_zoom selected )
    if isahr()
      if ALLTRIM(this.oParentObject.w_OLDRIF)<>ALLTRIM(this.oParentObject.w_MVRIFDIC)
        if .oParentObject.oparentobject.cFunction<>"Load"
          this.w_MESS = "Stornare manualmente l'importo utilizzato sulla dichiarazione di intento"
          ah_ErrorMsg(this.w_MESS)
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
