* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krw                                                        *
*              Wizard costruzione report                                       *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-22                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_krw",oParentObject))

* --- Class definition
define class tgsut_krw as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 787
  Height = 477+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=115950441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  WZMODREP_IDX = 0
  LINGUE_IDX = 0
  cPrg = "gsut_krw"
  cComment = "Wizard costruzione report"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MODELLO = space(10)
  o_MODELLO = space(10)
  w_FILLING = space(1)
  w_CUR_STEP = 0
  w_TOT_STEP = 0
  w_AREA = space(10)
  o_AREA = space(10)
  w_TITOLO = space(30)
  w_GDI = space(1)
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_CONFIG = space(10)
  o_CONFIG = space(10)
  w_LINGUA = space(3)
  w_AREAD = space(10)
  o_AREAD = space(10)
  w_AREAF = space(10)
  o_AREAF = space(10)
  w_AREAP = space(10)
  o_AREAP = space(10)
  w_PATHIMG = space(254)
  w_PATMOD = space(254)
  w_PATHREP = space(254)
  w_NEWREP = space(254)
  w_DESCLING = space(30)
  w_CODISOLING = space(3)
  w_CURCAMPI = space(10)
  o_CURCAMPI = space(10)
  w_OBJXFRX = .F.
  w_PATHREPORT = space(254)
  o_PATHREPORT = space(254)
  w_REPAPP = space(254)
  o_REPAPP = space(254)
  w_CURPARAM = space(10)
  o_CURPARAM = space(10)
  w_TABREPOR = space(10)
  w_CURSDATI = space(10)
  w_OLDMODEL = space(10)
  w_NOPARPAG = .F.

  * --- Children pointers
  GSUT_MMR = .NULL.
  GSUT_MMD = .NULL.
  GSUT_MMF = .NULL.
  GSUT_MMP = .NULL.
  w_GRUPFLDS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_REPIMG = .NULL.
  w_STEPS = .NULL.
  w_XFRXHEAD = .NULL.
  w_XFRXBODY = .NULL.
  w_XFRXRAGG = .NULL.
  w_XFRXFOOT = .NULL.
  w_XFRXPARA = .NULL.
  w_XFRXLAST = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_krw
  * Posiziono la finestra sempre al centro dell'applicativo
  Autocenter=.T.
  bNoRightClick=.T.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krwPag1","gsut_krw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgsut_krwPag2","gsut_krw",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(3).addobject("oPag","tgsut_krwPag3","gsut_krw",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Pag.3")
      .Pages(4).addobject("oPag","tgsut_krwPag4","gsut_krw",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pag.4")
      .Pages(5).addobject("oPag","tgsut_krwPag5","gsut_krw",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Pag.5")
      .Pages(6).addobject("oPag","tgsut_krwPag6","gsut_krw",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Pag.6")
      .Pages(7).addobject("oPag","tgsut_krwPag7","gsut_krw",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Pag.7")
      .Pages(7).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMODELLO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_krw
    this.parent.Closable = .F.
    * Nascondo le tabs in vecchia configurazione interfaccia
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_GRUPFLDS = this.oPgFrm.Pages(4).oPag.GRUPFLDS
    this.w_STEPS = this.oPgFrm.Pages(2).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(3).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(5).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(6).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(4).oPag.STEPS
    this.w_REPIMG = this.oPgFrm.Pages(1).oPag.REPIMG
    this.w_STEPS = this.oPgFrm.Pages(7).oPag.STEPS
    this.w_XFRXHEAD = this.oPgFrm.Pages(2).oPag.XFRXHEAD
    this.w_XFRXBODY = this.oPgFrm.Pages(3).oPag.XFRXBODY
    this.w_XFRXRAGG = this.oPgFrm.Pages(4).oPag.XFRXRAGG
    this.w_XFRXFOOT = this.oPgFrm.Pages(5).oPag.XFRXFOOT
    this.w_XFRXPARA = this.oPgFrm.Pages(6).oPag.XFRXPARA
    this.w_XFRXLAST = this.oPgFrm.Pages(7).oPag.XFRXLAST
    DoDefault()
    proc Destroy()
      this.w_GRUPFLDS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_REPIMG = .NULL.
      this.w_STEPS = .NULL.
      this.w_XFRXHEAD = .NULL.
      this.w_XFRXBODY = .NULL.
      this.w_XFRXRAGG = .NULL.
      this.w_XFRXFOOT = .NULL.
      this.w_XFRXPARA = .NULL.
      this.w_XFRXLAST = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='WZMODREP'
    this.cWorkTables[2]='LINGUE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_MMR = CREATEOBJECT('stdDynamicChild',this,'GSUT_MMR',this.oPgFrm.Page2.oPag.oLinkPC_2_7)
    this.GSUT_MMR.createrealchild()
    this.GSUT_MMD = CREATEOBJECT('stdDynamicChild',this,'GSUT_MMD',this.oPgFrm.Page3.oPag.oLinkPC_3_2)
    this.GSUT_MMD.createrealchild()
    this.GSUT_MMF = CREATEOBJECT('stdDynamicChild',this,'GSUT_MMF',this.oPgFrm.Page5.oPag.oLinkPC_5_2)
    this.GSUT_MMF.createrealchild()
    this.GSUT_MMP = CREATEOBJECT('stdDynamicChild',this,'GSUT_MMP',this.oPgFrm.Page6.oPag.oLinkPC_6_2)
    this.GSUT_MMP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MMR)
      this.GSUT_MMR.DestroyChildrenChain()
      this.GSUT_MMR=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_7')
    if !ISNULL(this.GSUT_MMD)
      this.GSUT_MMD.DestroyChildrenChain()
      this.GSUT_MMD=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_2')
    if !ISNULL(this.GSUT_MMF)
      this.GSUT_MMF.DestroyChildrenChain()
      this.GSUT_MMF=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_2')
    if !ISNULL(this.GSUT_MMP)
      this.GSUT_MMP.DestroyChildrenChain()
      this.GSUT_MMP=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_2')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MMR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSUT_MMD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSUT_MMF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSUT_MMP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MMR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSUT_MMD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSUT_MMF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSUT_MMP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MMR.NewDocument()
    this.GSUT_MMD.NewDocument()
    this.GSUT_MMF.NewDocument()
    this.GSUT_MMP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MMR.SetKey(;
            .w_SERIAL,"WRSERIAL";
            ,.w_CONFIG,"WRCONFIG";
            ,.w_AREA,"WR__AREA";
            )
      this.GSUT_MMD.SetKey(;
            .w_SERIAL,"WRSERIAL";
            ,.w_CONFIG,"WRCONFIG";
            ,.w_AREAD,"WR__AREA";
            )
      this.GSUT_MMF.SetKey(;
            .w_SERIAL,"WRSERIAL";
            ,.w_CONFIG,"WRCONFIG";
            ,.w_AREAF,"WR__AREA";
            )
      this.GSUT_MMP.SetKey(;
            .w_SERIAL,"WRSERIAL";
            ,.w_CONFIG,"WRCONFIG";
            ,.w_AREAP,"WR__AREA";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MMR.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"WRSERIAL";
             ,.w_CONFIG,"WRCONFIG";
             ,.w_AREA,"WR__AREA";
             )
      .WriteTo_GSUT_MMR()
      .GSUT_MMD.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"WRSERIAL";
             ,.w_CONFIG,"WRCONFIG";
             ,.w_AREAD,"WR__AREA";
             )
      .WriteTo_GSUT_MMD()
      .GSUT_MMF.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"WRSERIAL";
             ,.w_CONFIG,"WRCONFIG";
             ,.w_AREAF,"WR__AREA";
             )
      .WriteTo_GSUT_MMF()
      .GSUT_MMP.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"WRSERIAL";
             ,.w_CONFIG,"WRCONFIG";
             ,.w_AREAP,"WR__AREA";
             )
      .WriteTo_GSUT_MMP()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MMR)
        i_f=.GSUT_MMR.BuildFilter()
        if !(i_f==.GSUT_MMR.cQueryFilter)
          i_fnidx=.GSUT_MMR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MMR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MMR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MMR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MMR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSUT_MMD)
        i_f=.GSUT_MMD.BuildFilter()
        if !(i_f==.GSUT_MMD.cQueryFilter)
          i_fnidx=.GSUT_MMD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MMD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MMD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MMD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MMD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSUT_MMF)
        i_f=.GSUT_MMF.BuildFilter()
        if !(i_f==.GSUT_MMF.cQueryFilter)
          i_fnidx=.GSUT_MMF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MMF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MMF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MMF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MMF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSUT_MMP)
        i_f=.GSUT_MMP.BuildFilter()
        if !(i_f==.GSUT_MMP.cQueryFilter)
          i_fnidx=.GSUT_MMP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MMP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MMP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MMP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MMP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSUT_MMR()
  if at('gsut_mmr',lower(this.GSUT_MMR.class))<>0
    if this.GSUT_MMR.w_CURCAMPI<>this.w_CURCAMPI or this.GSUT_MMR.w_CURPARAM<>this.w_CURPARAM
      this.GSUT_MMR.w_CURCAMPI = this.w_CURCAMPI
      this.GSUT_MMR.w_CURPARAM = this.w_CURPARAM
      this.GSUT_MMR.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSUT_MMD()
  if at('gsut_mmd',lower(this.GSUT_MMD.class))<>0
    if this.GSUT_MMD.w_CURCAMPI<>this.w_CURCAMPI or this.GSUT_MMD.w_CURPARAM<>this.w_CURPARAM
      this.GSUT_MMD.w_CURCAMPI = this.w_CURCAMPI
      this.GSUT_MMD.w_CURPARAM = this.w_CURPARAM
      this.GSUT_MMD.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSUT_MMF()
  if at('gsut_mmf',lower(this.GSUT_MMF.class))<>0
    if this.GSUT_MMF.w_CURCAMPI<>this.w_CURCAMPI or this.GSUT_MMF.w_CURPARAM<>this.w_CURPARAM
      this.GSUT_MMF.w_CURCAMPI = this.w_CURCAMPI
      this.GSUT_MMF.w_CURPARAM = this.w_CURPARAM
      this.GSUT_MMF.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSUT_MMP()
  if at('gsut_mmp',lower(this.GSUT_MMP.class))<>0
    if this.GSUT_MMP.w_CURCAMPI<>this.w_CURCAMPI or this.GSUT_MMP.w_CURPARAM<>this.w_CURPARAM
      this.GSUT_MMP.w_CURCAMPI = this.w_CURCAMPI
      this.GSUT_MMP.w_CURPARAM = this.w_CURPARAM
      this.GSUT_MMP.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_MMR(.f.)
      this.Save_GSUT_MMD(.f.)
      this.Save_GSUT_MMF(.f.)
      this.Save_GSUT_MMP(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_MMR.IsAChildUpdated() or this.GSUT_MMD.IsAChildUpdated() or this.GSUT_MMF.IsAChildUpdated() or this.GSUT_MMP.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_MMR(i_ask)
    if this.GSUT_MMR.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MMR.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc
  proc Save_GSUT_MMD(i_ask)
    if this.GSUT_MMD.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MMD.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc
  proc Save_GSUT_MMF(i_ask)
    if this.GSUT_MMF.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MMF.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc
  proc Save_GSUT_MMP(i_ask)
    if this.GSUT_MMP.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MMP.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MODELLO=space(10)
      .w_FILLING=space(1)
      .w_CUR_STEP=0
      .w_TOT_STEP=0
      .w_AREA=space(10)
      .w_TITOLO=space(30)
      .w_GDI=space(1)
      .w_SERIAL=space(10)
      .w_CONFIG=space(10)
      .w_LINGUA=space(3)
      .w_AREAD=space(10)
      .w_AREAF=space(10)
      .w_AREAP=space(10)
      .w_PATHIMG=space(254)
      .w_PATMOD=space(254)
      .w_PATHREP=space(254)
      .w_NEWREP=space(254)
      .w_DESCLING=space(30)
      .w_CODISOLING=space(3)
      .w_CURCAMPI=space(10)
      .w_OBJXFRX=.f.
      .w_PATHREPORT=space(254)
      .w_REPAPP=space(254)
      .w_CURPARAM=space(10)
      .w_TABREPOR=space(10)
      .w_CURSDATI=space(10)
      .w_OLDMODEL=space(10)
      .w_NOPARPAG=.f.
      .w_PATHREPORT=oParentObject.w_PATHREPORT
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MODELLO))
          .link_1_1('Full')
        endif
        .w_FILLING = 'N'
        .w_CUR_STEP = 1
        .w_TOT_STEP = this.oPGFRM.PageCount - IIF( this.oParentObject.w_NOPARPAG, 1, 0)
        .w_AREA = 'Testata'
          .DoRTCalc(6,6,.f.)
        .w_GDI = 'S'
        .w_SERIAL = '0000000001'
        .w_CONFIG = space(15)
        .w_LINGUA = g_CODLIN
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_LINGUA))
          .link_2_6('Full')
        endif
      .GSUT_MMR.NewDocument()
      .GSUT_MMR.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREA,"WR__AREA")
      if not(.GSUT_MMR.bLoaded)
        .GSUT_MMR.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREA,"WR__AREA")
      endif
        .w_AREAD = 'Dettaglio'
      .GSUT_MMD.NewDocument()
      .GSUT_MMD.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAD,"WR__AREA")
      if not(.GSUT_MMD.bLoaded)
        .GSUT_MMD.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAD,"WR__AREA")
      endif
      .oPgFrm.Page4.oPag.GRUPFLDS.Calculate()
        .w_AREAF = 'Piede'
      .GSUT_MMF.NewDocument()
      .GSUT_MMF.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAF,"WR__AREA")
      if not(.GSUT_MMF.bLoaded)
        .GSUT_MMF.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAF,"WR__AREA")
      endif
        .w_AREAP = 'Parametri'
      .GSUT_MMP.NewDocument()
      .GSUT_MMP.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAP,"WR__AREA")
      if not(.GSUT_MMP.bLoaded)
        .GSUT_MMP.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAP,"WR__AREA")
      endif
      .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette colonne dettaglio e campi di testata",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento campi ed etichette del dettaglio", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette e campi di piede",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei parametri di selezione",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(AH_Msgformat("Questo wizard permette di abbinare un modello di report ai dati presenti nel cursore %0di lavoro."))
      .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei campi di raggruppamento",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
      .oPgFrm.Page1.oPag.REPIMG.Calculate(iif(empty(.w_PATHIMG),'bmp\EmptyRep.png',.w_PATHIMG))
          .DoRTCalc(14,16,.f.)
        .w_NEWREP = iif(empty(.w_REPAPP),.w_PATHREPORT,.w_REPAPP)
      .oPgFrm.Page7.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dati per la creazione del report",ALLTRIM(STR(.w_CUR_STEP-IIF(.w_NOPARPAG, 2, 1) )), ALLTRIM(STR(.w_TOT_STEP-1))),0)
          .DoRTCalc(18,19,.f.)
        .w_CURCAMPI = this.oParentObject.w_CURCAMPI
      .oPgFrm.Page2.oPag.XFRXHEAD.Calculate()
      .oPgFrm.Page3.oPag.XFRXBODY.Calculate()
      .oPgFrm.Page4.oPag.XFRXRAGG.Calculate()
      .oPgFrm.Page5.oPag.XFRXFOOT.Calculate()
      .oPgFrm.Page6.oPag.XFRXPARA.Calculate()
      .oPgFrm.Page7.oPag.XFRXLAST.Calculate()
          .DoRTCalc(21,23,.f.)
        .w_CURPARAM = this.oParentObject.w_CURPARAM
        .w_TABREPOR = SYS(2015)
        .w_CURSDATI = SYS(2015)
        .w_OLDMODEL = SPACE(10)
        .w_NOPARPAG = this.oParentObject.w_NOPARPAG
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_5.enabled = this.oPgFrm.Page4.oPag.oBtn_4_5.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_6.enabled = this.oPgFrm.Page5.oPag.oBtn_5_6.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_7.enabled = this.oPgFrm.Page7.oPag.oBtn_7_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_krw
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    WindowState=2
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_MMR.SetStatus(i_cOp)
    this.GSUT_MMD.SetStatus(i_cOp)
    this.GSUT_MMF.SetStatus(i_cOp)
    this.GSUT_MMP.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MMR.SetChildrenStatus(i_cOp)
  *  this.GSUT_MMD.SetChildrenStatus(i_cOp)
  *  this.GSUT_MMF.SetChildrenStatus(i_cOp)
  *  this.GSUT_MMP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PATHREPORT=.w_PATHREPORT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if  .o_CURCAMPI<>.w_CURCAMPI.or. .o_CURPARAM<>.w_CURPARAM
          .WriteTo_GSUT_MMR()
        endif
        if  .o_CURCAMPI<>.w_CURCAMPI.or. .o_CURPARAM<>.w_CURPARAM
          .WriteTo_GSUT_MMD()
        endif
        .oPgFrm.Page4.oPag.GRUPFLDS.Calculate()
        if  .o_CURCAMPI<>.w_CURCAMPI.or. .o_CURPARAM<>.w_CURPARAM
          .WriteTo_GSUT_MMF()
        endif
        if  .o_CURCAMPI<>.w_CURCAMPI.or. .o_CURPARAM<>.w_CURPARAM
          .WriteTo_GSUT_MMP()
        endif
        .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette colonne dettaglio e campi di testata",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento campi ed etichette del dettaglio", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette e campi di piede",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei parametri di selezione",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(AH_Msgformat("Questo wizard permette di abbinare un modello di report ai dati presenti nel cursore %0di lavoro."))
        .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei campi di raggruppamento",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page1.oPag.REPIMG.Calculate(iif(empty(.w_PATHIMG),'bmp\EmptyRep.png',.w_PATHIMG))
        .DoRTCalc(1,16,.t.)
        if .o_PATHREPORT<>.w_PATHREPORT.or. .o_REPAPP<>.w_REPAPP
            .w_NEWREP = iif(empty(.w_REPAPP),.w_PATHREPORT,.w_REPAPP)
        endif
        .oPgFrm.Page7.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dati per la creazione del report",ALLTRIM(STR(.w_CUR_STEP-IIF(.w_NOPARPAG, 2, 1) )), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page2.oPag.XFRXHEAD.Calculate()
        .oPgFrm.Page3.oPag.XFRXBODY.Calculate()
        .oPgFrm.Page4.oPag.XFRXRAGG.Calculate()
        .oPgFrm.Page5.oPag.XFRXFOOT.Calculate()
        .oPgFrm.Page6.oPag.XFRXPARA.Calculate()
        .oPgFrm.Page7.oPag.XFRXLAST.Calculate()
        if .o_MODELLO<>.w_MODELLO
          .Calculate_FOIEBHDIAJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_SERIAL<>.o_SERIAL or .w_CONFIG<>.o_CONFIG or .w_AREA<>.o_AREA
          .Save_GSUT_MMR(.t.)
          .GSUT_MMR.NewDocument()
          .GSUT_MMR.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREA,"WR__AREA")
          if not(.GSUT_MMR.bLoaded)
            .GSUT_MMR.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREA,"WR__AREA")
          endif
        endif
        if .w_SERIAL<>.o_SERIAL or .w_CONFIG<>.o_CONFIG or .w_AREAD<>.o_AREAD
          .Save_GSUT_MMD(.t.)
          .GSUT_MMD.NewDocument()
          .GSUT_MMD.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAD,"WR__AREA")
          if not(.GSUT_MMD.bLoaded)
            .GSUT_MMD.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAD,"WR__AREA")
          endif
        endif
        if .w_SERIAL<>.o_SERIAL or .w_CONFIG<>.o_CONFIG or .w_AREAF<>.o_AREAF
          .Save_GSUT_MMF(.t.)
          .GSUT_MMF.NewDocument()
          .GSUT_MMF.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAF,"WR__AREA")
          if not(.GSUT_MMF.bLoaded)
            .GSUT_MMF.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAF,"WR__AREA")
          endif
        endif
        if .w_SERIAL<>.o_SERIAL or .w_CONFIG<>.o_CONFIG or .w_AREAP<>.o_AREAP
          .Save_GSUT_MMP(.t.)
          .GSUT_MMP.NewDocument()
          .GSUT_MMP.ChangeRow('1',1,.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAP,"WR__AREA")
          if not(.GSUT_MMP.bLoaded)
            .GSUT_MMP.SetKey(.w_SERIAL,"WRSERIAL",.w_CONFIG,"WRCONFIG",.w_AREAP,"WR__AREA")
          endif
        endif
      endwith
      this.DoRTCalc(18,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page4.oPag.GRUPFLDS.Calculate()
        .oPgFrm.Page2.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette colonne dettaglio e campi di testata",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento campi ed etichette del dettaglio", ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page5.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento etichette e campi di piede",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page6.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei parametri di selezione",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(AH_Msgformat("Questo wizard permette di abbinare un modello di report ai dati presenti nel cursore %0di lavoro."))
        .oPgFrm.Page4.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dei campi di raggruppamento",ALLTRIM(STR(.w_CUR_STEP-1)), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page1.oPag.REPIMG.Calculate(iif(empty(.w_PATHIMG),'bmp\EmptyRep.png',.w_PATHIMG))
        .oPgFrm.Page7.oPag.STEPS.Calculate(AH_Msgformat("Passo %1 di %2 - Inserimento dati per la creazione del report",ALLTRIM(STR(.w_CUR_STEP-IIF(.w_NOPARPAG, 2, 1) )), ALLTRIM(STR(.w_TOT_STEP-1))),0)
        .oPgFrm.Page2.oPag.XFRXHEAD.Calculate()
        .oPgFrm.Page3.oPag.XFRXBODY.Calculate()
        .oPgFrm.Page4.oPag.XFRXRAGG.Calculate()
        .oPgFrm.Page5.oPag.XFRXFOOT.Calculate()
        .oPgFrm.Page6.oPag.XFRXPARA.Calculate()
        .oPgFrm.Page7.oPag.XFRXLAST.Calculate()
    endwith
  return

  proc Calculate_XZAKZPDXNE()
    with this
          * --- Caricamento campi zoom raggruppamenti
          GSUT_BRW(this;
              ,'FLDGROUP';
             )
    endwith
  endproc
  proc Calculate_OADBYXICHT()
    with this
          * --- Evidenzia campo anteprima report
          GSUT_BRW(this;
              ,'SELELE';
             )
    endwith
  endproc
  proc Calculate_FOIEBHDIAJ()
    with this
          * --- Verifiche su cambio modello
          .w_MODELLO = IIF(EMPTY(.w_OLDMODEL), .w_MODELLO, IIF(ah_YesNo("Il wizard del report � gi� stato avvioato, cambiando modello verranno azzerate tutte le impostagioni gi� effettuate.%0Continuare?"), .w_MODELLO, .w_OLDMODEL ) )
          .link_1_1('Full')
          gsut_brw(this;
              ,"CHKMOD";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_3.enabled = this.oPgFrm.Page3.oPag.oBtn_3_3.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_4.enabled = this.oPgFrm.Page3.oPag.oBtn_3_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_1.enabled = this.oPgFrm.Page4.oPag.oBtn_4_1.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_3.enabled = this.oPgFrm.Page4.oPag.oBtn_4_3.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_3.enabled = this.oPgFrm.Page5.oPag.oBtn_5_3.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_4.enabled = this.oPgFrm.Page5.oPag.oBtn_5_4.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_4.enabled = this.oPgFrm.Page6.oPag.oBtn_6_4.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_3.enabled = this.oPgFrm.Page7.oPag.oBtn_7_3.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_4.enabled = this.oPgFrm.Page7.oPag.oBtn_7_4.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_6.enabled = this.oPgFrm.Page7.oPag.oBtn_7_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_krw
    if lower(cEvent)=='done'
      iif(this.w_XFRXHEAD.pages.PageCount > 0,this.w_XFRXHEAD.Quit(),' ')
      iif(this.w_XFRXBODY.pages.PageCount > 0,this.w_XFRXBODY.Quit(),' ')
      iif(this.w_XFRXRAGG.pages.PageCount > 0,this.w_XFRXRAGG.Quit(), ' ')
      iif(this.w_XFRXFOOT.pages.PageCount > 0,this.w_XFRXFOOT.Quit(),' ')
      iif(this.w_XFRXPARA.pages.PageCount > 0,this.w_XFRXPARA.Quit(),' ')
      iif(this.w_XFRXLAST.pages.PageCount > 0,this.w_XFRXLAST.Quit(),' ')
    endif
    if lower(cEvent)=='edit aborted'
      this.oParentObject.w_PATHREPORT = ' '
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page4.oPag.GRUPFLDS.Event(cEvent)
      .oPgFrm.Page2.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page3.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page5.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page6.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page4.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page1.oPag.REPIMG.Event(cEvent)
      .oPgFrm.Page7.oPag.STEPS.Event(cEvent)
        if lower(cEvent)==lower("Activatepage 4")
          .Calculate_XZAKZPDXNE()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.XFRXHEAD.Event(cEvent)
      .oPgFrm.Page3.oPag.XFRXBODY.Event(cEvent)
      .oPgFrm.Page4.oPag.XFRXRAGG.Event(cEvent)
      .oPgFrm.Page5.oPag.XFRXFOOT.Event(cEvent)
      .oPgFrm.Page6.oPag.XFRXPARA.Event(cEvent)
      .oPgFrm.Page7.oPag.XFRXLAST.Event(cEvent)
        if lower(cEvent)==lower("CambioRiga")
          .Calculate_OADBYXICHT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MODELLO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.WZMODREP_IDX,3]
    i_lTable = "WZMODREP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.WZMODREP_IDX,2], .t., this.WZMODREP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.WZMODREP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODELLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSWZ_AMR',True,'WZMODREP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MRSERIAL like "+cp_ToStrODBC(trim(this.w_MODELLO)+"%");

          i_ret=cp_SQL(i_nConn,"select MRSERIAL,MRIMGMOD,MRPATMOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MRSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MRSERIAL',trim(this.w_MODELLO))
          select MRSERIAL,MRIMGMOD,MRPATMOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MRSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODELLO)==trim(_Link_.MRSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODELLO) and !this.bDontReportError
            deferred_cp_zoom('WZMODREP','*','MRSERIAL',cp_AbsName(oSource.parent,'oMODELLO_1_1'),i_cWhere,'GSWZ_AMR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MRSERIAL,MRIMGMOD,MRPATMOD";
                     +" from "+i_cTable+" "+i_lTable+" where MRSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MRSERIAL',oSource.xKey(1))
            select MRSERIAL,MRIMGMOD,MRPATMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODELLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MRSERIAL,MRIMGMOD,MRPATMOD";
                   +" from "+i_cTable+" "+i_lTable+" where MRSERIAL="+cp_ToStrODBC(this.w_MODELLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MRSERIAL',this.w_MODELLO)
            select MRSERIAL,MRIMGMOD,MRPATMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODELLO = NVL(_Link_.MRSERIAL,space(10))
      this.w_PATHIMG = NVL(_Link_.MRIMGMOD,space(254))
      this.w_PATMOD = NVL(_Link_.MRPATMOD,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_MODELLO = space(10)
      endif
      this.w_PATHIMG = space(254)
      this.w_PATMOD = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.WZMODREP_IDX,2])+'\'+cp_ToStr(_Link_.MRSERIAL,1)
      cp_ShowWarn(i_cKey,this.WZMODREP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODELLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LINGUA
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LINGUA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_LINGUA)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI,LUCODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_LINGUA))
          select LUCODICE,LUDESCRI,LUCODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LINGUA)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LINGUA) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oLINGUA_2_6'),i_cWhere,'GSAR_ALG',"Codici lingua",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI,LUCODISO";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI,LUCODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LINGUA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI,LUCODISO";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_LINGUA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_LINGUA)
            select LUCODICE,LUDESCRI,LUCODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LINGUA = NVL(_Link_.LUCODICE,space(3))
      this.w_DESCLING = NVL(_Link_.LUDESCRI,space(30))
      this.w_CODISOLING = NVL(_Link_.LUCODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_LINGUA = space(3)
      endif
      this.w_DESCLING = space(30)
      this.w_CODISOLING = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LINGUA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMODELLO_1_1.RadioValue()==this.w_MODELLO)
      this.oPgFrm.Page1.oPag.oMODELLO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILLING_1_2.RadioValue()==this.w_FILLING)
      this.oPgFrm.Page1.oPag.oFILLING_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTITOLO_2_2.value==this.w_TITOLO)
      this.oPgFrm.Page2.oPag.oTITOLO_2_2.value=this.w_TITOLO
    endif
    if not(this.oPgFrm.Page2.oPag.oGDI_2_3.RadioValue()==this.w_GDI)
      this.oPgFrm.Page2.oPag.oGDI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLINGUA_2_6.value==this.w_LINGUA)
      this.oPgFrm.Page2.oPag.oLINGUA_2_6.value=this.w_LINGUA
    endif
    if not(this.oPgFrm.Page7.oPag.oNEWREP_7_1.value==this.w_NEWREP)
      this.oPgFrm.Page7.oPag.oNEWREP_7_1.value=this.w_NEWREP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLING_2_18.value==this.w_DESCLING)
      this.oPgFrm.Page2.oPag.oDESCLING_2_18.value=this.w_DESCLING
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(lower(justext(.w_NEWREP)) == 'frx')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oNEWREP_7_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Estensione del file non ammessa")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSUT_MMR.CheckForm()
      if i_bres
        i_bres=  .GSUT_MMR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSUT_MMD.CheckForm()
      if i_bres
        i_bres=  .GSUT_MMD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSUT_MMF.CheckForm()
      if i_bres
        i_bres=  .GSUT_MMF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      *i_bRes = i_bRes .and. .GSUT_MMP.CheckForm()
      if i_bres
        i_bres=  .GSUT_MMP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MODELLO = this.w_MODELLO
    this.o_AREA = this.w_AREA
    this.o_SERIAL = this.w_SERIAL
    this.o_CONFIG = this.w_CONFIG
    this.o_AREAD = this.w_AREAD
    this.o_AREAF = this.w_AREAF
    this.o_AREAP = this.w_AREAP
    this.o_CURCAMPI = this.w_CURCAMPI
    this.o_PATHREPORT = this.w_PATHREPORT
    this.o_REPAPP = this.w_REPAPP
    this.o_CURPARAM = this.w_CURPARAM
    * --- GSUT_MMR : Depends On
    this.GSUT_MMR.SaveDependsOn()
    * --- GSUT_MMD : Depends On
    this.GSUT_MMD.SaveDependsOn()
    * --- GSUT_MMF : Depends On
    this.GSUT_MMF.SaveDependsOn()
    * --- GSUT_MMP : Depends On
    this.GSUT_MMP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_krwPag1 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=633
  resizeYpos=255
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMODELLO_1_1 as StdTableCombo with uid="ZOKXSKPEII",rtseq=1,rtrep=.f.,left=7,top=118,width=433,height=21;
    , HelpContextID = 169889990;
    , cFormVar="w_MODELLO",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="WZMODREP";
    , cTable='WZMODREP',cKey='MRSERIAL',cValue='MRDESCRI',cOrderBy='MRSERIAL',xDefault=space(10);
  , bGlobalFont=.t.


  func oMODELLO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODELLO_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oFILLING_1_2 as StdCheck with uid="SBZFWADUZC",rtseq=2,rtrep=.f.,left=7, top=149, caption="Completamento automatico valori testata, dettaglio e parametri report",;
    ToolTipText = "Abilita o meno il caricamento automatico del dettaglio e etichette di testata",;
    HelpContextID = 67646890,;
    cFormVar="w_FILLING", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILLING_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILLING_1_2.GetRadio()
    this.Parent.oContained.w_FILLING = this.RadioValue()
    return .t.
  endfunc

  func oFILLING_1_2.SetRadio()
    this.Parent.oContained.w_FILLING=trim(this.Parent.oContained.w_FILLING)
    this.value = ;
      iif(this.Parent.oContained.w_FILLING=='S',1,;
      0)
  endfunc


  add object oBtn_1_3 as StdButton with uid="IBRDXZOPMJ",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=1;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATMOD))
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="JUAVHGMTDZ",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=1;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="ABKYYMEJLO",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_9 as cp_calclbl with uid="UFNLBKNVLO",left=7, top=21, width=550,height=49,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 62047206


  add object REPIMG as cp_showimage with uid="ZVDHYBFZQE",left=568, top=4, width=213,height=473,;
    caption='REPIMG',;
   bGlobalFont=.t.,;
    default="Emptyrep.png",Enabled=.f.,stretch=1,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 87361302


  add object oBtn_1_25 as StdButton with uid="WRQYELTOQR",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="GTHFXDIIPM",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc

  add object oStr_1_11 as StdString with uid="WVUSAJNCDU",Visible=.t., Left=7, Top=94,;
    Alignment=0, Width=114, Height=18,;
    Caption="Seleziona il modello:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="URYVCGNFAN",left=10, top=421, width=280,height=2

  add object oBox_1_10 as StdBox with uid="DVBSPFWRMC",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag2 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=648
  resizeYpos=232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTITOLO_2_2 as StdField with uid="EYRFSRBWJW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TITOLO", cQueryName = "TITOLO",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Titolo del report wizard",;
    HelpContextID = 220941110,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=53, Top=38, InputMask=replicate('X',30)


  add object oGDI_2_3 as StdCombo with uid="RODQPEIPQW",rtseq=7,rtrep=.f.,left=314,top=38,width=99,height=21;
    , ToolTipText = "Consente di impostare l'uso o meno delle spcigiche GDI+";
    , HelpContextID = 115632794;
    , cFormVar="w_GDI",RowSource=""+"Attivo,"+"Non attivo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGDI_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    'N')))
  endfunc
  func oGDI_2_3.GetRadio()
    this.Parent.oContained.w_GDI = this.RadioValue()
    return .t.
  endfunc

  func oGDI_2_3.SetRadio()
    this.Parent.oContained.w_GDI=trim(this.Parent.oContained.w_GDI)
    this.value = ;
      iif(this.Parent.oContained.w_GDI=='S',1,;
      iif(this.Parent.oContained.w_GDI=='N',2,;
      0))
  endfunc

  add object oLINGUA_2_6 as StdField with uid="HDMLVONUHW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_LINGUA", cQueryName = "LINGUA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua assegnato al report wizard",;
    HelpContextID = 263383734,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=53, Top=64, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_LINGUA"

  func oLINGUA_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLINGUA_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLINGUA_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oLINGUA_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Codici lingua",'',this.parent.oContained
  endproc
  proc oLINGUA_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_LINGUA
     i_obj.ecpSave()
  endproc


  add object oLinkPC_2_7 as stdDynamicChildContainer with uid="CVBIMBFXVU",left=3, top=91, width=547, height=315, bOnScreen=.t.;



  add object oBtn_2_8 as StdButton with uid="OXWSHLFWQX",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=2;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_2_9 as StdButton with uid="OFBDBRBICW",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=2;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_10 as StdButton with uid="IBGLZEVYNM",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=2;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_2_11 as StdButton with uid="TDEMLPWGQB",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_12 as StdButton with uid="FPTOWIGFRA",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="KPTJALNOOY",left=13, top=11, width=440,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",FontBold=.t.,;
    nPag=2;
    , HelpContextID = 23370202

  add object oDESCLING_2_18 as StdField with uid="SAJWACHWPI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCLING", cQueryName = "DESCLING",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 119486077,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=116, Top=64, InputMask=replicate('X',30)


  add object XFRXHEAD as zcntxfrxmultipage with uid="EJREMJFPJM",left=568, top=4, width=213,height=473,;
    caption='XFRXHEAD',;
   bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 49555642

  add object oStr_2_16 as StdString with uid="WUWNIWLCMY",Visible=.t., Left=10, Top=41,;
    Alignment=1, Width=41, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="KXWDPYIFTV",Visible=.t., Left=4, Top=65,;
    Alignment=1, Width=47, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="CYJEXQWVEG",Visible=.t., Left=279, Top=41,;
    Alignment=1, Width=32, Height=18,;
    Caption="GDI:"  ;
  , bGlobalFont=.t.

  add object oBox_2_14 as StdBox with uid="QFMHNXZTPY",left=10, top=421, width=281,height=2

  add object oBox_2_15 as StdBox with uid="PCCXHOVRII",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag3 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=645
  resizeYpos=164
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_2 as stdDynamicChildContainer with uid="CYYTMBPKXW",left=3, top=91, width=547, height=315, bOnScreen=.t.;



  add object oBtn_3_3 as StdButton with uid="KFEDYUZQUB",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=3;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_3_4 as StdButton with uid="GHHBEBJDMM",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=3;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_3_5 as StdButton with uid="SMPZOXRECC",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_6 as StdButton with uid="KJJZMUCIKP",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="WVSVYMCMKH",left=13, top=11, width=408,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=3;
    , HelpContextID = 23370202


  add object XFRXBODY as zcntxfrxmultipage with uid="JHCFVNCOVW",left=568, top=4, width=213,height=473,;
    caption='XFRXBODY',;
   bGlobalFont=.t.,;
    nPag=3;
    , HelpContextID = 211036367


  add object oBtn_3_11 as StdButton with uid="CDZHIDIQHJ",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=3;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_11.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBox_3_8 as StdBox with uid="SFWNZERRDA",left=10, top=421, width=280,height=2

  add object oBox_3_9 as StdBox with uid="XSXBUONOEG",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag4 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=631
  resizeYpos=246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_4_1 as StdButton with uid="MUGAITKPGP",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=4;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_1.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object GRUPFLDS as cp_szoombox with uid="ICNTYSJQVQ",left=2, top=92, width=548,height=314,;
    caption='GRUPFLDS',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",cZoomFile="REPWIZ",bOptions=.f.,bAdvOptions=.f.,cTable="DETTFLD",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    nPag=4;
    , HelpContextID = 164389817


  add object oBtn_4_3 as StdButton with uid="IZXBTSLEIG",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=4;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_4_4 as StdButton with uid="RFVZKERCDF",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_5 as StdButton with uid="XPMWRIGDBP",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="CFKFHYHCTZ",left=13, top=11, width=453,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=4;
    , HelpContextID = 23370202


  add object XFRXRAGG as zcntxfrxmultipage with uid="VRECPIKKBN",left=568, top=4, width=213,height=473,;
    caption='XFRXRAGG',;
   bGlobalFont=.t.,;
    nPag=4;
    , HelpContextID = 7067459


  add object oBtn_4_11 as StdButton with uid="RWZTSTLZYZ",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=4;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_11.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBox_4_6 as StdBox with uid="ZZRFHTKWTJ",left=10, top=421, width=280,height=2

  add object oBox_4_8 as StdBox with uid="GFABMIPSLS",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag5 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=674
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_2 as stdDynamicChildContainer with uid="ZOODOCSHEY",left=3, top=91, width=547, height=315, bOnScreen=.t.;



  add object oBtn_5_3 as StdButton with uid="TLUTJVVGEW",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=5;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_5_4 as StdButton with uid="FXZHUNUMSV",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=5;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_5_5 as StdButton with uid="WRKYWKZHCV",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_6 as StdButton with uid="QEUPHKVQTX",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_6.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="YBVUIREWSR",left=13, top=11, width=430,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=5;
    , HelpContextID = 23370202


  add object XFRXFOOT as zcntxfrxmultipage with uid="XHFKZQOBLU",left=568, top=4, width=213,height=473,;
    caption='XFRXFOOT',;
   bGlobalFont=.t.,;
    nPag=5;
    , HelpContextID = 215230666


  add object oBtn_5_11 as StdButton with uid="BIWSIQVBYY",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=5;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_11.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBox_5_8 as StdBox with uid="LVAFXCMXRN",left=10, top=421, width=280,height=2

  add object oBox_5_9 as StdBox with uid="LWTNZPKYFQ",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag6 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=653
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_6_2 as stdDynamicChildContainer with uid="LJVTLEALSK",left=3, top=91, width=547, height=315, bOnScreen=.t.;



  add object oBtn_6_3 as StdButton with uid="TETDEFZTJF",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=6;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_6_4 as StdButton with uid="GQHIIYYKFA",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=6;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_6_5 as StdButton with uid="OSKGSKGFAU",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_6_6 as StdButton with uid="WKTZAXAJFS",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_6.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="SQMIVPMHYS",left=13, top=11, width=443,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=6;
    , HelpContextID = 23370202


  add object XFRXPARA as zcntxfrxmultipage with uid="BKNKYMAXSN",left=568, top=4, width=213,height=473,;
    caption='XFRXPARA',;
   bGlobalFont=.t.,;
    nPag=6;
    , HelpContextID = 259270839


  add object oBtn_6_11 as StdButton with uid="SXTEFOOFTL",left=125, top=428, width=48,height=45,;
    CpPicture="..\exe\bmp\refresh.bmp", caption="", nPag=6;
    , ToolTipText = "Aggiornamento anteprima";
    , HelpContextID = 41326695;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_11.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"AGGIORNA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBox_6_8 as StdBox with uid="QBDPTQDBTI",left=10, top=421, width=280,height=2

  add object oBox_6_9 as StdBox with uid="TUJBHJMQPS",left=563, top=4, width=4,height=473
enddefine
define class tgsut_krwPag7 as StdContainer
  Width  = 783
  height = 477
  stdWidth  = 783
  stdheight = 477
  resizeXpos=666
  resizeYpos=279
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNEWREP_7_1 as StdField with uid="FDREYNXXMM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NEWREP", cQueryName = "NEWREP",;
    bObbl = .f. , nPag = 7, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Estensione del file non ammessa",;
    ToolTipText = "Path e nome del nuovo report generato dal wizard",;
    HelpContextID = 230586070,;
   bGlobalFont=.t.,;
    Height=21, Width=423, Left=13, Top=73, InputMask=replicate('X',254)

  func oNEWREP_7_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (lower(justext(.w_NEWREP)) == 'frx')
    endwith
    return bRes
  endfunc


  add object oBtn_7_2 as StdButton with uid="PSLFIUFZAA",left=442, top=73, width=21,height=21,;
    caption="...", nPag=7;
    , HelpContextID = 115749418;
  , bGlobalFont=.t.

    proc oBtn_7_2.Click()
      with this.Parent.oContained
        .w_REPAPP=PUTFILE('Nome report','NewReport.frx','frx')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_3 as StdButton with uid="QTVLZSIZMV",left=19, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=7;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 204050443;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_3.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_7_4 as StdButton with uid="CMJKFJQAET",left=72, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=7;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 164316934;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_4.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_7_5 as StdButton with uid="YTCGXQFTSU",left=178, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ZOOM.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per eseguire lo zoom sull'anteprima del report";
    , HelpContextID = 108322410;
    , Caption='\<Zoom';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_5.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"ZOOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_6 as StdButton with uid="OMEIEJVQAB",left=125, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\SAVE.BMP", caption="", nPag=7;
    , ToolTipText = "Crea il report dal modello elaborato";
    , HelpContextID = 6036698;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_6.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"SAVE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NEWREP))
      endwith
    endif
  endfunc


  add object oBtn_7_7 as StdButton with uid="YSBAEBDXZE",left=231, top=428, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=7;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_7.Click()
      with this.Parent.oContained
        gsut_brw(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object STEPS as cp_calclbl with uid="SKFOQHVQDV",left=13, top=11, width=467,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=7;
    , HelpContextID = 23370202


  add object XFRXLAST as zcntxfrxmultipage with uid="TBVQZTGHIJ",left=568, top=4, width=213,height=473,;
    caption='XFRXLAST',;
   bGlobalFont=.t.,;
    nPag=7;
    , HelpContextID = 255076554

  add object oStr_7_11 as StdString with uid="RFNPCGBHTO",Visible=.t., Left=13, Top=53,;
    Alignment=0, Width=72, Height=18,;
    Caption="Nome report:"  ;
  , bGlobalFont=.t.

  add object oBox_7_9 as StdBox with uid="OICWEWEYKG",left=10, top=421, width=280,height=2

  add object oBox_7_10 as StdBox with uid="RFZCGTJVET",left=563, top=4, width=4,height=473
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
