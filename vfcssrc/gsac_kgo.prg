* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_kgo                                                        *
*              Generazione ordini da PDA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_102]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_kgo",oParentObject))

* --- Class definition
define class tgsac_kgo as StdForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 758
  Height = 451+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-10"
  HelpContextID=236296343
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=63

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  PDA_DETT_IDX = 0
  MAGAZZIN_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsac_kgo"
  cComment = "Generazione ordini da PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MVTIPCON = space(1)
  w_CODAZI = space(5)
  w_MVTIPDOC = space(5)
  w_MVCODESE = space(4)
  w_DESRIF = space(35)
  w_MVFLPROV = space(1)
  w_MVFLVEAC = space(1)
  w_MVPRD = space(2)
  w_MVNUMDOC = 0
  w_FLDATA = space(1)
  o_FLDATA = space(1)
  w_MVALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  o_DATDOC = ctod('  /  /  ')
  w_MVANNDOC = space(4)
  w_MVDATCIV = ctod('  /  /  ')
  w_DATDIV = ctod('  /  /  ')
  w_MVCLADOC = space(2)
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVTCAMAG = space(5)
  w_MVTFRAGG = space(1)
  w_MVCAUCON = space(5)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVNUMREG = 0
  w_MVDATREG = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FORINI = space(15)
  w_FORFIN = space(15)
  w_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_DATOBSO = ctod('  /  /  ')
  w_MVALFEST = space(10)
  w_MVPRP = space(2)
  w_MVNUMEST = 0
  w_FLPPRO = space(1)
  w_MVANNPRO = space(4)
  w_NUMSCO = 0
  w_MAGINI = space(5)
  w_DESMAGI = space(30)
  w_MAGFIN = space(5)
  w_DESMAGF = space(30)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_DESMAR = space(35)
  w_DESFOI = space(40)
  w_DESFOF = space(40)
  w_SELEZI = space(1)
  w_PRZVAC = space(1)
  w_MV_SEGNO = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_TIPATT = space(1)
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_kgoPag1","gsac_kgo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsac_kgoPag2","gsac_kgo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Lista PDA")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVTIPDOC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(2).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='ZONE'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='PDA_DETT'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='FAM_ARTI'
    this.cWorkTables[11]='GRUMERC'
    this.cWorkTables[12]='CATEGOMO'
    this.cWorkTables[13]='MARCHI'
    this.cWorkTables[14]='CAN_TIER'
    this.cWorkTables[15]='ATTIVITA'
    return(this.OpenAllTables(15))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVTIPCON=space(1)
      .w_CODAZI=space(5)
      .w_MVTIPDOC=space(5)
      .w_MVCODESE=space(4)
      .w_DESRIF=space(35)
      .w_MVFLPROV=space(1)
      .w_MVFLVEAC=space(1)
      .w_MVPRD=space(2)
      .w_MVNUMDOC=0
      .w_FLDATA=space(1)
      .w_MVALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_MVANNDOC=space(4)
      .w_MVDATCIV=ctod("  /  /  ")
      .w_DATDIV=ctod("  /  /  ")
      .w_MVCLADOC=space(2)
      .w_MVFLACCO=space(1)
      .w_MVFLINTE=space(1)
      .w_MVTCAMAG=space(5)
      .w_MVTFRAGG=space(1)
      .w_MVCAUCON=space(5)
      .w_MVSERIAL=space(10)
      .w_MVCODUTE=0
      .w_MVNUMREG=0
      .w_MVDATREG=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FORINI=space(15)
      .w_FORFIN=space(15)
      .w_ARTINI=space(20)
      .w_ARTFIN=space(20)
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVALFEST=space(10)
      .w_MVPRP=space(2)
      .w_MVNUMEST=0
      .w_FLPPRO=space(1)
      .w_MVANNPRO=space(4)
      .w_NUMSCO=0
      .w_MAGINI=space(5)
      .w_DESMAGI=space(30)
      .w_MAGFIN=space(5)
      .w_DESMAGF=space(30)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_DESMAR=space(35)
      .w_DESFOI=space(40)
      .w_DESFOF=space(40)
      .w_SELEZI=space(1)
      .w_PRZVAC=space(1)
      .w_MV_SEGNO=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_TIPATT=space(1)
        .w_MVTIPCON = 'F'
        .w_CODAZI = i_codazi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MVTIPDOC))
          .link_1_3('Full')
        endif
        .w_MVCODESE = g_CODESE
          .DoRTCalc(5,5,.f.)
        .w_MVFLPROV = 'N'
          .DoRTCalc(7,11,.f.)
        .w_DATDOC = i_DATSYS
        .w_MVANNDOC = STR(YEAR(.w_DATDOC), 4, 0)
        .w_MVDATCIV = .w_DATDOC
        .w_DATDIV = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
          .DoRTCalc(16,21,.f.)
        .w_MVSERIAL = SPACE(10)
        .w_MVCODUTE = IIF(g_MAGUTE='S', 0, i_CODUTE)
        .w_MVNUMREG = 0
        .w_MVDATREG = .w_DATDOC
        .DoRTCalc(26,28,.f.)
        if not(empty(.w_FORINI))
          .link_1_31('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_FORFIN))
          .link_1_32('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_ARTINI))
          .link_1_33('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_ARTFIN))
          .link_1_34('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_MVALFEST = '  '
        .w_MVPRP = 'NN'
        .w_MVNUMEST = 0
          .DoRTCalc(36,36,.f.)
        .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .DoRTCalc(38,39,.f.)
        if not(empty(.w_MAGINI))
          .link_1_48('Full')
        endif
        .DoRTCalc(40,41,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_51('Full')
        endif
        .DoRTCalc(42,45,.f.)
        if not(empty(.w_CODFAM))
          .link_1_59('Full')
        endif
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_CODGRU))
          .link_1_61('Full')
        endif
        .DoRTCalc(48,49,.f.)
        if not(empty(.w_CODCAT))
          .link_1_63('Full')
        endif
        .DoRTCalc(50,51,.f.)
        if not(empty(.w_CODMAR))
          .link_1_65('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_CODCOM))
          .link_1_66('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CODATT))
          .link_1_67('Full')
        endif
          .DoRTCalc(54,56,.f.)
        .w_SELEZI = "D"
      .oPgFrm.Page2.oPag.ZoomSel.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
          .DoRTCalc(58,59,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(61,62,.f.)
        .w_TIPATT = 'A'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_MVTIPCON = 'F'
            .w_CODAZI = i_codazi
        .DoRTCalc(3,3,.t.)
            .w_MVCODESE = g_CODESE
        .DoRTCalc(5,12,.t.)
            .w_MVANNDOC = STR(YEAR(.w_DATDOC), 4, 0)
        if .o_DATDOC<>.w_DATDOC
            .w_MVDATCIV = .w_DATDOC
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(15,24,.t.)
            .w_MVDATREG = .w_DATDOC
        .DoRTCalc(26,32,.t.)
            .w_MVALFEST = '  '
            .w_MVPRP = 'NN'
        .DoRTCalc(35,36,.t.)
            .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,63,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODATT_1_67.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_67.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATDOC_1_12.visible=!this.oPgFrm.Page1.oPag.oDATDOC_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCODCOM_1_66.visible=!this.oPgFrm.Page1.oPag.oCODCOM_1_66.mHide()
    this.oPgFrm.Page1.oPag.oCODATT_1_67.visible=!this.oPgFrm.Page1.oPag.oCODATT_1_67.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_3.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPDOC
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MVTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMVTIPDOC_1_3'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSAC_KGO.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDPRZVAC,TD_SEGNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_MVPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO = NVL(_Link_.TDFLPPRO,space(1))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
      this.w_PRZVAC = NVL(_Link_.TDPRZVAC,space(1))
      this.w_MV_SEGNO = NVL(_Link_.TD_SEGNO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_MVFLVEAC = space(1)
      this.w_MVALFDOC = space(10)
      this.w_MVPRD = space(2)
      this.w_MVCLADOC = space(2)
      this.w_MVFLACCO = space(1)
      this.w_MVFLINTE = space(1)
      this.w_MVTCAMAG = space(5)
      this.w_MVTFRAGG = space(1)
      this.w_MVCAUCON = space(5)
      this.w_FLPPRO = space(1)
      this.w_NUMSCO = 0
      this.w_PRZVAC = space(1)
      this.w_MV_SEGNO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVFLVEAC='A' AND .w_MVCLADOC = 'OR'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MVTIPDOC = space(5)
        this.w_DESRIF = space(35)
        this.w_MVFLVEAC = space(1)
        this.w_MVALFDOC = space(10)
        this.w_MVPRD = space(2)
        this.w_MVCLADOC = space(2)
        this.w_MVFLACCO = space(1)
        this.w_MVFLINTE = space(1)
        this.w_MVTCAMAG = space(5)
        this.w_MVTFRAGG = space(1)
        this.w_MVCAUCON = space(5)
        this.w_FLPPRO = space(1)
        this.w_NUMSCO = 0
        this.w_PRZVAC = space(1)
        this.w_MV_SEGNO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORINI
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_FORINI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORINI_1_31'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_FORINI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORINI = space(15)
      endif
      this.w_DESFOI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_FORFIN) OR  .w_FORINI<=.w_FORFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_FORINI = space(15)
        this.w_DESFOI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORFIN
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_FORFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORFIN_1_32'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_FORFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORFIN = space(15)
      endif
      this.w_DESFOF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_FORFIN>=.w_FORINI) or (empty(.w_FORINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_FORFIN = space(15)
        this.w_DESFOF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ARTINI))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_ARTINI)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oARTINI_1_33'),i_cWhere,'GSMA_ACA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARTINI)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_artfin)) OR  (.w_artini <= .w_artfin)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ARTFIN))
          select CACODICE,CADESART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_ARTFIN)+"%");

            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oARTFIN_1_34'),i_cWhere,'GSMA_ACA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARTFIN)
            select CACODICE,CADESART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_artini <= .w_artfin) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_48'),i_cWhere,'GSAR_AMA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_51'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_59'),i_cWhere,'GSAR_AFA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_61'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_63'),i_cWhere,'GSAR_AOM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_65'),i_cWhere,'GSAR_AMH',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_66'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_67'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOC_1_3.value==this.w_MVTIPDOC)
      this.oPgFrm.Page1.oPag.oMVTIPDOC_1_3.value=this.w_MVTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_5.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_5.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLPROV_1_6.RadioValue()==this.w_MVFLPROV)
      this.oPgFrm.Page1.oPag.oMVFLPROV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_9.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_9.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATA_1_10.RadioValue()==this.w_FLDATA)
      this.oPgFrm.Page1.oPag.oFLDATA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_11.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_11.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_12.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_12.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIV_1_15.value==this.w_DATDIV)
      this.oPgFrm.Page1.oPag.oDATDIV_1_15.value=this.w_DATDIV
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_29.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_29.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_30.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_30.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFORINI_1_31.value==this.w_FORINI)
      this.oPgFrm.Page1.oPag.oFORINI_1_31.value=this.w_FORINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFORFIN_1_32.value==this.w_FORFIN)
      this.oPgFrm.Page1.oPag.oFORFIN_1_32.value=this.w_FORFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARTINI_1_33.value==this.w_ARTINI)
      this.oPgFrm.Page1.oPag.oARTINI_1_33.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTFIN_1_34.value==this.w_ARTFIN)
      this.oPgFrm.Page1.oPag.oARTFIN_1_34.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_48.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_48.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_49.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_49.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_51.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_51.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_52.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_52.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_55.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_55.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_56.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_56.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_59.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_59.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_60.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_60.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_61.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_61.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_62.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_62.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_63.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_63.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_64.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_64.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_65.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_65.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_66.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_66.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_67.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_67.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_68.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_68.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOI_1_75.value==this.w_DESFOI)
      this.oPgFrm.Page1.oPag.oDESFOI_1_75.value=this.w_DESFOI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOF_1_76.value==this.w_DESFOF)
      this.oPgFrm.Page1.oPag.oDESFOF_1_76.value=this.w_DESFOF
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_1.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_83.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_83.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_85.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_85.value=this.w_DESATT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MVTIPDOC)) or not(.w_MVFLVEAC='A' AND .w_MVCLADOC = 'OR'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPDOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MVTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   (empty(.w_MVNUMDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNUMDOC_1_9.SetFocus()
            i_bnoObbl = !empty(.w_MVNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATDOC))  and not(.w_FLDATA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDOC_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATDIV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATDIV_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATDIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_FORFIN) OR  .w_FORINI<=.w_FORFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FORINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORINI_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_FORFIN>=.w_FORINI) or (empty(.w_FORINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FORFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORFIN_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_artfin)) OR  (.w_artini <= .w_artfin)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTINI_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((.w_artini <= .w_artfin) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTFIN_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale oppure obsoleto")
          case   not(.w_MAGINI <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLDATA = this.w_FLDATA
    this.o_DATDOC = this.w_DATDOC
    return

enddefine

* --- Define pages as container
define class tgsac_kgoPag1 as StdContainer
  Width  = 754
  height = 451
  stdWidth  = 754
  stdheight = 451
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVTIPDOC_1_3 as StdField with uid="HJNMKCAPBA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC", cQueryName = "MVTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale documenti di acquisto",;
    HelpContextID = 144428023,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=171, Top=18, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC"

  func oMVTIPDOC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPDOC_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPDOC_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMVTIPDOC_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSAC_KGO.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMVTIPDOC_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MVTIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESRIF_1_5 as StdField with uid="NZLUHJPELA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117632458,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=251, Top=18, InputMask=replicate('X',35)


  add object oMVFLPROV_1_6 as StdCombo with uid="AQKGHLCJVS",rtseq=6,rtrep=.f.,left=569,top=18,width=88,height=21;
    , HelpContextID = 177843172;
    , cFormVar="w_MVFLPROV",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMVFLPROV_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oMVFLPROV_1_6.GetRadio()
    this.Parent.oContained.w_MVFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oMVFLPROV_1_6.SetRadio()
    this.Parent.oContained.w_MVFLPROV=trim(this.Parent.oContained.w_MVFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLPROV=='N',1,;
      iif(this.Parent.oContained.w_MVFLPROV=='S',2,;
      0))
  endfunc

  add object oMVNUMDOC_1_9 as StdField with uid="ZXXUTHULLA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero di ordine da generare disponibile",;
    HelpContextID = 146811895,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=171, Top=42, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oFLDATA_1_10 as StdCheck with uid="HWVCBRLZTE",rtseq=10,rtrep=.f.,left=397, top=42, caption="Data ordine PDA",;
    ToolTipText = "Se � attivo: la data generazione � la stessa dell'ordine",;
    HelpContextID = 191157930,;
    cFormVar="w_FLDATA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDATA_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDATA_1_10.GetRadio()
    this.Parent.oContained.w_FLDATA = this.RadioValue()
    return .t.
  endfunc

  func oFLDATA_1_10.SetRadio()
    this.Parent.oContained.w_FLDATA=trim(this.Parent.oContained.w_FLDATA)
    this.value = ;
      iif(this.Parent.oContained.w_FLDATA=='S',1,;
      0)
  endfunc

  add object oMVALFDOC_1_11 as StdField with uid="LGHDPDHADI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 154794999,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=302, Top=42, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oDATDOC_1_12 as StdField with uid="SRXRGALYSN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 162587082,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=171, Top=66

  func oDATDOC_1_12.mHide()
    with this.Parent.oContained
      return (.w_FLDATA='S')
    endwith
  endfunc

  add object oDATDIV_1_15 as StdField with uid="WNWCHHHDLF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATDIV", cQueryName = "DATDIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 118546890,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=171, Top=91


  add object oObj_1_18 as cp_runprogram with uid="XPIOQBGYPF",left=0, top=476, width=465,height=17,;
    caption='GSVE_BS3',;
   bGlobalFont=.t.,;
    prg="GSAC_BS3",;
    cEvent = "w_MVALFDOC Changed,w_MVTIPDOC Changed,w_DATDOC Changed",;
    nPag=1;
    , HelpContextID = 162508647

  add object oDATINI_1_29 as StdField with uid="MOIVDUQLAC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio evasione di selezione (vuota=nessuna selezione)",;
    HelpContextID = 62644682,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=126, Top=154

  add object oDATFIN_1_30 as StdField with uid="ZMEBYTICOU",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine evasione di selezione (vuota=nessuna selezione)",;
    HelpContextID = 252633546,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=126, Top=178

  add object oFORINI_1_31 as StdField with uid="MTCSFFGEUP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_FORINI", cQueryName = "FORINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice fornitore di inizio selezione (spazio=no selezione)",;
    HelpContextID = 62649258,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=301, Top=154, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORINI"

  func oFORINI_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORINI_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORINI_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORINI_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORINI_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORINI
     i_obj.ecpSave()
  endproc

  add object oFORFIN_1_32 as StdField with uid="ZDRYRXMPVK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_FORFIN", cQueryName = "FORFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice fornitore di fine selezione (spazio=no selezione)",;
    HelpContextID = 252638122,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=301, Top=178, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORFIN"

  func oFORFIN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORFIN_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORFIN_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORFIN_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORFIN_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORFIN
     i_obj.ecpSave()
  endproc

  add object oARTINI_1_33 as StdField with uid="OHLMOGUFZM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice di ricerca iniziale",;
    HelpContextID = 62640378,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=126, Top=206, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARTINI"

  func oARTINI_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oARTINI_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Articoli",'',this.parent.oContained
  endproc
  proc oARTINI_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oARTFIN_1_34 as StdField with uid="ZLWAAQCTLT",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice di ricerca finale",;
    HelpContextID = 252629242,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=126, Top=230, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oARTFIN_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Articoli",'',this.parent.oContained
  endproc
  proc oARTFIN_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oMAGINI_1_48 as StdField with uid="EYLJBHJXTT",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 62697786,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=257, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"",'',this.parent.oContained
  endproc
  proc oMAGINI_1_48.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGINI
     i_obj.ecpSave()
  endproc

  add object oDESMAGI_1_49 as StdField with uid="VQLQRAKWTT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158863926,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=257, InputMask=replicate('X',30)

  add object oMAGFIN_1_51 as StdField with uid="WAYLPIMHRX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 252686650,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_51.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESMAGF_1_52 as StdField with uid="ZZVPSYCFIR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158863926,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=281, InputMask=replicate('X',30)

  add object oDESINI_1_55 as StdField with uid="GYHWGWGEHN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 62647754,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=301, Top=206, InputMask=replicate('X',40)

  add object oDESFIN_1_56 as StdField with uid="UNISYYDMZD",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 252636618,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=301, Top=230, InputMask=replicate('X',40)

  add object oCODFAM_1_59 as StdField with uid="DJATLFPXYP",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia",;
    HelpContextID = 9425882,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=308, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"",'',this.parent.oContained
  endproc
  proc oCODFAM_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_60 as StdField with uid="LAALSSYEUT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 9366986,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=308, InputMask=replicate('X',35)

  add object oCODGRU_1_61 as StdField with uid="AYHDNWUFZS",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 125752282,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=332, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_61('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_61.ecpDrop(oSource)
    this.Parent.oContained.link_1_61('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_61.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_61'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oCODGRU_1_61.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_62 as StdField with uid="SJBBBAPJFP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 125693386,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=332, InputMask=replicate('X',35)

  add object oCODCAT_1_63 as StdField with uid="WDRJJPKVVW",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 160617434,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=356, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_63.ecpDrop(oSource)
    this.Parent.oContained.link_1_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_63'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"",'',this.parent.oContained
  endproc
  proc oCODCAT_1_63.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_64 as StdField with uid="WBVEZRXNDG",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 160558538,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=356, InputMask=replicate('X',35)

  add object oCODMAR_1_65 as StdField with uid="UVXDVHJDBA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 193516506,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=380, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"",'',this.parent.oContained
  endproc
  proc oCODMAR_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oCODCOM_1_66 as StdField with uid="OHCXLYKMDQ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente",;
    ToolTipText = "Commessa",;
    HelpContextID = 263377882,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=126, Top=405, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_66.mHide()
    with this.Parent.oContained
      return ( g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  func oCODCOM_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_66('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_67('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_66.ecpDrop(oSource)
    this.Parent.oContained.link_1_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_66.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_66'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_67 as StdField with uid="TZMOGDJAHP",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� incongruente",;
    ToolTipText = "Attivit�",;
    HelpContextID = 140825562,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=126, Top=428, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_67.mHide()
    with this.Parent.oContained
      return ( g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  func oCODATT_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDESMAR_1_68 as StdField with uid="LQLRFAASKQ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193457610,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=195, Top=380, InputMask=replicate('X',35)

  add object oDESFOI_1_75 as StdField with uid="LYMDHOVTWY",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESFOI", cQueryName = "DESFOI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61795786,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=440, Top=154, InputMask=replicate('X',40)

  add object oDESFOF_1_76 as StdField with uid="EHZNHLTTIA",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESFOF", cQueryName = "DESFOF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112127434,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=440, Top=178, InputMask=replicate('X',40)


  add object oBtn_1_77 as StdButton with uid="XAOFRYAIID",left=651, top=405, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza lista PDA che soddisfano le selezioni impostate";
    , HelpContextID = 123652330;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      with this.Parent.oContained
        GSAC_BG1(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_78 as StdButton with uid="KYQESPJULE",left=702, top=405, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243613766;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_78.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_83 as StdField with uid="OKRUEJONSV",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263318986,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=253, Top=405, InputMask=replicate('X',30)

  add object oDESATT_1_85 as StdField with uid="WMKTLSOVSA",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 140766666,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=253, Top=428, InputMask=replicate('X',30)

  add object oStr_1_16 as StdString with uid="HOVDNAQJWB",Visible=.t., Left=290, Top=42,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="LTTIBSJSQS",Visible=.t., Left=10, Top=92,;
    Alignment=1, Width=158, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XFEDUGSZQH",Visible=.t., Left=51, Top=15,;
    Alignment=1, Width=117, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="JNOBILKAHU",Visible=.t., Left=39, Top=42,;
    Alignment=1, Width=129, Height=15,;
    Caption="Primo numero ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="FQFFGXFEAF",Visible=.t., Left=60, Top=66,;
    Alignment=1, Width=104, Height=15,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_FLDATA='S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="LMCMMPUTDJ",Visible=.t., Left=19, Top=155,;
    Alignment=1, Width=102, Height=15,;
    Caption="Da data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="TJQOEXMQYX",Visible=.t., Left=28, Top=179,;
    Alignment=1, Width=93, Height=15,;
    Caption="A data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="FCVIZXEPDZ",Visible=.t., Left=22, Top=258,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="XJHAUJHTHB",Visible=.t., Left=38, Top=282,;
    Alignment=1, Width=83, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="MILPAIGFEA",Visible=.t., Left=9, Top=128,;
    Alignment=0, Width=72, Height=15,;
    Caption="Selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="MLEWVPUOBT",Visible=.t., Left=59, Top=204,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="RNYZISDVNT",Visible=.t., Left=61, Top=228,;
    Alignment=1, Width=60, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="BVKDGBZXYK",Visible=.t., Left=70, Top=309,;
    Alignment=1, Width=51, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="HAUJBFAIUE",Visible=.t., Left=12, Top=333,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="MMLNXZPOOY",Visible=.t., Left=11, Top=357,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="NWIDCOFXVN",Visible=.t., Left=85, Top=381,;
    Alignment=1, Width=36, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="CJOPSWMXJT",Visible=.t., Left=216, Top=155,;
    Alignment=1, Width=82, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="HYKDQQLEYI",Visible=.t., Left=228, Top=179,;
    Alignment=1, Width=70, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="KXLMVYGVLS",Visible=.t., Left=519, Top=18,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="KSGESHCIFC",Visible=.t., Left=45, Top=428,;
    Alignment=1, Width=76, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="TGCQLLKMBK",Visible=.t., Left=45, Top=405,;
    Alignment=1, Width=76, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_53 as StdBox with uid="YFQPZHWVVJ",left=4, top=143, width=745,height=0
enddefine
define class tgsac_kgoPag2 as StdContainer
  Width  = 754
  height = 451
  stdWidth  = 754
  stdheight = 451
  resizeXpos=516
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELEZI_2_1 as StdRadio with uid="IRSHIEQNET",rtseq=57,rtrep=.f.,left=18, top=369, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 50355418
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 50355418
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_1.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_1.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_1.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object ZoomSel as cp_szoombox with uid="SBJCPVQPFT",left=2, top=3, width=754,height=359,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PDA_DETT",cZoomFile="GSAC_KGA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 122576922


  add object oObj_2_3 as cp_runprogram with uid="NRQAUDFRKG",left=1, top=471, width=147,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BG1("SS")',;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 122576922


  add object oObj_2_4 as cp_runprogram with uid="XMCEVCMXML",left=156, top=473, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAC_BG1("INTERROGA")',;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 122576922


  add object oBtn_2_5 as StdButton with uid="USGWYZFUVG",left=652, top=366, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 153949049;
    , Caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        do GSAC_BGO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_6 as StdButton with uid="QYSTWSNJYZ",left=703, top=366, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243613766;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_kgo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
