* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bka                                                        *
*              Controlli finali agg analitica                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-11                                                      *
* Last revis.: 2014-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bka",oParentObject)
return(i_retval)

define class tgsca_bka as StdBatch
  * --- Local variables
  w_MVVOCCEN = space(15)
  w_MVCODCEN = space(15)
  w_MVCODATT = space(15)
  w_MVCODCOM = space(15)
  w_MESS = space(100)
  w_TRIG = 0
  w_OK = .f.
  w_NR = space(6)
  w_CCOBSO = ctod("  /  /  ")
  w_FLCOCO = space(1)
  w_FLORCO = space(1)
  w_COSTO = space(5)
  w_PADRE = .NULL.
  w_ATTI = space(1)
  * --- WorkFile variables
  CENCOST_idx=0
  VOC_COST_idx=0
  DOC_DETT_idx=0
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCA_MAA  maschera aggiornamento dati analitica 
    *     nei documenti ai Controlli Finali (ChekcForm)
    this.w_PADRE = this.oParentObject
    this.w_OK = .T.
    this.w_TRIG = 0
    this.w_PADRE.MarkPos()     
    ah_Msg("Controlli finali...",.T.)
     
 Select ( this.w_PADRE.cTrsName ) 
 Go Top
    * --- Cicla sulle Righe del Documento
    SCAN FOR t_CPROWORD<>0 AND NVL(t_MVIMPNAZ,0)<>0
    this.w_TRIG = this.w_TRIG + 1
    this.w_NR = ALLTRIM(STR(t_CPROWORD))
    * --- Controllo Presenza dati Analitica 
    this.w_MVCODCEN = NVL(t_MVCODCEN, SPACE(15))
    this.w_MVVOCCEN = NVL(t_MVVOCCEN, SPACE(15))
    this.w_MVCODCOM = NVL(t_MVCODCOM, SPACE(15))
    this.w_MVCODATT = NVL(t_MVCODATT, SPACE(15))
    * --- Controllo Voce di Costo \ Ricavo
    if this.w_OK And (g_PERCCR="S" AND this.oParentObject.w_FLANAL="S") OR (g_COMM="S" AND this.oParentObject.w_FLGCOM="S" )
      do case
        case EMPTY(this.w_MVVOCCEN)
          this.w_MESS = "Verificare riga n.: %1 codice voce di costo non valorizzata"
          this.w_OK = .F.
        case EMPTY(this.w_MVCODCEN) AND g_PERCCR="S" AND this.oParentObject.w_FLANAL="S"
          this.w_MESS = "Verificare riga n.: %1 codice centro di costo non valorizzato"
          this.w_OK = .F.
        case EMPTY(this.w_MVCODCOM) AND g_COMM="S" AND this.oParentObject.w_FLGCOM="S"
          this.w_MESS = "Verificare riga n.: %1 codice commessa non valorizzato"
          this.w_OK = .F.
      endcase
    endif
    if this.w_OK And NOT EMPTY(this.w_MVVOCCEN)
      * --- Read from VOC_COST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOC_COST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VCDTOBSO"+;
          " from "+i_cTable+" VOC_COST where ";
              +"VCCODICE = "+cp_ToStrODBC(this.w_MVVOCCEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VCDTOBSO;
          from (i_cTable) where;
              VCCODICE = this.w_MVVOCCEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCOBSO = NVL(cp_ToDate(_read_.VCDTOBSO),cp_NullValue(_read_.VCDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_CCOBSO) AND (this.w_CCOBSO<=this.oParentObject.w_OBTEST)
        this.w_MESS = "Voce di costo o ricavo obsoleta su riga %1"
        this.w_OK = .F.
      endif
    endif
    * --- Controllo Centri di Costo \ Ricavo
    if this.w_OK And NOT EMPTY(this.w_MVCODCEN)
      * --- Read from CENCOST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CENCOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDTOBSO"+;
          " from "+i_cTable+" CENCOST where ";
              +"CC_CONTO = "+cp_ToStrODBC(this.w_MVCODCEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDTOBSO;
          from (i_cTable) where;
              CC_CONTO = this.w_MVCODCEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_CCOBSO) AND (this.w_CCOBSO<=this.oParentObject.w_OBTEST)
        this.w_MESS = "Centro di costo o ricavo obsoleto su riga %1"
        this.w_OK = .F.
      endif
    endif
    * --- Controllo Inserita Attivita' di Commessa
    if this.w_OK AND g_COMM="S" AND this.oParentObject.w_FLGCOM="S" AND EMPTY(this.w_MVCODATT) AND NOT EMPTY(this.w_MVCODCOM)
      this.w_MESS = "Verificare riga n. %1 attivit� di commessa non definita"
      this.w_OK = .F.
    endif
    if Not this.w_OK
      * --- Errore!
      EXIT
    endif
    ENDSCAN
    if this.w_OK AND this.w_TRIG=0
      this.w_MESS = "Registrazione senza righe di dettaglio"
      this.w_OK = .F.
    endif
    WAIT CLEAR
    this.w_PADRE.RePos()     
    if Not this.w_OK
      * --- Segnalo a video e disattivo la CheckForm
      ah_ErrorMsg(this.w_MESS,"!","", this.w_NR)
      this.oParentObject.w_CHKRES = this.w_OK
      i_retcode = 'stop'
      return
    endif
    * --- Aggiorno la tabella MA_COSTI solo se � attiva la gestione progetti
    if g_COMM="S" AND this.oParentObject.w_FLGCOM="S"
      this.w_PADRE.MarkPos()     
       
 Select ( this.w_PADRE.cTrsName ) 
 Go Top
      SCAN FOR t_CPROWORD<>0 and NVL(t_MVIMPNAZ,0)<>0 and (t_MVCODCOM<>t_CODCOM Or t_MVCODATT<>t_CODATT Or t_MVVOCCEN<>t_VOCCEN)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVFLCOCO,MVFLORCO"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(t_CPROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(t_MVNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVFLCOCO,MVFLORCO;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MVSERIAL;
              and CPROWNUM = t_CPROWNUM;
              and MVNUMRIF = t_MVNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
        this.w_FLORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MA_COSTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2],.t.,this.MA_COSTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CSCODCOS"+;
          " from "+i_cTable+" MA_COSTI where ";
              +"CSCODCOM = "+cp_ToStrODBC(t_MVCODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(t_TIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(t_MVCODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(t_CODCOS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CSCODCOS;
          from (i_cTable) where;
              CSCODCOM = t_MVCODCOM;
              and CSTIPSTR = t_TIPATT;
              and CSCODMAT = t_MVCODATT;
              and CSCODCOS = t_CODCOS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COSTO = NVL(cp_ToDate(_read_.CSCODCOS),cp_NullValue(_read_.CSCODCOS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_COSTO)
        this.w_ATTI = alltrim(t_TIPATT)
        * --- Try
        local bErr_00E9CAF8
        bErr_00E9CAF8=bTrsErr
        this.Try_00E9CAF8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00E9CAF8
        * --- End
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCOCO,'CSCONSUN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLORCO,'CSORDIN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
          +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(t_MVCODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(t_TIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(t_MVCODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(t_CODCOS);
                 )
        else
          update (i_cTable) set;
              CSCONSUN = &i_cOp1.;
              ,CSORDIN = &i_cOp2.;
              &i_ccchkf. ;
           where;
              CSCODCOM = t_MVCODCOM;
              and CSTIPSTR = t_TIPATT;
              and CSCODMAT = t_MVCODATT;
              and CSCODCOS = t_CODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCOCO,'CSCONSUN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLORCO,'CSORDIN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
          +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(t_MVCODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(t_TIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(t_MVCODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(t_CODCOS);
                 )
        else
          update (i_cTable) set;
              CSCONSUN = &i_cOp1.;
              ,CSORDIN = &i_cOp2.;
              &i_ccchkf. ;
           where;
              CSCODCOM = t_MVCODCOM;
              and CSTIPSTR = t_TIPATT;
              and CSCODMAT = t_MVCODATT;
              and CSCODCOS = t_CODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      do case
        case this.w_FLCOCO = "+"
          this.w_FLCOCO = "-"
        case this.w_FLCOCO = "-"
          this.w_FLCOCO = "+"
      endcase
      do case
        case this.w_FLORCO = "+"
          this.w_FLORCO = "-"
        case this.w_FLORCO = "-"
          this.w_FLORCO = "+"
      endcase
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLCOCO,'CSCONSUN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLORCO,'CSORDIN','t_MVIMPNAZ',t_MVIMPNAZ,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
        +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(t_CODCOM);
            +" and CSTIPSTR = "+cp_ToStrODBC(t_OTIPATT);
            +" and CSCODMAT = "+cp_ToStrODBC(t_CODATT);
            +" and CSCODCOS = "+cp_ToStrODBC(t_OCODCOS);
               )
      else
        update (i_cTable) set;
            CSCONSUN = &i_cOp1.;
            ,CSORDIN = &i_cOp2.;
            &i_ccchkf. ;
         where;
            CSCODCOM = t_CODCOM;
            and CSTIPSTR = t_OTIPATT;
            and CSCODMAT = t_CODATT;
            and CSCODCOS = t_OCODCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from MA_COSTI
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CSCODCOM = "+cp_ToStrODBC(t_CODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(t_OTIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(t_CODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(t_OCODCOS);
              +" and CSPREVEN = "+cp_ToStrODBC(0);
              +" and CSORDIN = "+cp_ToStrODBC(0);
              +" and CSCONSUN = "+cp_ToStrODBC(0);
               )
      else
        delete from (i_cTable) where;
              CSCODCOM = t_CODCOM;
              and CSTIPSTR = t_OTIPATT;
              and CSCODMAT = t_CODATT;
              and CSCODCOS = t_OCODCOS;
              and CSPREVEN = 0;
              and CSORDIN = 0;
              and CSCONSUN = 0;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ENDSCAN
      this.w_PADRE.RePos()     
    endif
  endproc
  proc Try_00E9CAF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(t_MVCODCOM),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATTI),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(t_MVCODATT),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(t_CODCOS),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',t_MVCODCOM,'CSTIPSTR',this.w_ATTI,'CSCODMAT',t_MVCODATT,'CSCODCOS',t_CODCOS)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           t_MVCODCOM;
           ,this.w_ATTI;
           ,t_MVCODATT;
           ,t_CODCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='MA_COSTI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
