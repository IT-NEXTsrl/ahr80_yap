* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_kdr                                                        *
*              Documenti dati di riga                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-01                                                      *
* Last revis.: 2013-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_kdr",oParentObject))

* --- Class definition
define class tgscp_kdr as StdForm
  Top    = 46
  Left   = 43

  * --- Standard Properties
  Width  = 647
  Height = 385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-02-06"
  HelpContextID=82417001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  LISTINI_IDX = 0
  VOCIIVA_IDX = 0
  CON_TRAM_IDX = 0
  cPrg = "gscp_kdr"
  cComment = "Documenti dati di riga"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ORCODLIS = space(5)
  w_ORSCOIN1 = 0
  w_ORSCOIN2 = 0
  w_ORSCOIN3 = 0
  w_ORSCOIN4 = 0
  w_DESLIS = space(40)
  w_ORDESSUP = space(0)
  w_ORCODIVA = space(5)
  w_CPROWORD = 0
  w_ORDATEVA = ctod('  /  /  ')
  w_ORCONTRA = space(15)
  w_DESIVA = space(35)
  w_DESCONT = space(50)
  w_ORFLOMAG = space(1)
  w_ORSCONT1 = 0
  w_ORSCONT2 = 0
  w_ORSCONT3 = 0
  w_ORSCONT4 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_kdrPag1","gscp_kdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='CON_TRAM'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ORCODLIS=space(5)
      .w_ORSCOIN1=0
      .w_ORSCOIN2=0
      .w_ORSCOIN3=0
      .w_ORSCOIN4=0
      .w_DESLIS=space(40)
      .w_ORDESSUP=space(0)
      .w_ORCODIVA=space(5)
      .w_CPROWORD=0
      .w_ORDATEVA=ctod("  /  /  ")
      .w_ORCONTRA=space(15)
      .w_DESIVA=space(35)
      .w_DESCONT=space(50)
      .w_ORFLOMAG=space(1)
      .w_ORSCONT1=0
      .w_ORSCONT2=0
      .w_ORSCONT3=0
      .w_ORSCONT4=0
      .w_ORCODLIS=oParentObject.w_ORCODLIS
      .w_ORSCOIN1=oParentObject.w_ORSCOIN1
      .w_ORSCOIN2=oParentObject.w_ORSCOIN2
      .w_ORSCOIN3=oParentObject.w_ORSCOIN3
      .w_ORSCOIN4=oParentObject.w_ORSCOIN4
      .w_ORDESSUP=oParentObject.w_ORDESSUP
      .w_ORCODIVA=oParentObject.w_ORCODIVA
      .w_CPROWORD=oParentObject.w_CPROWORD
      .w_ORDATEVA=oParentObject.w_ORDATEVA
      .w_ORCONTRA=oParentObject.w_ORCONTRA
      .w_ORFLOMAG=oParentObject.w_ORFLOMAG
      .w_ORSCONT1=oParentObject.w_ORSCONT1
      .w_ORSCONT2=oParentObject.w_ORSCONT2
      .w_ORSCONT3=oParentObject.w_ORSCONT3
      .w_ORSCONT4=oParentObject.w_ORSCONT4
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ORCODLIS))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,8,.f.)
        if not(empty(.w_ORCODIVA))
          .link_1_11('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_ORCONTRA))
          .link_1_18('Full')
        endif
    endwith
    this.DoRTCalc(12,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ORCODLIS=.w_ORCODLIS
      .oParentObject.w_ORSCOIN1=.w_ORSCOIN1
      .oParentObject.w_ORSCOIN2=.w_ORSCOIN2
      .oParentObject.w_ORSCOIN3=.w_ORSCOIN3
      .oParentObject.w_ORSCOIN4=.w_ORSCOIN4
      .oParentObject.w_ORDESSUP=.w_ORDESSUP
      .oParentObject.w_ORCODIVA=.w_ORCODIVA
      .oParentObject.w_CPROWORD=.w_CPROWORD
      .oParentObject.w_ORDATEVA=.w_ORDATEVA
      .oParentObject.w_ORCONTRA=.w_ORCONTRA
      .oParentObject.w_ORFLOMAG=.w_ORFLOMAG
      .oParentObject.w_ORSCONT1=.w_ORSCONT1
      .oParentObject.w_ORSCONT2=.w_ORSCONT2
      .oParentObject.w_ORSCONT3=.w_ORSCONT3
      .oParentObject.w_ORSCONT4=.w_ORSCONT4
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
          .link_1_11('Full')
        .DoRTCalc(9,10,.t.)
          .link_1_18('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ORCODLIS
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ORCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ORCODLIS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORCODIVA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ORCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ORCODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ORCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORCONTRA
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CONUMERO,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_ORCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CONUMERO',this.w_ORCONTRA)
            select CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORCONTRA = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCONT = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ORCONTRA = space(15)
      endif
      this.w_DESCONT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oORCODLIS_1_1.value==this.w_ORCODLIS)
      this.oPgFrm.Page1.oPag.oORCODLIS_1_1.value=this.w_ORCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCOIN1_1_4.value==this.w_ORSCOIN1)
      this.oPgFrm.Page1.oPag.oORSCOIN1_1_4.value=this.w_ORSCOIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCOIN2_1_6.value==this.w_ORSCOIN2)
      this.oPgFrm.Page1.oPag.oORSCOIN2_1_6.value=this.w_ORSCOIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCOIN3_1_7.value==this.w_ORSCOIN3)
      this.oPgFrm.Page1.oPag.oORSCOIN3_1_7.value=this.w_ORSCOIN3
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCOIN4_1_8.value==this.w_ORSCOIN4)
      this.oPgFrm.Page1.oPag.oORSCOIN4_1_8.value=this.w_ORSCOIN4
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_9.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_9.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oORDESSUP_1_10.value==this.w_ORDESSUP)
      this.oPgFrm.Page1.oPag.oORDESSUP_1_10.value=this.w_ORDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oORCODIVA_1_11.value==this.w_ORCODIVA)
      this.oPgFrm.Page1.oPag.oORCODIVA_1_11.value=this.w_ORCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_13.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_13.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oORDATEVA_1_16.value==this.w_ORDATEVA)
      this.oPgFrm.Page1.oPag.oORDATEVA_1_16.value=this.w_ORDATEVA
    endif
    if not(this.oPgFrm.Page1.oPag.oORCONTRA_1_18.value==this.w_ORCONTRA)
      this.oPgFrm.Page1.oPag.oORCONTRA_1_18.value=this.w_ORCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_20.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_20.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONT_1_21.value==this.w_DESCONT)
      this.oPgFrm.Page1.oPag.oDESCONT_1_21.value=this.w_DESCONT
    endif
    if not(this.oPgFrm.Page1.oPag.oORFLOMAG_1_22.RadioValue()==this.w_ORFLOMAG)
      this.oPgFrm.Page1.oPag.oORFLOMAG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT1_1_28.value==this.w_ORSCONT1)
      this.oPgFrm.Page1.oPag.oORSCONT1_1_28.value=this.w_ORSCONT1
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT2_1_33.value==this.w_ORSCONT2)
      this.oPgFrm.Page1.oPag.oORSCONT2_1_33.value=this.w_ORSCONT2
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT3_1_34.value==this.w_ORSCONT3)
      this.oPgFrm.Page1.oPag.oORSCONT3_1_34.value=this.w_ORSCONT3
    endif
    if not(this.oPgFrm.Page1.oPag.oORSCONT4_1_35.value==this.w_ORSCONT4)
      this.oPgFrm.Page1.oPag.oORSCONT4_1_35.value=this.w_ORSCONT4
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscp_kdrPag1 as StdContainer
  Width  = 643
  height = 385
  stdWidth  = 643
  stdheight = 385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oORCODLIS_1_1 as StdField with uid="BEDJOXZPWZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ORCODLIS", cQueryName = "ORCODLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino applicato",;
    HelpContextID = 195686969,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=111, Top=17, InputMask=replicate('X',5), cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ORCODLIS"

  func oORCODLIS_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oORSCOIN1_1_4 as StdField with uid="KQSRKNBMRE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ORSCOIN1", cQueryName = "ORSCOIN1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 112266729,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=112, Top=281

  add object oORSCOIN2_1_6 as StdField with uid="RVBNKYKPEM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ORSCOIN2", cQueryName = "ORSCOIN2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 112266728,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=182, Top=281

  add object oORSCOIN3_1_7 as StdField with uid="ICPLVIWQJT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ORSCOIN3", cQueryName = "ORSCOIN3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 112266727,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=252, Top=281

  add object oORSCOIN4_1_8 as StdField with uid="APWRZAHAGL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORSCOIN4", cQueryName = "ORSCOIN4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^ % Maggiorazione (se positiva) sconto (se negativo) di riga",;
    HelpContextID = 112266726,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=322, Top=281

  add object oDESLIS_1_9 as StdField with uid="FSDAOQCVKJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione listino",;
    HelpContextID = 218635210,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=163, Top=17, InputMask=replicate('X',40)

  add object oORDESSUP_1_10 as StdMemo with uid="SOUXZUDUGO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ORDESSUP", cQueryName = "ORDESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note di riga",;
    HelpContextID = 59769398,;
   bGlobalFont=.t.,;
    Height=161, Width=213, Left=405, Top=141, Enabled=.T., ReadOnly=.T.

  add object oORCODIVA_1_11 as StdField with uid="PBEIABCUPE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ORCODIVA", cQueryName = "ORCODIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA articolo",;
    HelpContextID = 145355303,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=111, Top=44, InputMask=replicate('X',5), cLinkFile="VOCIIVA", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ORCODIVA"

  func oORCODIVA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCPROWORD_1_13 as StdField with uid="YRMFZWXFLN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 2433174,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=549, Top=351

  add object oORDATEVA_1_16 as StdField with uid="VIUVGYJXXW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ORDATEVA", cQueryName = "ORDATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione riga",;
    HelpContextID = 94110247,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=111, Top=172

  add object oORCONTRA_1_18 as StdField with uid="FJXGFPRLVF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ORCONTRA", cQueryName = "ORCONTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto applicato",;
    HelpContextID = 71954983,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=111, Top=71, InputMask=replicate('X',15), cLinkFile="CON_TRAM", oKey_1_1="CONUMERO", oKey_1_2="this.w_ORCONTRA"

  func oORCONTRA_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESIVA_1_20 as StdField with uid="YSDIHVKYUD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione IVA articolo",;
    HelpContextID = 238754762,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=163, Top=44, InputMask=replicate('X',35)

  add object oDESCONT_1_21 as StdField with uid="EJLRNWBCGI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCONT", cQueryName = "DESCONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contratto",;
    HelpContextID = 240051254,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=230, Top=71, InputMask=replicate('X',50)


  add object oORFLOMAG_1_22 as StdCombo with uid="DLXJQKZOHJ",rtseq=14,rtrep=.f.,left=111,top=140,width=182,height=22, enabled=.f.;
    , ToolTipText = "Riga omaggio";
    , HelpContextID = 44621267;
    , cFormVar="w_ORFLOMAG",RowSource=""+"Sconto merce,"+"Imponibile,"+"Imponibile+IVA,"+"No omaggio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORFLOMAG_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'E',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oORFLOMAG_1_22.GetRadio()
    this.Parent.oContained.w_ORFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oORFLOMAG_1_22.SetRadio()
    this.Parent.oContained.w_ORFLOMAG=trim(this.Parent.oContained.w_ORFLOMAG)
    this.value = ;
      iif(this.Parent.oContained.w_ORFLOMAG=='S',1,;
      iif(this.Parent.oContained.w_ORFLOMAG=='I',2,;
      iif(this.Parent.oContained.w_ORFLOMAG=='E',3,;
      iif(this.Parent.oContained.w_ORFLOMAG=='X',4,;
      0))))
  endfunc

  add object oORSCONT1_1_28 as StdField with uid="DLPPNGOOOJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ORSCONT1", cQueryName = "ORSCONT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ % Maggiorazione (se positiva) sconto (se negativo) ordine web di riga",;
    HelpContextID = 240054807,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=112, Top=236

  add object oORSCONT2_1_33 as StdField with uid="HYTUPCEBQF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ORSCONT2", cQueryName = "ORSCONT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ % Maggiorazione (se positiva) sconto (se negativo) ordine web di riga",;
    HelpContextID = 240054808,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=182, Top=236

  add object oORSCONT3_1_34 as StdField with uid="OACUQOYNHS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ORSCONT3", cQueryName = "ORSCONT3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^ % Maggiorazione (se positiva) sconto (se negativo) ordine web di riga",;
    HelpContextID = 240054809,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=252, Top=236

  add object oORSCONT4_1_35 as StdField with uid="ZITMHPCBXF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ORSCONT4", cQueryName = "ORSCONT4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^ % Maggiorazione (se positiva) sconto (se negativo) ordine web di riga",;
    HelpContextID = 240054810,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=322, Top=236

  add object oStr_1_2 as StdString with uid="TOJKPJCNRO",Visible=.t., Left=57, Top=21,;
    Alignment=1, Width=52, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="PZMKZSACRY",Visible=.t., Left=366, Top=143,;
    Alignment=0, Width=35, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EBHBQVUZWX",Visible=.t., Left=112, Top=263,;
    Alignment=0, Width=262, Height=18,;
    Caption="Sconti/maggiorazioni applicati in ordine web"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GQAGUAYTTY",Visible=.t., Left=11, Top=48,;
    Alignment=1, Width=98, Height=18,;
    Caption="Cod. IVA articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WOOHYHNSNJ",Visible=.t., Left=547, Top=328,;
    Alignment=1, Width=36, Height=18,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FDWCKLEGGE",Visible=.t., Left=51, Top=176,;
    Alignment=1, Width=58, Height=18,;
    Caption="Evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QBQENJJEVN",Visible=.t., Left=41, Top=75,;
    Alignment=1, Width=68, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AIYQSDUXTB",Visible=.t., Left=55, Top=144,;
    Alignment=1, Width=54, Height=18,;
    Caption="Omaggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="HONUCSOSXY",Visible=.t., Left=169, Top=285,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="YLZGQOGPEP",Visible=.t., Left=240, Top=285,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JGAXPLIYDT",Visible=.t., Left=310, Top=285,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BYXXVKTBBL",Visible=.t., Left=112, Top=218,;
    Alignment=0, Width=279, Height=18,;
    Caption="Sconti/maggiorazioni determinati dalla procedura"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LQHFOQVZSK",Visible=.t., Left=169, Top=238,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YCPRSWZJGY",Visible=.t., Left=240, Top=238,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="IGVBRZFCPD",Visible=.t., Left=310, Top=238,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="DDCFCDWDIV",left=535, top=323, width=76,height=53

  add object oBox_1_27 as StdBox with uid="SEAHMXYGWT",left=112, top=123, width=265,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_kdr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
