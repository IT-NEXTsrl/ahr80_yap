* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bel                                                        *
*              Messaggi elaborazione                                           *
*                                                                              *
*      Author: POLLINA FABRIZIO                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-20                                                      *
* Last revis.: 2016-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE,pCALLER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bel",oParentObject,m.pAZIONE,m.pCALLER)
return(i_retval)

define class tgsut_bel as StdBatch
  * --- Local variables
  pAZIONE = space(5)
  pCALLER = space(2)
  w_NomeFile = space(254)
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice che identifica il chiamante (se passato):
    *     
    *     LL : Log ad hoc launcher
    if Vartype(this.pCALLER)<>"C"
      this.pCALLER = ""
    else
      this.pCALLER = Alltrim(this.pCALLER)
    endif
    do case
      case this.pAzione=="SAVE"
        this.w_NomeFile = Iif(this.pCALLER=="LL", PUTFILE("","AdHocLauncher.LOG","LOG|TXT"), PUTFILE("","Elaborazione.LOG","LOG|TXT"))
        if NOT EMPTY(this.w_NomeFile)
          FH=FCREATE(this.w_NomeFile)
          if FH>0
            FW=FPUTS(FH, Ah_MsgFormat("File di log %1 %2", Iif(this.pCALLER=="LL", "ad hoc launcher", "messaggi elaborazione"), IIF(isAlt(), "Alterego Top",g_APPLICATION)+ " " + TTOC(DATETIME())) )
            if FW>0
              FW=FPUTS(FH, REPL("*", 80) , LEN(ALLTRIM(this.oParentObject.w_MSG)))
            endif
            if FW>0
              FW=FPUTS(FH, ALLTRIM(this.oParentObject.w_MSG), LEN(ALLTRIM(this.oParentObject.w_MSG)))
            endif
            FCLOSE(FH)
            if FW<0
              ah_ErrorMsg("Impossibile scrivere informazioni sul file", 48)
            else
              ah_ErrorMsg("Informazioni salvate con successo", 64)
            endif
          else
            ah_ErrorMsg("Impossibile creare file", 48)
          endif
        endif
      case this.pAzione=="PRINT"
        this.w_oERRORLOG.AddMsgLogNoTranslate(ALLTRIM( this.oParentObject.w_MSG ))     
        this.w_oERRORLOG.PrintLog(this.oParentObject, Iif(this.pCALLER=="LL", "ad hoc launcher", "Messaggi elaborazione"), .F.)     
        this.w_oERRORLOG = .NULL.
      case this.pAzione=="INIT"
        * --- Variabile globale che punta alla maschera di log,
        *     usata da chi deve scriverci per sapere se la maschera � effettivamente aperta
        if Vartype(i_AhlLogViewer)="U"
          Public i_AhlLogViewer
        endif
        i_AhlLogViewer = This.oParentObject
        * --- Messaggi di inizializzazione log
        if Vartype(g_DDEServerName)="C" And !Empty(g_DDEServerName)
          addmsgnl("Server in ascolto [%1]", i_AhlLogViewer, g_DDEServerName)
        else
          addmsgnl("Nessun server in ascolto", i_AhlLogViewer)
        endif
        addmsgnl("%0********************************************************************************%0", i_AhlLogViewer)
      case this.pAzione=="DONE"
        * --- Alla chiusura della maschera rilascio la variabile globale che la riferisce
        i_AhlLogViewer = .null. 
 Release i_AhlLogViewer
    endcase
  endproc


  proc Init(oParentObject,pAZIONE,pCALLER)
    this.pAZIONE=pAZIONE
    this.pCALLER=pCALLER
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE,pCALLER"
endproc
