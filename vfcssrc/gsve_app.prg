* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_app                                                        *
*              Parametri provvigioni                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_78]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2013-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_app")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_app")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_app")
  return

* --- Class definition
define class tgsve_app as StdPCForm
  Width  = 644
  Height = 392+35
  Top    = 1
  Left   = 34
  cComment = "Parametri provvigioni"
  cPrg = "gsve_app"
  HelpContextID=108383639
  add object cnt as tcgsve_app
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_app as PCContext
  w_PPCODAZI = space(5)
  w_PPCODIVA = space(5)
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPPREAZI = 0
  w_PPPREAGE = 0
  w_PPMAXESC = 0
  w_PPMINESC = 0
  w_PPMAXNES = 0
  w_PPMINNES = 0
  w_PPPERASS = 0
  w_PPSOKAGE = 0
  w_PPIMPIN1 = 0
  w_PPPERFI1 = 0
  w_PPIMPIN2 = 0
  w_PPPERFI2 = 0
  w_PPPERFI3 = 0
  w_PPIMPPL1 = 0
  w_PPPERPL1 = 0
  w_PPIMPPL2 = 0
  w_PPPERPL2 = 0
  w_PPPERPL3 = 0
  w_DATOBSO = space(8)
  w_OBTEST = space(8)
  w_DESAPP = space(35)
  w_PPCALSCO = space(1)
  w_PPMATCAP = space(1)
  w_PPPOSENA = space(8)
  w_PPNUMTR1 = space(4)
  w_PPNUMTR2 = space(4)
  w_PPNUMTR3 = space(4)
  w_PPNUMTR4 = space(4)
  w_PPGENFIR = space(1)
  w_PPNUMTR5 = space(4)
  w_PPDIRGEN = space(254)
  w_PPANPREV = space(4)
  w_PPTRIPRE = space(1)
  w_PPANFIRR = space(4)
  w_PPPROGEN = space(1)
  w_PPLISRIF = space(5)
  proc Save(oFrom)
    this.w_PPCODAZI = oFrom.w_PPCODAZI
    this.w_PPCODIVA = oFrom.w_PPCODIVA
    this.w_PPCALPRO = oFrom.w_PPCALPRO
    this.w_OLDCALPRO = oFrom.w_OLDCALPRO
    this.w_PPPREAZI = oFrom.w_PPPREAZI
    this.w_PPPREAGE = oFrom.w_PPPREAGE
    this.w_PPMAXESC = oFrom.w_PPMAXESC
    this.w_PPMINESC = oFrom.w_PPMINESC
    this.w_PPMAXNES = oFrom.w_PPMAXNES
    this.w_PPMINNES = oFrom.w_PPMINNES
    this.w_PPPERASS = oFrom.w_PPPERASS
    this.w_PPSOKAGE = oFrom.w_PPSOKAGE
    this.w_PPIMPIN1 = oFrom.w_PPIMPIN1
    this.w_PPPERFI1 = oFrom.w_PPPERFI1
    this.w_PPIMPIN2 = oFrom.w_PPIMPIN2
    this.w_PPPERFI2 = oFrom.w_PPPERFI2
    this.w_PPPERFI3 = oFrom.w_PPPERFI3
    this.w_PPIMPPL1 = oFrom.w_PPIMPPL1
    this.w_PPPERPL1 = oFrom.w_PPPERPL1
    this.w_PPIMPPL2 = oFrom.w_PPIMPPL2
    this.w_PPPERPL2 = oFrom.w_PPPERPL2
    this.w_PPPERPL3 = oFrom.w_PPPERPL3
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DESAPP = oFrom.w_DESAPP
    this.w_PPCALSCO = oFrom.w_PPCALSCO
    this.w_PPMATCAP = oFrom.w_PPMATCAP
    this.w_PPPOSENA = oFrom.w_PPPOSENA
    this.w_PPNUMTR1 = oFrom.w_PPNUMTR1
    this.w_PPNUMTR2 = oFrom.w_PPNUMTR2
    this.w_PPNUMTR3 = oFrom.w_PPNUMTR3
    this.w_PPNUMTR4 = oFrom.w_PPNUMTR4
    this.w_PPGENFIR = oFrom.w_PPGENFIR
    this.w_PPNUMTR5 = oFrom.w_PPNUMTR5
    this.w_PPDIRGEN = oFrom.w_PPDIRGEN
    this.w_PPANPREV = oFrom.w_PPANPREV
    this.w_PPTRIPRE = oFrom.w_PPTRIPRE
    this.w_PPANFIRR = oFrom.w_PPANFIRR
    this.w_PPPROGEN = oFrom.w_PPPROGEN
    this.w_PPLISRIF = oFrom.w_PPLISRIF
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PPCODAZI = this.w_PPCODAZI
    oTo.w_PPCODIVA = this.w_PPCODIVA
    oTo.w_PPCALPRO = this.w_PPCALPRO
    oTo.w_OLDCALPRO = this.w_OLDCALPRO
    oTo.w_PPPREAZI = this.w_PPPREAZI
    oTo.w_PPPREAGE = this.w_PPPREAGE
    oTo.w_PPMAXESC = this.w_PPMAXESC
    oTo.w_PPMINESC = this.w_PPMINESC
    oTo.w_PPMAXNES = this.w_PPMAXNES
    oTo.w_PPMINNES = this.w_PPMINNES
    oTo.w_PPPERASS = this.w_PPPERASS
    oTo.w_PPSOKAGE = this.w_PPSOKAGE
    oTo.w_PPIMPIN1 = this.w_PPIMPIN1
    oTo.w_PPPERFI1 = this.w_PPPERFI1
    oTo.w_PPIMPIN2 = this.w_PPIMPIN2
    oTo.w_PPPERFI2 = this.w_PPPERFI2
    oTo.w_PPPERFI3 = this.w_PPPERFI3
    oTo.w_PPIMPPL1 = this.w_PPIMPPL1
    oTo.w_PPPERPL1 = this.w_PPPERPL1
    oTo.w_PPIMPPL2 = this.w_PPIMPPL2
    oTo.w_PPPERPL2 = this.w_PPPERPL2
    oTo.w_PPPERPL3 = this.w_PPPERPL3
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DESAPP = this.w_DESAPP
    oTo.w_PPCALSCO = this.w_PPCALSCO
    oTo.w_PPMATCAP = this.w_PPMATCAP
    oTo.w_PPPOSENA = this.w_PPPOSENA
    oTo.w_PPNUMTR1 = this.w_PPNUMTR1
    oTo.w_PPNUMTR2 = this.w_PPNUMTR2
    oTo.w_PPNUMTR3 = this.w_PPNUMTR3
    oTo.w_PPNUMTR4 = this.w_PPNUMTR4
    oTo.w_PPGENFIR = this.w_PPGENFIR
    oTo.w_PPNUMTR5 = this.w_PPNUMTR5
    oTo.w_PPDIRGEN = this.w_PPDIRGEN
    oTo.w_PPANPREV = this.w_PPANPREV
    oTo.w_PPTRIPRE = this.w_PPTRIPRE
    oTo.w_PPANFIRR = this.w_PPANFIRR
    oTo.w_PPPROGEN = this.w_PPPROGEN
    oTo.w_PPLISRIF = this.w_PPLISRIF
    PCContext::Load(oTo)
enddefine

define class tcgsve_app as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 644
  Height = 392+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-18"
  HelpContextID=108383639
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Constant Properties
  PAR_PROV_IDX = 0
  AZIENDA_IDX = 0
  VOCIIVA_IDX = 0
  LISTINI_IDX = 0
  cFile = "PAR_PROV"
  cKeySelect = "PPCODAZI"
  cKeyWhere  = "PPCODAZI=this.w_PPCODAZI"
  cKeyWhereODBC = '"PPCODAZI="+cp_ToStrODBC(this.w_PPCODAZI)';

  cKeyWhereODBCqualified = '"PAR_PROV.PPCODAZI="+cp_ToStrODBC(this.w_PPCODAZI)';

  cPrg = "gsve_app"
  cComment = "Parametri provvigioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PPCODAZI = space(5)
  w_PPCODIVA = space(5)
  w_PPCALPRO = space(2)
  o_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPPREAZI = 0
  w_PPPREAGE = 0
  w_PPMAXESC = 0
  w_PPMINESC = 0
  w_PPMAXNES = 0
  w_PPMINNES = 0
  w_PPPERASS = 0
  w_PPSOKAGE = 0
  w_PPIMPIN1 = 0
  w_PPPERFI1 = 0
  w_PPIMPIN2 = 0
  w_PPPERFI2 = 0
  w_PPPERFI3 = 0
  w_PPIMPPL1 = 0
  w_PPPERPL1 = 0
  w_PPIMPPL2 = 0
  w_PPPERPL2 = 0
  w_PPPERPL3 = 0
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DESAPP = space(35)
  w_PPCALSCO = space(1)
  o_PPCALSCO = space(1)
  w_PPMATCAP = space(1)
  w_PPPOSENA = space(8)
  w_PPNUMTR1 = space(4)
  w_PPNUMTR2 = space(4)
  w_PPNUMTR3 = space(4)
  w_PPNUMTR4 = space(4)
  w_PPGENFIR = space(1)
  w_PPNUMTR5 = space(4)
  w_PPDIRGEN = space(254)
  w_PPANPREV = space(4)
  o_PPANPREV = space(4)
  w_PPTRIPRE = space(1)
  o_PPTRIPRE = space(1)
  w_PPANFIRR = space(4)
  w_PPPROGEN = space(1)
  w_PPLISRIF = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_appPag1","gsve_app",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri")
      .Pages(1).HelpContextID = 44980984
      .Pages(2).addobject("oPag","tgsve_appPag2","gsve_app",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Invio file distinta ENASARCO")
      .Pages(2).HelpContextID = 177675867
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPPCODIVA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='PAR_PROV'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_PROV_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_PROV_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_app'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_PROV where PPCODAZI=KeySet.PPCODAZI
    *
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_PROV')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_PROV.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_PROV '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PPCODAZI',this.w_PPCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OLDCALPRO = space(2)
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_DESAPP = space(35)
        .w_PPCODAZI = NVL(PPCODAZI,space(5))
        .w_PPCODIVA = NVL(PPCODIVA,space(5))
          if link_1_2_joined
            this.w_PPCODIVA = NVL(IVCODIVA102,NVL(this.w_PPCODIVA,space(5)))
            this.w_DESAPP = NVL(IVDESIVA102,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO102),ctod("  /  /  "))
          else
          .link_1_2('Load')
          endif
        .w_PPCALPRO = NVL(PPCALPRO,space(2))
        .w_PPPREAZI = NVL(PPPREAZI,0)
        .w_PPPREAGE = NVL(PPPREAGE,0)
        .w_PPMAXESC = NVL(PPMAXESC,0)
        .w_PPMINESC = NVL(PPMINESC,0)
        .w_PPMAXNES = NVL(PPMAXNES,0)
        .w_PPMINNES = NVL(PPMINNES,0)
        .w_PPPERASS = NVL(PPPERASS,0)
        .w_PPSOKAGE = NVL(PPSOKAGE,0)
        .w_PPIMPIN1 = NVL(PPIMPIN1,0)
        .w_PPPERFI1 = NVL(PPPERFI1,0)
        .w_PPIMPIN2 = NVL(PPIMPIN2,0)
        .w_PPPERFI2 = NVL(PPPERFI2,0)
        .w_PPPERFI3 = NVL(PPPERFI3,0)
        .w_PPIMPPL1 = NVL(PPIMPPL1,0)
        .w_PPPERPL1 = NVL(PPPERPL1,0)
        .w_PPIMPPL2 = NVL(PPIMPPL2,0)
        .w_PPPERPL2 = NVL(PPPERPL2,0)
        .w_PPPERPL3 = NVL(PPPERPL3,0)
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .w_PPCALSCO = NVL(PPCALSCO,space(1))
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .w_PPMATCAP = NVL(PPMATCAP,space(1))
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_PPPOSENA = NVL(PPPOSENA,space(8))
        .w_PPNUMTR1 = NVL(PPNUMTR1,space(4))
        .w_PPNUMTR2 = NVL(PPNUMTR2,space(4))
        .w_PPNUMTR3 = NVL(PPNUMTR3,space(4))
        .w_PPNUMTR4 = NVL(PPNUMTR4,space(4))
        .w_PPGENFIR = NVL(PPGENFIR,space(1))
        .w_PPNUMTR5 = NVL(PPNUMTR5,space(4))
        .w_PPDIRGEN = NVL(PPDIRGEN,space(254))
        .w_PPANPREV = NVL(PPANPREV,space(4))
        .w_PPTRIPRE = NVL(PPTRIPRE,space(1))
        .w_PPANFIRR = NVL(PPANFIRR,space(4))
        .w_PPPROGEN = NVL(PPPROGEN,space(1))
        .w_PPLISRIF = NVL(PPLISRIF,space(5))
          * evitabile
          *.link_1_59('Load')
        cp_LoadRecExtFlds(this,'PAR_PROV')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsve_app
    *inizializza var.oldcalpro per batch gsve_bpp
    this.w_OLDCALPRO= this.w_PPCALPRO
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PPCODAZI = space(5)
      .w_PPCODIVA = space(5)
      .w_PPCALPRO = space(2)
      .w_OLDCALPRO = space(2)
      .w_PPPREAZI = 0
      .w_PPPREAGE = 0
      .w_PPMAXESC = 0
      .w_PPMINESC = 0
      .w_PPMAXNES = 0
      .w_PPMINNES = 0
      .w_PPPERASS = 0
      .w_PPSOKAGE = 0
      .w_PPIMPIN1 = 0
      .w_PPPERFI1 = 0
      .w_PPIMPIN2 = 0
      .w_PPPERFI2 = 0
      .w_PPPERFI3 = 0
      .w_PPIMPPL1 = 0
      .w_PPPERPL1 = 0
      .w_PPIMPPL2 = 0
      .w_PPPERPL2 = 0
      .w_PPPERPL3 = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DESAPP = space(35)
      .w_PPCALSCO = space(1)
      .w_PPMATCAP = space(1)
      .w_PPPOSENA = space(8)
      .w_PPNUMTR1 = space(4)
      .w_PPNUMTR2 = space(4)
      .w_PPNUMTR3 = space(4)
      .w_PPNUMTR4 = space(4)
      .w_PPGENFIR = space(1)
      .w_PPNUMTR5 = space(4)
      .w_PPDIRGEN = space(254)
      .w_PPANPREV = space(4)
      .w_PPTRIPRE = space(1)
      .w_PPANFIRR = space(4)
      .w_PPPROGEN = space(1)
      .w_PPLISRIF = space(5)
      if .cFunction<>"Filter"
        .w_PPCODAZI = I_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_PPCODIVA))
          .link_1_2('Full')
          endif
        .w_PPCALPRO = 'DI'
          .DoRTCalc(4,23,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
          .DoRTCalc(25,37,.f.)
        .w_PPANFIRR = IIF(.w_PPGENFIR<>'S',IIF(.w_PPTRIPRE='4',.w_PPANPREV,IIF(EMPTY(.w_PPANPREV),'',ALLTRIM(STR(VAL(.w_PPANPREV)-1)))),.w_PPANFIRR)
        .w_PPPROGEN = ' '
        .w_PPLISRIF = IIF(.w_PPCALSCO='S', .w_PPLISRIF,SPACE(5))
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_PPLISRIF))
          .link_1_59('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_PROV')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPPCODIVA_1_2.enabled = i_bVal
      .Page1.oPag.oPPCALPRO_1_3.enabled = i_bVal
      .Page1.oPag.oPPPREAZI_1_5.enabled = i_bVal
      .Page1.oPag.oPPPREAGE_1_6.enabled = i_bVal
      .Page1.oPag.oPPMAXESC_1_7.enabled = i_bVal
      .Page1.oPag.oPPMINESC_1_8.enabled = i_bVal
      .Page1.oPag.oPPMAXNES_1_9.enabled = i_bVal
      .Page1.oPag.oPPMINNES_1_10.enabled = i_bVal
      .Page1.oPag.oPPPERASS_1_11.enabled = i_bVal
      .Page1.oPag.oPPSOKAGE_1_12.enabled = i_bVal
      .Page1.oPag.oPPIMPIN1_1_13.enabled = i_bVal
      .Page1.oPag.oPPPERFI1_1_14.enabled = i_bVal
      .Page1.oPag.oPPIMPIN2_1_15.enabled = i_bVal
      .Page1.oPag.oPPPERFI2_1_16.enabled = i_bVal
      .Page1.oPag.oPPPERFI3_1_17.enabled = i_bVal
      .Page1.oPag.oPPIMPPL1_1_18.enabled = i_bVal
      .Page1.oPag.oPPPERPL1_1_19.enabled = i_bVal
      .Page1.oPag.oPPIMPPL2_1_20.enabled = i_bVal
      .Page1.oPag.oPPPERPL2_1_21.enabled = i_bVal
      .Page1.oPag.oPPPERPL3_1_22.enabled = i_bVal
      .Page1.oPag.oPPCALSCO_1_54.enabled = i_bVal
      .Page1.oPag.oPPMATCAP_1_56.enabled = i_bVal
      .Page2.oPag.oPPPOSENA_2_1.enabled = i_bVal
      .Page2.oPag.oPPNUMTR1_2_3.enabled = i_bVal
      .Page2.oPag.oPPNUMTR2_2_4.enabled = i_bVal
      .Page2.oPag.oPPNUMTR3_2_5.enabled = i_bVal
      .Page2.oPag.oPPNUMTR4_2_6.enabled = i_bVal
      .Page2.oPag.oPPGENFIR_2_7.enabled = i_bVal
      .Page2.oPag.oPPNUMTR5_2_15.enabled = i_bVal
      .Page2.oPag.oPPDIRGEN_2_16.enabled = i_bVal
      .Page2.oPag.oPPANPREV_2_18.enabled = i_bVal
      .Page2.oPag.oPPTRIPRE_2_19.enabled = i_bVal
      .Page2.oPag.oPPANFIRR_2_20.enabled = i_bVal
      .Page1.oPag.oPPPROGEN_1_58.enabled = i_bVal
      .Page1.oPag.oPPLISRIF_1_59.enabled = i_bVal
      .Page2.oPag.oBtn_2_25.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_PROV',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPCODAZI,"PPCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPCODIVA,"PPCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPCALPRO,"PPCALPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPREAZI,"PPPREAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPREAGE,"PPPREAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPMAXESC,"PPMAXESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPMINESC,"PPMINESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPMAXNES,"PPMAXNES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPMINNES,"PPMINNES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERASS,"PPPERASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPSOKAGE,"PPSOKAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPIMPIN1,"PPIMPIN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERFI1,"PPPERFI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPIMPIN2,"PPIMPIN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERFI2,"PPPERFI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERFI3,"PPPERFI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPIMPPL1,"PPIMPPL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERPL1,"PPPERPL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPIMPPL2,"PPIMPPL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERPL2,"PPPERPL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPERPL3,"PPPERPL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPCALSCO,"PPCALSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPMATCAP,"PPMATCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPOSENA,"PPPOSENA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPNUMTR1,"PPNUMTR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPNUMTR2,"PPNUMTR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPNUMTR3,"PPNUMTR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPNUMTR4,"PPNUMTR4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPGENFIR,"PPGENFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPNUMTR5,"PPNUMTR5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPDIRGEN,"PPDIRGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPANPREV,"PPANPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPTRIPRE,"PPTRIPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPANFIRR,"PPANFIRR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPPROGEN,"PPPROGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PPLISRIF,"PPLISRIF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROV_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_PROV
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_PROV')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_PROV')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PPCODAZI,PPCODIVA,PPCALPRO,PPPREAZI,PPPREAGE"+;
                  ",PPMAXESC,PPMINESC,PPMAXNES,PPMINNES,PPPERASS"+;
                  ",PPSOKAGE,PPIMPIN1,PPPERFI1,PPIMPIN2,PPPERFI2"+;
                  ",PPPERFI3,PPIMPPL1,PPPERPL1,PPIMPPL2,PPPERPL2"+;
                  ",PPPERPL3,PPCALSCO,PPMATCAP,PPPOSENA,PPNUMTR1"+;
                  ",PPNUMTR2,PPNUMTR3,PPNUMTR4,PPGENFIR,PPNUMTR5"+;
                  ",PPDIRGEN,PPANPREV,PPTRIPRE,PPANFIRR,PPPROGEN"+;
                  ",PPLISRIF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PPCODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_PPCODIVA)+;
                  ","+cp_ToStrODBC(this.w_PPCALPRO)+;
                  ","+cp_ToStrODBC(this.w_PPPREAZI)+;
                  ","+cp_ToStrODBC(this.w_PPPREAGE)+;
                  ","+cp_ToStrODBC(this.w_PPMAXESC)+;
                  ","+cp_ToStrODBC(this.w_PPMINESC)+;
                  ","+cp_ToStrODBC(this.w_PPMAXNES)+;
                  ","+cp_ToStrODBC(this.w_PPMINNES)+;
                  ","+cp_ToStrODBC(this.w_PPPERASS)+;
                  ","+cp_ToStrODBC(this.w_PPSOKAGE)+;
                  ","+cp_ToStrODBC(this.w_PPIMPIN1)+;
                  ","+cp_ToStrODBC(this.w_PPPERFI1)+;
                  ","+cp_ToStrODBC(this.w_PPIMPIN2)+;
                  ","+cp_ToStrODBC(this.w_PPPERFI2)+;
                  ","+cp_ToStrODBC(this.w_PPPERFI3)+;
                  ","+cp_ToStrODBC(this.w_PPIMPPL1)+;
                  ","+cp_ToStrODBC(this.w_PPPERPL1)+;
                  ","+cp_ToStrODBC(this.w_PPIMPPL2)+;
                  ","+cp_ToStrODBC(this.w_PPPERPL2)+;
                  ","+cp_ToStrODBC(this.w_PPPERPL3)+;
                  ","+cp_ToStrODBC(this.w_PPCALSCO)+;
                  ","+cp_ToStrODBC(this.w_PPMATCAP)+;
                  ","+cp_ToStrODBC(this.w_PPPOSENA)+;
                  ","+cp_ToStrODBC(this.w_PPNUMTR1)+;
                  ","+cp_ToStrODBC(this.w_PPNUMTR2)+;
                  ","+cp_ToStrODBC(this.w_PPNUMTR3)+;
                  ","+cp_ToStrODBC(this.w_PPNUMTR4)+;
                  ","+cp_ToStrODBC(this.w_PPGENFIR)+;
                  ","+cp_ToStrODBC(this.w_PPNUMTR5)+;
                  ","+cp_ToStrODBC(this.w_PPDIRGEN)+;
                  ","+cp_ToStrODBC(this.w_PPANPREV)+;
                  ","+cp_ToStrODBC(this.w_PPTRIPRE)+;
                  ","+cp_ToStrODBC(this.w_PPANFIRR)+;
                  ","+cp_ToStrODBC(this.w_PPPROGEN)+;
                  ","+cp_ToStrODBCNull(this.w_PPLISRIF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_PROV')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_PROV')
        cp_CheckDeletedKey(i_cTable,0,'PPCODAZI',this.w_PPCODAZI)
        INSERT INTO (i_cTable);
              (PPCODAZI,PPCODIVA,PPCALPRO,PPPREAZI,PPPREAGE,PPMAXESC,PPMINESC,PPMAXNES,PPMINNES,PPPERASS,PPSOKAGE,PPIMPIN1,PPPERFI1,PPIMPIN2,PPPERFI2,PPPERFI3,PPIMPPL1,PPPERPL1,PPIMPPL2,PPPERPL2,PPPERPL3,PPCALSCO,PPMATCAP,PPPOSENA,PPNUMTR1,PPNUMTR2,PPNUMTR3,PPNUMTR4,PPGENFIR,PPNUMTR5,PPDIRGEN,PPANPREV,PPTRIPRE,PPANFIRR,PPPROGEN,PPLISRIF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PPCODAZI;
                  ,this.w_PPCODIVA;
                  ,this.w_PPCALPRO;
                  ,this.w_PPPREAZI;
                  ,this.w_PPPREAGE;
                  ,this.w_PPMAXESC;
                  ,this.w_PPMINESC;
                  ,this.w_PPMAXNES;
                  ,this.w_PPMINNES;
                  ,this.w_PPPERASS;
                  ,this.w_PPSOKAGE;
                  ,this.w_PPIMPIN1;
                  ,this.w_PPPERFI1;
                  ,this.w_PPIMPIN2;
                  ,this.w_PPPERFI2;
                  ,this.w_PPPERFI3;
                  ,this.w_PPIMPPL1;
                  ,this.w_PPPERPL1;
                  ,this.w_PPIMPPL2;
                  ,this.w_PPPERPL2;
                  ,this.w_PPPERPL3;
                  ,this.w_PPCALSCO;
                  ,this.w_PPMATCAP;
                  ,this.w_PPPOSENA;
                  ,this.w_PPNUMTR1;
                  ,this.w_PPNUMTR2;
                  ,this.w_PPNUMTR3;
                  ,this.w_PPNUMTR4;
                  ,this.w_PPGENFIR;
                  ,this.w_PPNUMTR5;
                  ,this.w_PPDIRGEN;
                  ,this.w_PPANPREV;
                  ,this.w_PPTRIPRE;
                  ,this.w_PPANFIRR;
                  ,this.w_PPPROGEN;
                  ,this.w_PPLISRIF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_PROV_IDX,i_nConn)
      *
      * update PAR_PROV
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_PROV')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PPCODIVA="+cp_ToStrODBCNull(this.w_PPCODIVA)+;
             ",PPCALPRO="+cp_ToStrODBC(this.w_PPCALPRO)+;
             ",PPPREAZI="+cp_ToStrODBC(this.w_PPPREAZI)+;
             ",PPPREAGE="+cp_ToStrODBC(this.w_PPPREAGE)+;
             ",PPMAXESC="+cp_ToStrODBC(this.w_PPMAXESC)+;
             ",PPMINESC="+cp_ToStrODBC(this.w_PPMINESC)+;
             ",PPMAXNES="+cp_ToStrODBC(this.w_PPMAXNES)+;
             ",PPMINNES="+cp_ToStrODBC(this.w_PPMINNES)+;
             ",PPPERASS="+cp_ToStrODBC(this.w_PPPERASS)+;
             ",PPSOKAGE="+cp_ToStrODBC(this.w_PPSOKAGE)+;
             ",PPIMPIN1="+cp_ToStrODBC(this.w_PPIMPIN1)+;
             ",PPPERFI1="+cp_ToStrODBC(this.w_PPPERFI1)+;
             ",PPIMPIN2="+cp_ToStrODBC(this.w_PPIMPIN2)+;
             ",PPPERFI2="+cp_ToStrODBC(this.w_PPPERFI2)+;
             ",PPPERFI3="+cp_ToStrODBC(this.w_PPPERFI3)+;
             ",PPIMPPL1="+cp_ToStrODBC(this.w_PPIMPPL1)+;
             ",PPPERPL1="+cp_ToStrODBC(this.w_PPPERPL1)+;
             ",PPIMPPL2="+cp_ToStrODBC(this.w_PPIMPPL2)+;
             ",PPPERPL2="+cp_ToStrODBC(this.w_PPPERPL2)+;
             ",PPPERPL3="+cp_ToStrODBC(this.w_PPPERPL3)+;
             ",PPCALSCO="+cp_ToStrODBC(this.w_PPCALSCO)+;
             ",PPMATCAP="+cp_ToStrODBC(this.w_PPMATCAP)+;
             ",PPPOSENA="+cp_ToStrODBC(this.w_PPPOSENA)+;
             ",PPNUMTR1="+cp_ToStrODBC(this.w_PPNUMTR1)+;
             ",PPNUMTR2="+cp_ToStrODBC(this.w_PPNUMTR2)+;
             ",PPNUMTR3="+cp_ToStrODBC(this.w_PPNUMTR3)+;
             ",PPNUMTR4="+cp_ToStrODBC(this.w_PPNUMTR4)+;
             ",PPGENFIR="+cp_ToStrODBC(this.w_PPGENFIR)+;
             ",PPNUMTR5="+cp_ToStrODBC(this.w_PPNUMTR5)+;
             ",PPDIRGEN="+cp_ToStrODBC(this.w_PPDIRGEN)+;
             ",PPANPREV="+cp_ToStrODBC(this.w_PPANPREV)+;
             ",PPTRIPRE="+cp_ToStrODBC(this.w_PPTRIPRE)+;
             ",PPANFIRR="+cp_ToStrODBC(this.w_PPANFIRR)+;
             ",PPPROGEN="+cp_ToStrODBC(this.w_PPPROGEN)+;
             ",PPLISRIF="+cp_ToStrODBCNull(this.w_PPLISRIF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_PROV')
        i_cWhere = cp_PKFox(i_cTable  ,'PPCODAZI',this.w_PPCODAZI  )
        UPDATE (i_cTable) SET;
              PPCODIVA=this.w_PPCODIVA;
             ,PPCALPRO=this.w_PPCALPRO;
             ,PPPREAZI=this.w_PPPREAZI;
             ,PPPREAGE=this.w_PPPREAGE;
             ,PPMAXESC=this.w_PPMAXESC;
             ,PPMINESC=this.w_PPMINESC;
             ,PPMAXNES=this.w_PPMAXNES;
             ,PPMINNES=this.w_PPMINNES;
             ,PPPERASS=this.w_PPPERASS;
             ,PPSOKAGE=this.w_PPSOKAGE;
             ,PPIMPIN1=this.w_PPIMPIN1;
             ,PPPERFI1=this.w_PPPERFI1;
             ,PPIMPIN2=this.w_PPIMPIN2;
             ,PPPERFI2=this.w_PPPERFI2;
             ,PPPERFI3=this.w_PPPERFI3;
             ,PPIMPPL1=this.w_PPIMPPL1;
             ,PPPERPL1=this.w_PPPERPL1;
             ,PPIMPPL2=this.w_PPIMPPL2;
             ,PPPERPL2=this.w_PPPERPL2;
             ,PPPERPL3=this.w_PPPERPL3;
             ,PPCALSCO=this.w_PPCALSCO;
             ,PPMATCAP=this.w_PPMATCAP;
             ,PPPOSENA=this.w_PPPOSENA;
             ,PPNUMTR1=this.w_PPNUMTR1;
             ,PPNUMTR2=this.w_PPNUMTR2;
             ,PPNUMTR3=this.w_PPNUMTR3;
             ,PPNUMTR4=this.w_PPNUMTR4;
             ,PPGENFIR=this.w_PPGENFIR;
             ,PPNUMTR5=this.w_PPNUMTR5;
             ,PPDIRGEN=this.w_PPDIRGEN;
             ,PPANPREV=this.w_PPANPREV;
             ,PPTRIPRE=this.w_PPTRIPRE;
             ,PPANFIRR=this.w_PPANFIRR;
             ,PPPROGEN=this.w_PPPROGEN;
             ,PPLISRIF=this.w_PPLISRIF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsve_app
    * --- Notifico l'evento per aggiornare le variabili pubbliche
    this.NotifyEvent('Modifica')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_PROV_IDX,i_nConn)
      *
      * delete PAR_PROV
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PPCODAZI',this.w_PPCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_PROV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROV_IDX,2])
    if i_bUpd
      with this
            .w_PPCODAZI = I_CODAZI
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(2,37,.t.)
        if .o_PPTRIPRE<>.w_PPTRIPRE.or. .o_PPANPREV<>.w_PPANPREV
            .w_PPANFIRR = IIF(.w_PPGENFIR<>'S',IIF(.w_PPTRIPRE='4',.w_PPANPREV,IIF(EMPTY(.w_PPANPREV),'',ALLTRIM(STR(VAL(.w_PPANPREV)-1)))),.w_PPANFIRR)
        endif
        .DoRTCalc(39,39,.t.)
        if .o_PPCALSCO<>.w_PPCALSCO
            .w_PPLISRIF = IIF(.w_PPCALSCO='S', .w_PPLISRIF,SPACE(5))
          .link_1_59('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oPPNUMTR5_2_15.enabled = this.oPgFrm.Page2.oPag.oPPNUMTR5_2_15.mCond()
    this.oPgFrm.Page2.oPag.oPPANFIRR_2_20.enabled = this.oPgFrm.Page2.oPag.oPPANFIRR_2_20.mCond()
    this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.enabled = this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.visible=!this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODIVA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_PPCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_PPCODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_PPCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_PPCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PPCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oPPCODIVA_1_2'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_PPCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_PPCODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESAPP = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODIVA = space(5)
      endif
      this.w_DESAPP = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_PPCODIVA = space(5)
        this.w_DESAPP = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.IVCODIVA as IVCODIVA102"+ ",link_1_2.IVDESIVA as IVDESIVA102"+ ",link_1_2.IVDTOBSO as IVDTOBSO102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on PAR_PROV.PPCODIVA=link_1_2.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and PAR_PROV.PPCODIVA=link_1_2.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PPLISRIF
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPLISRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PPLISRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_PPLISRIF))
          select LSCODLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPLISRIF)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPLISRIF) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oPPLISRIF_1_59'),i_cWhere,'GSAR_ALI',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPLISRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PPLISRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PPLISRIF)
            select LSCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPLISRIF = NVL(_Link_.LSCODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPLISRIF = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPLISRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPPCODIVA_1_2.value==this.w_PPCODIVA)
      this.oPgFrm.Page1.oPag.oPPCODIVA_1_2.value=this.w_PPCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCALPRO_1_3.RadioValue()==this.w_PPCALPRO)
      this.oPgFrm.Page1.oPag.oPPCALPRO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPREAZI_1_5.value==this.w_PPPREAZI)
      this.oPgFrm.Page1.oPag.oPPPREAZI_1_5.value=this.w_PPPREAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPREAGE_1_6.value==this.w_PPPREAGE)
      this.oPgFrm.Page1.oPag.oPPPREAGE_1_6.value=this.w_PPPREAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMAXESC_1_7.value==this.w_PPMAXESC)
      this.oPgFrm.Page1.oPag.oPPMAXESC_1_7.value=this.w_PPMAXESC
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMINESC_1_8.value==this.w_PPMINESC)
      this.oPgFrm.Page1.oPag.oPPMINESC_1_8.value=this.w_PPMINESC
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMAXNES_1_9.value==this.w_PPMAXNES)
      this.oPgFrm.Page1.oPag.oPPMAXNES_1_9.value=this.w_PPMAXNES
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMINNES_1_10.value==this.w_PPMINNES)
      this.oPgFrm.Page1.oPag.oPPMINNES_1_10.value=this.w_PPMINNES
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERASS_1_11.value==this.w_PPPERASS)
      this.oPgFrm.Page1.oPag.oPPPERASS_1_11.value=this.w_PPPERASS
    endif
    if not(this.oPgFrm.Page1.oPag.oPPSOKAGE_1_12.value==this.w_PPSOKAGE)
      this.oPgFrm.Page1.oPag.oPPSOKAGE_1_12.value=this.w_PPSOKAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPPIMPIN1_1_13.value==this.w_PPIMPIN1)
      this.oPgFrm.Page1.oPag.oPPIMPIN1_1_13.value=this.w_PPIMPIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERFI1_1_14.value==this.w_PPPERFI1)
      this.oPgFrm.Page1.oPag.oPPPERFI1_1_14.value=this.w_PPPERFI1
    endif
    if not(this.oPgFrm.Page1.oPag.oPPIMPIN2_1_15.value==this.w_PPIMPIN2)
      this.oPgFrm.Page1.oPag.oPPIMPIN2_1_15.value=this.w_PPIMPIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERFI2_1_16.value==this.w_PPPERFI2)
      this.oPgFrm.Page1.oPag.oPPPERFI2_1_16.value=this.w_PPPERFI2
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERFI3_1_17.value==this.w_PPPERFI3)
      this.oPgFrm.Page1.oPag.oPPPERFI3_1_17.value=this.w_PPPERFI3
    endif
    if not(this.oPgFrm.Page1.oPag.oPPIMPPL1_1_18.value==this.w_PPIMPPL1)
      this.oPgFrm.Page1.oPag.oPPIMPPL1_1_18.value=this.w_PPIMPPL1
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERPL1_1_19.value==this.w_PPPERPL1)
      this.oPgFrm.Page1.oPag.oPPPERPL1_1_19.value=this.w_PPPERPL1
    endif
    if not(this.oPgFrm.Page1.oPag.oPPIMPPL2_1_20.value==this.w_PPIMPPL2)
      this.oPgFrm.Page1.oPag.oPPIMPPL2_1_20.value=this.w_PPIMPPL2
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERPL2_1_21.value==this.w_PPPERPL2)
      this.oPgFrm.Page1.oPag.oPPPERPL2_1_21.value=this.w_PPPERPL2
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPERPL3_1_22.value==this.w_PPPERPL3)
      this.oPgFrm.Page1.oPag.oPPPERPL3_1_22.value=this.w_PPPERPL3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP_1_51.value==this.w_DESAPP)
      this.oPgFrm.Page1.oPag.oDESAPP_1_51.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCALSCO_1_54.RadioValue()==this.w_PPCALSCO)
      this.oPgFrm.Page1.oPag.oPPCALSCO_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMATCAP_1_56.RadioValue()==this.w_PPMATCAP)
      this.oPgFrm.Page1.oPag.oPPMATCAP_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPPPOSENA_2_1.value==this.w_PPPOSENA)
      this.oPgFrm.Page2.oPag.oPPPOSENA_2_1.value=this.w_PPPOSENA
    endif
    if not(this.oPgFrm.Page2.oPag.oPPNUMTR1_2_3.value==this.w_PPNUMTR1)
      this.oPgFrm.Page2.oPag.oPPNUMTR1_2_3.value=this.w_PPNUMTR1
    endif
    if not(this.oPgFrm.Page2.oPag.oPPNUMTR2_2_4.value==this.w_PPNUMTR2)
      this.oPgFrm.Page2.oPag.oPPNUMTR2_2_4.value=this.w_PPNUMTR2
    endif
    if not(this.oPgFrm.Page2.oPag.oPPNUMTR3_2_5.value==this.w_PPNUMTR3)
      this.oPgFrm.Page2.oPag.oPPNUMTR3_2_5.value=this.w_PPNUMTR3
    endif
    if not(this.oPgFrm.Page2.oPag.oPPNUMTR4_2_6.value==this.w_PPNUMTR4)
      this.oPgFrm.Page2.oPag.oPPNUMTR4_2_6.value=this.w_PPNUMTR4
    endif
    if not(this.oPgFrm.Page2.oPag.oPPGENFIR_2_7.RadioValue()==this.w_PPGENFIR)
      this.oPgFrm.Page2.oPag.oPPGENFIR_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPPNUMTR5_2_15.value==this.w_PPNUMTR5)
      this.oPgFrm.Page2.oPag.oPPNUMTR5_2_15.value=this.w_PPNUMTR5
    endif
    if not(this.oPgFrm.Page2.oPag.oPPDIRGEN_2_16.value==this.w_PPDIRGEN)
      this.oPgFrm.Page2.oPag.oPPDIRGEN_2_16.value=this.w_PPDIRGEN
    endif
    if not(this.oPgFrm.Page2.oPag.oPPANPREV_2_18.value==this.w_PPANPREV)
      this.oPgFrm.Page2.oPag.oPPANPREV_2_18.value=this.w_PPANPREV
    endif
    if not(this.oPgFrm.Page2.oPag.oPPTRIPRE_2_19.value==this.w_PPTRIPRE)
      this.oPgFrm.Page2.oPag.oPPTRIPRE_2_19.value=this.w_PPTRIPRE
    endif
    if not(this.oPgFrm.Page2.oPag.oPPANFIRR_2_20.value==this.w_PPANFIRR)
      this.oPgFrm.Page2.oPag.oPPANFIRR_2_20.value=this.w_PPANFIRR
    endif
    if not(this.oPgFrm.Page1.oPag.oPPPROGEN_1_58.RadioValue()==this.w_PPPROGEN)
      this.oPgFrm.Page1.oPag.oPPPROGEN_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.value==this.w_PPLISRIF)
      this.oPgFrm.Page1.oPag.oPPLISRIF_1_59.value=this.w_PPLISRIF
    endif
    cp_SetControlsValueExtFlds(this,'PAR_PROV')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PPCODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPCODIVA_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   not(.w_PPTRIPRE $ ' 1234')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPPTRIPRE_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un trimestre valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PPCALPRO = this.w_PPCALPRO
    this.o_PPCALSCO = this.w_PPCALSCO
    this.o_PPANPREV = this.w_PPANPREV
    this.o_PPTRIPRE = this.w_PPTRIPRE
    return

enddefine

* --- Define pages as container
define class tgsve_appPag1 as StdContainer
  Width  = 640
  height = 392
  stdWidth  = 640
  stdheight = 392
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPCODIVA_1_2 as StdField with uid="ZDZGUIQBAV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PPCODIVA", cQueryName = "PPCODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Codice IVA su provvigioni (solo per agenti con P.IVA)",;
    HelpContextID = 67719991,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=225, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_PPCODIVA"

  func oPPCODIVA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCODIVA_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCODIVA_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oPPCODIVA_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oPPCODIVA_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_PPCODIVA
     i_obj.ecpSave()
  endproc


  add object oPPCALPRO_1_3 as StdCombo with uid="ALTJOPNACB",rtseq=3,rtrep=.f.,left=225,top=36,width=151,height=21;
    , HelpContextID = 75803835;
    , cFormVar="w_PPCALPRO",RowSource=""+"Disattivato,"+"Generazione differita,"+"Generazione immediata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPCALPRO_1_3.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'GD',;
    iif(this.value =3,'GI',;
    space(2)))))
  endfunc
  func oPPCALPRO_1_3.GetRadio()
    this.Parent.oContained.w_PPCALPRO = this.RadioValue()
    return .t.
  endfunc

  func oPPCALPRO_1_3.SetRadio()
    this.Parent.oContained.w_PPCALPRO=trim(this.Parent.oContained.w_PPCALPRO)
    this.value = ;
      iif(this.Parent.oContained.w_PPCALPRO=='DI',1,;
      iif(this.Parent.oContained.w_PPCALPRO=='GD',2,;
      iif(this.Parent.oContained.w_PPCALPRO=='GI',3,;
      0)))
  endfunc

  add object oPPPREAZI_1_5 as StdField with uid="ZWYUHOFLAH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PPPREAZI", cQueryName = "PPPREAZI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 203236159,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=143, Top=114, cSayPict='"99.999"', cGetPict='"99.999"'

  add object oPPPREAGE_1_6 as StdField with uid="HDOFXJXPEY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PPPREAGE", cQueryName = "PPPREAGE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 203236155,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=335, Top=114, cSayPict='"99.999"', cGetPict='"99.999"'

  add object oPPMAXESC_1_7 as StdField with uid="KUTLROFGCQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PPMAXESC", cQueryName = "PPMAXESC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 247729351,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=143, Top=160, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPMINESC_1_8 as StdField with uid="TYNHJUNAJH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PPMINESC", cQueryName = "PPMINESC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 257690823,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=143, Top=187, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPMAXNES_1_9 as StdField with uid="EYSEQUUYKO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PPMAXNES", cQueryName = "PPMAXNES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 171701065,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=279, Top=160, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPMINNES_1_10 as StdField with uid="UQCLKTAVXU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PPMINNES", cQueryName = "PPMINNES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 161739593,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=279, Top=187, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPPERASS_1_11 as StdField with uid="KTBEJDOXTI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PPPERASS", cQueryName = "PPPERASS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di contributo a carico dell�azienda",;
    HelpContextID = 52419767,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=143, Top=246, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPSOKAGE_1_12 as StdField with uid="RFFYCHCNCN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PPSOKAGE", cQueryName = "PPSOKAGE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Aliquota a carico dell'agente",;
    HelpContextID = 209343291,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=335, Top=246, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPIMPIN1_1_13 as StdField with uid="MDVADDHYNS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PPIMPIN1", cQueryName = "PPIMPIN1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 188239065,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=143, Top=317, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPPERFI1_1_14 as StdField with uid="DKJVDPLDSD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PPPERFI1", cQueryName = "PPPERFI1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 31466279,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=328, Top=317, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPIMPIN2_1_15 as StdField with uid="BQSPBZMVOZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PPIMPIN2", cQueryName = "PPIMPIN2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 188239064,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=143, Top=343, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPPERFI2_1_16 as StdField with uid="FQLLZJSIFN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PPPERFI2", cQueryName = "PPPERFI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 31466280,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=328, Top=343, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPPERFI3_1_17 as StdField with uid="HPKXSXHGIE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PPPERFI3", cQueryName = "PPPERFI3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 31466281,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=328, Top=369, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPIMPPL1_1_18 as StdField with uid="DGNDABWCMB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PPIMPPL1", cQueryName = "PPIMPPL1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 70798553,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=422, Top=317, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPPERPL1_1_19 as StdField with uid="VCTCJJFNPE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PPPERPL1", cQueryName = "PPPERPL1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 69197017,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=582, Top=317, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPIMPPL2_1_20 as StdField with uid="EKWRPWILMB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PPIMPPL2", cQueryName = "PPIMPPL2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 70798552,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=422, Top=343, cSayPict="v_PV[18]", cGetPict="v_GV[18]"

  add object oPPPERPL2_1_21 as StdField with uid="YYRZRQUQJA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PPPERPL2", cQueryName = "PPPERPL2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 69197016,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=582, Top=343, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oPPPERPL3_1_22 as StdField with uid="YRAOVYFXWW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PPPERPL3", cQueryName = "PPPERPL3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 69197015,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=582, Top=369, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oDESAPP_1_51 as StdField with uid="KCSINWKFSL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 71547082,;
   bGlobalFont=.t.,;
    Height=21, Width=343, Left=290, Top=9, InputMask=replicate('X',35)


  add object oObj_1_53 as cp_runprogram with uid="ORMXHDWZZE",left=-4, top=408, width=201,height=19,;
    caption='GSVE_BPP',;
   bGlobalFont=.t.,;
    prg="GSVE_BPP",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 21985866

  add object oPPCALSCO_1_54 as StdCheck with uid="KLUYNOUEKF",rtseq=26,rtrep=.f.,left=388, top=36, caption="Calcola sconto",;
    ToolTipText = "Se attivo: calcola in automatico lo sconto applicato",;
    HelpContextID = 25472187,;
    cFormVar="w_PPCALSCO", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oPPCALSCO_1_54.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPCALSCO_1_54.GetRadio()
    this.Parent.oContained.w_PPCALSCO = this.RadioValue()
    return .t.
  endfunc

  func oPPCALSCO_1_54.SetRadio()
    this.Parent.oContained.w_PPCALSCO=trim(this.Parent.oContained.w_PPCALSCO)
    this.value = ;
      iif(this.Parent.oContained.w_PPCALSCO=='S',1,;
      0)
  endfunc


  add object oObj_1_55 as cp_runprogram with uid="XBCQNFFPNK",left=198, top=408, width=203,height=19,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Modifica",;
    nPag=1;
    , HelpContextID = 71842650

  add object oPPMATCAP_1_56 as StdCheck with uid="FUXUMDFKUC",rtseq=27,rtrep=.f.,left=388, top=59, caption="Maturazione capoarea",;
    ToolTipText = "Se attivo, la % di maturazione (a data fattura, scadenza, incasso) del capoarea seguir� la categoria provvigioni dello stesso",;
    HelpContextID = 17042618,;
    cFormVar="w_PPMATCAP", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oPPMATCAP_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPMATCAP_1_56.GetRadio()
    this.Parent.oContained.w_PPMATCAP = this.RadioValue()
    return .t.
  endfunc

  func oPPMATCAP_1_56.SetRadio()
    this.Parent.oContained.w_PPMATCAP=trim(this.Parent.oContained.w_PPMATCAP)
    this.value = ;
      iif(this.Parent.oContained.w_PPMATCAP=='S',1,;
      0)
  endfunc


  add object oObj_1_57 as cp_runprogram with uid="LUHFRWOXIO",left=406, top=407, width=248,height=20,;
    caption='GSAR_BAM(info)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("Info")',;
    cEvent = "w_PPCALSCO Changed",;
    nPag=1;
    , HelpContextID = 15105741

  add object oPPPROGEN_1_58 as StdCheck with uid="ZLPWSXAHLH",rtseq=39,rtrep=.f.,left=388, top=82, caption="Ricalcola provvigioni",;
    ToolTipText = "Se attivo, nelle generazioni massive dei documenti verranno ricalcolate le provvigioni",;
    HelpContextID = 45949764,;
    cFormVar="w_PPPROGEN", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oPPPROGEN_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPPROGEN_1_58.GetRadio()
    this.Parent.oContained.w_PPPROGEN = this.RadioValue()
    return .t.
  endfunc

  func oPPPROGEN_1_58.SetRadio()
    this.Parent.oContained.w_PPPROGEN=trim(this.Parent.oContained.w_PPPROGEN)
    this.value = ;
      iif(this.Parent.oContained.w_PPPROGEN=='S',1,;
      0)
  endfunc

  add object oPPLISRIF_1_59 as StdField with uid="CPUSJDNIWP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_PPLISRIF", cQueryName = "PPLISRIF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino di riferimento",;
    HelpContextID = 234087228,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=570, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , TabStop=.f., cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PPLISRIF"

  func oPPLISRIF_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPCALSCO='S')
    endwith
   endif
  endfunc

  func oPPLISRIF_1_59.mHide()
    with this.Parent.oContained
      return (.w_PPCALSCO<>'S')
    endwith
  endfunc

  func oPPLISRIF_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPLISRIF_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPLISRIF_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oPPLISRIF_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'',this.parent.oContained
  endproc
  proc oPPLISRIF_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_PPLISRIF
     i_obj.ecpSave()
  endproc

  add object oStr_1_23 as StdString with uid="KYOLXCADGF",Visible=.t., Left=28, Top=160,;
    Alignment=1, Width=113, Height=15,;
    Caption="Max imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="TMUFBPZIPH",Visible=.t., Left=28, Top=187,;
    Alignment=1, Width=113, Height=15,;
    Caption="Minimo importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="BCKDXKIGSP",Visible=.t., Left=29, Top=246,;
    Alignment=1, Width=112, Height=15,;
    Caption="% a carico azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JQRTVMOFLB",Visible=.t., Left=27, Top=317,;
    Alignment=1, Width=114, Height=15,;
    Caption="Imponibile fino a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WNRYFKEHFR",Visible=.t., Left=146, Top=369,;
    Alignment=1, Width=154, Height=15,;
    Caption="Oltre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MHRFZQHBXU",Visible=.t., Left=309, Top=317,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="TNCYVFUHLY",Visible=.t., Left=309, Top=343,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="NBDVVORPGH",Visible=.t., Left=309, Top=369,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="REQZEJAFRS",Visible=.t., Left=30, Top=91,;
    Alignment=0, Width=352, Height=15,;
    Caption="Fondo previdenza ENASARCO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="RUPHQICJEL",Visible=.t., Left=11, Top=114,;
    Alignment=1, Width=130, Height=15,;
    Caption="% a carico azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="VKPFYUOOIC",Visible=.t., Left=197, Top=114,;
    Alignment=1, Width=135, Height=15,;
    Caption="% a carico agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="IYIMWPTCGY",Visible=.t., Left=30, Top=219,;
    Alignment=0, Width=571, Height=15,;
    Caption="Fondo assistenza ENASARCO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="VSPVWHVZBN",Visible=.t., Left=30, Top=274,;
    Alignment=0, Width=570, Height=15,;
    Caption="Fondo indennit� risoluzione rapporto (FIRR)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_36 as StdString with uid="MNOKJXHWQG",Visible=.t., Left=27, Top=343,;
    Alignment=1, Width=114, Height=15,;
    Caption="Imponibile fino a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="WOIMQLQFKS",Visible=.t., Left=423, Top=369,;
    Alignment=1, Width=133, Height=15,;
    Caption="Oltre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KXYTVQVYZP",Visible=.t., Left=565, Top=317,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="YANKSQPLON",Visible=.t., Left=565, Top=343,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="GGJMTRNRDR",Visible=.t., Left=565, Top=369,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="IMBGBUISZR",Visible=.t., Left=143, Top=143,;
    Alignment=0, Width=120, Height=15,;
    Caption="Monomandatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="UWSGNKPULL",Visible=.t., Left=279, Top=143,;
    Alignment=0, Width=119, Height=15,;
    Caption="Plurimandatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CXHHIUMXQA",Visible=.t., Left=143, Top=301,;
    Alignment=0, Width=152, Height=15,;
    Caption="Monomandatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="JUUTQSVVHD",Visible=.t., Left=422, Top=301,;
    Alignment=0, Width=133, Height=15,;
    Caption="Plurimandatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="TQCFLYHWHS",Visible=.t., Left=71, Top=9,;
    Alignment=1, Width=152, Height=15,;
    Caption="Cod.IVA su provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="JATDKGOGSM",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=218, Height=18,;
    Caption="Calcolo provvigioni sui documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="BQCUYWOKDT",Visible=.t., Left=500, Top=38,;
    Alignment=1, Width=69, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_PPCALSCO<>'S')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="PLTLYUUTAK",Visible=.t., Left=209, Top=246,;
    Alignment=1, Width=123, Height=18,;
    Caption="% a carico agente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_37 as StdBox with uid="GLMPWTQYJS",left=27, top=291, width=611,height=1

  add object oBox_1_46 as StdBox with uid="UPONEEOLHB",left=27, top=236, width=611,height=1

  add object oBox_1_47 as StdBox with uid="IOTEOJKHJI",left=27, top=109, width=611,height=1
enddefine
define class tgsve_appPag2 as StdContainer
  Width  = 640
  height = 392
  stdWidth  = 640
  stdheight = 392
  resizeXpos=558
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPPPOSENA_2_1 as StdField with uid="LMGNRWRFCO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PPPOSENA", cQueryName = "PPPOSENA",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Posizione ENASARCO azienda",;
    HelpContextID = 252042441,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=146, Top=12, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  add object oPPNUMTR1_2_3 as StdField with uid="KENTBVGSLX",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PPNUMTR1", cQueryName = "PPNUMTR1",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Numero 1� trimestre previdenza/assistenza",;
    HelpContextID = 6290649,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=207, Top=94, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oPPNUMTR2_2_4 as StdField with uid="BHZGPFTBQP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PPNUMTR2", cQueryName = "PPNUMTR2",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Numero 2� trimestre previdenza/assistenza",;
    HelpContextID = 6290648,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=207, Top=121, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oPPNUMTR3_2_5 as StdField with uid="QGSXIVMFYU",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PPNUMTR3", cQueryName = "PPNUMTR3",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Numero 3� trimestre previdenza/assistenza",;
    HelpContextID = 6290647,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=207, Top=148, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oPPNUMTR4_2_6 as StdField with uid="VYJZUBMMXZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PPNUMTR4", cQueryName = "PPNUMTR4",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Numero 4� trimestre previdenza/assistenza",;
    HelpContextID = 6290646,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=207, Top=175, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oPPGENFIR_2_7 as StdCheck with uid="GAXESDTNOG",rtseq=33,rtrep=.f.,left=401, top=91, caption="Invio disgiunto FIRR",;
    ToolTipText = "Se attivo genera un file specifico per il FIRR",;
    HelpContextID = 27235144,;
    cFormVar="w_PPGENFIR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPPGENFIR_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPGENFIR_2_7.GetRadio()
    this.Parent.oContained.w_PPGENFIR = this.RadioValue()
    return .t.
  endfunc

  func oPPGENFIR_2_7.SetRadio()
    this.Parent.oContained.w_PPGENFIR=trim(this.Parent.oContained.w_PPGENFIR)
    this.value = ;
      iif(this.Parent.oContained.w_PPGENFIR=='S',1,;
      0)
  endfunc

  add object oPPNUMTR5_2_15 as StdField with uid="AHQFFSWKVA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PPNUMTR5", cQueryName = "PPNUMTR5",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Numero 4� trimestre FIRR",;
    HelpContextID = 6290645,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=492, Top=121, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oPPNUMTR5_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPGENFIR='S')
    endwith
   endif
  endfunc

  add object oPPDIRGEN_2_16 as StdField with uid="ATBKVTRNBQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PPDIRGEN", cQueryName = "PPDIRGEN",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Directory di default ove sono memorizzate le distinte generate",;
    HelpContextID = 48456516,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=135, Top=255, InputMask=replicate('X',254)

  add object oPPANPREV_2_18 as StdField with uid="STPSJOUGQX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PPANPREV", cQueryName = "PPANPREV",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di ultima generazione previdenza/assistenza",;
    HelpContextID = 231224140,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=207, Top=202, InputMask=replicate('X',4)

  add object oPPTRIPRE_2_19 as StdField with uid="SOSCBTXXZP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_PPTRIPRE", cQueryName = "PPTRIPRE",;
    bObbl = .f. , nPag = 2, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un trimestre valido",;
    ToolTipText = "Trimestre di ultima generazione previdenza/assistenza",;
    HelpContextID = 77765829,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=253, Top=202, InputMask=replicate('X',1)

  func oPPTRIPRE_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPTRIPRE $ ' 1234')
    endwith
    return bRes
  endfunc

  add object oPPANFIRR_2_20 as StdField with uid="FTUVTJSAIM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PPANFIRR", cQueryName = "PPANFIRR",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di ultima generazione FIRR",;
    HelpContextID = 198692024,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=492, Top=148, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oPPANFIRR_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PPGENFIR='S')
    endwith
   endif
  endfunc


  add object oBtn_2_25 as StdButton with uid="PSACVHWWYM",left=572, top=255, width=25,height=22,;
    caption="...", nPag=2;
    , HelpContextID = 108584662;
  , bGlobalFont=.t.

    proc oBtn_2_25.Click()
      with this.Parent.oContained
        .w_PPDIRGEN=left(cp_GetDir(sys(5)+sys(2003),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_2 as StdString with uid="XFFEUHFRST",Visible=.t., Left=15, Top=12,;
    Alignment=1, Width=128, Height=18,;
    Caption="Posizione ENASARCO:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="DYOHXMWHHD",Visible=.t., Left=30, Top=43,;
    Alignment=0, Width=132, Height=18,;
    Caption="Numeri ENASARCO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_9 as StdString with uid="QISTFOSAUK",Visible=.t., Left=118, Top=94,;
    Alignment=1, Width=80, Height=18,;
    Caption="1� Trimestre:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="URPGMHPWWT",Visible=.t., Left=118, Top=121,;
    Alignment=1, Width=80, Height=18,;
    Caption="2� Trimestre:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="QEPNWAIXTI",Visible=.t., Left=118, Top=148,;
    Alignment=1, Width=80, Height=18,;
    Caption="3� Trimestre:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="IXXVBVJLYN",Visible=.t., Left=118, Top=175,;
    Alignment=1, Width=80, Height=18,;
    Caption="4� Trimestre:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="YLHYAIDEKT",Visible=.t., Left=375, Top=121,;
    Alignment=1, Width=107, Height=18,;
    Caption="4� Trimestre FIRR:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="XKIWFBRYLX",Visible=.t., Left=32, Top=66,;
    Alignment=1, Width=126, Height=18,;
    Caption="Previdenza/assistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="KGXLXXDTPZ",Visible=.t., Left=13, Top=256,;
    Alignment=1, Width=122, Height=18,;
    Caption="Directory di default:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="SCOQDXGJZU",Visible=.t., Left=335, Top=66,;
    Alignment=1, Width=31, Height=18,;
    Caption="FIRR:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="EJQCNJOTAJ",Visible=.t., Left=35, Top=202,;
    Alignment=1, Width=163, Height=18,;
    Caption="Ultima generazione prev/ass:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="INSUEPFJTR",Visible=.t., Left=339, Top=149,;
    Alignment=1, Width=143, Height=18,;
    Caption="Ultima generazione FIRR:"  ;
  , bGlobalFont=.t.

  add object oBox_2_22 as StdBox with uid="TUHHGUKWVZ",left=311, top=60, width=2,height=169

  add object oBox_2_26 as StdBox with uid="JUWVVJATRF",left=23, top=59, width=609,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_app','PAR_PROV','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PPCODAZI=PAR_PROV.PPCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
