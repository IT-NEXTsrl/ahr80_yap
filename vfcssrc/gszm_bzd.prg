* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bzd                                                        *
*              Tasto destro descrizione parametrica                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-20                                                      *
* Last revis.: 2010-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPDES,pPARAM,pNoTruncate
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bzd",oParentObject,m.pTIPDES,m.pPARAM,m.pNoTruncate)
return(i_retval)

define class tgszm_bzd as StdBatch
  * --- Local variables
  pTIPDES = space(10)
  pPARAM = space(10)
  pNoTruncate = .f.
  w_GEST = .NULL.
  w_OBJECT = .NULL.
  w_SelStart = 0
  w_StringaNuova = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce tasto destro campi CCDESSUP,CCDESRIG
    * --- Campo da aggiornare
    * --- tipo descrizione R:Riga o T:Testata
    * --- Parametro per gestione campi Memo
    * --- Parametro da inserire
    this.w_GEST = g_oMenu.oParentobject
    if  Type("This.w_GEST.w_"+ Alltrim(this.pTIPDES))="C"
      this.w_OBJECT = this.w_GEST.GETCTRL("w_"+Alltrim(this.pTIPDES))
    endif
    if Type("This.w_OBJECT")="O"
      this.w_SelStart = this.w_OBJECT.SelStart
      if this.w_SelStart >0
        this.w_StringaNuova = LEFT(this.w_OBJECT.value,this.w_SelStart)+"<"+this.pPARAM+">"+ALLTRIM(SUBSTR(this.w_OBJECT.value,this.w_SelStart+1))
      else
        this.w_StringaNuova = Alltrim(this.w_OBJECT.value)+"<"+this.pPARAM+">"
      endif
      if this.pNoTruncate
        this.w_OBJECT.value = Alltrim(this.w_StringaNuova)
      else
        this.w_OBJECT.value = left(Alltrim(this.w_StringaNuova),254)
      endif
      this.w_SelStart = this.w_SelStart+LEN(this.pPARAM)+2
      this.w_OBJECT.SelStart = this.w_SelStart
    endif
  endproc


  proc Init(oParentObject,pTIPDES,pPARAM,pNoTruncate)
    this.pTIPDES=pTIPDES
    this.pPARAM=pPARAM
    this.pNoTruncate=pNoTruncate
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPDES,pPARAM,pNoTruncate"
endproc
