* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bam                                                        *
*              Seleziona account email                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-28                                                      *
* Last revis.: 2016-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bam",oParentObject,m.pSERIAL)
return(i_retval)

define class tgsut_bam as StdBatch
  * --- Local variables
  pSERIAL = space(5)
  w_AMSERIAL = space(5)
  w_AM_EMAIL = space(254)
  w_AM_LOGIN = space(254)
  w_AMPASSWD = space(254)
  w_AM_INVIO = space(1)
  w_PWDOK = .f.
  w_GSUT_MMU = .NULL.
  w_GSUT_AAM = .NULL.
  w_UTCODICE = 0
  w_AMTYPSEA = 0
  * --- WorkFile variables
  ACC_MAIL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zoom Selezione account mail, da:
    *     - GSUT_MMU, zoom campo w_UMSERIAL
    *     - GSUT_AAM, record inserted (crea nuovo account mail da GSUT_MMU)
    do case
      case inlist(this.oParentObject.cFunction,"Load","Edit")
        if empty(this.pSERIAL)
          * --- da GSUT_MMU, zoom campo w_UMSERIAL
          this.w_UTCODICE = this.oParentObject.oParentObject.w_UTCODICE
          do GSUT1KAM with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if EMPTY (NVL (this.w_AMPASSWD,"")) AND this.w_AMTYPSEA=4
            this.w_PWDOK = .T.
            this.w_GSUT_MMU = this.oParentObject
          else
            if not empty(this.w_AMSERIAL)
              this.w_PWDOK = EMPTY(NVL(this.w_AM_LOGIN,""))
              if NOT this.w_PWDOK
                * --- Verifico la password di accesso
                do GSUT_KAM with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_GSUT_MMU = this.oParentObject
            endif
          endif
        else
          * --- da GSUT_AAM, record inserted (crea nuovo account mail da GSUT_MMU)
          this.w_GSUT_AAM = this.oParentObject
          this.w_GSUT_MMU = this.w_GSUT_AAM.w_GSUT_MMU
          this.w_PWDOK = .T.
          this.w_AMSERIAL = this.pSERIAL
        endif
        if this.w_PWDOK
          * --- Read from ACC_MAIL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AMSERIAL,AM_EMAIL,AM_INVIO,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AMSHARED"+;
              " from "+i_cTable+" ACC_MAIL where ";
                  +"AMSERIAL = "+cp_ToStrODBC(this.w_AMSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AMSERIAL,AM_EMAIL,AM_INVIO,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AMSHARED;
              from (i_cTable) where;
                  AMSERIAL = this.w_AMSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GSUT_MMU.w_UMSERIAL = NVL(cp_ToDate(_read_.AMSERIAL),cp_NullValue(_read_.AMSERIAL))
            this.w_GSUT_MMU.w_AM_EMAIL = NVL(cp_ToDate(_read_.AM_EMAIL),cp_NullValue(_read_.AM_EMAIL))
            this.w_GSUT_MMU.w_AM_INVIO = NVL(cp_ToDate(_read_.AM_INVIO),cp_NullValue(_read_.AM_INVIO))
            this.w_GSUT_MMU.w_AMAUTSRV = NVL(cp_ToDate(_read_.AMAUTSRV),cp_NullValue(_read_.AMAUTSRV))
            this.w_GSUT_MMU.w_AMCC_EML = NVL(cp_ToDate(_read_.AMCC_EML),cp_NullValue(_read_.AMCC_EML))
            this.w_GSUT_MMU.w_AMCCNEML = NVL(cp_ToDate(_read_.AMCCNEML),cp_NullValue(_read_.AMCCNEML))
            this.w_GSUT_MMU.w_AM_FIRMA = NVL(cp_ToDate(_read_.AM_FIRMA),cp_NullValue(_read_.AM_FIRMA))
            this.w_GSUT_MMU.w_AMSHARED = NVL(cp_ToDate(_read_.AMSHARED),cp_NullValue(_read_.AMSHARED))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_GSUT_MMU.w_AM_EMAIL = STRTRAN( this.w_GSUT_MMU.w_AM_EMAIL, "infinity:", "")
          this.w_GSUT_MMU.w_EDITMAIL = Empty(this.w_AMSERIAL)
          this.w_GSUT_MMU.SaveRow()     
          this.w_GSUT_MMU.SetControlsValue()     
          this.w_GSUT_MMU.mEnableControls()     
          this.w_GSUT_MMU.SetCallerVars()     
        endif
      case this.oParentObject.cFunction="Query"
        if not empty(this.oParentObject.w_UMSERIAL)
          opengest("A", "GSUT_AAM", "AMSERIAL", this.oParentObject.w_UMSERIAL)
        endif
    endcase
  endproc


  proc Init(oParentObject,pSERIAL)
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ACC_MAIL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL"
endproc
