* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kfg                                                        *
*              Ricerca documenti archiviati                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_361]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-14                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kfg
if cp_TablePropScan('TMPZ') > 0
   if IsAlt()
      ah_errormsg("Attenzione: E' gi� aperta un'istanza di Ricerca documenti archiviati, quella corrente verr� chiusa",48,'')
   else
      ah_errormsg("Attenzione: E' gi� aperta un'istanza di Ricerca documenti archiviati/Monitor esportazione verso SOS, quella corrente verr� chiusa",48,'')
   endif
  return
endif
public cTipo
cTipo = ' '
* --- Fine Area Manuale
return(createobject("tgsut_kfg",oParentObject))

* --- Class definition
define class tgsut_kfg as StdForm
  Top    = 1
  Left   = 8

  * --- Standard Properties
  Width  = 795
  Height = 543+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-21"
  HelpContextID=48841577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=86

  * --- Constant Properties
  _IDX = 0
  PROMCLAS_IDX = 0
  CPUSERS_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  PROMINDI_IDX = 0
  STATISOS_IDX = 0
  CAN_TIER_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsut_kfg"
  cComment = "Ricerca documenti archiviati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ISALT = .F.
  w_SELCODCLA = space(15)
  o_SELCODCLA = space(15)
  w_SELCODCLA = space(15)
  w_cTipo = space(3)
  w_READAZI = space(5)
  w_HIDSCA = space(1)
  w_HIDARC = space(1)
  w_DESCLA = space(50)
  w_TIPOALL = space(5)
  o_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_SELOGGETTO = space(100)
  w_SELNOMEFILE = space(100)
  w_SELNOMEORIG = space(100)
  w_SESTATSOS = space(18)
  w_CODPRAT = space(15)
  w_GGAVVISO = 0
  w_DESCRALL = space(75)
  w_FLPROV = space(1)
  w_PERCARC = space(150)
  w_INDICE = space(25)
  w_DESTIPO = space(40)
  w_DESCLAAL = space(40)
  w_ALLPRIUNI = .F.
  w_SELEZIONE = space(1)
  w_FILE = space(20)
  w_Programma = space(10)
  w_GFDESCFILE = space(50)
  w_Periodo = ctod('  /  /  ')
  w_GFPERIODO = space(10)
  w_GFKEYINDIC = space(15)
  w_GFMOTARCHI = space(1)
  w_GFCLASAWEB = space(10)
  w_GFUIDEXTER = space(50)
  w_GFORIGFILE = space(10)
  w_GFIDMODALL = space(1)
  w_GFIDWEBAPP = space(1)
  w_GFIDWEBFIL = space(254)
  w_GFALLPRI = space(1)
  w_GFCLASDOCU = space(10)
  w_GFNOMEFILE = space(60)
  w_GFPATHFILE = space(60)
  w_ISFILE = space(10)
  w_ESTENS = space(3)
  w_CDMODALL = space(1)
  w_CLASOS = space(1)
  w_SELMULT = .F.
  w_CODCLASOS = space(10)
  w_DESPRAT = space(100)
  w_OBTEST = space(10)
  w_NEWCODPRA = space(15)
  w_CMODALL = space(1)
  w_TIPOALL1 = space(5)
  w_CLASALL1 = space(5)
  w_SELNOTEALL = space(0)
  w_MODALL = space(1)
  o_MODALL = space(1)
  w_MODALL = space(1)
  w_VDATINI = ctod('  /  /  ')
  w_VDATFIN = ctod('  /  /  ')
  w_SELDATARC = 0
  o_SELDATARC = 0
  w_ADATINI = ctod('  /  /  ')
  w_ADATFIN = ctod('  /  /  ')
  w_TIPOFILE = space(254)
  w_TIPORD = space(1)
  w_SELUTEARC = 0
  w_IDPUBWEB = space(1)
  w_IDWEBAPP = space(1)
  w_ADESUTE = space(20)
  w_SELDELFI = space(10)
  w_BLOCCO1 = space(1)
  o_BLOCCO1 = space(1)
  w_WEBRET = space(15)
  w_ALLPRIN = space(1)
  w_SELUTEEXP = 0
  w_ARCHIVIO = space(20)
  w_Chiave = space(50)
  w_IDWEBAPP = space(1)
  w_IDSERIAL = space(10)
  o_IDSERIAL = space(10)
  w_EXPRFILTRO = space(0)
  w_EDESUTE = space(20)
  w_BLOCCO = space(1)
  w_PATHFA = space(254)
  w_CN__ENTE = space(10)
  w_CNTIPPRA = space(10)
  w_CNMATPRA = space(10)
  w_ORDFIL = space(1)
  w_ORIFIL = space(254)
  w_CREAZIP = space(1)

  * --- Children pointers
  GSUT_MA1 = .NULL.
  w_ZoomGF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kfg
  ClasseMod=.F.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kfgPag1","gsut_kfg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsut_kfgPag2","gsut_kfg",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELCODCLA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_kfg
    local i_cTempTable,i_nIdx
    *Temporaneo indici
    i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    IF Isalt()
      declare indexes_JRWKGCOGXE[1]
      indexes_JRWKGCOGXE[1]='GFKEYINDIC'
    endif
    vq_exec('DOCMTMPZ.VQR',this.parent,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    
    * Gestione interfaccia
    WITH THIS.PARENT
        oParentObject = iif(isnull(oParentObject),.F.,oParentObject)
       .w_cTipo=iif(vartype(oParentObject)='L' or vartype(oParentObject)='O' ,' ',oParentObject)
       .cComment=iif(vartype(oParentObject)='L' or vartype(oParentObject)='O','Ricerca documenti archiviati',;
       iif(vartype(oParentObject)='C' and oParentObject == 'AET','Ricerca documenti pratica','Monitor esportazione verso SOS'))  
       .oPgFrm.Pages(1).oPag.ZoomGF.cZoomFile=iif(isalt(), "GSUTAKFG", .oPgFrm.Pages(1).oPag.ZoomGF.cZoomFile)
       .oPgFrm.Pages(1).oPag.ZoomGF.GRD.FontSize=8
    endwith
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGF = this.oPgFrm.Pages(1).oPag.ZoomGF
    DoDefault()
    proc Destroy()
      this.w_ZoomGF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='TIP_ALLE'
    this.cWorkTables[4]='CLA_ALLE'
    this.cWorkTables[5]='PROMINDI'
    this.cWorkTables[6]='STATISOS'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='PAR_ALTE'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_MA1 = CREATEOBJECT('stdDynamicChild',this,'GSUT_MA1',this.oPgFrm.Page2.oPag.oLinkPC_2_34)
    this.GSUT_MA1.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MA1)
      this.GSUT_MA1.DestroyChildrenChain()
      this.GSUT_MA1=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_34')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MA1.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MA1.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MA1.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MA1.SetKey(;
            .w_IDSERIAL,"IDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MA1.ChangeRow(this.cRowID+'      1',1;
             ,.w_IDSERIAL,"IDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MA1)
        i_f=.GSUT_MA1.BuildFilter()
        if !(i_f==.GSUT_MA1.cQueryFilter)
          i_fnidx=.GSUT_MA1.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MA1.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MA1.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MA1.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MA1.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_MA1(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_MA1.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_MA1(i_ask)
    if this.GSUT_MA1.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSUT_MA1.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISALT=.f.
      .w_SELCODCLA=space(15)
      .w_SELCODCLA=space(15)
      .w_cTipo=space(3)
      .w_READAZI=space(5)
      .w_HIDSCA=space(1)
      .w_HIDARC=space(1)
      .w_DESCLA=space(50)
      .w_TIPOALL=space(5)
      .w_CLASALL=space(5)
      .w_SELOGGETTO=space(100)
      .w_SELNOMEFILE=space(100)
      .w_SELNOMEORIG=space(100)
      .w_SESTATSOS=space(18)
      .w_CODPRAT=space(15)
      .w_GGAVVISO=0
      .w_DESCRALL=space(75)
      .w_FLPROV=space(1)
      .w_PERCARC=space(150)
      .w_INDICE=space(25)
      .w_DESTIPO=space(40)
      .w_DESCLAAL=space(40)
      .w_ALLPRIUNI=.f.
      .w_SELEZIONE=space(1)
      .w_FILE=space(20)
      .w_Programma=space(10)
      .w_GFDESCFILE=space(50)
      .w_Periodo=ctod("  /  /  ")
      .w_GFPERIODO=space(10)
      .w_GFKEYINDIC=space(15)
      .w_GFMOTARCHI=space(1)
      .w_GFCLASAWEB=space(10)
      .w_GFUIDEXTER=space(50)
      .w_GFORIGFILE=space(10)
      .w_GFIDMODALL=space(1)
      .w_GFIDWEBAPP=space(1)
      .w_GFIDWEBFIL=space(254)
      .w_GFALLPRI=space(1)
      .w_GFCLASDOCU=space(10)
      .w_GFNOMEFILE=space(60)
      .w_GFPATHFILE=space(60)
      .w_ISFILE=space(10)
      .w_ESTENS=space(3)
      .w_CDMODALL=space(1)
      .w_CLASOS=space(1)
      .w_SELMULT=.f.
      .w_CODCLASOS=space(10)
      .w_DESPRAT=space(100)
      .w_OBTEST=space(10)
      .w_NEWCODPRA=space(15)
      .w_CMODALL=space(1)
      .w_TIPOALL1=space(5)
      .w_CLASALL1=space(5)
      .w_SELNOTEALL=space(0)
      .w_MODALL=space(1)
      .w_MODALL=space(1)
      .w_VDATINI=ctod("  /  /  ")
      .w_VDATFIN=ctod("  /  /  ")
      .w_SELDATARC=0
      .w_ADATINI=ctod("  /  /  ")
      .w_ADATFIN=ctod("  /  /  ")
      .w_TIPOFILE=space(254)
      .w_TIPORD=space(1)
      .w_SELUTEARC=0
      .w_IDPUBWEB=space(1)
      .w_IDWEBAPP=space(1)
      .w_ADESUTE=space(20)
      .w_SELDELFI=space(10)
      .w_BLOCCO1=space(1)
      .w_WEBRET=space(15)
      .w_ALLPRIN=space(1)
      .w_SELUTEEXP=0
      .w_ARCHIVIO=space(20)
      .w_Chiave=space(50)
      .w_IDWEBAPP=space(1)
      .w_IDSERIAL=space(10)
      .w_EXPRFILTRO=space(0)
      .w_EDESUTE=space(20)
      .w_BLOCCO=space(1)
      .w_PATHFA=space(254)
      .w_CN__ENTE=space(10)
      .w_CNTIPPRA=space(10)
      .w_CNMATPRA=space(10)
      .w_ORDFIL=space(1)
      .w_ORIFIL=space(254)
      .w_CREAZIP=space(1)
        .w_ISALT = Isalt()
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SELCODCLA))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_SELCODCLA))
          .link_1_3('Full')
        endif
        .w_cTipo = iif(type('this.oParentobject')='L' or type('this.oParentobject')='O','   ',this.oParentObject)
        .w_READAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READAZI))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,8,.f.)
        .w_TIPOALL = .w_TIPOALL
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_TIPOALL))
          .link_1_10('Full')
        endif
        .w_CLASALL = .w_CLASALL
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CLASALL))
          .link_1_11('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
          .DoRTCalc(11,13,.f.)
        .w_SESTATSOS = IIF(.w_cTipo # 'SOS','ALL','DA_INVIARE')
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODPRAT))
          .link_1_18('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomGF.Calculate()
          .DoRTCalc(16,17,.f.)
        .w_FLPROV = .w_ZoomGF.GetVar("IDFLPROV")
        .w_PERCARC = iif(empty(.w_ZoomGF.GetVar("GFPATHFILE")),' ',iif(empty(justdrive(.w_ZoomGF.GetVar("GFPATHFILE"))) AND !LEFT(.w_ZoomGF.GetVar("GFPATHFILE"),2)=='\\',addbs(SYS(5)+SYS(2003))+alltrim(.w_ZoomGF.GetVar("GFPATHFILE")),.w_ZoomGF.GetVar("GFPATHFILE")))
        .w_INDICE = alltrim(.w_ZoomGF.GetVar('GFKEYINDIC'))
          .DoRTCalc(21,22,.f.)
        .w_ALLPRIUNI = .F.
        .w_SELEZIONE = 'D'
        .w_FILE = IIF(TYPE('this.oParentObject.w_file')='C',this.oParentObject.w_file,' ')
        .w_Programma = IIF(TYPE('this.oParentObject.w_programma')='C',this.oParentObject.w_programma,' ')
        .w_GFDESCFILE = .w_ZoomGF.GetVar("GFDESCFILE")
        .w_Periodo = IIF(TYPE('this.oParentObject.w_periodo')='C',this.oParentObject.w_periodo,' ')
        .w_GFPERIODO = .w_ZoomGF.GetVar("GFPERIODO")
        .w_GFKEYINDIC = .w_ZoomGF.GetVar("GFKEYINDIC")
        .w_GFMOTARCHI = .w_ZoomGF.GetVar("GFMOTARCHI")
        .w_GFCLASAWEB = .w_ZoomGF.GetVar("GFCLASAWEB")
        .w_GFUIDEXTER = .w_ZoomGF.GetVar("GFUIDEXTER")
        .w_GFORIGFILE = .w_ZoomGF.GetVar("GFORIGFILE")
        .w_GFIDMODALL = .w_ZoomGF.GetVar("IDMODALL")
        .w_GFIDWEBAPP = .w_ZoomGF.GetVar("IDWEBAPP")
        .w_GFIDWEBFIL = .w_ZoomGF.GetVar("IDWEBFIL")
        .w_GFALLPRI = .w_ZoomGF.GetVar("GFALLPRI")
        .w_GFCLASDOCU = .w_ZoomGF.GetVar("GFCLASDOCU")
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_GFCLASDOCU))
          .link_1_70('Full')
        endif
        .w_GFNOMEFILE = .w_ZoomGF.GetVar("GFNOMEFILE")
        .w_GFPATHFILE = .w_ZoomGF.GetVar("GFPATHFILE")
        .w_ISFILE = IIF(EMPTY(NVL(.w_ZoomGF.GetVar("ISFILE"), ' ')), 'N', .w_ZoomGF.GetVar("ISFILE"))
        .w_ESTENS = justext(.w_GFNOMEFILE)
      .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
          .DoRTCalc(44,44,.f.)
        .w_CLASOS = 'N'
      .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .w_SELMULT = .F.
      .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
          .DoRTCalc(47,48,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(50,54,.f.)
        .w_MODALL = iif(.w_ISALT OR empty(nvl(.w_SELCODCLA,'')) or Empty(NVL(.w_CMODALL,'')) ,'T',.w_CMODALL)
        .w_MODALL = iif(.w_ISALT OR empty(nvl(.w_SELCODCLA,'')) or Empty(NVL(.w_CMODALL,'')) ,'T',.w_CMODALL)
        .w_VDATINI = i_inidat
        .w_VDATFIN = i_findat
        .w_SELDATARC = 1
        .w_ADATINI = IIF(.w_SELDATARC=1,iif(empty(.w_ADATINI),i_INIDAT,.w_ADATINI),i_DATSYS-.w_SELDATARC)
        .w_ADATFIN = IIF(.w_SELDATARC=1,iif(empty(.w_ADATFIN),i_FINDAT,.w_ADATFIN),i_DATSYS)
          .DoRTCalc(62,62,.f.)
        .w_TIPORD = IIF(.w_ISALT,.w_ORDFIL,'D')
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_SELUTEARC))
          .link_2_15('Full')
        endif
        .w_IDPUBWEB = 'T'
        .w_IDWEBAPP = 'T'
          .DoRTCalc(67,67,.f.)
        .w_SELDELFI = 'N'
        .w_BLOCCO1 = 'T'
        .w_WEBRET = IIF(.w_MODALL#'I','T',.w_WEBRET)
        .w_ALLPRIN = ' '
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_SELUTEEXP))
          .link_2_28('Full')
        endif
        .w_ARCHIVIO = IIF(TYPE('this.oParentObject.w_ARCHIVIO')='C',this.oParentObject .w_ARCHIVIO,iif(.w_cTipo#'AET',' ',.w_ARCHIVIO))
        .w_Chiave = IIF(TYPE('this.oParentObject.w_CHIAVE')='C',this.oParentObject.w_CHIAVE,' ')
        .w_IDWEBAPP = 'T'
        .w_IDSERIAL = REPLICATE('@', 10)
      .GSUT_MA1.NewDocument()
      .GSUT_MA1.ChangeRow('1',1,.w_IDSERIAL,"IDSERIAL")
      if not(.GSUT_MA1.bLoaded)
        .GSUT_MA1.SetKey(.w_IDSERIAL,"IDSERIAL")
      endif
          .DoRTCalc(77,78,.f.)
        .w_BLOCCO = iif(.w_BLOCCO1='T',' ',.w_BLOCCO1)
      .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
          .DoRTCalc(80,84,.f.)
        .w_ORIFIL = Nvl(.w_ZoomGF.GetVar("GFPATHFILE"),' ')
    endwith
    this.DoRTCalc(86,86,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_35.enabled = this.oPgFrm.Page2.oPag.oBtn_2_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_102.enabled = this.oPgFrm.Page1.oPag.oBtn_1_102.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kfg
    cTipo=this.w_cTipo
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_MA1.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MA1.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,8,.t.)
        if .o_SELCODCLA<>.w_SELCODCLA
            .w_TIPOALL = .w_TIPOALL
          .link_1_10('Full')
        endif
        if .o_TIPOALL<>.w_TIPOALL
            .w_CLASALL = .w_CLASALL
          .link_1_11('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.ZoomGF.Calculate()
        .DoRTCalc(11,17,.t.)
            .w_FLPROV = .w_ZoomGF.GetVar("IDFLPROV")
            .w_PERCARC = iif(empty(.w_ZoomGF.GetVar("GFPATHFILE")),' ',iif(empty(justdrive(.w_ZoomGF.GetVar("GFPATHFILE"))) AND !LEFT(.w_ZoomGF.GetVar("GFPATHFILE"),2)=='\\',addbs(SYS(5)+SYS(2003))+alltrim(.w_ZoomGF.GetVar("GFPATHFILE")),.w_ZoomGF.GetVar("GFPATHFILE")))
            .w_INDICE = alltrim(.w_ZoomGF.GetVar('GFKEYINDIC'))
        .DoRTCalc(21,26,.t.)
            .w_GFDESCFILE = .w_ZoomGF.GetVar("GFDESCFILE")
        .DoRTCalc(28,28,.t.)
            .w_GFPERIODO = .w_ZoomGF.GetVar("GFPERIODO")
            .w_GFKEYINDIC = .w_ZoomGF.GetVar("GFKEYINDIC")
            .w_GFMOTARCHI = .w_ZoomGF.GetVar("GFMOTARCHI")
            .w_GFCLASAWEB = .w_ZoomGF.GetVar("GFCLASAWEB")
            .w_GFUIDEXTER = .w_ZoomGF.GetVar("GFUIDEXTER")
            .w_GFORIGFILE = .w_ZoomGF.GetVar("GFORIGFILE")
            .w_GFIDMODALL = .w_ZoomGF.GetVar("IDMODALL")
            .w_GFIDWEBAPP = .w_ZoomGF.GetVar("IDWEBAPP")
            .w_GFIDWEBFIL = .w_ZoomGF.GetVar("IDWEBFIL")
            .w_GFALLPRI = .w_ZoomGF.GetVar("GFALLPRI")
            .w_GFCLASDOCU = .w_ZoomGF.GetVar("GFCLASDOCU")
          .link_1_70('Full')
            .w_GFNOMEFILE = .w_ZoomGF.GetVar("GFNOMEFILE")
            .w_GFPATHFILE = .w_ZoomGF.GetVar("GFPATHFILE")
            .w_ISFILE = IIF(EMPTY(NVL(.w_ZoomGF.GetVar("ISFILE"), ' ')), 'N', .w_ZoomGF.GetVar("ISFILE"))
            .w_ESTENS = justext(.w_GFNOMEFILE)
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .DoRTCalc(44,56,.t.)
        if .o_SELDATARC<>.w_SELDATARC
            .w_VDATINI = i_inidat
        endif
        if .o_SELDATARC<>.w_SELDATARC
            .w_VDATFIN = i_findat
        endif
        .DoRTCalc(59,59,.t.)
        if .o_SELDATARC<>.w_SELDATARC
            .w_ADATINI = IIF(.w_SELDATARC=1,iif(empty(.w_ADATINI),i_INIDAT,.w_ADATINI),i_DATSYS-.w_SELDATARC)
        endif
        if .o_SELDATARC<>.w_SELDATARC
            .w_ADATFIN = IIF(.w_SELDATARC=1,iif(empty(.w_ADATFIN),i_FINDAT,.w_ADATFIN),i_DATSYS)
        endif
        .DoRTCalc(62,67,.t.)
        if .o_SELCODCLA<>.w_SELCODCLA
            .w_SELDELFI = 'N'
        endif
        .DoRTCalc(69,69,.t.)
        if .o_MODALL<>.w_MODALL
            .w_WEBRET = IIF(.w_MODALL#'I','T',.w_WEBRET)
        endif
        .DoRTCalc(71,78,.t.)
        if .o_BLOCCO1<>.w_BLOCCO1
            .w_BLOCCO = iif(.w_BLOCCO1='T',' ',.w_BLOCCO1)
        endif
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
        .DoRTCalc(80,84,.t.)
            .w_ORIFIL = Nvl(.w_ZoomGF.GetVar("GFPATHFILE"),' ')
        if .o_SELCODCLA<>.w_SELCODCLA
          .Calculate_FTVFADDFZB()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_IDSERIAL<>.o_IDSERIAL
          .Save_GSUT_MA1(.t.)
          .GSUT_MA1.NewDocument()
          .GSUT_MA1.ChangeRow('1',1,.w_IDSERIAL,"IDSERIAL")
          if not(.GSUT_MA1.bLoaded)
            .GSUT_MA1.SetKey(.w_IDSERIAL,"IDSERIAL")
          endif
        endif
      endwith
      this.DoRTCalc(86,86,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.ZoomGF.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
    endwith
  return

  proc Calculate_ZCMMZUGMMI()
    with this
          * --- Riesegue lo zoom al cambio del codice pratica
          AggiornaZoomCodpra(this;
             )
    endwith
  endproc
  proc Calculate_EYDOQUQJRT()
    with this
          * --- Riassegna il valore della pratica
          .w_CODPRAT = IIF(Isalt(),g_CHIAVE,' ')
          .link_1_18('Full')
    endwith
  endproc
  proc Calculate_NAIZENHPTI()
    with this
          * --- Carica attributi di ricerca
          GSUT_BFG(this;
              ,'X';
             )
    endwith
  endproc
  proc Calculate_FTVFADDFZB()
    with this
          * --- Imposta MODALL al variare di SELCODCLA
          .w_MODALL = iif(.w_ISALT OR empty(nvl(.w_SELCODCLA,'')) or Empty(NVL(.w_CMODALL,'')) ,'T',.w_CMODALL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.enabled = this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.mCond()
    this.oPgFrm.Page2.oPag.oADATINI_2_7.enabled = this.oPgFrm.Page2.oPag.oADATINI_2_7.mCond()
    this.oPgFrm.Page2.oPag.oADATFIN_2_8.enabled = this.oPgFrm.Page2.oPag.oADATFIN_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_102.enabled = this.oPgFrm.Page1.oPag.oBtn_1_102.mCond()
    this.GSUT_MA1.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.visible=!this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.mHide()
    this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.visible=!this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.mHide()
    this.oPgFrm.Page1.oPag.oSESTATSOS_1_17.visible=!this.oPgFrm.Page1.oPag.oSESTATSOS_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODPRAT_1_18.visible=!this.oPgFrm.Page1.oPag.oCODPRAT_1_18.mHide()
    this.oPgFrm.Page1.oPag.oGGAVVISO_1_19.visible=!this.oPgFrm.Page1.oPag.oGGAVVISO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_41.visible=!this.oPgFrm.Page1.oPag.oBtn_1_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_46.visible=!this.oPgFrm.Page1.oPag.oBtn_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_48.visible=!this.oPgFrm.Page1.oPag.oBtn_1_48.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_49.visible=!this.oPgFrm.Page1.oPag.oBtn_1_49.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_50.visible=!this.oPgFrm.Page1.oPag.oBtn_1_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_51.visible=!this.oPgFrm.Page1.oPag.oBtn_1_51.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_52.visible=!this.oPgFrm.Page1.oPag.oBtn_1_52.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_54.visible=!this.oPgFrm.Page1.oPag.oBtn_1_54.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_56.visible=!this.oPgFrm.Page1.oPag.oBtn_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oDESPRAT_1_92.visible=!this.oPgFrm.Page1.oPag.oDESPRAT_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_95.visible=!this.oPgFrm.Page1.oPag.oBtn_1_95.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_96.visible=!this.oPgFrm.Page1.oPag.oBtn_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page2.oPag.oMODALL_2_2.visible=!this.oPgFrm.Page2.oPag.oMODALL_2_2.mHide()
    this.oPgFrm.Page2.oPag.oMODALL_2_3.visible=!this.oPgFrm.Page2.oPag.oMODALL_2_3.mHide()
    this.oPgFrm.Page2.oPag.oIDPUBWEB_2_16.visible=!this.oPgFrm.Page2.oPag.oIDPUBWEB_2_16.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBAPP_2_17.visible=!this.oPgFrm.Page2.oPag.oIDWEBAPP_2_17.mHide()
    this.oPgFrm.Page2.oPag.oSELDELFI_2_19.visible=!this.oPgFrm.Page2.oPag.oSELDELFI_2_19.mHide()
    this.oPgFrm.Page2.oPag.oWEBRET_2_21.visible=!this.oPgFrm.Page2.oPag.oWEBRET_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oARCHIVIO_2_29.visible=!this.oPgFrm.Page2.oPag.oARCHIVIO_2_29.mHide()
    this.oPgFrm.Page2.oPag.oChiave_2_30.visible=!this.oPgFrm.Page2.oPag.oChiave_2_30.mHide()
    this.oPgFrm.Page2.oPag.oIDWEBAPP_2_31.visible=!this.oPgFrm.Page2.oPag.oIDWEBAPP_2_31.mHide()
    this.oPgFrm.Page2.oPag.oEXPRFILTRO_2_33.visible=!this.oPgFrm.Page2.oPag.oEXPRFILTRO_2_33.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_34.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_102.visible=!this.oPgFrm.Page1.oPag.oBtn_1_102.mHide()
    this.oPgFrm.Page1.oPag.oCREAZIP_1_112.visible=!this.oPgFrm.Page1.oPag.oCREAZIP_1_112.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
        if lower(cEvent)==lower("w_CODPRAT Changed")
          .Calculate_ZCMMZUGMMI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomGF.Event(cEvent)
        if lower(cEvent)==lower("CfgGestLoaded")
          .Calculate_EYDOQUQJRT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_91.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_NAIZENHPTI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_104.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_kfg
    if cevent='Done'
    * --- Drop temporary table TMPZ
    local i_nIdx
        i_nIdx=cp_GetTableDefIdx('TMPZ')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPZ')
        endif
    * Elimino la variabile cTipo
     release cTipo
    endif
    
    if cevent='w_SELCODCLA Changed' and Not EMPTY(This.w_SELCODCLA)
      this.NotifyEvent('Aggiorna')
        this.NotifyEvent('Agglink')
      this.w_MODALL = iif(empty(this.w_SELCODCLA) or this.w_ISALT ,'T',this.w_CMODALL)
    endif
    if cevent='w_CODPRAT Changed'
       This.w_CHIAVE=This.w_CODPRAT
    endif
    *Gestione interfaccia
    if cEvent = 'Init' and empty(this.w_cTipo) and !IsAlt()
      this.NotifyEvent('Inizio')
    endif  
    *--- Esegue la Setfocus per valorizzare lo zoom all'apertura
    If cEvent = 'Init'
      local l_objCtrl
      If g_DOCM='S'
        l_objCtrl = This.GetCtrl("w_SELCODCLA", 2)
      Else
        l_objCtrl = This.GetCtrl("w_SELCODCLA", 1)  
      Endif
      l_objCtrl.SetFocus()
      l_objCtrl = .Null.
    Endif
    *Carico gli allegati associati alla funzionalit� aperta su cui ho cliccato 'Visualizza'
    if cEvent = 'Init' and TYPE('this.oParentObject.w_ARCHIVIO')='C' AND NOT EMPTY(this.oParentObject.w_ARCHIVIO)
     do GSUT_BES with this,this.w_ARCHIVIO,this.w_CHIAVE,'QUERY2'
    endif
    
    if cevent='Init' or cevent='w_CODPRAT Changed'
       Local CTRL_CONCON
       CTRL_CONCON= This.GetCtrl("w_TIPOFILE")
       CTRL_CONCON.Popola()
    endif
    if cevent='Done'
       if Vartype(g_CHIAVE)='C'
         Release g_CHIAVE
       Endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SELCODCLA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_ACL',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_SELCODCLA))
          select CDCODCLA,CDDESCLA,CDMODALL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oSELCODCLA_1_2'),i_cWhere,'GSUT_ACL',"Classi librerie allegati",''+iif((type("this.w_cTipo")="C" and this.w_cTipo="SOS") OR (type("this.parent.ocontained.w_cTipo")="C" and this.parent.ocontained.w_cTipo="SOS"),"GSDM_SOS","gsut_kfg")+'.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_SELCODCLA)
            select CDCODCLA,CDDESCLA,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_CMODALL = NVL(_Link_.CDMODALL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SELCODCLA = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_CMODALL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CMODALL$'FL' and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endif
        this.w_SELCODCLA = space(15)
        this.w_DESCLA = space(50)
        this.w_CMODALL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELCODCLA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_SELCODCLA))
          select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oSELCODCLA_1_3'),i_cWhere,'GSUT_MCD',"Classi documentali",''+iif((type("this.w_cTipo")="C" and this.w_cTipo="SOS") OR (type("this.parent.ocontained.w_cTipo")="C" and this.parent.ocontained.w_cTipo="SOS"),"GSDM_SOS","gsut_kfg")+'.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_SELCODCLA)
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDTIPALL,CDCLAALL,CDMODALL,CDCONSOS,CDCLASOS,CDAVVSOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_ARCHIVIO = NVL(_Link_.CDRIFTAB,space(20))
      this.w_TIPOALL1 = NVL(_Link_.CDTIPALL,space(5))
      this.w_CLASALL1 = NVL(_Link_.CDCLAALL,space(5))
      this.w_CMODALL = NVL(_Link_.CDMODALL,space(1))
      this.w_CLASOS = NVL(_Link_.CDCONSOS,space(1))
      this.w_CODCLASOS = NVL(_Link_.CDCLASOS,space(10))
      this.w_GGAVVISO = NVL(_Link_.CDAVVSOS,0)
    else
      if i_cCtrl<>'Load'
        this.w_SELCODCLA = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_ARCHIVIO = space(20)
      this.w_TIPOALL1 = space(5)
      this.w_CLASALL1 = space(5)
      this.w_CMODALL = space(1)
      this.w_CLASOS = space(1)
      this.w_CODCLASOS = space(10)
      this.w_GGAVVISO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CMODALL<>'S' and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' ) and (empty(.w_cTipo) or (.w_cTipo='SOS' and .w_CLASOS='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endif
        this.w_SELCODCLA = space(15)
        this.w_DESCLA = space(50)
        this.w_ARCHIVIO = space(20)
        this.w_TIPOALL1 = space(5)
        this.w_CLASALL1 = space(5)
        this.w_CMODALL = space(1)
        this.w_CLASOS = space(1)
        this.w_CODCLASOS = space(10)
        this.w_GGAVVISO = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAHIDARC,PAHIDSCA,PAORDFIL";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAHIDARC,PAHIDSCA,PAORDFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_HIDARC = NVL(_Link_.PAHIDARC,space(1))
      this.w_HIDSCA = NVL(_Link_.PAHIDSCA,space(1))
      this.w_ORDFIL = NVL(_Link_.PAORDFIL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_HIDARC = space(1)
      this.w_HIDSCA = space(1)
      this.w_ORDFIL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOALL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_TIPOALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_TIPOALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oTIPOALL_1_10'),i_cWhere,'',"Tipologie allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIPO = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOALL = space(5)
      endif
      this.w_DESTIPO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASALL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CLASALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_TIPOALL;
                     ,'TACODCLA',trim(this.w_CLASALL))
          select TACODICE,TACODCLA,TACLADES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCLASALL_1_11'),i_cWhere,'GSUT_MTA',"Classi allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLASALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALL;
                       ,'TACODCLA',this.w_CLASALL)
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLAAL = NVL(_Link_.TACLADES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLASALL = space(5)
      endif
      this.w_DESCLAAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRAT
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRAT))
          select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRAT)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRAT) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRAT_1_18'),i_cWhere,'GSPR_ACN',"Elenco pratiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRAT)
            select CNCODCAN,CNDESCAN,CNPATHFA,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRAT = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRAT = NVL(_Link_.CNDESCAN,space(100))
      this.w_PATHFA = NVL(_Link_.CNPATHFA,space(254))
      this.w_CN__ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_CNTIPPRA = NVL(_Link_.CNTIPPRA,space(10))
      this.w_CNMATPRA = NVL(_Link_.CNMATPRA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRAT = space(15)
      endif
      this.w_DESPRAT = space(100)
      this.w_PATHFA = space(254)
      this.w_CN__ENTE = space(10)
      this.w_CNTIPPRA = space(10)
      this.w_CNMATPRA = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GFCLASDOCU
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GFCLASDOCU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GFCLASDOCU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDMODALL";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_GFCLASDOCU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_GFCLASDOCU)
            select CDCODCLA,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GFCLASDOCU = NVL(_Link_.CDCODCLA,space(10))
      this.w_CDMODALL = NVL(_Link_.CDMODALL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GFCLASDOCU = space(10)
      endif
      this.w_CDMODALL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GFCLASDOCU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELUTEARC
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELUTEARC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_SELUTEARC);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_SELUTEARC)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SELUTEARC) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oSELUTEARC_2_15'),i_cWhere,'',"Lista utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELUTEARC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_SELUTEARC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_SELUTEARC)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELUTEARC = NVL(_Link_.CODE,0)
      this.w_ADESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SELUTEARC = 0
      endif
      this.w_ADESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELUTEARC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELUTEEXP
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELUTEEXP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_SELUTEEXP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_SELUTEEXP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SELUTEEXP) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oSELUTEEXP_2_28'),i_cWhere,'',"Lista utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELUTEEXP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_SELUTEEXP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_SELUTEEXP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELUTEEXP = NVL(_Link_.CODE,0)
      this.w_EDESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_SELUTEEXP = 0
      endif
      this.w_EDESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELUTEEXP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.value==this.w_SELCODCLA)
      this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.value=this.w_SELCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.value==this.w_SELCODCLA)
      this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.value=this.w_SELCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_9.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_9.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOALL_1_10.value==this.w_TIPOALL)
      this.oPgFrm.Page1.oPag.oTIPOALL_1_10.value=this.w_TIPOALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASALL_1_11.value==this.w_CLASALL)
      this.oPgFrm.Page1.oPag.oCLASALL_1_11.value=this.w_CLASALL
    endif
    if not(this.oPgFrm.Page1.oPag.oSELOGGETTO_1_12.value==this.w_SELOGGETTO)
      this.oPgFrm.Page1.oPag.oSELOGGETTO_1_12.value=this.w_SELOGGETTO
    endif
    if not(this.oPgFrm.Page1.oPag.oSELNOMEFILE_1_13.value==this.w_SELNOMEFILE)
      this.oPgFrm.Page1.oPag.oSELNOMEFILE_1_13.value=this.w_SELNOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELNOMEORIG_1_14.value==this.w_SELNOMEORIG)
      this.oPgFrm.Page1.oPag.oSELNOMEORIG_1_14.value=this.w_SELNOMEORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oSESTATSOS_1_17.RadioValue()==this.w_SESTATSOS)
      this.oPgFrm.Page1.oPag.oSESTATSOS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRAT_1_18.value==this.w_CODPRAT)
      this.oPgFrm.Page1.oPag.oCODPRAT_1_18.value=this.w_CODPRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oGGAVVISO_1_19.value==this.w_GGAVVISO)
      this.oPgFrm.Page1.oPag.oGGAVVISO_1_19.value=this.w_GGAVVISO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPO_1_28.value==this.w_DESTIPO)
      this.oPgFrm.Page1.oPag.oDESTIPO_1_28.value=this.w_DESTIPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAAL_1_30.value==this.w_DESCLAAL)
      this.oPgFrm.Page1.oPag.oDESCLAAL_1_30.value=this.w_DESCLAAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRAT_1_92.value==this.w_DESPRAT)
      this.oPgFrm.Page1.oPag.oDESPRAT_1_92.value=this.w_DESPRAT
    endif
    if not(this.oPgFrm.Page2.oPag.oSELNOTEALL_2_1.value==this.w_SELNOTEALL)
      this.oPgFrm.Page2.oPag.oSELNOTEALL_2_1.value=this.w_SELNOTEALL
    endif
    if not(this.oPgFrm.Page2.oPag.oMODALL_2_2.RadioValue()==this.w_MODALL)
      this.oPgFrm.Page2.oPag.oMODALL_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMODALL_2_3.RadioValue()==this.w_MODALL)
      this.oPgFrm.Page2.oPag.oMODALL_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVDATINI_2_4.value==this.w_VDATINI)
      this.oPgFrm.Page2.oPag.oVDATINI_2_4.value=this.w_VDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oVDATFIN_2_5.value==this.w_VDATFIN)
      this.oPgFrm.Page2.oPag.oVDATFIN_2_5.value=this.w_VDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSELDATARC_2_6.RadioValue()==this.w_SELDATARC)
      this.oPgFrm.Page2.oPag.oSELDATARC_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oADATINI_2_7.value==this.w_ADATINI)
      this.oPgFrm.Page2.oPag.oADATINI_2_7.value=this.w_ADATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oADATFIN_2_8.value==this.w_ADATFIN)
      this.oPgFrm.Page2.oPag.oADATFIN_2_8.value=this.w_ADATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPOFILE_2_13.RadioValue()==this.w_TIPOFILE)
      this.oPgFrm.Page2.oPag.oTIPOFILE_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPORD_2_14.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page2.oPag.oTIPORD_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELUTEARC_2_15.value==this.w_SELUTEARC)
      this.oPgFrm.Page2.oPag.oSELUTEARC_2_15.value=this.w_SELUTEARC
    endif
    if not(this.oPgFrm.Page2.oPag.oIDPUBWEB_2_16.RadioValue()==this.w_IDPUBWEB)
      this.oPgFrm.Page2.oPag.oIDPUBWEB_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBAPP_2_17.RadioValue()==this.w_IDWEBAPP)
      this.oPgFrm.Page2.oPag.oIDWEBAPP_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oADESUTE_2_18.value==this.w_ADESUTE)
      this.oPgFrm.Page2.oPag.oADESUTE_2_18.value=this.w_ADESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oSELDELFI_2_19.RadioValue()==this.w_SELDELFI)
      this.oPgFrm.Page2.oPag.oSELDELFI_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBLOCCO1_2_20.RadioValue()==this.w_BLOCCO1)
      this.oPgFrm.Page2.oPag.oBLOCCO1_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oWEBRET_2_21.RadioValue()==this.w_WEBRET)
      this.oPgFrm.Page2.oPag.oWEBRET_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oALLPRIN_2_27.RadioValue()==this.w_ALLPRIN)
      this.oPgFrm.Page2.oPag.oALLPRIN_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELUTEEXP_2_28.value==this.w_SELUTEEXP)
      this.oPgFrm.Page2.oPag.oSELUTEEXP_2_28.value=this.w_SELUTEEXP
    endif
    if not(this.oPgFrm.Page2.oPag.oARCHIVIO_2_29.value==this.w_ARCHIVIO)
      this.oPgFrm.Page2.oPag.oARCHIVIO_2_29.value=this.w_ARCHIVIO
    endif
    if not(this.oPgFrm.Page2.oPag.oChiave_2_30.value==this.w_Chiave)
      this.oPgFrm.Page2.oPag.oChiave_2_30.value=this.w_Chiave
    endif
    if not(this.oPgFrm.Page2.oPag.oIDWEBAPP_2_31.RadioValue()==this.w_IDWEBAPP)
      this.oPgFrm.Page2.oPag.oIDWEBAPP_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEXPRFILTRO_2_33.value==this.w_EXPRFILTRO)
      this.oPgFrm.Page2.oPag.oEXPRFILTRO_2_33.value=this.w_EXPRFILTRO
    endif
    if not(this.oPgFrm.Page2.oPag.oEDESUTE_2_38.value==this.w_EDESUTE)
      this.oPgFrm.Page2.oPag.oEDESUTE_2_38.value=this.w_EDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCREAZIP_1_112.RadioValue()==this.w_CREAZIP)
      this.oPgFrm.Page1.oPag.oCREAZIP_1_112.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CMODALL$'FL' and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' ))  and not(g_DOCM = 'S' and ! .w_ISALT)  and not(empty(.w_SELCODCLA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELCODCLA_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
          case   not(.w_CMODALL<>'S' and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' ) and (empty(.w_cTipo) or (.w_cTipo='SOS' and .w_CLASOS='S')))  and not(g_DOCM <> 'S' or .w_ISALT)  and (.w_cTipo # 'AET')  and not(empty(.w_SELCODCLA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELCODCLA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
          case   not(.w_VDATINI<=.w_VDATFIN or empty(.w_VDATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVDATINI_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VDATINI<=.w_VDATFIN or empty(.w_VDATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVDATFIN_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ADATINI<=.w_ADATFIN or empty(.w_ADATFIN))  and (.w_SELDATARC=1)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oADATINI_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ADATINI<=.w_ADATFIN or empty(.w_ADATFIN))  and (.w_SELDATARC=1)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oADATFIN_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSUT_MA1.CheckForm()
      if i_bres
        i_bres=  .GSUT_MA1.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELCODCLA = this.w_SELCODCLA
    this.o_TIPOALL = this.w_TIPOALL
    this.o_MODALL = this.w_MODALL
    this.o_SELDATARC = this.w_SELDATARC
    this.o_BLOCCO1 = this.w_BLOCCO1
    this.o_IDSERIAL = this.w_IDSERIAL
    * --- GSUT_MA1 : Depends On
    this.GSUT_MA1.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_kfgPag1 as StdContainer
  Width  = 791
  height = 543
  stdWidth  = 791
  stdheight = 543
  resizeXpos=561
  resizeYpos=433
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELCODCLA_1_2 as StdField with uid="PKOJJUZXPM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SELCODCLA", cQueryName = "SELCODCLA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata",;
    ToolTipText = "Codice classe libreria allegati",;
    HelpContextID = 105827202,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=154, Top=7, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_ACL", oKey_1_1="CDCODCLA", oKey_1_2="this.w_SELCODCLA"

  func oSELCODCLA_1_2.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S' and ! .w_ISALT)
    endwith
  endfunc

  func oSELCODCLA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELCODCLA_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELCODCLA_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oSELCODCLA_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_ACL',"Classi librerie allegati",''+iif((type("this.w_cTipo")="C" and this.w_cTipo="SOS") OR (type("this.parent.ocontained.w_cTipo")="C" and this.parent.ocontained.w_cTipo="SOS"),"GSDM_SOS","gsut_kfg")+'.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oSELCODCLA_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSUT_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_SELCODCLA
     i_obj.ecpSave()
  endproc

  add object oSELCODCLA_1_3 as StdField with uid="TBHDXNLUCM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SELCODCLA", cQueryName = "SELCODCLA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata",;
    ToolTipText = "Codice classe documentale",;
    HelpContextID = 105827202,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=154, Top=7, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_SELCODCLA"

  func oSELCODCLA_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_cTipo # 'AET')
    endwith
   endif
  endfunc

  func oSELCODCLA_1_3.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S' or .w_ISALT)
    endwith
  endfunc

  func oSELCODCLA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELCODCLA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELCODCLA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oSELCODCLA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",''+iif((type("this.w_cTipo")="C" and this.w_cTipo="SOS") OR (type("this.parent.ocontained.w_cTipo")="C" and this.parent.ocontained.w_cTipo="SOS"),"GSDM_SOS","gsut_kfg")+'.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oSELCODCLA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_SELCODCLA
     i_obj.ecpSave()
  endproc


  add object oBtn_1_8 as StdButton with uid="YQWTLIBGOF",left=741, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 7397926;
    , TABSTOP=.F., caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCLA_1_9 as StdField with uid="NONZZUTMSV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 52377142,;
   bGlobalFont=.t.,;
    Height=21, Width=445, Left=293, Top=7, InputMask=replicate('X',50)

  add object oTIPOALL_1_10 as StdField with uid="XPQGWCLJZD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TIPOALL", cQueryName = "TIPOALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia allegato",;
    HelpContextID = 42267850,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=154, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALL"

  func oTIPOALL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
      if .not. empty(.w_CLASALL)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTIPOALL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOALL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oTIPOALL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie allegato",'',this.parent.oContained
  endproc

  add object oCLASALL_1_11 as StdField with uid="QNZTGZLJFT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CLASALL", cQueryName = "CLASALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe allegato",;
    HelpContextID = 42066650,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=154, Top=58, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_CLASALL"

  func oCLASALL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASALL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASALL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPOALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_TIPOALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCLASALL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classi allegato",'',this.parent.oContained
  endproc
  proc oCLASALL_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_TIPOALL
     i_obj.w_TACODCLA=this.parent.oContained.w_CLASALL
     i_obj.ecpSave()
  endproc

  add object oSELOGGETTO_1_12 as AH_SEARCHFLD with uid="JJAHMJZXJU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SELOGGETTO", cQueryName = "SELOGGETTO",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indicare parole/stringhe di ricerca contenute nell'oggetto dei documenti da ricercare",;
    HelpContextID = 148577210,;
   bGlobalFont=.t.,;
    Height=21, Width=584, Left=154, Top=82, InputMask=replicate('X',100)

  add object oSELNOMEFILE_1_13 as AH_SEARCHFLD with uid="XCEDKQKDCJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SELNOMEFILE", cQueryName = "SELNOMEFILE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Inserire nome del file o parte del nome del file...",;
    HelpContextID = 257845244,;
   bGlobalFont=.t.,;
    Height=21, Width=584, Left=154, Top=108, InputMask=replicate('X',100)

  add object oSELNOMEORIG_1_14 as AH_SEARCHFLD with uid="JRAVMKLDNS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SELNOMEORIG", cQueryName = "SELNOMEORIG",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Inserire nome del file originale o parte del nome del file...",;
    HelpContextID = 257852821,;
   bGlobalFont=.t.,;
    Height=21, Width=584, Left=154, Top=134, InputMask=replicate('X',100)


  add object oObj_1_15 as cp_runprogram with uid="SIRDIJYIRW",left=420, top=732, width=219,height=22,;
    caption='Valorizzazione attributo primario per AET',;
   bGlobalFont=.t.,;
    prg="GSUT_BAE('V')",;
    cEvent = "w_CODPRAT Changed,CfgGestLoaded",;
    nPag=1;
    , HelpContextID = 178237746


  add object oSESTATSOS_1_17 as StdTableCombo with uid="VYOHPEHYCF",rtseq=14,rtrep=.f.,left=154,top=160,width=153,height=21;
    , ToolTipText = "Stato del documento in SOStitutiva";
    , HelpContextID = 92290213;
    , cFormVar="w_SESTATSOS",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='STATISOS',cKey='STATOVAL',cValue='STATODES',cOrderBy='STATOVAL',xDefault=space(18);
  , bGlobalFont=.t.


  func oSESTATSOS_1_17.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'SOS')
    endwith
  endfunc

  add object oCODPRAT_1_18 as StdField with uid="KQRLCMVUOH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODPRAT", cQueryName = "CODPRAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 59461670,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=154, Top=160, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRAT"

  func oCODPRAT_1_18.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'AET')
    endwith
  endfunc

  proc oCODPRAT_1_18.mAfter
    with this.Parent.oContained
      this.parent.ocontained .w_NEWCODPRA=this.value
    endwith
  endproc

  func oCODPRAT_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRAT_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRAT_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRAT_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',"Elenco pratiche",'',this.parent.oContained
  endproc
  proc oCODPRAT_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRAT
     i_obj.ecpSave()
  endproc

  add object oGGAVVISO_1_19 as StdField with uid="APZGNYAYAE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GGAVVISO", cQueryName = "GGAVVISO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di avviso",;
    HelpContextID = 198252725,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=691, Top=160

  func oGGAVVISO_1_19.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'SOS')
    endwith
  endfunc


  add object ZoomGF as cp_szoombox with uid="SMOEETVGMD",left=-2, top=186, width=739,height=309,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONTI",cZoomFile="GSUT_KFG",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga,Carica",;
    nPag=1;
    , HelpContextID = 129156070

  add object oDESTIPO_1_28 as StdField with uid="IHXQIEVZXT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESTIPO", cQueryName = "DESTIPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 33568310,;
   bGlobalFont=.t.,;
    Height=21, Width=392, Left=220, Top=32, InputMask=replicate('X',40)

  add object oDESCLAAL_1_30 as StdField with uid="GNLVBCYZIJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCLAAL", cQueryName = "DESCLAAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 52377218,;
   bGlobalFont=.t.,;
    Height=21, Width=392, Left=220, Top=58, InputMask=replicate('X',40)


  add object oBtn_1_33 as StdButton with uid="GJZUUMSDLJ",left=2, top=498, width=48,height=45,;
    CpPicture="bmp\codici.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento selezionato";
    , HelpContextID = 246423958;
    , caption='A\<pri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"V",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE) )
      endwith
    endif
  endfunc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' or .w_SELMULT OR .w_FLPROV='S')
     endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="BGOOKNIKBY",left=51, top=498, width=48,height=45,;
    CpPicture="BMP\apertura.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'indice del documento archiviato";
    , HelpContextID = 146173830;
    , caption='Vi\<sualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"I",.w_GFKEYINDIC,"","")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_INDICE))
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SELMULT)
     endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="PQVUDGYYCZ",left=100, top=498, width=48,height=45,;
    CpPicture="BMP\COPY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per copiare su un disco il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 113170789;
    , caption='C\<opia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"E",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' and ! .w_SELMULT OR .w_FLPROV='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="ZLDYMKRJAC",left=149, top=498, width=48,height=45,;
    CpPicture="BMP\MAIL.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inviare tramite mail il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 60735235;
    , caption='\<Invia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"M",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE) or g_INVIO='O' )
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N'  and ! .w_SELMULT and  g_INVIO<>'O' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="YCZHRKWETC",left=198, top=498, width=48,height=45,;
    CpPicture="BMP\FXMAIL_PEC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inviare tramite PEC il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 60265467;
    , caption='\<PEC';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"M",.w_Archivio,.w_Chiave,.w_GFPERIODO,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE) or g_INVIO='O' )
      endwith
    endif
  endfunc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N'  and ! .w_SELMULT and  g_INVIO<>'O' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="GPAAAIPTUF",left=247, top=498, width=48,height=45,;
    CpPicture="bmp\mail_pdf.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per trasformare in pdf ed inviare tramite mail il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 48396810;
    , caption='Pd\<f';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"G"," "," ",.w_periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_SELCODCLA) or (!.w_SELMULT and (.w_ISFILE='N' or .w_cTipo='SOS' or not CHECKPRINTTO('.' + justext(.w_GFNOMEFILE)))) or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="GBNLIJIDJE",left=296, top=498, width=48,height=45,;
    CpPicture="BMP\MAILARCH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inviare tramite mail i file selezionati con il segno di spunta ed archiviare la mail inviata (senza spuntare nessun file si archivia la mail inviata senza allegati)";
    , HelpContextID = 208769257;
    , caption='Arc\<h. mail';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"A",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_GFNOMEFILE) and .w_SELMULT) or g_INVIO='O' )
      endwith
    endif
  endfunc

  func oBtn_1_39.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_INVIO<>'O' OR (.w_ISFILE='N' and ! .w_SELMULT and g_INVIO<>'O') or !.w_ISALT or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="RGXKHLKREU",left=345, top=498, width=48,height=45,;
    CpPicture="BMP\MAILARCH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inviare tramite mail i file selezionati con il segno di spunta ed archiviare la mail inviata (senza spuntare nessun file si archivia la mail inviata senza allegati)";
    , HelpContextID = 14539287;
    , caption='Arc\<h. PEC';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"A",.w_Archivio,.w_Chiave,.w_GFPERIODO,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_GFNOMEFILE) and .w_SELMULT) or g_INVIO='O' )
      endwith
    endif
  endfunc

  func oBtn_1_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_INVIO<>'O' OR (.w_ISFILE='N' and ! .w_SELMULT and g_INVIO<>'O') or !.w_ISALT or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="MBXAIGGBZE",left=741, top=58, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa Elenco documenti archiviati";
    , HelpContextID = 92948006;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        do GSUT_BLS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="QPSJARMSUV",left=394, top=498, width=48,height=45,;
    CpPicture="BMP\ELIMINA2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 35306822;
    , caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"D",.w_Archivio,.w_Chiave,.w_Periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' and ! .w_SELMULT OR .w_FLPROV='S')
     endwith
    endif
  endfunc


  add object oBtn_1_43 as StdButton with uid="FKYRBZXCGY",left=443, top=498, width=48,height=45,;
    CpPicture="bmp\document_certificate.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per apporre la firma digitale al documento archiviato";
    , HelpContextID = 207926698;
    , caption='Fir\<ma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSUT_BFI(this.Parent.oContained,.w_ESTENS, .w_ORIFIL, .w_GFNOMEFILE, .w_GFCLASDOCU, alltrim(.w_GFORIGFILE), .w_CDMODALL,.w_GFCLASDOCU,.w_GFDESCFILE,.w_GFKEYINDIC,.w_Archivio,.w_Chiave)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE) and not empty(.w_GFCLASDOCU) and not empty(.w_ARCHIVIO) and ! empty(.w_SELCODCLA))
      endwith
    endif
  endfunc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (ALLTRIM(UPPER(.w_ESTENS))='P7M' or .w_ISFILE='N' or g_DOCM<>'S' or empty(.w_GFCLASDOCU) or empty(.w_ARCHIVIO) or .w_SELMULT or .w_MODALL='E' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_44 as StdButton with uid="EZZKKFWEFK",left=590, top=498, width=48,height=45,;
    CpPicture="..\vfcsim\fxinf.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inviare ad applicazione web il documento selezionato";
    , HelpContextID = 90730520;
    , caption='Invia \<Web';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"W",.w_Archivio,.w_Chiave,.w_Periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SELMULT AND not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_CPIN<>'S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_45 as StdButton with uid="CKNHTSNWUY",left=741, top=200, width=48,height=45,;
    CpPicture="BMP\FOLDER_EDIT.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per archiviare un nuovo documento da un modello di riferimento";
    , HelpContextID = 3116427;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"N",.w_Archivio,IIF(Not empty(.w_CODPRAT),.w_CODPRAT,.w_Chiave),.w_Periodo,iif(vartype(.oParentObject)="O",.oParentObject,.null.))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' or  empty(.w_SELCODCLA) or .w_cTipo='SOS' or (.w_cTipo='AET' and empty(.w_CODPRAT)) or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="IIQPSVJWSW",left=741, top=153, width=48,height=45,;
    CpPicture="BMP\ZARCHIVIA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per archiviare un nuovo documento da disco";
    , HelpContextID = 99261593;
    , caption='\<Archivia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"C",.w_Archivio,.w_Chiave,.w_Periodo,iif(vartype(.oParentObject)="O",.oParentObject,.null.))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(.w_SELCODCLA))
      endwith
    endif
  endfunc

  func oBtn_1_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_SELCODCLA)  or g_DOCM<>'S' or .w_cTipo='SOS' or (.w_cTipo='AET' and empty(.w_CODPRAT)) or .w_HIDARC='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_47 as StdButton with uid="XPELXZOFEE",left=741, top=153, width=48,height=45,;
    CpPicture="bmp\carlotti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per creare il file evidenze.seq";
    , HelpContextID = 226396827;
    , caption='\<Crea .seq';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSDM_BCZ(this.Parent.oContained,"SEQ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(.w_CODCLASOS))
      endwith
    endif
  endfunc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipo # 'SOS')
     endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="PTINFOKJAL",left=741, top=200, width=48,height=45,;
    CpPicture="root.ico", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare un metodo WS SOStitutiva";
    , HelpContextID = 95031782;
    , caption='\<Oper. WS';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSDM_BCZ(this.Parent.oContained,"MET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(.w_CODCLASOS))
      endwith
    endif
  endfunc

  func oBtn_1_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipo # 'SOS')
     endwith
    endif
  endfunc


  add object oBtn_1_49 as StdButton with uid="VZJWFPOZEX",left=741, top=247, width=48,height=45,;
    CpPicture="BMP\SCANNER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per archiviare un nuovo documento da scanner";
    , HelpContextID = 158020902;
    , caption='S\<canner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"S",.w_Archivio,.w_Chiave,.w_Periodo,iif(vartype(.oParentObject)="O",.oParentObject,.null.))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_49.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(.w_SELCODCLA))
      endwith
    endif
  endfunc

  func oBtn_1_49.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_SELCODCLA)  or g_DOCM<>'S' or .w_cTipo='SOS' or (.w_cTipo='AET' and empty(.w_CODPRAT)) OR .w_HIDSCA='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_50 as StdButton with uid="CAZBSVDBWV",left=741, top=294, width=48,height=45,;
    CpPicture="COPY.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per archiviare un nuovo documento duplicando il documento archiviato";
    , HelpContextID = 211426566;
    , caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"U",.w_Archivio,.w_Chiave,.w_Periodo,iif(vartype(.oParentObject)="O",.oParentObject,.null.))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_50.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFCLASDOCU) and not empty(.w_ARCHIVIO) and ! empty(.w_SELCODCLA))
      endwith
    endif
  endfunc

  func oBtn_1_50.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' or empty(.w_GFCLASDOCU) or empty(.w_ARCHIVIO) or g_DOCM<>'S' or .w_CDMODALL='E'  or .w_SELMULT or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_51 as StdButton with uid="NBZVJTSQXB",left=741, top=341, width=48,height=45,;
    CpPicture="bmp\FOLDER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la cartella in cui si trova il documento archiviato";
    , HelpContextID = 42436280;
    , caption='\<Apri cart.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        stapdf(.w_PERCARC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (! empty(.w_PERCARC) and ! .w_SELMULT)
      endwith
    endif
  endfunc

  func oBtn_1_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_cTipo # 'AET' OR .w_FLPROV='S' or empty(.w_PERCARC) or  .w_SELMULT or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_52 as StdButton with uid="HUFVSRGLOH",left=741, top=388, width=48,height=45,;
    CpPicture="BMP\SBLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rendere provvisorio l'indice del singolo file evidenziato (o dei file selezionati con il segno di spunta)";
    , HelpContextID = 113042470;
    , caption='Sb\<locca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"F",.w_Archivio,.w_Chiave,.w_Periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE) and cp_IsAdministrator())
      endwith
    endif
  endfunc

  func oBtn_1_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLPROV='N')
     endwith
    endif
  endfunc


  add object oBtn_1_54 as StdButton with uid="RJDCGTGPPQ",left=741, top=388, width=48,height=45,;
    CpPicture="BMP\BLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'indice del singolo file evidenziato (o dei file selezionati con il segno di spunta)";
    , HelpContextID = 78716182;
    , caption='B\<locca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"B",.w_Archivio,.w_Chiave,.w_Periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLPROV='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_56 as StdButton with uid="LUXVPJGBKY",left=741, top=435, width=48,height=45,;
    CpPicture="..\vfcsim\fxinf.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per archiviare su applicazione web il documento selezionato";
    , HelpContextID = 267033850;
    , caption='Archi \<Inf';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"WA",.w_Archivio,.w_Chiave,.w_Periodo)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SELMULT AND not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_56.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((g_CPIN<>'S' or g_DOCM # 'S') AND g_DMIP<>'S')
     endwith
    endif
  endfunc


  add object oObj_1_75 as cp_runprogram with uid="FMLQFGPMYM",left=-1, top=598, width=334,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSUT_BES(w_Archivio,w_Chiave)",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 129156070


  add object oObj_1_76 as cp_runprogram with uid="OCUAYFLIXD",left=-1, top=621, width=334,height=19,;
    caption='Visualizza  indice collegato',;
   bGlobalFont=.t.,;
    prg="GSUT_BFG('I',w_GFKEYINDIC,'','')",;
    cEvent = "w_ZoomGF selected",;
    nPag=1;
    , HelpContextID = 131999778


  add object oObj_1_77 as cp_runprogram with uid="MPZOEKPFQP",left=-1, top=646, width=334,height=19,;
    caption='Gestione operazione allegato',;
   bGlobalFont=.t.,;
    prg="GSUT_BEO",;
    cEvent = "GestOp",;
    nPag=1;
    , HelpContextID = 240923897


  add object oObj_1_78 as cp_outputCombo with uid="ZLERXIVPWT",left=420, top=573, width=219,height=19,;
    caption='Per stampa',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",Visible=.f.,;
    nPag=1;
    , HelpContextID = 141471587


  add object oObj_1_84 as cp_runprogram with uid="JJPVZCCDYV",left=-1, top=701, width=334,height=19,;
    caption='Gestione selezione/deselezione indici',;
   bGlobalFont=.t.,;
    prg="GSUT_BII('S')",;
    cEvent = "w_SELEZIONE Changed",;
    nPag=1;
    , HelpContextID = 252935137


  add object oObj_1_85 as cp_runprogram with uid="NWQJLRCSNS",left=-1, top=728, width=334,height=19,;
    caption='Controllo congruit� della selezione',;
   bGlobalFont=.t.,;
    prg="GSUT_BII('Z')",;
    cEvent = "ZoomGF row checked",;
    nPag=1;
    , HelpContextID = 106365348


  add object oObj_1_87 as cp_runprogram with uid="MGWRRHLHMW",left=-1, top=755, width=334,height=19,;
    caption='Controllo numero selezioni',;
   bGlobalFont=.t.,;
    prg="GSUT_BII('U')",;
    cEvent = "ZoomGF row unchecked",;
    nPag=1;
    , HelpContextID = 255767232


  add object oObj_1_91 as cp_runprogram with uid="RPOHUFJCRI",left=420, top=662, width=296,height=19,;
    caption='Inizializzazione classe documentale per AET',;
   bGlobalFont=.t.,;
    prg="GSUT_BAE('I')",;
    cEvent = "Init,CfgGestLoaded",;
    nPag=1;
    , HelpContextID = 28809208

  add object oDESPRAT_1_92 as StdField with uid="TWDUPXIWMC",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESPRAT", cQueryName = "DESPRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 59520566,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=298, Top=160, InputMask=replicate('X',100)

  func oDESPRAT_1_92.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'AET')
    endwith
  endfunc


  add object oBtn_1_95 as StdButton with uid="ICYYFHFWMM",left=492, top=498, width=48,height=45,;
    CpPicture="BMP\FOLDERZIP.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esportare in un file zip il singolo file evidenziato (o i file selezionati con il segno di spunta)";
    , HelpContextID = 237032582;
    , caption='\<Zip';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_95.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"Z",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_95.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_95.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' and ! .w_SELMULT OR .w_FLPROV='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_96 as StdButton with uid="ADDMRHTNBU",left=541, top=498, width=48,height=45,;
    CpPicture="BMP\FolderZipIn.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i documenti archiviati importando i file precedentemente esportati in formato zip";
    , HelpContextID = 195888806;
    , caption='Impor\<ta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_96.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"R",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_96.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GFNOMEFILE))
      endwith
    endif
  endfunc

  func oBtn_1_96.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISFILE='N' and ! .w_SELMULT OR .w_FLPROV='S' or g_DMIP='S')
     endwith
    endif
  endfunc


  add object oBtn_1_102 as StdButton with uid="QYJZXPKSLD",left=741, top=106, width=48,height=45,;
    CpPicture="BMP\comm.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per sincronizzare file";
    , HelpContextID = 61316399;
    , caption='Sincr\<o';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_102.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"K",.w_Archivio,.w_Chiave,.w_GFPERIODO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_102.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATHFA))
      endwith
    endif
  endfunc

  func oBtn_1_102.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLPROV='S' OR !.w_ISALT)
     endwith
    endif
  endfunc


  add object oObj_1_104 as cp_runprogram with uid="LYUWQEYQVX",left=663, top=581, width=344,height=22,;
    caption='GSUT_BES(w_ARCHIVIO,w_CHIAVE,QUERY2)',;
   bGlobalFont=.t.,;
    prg="GSUT_BES(w_ARCHIVIO,w_CHIAVE,'QUERY2')",;
    cEvent = "Ricerca,CfgGestLoaded",;
    nPag=1;
    , HelpContextID = 144716063


  add object oBtn_1_109 as StdButton with uid="VVCSESRAJP",left=641, top=498, width=48,height=45,;
    CpPicture="bmp\Check.ico", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le righe";
    , HelpContextID = 182261978;
    ,  Caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_109.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"checkall")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_110 as StdButton with uid="DSBYBNBLSB",left=690, top=498, width=48,height=45,;
    CpPicture="bmp\unCheck.ico", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le righe";
    , HelpContextID = 196913610;
    ,  Caption='\<Deselez';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_110.Click()
      with this.Parent.oContained
        GSUT_BFG(this.Parent.oContained,"uncheckall")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_111 as StdButton with uid="ONORYGBAFF",left=741, top=498, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41524154;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_111.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCREAZIP_1_112 as StdCheck with uid="DLKKEPBIZR",rtseq=86,rtrep=.f.,left=615, top=58, caption="Crea zip unico",;
    ToolTipText = "Se attivo crea zip file cumulativo",;
    HelpContextID = 201089830,;
    cFormVar="w_CREAZIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCREAZIP_1_112.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCREAZIP_1_112.GetRadio()
    this.Parent.oContained.w_CREAZIP = this.RadioValue()
    return .t.
  endfunc

  func oCREAZIP_1_112.SetRadio()
    this.Parent.oContained.w_CREAZIP=trim(this.Parent.oContained.w_CREAZIP)
    this.value = ;
      iif(this.Parent.oContained.w_CREAZIP=='S',1,;
      0)
  endfunc

  func oCREAZIP_1_112.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'SOS')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="SOSAKNBJEM",Visible=.t., Left=46, Top=83,;
    Alignment=1, Width=105, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="CBOUTPXTJD",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe libreria allegati:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="AFGTZIPGXT",Visible=.t., Left=18, Top=35,;
    Alignment=1, Width=133, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="OPLAJOUHVU",Visible=.t., Left=66, Top=60,;
    Alignment=1, Width=85, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="LEJYNPZFDU",Visible=.t., Left=0, Top=174,;
    Alignment=2, Width=140, Height=17,;
    Caption="Allegato inesistente"    , forecolor = rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (!.w_ISFILE = 'N')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="DZSKQUBPYR",Visible=.t., Left=6, Top=109,;
    Alignment=1, Width=145, Height=18,;
    Caption="Nome file archiviato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="RORDGCRGKZ",Visible=.t., Left=2, Top=135,;
    Alignment=1, Width=149, Height=18,;
    Caption="Nome file originale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="FPCBZHJYNL",Visible=.t., Left=29, Top=161,;
    Alignment=1, Width=122, Height=18,;
    Caption="Stato SOS:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'SOS')
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="ZAEVWZNPQU",Visible=.t., Left=560, Top=161,;
    Alignment=1, Width=128, Height=18,;
    Caption="Giorni avviso:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'SOS')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="FOFPNBOHMW",Visible=.t., Left=97, Top=161,;
    Alignment=1, Width=54, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_cTipo # 'AET')
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="BWPJHUPIDO",Visible=.t., Left=18, Top=8,;
    Alignment=1, Width=133, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S')
    endwith
  endfunc
enddefine
define class tgsut_kfgPag2 as StdContainer
  Width  = 791
  height = 543
  stdWidth  = 791
  stdheight = 543
  resizeXpos=382
  resizeYpos=318
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELNOTEALL_2_1 as AH_SEARCHFLD with uid="QUNURGOFYL",rtseq=54,rtrep=.f.,;
    cFormVar = "w_SELNOTEALL", cQueryName = "SELNOTEALL",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Nota o parte di essa",;
    HelpContextID = 161867737,;
   bGlobalFont=.t.,;
    Height=21, Width=581, Left=156, Top=10


  add object oMODALL_2_2 as StdCombo with uid="FLYNJLXNER",rtseq=55,rtrep=.f.,left=157,top=38,width=177,height=21;
    , ToolTipText = "Modalit� archivizione";
    , HelpContextID = 236736710;
    , cFormVar="w_MODALL",RowSource=""+"Copia file,"+"Collegamento,"+"Extended document server,"+"Infinity D.M.S.,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oMODALL_2_2.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'L',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oMODALL_2_2.GetRadio()
    this.Parent.oContained.w_MODALL = this.RadioValue()
    return .t.
  endfunc

  func oMODALL_2_2.SetRadio()
    this.Parent.oContained.w_MODALL=trim(this.Parent.oContained.w_MODALL)
    this.value = ;
      iif(this.Parent.oContained.w_MODALL=='F',1,;
      iif(this.Parent.oContained.w_MODALL=='L',2,;
      iif(this.Parent.oContained.w_MODALL=='E',3,;
      iif(this.Parent.oContained.w_MODALL=='I',4,;
      iif(this.Parent.oContained.w_MODALL=='T',5,;
      0)))))
  endfunc

  func oMODALL_2_2.mHide()
    with this.Parent.oContained
      return (g_DOCM # 'S' OR .w_ISALT)
    endwith
  endfunc


  add object oMODALL_2_3 as StdCombo with uid="QHPORAQAXE",rtseq=56,rtrep=.f.,left=157,top=38,width=131,height=21;
    , ToolTipText = "Modalit� archivizione";
    , HelpContextID = 236736710;
    , cFormVar="w_MODALL",RowSource=""+"Copia file,"+"Collegamento,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oMODALL_2_3.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'L',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oMODALL_2_3.GetRadio()
    this.Parent.oContained.w_MODALL = this.RadioValue()
    return .t.
  endfunc

  func oMODALL_2_3.SetRadio()
    this.Parent.oContained.w_MODALL=trim(this.Parent.oContained.w_MODALL)
    this.value = ;
      iif(this.Parent.oContained.w_MODALL=='F',1,;
      iif(this.Parent.oContained.w_MODALL=='L',2,;
      iif(this.Parent.oContained.w_MODALL=='T',3,;
      0)))
  endfunc

  func oMODALL_2_3.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S' and ! .w_ISALT )
    endwith
  endfunc

  add object oVDATINI_2_4 as StdField with uid="ZGKHQIAYTE",rtseq=57,rtrep=.f.,;
    cFormVar = "w_VDATINI", cQueryName = "VDATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da inizio validit�",;
    HelpContextID = 59818,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=524, Top=38

  func oVDATINI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VDATINI<=.w_VDATFIN or empty(.w_VDATFIN))
    endwith
    return bRes
  endfunc

  add object oVDATFIN_2_5 as StdField with uid="SFBXBTDGIG",rtseq=58,rtrep=.f.,;
    cFormVar = "w_VDATFIN", cQueryName = "VDATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A fine validit�",;
    HelpContextID = 181343830,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=660, Top=38

  func oVDATFIN_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VDATINI<=.w_VDATFIN or empty(.w_VDATFIN))
    endwith
    return bRes
  endfunc


  add object oSELDATARC_2_6 as StdCombo with uid="RPSCJEWDQM",rtseq=59,rtrep=.f.,left=157,top=67,width=177,height=21;
    , ToolTipText = "Selezione intervallo temporale di ricerca";
    , HelpContextID = 91212712;
    , cFormVar="w_SELDATARC",RowSource=""+"Ultimi 7 giorni,"+"Ultimi 15 giorni,"+"Ultimi 30 giorni (mese),"+"Ultimi 90 giorni (trimestre),"+"Ultimi 180 giorni (semestre),"+"Ultimi 360 giorni (anno),"+"Selezione manuale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSELDATARC_2_6.RadioValue()
    return(iif(this.value =1,7,;
    iif(this.value =2,15,;
    iif(this.value =3,30,;
    iif(this.value =4,90,;
    iif(this.value =5,180,;
    iif(this.value =6,360,;
    iif(this.value =7,1,;
    0))))))))
  endfunc
  func oSELDATARC_2_6.GetRadio()
    this.Parent.oContained.w_SELDATARC = this.RadioValue()
    return .t.
  endfunc

  func oSELDATARC_2_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SELDATARC==7,1,;
      iif(this.Parent.oContained.w_SELDATARC==15,2,;
      iif(this.Parent.oContained.w_SELDATARC==30,3,;
      iif(this.Parent.oContained.w_SELDATARC==90,4,;
      iif(this.Parent.oContained.w_SELDATARC==180,5,;
      iif(this.Parent.oContained.w_SELDATARC==360,6,;
      iif(this.Parent.oContained.w_SELDATARC==1,7,;
      0)))))))
  endfunc

  add object oADATINI_2_7 as StdField with uid="VBSGOJUTVU",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ADATINI", cQueryName = "ADATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data archiviazione",;
    HelpContextID = 60154,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=524, Top=67

  func oADATINI_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELDATARC=1)
    endwith
   endif
  endfunc

  func oADATINI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ADATINI<=.w_ADATFIN or empty(.w_ADATFIN))
    endwith
    return bRes
  endfunc

  add object oADATFIN_2_8 as StdField with uid="QGXZMKIZDR",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ADATFIN", cQueryName = "ADATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A data archiviazione",;
    HelpContextID = 181343494,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=660, Top=67

  func oADATFIN_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELDATARC=1)
    endwith
   endif
  endfunc

  func oADATFIN_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ADATINI<=.w_ADATFIN or empty(.w_ADATFIN))
    endwith
    return bRes
  endfunc


  add object oTIPOFILE_2_13 as StdZTamTableCombo with uid="FAJJZHLEUF",rtseq=62,rtrep=.f.,left=157,top=95,width=177,height=21;
    , ToolTipText = "Ricerca dei file archiviati in base all'estensione o in base al raggruppamento di estensioni (campo vuoto = nessuna selezione)";
    , HelpContextID = 87356549;
    , cFormVar="w_TIPOFILE",tablefilter="", bObbl = .f. , nPag = 2;
    , cTable='QUERY\TIPIFILE.VQR',cKey='IDTIPFIL',cValue='RADESCRI',cOrderBy='RADESCRI',xDefault=space(254);
  , bGlobalFont=.t.



  add object oTIPORD_2_14 as StdCombo with uid="CUTXOCATUO",rtseq=63,rtrep=.f.,left=622,top=99,width=153,height=21;
    , HelpContextID = 109775670;
    , cFormVar="w_TIPORD",RowSource=""+"Data archiviazione file,"+"Descrizione file", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPORD_2_14.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPORD_2_14.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_2_14.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='D',1,;
      iif(this.Parent.oContained.w_TIPORD=='F',2,;
      0))
  endfunc

  add object oSELUTEARC_2_15 as StdField with uid="JGHXQOLBII",rtseq=64,rtrep=.f.,;
    cFormVar = "w_SELUTEARC", cQueryName = "SELUTEARC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha eseguito l'archiviazione",;
    HelpContextID = 129026984,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=123, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_SELUTEARC"

  func oSELUTEARC_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELUTEARC_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELUTEARC_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oSELUTEARC_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lista utenti",'',this.parent.oContained
  endproc


  add object oIDPUBWEB_2_16 as StdCombo with uid="FVWSLSZOXF",rtseq=65,rtrep=.f.,left=623,top=123,width=150,height=21;
    , ToolTipText = "Selezione degli allegati in base al flag pubblica sul web (CPZ)";
    , HelpContextID = 124713528;
    , cFormVar="w_IDPUBWEB",RowSource=""+"Tutti,"+"Solo pubblicabili,"+"Solo non pubblicabili", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDPUBWEB_2_16.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oIDPUBWEB_2_16.GetRadio()
    this.Parent.oContained.w_IDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oIDPUBWEB_2_16.SetRadio()
    this.Parent.oContained.w_IDPUBWEB=trim(this.Parent.oContained.w_IDPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_IDPUBWEB=='T',1,;
      iif(this.Parent.oContained.w_IDPUBWEB=='S',2,;
      iif(this.Parent.oContained.w_IDPUBWEB=='N',3,;
      0)))
  endfunc

  func oIDPUBWEB_2_16.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oIDWEBAPP_2_17 as StdCombo with uid="ANKYTXNENJ",rtseq=66,rtrep=.f.,left=623,top=123,width=150,height=21;
    , ToolTipText = "Tipo archiviazione";
    , HelpContextID = 42038742;
    , cFormVar="w_IDWEBAPP",RowSource=""+"Tutti,"+"Archiviazione Web,"+"Archiviazione Std,"+"Archiviazione Std e Web", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDWEBAPP_2_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oIDWEBAPP_2_17.GetRadio()
    this.Parent.oContained.w_IDWEBAPP = this.RadioValue()
    return .t.
  endfunc

  func oIDWEBAPP_2_17.SetRadio()
    this.Parent.oContained.w_IDWEBAPP=trim(this.Parent.oContained.w_IDWEBAPP)
    this.value = ;
      iif(this.Parent.oContained.w_IDWEBAPP=='T',1,;
      iif(this.Parent.oContained.w_IDWEBAPP=='S',2,;
      iif(this.Parent.oContained.w_IDWEBAPP=='N',3,;
      iif(this.Parent.oContained.w_IDWEBAPP=='A',4,;
      0))))
  endfunc

  func oIDWEBAPP_2_17.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' or g_DMIP='S')
    endwith
  endfunc

  add object oADESUTE_2_18 as StdField with uid="YSFLLGWIXG",rtseq=67,rtrep=.f.,;
    cFormVar = "w_ADESUTE", cQueryName = "ADESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 155298554,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=221, Top=123, InputMask=replicate('X',20)

  add object oSELDELFI_2_19 as StdCheck with uid="ATUFCLPYYY",rtseq=68,rtrep=.f.,left=157, top=150, caption="Non visualizzare documenti inesistenti",;
    ToolTipText = "Se attivo, non visualizza gli indici dei documenti cancellati, spostati o rinominati.",;
    HelpContextID = 38811793,;
    cFormVar="w_SELDELFI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELDELFI_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oSELDELFI_2_19.GetRadio()
    this.Parent.oContained.w_SELDELFI = this.RadioValue()
    return .t.
  endfunc

  func oSELDELFI_2_19.SetRadio()
    this.Parent.oContained.w_SELDELFI=trim(this.Parent.oContained.w_SELDELFI)
    this.value = ;
      iif(this.Parent.oContained.w_SELDELFI=='S',1,;
      0)
  endfunc

  func oSELDELFI_2_19.mHide()
    with this.Parent.oContained
      return (EMPTY(NVL(.w_SELCODCLA,'')))
    endwith
  endfunc


  add object oBLOCCO1_2_20 as StdCombo with uid="RXMKPBIEFX",rtseq=69,rtrep=.f.,left=622,top=148,width=119,height=21;
    , ToolTipText = "Selezione degli indici in base allo stato degli stessi";
    , HelpContextID = 9370902;
    , cFormVar="w_BLOCCO1",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oBLOCCO1_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oBLOCCO1_2_20.GetRadio()
    this.Parent.oContained.w_BLOCCO1 = this.RadioValue()
    return .t.
  endfunc

  func oBLOCCO1_2_20.SetRadio()
    this.Parent.oContained.w_BLOCCO1=trim(this.Parent.oContained.w_BLOCCO1)
    this.value = ;
      iif(this.Parent.oContained.w_BLOCCO1=='S',1,;
      iif(this.Parent.oContained.w_BLOCCO1=='N',2,;
      iif(this.Parent.oContained.w_BLOCCO1=='T',3,;
      0)))
  endfunc


  add object oWEBRET_2_21 as StdCombo with uid="NYALRKTPBD",value=1,rtseq=70,rtrep=.f.,left=623,top=177,width=153,height=21;
    , ToolTipText = "Stato conferma archiviazione web (S=inviato, E=errori nell'invio, Blank=da inviare)";
    , HelpContextID = 96282470;
    , cFormVar="w_WEBRET",RowSource=""+"Da inviare,"+"Archiviato,"+"Errore di archiviazione,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oWEBRET_2_21.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'T',;
    space(15))))))
  endfunc
  func oWEBRET_2_21.GetRadio()
    this.Parent.oContained.w_WEBRET = this.RadioValue()
    return .t.
  endfunc

  func oWEBRET_2_21.SetRadio()
    this.Parent.oContained.w_WEBRET=trim(this.Parent.oContained.w_WEBRET)
    this.value = ;
      iif(this.Parent.oContained.w_WEBRET=='',1,;
      iif(this.Parent.oContained.w_WEBRET=='S',2,;
      iif(this.Parent.oContained.w_WEBRET=='E',3,;
      iif(this.Parent.oContained.w_WEBRET=='T',4,;
      0))))
  endfunc

  func oWEBRET_2_21.mHide()
    with this.Parent.oContained
      return (.w_MODALL # 'I' AND g_DMIP#'S')
    endwith
  endfunc

  add object oALLPRIN_2_27 as StdCheck with uid="FEMIEMJLDG",rtseq=71,rtrep=.f.,left=405, top=123, caption="Solo principali",;
    ToolTipText = "Se attivo vengono estratti solo gli allegati principali",;
    HelpContextID = 193711366,;
    cFormVar="w_ALLPRIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oALLPRIN_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oALLPRIN_2_27.GetRadio()
    this.Parent.oContained.w_ALLPRIN = this.RadioValue()
    return .t.
  endfunc

  func oALLPRIN_2_27.SetRadio()
    this.Parent.oContained.w_ALLPRIN=trim(this.Parent.oContained.w_ALLPRIN)
    this.value = ;
      iif(this.Parent.oContained.w_ALLPRIN=='S',1,;
      0)
  endfunc

  add object oSELUTEEXP_2_28 as StdField with uid="YBWNQXLWRE",rtseq=72,rtrep=.f.,;
    cFormVar = "w_SELUTEEXP", cQueryName = "SELUTEEXP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha eseguito l'esportazione",;
    HelpContextID = 129027198,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=172, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_SELUTEEXP"

  func oSELUTEEXP_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELUTEEXP_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELUTEEXP_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oSELUTEEXP_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lista utenti",'',this.parent.oContained
  endproc

  add object oARCHIVIO_2_29 as StdField with uid="YBQBJWBMMB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ARCHIVIO", cQueryName = "ARCHIVIO",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella di filtro...",;
    HelpContextID = 135052459,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=157, Top=201, InputMask=replicate('X',20)

  func oARCHIVIO_2_29.mHide()
    with this.Parent.oContained
      return (! EMPTY(.w_SELCODCLA))
    endwith
  endfunc

  add object oChiave_2_30 as StdField with uid="BOJGVJVNPW",rtseq=74,rtrep=.f.,;
    cFormVar = "w_Chiave", cQueryName = "Chiave",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Chiave di ricerca...",;
    HelpContextID = 165591334,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=402, Top=201, InputMask=replicate('X',50)

  func oChiave_2_30.mHide()
    with this.Parent.oContained
      return (! EMPTY(.w_SELCODCLA))
    endwith
  endfunc


  add object oIDWEBAPP_2_31 as StdCombo with uid="ZPZNPJXLYI",rtseq=75,rtrep=.f.,left=622,top=123,width=150,height=21;
    , ToolTipText = "Tipo archiviazione";
    , HelpContextID = 42038742;
    , cFormVar="w_IDWEBAPP",RowSource=""+"Tutti,"+"Archiviazione Web,"+"Archiviazione Std,"+"Archiviazione Std e Web,"+"In trasferimento,"+"Trasferito", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIDWEBAPP_2_31.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'A',;
    iif(this.value =5,'D',;
    iif(this.value =6,'X',;
    space(1))))))))
  endfunc
  func oIDWEBAPP_2_31.GetRadio()
    this.Parent.oContained.w_IDWEBAPP = this.RadioValue()
    return .t.
  endfunc

  func oIDWEBAPP_2_31.SetRadio()
    this.Parent.oContained.w_IDWEBAPP=trim(this.Parent.oContained.w_IDWEBAPP)
    this.value = ;
      iif(this.Parent.oContained.w_IDWEBAPP=='T',1,;
      iif(this.Parent.oContained.w_IDWEBAPP=='S',2,;
      iif(this.Parent.oContained.w_IDWEBAPP=='N',3,;
      iif(this.Parent.oContained.w_IDWEBAPP=='A',4,;
      iif(this.Parent.oContained.w_IDWEBAPP=='D',5,;
      iif(this.Parent.oContained.w_IDWEBAPP=='X',6,;
      0))))))
  endfunc

  func oIDWEBAPP_2_31.mHide()
    with this.Parent.oContained
      return (g_DMIP#'S')
    endwith
  endfunc

  add object oEXPRFILTRO_2_33 as StdMemo with uid="CFMIVGJPYA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_EXPRFILTRO", cQueryName = "EXPRFILTRO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di filtro per ricerca avanzata (su attributi classe documentale)",;
    HelpContextID = 87134790,;
   bGlobalFont=.t.,;
    Height=58, Width=763, Left=10, Top=483

  func oEXPRFILTRO_2_33.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S')
    endwith
  endfunc


  add object oLinkPC_2_34 as stdDynamicChildContainer with uid="HSNPGHVISJ",left=10, top=227, width=763, height=253, bOnScreen=.t.;


  func oLinkPC_2_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SELCODCLA,space(10))))
      endwith
    endif
  endfunc

  func oLinkPC_2_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM <> 'S')
     endwith
    endif
  endfunc


  add object oBtn_2_35 as StdButton with uid="DHWVRTXNMQ",left=741, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 7397926;
    , TABSTOP=.F., caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_35.Click()
      with this.Parent.oContained
        GSUT_BES(this.Parent.oContained,.w_ARCHIVIO,.w_CHIAVE,"QUERY2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEDESUTE_2_38 as StdField with uid="FETXWPXUKV",rtseq=78,rtrep=.f.,;
    cFormVar = "w_EDESUTE", cQueryName = "EDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 155298490,;
   bGlobalFont=.t.,;
    Height=21, Width=175, Left=221, Top=172, InputMask=replicate('X',20)

  add object oStr_2_9 as StdString with uid="ZTSNSOQTRF",Visible=.t., Left=15, Top=66,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="AOSBWUIPJD",Visible=.t., Left=433, Top=66,;
    Alignment=1, Width=87, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="UOIUEAWLHZ",Visible=.t., Left=594, Top=66,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="RWMJRMEDIV",Visible=.t., Left=14, Top=123,;
    Alignment=1, Width=139, Height=18,;
    Caption="Utente archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="UPUJKOWOLN",Visible=.t., Left=61, Top=201,;
    Alignment=1, Width=92, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (! EMPTY(.w_SELCODCLA))
    endwith
  endfunc

  add object oStr_2_23 as StdString with uid="FUMAWTVIMS",Visible=.t., Left=328, Top=201,;
    Alignment=1, Width=71, Height=18,;
    Caption="Chiave:"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (! EMPTY(.w_SELCODCLA))
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="GOUGQHJPKP",Visible=.t., Left=394, Top=38,;
    Alignment=1, Width=126, Height=18,;
    Caption="Da data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="EUKZEDTDJI",Visible=.t., Left=594, Top=38,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="SNJWZYEFOB",Visible=.t., Left=1, Top=38,;
    Alignment=1, Width=152, Height=18,;
    Caption="Modalit� archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="IXSAKWGZDL",Visible=.t., Left=79, Top=13,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="PMCUVDXCHI",Visible=.t., Left=13, Top=172,;
    Alignment=1, Width=139, Height=18,;
    Caption="Utente esportazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="UJWMZMYRQP",Visible=.t., Left=581, Top=123,;
    Alignment=1, Width=38, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc

  add object oStr_2_40 as StdString with uid="MCUSLDBXBV",Visible=.t., Left=512, Top=123,;
    Alignment=1, Width=107, Height=18,;
    Caption="Pub. sul web:"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="XBHYHZAFYY",Visible=.t., Left=491, Top=176,;
    Alignment=1, Width=128, Height=18,;
    Caption="Stato archiv. web:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (.w_MODALL # 'I' AND g_DMIP#'S')
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="VKQYUJCKOU",Visible=.t., Left=588, Top=147,;
    Alignment=0, Width=31, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="RNRJKJZKEZ",Visible=.t., Left=107, Top=96,;
    Alignment=1, Width=46, Height=18,;
    Caption="Tipo file:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="VRZDMVVEMA",Visible=.t., Left=532, Top=98,;
    Alignment=1, Width=87, Height=18,;
    Caption="Ordina per:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kfg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kfg
Procedure Zoom_Select_color(pParent)
   local l_macro,l_i,l_count,olderror,mess
   dimension Colonne[2]
   l_count=0
   For l_i=1 to pParent.w_ZoomGF.nFields
    if  pParent.w_ZoomGF.cFields[l_i]=='GFDESCFILE'
       Colonne[1]=l_i
    endif
   Next
   olderror=on ('Error')
   on error mess='Errore'
   For l_i = 1 to 2
     If Colonne(l_i)>0
       l_macro=pParent.w_ZoomGF.grd.columns[Colonne(l_i)].dynamicforecolor
       pParent.w_ZoomGF.grd.columns[Colonne(l_i)].text1.DisabledForeColor=&l_macro
       pParent.w_ZoomGF.grd.columns[Colonne(l_i)].text1.enabled=.F.
       l_macro=pParent.w_ZoomGF.grd.columns[Colonne(l_i)].dynamicbackcolor
       pParent.w_ZoomGF.grd.columns[Colonne(l_i)].text1.DisabledBackColor=&l_macro
     Endif
   Next
   on error &olderror
Endproc

Procedure SearchingOn (pCaller)
  pCaller.NotifyEvent('Carica')
Endproc

Procedure AggiornaZoomCodPra(pParent)
   * -- Se ho selezionato una pratica esistente oppure ho azzerato la pratica
   if !Empty(pParent.w_CODPRAT) or (Empty(pParent.w_CODPRAT) and Empty(pParent.w_NEWCODPRA))
      * -- Esegue lo zoom
      GSUT_BES(pParent, pParent.w_ARCHIVIO, pParent.w_CHIAVE, 'QUERY2')
   else
      * -- Se ho digitato una pratica errata: Valore non ammesso!
      if !pParent.bDontReportError
         * -- Esegue lo zoom
         GSUT_BES(pParent, pParent.w_ARCHIVIO, pParent.w_CHIAVE, 'QUERY2')
      endif
   endif
Endproc

* --- Classe per gestire la combo TIPOFILE
* --- derivata dalla classe combo da tabella
define class StdZTamTableCombo as StdTableCombo

  proc Init()
    IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor=i_EBackColor
	  ENDIF
  endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    If Used(i_curs)
      Select (i_curs)
      This.nValues=Reccount()+1
      Dimension This.combovalues[MAX(1,this.nValues)]
      this.Clear
      i_bCharKey=Type(i_fk)='C'
      *this.AddItem(cp_Translate('<Nessun Valore>'))
      This.AddItem(' ')
      This.combovalues[1]=Iif(i_bCharKey,Space(1),0)
      Do While !Eof()
        This.AddItem(Iif(Type(i_fd)='C',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
        If i_bCharKey
          This.combovalues[recno()+1]=Trim(&i_fk)
        Else
          This.combovalues[recno()+1]=&i_fk
        Endif
        Skip
      Enddo
      Use
    Endif
enddefine
* --- Fine Area Manuale
